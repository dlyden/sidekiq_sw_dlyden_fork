def BUILD_CONFIGS = ['x86_64.gcc9.4.0_glibc2.17', 'x86_64.gcc', 'x86_64.gcc4.8_glibc2.11', 'x86_32.gcc4.8_glibc2.11', 'arm_cortex-a9.gcc4.8_gnueabihf_linux', 'arm_cortex-a9.gcc4.9.2_gnueabi', 'arm_cortex-a9.gcc5.2_glibc_openwrt', 'arm_cortex-a9.gcc7.2.1_gnueabihf', 'aarch64.gcc6.3']

def parallelBuildStagesMap = BUILD_CONFIGS.collectEntries {
    ["${it}" : generateBuildStage(it)]
}

def generateBuildStage(String BUILD_CONFIG) {
    return {
        dir(BUILD_CONFIG) {
            stage("stage: ${BUILD_CONFIG}") {
                script {
                    sh "cp -r ${WORKSPACE}/sidekiq_sw/* ${WORKSPACE}/${BUILD_CONFIG}/"
                    sh "make clean_all"
                    sh "make BUILD_CONFIG=${BUILD_CONFIG} V=yes -j8 --output-sync=recurse"
                    
                    def SUPPRESSIONS
                    if(fileExists('.cppcheck-suppressions.txt')) {
                        SUPPRESSIONS="--suppressions-list=.cppcheck-suppressions.txt"
                    }
                    
                    sh "echo 'using SUPPRESSIONS=$SUPPRESSIONS'"
                    if(BUILD_CONFIG=="x86_64.gcc") {
                        sh "cppcheck --enable=all --force --inconclusive $SUPPRESSIONS -i libraries/libiio/libraries/libiio/examples/ -i libraries/libiio/libraries/libiio/tests/ -i libraries/nw_logic_dma/dma_driver/DMADriver/TestCli/ --xml --xml-version=2 . 2> cppcheck.xml"
                    }
                }
            }
            if (BUILD_CONFIG ==~ /(x86_64.gcc|aarch64.gcc6.3|arm_cortex-a9.gcc5.2_glibc_openwrt|arm_cortex-a9.gcc7.2.1_gnueabihf)/) {
                stage("Trigger Regression: ${BUILD_CONFIG}") {
                    switch (BUILD_CONFIG) {
                        case "x86_64.gcc":
                            def jobs = ['Alpaca', 'Mule', 'Turkey', 'Buffalo', 'Honeybee', 'Worm', 'Giraffe', 'BillyGoat', 'Muskrat', 'Yak']
                            jobs.each { job ->
                                catchError(buildResult: "SUCCESS", stageResult: "SUCCESS") {
                                    build job: "/Sidekiq/SkiqRegression/${job}", wait: false, parameters: [[$class: "StringParameterValue", name: "GIT_REVISION", value: GIT_REVISION]]
                                }
                            }
                            break
                        case "aarch64.gcc6.3":
                            def jobs = ['Rabbit', 'Ibex', 'Zebra']
                            jobs.each { job ->
                                catchError(buildResult: "SUCCESS", stageResult: "SUCCESS") {
                                    build job: "/Sidekiq/SkiqRegression/${job}", wait: false, parameters: [[$class: "StringParameterValue", name: "GIT_REVISION", value: GIT_REVISION]]
                                }
                            }
                            break
                        case "arm_cortex-a9.gcc5.2_glibc_openwrt":
                            def jobs = ['Cow', 'Kangaroo']
                            jobs.each { job ->
                                catchError(buildResult: "SUCCESS", stageResult: "SUCCESS") {
                                    build job: "/Sidekiq/SkiqRegression/${job}", wait: false, parameters: [[$class: "StringParameterValue", name: "GIT_REVISION", value: GIT_REVISION]]
                                }
                            }
                            break
                        case "arm_cortex-a9.gcc7.2.1_gnueabihf":
                            def jobs = ['Mouse', 'DangerMouse']
                            jobs.each { job ->
                                catchError(buildResult: "SUCCESS", stageResult: "SUCCESS") {
                                    build job: "/Sidekiq/SkiqRegression/${job}", wait: false, parameters: [[$class: "StringParameterValue", name: "GIT_REVISION", value: GIT_REVISION]]
                                }
                            }
                            break
                        default:
                            echo "[WARNING] No builds have been setup for BUILD_CONFIG: ${BUILD_CONFIG}"
                    }
                }
            }
        }
    }
}

pipeline {
    agent { label 'sidekiq-testing' }
    stages {
        stage('Checkout SCM'){
            steps {
                dir('sidekiq_sw') {
                    checkout(
                        scm: [
                            $class: 'GitSCM', 
                            branches: [[name: GIT_REVISION ]],
                            extensions: [
                                [
                                    $class: 'SubmoduleOption',
                                    disableSubmodules: false,
                                    recursiveSubmodules: true
                                ],
                                [
                                    $class: 'CleanBeforeCheckout'    
                                ]
                            ],
                            userRemoteConfigs: [[credentialsId: 'b53c0433-63f3-4631-ae25-36707202f69a', url: 'git@bitbucket.org:epiq_solutions/sidekiq_sw.git']]
                        ]
                    )
                    script {
                        GIT_HASH = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                        currentBuild.displayName = "#$currentBuild.number: $GIT_REVISION[$GIT_HASH]"
                    }
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    parallel parallelBuildStagesMap
                }
            }
        }
    }
    post {
        always {
            scanForIssues tool: gcc()
            publishCppcheck pattern: '**/cppcheck.xml', allowNoReport: true
            script {
                def mailRecipients = "sidekiq-core-sw@epiq-solutions.com"
                def result = currentBuild.result.toLowerCase().capitalize()
                emailext from: "epiqbuildbot",
                    body: "$currentBuild.projectName - Build # $currentBuild.number - $result:<br><br>Check console output at $env.BUILD_URL to view the results.",
                    mimeType: 'text/html',
                    subject: "[Jenkins] $currentBuild.projectName - Build # $currentBuild.number - $result!",
                    to: "$mailRecipients"
            }
            deleteDir()
        }
    }
}
