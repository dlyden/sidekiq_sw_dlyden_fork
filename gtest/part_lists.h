/*! \file part_lists.h
 * \brief This file contains the lists defining the different
 * supporting features that any card in the libsidekiq test farm may
 * have.  The lists in this file will grow as necessary.  This file
 * also provides the functionality to determine whether or not a card
 * is in a supported list.
 *
 * <pre>
 * Copyright 2013-2022 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 8/2/2022     Ben   Created file
 *
 *</pre>*/

#pragma once

#include <set>
#include <unistd.h>

extern "C" {
    #include <sidekiq_api.h>
};

inline bool contains(std::set<int> valid_parts, int part) {
    return (std::find(valid_parts.begin(), valid_parts.end(), part) != valid_parts.end());
}

const std::set<int> SkiqGPSDO_supported_part_list = {skiq_m2_2280, skiq_z3u, skiq_nv100};
const std::set<int> SkiqRef_clock_select_unsupported_part_list = {skiq_m2, skiq_mpcie};
