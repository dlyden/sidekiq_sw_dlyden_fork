/*! \file test_read_warp_voltage.cpp
 * \brief This file tests the ability to read the
 * warp voltage value using the public API.
 *
 * <pre>
 * Copyright 2013-2022 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 8/2/2022     Ben   Created file
 *
 *</pre>*/

#include <iostream>
#include <gtest/gtest.h>

extern "C" {
    #include <sidekiq_api.h>
};

class SkiqReadWarpVoltage : public ::testing::Test
{
protected:
    uint8_t num_cards;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t card_under_test;

    uint16_t warp_voltage;
    uint32_t status;

    void SetUp() override
    {
        status = skiq_get_cards(skiq_xport_type_auto, &num_cards, cards);
        ASSERT_EQ(status, 0);
        ASSERT_GT(num_cards, 0);

        card_under_test = cards[0];
    }

    void TearDown() override
    {
        skiq_exit();
    }
};

TEST_F(SkiqReadWarpVoltage, NoInit) {
    status = skiq_read_tcvcxo_warp_voltage(card_under_test, &warp_voltage);
    ASSERT_EQ(status, -ENODEV);
}

TEST_F(SkiqReadWarpVoltage, BasicInit) {
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_basic, &card_under_test, 1);
    ASSERT_EQ(status, 0);

    status = skiq_read_tcvcxo_warp_voltage(card_under_test, &warp_voltage);
    ASSERT_EQ(status, -ENODEV);
}

TEST_F(SkiqReadWarpVoltage, FullInit) {
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card_under_test, 1);
    ASSERT_EQ(status, 0);

    status = skiq_read_tcvcxo_warp_voltage(card_under_test, &warp_voltage);
    ASSERT_EQ(status, 0);
}