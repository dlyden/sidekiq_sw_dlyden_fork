/*! \file card_capabilities.h
 * \brief This file contains the struct defining the different
 * capabilities that any card in the libsidekiq test farm may
 * have.  The properties in the struct will grow as time goes
 * on.  This file also provides the functionality to determine
 * what capabilities a card has.
 *
 * <pre>
 * Copyright 2013-2022 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 8/2/2022     Ben   Created file
 *
 *</pre>*/

#pragma once

#include <algorithm>
#include <set>
#include <string>
#include <vector>

extern "C" {
    #include <sidekiq_api.h>
};

// card_capabilities struct contains information to what a card is capable of being tested on.
// This struct is meant to grow as more need arises based on new tests
struct card_capabilities {
    // The cards will be identified based on their serial number.
    std::string serial_num;

    // Does the card have an internal PPS signal?
    bool has_int_pps;

    // Does the card have an external PPS signal connected to it?
    bool has_ext_pps;

    // Does the card have a GPS signal connected to it?
    bool has_gps;

    bool operator<(const card_capabilities& rhs) const
    {
        if(this->serial_num < rhs.serial_num) return true;
        else return false;
    }

    bool operator==(const card_capabilities& rhs) const
    {
        if(this->serial_num == rhs.serial_num) return true;
        else return false;
    }
};

const card_capabilities local = {
    .serial_num = "9Y1Z",
    .has_int_pps = false,
    .has_ext_pps = true,
    .has_gps = false
};

const card_capabilities giraffe = {
    .serial_num = "8N0F",
    .has_int_pps = false,
    .has_ext_pps = true,
    .has_gps = true
};

const card_capabilities ibex = {
    .serial_num = "8N02",
    .has_int_pps = false,
    .has_ext_pps = false,
    .has_gps = true
};

const card_capabilities yak = {
    .serial_num = "9Y83",
    .has_int_pps = false,
    .has_ext_pps = true,
    .has_gps = true
};

const card_capabilities zebra = {
    .serial_num = "9X03",
    .has_int_pps = false,
    .has_ext_pps = true,
    .has_gps = true
};

const std::set<card_capabilities> card_capabilities_mgr = {
    giraffe,
    ibex,
    yak,
    zebra,
    local
};

/* C++11 only allows a user to switch over integral types which means that
** you can evaluate a string literal.  However, taking advantage of the 
** constexpr feature, a user can evaluate a string at compile-time, replacing
** the string with a constant expression.  This allows the switch statement
** to evaluate the equality of two constant integral values rather than two
** literal strings.
**
** Explained in more detail: https://www.rioki.org/2016/03/31/cpp-switch-string.html
*/
inline constexpr unsigned int hash(const char* str, int h = 0) {
    return !str[h] ? 5381 : (hash(str, h+1) * 33 ) ^ str[h];
}

inline bool has_capabilities(std::string serial_num, std::vector<std::string> capabilities) {
    card_capabilities temp = {.serial_num = serial_num};
    auto it = std::find(card_capabilities_mgr.begin(), card_capabilities_mgr.end(), temp);
    bool has_capabilities = true;
    if(it != card_capabilities_mgr.end())
    {
        for(const std::string capability : capabilities) {
            switch (hash(capability.c_str()))
            {
            case hash("has_int_pps"):
                has_capabilities = has_capabilities && it->has_int_pps;
                break;

            case hash("has_ext_pps"):
                has_capabilities = has_capabilities && it->has_ext_pps;
                break;

            case hash("has_gps"):
                has_capabilities = has_capabilities && it->has_gps;
                break;
            
            default:
                break;
            }
            
        }
    }

    return has_capabilities;
}