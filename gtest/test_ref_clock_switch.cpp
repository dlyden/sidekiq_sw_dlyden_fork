/*! \file test_ref_clock_switch.cpp
 * \brief This file tests the ability to write the
 * reference clock configuration using the public API
 * without writing the configuration to EEPROM.
 *
 * <pre>
 * Copyright 2013-2022 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 8/2/2022     Ben   Created file
 *
 *</pre>*/

#include <iostream>
#include <gtest/gtest.h>

#include "part_lists.h"

extern "C" {
    #include <sidekiq_api.h>
};

class SkiqRefClockSwitch : public ::testing::Test
{
protected:
    uint8_t num_cards;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t card_under_test;

    skiq_ref_clock_select_t ref_clock;
    skiq_param_t param;
    skiq_part_t part;
    uint32_t status = 0;
    
    void SetUp() override
    {
        status = skiq_get_cards(skiq_xport_type_auto, &num_cards, cards);
        ASSERT_EQ(status, 0);
        ASSERT_GT(num_cards, 0);

        card_under_test = cards[0];
        status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card_under_test, 1);
        ASSERT_EQ(status, 0);

        status = skiq_read_parameters(card_under_test, &param);
        ASSERT_EQ(status, 0);

        part = param.card_param.part_type;
        if(contains(SkiqRef_clock_select_unsupported_part_list, part))
        {
            GTEST_SKIP();
        }
    }

    void TearDown() override
    {
        skiq_exit();
    }
};

TEST_F(SkiqRefClockSwitch, CurrentReference) {
    skiq_read_ref_clock_select(card_under_test, &ref_clock);
    status = skiq_write_ref_clock_select(card_under_test, ref_clock);
    ASSERT_EQ(status, 0);
}

int32_t crit_error_status;
int32_t expected_crit_error;
static void critErrorCallback(int32_t status, void *p_user_data) {
    (void) p_user_data;
    std::cout << "Warning: received critical error from libsidekiq (result code " << status << ")\n";
    crit_error_status = status;
}

TEST_F(SkiqRefClockSwitch, ExternalReferenceWithoutClock) {
    crit_error_status = 0;
    expected_crit_error = (part == skiq_nv100) ? -EPROTO : -EPERM;
    skiq_register_critical_error_callback(critErrorCallback, NULL);
    skiq_write_ref_clock_select(card_under_test, skiq_ref_clock_external);
    ASSERT_EQ(crit_error_status, expected_crit_error);
}

TEST_F(SkiqRefClockSwitch, Invalid) {
    status = skiq_write_ref_clock_select(card_under_test, skiq_ref_clock_invalid);
    ASSERT_EQ(status, -EINVAL);
}