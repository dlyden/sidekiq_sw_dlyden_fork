/*! \file test_gpsdo_lock.cpp
 * \brief This file tests the ability to obtain a
 * lock in the GPSDO algorithm using the public API.
 * The tests include support of GPSDO, Lock Testing,
 * Lock persistence testing, and Lock Accuracy.
 *
 * <pre>
 * Copyright 2013-2022 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 8/2/2022     Ben   Created file
 *
 *</pre>*/

#include <iostream>
#include <gtest/gtest.h>
#include <string>
#include <vector>

#include "card_capabilities.h"
#include "part_lists.h"

extern "C" {
    #include <sidekiq_api.h>
};

class SkiqGPSDOLock : public ::testing::Test
{
protected:
    uint8_t num_cards;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    skiq_param_t param;
    int part = skiq_part_invalid;
    uint8_t card_under_test;
    std::string card_under_test_serial;
    std::vector<std::string> gpsdo_ext_pps_requirements {"has_ext_pps"};

    bool is_locked = false;
    int timeout;
    // the sleep_secs variable (in seconds) was determined by repeated testing to allow the lock
    // algorithm to go unstable and then become stable again
    int sleep_secs = 5;
    int usleep_poll_30Hz = 33333;
    skiq_gpsdo_support_t is_supported = skiq_gpsdo_support_unknown;
    uint32_t status;

    void SetUp() override
    {
        status = skiq_get_cards(skiq_xport_type_auto, &num_cards, cards);
        ASSERT_EQ(status, 0);
        ASSERT_GT(num_cards, 0);

        card_under_test = cards[0];

        status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card_under_test, 1);
        ASSERT_EQ(status, 0);

        status = skiq_read_parameters(card_under_test, &param);
        ASSERT_EQ(status, 0);

        part = param.card_param.part_type;
        card_under_test_serial = param.card_param.serial_string;
        timeout = 120;
    }

    void TearDown() override
    {
        skiq_exit();
    }
};

TEST_F(SkiqGPSDOLock, GPSDOIsSupported) {
    // test whether or not GPSDO is supported and if not exit early 
    status = skiq_is_gpsdo_supported(card_under_test, &is_supported);
    ASSERT_EQ(status, 0);

    if(!contains(SkiqGPSDO_supported_part_list, part))
    {
        ASSERT_EQ(is_supported, skiq_gpsdo_support_card_not_supported);
    }
    else
    {
        ASSERT_EQ(is_supported, skiq_gpsdo_support_is_supported);
    }
}

TEST_F(SkiqGPSDOLock, GPSDOExtPPSLock) {
    // confirm support for test before continuing -- if not skip
    if((!contains(SkiqGPSDO_supported_part_list, part)) || (!has_capabilities(card_under_test_serial, gpsdo_ext_pps_requirements)))
    {
        GTEST_SKIP();
    }

    // enable GPSDO
    status = skiq_gpsdo_enable(card_under_test);
    ASSERT_EQ(status, 0);

    // change the PPS source to external
    status = skiq_write_1pps_source(card_under_test, skiq_1pps_source_external);
    ASSERT_EQ(status, 0);

    while(!is_locked && timeout > 0)
    {
        status = skiq_gpsdo_is_locked(card_under_test, &is_locked);
        ASSERT_EQ(status, 0);
        if(!is_locked) {
            sleep(1);
            timeout--;
        }
    }

    // confirm that the lock did not timeout
    ASSERT_GT(timeout, 0);

    // disable GPSDO
    status = skiq_gpsdo_disable(card_under_test);
    ASSERT_EQ(status, 0);

    // confirm that GPSDO no longer has a lock
    status = skiq_gpsdo_is_locked(card_under_test, &is_locked);
    ASSERT_EQ(status, 0);
    ASSERT_FALSE(is_locked);
}

TEST_F(SkiqGPSDOLock, GPSDOExtPPSLockAccuracy) {
    // confirm support for test before continuing -- if not skip
    if((!contains(SkiqGPSDO_supported_part_list, part)) || (!has_capabilities(card_under_test_serial, gpsdo_ext_pps_requirements)))
    {
        GTEST_SKIP();
    }

    // enable GPSDO
    status = skiq_gpsdo_enable(card_under_test);
    ASSERT_EQ(status, 0);

    // change the PPS source to external
    status = skiq_write_1pps_source(card_under_test, skiq_1pps_source_external);
    ASSERT_EQ(status, 0);

    while(!is_locked && timeout > 0)
    {
        status = skiq_gpsdo_is_locked(card_under_test, &is_locked);
        ASSERT_EQ(status, 0);
        if(!is_locked) {
            sleep(1);
            timeout--;
        }
    }

    // confirm that the lock did not timeout
    ASSERT_GT(timeout, 0);

    // test the accuracy of the GPSDO lock
    double gpsdo_freq_ppm = 0;
    double cumulative_average = 0;
    int polling_time = 20;
    int polling_count = 0;

    uint64_t rf_ts = 0;
    uint64_t prev_rf_ts = 0;
    uint64_t sys_ts = 0;
    uint64_t prev_sys_ts = 0;

    while(polling_count < polling_time)
    {
        status = skiq_gpsdo_is_locked(card_under_test, &is_locked);
        ASSERT_EQ(status, 0);
        ASSERT_TRUE(is_locked);

        status = skiq_read_last_1pps_timestamp(card_under_test, &rf_ts, &sys_ts);
        ASSERT_EQ(status, 0);
        
        if((prev_rf_ts != rf_ts) || (prev_sys_ts != sys_ts))
        {
            prev_rf_ts = rf_ts;
            prev_sys_ts = sys_ts;
            polling_count++;

            status = skiq_gpsdo_read_freq_accuracy(card_under_test, &gpsdo_freq_ppm);
            ASSERT_EQ(status, 0);


            //A bufferless cumulative moving average. Equivalent to storing all values and 
            //summming then dividing by n.
            cumulative_average = (gpsdo_freq_ppm + cumulative_average * (polling_count - 1)) / polling_count;

            ASSERT_LT(std::abs(cumulative_average), 1);
            ASSERT_LT(std::abs(gpsdo_freq_ppm), 1);
        }
        else
        {
            usleep(usleep_poll_30Hz);
        }
    }

    // disable GPSDO
    status = skiq_gpsdo_disable(card_under_test);
    ASSERT_EQ(status, 0);
}

TEST_F(SkiqGPSDOLock, GPSDOExtPPSLockPersistence) {
    // confirm support for test before continuing -- if not skip
    if((!contains(SkiqGPSDO_supported_part_list, part)) || (!has_capabilities(card_under_test_serial, gpsdo_ext_pps_requirements)))
    {
        GTEST_SKIP();
    }

    // test whether or not GPSDO is supported and if not exit early 
    status = skiq_is_gpsdo_supported(card_under_test, &is_supported);
    ASSERT_EQ(status, 0);

    if(!contains(SkiqGPSDO_supported_part_list, part))
    {
        ASSERT_EQ(is_supported, skiq_gpsdo_support_card_not_supported);
    } 
    else
    {
        ASSERT_EQ(is_supported, skiq_gpsdo_support_is_supported);

        // enable GPSDO
        status = skiq_gpsdo_enable(card_under_test);
        ASSERT_EQ(status, 0);

        // change the PPS source to external
        status = skiq_write_1pps_source(card_under_test, skiq_1pps_source_external);
        ASSERT_EQ(status, 0);

        while(!is_locked && timeout > 0)
        {
            status = skiq_gpsdo_is_locked(card_under_test, &is_locked);
            ASSERT_EQ(status, 0);
            if(!is_locked) {
                sleep(1);
                timeout--;
            }
        }

        // confirm that the lock did not timeout
        ASSERT_GT(timeout, 0);

        // Test the reset persistence of the GPSDO lock by allowing the algorithm to become unstable
        // after a reset and then verifying that it stops oscillating in and out of a lock and becomes
        // stable after a re-init
        for(int i=0; i<10; i++) {
            skiq_exit();
            // sleep to give the control algorithm enough time to drift and become unstable
            sleep(sleep_secs);

            status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card_under_test, 1);            
            ASSERT_EQ(status, 0);
            // sleep to allow for the system to stop oscillating in and out of lock
            sleep(sleep_secs);

            status = skiq_gpsdo_is_locked(card_under_test, &is_locked);
            ASSERT_EQ(status, 0);
            ASSERT_EQ(is_locked, true);
        }

        // disable GPSDO
        status = skiq_gpsdo_disable(card_under_test);
        ASSERT_EQ(status, 0);
    }
}