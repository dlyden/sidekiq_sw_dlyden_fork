#/bin/bash

if test -z "$1"
then
    echo "Error: no date specified (must be YYYYMMDD)"
    exit -1
fi

sidekiq_sw=${PWD}
githash=$(git rev-parse HEAD)
snapshot_date=$1
standalone_dir=$sidekiq_sw/sidekiq_snapshot_$snapshot_date-$githash

mkdir $standalone_dir

# build up the directory structure
cd $standalone_dir

# top level files
echo "copy top level files"
cp $sidekiq_sw/API_INTERFACE $standalone_dir
cp $sidekiq_sw/Makefile $standalone_dir

# create and copy build dir
echo "creating build dir"
mkdir build
cp -r $sidekiq_sw/build/* build/
echo "updating build scripts"
cd build
cp tools.mk__platform_clean tools.mk
rm tools.mk__platform_clean
cp top.mk__platform_clean top.mk
rm top.mk__platform_clean
cp common_rules.mk__NO_GIT common_rules.mk
rm common_rules.mk__NO_GIT
rm changelog
rm hardware.mk

# common_inc
# TODO: How do we capture snapshot of firmware?
echo "creating common_inc"
cd $standalone_dir
mkdir $standalone_dir/common_inc/
cp -r $sidekiq_sw/sidekiq_fw/inc/* $standalone_dir/common_inc/

# libsidekiq
mkdir host_code
mkdir host_code/libsidekiq

# ad9361_driver
echo "creating ad9361_driver"
cp -r $sidekiq_sw/host_code/libsidekiq/ad9361_driver $standalone_dir/host_code/libsidekiq/
rm -rf $sidekiq_sw/host_code/libsidekiq/ad9361_driver/.git

# core libsidekiq
echo "creating core libsidekiq"
mkdir host_code/libsidekiq/lib
cp $sidekiq_sw/host_code/libsidekiq/Makefile $standalone_dir/host_code/libsidekiq/

# sidekiq_core
echo "creating sidekiq_core"
mkdir host_code/libsidekiq/sidekiq_core
cp -r $sidekiq_sw/host_code/libsidekiq/sidekiq_core/ $standalone_dir/host_code/libsidekiq/

# test_apps
echo "creating test_apps"
mkdir $standalone_dir/host_code/libsidekiq/test_apps
mkdir $standalone_dir/host_code/libsidekiq/test_apps/src
mkdir $standalone_dir/host_code/libsidekiq/test_apps/bin
cp -r $sidekiq_sw/host_code/libsidekiq/test_apps/Makefile $standalone_dir/host_code/libsidekiq/test_apps/
cp -r $sidekiq_sw/host_code/libsidekiq/test_apps/src/*.c  $standalone_dir/host_code/libsidekiq/test_apps/src/

# cleanup nw_logic
echo "cleaning up nw_logic_dma repo"
rm -rf $standalone_dir/host_code/libsidekiq/sidekiq_core/nw_logic_dma/.git
rm -rf $standalone_dir/host_code/libsidekiq/sidekiq_core/nw_logic_dma/dma_driver/DMADriver/Linux/releases
rm -rf $standalone_dir/host_code/libsidekiq/sidekiq_core/nw_logic_dma/dma_driver/DMADriver/Linux/src/*-*

# cleanup pci_manager
echo "cleaning up pci_manager repo"
rm -rf $standalone_dir/host_code/libsidekiq/sidekiq_core/pci_manager/.git
rm -rf $standalone_dir/host_code/libsidekiq/sidekiq_core/pci_manager/driver/releases*
rm -rf $standalone_dir/host_code/libsidekiq/sidekiq_core/pci_manager/driver/*-*
