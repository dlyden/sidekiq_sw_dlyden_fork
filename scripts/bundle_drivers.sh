#! /bin/bash

if [ $# -lt 1 ]; then
    echo "usage: $(basename $(readlink -f $0)) <version> [ <version> ... ]" >&2
    exit 1
fi

D=$(dirname $(readlink -f $0))

PCI=$(readlink -f $D/../libraries/pci_manager/driver)
DMA=$(readlink -f $D/../libraries/nw_logic_dma/dma_driver/DMADriver/Linux/src)

TAG=$(git describe --tags)

for VER; do
    if [ -d $PCI/$VER -a -d $DMA/$VER ]; then
        FN=linux_kernel-$VER-for-$TAG.tar.xz

        # create a temporary directory, copy both modules there, tar it up
        MKT=$(mktemp -d)
        mkdir $MKT/linux_kernel-$VER
        cp $PCI/$VER/pci_manager.ko $DMA/$VER/dmadriver.ko $DMA/$VER/skiq_platform_device.ko $MKT/linux_kernel-$VER
        XZ_OPT=-9ev tar --owner=root --group=root -cJf $FN -C $MKT linux_kernel-$VER
        rm -rf $MKT

        tar tvf $FN
        echo "  bundled drivers in '$FN'" >&2
    else
        [ -d $PCI/$VER ] || echo "$PCI/$VER is missing!"
        [ -d $DMA/$VER ] || echo "$DMA/$VER is missing!"
    fi
done
