#!/bin/bash
#

# THE DEFAULTS INITIALIZATION - POSITIONALS
_positionals=()
_arg_directory="/tmp/regression"

die()
{
    local _ret=$2
    test -n "$_ret" || _ret=1
    test "$_PRINT_HELP" = yes && print_help >&2
    echo "$1" >&2
    exit ${_ret}
}

print_help ()
{
    printf '%s\n' "Run a network transport regression test using the latest code (assuming client is x86_64)"
    printf '\t%s\n' "<server>: server target user@host"
}

_arg_card=""
_arg_serial=""

parse_commandline ()
{
    while test $# -gt 0
    do
        _key="$1"
        case "$_key" in
            -h|--help)
                print_help
                exit 0
                ;;
            -h*)
                print_help
                exit 0
                ;;
            -c|--card)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_card="$2"
                shift
                ;;
            --card=*)
                _arg_card="${_key##--card=}"
                ;;
            -c*)
                _arg_card="${_key##-c}"
                ;;
            -s|--serial)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_serial="$2"
                shift
                ;;
            --serial=*)
                _arg_serial="${_key##--serial=}"
                ;;
            -s*)
                _arg_serial="${_key##-s}"
                ;;
            *)
                _positionals+=("$1")
                ;;
        esac
        shift
    done

}

handle_passed_args_count ()
{
    _required_args_string="'remote'"
    test ${#_positionals[@]} -ge 1 || _PRINT_HELP=yes die "FATAL ERROR: Not enough positional arguments - we require between 1 and 2 (namely: $_required_args_string), but got only ${#_positionals[@]}." 1
    test ${#_positionals[@]} -le 2 || _PRINT_HELP=yes die "FATAL ERROR: There were spurious positional arguments --- we expect between 1 and 2 (namely: $_required_args_string), but got ${#_positionals[@]} (the last one was: '${_positionals[*]: -1}')." 1
}

assign_positional_args ()
{
    _positional_names=('_arg_remote' '_arg_directory')

    for (( ii = 0; ii < ${#_positionals[@]}; ii++))
    do
    eval "${_positional_names[ii]}=\${_positionals[ii]}" || die "Error during argument parsing, possibly an Argbash bug." 1
    done
}

parse_commandline "$@"
handle_passed_args_count
assign_positional_args

REMOTE="$_arg_remote"

# TARGET_DIR and TARGET_DIR_PRIVATE are used to preserve the original directory for use on the target machine when needed
TARGET_DIR="$_arg_directory"
TARGET_DIR_PRIVATE=$TARGET_DIR

# DIR and DIR_PRIVATE are set here for tests that get run before and after those variables get overwritten
DIR=$TARGET_DIR
DIR_PRIVATE=$DIR

GIT_TAG=$(git describe --tags)
GIT_TAG=${GIT_TAG:-unknown}

HASH=$(echo "$REMOTE" | openssl enc -base64 | tr -d '=')
SSH="ssh -o ControlPath=$HASH.socket -o ControlMaster=auto -o ControlPersist=600"
SCP="scp -o ControlPath=$HASH.socket -o ControlMaster=auto -o ControlPersist=600"

SERVER_HOSTNAME=$($SSH "$REMOTE" "uname -n")

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
ROOT_DIR=$(dirname "$(readlink -f "$SCRIPT_DIR/")")

_arg_directory="/tmp/regression"

APP_DIR=$ROOT_DIR/host_code/libsidekiq/test_apps/bin/x86_64.gcc
APP_DIR_PRIVATE=$APP_DIR
APP_SUFFIX=""

SKIQ_DAEMON="skiq_daemon"

SUDO=""
RC=""
TS=$(date +'%Y%m%d-%H%M%S')

if [[ -n "$JENKINS_HOME" ]]; then
    echo "################################################################################"
    echo "# WARNING: Detected Jenkins build, not logging"
    echo "################################################################################"
    echo
else
    echo "################################################################################"
    echo "# INFO: Logging regression test output in 'log-net-regression-${SERVER_HOSTNAME}-${REMOTE}-${TS}.txt'"
    echo "################################################################################"
    echo

    # Redirect stdout ( > ) into a named pipe ( >() ) running "tee"
    exec > >(tee -i "log-net-regression-${SERVER_HOSTNAME}-${REMOTE}-${TS}.txt")

    # Without this, only stdout would be captured - i.e. your
    # log file would not contain any error messages.
    # SEE (and upvote) the answer by Adam Spiers, which keeps STDERR
    # as a separate stream - I did not want to steal from him by simply
    # adding his answer to mine.
    exec 2>&1
fi

echo "################################################################################"
echo "# REGRESSION RUN"
echo "# $0 $@"
echo "# Timestamp: $TS"
echo "# Remote Tests: ${REMOTE_TESTS:-none specified}"
echo "################################################################################"
echo

if [[ $SERVER_HOSTNAME == "sidekiqz2" ]] ; then        
    CFG="arm_cortex-a9.gcc7.2.1_gnueabihf"
    PKILL="killall"
    echo "Got Z2"
elif [[ $SERVER_HOSTNAME == "z3u" ]] ; then    
    CFG="aarch64.gcc6.3"
    PKILL="pkill"
    echo "Got Z3u"
else
    echo "Unknown target platform ($SERVER_HOSTNAME)!" 
    exit 1
fi

REMOTE_THINGS="$ROOT_DIR/host_code/libsidekiq/test_apps/bin/$CFG/$SKIQ_DAEMON"

$SSH -t "$REMOTE" "rm -rf ${TARGET_DIR:?}/* &&
                   [ ! -d $TARGET_DIR ] &&
                   echo 'directory $TARGET_DIR does not exist, creating' &&
                   mkdir -p $TARGET_DIR"

echo "Copying daemon to target"
if [[ -n "$JENKINS_HOME" ]]; then
    shopt -s nullglob
    $SCP -r $REMOTE_THINGS $REMOTE:$TARGET_DIR
else
    script -qc "shopt -s nullglob; $SCP -r $REMOTE_THINGS $REMOTE:$TARGET_DIR" /dev/null
fi

################################################################################
mkdir -p $TARGET_DIR
cp $APP_DIR/* $TARGET_DIR

start_skiq_daemon()
{
    local rc
    SERVER_IP_ADDR=$(echo $REMOTE | cut -f2 -d@)
    export SKIQ_DAEMON_IP=$SERVER_IP_ADDR
    export SKIQ_DAEMON_PORT=3417

    ################################################################################
    # TODO: This is probably a horrible way to do this, but I'm not sure of another way...all those script gurus please help
    echo "starting daemon on server"
    $SSH -t "$REMOTE" "cd $TARGET_DIR; ./$SKIQ_DAEMON" &
    ################################################################################

    # wait a bit for daemon to startup
    sleep 5

    # poll the card until skiq_daemon is ready to go
    for i in $(seq 20); do
        version_test
        rc=$?
        if [[ $rc -eq 0 ]] ; then
            break
        fi
    done

    if [[ $rc -ne 0 ]] ; then
        echo "Timed out waiting on version_test to succeed"
        cleanup
        exit 1
    fi
}

################################################################################
# Stolen from skiq_z2_regression.sh
run_test()
{
    local t rc

    if [[ "$DRY_RUN" == "true" ]]; then
        echo "+ $*"
    else
        echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
        echo "+ $*"
        t=$(type -f $1)
        if [ x"${t:0:1}" = x"/" ]; then
            time "$@"
            rc=$?
        else
            "$@"
            rc=$?
        fi
        if [ $rc -ne 0 ]; then
            echo "+ test failed with status=$rc"

            # attempt to print out the version for debugging purposes
            if [[ $CARD ]] && [[ $MODE ]] && [[ $DIR ]] ; then
                echo "+ grabbing version info of failed regression..."
                $DIR/version_test$APP_SUFFIX -c $CARD
            fi

            echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
            echo
            echo "-----------------------------------------------------------"
            echo "---------------- TESTS EXECUTION SUMMARY ------------------"
            for t in $TEST_PASS; do
                echo "  * [PASS] $t"
            done
            echo "  * [FAIL] $TEST_RUN"
            echo "-----------------------------------------------------------"

            ################################################################################
            echo "stopping daemon on server"
            $SSH -t "$REMOTE" "$PKILL $SKIQ_DAEMON"
            ################################################################################

            ################################################################################
            # perform remote clean-up
            $SSH -t "$REMOTE" "rm -rf ${DIR}"
            ################################################################################

            exit $rc
        fi
        echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
        echo
    fi
}

#
# echos the minimum of two values
#
minimum()
{
    local A B
    A=$1
    B=$2
    echo $(( A < B ? A : B ))
}

#
# echos the maximum of two values
#
maximum()
{
    local A B
    A=$1
    B=$2
    echo $(( A > B ? A : B ))
}

#
# returns a sequence of NUM rates between START and STOP
#
rate_list()
{
    local START STOP NUM STEP
    START=$1
    STOP=$2
    NUM=$3
    STEP=$(( ( STOP - START ) / NUM ))

    if [[ $STEP -le 0 ]]; then
        echo $STOP
    else
        seq $START $STEP $STOP
    fi
}

#
# given a number of bytes and an output file path, verify if the output file size is correct
#
_verify_file_size()
{
    local BYTES OUTPUT_FN EXPECTED PRESENTLY
    BYTES=$1
    OUTPUT_FN=$2
    EXPECTED=$BYTES
    PRESENTLY=$(ls -la "$OUTPUT_FN" | awk '{ print $5 }')

    if [[ $PRESENTLY -eq $EXPECTED ]]; then
        echo "output size verified, expected $EXPECTED bytes in $OUTPUT_FN, found $PRESENTLY bytes" >&2
        return 0
    else
        echo "output size NOT verified, expected $EXPECTED bytes in $OUTPUT_FN, found $PRESENTLY bytes" >&2
        return 1
    fi
}

#
# given a number of samples and an output file path, verify if the output file size is correct for
# unpacked mode
#
verify_output_file_size()
{
    local WORDS OUTPUT_FN
    WORDS=$1
    OUTPUT_FN=$2

    _verify_file_size "$((WORDS * 4))" "$OUTPUT_FN"
}

rfic_rw_test()
{
    run_test $DIR_PRIVATE/rfic_rw_test$APP_SUFFIX $CARD 25000
}

i2c_rw_test()
{
    run_test $DIR_PRIVATE/i2c_rw_test$APP_SUFFIX $CARD 1000
}

fpga_rw_test()
{
    run_test $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 25000
    run_test $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 25000 --64bit=aligned

    # Earlier BSPs don't have an FPGA that supports the register used in unaligned testing
    CMD="grep ^z2-sw-version /opt/VERSIONS"
    SW_VER=$($CMD | /usr/bin/awk '{print $2}')
    case $SW_VER in
        v3.*)
            run_test $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000 --64bit=unaligned
            ;;
    esac
}

user_reg_verify()
{
    run_test $DIR_PRIVATE/user_reg_verify$APP_SUFFIX $CARD
}

timestamp_reset()
{
    if [[ $HAS_PPS == "true" ]] && [[ $HAS_PCIE == "true" ]] ; then
        run_test $DIR/timestamp_reset$APP_SUFFIX $CARD 1 0
    elif [[ $HAS_USB == "false" ]] ; then
        # timestamp reset for USB needs to be investigated, likely just needs
        # a more generous timestamp check
        run_test $DIR/timestamp_reset$APP_SUFFIX $CARD 0 0
    fi
}

rx_benchmark()
{
    local ARGS
    ARGS="-c $CARD --handle=A1 --time=5 --threshold=200 -r $MAX_SAMPLE_RATE --target=$(((MAX_SAMPLE_RATE - 1000000) * 4 / 1000000))"

    run_test $DIR/rx_benchmark$APP_SUFFIX $ARGS
}

_rx_samples()
{
    local RATES ARGS
    ARGS="-c $CARD -d $OUTDIR/out --counter $*"

    # test 8 rates between 1e6 and max sample rate
    RATES=$(rate_list 1000000 $MAX_SAMPLE_RATE 8)

    for RATE in $RATES ; do
        # test rx for A1, first unpacked, then packed
        run_test $DIR/rx_samples$APP_SUFFIX $ARGS -w $RATE -r $RATE --handle=A1 --retries-on-ts-err=10
        run_test verify_output_file_size $RATE $OUTDIR/out.a1
    done
}

rx_samples()
{
    local SAVE_MAX_SAMPLE_RATE
    SAVE_MAX_SAMPLE_RATE=$MAX_SAMPLE_RATE

    # specify receive gain based on hardware type
    if [[ "$SKIQ_HW" == "X2" ]]; then
        GAIN="-g 237"
    else
        GAIN="-g 40"
    fi

    # call _rx-samples test with an additional option if blocking RX is available
    _rx_samples $GAIN
}

prbs_test()
{
    if [[ "$SKIQ_HW" == "X2" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for PRBS test" >&2
    else
        local ARGS
        ARGS="-c $CARD --time=3 -d $OUTDIR/prbs_results.txt --start=1000000 --step=5000000"

        run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000
        if [[ $HAS_2CHAN == "true" ]] ; then
            if [[ "$SKIQ_HW" == "M2" ]]; then
                run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000 --channels=2
            else
                run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=30720000 --channels=2
            fi
        fi
    fi
}

test_fir_api()
{
    if [[ "$SKIQ_HW" == "X2" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for custom FIR coefficients" >&2
    else
        local RATES
        RATES="233000 13300000 23000000 40000000 46000000"

        # Note: this doesn't test thruput, so sample rate range doesn't matter (neither does BW)
        for RATE in $RATES; do
            run_test $DIR/test_fir_api$APP_SUFFIX -c $CARD -r $RATE
        done

        if [[ $HAS_2TXCHAN == "true" ]] ; then
            for RATE in $RATES; do
                run_test $DIR/test_fir_api$APP_SUFFIX -c $CARD -r $RATE -h A2
            done
        fi
    fi
}

timestamp_test()
{
    if [[ "$SKIQ_HW" == "X2" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for timestamp test (yet)" >&2
    else
        local START STEP STOP ARGS
        START=1000000
        STEP=$(( ( MAX_SAMPLE_RATE - START ) / 25 ))
        STOP=$(( MAX_SAMPLE_RATE + 1 ))
        ARGS="--start=$START --blocks=20000 -c $CARD"

        run_test $DIR/timestamp_test$APP_SUFFIX $ARGS --stop=$STOP --step=$STEP --handle=A1
        if [[ $HAS_2CHAN == "true" ]] ; then
            STEP=$(( ( ( MAX_SAMPLE_RATE / 2 ) - START ) / 25 ))
            STOP=$(( ( MAX_SAMPLE_RATE / 2 ) + 1 ))
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS --stop=$STOP --step=$STEP --handle=A2
        fi
    fi
}

timestamp_read_test()
{
    local ARGS
    ARGS="-c $CARD --verbose -n 10000"

    run_test $DIR/timestamp_read_test$APP_SUFFIX $ARGS
}

sweep_receive()
{
    local ARGS REPEAT START STOP STEP RATE
    REPEAT=20

    if [[ $HAS_DKIQ == "true" ]] ; then
        START=20000000
        STOP=900000000
    else
        START=800000000
        STOP=900000000
    fi
    STEP=$(( (STOP-START) / 10 ))

    if [[ "$SKIQ_HW" == "X2" ]]; then
        RATE=153600000
    else
        RATE=$(( (MAX_SAMPLE_RATE / 4) * 3 ))
    fi
    ARGS="-c $CARD -r $RATE --start $START --stop $STOP --step $STEP  --repeat $REPEAT"

    run_test $DIR/sweep_receive$APP_SUFFIX $ARGS
    #run_test $DIR/sweep_receive$APP_SUFFIX $ARGS --blocking
}

test_ref_clock_switch()
{
    local ARGS
    ARGS="-c $CARD"

    run_test $DIR/test_ref_clock_switch$APP_SUFFIX $ARGS
}

read_accel()
{
    run_test $DIR/read_accel$APP_SUFFIX -c $CARD --repeat 10
}

read_temp()
{
    run_test $DIR/read_temp$APP_SUFFIX -c $CARD --repeat 10
    run_test $DIR/read_temp_factory$APP_SUFFIX -c $CARD --repeat 10
}

init_and_exit()
{
    run_test $DIR/init_and_exit$APP_SUFFIX -c $CARD --sleep-duration=1 --repeat 4
    run_test $DIR/init_and_exit$APP_SUFFIX -c $CARD --sleep-duration=1 --repeat 4 --full
}

version_test()
{
    if [[ -z $CARD ]] ; then
        run_test $DIR/version_test$APP_SUFFIX
    else
        run_test $DIR/version_test$APP_SUFFIX -c $CARD
    fi
}

version_test_full()
{
    run_test $DIR/version_test$APP_SUFFIX -c $CARD --full
}

tx_benchmark()
{
    ######################################################################
    # Synchronous Transmit
    ######################################################################

    local ARGS
    ARGS="-c $CARD --time=5 --threshold=200"

    run_test $DIR/tx_benchmark$APP_SUFFIX -c $CARD --time=5 --threshold=200 -r $MAX_TX_SAMPLE_RATE --target=$(((MAX_TX_SAMPLE_RATE-1000000) * 4 / 1000000)) --block-size=65532
}

card_mgr_uninit()
{
    echo "..........................................................."
    echo "                   Card Mgr Uninit"

    if [[ $PLATFORM != $WIN64_MSYS ]] ; then
        local rc

        run_test $DIR_PRIVATE/card_mgr_uninit$APP_SUFFIX
        rc=$?
        if [[ $rc -ne 0 ]] ; then
            exit 1
        fi
    fi
}

detect_sidekiq()
{
    local SKP
    echo "..........................................................."
    echo "                   Detecting Sidekiq"

    SKP=$DIR_PRIVATE/sidekiq_probe$APP_SUFFIX

    if [[ ! -e "$SKP" ]] ; then
        echo "ERROR: $SKP not found!"
        exit 1
    elif [[ ! -x "$SKP" ]] ; then
        echo "ERROR: $SKP not executable!"
        exit 1
    fi

    CARD=$($SKP --serial $SERIAL --fmtstring=%c)
    if [[ -z $CARD ]] ; then
        echo "No card found with serial number: $SERIAL"
        cleanup
        exit 1
    fi
}

cleanup()
{
    ################################################################################
    echo "stopping daemon on server"
    $SSH -t "$REMOTE" "$PKILL $SKIQ_DAEMON"

    ################################################################################

    ################################################################################
    # perform remote clean-up
    $SSH -t "$REMOTE" "rm -rf ${TARGET_DIR}"
    ################################################################################

    # stop ControlMaster
    $SSH -O stop -S $HASH.socket $REMOTE
}

set_test_parameters()
{
    # set global test parameters
    if [[ $SERVER_HOSTNAME == "sidekiqz2" ]] ; then        
        MAX_SAMPLE_RATE=4000000
        MAX_TX_SAMPLE_RATE=3000000
        HAS_2CHAN="false"
    elif [[ $SERVER_HOSTNAME == "z3u" ]] ; then
        MAX_SAMPLE_RATE=15000000
        MAX_TX_SAMPLE_RATE=20000000
        # TODO: get 2 chan working for Z3u
        #HAS_2CHAN="true"
        HAS_2CHAN="false"
    else
        echo "Unknown target platform ($SERVER_HOSTNAME)!" 
        exit 1
    fi

    HAS_PPS="false"

    if [[ -n $_arg_card ]] && [[ -n $_arg_serial ]] ; then
        echo "Error: Cannot specify both card and serial number"
        cleanup
        exit 1
    elif [[ -n $_arg_serial ]] ; then
        SERIAL="$_arg_serial"
    elif [[ -n $_arg_card ]] ; then
        CARD="$_arg_card"
    else
    # default card to 0 if nothing was set
    : ${CARD: 0}
    fi
}

exiting()
{
    echo -e "\n\nInfo: Performing cleanup before exiting:"
    cleanup
    echo "Info: Exiting"
    exit 1
}

trap exiting SIGINT SIGTERM

if [ $# -gt 0 ]; then
    if [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
        print_help
        exit 0
    fi
fi

# before any detection, we need to know the directory to find our test applications

# default to running tests, not just displaying what would run
: ${DRY_RUN:=false}

if [[ -n "$APP_DIR" ]] ; then
    DIR=$(readlink -f "$APP_DIR")
fi

if [[ -n "$APP_PRIVATE_DIR" ]] ; then
    DIR_PRIVATE=$(readlink -f "$APP_PRIVATE_DIR")
fi

if [[ -n "$APP_DATA_DIR" ]] ; then
    DATA_DIR=$(readlink -f "$APP_DATA_DIR")
fi

# define where output files go during testing
OUTDIR=$DIR/tmp_output
RESULTS=$DIR/results
mkdir -p "$OUTDIR" "$RESULTS"

################################################################################

start_skiq_daemon
set_test_parameters
if [[ -n $SERIAL ]] ; then
    detect_sidekiq
fi
card_mgr_uninit


################################################################################
# Tests to run
################################################################################
unset TEST

TEST="$TEST rfic_rw_test"
TEST="$TEST i2c_rw_test"
TEST="$TEST fpga_rw_test"
TEST="$TEST user_reg_verify"
TEST="$TEST timestamp_reset"
TEST="$TEST rx_benchmark"
TEST="$TEST tx_benchmark"
TEST="$TEST rx_samples" # TODO - add counter (LIB-213)
TEST="$TEST prbs_test"
TEST="$TEST timestamp_test"
TEST="$TEST sweep_receive" 
#TEST="$TEST test_ref_clock_switch" #skiq_write_ref_clock_select unsupported on net transport
TEST="$TEST read_temp"
TEST="$TEST init_and_exit"
TEST="$TEST timestamp_read_test"
TEST="$TEST version_test"
TEST="$TEST version_test_full"

unset TEST_PASS TEST_RUN

for t in $TEST; do
    TEST_RUN="$t"
    $t
    TEST_PASS="$TEST_PASS $t"
done
echo "-----------------------------------------------------------"
echo "---------------- TESTS EXECUTION SUMMARY ------------------"
for t in $TEST; do
    echo "  * [PASS] $t"
done
echo "-----------------------------------------------------------"

cleanup
