#! /bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Info
#
#   This script is used to launch a set of regression tests on a Sidekiq unit
#   to ensure that all functionality is maintained.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Variables
#
#   These are used during test execution in order to determine how they should
#   be run. Before running any tests, these will be updated based upon what is
#   detected on the platform and its associated Sidekiq.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Contains the directory where executables are located.
DIR=$(pwd)

# Symbolic variables for supported platforms.
GATEWORKS_51XX="Gateworks 51XX"
GATEWORKS_54XX="Gateworks 54XX"
X86_64="x86_64"
ARMV7="ARMv7"
WIN64_MSYS="Msys"
AARCH64="ARM 64-bit"

# Host platform that this script is being run on.
PLATFORM=""
# Sidekiq hardware version, one of "M2", "MPCIE", "X2", "X4", "M2_2280".
SKIQ_HW=""
# Sidekiq product version, either "001" or "002".
SKIQ_PROD=""
# FMC carrier (applies to X2 and X4)
SKIQ_FMC_CARRIER=""
# FPGA type (applies to products in an FMC carrier)
SKIQ_FPGA=""

# Command used to report BSP version
REPORT_BSP_VERS=""

# Boolean ("true"/"false") values used to determine Sidekiq features.
HAS_PCIE="false"
HAS_USB="false"
HAS_PPS="false"
HAS_GPS="false"
HAS_DKIQ="false"
HAS_2CHAN="false"
HAS_2TXCHAN="false"
HAS_BLOCK_RX="false"
HAS_PACKED_RX="false"
HAS_ASYNC_TX="false"
HAS_LOW_LATENCY="false"
HAS_RCS="false"
PERFORM_TRANSMIT="true"
PERFORM_EXTENDED="false"
HAS_I2C_RW="false"
HAS_TX="false"
HAS_FLASH="true"
HAS_RUNTIME_PROG_FPGA="false"
HAS_FREQ_HOP="true"
PERFORM_OLD_VERSION_TEST="true"
HAS_EXT_REF_CLK="false"

# Sidekiq minimum frequency - The minumum frequency is unique for each card.
# The min frequency is used in the following tests
# tx_sweet_testbench  - Originally 70MHz (default in test_app)
# rx_sweet_testbench  - Originally 70MHz (default in test_app)
# sweep_receive_extended - Originally 70MHz
# tx_configure_steps - Originally 70MHz

ONE_MHZ=1000000
SKIQ_MIN_TX_FREQ_HZ=""
SKIQ_MIN_RX_FREQ_HZ=""

# DROPKIQ   RX: 1 to 6000 MHz
#           TX: 1 to 6000 MHz
DROPK_MIN_RX_FREQ_HZ=$((1*$ONE_MHZ))
DROPK_MIN_TX_FREQ_HZ=$((1*$ONE_MHZ))

# MPCIE     RX: 70 to 6000 MHz
#           TX: 47 to 6000 MHz
MPCIE_MIN_RX_FREQ_HZ=$((70*$ONE_MHZ))
MPCIE_MIN_TX_FREQ_HZ=$((47*$ONE_MHZ))

# M.2       RX: 70 to 6000 MHz
#           TX: 47 to 6000 MHz
M2_MIN_RX_FREQ_HZ=$((70*$ONE_MHZ))
M2_MIN_TX_FREQ_HZ=$((47*$ONE_MHZ))

# M.2-2280  RX: 70 to 6000 MHz
#           TX: 47 to 6000 MHz
M2_2280_MIN_RX_FREQ_HZ=$((70*$ONE_MHZ))
M2_2280_MIN_TX_FREQ_HZ=$((47*$ONE_MHZ))

# X4        RX: 75 to 6000 MHz
#           TX: 75 to 6000 MHz
X4_MIN_RX_FREQ_HZ=$((75*$ONE_MHZ))
X4_MIN_TX_FREQ_HZ=$((75*$ONE_MHZ))

# X2        RX: 1 to 6000 MHz
#           TX: 1 to 6000 MHz
X2_MIN_RX_FREQ_HZ=$((1*$ONE_MHZ))
X2_MIN_TX_FREQ_HZ=$((1*$ONE_MHZ))

# Z3u       RX: 70 to 6000 MHz
#           TX: 47 to 6000 MHz
Z3U_MIN_RX_FREQ_HZ=$((70*$ONE_MHZ))
Z3U_MIN_TX_FREQ_HZ=$((47*$ONE_MHZ))

# Z2p       RX: 70 to 6000 MHz
#           TX: 47 to 6000 MHz
Z2P_MIN_RX_FREQ_HZ=$((70*$ONE_MHZ))
Z2P_MIN_TX_FREQ_HZ=$((47*$ONE_MHZ))

# NV100     RX: 30 to 6000 MHz
#           TX: 45 to 6000 MHz
NV100_MIN_RX_FREQ_HZ=$((30*$ONE_MHZ))
NV100_MIN_TX_FREQ_HZ=$((45*$ONE_MHZ))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Constant list of values for supported X2 sample rates
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
X2_JESD_RATE_LIST="\
    30.72e6
    36.864e6
    38.4e6
    50e6
    61.44e6
    73.728e6
    100e6
    122.88e6
    153.6e6
"
X2_RATE_LIST_RX_A1A2_WIN64="\
    480000:480000 \
    576000:576000 \
    600000:600000 \
    781250:781250 \
    960000:960000 \
    1152000:1152000 \
    1200000:1200000 \
    1562500:1562500 \
    1920000:1920000 \
    2304000:2304000 \
    2400000:2400000 \
    3125000:3125000 \
    3840000:3840000 \
    4608000:4608000 \
    4800000:4800000 \
    6250000:6250000 \
    7680000:7680000 \
    9216000:9216000 \
    9600000:9600000 \
    12500000:12500000 \
    15360000:12300000 \
    15360000:15360000 \
    18432000:18432000 \
    19200000:15360000 \
    19200000:19200000 \
    25000000:20000000 \
    25000000:25000000 \
    30720000:12300000 \
    30720000:18000000 \
    30720000:20000000 \
    30720000:25000000 \
    30720000:30720000 \
    36864000:30228000 \
    36864000:36864000 \
    38400000:15360000 \
    38400000:25000000 \
    50000000:20000000 \
    50000000:41000000 \
    61440000:25000000 \
    61440000:50000000 \
    73728000:30228000 \
    73728000:60456000 \
    100000000:82000000 \
    122880000:61440000 \
    122880000:64000000 \
    122880000:72000000 \
    122880000:100000000 \
"
X2_RATE_LIST_RX_A1A2="$X2_RATE_LIST_RX_A1A2_WIN64 153600000:100000000"
X2_RATE_LIST_EXT_RX_A1A2_WIN64="\
    30720000:25000000 \
    50000000:41000000 \
    61440000:25000000 \
    61440000:50000000 \
    73728000:30228000 \
    73728000:60456000 \
    100000000:82000000 \
    122880000:100000000 \
"
X2_RATE_LIST_EXT_RX_A1A2="$X2_RATE_LIST_EXT_RX_A1A2_WIN64 153600000:100000000"
X2_RATE_LIST_RX_B1_WIN64="\
    50000000:41000000 \
    61440000:25000000 \
    61440000:50000000 \
    73728000:30228000 \
    73728000:60456000 \
    100000000:82000000 \
    122880000:100000000 \
"
X2_RATE_LIST_RX_B1="$X2_RATE_LIST_RX_B1_WIN64 \
    153600000:100000000 \
    245760000:100000000 \
    245760000:200000000 \
"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Constant list of values for supported X4 sample rates
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
X4_JESD_RATE_LIST="\
    61.44e6
    100e6
    122.88e6
    200e6
    245.76e6
    250e6
"

X4_RATE_LIST_RX_A1A2_WIN64="\
    50000000:20000000 \
    50000000:41000000 \
    61440000:25000000 \
    61440000:50000000 \
    73728000:30228000 \
    73728000:60456000 \
    76800000:30720000 \
    100000000:82000000 \
    122880000:61440000 \
    122880000:64000000 \
    122880000:72000000 \
    122880000:100000000 \
    153600000:100000000 \
"

X4_RATE_LIST_RX_A1A2="$X4_RATE_LIST_RX_A1A2_WIN64 \
    200000000:164000000 \
    245760000:100000000 \
    245760000:200000000 \
    250000000:100000000 \
    250000000:200000000 \
"

X4_RATE_LIST_RX_B1B2_WIN64="\
    50000000:20000000 \
    50000000:41000000 \
    61440000:25000000 \
    61440000:50000000 \
    73728000:30228000 \
    73728000:60456000 \
    76800000:30720000 \
    100000000:82000000 \
    122880000:61440000 \
    122880000:64000000 \
    122880000:72000000 \
    122880000:100000000 \
    153600000:100000000 \
"

X4_RATE_LIST_RX_B1B2="$X4_RATE_LIST_RX_A1A2_WIN64 \
    200000000:164000000 \
    245760000:100000000 \
    245760000:200000000 \
    250000000:100000000 \
    250000000:200000000 \
"

X4_RATE_LIST_RX_C1D1_WIN64="\
    50000000:20000000 \
    50000000:41000000 \
    61440000:25000000 \
    61440000:50000000 \
    73728000:30228000 \
    73728000:60456000 \
    76800000:30720000 \
    100000000:82000000 \
    122880000:61440000 \
    122880000:64000000 \
    122880000:72000000 \
    122880000:100000000 \
    153600000:100000000 \
"

X4_RATE_LIST_RX_C1D1="$X4_RATE_LIST_RX_A1A2_WIN64 \
    200000000:164000000 \
    245760000:100000000 \
    245760000:200000000 \
    250000000:100000000 \
    250000000:200000000 \
    491520000:400000000 \
    491520000:450000000 \
    500000000:400000000 \
    500000000:450000000 \
"

X4_RATE_LIST_EXT_RX_A1A2_WIN64="\
    61440000:50000000 \
   100000000:82000000 \
   122880000:100000000 \
"

X4_RATE_LIST_EXT_RX_A1A2="$X4_RATE_LIST_EXT_RX_A1A2_WIN64"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Constant list of values for supported NV100 sample rates
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

NV100_RATE_LIST_RX_A1B1=" \
      541667:230883 \
     1400000:1200000 \
     1920000:960000 \
     2457600:1843200 \
     2800000:1200000 \
     3840000:2000000 \
     4000000:1200000 \
     4915200:1228800 \
     5600000:1200000 \
     7680000:4000000 \
     9830400:9338880 \
    10000000:8000000 \
    11200000:1200000 \
    11200000:10000000 \
    15360000:9000000 \
    16000000:2000000 \
    20000000:16000000 \
    21666665:4116666 \
    22000000:16000000 \
    23040000:13500000 \
    30720000:18000000 \
    40000000:38000000 \
"


# Space-delimited string of tests to run.
TEST=""

awk_shuf() {
    local pid="$1"
    local tests="$2"

    SHUFFLE_TEST_LIST=$(echo "${tests}" | awk "BEGIN { srand(${pid}); OFMT=\"%.17f\" } { split(\$0, a, \" \"); for (i in a) { print rand(), a[i]; } }" | sort -k1,1n | cut -d ' ' -f2- | tr '\n' ' ')
    return $?
}

retry() {
    re='^[0-9]+$'
    if ! [[ $1 =~ $re ]] ; then
        echo "error: retry() expects a numeric input" >&2; exit 1
    fi
    local -r -i max_attempts="$1"; shift
    local -i attempt_num=1
    until "$@"
    do
        if ((attempt_num==max_attempts))
        then
            echo "Attempt $attempt_num failed and there are no more attempts left!"
            return 1
        else
            echo "Attempt $attempt_num failed! Trying again in $attempt_num seconds..."
            sleep $((attempt_num++))
        fi
    done
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Tests
#
#   This sections defines the possible tests cases that can be run by this
#   script. Constructing a test requires a function that defines how the test
#   is to be run and then adding it's name to the "TEST" array. The "TEST"
#   array will be run-testcuted in sequential order by the script if all of the
#   tests are to be run.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

run_test()
{
    local rc

    if [[ "$DRY_RUN" == "true" ]]; then
        echo "+ DRY RUN -- $*"
    else
        echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
        echo "+ RUNNING -- $*"
        time "$@"
        rc=$?
        if [[ $rc -ne 0 ]]
        then
            echo "+ FAILED -- $*"
            echo "+ test FAILED with status=$rc"

            # attempt to print out the version for debugging purposes
            if [[ -n $CARD ]] && [[ -n $DIR ]] ; then
                echo "+ grabbing version info of failed regression..."
                $DIR/version_test$APP_SUFFIX -c $CARD
            fi

            echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
            echo
            echo "-----------------------------------------------------------"
            echo "---------------- TESTS EXECUTION SUMMARY ------------------"
            for t in $TEST_PASS; do
                echo "  * [PASS] $t"
            done
            echo "  * [FAIL] $TEST_RUN"
            echo "-----------------------------------------------------------"
            exit $rc
        fi
        echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
        echo
    fi
}

run_test_capture()
{
    local OF STDBUF_STDIO STDBUF_STDERR
    OF=$1
    shift

    if [[ $PLATFORM != $WIN64_MSYS ]] ; then
        STDBUF_STDIO="stdbuf -oL"
        STDBUF_STDERR="stdbuf -eL"
    fi

    exec 4>$OF 5>&1 6>&2
    exec >  >($STDBUF_STDIO tee -ia /dev/fd/4)
    exec 2> >($STDBUF_STDERR tee -ia /dev/fd/4 >&2)

    run_test "$@"

    exec 4>&-
    exec 1>&5 5>&-
    exec 2>&6 6>&-
}

get_seeded_random()
{
    local seed openssl_options

    seed="$1"
    if [[ $PLATFORM == $WIN64_MSYS ]] ; then
        openssl_options="-pbkdf2"
    fi

    echo "SRFS is dead, long live SRFS!" | \
        openssl enc -aes-256-ctr -pass pass:"$seed" ${openssl_options} -nosalt 2>/dev/null
}

fn_exists()
{
    LC_ALL=C type -t $1 | grep -q 'function'
}

#
# echos the minimum of two values
#
minimum()
{
    local A B
    A=$1
    B=$2
    echo $(( A < B ? A : B ))
}

#
# echos the maximum of two values
#
maximum()
{
    local A B
    A=$1
    B=$2
    echo $(( A > B ? A : B ))
}


#
# calculates `target throughput` (MB/sec) approximately 90% of the `sample rate` (specified in samples/sec)
#
# `throughput` (bytes/sec) is `sample rate` (samples/sec) x 4 bytes/sample
# `target throughput` (bytes/sec) is 90% (0.9 = 9 / 10) of `throughput` (bytes/sec)
# `target throughput` (MB/sec) is `target throughput` (bytes/sec) x 1MB/1,000,000bytes
#
# Here's an example, a `sample rate` of 25,000,000 samples/sec has a `target throughput` of 90 MB/sec
#
#     25,000,000 samples/sec x 4 bytes/sample = 100,000,000 bytes/sec
#    100,000,000   bytes/sec x 90% = (100,000,000 bytes/sec x 9) / 10 = 90,000,000 bytes/sec
#     90,000,000   bytes/sec x 1MB/1,000,000bytes = 90MB/sec
#
# WARNING: For low sample rates, this function loses precision.  To fix this drawback, any test
# application that accepts --target *should* be updated to allow specifying (e.g. --target-kbps) to
# allow for a finer resolution in expected throughput (KB/sec)
#
target_90pct_thruput()
{
    local SR t
    SR=$1

    # `throughput` (bytes/sec) is `sample rate` (samples/sec) x 4 bytes/sample
    t=$(( SR * 4 ))

    # `target throughput` (bytes/sec) is 90% (0.9 = 9 / 10) of `throughput` (bytes/sec)
    t=$(( (t * 9) / 10 ))

    # `target throughput` (MB/sec) is `target throughput` (bytes/sec) x 1MB/1,000,000bytes
    t=$(( t / 1000000 ))

    # protect against calculations that result in a --target=0
    maximum $t 1
}


#
# calculates an equivalent sample rate when in I/Q packed mode and limits to
# 61440000
#
packed_rate()
{
    local R PR
    R=$1
    PR=$((R * 4 / 3))
    minimum $PR 61440000
}

#
# returns a sequence of NUM rates between START and STOP
#
rate_list()
{
    local START STOP NUM STEP
    START=$1
    STOP=$2
    NUM=$3
    STEP=$(( ( STOP - START ) / NUM ))

    if [[ $STEP -le 0 ]]; then
        echo $STOP
    else
        seq $START $STEP $STOP
    fi
}

fx2_reprogram()
{
    local ARGS
    ARGS="-c $CARD -r"

    if [[ -n $FW_MPCIE ]] ; then
        ARGS="$ARGS --mpcie=$(readlink -f $FW_MPCIE)"
    fi

    if [[ -n $FW_M2 ]] ; then
        ARGS="$ARGS --m2=$(readlink -f $FW_M2)"
    fi

    run_test_capture $RESULTS/fx2-reprogram.txt $DIR/fx2_reprogram$APP_SUFFIX $ARGS
}

prog_fpga()
{
    run_test_capture $RESULTS/prog-fpga.txt $DIR/prog_fpga$APP_SUFFIX -c $CARD -s $FPGA
}

rand_prog_fpga_and_receive()
{
    local sample_rate

    sample_rate=$MAX_SAMPLE_RATE

    run_test_capture $RESULTS/rand-prog-and-rx.txt $DIR/rand_prog_fpga_and_receive$APP_SUFFIX -c $CARD -b $FPGA --sample-rate-high-tput=$sample_rate --num-receive-blocks=2000 --iterations=10 --receive-iterations=5
}

store_user_fpga()
{
    local ARGS
    ARGS="-c $CARD -s $FPGA --config-slot=$FPGA_CONFIG_SLOT --verbose --verify --reload"

    run_test_capture $RESULTS/store-user-fpga.txt $DIR/store_user_fpga$APP_SUFFIX $ARGS
}

test_fpga_vers()
{
    local ARGS
    ARGS="-c $CARD -j $FPGA_MAJOR -n $FPGA_MINOR -p $FPGA_PATCH -b 0x$FPGA_BUILD_DATE -g 0x$FPGA_GIT_HASH"

    if [[ $HAS_USB == "true" ]] ; then
        ARGS="$ARGS -m USB"
    fi

    run_test_capture $RESULTS/test_fpga_vers.txt $DIR/test_fpga_vers$APP_SUFFIX $ARGS
}

rfic_rw_test()
{
    if [[ $HAS_PCIE == "true" ]] ; then
        run_test_capture $RESULTS/rfic-rw-test.txt $DIR_PRIVATE/rfic_rw_test$APP_SUFFIX $CARD 250000
    else
        run_test_capture $RESULTS/rfic-rw-test.txt $DIR_PRIVATE/rfic_rw_test$APP_SUFFIX $CARD 100
    fi
}

i2c_rw_test()
{
    if [[ $HAS_I2C_RW == "true" ]] ; then
        run_test_capture $RESULTS/i2c-rw-test.txt $DIR_PRIVATE/i2c_rw_test$APP_SUFFIX $CARD 1000
    else
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for I2C RW test" >&2
        return
    fi
}

fpga_rw_test()
{
    if [[ $HAS_PCIE == "true" ]] ; then
        run_test_capture $RESULTS/fpga-rw-test.txt $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000
        run_test_capture $RESULTS/fpga-rw-test.txt $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000 --64bit=aligned 
        run_test_capture $RESULTS/fpga-rw-test.txt $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000 --64bit=unaligned
    else
        run_test_capture $RESULTS/fpga-rw-test.txt $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000
        run_test_capture $RESULTS/fpga-rw-test.txt $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000 --64bit=aligned 
        run_test_capture $RESULTS/fpga-rw-test.txt $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000 --64bit=unaligned
    fi
}

user_reg_verify()
{
    if [[ $HAS_PCIE == "true" ]] ; then
        run_test_capture $RESULTS/user-reg-verify.txt $DIR_PRIVATE/user_reg_verify$APP_SUFFIX $CARD
    fi
}

timestamp_reset()
{
    if [[ $HAS_PPS == "true" ]] && [[ $HAS_PCIE == "true" ]] ; then
        run_test_capture $RESULTS/timestamp-reset.txt $DIR/timestamp_reset$APP_SUFFIX $CARD 1 0
    elif [[ $HAS_USB == "false" ]] ; then
        # timestamp reset for USB needs to be investigated, likely just needs
        # a more generous timestamp check
        run_test_capture $RESULTS/timestamp-reset.txt $DIR/timestamp_reset$APP_SUFFIX $CARD 0 0
    fi
}

rx_benchmark()
{
    local ARGS TARGET
    ARGS="-c $CARD --handle=A1 --time=5 --threshold=200"

    # MAX_SAMPLE_RATE is set in set_test_parameters() according to build architecture, product, and
    # transport
    TARGET=$(target_90pct_thruput $MAX_SAMPLE_RATE)
    ARGS="$ARGS -r $MAX_SAMPLE_RATE --target=$TARGET"

    run_test_capture $RESULTS/rx-benchmark.txt $DIR/rx_benchmark$APP_SUFFIX $ARGS
}

tx_benchmark()
{
    if [[ $HAS_TX == "false" ]] ; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for Tx" >&2
        return
    fi

    ######################################################################
    # Synchronous Transmit
    ######################################################################

    local ARGS
    ARGS="-c $CARD --time=5 --threshold=200"

    case "$PLATFORM" in
        $X86_64)
            if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]] ; then
                ARGS="$ARGS --block-size=65532 -r 100000000 --target=372 --threads=1"
            elif [[ "$SKIQ_HW" == "NV100" ]]; then
                ARGS="$ARGS --block-size=16380 -r 30720000 --target=110 --threads=1"
            else
                ARGS="$ARGS --block-size=65532 -r 20000000 --target=75 --threads=1"
            fi
            ;;

        $GATEWORKS_51XX|$GATEWORKS_54XX)
            ARGS="$ARGS --block-size=16380 -r 20000000 --target=70 --threads=1"
            ;;

        $WIN64_MSYS)
            if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]] ; then
                ARGS="$ARGS --block-size=65532 -r 100000000 --target=372 --threads=1"
            elif [[ "$SKIQ_HW" == "NV100" ]]; then
                ARGS="$ARGS --block-size=16380 -r 30720000 --target=110 --threads=1"
            else
                ARGS="$ARGS --block-size=65532 -r 25000000 --target=90 --threads=1"
            fi
            ;;

        $AARCH64)
            if [[ "$SKIQ_HW" == "NV100" ]]; then
                ARGS="$ARGS --block-size=16380 -r 15360000 --target=55 --threads=1"
            else
                ARGS="$ARGS --block-size=65532 -r 28000000 --target=104 --threads=1"
            fi
            ;;
    esac

    run_test_capture $RESULTS/tx-benchmark.txt $DIR/tx_benchmark$APP_SUFFIX $ARGS

    ######################################################################
    # Asynchronous Transmit
    ######################################################################

    if [[ $HAS_ASYNC_TX == "true" ]] ; then
        ARGS="-c $CARD --time=5 --threshold=200"

        case $PLATFORM in
            $X86_64)
                if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]] ; then
                    ARGS="$ARGS --block-size=65532 -r 100000000 --target=372 --threads=4"
                elif [[ "$SKIQ_HW" == "NV100" ]]; then
                    ARGS="$ARGS --block-size=16380 -r 30720000 --target=110 --threads=4"
                else
                    ARGS="$ARGS --block-size=65532 -r 22500000 --target=85 --threads=4"
                fi
                ;;

            $GATEWORKS_51XX|$GATEWORKS_54XX)
                ARGS="$ARGS --block-size=16380 -r 30000000 --target=110 --threads=4"
                ;;

            $WIN64_MSYS)
                if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]] ; then
                    ARGS="$ARGS --block-size=65532 -r 100000000 --target=372 --threads=4"
                elif [[ "$SKIQ_HW" == "NV100" ]]; then
                    ARGS="$ARGS --block-size=16380 -r 30720000 --target=110 --threads=4"
                else
                    ARGS="$ARGS --block-size=65532 -r 28000000 --target=101 --threads=4"
                fi
                ;;

            $AARCH64)
                if [[ "$SKIQ_HW" == "NV100" ]]; then
                    ARGS="$ARGS --block-size=16380 -r 30720000 --target=110 --threads=4"
                else
                    ARGS="$ARGS --block-size=65532 -r 36000000 --target=129 --threads=4"
                fi
                ;;
        esac

        run_test_capture $RESULTS/tx-benchmark-async.txt $DIR/tx_benchmark$APP_SUFFIX $ARGS
    fi
}

xcv_benchmark()
{
    if [[ $HAS_TX == "false" ]] ; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for Tx" >&2
        return
    fi

    if [[ "$SKIQ_HW" == "X4" ]] ; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for X4" >&2
        return
    fi

    if [[ "$SKIQ_HW" == "NV100" ]] ; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for NV100" >&2
        return
    fi

    ######################################################################
    # Synchronous Transceive
    ######################################################################
    
    # SR: sample rate - used to set the sample rate
    # THRSH: threshold - used to set the threshold value for allowed underruns
    local ARGS SR THRSH
    ARGS="-c $CARD --time=5"

    case "$PLATFORM" in
        $X86_64)         SR=25000000 ; THRSH=200 ;;
        $GATEWORKS_51XX) SR=11000000 ; THRSH=275 ;;
        $GATEWORKS_54XX) SR=11000000 ; THRSH=275 ;;
        $WIN64_MSYS)     SR=20000000 ; THRSH=200 ;;
        $AARCH64)        SR=25000000 ; THRSH=200 ;;
        *)               
            echo "WARNING: Sidekiq platform not found - reverting to default settings"
            SR=11000000
            THRSH=200
            ;;
    esac
    ARGS="$ARGS --block-size=16380 -r $SR --threshold=$THRSH --target=$(target_90pct_thruput $SR) --threads=1"

    case "$PLATFORM" in
        $GATEWORKS_51XX|$GATEWORKS_54XX)
            run_test_capture $RESULTS/xcv-benchmark.txt taskset 0xE $DIR/xcv_benchmark$APP_SUFFIX $ARGS
            ;;
        *)
            run_test_capture $RESULTS/xcv-benchmark.txt $DIR/xcv_benchmark$APP_SUFFIX $ARGS
            ;;
    esac

    ######################################################################
    # Asynchronous Transceive
    ######################################################################
    if [[ $HAS_ASYNC_TX == "true" ]] ; then
        
        # BS: block size - used to set the block size
        local BS
        ARGS="-c $CARD --time=5"

        case "$PLATFORM" in
            $X86_64)         SR=32000000 ; THRSH=200 ; BS=65532 ;;
            $GATEWORKS_51XX) SR=22000000 ; THRSH=275 ; BS=16380 ;;
            $GATEWORKS_54XX) SR=22000000 ; THRSH=275 ; BS=16380 ;;
            $WIN64_MSYS)     SR=28000000 ; THRSH=200 ; BS=65532 ;;
            $AARCH64)        SR=27000000 ; THRSH=200 ; BS=65532 ;;
            *)               
                echo "WARNING: Sidekiq platform not found - reverting to default settings"
                SR=11000000
                THRSH=200
                BS=65532
                ;;
        esac
        ARGS="$ARGS --block-size=$BS -r $SR --threshold=$THRSH --target=$(target_90pct_thruput $SR) --threads=4"

        case "$PLATFORM" in
            $GATEWORKS_51XX|$GATEWORKS_54XX)
                run_test_capture $RESULTS/xcv-benchmark-async.txt taskset 0xE $DIR/xcv_benchmark$APP_SUFFIX $ARGS
                ;;
            *)
                run_test_capture $RESULTS/xcv-benchmark-async.txt $DIR/xcv_benchmark$APP_SUFFIX $ARGS
                ;;
        esac
    fi
}

#
# given a number of words and an output file path, verify if the output file size is correct
#
_verify_file_size()
{
    local BYTES WORDS OUTPUT_FN EXPECTED PRESENTLY
    WORDS=$1
    OUTPUT_FN=$2
    BYTES=$((WORDS * 4))
    EXPECTED=$BYTES
    PRESENTLY=$(stat --printf="%s" "$OUTPUT_FN")

    if [[ $PRESENTLY -eq $EXPECTED ]]; then
        echo "output size verified, expected $EXPECTED bytes in $OUTPUT_FN, found $PRESENTLY bytes" >&2
        rm -f "$OUTPUT_FN"
        return 0
    else
        echo "output size NOT verified, expected $EXPECTED bytes in $OUTPUT_FN, found $PRESENTLY bytes" >&2
        return 1
    fi
}

#
# given a number of samples and an output file path, verify if the output file size is correct for
# unpacked mode
#
verify_output_file_size()
{
    local NR_SAMPLES NR_WORDS OUTPUT_FN
    NR_SAMPLES=$1
    OUTPUT_FN=$2

    # Calculate number of words for the given number of UNPACKED samples
    NR_WORDS=$NR_SAMPLES

    _verify_file_size "$NR_WORDS" "$OUTPUT_FN"
}

#
# given a number of samples and an output file path, verify if the output file size is correct for
# packed mode
#
verify_packed_output_file_size()
{
    local NR_SAMPLES NR_WORDS OUTPUT_FN
    NR_SAMPLES=$1
    OUTPUT_FN=$2

    # Calculate number of words for the given number of PACKED samples
    NR_WORDS=$(( ((NR_SAMPLES * 3) + 3) / 4 ))

    _verify_file_size "$NR_WORDS" "$OUTPUT_FN"
}

_rx_samples()
{
    local ARGS RATES BW RX_SAMPLES WORDS
    ARGS="-c $CARD -d $OUTDIR/out --counter $*"

    #
    # HACK for Jetson platforms that need rx_samples to run in a single thread for DMA coherency
    #
    if [[ $PLATFORM == $AARCH64 && $HAS_PCIE == "true" ]] ; then
        RX_SAMPLES="taskset -c 0 $DIR/rx_samples$APP_SUFFIX"
    else
        RX_SAMPLES=$DIR/rx_samples$APP_SUFFIX
    fi

    if [[ "$SKIQ_HW" == "X2" ]]; then
        # test the full list of X2 sample rates
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X2_RATE_LIST_RX_A1A2_WIN64
        else
            RATES=$X2_RATE_LIST_RX_A1A2
        fi
    elif [[ "$SKIQ_HW" == "X4" ]]; then
        # test the full list of X4 sample rates
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X4_RATE_LIST_RX_A1A2_WIN64
        else
            RATES=$X4_RATE_LIST_RX_A1A2
        fi
    elif [[ "$SKIQ_HW" == "NV100" ]]; then
        # test the full list of NV100 sample rates
        RATES=$NV100_RATE_LIST_RX_A1B1
    elif [[ "$SKIQ_HW" == "Z3U" || "$SKIQ_HW" == "Z2P" ]]; then
        # test 8 rates between 1e6 and the max safe receive sample rate
        RATES=$(rate_list 1000000 $MAX_SAMPLE_RATE 8)
    else
        # test 8 rates between 1e6 and max sample rate
        RATES=$(rate_list 1000000 $MAX_SAMPLE_RATE 8)
    fi

    for RATE in $RATES ; do
        case "$RATE" in
            *:*)
                BW="-b $(cut -f 2 -d: <<< "$RATE")"
                RATE=$(cut -f 1 -d: <<< "$RATE")
                WORDS=$RATE
                ;;
            *)
                unset BW
                WORDS=$RATE
                ;;
        esac

        # test rx for A1, first unpacked, then packed
        run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=A1
        run_test verify_output_file_size $RATE "$OUTDIR/out.a1"

        if [[ $HAS_PACKED_RX == "true" ]]; then
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $(packed_rate $RATE) $BW --packed --handle=A1
            run_test verify_packed_output_file_size $RATE "$OUTDIR/out.a1"
        fi
    done

    # Perform testing on RxA2 only if HAS_2CHAN is set, then check for SKIQ_HW for support
    if [[ $HAS_2CHAN == "true" ]] ; then

        if [[ "$SKIQ_HW" == "X2" ]]; then
            # test the full list of X2 sample rates
            if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
                RATES=$X2_RATE_LIST_RX_A1A2_WIN64
            else
                RATES=$X2_RATE_LIST_RX_A1A2
            fi
        elif [[ "$SKIQ_HW" == "X4" ]]; then
            # test the full list of X4 sample rates
            if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
                RATES=$X4_RATE_LIST_RX_A1A2_WIN64
            else
                RATES=$X4_RATE_LIST_RX_A1A2
            fi
        elif [[ "$SKIQ_HW" == "NV100" ]]; then
            # The NV100 does not currently support A2, so the rate list is empty
            RATES=""
        elif [[ "$SKIQ_HW" == "M2" ]]; then
            # test 8 rates between 1e6 and max sample rate
            RATES=$(rate_list 1000000 $MAX_SAMPLE_RATE 8)
        elif [[ "$SKIQ_HW" == "Z3U" || "$SKIQ_HW" == "Z2P" ]]; then
            # test 8 rates between 1e6 and half of the max safe receive sample rate
            RATES=$(rate_list 1000000 $((MAX_SAMPLE_RATE / 2)) 8)
        else
            # test 8 rates between 1e6 and half of the max sample rate
            RATES=$(rate_list 1000000 $((MAX_SAMPLE_RATE / 2)) 8)
        fi

        for RATE in $RATES ; do
            case "$RATE" in
                *:*)
                    BW="-b $(cut -f 2 -d: <<< "$RATE")"
                    RATE=$(cut -f 1 -d: <<< "$RATE")
                    WORDS=$RATE
                    ;;
                *)
                    unset BW
                    WORDS=$RATE
                    ;;
            esac

            # test rx for A2, first unpacked, then packed
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=A2
            run_test verify_output_file_size $RATE "$OUTDIR/out.a2"

            if [[ $HAS_PACKED_RX == "true" ]]; then
                run_test $RX_SAMPLES $ARGS -w $WORDS -r $(packed_rate $RATE) $BW --packed --handle=A2
                run_test verify_packed_output_file_size $RATE "$OUTDIR/out.a2"
            fi
        done
    fi

    # test RxB1 if the hardware is X2 and if HAS_2CHAN is set
    if [[ "$SKIQ_HW" == "X2" && "$HAS_2CHAN" == "true" ]]; then

        # test the full list of X2 sample rates available to RxB1
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X2_RATE_LIST_RX_B1_WIN64
        else
            RATES=$X2_RATE_LIST_RX_B1
        fi

        for RATE in $RATES ; do
            case "$RATE" in
                *:*)
                    BW="-b $(cut -f 2 -d: <<< "$RATE")"
                    RATE=$(cut -f 1 -d: <<< "$RATE")
                    WORDS=$RATE
                    ;;
                *)
                    unset BW
                    WORDS=$RATE
                    ;;
            esac

            # test rx for B1
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=B1
            run_test verify_output_file_size $RATE "$OUTDIR/out.b1"
        done
    fi

    # test RxB1 and RxB2 if the hardware is X4 and if HAS_2CHAN is set
    if [[ "$SKIQ_HW" == "X4" && "$HAS_2CHAN" == "true" ]]; then

        # test the full list of X4 sample rates available to RxB1/RxB2
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X4_RATE_LIST_RX_B1B2_WIN64
        else
            RATES=$X4_RATE_LIST_RX_B1B2
        fi

        for RATE in $RATES ; do
            case "$RATE" in
                *:*)
                    BW="-b $(cut -f 2 -d: <<< "$RATE")"
                    RATE=$(cut -f 1 -d: <<< "$RATE")
                    WORDS=$RATE
                    ;;
                *)
                    unset BW
                    WORDS=$RATE
                    ;;
            esac

            # test rx for B1
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=B1
            run_test verify_output_file_size $RATE "$OUTDIR/out.b1"

            # test rx for B2
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=B2
            run_test verify_output_file_size $RATE "$OUTDIR/out.b2"
        done
    fi

    # test RxB1 if the hardware is NV100 and if HAS_2CHAN is set
    if [[ "$SKIQ_HW" == "NV100" && "$HAS_2CHAN" == "true" ]]; then

        # test the full list of NV100 sample rates available to RxB1
        RATES=$NV100_RATE_LIST_RX_A1B1

        for RATE in $RATES ; do
            case "$RATE" in
                *:*)
                    BW="-b $(cut -f 2 -d: <<< "$RATE")"
                    RATE=$(cut -f 1 -d: <<< "$RATE")
                    WORDS=$RATE
                    ;;
                *)
                    unset BW
                    WORDS=$RATE
                    ;;
            esac

            # test rx for B1
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=B1
            run_test verify_output_file_size $RATE "$OUTDIR/out.b1"
        done
    fi

    # test RxC1 and RxD1 if the hardware is X4 and if HAS_2CHAN is set
    if [[ "$SKIQ_HW" == "X4" && "$HAS_2CHAN" == "true" ]]; then

        # test the full list of X4 sample rates available to RxC1/RxD1
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X4_RATE_LIST_RX_C1D1_WIN64
        else
            RATES=$X4_RATE_LIST_RX_C1D1
        fi

        for RATE in $RATES ; do
            case "$RATE" in
                *:*)
                    BW="-b $(cut -f 2 -d: <<< "$RATE")"
                    RATE=$(cut -f 1 -d: <<< "$RATE")
                    WORDS=$RATE
                    ;;
                *)
                    unset BW
                    WORDS=$RATE
                    ;;
            esac

            # test rx for C1
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=C1
            run_test verify_output_file_size $RATE "$OUTDIR/out.c1"

            # test rx for D1
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE $BW --handle=D1
            run_test verify_output_file_size $RATE "$OUTDIR/out.d1"
        done
    fi
}
 
rx_samples_minimal()
{
    local ARGS
    local ARGS RATES BW RX_SAMPLES WORDS
    ARGS="-c $CARD -d $OUTDIR/out $*"

    # test multiple sample rates if the hardware is x4.
    if [[ "$SKIQ_HW" == "X4" ]]; then
        # use the WIN64 set since decimation isn't available for the higher rates.
        RATES="${X4_RATE_LIST_RX_A1A2_WIN64% 153600000:100000000*}" #and the last one is too large..

        for RATE in $RATES ; do
            case "$RATE" in
                *:*)
                    BW="-b $(cut -f 2 -d: <<< "$RATE")"
                    RATE=$(cut -f 1 -d: <<< "$RATE")
                    WORDS=$RATE
                    ;;
                *)
                    unset BW
                    WORDS=$RATE
                    ;;
            esac
            local OTHER_HDL_RATE
            OTHER_HDL_RATE=$((RATE / 2))
            OTHER_HDL_BW=$((OTHER_HDL_RATE * 4 / 5))

            run_test $DIR/rx_samples_minimal$APP_SUFFIX $ARGS -w $WORDS -r $RATE,$OTHER_HDL_RATE $BW,$OTHER_HDL_BW -f 850000000,950000000 --handle=B1,A1

        done
    fi
}

_rx_samples_low_latency()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" || "$SKIQ_HW" == "NV100" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - support for low latency on X2/X4 is touchy, even at 30.72Msps, skipping test until decimation is supported" >&2
    else
        # maximum sample rate for low latency is about 1/8 maximum sample rate for high throughput
        local LL_MSR
        if [[ $PLATFORM == $GATEWORKS_51XX ]] ; then
            LL_MSR=$((MAX_SAMPLE_RATE / 8))
            LL_MSR=$(minimum $LL_MSR 3000000)
        elif [[ $PLATFORM == $GATEWORKS_54XX ]] ; then
            LL_MSR=$((MAX_SAMPLE_RATE / 8))
            LL_MSR=$(minimum $LL_MSR 3000000)
        else
            LL_MSR=$((MAX_SAMPLE_RATE / 4))
            LL_MSR=$(minimum $LL_MSR 8000000)
        fi
        local ARGS RATES WORDS RX_SAMPLES
        ARGS="-c $CARD -d $OUTDIR/out --counter --low-latency $*"

        #
        # HACK for Jetson platforms that need rx_samples to run in a single thread for DMA coherency
        #
        if [[ $PLATFORM == $AARCH64 ]] ; then
            RX_SAMPLES="taskset -c 0 $DIR/rx_samples$APP_SUFFIX"
        else
            RX_SAMPLES=$DIR/rx_samples$APP_SUFFIX
        fi

        RATES="233000 $(rate_list 1000000 $LL_MSR 8)"
        for RATE in $RATES ; do
            WORDS=$RATE

            # test rx for A1, packing not supported, so just unpacked
            run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE --handle=A1
            run_test verify_output_file_size $WORDS "$OUTDIR/out.a1"
        done

        if [[ $HAS_2CHAN == "true" ]] ; then
            if [[ "$SKIQ_HW" == "M2" ]]; then
                RATES="233000 $(rate_list 1000000 $LL_MSR 8)"
            else
                RATES="233000 $(rate_list 1000000 $((LL_MSR / 2)) 8)"
            fi

            for RATE in $RATES ; do
                WORDS=$RATE

                # test rx for A2, packing not supported, so just unpacked
                run_test $RX_SAMPLES $ARGS -w $WORDS -r $RATE --handle=A2
                run_test verify_output_file_size $WORDS "$OUTDIR/out.a2"
            done
        fi
    fi
}

rx_samples()
{
    local GAIN

    # specify receive gain based on hardware type
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" || "$SKIQ_HW" == "NV100" ]]; then
        GAIN="-g 237"
    else
        GAIN="-g 40"
    fi

    # call _rx_samples test with an additional option if blocking RX is available
    _rx_samples $GAIN
    if [[ $HAS_BLOCK_RX == "true" ]]; then
        _rx_samples $GAIN --blocking
    fi

    # call _rx_samples test with an additional option if IQ ordering mode
    _rx_samples $GAIN --sample-order-iq

    # call _rx_samples_low_latency test if low latency is available with an
    # additional option if blocking RX is available
    if [[ $HAS_LOW_LATENCY == "true" ]]; then
        _rx_samples_low_latency $GAIN

        _rx_samples_low_latency $GAIN --sample-order-iq

        if [[ $HAS_BLOCK_RX == "true" ]]; then
            _rx_samples_low_latency $GAIN --blocking
        fi
    fi
}

rx_samples_on_1pps()
{
    local ARGS
    ARGS="-c $CARD -d $OUTDIR/out -w 1.92e6 --trigger-src=1pps"

    run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=A1 --counter
    if [[ $HAS_2CHAN == "true" ]]; then
        if [[ "$SKIQ_HW" != "NV100" ]]; then
            run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=A2 --counter
            run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=ALL --perform-verify
        else #system timestamps aren't expected between A1,A2 on nv100 to match so skip that comparison
            run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=B1 --counter
            run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=A1,B1 --perform-verify
        fi
    fi
}

rx_samples_on_synced()
{
    local ARGS
    ARGS="-c $CARD -d /dev/null -w 1.92e6 --trigger-src=synced --perform-verify"
    
    if [[ "$SKIQ_HW" != "NV100" ]]; then
        run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=ALL
    else #rf timestamps between A1,A2 on nv100 aren't expected to match when trigger-src=synced so skip that comparison
        run_test $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS -r 1.92e6 --handle=A1,B1
    fi

    return
}

rx_samples_in_phase()
{
    #####################################################################################
    # This test requires the following equipment:
    # Noisecom NC346B, connected to A1,A2,B1,B2 through a 1x4 RF splitter amplified
    # by a ZHL-42W+
    # The presence of this hardware is indicated by CHK_PHASE (--check-phase), and if
    # unavailable the test will operate in a counter mode.
    #####################################################################################
    local ARGS EXTRA_ARGS i
    ARGS="-c $CARD -w 1000"

    RATES="50000000 61440000 122880000"
    MIN_TEST_FREQ="600000000"
    MAX_TEST_FREQ="4500000000"
    NUM_RUNS="5"
    FREQS="75000000 150000000 300000000 600000000 1000000000 1500000000 2000000000 2500000000 3500000000 4500000000 5000000000 5500000000 6000000000"

    if [[ "$SKIQ_HW" == "X4" && $CHK_PHASE == "true" ]]; then
        for RATE in $RATES; do
            BW=$[RATE * 8 / 10]
            for FREQ in $FREQS; do
                for i in $(seq $NUM_RUNS); do
                    EXTRA_ARGS=" -d $OUTDIR/rx_samples_on_trigger-${RATE}-${FREQ}-$i -r $RATE -f $FREQ -b $BW --handle a1,a2,b1,b2 --trigger-src synced"
                    #Adding a retry mechanism in case we get timestamp errors since the correlation expects samples without gaps
                    retry 10 $DIR/rx_samples_on_trigger$APP_SUFFIX $ARGS $EXTRA_ARGS;
                done
                if [[ $FREQ -ge $MIN_TEST_FREQ  && $FREQ -le $MAX_TEST_FREQ ]] ; then
                    run_test $DIR/check_phase_coherence.py $OUTDIR/rx_samples_on_trigger-${RATE}-${FREQ}- $NUM_RUNS $RESULTS/phase_coherence_summary >> $RESULTS/phase_coherence_summary.txt
                else
                    echo "===== Excluding $FREQ from the pass/fail criteria ====" >&2
                    $DIR/check_phase_coherence.py $OUTDIR/rx_samples_on_trigger-${RATE}-${FREQ}- $NUM_RUNS $RESULTS/phase_coherence_summary >> $RESULTS/phase_coherence_summary.txt
                fi
            done
        done
    else
        echo "===== SKIPPING ${FUNCNAME[0]} - supported only on X4" >&2
        return
    fi
}

_rx_benchmark_extended()
{
    local ARGS RUNS SR TARGET
    RUNS=$1
    SR=$2
    TARGET=$(target_90pct_thruput $SR)
    ARGS="-c $CARD --time=5 --threshold=200 --target=$TARGET"

    local i good bad
    i=0
    good=0
    bad=0
    while [ $i -lt $RUNS ]; do
        echo GOOD $good, BAD $bad
        echo "+ RUNNING -- $DIR/rx_benchmark$APP_SUFFIX $ARGS --handle=A1 -r $SR"
        $DIR/rx_benchmark$APP_SUFFIX $ARGS --handle=A1 -r $SR
        if [ $? -eq 0 ]; then
            good=$((good + 1))
        else
            bad=$((bad + 1))
        fi
        i=$((i + 1))
    done
    echo FINAL: GOOD $good, BAD $bad

    return $((bad > 0))
}

rx_benchmark_extended()
{
    local RATES BW RATE WORDS

    if [[ "$SKIQ_HW" == "X2" ]]; then
        # test the full list of X2 sample rates
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X2_RATE_LIST_EXT_RX_A1A2_WIN64
        else
            RATES=$X2_RATE_LIST_EXT_RX_A1A2
        fi
    elif [[ "$SKIQ_HW" == "X4" ]]; then
        # test the full list of X4 sample rates
        if [[ "$PLATFORM" == "$WIN64_MSYS" ]]; then
            RATES=$X4_RATE_LIST_EXT_RX_A1A2_WIN64
        else
            RATES=$X4_RATE_LIST_EXT_RX_A1A2
        fi
    elif [[ "$SKIQ_HW" == "NV100" ]]; then
        # test the full list of NV100 sample rates
        RATES=$NV100_RATE_LIST_RX_A1B1
    elif [[ "$SKIQ_HW" == "M2" || "$SKIQ_HW" == "M2_2280" ]]; then
        # test 8 rates between 1e6 and max sample rate
        RATES=$(rate_list 1000000 $MAX_SAMPLE_RATE 8)
    else
        # test 8 rates between 1e6 and half of the max sample rate
        RATES=$(rate_list 1000000 $((MAX_SAMPLE_RATE / 2)) 8)
    fi

    # remove duplicated rates
    RATES=$(echo $RATES | xargs -n1 | cut -d: -f1 | sort -ug)
    for RATE in $RATES; do
        case "$RATE" in
            *:*)
                BW="-b $(cut -f 2 -d: <<< "$RATE")"
                RATE=$(cut -f 1 -d: <<< "$RATE")
                WORDS=$RATE
                ;;
            *)
                unset BW
                WORDS=$RATE
                ;;
        esac
        run_test _rx_benchmark_extended 50 $RATE
    done
}

_tx_benchmark_extended()
{
    local ARGS RUNS BS SR TARGET
    RUNS=$1
    BS=$2
    SR=$3
    TARGET=$(target_90pct_thruput $SR)
    ARGS="-c $CARD --time=5 --threshold=200 --rate=$SR --target=$TARGET --block-size=$BS --threads=4"

    local i=0 good=0 bad=0
    while [ $i -lt $RUNS ]; do
        echo GOOD $good, BAD $bad
        echo "--- RUNNING $DIR/tx_benchmark$APP_SUFFIX $ARGS"
        $DIR/tx_benchmark$APP_SUFFIX $ARGS
        if [ $? -eq 0 ]; then
            good=$((good + 1))
        else
            bad=$((bad + 1))
        fi
        i=$((i + 1))
    done
    echo FINAL: GOOD $good, BAD $bad

    return $((bad > 0))
}

tx_benchmark_extended()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        for sr in 50000000 61440000 100000000 122880000; do
            run_test _tx_benchmark_extended 25 65532 $sr
        done
    else
        echo "===== SKIPPING ${FUNCNAME[0]} - running lots of tx_benchmark on non-JESD platform misses the point of this test" >&2
    fi
}

_rx_sweep_testbench()
{
    local ARGS STOP REPEAT ARGST1 MINFREQ

    REPEAT=$1
    ARGS="-c $CARD --rx --step 1e6 --repeat $REPEAT"

    MINFREQ=$((SKIQ_MIN_RX_FREQ_HZ/ONE_MHZ))

    # for X2 and X4, run at a sample rate of 61.44Msps in order to tune down to 70MHz
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        ARGS="$ARGS -r 61.44e6"
        if (($MINFREQ < 46)); then
            MINFREQ=46 # must be > -r/2
        fi
    fi

    # for NV100, run at a specific sample rate since the RFIC is profile based and cannot accept the
    # default sample rate of sweep_receive
    if [[ "$SKIQ_HW" == "NV100" ]]; then
        ARGS="$ARGS -r 30.72e6"
    fi

    # Adjust settings for Gateworks boards only running USB
    if [[ "$PLATFORM" == *"Gateworks"* && "$HAS_PCIE" == "false" && "$HAS_USB" == "true" ]] ; then
        echo -e "\n===== USB Gateworks configuration; setting sample rate to $MAX_SAMPLE_RATE Hz..."
        ARGS="$ARGS -r $MAX_SAMPLE_RATE"
    fi

    # Test 1
    if [[ $HAS_DKIQ == "true" ]]; then
        ARGST1="$ARGS --start 5e6"     # Why do we do this??????
    else
        ARGST1="$ARGS --start $((MINFREQ))e6"
    fi

    run_test $DIR/sweep_testbench$APP_SUFFIX $ARGST1 -f $RESULTS/tune.txt

    if [[ $HAS_FREQ_HOP == "true" && $HAS_DKIQ == "false" ]]; then
        # split up the frequency range since frequency hopping can only be performed on a list of up
        # to 512 entries
        for start in $(seq $((MINFREQ)) 512 6000); do
            STOP=$(minimum $((start+511)) 6000)
            run_test $DIR/sweep_testbench$APP_SUFFIX $ARGS --freq-hop \
                     --start ${start}e6 \
                     --stop ${STOP}e6 \
                     -f $RESULTS/tune_freq_hop_${start}MHz_to_${STOP}MHz.txt
        done
    fi
}

rx_sweep_testbench()
{
    _rx_sweep_testbench 0
}

rx_sweep_testbench_extended()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        _rx_sweep_testbench 2
    elif [[ "$HAS_USB" == "true" && "$HAS_PCIE" == "false" ]]; then
        # if the DUT has USB without PCIe, then it must be a USB bitstream, so don't sweep as many
        # times
        _rx_sweep_testbench 4
    else
        _rx_sweep_testbench 9
    fi
}

_tx_sweep_testbench()
{
    local ARGS STOP REPEAT ARGST1 MINFREQ

    REPEAT=$1
    ARGS="-c $CARD --tx --step 1e6 --repeat $REPEAT"

    MINFREQ=$((SKIQ_MIN_TX_FREQ_HZ/ONE_MHZ))

    # for X2 and X4, run at a sample rate of 61.44Msps in order to tune down to 70MHz
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        ARGS="$ARGS -r 61.44e6"
        if (($MINFREQ < 46)); then
            MINFREQ=46 # must be > -r/2
        fi
    fi

    # for NV100, run at a specific sample rate since the RFIC is profile based and cannot accept the
    # default sample rate of sweep_receive
    if [[ "$SKIQ_HW" == "NV100" ]]; then
        ARGS="$ARGS -r 30.72e6"
    fi

    # Test 1
    if [[ $HAS_DKIQ == "true" ]]; then
        ARGST1="$ARGS --start 5e6"   # Why do we do this?
    else
        ARGST1="$ARGS --start $((MINFREQ))e6"
    fi

    run_test $DIR/sweep_testbench$APP_SUFFIX $ARGST1 -f $RESULTS/tune.txt

    # Test 2
    if [[ $HAS_FREQ_HOP == "true" && $HAS_DKIQ == "false" ]]; then
        # split up the frequency range since frequency hopping can only be performed on a list of up
        # to 512 entries
        for start in $(seq $((MINFREQ)) 512 6000); do
            STOP=$(minimum $((start+511)) 6000)
            run_test $DIR/sweep_testbench$APP_SUFFIX $ARGS --freq-hop \
                     --start ${start}e6 \
                     --stop ${STOP}e6 \
                     -f $RESULTS/tune_freq_hop_${start}MHz_to_${STOP}MHz.txt
        done
    fi
}

tx_sweep_testbench()
{
    _tx_sweep_testbench 0
}

tx_sweep_testbench_extended()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        _tx_sweep_testbench 2
    elif [[ "$HAS_USB" == "true" && "$HAS_PCIE" == "false" ]]; then
        # if the DUT has USB without PCIe, then it must be a USB bitstream, so don't sweep as many
        # times
        _tx_sweep_testbench 4
    else
        _tx_sweep_testbench 9
    fi
}

tx_timestamp_tests()
{
    local ARGS

    ARGS="-c $CARD --run-empty-fifo-test"
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        ARGS="$ARGS -r 50e6 -s $DATA_DIR/tx_waveform_20MS_le_16b.dat --run-late-test"
    elif [[ "$SKIQ_HW" == "NV100" ]]; then
        ARGS="$ARGS -r 30.72e6 -s $DATA_DIR/tx_waveform_20MS_le_16b.dat --run-late-test"
    else
        ARGS="$ARGS -r 5e6 -s $DATA_DIR/tx_waveform_20MS_le.dat"
    fi

    if [[ $HAS_TX == "false" ]] ; then
        # no support for Tx
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for Tx" >&2
        return
    else
        run_test $DIR/tx_timestamp_tests$APP_SUFFIX $ARGS
        if [[ $HAS_2TXCHAN == "true" ]]; then
            run_test $DIR/tx_timestamp_tests$APP_SUFFIX $ARGS --handle=A2
        fi
        if [[ "$SKIQ_HW" != "MPCIE" ]]; then
            run_test $DIR/tx_timestamp_tests$APP_SUFFIX $ARGS --timestamp-base=system
        fi
    fi
}

prbs_test()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for PRBS test" >&2
    elif [[ "$SKIQ_HW" == "NV100" ]]; then
        local ARGS
        ARGS="-c $CARD --time=10 -d $OUTDIR/prbs_results.txt"
        ARGS="$ARGS --start=30720000 --step=30720000"

        run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000
        if [[ $HAS_2CHAN == "true" ]] ; then
            run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000 --channels=2
        fi
    else
        local ARGS
        ARGS="-c $CARD --time=3 -d $OUTDIR/prbs_results.txt"
        ARGS="$ARGS --start=1000000 --step=5000000"

        run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000
        if [[ $HAS_2CHAN == "true" ]] ; then
            if [[ "$SKIQ_HW" == "M2" ]]; then
                run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000 --channels=2
            else
                run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=30720000 --channels=2
            fi
        fi
    fi
}

test_fir_api()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" || "$SKIQ_HW" == "NV100" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for custom FIR coefficients" >&2
    else
        local RATES
        RATES="233000 13300000 23000000 40000000 46000000"

        if [[ $HAS_TX == "false" ]] ; then
            SKIP_TRANSMIT="--skip-transmit"
        fi

        # Note: this doesn't test thruput, so sample rate range doesn't matter (neither does BW)
        for RATE in $RATES; do
            run_test $DIR/test_fir_api$APP_SUFFIX -c $CARD -r $RATE $SKIP_TRANSMIT
        done

        if [[ $HAS_2TXCHAN == "true" ]] ; then
            for RATE in $RATES; do
                run_test $DIR/test_fir_api$APP_SUFFIX -c $CARD -r $RATE -h A2 $SKIP_TRANSMIT
            done
        fi
    fi
}

timestamp_test()
{
    #################################################################################
    # TODO: revisit this during or on completion of LIB-340 ->
    # https://epiqsolutions.atlassian.net/browse/LIB-340
    #
    # have to multiply MAX_SAMPLE_RATE by 2 due to the reduction for timestamp_tests
    # specificly for MPCIE dual channel there is no need to divide by two and for
    # the other options explicitly multiply by two
    #################################################################################
    
    local START ARGS EXTRA_ARGS STEP STOP

    START=1000000
    if [[ "$SKIQ_HW" != "X2" && "$SKIQ_HW" != "X4" && "$SKIQ_HW" != "NV100" ]]; then
        STEP=$(( ( (MAX_SAMPLE_RATE * 2) - START ) / 25 ))
        STOP=$(( (MAX_SAMPLE_RATE * 2) + 1 ))
        ARGS="--start=$START"
        EXTRA_ARGS="--stop=$STOP --step=$STEP"
    fi

    ARGS="$ARGS --blocks=2000 -c $CARD --verbose --exit-on-fail"
    if [[ $PLATFORM == $X86_64 ]] ; then
        ARGS="$ARGS --counter-validation"
    fi

    run_test $DIR/timestamp_test$APP_SUFFIX $ARGS $EXTRA_ARGS --handle=A1

    # Perform testing on secondary (and above) channels only if HAS_2CHAN is set
    if [[ $HAS_2CHAN == "true" ]]; then
        if [[ "$SKIQ_HW" == "MPCIE" ]]; then
            STEP=$(( ( MAX_SAMPLE_RATE - START ) / 25 ))
            STOP=$(( MAX_SAMPLE_RATE + 1 ))
            EXTRA_ARGS="--stop=$STOP --step=$STEP"
        fi

        if [[ "$SKIQ_HW" != "NV100" ]]; then
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS $EXTRA_ARGS --handle=A2
        fi

        if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" || "$SKIQ_HW" == "NV100" ]]; then
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS $EXTRA_ARGS --handle=B1
        fi

        if [[ "$SKIQ_HW" == "X4" ]]; then
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS $EXTRA_ARGS --handle=B2
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS $EXTRA_ARGS --handle=C1
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS $EXTRA_ARGS --handle=D1
        fi
    fi
}

timestamp_read_test()
{
    local ARGS

    ARGS="-c $CARD --verbose"
    if [[ $HAS_PCIE == "true" ]]; then
        ARGS="$ARGS -n 1000000"
    elif [[ $HAS_USB == "true" ]]; then
        ARGS="$ARGS -n 100000"
    fi

    run_test $DIR/timestamp_read_test$APP_SUFFIX $ARGS
    case "$SKIQ_HW" in
        MPCIE|M2)
            run_test $DIR/timestamp_read_test$APP_SUFFIX $ARGS --rfic-config $DATA_DIR/lte_10MHz_rx1_rx2_agc_on.txt
            ;;
        *)
            ;;
    esac
}

sweep_receive()
{
    local ARGS REPEAT START STOP STEP RATE

    if [[ $HAS_PCIE == "true" ]] ; then
        REPEAT=3999
    else
        REPEAT=999
    fi

    if [[ $HAS_DKIQ == "true" ]] ; then
        START=20000000
        STOP=900000000
    else
        START=800000000
        STOP=900000000
    fi
    STEP=$(( (STOP-START) / 10 ))

    if [ "$SKIQ_HW" == "X2" ]; then
        RATE=153600000
    elif [ "$SKIQ_HW" == "X4" ]; then
        RATE=245760000
        REPEAT=100
    elif [ "$SKIQ_HW" == "NV100" ]; then
        RATE=30720000
    else
        RATE=$(( (MAX_SAMPLE_RATE / 4) * 3 ))
    fi
    ARGS="-c $CARD -r $RATE --start $START --stop $STOP --step $STEP"
    ARGS="$ARGS --repeat $REPEAT"

    run_test_capture $RESULTS/sweep-receive.txt $DIR/sweep_receive$APP_SUFFIX $ARGS
    if [[ $HAS_BLOCK_RX == "true" ]] ; then
        run_test_capture $RESULTS/sweep-receive-blocking.txt $DIR/sweep_receive$APP_SUFFIX $ARGS --blocking
    fi
}

sweep_receive_extended()
{
    local ARGS RATE REPEAT START STOP STEP

    if [[ $HAS_DKIQ == "true" ]] ; then
        START=20000000
        STOP=6000000000
    else
        START=$((SKIQ_MIN_RX_FREQ_HZ))
        STOP=6000000000
    fi

    case "$SKIQ_HW" in
        X2)
            RATE=153600000
            START=$(maximum $START $((RATE / 2)))
            STEP=1024000
            REPEAT=99
            ;;
        X4)
            RATE=245760000
            START=$(maximum $START $((RATE / 2)))
            STEP=$((5 * 1024000))
            REPEAT=99
            ;;
        NV100)
            RATE=40000000
            STEP=1024000
            REPEAT=99
            ;;
        *)
            RATE=$(( (MAX_SAMPLE_RATE / 4) * 3 ))
            STEP=1024000
            if [[ $HAS_PCIE == "true" ]] ; then
                REPEAT=99
            else
                REPEAT=1
            fi
            ARGS="$ARGS --blocks 10"
            ;;
    esac
    ARGS="$ARGS -c $CARD -r $RATE --start $START --stop $STOP --step $STEP"
    if (( REPEAT > 0 )); then
        ARGS="$ARGS --repeat $REPEAT"
    fi

    run_test_capture $RESULTS/sweep-receive-extended.txt $DIR/sweep_receive$APP_SUFFIX $ARGS
    if [[ $HAS_BLOCK_RX == "true" ]] ; then
        run_test_capture $RESULTS/sweep-receive-blocking-extended.txt $DIR/sweep_receive$APP_SUFFIX $ARGS --blocking
    fi
}

version_test()
{
    if [ -n "$REPORT_BSP_VERS" ]
    then
        echo "=== BSP VERSION INFORMATION ==="
        run_test_capture $RESULTS/bsp-version.txt $REPORT_BSP_VERS
        echo "==============================="
    fi
    run_test_capture $RESULTS/version-test.txt $DIR/version_test$APP_SUFFIX -c $CARD
    run_test_capture $RESULTS/version-test-full.txt $DIR/version_test$APP_SUFFIX -c $CARD --full
}

old_version_test()
{
    old_version_test=( $DIR/version_test_v* )
    if [ -z "${old_version_test[*]}" ]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no older version of 'version_test' found" >&2
    elif [ "$PERFORM_OLD_VERSION_TEST" == "false" ]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - skip flag explicitly set" >&2
    else
        run_test "${old_version_test[0]}"
    fi
}

tx_configure_steps()
{
    if [[ $HAS_TX == "false" ]] ; then
        # no support for Tx
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for Tx" >&2
        return
    else
        local ARGS
        ARGS="-c $CARD --start-freq $((SKIQ_MIN_TX_FREQ_HZ)) --end-freq 6000000000 --freq-step 300000000 -a 359"
        ARGS="$ARGS --start-rate 1000000 --end-rate 2000000 --rate-step 1000000 -t 1"
        run_test $DIR/tx_configure_steps$APP_SUFFIX $ARGS
    fi

    echo "===== TODO ${FUNCNAME[0]}" >&2
}

test_ref_clock_switch()
{

    if [[ "$SKIQ_HW" == "M2" || "$SKIQ_HW" == "MPCIE" || "$SKIQ_HW" == "Z2P" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for switching reference clock on the fly" >&2
    elif [ "$HAS_EXT_REF_CLK" == "true" ]; then
        local ARGS
        ARGS="-c $CARD --ext-ref-clk"
        run_test_capture $RESULTS/test-ref-clock-switch.txt $DIR/test_ref_clock_switch$APP_SUFFIX $ARGS
    else
        local ARGS
        ARGS="-c $CARD"
        run_test_capture $RESULTS/test-ref-clock-switch.txt $DIR/test_ref_clock_switch$APP_SUFFIX $ARGS
    fi
}

read_accel()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for accelerometer" >&2
    else
        run_test_capture $RESULTS/read-accel.txt $DIR/read_accel$APP_SUFFIX -c $CARD --repeat 10
    fi
}

read_temp()
{
    local ARGS
    ARGS="-c $CARD --repeat 10"
    run_test_capture $RESULTS/read-temp.txt $DIR/read_temp$APP_SUFFIX $ARGS
    run_test_capture $RESULTS/read-temp-full.txt $DIR/read_temp$APP_SUFFIX $ARGS --full
    run_test_capture $RESULTS/read-temp-factory.txt $DIR/read_temp_factory$APP_SUFFIX $ARGS
    run_test_capture $RESULTS/read-temp-factory-full.txt $DIR/read_temp_factory$APP_SUFFIX $ARGS --full
}

read_voltage()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        run_test_capture $RESULTS/read-voltage.txt $DIR/read_voltage$APP_SUFFIX -c $CARD --repeat 10
    else
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for reading voltage / current" >&2
    fi
}

init_and_exit()
{
    local ARGS
    ARGS="-c $CARD --sleep-duration=1 --repeat 4"
    run_test_capture $RESULTS/init-and-exit.txt $DIR/init_and_exit$APP_SUFFIX $ARGS
    run_test_capture $RESULTS/init-and-exit-full.txt $DIR/init_and_exit$APP_SUFFIX $ARGS --full
}

jesd_sync_test()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        local ARGS RATE_LIST
        ARGS="-c $CARD --repeat 9999"

        if [[ "$SKIQ_HW" == "X2" ]]; then
            RATE_LIST=$X2_JESD_RATE_LIST
        elif [[ "$SKIQ_HW" == "X4" ]]; then
            RATE_LIST=$X4_JESD_RATE_LIST
        fi

        for rate in $RATE_LIST; do
            local EXTRA_ARGS

            mkdir -p $RESULTS/jesd-sync
            EXTRA_ARGS=" -d $RESULTS/jesd-sync/jesd-sync-regression-${rate}.csv -r $rate"
            run_test_capture $RESULTS/jesd-sync/jesd-sync-regression-${rate}.txt $DIR/jesd_sync_test_regression$APP_SUFFIX $ARGS $EXTRA_ARGS
        done

        # grab the summary from each test log and copy to jesd-sync-summary.txt
        grep -hE '(trials|Completed testing)' $RESULTS/jesd-sync/jesd-sync-regression-*.txt > $RESULTS/jesd-sync-summary.txt
    else
        echo "===== SKIPPING ${FUNCNAME[0]} - no JESD available" >&2
    fi
}

test_delay()
{
    # test various delay values, no need to run every time however so it's purposefully left out of
    # the TEST array
    run_test $DIR/test_delay
}

rfic_ctrl_in_test()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" || "$SKIQ_HW" == "NV100" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for rfic_ctrl_in test" >&2
    else
        local ARGS
        ARGS="$CARD"
        run_test $DIR/rfic_ctrl_in_test$APP_SUFFIX $ARGS
    fi
}

pps_tester()
{
    local ARGS
    ARGS="--cards $CARD --runtime=100 --displaytstable=1 --success=99"

    run_test $DIR/pps_tester$APP_SUFFIX $ARGS
}

# skiq_exit disables the clock that is running the GPSDO control algorithm, 
# of course PPS pulses are still coming in. This causes the GPSDO control algorithm to "drift". 
# If the drift is long enough, the control algorithm becomes unstable and will go in and out of lock. 
# This is the reason for the 5 second delay, to give the control algorithm time to lose stability (for a failure).
# So if you imagine, skiq_exit stops the control algorithm (but the PPS signal is still coming on). 
# Skiq_init re-enables the clock but now the system is unstable and oscillating (possibly in and out of lock).
# Increasing POLLING_TIME along with CONSECUTIVE_ERRORS may help if the test is having difficulty locking to a GPS signal.
_gpsdo_tester()
{
    local i=0
    local NUM_OF_LOOPS=5
    local PPS_SOURCE=$1
    local POLLING_TIME=15
    local CONSECUTIVE_ERRORS=15
    local ARGS="-c $CARD --leave-enabled -e $CONSECUTIVE_ERRORS -p $POLLING_TIME --pps-source=$PPS_SOURCE"

    # run_test will exit if there is an error
    run_test $DIR/read_gpsdo$APP_SUFFIX $ARGS
    # We need to verify multiple times because the system can be oscillating in and out of lock.
    for i in $(seq $NUM_OF_LOOPS)
    do
        # run_test will exit if there is an error
        run_test $DIR/read_gpsdo$APP_SUFFIX $ARGS -t 30
        # A delay is required to give the control algorithm time to unlock to indicate a failure
        sleep 5
    done
    #run test one additional time but disable gpsdo before exit
    run_test $DIR/read_gpsdo$APP_SUFFIX -c $CARD -e $CONSECUTIVE_ERRORS -p $POLLING_TIME --pps-source=$PPS_SOURCE -t 30
}

gpsdo_tester()
{
    # This test should only be called if HAS_PPS or HAS_GPS is set and
    # the hardware supports it.
    if [[ "$HAS_PPS" == "true" ]]; then
        _gpsdo_tester "external"
    fi
    if [[ "$HAS_GPS" == "true" ]]; then
        _gpsdo_tester "host"
    fi
}

fpga_soft_reset()
{
    run_test $DIR/test_fpga_soft_reset$APP_SUFFIX -c $CARD --regression --verbose
}

test_warp_voltage_read()
{
    run_test $DIR/test_warp_voltage_read$APP_SUFFIX -c $CARD
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Functions
#
#   The functions listed below are utility functions used within the script.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

init_test()
{
    echo "..........................................................."
    echo "                Preparing system for test"
    echo "..........................................................."
    echo "..........................................................."
    echo "..."
    echo

    unset TEST
    local BEFORE_TEST
    if [[ $PLATFORM != $WIN64_MSYS ]] ; then
        if [[ $HAS_USB == "true" ]] && [[ $SKIQ_HW == "MPCIE" || $SKIQ_HW == "M2" ]] ; then
            BEFORE_TEST="$BEFORE_TEST fx2_reprogram"
        fi
        if [[ -n $FPGA ]] ; then
            if [[ $HAS_RUNTIME_PROG_FPGA == "true" ]] ; then
                BEFORE_TEST="$BEFORE_TEST prog_fpga"
            fi
            if [[ $HAS_FLASH == "true" ]] ; then
                BEFORE_TEST="$BEFORE_TEST store_user_fpga"
            fi
            if [[ $PLATFORM != $GATEWORKS_51XX && $PLATFORM != $GATEWORKS_54XX ]] ; then
                BEFORE_TEST="$BEFORE_TEST test_fpga_vers"
            fi
        fi
    fi
    if [[ "$HAS_PPS" == "true" ]] ; then
        BEFORE_TEST="$BEFORE_TEST pps_tester"
    fi

    if [[ "$SKIQ_HW" == "M2_2280" || "$SKIQ_HW" == "NV100" || "$SKIQ_HW" == "Z3U" ]]; then
        if [[ "$HAS_PPS" == "true" || "$HAS_GPS" == "true" ]]; then
            BEFORE_TEST="$BEFORE_TEST gpsdo_tester"
        fi
    fi

    TEST="$TEST rfic_rw_test"
    TEST="$TEST i2c_rw_test"
    TEST="$TEST fpga_rw_test"

    if [[ "$HAS_RCS" == "false" ]]; then
        echo "..........................................................."
        echo "Not an RCS bitstream, allowing some additional tests..."
        echo
        TEST="$TEST user_reg_verify"
    fi

    TEST="$TEST timestamp_reset"
    TEST="$TEST rx_benchmark"
    TEST="$TEST rx_sweep_testbench"

    if [[ "$PERFORM_TRANSMIT" == "true" ]]; then
        echo "..........................................................."
        echo "Adding transmit related tests..."
        echo
        TEST="$TEST test_fir_api"
        TEST="$TEST tx_benchmark"
        TEST="$TEST xcv_benchmark"
        TEST="$TEST tx_sweep_testbench"
        TEST="$TEST tx_configure_steps"
        TEST="$TEST tx_timestamp_tests"
    fi

    if [[ "$PERFORM_EXTENDED" == "true" ]]; then
        echo "..........................................................."
        echo "Adding extended tests..."
        echo
        TEST="$TEST rx_benchmark_extended"
        TEST="$TEST rx_sweep_testbench_extended"
        if [[ "$PERFORM_TRANSMIT" == "true" ]]; then
            TEST="$TEST tx_benchmark_extended"
            TEST="$TEST tx_sweep_testbench_extended"
        fi
        TEST="$TEST timestamp_test"
        TEST="$TEST jesd_sync_test"
        TEST="$TEST sweep_receive_extended"
    fi

    if [[ -n $FPGA && "$HAS_RUNTIME_PROG_FPGA" == true ]]; then
        TEST="$TEST rand_prog_fpga_and_receive"
    fi

    TEST="$TEST rx_samples"
    if [[ "$HAS_PPS" == "true" ]] ; then
        TEST="$TEST rx_samples_on_1pps"
    fi
    if [[ $HAS_2CHAN == "true" ]]; then
        TEST="$TEST rx_samples_on_synced"
    fi

    if [[ "$SKIQ_HW" == "X4" ]]; then
        TEST="$TEST rx_samples_in_phase"
    fi

    TEST="$TEST rx_samples_minimal"
    TEST="$TEST prbs_test"
    TEST="$TEST timestamp_read_test"
    TEST="$TEST sweep_receive"
    TEST="$TEST test_ref_clock_switch"
    TEST="$TEST read_accel"
    TEST="$TEST read_temp"
    TEST="$TEST read_voltage"
    TEST="$TEST rfic_ctrl_in_test"
    TEST="$TEST init_and_exit"
    TEST="$TEST version_test"
    TEST="$TEST fpga_soft_reset"
    TEST="$TEST old_version_test"
    TEST="$TEST test_warp_voltage_read"

    exec 3>&1 1> >(tee -a $RESULTS/test-order.txt)
    if [[ "$SHUFFLE_TESTS" == "true" ]]; then
        local res SEED_TYPE SHUFFLE_TEST_LIST shuffled has_shuf has_awk

        # If SHUFFLE_TEST_SEED is unset or empty, use the current process ID as a seed
        if [[ ! $SHUFFLE_TEST_SEED ]]; then
            SHUFFLE_TEST_SEED=$$
            SEED_TYPE="PID"
        else
            SEED_TYPE="specified PID"
        fi

        shuffled=0
        has_shuf=$(command -v shuf)
        if [ "$has_shuf" != "" ]; then
            echo "Using $SEED_TYPE ($SHUFFLE_TEST_SEED) to seed the random shuffle for reproducibility"
            SHUFFLE_TEST_LIST=$(shuf --random-source=<(get_seeded_random $SHUFFLE_TEST_SEED) -e $TEST)
            res=$?
            if [ "$res" -eq "0" ]; then
                shuffled=1
                TEST="$SHUFFLE_TEST_LIST"
            else
                echo "Failed to randomize test list ($res)"
            fi
        fi
        if [ "$shuffled" -eq "0" ]; then
            has_awk=$(command -v awk)
            if [ "$has_awk" != "" ]; then
                echo "Using AWK with $SEED_TYPE ($SHUFFLE_TEST_SEED) to seed the random shuffle for reproducibility"
                awk_shuf "$SHUFFLE_TEST_SEED" "${TEST[@]}"
                res=$?
                if [ "$res" -eq "0" ]; then
                    shuffled=1
                    TEST="$SHUFFLE_TEST_LIST"
                else
                    echo "Failed to randomize test list ($res)"
                fi
            fi
        fi
        if [ "$shuffled" -eq "0" ]; then
            echo "Shuffle failed or platform does not have shuffle command; running linear test set"
        fi
    else
        echo "Skipping shuffle and running linear test set"
    fi
    TEST="$BEFORE_TEST $TEST"
    exec 1>&3 3>&-

    echo "Running tests in this order:" | tee -a $RESULTS/test-order.txt
    for t in $TEST; do
        echo "  * $t"
    done | tee -a $RESULTS/test-order.txt
    echo
    echo "..........................................................."
}

card_mgr_uninit()
{
    echo "..........................................................."
    echo "                   Card Mgr Uninit"

    if [[ $PLATFORM != $WIN64_MSYS ]] ; then
        local rc

        run_test $DIR_PRIVATE/card_mgr_uninit$APP_SUFFIX
        rc=$?
        if [[ $rc -ne 0 ]] ; then
            exit 1
        fi
    fi
}


detect_platform()
{
    local rc

    echo "..........................................................."
    echo "                   Detecting Platform"

    # Specific to Gateworks boards
    if [ -e /sys/devices/soc0/machine ] ; then
        awk 'BEGIN {r=1} /GW51XX/{r=0} END {exit r}' /sys/devices/soc0/machine
        rc=$?
        if [[ $rc -eq 0 ]] ; then
            PLATFORM=$GATEWORKS_51XX
            return
        fi

        awk 'BEGIN {r=1} /GW54XX/{r=0} END {exit r}' /sys/devices/soc0/machine
        rc=$?
        if [[ $rc -eq 0 ]] ; then
            PLATFORM=$GATEWORKS_54XX
            return
        fi

        # Map the GW52xx boards to the 54XX platform.
        awk 'BEGIN {r=1} /GW52XX/{r=0} END {exit r}' /sys/devices/soc0/machine
        rc=$?
        if [[ $rc -eq 0 ]]; then
            PLATFORM=$GATEWORKS_54XX
            return
        fi

        # Map the GW53xx boards to the 54XX platform.
        awk 'BEGIN {r=1} /GW53XX/{r=0} END {exit r}' /sys/devices/soc0/machine
        rc=$?
        if [[ $rc -eq 0 ]]; then
            PLATFORM=$GATEWORKS_54XX
            return
        fi
    fi

    if [[ $(uname -o) == "Msys" ]]; then
        PLATFORM=$WIN64_MSYS
        PERFORM_OLD_VERSION_TEST="false"
        return
    fi

    case $(uname -m) in
        armv7)
            PLATFORM=$ARMV7
            ;;
        x86_64)
            PLATFORM=$X86_64
            ;;
        aarch64)
            PLATFORM=$AARCH64
            ;;
        *)
    esac
}


detect_sidekiq_product_type()
{
    local rc HW_VER_APP
    HW_VER_APP=$DIR_PRIVATE/test_hardware_vers$APP_SUFFIX

    if [[ ! -e "$HW_VER_APP" ]] ; then
        echo "ERROR: $HW_VER_APP not found!"
        exit 1
    elif [[ ! -x "$HW_VER_APP" ]] ; then
        echo "ERROR: $HW_VER_APP not executable!"
        exit 1
    fi

    $HW_VER_APP -c $CARD -p 001 &> /dev/null
    rc=$?
    if [[ $rc -eq 0 ]] ; then
        echo 001
    else
        $HW_VER_APP -c $CARD -p 002 &> /dev/null
        rc=$?
        if [[ $rc -eq 0 ]] ; then
            echo 002
        else
            echo "ERROR: unable to determine Sidekiq hardware type"
            exit 1
        fi
    fi
}


detect_sidekiq()
{
    local rc HW_VER_APP SKP ADDITIONAL_HW_DETECT
    echo "..........................................................."
    echo "                   Detecting Sidekiq"

    ADDITIONAL_HW_DETECT="true"

    HW_VER_APP=$DIR_PRIVATE/test_hardware_vers$APP_SUFFIX
    SKP=$DIR_PRIVATE/sidekiq_probe$APP_SUFFIX

    if [[ ! -e "$HW_VER_APP" ]] ; then
        echo "ERROR: $HW_VER_APP not found!"
        exit 1
    elif [[ ! -x "$HW_VER_APP" ]] ; then
        echo "ERROR: $HW_VER_APP not executable!"
        exit 1
    fi

    if [[ ! -e "$SKP" ]] ; then
        echo "ERROR: $SKP not found!"
        exit 1
    elif [[ ! -x "$SKP" ]] ; then
        echo "ERROR: $SKP not executable!"
        exit 1
    fi

    if [[ -n $SERIAL ]] ; then
        CARD=$($SKP --serial "$SERIAL" --fmtstring=%c)
        if [[ -z $CARD ]] ; then
            echo "No card found with serial number: $SERIAL"
            exit 1
        fi
    fi

    $HW_VER_APP -c $CARD -a DROPKIQ &> /dev/null
    rc=$?
    if [[ $rc -eq 0 ]] ; then
        HAS_DKIQ="true"
    fi

    SKIQ_HW=$($SKP -c $CARD --fmtstring=%P | awk '{print toupper($0)}')
    case "$SKIQ_HW" in
        MPCIE)
            SKIQ_PROD=$(detect_sidekiq_product_type)
            HAS_PACKED_RX=true
            if [[ $HAS_DKIQ == "true" ]] ; then
                SKIQ_MIN_TX_FREQ_HZ=$DROPK_MIN_TX_FREQ_HZ
                SKIQ_MIN_RX_FREQ_HZ=$DROPK_MIN_RX_FREQ_HZ
            else
                SKIQ_MIN_TX_FREQ_HZ=$MPCIE_MIN_TX_FREQ_HZ
                SKIQ_MIN_RX_FREQ_HZ=$MPCIE_MIN_RX_FREQ_HZ
            fi
            ;;
        X2)
            HAS_2CHAN=true
            HAS_PACKED_RX=false
            SKIQ_MIN_TX_FREQ_HZ=$X2_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$X2_MIN_RX_FREQ_HZ
            ;;
        M.2)
            SKIQ_HW=M2
            SKIQ_PROD=$(detect_sidekiq_product_type)
            HAS_PACKED_RX=true
            SKIQ_MIN_TX_FREQ_HZ=$M2_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$M2_MIN_RX_FREQ_HZ
            ;;
        X4)
            HAS_2CHAN=true
            HAS_PACKED_RX=false
            SKIQ_MIN_TX_FREQ_HZ=$X4_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$X4_MIN_RX_FREQ_HZ
            ;;
        M.2-2280)
            SKIQ_HW=M2_2280
            HAS_PACKED_RX=true
            SKIQ_MIN_TX_FREQ_HZ=$M2_2280_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$M2_2280_MIN_RX_FREQ_HZ
            ;;
        Z3U)
            HAS_PACKED_RX=false
            SKIQ_HW=Z3U
            # TOOD: figure out why dual counter is failing
            #HAS_2CHAN=true
            HAS_2CHAN=false
            # As of June 2021, the "old version_test" is using 4.11.1 which
            # has no concept of the Z3u... so skip it.
            PERFORM_OLD_VERSION_TEST=false
            SKIQ_MIN_TX_FREQ_HZ=$Z3U_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$Z3U_MIN_RX_FREQ_HZ
            REPORT_BSP_VERS='apt list z3u*'
            ADDITIONAL_HW_DETECT="false"
            ;;
        Z2P)
            HAS_PACKED_RX=false
            SKIQ_HW=Z2P
            HAS_2CHAN=false
            # As of June 2021, the "old version_test" is using 4.11.1 which
            # has no concept of the Z2p... so skip it.
            PERFORM_OLD_VERSION_TEST=false
            SKIQ_MIN_TX_FREQ_HZ=$Z2P_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$Z2P_MIN_RX_FREQ_HZ
            ADDITIONAL_HW_DETECT="false"
            ;;
        NV100)
            HAS_2CHAN=true
            HAS_PACKED_RX=false
            HAS_FREQ_HOP=false
            # As of August 2021, the "old version_test" is using 4.11.1 which
            # has no concept of the NV100, so skip it.
            PERFORM_OLD_VERSION_TEST=false
            SKIQ_MIN_TX_FREQ_HZ=$NV100_MIN_TX_FREQ_HZ
            SKIQ_MIN_RX_FREQ_HZ=$NV100_MIN_RX_FREQ_HZ
            ;;
        *)
            echo "ERROR: found unsupported Sidekiq hardware type: $SKIQ_HW"
            exit 1
            ;;
    esac
    echo "INFO: found supported Sidekiq hardware type: $SKIQ_HW"

    case $SKIQ_PROD in
        001)
            HAS_2CHAN=true
            ;;
        002)
            HAS_2CHAN=false
            ;;
    esac

    if [[ $ADDITIONAL_HW_DETECT == "true" ]]; then
        $HW_VER_APP -c $CARD -m PCIE &> /dev/null
        rc=$?
        if [[ $rc -eq 0 || "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" || "$SKIQ_HW" == "M2_2280" || "$SKIQ_HW" == "NV100" ]] ; then
            HAS_PCIE="true"
            if [[ $PLATFORM != $WIN64_MSYS ]] ; then
                # Only Linux platforms have blocking receive, asynchronous transmit, and low latency
                # receive over PCIe
                HAS_BLOCK_RX="true"
                HAS_LOW_LATENCY="true"
                HAS_ASYNC_TX="true"
            fi
        fi

        $HW_VER_APP -c $CARD -m USB &> /dev/null
        rc=$?
        if [[ $rc -eq 0 ]] ; then
            HAS_USB="true"
            if [[ $PLATFORM != $WIN64_MSYS ]] ; then
                # Only Linux platforms have blocking receive over USB transport
                HAS_BLOCK_RX="true"
            fi

            # treat the USB-only case differently, only single channel, no asynchronous transmit, and no
            # low latency support
            if [[ $HAS_PCIE == "false" ]] ; then
                HAS_2CHAN="false"
                HAS_ASYNC_TX="false"
                HAS_LOW_LATENCY="false"
            fi
        fi
    fi
}

set_test_parameters()
{
    echo "..........................................................."
    echo "                 Setting test parameters"

    # check if transmit related testing is to be performed
    case "$TRANSMIT" in
        y|yes|1)
            PERFORM_TRANSMIT=true
            ;;
        *)
            PERFORM_TRANSMIT=false
            ;;
    esac

    # check if RCS related testing is to be performed (or some tests skipped)
    case "$RCS" in
        y|yes|1)
            HAS_RCS=true

            # override transmit testing, RCS bitstreams do not have transmit capability
            PERFORM_TRANSMIT=false
            ;;
        *)
            HAS_RCS=false
            ;;
    esac

    # check if extended testing is to be performed
    case "$EXTENDED" in
        y|yes|1)
            PERFORM_EXTENDED=true
            ;;
        *)
            PERFORM_EXTENDED=false
            ;;
    esac

    if [[ $PLATFORM == $X86_64 && $HAS_PCIE == "true" ]] ; then
        # generic x86_64 with PCIE

        if [[ $SKIQ_HW == "M2" ]] ; then
            MAX_SAMPLE_RATE=61440000
            HAS_2TXCHAN=$HAS_2CHAN
        elif [[ $SKIQ_HW == "X2" ]] ; then
            # For X2, the maximum sample rate is ultimately only used in the rx_benchmark test,
            # which only tests A1, so keep it at 153.6 Msps
            MAX_SAMPLE_RATE=153600000
            HAS_2TXCHAN=$HAS_2CHAN
        elif [[ $SKIQ_HW == "X4" ]] ; then
            # For X4, the maximum sample rate is ultimately only used in the rx_benchmark test,
            # which only tests A1, so keep it at 122.88 Msps
            MAX_SAMPLE_RATE=122880000
            HAS_2TXCHAN=$HAS_2CHAN
        elif [[ $SKIQ_HW == "M2_2280" ]] ; then
            MAX_SAMPLE_RATE=61440000
        elif [[ $SKIQ_HW == "NV100" ]] ; then
            MAX_SAMPLE_RATE=61440000
        else
            MAX_SAMPLE_RATE=45000000
        fi
    elif [[ $PLATFORM == $X86_64 && $HAS_USB == "true" ]] ; then
        # generic x86_64 with USB
        MAX_SAMPLE_RATE=8000000
    elif [[ $PLATFORM == $GATEWORKS_51XX && $HAS_PCIE == "true" ]] ; then
        # Gateworks 51XX (S10) with PCIE
        MAX_SAMPLE_RATE=37000000
    elif [[ $PLATFORM == $GATEWORKS_54XX && $HAS_PCIE == "true" ]] ; then
        # Gateworks 54XX (Flying Fox, MSK+) with PCIE
        MAX_SAMPLE_RATE=33000000
    elif [[ $PLATFORM == $GATEWORKS_51XX && $HAS_USB == "true" ]] ; then
        # Gateworks 51XX (S10) with USB
        MAX_SAMPLE_RATE=7000000
    elif [[ $PLATFORM == $GATEWORKS_54XX && $HAS_USB == "true" ]] ; then
        # Gateworks 54XX (Flying Fox, MSK+) with USB
        MAX_SAMPLE_RATE=6000000
    elif [[ $PLATFORM == $AARCH64 && $HAS_PCIE == "true" ]] ; then
        # NVIDIA Jetson platforms with PCIE
        if [[ $SKIQ_HW == "NV100" ]] ; then
            MAX_SAMPLE_RATE=61440000
        else
            MAX_SAMPLE_RATE=45000000
        fi
    elif [[ $PLATFORM == $AARCH64 && $HAS_USB == "true" ]] ; then
        # NVIDIA Jetson TX2 with USB
        MAX_SAMPLE_RATE=8000000
    elif [[ $HAS_PCIE == "true" ]] ; then
        # Unknown platform with PCIE
        MAX_SAMPLE_RATE=37000000
    elif [[ $HAS_USB == "true" ]] ; then
        # Unknown platform with USB
        MAX_SAMPLE_RATE=8000000
    elif [[ $SKIQ_HW == "Z3U" ]] ; then
        MAX_SAMPLE_RATE=40000000
        HAS_I2C_RW="true"
        HAS_TX="true"
        HAS_RUNTIME_PROG_FPGA="true"
        HAS_FLASH="false"
    elif [[ $SKIQ_HW == "Z2P" ]] ; then
        MAX_SAMPLE_RATE=40000000
        HAS_I2C_RW="true"
        HAS_TX="true"
        # Z2p does actually support this, but due to Z2p not being public and
        # also due to lack of udev rules related to the device tree entries, we'll 
        # say that reprogramming the FPGA isn't supported for now
        HAS_RUNTIME_PROG_FPGA="false"
        HAS_FLASH="false"
    else
        echo "ERROR: failed to set test parameters"
        exit 1
    fi

    #################################################################################
    # TODO: revisit this during or on completion of LIB-340 ->
    # https://epiqsolutions.atlassian.net/browse/LIB-340
    #
    # use 50% for MAX_SAMPLE_RATE to reduce failures at the top ends of performance
    # removed 75% MAX_SAFE_RECEIVE_SAMPLE_RATE from Z products since it is no longer
    # needed using the new 50% safe guard
    #################################################################################
    echo "Warning: using 50% of MAX_SAMPLE_RATE to reduce failures" >&2
    MAX_SAMPLE_RATE=$((MAX_SAMPLE_RATE / 2))

    if [[ "$PPS" -eq 1 ]] ; then
        HAS_PPS="true"
    else
        HAS_PPS="false"
    fi

    if [[ "$GPS" -eq 1 ]] ; then
        HAS_GPS="true"
    else
        HAS_GPS="false"
    fi

    if [[ $HAS_PCIE == "true" ]] ; then
        HAS_I2C_RW="true"
        HAS_TX="true"
    fi

    if [[ $HAS_USB == "true" ]] && [[ $PLATFORM != $WIN64_MSYS ]] ; then
        HAS_RUNTIME_PROG_FPGA="true"
    fi

    # RCS bitstreams only have one receive channel and no transmit channels, so override any
    # findings
    if [[ "$HAS_RCS" == "true" ]]; then
        HAS_2CHAN="false"
        HAS_2TXCHAN="false"
    fi

    if [[ $HAS_TX == "false" ]]; then
        PERFORM_TRANSMIT=false
    fi

     if [[ "$EXT_REF_CLK" -eq 1 ]] ; then
        HAS_EXT_REF_CLK="true"
    else
        HAS_EXT_REF_CLK="false"
    fi
}

set_fpga_bitstream()
{
    local rc SKP

    SKP=$DIR_PRIVATE/sidekiq_probe$APP_SUFFIX
    SKIQ_FPGA=$($SKP -c $CARD --fmtstring='%G')
    rc=$?
    if [[ $rc -ne 0 || ! $SKIQ_FPGA ]];  then
        echo "ERROR: failed to determine FPGA type on card $CARD"
        exit 1
    fi

    if [[ -n $FPGA ]] ; then
        FPGA=$(readlink -f $FPGA)
    else
        case $SKIQ_HW in
            M2)
                if [[ -n $FPGA_M2_PCIE && $HAS_PCIE == "true" ]] ; then
                    FPGA=$(readlink -f $FPGA_M2_PCIE)
                elif [[ -n $FPGA_M2_USB && $HAS_USB == "true" ]] ; then
                    FPGA=$(readlink -f $FPGA_M2_USB)
                fi
                ;;
            MPCIE)
                if [[ -n $FPGA_MPCIE_PCIE && $HAS_PCIE == "true" ]] ; then
                    FPGA=$(readlink -f $FPGA_MPCIE_PCIE)
                elif [[ -n $FPGA_MPCIE_USB && $HAS_USB == "true" ]] ; then
                    FPGA=$(readlink -f $FPGA_MPCIE_USB)
                fi
                ;;
            X2)
                if [[ -n $FPGA_X2_PCIE ]]; then
                    FPGA=$(readlink -f $FPGA_X2_PCIE)
                fi
                ;;
            X4)
                local FPGA_X4_PCIE_REF

                #
                # X4 (and to some point X2) has lots of different bitstreams, so check the FMC
                # carrier and FPGA type to determine which FPGA bitstream to use
                #
                SKIQ_FMC_CARRIER=$($SKP -c $CARD --fmtstring='%M')
                rc=$?
                if [[ $rc -ne 0 || ! $SKIQ_FMC_CARRIER ]];  then
                    echo "ERROR: failed to determine FMC carrier on card $CARD"
                    exit 1
                else
                    echo "Found a Sidekiq with $SKIQ_FPGA FPGA in a $SKIQ_FMC_CARRIER FMC carrier"
                fi

                # This is a neat way to construct a variable name in bash, then evalulate that
                # variable.  This way we can define a variable named FPGA_X4_PCIE_htg_k800_xcku060,
                # for example, and then evaluate it based on the discovered Sidekiq configuration
                # (SKIQ_FMC_CARRIER, SKIQ_FPGA)
                FPGA_X4_PCIE_REF=FPGA_X4_PCIE_${SKIQ_FMC_CARRIER}_${SKIQ_FPGA}
                FPGA_X4_PCIE=${!FPGA_X4_PCIE_REF}

                if [[ -n $FPGA_X4_PCIE ]]; then
                    FPGA=$(readlink -f $FPGA_X4_PCIE)
                fi
                ;;
            M2_2280)
                if [[ -n $FPGA_M2_2280_PCIE ]]; then
                    FPGA=$(readlink -f $FPGA_M2_2280_PCIE)
                fi
                ;;
            Z3U)
                if [[ -n $FPGA_Z3U ]]; then
                    FPGA=$(readlink -f $FPGA_Z3U)
                fi
                ;;
            NV100)
                if [[ -n $FPGA_NV100 ]]; then
                    FPGA=$(readlink -f $FPGA_NV100)
                fi
                ;;
        esac
    fi
}

parse_fpga_info()
{
    REV_FPGA_WITHOUT_SUFFIX=$(echo $FPGA | cut -d'.' -f 1 | rev)

    FPGA_MAJOR=$(echo $REV_FPGA_WITHOUT_SUFFIX | cut -d'_' -f 3 | rev | cut -d'v' -f 2)
    FPGA_MINOR=$(echo $REV_FPGA_WITHOUT_SUFFIX | cut -d'_' -f 2 | rev)
    FPGA_PATCH=$(echo $REV_FPGA_WITHOUT_SUFFIX | cut -d'_' -f 1 | rev)
    FPGA_GIT_HASH=$(echo $REV_FPGA_WITHOUT_SUFFIX | cut -d'_' -f 4 | rev)
    FPGA_BUILD_DATE=$(echo $REV_FPGA_WITHOUT_SUFFIX | cut -d'_' -f 5 | rev)
}



print_help()
{
    echo "Usage: $0 [test case(s)]"
    echo ""
    echo "Performs test cases to validate integrity of libsidekiq. Execution"
    echo "can be influenced by setting the following environment variables."
    echo ""
    echo "Variables:"
    echo -e "\tAPP_DIR=<dir>              location of test apps"
    echo -e "\tAPP_PRIVATE_DIR=<dir>      location of test apps using private APIs"
    echo -e "\tAPP_DATA_DIR=<dir>         location of test data (*.dat)"
    echo -e "\tAPP_SUFFIX=<suffix>        optional suffix on names of test apps"
    echo -e "\tCARD=<number>              Sidekiq card number to target"
    echo -e "\tFPGA=<file>                bitstream to test"
    echo -e "\tFPGA_MPCIE_PCIE=<file>     bitstream to test if MPCIE and PCIE"
    echo -e "\tFPGA_MPCIE_USB=<file>      bitstream to test if MPCIE and USB"
    echo -e "\tFPGA_M2_PCIE=<file>        bitstream to test if M2 and PCIE"
    echo -e "\tFPGA_M2_USB=<file>         bitstream to test if M2 and USB"
    echo -e "\tFPGA_X2_PCIE=<file>        bitstream to test if X2 and PCIE"
    echo -e "\tFPGA_X4_PCIE=<file>        bitstream to test if X4 and PCIE"
    echo -e "\tFPGA_M2_2280_PCIE=<file>   bitstream to test if M.2-2280 and PCIE"
    echo -e "\tFPGA_Z3U=<file>            bitstream to test if Z3U"
    echo -e "\tFPGA_NV100=<file>          bitstream to test if NV100"
    echo -e "\tFPGA_CONFIG_SLOT=<number>  config slot to store the FPGA bitstream in"
    echo -e "\tFW_MPCIE=<file>            firmware to test if MPCIE"
    echo -e "\tFW_M2=<file>               firmware to test if M2"
    echo -e "\tPPS=<0|1>                  set to 1 to enable PPS"
    echo -e "\tGPS=<0|1>                  set to 1 to enable GPS"
    echo -e "\tEXTENDED=<0|1>             set to 1 to perform extended testing"
    echo -e "\tTRANSMIT=<0|1>             set to 1 to perform transmit related testing"
    echo -e "\tRCS=<0|1>                  set to 1 to perform RCS related testing and skip conflicting tests"
    echo -e "\tDRY_RUN=<true|false>       set to true to show what commands would execute"
    echo -e "\tSHUFFLE_TESTS=<true|false> set to false to not shuffle test order (perform tests linearly)"
    echo -e "\tSHUFFLE_TEST_SEED=<#>      leave empty / unset to use the process ID to shuffle test order,"
    echo -e "\t                           or provide a number to use a previous shuffle test order"
    echo -e "\tCHECK_PHASE=<true|false>   set to true if equipment to measure phase coherence is present"
    echo ""
    echo "Test Cases:"

    for i in "${!TEST[@]}"; do
      echo -e "\t--${TEST[$i]}"
    done
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Script
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# exit everything on listed signals
trap "echo Exited!; exit;" SIGINT SIGTERM

if [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
    print_help
    exit 0
fi

# before any detection, we need to know the card number and directory to find
# our test applications

# default to a value of 0 if serial is not set
if [[ -z $SERIAL ]] ; then
    : ${CARD:=0}
fi

# default to running tests, not just displaying what would run
: ${DRY_RUN:=false}

# default to shuffling tests
: ${SHUFFLE_TESTS:=true}

# default to not checking phase
: ${CHK_PHASE:=false}

if [[ -n "$APP_DIR" ]] ; then
    DIR=$(readlink -f "$APP_DIR")
fi

if [[ -n "$APP_PRIVATE_DIR" ]] ; then
    DIR_PRIVATE=$(readlink -f "$APP_PRIVATE_DIR")
fi

if [[ -n "$APP_DATA_DIR" ]] ; then
    DATA_DIR=$(readlink -f "$APP_DATA_DIR")
fi

# define where output files go during testing
OUTDIR=$DIR/tmp_output
RESULTS=$DIR/results
mkdir -p "$OUTDIR" "$RESULTS"

# detect_platform needs:
#
# detect_platform assigns:
# - PLATFORM
detect_platform

card_mgr_uninit

# detect_sidekiq needs:
# - PLATFORM
#
# detect_sidekiq optionally needs:
# - SERIAL
#
# detect_sidekiq assigns:
# - HAS_2CHAN
# - HAS_ASYNC_TX
# - HAS_BLOCK_RX
# - HAS_DKIQ
# - HAS_FREQ_HOP
# - HAS_LOW_LATENCY
# - HAS_PACKED_RX
# - HAS_PCIE
# - PERFORM_OLD_VERSION_TEST
# - SKIQ_HW
# - SKIQ_MIN_RX_FREQ_HZ
# - SKIQ_MIN_TX_FREQ_HZ
# - SKIQ_PROD
#
# detect_sidekiq optionally assigns:
# - CARD
detect_sidekiq

# set_test_parameters needs:
# - SKIQ_HW
# - HAS_PCIE
# - PLATFORM
# - HAS_USB
#
# set_test_parameters assigns:
# - HAS_2TXCHAN
# - HAS_FLASH
# - HAS_I2C_RW
# - HAS_PPS
# - HAS_GPS
# - HAS_RCS
# - HAS_RUNTIME_PROG_FGPA
# - HAS_TX
# - MAX_SAMPLE_RATE
# - PERFORM_EXTENDED
# - PERFORM_TRANSMIT
# - HAS_EXT_REF_CLK

set_test_parameters

# set_fpga_bitstream needs:
# - FPGA
# - FPGA_M2_2280_PCIE
# - FPGA_M2_PCIE
# - FPGA_MPCIE_PCIE
# - FPGA_MPCIE_USB
# - FPGA_NV100
# - FPGA_X2_PCIE
# - FPGA_X4_PCIE_*
# - FPGA_Z3U
# - HAS_PCIE
# - HAS_USB
# - SKIQ_HW
# set_fpga_bitstream assigns:
# - FPGA (overwrites)
# - SKIQ_FPGA
# - SKIQ_FMC_CARRIER
set_fpga_bitstream

# init_test assigns:
# - TEST
# init_test needs:
# - FPGA
# - HAS_2CHAN
# - HAS_FLASH
# - HAS_PPS
# - HAS_GPS
# - HAS_RCS
# - HAS_RUNTIME_PROG_FPGA
# - PERFORM_EXTENDED
# - PERFORM_TRANSMIT
# - PLATFORM
# - SKIQ_HW
# - HAS_EXT_REF_CLK
init_test

if [[ $PLATFORM != $GATEWORKS_51XX && $PLATFORM != $GATEWORKS_54XX ]] ; then
    parse_fpga_info
fi

if true; then
    echo "-----------------------------------------------------------"
    echo "Test Info"
    echo "      Sidekiq $SKIQ_HW $SKIQ_PROD $PLATFORM"
    echo "      FPGA           [$SKIQ_FPGA]"
    [[ -n "$SKIQ_FMC_CARRIER" ]] && echo "      FMC carrier    [$SKIQ_FMC_CARRIER]"
    echo "      Dropkiq        [$HAS_DKIQ]"
    echo "      PCIe           [$HAS_PCIE]"
    echo "      USB            [$HAS_USB]"
    echo "      Card           [$CARD]"
    echo "      Serial         [$SERIAL]"
    echo "      Min RX Freq    [$SKIQ_MIN_RX_FREQ_HZ]"
    echo "      Min TX Freq    [$SKIQ_MIN_TX_FREQ_HZ]"
    if [[ -n $FPGA ]] ; then
        echo "      FPGA bitstream [$(basename $FPGA)]"
    else
        echo "      FPGA bitstream [N/A]"
    fi
    if [[ $SKIQ_HW == "M2" && -n $FW_M2 ]] ; then
        echo "      FW             [$(basename $FW_M2)]"
    elif [[ $SKIQ_HW == "MPCIE" && -n $FW_MPCIE ]] ; then
        echo "      FW             [$(basename $FW_MPCIE)]"
    else
        echo "      FW             [false]"
    fi
    echo "      PPS            [$HAS_PPS]"
    echo "      GPS            [$HAS_GPS]"
    echo "      Packed Rx      [$HAS_PACKED_RX]"
    echo "      Blocking Rx    [$HAS_BLOCK_RX]"
    echo "      Low Latency Rx [$HAS_LOW_LATENCY]"
    echo "      Async Tx       [$HAS_ASYNC_TX]"
    echo "      2 Rx Chan      [$HAS_2CHAN]"
    echo "      2 Tx Chan      [$HAS_2TXCHAN]"
    echo "      Run-time FPGA  [$HAS_RUNTIME_PROG_FPGA]"
    echo "      RCS            [$HAS_RCS]"
    echo "      I2C R/W        [$HAS_I2C_RW]"
    echo "      Flash access   [$HAS_FLASH]"
    echo "      Check Phase    [$CHK_PHASE]"
    echo "      Ext Ref Clk    [$HAS_EXT_REF_CLK]"
    echo "-----------------------------------------------------------"
    echo ""
fi | tee $RESULTS/test-info.txt


unset TEST_PASS TEST_RUN
if [[ -z $1 ]]; then
    for t in $TEST; do
        TEST_RUN="$t"
        if fn_exists "$t"; then
            $t
            TEST_PASS="$TEST_PASS $t"
        else
            break
        fi
        unset TEST_RUN
    done
else
    for var in "$@" ; do
        TEST_RUN="${var}"
        type -t ${var}
        if fn_exists "${var}"; then
            ${var}
            TEST_PASS="$TEST_PASS ${var}"
        else
            break
        fi
        unset TEST_RUN
    done
fi

echo "-----------------------------------------------------------"
echo "---------------- TESTS EXECUTION SUMMARY ------------------"
for t in $TEST_PASS ; do
    echo "  * [PASS] $t"
done
[[ -z "$TEST_RUN" ]] || echo "  * [NOT_FOUND] $TEST_RUN"
echo "-----------------------------------------------------------"
$DIR/version_test$APP_SUFFIX -c $CARD
echo "-----------------------------------------------------------"
[[ -z "$TEST_RUN" ]] || exit 1
