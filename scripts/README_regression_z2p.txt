Note: it is possible that the image does not have realtime priority settings, in which case, you may need to create
/etc/security/limits.d/99-fastrak_limits.conf with the following contents: "fastrak - rtprio 50"

    To prepare a fresh powered up Z2p, the following steps need to be performed:
    1) Manually load the drivers
       sudo /home/fastrak/storage/pcg/pcg_cmd load_drivers
    2) Update the permissions
       sudo chown fastrak /dev/uio*
       sudo chown -R fastrak /sys/devices/platform/pcg_adc_core*
       sudo chown -R fastrak /sys/devices/platform/pcg_dac_core*
       sudo chown -R fastrak /sys/bus/iio/devices/*
       sudo chown -R fastrak /dev/iio:device*
    3) Reprogram the FPGA with the BIN from https://jenkins-lts.epiq.rocks/job/Sidekiq_FPGAs/job/sidekiq-z-fpga-build/15/artifact/.output/build_sidekiq_z2p/
    4) Update the permssions again
       sudo chown fastrak /dev/uio*
       sudo chown -R fastrak /sys/devices/platform/pcg_adc_core*
       sudo chown -R fastrak /sys/devices/platform/pcg_dac_core*
       sudo chown -R fastrak /sys/bus/iio/devices/*
       sudo chown -R fastrak /dev/iio:device*

To run the test: ./skiq_regression_remote.sh --no-load-driver --no-load-fpga fastrak@192.168.0.15 /home/fastrak/storage/regression/
