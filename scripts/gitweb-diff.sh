#! /bin/bash
 
function print-help-and-exit()
{
    echo -e "Usage: ${0##*/} BASELINE-HASH FEATURE-HASH\n"
    echo -e "Generates a gitweb URL for viewing commit changes in a repository."
    exit 0
}
 
if [ 2 -ne $# ] ; then
    print-help-and-exit
fi
 
# This method is not specific to Epiq's /mnt/storage/gitrepo... path, but fails
# for repositories in subdirs of Epiq's gitrepo
#REPO=$(basename -s .git `git config --get remote.origin.url`).git
 
 
# The following will work with repositories in subdirs of .../mnt/storage/gitrepo/...
# but is specific to Epiq
if [ -e $(git config --get remote.origin.url) ]; then
    REPO=$(readlink -f $(git config --get remote.origin.url))
else
    REPO=$(git config --get remote.origin.url)
fi
REPO=${REPO#*gitrepo/}
BASE=$(git rev-parse --short $1)
FEATURE=$(git rev-parse --short $2)

URL="https://gitweb.epiq.rocks/?p=$REPO;a=commitdiff;hp=$BASE;h=$FEATURE;ds=sidebyside"
 
echo $URL
 
# If desired, un-comment the following to launch the preferred browser and fetch the URL
echo "Open in browser on Ubuntu using..."
echo "xdg-open \"$URL\""
echo
echo " ===== Use the following markdown in JIRA ===== "
echo
echo "h3. Review Material"
echo "* {{git difftool $BASE $FEATURE}}"
echo "* gitweb [diff|$URL]"
echo
echo "h3. Commit History"
echo "* {{git log \-\-reverse \-\-pretty=format:\"- \*%h\* %s\" $BASE..$FEATURE}}"
git log --reverse --pretty=format:"- {{*%h*}} %s" $BASE...$FEATURE | tee

# xdg-open $URL

echo
echo
echo " ===== Use the following markdown in Trello ===== "
echo
echo
echo "-----"
echo "### Review Material"
echo
echo "- \`git difftool $BASE $FEATURE\`"
echo "- gitweb [diff]($URL)"
echo
echo "-----"
echo "### Commit History"
echo
echo "- \`git log --reverse --pretty=format:\" - %h %s\" $BASE..$FEATURE\`"
git log --reverse --pretty=format:" - \`%h\` %s" $BASE...$FEATURE | tee
echo
