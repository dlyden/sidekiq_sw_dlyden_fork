#! /usr/bin/python3

###############################################################################
# Copyright 2021 Epiq Solutions, All Rights Reserved
#
# Author      : info@epiqsolutions.com
# Company     : Epiq Solutions
# File        : check_phase_coherence.py
# Description :
#   Designed to be used in conjunction with the rx_samples_in_phase regression
#   test, this script analyzes sets of IQ to verify phase coherent receive
#   operation on Sidekiq X4. A "set" is some number of IQ captures (currently 5)
#   made using handles A1,A2,B1,B2. For each capture, correlation is performed
#   relative to A1 to determine the phase difference. This phase difference is
#   expected to be fairly constant between captures, so a standard deviation of
#   the phase difference is calculated per handle to detect outliers. If any
#   handle deviates more than an empirically determined ACCEPTABLE_THRESHOLD,
#   the script will exit with a non-zero return code.
#
#   In addition, two csv files are created for historical perspective and/or
#   generating plots.  The first file contains the phase deltas and the
#   corresponding sample rate and frequency at which they were captured. The
#   second csv contains the maximum standard deviation of any handle, as well as
#   the corresponding rate/frequency.
#
#   Usage:
#   check_phase_coherence.py  <file prefix for a set of captures>
#                             <Number of runs per set>
#                             <Filename for the output CSV files>
#
#  File import and correlation functions taken from:
#  /mnt/storage/gitrepo/eval_rx.git @ commit e732bdc3
#
#  https://confluence.epiq.rocks/display/~jbaugher/Phase+coherence+notes
#
# LICENSE : The use of this source code is restricted to the terms and
# conditions of the license agreement, a copy of which can be found in the
# top level of the design directory or upon request. EXCEPT AS SPECIFICALLY
# STATED IN THE LICENSE AGREEMENT, THIS SOURCE CODE IS PROVIDED "AS IS"
# WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED, IMPLIED OR STATUTORY,
# INCLUDING WITHOUT LIMITATION, ANY WARRANTY WITH RESPECT TO
# NONINFRINGEMENT, MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE.
#
###############################################################################

import csv
import numpy as np
import os
import sys
import struct

#  This threshold serves as the acceptance critera and is valid ranging from
#  600MHz -> 4.5GHz.  If the per handle standard deviation between X runs exceeds
#  this value, it can be concluded the receiver is NOT operating in a phase coherent
#  receive mode.
ACCEPTABLE_THRESHOLD = .25

if os.path.isfile(sys.argv[3] + ".csv"):
    filename = open(sys.argv[3]+ ".csv",'ab')
    header2 = ""
    header = ""
else:
    filename = open(sys.argv[3]+".csv",'w')
    header = "phiA1,phiA2,phiB1,phiB2,rate(Hz),freq"
    header2 = ["max std_dev", "rate(Hz)", "freq"]
    with open(sys.argv[3]+"_std.csv", 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(header2)

def summary(DAT):
    np.set_printoptions(formatter={'float': lambda x: "{:8.5f}".format(x)})
    a = np.zeros(shape=(len(DAT),6))
    print("   phiA1    phiA2    phiB1    phiB2")
    frequency = sys.argv[1].split('-')[-2]
    rate = sys.argv[1].split('-')[-3]

    if type(DAT) != list:
        DAT = [DAT]
    for index,dat in enumerate(DAT):
        P = get_correlation_metrics(dat)
        tmp = []
        for p in sorted(P.keys()):
            tmp.append(P[p]['phi'])
        tmp.append(rate)
        tmp.append(frequency)

        s = ''.join([ ' {:8.5f}'.format(P[p]['phi']) for p in sorted(P.keys()) ])
        a[index]=tmp
        print(s)
        frequency = sys.argv[1].split('-')[-2]
        rate = sys.argv[1].split('-')[-3]
    #delimter gets ignored with a multi-format string
    np.savetxt(filename, a, fmt='%.5f,%.5f,%.5f,%.5f,%d,%d', delimiter=",", header=header, comments='')
    print("****standard deviation (per hdl)*****")
    print(np.std(a[:,[0,1,2,3]], axis = 0))
    std_max = max(np.std(a, axis = 0), key=abs)
    with open(sys.argv[3]+"_std.csv", 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["{:0.4f}".format(std_max), rate, frequency])
    return std_max


def collect_samples_files(fa1, fa2, fb1, fb2, card=0, first_sample='Q'):
    """
    """
    #This can take a little time depending on the system
    a1 = read_rx_samples(fa1, first_sample=first_sample)
    a2 = read_rx_samples(fa2, first_sample=first_sample)
    b1 = read_rx_samples(fb1, first_sample=first_sample)
    b2 = read_rx_samples(fb2, first_sample=first_sample)

    dat = {}
    dat['cd{}_hdl0'.format(card)] = {}
    dat['cd{}_hdl0'.format(card)]['i'] = np.real(a1)
    dat['cd{}_hdl0'.format(card)]['q'] = np.imag(a1)
    dat['cd{}_hdl1'.format(card)] = {}
    dat['cd{}_hdl1'.format(card)]['i'] = np.real(a2)
    dat['cd{}_hdl1'.format(card)]['q'] = np.imag(a2)
    dat['cd{}_hdl2'.format(card)] = {}
    dat['cd{}_hdl2'.format(card)]['i'] = np.real(b1)
    dat['cd{}_hdl2'.format(card)]['q'] = np.imag(b1)
    dat['cd{}_hdl3'.format(card)] = {}
    dat['cd{}_hdl3'.format(card)]['i'] = np.real(b2)
    dat['cd{}_hdl3'.format(card)]['q'] = np.imag(b2)
    return dat
    # return (a1, a2, b1, b2)

def read_rx_samples(f_in, meta=False, first_sample='Q'):
    """
    read output from rx_samples file and return numpy.array
    of complex numbers representing the i and q values.
    :Args:
        :fin (str): path to file
        :meta (bool): use meta data if True (not currently supported)
        :first_sample (str): determines order of I and Q
                             'Q' == QIQI...
                             'I' == IQIQ...
    """
    f_open = open(f_in, "rb")
    i_dat = []
    q_dat = []
    dat1 = []
    dat2 = []

    while True:
        sample = f_open.read(2)
        try:
            dat1.append(struct.unpack('<h', sample)[0])
        except struct.error as e:
            break

        sample = f_open.read(2)
        try:
            dat2.append(struct.unpack('<h', sample)[0])
        except struct.error as e:
            break

    if first_sample == 'Q':
        iq = np.array(dat2) + 1j*np.array(dat1)
    else:
        iq = np.array(dat1) + 1j*np.array(dat2)
    f_open.close()
    return iq

def collect_samples_files_with_prefix(prefix, card=0, first_sample='Q'):
    files = map(lambda f: prefix + f, ['.A1','.A2','.B1','.B2'])
    return collect_samples_files(*files,card=card,first_sample=first_sample)

def import_file_sets(prefix, seq=None):
    #print("Importing file sets...")
    if seq is None:
        return collect_samples_files_with_prefix('{}.0'.format(prefix))
    else:
        return list(map(lambda i: collect_samples_files_with_prefix('{}{}.0'.format(prefix,i)), seq))

def get_correlation_metrics(in_dat, ref_card=0, ref_hdl=0, eval_samples=1024*8):
    """
    :Args:
        :in_dat: dict returned from call to hwdev_receive_samples_aligned_multicard()
            :keys:
                :cd0_hdl0, cd0_hdl1, ..., cd1_hdl2, cd1_hdl3:
                    :keys:
                        :i:                 list of I values
                        :q:                 list of Q values
                        :ts_delta:          timestamp delta from reference
                        :ref_timestamp:     reference timestamp for all samples
    :Returns: dict of dicts
        :keys: channels in the form of cd<card>_hdl<handle>
                :keys:
                    :scale:         linear scale difference
                    :phi:           phase rotation in radians
                    :sample_shift:  number of samples shifted between channels
    """
    dat = {}
    if isinstance(in_dat, dict):
        for k in in_dat.keys():
            dat[k] = np.array(in_dat[k]['i']) + 1j*np.array(in_dat[k]['q'])
    elif isinstance(in_dat, tuple):
        dat['cd{}_hdl0'.format(ref_card)] = np.array(in_dat[0]) + 1j*np.array(in_dat[1])
        dat['cd{}_hdl1'.format(ref_card)] = np.array(in_dat[2]) + 1j*np.array(in_dat[3])
        dat['cd{}_hdl2'.format(ref_card)] = np.array(in_dat[4]) + 1j*np.array(in_dat[5])
        dat['cd{}_hdl3'.format(ref_card)] = np.array(in_dat[6]) + 1j*np.array(in_dat[7])

    ref_ch = 'cd{}_hdl{}'.format(ref_card, ref_hdl)
    if eval_samples == None:
        eval_samples = len(dat[dat.keys()[0]])
    x_avg = {}

    for ch in dat.keys():
        x_avg[ch] = np.mean(np.abs(np.fft.fft(dat[ch])))
    ch_min = min(x_avg)

    d = {}
    x_avg = {}
    for ch in dat.keys():
        x_avg[ch] = np.mean(np.abs(np.fft.fft(dat[ch][:eval_samples])))
    ch_min = min(x_avg, key=x_avg.get)

    ref = dat[ref_ch][:eval_samples]

    # order the dict keys in such a way that 'ref_ch' is first and all others follow.  this loop
    # references d[ref_ch] in every iteration, so it needs to be populated first
    keys = sorted(dat.keys(), key=lambda ch: ch != ref_ch)
    for ch in keys:
        d[ch] = {}
        scale = x_avg[ch_min]/x_avg[ch]

        # perform correlation relative to a common reference channel A1
        corr = np.correlate(dat[ref_ch][:eval_samples], dat[ch][:eval_samples], mode='full')
        max_index = np.argmax(np.abs(corr))

        lst = list(corr)
        lst.sort()
        penultimate = lst[-2]
        next_max = np.nonzero(corr==penultimate)[0][0]

        # corr_diff = np.abs(corr[max_index])/np.abs(corr[next_max])
        corr_diff = np.abs(corr[max_index])/np.abs(penultimate)
        phase_shift = np.angle(corr[max_index])

        # d[ch]['corr'] = corr
        d[ch]['scale'] = scale
        d[ch]['phi'] = phase_shift
        d[ch]['max_index'] = max_index
        # d[ch]['sample_shift'] = sample_shift
        d[ch]['sample_shift'] = d[ref_ch]['max_index'] - d[ch]['max_index']
    return d


'''*******main function*******'''
print("\n\n**************************************************************" +
        "******************")
print ("Analyzing " + sys.argv[2]+ " sets of captures w/ prefix: \"\n" + sys.argv[1] + "\"\n", file=sys.stdout)
data = import_file_sets(sys.argv[1], range(1,int(sys.argv[2])+1))

stdmax = summary(data)
if (stdmax > ACCEPTABLE_THRESHOLD):
    retcode = 1
    print("********TEST FAILED********")
else:
    retcode = 0

print("**************************************************************" +
        "******************\n")

sys.exit(retcode)





