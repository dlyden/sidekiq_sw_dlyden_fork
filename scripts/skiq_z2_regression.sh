#! /bin/sh

# Contains the directory where executables are located.
DIR=$(pwd)

# Host platform that this script is being run on.
PLATFORM="armv7l"
# Sidekiq hardware version, one of "M2", "MPCIE", "X2", "X4", "M2_2280".
SKIQ_HW="Z2"
# Sidekiq product version, either "001" or "002".
SKIQ_PROD=""

REPORT_BSP_VERS='cat /opt/VERSIONS'

# Boolean ("true"/"false") values used to determine Sidekiq features.
HAS_PCIE="false"
HAS_USB="false"
HAS_PPS="false"
HAS_DKIQ="false"
HAS_2CHAN="false"
HAS_2TXCHAN="false"
HAS_BLOCK_RX="false"
HAS_PACKED_RX="false"
HAS_ASYNC_TX="false"
HAS_LOW_LATENCY="false"
HAS_BALANCED="true"
HAS_RCS="false"
PERFORM_TRANSMIT="true"
PERFORM_EXTENDED="false"

run_test()
{
    local t rc

    if [[ "$DRY_RUN" == "true" ]]; then
        echo "+ $*"
    else
        echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
        echo "+ $*"
        t=$(type -f $1)
        if [ x"${t:0:1}" = x"/" ]; then
            time "$@"
            rc=$?
        else
            "$@"
            rc=$?
        fi
        if [ $rc -ne 0 ]; then
            echo "+ test failed with status=$rc"

            # attempt to print out the version for debugging purposes
            if [[ $CARD ]] && [[ $MODE ]] && [[ $DIR ]] ; then
                echo "+ grabbing version info of failed regression..."
                $DIR/version_test$APP_SUFFIX -c $CARD
            fi

            echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
            echo
            echo "-----------------------------------------------------------"
            echo "---------------- TESTS EXECUTION SUMMARY ------------------"
            for t in $TEST_PASS; do
                echo "  * [PASS] $t"
            done
            echo "  * [FAIL] $TEST_RUN"
            echo "-----------------------------------------------------------"
            exit $rc
        fi
        echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #"
        echo
    fi
}

#
# echos the minimum of two values
#
minimum()
{
    local A B
    A=$1
    B=$2
    echo $(( A < B ? A : B ))
}

#
# echos the maximum of two values
#
maximum()
{
    local A B
    A=$1
    B=$2
    echo $(( A > B ? A : B ))
}

#
# returns a sequence of NUM rates between START and STOP
#
rate_list()
{
    local START STOP NUM STEP
    START=$1
    STOP=$2
    NUM=$3
    STEP=$(( ( STOP - START ) / NUM ))

    if [[ $STEP -le 0 ]]; then
        echo $STOP
    else
        seq $START $STEP $STOP
    fi
}

prog_fpga()
{
    if [[ -n "$FPGA" ]] ; then
         # Test programming FPGA into RAM
        run_test $DIR/prog_fpga$APP_SUFFIX -c $CARD -s $FPGA
    elif [[ -z $FPGA ]] ; then
        echo ""
        echo "INFO: skipping prog_fpga, no FPGA bitstream specified"
        echo ""
    fi
}

#
# given a number of bytes and an output file path, verify if the output file size is correct
#
_verify_file_size()
{
    local BYTES OUTPUT_FN EXPECTED PRESENTLY
    BYTES=$1
    OUTPUT_FN=$2
    EXPECTED=$BYTES
    PRESENTLY=$(ls -la "$OUTPUT_FN" | awk '{ print $5 }')

    if [[ $PRESENTLY -eq $EXPECTED ]]; then
        echo "output size verified, expected $EXPECTED bytes in $OUTPUT_FN, found $PRESENTLY bytes" >&2
        return 0
    else
        echo "output size NOT verified, expected $EXPECTED bytes in $OUTPUT_FN, found $PRESENTLY bytes" >&2
        return 1
    fi
}

#
# given a number of samples and an output file path, verify if the output file size is correct for
# unpacked mode
#
verify_output_file_size()
{
    local WORDS OUTPUT_FN
    WORDS=$1
    OUTPUT_FN=$2

    _verify_file_size "$((WORDS * 4))" "$OUTPUT_FN"
}

#
# returns a sequence of NUM rates between START and STOP
#
rate_list()
{
    local START STOP NUM STEP
    START=$1
    STOP=$2
    NUM=$3
    STEP=$(( ( STOP - START ) / NUM ))

    if [[ $STEP -le 0 ]]; then
        echo $STOP
    else
        seq $START $STEP $STOP
    fi
}

rfic_rw_test()
{
    run_test $DIR_PRIVATE/rfic_rw_test$APP_SUFFIX $CARD 250000
}

i2c_rw_test()
{
    run_test $DIR_PRIVATE/i2c_rw_test$APP_SUFFIX $CARD 1000
}

fpga_rw_test()
{
    run_test $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000
    run_test $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000 --64bit=aligned

    # Earlier BSPs don't have an FPGA that supports the register used in unaligned testing
    CMD="grep ^z2-sw-version /opt/VERSIONS"
    SW_VER=$($CMD | /usr/bin/awk '{print $2}')
    case $SW_VER in
        v3.*)
            run_test $DIR_PRIVATE/fpga_rw_test$APP_SUFFIX -c $CARD -n 1000000 --64bit=unaligned
            ;;
    esac
}

user_reg_verify()
{
    run_test $DIR_PRIVATE/user_reg_verify$APP_SUFFIX $CARD
}

timestamp_reset()
{
    if [[ $HAS_PPS == "true" ]] && [[ $HAS_PCIE == "true" ]] ; then
        run_test $DIR/timestamp_reset$APP_SUFFIX $CARD 1 0
    elif [[ $HAS_USB == "false" ]] ; then
        # timestamp reset for USB needs to be investigated, likely just needs
        # a more generous timestamp check
        run_test $DIR/timestamp_reset$APP_SUFFIX $CARD 0 0
    fi
}

rx_benchmark()
{
    local ARGS
    ARGS="-c $CARD --handle=A1 --time=5 --threshold=200 -r 31000000 --target=115"

    run_test $DIR/rx_benchmark$APP_SUFFIX $ARGS

    if [[ $HAS_BALANCED == "true" ]]; then
        ARGS="-c $CARD --handle=A1 --time=5 --threshold=200 -r 10000000 --target=37 --balanced"
        run_test $DIR/rx_benchmark$APP_SUFFIX $ARGS
    fi
}

tx_benchmark()
{
    ######################################################################
    # Synchronous Transmit
    ######################################################################
    local ARGS
    ARGS="-c $CARD --time=5 --threshold=200 --block-size=65532 -r 40000000 --target=150 --threads=1"

    run_test $DIR/tx_benchmark$APP_SUFFIX $ARGS
}

xcv_benchmark()
{
    ######################################################################
    # Synchronous Transceive
    ######################################################################
    local ARGS
    ARGS="-c $CARD --time=5 --threshold=200  --block-size=16380 -r 20000000 --target=74 --threads=1"

    run_test $DIR/xcv_benchmark$APP_SUFFIX $ARGS
}

_rx_samples()
{
    local RATES ARGS
    ARGS="-c $CARD -d $OUTDIR/out --counter $*"

    # test 8 rates between 1e6 and max sample rate
    RATES=$(rate_list 1000000 $MAX_SAFE_RECEIVE_SAMPLE_RATE 8)

    for RATE in $RATES ; do
        # test rx for A1, first unpacked, then packed
        run_test $DIR/rx_samples$APP_SUFFIX $ARGS -w $RATE -r $RATE --handle=A1
        run_test verify_output_file_size $RATE $OUTDIR/out.a1
    done
}

rx_samples()
{
    local SAVE_MAX_SAMPLE_RATE
    SAVE_MAX_SAMPLE_RATE=$MAX_SAFE_RECEIVE_SAMPLE_RATE

    # specify receive gain based on hardware type
    if [[ "$SKIQ_HW" == "X2" ]]; then
        GAIN="-g 237"
    else
        GAIN="-g 40"
    fi

    # call _rx-samples test with an additional option if blocking RX is available
    _rx_samples $GAIN
    _rx_samples $GAIN --blocking

    if [[ $HAS_BALANCED == "true" ]]; then
        MAX_SAFE_RECEIVE_SAMPLE_RATE=5000000
        _rx_samples $GAIN --balanced
        MAX_SAFE_RECEIVE_SAMPLE_RATE=$SAVE_MAX_SAMPLE_RATE
    fi
}

_rx_sweep_testbench()
{
    local ARGS STOP REPEAT

    REPEAT=$1
    ARGS="--rx --step 1e6 --repeat $REPEAT"

    # for X2 and X4, run at a sample rate of 61.44Msps in order to tune down to 70MHz
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        ARGS="$ARGS -r 61.44e6"
    fi

    run_test $DIR/sweep_testbench$APP_SUFFIX $ARGS -f $RESULTS/tune.txt

    if [[ "$HAS_DKIQ" == "false" ]]; then
        # split up the frequency range since frequency hopping can only be performed on a list of up
        # to 512 entries
        for start in $(seq 70 512 6000); do
            STOP=$(minimum $((start+511)) 6000)
            run_test $DIR/sweep_testbench$APP_SUFFIX $ARGS --freq-hop \
                     --start ${start}e6 \
                     --stop ${STOP}e6 \
                     -f $RESULTS/tune_freq_hop_${start}MHz_to_${STOP}MHz.txt
        done
    fi
}

rx_sweep_testbench()
{
    _rx_sweep_testbench 0
}

rx_sweep_testbench_extended()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        _rx_sweep_testbench 2
    else
        _rx_sweep_testbench 9
    fi
}

_tx_sweep_testbench()
{
    local ARGS STOP REPEAT

    REPEAT=$1
    ARGS="--tx --step 1e6 --repeat $REPEAT"

    # for X2 and X4, run at a sample rate of 61.44Msps in order to tune down to 70MHz
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        ARGS="$ARGS -r 61.44e6"
    fi

    run_test $DIR/sweep_testbench$APP_SUFFIX $ARGS -f $RESULTS/tune.txt

    if [[ "$HAS_DKIQ" == "false" ]]; then
        # split up the frequency range since frequency hopping can only be performed on a list of up
        # to 512 entries
        for start in $(seq 70 512 6000); do
            STOP=$(minimum $((start+511)) 6000)
            run_test $DIR/sweep_testbench$APP_SUFFIX $ARGS --freq-hop \
                     --start ${start}e6 \
                     --stop ${STOP}e6 \
                     -f $RESULTS/tune_freq_hop_${start}MHz_to_${STOP}MHz.txt
        done
    fi
}

tx_sweep_testbench()
{
    _tx_sweep_testbench 0
}

tx_sweep_testbench_extended()
{
    if [[ "$SKIQ_HW" == "X2" || "$SKIQ_HW" == "X4" ]]; then
        _tx_sweep_testbench 2
    elif [[ "$HAS_USB" == "true" && "$HAS_PCIE" == "false" ]]; then
        # if the DUT has USB without PCIe, then it must be a USB bitstream, so don't sweep as many
        # times
        _tx_sweep_testbench 4
    else
        _tx_sweep_testbench 9
    fi
}

prbs_test()
{
    if [[ "$SKIQ_HW" == "X2" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for PRBS test" >&2
    else
        local ARGS
        ARGS="-c $CARD --time=3 -d $OUTDIR/prbs_results.txt --start=1000000 --step=5000000"

        run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000
        if [[ $HAS_2CHAN == "true" ]] ; then
            if [[ "$SKIQ_HW" == "M2" ]]; then
                run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=61440000 --channels=2
            else
                run_test $DIR/prbs_test$APP_SUFFIX $ARGS --stop=30720000 --channels=2
            fi
        fi
    fi
}

test_fir_api()
{
    if [[ "$SKIQ_HW" == "X2" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for custom FIR coefficients" >&2
    else
        local RATES
        RATES="233000 13300000 23000000 40000000 46000000"

        # Note: this doesn't test thruput, so sample rate range doesn't matter (neither does BW)
        for RATE in $RATES; do
            run_test $DIR/test_fir_api$APP_SUFFIX -c $CARD -r $RATE
        done

        if [[ $HAS_2TXCHAN == "true" ]] ; then
            for RATE in $RATES; do
                run_test $DIR/test_fir_api$APP_SUFFIX -c $CARD -r $RATE -h A2
            done
        fi
    fi
}

timestamp_test()
{
    if [[ "$SKIQ_HW" == "X2" ]]; then
        echo "===== SKIPPING ${FUNCNAME[0]} - no support for timestamp test (yet)" >&2
    else
        local START STEP STOP ARGS
        START=1000000
        STEP=$(( ( MAX_SAMPLE_RATE - START ) / 25 ))
        STOP=$(( MAX_SAMPLE_RATE + 1 ))
        ARGS="--start=$START --blocks=20000 -c $CARD --verbose --exit-on-fail"

        run_test $DIR/timestamp_test$APP_SUFFIX $ARGS --stop=$STOP --step=$STEP --handle=A1
        if [[ $HAS_2CHAN == "true" ]] ; then
            STEP=$(( ( ( MAX_SAMPLE_RATE / 2 ) - START ) / 25 ))
            STOP=$(( ( MAX_SAMPLE_RATE / 2 ) + 1 ))
            run_test $DIR/timestamp_test$APP_SUFFIX $ARGS --stop=$STOP --step=$STEP --handle=A2
        fi
    fi
}

timestamp_read_test()
{
    local ARGS
    ARGS="-c $CARD --verbose -n 1000000"

    run_test $DIR/timestamp_read_test$APP_SUFFIX $ARGS
    run_test $DIR/timestamp_read_test$APP_SUFFIX $ARGS --rfic-config lte_10MHz_rx1_rx2_agc_on.txt
}

sweep_receive()
{
    local ARGS REPEAT START STOP STEP RATE
    REPEAT=1000

    if [[ $HAS_DKIQ == "true" ]] ; then
        START=20000000
        STOP=900000000
    else
        START=800000000
        STOP=900000000
    fi
    STEP=$(( (STOP-START) / 10 ))

    if [[ "$SKIQ_HW" == "X2" ]]; then
        RATE=153600000
    else
        RATE=$MAX_SAFE_RECEIVE_SAMPLE_RATE
    fi
    ARGS="-c $CARD -r $RATE --start $START --stop $STOP --step $STEP  --repeat $REPEAT"

    run_test $DIR/sweep_receive$APP_SUFFIX $ARGS
    run_test $DIR/sweep_receive$APP_SUFFIX $ARGS --blocking
}

sweep_receive_extended()
{
    local ARGS RATE REPEAT START STOP STEP

    if [[ $HAS_DKIQ == "true" ]] ; then
        START=20000000
        STOP=6000000000
    else
        START=70000000
        STOP=6000000000
    fi

    case "$SKIQ_HW" in
        X2)
            RATE=153600000
            START=$(maximum $START $((RATE / 2)))
            STEP=1024000
            REPEAT=99
            ;;
        X4)
            RATE=245760000
            START=$(maximum $START $((RATE / 2)))
            STEP=$((5 * 1024000))
            REPEAT=99
            ;;
        *)
            RATE=$MAX_SAFE_RECEIVE_SAMPLE_RATE
            STEP=1024000
            if [[ $HAS_PCIE == "true" ]] ; then
                REPEAT=99
            else
                REPEAT=1
            fi
            ARGS="$ARGS --blocks 10"
            ;;
    esac
    ARGS="$ARGS -c $CARD -r $RATE --start $START --stop $STOP --step $STEP"
    if (( $REPEAT > 0 )); then
        ARGS="$ARGS --repeat $REPEAT"
    fi

    run_test $DIR/sweep_receive$APP_SUFFIX $ARGS
    if [[ $HAS_BLOCK_RX == "true" ]] ; then
        run_test $DIR/sweep_receive$APP_SUFFIX $ARGS --blocking
    fi
}

tx_configure_steps()
{
    local ARGS
    ARGS="-c $CARD --start-freq 70000000 --end-freq 6000000000 --freq-step 300000000 -a 359"
    ARGS="$ARGS --start-rate 1000000 --end-rate 2000000 --rate-step 1000000 -t 1"

    run_test $DIR/tx_configure_steps$APP_SUFFIX $ARGS
}

tx_timestamp_tests()
{
    local ARGS
    ARGS="-c $CARD --run-empty-fifo-test -r 5000000 -s $DATA_DIR/tx_waveform_20MS_le.dat"

    run_test $DIR/tx_timestamp_tests$APP_SUFFIX $ARGS
}

test_ref_clock_switch()
{
    local ARGS
    ARGS="-c $CARD"

    run_test $DIR/test_ref_clock_switch$APP_SUFFIX $ARGS
}

read_accel()
{
    run_test $DIR/read_accel$APP_SUFFIX -c $CARD --repeat 10
}

read_temp()
{
    run_test $DIR/read_temp$APP_SUFFIX -c $CARD --repeat 10
    run_test $DIR/read_temp_factory$APP_SUFFIX -c $CARD --repeat 10
}

init_and_exit()
{
    run_test $DIR/init_and_exit$APP_SUFFIX -c $CARD --sleep-duration=1 --repeat 4
    run_test $DIR/init_and_exit$APP_SUFFIX -c $CARD --sleep-duration=1 --repeat 4 --full
}

version_test()
{
    echo "=== BSP VERSION INFORMATION ==="
    run_test $REPORT_BSP_VERS
    echo "==============================="

    run_test $DIR/version_test$APP_SUFFIX -c $CARD
    run_test $DIR/version_test$APP_SUFFIX -c $CARD --full
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Functions
#
#   The functions listed below are utility functions used within the script.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


detect_sidekiq()
{
    local SKP SKIQ_HW_REV
    SKP=$DIR_PRIVATE/sidekiq_probe$APP_SUFFIX

    if [[ ! -e "$SKP" ]] ; then
        echo "ERROR: $SKP not found!"
        exit 1
    elif [[ ! -x "$SKP" ]] ; then
        echo "ERROR: $SKP not executable!"
        exit 1
    fi

    SKIQ_HW_REV=$($SKP -c $CARD --fmtstring=%r | awk '{print toupper($0)}')
    rc=$?
    if [ $rc -ne 0 ]; then
        case $SKIQ_HW_REV in
            B*)
                HAS_ACCEL="false"
                ;;
            *)
                HAS_ACCEL="true"
                ;;
        esac
    fi

    # set global test parameters
    HAS_2CHAN="false"
    HAS_BLOCK_RX="true"
    HAS_LOW_LATENCY="true"
    HAS_BALANCED="true"
}

set_test_parameters()
{
    # set global test parameters
    MAX_SAMPLE_RATE=22000000
    # Use 75% of the max sample rate as the max safe receive sample rate to improve stability
    # of regression testing
    MAX_SAFE_RECEIVE_SAMPLE_RATE=$(((MAX_SAMPLE_RATE * 75) / 100)) 
    HAS_PPS="false"
}

set_fpga_bitstream()
{
    if [[ -n "$FPGA" ]] ; then
        FPGA=$(readlink -f $FPGA)
    elif [[ -n "$FPGA_Z2" ]] ; then
        FPGA=$(readlink -f $FPGA_Z2)
    fi
}

trap "echo Exited!; exit;" SIGINT SIGTERM

if [ $# -gt 0 ]; then
    if [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
        print_help
        exit 0
    fi
fi

# before any detection, we need to know the card number and directory to find
# our test applications

# default to a value of 0
: ${CARD:=0}

# default to running tests, not just displaying what would run
: ${DRY_RUN:=false}

if [[ -n "$APP_DIR" ]] ; then
    DIR=$(readlink -f "$APP_DIR")
fi

if [[ -n "$APP_PRIVATE_DIR" ]] ; then
    DIR_PRIVATE=$(readlink -f "$APP_PRIVATE_DIR")
fi

if [[ -n "$APP_DATA_DIR" ]] ; then
    DATA_DIR=$(readlink -f "$APP_DATA_DIR")
fi

# define where output files go during testing
OUTDIR=$DIR/tmp_output
RESULTS=$DIR/results
mkdir -p "$OUTDIR" "$RESULTS"

# check if transmit related testing is to be performed
case "$TRANSMIT" in
    y|yes|1)
        PERFORM_TRANSMIT=true
        ;;
    *)
        PERFORM_TRANSMIT=false
        ;;
esac

# check if RCS related testing is to be performed (or some tests skipped)
case "$RCS" in
    y|yes|1)
        HAS_RCS=true

        # override transmit testing, RCS bitstreams do not have transmit capability
        PERFORM_TRANSMIT=false
        ;;
    *)
        HAS_RCS=false
        ;;
esac

# check if extended testing is to be performed
case "$EXTENDED" in
    y|yes|1)
        PERFORM_EXTENDED=true
        ;;
    *)
        PERFORM_EXTENDED=false
        ;;
esac

detect_sidekiq
set_test_parameters
set_fpga_bitstream

echo "-----------------------------------------------------------"
echo "Test Info"
echo "      Sidekiq $SKIQ_HW $SKIQ_PROD $PLATFORM"
echo "      Dropkiq        [$HAS_DKIQ]"
echo "      PCIe           [$HAS_PCIE]"
echo "      USB            [$HAS_USB]"
echo "      Card           [$CARD]"
if [[ -n "$FPGA" ]] ; then
echo "      FPGA           [$(basename $FPGA)]"
else
echo "      FPGA           [false]"
fi
echo "      FW             [false]"
echo "      PPS            [$HAS_PPS]"
echo "      Packed Rx      [$HAS_PACKED_RX]"
echo "      Blocking Rx    [$HAS_BLOCK_RX]"
echo "      Low Latency Rx [$HAS_LOW_LATENCY]"
echo "      Async Tx       [$HAS_ASYNC_TX]"
echo "      2 Rx Chan      [$HAS_2CHAN]"
echo "      2 Tx Chan      [$HAS_2TXCHAN]"
echo "-----------------------------------------------------------"
echo ""

################################################################################
# Tests to run
################################################################################
unset TEST

TEST="$TEST prog_fpga"
TEST="$TEST rfic_rw_test"
TEST="$TEST i2c_rw_test"
TEST="$TEST fpga_rw_test"

if [[ "$HAS_RCS" == "false" ]]; then
    echo "..........................................................."
    echo "Not an RCS bitstream, allowing some additional tests..."
    echo
    TEST="$TEST user_reg_verify"
fi


if [[ "$HAS_ACCEL" == "true" ]]; then
    echo "..........................................................."
    echo "Hardware revision supports accelerometer..."
    echo
    TEST="$TEST read_accel"
fi

TEST="$TEST timestamp_reset"
TEST="$TEST rx_benchmark"
TEST="$TEST rx_sweep_testbench"

if [[ "$PERFORM_TRANSMIT" == "true" ]]; then
    echo "..........................................................."
    echo "Adding transmit related tests..."
    echo
    TEST="$TEST test_fir_api"
    TEST="$TEST tx_benchmark"
    TEST="$TEST xcv_benchmark"
    TEST="$TEST tx_sweep_testbench"
    TEST="$TEST tx_configure_steps"
    TEST="$TEST tx_timestamp_tests"
fi

if [[ "$PERFORM_EXTENDED" == "true" ]]; then
    echo "..........................................................."
    echo "Adding extended tests..."
    echo
    TEST="$TEST rx_sweep_testbench_extended"
    if [[ "$PERFORM_TRANSMIT" == "true" ]]; then
        TEST="$TEST tx_sweep_testbench_extended"
    fi
    TEST="$TEST sweep_receive_extended"
fi

TEST="$TEST rx_samples"
TEST="$TEST prbs_test"
TEST="$TEST timestamp_test"
TEST="$TEST timestamp_read_test"
TEST="$TEST sweep_receive"
TEST="$TEST test_ref_clock_switch"
TEST="$TEST read_temp"
TEST="$TEST init_and_exit"
TEST="$TEST version_test"

unset TEST_PASS TEST_RUN
if [[ -z $1 ]]; then
    for t in $TEST; do
        TEST_RUN="$t"
        $t
        TEST_PASS="$TEST_PASS $t"
    done
    echo "-----------------------------------------------------------"
    echo "---------------- TESTS EXECUTION SUMMARY ------------------"
    for t in $TEST; do
        echo "  * [PASS] $t"
    done
    echo "-----------------------------------------------------------"
else
    for var in "$@" ; do
        TEST_RUN="${var}"
        ${var}
        TEST_PASS="$TEST_PASS ${var}"
    done
    echo "-----------------------------------------------------------"
    echo "---------------- TESTS EXECUTION SUMMARY ------------------"
    for var in "$@" ; do
        echo "  * [PASS] $var"
    done
    echo "-----------------------------------------------------------"
fi
