#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# configure directories
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
SCRIPT_DIR=`dirname $(readlink -f $0)`
ROOT_DIR=`dirname $(readlink -f $SCRIPT_DIR/)`
FPGA_DIR=$SCRIPT_DIR/../sidekiq_fpga_bitstreams

# Add the DMA DLL into the PATH to get picked up for regression testing
PATH=./libraries/nw_logic_dma/dma_driver/DMADriver_2015/DMADriver/TestCli/DmaDriverDLL/x64/Release/:$PATH

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# export environment variables
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
OS_TYPE=`uname -o`
if [[ "$OS_TYPE" == "Msys" ]]; then
export APP_DIR=$ROOT_DIR/host_code/libsidekiq/test_apps/bin
export APP_PRIVATE_DIR=$ROOT_DIR/host_code/libsidekiq/test_apps/bin
export APP_DATA_DIR=$ROOT_DIR/scripts/test_data
export APP_SUFFIX="__win64.exe"
else
export APP_DIR=$ROOT_DIR/host_code/libsidekiq/test_apps/bin/x86_64.gcc
export APP_PRIVATE_DIR=$ROOT_DIR/host_code/libsidekiq/test_apps/bin/x86_64.gcc
export APP_DATA_DIR=$ROOT_DIR/scripts/test_data
export APP_SUFFIX=""
fi

export FPGA_MPCIE_PCIE=$FPGA_DIR/sidekiq_image_mpcie_xport_pcie_*.bin
export FPGA_MPCIE_USB=$FPGA_DIR/sidekiq_image_mpcie_xport_usb_*.bin
export FPGA_M2_PCIE=$FPGA_DIR/sidekiq_image_m2_xport_pcie_*.bit
export FPGA_M2_2280_PCIE=$FPGA_DIR/sidekiq_image_m2_2280_xport_pcie_*.bi?
export FPGA_M2_USB=$FPGA_DIR/sidekiq_image_m2_xport_usb_*.bit
export FPGA_X2_PCIE=$FPGA_DIR/sidekiq_image_x2_xport_pcie_*.bin
export FPGA_X4_PCIE=$FPGA_DIR/sidekiq_image_x4_xport_pcie_*.bin
export FW_MPCIE=$ROOT_DIR/libraries/sidekiq_fw/sidekiq_fw_mpcie_*.hex
export FW_M2=$ROOT_DIR/libraries/sidekiq_fw/sidekiq_fw_m2_*.hex

export CARD=${CARD:-0}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# run the regression test
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
if [ "$OS_TYPE" == "Msys" ]; then
. $SCRIPT_DIR/skiq_regression.sh "$@" &
else
setsid $SCRIPT_DIR/skiq_regression.sh "$@" &
fi

PID=$!
trap "pkill -TERM -P $PID" SIGINT SIGTERM
wait $PID
