#!/bin/bash

# This script is designed to be used by FPGA developers. Typically, they are
# given a set of test apps from the latest release and then run an FPGA they
# built against it. We'll stick to Linux only now as I don't think there is a
# need for Windows support for FPGA develops running regression.

if [ 1 -ne $# ] ; then
    echo "Usage: ${0##*/} [FPGA]"
    exit 1
fi

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# export environment variables
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TOP=`dirname $(readlink -f $0)`
export APP_DIR=$TOP
export APP_PRIVATE_DIR=$TOP
export APP_SUFFIX=""

export FW_MPCIE=$TOP/sidekiq_fw_mpcie_*.hex
export FW_M2=$TOP/sidekiq_fw_m2_*.hex

export CARD=0
export FPGA=$1

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# run the regression test
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
setsid $TOP/skiq_regression.sh &
PID=$!
trap "pkill -TERM -P $PID" SIGINT SIGTERM
wait $PID

