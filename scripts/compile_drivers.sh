#! /bin/bash

function compile-driver()
{
    local DIR=$1
    local KVER=$2
    local FN=$3
    local RC=""

    if [ "$PLATFORM" ] ; then
        echo "Please use BUILD_CONFIG, not PLATFORM."
        exit 1
    elif [ -z "$BUILD_CONFIG" ] ; then
        # default to x86_64
        BUILD_CONFIG="x86_64.gcc"
    fi

    cd $DIR
    make clean

    case $BUILD_CONFIG in
        x86_64.gcc)
            unset KDIR
            if [ -e /usr/src/kernels/$KVER ]; then
                KDIR=KDIR=/usr/src/kernels/$KVER
            elif [ -e /usr/src/linux-headers-$KVER ]; then
                KDIR=KDIR=/usr/src/linux-headers-$KVER
            fi
            make KVER=$KVER $KDIR -j

            RC=$?
            if [[ $RC -ne 0 ]] ; then
                exit $RC
            fi

            strip --strip-unneeded $FN
            ;;

        arm_cortex-a9.gcc4.8_uclibc_openwrt|arm_cortex-a9.gcc5.2_glibc_openwrt)
            local TOOL="/opt/toolchains/$BUILD_CONFIG"

            STAGING_DIR=$TOOL \
            KDIR=/opt/linux-headers-openwrt-$KVER \
            KVER=$KVER \
            make \
            ARCH=arm \
            CROSS_COMPILE=$TOOL/bin/arm-openwrt-linux- -j

            RC=$?
            if [[ $RC -ne 0 ]] ; then
                exit $RC
            fi

            $TOOL/bin/arm-openwrt-linux-strip --strip-unneeded $FN
            ;;

        aarch64.gcc6.3)
            local TOOL="/opt/toolchains/gcc-linaro-6.3.1-2017.05-x86_64_aarch64-linux-gnu"

            case "$MODEL" in
                jetson-tx1|jetson-tx2|jetson-xavier)
                    STAGING_DIR=$TOOL \
                        KDIR=/opt/$MODEL/$L4TREL/linux-headers-$KVER \
                        KVER=$KVER \
                        make \
                        ARCH=arm64 \
                        CROSS_COMPILE=$TOOL/bin/aarch64-linux-gnu- -j

                    RC=$?
                    if [[ $RC -ne 0 ]] ; then
                        exit $RC
                    fi

                    $TOOL/bin/aarch64-linux-gnu-strip --strip-unneeded $FN
                    ;;
                *)
                    echo "unknown model, cannot continue" >&2
                    exit 1
                    ;;
            esac

            ;;

        *)
            echo "Unsupported BUILD_CONFIG ($BUILD_CONFIG)!" >&2
            exit 1
            ;;
    esac
}

if [ $# -lt 1 ]; then
    echo "usage: $(basename $(readlink -f $0)) <version> [ <version> ... ]" >&2
    exit 1
fi

D=$(dirname $(readlink -f $0))

PCI=$(readlink -f $D/../libraries/pci_manager/driver)
DMA=$(readlink -f $D/../libraries/nw_logic_dma/dma_driver/DMADriver/Linux/src)

for VER; do
    for fn in $PCI/$VER/pci_manager.ko $DMA/$VER/dmadriver.ko; do
    if [ ! -e $fn -o ! -z "$FORCE_BUILD" ]; then
        D=$(dirname $(dirname $fn))
        (compile-driver $D $VER $fn)
    else
        echo "found compiled driver $fn, skipping build" >&2
    fi
    done
done
