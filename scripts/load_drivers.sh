#! /bin/bash

# The number of modules the need to exist / load in order to consider the script successful
NEEDED_SUCCESSES=3


loadModule() {
    # The name of the module (including path if using 'insmod')
    local module
    # The method used to load the module (either "insmod" or "modprobe"; if not specified,
    # attempt to guess and/or fall back)
    local method
    local res

    module="$1"
    method="$2"
    res=0

    if [ "$method" == "" ]; then
        if [ -e "$module" ] ; then
            method="insmod"
        else
            method="modprobe"
        fi
    fi

    case "$method" in
        insmod | INSMOD )
            echo "loading module $module (via $method)" >&2
            insmod "$module" 2>&1
            res=$?
        ;;

        modprobe | MODPROBE | *)
            echo "loading module $module (via $method)" >&2
            modprobe "$module" 2>&1
            res=$?
        ;;
    esac

    return $res
}

# Determine if a module is present on the system (using 'modinfo')
isModulePresent() {
    # The name of the module
    local module
    local modinfo

    module="$(basename "$1" ".ko")"

    modinfo=$(command -v modinfo)
    if [ -z "$modinfo" ]; then
        echo "Error: failed to locate 'modinfo' utility"
        return 1
    fi

    $modinfo "$module" >/dev/null 2>&1 && res=0 || res=1

    return $res
}

wasASuccess() {
    SUCCESSES=$((SUCCESSES + 1))
}

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
    echo "Warning: This script must be run as root, re-running with 'sudo'" 1>&2
    sudo "$0"
    exit $?
fi

D=$(dirname $(readlink -f $0))

PCI=$(readlink -f $D/../libraries/pci_manager/driver)
DMA=$(readlink -f $D/../libraries/nw_logic_dma/dma_driver/DMADriver/Linux/src)

pci_prefix=""
dma_prefix=""
method=""
VER=$(uname -r)
if [ -d "$PCI/$VER" ] && [ -d "$DMA/$VER" ]; then
    pci_prefix="$PCI/$VER/" 
    dma_prefix="$DMA/$VER/"
    method="insmod"
else
    # Attempt to load the modules regardless if the version exists in the local
    # directories; this might be useful if the user compiled their own drivers
    # or DKMS is in use

    echo "Cannot find pre-made modules for '$VER'; attempting to load system-wide versions"

    SUCCESSES=0
    isModulePresent "pci_manager" && wasASuccess
    isModulePresent "skiq_platform_device" && wasASuccess
    isModulePresent "dmadriver" && wasASuccess

    if [ "$SUCCESSES" == "$NEEDED_SUCCESSES" ]; then
        method="modprobe"
    fi
fi

if [ "$method" == "" ]; then
    echo "Cannot find drivers for kernel version '$VER'"
    # 2 = ENOENT ("No such file or directory")
    exit 2
fi

echo "removing existing pci_manager, dmadriver, skiq_platform_device" >&2
rmmod pci_manager
rmmod dmadriver
rmmod skiq_platform_device

SUCCESSES=0
loadModule "${pci_prefix}pci_manager.ko" "$method" && wasASuccess
loadModule "${dma_prefix}skiq_platform_device.ko" "$method" && wasASuccess
loadModule "${dma_prefix}dmadriver.ko" "$method" && wasASuccess

if [ "$SUCCESSES" -ne "$NEEDED_SUCCESSES" ]; then
    echo "Failed to load all modules!"
fi
