#!/usr/bin/env python3

# example of running the script
#
# ./plot_spectrum.py --samp-rate=19660800 /mnt/storage/projects/skylight/captures/evdo_detect_seq_and_vectors_2017-02-01/captures/cell_pwr_-50_f=888900000_r=19660800_n=9830400_g=76.a1

import sys
import numpy as np
from os.path import basename

# Default the number of sample read from a file to be 10,000.
DEFAULT_IQ_LEN=10000

def read_iq_from_fp(fp, skip=0, count=DEFAULT_IQ_LEN, swap_iq=False):

    # count is number of dtype size while offset is number of bytes
    tmp = np.fromfile(fp, dtype=np.uint32, count=count, offset=4*skip)
    tmp.dtype = np.int16

    if swap_iq:
        # Everyone else's I/Q ordering
        return tmp[0::2] + 1j*tmp[1::2]
    else:
        # Traditional Sidekiq I/Q ordering, i.e. Q-then-I
        return tmp[1::2] + 1j*tmp[0::2]


def compute_mag_spectrum(x, samp_rate_hz, fft_len):
    # bin frequencies
    t_samp = 1./samp_rate_hz
    bin_freq_hz = np.fft.fftfreq(fft_len, t_samp)

    # get mag spectrum of windowed zero-padded input
    window = np.hamming(len(x))
    x_zp = np.zeros((fft_len,), dtype=complex)
    x_zp[:len(x)] = x * window
    mag_spectrum = np.abs(np.fft.fft(x_zp, norm='ortho'))**2

    # figure out scale factor (each factor is squared since applied to mag spectrum)
    # so that 0 dBFS sinusoid will have mag spectrum peak at 0 dB
    xlen_factor = len(x)
    window_factor = 1./(window.sum()**2)
    zp_factor = fft_len/len(x)
    scale = xlen_factor * window_factor * zp_factor

    return (np.fft.fftshift(bin_freq_hz), np.fft.fftshift(scale*mag_spectrum))

def process_fp(fp, args):
    lo_freq = int(args.lo_freq)
    samp_rate = int(args.samp_rate)
    n_bits_iq = args.n_bits_iq
    iq_len = args.iq_len
    fft_len = args.fft_len

    # read, scale, truncate IQ sequence
    x = read_iq_from_fp(fp, skip=args.skip, count=iq_len, swap_iq=args.swap_iq)
    full_scale_mag = 2.**(n_bits_iq-1)
    x /= full_scale_mag

    # calc spectrum
    bin_freq, bin_mag = compute_mag_spectrum(x, samp_rate, fft_len)
    bin_mag_db = 10*np.log10(bin_mag)


    # find top peak and plot
    idx = np.argmax(bin_mag_db)
    peak_db = bin_mag_db[idx]
    peak_freq_hz = bin_freq[idx]
    peak_abs_freq_hz = lo_freq + peak_freq_hz
    if lo_freq != 0:
        offset =  ' ({:.3f} kHz offset)'.format(peak_freq_hz/1e3)
    else:
        offset = ''
    title = '{}\npeak: {:.1f} dBFS at {:.3f} MHz'.format(basename(fp.name),
                                                         peak_db,
                                                         peak_abs_freq_hz/1e6) + offset

    return bin_mag_db, bin_freq, title

def main(argv=None):
    import argparse

    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description='quick spectrum app')
    parser.add_argument('--lo-freq', type=float, default=0, help='LO freq of capture in Hz')
    parser.add_argument('--samp-rate', type=float, required=True, help='sample rate in Hz')
    parser.add_argument('--n-bits-iq', type=int, default=12, help='number of bits per I or Q sample (12 or 16)')
    parser.add_argument('--iq-len', type=int, default=DEFAULT_IQ_LEN, help='number of I/Q samples from file are used')
    parser.add_argument('--skip', type=int, default=0, help='number of samples to skip from the start of the file')
    parser.add_argument('--fft-len', type=int, default=2**17, help='FFT length; usually power of 2 and larger than iq-len')
    parser.add_argument('--no-plot', action='store_true', help='Only print peak information, no plotting spectrum')
    parser.add_argument('--save-plot', action='store_true', help='Save entire plot to a PNG')
    parser.add_argument('--plot-name', type=str, help='Filename for the saved plot')
    parser.add_argument('--swap-iq', action='store_true', help='Swap I/Q ordering during file read (default is Sidekiq ordering; Q-then-I)')
    parser.add_argument('capture', nargs='+', type=argparse.FileType('rb'), help='filename(s) of capture(s) in rx_samples format')

    args = parser.parse_args(argv[1:])

    lo_freq = int(args.lo_freq)
    plot_name = args.plot_name

    # If a plot_name isn't defined, use 'image.png' if more than one capture file is specified
    # otherwise use the basename of the capture file
    if not plot_name:
        if len(args.capture) > 1:
            plot_name = 'image.png'
        else:
            plot_name = basename(args.capture[0].name) + '.png'

    # Process all of the capture files into the data[] list
    data = [ process_fp(fp, args) for fp in args.capture ]

    if args.no_plot:
        for bin_mag_db, bin_freq, title in data:
            print(title.replace('\n',' '))
    else:
        import matplotlib.pyplot as plt

        # Try to make a square-ish set of subplots
        ncols = int(np.sqrt(len(data)))
        nrows = int(np.ceil(len(data) / ncols))

        fig, axes = plt.subplots(nrows, ncols, figsize=(12,12))
        if len(data) == 1:
            axes = np.array([axes])

        axes = axes.flatten()
        for ax,datum in zip(axes, data):
            bin_mag_db, bin_freq, title = datum
            min_bin_mag_db = min(bin_mag_db)

            xmin = (lo_freq + bin_freq[0]) / 1000
            xmax = (lo_freq + bin_freq[-1]) / 1000
            ymin = -120
            ymax = 0

            ax.plot((lo_freq + bin_freq)/1000, bin_mag_db)
            ax.set(xlabel='freq [kHz]', ylabel='dBFS')
            ax.grid(True)
            ax.set_xlim((xmin, xmax))
            ax.set_ylim((ymin, ymax))
            ax.set_title(title)
            ax.title.set_size(10)

        plt.subplots_adjust(left=0.08, bottom=0.05, right=0.95, top=0.95, hspace=0.4, wspace=0.2)

        if args.save_plot:
            print("Saving to", plot_name)
            plt.savefig(plot_name)

        plt.show()


if __name__ == '__main__':
    sys.exit(main())
