import argparse
import jenkins
import os

from jenkins_control import JenkinsController

def main(jobs: list):
    ID = os.environ['JENKINS_ID']
    TOKEN = os.environ['JENKINS_TOKEN']
    ADDR = "jenkins.epiq.rocks"
    JC = JenkinsController(f"https://{ID}:{TOKEN}@{ADDR}/")

    try:
        JC.disable_jobs(jobs)
    except jenkins.JenkinsException:
        print("Invalid or missing, ID or Token - refer to the README for more information")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Disable jenkins test jobs')
    parser.add_argument('-j', '--jobs', type=str, required=True,
                            help='comma-delimited list of jobs to disable, use "all" to disable all jobs (note: do not include spaces between job names')
    args = parser.parse_args()

    jobs = args.jobs.split(',')
    main(jobs)