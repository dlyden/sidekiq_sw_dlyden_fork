import jenkins

class JenkinsController:
    def __init__(self, url):
        self.server = jenkins.Jenkins(url)
        self.dir_list = ['TestNodes', 'TestNodesExtended', 'TestNodesFPGA','TestNodesRF']
        self.job_found = False

    def toggle_jobs(self, jobs, enable):
        if 'all' in (job.lower() for job in jobs):
            all_jobs = self.server.get_jobs()
            for d in self.dir_list:
                jobs = [(j) for j in [jobs_list for jobs_list in all_jobs if jobs_list['name'] == "Sidekiq"][0]['jobs'] if j['name'] == d]
                for job in jobs[0]['jobs']:
                    status = "Enabling" if enable else "Disabling"
                    print(f"{status} Sidekiq/{d}/{job['name']}...")
                    self.server.enable_job(f"Sidekiq/{d}/{job['name']}") if enable else self.server.disable_job(f"Sidekiq/{d}/{job['name']}")
        else:
            for job in jobs:
                for d in self.dir_list:
                    if self.server.job_exists(f"Sidekiq/{d}/{job}"):
                        status = "Enabling" if enable else "Disabling"
                        print(f"{status} Sidekiq/{d}/{job}...")
                        self.server.enable_job(f"Sidekiq/{d}/{job}") if enable else self.server.disable_job(f"Sidekiq/{d}/{job}")
                        if self.job_found is False:
                            self.job_found = True
                if self.job_found is False:
                    print(f"\t -- {job} not found --")
                self.job_found = False

    def enable_jobs(self, jobs):
        self.toggle_jobs(jobs, True)

    def disable_jobs(self, jobs):
        self.toggle_jobs(jobs, False)