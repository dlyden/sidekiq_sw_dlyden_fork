#!/bin/bash
set -e

# Get location of this script. Used to:
#   1. Find the build.git run_in_docker.sh relative to this file
#   2. Specify --local-mount (used to volume-map in the docker container) as
#      the root directory of the app/lib, which should be at the same as the
#      top-level Makefile, with all source files at or below that level
D="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Configure default docker image used by build.git run_in_docker.sh
export BG_DOCKER_IMAGE='docker.epiq.rocks/epiq-sw-dev-env-ubuntu20.04:0.6'

CONFIGS=(
    'x86_64.gcc'
    'x86_64.gcc4.8_glibc2.11'
    'x86_32.gcc4.8_glibc2.11'
    'arm_cortex-a9.gcc4.8_gnueabihf_linux'
    'arm_cortex-a9.gcc4.9.2_gnueabi'
    'arm_cortex-a9.gcc5.2_glibc_openwrt'
    'arm_cortex-a9.gcc7.2.1_gnueabihf'
    'aarch64.gcc6.3'
)

"${D}/build/run_in_docker.sh" \
    --local-mount="${D}" \
    --disable-configs \
    --disable-platforms \
    "${CONFIGS[@]/#/--enable-config=}" \
    "$@"
