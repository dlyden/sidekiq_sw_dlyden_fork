# retry function inspired by: https://davidoha.medium.com/python-retry-on-exception-d36fa58df4e1

import time

def retry(e_type=Exception, limit=0, err_str=None, wait_ms=0, wait_increase_ratio=0):
    def retry_decorator(func):
        def retry_wrapper(*args, **kwargs):
            attempt = 1
            while True:
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    if not isinstance(e, e_type):
                        raise e
                    if 0 < limit <= attempt:
                        raise e

                    retries = limit - attempt
                    if err_str:
                        print(f"{err_str} (retries left: {retries}).")
                    else:
                        print(f"[INFO] Failed execution attempt {attempt} (retries left: {retries}).")

                    attempt += 1
                    wait = wait_ms
                    if wait:
                        print(f"[INFO] Waiting {wait}ms before attempt {attempt}.")
                        time.sleep(wait / 1000)
                        wait*=wait_increase_ratio
        return retry_wrapper
    return retry_decorator