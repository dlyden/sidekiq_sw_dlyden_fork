import os
import subprocess

from test.card import Card

''' This file contains relevant information and shared variables for pytest execution,
    but does not provide any information necessary to implementing the Card class with
    the exception of the card's serial number.  However, this file is responsible for
    creating the list of Card implementations for use within the testing.
'''

try: 
    test_app_path = os.environ['APP_DIR']
    if not test_app_path:
        raise KeyError
except KeyError:
    print("APP_DIR is not defined, using default path to test apps.")
    test_app_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'host_code/libsidekiq/test_apps/bin/x86_64.gcc'))
    pass

init_levels = ['basic', 'full']
pps_sources = ['external']

serials_in_system = subprocess.run([f"{test_app_path}/sidekiq_probe"], capture_output=True, text=True, check=True).stdout.splitlines()

cards_in_system = []
for serial in serials_in_system:
    cards_in_system.append(Card(serial))
