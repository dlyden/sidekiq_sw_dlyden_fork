import pytest
import subprocess
from test.variables import test_app_path, init_levels, cards_in_system

@pytest.mark.parametrize("card", cards_in_system)
@pytest.mark.parametrize("init_level", init_levels)
def test_version(card, init_level):
    result = None
    execution_list = [f"{test_app_path}/version_test", f"--serial={card.serial}"]
    if init_level == "basic":
        result = subprocess.run(execution_list, capture_output=True)
    else:
        execution_list.append("--full")
        result = subprocess.run(execution_list, capture_output=True, check=True, text=True)

    card.log(result.stdout)
    assert result.returncode == 0