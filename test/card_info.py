import os
import subprocess

''' This file contains all relevant information that needs to be determined
    to fulfill the needs of the Card class and the tests that it will be run
    against.
'''

x86_64 = 'x86_64'
aarch64 = 'aarch64'
armv7 = 'armv7'
win64 = 'win64_msys'
gw_51xx = 'gateworks_51xx'
gw_54xx = 'gateworks_54xx'

max_sample_rate_map =   {
                            'platform': {
                                x86_64: {
                                    'pcie': {'all': 45000000, 
                                                'm.2': 61440000,
                                                'm.2-2280': 61440000,
                                                'nv100': 61440000,
                                                'x2': 153600000,
                                                'x4': 122880000
                                            }, 
                                    'usb': {'all': 8000000}
                                },
                                aarch64: {
                                    'pcie': {'all': 45000000},
                                    'usb': {'all': 8000000}
                                },
                                gw_51xx: {
                                    'pcie': {'all': 37000000},
                                    'usb': {'all': 7000000}
                                },
                                gw_54xx: {
                                    'pcie': {'all': 33000000},
                                    'usb': {'all': 6000000}
                                },
                                'unknown': {
                                    'pcie': {'all': 37000000},
                                    'usb': {'all': 8000000}
                                }
                            },
                            'z3u': 40000000,
                            'z2p': 40000000
                        }

gpsdo = ['m.2-2280', 'nv100', 'z3u']

def detect_platform() -> str:
    platform = 'unknown'

    gw_platform_path = '/sys/devices/soc0/machine'
    if os.path.exists(gw_platform_path):
        gw_res = int(subprocess.run(["awk", "''BEGIN {r=1} /GW51XX/{r=0} END {exit r}''", gw_platform_path],
                                        capture_output=True, text=True, check=True).stdout)
        if gw_res == 0:
            platform = gw_51xx

        gw_res = int(subprocess.run(["awk", "''BEGIN {r=1} /GW54XX/{r=0} END {exit r}''", gw_platform_path],
                                        capture_output=True, text=True, check=True).stdout)
        if gw_res == 0:
            platform = gw_54xx

        gw_res = int(subprocess.run(["awk", "''BEGIN {r=1} /GW52XX/{r=0} END {exit r}''", gw_platform_path],
                                        capture_output=True, text=True, check=True).stdout)
        if gw_res == 0:
            platform = gw_54xx # Map the GW52xx boards to the 54XX platform.

        gw_res = int(subprocess.run(["awk", "''BEGIN {r=1} /GW53XX/{r=0} END {exit r}''", gw_platform_path],
                                        capture_output=True, text=True, check=True).stdout)
        if gw_res == 0:
            platform = gw_54xx # Map the GW53xx boards to the 54XX platform.

    if platform == 'unknown':
        if subprocess.run(["uname", "-o"], capture_output=True, text=True, check=True).stdout == 'Msys':
            platform = win64

        else:
            generic = subprocess.run(["uname", "-m"], capture_output=True, text=True).stdout
            if generic == armv7:
                platform = armv7
            elif generic == x86_64:
                platform = x86_64
            elif generic == aarch64:
                platform == aarch64

    return platform

def set_max_sample_rate(hardware: str, transport: str) -> int:    
    special_platforms = ['z2', 'z3u']
    if hardware in special_platforms:
        return max_sample_rate_map[hardware]

    platform = detect_platform()
    sample_rates = max_sample_rate_map['platform'][platform][transport]

    if hardware in sample_rates.keys():
        return max_sample_rate_map['platform'][platform][transport][hardware]
    else:
        return max_sample_rate_map['platform'][platform][transport]['all']