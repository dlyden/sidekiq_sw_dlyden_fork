# This pytest unit does:
# Step 1: Transmitting tx_configure using card_tx on rf handle a1
# Step 2: Receiving rx_samples using card_rx on rf handle a1
# Step 3: Extract Tx tone frequency
# Step 4: Process the output samples to find the peak magnitude and peak frequency

import pytest
from time import sleep
from test.peak_detect import process_fp, peak_detect_input_args
from test.variables import test_app_path, cards_in_system
from subprocess import Popen, PIPE, run
from copy import deepcopy

# the peak magnitude level in dB and tone frequency diffrence level in Hz
tone_detect_level=-30
tone_freq_diff=5000

@pytest.mark.parametrize("test_freq", [1950e6])
@pytest.mark.parametrize("sample_rate", [1536e4])

def test_rx_tone_detect(test_freq, sample_rate):
        card_tx = cards_in_system[0]
        card_rx = cards_in_system[1]
        tx_time = 20
        rx_result = None
              
        # Step 1: Transmitting tx_configure using card_tx on rf handle a1
        test_tx_configure = [f"{test_app_path}/tx_configure", f"--serial={card_tx.serial}", f"--frequency={test_freq}", f"--time={tx_time}", f"--rate={sample_rate}"]
        with Popen(test_tx_configure, stderr=None, stdout=PIPE, close_fds=True, text=True) as tx_proc:
                #add some delay here to ensure tx_configure complete the initializations and started streaming before receiving samples
                sleep(10)

                # Step 2: Receiving rx_samples using card_rx on rf handle a1
                test_rx_samples = [f"{test_app_path}/rx_samples", f"--serial={card_rx.serial}", f"--frequency={test_freq}", f"--rate={sample_rate}", "--destination=output.bin"]
                rx_result = run(test_rx_samples, stderr=None, stdout=PIPE, check=True, text=True)

                # Step 3: Extract Tx tone frequency
                stdout, stderr = tx_proc.communicate()
                tone_found = False
                for line in stdout.splitlines():
                        if line.startswith("Info: TX test tone located at freq"):
                                tone_frequency = int(''.join(list(filter(str.isdigit, line))))
                                tone_found = True
                                break
                if not tone_found:
                                print("Error: not able to extract Tx tone frequency!") 

        # Step 4: Process the output samples to find the peak magnitude and peak frequency
        input_args = deepcopy(peak_detect_input_args)
        input_args['lo_freq'] = test_freq
        input_args['samp_rate'] = sample_rate
        
        peak_value, peak_freq = process_fp('output.bin.a1', input_args)

        final_result = (peak_value > tone_detect_level) and (abs(peak_freq - tone_frequency) < tone_freq_diff)
        card_rx.log(rx_result.stdout)
        assert final_result