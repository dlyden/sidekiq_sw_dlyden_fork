import pytest
import subprocess

from test.variables import test_app_path, cards_in_system

@pytest.mark.parametrize("card", cards_in_system)
@pytest.mark.parametrize("target_throughput_proportion", [0.9])
def test_rx_benchmark(card, target_throughput_proportion):
    result = None
    ''' The target throughput is defined as a percentage of the sample rate and is specified in MB/sec.

        Since sample rate is specified in samples/sec a conversion is needed to determine what
        the target throughput is.  That conversion is done as follows:
        - sample_rate (samples/second) * 4 (bytes/sample) results in target throughput specified
          in bytes/sec.
        - target throughput (bytes/sec) * 0.9 (90% of the total throughput) [target_throughput_proportion]
          specified in bytes/sec.
        - target throughput (bytes/sec) * 1MB/1,000,000bytes converts target throughput to MB/sec.
    ''' 
    target = int(((card.max_sample_rate * 4) * target_throughput_proportion) / 1000000)
    execution_list = [f"{test_app_path}/rx_benchmark", f"--serial={card.serial}", "--handle=A1", "--time=5",
                        "--threshold=7", f"--rate={card.max_sample_rate}", f"--target={target}"]

    result = subprocess.run(execution_list, capture_output=True, text=True)
    card.log(result.stdout)
    assert result.returncode == 0