import logging
import subprocess

from test import variables as v
from test import card_info

class Card():
    ''' The Card class contains all the information that is needed for a single card
        installed in the DUT.  It also sets up an individual logger for each card in the DUT.
    '''
    def __init__(self, serial):
        self.serial = serial
        self.index = int(subprocess.run([f"{v.test_app_path}/sidekiq_probe", f"--serial={self.serial}", "--fmtstring=%c"], capture_output=True, text=True, check=True).stdout.lower())
        self.hw = subprocess.run([f"{v.test_app_path}/sidekiq_probe", f"--serial={self.serial}", "--fmtstring=%P"], capture_output=True, text=True, check=True).stdout.lower()
        self.xport = subprocess.run([f"{v.test_app_path}/sidekiq_probe", f"--serial={self.serial}", "--fmtstring=%t"], capture_output=True, text=True, check=True).stdout.lower()
        self.platform = card_info.detect_platform()
        self.max_sample_rate = card_info.set_max_sample_rate(self.hw, self.xport)
        self.has_gpsdo = self.hw in card_info.gpsdo
        self.has_tx = True if self.xport != 'usb' else False

        # setup the logger for card level logging
        self.logger = logging.getLogger(f"card[{self.serial}]")
        self.logger.setLevel(logging.INFO)
        log_handler = logging.FileHandler('/dev/null')
        log_handler.setFormatter(logging.Formatter('%(name)s[%(levelname)s] %(message)s'))
        self.logger.addHandler(log_handler)

    def log(self, msg: str):
        lines = msg.splitlines()
        for line in lines:
            self.logger.info(line)