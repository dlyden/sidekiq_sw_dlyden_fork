#!/usr/bin/env python3

## modified from sidekiq_sw/scripts/plot_spectrum.py, return the peak value and frequency

import numpy as np

# Default the number of samples read from a file
default_iq_len=10000

peak_detect_input_args = {
    'lo_freq' : 1950000000,
    'samp_rate' : 15360000,
    'n_bits_iq' : 12,
    'iq_len' : 10000,
    'skip' : 1024,
    'fft_len' : 2**17,
    'swap_iq' : False
}

def read_iq_from_fp(fp, skip=0, count=default_iq_len, swap_iq=False):

    # count is number of dtype size while offset is number of bytes
    tmp = np.fromfile(fp, dtype=np.uint32, count=count, offset=4*skip)
    tmp.dtype = np.int16

    if swap_iq:
        # Everyone else's I/Q ordering
        return tmp[0::2] + 1j*tmp[1::2]
    else:
        # Traditional Sidekiq I/Q ordering, i.e. Q-then-I
        return tmp[1::2] + 1j*tmp[0::2]


def compute_mag_spectrum(x, samp_rate_hz, fft_len):
    # bin frequencies
    t_samp = 1./samp_rate_hz
    bin_freq_hz = np.fft.fftfreq(fft_len, t_samp)

    # get mag spectrum of windowed zero-padded input
    window = np.hamming(len(x))
    x_zp = np.zeros((fft_len,), dtype=complex)
    x_zp[:len(x)] = x * window
    mag_spectrum = np.abs(np.fft.fft(x_zp, norm='ortho'))**2

    # figure out scale factor (each factor is squared since applied to mag spectrum)
    # so that 0 dBFS sinusoid will have mag spectrum peak at 0 dB
    xlen_factor = len(x)
    window_factor = 1./(window.sum()**2)
    zp_factor = fft_len/len(x)
    scale = xlen_factor * window_factor * zp_factor

    return (np.fft.fftshift(bin_freq_hz), np.fft.fftshift(scale*mag_spectrum))

def process_fp(fp, args):
    lo_freq = int(args['lo_freq'])
    samp_rate = int(args['samp_rate'])
    n_bits_iq = args['n_bits_iq']
    iq_len = args['iq_len']
    fft_len = args['fft_len']

    # read, scale, truncate IQ sequence
    x = read_iq_from_fp(fp, skip=args['skip'], count=iq_len, swap_iq=args['swap_iq'])
    full_scale_mag = 2.**(n_bits_iq-1)
    x /= full_scale_mag

    # calc spectrum
    bin_freq, bin_mag = compute_mag_spectrum(x, samp_rate, fft_len)
    bin_mag_db = 10*np.log10(bin_mag)


    # find top peak and plot
    idx = np.argmax(bin_mag_db)
    peak_db = bin_mag_db[idx]
    peak_freq_hz = bin_freq[idx]
    peak_abs_freq_hz = lo_freq + peak_freq_hz

    return peak_db, peak_abs_freq_hz

