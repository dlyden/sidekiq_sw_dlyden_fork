import pytest
import subprocess

from test.variables import test_app_path, cards_in_system, pps_sources
from time import sleep

@pytest.mark.parametrize("card", cards_in_system)
@pytest.mark.parametrize("pps_source", pps_sources)
def test_gpsdo_persistence(card, pps_source):
    if not card.has_gpsdo:
        pytest.skip(reason="GPSDO persistence skipped due to unsupported hardware")

    result = None
    execution_list = [f"{test_app_path}/read_gpsdo", f"--serial={card.serial}", "--leave-enabled", "--polling-time=7", f"--pps-source={pps_source}"]
    
    result = subprocess.run(execution_list, capture_output=True)
    card.log(result.stdout.decode("utf-8"))
    assert result.returncode == 0

    execution_list.append("--timeout=30")
    for i in range(5):
        result = subprocess.run(execution_list, capture_output=True)
        card.log(result.stdout.decode("utf-8"))
        assert result.returncode == 0
        sleep(5) # a delay is required to give the control algorithm time to unlock to indicate a failure

    execution_list.remove("--leave-enabled")
    result = subprocess.run(execution_list, capture_output=True)
    card.log(result.stdout.decode("utf-8"))
    assert result.returncode == 0