import argparse
import getpass
import os
import paramiko
import re
import subprocess

from custom_sftp_client import CustomSFTPClient
from retry_decorator import retry

""" This file sets up the environment for pytest execution on the DUT, collects all
    relevant information necessary for determining what applications need to be
    copied to the DUT, and copies those relevant files to the DUT.
"""

# get directory access to sidekiq_sw through root directory
root_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

# create a test apps list to reduce time needed to copy test bundle to target until it no longer makes sense
# TODO: when this no longer makes sense as a whitelist, revisit the custom_sftp_client's TODO as well
test_apps = ['sidekiq_probe', 'version_test', 'read_gpsdo', 'rx_benchmark','rx_samples','tx_configure']

@retry(e_type=paramiko.ssh_exception.AuthenticationException,
        limit=3,
        err_str="[ERROR] Password incorrect, please try again")
def get_ssh_client(host: str, user: str):
    """ Attempts to make an ssh connection to user@host without a password.
        If that fails it then prompts the user for a password to authenticate
        with.
    """
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy())
    try:
        ssh_client.connect(host, username=user)
        return ssh_client
    except paramiko.ssh_exception.AuthenticationException:
        try:
            password = getpass.getpass(prompt=f"Enter password for {user}@{host}: ", stream = None)
            ssh_client.connect(host, username=user, password=password)
            password = None
            return ssh_client
        except paramiko.ssh_exception.AuthenticationException as e:
            password = None
            raise e

def determine_build_config(ssh_client) -> str:
    """ Using the ssh client, send commands that allows detection of what build config to use.
    """
    # for each of the following variables:
    #   grab the stdout from the command (tuple value 1), then decode and strip the output
    stdout = 1
    platform = ssh_client.exec_command("uname -m")[stdout].read().decode('utf-8').strip()
    os_type = ssh_client.exec_command("uname -o")[stdout].read().decode('utf-8').strip()
    hostname = ssh_client.exec_command("uname -n")[stdout].read().decode('utf-8').strip()

    if platform == "x86_64" and os_type == "Msys":
        build_config = "windows"
    else:
        if platform == "x86_64" and os_type == "GNU/Linux":
            build_config = "x86_64.gcc"
        elif platform == "aarch64" and os_type == "GNU/Linux":
            build_config = "aarch64.gcc6.3"
        elif platform == "armv7l" and os_type == "GNU/Linux":
            # if the platform is an armv7l device then we need to find LIBC version information for build config determination
            version = ssh_client.exec_command("/bin/opkg info libc | /bin/grep '^Version:' | /usr/bin/awk '{print $2}'")[stdout].read().decode('utf-8').strip()
            if re.search("^0.9.33.2-", version):
                build_config = "arm_cortex-a9.gcc4.8_uclibc_openwrt"
            elif re.search("^(2.21-|2.22-)", version):
                build_config = "arm_cortex-a9.gcc5.2_glibc_openwrt"
            else:
                print("[ERROR] Unsupported libc version!")
                raise ValueError
        elif hostname == "sidekiqz2":
            # if the hostname determines that a Sidekiq Z2 is the product then we need to find BSP version information for build config determination
            version = ssh_client.exec_command("grep ^z2-sw-version /opt/VERSIONS | grep /usr/bin/awk '{print $2}'")[stdout].read().decode('utf-8').strip()
            if re.search("^v3.", version):
                build_config = "arm_cortex-a9.gcc4.8_uclibc_openwrt"
            elif re.search("^v2.", version):
                build_config = "arm_cortex-a9.gcc4.9.2_gnueabi"
            else:
                print("[ERROR] Unsupported z2-sw-verwion version!")
                raise ValueError
        elif hostname == "z3u" or hostname == "cinch-a-00":
            build_config = "aarch64.gcc6.3"
        else:
            print("[ERROR] Unknown target platform!")
            raise ValueError

    return build_config

def copy_test_bundle(ssh_client, build_config: str) -> str:
    """ Copy the relevant test artifacts to the DUT.
    """
    # create relevant bundle paths
    app_dir = os.path.abspath(os.path.join(root_dir, "host_code/libsidekiq/test_apps/bin", build_config))
    pytest_dir = os.path.abspath(os.path.join(root_dir, "test"))
    pytest_path = os.path.join(args.dir, "test")
    pipfile_path = os.path.abspath(os.path.join(root_dir, "Pipfile"))
    
    # clear current contents of remote app directory
    print("[INFO] Clearing current contents of target directory")
    ssh_client.exec_command(f"rm -rf {args.dir}")
    ssh_client.exec_command(f"mkdir -p {args.dir}")
    ssh_client.exec_command(f"mkdir -p {pytest_path}")
    
    # copy all C test apps, pytest tests and Pipfile to target location
    print(f"[INFO] Creating SFTP connection to {args.user}@{args.host}...")
    sftp_client = CustomSFTPClient.from_transport(ssh_client.get_transport())

    print("[INFO] Copying C test apps to target")
    sftp_client.put_dir(app_dir, args.dir, whitelist=test_apps)

    print("[INFO] Copying Pytest tests to target")
    sftp_client.put_dir(pytest_dir, pytest_path)

    print("[INFO] Copying Pipfile to target")
    sftp_client.put(pipfile_path, os.path.join(args.dir, "Pipfile"))

    print("[INFO] Closing SFTP connection")
    sftp_client.close()

def main():
    """ Create an ssh connection to the DUT, determine what build config is needed and copy
        the relevant test artifacts to the DUT.
    """
    print(f"[INFO] Creating SSH connection to {args.user}@{args.host}...")
    try:
        ssh_client = get_ssh_client(args.host, args.user)
    except paramiko.ssh_exception.AuthenticationException as e:
            exit(code=f"[ERROR] Failed to create SSH connection! {e}")

    build_config = determine_build_config(ssh_client)
    copy_test_bundle(ssh_client, build_config)

    print("[INFO] Closing SSH connection")
    ssh_client.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Configure target DUT to have pytest regression executed.')
    parser.add_argument('--host', type=str, required=True,
                            help='remote host address for target DUT')
    parser.add_argument('-u', '--user', type=str, required=False,
                            help='remote user for target DUT')
    parser.add_argument('-d', '--dir', type=str, required=False, default='/tmp/regression',
                            help='directory remote apps will be copied to')
    args = parser.parse_args()

    if not args.user:
        # if args.user is None, get the user from the calling PC, decode the byte return value, and strip the trailing newline
        args.user = subprocess.run(['whoami'], capture_output=True).stdout.decode('utf-8').strip()

    main()