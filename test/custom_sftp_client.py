import paramiko
import os
import stat

''' Paramiko's current implementation of the SFTP client does not support recursive
    put and forces the user to call put for each individual file that needs to be
    copied over.  This custom implementation extends the paramiko.SFTPClient class
    and implements recursive put as put_dir.
'''
class CustomSFTPClient(paramiko.SFTPClient):
    def put_dir_handler(self, source, target, item, permissions):
        ''' Uploads the contents of the source directory to the target path. The
            target directory needs to exists. All subdirectories in source are 
            created under target.
        '''
        target_path = f"{target}/{item}"
        source_path = os.path.join(source, item)
        if os.path.isfile(source_path):
            print(f"[SFTP] {source_path} --> {target_path}")
            self.put(source_path, target_path)
            self.chmod(target_path, permissions)
        else:
            print(f"[SFTP] mkdir {target_path}")
            self.mkdir(target_path, ignore_existing=True)
            self.put_dir(source_path, target_path)
    
    def put_dir(self, source, target, whitelist=None):
        ''' Sets up permissions for the put commands and allows items to be whitelisted
            within the directory if need be.  The whitelisting can increase the speed at
            which recursive put finishes while still allowing a user to use a single
            command to recursively put a directory on a target.

            TODO: this may need to evolve into a blacklist when it makes sense.
        '''
        permissions = stat.S_IRUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH
        for item in os.listdir(source):
            if whitelist:
                if item in whitelist:
                    self.put_dir_handler(source, target, item, permissions)
            else:
                self.put_dir_handler(source, target, item, permissions)

    def mkdir(self, path, mode=(stat.S_IRWXU | stat.S_IRWXO | stat.S_IRWXO), ignore_existing=False):
        ''' Augments mkdir by adding an option to not fail if the folder exists  '''
        try:
            super(CustomSFTPClient, self).mkdir(path, mode)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise