export libtirpc_ROOT:= $(SIDEKIQ_ROOT)/libraries/libtirpc/build.$(SUFFIX)/support

#
# CFLAGS, LDFLAGS for use by modules when dynamically linking
#
# \$$$$ORIGIN is not a typo.  :(
# since libtirpc_LDFLAGS is used at levels of dereferencing in make here, $$$$ => $$, then $$ => $
# then bash needs \ to stop it from dereference, it removes \
# linker then sees $ORIGIN as required
#
# this is likely not a portable solution and will need more investigation
#
export libtirpc_CFLAGS:= -I$(libtirpc_ROOT)/usr/include/tirpc

ifeq ($(BUILD_CONFIG),arm_cortex-a9.gcc4.8_uclibc_openwrt)
export libtirpc_LDFLAGS:= -L$(libtirpc_ROOT)/usr/lib/epiq -Wl,-rpath=/usr/lib/epiq -ltirpc
else ifeq ($(BUILD_CONFIG),mingw64)
export libtirpc_LDFLAGS:= -L$(libtirpc_ROOT)/usr/lib/bin -Wl,-rpath=\$$$$ORIGIN:/usr/lib/bin -ltirpc
else
export libtirpc_LDFLAGS:= -L$(libtirpc_ROOT)/usr/lib/epiq -Wl,-rpath=\$$$$ORIGIN:/usr/lib/epiq -ltirpc
endif

# LDFLAGS for static linking
export libtirpc_LDFLAGS_STATIC:= $(libtirpc_ROOT)/usr/lib/epiq/libtirpc.a
