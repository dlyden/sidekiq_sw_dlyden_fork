/**
 * @file   fpga_reg_hal.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <unistd.h>
#include <string.h>

#include "sidekiq_hal.h"
#include "sidekiq_private.h"
#include "hal_file_ops.h"
#include "fpga_reg_hal.h"
#include "fpga_reg_hal_icap.h"
#include "card_services.h"

#include "bit_ops.h"
#include "flash_defines.h"
#include "sidekiq_usb_cmds.h"   /* for FIRMWARE_VERSION_INFO_ADDR */

#include "sidekiq_xport.h"

/* enable debug_print and debug_print_plain when DEBUG_FPGA_REG_HAL_TRACE is defined */
#if (defined DEBUG_FPGA_REG_HAL_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

/* enable debug_print and debug_print_plain when DEBUG_FPGA_REG_HAL is defined */
#if (defined DEBUG_FPGA_REG_HAL)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/* we have a max num of access attempts that we'll make to the hardware for a given read or write
   operation...after which, we'll fail gracefully */
#define MAX_NUM_ACCESS_ATTEMPTS 2000

/* I2C read/write operations can be NACK'ed by the slave (and we see it in practice when an I2C
   slave is busy processing the previous transaction when a new one comes in).  We need to detect
   this and re-issue the transaction again, up to a point where we'll finally give up and report an
   error */
#define MAX_NUM_I2C_RETRIES 512

/* hardware currently imposes a max of 4 bytes per I2C transaction, can be fixed later by moving to
   a FIFO interface in the FPGA */
#define MAX_NUM_I2C_DATA_BYTES_PER_TRANSACTION 4

#define I2C_WRITE_CMD                   0
#define I2C_READ_CMD                    1

#define EEPROM_I2C_ADDR                 (0x51)
#define FMC_EEPROM_I2C_ADDR             (0x50)
#define HTG_MUX_I2C_ADDR                (0x70)

/* define the I2C EEPROM page size.  Most EEPROMs will not allow a sequential write across page
 * boundaries, regardless of length */
#define EEPROM_PAGE_SIZE                16


/***** TYPE DEFINITIONS *****/

enum i2c_bus {
    i2c_bus_0 = 0,
    i2c_bus_1 = 1,
    i2c_bus_2 = 2,
    i2c_bus_max,
};


/***** STRUCTS *****/

struct i2c_master
{
    uint32_t write_data_register;
    uint32_t slave_cmd_register;
    uint32_t read_data_register;
    uint32_t status_register;
};


/***** LOCAL DATA *****/

struct i2c_master masters[] =
{
    [i2c_bus_0] = {
        .write_data_register = FPGA_REG_I2C_WR_DATA,
        .slave_cmd_register  = FPGA_REG_I2C_SLAVE_CMD,
        .read_data_register  = FPGA_REG_I2C_RD_DATA,
        .status_register     = FPGA_REG_I2C_STATUS,
    },
    [i2c_bus_1] = {
        .write_data_register = FPGA_REG_I2C_1_WR_DATA,
        .slave_cmd_register  = FPGA_REG_I2C_1_SLAVE_CMD,
        .read_data_register  = FPGA_REG_I2C_1_RD_DATA,
        .status_register     = FPGA_REG_I2C_1_STATUS,
    },
    [i2c_bus_2] = {
        .write_data_register = FPGA_REG_I2C_2_WR_DATA,
        .slave_cmd_register  = FPGA_REG_I2C_2_SLAVE_CMD,
        .read_data_register  = FPGA_REG_I2C_2_RD_DATA,
        .status_register     = FPGA_REG_I2C_2_STATUS,
    },
};


#if (defined ATE_SUPPORT)
/* The first time the FMC I2C bus is accessed for a particular card, it must configure the
 * multiplexer in front of the EEPROM */
static ARRAY_WITH_DEFAULTS(bool, fmc_i2c_bus_accessible, SKIQ_MAX_NUM_CARDS, false);
#endif


/***** LOCAL FUNCTIONS *****/

static bool
is_i2c_bus_supported( uint8_t card,
                      uint8_t bus )
{
    bool is_supported = false;
    skiq_part_t part = _skiq_get_part( card );

    if ( bus == i2c_bus_0 )
    {
        is_supported = true;
    }
    else if ( bus == i2c_bus_1 )
    {
        skiq_fmc_carrier_t carrier = _skiq_get_fmc_carrier( card );

        /* The secondary I2C bus was not available until FPGA version v3.10.0 and is only supported
         * on the HTG-K800 carrier board or the Sidekiq M.2-2280/Z2p/Z3u */
        if ( !_skiq_meets_fpga_version( card, 3, 10, 0 ) )
        {
            skiq_error("Minimum FPGA version of v3.10.0 required for secondary I2C bus support on "
                       "card %u\n", card);
            is_supported = false;
        }
        else
        {
            if ( ( carrier == skiq_fmc_carrier_htg_k800 ) ||
                 ( part == skiq_m2_2280 ) ||
                 ( part == skiq_z2p ) ||
                 ( part == skiq_z3u ) )
            {
                is_supported = true;
            }
            else if ( carrier != skiq_fmc_carrier_not_applicable )
            {
                /* if the carrier is "applicable", it means the part associates itself with a
                 * carrier, so a more direct error message can be displayed */
                skiq_error("HTG-K800 FMC carrier board required for secondary I2C bus support on "
                           "card %u\n", card);
                is_supported = false;
            }
            else
            {
                skiq_error("Sidekiq part (%s at card %u) does not have secondary I2C bus support\n",
                           part_cstr( part ), card );
                is_supported = false;
            }
        }
    }
    else if ( bus == i2c_bus_2 )
    {
        /* M.2-2280, Z2+, Z3u, and NV100 all support a third I2C bus (I2C_BUS2) */
        if ( (part == skiq_m2_2280) ||
             (part == skiq_z2p) ||
             (part == skiq_z3u) ||
             (part == skiq_nv100) )
        {
            is_supported = true;
        }
    }

    return is_supported;
}


/**************************************************************************************************/
static int32_t
_write_i2c( uint8_t card,
            enum i2c_bus bus,
            uint8_t chip_addr,
            uint8_t* p_data,
            uint8_t num_bytes )
{
    int32_t status = 0;
    bool done = false;
    uint32_t num_retries = MAX_NUM_I2C_RETRIES;
    struct i2c_master master = masters[0];
    uint32_t data_val = 0, slave_val = 0;

    TRACE_ENTER;

    if (num_bytes > MAX_NUM_I2C_DATA_BYTES_PER_TRANSACTION)
    {
        skiq_error("Maximum I2C write size is 4 bytes\n");
        status = -1;
        goto trace_exit;
    }

    /* bus identifier out of bounds */
    if ( ( bus < i2c_bus_0 ) || ( bus >= i2c_bus_max ) )
    {
        status = -EINVAL;
    }
    else
    {
        master = masters[bus];
    }

    /* check for bus support for the given card */
    if ( status == 0 )
    {
        if ( !is_i2c_bus_supported( card, bus ) )
        {
            status = -ENOTSUP;
        }
    }

    /* populate data_val with supplied (p_data,num_bytes) and slave_val with slave address and
     * number of bytes to send */
    if ( status == 0 )
    {
        slave_val = 0;
        RBF_SET(slave_val, I2C_WRITE_CMD, I2C_SLAVE_RD_WR); /* write=0 */
        RBF_SET(slave_val, chip_addr, I2C_SLAVE_ADDR);
        RBF_SET(slave_val, (num_bytes-1), I2C_CMD_NUM_BYTES);
        memcpy(&data_val, p_data, num_bytes);
    }

    while ( ( !done ) && ( num_retries > 0 ) && ( status == 0 ) )
    {
        uint32_t write_attempts;
        bool op_complete;

        /* start off variables for the transaction to a known state */
        write_attempts = MAX_NUM_ACCESS_ATTEMPTS;
        op_complete = false;

        /* write down up to four bytes to the I2C data reg in the FPGA */
        status = sidekiq_fpga_reg_write_and_verify(card, master.write_data_register, data_val);

        /* do the reg write which will trigger the FPGA to execute the I2C write operation */
        if ( status == 0 )
        {
            sidekiq_fpga_reg_write_and_verify(card, master.slave_cmd_register, slave_val);
        }

        /* poll waiting for the FPGA to indicate the operation has completed */
        while ( (!op_complete) && ( write_attempts > 0 ) && ( status == 0 ) )
        {
            uint32_t i2c_status = 0;

            status = sidekiq_fpga_reg_read(card, master.status_register, &i2c_status);
            if ( status == 0 )
            {
                if ( RBF_GET(i2c_status, I2C_DONE ) > 0 )
                {
                    op_complete = true;
                    /* need to check and see if the slave NACKed us */
                    if ( RBF_GET(i2c_status, I2C_SLAVE_NACK) > 0 )
                    {
                        /* slave NACKed us, we need to re-try the entire transaction again, no
                         * biggie */
                        write_attempts--;
                        continue;
                    }
                    else
                    {
                        /* transaction completed successfully */
                        done = true;
                    }
                }
                else
                {
                    write_attempts--;
                    hal_nanosleep(50*MICROSEC);
                }
            }
        }

        if ( status == 0 )
        {
            if ( !op_complete )
            {
                /* the FPGA never indicated the op completed, so bail out of the transaction by
                   forcing num_retries to 0 */
                num_retries = 0;
                skiq_error("FPGA never indicated I2C write operation completed on card %u address 0x%x bus %u\n", card, chip_addr, bus);
            }
            else if ( !done )
            {
                /* the operation did complete, but we got a NACK from the slave device, so we need
                   to try again */
                num_retries--;
            }
        }
    }

    if (num_retries == 0)
    {
        skiq_error("Write I2C transaction did not complete successfully (address 0x%x) on card "
                   "%u\n", chip_addr, card);
        status = -1;
    }
    else if ( status == 0 )
    {
        uint8_t i = 0;

        debug_print("Info: Write I2C transaction completed successfully: \n");
        debug_print("Bus=%u, Slave addr=0x%02x, num bytes=%d, data attempted:",
                    bus, chip_addr, num_bytes);
        for (i=0; i<num_bytes; i++)
        {
            debug_print_plain("0x%02x ",*(p_data+i));
        }
        debug_print_plain("\n");
    }

trace_exit:
    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
static int32_t
_read_i2c( uint8_t card,
           enum i2c_bus bus,
           uint8_t slave_addr,
           uint8_t* p_data,
           uint8_t num_bytes )
{
    int32_t status=0;
    bool done = false;
    uint32_t num_retries = MAX_NUM_I2C_RETRIES;
    struct i2c_master master = masters[0];
    uint32_t slave_val = 0;

    TRACE_ENTER;

    if (num_bytes > MAX_NUM_I2C_DATA_BYTES_PER_TRANSACTION)
    {
        skiq_error("Maximum I2C read size is 4 bytes\n");
        status = -1;
        goto trace_exit;
    }

    /* bus identifier out of bounds */
    if ( ( bus < i2c_bus_0 ) || ( bus >= i2c_bus_max ) )
    {
        status = -EINVAL;
    }
    else
    {
        master = masters[bus];
    }

    /* check for bus support for the given card */
    if ( status == 0 )
    {
        if ( !is_i2c_bus_supported( card, bus ) )
        {
            status = -ENOTSUP;
        }
    }

    /* populate slave_val with slave address and number of bytes to send */
    if ( status == 0 )
    {
        slave_val = 0;
        RBF_SET(slave_val, I2C_READ_CMD, I2C_SLAVE_RD_WR); /* read=1 */
        RBF_SET(slave_val, slave_addr, I2C_SLAVE_ADDR);
        RBF_SET(slave_val, (num_bytes-1), I2C_CMD_NUM_BYTES);
    }

    while ( ( !done ) && ( num_retries > 0 ) && ( status == 0 ) )
    {
        uint32_t read_attempts;
        bool op_complete;

        /* start off variables for the transaction to a known state */
       read_attempts = MAX_NUM_ACCESS_ATTEMPTS;
       op_complete = false;

       /* do the actual I2C read by writing the register */
       status = sidekiq_fpga_reg_write_and_verify(card, master.slave_cmd_register, slave_val);

       /* poll waiting for the FPGA to indicate the operation has completed */
       while ( ( !op_complete ) && ( read_attempts > 0 ) && ( status == 0 ) )
       {
           uint32_t i2c_status = 0;

           status = sidekiq_fpga_reg_read(card, master.status_register, &i2c_status);
           if ( status == 0 )
           {
               if ( RBF_GET(i2c_status, I2C_DONE) > 0 )
               {
                   op_complete = true;
                   /* need to check and see if the slave NACKed us */
                   if ( RBF_GET(i2c_status, I2C_SLAVE_NACK) > 0 )
                   {
                       /* slave NACKed us, we need to re-try the entire transaction again, no
                        * biggie */
                       read_attempts--;
                       continue;
                   }
                   else
                   {
                       /* transaction completed successfully */
                       done = true;
                   }
               }
               else
               {
                   read_attempts--;
                   hal_nanosleep(50*MICROSEC);
               }
           }
       }

       if ( status == 0 )
       {
           if ( !op_complete )
           {
               /* the FPGA never responded, so completely bail out of the transaction by forcing
                  num_retries to 0 */
               num_retries=0;
               skiq_error("FPGA never indicated I2C read operation completed on card %u\n", card);
           }
           else if ( !done )
           {
               /* the operation did complete, but we got a NACK, so we need to try again */
               num_retries--;
           }
       }
    }

    if (num_retries == 0)
    {
        skiq_error("Read I2C transaction did not complete successfully (address 0x%x) on card "
                   "%u\n", slave_addr, card);
        status = -1;
    }
    else if ( status == 0 )
    {
        uint32_t i, val = 0;

        /* we were successful, so read the data out from the I2C data reg */
        status = sidekiq_fpga_reg_read(card, master.read_data_register, &val);

        /* copy the relevant bytes over to the return pointer */
        if ( status == 0 )
        {
            memcpy(p_data, &val, num_bytes);
            debug_print("Info: Read I2C transaction completed successfully: \n");
            debug_print("Bus=%u, Slave addr=0x%02x, num bytes=%d, data attempted:",
                        bus, slave_addr, num_bytes);
            for (i=0; i<num_bytes; i++)
            {
                debug_print_plain("0x%02x ",*(p_data+i));
            }
            debug_print_plain("\n");
        }
    }

trace_exit:
    TRACE_EXIT_STATUS(status);

    return(status);
 }


/**************************************************************************************************/
/** Request 'num_bytes' bytes to read from a Double Byte Addressed (DBA) EEPROM device at I2C
    address 'chip_addr' and at memory location offset starting at 'mem_addr'.
 */
static int32_t
_read_eeprom( uint8_t card,
              enum i2c_bus bus,
              uint8_t chip_addr,
              uint16_t mem_addr,
              bool two_byte_addressable,
              uint8_t *p_data,
              uint16_t num_bytes )
{
    int32_t status = 0;
    uint8_t buf[2];
    uint8_t read_len = 0, write_len = 0;
    uint16_t i = 0;

    TRACE_ENTER;

    /* populate either one or two bytes of 'buf' depending on whether this is a DBA or SBA EEPROM
     * device */
    if ( two_byte_addressable )
    {
        buf[write_len++] = (mem_addr >> 8) & 0xFF;
        buf[write_len++] = (mem_addr >> 0) & 0xFF;
    }
    else
    {
        if ( mem_addr < UINT8_MAX )
        {
            buf[write_len++] = mem_addr & 0xFF;
        }
        else
        {
            /* single byte addressed ICs must have an address less than a single byte */
            debug_print("ERROR: single byte address MUST be less than %u\n", UINT8_MAX);
            status = -EINVAL;
        }
    }

    /* write the desired memory address before reading back contents */
    if ( status == 0 )
    {
        status = _write_i2c(card, bus, chip_addr, buf, write_len);
    }

    /* keep reading all the requested number of bytes in blocks of 4 */
    for ( i = 0; (i < num_bytes) && (0 == status); i += read_len )
    {
        /* read either 4 bytes or the remainder, whichever is less */
        read_len = MIN(4, num_bytes - i);
        status = _read_i2c(card, bus, chip_addr, p_data, read_len);
        if( 0 == status )
        {
            p_data += read_len;
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
static int32_t
_write_eeprom( uint8_t card,
               enum i2c_bus bus,
               uint8_t chip_addr,
               uint16_t mem_addr,
               bool two_byte_addressable,
               uint8_t *p_data,
               uint16_t num_bytes )
{
    int32_t status = 0;
    uint8_t write_len = 0, addr_len;
    uint8_t buf[MAX_NUM_I2C_DATA_BYTES_PER_TRANSACTION];
    uint32_t i = 0;

    TRACE_ENTER;

    for ( i = 0; (i < num_bytes) && (0 == status); i += write_len )
    {
        addr_len = 0;

        /* populate either one or two bytes of 'buf' depending on whether this is a DBA or SBA
         * EEPROM device */
        if ( two_byte_addressable )
        {
            buf[addr_len++] = (mem_addr >> 8) & 0xFF;
            buf[addr_len++] = (mem_addr >> 0) & 0xFF;
        }
        else
        {
            if ( mem_addr <= UINT8_MAX )
            {
                buf[addr_len++] = mem_addr & 0xFF;
            }
            else
            {
                /* single byte addressed ICs must have an address less than a single byte */
                debug_print("ERROR: single byte address MUST be less than %u\n", UINT8_MAX);
                status = -EINVAL;
            }
        }

        if ( status == 0 )
        {
            /* there are three different values that determine how much to write for a given
               transaction, let's find the minimum of:

               1. Remaining bytes in the 'buf' array after the memory address: MAX_NUM_I2C_DATA_BYTES_PER_TRANSACTION - addr_len
               2. Remaining bytes of the user provided buffer (p_data): num_bytes - i
               3. Alignment of a multi-byte write on the EEPROM page boundary: EEPROM_PAGE_SIZE - (mem_addr % EEPROM_PAGE_SIZE)
            */
            write_len = MIN(MAX_NUM_I2C_DATA_BYTES_PER_TRANSACTION - addr_len, num_bytes - i);
            write_len = MIN(write_len, EEPROM_PAGE_SIZE - (mem_addr % EEPROM_PAGE_SIZE));
            memcpy( &(buf[addr_len]), p_data, write_len );
            p_data += write_len;
            mem_addr += write_len;

            status = _write_i2c(card, bus, chip_addr, buf, addr_len + write_len);
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
#if (defined ATE_SUPPORT)
static int32_t
_enable_i2c_bus( uint8_t card,
                 uint8_t bus )
{
    int32_t status = 0;

    if ( ( bus == i2c_bus_1 ) &&
         ( _skiq_get_fmc_carrier( card ) == skiq_fmc_carrier_htg_k800 ) )
    {
        /* on HTG-K800 boards, open port 0 on the multiplexer at I2C address 0x70 */
        if ( !fmc_i2c_bus_accessible[card] )
        {
            uint8_t data = 0x01;

            debug_print("opening up port 0 on HTG-K800 mux for card %u\n", card);
            status = _write_i2c( card, bus, HTG_MUX_I2C_ADDR, &data, 1 );
            if ( status == 0 )
            {
                fmc_i2c_bus_accessible[card] = true;
            }
            debug_print("FMC I2C bus accessible for card %u: %s\n", card,
                        fmc_i2c_bus_accessible[card] ? "true" : "false");
        }
    }

    return ( status == 0 ) ? status : -EIO;
}
#else
static inline int32_t
_enable_i2c_bus( uint8_t card,
                 uint8_t bus )
{
    return 0;
}
#endif  /* ATE_SUPPORT */


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
int32_t
fpga_reg_hal_write_i2c_by_bus( uint8_t card,
                               uint8_t bus,
                               uint8_t chip_addr,
                               uint8_t data[],
                               uint8_t num_bytes )
{
    int32_t status = 0;

    TRACE_ENTER;

    status = _enable_i2c_bus( card, bus );
    if ( status == 0 )
    {
        /* special case: All I2C busses on Sidekiq Z2 get mapped to 0 */
        if ( _skiq_get_part( card ) == skiq_z2 )
        {
            bus = 0;
        }

        status = _write_i2c( card, bus, chip_addr, data, num_bytes );
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
int32_t
fpga_reg_hal_read_i2c_by_bus( uint8_t card,
                              uint8_t bus,
                              uint8_t chip_addr,
                              uint8_t data[],
                              uint8_t num_bytes )
{
    int32_t status;

    TRACE_ENTER;

    status = _enable_i2c_bus( card, bus );
    if ( status == 0 )
    {
        /* SPECIAL CASE: All I2C busses on Sidekiq Z2 get mapped to 0 */
        if ( _skiq_get_part( card ) == skiq_z2 )
        {
            bus = 0;
        }

        status = _read_i2c( card, bus, chip_addr, data, num_bytes );
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
int32_t
fpga_reg_hal_write_then_read_i2c_by_bus( uint8_t card,
                                         uint8_t bus,
                                         uint8_t periph_addr,
                                         uint8_t reg_addr,
                                         uint8_t data[],
                                         uint8_t num_bytes )
{
    int32_t status;

    TRACE_ENTER;

    status = fpga_reg_hal_write_i2c_by_bus(card, bus, periph_addr, &reg_addr, 1);
    if ( status == 0 )
    {
        status = fpga_reg_hal_read_i2c_by_bus(card, bus, periph_addr, data, num_bytes);
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
int32_t
fpga_reg_hal_write_i2c( uint8_t card,
                        uint8_t chip_addr,
                        uint8_t* p_data,
                        uint8_t num_bytes )
{
    int32_t status = 0;

    TRACE_ENTER;
    
    status = _write_i2c( card, i2c_bus_0, chip_addr, p_data, num_bytes );

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
int32_t
fpga_reg_hal_read_i2c( uint8_t card,
                       uint8_t chip_addr,
                       uint8_t* p_data,
                       uint8_t num_bytes )
{
    int32_t status = 0;

    TRACE_ENTER;
    
    status = _read_i2c( card, i2c_bus_0, chip_addr, p_data, num_bytes );

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
int32_t
fpga_reg_hal_write_then_read_i2c( uint8_t card,
                                  uint8_t periph_addr,
                                  uint8_t reg_addr,
                                  uint8_t* p_data,
                                  uint8_t num_bytes )
{
    int32_t status;

    TRACE_ENTER;

    status = fpga_reg_hal_write_i2c(card, periph_addr, &reg_addr, 1);
    if ( status == 0 )
    {
        status = fpga_reg_hal_read_i2c(card, periph_addr, p_data, num_bytes);
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
int32_t
fpga_reg_hal_prog_fpga_from_file( uint8_t card,
                                  FILE *fp)
{
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( card );

    TRACE_ENTER;

    if ( part == skiq_m2_2280 )
    {
        status = fpga_icap_prog_fpga_from_file( card, fp );
    }
    else
    {
        skiq_error("Unable to reprogram FPGA from a file using PCIe only on card %u\n", card);
        status = -ENOTSUP;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
int32_t
fpga_reg_hal_read_eeprom( uint8_t card,
                          uint16_t mem_addr,
                          uint8_t *p_data,
                          uint16_t num_bytes )
{
    int32_t status = 0;

    TRACE_ENTER;
    
    status = _read_eeprom( card, i2c_bus_0, EEPROM_I2C_ADDR, mem_addr, true /* DBA */, p_data,
                         num_bytes );

    TRACE_EXIT_STATUS(status);

    return status;
}


#if (defined ATE_SUPPORT)
/**************************************************************************************************/
int32_t
fpga_reg_hal_read_fmc_eeprom( uint8_t card,
                              uint16_t mem_addr,
                              uint8_t *p_data,
                              uint16_t num_bytes )
{
    int32_t status = 0, capacity;

    status = _enable_i2c_bus( card, i2c_bus_1 );
    if ( status == 0 )
    {
        capacity = card_fmc_eeprom_size( card );
        if ( capacity == 0 )
        {
            status = -ENOTSUP;
        }
        else if ( mem_addr >= capacity )
        {
            status = -ESPIPE;
        }
        else if ( ( num_bytes + mem_addr ) > capacity )
        {
            status = -ENOSPC;
        }
    }

    if ( status == 0 )
    {
        status = _read_eeprom( card, i2c_bus_1, FMC_EEPROM_I2C_ADDR, mem_addr,
                               card_fmc_eeprom_has_dba( card ), p_data, num_bytes );
    }

    return status;
}
#else
int32_t
fpga_reg_hal_read_fmc_eeprom( uint8_t card,
                              uint16_t mem_addr,
                              uint8_t *p_data,
                              uint16_t num_bytes )
{
    skiq_error("Support for reading FMC EEPROM not available in this library configuration\n");
    return -ENOTSUP;
}
#endif  /* _ATE_SUPPORT */


/**************************************************************************************************/
int32_t
fpga_reg_hal_write_eeprom( uint8_t card,
                           uint16_t mem_addr,
                           uint8_t *p_data,
                           uint16_t num_bytes )
{
    int32_t status = 0;

    TRACE_ENTER;

    status = _write_eeprom( card, i2c_bus_0, EEPROM_I2C_ADDR, mem_addr,
                                true /* DBA */, p_data, num_bytes );

    TRACE_EXIT_STATUS(status);

    return status;
}


#if (defined ATE_SUPPORT)
/**************************************************************************************************/
int32_t
fpga_reg_hal_write_fmc_eeprom( uint8_t card,
                               uint16_t mem_addr,
                               uint8_t *p_data,
                               uint16_t num_bytes )
{
    int32_t status = 0, capacity;

    status = _enable_i2c_bus( card, i2c_bus_1 );
    if ( status == 0 )
    {
        capacity = card_fmc_eeprom_size( card );
        if ( capacity == 0 )
        {
            status = -ENOTSUP;
        }
        else if ( mem_addr >= capacity )
        {
            status = -ESPIPE;
        }
        else if ( ( num_bytes + mem_addr ) > capacity )
        {
            status = -EFBIG;
        }
    }

    if ( status == 0 )
    {
        status = _write_eeprom( card, i2c_bus_1, FMC_EEPROM_I2C_ADDR, mem_addr,
                                card_fmc_eeprom_has_dba( card ), p_data, num_bytes );
    }

    return status;
}
#else
int32_t
fpga_reg_hal_write_fmc_eeprom( uint8_t card,
                               uint16_t mem_addr,
                               uint8_t *p_data,
                               uint16_t num_bytes )
{
    skiq_error("Support for writing FMC EEPROM not available in this library configuration\n");
    return -ENOTSUP;
}
#endif  /* defined ATE_SUPPORT */


/**************************************************************************************************/
int32_t fpga_reg_hal_read_fw_version(uint8_t card, uint8_t* p_ver_maj, uint8_t* p_ver_min)
{
    int32_t status = 0;
    uint16_t addr = FIRMWARE_VERSION_INFO_ADDR;
    uint8_t buf[2];

    TRACE_ENTER;

    status = fpga_reg_hal_read_eeprom( card, addr, buf, 2 );
    if ( status == 0 )
    {
        *p_ver_maj = buf[0];
        *p_ver_min = buf[1];

        if( (*p_ver_maj == 0xFF) && (*p_ver_min == 0xFF) )
        {
            status = -1;
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
hal_functions_t fpga_reg_hal = {
    .read_fw_version            = fpga_reg_hal_read_fw_version,
    .read_eeprom                = fpga_reg_hal_read_eeprom,
    .write_eeprom               = fpga_reg_hal_write_eeprom,
    .write_i2c                  = fpga_reg_hal_write_i2c,
    .read_i2c                   = fpga_reg_hal_read_i2c,
    .write_then_read_i2c        = fpga_reg_hal_write_then_read_i2c,
    .prog_fpga_from_file        = fpga_reg_hal_prog_fpga_from_file, /* just an error message */
    .read_i2c_by_bus            = fpga_reg_hal_read_i2c_by_bus,
    .write_i2c_by_bus           = fpga_reg_hal_write_i2c_by_bus,
    .write_then_read_i2c_by_bus = fpga_reg_hal_write_then_read_i2c_by_bus,
    .read_fmc_eeprom            = fpga_reg_hal_read_fmc_eeprom,
    .write_fmc_eeprom           = fpga_reg_hal_write_fmc_eeprom,
};
/**************************************************************************************************/
