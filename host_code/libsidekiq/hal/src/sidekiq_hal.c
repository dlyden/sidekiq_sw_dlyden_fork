/*! \file sidekiq_hal.c
 * \brief This file contains the hardware abstraction layer
 * interface for sidekiq.
 *
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 7/17/2013   MEZ   Created file
 *
 * </pre>
*/

/***** INCLUDES *****/

#include <sys/time.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <unistd.h>

#ifndef __MINGW32__
#include <sys/ioctl.h>
#endif /* __MINGW32__ */

#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "bit_ops.h"
#include "sidekiq_xport.h"

#include "fpga_reg_hal.h"
#include "zynq_hal.h"
#include "usb_hal.h"

/* enable debug_print and debug_print_plain when DEBUG_HAL is defined */
#if (defined DEBUG_HAL)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/**
   @brief Maximum number of attempts to check if the SPI controller indicates completion before
   considering it a failure
*/
#define MAX_RFIC_SPI_READY_CHECK_ATTEMPTS                   100

/**
   @brief Consecutive number of checks if the SPI controller indicates completion before delaying
   between checks (up to MAX_RFIC_SPI_READY_ATTEMPTS)
*/
#define CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP       MIN(10,MAX_RFIC_SPI_READY_CHECK_ATTEMPTS)

/**
   @brief Number of microseconds to delay between checks of the SPI controller status (after
   CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP checks have been reached)
*/
#define RFIC_SPI_READY_CHECK_DELAY_US                       1


#define DAC_SPI_CS (3) // X2/X4 only

#define HAL_INITIALIZER                                                 \
    {                                                                   \
        .read_fw_version        = fpga_reg_hal_read_fw_version,         \
        .read_eeprom            = fpga_reg_hal_read_eeprom,             \
        .write_eeprom           = fpga_reg_hal_write_eeprom,            \
        .write_i2c              = fpga_reg_hal_write_i2c,               \
        .read_i2c               = fpga_reg_hal_read_i2c,                \
        .prog_fpga_from_file    = fpga_reg_hal_prog_fpga_from_file,     \
        .read_i2c_by_bus        = NULL,                                 \
        .write_i2c_by_bus       = NULL,                                 \
        .read_fmc_eeprom        = NULL,                                 \
        .write_fmc_eeprom       = NULL,                                 \
    }

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL FUNCTIONS *****/


/***** LOCAL VARIABLES *****/

/* HAL function pointers for each card */
static ARRAY_WITH_DEFAULTS(hal_functions_t, hal_funcs, SKIQ_MAX_NUM_CARDS, HAL_INITIALIZER);

const uint8_t fpga_sync_word[FPGA_SYNC_WORD_LEN] = { 0xAA, 0x99, 0x55, 0x66 };


/***** LOCAL INITIALIZATIONS *****/

static void (*hal_critical_callback)(int32_t status, void* p_user_data) = NULL;
static void* hal_critical_callback_user_data = NULL;

static void hal_register_funcs( uint8_t card,
                                hal_functions_t *p_funcs )
{
    // TODO: validate functions?? (for required minimum support?)
    hal_funcs[card] = (hal_functions_t) {
        .read_fw_version            = p_funcs->read_fw_version,
        .read_eeprom                = p_funcs->read_eeprom,
        .write_eeprom               = p_funcs->write_eeprom,
        .write_i2c                  = p_funcs->write_i2c,
        .read_i2c                   = p_funcs->read_i2c,
        .write_then_read_i2c        = p_funcs->write_then_read_i2c,
        .prog_fpga_from_file        = p_funcs->prog_fpga_from_file,
        .read_i2c_by_bus            = p_funcs->read_i2c_by_bus,
        .write_i2c_by_bus           = p_funcs->write_i2c_by_bus,
        .write_then_read_i2c_by_bus = p_funcs->write_then_read_i2c_by_bus,
        .read_fmc_eeprom            = p_funcs->read_fmc_eeprom,
        .write_fmc_eeprom           = p_funcs->write_fmc_eeprom,
    };
}

void hal_select_functions_for_card( uint8_t card,
                                    skiq_xport_type_t type )
{
    switch (type)
    {
        case skiq_xport_type_pcie:
            hal_register_funcs( card, &fpga_reg_hal );
            break;

        case skiq_xport_type_usb:
            hal_register_funcs( card, &usb_hal );
            break;

        case skiq_xport_type_custom:
            hal_register_funcs( card, &fpga_reg_hal );
            break;

        case skiq_xport_type_net:
            hal_register_funcs( card, &fpga_reg_hal );
            break;
            
        default:
            skiq_error("Unsupported transport type (%u) for selecting HAL functions\n", type);
            break;
    }
}

void hal_select_functions_for_card_by_part( uint8_t card,
                                            skiq_part_t part )
{
    switch (part)
    {
    case skiq_z2:
    case skiq_z2p:
    case skiq_z3u:
        hal_register_funcs( card, &zynq_hal );
        break;
    default:
        break;
    }
}

void hal_reset_functions( uint8_t card )
{
    hal_select_functions_for_card( card, skiq_xport_type_pcie );
}


int32_t hal_write_i2c(uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes)
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].write_i2c != NULL )
    {
        status = hal_funcs[card].write_i2c( card, addr, p_data, num_bytes );
    }

    return(status);
}

int32_t hal_read_i2c(uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes)
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].read_i2c != NULL )
    {
        status = hal_funcs[card].read_i2c( card, addr, p_data, num_bytes );
    }

    return (status);
}

int32_t hal_write_then_read_i2c( uint8_t card,
                                 uint8_t periph_addr,
                                 uint8_t reg_addr,
                                 uint8_t* p_data,
                                 uint8_t num_bytes )
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].write_then_read_i2c != NULL )
    {
        status = hal_funcs[card].write_then_read_i2c( card, periph_addr, reg_addr, p_data, num_bytes );
    }

    return (status);
}


/**************************************************************************************************/
/** Read a 2 byte register 'reg' from an I2C device at 'addr' */
int32_t hal_read_2byte_i2c_reg( uint8_t card,
                                uint8_t addr,
                                uint8_t reg,
                                uint8_t data[2] )
{
    int32_t status = 0;

    status = hal_write_then_read_i2c( card, addr, reg, data, 2 );

    return status;
}

/**************************************************************************************************/
/** Read a 16-bit register from an I2C device that orders the received data as Big Endian or Little
    Endian depending on @a big_endian argument */
int32_t hal_read_16bit_i2c_reg( uint8_t card,
                                uint8_t addr,
                                uint8_t reg,
                                int16_t *p_data16,
                                bool big_endian )
{
    int32_t status = 0;
    uint8_t data[2];

    status = hal_read_2byte_i2c_reg( card, addr, reg, data );
    if ( status == 0 )
    {
        union
        {
            uint16_t u;
            int16_t i;
        } v;

        /* perform byte ordering only when interpreted (not typecast) as unsigned */
        if ( big_endian )
        {
            v.u = (data[0] << 8) | data[1];
        }
        else
        {
            v.u = (data[1] << 8) | data[0];
        }

        *p_data16 = v.i;
    }

    return status;
}


/**************************************************************************************************/
/** Read a 16-bit register from an I2C device that presents its data in little endian ordering */
int32_t hal_read_le16_i2c_reg( uint8_t card,
                               uint8_t addr,
                               uint8_t reg,
                               int16_t *p_data16 )
{
    return hal_read_16bit_i2c_reg( card, addr, reg, p_data16, false /* big_endian */ );
}


/**************************************************************************************************/
/** Read a 16-bit register from an I2C device that presents its data in big endian ordering */
int32_t hal_read_be16_i2c_reg( uint8_t card,
                               uint8_t addr,
                               uint8_t reg,
                               int16_t *p_data16 )
{
    return hal_read_16bit_i2c_reg( card, addr, reg, p_data16, true /* big_endian */ );
}


/**************************************************************************************************/
/** Read an unsigned 16-bit register from an I2C device that presents its data in big endian
 * ordering */
int32_t hal_read_beu16_i2c_reg( uint8_t card,
                                uint8_t addr,
                                uint8_t reg,
                                uint16_t *p_data16 )
{
    int32_t status = 0;
    uint8_t data[2];

    status = hal_read_2byte_i2c_reg( card, addr, reg, data );
    if ( status == 0 )
    {
        *p_data16 = (data[0] << 8) | data[1];
    }

    return status;
}


int32_t hal_read_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status = -ENOTSUP;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! READ ACCESS TO EEPROM IS DISABLED !!!!!");
    // need to add the correct number of bytes to the p_data buffer based on num_bytes
    int i;
    for (i = 0; i < num_bytes; i++) {
        p_data[i] = 0xFF;
    }
    status = 0;
#else
    if ( hal_funcs[card].read_eeprom != NULL )
    {
        status = hal_funcs[card].read_eeprom( card, addr, p_data, num_bytes );
    }
#endif
    return (status);
}

int32_t hal_write_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status = -ENOTSUP;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! WRITE ACCESS TO EEPROM IS DISABLED !!!!!");
    status = 0;
#else
    if ( hal_funcs[card].write_eeprom != NULL )
    {
        status = hal_funcs[card].write_eeprom( card, addr, p_data, num_bytes );
    }
#endif
    return (status);
}


int32_t hal_write_i2c_by_bus( uint8_t card,
                              uint8_t bus,
                              uint8_t addr,
                              uint8_t data[],
                              uint8_t num_bytes)
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].write_i2c_by_bus != NULL )
    {
        status = hal_funcs[card].write_i2c_by_bus( card, bus, addr, data, num_bytes );
    }

    return (status);
}


int32_t hal_read_i2c_by_bus( uint8_t card,
                             uint8_t bus,
                             uint8_t addr,
                             uint8_t data[],
                             uint8_t num_bytes )
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].read_i2c_by_bus != NULL )
    {
        status = hal_funcs[card].read_i2c_by_bus( card, bus, addr, data, num_bytes );
    }

    return (status);
}

int32_t hal_write_then_read_i2c_by_bus( uint8_t card,
                                        uint8_t bus,
                                        uint8_t periph_addr,
                                        uint8_t reg_addr,
                                        uint8_t data[],
                                        uint8_t num_bytes )
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].write_then_read_i2c_by_bus != NULL )
    {
        status = hal_funcs[card].write_then_read_i2c_by_bus( card, bus, periph_addr, reg_addr, data, num_bytes );
    }

    return (status);
}


#if (defined ATE_SUPPORT)
int32_t hal_read_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].read_fmc_eeprom != NULL )
    {
        status = hal_funcs[card].read_fmc_eeprom( card, addr, p_data, num_bytes );
    }

    return (status);
}


int32_t hal_write_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].write_fmc_eeprom != NULL )
    {
        status = hal_funcs[card].write_fmc_eeprom( card, addr, p_data, num_bytes );
    }

    return (status);
}
#else
int32_t hal_read_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes ) { return -ENOTSUP; }
int32_t hal_write_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes ) { return -ENOTSUP; }
#endif

int32_t hal_read_fw_ver(uint8_t card,
                        uint8_t* p_ver_maj,
                        uint8_t* p_ver_min)
{
    int32_t status = -ENOTSUP;

    if ( hal_funcs[card].read_fw_version != NULL )
    {
        status = hal_funcs[card].read_fw_version( card, p_ver_maj, p_ver_min );
    }

    return (status);
}


/**************************************************************************************************/
int32_t hal_rfic_write_reg( uint8_t card,
                            uint8_t cs,
                            uint16_t addr,
                            uint8_t data )
{
    uint32_t reg = 0;
    uint32_t spi_ready_attempts = 0;
    int32_t status = 0;

    debug_print("Attempt to write RFIC (card %" PRIu8 ", CS %" PRIu8 "), addr=0x%04x, "
                "data=0x%02x\n", card, cs, addr, data);

    RBF_SET(reg, cs, RFIC_SPI_CS);
    RBF_SET(reg, 1, RFIC_SPI_OP); /* write */
    RBF_SET(reg, addr, RFIC_SPI_ADDR);
    RBF_SET(reg, data, RFIC_SPI_WR_DATA);
    status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_RFIC_SPI_CMD, reg);

    while ( (spi_ready_attempts < MAX_RFIC_SPI_READY_CHECK_ATTEMPTS) && (status == 0) )
    {
        reg = 0;
        status = sidekiq_fpga_reg_read(card, FPGA_REG_RFIC_SPI_RD_DATA, &reg);
        if ( ( status == 0 ) && ( RBF_GET(reg, RFIC_SPI_DONE) != 0 ) )
        {
            break;
        }

        /* The idea with CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP is that nearly all of the
         * time, the SPI controller is done right away, so checking without delay helps complete the
         * transaction as soon as possible.  If the SPI transaction is taking a while, then delaying
         * between checks frees up host CPU resources. */
        spi_ready_attempts++;
        if ( spi_ready_attempts > CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP )
        {
            hal_microsleep(RFIC_SPI_READY_CHECK_DELAY_US);
        }
    }

    if ( ( status == 0 ) && ( spi_ready_attempts >= MAX_RFIC_SPI_READY_CHECK_ATTEMPTS ) )
    {
        skiq_warning("Failed to write RFIC reg address 0x%04x after %u attempts on card %" PRIu8
                     " and CS %" PRIu8 ", SPI transaction timed out\n", addr,
                     MAX_RFIC_SPI_READY_CHECK_ATTEMPTS, card, cs);
        status = -ETIMEDOUT;
    }

    if ( status == 0 )
    {
        debug_print("Successfully wrote RFIC (card %" PRIu8 ", CS %" PRIu8 "), addr=0x%04x, "
                    "data=0x%02x\n", card, cs, addr, data);
    }

    return status;
}


/**************************************************************************************************/
int32_t hal_rfic_read_reg( uint8_t card,
                           uint8_t cs,
                           uint16_t addr,
                           uint8_t* p_data)
{
    uint32_t reg = 0;
    uint32_t spi_ready_attempts = 0;
    int32_t status = 0;

    debug_print("Attempt to read RFIC (card %" PRIu8 ", CS %" PRIu8 "), addr=0x%04x\n", card, cs,
                addr);

    RBF_SET(reg, cs, RFIC_SPI_CS);
    RBF_SET(reg, 0, RFIC_SPI_OP); /* read */
    RBF_SET(reg, addr, RFIC_SPI_ADDR);
    status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_RFIC_SPI_CMD, reg);

    while ( (spi_ready_attempts < MAX_RFIC_SPI_READY_CHECK_ATTEMPTS) && (status == 0) )
    {
        reg = 0;
        status = sidekiq_fpga_reg_read(card, FPGA_REG_RFIC_SPI_RD_DATA, &reg);
        if ( ( status == 0 ) && ( RBF_GET(reg, RFIC_SPI_DONE) != 0 ) )
        {
            break;
        }

        /* The idea with CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP is that nearly all of the
         * time, the SPI controller is done right away, so checking without delay helps complete the
         * transaction as soon as possible.  If the SPI transaction is taking a while, then delaying
         * between checks frees up host CPU resources. */
        spi_ready_attempts++;
        if ( spi_ready_attempts > CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP )
        {
             hal_microsleep(RFIC_SPI_READY_CHECK_DELAY_US);
        }
    }

    if ( ( status == 0 ) && ( spi_ready_attempts >= MAX_RFIC_SPI_READY_CHECK_ATTEMPTS ) )
    {
        skiq_warning("Failed to read RFIC reg address 0x%04x after %u attempts on card %" PRIu8
                     " and CS %" PRIu8 ", SPI transaction timed out\n", addr,
                     MAX_RFIC_SPI_READY_CHECK_ATTEMPTS, card, cs);
        status = -ETIMEDOUT;
    }

    if ( status == 0 )
    {
        *p_data = RBF_GET(reg, RFIC_SPI_RD_DATA);
        debug_print("Successfully read RFIC (card %" PRIu8 ", CS %" PRIu8 "), addr=0x%04x, "
                    "data=0x%02x\n", card, cs, addr, *p_data);
    }

    return status;
}


/**************************************************************************************************/
int32_t hal_spi_dac_write( uint8_t card,
                           uint16_t data )
{
    uint32_t reg = 0;
    uint32_t spi_ready_attempts = 0;
    int32_t status = 0;

    RBF_SET(reg, DAC_SPI_CS, RFIC_SPI_CS);
    RBF_SET(reg, 1, RFIC_SPI_OP); /* write */
    /* Note: this is a bit goofy...checkout datasheet on AD5660, but the 16-bit value to write is in
       the lower 16 bits, and the upper 8 bits of the transaction should be all 0.  This spans the
       "normal" addr/data fields so we need to make sure that the correct part of data falls in to
       the proper fields of the SPI transaction */
    RBF_SET(reg, (data>>8), RFIC_SPI_ADDR);
    RBF_SET(reg, (data), RFIC_SPI_WR_DATA);
    status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_RFIC_SPI_CMD, reg);

    while ( (spi_ready_attempts < MAX_RFIC_SPI_READY_CHECK_ATTEMPTS) && (status == 0) )
    {
        reg = 0;
        status = sidekiq_fpga_reg_read(card, FPGA_REG_RFIC_SPI_RD_DATA, &reg);
        if ( ( status == 0 ) && ( RBF_GET(reg, RFIC_SPI_DONE) != 0 ) )
        {
            break;
        }

        /* The idea with CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP is that nearly all of the
         * time, the SPI controller is done right away, so checking without delay helps complete the
         * transaction as soon as possible.  If the SPI transaction is taking a while, then delaying
         * between checks frees up host CPU resources. */
        spi_ready_attempts++;
        if ( spi_ready_attempts > CONSECUTIVE_RFIC_SPI_READY_CHECK_BEFORE_SLEEP )
        {
            hal_microsleep(RFIC_SPI_READY_CHECK_DELAY_US);
        }
    }

    if ( ( status == 0 ) && ( spi_ready_attempts >= MAX_RFIC_SPI_READY_CHECK_ATTEMPTS ) )
    {
        skiq_warning("Failed to write DAC SPI device after %u attempts on card %" PRIu8 " and CS %"
                     PRIu8 ", SPI transaction timed out\n", MAX_RFIC_SPI_READY_CHECK_ATTEMPTS, card,
                     DAC_SPI_CS);
        status = -ETIMEDOUT;
    }

    return status;
}


void hal_critical_exit( int32_t status )
{
    skiq_error("A critical error was encountered, undefined behavior will occur if continued!\n");
    if( hal_critical_callback == NULL )
    {
        _exit(status);
    }
    else
    {
        hal_critical_callback(status, hal_critical_callback_user_data);
    }
}

void hal_register_critical_error_cb( void (*critical_cb)(int32_t status, void* p_user_data),
                                     void* p_user_data )
{
    hal_critical_callback = critical_cb;
    hal_critical_callback_user_data = p_user_data;
}

int32_t hal_prog_fpga_from_file(uint8_t card, FILE *fp)
{
    int32_t status = -ENOTSUP;

    if( hal_funcs[card].prog_fpga_from_file != NULL )
    {
        status = hal_funcs[card].prog_fpga_from_file( card, fp );
    }

    if (-ENOTSUP == status)
    {
        errno = ENOTSUP;
    }

    return (status);
}
