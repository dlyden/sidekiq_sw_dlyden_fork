/**
 * @file   io_m2_2280.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Apr 17 13:13:07 CDT 2019
 *
 * @brief This file captures I/O functionality for the Sidekiq M.2-2280 that is not implemented in
 * rfe_m2_2280_ab.c.  For example, access to the EEPROM's write protection line is provided by
 * functions implemented here.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <errno.h>
#include <inttypes.h>

#include "io_m2_2280.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_private.h"
#include "pll_adf4002.h"
#include "gpsdo_fpga.h"         /* to enable/disable GPSDO depending on reference clock source */


/***** DEFINES *****/

#define FREQ_10MHZ                      ((uint32_t)10e6)
#define FREQ_30p72MHZ                   ((uint32_t)30.72e6)
#define FREQ_40MHZ                      ((uint32_t)40e6)


/***** MACROS *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t io_disable_eeprom_wp( uint8_t card )
{
    int32_t status = 0;

    /**
       @note EEPROM_WP_N in FPGA design is output-only
    */
    status = sidekiq_fpga_reg_RMWV( card, 1, EEPROM_WP_N, FPGA_REG_GPIO_WRITE);

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t io_enable_eeprom_wp( uint8_t card )
{
    int32_t status = 0;

    /**
       @note EEPROM_WP_N in FPGA design is output-only
    */
    status = sidekiq_fpga_reg_RMWV( card, 0, EEPROM_WP_N, FPGA_REG_GPIO_WRITE);

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t m2_2280_io_set_int_ref_clock_source( uint8_t card )
{
    int32_t status;
    uint32_t value = 0;

    status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
    if ( status == 0 )
    {
        /**
           @note REF_EXT_40M, REF_EXT_10M, and REF_INT_MEMS in FPGA design are output-only GPIO
        */

        RBF_SET( value, 0, REF_EXT_40M );
        RBF_SET( value, 0, REF_EXT_10M );
        RBF_SET( value, 1, REF_INT_MEMS );

        status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPIO_WRITE, value );
    }

    /* configure the on-board ADF4002 PLL to 1x when using the internal reference clock */
    if ( status == 0 )
    {
        status = pll_adf4002_configure( card, pll_adf4002_setting_40MHz );
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t m2_2280_io_set_ext_ref_clock_source( uint8_t card,
                                             uint32_t ref_clk_freq )
{
    int32_t status = 0;
    uint32_t value = 0;

    status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
    if ( status == 0 )
    {
        /**
           @note REF_EXT_40M, REF_EXT_10M, and REF_INT_MEMS in FPGA design are output-only GPIO
        */

        RBF_SET( value, 0, REF_INT_MEMS );

        switch (ref_clk_freq)
        {
            case FREQ_30p72MHZ:
            case FREQ_40MHZ:
                /* weird, I know, but REF_EXT_40M really means just take the external reference
                 * unconditionally (no PLL), so both 40 and 30.72 MHz can be accepted */
                RBF_SET( value, 1, REF_EXT_40M );
                RBF_SET( value, 0, REF_EXT_10M );
                break;

            case FREQ_10MHZ:
                RBF_SET( value, 0, REF_EXT_40M );
                RBF_SET( value, 1, REF_EXT_10M );
                break;

            default:
                /* unsupported external reference frequency */
                status = -EINVAL;
                break;
        }
    }

    /* commit the REF selection */
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPIO_WRITE, value );
    }

    if ( status == 0 )
    {
        /* configure the on-board ADF4002 PLL to 4x or leave it be depending on requested reference
         * clock frequency */
        if ( ref_clk_freq == FREQ_10MHZ )
        {
            status = pll_adf4002_configure( card, pll_adf4002_setting_10MHz );
        }
        else
        {
            /* ADF4002 is powered off in hardware (when REF_EXT_40M = 1, p3 of the rev A or rev B
             * Stretch schematic) when using external reference clock at 40MHz or 30.72MHz */
        }
    }

    /* disable the GPSDO (if available) */
    if ( status == 0 )
    {
        status = gpsdo_fpga_disable( card );
        if ( ( status == -ENOSYS ) || ( status == -ENOTSUP ) )
        {
            /* not having the GPSDO is not a crime, overwrite `status` when the feature is not
             * implemented */
             status = 0;
        }
    }

    if ( ( status != 0 ) && ( status != -EINVAL ) )
    {
        status = -EBADMSG;
    }

    return status;
}
