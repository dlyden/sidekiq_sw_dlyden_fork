/**
    @file   zynq_hal.c
    @author <info@epiqsolutions.com>
    @date   Tue May 28 17:08:15 2019

    @brief  Zynq HAL routines.

    <pre>
    Copyright 2019 Epiq Solutions, All Rights Reserved
    </pre>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>

#include "sidekiq_hal.h"
#include "sidekiq_private.h"
#include "zynq_hal.h"
#include "fpga_reg_hal.h"
#include "card_services.h"

#include "sidekiq_xport.h"

#if (defined DEBUG_HAL_ZYNQ)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/**
    @brief  The path where FPGA bitstreams must be stored in order for the FPGA Manager to
            load them.
*/
#ifndef HAL_ZYNQ_FIRMWARE_PATH
#   define HAL_ZYNQ_FIRMWARE_PATH     "/lib/firmware"
#endif

/**
    @brief  The filename of the temporary bitstream used to program the FPGA.
*/
#ifndef HAL_ZYNQ_TEMP_BITSTREAM_FILENAME
#   define HAL_ZYNQ_TEMP_BITSTREAM_FILENAME     "zynq_sdr_tmpbitstream.bit"
#endif

/**
    @brief  The full pathname of the temporary bitstream used to program the FPGA.
*/
#ifndef HAL_ZYNQ_TEMP_BITSTREAM_FULLPATH
#   define HAL_ZYNQ_TEMP_BITSTREAM_FULLPATH       HAL_ZYNQ_FIRMWARE_PATH "/" HAL_ZYNQ_TEMP_BITSTREAM_FILENAME
#endif

/** @brief  The base path to the FPGA Manager's FPGA0 sysfs entry. */
#ifndef HAL_ZYNQ_FPGA_MANAGER_PATH
#   define HAL_ZYNQ_FPGA_MANAGER_PATH             "/sys/class/fpga_manager/fpga0"
#endif

/** @brief  The filename used to set FPGA0's programming flags. */
#ifndef HAL_ZYNQ_FPGA_MANAGER_FLAGS_PATH
#   define HAL_ZYNQ_FPGA_MANAGER_FLAGS_PATH       HAL_ZYNQ_FPGA_MANAGER_PATH "/flags"
#endif

/**
    @brief  The filename used to specify which bitstream file in @a HAL_ZYNQ_FIRMWARE_PATH
            should be programmed on FPGA0.
*/
#ifndef HAL_ZYNQ_FPGA_MANAGER_FIRMWARE_PATH
#   define HAL_ZYNQ_FPGA_MANAGER_FIRMWARE_PATH    HAL_ZYNQ_FPGA_MANAGER_PATH "/firmware"
#endif

/**
    @brief  The filename used to get the current operating state of FPGA0.
*/
#ifndef HAL_ZYNQ_FPGA_MANAGER_STATE_PATH
#   define HAL_ZYNQ_FPGA_MANAGER_STATE_PATH       HAL_ZYNQ_FPGA_MANAGER_PATH "/state"
#endif

/**
    @brief  The filename used to program the FPGA using the devcfg method.
*/
#ifndef HAL_ZYNQ_DEVCFG_FIRMWARE_PATH
#   define HAL_ZYNQ_DEVCFG_FIRMWARE_PATH          "/dev/xdevcfg"
#endif

/**
    @brief  The filename used to get the programming state of the FPGA using the devcfg method.
*/
#ifndef HAL_ZYNQ_DEVCFG_STATE_PATH
#   define HAL_ZYNQ_DEVCFG_STATE_PATH             "/sys/class/xdevcfg/xdevcfg/device/prog_done"
#endif

/**
    @brief  The path of the configfs device tree overlay entry
*/
#ifndef HAL_ZYNQ_CONFIGFS_DT_OVERLAY_PATH
#   define HAL_ZYNQ_CONFIGFS_DT_OVERLAY_PATH      "/configfs/device-tree/overlays"
#endif

/** @brief Script that supports removing / installing PL DT overlay */
#ifndef HAL_ZYNQ_PL_DT_OVERLAY
#   define HAL_ZYNQ_PL_DT_OVERLAY                 "/usr/sbin/pl_dt_overlay"
#endif

/** @brief Command to remove overlay */
#ifndef HAL_ZYNQ_PL_DT_OVERLAY_REMOVE
#   define HAL_ZYNQ_PL_DT_OVERLAY_REMOVE          HAL_ZYNQ_PL_DT_OVERLAY " remove"
#endif

/** @brief Command to install overlay */
#ifndef HAL_ZYNQ_PL_DT_OVERLAY_INSTALL
#   define HAL_ZYNQ_PL_DT_OVERLAY_INSTALL         HAL_ZYNQ_PL_DT_OVERLAY " install"
#endif

/** @brief Command to install overlay */
#ifndef HAL_ZYNQ_PL_DT_OVERLAY_VERIFY
#   define HAL_ZYNQ_PL_DT_OVERLAY_VERIFY         HAL_ZYNQ_PL_DT_OVERLAY " verify"
#endif

/**
    @brief  The number of times to check FPGA0's operating state register for a success message
            before giving up.

    The FPGA Manager tracks the FPGA's operating state in the @a HAL_ZYNQ_FPGA_MANAGER_STATE_PATH
    file. After notifying the FPGA Manager of the filename to program the FPGA with, it is good
    practice to check the operating state register to determine if the procedure succeeded;
    in general, if the contents of the operating state register reads "operating" then the
    reprogram was successful and the new bitstream should be running.
*/
#ifndef HAL_ZYNQ_REPROGRAM_NUM_STATUS_RETRIES
#   define HAL_ZYNQ_REPROGRAM_NUM_STATUS_RETRIES  (10)
#endif

/**
    @brief  The flag value set in HAL_ZYNQ_FPGA_MANAGER_FLAGS_PATH to indicate that the
            bitstream being programmed is a full bitstream and not a partial.
*/
#define HAL_ZYNQ_FPGA_MANAGER_FLAG_FULLBITSTREAM   '0'




/**
    @brief  Check for the presence of the FPGA Manager on the system

    @param[out]  p_present      If not NULL, true if the FPGA Manager exists on the system else
                                false

    FPGA Manager creates some sysfs entries on startup; it is assumed that if these files
    exist then the FPGA Manager is loaded & running on the system.

    This method of reprogramming the FPGA is used by v3.0.0 and greater of the Z2 BSP.

    @return 0 on success, else a negative errno.
*/
static int32_t
is_fpga_manager_present(bool *p_present)
{
    const char *fpgaManagerFwPath = HAL_ZYNQ_FPGA_MANAGER_FIRMWARE_PATH;

    return hal_file_is_present(fpgaManagerFwPath, p_present);
}

/**
    @brief  Check for the presence of the Xilinx devcfg programming method on the system

    @param[out]  p_present      If not NULL, true if the devcfg method exists on the system else
                                false

    The Xilinx devcfg method of programming has an entry in /dev; it is assumed that if this
    file exists then the devcfg method of reprogramming is available.

    This method of reprogramming the FPGA is used by v2.0.0 and prior of the Z2 BSP.

    @return 0 on success, else a negative errno.
*/
static int32_t
is_devcfg_present(bool *p_present)
{
    const char *devcfgFwPath = HAL_ZYNQ_DEVCFG_FIRMWARE_PATH;

    return hal_file_is_present(devcfgFwPath, p_present);
}

/**
    @brief  Check for the presence of the configfs device tree overlay

    @param[out]  p_present      If not NULL, true if the configfs device tree overlay present

    The configs device tree overlay has an entry at /configfs/device-tree/overlays; it is assumed
    that if this file exists then the device tree overlay for reloading the FPGA is available

    This method is available in v3.2.0 of the Z2 BSP.

    @return 0 on success, else a negative errno.
*/
static int32_t
is_configfs_dt_overlay_present(bool *p_present)
{
    const char *configfs_dt_overlay = HAL_ZYNQ_CONFIGFS_DT_OVERLAY_PATH;

    return hal_file_is_present(configfs_dt_overlay, p_present);
}

/**
    @brief  Reprogram the FPGA using the Xilinx devcfg method

    This method is used in v2.0.0 and prior of the Z2 BSP.

    @retval 0               on success
    @retval -EBADF          if the temporary bitstream file couldn't be created
    @retval -EFAULT         if the FPGA reported that the supplied bitstream is not valid
                            or in an invalid format
    @return -EIO            if the FPGA reported a failure to reprogram
    @return -ETIMEDOUT      if the function timed out waiting for the FPGA to report that
                            it was successfully reprogrammed
*/
static int32_t
devcfg_reprogram_fpga(uint8_t card, FILE *fp)
{
#define BUFFER_LEN  (32)
    const char *devcfgName = HAL_ZYNQ_DEVCFG_FIRMWARE_PATH;
    int32_t status = 0;
    int result = 0;
    uint32_t retries = HAL_ZYNQ_REPROGRAM_NUM_STATUS_RETRIES;
    int tempFd = -1;
    int userFd = -1;
    bool valid = false;

    char buffer[BUFFER_LEN] = { 0 };
    ssize_t numRead = 0;

    userFd = fileno(fp);

    debug_print("Attempting to reprogram using devcfg...\n");

    /* Open the devcfg device for writing to. */
    debug_print("Opening devcfg device '%s'...\n", devCfgName);
    errno = 0;
    tempFd = open(devcfgName, O_WRONLY);
    if (-1 == tempFd)
    {
        skiq_warning("Failed to open FPGA programming device (errno %d)\n",
                errno);
        status = -EBADF;
    }

    /* Copy the user-provided FILE contents to the devcfg device. */
    if (0 == status)
    {
        debug_print("Copying user file contents to devcfg device...\n");
        status = hal_file_copy(userFd, tempFd);
        if (0 != status)
        {
            skiq_warning("Failed to write bitstream to FPGA (errno %d)\n", status);
        }
    }

    /* Close the temp file and mark it closed */
    if (-1 != tempFd)
    {
        close(tempFd);
        tempFd = -1;
    }

    if (0 == status)
    {
        debug_print("Waiting for FPGA to finish programming...\n");

        while ((0 == status) && (0 < retries) && (!valid))
        {
            errno = 0;
            tempFd = open(HAL_ZYNQ_DEVCFG_STATE_PATH, O_RDONLY);
            if (-1 == tempFd)
            {
                skiq_warning("Failed to get programming status from FPGA (errno %d)\n",
                        errno);
            }
            else
            {
                debug_print("Reading in programming state (retries = %" PRIu32 ")\n",
                    retries);

                result = hal_file_read_line(tempFd, &(buffer[0]), BUFFER_LEN, &numRead);
                if (0 != result)
                {
                    debug_print("Failed to read programming status (result = %d)\n",
                        result);
                }
                else
                {
                    debug_print("Read in programming state as '%s'\n",
                        buffer);

                    if (0 == strncmp(buffer, "1", numRead))
                    {
                        valid = true;
                    }
                }
            }

            if (-1 != tempFd)
            {
                close(tempFd);
                tempFd = -1;
            }

            if ((0 == status) && (!valid))
            {
                (void) hal_microsleep(500);
                retries--;
            }
        }
    }

    if ((!valid) && (0 == retries))
    {
        skiq_warning("Timed out waiting for FPGA programming to report success!\n");
        status = -ETIMEDOUT;
    }

    return (status);
#undef BUFFER_LEN
}

/**
    @brief  Reprogram the FPGA using the FPGA Manager

    This method is used in the Z2 v3.0.0 BSP and greater

    @retval 0               on success
    @retval -EBADF          if the temporary bitstream file couldn't be created
    @retval -EFAULT         if the FPGA reported that the supplied bitstream is not valid
                            or in an invalid format
    @return -EIO            if the FPGA reported a failure to reprogram
    @return -ETIMEDOUT      if the function timed out waiting for the FPGA to report that
                            it was successfully reprogrammed
*/
static int32_t
fpga_manager_reprogram_fpga(uint8_t card, FILE *fp)
{
#define BUFFER_LEN  (32)
    const char *tempBitstreamName = HAL_ZYNQ_TEMP_BITSTREAM_FULLPATH;
    int32_t status = 0;
    int32_t result = 0;
    uint32_t retries = HAL_ZYNQ_REPROGRAM_NUM_STATUS_RETRIES;
    bool valid = false;
    int tempFd = -1;
    int userFd = -1;

    char buffer[BUFFER_LEN] = { 0 };
    uint32_t bufferLen = 0;
    ssize_t numRead = 0;

    userFd = fileno(fp);

    debug_print("Attempting to reprogram using the FPGA Manager...\n");

    /* Remove the temporary bitstream if it already exists. */
    if (0 == status)
    {
        debug_print("Removing any existing temporary bitstream file"
            " ('%s')...\n", tempBitstreamName);

        result = hal_file_remove(tempBitstreamName);
        if (0 != result)
        {
            skiq_warning("Failed to remove any temporary bitstream files"
                " (result = %" PRIi32 "); continuing...\n", result);
        }
    }

    /* Create the temporary bitstream file. */
    if (0 == status)
    {
        debug_print("Creating temporary bitstream file...\n");

        errno = 0;
        tempFd = open(tempBitstreamName, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
        if (-1 == tempFd)
        {
            skiq_warning("Failed to create temporary bitstream file '%s' (errno %d)\n",
                tempBitstreamName, errno);
            status = -EBADF;
        }
    }

    /* Copy the user-provided FILE contents to the temp file. */
    if (0 == status)
    {
        debug_print("Copying user file contents to temp file...\n");

        status = hal_file_copy(userFd, tempFd);
        if (0 != status)
        {
            skiq_warning("Failed to write to temporary bitstream file (errno %d)\n",
                         status);
        }
        else
        {
#if (!defined __MINGW32__)
            /* Attempt to commit the temp bitstream file fully to disk before using it. */
            (void) fsync(tempFd);
#endif
        }
    }

    /* Close the temp file and mark it closed */
    if (-1 != tempFd)
    {
        close(tempFd);
        tempFd = -1;
    }

    /* Set the FPGA Manager flags to indicate that the bitstream is full, not partial. */
    if (0 == status)
    {
        buffer[0] = HAL_ZYNQ_FPGA_MANAGER_FLAG_FULLBITSTREAM;
        buffer[1] = '\0';
        bufferLen = 2;

        status = hal_file_write_string(HAL_ZYNQ_FPGA_MANAGER_FLAGS_PATH, O_WRONLY,
                    &(buffer[0]), bufferLen);
        if (0 != status)
        {
            debug_print("Failed to write to FPGA Manager flags file '%s'"
                " (status = %" PRIi32 "); ignoring and hoping things work.\n",
                HAL_ZYNQ_FPGA_MANAGER_FLAGS_PATH, status);
            status = 0;
        }
    }

    /* Write the temporary bitstream file name to the FPGA Manager's file name device. */
    if (0 == status)
    {
        debug_print("Writing bitstream filename '%s' to FPGA Manager...\n",
            HAL_ZYNQ_TEMP_BITSTREAM_FILENAME);

        status = hal_file_write_string(HAL_ZYNQ_FPGA_MANAGER_FIRMWARE_PATH, O_WRONLY,
                    HAL_ZYNQ_TEMP_BITSTREAM_FILENAME,
                    strlen(HAL_ZYNQ_TEMP_BITSTREAM_FILENAME));
        if (0 != status)
        {
            switch (status)
            {
            case -EIO:
                skiq_warning("Failed to reprogram FPGA (%" PRIi32 ": invalid bitstream"
                    " - no sync word found, is this in .bin format?)\n", status);
                status = -EFAULT;
                break;
            default:
                skiq_warning("Failed to reprogram FPGA (%" PRIi32 ")\n", status);
                status = -EIO;
                break;
            }
        }
    }

    /* Wait for the FPGA to finish programming. */
    if (0 == status)
    {
        debug_print("Waiting for FPGA to finish programming...\n");

        while ((0 == status) && (0 < retries) && (!valid))
        {
            errno = 0;
            tempFd = open(HAL_ZYNQ_FPGA_MANAGER_STATE_PATH, O_RDONLY);
            if (-1 == tempFd)
            {
                skiq_warning("Failed to get status from Linux FPGA Manager (%" PRIi32 ")\n",
                    errno);
            }
            else
            {
                debug_print("Reading in FPGA Manager programming state (retries = %"
                    PRIu32 ")\n", retries);

                result = hal_file_read_line(tempFd, &(buffer[0]), BUFFER_LEN, &numRead);
                if (0 != result)
                {
                    debug_print("Failed to read FPGA Manager programming status"
                        " (result = %" PRIi32 ")\n", result);
                }
                else
                {
                    debug_print("Read in FPGA Manager programming status as '%s'\n",
                        readBuffer);

                    if (0 == strncmp(buffer, "operating", numRead))
                    {
                        valid = true;
                    }
                    else if (0 == strncmp(buffer, "write error", numRead))
                    {
                        skiq_warning("Linux FPGA Manager indicates that writing the bitstream"
                            " failed\n");
                        status = -EIO;
                    }
                }
            }

            if (-1 != tempFd)
            {
                close(tempFd);
                tempFd = -1;
            }

            if ((0 == status) && (!valid))
            {
                (void) hal_microsleep(500);
                retries--;
            }
        }
    }

    if ((!valid) && (0 == retries))
    {
        skiq_warning("Timed out waiting for Linux FPGA Manager to report reprogramming"
            " success!\n");
        status = -ETIMEDOUT;
    }

    debug_print("Cleaning up...\n");

    /* Remove the temporary bitstream file (if it exists). */
    errno = 0;
    result = (int32_t) hal_file_remove(tempBitstreamName);
    if (0 != result)
    {
        skiq_warning("Failed to remove temporary bitstream file '%s'"
            " (errno %d)\n", tempBitstreamName, errno);
    }
    else
    {
        debug_print("Removed temporary bitstream file '%s'\n", tempBitstreamName);
    }

    return (status);
#undef BUFFER_LEN
}



/**************************************************************************************************/
/**
    @note   The FPGA Manager requires all bitstreams to be stored within a specified directory
            (@a HAL_ZYNQ_FIRMWARE_PATH) before they can be programmed. As there's no standard
            way to get a full pathname from a FILE *, the most straightforward way is to write
            a copy of the user provided bitstream from the FILE * into a temporary bitstream file
            in the specified directory. After attempting the programming process, the temporary
            bitstream file is removed.

    @retval 0           on successful reprogramming
    @retval -EINVAL     if the FILE * is invalid
    @retval -ENOTSUP    if reprogramming isn't supported on this Z2 (e.g. the FPGA Manager
                        doesn't exist)
    @retval -EIO        if one of the following failures occurred:
                        - the FPGA Manager check failed or
                        - file (input from user or output to temp file) I/O failed or
                        - a failure happened while reprogramming the FPGA
    @retval -EFAULT     if the specified bitfile is the wrong format (e.g. doesn't have a
                        sync word or isn't a .bin file)
    @retval -ETIMEDOUT  if the FPGA Manager didn't indicate that the FPGA was programmed
                        successfully in a certain amount of time
*/
int32_t
zynq_hal_prog_fpga_from_file( uint8_t card,
                              FILE *fp )
{
    enum {
        reprogram_devcfg = 0,
        reprogram_fpgamanager,
        reprogram_none
    } reprogramMethod = reprogram_none;

    int32_t status = 0;
    bool valid = false;
    bool present = false;
    bool configfs_dt_overlay = false;
    skiq_xport_init_level_t currentInitLevel = skiq_xport_init_level_unknown;

    debug_print("Starting Z2 reprogramming...\n");

    /* Verify that the arguments aren't invalid. */
    if (NULL == fp)
    {
        skiq_warning("Invalid file pointer provided!\n");
        return -EINVAL;
    }

    /* Verify the provided FILE * points to a regular file or symlink before using it. */
    if (0 == status)
    {
        debug_print("Validating user file pointer...\n");

        status = hal_file_is_file_or_link(fp, &valid);
        if ((0 != status) || (!valid))
        {
            skiq_warning("The provided bitstream does not appear to exist or is not a"
                " regular file\n");
            return -EBADF;
        }
    }

    currentInitLevel = skiq_xport_card_get_level( card );

    /* Choose the correct reprogramming method. */
    status = is_devcfg_present(&present);
    if ((0 == status) && (present))
    {
        debug_print("Devcfg reprogramming method available!\n");
        reprogramMethod = reprogram_devcfg;
    }

    /* Verify that the FPGA Manager is present. */
    status = is_fpga_manager_present(&present);
    if ((0 == status) && (present))
    {
        debug_print("Linux FPGA Manager reprogramming method available!\n");
        reprogramMethod = reprogram_fpgamanager;
    }

    if ((reprogram_devcfg == reprogramMethod) ||
        (reprogram_fpgamanager == reprogramMethod))
    {
        if( status == 0 )
        {
            debug_print("Shutting down AXI card...\n");
            status = skiq_xport_card_exit( card );

            // if the device tree overlay is present, remove the directory
            status = is_configfs_dt_overlay_present(&configfs_dt_overlay);
            if( (status==0) && (configfs_dt_overlay) )
            {
                int32_t remove_status=0;
                if( (remove_status = system(HAL_ZYNQ_PL_DT_OVERLAY_REMOVE)) != 0 )
                {
                    skiq_error("Error when trying remove PL overlay prior to reprogramming (status=%d) (card=%u)\n",
                               remove_status, card);
                    status = remove_status;
                }
            }
            else
            {
                skiq_warning("Out of date Z2 BSP detected, transmit will not function after reprogramming.  Please update BSP to ensure proper operation.");
            }
        }
    }

    /* rewind the FILE pointer like the other HAL implementations to maintain consistency */
    rewind(fp);

    if( status == 0 )
    {
        switch (reprogramMethod)
        {
            case reprogram_devcfg:
                status = devcfg_reprogram_fpga(card, fp);
                break;
            case reprogram_fpgamanager:
                status = fpga_manager_reprogram_fpga(card, fp);
                break;
            case reprogram_none:
            default:
                skiq_warning("Cannot determine correct reprogramming method\n");
                status = -ENOTSUP;
                break;
        }
        if( status == 0 )
        {
            if( configfs_dt_overlay )
            {
                status = system( HAL_ZYNQ_PL_DT_OVERLAY_INSTALL );
                if( status != 0 )
                {
                    skiq_error("Error when trying to install PL overlay after to reprogramming (status=%d) (card=%u)\n",
                               status, card);
                }
                else
                {
                    status = system( HAL_ZYNQ_PL_DT_OVERLAY_VERIFY );
                    if( status != 0 )
                    {
                        skiq_error("Error when verifying successful PL overlay after reprogramming (status=%d) (card=%u)\n",
                                   status, card);
                    }
                }
            }
        }
    }

    if (0 == status)
    {
        debug_print("Re-initializing AXI card %u...\n", card);
        status = skiq_xport_card_init(skiq_xport_type_custom, currentInitLevel, card);
        if (0 != status)
        {
            skiq_warning("AXI card did not re-initialize properly, a host reboot may be required to"
                " restore normal card operation (status = %" PRIi32 ")\n", status);
        }
    }

    return (status);
}



/**************************************************************************************************/
hal_functions_t zynq_hal = {
    .read_fw_version = fpga_reg_hal_read_fw_version,
    .read_eeprom = fpga_reg_hal_read_eeprom,
    .write_eeprom = fpga_reg_hal_write_eeprom,
    .write_i2c = fpga_reg_hal_write_i2c,
    .read_i2c = fpga_reg_hal_read_i2c,
    .write_then_read_i2c = fpga_reg_hal_write_then_read_i2c,
    .prog_fpga_from_file = zynq_hal_prog_fpga_from_file,
    .read_i2c_by_bus = fpga_reg_hal_read_i2c_by_bus,
    .write_i2c_by_bus = fpga_reg_hal_write_i2c_by_bus,
    .write_then_read_i2c_by_bus = fpga_reg_hal_write_then_read_i2c_by_bus,
    .read_fmc_eeprom = NULL,
    .write_fmc_eeprom = NULL,
};

/**************************************************************************************************/

