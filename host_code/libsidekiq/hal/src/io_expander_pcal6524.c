/**
 * @file   io_expander_pcal6524.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Apr 18 10:15:09 CDT 2019
 *
 * @brief This file provides generic access to the PCAL6524EV I/O expander populated on the
 * Sidekiq M.2-2280.
 *
 * Datasheet is available:
 * - on the web -- https://www.nxp.com/docs/en/data-sheet/PCAL6524.pdf
 * - on the network -- /mnt/storage/datasheets/NXP/PCAL6524.pdf
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include "io_expander_pcal6524.h"
#include "io_expander_cache.h"
#include "sidekiq_hal.h"

/* enable debug_print and debug_print_plain when DEBUG_IO_EXP_PCAL6524 is defined */
#if (defined DEBUG_IO_EXP_PCAL6524)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/* The upper most bit in the register address indicates an "auto-increment" feature when reading /
 * writing multiple registers */
#define IO_EXPANDER_INPUT_PORT_REG      0x80 /* regs 0x00-0x02 with AI */
#define IO_EXPANDER_OUTPUT_PORT_REG     0x84 /* regs 0x04-0x06 with AI */
#define IO_EXPANDER_POLARITY_INV_REG    0x88 /* regs 0x08-0x0A with AI */
#define IO_EXPANDER_CONFIG_REG          0x8C /* regs 0x0C-0x0E with AI */
#define IO_EXPANDER_MIN_REG_ADDR        IO_EXPANDER_INPUT_PORT_REG
#define IO_EXPANDER_MAX_REG_ADDR        (IO_EXPANDER_CONFIG_REG+3)

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/



#if (defined DEBUG_PRINT_ENABLED)
/** This flag is used by debug_dump_io_exp to suppress debug_print statements in io_exp_read_reg()
 * since they are used within debug_dump_io_exp and it clutters the output. */
static bool suppress_debug = false;
#define debug_print_suppressible(...)   do { if ( !suppress_debug ) debug_print(__VA_ARGS__); } while (0)
#else
#define debug_print_suppressible(...)   do { ; } while (0)
#endif


/***** LOCAL FUNCTION PROTOTYPES *****/

static int32_t io_exp_read_reg_8bit( uint8_t card,
                                     uint8_t bus,
                                     uint8_t slave,
                                     uint8_t reg,
                                     uint8_t *p_val );


/***** LOCAL FUNCTIONS *****/


#if (defined DEBUG_PRINT_ENABLED)
/*****************************************************************************/
/** @brief Debug function used to print out the current state of the IO
    expander

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C device address

    @return int32_t status where 0=success, anything else is an error
*/
static void debug_dump_io_exp( uint8_t card,
                               uint8_t bus,
                               uint8_t slave )
{
    uint8_t n = 0, t = 0;

    debug_print("IO Expander (address = 0x%02X)\n", slave );
    for ( n = IO_EXPANDER_MIN_REG_ADDR; n < IO_EXPANDER_MAX_REG_ADDR; n++ )
    {
        suppress_debug = true;
        if ( ( n & 0x3 ) != 0x3 )
        {
            if ( 0 == io_exp_read_reg_noncacheable(card, bus, slave, n, &t, 1) )
            {
                debug_print("\tReg[0x%02x] = 0x%02x (P%u)\n", n, t, n & 0x3);
            }
            else
            {
                debug_print("\tReg[0x%02x] = ERROR\n", n);
            }
        }
        suppress_debug = false;
    }
}
#else
static void debug_dump_io_exp( uint8_t card, uint8_t bus, uint8_t slave ) { ; }
#endif  /* DEBUG_PRINT_ENABLED */


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** @brief Helper function used to write to an I/O expander 8-bit register, that is, the first
    ports of the I/O expander

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val The value to write

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_write_reg_8bit( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint8_t val )
{
    int32_t status = 0;

    status = io_exp_write_reg_cacheable( card, bus, slave, reg, &val, 1 );

    /* Read back the state of the IO expander registers post write. */
    debug_dump_io_exp(card, bus, slave);

    return status;
}


/**************************************************************************************************/
/** @brief Helper function used to write to @b two I/O expander 8-bit registers, that is, the first
    two ports of the I/O expander for a given register.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val The value to write

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_write_reg_16bit( uint8_t card,
                                       uint8_t bus,
                                       uint8_t slave,
                                       uint8_t reg,
                                       uint16_t val )
{
    int32_t status = 0;
    uint8_t i, num_bytes = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, buf,
                        sizeof(uint16_t) /* nr_elem */,
                        0 /* default value */);

    /* convert `val` into array of bytes */
    for (i = 0; i < sizeof(uint16_t); i++ )
    {
        buf[num_bytes++] = ( val & 0xFF );
        val >>= 8;
    }

    status = io_exp_write_reg_cacheable( card, bus, slave, reg, buf, num_bytes );

    /* Read back the state of the IO expander registers post write. */
    debug_dump_io_exp(card, bus, slave);

    return status;
}


/**************************************************************************************************/
/** @brief Helper function used to write to @b three I/O expander 8-bit registers, that is, all
    three ports of the I/O expander for a given register.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val The value to write

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_write_reg_24bit( uint8_t card,
                                       uint8_t bus,
                                       uint8_t slave,
                                       uint8_t reg,
                                       uint32_t val )
{
    int32_t status = 0;
    uint8_t i, num_bytes = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, buf,
                        3 /* nr_elem */,
                        0 /* default value */);


    if ( ( val & 0xFFFFFF ) != val )
    {
        skiq_error("Value (%08X) passed to I/O expander write register function exceeds capability "
                   "(> 24 bits)\n", val);
        status = -EINVAL;
    }
    else
    {
        val &= 0xFFFFFF;
        debug_print_suppressible("Writing x%06X to register x%02X on device addr x%02X and card "
                                 "%u\n", val, reg, slave, card);
    }

    if ( status == 0 )
    {
        /* convert `val` into array of bytes */
        for (i = 0; i < 3; i++ )
        {
            buf[num_bytes++] = ( val & 0xFF );
            val >>= 8;
        }

        status = io_exp_write_reg_cacheable( card, bus, slave, reg, buf,
                                             num_bytes );
    }

    /* Read back the state of the IO expander registers post write. */
    debug_dump_io_exp(card, bus, slave);

    return status;
}


/**************************************************************************************************/
/** @brief Helper function used to read from an I/O expander 8-bit register

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[out] p_val Pointer to be updated with read back value

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_read_reg_8bit( uint8_t card,
                                     uint8_t bus,
                                     uint8_t slave,
                                     uint8_t reg,
                                     uint8_t *p_val )
{
    int32_t status = 0;

    /* When reading 8 bits from one of the I/O expander input registers, use a non-cacheable
       register read since their values can change independent of the cache.  There are three input
       registers on the PCAL6524:

       - IO_EXPANDER_INPUT_PORT_REG
       - IO_EXPANDER_INPUT_PORT_REG+1
       - IO_EXPANDER_INPUT_PORT_REG+2
    */
    if ( ( reg == IO_EXPANDER_INPUT_PORT_REG ) ||
         ( reg == IO_EXPANDER_INPUT_PORT_REG+1 ) ||
         ( reg == IO_EXPANDER_INPUT_PORT_REG+2 ) )
    {
        status = io_exp_read_reg_noncacheable( card, bus, slave, reg, p_val, 1 );
    }
    else
    {
        status = io_exp_read_reg_cacheable( card, bus, slave, reg, p_val, 1 );
    }

    return status;
}


/**************************************************************************************************/
/** @brief Helper function used to read from an I/O expander 16-bit register, that is, the first two
    I/O ports.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] p_val Pointer to be updated with read back value

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_read_reg_16bit( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint16_t *p_val )
{
    int32_t status = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, buf,
                        sizeof(uint16_t) /* nr_elem */,
                        0 /* default value */);

    /* When reading 16 bits from one of the I/O expander input registers, use a non-cacheable
       register read since their values can change independent of the cache.  Since this is a 16-bit
       read, there are two overlapping cases:

       - IO_EXPANDER_INPUT_PORT_REG   and IO_EXPANDER_INPUT_PORT_REG+1
       - IO_EXPANDER_INPUT_PORT_REG+1 and IO_EXPANDER_INPUT_PORT_REG+2
    */
    if ( ( reg == IO_EXPANDER_INPUT_PORT_REG ) ||
         ( reg == IO_EXPANDER_INPUT_PORT_REG+1 ) )
    {
        status = io_exp_read_reg_noncacheable( card, bus, slave, reg, buf,
                                               sizeof(uint16_t) );
    }
    else if ( reg == IO_EXPANDER_INPUT_PORT_REG+2 )
    {
        /* Reading 16 bits starting at IO_EXPANDER_INPUT_PORT_REG+2 is invalid since
         * IO_EXPANDER_INPUT_PORT_REG+3 is undefined on the PCAL6524 */
        status = -EFAULT;
    }
    else
    {
        status = io_exp_read_reg_cacheable( card, bus, slave, reg, buf,
                                            sizeof(uint16_t) );
    }

    if ( status == 0 )
    {
        uint8_t num_bytes = sizeof(uint16_t);
        *p_val = 0;

        /* convert array of bytes into `*p_val` */
        while (num_bytes-- > 0)
        {
            *p_val |= buf[num_bytes];
            if ( num_bytes > 0 ) *p_val <<= 8;
        }
    }

    return status;
}


/**************************************************************************************************/
/** @brief Helper function used to read from an I/O expander 24-bit register

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] p_val Pointer to be updated with read back value

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_read_reg_24bit( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint32_t *p_val )
{
    int32_t status = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, buf,
                        3 /* nr_elem */,
                        0 /* default value */);

    if ( reg == IO_EXPANDER_INPUT_PORT_REG )
    {
        status = io_exp_read_reg_noncacheable( card, bus, slave, reg, buf, 3 );
    }
    else if ( ( reg == IO_EXPANDER_INPUT_PORT_REG+1 ) ||
              ( reg == IO_EXPANDER_INPUT_PORT_REG+2 ) )
    {
        /* Reading 24 bits starting at IO_EXPANDER_INPUT_PORT_REG+1 or IO_EXPANDER_INPUT_PORT_REG+2
         * is invalid since IO_EXPANDER_INPUT_PORT_REG+3 is undefined on the PCAL6524 */
        status = -EFAULT;
    }
    else
    {
        status = io_exp_read_reg_cacheable( card, bus, slave, reg, buf, 3 );
    }

    if ( status == 0 )
    {
        uint8_t num_bytes = 3;
        *p_val = 0;

        /* convert array of bytes into `*p_val` */
        while (num_bytes-- > 0)
        {
            *p_val |= buf[num_bytes];
            if ( num_bytes > 0 ) *p_val <<= 8;
        }
    }

    return status;
}


/**************************************************************************************************/
/** @brief Helper function used to write to the IO expander register in a manner such that only bits
    defined in the mask are affected.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val The value to write
    @param[in] mask Bitmask used to select bits

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_write_reg_mask( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint32_t val,
                                      uint32_t mask )
{
    uint32_t tmp = 0;
    int32_t status = 0;

    bool is_8bit = (( mask & 0xFF ) == mask);
    bool is_16bit = (( mask & 0xFFFF ) == mask);
    bool is_24bit = (( mask & 0xFFFFFF ) == mask);

    // Read the register's value...
    if ( is_8bit )
    {
        uint8_t tmp8 = 0;
        status = io_exp_read_reg_8bit(card, bus, slave, reg, &tmp8);
        if ( status == 0 ) tmp = tmp8;
    }
    else if ( is_16bit )
    {
        uint16_t tmp16 = 0;
        status = io_exp_read_reg_16bit(card, bus, slave, reg, &tmp16);
        if ( status == 0 ) tmp = tmp16;
    }
    else if ( is_24bit )
    {
        status = io_exp_read_reg_24bit(card, bus, slave, reg, &tmp);
    }
    else
    {
        skiq_error("Mask (%08X) passed to I/O expander write register function exceeds capability "
                   "(> 24 bits)\n", mask);
    }

    if ( 0 == status )
    {
        uint32_t tmp2 = tmp;

        // clear out the bits that are going to be written...
        tmp2 &= ~mask;
        // and OR in the data...
        tmp2 |= (val & mask);
        // then write.

        /* skip writing 'tmp2' if it isn't different from 'tmp' */
        if ( tmp2 != tmp )
        {
            if ( is_8bit )
            {
                status = io_exp_write_reg_8bit(card, bus, slave, reg, tmp2);
            }
            else if ( is_16bit )
            {
                status = io_exp_write_reg_16bit(card, bus, slave, reg, tmp2);
            }
            else if ( is_24bit )
            {
                status = io_exp_write_reg_24bit(card, bus, slave, reg, tmp2);
            }
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t pcal6524_io_exp_set_as_input( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint32_t pin_mask )
{
    int32_t status = 0;

    /* configure pins as input */
    status = io_exp_write_reg_mask( card, bus, slave, IO_EXPANDER_CONFIG_REG, pin_mask, pin_mask );

    /* set pin values */
    if ( 0 == status )
    {
        status = io_exp_write_reg_mask( card, bus, slave, IO_EXPANDER_OUTPUT_PORT_REG, 0, pin_mask );
    }

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t pcal6524_io_exp_set_as_output( uint8_t card,
                                       uint8_t bus,
                                       uint8_t slave,
                                       uint32_t pin_mask,
                                       uint32_t value_mask )
{
    int32_t status = 0;

    /* set pin values */
    status = io_exp_write_reg_mask( card, bus, slave, IO_EXPANDER_OUTPUT_PORT_REG, value_mask,
                                    pin_mask );

    if( 0 == status )
    {
        /* configure pins as output */
        status = io_exp_write_reg_mask( card, bus, slave, IO_EXPANDER_CONFIG_REG, 0, pin_mask );
    }

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t pcal6524_io_exp_set_as_hiz( uint8_t card,
                                    uint8_t bus,
                                    uint8_t slave,
                                    uint32_t pin_mask,
                                    uint32_t hiz_mask,
                                    uint32_t high_mask )
{
    int32_t status = 0;

    if ( ( pin_mask & hiz_mask ) != hiz_mask )
    {
        skiq_error( "Internal Error: hiz_mask 0x%08X specifies bits outside of pin_mask 0x%08X on "
                    "card %u\n", hiz_mask, pin_mask, card );
        status = -EPROTO;
    }

    if ( ( pin_mask & high_mask ) != high_mask )
    {
        skiq_error( "Internal Error: high_mask 0x%08X specifies bits outside of pin_mask 0x%08X on "
                    "card %u\n", high_mask, pin_mask, card );
        status = -EPROTO;
    }

    if ( ( hiz_mask & high_mask ) != 0 )
    {
        skiq_error( "Internal Error: high_mask 0x%08X and hiz_mask 0x%08X share asserted bits "
                    "card %u\n", high_mask, hiz_mask, card );
        status = -EPROTO;
    }

    if ( status == 0 )
    {
        /* set those pin values not in the 'high_mask' to low (0) and the rest as high (1) to
         * IO_EXPANDER_OUTPUT_PORT_REG */
        status = io_exp_write_reg_mask( card, bus, slave, IO_EXPANDER_OUTPUT_PORT_REG, high_mask,
                                        pin_mask );
    }

    if ( 0 == status )
    {
        /* configure those pins in the 'hiz_mask' to inputs (1) and the rest outputs (0) to
         * IO_EXPANDER_CONFIG_REG */
        status = io_exp_write_reg_mask( card, bus, slave, IO_EXPANDER_CONFIG_REG, hiz_mask, pin_mask );
    }

    /* consider any non-zero status code to be an I/O error (except -EPROTO) */
    if ( ( status != 0 ) && ( status != -EPROTO ) )
    {
        status = -EIO;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t pcal6524_io_exp_read_output( uint8_t card,
                                     uint8_t bus,
                                     uint8_t slave,
                                     uint32_t *p_value_mask )
{
    int32_t status;

    status = io_exp_read_reg_24bit( card, bus, slave, IO_EXPANDER_OUTPUT_PORT_REG, p_value_mask);

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t pcal6524_io_exp_read_input( uint8_t card,
                                    uint8_t bus,
                                    uint8_t slave,
                                    uint32_t *p_value_mask )
{
    int32_t status;

    status = io_exp_read_reg_24bit( card, bus, slave, IO_EXPANDER_INPUT_PORT_REG, p_value_mask);

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t pcal6524_io_exp_read_direction( uint8_t card,
                                        uint8_t bus,
                                        uint8_t slave,
                                        uint32_t *p_dir_mask )
{
    int32_t status;

    status = io_exp_read_reg_24bit( card, bus, slave, IO_EXPANDER_CONFIG_REG, p_dir_mask);

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}
