/**
    @file   hal_time.c
    @brief  Platform abstractions for time-based functions

    @note   For Windows, see
            https://docs.microsoft.com/en-us/windows/win32/sysinfo/acquiring-high-resolution-time-stamps
            for more information on the subject (some of the below
            code is heavily inspired by given examples)

    <pre>
    Copyright 2019 - 2020 Epiq Solutions, All Rights Reserved
    </pre>
*/

#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <time.h>
#include <errno.h>

#include <stdio.h>

#ifdef __MINGW32__
#include <windows.h>
#endif /* __MINGW32__ */

#include "hal_delay.h"
#include "hal_time.h"

#if (defined __MINGW32__)
static int32_t
windows_gettime_monotonic(hal_timespec_t *p_clockVal)
{
    int32_t success = 0;
    LARGE_INTEGER qpcVal = { 0 };
    LARGE_INTEGER frequency = { 0 };
    LARGE_INTEGER microsecs = { 0 };

    /* On WinXP and greater, these functions should always succeed. */
    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&qpcVal);

    microsecs.QuadPart = qpcVal.QuadPart * 1000000;
    microsecs.QuadPart /= frequency.QuadPart;

    p_clockVal->tv_sec = (uint64_t) (microsecs.QuadPart / 1000000);
    p_clockVal->tv_nsec = (uint64_t) ((microsecs.QuadPart % 1000000) * 1000);

    return (success);
}

/** @todo   use GetSystemTimePreciseAsFileTime if available (>= Win8) */
static int32_t
windows_gettime_realtime(hal_timespec_t *p_clockVal)
{
/* Jan 1, 1601 (as measured in 100ns intervals) */
#define UNIX_EPOCH_OFFSET   (116444736000000000ull)

    int32_t success = 0;
    FILETIME currentTime = { 0 };
    ULARGE_INTEGER value = { 0 };
    __int64 fileTime = 0;

    GetSystemTimeAsFileTime(&currentTime);
    value.LowPart = currentTime.dwLowDateTime;
    value.HighPart = currentTime.dwHighDateTime;
    fileTime = value.QuadPart;

    /*
        The Win32 API returns the system time as a FILETIME, which is the time in 100ns intervals
        as measured from Jan 1, 1601; the UNIX epoch is Jan 1, 1970. In order to make this function
        work similiarly to its Linux counterpart, the epoch should be the same.
    */
    printf("fileTime = %" PRIu64 "\n", (uint64_t) fileTime);

    if (UNIX_EPOCH_OFFSET <= fileTime)
    {
        fileTime -= UNIX_EPOCH_OFFSET;
    }
    else
    {
        fileTime = 0;
    }

    /* FILETIME is measured in 100ns intervals */
    p_clockVal->tv_sec = (uint64_t) (fileTime / 10000000ull);
    p_clockVal->tv_nsec = (uint64_t) (fileTime % 10000000ull);

    return (success);
#undef UNIX_EPOCH_OFFSET
}

static int32_t
windows_gettime(hal_time_clock_type_t clockType, hal_timespec_t *p_clockVal)
{
    int32_t status = 0;

    switch (clockType)
    {
    case hal_time_clock_type_monotonic:
        status = windows_gettime_monotonic(p_clockVal);
        break;
    case hal_time_clock_type_realtime:
        status = windows_gettime_realtime(p_clockVal);
        break;
    case hal_time_clock_type_process_cputime_id:
    case hal_time_clock_type_thread_cputime_id:
    case hal_time_clock_type_invalid:
    default:
        status = -ENOTSUP;
        break;
    }

    return (status);
}

#else /* defined __MINGW32__ */

static int32_t
linux_gettime(hal_time_clock_type_t clockType, hal_timespec_t *p_clockVal)
{
    int32_t status = 0;
    clockid_t clock = CLOCK_MONOTONIC;
    struct timespec t = { 0, 0 };

    switch (clockType)
    {
    case hal_time_clock_type_monotonic:
        clock = CLOCK_MONOTONIC;
        break;
    case hal_time_clock_type_realtime:
        clock = CLOCK_REALTIME;
        break;
    case hal_time_clock_type_process_cputime_id:
        clock = CLOCK_PROCESS_CPUTIME_ID;
        break;
    case hal_time_clock_type_thread_cputime_id:
        clock = CLOCK_THREAD_CPUTIME_ID;
        break;
    case hal_time_clock_type_invalid:
    default:
        status = -EINVAL;
        break;
    }

    if (0 == status)
    {
        errno = 0;
        status = clock_gettime(clock, &t);
        if (0 == status)
        {
            p_clockVal->tv_sec = (uint64_t) t.tv_sec;
            p_clockVal->tv_nsec = (uint64_t) t.tv_nsec;
        }
        else
        {
            status = (int32_t) -errno;
        }
    }

    return (status);
}

#endif /* defined __MINGW32__ */

int32_t
hal_time_get(hal_time_clock_type_t clockType, hal_timespec_t *p_clockVal)
{
#if (defined  __MINGW32__)
    return windows_gettime(clockType, p_clockVal);
#else
    return linux_gettime(clockType, p_clockVal);
#endif
}

int32_t
hal_time_get_ns(hal_time_clock_type_t clockType, uint64_t *p_clockVal)
{
    int32_t status = 0;
    hal_timespec_t now = { 0, 0 };

#if (defined __MINGW32__)
    status = windows_gettime(clockType, &now);
#else
    status = linux_gettime(clockType, &now);
#endif

    if (0 == status)
    {
        *p_clockVal = (now.tv_sec * SEC) + now.tv_nsec;
    }

    return (status);
}


