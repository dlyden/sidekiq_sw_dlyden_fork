/**
 * @file   fpga_reg_hal_icap.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Aug  8 12:26:48 CDT 2019
 *
 * @brief
 *
 * <pre>
 * Copyright 2019-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <stdint.h>

#include "portable_endian.h"
#include "sidekiq_private.h"

#include "fpga_reg_hal_icap.h"
#include "xilinx_fpga_bitstream.h"
#include "hal_file_ops.h"
#include "sidekiq_xport.h"
#include "sidekiq_fpga_reg_defs.h"

/* enable debug_print and debug_print_plain when DEBUG_FPGA_REG_HAL_ICAP is defined */
#if (defined DEBUG_FPGA_REG_HAL_ICAP)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/*
 * Maximum configuration bitstream length is 2,192,012 bytes (17,536,096 bits):
 * - Table 1-1: "Bitstream Length" in 7 Series FPGAs Configuration User Guide (UG470)
 * - on the web -- https://www.xilinx.com/support/documentation/user_guides/ug470_7Series_Config.pdf
 * - on the network -- /mnt/storage/datasheets/Xilinx/Artix_7_Configuration_Guide_ug470.pdf
 */
#define BITFILE_HEADER_SIZE             256
#define MIN_SIZE_M2_2280_BITSTREAM      BITFILE_HEADER_SIZE
#define MAX_SIZE_M2_2280_BITSTREAM      (2192012 + BITFILE_HEADER_SIZE)

/*
 * ICAP "O" Port Bits:
 * - Table 6-2 in Partial Reconfiguration User Guide UG702 (v14.2) July 25, 2012
 * - on the web -- https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_2/ug702.pdf
 * - on the network -- /mnt/storage/datasheets/Xilinx/Partial_Reconfiguration_User_Guide_ug702.pdf
 */
#define DALIGN_OFFSET                   6
#define DALIGN_LEN                      1
#define CFGERR_B_OFFSET                 7
#define CFGERR_B_LEN                    1


/***** TYPE DEFINITIONS *****/


/***** STRUCTS *****/


/***** LOCAL DATA *****/


/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
static int32_t
write_buffer_to_fpga_icap( uint8_t card,
                           uint32_t data[],
                           uint32_t nr_words,
                           bool *had_sync_transition,
                           bool *had_cfg_error )
{
    int32_t status = 0;
    bool sync_transition = false, cfg_error = false;
    bool synced;
    uint32_t i, val;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_ICAP_READ_DATA, &val);
    if ( status == 0 )
    {
        synced = RBF_GET(val, DALIGN) > 0;
        cfg_error = cfg_error || (RBF_GET(val, CFGERR_B) == 0);
    }

    debug_print("Starting ICAP process:\n\t");
    for ( i = 0; ( i < nr_words ) && ( status == 0 ); i++ )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_ICAP_WRITE_DATA, htobe32(data[i]));
        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_read(card, FPGA_REG_ICAP_READ_DATA, &val);
        }

        if ( status == 0 )
        {
            bool curr_synced;

            curr_synced = RBF_GET(val, DALIGN) > 0;
            if ( !synced && curr_synced )
            {
                debug_print_plain(" FPGA SYNC");
                sync_transition = true;
            }
            synced = curr_synced;
            cfg_error = cfg_error || (RBF_GET(val, CFGERR_B) == 0);

            if ( ( i > 0 ) && ( ( i % 8 ) == 0 ) ) debug_print_plain("\n\t");
            debug_print_plain(" %08X/%02X", htobe32(data[i]), val & 0xFF);
        }
    }
    debug_print_plain("\n");

    if ( status == 0 )
    {
        if ( had_sync_transition != NULL )
        {
            *had_sync_transition = sync_transition;
        }

        if ( had_cfg_error != NULL )
        {
            *had_cfg_error = cfg_error;
        }
    }

    return status;
}


/**************************************************************************************************/
static int32_t
perform_fpga_icap_desync_if_sync( uint8_t card )
{
    int32_t status;
    bool desync_performed = false;
    uint32_t val;
    uint32_t desync[] = {
        htobe32(0x30008001), /* Type 1 write 1 word to CMD */
        htobe32(0x0000000D), /* Desync command */
        htobe32(0x20000000), /* NO-OP */
        htobe32(0x20000000), /* NO-OP */
    };

    /* check sync state of FPGA ICAP interface */
    status = sidekiq_fpga_reg_read(card, FPGA_REG_ICAP_READ_DATA, &val);
    if ( status == 0 )
    {
        if ( RBF_GET(val, DALIGN) > 0 )
        {
            skiq_warning("FPGA ICAP in sync state, need to de-sync on card %u\n", card);
            desync_performed = true;
            status = write_buffer_to_fpga_icap( card, desync, ARRAY_SIZE(desync),
                                                NULL, /* had_sync_transition */
                                                NULL  /* had_cfg_error */ );
        }
    }

    /* if a de-sync was performed, check that it was successful */
    if ( ( status == 0 ) && ( desync_performed ) )
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_ICAP_READ_DATA, &val);
        if ( status == 0 )
        {
            if ( RBF_GET(val, DALIGN) > 0 )
            {
                skiq_error("FPGA ICAP state failed to de-sync on card %u\n", card);
                status = -EIO;
            }
        }
    }

    /* indicate a desync was performed by returning a `1` */
    if ( ( status == 0 ) && ( desync_performed ) )
    {
        status = 1;
    }

    return status;
}


/**************************************************************************************************/
static int32_t
perform_fpga_icap_partial_reconfig( uint8_t card,
                                    uint32_t data[],
                                    uint32_t nr_words )
{
    int32_t status = 0;

    /* Step 1. tell FPGA ICAP to de-sync if it's in a sync state */
    {
        status = perform_fpga_icap_desync_if_sync( card );

        /* suppress the `1`, it doesn't matter at this stage of reconfiguration */
        if ( status > 0 ) status = 0;
    }

    /* Step 2. write_buffer_to_fpga_icap() and check:
       - `had_sync_transition` == true
       - `had_cfg_error` == false
    */
    if ( status == 0 )
    {
        bool had_sync_transition, had_cfg_error;

        status = write_buffer_to_fpga_icap( card, data, nr_words,
                                            &had_sync_transition, &had_cfg_error );
        if ( status == 0 )
        {
            if ( !had_sync_transition )
            {
                skiq_warning("FPGA ICAP did not receive a sync word on card %u\n", card);
                status = -EPROTO;
            }
            else if ( had_cfg_error )
            {
                skiq_warning("FPGA ICAP indicated a configuration error (corrupt or incompatible "
                             "bitstream?) on card %u\n", card);
                status = -EPROTO;
            }
        }
    }

    /* Step 3. if FPGA ICAP is sync here, that's an error, but perform de-sync */
    if ( status == 0 )
    {
        status = perform_fpga_icap_desync_if_sync( card );

        /* transform the `1` to an error message, if partial reconfiguration was successful, then
         * the FPGA ICAP should not have needed to be de-synced */
        if ( status > 0 ) status = -ECANCELED;
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
int32_t
fpga_icap_prog_fpga_from_file( uint8_t card,
                               FILE *fp )
{
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( card );

    if ( part == skiq_m2_2280 )
    {
        uint32_t *data = NULL;
        long size, size_in_words;
        size_t nr_words;

        /* determine file size of provided file stream */
        size = hal_file_stream_size( fp );
        if ( !IN_RANGE_INCL(size, MIN_SIZE_M2_2280_BITSTREAM, MAX_SIZE_M2_2280_BITSTREAM) )
        {
            status = -EINVAL;
        }
        else
        {
            size_in_words = size / 4;
        }

        /* allocate memory to temporarily store bitstream */
        if ( status == 0 )
        {
            data = malloc( size );
            if ( data == NULL )
            {
                status = -ENOMEM;
            }
        }

        /* read bitstream into allocated memory */
        if ( status == 0 )
        {
            nr_words = fread(data, sizeof(uint32_t), size_in_words, fp);
            if ( nr_words != size_in_words )
            {
                status = -EBADF;
            }
        }

        /* check to see if it's a supported file format */
        if ( status == 0 )
        {
            if ( !xilinx_fpga_bitstream_is_bitfile( card, data, nr_words ) )
            {
                skiq_warning("FPGA bitstream is not a .BIT file format on card %u, other formats "
                             "not yet supported\n", card);
                status = -EINVAL;
            }
        }

        /* check to see if it's a supported FPGA format */
        if ( status == 0 )
        {
            if ( !xilinx_fpga_bitstream_is_partial( card, data, nr_words ) )
            {
                skiq_warning("FPGA bitstream is not a Partial Reconfiguration (PR) bitstream "
                             "on card %u, PR bitstream required for this functionality on Sidekiq "
                             "%s\n", card, part_cstr( _skiq_get_part( card ) ) );
                status = -EINVAL;
            }
        }

        /* word alignment of the data is necessary for the ICAP interface */
        if ( status == 0 )
        {
            status = xilinx_fpga_bitstream_word_align( card, data, nr_words );
        }

        /* finally perform the partial reconfiguration of the FPGA over ICAP */
        if ( status == 0 )
        {
            status = perform_fpga_icap_partial_reconfig( card, data, nr_words );
        }

        FREE_IF_NOT_NULL(data);
    }
    else
    {
        skiq_error("Unable to reprogram FPGA from a file using ICAP on card %u\n", card);
        status = -ENOTSUP;
    }

    return status;
}
