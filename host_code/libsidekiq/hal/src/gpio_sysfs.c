/*****************************************************************************/
/** @file gpio_sysfs.c

 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

 @brief This file contains the implementation for the GPIO sysfs control

*/

#include "gpio_sysfs.h"
#include "sidekiq_private.h"
#include "sidekiq_rpc_client_api.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

#define MAX_BUF_LEN (1000) // arbitrarily large...used for various string manip with GPIO

#define GPIOSYS ("/sys/class/gpio/") // base of sysfs entry
#define GPIO_EXPORT ("export") // used to "enable" GPIO
#define GPIO_UNEXPORT ("unexport") // used to "disable" GPIO

#define GPIO_DIR_IN ("in") // input
#define GPIO_DIR_OUT ("out") // output

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t gpio_sysfs_enable_ctrl( uint8_t card, uint32_t gpio_class, uint32_t gpio_offset )
{
    int32_t status=0;
    char buf[MAX_BUF_LEN];
    FILE *p_export_file=NULL;
    int32_t len=0;

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        status = rpc_client_gpio_sysfs_enable_ctrl(card, gpio_class, gpio_offset);
    }
    else
    {
        if( snprintf( buf, MAX_BUF_LEN, "%s%s", GPIOSYS, GPIO_EXPORT ) < MAX_BUF_LEN )
        {
            if( (p_export_file = fopen( buf, "w" )) != NULL )
            {
                len = snprintf( buf, MAX_BUF_LEN, "%u", (gpio_class + gpio_offset) );
                if( len < MAX_BUF_LEN )
                {
                    if( fwrite( buf, 1, len, p_export_file ) != len )
                    {
                        _skiq_log(SKIQ_LOG_ERROR, "unable to write desired length for export (errno=%u)\n", errno);
                        status = -ENODEV;
                    }
                }
                else
                {
                    status = -E2BIG;
                }
                fclose( p_export_file );
            }
            else
            {
                status = -errno;
                _skiq_log(SKIQ_LOG_ERROR, "unable to open export file %s (strerror %s)\n", buf, strerror(errno));
            }
        }
        else
        {
            status = -E2BIG;
        }
    }
    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t gpio_sysfs_disable_ctrl( uint8_t card, uint32_t gpio_class, uint8_t gpio_offset )
{
    int32_t status=0;
    char buf[MAX_BUF_LEN];
    FILE *p_unexport_file=NULL;
    int32_t len=0;

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        status = rpc_client_gpio_sysfs_disable_ctrl( card, gpio_class, gpio_offset );
    }
    else
    {
        if( snprintf( buf, MAX_BUF_LEN, "%s%s", GPIOSYS, GPIO_UNEXPORT ) < MAX_BUF_LEN )
        {
            if( (p_unexport_file = fopen( buf, "w" )) != NULL )
            {
                len = snprintf( buf, MAX_BUF_LEN, "%u", (gpio_class + gpio_offset) );
                if( len < MAX_BUF_LEN )
                {
                    if( fwrite( buf, 1, len, p_unexport_file ) != len )
                    {
                        _skiq_log(SKIQ_LOG_ERROR, "unable to write desired length for unexport (errno=%u)\n", errno);
                        status = -ENODEV;
                    }
                }
                else
                {
                    status = -E2BIG;
                }
                fclose( p_unexport_file );
            }
            else
            {
                status = -errno;
                _skiq_log(SKIQ_LOG_ERROR, "unable to open export file %s (strerror %s)\n", buf, strerror(errno));
            }
        }
        else
        {
            status = -E2BIG;
        }
    }
    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t gpio_sysfs_configure_direction( uint8_t card, uint32_t gpio_class, uint32_t gpio_offset, gpio_ddr_t dir )
{
    int32_t status=0;
    char buf[MAX_BUF_LEN];
    FILE *p_gpio_dir_file=NULL;
    int32_t len=0;

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        status = rpc_client_gpio_sysfs_configure_direction( card, gpio_class, gpio_offset, dir );
    }
    else
    {
        if( snprintf( buf, MAX_BUF_LEN, "%sgpio%u/direction", GPIOSYS, (gpio_class + gpio_offset) ) < MAX_BUF_LEN )
        {
            if( (p_gpio_dir_file = fopen( buf, "w" )) != NULL )
            {
                if( dir == gpio_ddr_in )
                {
                    len = snprintf( buf, MAX_BUF_LEN, "%s", GPIO_DIR_IN );
                    if( len >= MAX_BUF_LEN )
                    {
                        status = -E2BIG;
                    }
                }
                else if( dir == gpio_ddr_out )
                {
                    len = snprintf( buf, MAX_BUF_LEN, "%s", GPIO_DIR_OUT );
                    if( len >= MAX_BUF_LEN )
                    {
                        status = -E2BIG;
                    }
                }
                else
                {
                    _skiq_log(SKIQ_LOG_ERROR, "invalid direction specified\n");
                    status = -EINVAL;
                }
                if( status == 0 )
                {
                    if( fwrite( buf, 1, len, p_gpio_dir_file ) != len )
                    {
                        _skiq_log(SKIQ_LOG_ERROR, "unable to write desired length for direction (errno=%u)\n", errno);
                        status = -ENODEV;
                    }
                }
                fclose( p_gpio_dir_file );
            }
            else
            {
                status = -errno;
                _skiq_log(SKIQ_LOG_ERROR, "unable to open export file %s (strerror %s)\n", buf, strerror(errno));
            }
        }
        else
        {
            status = -E2BIG;
        }
    }

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t gpio_sysfs_read_value( uint8_t card, uint32_t gpio_class, uint32_t gpio_offset, uint8_t *p_value )
{
    int32_t status=0;
    char buf[MAX_BUF_LEN];
    FILE *p_gpio_value_file=NULL;

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        status = rpc_client_gpio_sysfs_read_value( card, gpio_class, gpio_offset, p_value );
    }
    else
    {
        if( snprintf( buf, MAX_BUF_LEN, "%sgpio%u/value", GPIOSYS, (gpio_class + gpio_offset) ) < MAX_BUF_LEN )
        {
            if( (p_gpio_value_file = fopen( buf, "r" )) != NULL )
            {
                if( fread( buf, 1, 1, p_gpio_value_file ) == 1 )
                {
                    if( buf[0] == '0' )
                    {
                        *p_value = 0;
                    }
                    else if( buf[0] == '1' )
                    {
                        *p_value = 1;
                    }
                    else
                    {
                        _skiq_log(SKIQ_LOG_ERROR, "invalid value for GPIO %c\n", buf[0]);
                        status = -EINVAL;
                    }
                }
                else
                {
                    status = -errno;
                    _skiq_log(SKIQ_LOG_ERROR, "unable to read GPIO value (errno=%u)\n", errno);
                }
                fclose( p_gpio_value_file );
            }
            else
            {
                status = -errno;
                _skiq_log(SKIQ_LOG_ERROR, "unable to open GPIO value file %s (strerror %s)\n", buf, strerror(errno));
            }
        }
        else
        {
            status = -E2BIG;
        }
    }

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t gpio_sysfs_write_value( uint8_t card, uint32_t gpio_class, uint32_t gpio_offset, uint8_t value )
{
    int32_t status=0;
    char buf[MAX_BUF_LEN];
    FILE *p_gpio_value_file=NULL;
    int32_t len=0;

    if( value != 0 && value != 1 )
    {
        status = -EINVAL;
        _skiq_log(SKIQ_LOG_ERROR, "invalid value %u specified for GPIO\n", value);
    }
    else
    {
        if( sidekiq_rpc_is_client_avail(card) == true )
        {
            status = rpc_client_gpio_sysfs_write_value( card, gpio_class, gpio_offset, value );
        }
        else
        {
            if( snprintf( buf, MAX_BUF_LEN, "%sgpio%u/value", GPIOSYS, (gpio_class + gpio_offset) ) < MAX_BUF_LEN )
            {
                if( (p_gpio_value_file = fopen( buf, "w" )) != NULL )
                {
                    len = snprintf(buf, MAX_BUF_LEN, "%u", value);
                    if( len < MAX_BUF_LEN )
                    {
                        if( fwrite( buf, 1, len, p_gpio_value_file ) != len )
                        {
                            status = -errno;
                            _skiq_log(SKIQ_LOG_ERROR, "unable to write GPIO value (errno=%u)\n", errno);
                        }
                    }
                    else
                    {
                        status = -E2BIG;
                    }
                    fclose( p_gpio_value_file );
                }
                else
                {
                    status = -errno;
                    _skiq_log(SKIQ_LOG_ERROR, "unable to open file %s (errno=%u)\n", buf, errno);
                }
            }
            else
            {
                status = -E2BIG;
            }
        }
    }

    return (status);
}
