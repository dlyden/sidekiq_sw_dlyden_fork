/**
 * @file   io_expander_tca6408.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu May 10 13:38:14 2018
 *
 * @brief This file provides generic access to the TCA6408ARSVR I/O expander populated on the
 * Sidekiq Z2.
 *
 * <pre>
 * Copyright 2018-2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include "io_expander_tca6408.h"
#include "io_expander_cache.h"

#include "sidekiq_hal.h"
#include "sidekiq_private.h"

/* enable debug_print and debug_print_plain when DEBUG_IO_EXP_TCA6408 is defined */
#if (defined DEBUG_IO_EXP_TCA6408)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define IO_EXPANDER_I2C_BUS             0 /* on Z2, I2C_????, ?? MHz bus */
#define IO_EXPANDER_INPUT_PORT_REG      0x00
#define IO_EXPANDER_OUTPUT_PORT_REG     0x01
#define IO_EXPANDER_POLARITY_INV_REG    0x02
#define IO_EXPANDER_CONFIG_REG          0x03
#define IO_EXPANDER_MAX_REG_ADDR        (IO_EXPANDER_CONFIG_REG+1)


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/



#if (defined DEBUG_PRINT_ENABLED)
/** This flag is used by debug_dump_io_exp to suppress debug_print statements in io_exp_read_reg()
 * since they are used within debug_dump_io_exp and it clutters the output. */
static bool suppress_debug = false;
#define debug_print_suppressible(...)   do { if ( !suppress_debug ) debug_print(__VA_ARGS__); } while (0)
#else
#define debug_print_suppressible(...)   do { ; } while (0)
#endif


/***** LOCAL FUNCTIONS *****/

#if (defined DEBUG_PRINT_ENABLED)
/*****************************************************************************/
/** @brief Debug function used to print out the current state of the IO
    expander

    @param[in] card Sidekiq card index
    @param[in] slave I2C device address

    @return int32_t status where 0=success, anything else is an error
*/
static void debug_dump_io_exp( uint8_t card,
                               uint8_t slave )
{
    uint8_t n = 0, t = 0;

    debug_print("IO Expander (address = 0x%02X)\n", slave );
    for ( n = 0; n < IO_EXPANDER_MAX_REG_ADDR; n++ )
    {
        suppress_debug = true;
        if ( 0 == io_exp_read_reg_noncacheable(card, IO_EXPANDER_I2C_BUS, slave, n, &t, 1) )
        {
            debug_print("\tReg[0x%02x] = 0x%02x\n", n, t);
        }
        else
        {
            debug_print("\tReg[0x%02x] = ERROR\n", n);
        }
        suppress_debug = false;
    }
}
#else
static void debug_dump_io_exp( uint8_t card, uint8_t slave ) { ; }
#endif  /* DEBUG_PRINT_ENABLED */


/***** GLOBAL FUNCTIONS *****/


/*****************************************************************************/
/** @brief Helper function used to write to the IO expander register

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val The value to write

    @return int32_t status where 0=success, anything else is an error
    @retval -EINVAL invalid slave or register address
    @retval -EIO Input/output error communicating with the I/O expander
*/
static int32_t io_exp_write_reg( uint8_t card,
                                 uint8_t slave,
                                 uint8_t reg,
                                 uint8_t val )
{
    int32_t status = 0;

    status = io_exp_write_reg_cacheable( card, IO_EXPANDER_I2C_BUS, slave, reg, &val, 1 );

    /* Read back the state of the IO expander registers post write. */
    debug_dump_io_exp(card, slave);

    return status;
}

/*****************************************************************************/
/** @brief Helper function used to read from the IO expander register

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] p_val Pointer to be updated with read back value

    @return int32_t status where 0=success, anything else is an error
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EFAULT - NULL pointer passed to function
    @retval -EINVAL invalid slave or register address
*/
static int32_t io_exp_read_reg( uint8_t card,
                                uint8_t slave,
                                uint8_t reg,
                                uint8_t *p_val )
{
    return io_exp_read_reg_cacheable( card, IO_EXPANDER_I2C_BUS, slave, reg, p_val, 1 );
}

/*****************************************************************************/
/** @brief Helper function used to write to the IO expander register in a
    manner such that only bits defined in the mask are affected.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val The value to write
    @param[in] mask Bitmask used to select bits

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t io_exp_write_reg_mask( uint8_t card,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint8_t val,
                                      uint8_t mask )
{
    uint8_t tmp = 0;
    int32_t status = 0;

    // Read back the register's value...
    status = io_exp_read_reg(card, slave, reg, &tmp);
    if( 0 == status )
    {
        uint8_t tmp2 = tmp;

        // clear out the bits that are going to be written...
        tmp2 &= ~mask;
        // and OR in the data...
        tmp2 |= (val & mask);
        // then write.

        /* skip writing 'tmp2' if it isn't different from 'tmp' */
        if ( tmp2 != tmp )
        {
            status = io_exp_write_reg(card, slave, reg, tmp2);
        }
    }

    return status;
}


/**************************************************************************************************/
/** tca6408_io_exp_set_as_input() sets the pins specified by 'pin_mask' as inputs and also sets
    their output values to low.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_as_input( uint8_t card,
                                     uint8_t slave,
                                     uint8_t pin_mask )
{
    int32_t status = 0;

    /* configure pins as input */
    status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_CONFIG_REG, pin_mask, pin_mask );

    /* set pin values */
    if ( 0 == status )
    {
        status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_OUTPUT_PORT_REG, 0, pin_mask );
    }

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/**************************************************************************************************/
/** tca6408_io_exp_set_pin_polarity() sets the pin polarity.  The pins specified by 'pin_mask' are
    inverted according to 'invert_mask' where a 1 means to invert while a 0 means not to invert.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] invert_mask Inversion bitmask that specifies whether or not a pin's value should be inverted

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_pin_polarity( uint8_t card,
                                         uint8_t slave,
                                         uint8_t pin_mask,
                                         uint8_t invert_mask )
{
    int32_t status;

    status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_POLARITY_INV_REG, invert_mask, pin_mask );

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/**************************************************************************************************/
/** tca6408_io_exp_set_as_output() configures the pins specified by 'pin_mask' as outputs and sets
    their voltage levels according to 'value_mask'.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] value_mask Bitmask that specifies pin output levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_as_output( uint8_t card,
                                      uint8_t slave,
                                      uint8_t pin_mask,
                                      uint8_t value_mask )
{
    int32_t status = 0;

    /* set pin values */
    status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_OUTPUT_PORT_REG, value_mask, pin_mask );
    if( 0 == status )
    {
        /* configure pins as output */
        status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_CONFIG_REG, 0, pin_mask );
    }

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/**************************************************************************************************/
/** tca6408_io_exp_set_as_hiz() configures the pins specified by 'pin_mask & hiz_mask' as inputs
    (Hi-Z) and pins specified by 'pin_mask & ~hi_mask` as high outputs

    @attention Pins NOT indicated in @a hiz_mask are configured as outputs and their values are set
    to high

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] hiz_mask Bitmask that specifies which pins to configure as Hi-Z

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_as_hiz( uint8_t card,
                                   uint8_t slave,
                                   uint8_t pin_mask,
                                   uint8_t hiz_mask )
{
    int32_t status = 0;

    /* @todo confirm that setting Hi-Z pins to low and input does in fact make them Hi-Z */

    /* set those pin values in the 'hiz_mask' to low (0) and the rest as high (1) to
     * IO_EXPANDER_OUTPUT_PORT_REG */
    status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_OUTPUT_PORT_REG, ~hiz_mask, pin_mask );
    if ( 0 == status )
    {
        /* configure those pins in the 'hiz_mask' to inputs (1) and the rest outputs (0) to
         * IO_EXPANDER_CONFIG_REG */
        status = io_exp_write_reg_mask( card, slave, IO_EXPANDER_CONFIG_REG, hiz_mask, pin_mask );
    }

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/**************************************************************************************************/
/** tca6408_io_exp_read_output() queries the output pin level.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[out] p_value_mask reference to uint8_t to be populated with the output pin levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_read_output( uint8_t card,
                                    uint8_t slave,
                                    uint8_t *p_value_mask )
{
    int32_t status;

    status = io_exp_read_reg( card, slave, IO_EXPANDER_OUTPUT_PORT_REG, p_value_mask);

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/**************************************************************************************************/
/** tca6408_io_exp_read_direction() queries the pin direction.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[out] p_dir_mask reference to uint8_t to be populated with the pin directions (0=output, 1=input)

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_read_direction( uint8_t card,
                                       uint8_t slave,
                                       uint8_t *p_dir_mask )
{
    int32_t status;

    status = io_exp_read_reg( card, slave, IO_EXPANDER_CONFIG_REG, p_dir_mask);

    /* consider any non-zero status code to be an I/O error */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}

