/**
 * @file   io_expander_cache.c
 * @author  <dan@epiq-solutions.com>
 * @date   Thu Apr 18 10:15:09 CDT 2019
 *
 * @brief This file provides generic access to I/O expander caching.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <ctype.h>              /* for isprint() */
#include <string.h>             /* for memcpy() */
#include <glib.h>

#include "io_expander_cache.h"

/* gmacros.h (by way of glib.h) provides a simplistic MIN and MAX that are better defined by our own
 * sidekiq_private.h */
#undef MIN
#undef MAX

#include "sidekiq_hal.h"

/* enable debug_print and debug_print_plain when DEBUG_IO_EXP_PCAL6524 is defined */
#if (defined DEBUG_IO_EXP_CACHE)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/* enable cache validation when DEBUG_IO_EXP_CACHE_VALIDATION is defined */
#if (defined DEBUG_IO_EXP_CACHE_VALIDATION)
#define VALIDATE_CACHE_DEFAULT          true
#else
#define VALIDATE_CACHE_DEFAULT          false
#endif


/***** DEFINES *****/

#define IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION  3


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static GHashTable * hash = NULL;
// flag indicating whether validation of the cache should be performed
static ARRAY_WITH_DEFAULTS( bool, validate_cache, SKIQ_MAX_NUM_CARDS, VALIDATE_CACHE_DEFAULT );
// flag enabling caching of IOE values (if disabled, all reads/writes go to hardware)
static ARRAY_WITH_DEFAULTS( bool, enable_cache, SKIQ_MAX_NUM_CARDS, true );
static uint32_t cache_hits = 0, cache_misses = 0, cache_invalidations = 0, cache_bypasses = 0;
static uint32_t num_read_requests = 0, num_write_requests = 0;

#if (defined DEBUG_PRINT_ENABLED)
/** This flag is used by debug_dump_io_exp to suppress debug_print statements in io_exp_read_reg()
 * since they are used within debug_dump_io_exp and it clutters the output. */
static bool suppress_debug = false;
#define debug_print_suppressible(...)   do { if ( !suppress_debug ) debug_print(__VA_ARGS__); } while (0)
#else
#define debug_print_suppressible(...)   do { ; } while (0)
#endif


/***** LOCAL FUNCTIONS *****/


#if (defined DEBUG_PRINT_ENABLED)
/*****************************************************************************/
/** This function prints contents of raw data

    @param p_data: a reference to raw bytes
    @param length: the number of bytes references by p_data
    @return: void
*/
#define debug_hex_dump(p_data,length)                           \
    do {                                                        \
        int i, j;                                               \
        uint8_t c;                                              \
                                                                \
        for (i = 0; i < length; i += 16)                        \
        {                                                       \
            /* print offset */                                  \
            debug_print("\t%06X:", i);                          \
                                                                \
            /* print HEX */                                     \
            for (j = 0; j < 16; j++)                            \
            {                                                   \
                if ( ( j % 2 ) == 0 ) debug_print_plain(" ");   \
                if ( ( j % 8 ) == 0 ) debug_print_plain(" ");   \
                if ( i + j < length )                           \
                    debug_print_plain("%02X", p_data[i + j]);   \
                else                                            \
                    debug_print_plain("  ");                    \
            }                                                   \
            debug_print_plain("    ");                          \
                                                                \
            /* print ASCII (if printable) */                    \
            for (j = 0; j < 16; j++)                            \
            {                                                   \
                if ( ( j % 8 ) == 0 ) debug_print_plain(" ");   \
                if ( i + j < length )                           \
                {                                               \
                    c = p_data[i + j];                          \
                    if ( isprint(c) )                           \
                        debug_print_plain("%c", c);             \
                    else                                        \
                        debug_print_plain(".");                 \
                }                                               \
            }                                                   \
            debug_print_plain("\n");                            \
        }                                                       \
    } while (0)
#else
#define debug_hex_dump(p_data,length)   do { ; } while (0)
#endif  /* DEBUG_PRINT_ENABLED */


static gboolean
check_keys_for_deletion(gpointer key, gpointer data, gpointer p_card)
{

    uint32_t my_key = GPOINTER_TO_UINT(key);
    gboolean to_be_deleted = false;

    debug_print_suppressible("Found key: x%08X\n", my_key);

    if (p_card != NULL)
    {
        uint32_t cardnum = *((uint8_t*)(p_card));

        /* Check if the key is for the requested card, if so, remove it.
            Recall that the key is defined by:
            key = (card << 24) + (bus << 16) + (slave << 8) + reg */

        if ( ((my_key & 0xFF000000) >> 24) == cardnum)
        {
            debug_print_suppressible("Removed key: x%08X\n", my_key);
            to_be_deleted = true;
        }

    }
    return to_be_deleted;
}


static int32_t
invalidate_cache(uint8_t card)
{
    uint32_t status = 0, keys_removed = 0;
    debug_print_suppressible("Invalidating cache for card %u\n", card);

    keys_removed = g_hash_table_foreach_remove (hash,
                             (GHRFunc) check_keys_for_deletion,
                             (gpointer)(&card));
    (void)keys_removed;

    debug_print_suppressible("Cache invalidation complete.  Removed %u items\n", keys_removed);

    return status;
}


#if (defined DEBUG_PRINT_ENABLED)
/*****************************************************************************/
/** @brief Debug function used to print out the current state of the IO
    expander

    @param[in] card Sidekiq card index
    @param[in] slave I2C device address

    @return int32_t status where 0=success, anything else is an error*/

static void io_exp_dump_stats(void)
{
    debug_print("       Reads : %u\n", num_read_requests);
    debug_print("       Writes: %u\n", num_write_requests);
    debug_print("       Hits  : %u\n", cache_hits);
    debug_print("       Misses: %u\n", cache_misses);
    debug_print("Invalidations: %u\n", cache_invalidations);
    debug_print("     Bypasses: %u\n", cache_bypasses);

    debug_print("Hash size: %u\n", (uint32_t)g_hash_table_size(hash));

    g_hash_table_foreach_remove (hash,
                                 (GHRFunc) check_keys_for_deletion,
                                 NULL);  //don't pass in the card number, just want to dump the keys
    return;
}
#else
static void io_exp_dump_stats( void ) { ; }
#endif  /* DEBUG_PRINT_ENABLED */


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** io_exp_write_cache populates a cache of the i2c slave register values.  This function should be
    called after performing a write on i2c.  The register value for the specified slave is stored
    and a flag is set indicating the cache has been initialized.

    @param[in] card Sidekiq card index
    @param[in] bus I2C bus
    @param[in] slave I2C slave address
    @param[in] reg I2C register address
    @param[in] data array of uint8_t values to write to specified register address
    @param[in] num_bytes number of valid bytes in @a data

    @return int32_t status where 0=success
    @retval -EPROTO - glib hash is not yet initialized
    @retval -EINVAL out-of-range number of bytes requested
 */
static int32_t
io_exp_write_cache( uint8_t card,
                    uint8_t bus,
                    uint8_t slave,
                    uint8_t reg,
                    uint8_t data[],
                    uint8_t num_bytes )
{
    int32_t status = 0;
    uint8_t i = 0;

    CHECK_NULL_PARAM(hash);

    if (num_bytes > IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION)
    {
        skiq_error("Invalid I2C write. %u bytes specified, max is %u (card=%u)",
                   num_bytes, IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION, card);
        status = -EPROTO;
    }

    for ( i = 0; ( i < num_bytes ) && ( status == 0 ); i++)
    {
        uint8_t val;
        uint32_t key;

        key = (card << 24) + (bus << 16) + (slave << 8) + (reg+i);
        val = data[i];
        debug_print_suppressible("Insert %X %X\n", key, val);

        g_hash_table_insert(hash, GUINT_TO_POINTER(key), GUINT_TO_POINTER(val));
    }

    return status;
}


/**************************************************************************************************/
/** io_exp_read_cache queries a cache of the i2c slave register values.  A majority of registers
    only change state when written to by libsidekiq, so upon writing, a cache is populated.  To
    minimize i2c bus access, this function should be called before attempting to read the value from
    i2c.

    @param[in]  card Sidekiq card index
    @param[in]  bus I2C bus
    @param[in]  slave I2C slave address
    @param[in]  reg I2C register address
    @param[out] data array of uint8_t to be populated with the register value(s)
    @param[in]  num_bytes number of bytes to read from the specified register

    @return int32_t status where 0=success
    @retval 0 Success
    @retval -EXDEV Cache miss
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EPROTO - glib hash is not yet initialized
    @retval -EINVAL out-of-range number of bytes requested
 */
static int32_t
io_exp_read_cache( uint8_t card,
                   uint8_t bus,
                   uint8_t slave,
                   uint8_t reg,
                   uint8_t data[],
                   uint8_t num_bytes)
{
    int32_t status = 0;
    uint8_t result = 0;
    uint8_t i = 0;

    gboolean cache_hit = false;

    CHECK_NULL_PARAM(hash);

    if (num_bytes > IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION)
    {
        skiq_error("Invalid I2C write. %u bytes specified, max is %u (card=%u)",
                   num_bytes, IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION, card);
        status = -EPROTO;
    }

    for ( i = 0; ( i < num_bytes ) && ( status == 0 ); i++)
    {
        gpointer output_value;
        uint32_t key;

        key = (card << 24) + (bus << 16) + (slave << 8) + reg+i;
        cache_hit = g_hash_table_lookup_extended(hash, GUINT_TO_POINTER(key), NULL, &output_value);
        if (cache_hit == true)
        {
            result = GPOINTER_TO_UINT(output_value);
            debug_print_suppressible(" Got key=x%08X val=x%02X from cache\n", key, result);
            data[i] =  result & 0xFF;
        }
        else
        {
            status = -EXDEV;
        }
    }

    if (status == 0)
    {
        if (validate_cache[card])
        {
            uint32_t val_from_i2c = 0, val_from_cache = 0;
            uint8_t buf[3] = { 0, 0, 0 };

            // go grab the values from the bus to validate

            // Write the register address to read from first then read 'num_bytes' from the register.
            status = hal_write_then_read_i2c_by_bus(card, bus, slave, reg, buf, num_bytes);

            if ( 0 == status )
            {
                switch (num_bytes)
                {
                    case 1:
                        val_from_cache = data[0];
                        val_from_i2c =   buf[0];
                        break;

                    case 2:
                        val_from_cache = data[0] | ( data[1] << 8 );
                        val_from_i2c =   buf[0]  | ( buf[1]  << 8 );
                        break;

                    case 3:
                        val_from_cache = data[0] | ( data[1] << 8 ) | ( data[2] << 16 );
                        val_from_i2c =   buf[0]  | ( buf[1]  << 8 ) | ( buf[2]  << 16 );
                        break;

                    default:
                        skiq_error("Internal Error: Invalid number of bytes (%u)\n", num_bytes);
                        status = -EPROTO;
                        break;
                }

                debug_print_suppressible("Read x%06X from register x%02X on device addr x%02X and "
                                         "card %u num_bytes %u\n", val_from_i2c, reg, slave, card,
                                         num_bytes);

                if ( val_from_cache != val_from_i2c )
                {
                    skiq_error("Error: I/O expander I2C cache mismatch! reg: x%02X  slave: x%02X  "
                               "num_bytes:%u\n", reg, slave, num_bytes);
                    status = -EIO;
                    hal_critical_exit(status);
                }
            }
        }
    }

    return status;
}


/**************************************************************************************************/
int32_t io_exp_write_reg_noncacheable( uint8_t card,
                                       uint8_t bus,
                                       uint8_t slave,
                                       uint8_t reg,
                                       uint8_t val[],
                                       uint8_t num_bytes )
{
    int32_t status = 0;

    if (num_bytes > IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION)
    {
        skiq_error("Invalid I2C write. %u bytes specified, max is %u (card=%u)",
                   num_bytes, IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION, card);
        status = -EPROTO;
    }

    if ( status == 0 )
    {
        ARRAY_WITH_DEFAULTS(uint8_t, buf,
                            IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION + 1 /* nr_elem */,
                            0 /* default value */);

        buf[0] = reg;
        memcpy( &(buf[1]), val, num_bytes );

        status = hal_write_i2c_by_bus(card, bus, slave, buf, num_bytes + 1);
    }

    return status;
}


/**************************************************************************************************/
int32_t io_exp_write_reg_cacheable( uint8_t card,
                                    uint8_t bus,
                                    uint8_t slave,
                                    uint8_t reg,
                                    uint8_t val[],
                                    uint8_t num_bytes )
{
    int32_t status = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, buf,
                        IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION + 1 /* nr_elem */,
                        0 /* default value */);

    num_write_requests++;
    debug_print_suppressible("Write request for register x%02X on device %02X:%02X and card %u:\n",
                             reg, bus, slave, card);
    debug_hex_dump( val, num_bytes );

    if( enable_cache[card] == false )
    {
        cache_bypasses++;
        return (io_exp_write_reg_noncacheable( card, bus, slave, reg, val, num_bytes ));
    }

    /* check the I/O cache to see if the value needs to be written or not */
    if ( status == 0 )
    {
        status = io_exp_read_cache( card, bus, slave, reg, buf, num_bytes);
    }

    /* if there was a cache hit, see if it matches what the caller intended to write */
    if ( status == 0 )
    {
        if ( 0 != memcmp( val, buf, num_bytes ) )
        {
            cache_invalidations++;
            debug_print_suppressible("Cache inv - (cache) != (desired) for reg x%02X on "
                                     "device %02X:%02X and card %u\n", reg, bus, slave, card);
            status = -EXDEV;
        }
        else
        {
            cache_hits++;
            debug_print_suppressible("Cache hit - Read from cache x%02X on device %02X:%02X and "
                                     "card %u:\n", reg, bus, slave, card);
            debug_hex_dump( buf, num_bytes );
        }
    }
    else
    {
        cache_misses++;
        debug_print_suppressible("Cache miss - Read from register x%02X on device %02X:%02X "
                                 "and card %u:\n", reg, bus, slave, card);
        debug_hex_dump( buf, num_bytes );
    }

    /* either a cache miss or cache invalidation means write the value to the I/O expander and
     * update the cache */
    if ( status == -EXDEV )
    {
        status = io_exp_write_reg_noncacheable( card, bus, slave, reg, val, num_bytes );
        if ( status == 0 )
        {
            // if successful, then write the cache
            status = io_exp_write_cache(card, bus, slave, reg, val, num_bytes);
        }
    }

    return status;
}


/**************************************************************************************************/
int32_t io_exp_read_reg_noncacheable( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint8_t val[],
                                      uint8_t num_bytes )
{
    int32_t status = 0;

    if (num_bytes > IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION)
    {
        skiq_error("Invalid I2C write. %u bytes specified, max is %u (card=%u)",
                   num_bytes, IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION, card);
        status = -EPROTO;
    }

    if ( status == 0 )
    {
        ARRAY_WITH_DEFAULTS(uint8_t, buf,
                            IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION + 1 /* nr_elem */,
                            0 /* default value */);

        // Write the register address to read from first then read 'num_bytes' from the register.
        status = hal_write_then_read_i2c_by_bus(card, bus, slave, reg, buf, num_bytes);

        if ( status == 0 )
        {
            memcpy( val, buf, num_bytes );
        }
    }

    return status;
}


/**************************************************************************************************/
int32_t io_exp_read_reg_cacheable( uint8_t card,
                                   uint8_t bus,
                                   uint8_t slave,
                                   uint8_t reg,
                                   uint8_t val[],
                                   uint8_t num_bytes )
{
    int32_t status = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, buf,
                        IO_EXP_MAX_NUM_BYTES_PER_TRANSACTION + 1 /* nr_elem */,
                        0 /* default value */);

    num_read_requests++;
    debug_print_suppressible("Read request for register x%02X on device %02X:%02X and card "
                             "%u\n", reg, bus, slave, card);

    if( enable_cache[card] == false )
    {
        cache_bypasses++;
        return (io_exp_read_reg_noncacheable(card, bus, slave, reg, val, num_bytes));
    }


    status = io_exp_read_cache(card, bus, slave, reg, buf, num_bytes);
    if (status == -EXDEV)
    {
        /* read directly from the I/O expander */
        status = io_exp_read_reg_noncacheable( card, bus, slave, reg, buf, num_bytes );

        /* update the I/O cache with the latest and greatest */
        if ( 0 == status )
        {
            io_exp_write_cache(card, bus, slave, reg, buf, num_bytes);
        }

        cache_misses++;
        debug_print_suppressible("Cache miss - Read from register x%02X on device %02X:%02X "
                                 "and card %u:\n", reg, bus, slave, card);
        debug_hex_dump( buf, num_bytes );
    }
    else if ( status == 0 )
    {
        cache_hits++;
        debug_print_suppressible("Cache hit -  Read from cache    x%02X on device %02X:%02X "
                                 "and card %u\n", reg, bus, slave, card);
        debug_hex_dump( buf, num_bytes );
    }

    if ( status == 0 )
    {
        memcpy( val, buf, num_bytes );
    }

    return status;
}


void io_exp_initialize_cache( uint8_t card)
{
    if (NULL == hash)
    {
       debug_print_suppressible("Init hash table\n");
       hash = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, NULL);
    }

    invalidate_cache(card);

    return;
}


void io_exp_enable_cache_validation( uint8_t card, bool enable )
{
    validate_cache[card] = enable;
    return;
}


void io_exp_free_cache_mem(void)
{
    if (hash != NULL)
    {
        io_exp_dump_stats();
        g_hash_table_destroy(hash);
        hash = NULL;
    }

    return;
}

void io_exp_enable_cache( uint8_t card, bool enable )
{
    enable_cache[card] = enable;
}