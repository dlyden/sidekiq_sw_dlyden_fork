/**
 * @file   gpsdo_fpga.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Fri Mar  6 14:15:57 CST 2020
 *
 * @brief   Implements an interface to the GPSDO implementation in the FPGA design
 *
 * @note    Most of the logic resides in the FPGA; for more information on the GPSDO algorithm
 *          being used (at time of writing), see:
 *          https://confluence.epiq.rocks/pages/viewpage.action?title=Sidekiq+Stretch+GPSDO+Status+and+Plans+as+of+September+2020&spaceKey=EN
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <errno.h>
#include <inttypes.h>

#include "gpsdo_fpga.h"
#include "sidekiq_fpga.h"       /* for sidekiq_fpga_reg_read() et al */
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_private.h"    /* for _skiq_meets_FPGA_VERSION */


/* enable debug_print and debug_print_plain when DEBUG_GPSDO_FPGA is defined */
#if (defined DEBUG_GPSDO_FPGA)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define WARP_VOLTAGE_OFFSET             (1 << (REG_RD_WARP_LEN - 1))

/** @todo   Is this a fixed value or something that should be read from the FPGA? */
#define FPGA_PLL_IN_MHZ                 (double)(270.0)

/***** MACROS *****/

/** @brief  Enable the GPSDO algorithm */
#define enable_gpsdo(_card)             control_gpsdo(_card, true)
/** @brief  Disable the GPSDO algorithm */
#define disable_gpsdo(_card)            control_gpsdo(_card, false)


/***** TYPEDEFS *****/


/***** STRUCTS *****/

static ARRAY_WITH_DEFAULTS(gpsdo_fpga_control_parameters_t, pid_params, SKIQ_MAX_NUM_CARDS,
                           GPSDO_FPGA_CONTROL_PARAMETERS_T_INITIALIZER);

/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/

/**
    @brief  Write the GPSDO control parameters to the FPGA

    @param[in]  card        The libsidekiq card to update

    @return 0 on success, else a negative errno
    @retval -EBADMSG    if any of the register transactions fail
*/
static int32_t write_gpsdo_parameters(uint8_t card)
{
    int32_t status = 0;
    uint32_t config1 = 0;
    uint32_t config2 = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPSDO_CONFIG1, &config1);
    if (0 != status)
    {
        skiq_error("[card %" PRIu8 "] failed to read GPSDO config1 register (result = %" PRIi32
            ")\n", card, status);
    }

    if (0 == status)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_GPSDO_CONFIG2, &config2);
        if (0 != status)
        {
            skiq_error("[card %" PRIu8 "] failed to read GPSDO config2 register (result = %" PRIi32
                ")\n", card, status);
        }
    }

    if (0 == status)
    {
        debug_print("[card %" PRIu8 "] current settings: Ki_start=%" PRIu8 ", Ki_end=%" PRIu8
            ", thresh_locked=%" PRIu8 ", thresh_unlocked=%" PRIu8 ", count_locked=%" PRIu8
            ", count_unlocked=%" PRIu8 ", time_in_stage=%" PRIu8 " warp_set_value=%" PRIu32 "\n",
            card,
            RBF_GET(config2, REG_KI_START), RBF_GET(config2, REG_KI_END),
            RBF_GET(config1, REG_THRESH_LOCKED), RBF_GET(config2, REG_THRESH_UNLOCKED),
            RBF_GET(config2, REG_COUNT_LOCKED), RBF_GET(config2, REG_COUNT_UNLOCKED),
            RBF_GET(config2, REG_TIME_IN_STAGE), RBF_GET(config1, REG_WARP_SET_VALUE));
    }

    if (0 == status)
    {
        RBF_SET(config2, pid_params[card].ki_start, REG_KI_START);
        RBF_SET(config2, pid_params[card].ki_end, REG_KI_END);
        RBF_SET(config2, pid_params[card].thresh_unlocked, REG_THRESH_UNLOCKED);
        RBF_SET(config2, pid_params[card].count_locked, REG_COUNT_LOCKED);
        RBF_SET(config2, pid_params[card].count_unlocked, REG_COUNT_UNLOCKED);
        RBF_SET(config2, pid_params[card].time_in_stage, REG_TIME_IN_STAGE);

        RBF_SET(config1, pid_params[card].thresh_locked, REG_THRESH_LOCKED);
        RBF_SET(config1, pid_params[card].warp_set_value, REG_WARP_SET_VALUE);

        debug_print("[card %" PRIu8 "] current settings: Ki_start=%" PRIu8 ", Ki_end=%" PRIu8
            ", thresh_locked=%" PRIu8 ", thresh_unlocked=%" PRIu8 ", count_locked=%" PRIu8
            ", count_unlocked=%" PRIu8 ", time_in_stage=%" PRIu8 " warp_set_value=%" PRIu32 "\n",
            card,
            RBF_GET(config2, REG_KI_START), RBF_GET(config2, REG_KI_END),
            RBF_GET(config1, REG_THRESH_LOCKED), RBF_GET(config2, REG_THRESH_UNLOCKED),
            RBF_GET(config2, REG_COUNT_LOCKED), RBF_GET(config2, REG_COUNT_UNLOCKED),
            RBF_GET(config2, REG_TIME_IN_STAGE), RBF_GET(config1, REG_WARP_SET_VALUE));
    }

    if (0 == status)
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_GPSDO_CONFIG1, config1);
        if (0 != status)
        {
            skiq_error("[card %" PRIu8 "] failed to write GPSDO config1 register (result = %"
                PRIi32 ")\n", card, status);
        }
    }

    if (0 == status)
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_GPSDO_CONFIG2, config2);
        if (0 != status)
        {
            skiq_error("[card %" PRIu8 "] failed to write GPSDO config2 register (result = %"
                PRIi32 ")\n", card, status);
        }
    }

    /* squash error messages from register transactions into -EBADMSG */
    if (0 != status)
    {
        status = -EBADMSG;
    }

    return status;
}


/**
    @brief  Set the enable bit on the GPSDO control

    @param[in]  card    The libsidekiq card to update
    @param[in]  enable  If true, enable the GPSDO; else disable it

    @return 0 on success, else a negative errno
    @retval -EBADMSG    if any of the register transactions fail
*/
static int32_t control_gpsdo(uint8_t card, bool enable)
{
    int32_t status = 0;

    debug_print("[card %" PRIu8 "] %s GPSDO module\n", card, (enable) ? "Enabling" : "Disabling");
    status = sidekiq_fpga_reg_RMW( card, enable ? 1 : 0, REG_ENABLE, FPGA_REG_GPSDO_CONFIG1 );

    /* squash error messages from register transactions into -EBADMSG */
    if ( status != 0 )
    {
        status = -EBADMSG;
    }

    return status;
}


/**
    @brief   Check if the hardware & FPGA support GPSDO functionality

    @param[in]  card    The libsidekiq card to query

    @return 0 on success, else a negative errno
    @retval -ENOTSUP    if the card does not have GPSDO support
    @retval -ENOSYS     if the GPSDO is not present
*/
static int32_t check_for_gpsdo(uint8_t card)
{
    int32_t status = -ENOTSUP;
    skiq_part_t part = _skiq_get_part( card );

    /* not all parts support GPSDO in the FPGA */
    if ( (skiq_m2_2280 == part) ||
         (skiq_z3u == part) ||
         (skiq_nv100 == part) )
    {
        status = 0;
    }

    /* after part check, then check the FPGA version */
    if ( status == 0 )
    {
        debug_print("Checking FPGA bitstream version for GPSDO module for %s on card %" PRIu8 "\n",
            part_cstr( _skiq_get_part( card ) ), card);
        if ( !_skiq_meets_FPGA_VERSION( card, FPGA_VERS_GPSDO ) )
        {
            debug_print("[card %" PRIu8 "] GPSDO module not present\n", card);
            status = -ENOSYS;
        }
        else
        {
            debug_print("[card %" PRIu8 "] GPSDO module found\n", card);
        }
    }

    return status;
}

/**
    @brief  Check if the GPSDO module is enabled

    @param[in]  card            The libsidekiq card to query
    @param[out] p_is_enabled    true if the GPSDO module is enabled, else false

    @note   This function assumes that the FPGA has a GPSDO module (and that its presence has
            already been checked).

    @return 0 on success, else a negative errno
    @retval -EBADMSG        if the status couldn't be read from the FPGA
*/
static int32_t
is_gpsdo_enabled(uint8_t card, bool *p_is_enabled)
{
    int32_t status = 0;
    uint32_t value = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPSDO_CONFIG1, &value);
    if (0 != status)
    {
        /* squash error messages from register transactions into -EBADMSG */
        status = -EBADMSG;
    }
    else
    {
        *p_is_enabled = ( RBF_GET( value, REG_ENABLE ) > 0 );
    }

    return status;
}


/*****************************************************************************/
/**
    @brief  Read the GPSDO algorithm's status registers
*/
static int32_t
read_status(uint8_t card, gpsdo_fpga_status_registers_t *p_status)
{
    int32_t status = 0;
    uint32_t value = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPSDO_STATUS, &value);
    if (0 != status)
    {
        debug_print("[card %" PRIu8 "] failed to read GPSDO status (result = %" PRIi32 ")\n",
                    card, status);

        /* squash error messages from register transactions into -EBADMSG */
        status = -EBADMSG;
    }
    else
    {
        if (NULL != p_status)
        {
            p_status->raw_value = value;
            p_status->gpsdo_locked_state = (RBF_GET(value, REG_RD_GPSDO_LOCKED) > 0);
            p_status->gps_has_fix = (RBF_GET(value, REG_RD_FIX_OUT) > 0);
            p_status->pll_locked_state = (RBF_GET(value, REG_RD_PLL_LOCKED) > 0);
            p_status->counter_unlocked = RBF_GET(value, REG_RD_COUNTER_UNLOCKED);
            p_status->counter_locked = RBF_GET(value, REG_RD_COUNTER_LOCKED);
            p_status->ki = RBF_GET(value, REG_RD_KI);
            p_status->stage_timer = RBF_GET(value, REG_RD_STAGE_TIMER);
        }
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/**
    @brief  Configures the GPSDO module with the default control values and enables the GPSDO
            algorithm control of the DCTCXO.

            The PPS_SEL signal is in the FPGA CTRL register which gets reset FPGA register 
            soft-reset on initialization & exit.                                                                                            
            This is a problem when we want to maintain the GPSDO lock.                      
            To solve this, we use a PPS_SEL override introduced in FPGA V3.16.3.            
            The override is a MUX that selects which PPS_SEL signal is used.                
            This MUX control selects which PPS_SEL is is used: GPSDO_CONFIG2 or FPGA CTRL.  
            Out of hard reset, the FPGA sets the PPS_SEL in GPSDO_CONFIG2 and FPGA CTRL     
            to the appropriate state depending on the SDR model.                            
            GPSDO_RESET_OVERRIDE: controls the MUX.                                             
            When the MUX selects GPSDO_CONFIG2, PPS_SEL_GPS controls the PPS source (1 = GPS)
*/
int32_t gpsdo_fpga_configure_and_enable(uint8_t card)
{
    int32_t status = 0;

    status = check_for_gpsdo( card );
    if(status == 0) 
    {
        // If the control algorithm is already locked, do not call write_gpsdo_parameters()
        gpsdo_fpga_status_registers_t gpsdo_status;
        status = read_status(card, &gpsdo_status);

        if(status == 0)
        {
            if(gpsdo_status.gpsdo_locked_state == false)
            {
                /* configure the GPSDO with the default PID values */
                printf("[card %" PRIu8 "] GPSDO module found, configuring\n", card);
                status = write_gpsdo_parameters( card );
                /* enable the GPSDO algorithm loop */
                if (0 == status)
                {
                    printf("[card %" PRIu8 "] enabling GPSDO module\n", card);
                    status = enable_gpsdo(card);
                    printf("[card %" PRIu8 "] GPSDO %s enabled (%" PRIi32 ")\n", card,
                        (0 == status) ? "was" : "WAS NOT", status);
                }
            }
            else
            {
                printf("[card %" PRIu8 "] GPSDO lock detected - skipping control algorithm reset\n", card);
            }
        }
        
        if ( status == 0 )
        {
            status = gpsdo_fpga_update_reset_override( card );
        }
    }

    return status;
}


/*****************************************************************************/
/**
    @brief   Provides indication whether or not the GPSDO is enabled in the FPGA design.
*/
int32_t gpsdo_fpga_is_enabled(uint8_t card, bool *p_is_enabled)
{
    int32_t status = 0;

    status = check_for_gpsdo( card );
    if (0 == status)
    {
        status = is_gpsdo_enabled(card, p_is_enabled);
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief  Provides the frequency accuracy (in ppm).
*/
int32_t gpsdo_fpga_get_freq_accuracy(uint8_t card, double *p_ppm, int32_t *p_freq_counter)
{
    int32_t status = 0;
    uint32_t value = 0;
    bool is_enabled = false;

    /*
        check if the GPSDO is enabled before attempting to observe frequency accuracy.  skip call
        to check_for_gpsdo() since gpsdo_fpga_is_enabled() performs that check.
    */
    status = gpsdo_fpga_is_enabled( card, &is_enabled );
    if ( status == 0 )
    {
        /* if the GPSDO isn't enabled, tell the caller that the feature is not available because the
         * GPSDO is not running */
        if ( !is_enabled )
        {
            status = -ESRCH;
        }
    }

    if ( status == 0 )
    {
        gpsdo_fpga_status_registers_t gpsdo_status;

        status = read_status(card, &gpsdo_status);
        if ( status == 0 )
        {
            /* If the GPSDO algorithm is not locked or if the GPS has no fix, inform the caller that
             * the accuracy is temporarily unavailable */
            if ( !gpsdo_status.gpsdo_locked_state || !gpsdo_status.gps_has_fix )
            {
                status = -EAGAIN;
            }
        }
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPSDO_READ_COUNTER, &value );
    }

    if ( status == 0 )
    {
        /*
            http://graphics.stanford.edu/~seander/bithacks.html#FixedSignExtend

            Perform a sign extension from REG_RD_COUNT_LEN bits to 32 bits
        */
        struct { int32_t freq_count:REG_RD_COUNT_LEN; } reg;
        int32_t freq_count;
        freq_count = reg.freq_count = RBF_GET( value, REG_RD_COUNT );

        debug_print("[card %" PRIu8 "] raw freq count: %" PRIi32 " at %" PRIu32 "\n", card,
            freq_count, RBF_GET(value, REG_RD_SECS));

        /*
            The FPGA has a PLL that clocks a register counting down from its frequency in Hertz.
            The freq_count field is the value in the register when the PPS occurs (e.g. the
            residual). Typically this value is between -10 and +10 once the GPSDO algorithm has
            converged.

            https://confluence.epiq.rocks/display/EN/Sidekiq+Stretch+GPSDO+Overview

            Convert freq_count to ppm (part per million) by dividing by the FPGA's PLL frequency in
            MHz
        */
        *p_ppm = (double)freq_count / (double)FPGA_PLL_IN_MHZ;

        *p_freq_counter = freq_count;
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief   Read the warp voltage that the GPSDO algorithm has written to the DCTCXO.
*/
int32_t gpsdo_fpga_read_warp_voltage(uint8_t card, uint32_t *p_warp_voltage)
{
    int32_t status;
    uint32_t value = 0;

    status = check_for_gpsdo( card );
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPSDO_READ_WARP, &value );
        if ( status != 0 )
        {
            /* squash error messages from register transactions into -EBADMSG */
            status = -EBADMSG;
        }
    }

    if ( status == 0 )
    {
        /*
            http://graphics.stanford.edu/~seander/bithacks.html#FixedSignExtend

            Perform a sign extension from `REG_RD_WARP_LEN` bits to 32 bits
        */
        struct { int32_t warp_voltage:REG_RD_WARP_LEN; } reg;
        reg.warp_voltage = RBF_GET( value, REG_RD_WARP );

        *p_warp_voltage = (uint32_t) (reg.warp_voltage + WARP_VOLTAGE_OFFSET);
    }

    return status;
}


/*****************************************************************************/
/**
    @brief  Read the GPSDO algorithm's operating counter registers
*/
int32_t gpsdo_fpga_read_counters(uint8_t card, gpsdo_fpga_counters_t *p_counters)
{
    int32_t status = 0;
    uint32_t value = 0;

    status = check_for_gpsdo( card );
    if (0 == status)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_GPSDO_READ_COUNTER, &value);
        if (0 != status)
        {
            /* squash error messages from register transactions into -EBADMSG */
            status = -EBADMSG;
        }
        else
        {
            /*
                http://graphics.stanford.edu/~seander/bithacks.html#FixedSignExtend

                Perform a sign extension from `REG_RD_COUNT_LEN` bits to 32 bits
            */
            struct { int32_t rd_count:REG_RD_COUNT_LEN; } reg;
            reg.rd_count = RBF_GET( value, REG_RD_COUNT );

            p_counters->seconds_counter = RBF_GET(value, REG_RD_SECS);
            p_counters->freq_counter = (int32_t) reg.rd_count;
        }
    }

    return status;
}


/*****************************************************************************/
/**
    @brief  Read the GPSDO algorithm's status registers
*/
int32_t gpsdo_fpga_read_status(uint8_t card, gpsdo_fpga_status_registers_t *p_status)
{
    int32_t status = 0;

    status = check_for_gpsdo(card);
    if (0 == status)
    {
        status = read_status(card, p_status);
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief  Set the GPSDO algorithm control parameters
*/
int32_t gpsdo_fpga_set_control_parameters( uint8_t card,
    const gpsdo_fpga_control_parameters_t *p_params)
{
    int32_t status = 0;
    bool is_enabled = false;

    /*
        check if the GPSDO is present before allowing it to be configured; skip the enable check
        as the user should be able to set parameters without the GPSDO being enabled.
    */
    status = check_for_gpsdo(card);
    if (0 == status)
    {
        /* set the parameters in the local structure */
        debug_print("[card %" PRIu8 "] Storing settings: Ki_start=%" PRIu8 ", Ki_end=%" PRIu8
            ", thresh_locked=%" PRIu8 ", thresh_unlocked=%" PRIu8 ", count_locked=%" PRIu8
            ", count_unlocked=%" PRIu8 ", time_in_stage=%" PRIu8 " warp_set_value=%" PRIu32 "\n",
            card, p_params->ki_start, p_params->ki_end, p_params->thresh_locked,
            p_params->thresh_unlocked, p_params->count_locked, p_params->count_unlocked,
            p_params->time_in_stage, p_params->warp_set_value);

        pid_params[card].ki_start = p_params->ki_start;
        pid_params[card].ki_end = p_params->ki_end;
        pid_params[card].thresh_locked = p_params->thresh_locked;
        pid_params[card].thresh_unlocked = p_params->thresh_unlocked;
        pid_params[card].count_locked = p_params->count_locked;
        pid_params[card].count_unlocked = p_params->count_unlocked;
        pid_params[card].time_in_stage = p_params->time_in_stage;
        pid_params[card].warp_set_value = p_params->warp_set_value;

        /* if the GPSDO is enabled, additionally set them in the FPGA module */
        status = is_gpsdo_enabled(card, &is_enabled);
        if ((0 == status) && (is_enabled))
        {
            status = write_gpsdo_parameters(card);
        }
    }

    return status;
}


/*****************************************************************************/
/**
    @brief  Enable the GPSDO control algorithm on the specified card.
*/
int32_t gpsdo_fpga_enable(uint8_t card)
{
    int32_t status = 0;

    status = check_for_gpsdo(card);

    /* enable the GPSDO algorithm loop */
    if (0 == status)
    {
        status = enable_gpsdo(card);
    }

    return status;
}


/*****************************************************************************/
/**
    @brief   Disables the GPSDO algorithm's control of the DCTCXO.
*/
int32_t gpsdo_fpga_disable(uint8_t card)
{
    int32_t status = 0;

    status = check_for_gpsdo(card);

    /* disable the GPSDO */
    if (0 == status)
    {
        status = disable_gpsdo(card);
    }

    if( status == 0 )
    {
        status = gpsdo_fpga_update_reset_override( card );
    }

    return status;
}


/*****************************************************************************/
/**
    @brief Indicates whether or not the specified card has a GPSDO module present in the FPGA
*/
int32_t gpsdo_fpga_has_module( uint8_t card,
                               bool *p_has_module,
                               int32_t *p_detailed_status )
{
    int32_t status = 0;

    status = check_for_gpsdo(card);
    if ( status == 0 )
    {
        *p_has_module = true;
        *p_detailed_status = 0;
    }
    else if ( ( status == -ENOTSUP ) || ( status == -ENOSYS ) )
    {
        *p_has_module = false;
        *p_detailed_status = status;
        status = 0;
    }

    return status;
}

int32_t gpsdo_fpga_update_reset_override( uint8_t card )
{
    int32_t status=0;
    bool is_enabled = false;
    uint32_t val=0;
    uint32_t pps_sel=0;

    status = is_gpsdo_enabled(card, &is_enabled);   

    if( status == 0 )
    {

        // update the override settings based on whether GPSDO is enabled or not
        if( is_enabled == true )
        {
            // figure out PPS_SEL setting 
            status = sidekiq_fpga_reg_read( card, FPGA_REG_FPGA_CTRL, &val );
            if( status == 0 )
            {
                if( RBF_GET( val, PPS_SEL ) > 0 )
                {
                    pps_sel = 1;
                }
                else
                {
                    pps_sel = 0;
                }
            }

            if( status == 0 )
            {
                status = sidekiq_fpga_reg_read( card, FPGA_REG_GPSDO_CONFIG2, &val );
            }

            // set the config fields and then the override
            if( status == 0 )
            {
                RBF_SET(val, pps_sel, PPS_SEL_GPS);
                RBF_SET(val, 1, MEMS_ENABLE_GPS );

                status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPSDO_CONFIG2, val );
            }

            if( status == 0 )
            {
                RBF_SET( val, 1, GPSDO_RESET_OVERRIDE );
                status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPSDO_CONFIG2, val );
            }
        }
        else
        {
            // GPSDO disabled, clear the reset override and MEMS_ENABLE_GPS

            status = sidekiq_fpga_reg_read( card, FPGA_REG_GPSDO_CONFIG2, &val );
            if( status == 0 )
            {
                RBF_SET( val, 0, GPSDO_RESET_OVERRIDE );
                status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPSDO_CONFIG2, val );
            }
            if( status == 0 )
            {
                RBF_SET(val, 0, MEMS_ENABLE_GPS );
                status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPSDO_CONFIG2, val );
            }
        }
    }

    if( status != 0 )
    {
        status = -EBADMSG;
    }

    return (status);
    
}