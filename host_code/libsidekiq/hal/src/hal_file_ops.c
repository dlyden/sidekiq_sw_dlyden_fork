/**
    @file   hal_file_ops.c
    @author <info@epiqsolutions.com>
    @date   Tue May 28 17:08:15 2019

    @brief  HAL file operations routines.

    <pre>
    Copyright 2019 Epiq Solutions, All Rights Reserved
    </pre>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>

#if (defined __MINGW32__)
#include <windows.h>
#endif

#include "hal_file_ops.h"
#include "sidekiq_private.h"

#if (defined DEBUG_HAL_FILE_OPS)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/**
    @brief  Verify that a FILE * points to a file or symlink
*/
int32_t
hal_file_is_file_or_link(FILE *fp, bool *p_is_valid)
{
    int tmpFd = -1;
    int result = 0;
    struct stat fileStat;
    bool isValid = false;

    if (NULL == fp)
    {
        return -EINVAL;
    }

    errno = 0;
    tmpFd = fileno(fp);
    if (-1 == tmpFd)
    {
        debug_print("FILE * is invalid (errno = %d)\n", errno);
        return -EBADF;
    }

    errno = 0;
    result = fstat(tmpFd, &fileStat);
    if (0 != result)
    {
        debug_print("Failed to get information on FILE * (errno = %d)\n", errno);
        return -ENOENT;
    }

#if (!defined __MINGW32__)
    if ((S_IFREG != (fileStat.st_mode & S_IFMT)) &&
        (S_IFLNK != (fileStat.st_mode & S_IFMT)))
#else
    /* Assume that it's not a symbolic link on Windows (for now). */
    if (S_IFREG != (fileStat.st_mode & S_IFMT))
#endif
    {
        debug_print("File is not a symlink or regular file (st_mode = %ld)\n",
            (long) fileStat.st_mode);
    }
    else
    {
        isValid = true;
    }

    if (NULL != p_is_valid)
    {
        *p_is_valid = isValid;
    }

    return result;
}

/**
    @brief  Verify that a file is present on a filesystem.
*/
int32_t
hal_file_is_present(const char *filename, bool *p_present)
{
    int32_t status = 0;
    int result = 0;
    bool present = false;
    struct stat fileStat;

    errno = 0;
    result = stat(filename, &fileStat);
    if (0 != result)
    {
        if (ENOENT == errno)
        {
            debug_print("File '%s' does not exist (errno %d)\n", filename, errno);
            present = false;
        }
        else
        {
            debug_print("Failed to stat() '%s' (errno %d)\n", filename, errno);
            status = -errno;
        }
    }
    else
    {
        debug_print("Found the file '%s'\n", filename);
        present = true;
    }

    if ((0 == status) && (NULL != p_present))
    {
        *p_present = present;
    }

    return status;
}

/**
    @brief  Remove a file from a filesystem
*/
int32_t
hal_file_remove(const char *filename)
{
    int result = 0;

    errno = 0;
    result = unlink(filename);
    if (0 != result)
    {
        if (ENOENT == errno)
        {
            debug_print("File '%s' doesn't exist; reporting success.\n",
                filename);
            result = 0;
        }
        else
        {
            debug_print("Failed to remove file '%s' (errno %d)\n",
                filename, errno);
            result = -errno;
        }
    }
    else
    {
        debug_print("Removed file '%s'\n", filename);
    }

    return ((int32_t) result);
}

/**
    @brief  Write a buffer to a file
*/
int32_t
hal_file_write_buffer(int fd, const char *p_buffer, uint32_t buffer_len)
{
    int32_t result = 0;
    ssize_t toWrite = (ssize_t) buffer_len;
    ssize_t totalWritten = 0;
    ssize_t numWritten = 0;
    ssize_t writeIdx = 0;

    while ((0 == result) && (toWrite > numWritten))
    {
        errno = 0;
        numWritten = write(fd, &(p_buffer[writeIdx]), toWrite - totalWritten);
        if (-1 == numWritten)
        {
            debug_print("Error while writing to file FD #%d (errno = %d)\n",
                fd, errno);
            result = -EIO;
        }
        else
        {
            totalWritten += numWritten;
            writeIdx += numWritten;
        }
    }

    return (result);
}

/**
    @brief  Write a string to a specified file
*/
int32_t
hal_file_write_string(const char *filename, int flags, const char *p_buffer, uint32_t buffer_len)
{
    int32_t result = 0;
    int fd = -1;

    errno = 0;
    fd = open(filename, flags);
    if (-1 == fd)
    {
        debug_print("Failed to open file '%s' for writing! (errno = %d)\n",
            filename, errno);
        result = -EBADF;
    }
    else
    {
        result = hal_file_write_buffer(fd, p_buffer, buffer_len);
    }

    /*
        The current code assumes that the CLOSE_FD_IF_OPEN() macro
        should use CloseHandle with a HANDLE value under MinGW... which
        would fail here as we're explicitly using a file descriptor.
    */
    if (-1 != fd)
    {
        close(fd);
        fd = -1;
    }

    debug_print("Finished writing file (result = %" PRIi32 ")!\n", result);

    return (result);
}

/**
    @brief  Copy a file from a source file handle to a destination file handle
*/
int32_t
hal_file_copy(int srcFd, int destFd)
{
#define READ_BUFFER_LEN (4096)
    int32_t status = 0;

    char *readBuffer = NULL;
    ssize_t numRead = 0;

    errno = 0;
    readBuffer = (char *) calloc(READ_BUFFER_LEN, sizeof(char));
    if (NULL == readBuffer)
    {
        skiq_error("Could not allocate memory for copy buffer; aborting");
        return (-ENOMEM);
    }

    numRead = READ_BUFFER_LEN;
    while ((0 == status) && (0 < numRead))
    {
        errno = 0;
        numRead = read(srcFd, &(readBuffer[0]), READ_BUFFER_LEN);
        if (-1 == numRead)
        {
            skiq_warning("Error while reading from user specified FILE handle"
                " (errno = %d)\n", errno);
            status = -EIO;
            continue;
        }

        status = hal_file_write_buffer(destFd, readBuffer, (uint32_t) numRead);
        if (0 != status)
        {
            skiq_warning("Error while writing to destination FILE handle (error code %"
                PRIi32 ")\n", status);
            status = -EIO;
            continue;
        }
    }

    FREE_IF_NOT_NULL(readBuffer);

    return (status);
#undef READ_BUFFER_LEN
}


/**
    @brief  Read a single line from the specified file handle
*/
int32_t
hal_file_read_line(int fd, char *p_read_buffer, uint32_t read_buffer_len, ssize_t *p_num_read)
{
    int32_t result = 0;
    ssize_t count = 0;
    ssize_t writeIdx = 0;
    bool found = false;
    bool eof = false;

    while ((0 == result) && ((read_buffer_len - 1) > writeIdx) && (!found) && (!eof))
    {
        errno = 0;
        count = read(fd, &(p_read_buffer[writeIdx]), 1);
        if (-1 == count)
        {
            debug_print("Failed to read byte from file (errno = %d)\n", errno);
            result = -EIO;
            continue;
        }
        else if (0 == count)
        {
            debug_print("End of file reached\n");
            eof = true;
        }

        if ((0x00 == p_read_buffer[writeIdx]) ||
            (0x0D == p_read_buffer[writeIdx]) ||
            (0x0A == p_read_buffer[writeIdx]))
        {
            found = true;
        }
        else
        {
            writeIdx++;
        }
    }

    if ((eof && !found) ||
        ((read_buffer_len - 1) == writeIdx))
    {
        result = -ERANGE;
    }
    else if (found)
    {
        p_read_buffer[writeIdx] = '\0';

        if (NULL != p_num_read)
        {
            *p_num_read = writeIdx;
        }
    }

    return (result);
}


/**
   @brief Determine the file stream's size
*/
long
hal_file_stream_size( FILE *fp )
{
    long file_size = (long)-EBADF;

    if ( fseek(fp, 0, SEEK_END) == 0 )
    {
        file_size = ftell(fp);
        rewind(fp);
    }

    return file_size;
}
