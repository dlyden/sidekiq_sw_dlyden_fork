/**
 * @file   hal_delay.c
 * @author  <info@epiq-solutions.com>
 * @date   Sun Nov 18 21:51:09 2018
 *
 * @brief  Implement an OS agnostic delay function
 *
 * <pre>
 * Copyright 2013-2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <sys/time.h>
#include <time.h>
#include <errno.h>

#ifdef __MINGW32__
#include <windef.h>
#include <windows.h>
#endif /* __MINGW32__ */

#include "sidekiq_private.h"
#include "hal_delay.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL FUNCTIONS *****/


#if (defined __MINGW32__)
/* https://stackoverflow.com/a/17283549 */
static int32_t win_usleep(__int64 usec)
{
    HANDLE timer = NULL;
    LARGE_INTEGER ft;
    int32_t status = 0;

    /* Convert to 100 nanosecond interval, negative value indicates relative time */
    ft.QuadPart = -(10*usec);

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    if ( NULL == timer )
    {
        status = GetLastError();
        skiq_error("Failed to create waitable timer, status = %d\n", status);
    }
    else
    {
        if ( SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0) == 0 )
        {
            status = GetLastError();
            skiq_error("Failed to set waitable timer duration, status = %d\n", status);
        }
        else
        {
            if ( WaitForSingleObject(timer, INFINITE) != WAIT_OBJECT_0 )
            {
                status = GetLastError();
                skiq_error("Failed to wait for timer to complete, status = %d\n", status);
            }
        }

        /* close timer handle */
        CloseHandle(timer);
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/

/**************************************************************************************************/
/** hal_nanosleep -- The Windows implementation of hal_nanosleep() that uses a combination of
    Sleep() and win_usleep() depending on the number of nanoseconds requested

   @return int32_t
   @retval 0 success
   @retval -ESRCH Error occurred while sleeping for specified number of nanoseconds
 */
/**************************************************************************************************/
int32_t hal_nanosleep(uint64_t num_nanosecs)
{
    int32_t status = 0;

    if ( num_nanosecs < MICROSEC )
    {
        /* if the user wants less than a microsecond, sleep for a microsecond, it's the finest
         * resolution supported by win_usleep() */
        status = win_usleep(1);
    }
    else if ( num_nanosecs >= MILLISEC )
    {
        /* if the user wants to sleep at least 1 millisecond, use Window's Sleep() function for the
         * big sleep, then win_usleep() for the remainder */
        Sleep( num_nanosecs / MILLISEC );
        status = win_usleep( num_nanosecs % MILLISEC );
    }
    else
    {
        /* between 1 microsecond and 1 millisecond, sleep for some number of microseconds, always
         * rounded up by one microsecond */
        status = win_usleep( ( num_nanosecs / MICROSEC ) + 1 );
    }

    if ( status != 0 )
    {
        /* collapse any error into a single error (No such process) for the caller */
        status = -ESRCH;
    }

    return status;
}

#else  /* defined __MINGW32__ */

/**************************************************************************************************/
/** hal_nanosleep -- The Linux implementation of hal_nanosleep() that uses nanosleep()

   @return int32_t
   @retval 0 success
   @retval -ESRCH Error occurred while sleeping for specified number of nanoseconds
 */
/**************************************************************************************************/
int32_t hal_nanosleep(uint64_t num_nanosecs)
{
    int32_t status = 0;

    if ( num_nanosecs > 0 )
    {
        struct timespec ts;

        ts.tv_sec = num_nanosecs / SEC;
        ts.tv_nsec = num_nanosecs % SEC;

        while (nanosleep(&ts, &ts) == -1)
        {
            /* if errno indicates any failure other than EINTR (interrupted system call), consider
             * it a catastrophic timer failure */
            if (errno != EINTR)
            {
                status = -errno;
                skiq_error("Catastrophic timer failure (errno = %d)!\n", errno);
                break;
            }
        }
    }

    if ( status != 0 )
    {
        /* collapse any error into a single error (No such process) for the caller */
        status = -ESRCH;
    }

    return status;
}

#endif  /* defined __MINGW32__ */


int32_t hal_microsleep(uint32_t num_microsecs)
{
    return hal_nanosleep((uint64_t)num_microsecs * MICROSEC);
}


int32_t hal_millisleep(uint32_t num_millisecs)
{
    return hal_nanosleep((uint64_t)num_millisecs * MILLISEC);
}


int32_t hal_sleep(uint32_t num_secs)
{
    return hal_nanosleep((uint64_t)num_secs * SEC);
}
