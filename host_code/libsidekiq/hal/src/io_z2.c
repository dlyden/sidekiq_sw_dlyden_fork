/**
 * @file   io_z2.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu May 10 13:38:14 2018
 *
 * @brief This file captures I/O functionality for the Sidekiq Z2 that is not implemented in
 * rfe_z2_b.c.  For example, access of the external I2C bus is provided by functions implemented
 * here.
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <errno.h>

#include "io_z2.h"
#include "io_expander_tca6408.h"

/***** DEFINES *****/

#define U21_EN_EXT_I2C_3V3_N_MASK       0x01 // P0


/***** TYPEDEFS *****/

typedef enum
{
    bus_enabled = 0,
    bus_disabled,

} bus_state_t;


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
/** configure_bus_access is a helper function that configures access to the external bus, but only
    if it is not already accessible.

    @param[in] card Sidekiq card index

    @return
 */
static int32_t configure_bus_access( uint8_t card,
                                     bus_state_t new_state )
{
    int32_t status = 0;

    switch (new_state)
    {
        case bus_enabled:
            if ( !io_z2_is_external_bus_accessible( card ) )
            {
                /* enable access to the external I2C bus */
                status = tca6408_io_exp_set_as_output( card, U21_IO_EXPANDER_ADDR,
                                                       U21_EN_EXT_I2C_3V3_N_MASK, 0 );
            }
            break;

        case bus_disabled:
            if ( io_z2_is_external_bus_accessible( card ) )
            {
                /* disable access to the external I2C bus */
                status = tca6408_io_exp_set_as_output( card, U21_IO_EXPANDER_ADDR,
                                                       U21_EN_EXT_I2C_3V3_N_MASK, ~0 );
            }
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** io_z2_enable_external_bus enables access to the external I2C bus on the Sidekiq Z2

    @param[in] card Sidekiq card index

    @return int32_t
    @retval 0 Success
    @retval -EIO Communications error with I/O expander
 */
int32_t io_z2_enable_external_bus_access( uint8_t card )
{
    return configure_bus_access( card, bus_enabled );
}


/**************************************************************************************************/
/** io_z2_disable_external_bus disables access to the external I2C bus on the Sidekiq Z2

    @param[in] card Sidekiq card index

    @return int32_t
    @retval 0 Success
    @retval -EIO Communications error with I/O expander
 */
int32_t io_z2_disable_external_bus_access( uint8_t card )
{
    return configure_bus_access( card, bus_disabled );
}


/**************************************************************************************************/
/** io_z2_is_external_bus_accessible queries to see whether or not the Z2 I2C external bus access is
    presently enabled

    @param[in] card Sidekiq card index

    @return bool
    @retval true Z2 I2C external bus is accessible
    @retval false Z2 I2C external bus is NOT accessible
 */
bool io_z2_is_external_bus_accessible( uint8_t card )
{
    int32_t status = 0;
    uint8_t direction = 0, value = 0;

    /* determine if bus is currently enabled or disabled */
    status = tca6408_io_exp_read_direction( card, U21_IO_EXPANDER_ADDR, &direction );
    if ( status == 0 )
    {
        if ( ( direction & U21_EN_EXT_I2C_3V3_N_MASK ) > 0 )
        {
            /* pin configured as input, so not accessible */
            return false;
        }

        /* determine if bus is currently enabled or disabled */
        status = tca6408_io_exp_read_output( card, U21_IO_EXPANDER_ADDR, &value );
    }

    return ( ( status == 0 ) && ( ( value & U21_EN_EXT_I2C_3V3_N_MASK ) == 0 ) );
}
