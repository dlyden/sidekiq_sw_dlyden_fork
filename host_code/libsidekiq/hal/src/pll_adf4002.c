/**
 * @file   pll_adf4002.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu May 16 09:59:46 CDT 2019
 *
 * @brief This file provides generic access to the ADF4002 Phase Detector / Frequency Synthesizer
 * populated on the Sidekiq M.2-2280.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <errno.h>

#include "pll_adf4002.h"

#include "sidekiq_private.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_ctrl.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_hal.h"


/* enable debug_print and debug_print_plain when DEBUG_PLL_ADF4002 is defined */
#if (defined DEBUG_PLL_ADF4002)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define PLL_SETTING(_enum,_regs)                \
    {                                           \
        .setting = (_enum),                     \
        .registers = (_regs),                   \
        .nr_registers = ARRAY_SIZE(_regs),      \
    }


/***** MACROS *****/

/* PLL SPI interface is running at 238.1kHz.  Each transaction is 8 bits.  A timeout of 100
 * microseconds is more than adequate for the transaction to complete. */
#define wait_for_ATTEN_STAT(_card)  fpga_ctrl_wait_for_bit(_card, 1, ATTEN_STAT_OFFSET, \
                                                           ATTEN_STAT_LEN, FPGA_REG_ATTEN_STAT, \
                                                           MICROSEC, 100 * MICROSEC)


/***** TYPEDEFS *****/

typedef struct
{
    pll_adf4002_setting_t setting;
    uint32_t *registers;
    uint8_t nr_registers;

} pll_setting;


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

/* Refer to https://confluence.epiq.rocks/display/EN/Sidekiq+Stretch+GPSDO+ADF4002+PLL+settings for
 * additional details */

static uint32_t pll_40MHz_registers[] =
{
    0x1f8093,
    0x1f8092,
    0x100004,
    0x000101,
};

static uint32_t pll_10MHz_registers[] =
{
    0x1f8093,
    0x1f8092,
    0x100004,
    0x000401,
};

static pll_setting pll_settings[] =
{
    PLL_SETTING(pll_adf4002_setting_40MHz, pll_40MHz_registers),
    PLL_SETTING(pll_adf4002_setting_10MHz, pll_10MHz_registers),
};


/***** LOCAL FUNCTION PROTOTYPES *****/


/***** LOCAL FUNCTIONS *****/

static int32_t
adf4002_reg_write( uint8_t card,
                   uint32_t data )
{
    int32_t status = 0;
    uint8_t i;

    /*
      data XXAABBCC
      byte  X 2 1 0
    */

    /**
       @note REF_SPI_PLL_LE in FPGA design is output-only
    */
    debug_print("Starting to write 0x%08X to PLL on card %u\n", data, card);
    status = sidekiq_fpga_reg_RMWV( card, 0, REF_SPI_PLL_LE, FPGA_REG_GPIO_WRITE );
    for ( i = 0; ( i < 3 ) && ( status == 0 ); i++ )
    {
        uint32_t value = 0;
        uint8_t byte = (data >> 16) & 0xFF;

        debug_print("Writing 0x%02X to PLL on card %u\n", byte, card);
        RBF_SET(value, byte, ATTEN_DATA);
        status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_ATTEN_DATA, value);
        if ( status == 0 )
        {
            status = wait_for_ATTEN_STAT(card);
            if ( status != 0 )
            {
                skiq_error("Failed to write to reference PLL IC on card %u, aborting\n", card);
                hal_critical_exit(-1);
            }
        }

        data <<= 8;
    }

    if ( status == 0 )
    {
        /**
           @note REF_SPI_PLL_LE in FPGA design is output-only
        */
        status = sidekiq_fpga_reg_RMWV( card, 1, REF_SPI_PLL_LE, FPGA_REG_GPIO_WRITE );
    }

    if ( status == 0 )
    {
        /* Table 2 of ADF4002 datasheet, LE must remain high for at least 20ns before sending
         * the next control */
        hal_nanosleep(21);
    }

    return status;
}


static int32_t
adf4002_configure( uint8_t card,
                   pll_adf4002_setting_t setting )
{
    int32_t status = 0;
    uint8_t i, reg;

    for ( i = 0; ( i < ARRAY_SIZE(pll_settings) ) && ( status == 0 ); i++ )
    {
        if ( pll_settings[i].setting == setting )
        {
            for ( reg = 0; ( reg < pll_settings[i].nr_registers ) && ( status == 0 ); reg++ )
            {
                status = adf4002_reg_write( card, pll_settings[i].registers[reg] );
            }

            break;
        }
    }

    return status;
}

/***** GLOBAL FUNCTIONS *****/


int32_t pll_adf4002_configure( uint8_t card,
                               pll_adf4002_setting_t setting )
{
    int32_t status = 0;

    switch (setting)
    {
        case pll_adf4002_setting_10MHz:
        case pll_adf4002_setting_40MHz:
            status = adf4002_configure(card, setting);
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}
