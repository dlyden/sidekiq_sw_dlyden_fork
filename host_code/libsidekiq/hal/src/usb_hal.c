/**
 * @file   usb_hal.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2017 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
#include <unistd.h>
#include <string.h>

#include <usb_hal.h>
#include <usb_interface.h>
#include <usb_private.h>
#include <prog_fpga.h>
#include <sidekiq_hal.h>
#include <flash_defines.h>

#include "sidekiq_xport.h"
#include "sidekiq_private.h"

/** @brief  A simple macro for comparing the firmware version number. */
// Fast FPGA reprogramming wasn't supported until firmware 1.14.  We need to make
// sure the firmware is at least that firmware version otherwise we need to use legacy mode
#define FAST_FPGA_PROGRAMMING_FW_MAJ_REQ (1)
#define FAST_FPGA_PROGRAMMING_FW_MIN_REQ (14)

/* @brief these definitions dictate the number of times we retry programming the
 * FPGA (either from file or flash) and the number of times we call sidekiq_xport_fpga_up. */
#define MAX_NUM_PROG_RETRIES            (2)
#define MAX_NUM_FPGA_UP_RETRIES         (20)
#define FPGA_UP_RETRY_DELAY_MS          (5) /* 5 milliseconds */

#define USB_EEPROM_BLOCK_SIZE           (64)

int32_t usb_hal_read_fw_version( uint8_t card, uint8_t *p_ver_maj, uint8_t *p_ver_min )
{
    int32_t status;
    uint8_t index=0;
    uint32_t serial_num;
    uint16_t board;

    // we need to translate from card to UID to index
    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_read_version(index, p_ver_maj, p_ver_min, &serial_num, &board);
    }

    return (status);
}


int32_t usb_hal_read_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status=0;
    uint8_t index=0;

    // we need to translate from card to UID to index
    if( (status=usb_get_index(card, &index)) == 0 )
    {
        while ( ( status == 0 ) && ( num_bytes > 0 ) )
        {
            uint16_t bytes_to_read = MIN(USB_EEPROM_BLOCK_SIZE,num_bytes);
            status = usb_read_eeprom_contents( index, addr, bytes_to_read, p_data);
            addr += bytes_to_read;
            num_bytes -= bytes_to_read;
            p_data += bytes_to_read;
        }
    }

    return (status);
}

int32_t usb_hal_write_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status=0;
    uint8_t index=0;

    // we need to translate from card to UID to index
    if( (status=usb_get_index(card, &index)) == 0 )
    {
        while ( ( status == 0 ) && ( num_bytes > 0 ) )
        {
            uint16_t bytes_to_write = MIN(USB_EEPROM_BLOCK_SIZE,num_bytes);
            status = usb_write_eeprom_contents( index, addr, bytes_to_write, p_data);
            addr += bytes_to_write;
            num_bytes -= bytes_to_write;
            p_data += bytes_to_write;
        }
    }
    
    return (status);
}

int32_t usb_hal_write_i2c(uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes)
{
    int32_t status=0;
    uint8_t index=0;
    
    // we need to translate from card to UID to index
    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_write_i2c( index, addr, p_data, num_bytes );
    }

    return (status);
}

int32_t usb_hal_write_then_read_i2c( uint8_t card,
                                     uint8_t periph_addr,
                                     uint8_t reg_addr,
                                     uint8_t* p_data,
                                     uint8_t num_bytes )
{
    int32_t status=0;
    uint8_t index=0;
    
    // we need to translate from card to UID to index
    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_write_then_read_i2c( index, periph_addr, reg_addr, p_data, num_bytes );
    }
    return (status);
}

static int32_t
_write_i2c_by_bus( uint8_t card,
                   uint8_t bus,
                   uint8_t addr,
                   uint8_t* p_data,
                   uint8_t num_bytes )
{
    int32_t status = -ENOTSUP;

    /* The USB HAL implementation only supports a bus index of 0, all others are unsupported */
    if ( bus == 0 )
    {
        status = usb_hal_write_i2c( card, addr, p_data, num_bytes );
    }

    return (status);
}

static int32_t
_write_then_read_i2c_by_bus( uint8_t card,
                        uint8_t bus,
                        uint8_t periph_addr,
                        uint8_t reg_addr,
                        uint8_t* p_data,
                        uint8_t num_bytes)
{
    int32_t status = -ENOTSUP;

    /* The USB HAL implementation only supports a bus index of 0, all others are unsupported */
    if ( bus == 0 )
    {
        status = usb_hal_write_then_read_i2c( card, periph_addr, reg_addr, p_data, num_bytes );
    }

    return (status);
}

int32_t usb_hal_prog_fpga_from_file(uint8_t card, FILE *fp)
{
    int32_t status=0;
    uint8_t prog_retries;
    uint8_t legacy_method = 1; // default to use legacy method
    uint8_t fw_maj = 0;
    uint8_t fw_min = 0;
    uint32_t serial_num=0;
    uint16_t board=0;
    uint8_t index = 0;

    // we need to translate from card to UID to index
    if( (status=usb_get_index(card, &index)) != 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to access USB interface, unable to reprogram FPGA from file\n");
        return (status);
    }

    status = usb_read_version(index, &fw_maj, &fw_min, &serial_num, &board);
    if ( 0 == status )
    {
        // determine if we can fast program
        if ( FW_VERSION(fw_maj, fw_min) >= FW_VERSION(FAST_FPGA_PROGRAMMING_FW_MAJ_REQ, FAST_FPGA_PROGRAMMING_FW_MIN_REQ) )
        {
            legacy_method = 0;
        }

        // bring down the FPGA interface
        status = sidekiq_xport_fpga_down( card );
        if ( status != 0 )
        {
            _skiq_log(SKIQ_LOG_WARNING, "%s:%d FPGA transport did not stop properly, a host reboot may be required to restore FPGA transport\n",
                    __FUNCTION__, __LINE__);
        }
    }

    /* try program_fpga_from_file up to MAX_NUM_PROG_RETRIES */
    if ( 0 == status )
    {
        prog_retries = MAX_NUM_PROG_RETRIES;
        do
        {
            rewind(fp);
            status = program_fpga_from_file(index, fp, legacy_method);
            if ( 0 == status )
            {
                status = sidekiq_fpga_bring_up( card );
            }
            prog_retries--;

        } while ( ( status != 0 ) && ( prog_retries > 0 ) );

        if ( status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR, "Programming FPGA from file failed after %d tries\n", MAX_NUM_PROG_RETRIES);
        }
        else if ( prog_retries < MAX_NUM_PROG_RETRIES - 1 )
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Programming FPGA from file took %d tries\n", MAX_NUM_PROG_RETRIES - prog_retries);
        }
    }

    return (status);
}

/** USB transport does not support a simple read over I2C, the register address
    to start reading from needs to be written first.  In this case,
    write_then_read_i2c is implemented to be more explicit about the actual
    behavior and read_i2c is left unimplemented.
 */
hal_functions_t usb_hal = {
    .read_fw_version            = usb_hal_read_fw_version,
    .read_eeprom                = usb_hal_read_eeprom,
    .write_eeprom               = usb_hal_write_eeprom,
    .write_i2c                  = usb_hal_write_i2c,
    .read_i2c                   = NULL,
    .write_then_read_i2c        = usb_hal_write_then_read_i2c,
    .prog_fpga_from_file        = usb_hal_prog_fpga_from_file,
    .read_i2c_by_bus            = NULL,
    .write_i2c_by_bus           = _write_i2c_by_bus,
    .write_then_read_i2c_by_bus = _write_then_read_i2c_by_bus,
    .read_fmc_eeprom            = NULL,
    .write_fmc_eeprom           = NULL,
};
