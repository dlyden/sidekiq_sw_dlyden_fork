/**
 * @file   xilinx_fpga_bitstream.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Aug  8 10:53:56 CDT 2019
 *
 * @brief
 *
 * <pre>
 * Copyright 2019-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#if (!defined _GNU_SOURCE)
#define _GNU_SOURCE             /* for memmem */
#endif
#include <string.h>

#include "sidekiq_types.h"
#include "sidekiq_hal.h"
#include "sidekiq_private.h"

/* enable debug_print and debug_print_plain when DEBUG_XILINX_FPGA_BITSTREAM is defined */
#if (defined DEBUG_XILINX_FPGA_BITSTREAM)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define PARTIAL_STRING                  "PARTIAL=TRUE" /* Xilinx bitstreams that are PR will have
                                                        * this string in their .bit header */
#define NR_BYTES_SEARCH                 1024

#define FPGA_STRING(_p,_n)              { .part = (_p), .name = #_n }

/** @brief Calls memmem() to find a string (_str,len) in a block of memory defined by
 * (_data,_nr_words).  However, _nr_words is limited to a maximum of NR_BYTES_SEARCH, regardless of
 * what's passed to the macro.
 *
 * @note both _data and _str need to be non-NULL pointers and _nr_words must be strictly greater
 * than zero.
 */
#define find_string(_data,_nr_words,_str)                               \
    memmem( _data, MIN(_nr_words, NR_BYTES_SEARCH), _str, strlen(_str) )

/** @brief Calls memmem() to find a needle pattern (_patt,len) in a block of memory defined by
 * (_data,_nr_words).  However, _nr_words is limited to a maximum of NR_BYTES_SEARCH, regardless of
 * what's passed to the macro.
 *
 * @note both _data and _patt need to be non-NULL pointers and _nr_words must be strictly greater
 * than zero.
 */
#define find_patt(_data,_nr_words,_patt)                                \
    memmem( _data, MIN(_nr_words, NR_BYTES_SEARCH), _patt, ARRAY_SIZE(_patt) )


/***** TYPE DEFINITIONS *****/



/***** STRUCTS *****/

struct fpga_string
{
    skiq_part_t part;
    const char *name;
};

/***** LOCAL DATA *****/

static struct fpga_string fpga_bitfile_strings[] =
{
    FPGA_STRING(skiq_m2_2280, 7a50tcpg236),
    FPGA_STRING(skiq_nv100, 7a50tcpg236),
};

/***** LOCAL FUNCTIONS *****/

#if defined(WIN32) || defined(__MINGW32__)
// The function memmem is missing from Windows (appears to be in newlib). We'll
// implement it here for the time being.

/* Byte-wise substring search, using the Two-Way algorithm.
 * Copyright (C) 2008 Eric Blake
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
static void *memmem(const void *haystack,
                    size_t haystacklen,
                    const void *needle,
                    size_t needlelen)
{
    /* Abstract memory is considered to be an array of 'unsigned char' values,
    not an array of 'char' values.  See ISO C 99 section 6.2.6.1.  */

    if (needlelen == 0)
    {
        /* The first occurrence of the empty string is deemed to occur at
        the beginning of the string.  */
        return (void *) haystack;
    }

    /* Less code size, but quadratic performance in the worst case.  */
    while (needlelen <= haystacklen)
    {
        if (!memcmp (haystack, needle, needlelen))
        {
            return (void *) haystack;
        }

        haystack++;
        haystacklen--;
    }
    return NULL;
}
#endif


/***** GLOBAL FUNCTIONS *****/


bool
xilinx_fpga_bitstream_is_bitfile( uint8_t card,
                                  const void *data,
                                  uint32_t nr_words )
{
    uint8_t i;
    bool bitfile = true;
    skiq_part_t part;
    const char *search_string = NULL;

    if ( NULL == data )
    {
        skiq_error("FPGA bitstream data reference is NULL for file type check on card %u, "
                   "rejecting input\n", card);
        return false;
    }

    if ( 0 == nr_words )
    {
        skiq_error("FPGA bitstream length is zero for file type check on card %u, rejecting "
                   "input\n", card);
        return false;
    }

    part = _skiq_get_part( card );
    for ( i = 0; ( i < ARRAY_SIZE(fpga_bitfile_strings) ) && ( search_string == NULL ); i++ )
    {
        if ( part == fpga_bitfile_strings[i].part )
        {
            search_string = fpga_bitfile_strings[i].name;
        }
    }

    if ( NULL == search_string )
    {
        debug_print("No search string associated with Sidekiq %s on card %u\n", part_cstr( part ),
                    card);
        bitfile = false;
    }
    else if ( NULL == find_string( data, nr_words, search_string) )
    {
        debug_print("Search string '%s' not found in bitstream provided for card %u\n",
                    search_string, card);
        bitfile = false;
    }

    return bitfile;
}


bool
xilinx_fpga_bitstream_is_partial( uint8_t card,
                                  const void *data,
                                  uint32_t nr_words )
{
    bool partial_file = true;

    if ( NULL == data )
    {
        skiq_error("FPGA bitstream data reference is NULL for partial check on card %u, rejecting "
                   "input\n", card);
        return false;
    }

    if ( 0 == nr_words )
    {
        skiq_error("FPGA bitstream length is zero for partial check on card %u, rejecting "
                   "input\n", card);
        return false;
    }

    if ( NULL == find_string( data, nr_words, PARTIAL_STRING ) )
    {
        partial_file = false;
    }

    return partial_file;
}


int32_t
xilinx_fpga_bitstream_word_align( uint8_t card,
                                  uint32_t *data,
                                  uint32_t nr_words )
{
    int32_t status = 0;
    void *sync_start;

    if ( NULL == data )
    {
        skiq_error("FPGA bitstream data reference is NULL for word alignment on card %u, rejecting "
                   "input\n", card);
        status = -EINVAL;
    }

    if ( 0 == nr_words )
    {
        skiq_error("FPGA bitstream length is zero for word alignment on card %u, rejecting "
                   "input\n", card);
        status = -EINVAL;
    }

    if ( 0 == status )
    {
        sync_start = find_patt(data, nr_words, fpga_sync_word);
        if ( sync_start != NULL )
        {
            uint8_t adjust;
            uint8_t sync_alignment, data_alignment;

            /* Data must be 32-bit aligned with the sync word before being passed to the ICAP
             * interface.  This function serves to "trim" 0 to 3 bytes from the beginning of `data`
             * in order for the syncword to align on a 32-bit boundary. */

            sync_alignment = ((uintptr_t)sync_start & 0x3);
            data_alignment = ((uintptr_t)data & 0x3);

            /* adjust is the difference between the syncword's byte alignment and the data's byte
             * alignment, accounting for subtraction underflow */
            adjust = ((sync_alignment + 4) - data_alignment) % 4;

            /* Instead of "trimming", just move the data+offset to data */
            memmove( data,
                     (((uint8_t*)data) + adjust),
                     (sizeof(uint32_t) * nr_words) - adjust );
        }
        else
        {
            status = -EINVAL;
        }
    }

    return status;
}
