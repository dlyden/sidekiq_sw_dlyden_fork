/**
 * @file   dctcxo_sit5356.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Mon May 13 12:51:39 CDT 2019
 *
 * @brief This file provides access to the TCXO (SiT5356) I2C device.
 *
 * Datasheet is available:
 * - on the web -- https://www.sitime.com/datasheet/SiT5356
 * - on the network -- /mnt/storage/datasheets/SiTime/SiT5356-rev0.60_180228.pdf
 *
 * <pre>
 * Copyright 2019-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include "dctcxo_sit5356.h"

#include "sidekiq_hal.h"

/* enable debug_print and debug_print_plain when DEBUG_DCTCXO_SIT5356 is defined */
#if (defined DEBUG_DCTCXO_SIT5356)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define SIT5356_I2C_ADDR                0x62

#define DCTCXO_FREQ_CTRL_LSW_REG        0x00 /* 16 bits wide, sent [15:8], [7:0] */
#define DCTCXO_FREQ_CTRL_MSW_REG        0x01 /* 16 bits wide, sent [15:8], [7:0] */

#define WARP_VOLTAGE_UNSIGNED_WIDTH     26
#define WARP_VOLTAGE_UNSIGNED_MAX       ((1 << WARP_VOLTAGE_UNSIGNED_WIDTH) - 1)
#define WARP_VOLTAGE_OFFSET             (1 << (WARP_VOLTAGE_UNSIGNED_WIDTH - 1))


/***** TYPEDEFS *****/

typedef union
{
    /* The frequency control word spans two registers, this struct represents the entire control
     * word */
    struct {
        int32_t freq_control_word:26;
        uint32_t oe_control:1;
        uint32_t unused:5;
    };

    /* This 'raw' value is used when debugging, to show the entire value without cast */
    uint32_t raw;

} freq_ctrl_reg_t;


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static const i2c_device_t dctcxo_list[] = {
    I2C_DEVICE(skiq_m2_2280, 2, SIT5356_I2C_ADDR), /* on M.2-2280, I2C_2, 100kHz bus */
    I2C_DEVICE(skiq_z3u,     2, SIT5356_I2C_ADDR), /* on Z3u, I2C_2, 100kHz bus */
    I2C_DEVICE(skiq_nv100,   2, SIT5356_I2C_ADDR), /* on NV100, I2C_2, 100kHz bus */
};
static const uint8_t nr_dctcxo = ARRAY_SIZE(dctcxo_list);


/***** LOCAL FUNCTION PROTOTYPES *****/


/***** LOCAL FUNCTIONS *****/



/*************************************************************************************************/
/** Returns the ::i2c_device_t associated with the card, if any
 *
 * @retval NULL card has no associated DCTCXO
 * @retval ptr reference to a ::i2c_device_t
 */
static i2c_device_t *
get_dctcxo( uint8_t card )
{
    skiq_part_t part = _skiq_get_part( card );
    i2c_device_t *ic = NULL;
    uint8_t i;

    for ( i = 0; i < nr_dctcxo; i++ )
    {
        if ( part == dctcxo_list[i].part )
        {
            ic = (i2c_device_t *)&(dctcxo_list[i]);
            break;
        }
    }

    return ic;
}


static int32_t
read_sit5356_reg( uint8_t card,
                  i2c_device_t *dev,
                  uint8_t reg,
                  uint16_t *p_data )
{
    int32_t status;
    uint8_t data[2];

    status = hal_write_then_read_i2c_by_bus( card, dev->bus, dev->addr, reg, data, 2 );
    if ( status == 0 )
    {
        /* read configuration registers for SiT5356, data is received MSB: [15:8] then [7:0] */
        *p_data = (data[0] << 8) | data[1];
    }

    return status;
}

static int32_t
write_sit5356_reg( uint8_t card,
                   i2c_device_t *dev,
                   uint8_t reg,
                   uint16_t data )
{
    int32_t status;
    uint8_t i2c_data[3];

    /* write configuration registers for SiT5356, data is sent MSB: [15:8] then [7:0] */
    i2c_data[0] = reg;
    i2c_data[1] = (data >> 8) & 0xFF;
    i2c_data[2] = (data >> 0) & 0xFF;

    status = hal_write_i2c_by_bus( card, dev->bus, dev->addr, i2c_data, 3 );

    return status;
}


/***** GLOBAL FUNCTIONS *****/


int32_t
sit5356_dctcxo_read_warp_voltage( uint8_t card,
                                  uint32_t *p_warp_voltage )
{
    int32_t status = 0;
    uint16_t lsw = 0, msw = 0;
    freq_ctrl_reg_t reg = { .raw = 0 };
    i2c_device_t *dev = get_dctcxo( card );

    if ( dev == NULL )
    {
        status = -EFAULT;
    }

    if ( status == 0 )
    {
        status = read_sit5356_reg( card, dev, DCTCXO_FREQ_CTRL_LSW_REG, &lsw );
    }

    if ( status == 0 )
    {
        status = read_sit5356_reg( card, dev, DCTCXO_FREQ_CTRL_MSW_REG, &msw );
    }

    if ( status == 0 )
    {
        reg.raw = (msw << 16) | lsw;
        debug_print("raw 0x%08X, freq_control_word: %d, oe_control: %u\n", reg.raw,
                    reg.freq_control_word, reg.oe_control);
        *p_warp_voltage = (uint32_t)reg.freq_control_word + WARP_VOLTAGE_OFFSET;
    }

    return status;
}


int32_t
sit5356_dctcxo_write_warp_voltage( uint8_t card,
                                   uint32_t warp_voltage )
{
    int32_t status = 0;
    int32_t warp_voltage_signed;
    int16_t lsw = 0, msw = 0;
    freq_ctrl_reg_t reg = { .raw = 0 };
    i2c_device_t *dev = get_dctcxo( card );

    if ( dev == NULL )
    {
        status = -EFAULT;
    }

    if ( status == 0 )
    {
        /* bound and translate warp voltage from unsigned value to a signed value centered around
         * zero */
        warp_voltage = MIN(warp_voltage, WARP_VOLTAGE_UNSIGNED_MAX);
        warp_voltage_signed = (int32_t)warp_voltage - WARP_VOLTAGE_OFFSET;

        /* set the fields of the register */
        reg.freq_control_word = warp_voltage_signed;
        reg.oe_control = 1;
        debug_print("raw 0x%08X, freq_control_word: %d, oe_control: %u\n", reg.raw,
                    reg.freq_control_word, reg.oe_control);

        /* split the register into the least and most significant words */
        lsw = (uint16_t)(reg.raw & 0xFFFF);
        msw = (uint16_t)( ( reg.raw >> 16 ) & 0xFFFF);
    }

    if ( status == 0 )
    {
        status = write_sit5356_reg( card, dev, DCTCXO_FREQ_CTRL_LSW_REG, lsw );
    }

    if ( status == 0 )
    {
        status = write_sit5356_reg( card, dev, DCTCXO_FREQ_CTRL_MSW_REG, msw );
    }

    return status;
}
