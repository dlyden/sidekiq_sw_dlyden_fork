/**
 * @file   hal_nv100.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Fri Sep 18 21:02:43 CDT 2020
 *
 * @brief This file captures functionality for the Sidekiq NV100 that is not implemented in
 * rfe_nv100.c.
 *
 * Reference(s):
 * - https://confluence.epiq.rocks/display/EN/Sidekiq+Stretch+2+Clock+Reference+Options
 * - https://confluence.epiq.rocks/display/EN/Sidekiq+Stretch2+FPGA+Development
 * - /mnt/storage/projects/sidekiq_stretch2/docs/nv100_icd_v0.1.0_build5.pdf
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <errno.h>
#include <inttypes.h>
#include <math.h>

#include "hal_nv100.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_private.h"
#include "sidekiq_rfic.h"
#include "io_expander_pcal6524.h"
#include "pll_adf4002.h"
#include "card_services.h"
#include "hal_delay.h"
#include "sidekiq_hal.h"

/* enable debug_print and debug_print_plain when DEBUG_HAL_NV100 is defined */
#if (defined DEBUG_HAL_NV100)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/* Lock detect delay at 100ms as recommended by Epiq RF engineering team */
#define NV100_LOCK_DETECT_DELAY_MS      100

/*
   Reference: /mnt/storage/projects/sidekiq_stretch2/docs/nv100_icd_v0.1.0_build5.pdf
   Release 0.1.0
   August 5th, 2021
 */

#define NV100_IOE_I2C_BUS               0
#define NV100_IOE_I2C_ADDR              0x22

#define REF_EDGE                        PORT(0,0)
#define REF_WFL                         PORT(0,1)
#define REF_INT_MEMS                    PORT(0,2)
#define PLL_BYPASS                      PORT(0,3)
#define DEVCLK_SEL_N                    PORT(0,4)
#define EEPROM_WP                       PORT(0,5)
#define RFIC_RESET_N                    PORT(0,6)
#define PLL_LOCK_DETECT                 PORT(0,7)

#define NV100_REF_CTRL_MASK             (REF_EDGE |     \
                                         REF_WFL |      \
                                         REF_INT_MEMS | \
                                         PLL_BYPASS |   \
                                         DEVCLK_SEL_N)


/***** MACROS *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/*****************************************************************************/
static int32_t
check_pll_lock_detect( uint8_t card )
{
    int32_t status = 0;

    /* Check the PLL_LD port after some settling time */
    hal_millisleep( NV100_LOCK_DETECT_DELAY_MS );

    status = pcal6524_io_exp_set_as_input( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                           PLL_LOCK_DETECT );
    if ( status == 0 )
    {
        uint32_t input;

        status = pcal6524_io_exp_read_input( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR, &input );
        if ( ( status == 0 ) && ( ( input & PLL_LOCK_DETECT ) == 0 ) )
        {
            skiq_error( "Failed to detect PLL lock after %u milliseconds on card %" PRIu8 "\n",
                        NV100_LOCK_DETECT_DELAY_MS, card );
            status = -ETIMEDOUT;
            hal_critical_exit(status);
        }
    }

    return status;
}


/*****************************************************************************/
static int32_t
nv100_select_internal_ref_clock( uint8_t card )
{
    int32_t status;

    /* assert the REF_INT_MEMS port in the NV100_REF_CTRL_MASK group */
    status = pcal6524_io_exp_set_as_output( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                            NV100_REF_CTRL_MASK, REF_INT_MEMS );

    /* configure the on-board ADF4002 PLL to 1x when using the internal reference clock */
    if ( status == 0 )
    {
        status = pll_adf4002_configure( card, pll_adf4002_setting_40MHz );
    }

    if ( status == 0 )
    {
        status = check_pll_lock_detect( card );
    }

    return status;
}


/*****************************************************************************/
/**
   @note The NV100 design names the host reference signal as REF_EDGE because it arrives on the edge
   trace of the card.
 */
static int32_t
nv100_select_host_ref_clock( uint8_t card,
                             uint32_t ref_clk_freq )
{
    int32_t status = 0;
    uint32_t value = 0;

    /* choose PLL_BYPASS if the requested frequency is 40MHz */
    switch (ref_clk_freq)
    {
        case SIGNAL_40MHZ:
            value = PLL_BYPASS;
            break;

        case SIGNAL_10MHZ:
            value = 0;
            break;

        default:
            /* unsupported external reference frequency */
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        /* assert the REF_EDGE port in the NV100_REF_CTRL_MASK group */
        status = pcal6524_io_exp_set_as_output( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                                NV100_REF_CTRL_MASK, REF_EDGE | value );
    }

    if ( status == 0 )
    {
        /* configure the on-board ADF4002 PLL to 4x if the requested reference clock frequency is
         * 10MHz, if the requested frequency is not 10MHz then the PLL is bypassed */
        if ( ref_clk_freq == SIGNAL_10MHZ )
        {
            status = pll_adf4002_configure( card, pll_adf4002_setting_10MHz );
            if ( status == 0 )
            {
                /* check for PLL lock detect when using an external 10MHz reference */
                status = check_pll_lock_detect( card );
            }
        }
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/

/**************************************************************************************************/
/* nv100_select_external_ref_clock() performs the necessary steps to select an external reference
   clock source for a given NV100 card and sets the pll based on the input frequency. */
/**************************************************************************************************/
int32_t nv100_select_external_ref_clock( uint8_t card,
                                         uint32_t ref_clk_freq )
{
    int32_t status = 0;
    uint32_t value = 0;

    /* choose PLL_BYPASS if the requested frequency is 40MHz */
    switch (ref_clk_freq)
    {
        case SIGNAL_40MHZ:
            value = PLL_BYPASS;
            break;

        case SIGNAL_10MHZ:
            value = 0;
            break;

        default:
            /* unsupported external reference frequency */
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        /* assert the REF_WFL port in the NV100_REF_CTRL_MASK group */
        status = pcal6524_io_exp_set_as_output( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                                NV100_REF_CTRL_MASK, REF_WFL | value );
    }

    if ( status == 0 )
    {
        /* configure the on-board ADF4002 PLL to 4x if the requested reference clock frequency is
         * 10MHz, if the requested frequency is not 10MHz then the PLL is bypassed */
        if ( ref_clk_freq == SIGNAL_10MHZ )
        {
            status = pll_adf4002_configure( card, pll_adf4002_setting_10MHz );
            if ( status == 0 )
            {
                /* check for PLL lock detect when using an external 10MHz reference */
                status = check_pll_lock_detect( card );
            }
        }
    }

    return status;
}


/**************************************************************************************************/
/* nv100_select_ref_clock() performs the necessary steps to select the reference clock source for a
   given NV100 card. */
/**************************************************************************************************/
int32_t nv100_select_ref_clock( uint8_t card )
{
    int32_t status = 0;
    skiq_ref_clock_select_t ref_clock;
    uint32_t ref_clk_freq = 0;

    status = skiq_read_ref_clock_select( card, &ref_clock );
    if ( status == 0 )
    {
        if ( ( ref_clock == skiq_ref_clock_external ) ||
             ( ref_clock == skiq_ref_clock_host ) )
        {
            status = card_read_ext_ref_clock_freq(card, &ref_clk_freq);
        }
    }

    if ( status == 0 )
    {
        switch (ref_clock)
        {
            case skiq_ref_clock_host:
                status = nv100_select_host_ref_clock( card, ref_clk_freq );
                break;

            case skiq_ref_clock_external:
                status = nv100_select_external_ref_clock( card, ref_clk_freq );
                break;

            case skiq_ref_clock_internal:
                status = nv100_select_internal_ref_clock( card );
                break;

            default:
                status = -EPROTO;
        }
    }

    return status;
}


/**************************************************************************************************/
/* nv100_read_temp() performs the necessary steps to measure the temperature sensor for a given
   NV100 card through the RFIC's AuxADC that is connected to a TMP61 Linear Thermistor */
/**************************************************************************************************/
int32_t nv100_read_temp( uint8_t card,
                         uint8_t sensor,
                         int8_t *p_temp_in_deg_C )
{
    int32_t status = -EINVAL;

    if ( sensor == 0 )
    {
        uint16_t voltage_mV;
        rfic_t rfic;

        rfic = _skiq_get_generic_rfic_instance(card);

        status = rfic_read_auxadc( &rfic, sensor, &voltage_mV );
        if ( status == 0 )
        {
            double r_tmp61 = 0.0, v_tmp61, temperature_float;
            double r_bias, v_bias;
            double a4, a3, a2, a1, a0;

            /* Reference: NV100 rev C schematics */
            r_bias = 10e3;
            v_bias = 1.8;

            /* These coefficients come from the Thermistor Design Tool - Voltage Bias on the "4th
               Order Polynomial Regression" sheet when the above r_bias and v_bias are used */
            a4 = -3.516134E-15;
            a3 = 1.968827E-10;
            a2 = -4.433964E-06;
            a1 = 5.958913E-02;
            a0 = -2.871303E+02;

            v_tmp61 = (double)voltage_mV / 1000.0;

            /* Reference: TMP61 datasheet, page 1, v_tmp61 equation solved for r_tmp61 */
            if ( v_bias != v_tmp61 )
            {
                r_tmp61 = (v_tmp61 * r_bias) / (v_bias - v_tmp61);
            }

            debug_print("v_tmp61 = %f V, r_tmp61 = %f Ohm\n", v_tmp61, r_tmp61);

            /* This equation comes from the Thermistor Design Tool - Voltage Bias on the "4th
               Order Polynomial Regression" sheet and uses the measured r_tmp61 (by way of the
               AuxADC) and the pre-determined coefficients */
            temperature_float =
                a4 * pow(r_tmp61, 4) + a3 * pow(r_tmp61, 3) +
                a2 * pow(r_tmp61, 2) + a1 * r_tmp61 + a0;

            temperature_float = MAX(temperature_float, INT8_MIN);
            temperature_float = MIN(temperature_float, INT8_MAX);
            *p_temp_in_deg_C = (int8_t)temperature_float;
        }
    }

    return status;
}

/**************************************************************************************************/
/* nv100_set_eeprom_wp() performs the necessary steps to enable or disable the EEPROM write protect
   as line as specified by @p write_protect for a given NV100 card. */
/**************************************************************************************************/
int32_t nv100_set_eeprom_wp( uint8_t card,
                             bool write_protect )
{
    int32_t status = 0;
    uint32_t direction = 0, value = 0;
    const uint32_t high_value = EEPROM_WP, low_value = 0;

    status = pcal6524_io_exp_set_as_output( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                            EEPROM_WP, write_protect ? high_value : low_value );

    /* After setting the EEPROM_WP pin as output at the desired level, perform validation on pin
     * direction AND pin value */
    if ( status == 0 )
    {

        status = pcal6524_io_exp_read_direction( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                                 &direction );
    }

    if ( status == 0 )
    {

        status = pcal6524_io_exp_read_output( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                              &value );
    }

    if ( status == 0 )
    {
        direction &= EEPROM_WP;
        debug_print("EEPROM_WP is set as an %s (raw:%u) on card %" PRIu8 "\n",
                    ( direction == low_value ) ? "output" : "input", direction, card);

        /* pin directions (0=output, 1=input) */
        if ( direction != low_value )
        {
            status = -EIO;
            skiq_error("EEPROM WP pin direction is not output as expected on card %" PRIu8 "\n",
                       card);
        }
    }

    if ( status == 0 )
    {
        value &= EEPROM_WP;
        debug_print("EEPROM_WP is set %s (raw:%u) on card %" PRIu8 "\n",
                    ( value == low_value ) ? "LOW" : "HIGH", value, card);

        /* if WP is requested, but the value is low, then it's considered a validation error  */
        if ( write_protect && ( value == low_value ) )
        {
            status = -EIO;
            skiq_error("EEPROM WP pin value is not HIGH as expected on card %" PRIu8 "\n",
                       card);
        }
        /* -OR- if WP is not requested, but the value is high, then it's considered a validation
         * error */
        else if ( !write_protect && ( value == high_value ) )
        {
            status = -EIO;
            skiq_error("EEPROM WP pin value is not LOW as expected on card %" PRIu8 "\n",
                       card);
        }
    }

    return status;
}

/**************************************************************************************************/
/* nv100_set_rfic_reset() performs the necessary steps to enable or disable the RFIC_RESET_N line
   as specified by @p rfic_reset for a given NV100 card. */
/**************************************************************************************************/
int32_t nv100_set_rfic_reset( uint8_t card,
                              bool rfic_reset )
{
    int32_t status = 0;
    bool rfic_reset_n = !rfic_reset;

    status = pcal6524_io_exp_set_as_output( card, NV100_IOE_I2C_BUS, NV100_IOE_I2C_ADDR,
                                            RFIC_RESET_N, rfic_reset_n ? RFIC_RESET_N : 0 );

    return status;
}
