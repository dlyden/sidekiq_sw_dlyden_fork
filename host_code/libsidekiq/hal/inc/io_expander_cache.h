#ifndef __IO_EXPANDER_CACHE_H__
#define __IO_EXPANDER_CACHE_H__

/**
 * @file   io_expander_cache.h
 * @author  <dan@epiq-solutions.com>
 * @date   Wed Jun 24 13:28:45 CDT 2019
 *
 * @brief This file provides a caching mechanism used to reduce the number I/O expander reads.
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/
#include <stdint.h>
#include <stdbool.h>




/**************************************************************************************************/
/** Helper function used to write directly to the IO expander register, without caching the value

    @param[in] card Sidekiq card index
    @param[in] bus I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val array of bytes to write to the specified register address
    @param[in] num_bytes number of valid bytes in @a val

    @return int32_t status where 0=success, anything else is an error
    @retval -EINVAL out-of-range number of bytes requested
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EIO cache validation failed (only relevant if enabled)
    @retval -EPROTO Internal error
*/
int32_t io_exp_write_reg_noncacheable( uint8_t card,
                                       uint8_t bus,
                                       uint8_t slave,
                                       uint8_t reg,
                                       uint8_t val[],
                                       uint8_t num_bytes );

/**************************************************************************************************/
/** Helper function used to read from the IO expander register, allowing lookup in cache

    @param[in] card Sidekiq card index
    @param[in] bus I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val array to store retrieved bytes
    @param[in] num_bytes number of bytes to read and store in @a val

    @return int32_t status where 0=success, anything else is an error
    @retval -EINVAL out-of-range number of bytes requested
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EIO cache validation failed (only relevant if enabled)
    @retval -EPROTO Internal error
*/
int32_t io_exp_read_reg_noncacheable( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint8_t reg,
                                      uint8_t val[],
                                      uint8_t num_bytes );

/**************************************************************************************************/
/** Helper function used to write to the IO expander register, allowing lookup in cache before
    writing and/or caching the written value(s)

    @param[in] card Sidekiq card index
    @param[in] bus I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val array of bytes to write to the specified register address
    @param[in] num_bytes number of valid bytes in @a val

    @return int32_t status where 0=success, anything else is an error
    @retval -EINVAL out-of-range number of bytes requested
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EIO cache validation failed (only relevant if enabled)
    @retval -EPROTO Internal error
*/
int32_t io_exp_write_reg_cacheable( uint8_t card,
                                    uint8_t bus,
                                    uint8_t slave,
                                    uint8_t reg,
                                    uint8_t val[],
                                    uint8_t num_bytes );

/**************************************************************************************************/
/** Helper function used to read from the IO expander register, allowing lookup in cache before
    reading

    @param[in] card Sidekiq card index
    @param[in] bus I2C bus
    @param[in] slave I2C slave address
    @param[in] reg Register address
    @param[in] val array of bytes to write to the specified register address
    @param[in] num_bytes number of valid bytes in @a val

    @return int32_t status where 0=success, anything else is an error
    @retval -EINVAL out-of-range number of bytes requested
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EIO cache validation failed (only relevant if enabled)
    @retval -EPROTO Internal error
*/
int32_t io_exp_read_reg_cacheable( uint8_t card,
                                   uint8_t bus,
                                   uint8_t slave,
                                   uint8_t reg,
                                   uint8_t val[],
                                   uint8_t num_bytes );

/**************************************************************************************************/
/** io_exp_initialize_cache initalizes the hash table used in the io_expander caching and
    invalidates any cachhed entries for the specified card.  This function should be
    called before performing a i2c operations on designs containing io_expanders.  (m2.2280, z2)

    @param[in]  card Sidekiq card index

    @return void

 */
void io_exp_initialize_cache( uint8_t card);

/**************************************************************************************************/
/** io_exp_enable_cache_validation configurures the cache to validate itself.  Upon a cache hit,
    the reads will be performed on i2c to confirm the actual value is consistent with the cached

    @param[in]  card Sidekiq card index
    @param[in]  enable flag indicating whether to validate the cache
 
    @return void

 */
void io_exp_enable_cache_validation(uint8_t card, bool enable);

/**************************************************************************************************/
/** io_exp_enable_cache provides the ability to disable the IO expander cache entirely.  As a result
    every register read and write will result in an access of the hardware.

    @param[in]  card Sidekiq card index
    @param[in]  enable flag indicating whether to validate the cache
 
    @return void

 */
void io_exp_enable_cache( uint8_t card, bool enable );

/**************************************************************************************************/
/** io_exp_free_cache_mem performs the teardown and frees any memory allocated for caching.  This
    should be called on shutdown

    @param[in]  card Sidekiq card index

    @return void

 */
void io_exp_free_cache_mem(void);


#endif
