/**
 * @file   io_expander_tca6408.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu May 10 14:21:01 2018
 *
 * @brief This file provides generic access to the TCA6408ARSVR I/O expander populated on the
 * Sidekiq Z2.
 *
 * Datasheet is available:
 * - on the web -- https://www.ti.com/lit/ds/symlink/tca6408a.pdf
 * - on the network -- /mnt/storage/datasheets/TexasInstruments/tca6408a.pdf
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __IO_EXPANDER_TCA6408_H__
#define __IO_EXPANDER_TCA6408_H__

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>

/***** DEFINES *****/

#define IO_EXPANDER_ALL_PINS            ~((uint8_t)0)

/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

/**************************************************************************************************/
/** tca6408_io_exp_set_as_input() sets the pins specified by 'pin_mask' as inputs and also sets
    their output values to low.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_as_input( uint8_t card,
                                     uint8_t slave,
                                     uint8_t pin_mask );

/**************************************************************************************************/
/** tca6408_io_exp_set_pin_polarity() sets the pin polarity.  The pins specified by 'pin_mask' are
    inverted according to 'invert_mask' where a 1 means to invert while a 0 means not to invert.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] invert_mask Inversion bitmask that specifies whether or not a pin's value should be inverted

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_pin_polarity( uint8_t card,
                                         uint8_t slave,
                                         uint8_t pin_mask,
                                         uint8_t invert_mask );

/**************************************************************************************************/
/** tca6408_io_exp_set_as_output() configures the pins specified by 'pin_mask' as outputs and sets
    their voltage levels according to 'value_mask'.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] value_mask Bitmask that specifies pin output levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_as_output( uint8_t card,
                                      uint8_t slave,
                                      uint8_t pin_mask,
                                      uint8_t value_mask );

/**************************************************************************************************/
/** tca6408_io_exp_set_as_hiz() configures the pins specified by 'pin_mask & hiz_mask' as inputs
    (Hi-Z) and pins specified by 'pin_mask & ~hi_mask` as high outputs

    @attention Pins NOT indicated in @a hiz_mask are configured as outputs and their values are set
    to high

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] hiz_mask Bitmask that specifies which pins to configure as Hi-Z

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_set_as_hiz( uint8_t card,
                                   uint8_t slave,
                                   uint8_t pin_mask,
                                   uint8_t hiz_mask );

/**************************************************************************************************/
/** tca6408_io_exp_read_output() queries the output pin level.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[out] p_value_mask reference to uint8_t to be populated with the output pin levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_read_output( uint8_t card,
                                    uint8_t slave,
                                    uint8_t *p_value_mask );

/**************************************************************************************************/
/** tca6408_io_exp_read_direction() queries the pin direction.

    @param[in] card Sidekiq card index
    @param[in] slave I2C slave address
    @param[out] p_dir_mask reference to uint8_t to be populated with the pin directions (0=output, 1=input)

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t tca6408_io_exp_read_direction( uint8_t card,
                                       uint8_t slave,
                                       uint8_t *p_dir_mask );

/**************************************************************************************************/
/** tca6408_io_exp_initialize_cache() invalidates a cache of the i2c slave register values.  A
    majority of registers only change state when written to by libsidekiq, so upon writing, a cache
    is populated.  This function should be called on skiq_init() and clears all previously cached
    values.

    @param[in]  card Sidekiq card index
    @return void

 */
void tca6408_io_exp_initialize_cache( uint8_t card );

void tca6408_io_exp_enable_cache_validation( bool enabled );

#endif  /* __IO_EXPANDER_TCA6408_H__ */
