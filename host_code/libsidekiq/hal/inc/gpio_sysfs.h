#ifndef __GPIO_SYSFS_H__
#define __GPIO_SYSFS_H__

/*****************************************************************************/
/** @file gpio_sysfs.h
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the controlling GPIOs
    through the sysfs interface.
*/

#include <stdint.h>

typedef enum
{
    gpio_ddr_in=0,
    gpio_ddr_out
} gpio_ddr_t;

/*****************************************************************************/
/** The gpio_sysfs_enable_ctrl function is responsible for enabling sysfs 
    control for the GPIO specified.  After this function completes successfully, 
    there should be a sysfs entry available for the GPIO specified.

    @param[in] card card number of interest
    @param[in] gpio_class class number of GPIO sysfs entry
    @param[in] gpio_offset specific offset into GPIO controller

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t gpio_sysfs_enable_ctrl( uint8_t card, uint32_t gpio_class, uint32_t gpio_offset );

/*****************************************************************************/
/** The gpio_sysfs_disable_ctrl function is responsible for disabling sysfs
    control for the GPIO specified.  After this function completes successfully,
    the sysfs entry for the GPIO specified should be removed.

    @param[in] card card number of interest
    @param[in] gpio_class class number of GPIO sysfs entry
    @param[in] gpio_offset specific offset into GPIO controller

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t gpio_sysfs_disable_ctrl( uint8_t card, uint32_t gpio_class, uint8_t gpio_offset );

/*****************************************************************************/
/** The gpio_sysfs_configure_direction function is responsible for configuring 
    the direction (input or output) for the specified GPIO.

    @param[in] card card number of interest
    @param[in] gpio_class class number of GPIO sysfs entry
    @param[in] gpio_offset specific offset into GPIO controller
    @param[in] dir direction of the GPIO (input or output)

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t gpio_sysfs_configure_direction( uint8_t card,
                                        uint32_t gpio_class,
                                        uint32_t gpio_offset,
                                        gpio_ddr_t dir );

/*****************************************************************************/
/** The gpio_sysfs_read_value function is responsible for reporting the current
    value of the GPIO specified.

    @param[in] card card number of interest
    @param[in] gpio_class class number of GPIO sysfs entry
    @param[in] gpio_offset specific offset into GPIO controller
    @param[out] p_value pointer to where to store the value of the GPIO

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t gpio_sysfs_read_value( uint8_t card,
                               uint32_t gpio_class,
                               uint32_t gpio_offset,
                               uint8_t *p_value );

/*****************************************************************************/
/** The gpio_sysfs_write_value function is responsible for configuring the 
    value of the GPIO specified.

    @param[in] card card number of interest
    @param[in] gpio_class class number of GPIO sysfs entry
    @param[in] gpio_offset specific offset into GPIO controller
    @param[in] value value of the GPIO

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t gpio_sysfs_write_value( uint8_t card,
                                uint32_t gpio_class,
                                uint32_t gpio_offset,
                                uint8_t value );

#endif // __GPIO_SYSFS_H__
