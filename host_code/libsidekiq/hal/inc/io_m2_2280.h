/**
 * @file   io_m2_2280.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Apr 17 13:17:14 CDT 2019
 *
 * @brief This file captures I/O functionality for the Sidekiq M.2-2280 that is not implemented in
 * rfe_m2_2280_ab.c.  For example, access to the EEPROM's write protection line is provided by
 * functions implemented here.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __IO_M2_2280_H__
#define __IO_M2_2280_H__

/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_types.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/


/**************************************************************************************************/
/** io_disable_eeprom_wp() disables the write protection of the EEPROM on the Sidekiq M.2-2280

    @param[in] card Sidekiq card index of interest

    @return int32_t

    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t io_disable_eeprom_wp( uint8_t card );


/**************************************************************************************************/
/** io_enable_eeprom_wp() enables the write protection of the EEPROM on the Sidekiq M.2-2280

    @param[in] card Sidekiq card index of interest

    @return int32_t

    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t io_enable_eeprom_wp( uint8_t card );


/**************************************************************************************************/
/** m2_2280_io_set_int_ref_clock_source() sets and clears GPO and sets up the on-board PLL to
    configure an internal reference clock on the Sidekiq M.2-2280.

    @param[in] card Sidekiq card index of interest

    @return int32_t

    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t m2_2280_io_set_int_ref_clock_source( uint8_t card );


/**************************************************************************************************/
/** m2_2280_io_set_ext_ref_clock_source() sets and clears GPO and sets up the on-board PLL to
    configure an external reference clock on the Sidekiq M.2-2280 at the specified clock frequency.

    @param[in] card Sidekiq card index of interest
    @param[in] ref_clk_freq reference clock frequency

    @return int32_t

    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -EINVAL Specified ref_clk_freq is not a valid value
 */
int32_t m2_2280_io_set_ext_ref_clock_source( uint8_t card,
                                             uint32_t ref_clk_freq );

#endif  /* __IO_M2_2280_H__ */
