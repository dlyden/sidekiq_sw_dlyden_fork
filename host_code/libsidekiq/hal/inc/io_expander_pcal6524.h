/**
 * @file   io_expander_pcal6524.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Apr 17 13:28:45 CDT 2019
 *
 * @brief This file provides generic access to the PCAL6524EV I/O expander populated on the
 * Sidekiq M.2-2280.
 *
 * Datasheet is available:
 * - on the web -- https://www.nxp.com/docs/en/data-sheet/PCAL6524.pdf
 * - on the network -- /mnt/storage/datasheets/NXP/PCAL6524.pdf
 *
 * <pre>
 * Copyright 2019-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __IO_EXPANDER_PCAL6524_H__
#define __IO_EXPANDER_PCAL6524_H__

/***** INCLUDES *****/

#include <stdint.h>

/***** DEFINES *****/

#define IO_EXPANDER_ALL_PINS            ~((uint32_t)0)

#define NUM_PORT_BITS                   8
#define PORT(_bank,_pin)                (1 << ((_bank) * NUM_PORT_BITS + (_pin)))

/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

/**************************************************************************************************/
/** pcal6524_io_exp_set_as_input() sets the pins specified by 'pin_mask' as inputs and also sets
    their output values to low.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t pcal6524_io_exp_set_as_input( uint8_t card,
                                      uint8_t bus,
                                      uint8_t slave,
                                      uint32_t pin_mask );

/**************************************************************************************************/
/** pcal6524_io_exp_set_as_output() configures the pins specified by 'pin_mask' as outputs and sets
    their voltage levels according to 'value_mask'.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] value_mask Bitmask that specifies pin output levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t pcal6524_io_exp_set_as_output( uint8_t card,
                                       uint8_t bus,
                                       uint8_t slave,
                                       uint32_t pin_mask,
                                       uint32_t value_mask );

/**************************************************************************************************/
/** pcal6524_io_exp_set_as_hiz() configures the pins specified by 'pin_mask & hiz_mask' as inputs
    (Hi-Z) and pins specified by 'pin_mask & ~hiz_mask & high_mask` as high outputs and pins
    specified by 'pin_mask & ~hiz_mask & ~high_mask' as low outputs

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[in] pin_mask Pin bitmask that specifies pins on which to operate
    @param[in] hiz_mask Bitmask that specifies which pins to configure as Hi-Z
    @param[in] high_mask Bitmask that specifies which pins to configure as high outputs

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EPROTO if @a hiz_mask has bits asserted outside of @a pin_mask
    @retval -EPROTO if @a high_mask has bits asserted outside of @a pin_mask
    @retval -EPROTO if @a hiz_mask and @a high_mask have asserted bits in common
 */
int32_t pcal6524_io_exp_set_as_hiz( uint8_t card,
                                    uint8_t bus,
                                    uint8_t slave,
                                    uint32_t pin_mask,
                                    uint32_t hiz_mask,
                                    uint32_t high_mask );

/**************************************************************************************************/
/** pcal6524_io_exp_read_output() queries the output pin level.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[out] p_value_mask reference to uint32_t to be populated with the output pin levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t pcal6524_io_exp_read_output( uint8_t card,
                                     uint8_t bus,
                                     uint8_t slave,
                                     uint32_t *p_value_mask );

/**************************************************************************************************/
/** pcal6524_io_exp_read_input() queries the input pin level.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[out] p_value_mask reference to uint32_t to be populated with the input pin levels

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t pcal6524_io_exp_read_input( uint8_t card,
                                    uint8_t bus,
                                    uint8_t slave,
                                    uint32_t *p_value_mask );

/**************************************************************************************************/
/** pcal6524_io_exp_read_direction() queries the pin direction.

    @param[in] card Sidekiq card index
    @param[in] bus IO Expander I2C bus
    @param[in] slave I2C slave address
    @param[out] p_dir_mask reference to uint32_t to be populated with the pin directions (0=output, 1=input)

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t pcal6524_io_exp_read_direction( uint8_t card,
                                        uint8_t bus,
                                        uint8_t slave,
                                        uint32_t *p_dir_mask );

#endif  /* __IO_EXPANDER_PCAL6524_H__ */
