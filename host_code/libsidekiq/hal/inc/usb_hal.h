/**
 * @file   usb_hal.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef USB_HAL_H
#define USB_HAL_H

#include <stdint.h>
#include <stdio.h>

#include "sidekiq_hal.h"

extern hal_functions_t usb_hal;

/* offer this function to fpga_reg_hal for use when FX2 support is available */
extern int32_t usb_hal_read_fw_version( uint8_t card, uint8_t *p_ver_maj, uint8_t *p_ver_min );
extern int32_t usb_hal_read_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );
extern int32_t usb_hal_write_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );
extern int32_t usb_hal_prog_fpga_from_file( uint8_t card, FILE *fp );
extern int32_t usb_hal_write_i2c( uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes );
extern int32_t usb_hal_write_then_read_i2c( uint8_t card,
                                            uint8_t periph_addr,
                                            uint8_t reg_addr,
                                            uint8_t* p_data,
                                            uint8_t num_bytes );

#endif
