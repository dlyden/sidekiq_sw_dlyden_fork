/**
 * @file    zynq_hal.h
 * @author  <info@epiqsolutions.com>
 * @date    Tue May 28 17:04:37 2019
 *
 * @brief
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __ZYNQ_HAL_H__
#define __ZYNQ_HAL_H__

#include <stdint.h>

#include "sidekiq_hal.h"

int32_t zynq_hal_prog_fpga_from_file(uint8_t card, FILE *fp);

extern hal_functions_t zynq_hal;

#endif /* __ZYNQ_HAL_H__ */

