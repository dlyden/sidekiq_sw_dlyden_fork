/**
    @file   hal_file_ops.h
    @author <info@epiqsolutions.com>
    @date   Wed Jun 19 13:58:00 2019

    @brief  Header file for hal_file_ops.c

    <pre>
    Copyright 2019 Epiq Solutions, All Rights Reserved
    </pre>
*/

#ifndef __HAL_FILE_OPS_H_
#define __HAL_FILE_OPS_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

/**
    @brief  Verify that a FILE * points to a file or symlink

    @param[in]  fp          A valid file pointer
    @param[out] p_is_valid  If not NULL, indicates the file pointer points a file or symlink

    @retval 0           on success
    @retval -EINVAL     if @p fp is NULL
    @retval -EBADF      if @p fp is invalid
    @retval -ENOENT     if information on the file pointer couldn't be retrieved
*/
int32_t
hal_file_is_file_or_link(FILE *fp, bool *p_is_valid);

/**
    @brief  Verify that a file is present on a filesystem.

    @param[in]  filename        The name of the file to check
    @param[out] p_present       If not NULL, true if the file exists else false.

    @return 0 on success, else a negative errno
*/
int32_t
hal_file_is_present(const char *filename, bool *p_present);

/**
    @brief  Remove a file from a filesystem

    @param[in]  filename        The name of the file to remove

    @note   If @a filename doesn't exist, the success code is returned.

    @return 0 on success, else a negative errno.
*/
int32_t
hal_file_remove(const char *filename);

/**
    @brief  Write a buffer to a file

    @param[in]  fd          The file descriptor to write to
    @param[in]  buffer      The buffer to be written to the file descriptor
    @param[in]  bufferLen   The length of @p buffer (in bytes)

    @retval 0       on success
    @retval -EIO    if writing to the file descriptor failed for any reason
*/
int32_t
hal_file_write_buffer(int fd, const char *p_buffer, uint32_t buffer_len);

/**
    @brief  Write a string to a specified file

    @param[in]  filename        The name of the file to write to
    @param[in]  flags           The flags to use when opening the file (see the @a open()
                                command for more details; generally this is O_WRONLY)
    @param[in]  p_buffer        The buffer containing the string to write to @p filename
    @param[in]  buffer_len      The length of @p_buffer in bytes

    @retval 0       on success
    @retval -EBADF  if the file couldn't be opened for writing
    @retval -EIO    if writing to the file failed
*/
int32_t
hal_file_write_string(const char *filename, int flags, const char *p_buffer, uint32_t buffer_len);

/**
    @brief  Copy a file from a source file handle to a destination file handle

    @param[in]  srcFd       The source file handle
    @param[out] destFd      The destination file handle

    @retval 0       on success
    @retval -EIO    if the was an error while reading from the source file handle
                    or writing to the destination file handle
    @retval -ENOMEM if memory couldn't be allocated for the copy buffer
*/
int32_t
hal_file_copy(int srcFd, int destFd);

/**
    @brief  Read a single line from the specified file handle

    @param[in]  fd              The file handle to read from
    @param[out] p_read_buffer   The buffer to store the read bytes into
    @param[in]  read_buffer_len The length of @p p_read_buffer (in bytes)
    @param[out] p_num_read      If not NULL, the number of bytes read in the line

    @note   This function considers the "end of line" characters to be NUL ('\0'), CR,
            and LF.

    @retval 0       on success
    @retval -EIO    if a failure occurred while reading from the file
    @retval -ERANGE if an "end of line" wasn't found within @p read_buffer_len bytes or before EOF
*/
int32_t
hal_file_read_line(int fd, char *p_read_buffer, uint32_t read_buffer_len, ssize_t *p_num_read);


/**
    @brief Determine the file stream's size

    @param[in]  fp              The file stream of interest

    @retval >=0     on success, representing the number of bytes in the file
    @retval -EBADF  failed to determine file stream size
*/
long
hal_file_stream_size( FILE *fp );


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __HAL_FILE_OPS_H_ */

