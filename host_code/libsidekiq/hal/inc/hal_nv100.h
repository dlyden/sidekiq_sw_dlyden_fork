/**
 * @file   hal_nv100.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Fri Sep 18 21:25:51 CDT 2020
 *
 * @brief This file captures functionality for the Sidekiq NV100 that is not implemented in
 * rfe_nv100.c.
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __HAL_NV100_H__
#define __HAL_NV100_H__

/***** INCLUDES *****/

#include <stdbool.h>
#include <stdint.h>

#include "sidekiq_types.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

/**************************************************************************************************/
/** nv100_select_external_ref_clock() performs the necessary steps to select an external reference
    clock source for a given NV100 card and sets the pll based on the input frequency.

    @param[in] card Sidekiq card index of interest
    @param[in] ref_clk_freq Frequency that will be set in conjuction with setting an external
    reference clock

    @return int32_t

    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EINVAL Specified ref_clk_freq is not a valid value
 */
int32_t nv100_select_external_ref_clock( uint8_t card, uint32_t ref_clk_freq );


/**************************************************************************************************/
/** nv100_select_ref_clock() performs the necessary steps to select the reference clock source
    for a given NV100 card.

    @param[in] card Sidekiq card index of interest

    @return int32_t

    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
    @retval -EINVAL Specified ref_clk_freq is not a valid value
 */
int32_t nv100_select_ref_clock( uint8_t card );


/**************************************************************************************************/
/** nv100_read_temp() performs the necessary steps to measure the temperature sensor for a given
    NV100 card.  This function requires that the RFIC is initialized or will return @a -ENODEV.

    @param[in] card Sidekiq card index of interest
    @param[in] sensor sensor index of interest
    @param[out] p_temp_in_deg_C Reference to a signed 8-bit integer to populate with the sensor measurement

    @return int32_t

    @retval 0 Success
    @retval -ENODEV device is not available to perform temperature measurement
    @retval -EINVAL Specified sensor index is not a valid value
 */
int32_t nv100_read_temp( uint8_t card,
                         uint8_t sensor,
                         int8_t *p_temp_in_deg_C );


/**************************************************************************************************/
/** nv100_set_eeprom_wp() performs the necessary steps to enable or disable the EEPROM write protect
    line as specified by @p write_protect for a given NV100 card.

    @param[in] card Sidekiq card index of interest
    @param[in] write_protect desired write protect state

    @return int32_t

    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t nv100_set_eeprom_wp( uint8_t card,
                             bool write_protect );


/**************************************************************************************************/
/** nv100_set_rfic_reset() performs the necessary steps to enable or disable the RFIC_RESET_N line
    as specified by @p rfic_reset for a given NV100 card.

    Resets the RFIC if @p rfic_reset = true

    Allows the RFIC to run if @p rfic_reset = false

    @param[in] card Sidekiq card index of interest
    @param[in] rfic_reset desired RFIC reset state

    @return int32_t

    @retval 0 Success
    @retval -EIO Input/output error communicating with the I/O expander
 */
int32_t nv100_set_rfic_reset( uint8_t card,
                              bool rfic_reset );

#endif  /* __HAL_NV100_H__ */
