/**
 * @file   hal_delay.h
 * @author  <info@epiq-solutions.com>
 * @date   Fri Nov 30 17:04:18 2018
 * 
 * @brief  Export an OS agnostic delay function prototype and conversion definitions
 * 
 * 
 * <pre>
 * Copyright 2013-2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

#ifndef __HAL_DELAY_H__
#define __HAL_DELAY_H__


/***** INCLUDES *****/

#include <stdint.h>


/***** DEFINES *****/

/* convenient conversions for nanoseconds to other times */
#define MICROSEC                        1000
#define MILLISEC                        1000000
#define SEC                             1000000000


/***** Extern Functions  *****/

extern int32_t hal_nanosleep(uint64_t num_nanosecs);
extern int32_t hal_microsleep(uint32_t num_microsecs);
extern int32_t hal_millisleep(uint32_t num_millisecs);
extern int32_t hal_sleep(uint32_t num_secs);

#endif  /* __HAL_DELAY_H__ */
