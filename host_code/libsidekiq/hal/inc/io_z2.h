/**
 * @file   io_z2.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu May 10 14:21:01 2018
 *
 * @brief This file captures I/O functionality for the Sidekiq Z2 that is not implemented in
 * rfe_z2_b.c.  For example, access of the external I2C bus is provided by functions implemented
 * here.
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __Z2_IO_H__
#define __Z2_IO_H__

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>

/***** DEFINES *****/

/* These are the I2C device addresses of the two I/O expanders populated on the Sidekiq Z2 */
#define U20_IO_EXPANDER_ADDR            0x21
#define U21_IO_EXPANDER_ADDR            0x20


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

/**************************************************************************************************/
/** io_z2_enable_external_bus enables access to the external I2C bus on the Sidekiq Z2

    @param[in] card Sidekiq card index

    @return int32_t
    @retval 0 Success
    @retval -EIO Communications error with I/O expander
 */
int32_t io_z2_enable_external_bus_access( uint8_t card );


/**************************************************************************************************/
/** io_z2_disable_external_bus disables access to the external I2C bus on the Sidekiq Z2

    @param[in] card Sidekiq card index

    @return int32_t
    @retval 0 Success
    @retval -EIO Communications error with I/O expander
 */
int32_t io_z2_disable_external_bus_access( uint8_t card );


/**************************************************************************************************/
/** io_z2_is_external_bus_accessible queries to see whether or not the Z2 I2C external bus access is
    presently enabled

    @param[in] card Sidekiq card index

    @return bool
    @retval true Z2 I2C external bus is accessible
    @retval false Z2 I2C external bus is NOT accessible
 */
bool io_z2_is_external_bus_accessible( uint8_t card );

#endif  /* __Z2_IO_H__ */
