#ifndef SIDEKIQ_HAL_H
#define SIDEKIQ_HAL_H

/*! \file sidekiq_hal.h
 * \brief This file contains the hardware application layer
 * interface for sidekiq.
 *
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 7/17/2013   MEZ   Created file
 *
 *</pre>*/

/***** INCLUDES *****/
#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_api.h"
#include "sidekiq_xport_api.h"
#include "sidekiq_private.h"
#include "sidekiq_fpga_ctrl.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_rf.h"
#include "sidekiq_rfe.h"
#include "sidekiq_rfic.h"
#include "hal_delay.h"
#include "hal_file_ops.h"
#include "hal_time.h"
#include "gpsdo_fpga.h"


/***** DEFINES *****/

#define INVALID_SERIAL_NUM (0xFFFF)
#define INVALID_CARD_INDEX   (0xFF)

// if the warp voltage hasn't been stored in eeprom, then it will be unconfigured
// reading, in this case, should return an error
#define UNCONFIGURED_WARP_VOLTAGE (0xFFFF)

// the default warp voltage offset is located at 0
#define DEFAULT_WARP_VOLTAGE_OFFSET (0)
// the user defined warp voltage offset is located at offset 4
#define DEFAULT_USER_WARP_VOLTAGE_OFFSET (4)
// we need to store the calibration date info in the warp voltage slots...let's use 1
#define CAL_DATE_WARP_VOLTAGE_OFFSET (1)

#define GOLDEN_FPGA_COMPARE_LEN (4)
#define GOLDEN_FPGA_COMPARE_OFFSET (28)

// The sync word is a predefined value that the FPGA uses to mark where the
// bitstream configuration data begins in flash memory. In our case, it is
// always found in the first page of flash memory that begins storage of the
// bitstream.
#define FPGA_SYNC_WORD_MPCIE_OFFSET (0x10)
#define FPGA_SYNC_WORD_M2_OFFSET (0x96)
#define FPGA_SYNC_WORD_LEN (4)
extern const uint8_t fpga_sync_word[FPGA_SYNC_WORD_LEN];

///////////////////////////////////////////////
// m.2 / Z2 ad9361 GPIO mapping
// see: http://confluence/display/EN/Sidekiq+M.2+Circuit+Card
#define AD9361_GPIO_RF_PWR_ENA_POS   (3)  // should always be "on", 1
#define AD9361_GPIO_OSC_PWR_ENA_POS  (2)  // set to 1 for internal ref, o.w. 0
#define AD9361_GPIO_REF_CLOCK_EXT    (1)  // set to 1 for external, o.w. 0
#define AD9361_GPIO_REF_CLOCK_HOST   (0)  // set to 1 for edge, o.w. 0
#define AD9361_MAX_NUM_GPIO_INIT     (3)
///////////////////////////////////////////////

/* struct initializers */
#define SKIQ_INITIALIZER                                              \
    {                                                                 \
        .card = {                                                     \
            [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = SKIQ_CARD_INITIALIZER, \
        },                                                            \
        .num_cards_avail = 0,                                         \
        .initialized = false,                                         \
        .log_msg_cb = _skiq_syslog,                                   \
        .use_exit_handler = true,                                     \
    }

/* @todo fill this out to initialize all fields in skiq_card_t */
#define SKIQ_CARD_INITIALIZER                                           \
    {                                                                   \
        .serial_num = INVALID_SERIAL_NUM,                               \
        .hardware_vers = skiq_hw_vers_invalid,                          \
        .product_vers = skiq_product_invalid,                           \
        .hardware_rev = hw_rev_invalid,                                 \
        .hw_iface_vers = HW_IFACE_VERSION(255, 255),                    \
        .card_active = 0,                                               \
        .present = false,                                               \
        .chan_mode = skiq_chan_mode_single,                             \
        /* .rx_list[] = { [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = ... }, */  \
        /* .tx_list[] = { [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = ... }, */  \
        .ref_clock = skiq_ref_clock_invalid,                            \
        .timestamp_base = skiq_tx_rf_timestamp,                         \
        .accel_enabled = false,                                         \
        .has_iq_packed_mode = false,                                    \
        .iq_packed = false,                                             \
        .iq_complex_mult_enabled = true,                                \
        .iq_order_mode = skiq_iq_order_qi,                              \
        .tx_complete = NULL,                                            \
        .tx_enabled_cb = NULL,                                          \
        .num_send_threads = 0,                                          \
        .thread_priority = 0,                                           \
        .rf_port_config = skiq_rf_port_config_fixed,                    \
        .rx_fir_gain = skiq_rx_fir_gain_0,                              \
        .rx_stream_mode = skiq_rx_stream_mode_high_tput,                \
        .p_rfe_funcs = NULL,                                            \
        .p_rfic_funcs = NULL,                                           \
    }

/** @brief  Static initializer macro for "struct skiq_fpga_priv_t". */
/**
    @todo   Should this somehow use FPGA_CAPABILITIES_INITIALIZER from sidekiq_fpga_ctrl.h for
            the "caps" field?
*/
#define SKIQ_FPGA_PRIV_INITIALIZER                                      \
    {                                                                   \
        .board_id = 0,                                                  \
        .caps = { 0 },                                                  \
    }

/***** ENUMS *****/

/***** STRUCTS *****/

typedef void (*skiq_log_msg_callback_t)(int32_t priority, const char *message);

typedef struct
{
    // HAL functions which can be overwritten

    // Access firmware version
    int32_t (*read_fw_version)( uint8_t card, uint8_t* p_ver_maj, uint8_t* p_ver_min);

    // EEPROM read/write
    int32_t (*read_eeprom)( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );
    int32_t (*write_eeprom)( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

    // Read/Write I2C through FPGA or USB register transactions
    int32_t (*write_i2c)(uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes);
    int32_t (*read_i2c)(uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes);
    // Write then Read I2C through FPGA or USB register transactions
    int32_t (*write_then_read_i2c)(uint8_t card, uint8_t periph_addr, uint8_t reg_addr, uint8_t* p_data, uint8_t num_bytes);

    // program FPGA from file
    int32_t (*prog_fpga_from_file)(uint8_t card, FILE *fp);

    /** FMC I2C read/write and FMC EEPROM read/write
        These interfaces are only accessible on Sidekiq X2 / X4 and when ATE_SUPPORT is defined
     */
    int32_t (*write_i2c_by_bus)(uint8_t card, uint8_t bus, uint8_t addr, uint8_t data[], uint8_t num_bytes);
    int32_t (*read_i2c_by_bus)(uint8_t card, uint8_t bus, uint8_t addr, uint8_t data[], uint8_t num_bytes);
    int32_t (*write_then_read_i2c_by_bus)(uint8_t card, uint8_t bus, uint8_t periph_addr, uint8_t reg_addr, uint8_t data[], uint8_t num_bytes);
    int32_t (*read_fmc_eeprom)( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );
    int32_t (*write_fmc_eeprom)( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

} hal_functions_t;

typedef struct
{
    uint32_t hdl;
    uint32_t chip_id;
} skiq_iface_id;

typedef struct
{
    skiq_iface_id id;
    skiq_rx_param_t params;
    rf_id_t rf_id;
    skiq_data_src_t data_src;
    uint32_t gain;
    skiq_rx_gain_t gain_mode;
    bool streaming;
    bool dc_offset_ena;
    skiq_freq_tune_mode_t tune_mode;
    uint16_t num_hop_freqs;
} skiq_rx_iface;

typedef struct
{
    skiq_iface_id id;
    skiq_tx_param_t params;
    rf_id_t rf_id;
    skiq_tx_flow_mode_t flow_mode;
    uint16_t block_size;
    uint32_t num_bytes_to_send;
    skiq_tx_transfer_mode_t tx_transfer_mode;
    bool streaming;
    uint16_t attenuation;
    skiq_freq_tune_mode_t tune_mode;
    uint16_t num_hop_freqs;
} skiq_tx_iface;

typedef struct
{
    uint8_t board_id;
    uint8_t board_id_ext;
    struct fpga_capabilities caps;

} skiq_fpga_priv_t;

typedef struct
{
    uint32_t serial_num;
    skiq_hw_vers_t hardware_vers; // TODO: replaced with part/hardware_rev?
    skiq_product_t product_vers;  // TODO: replaced with part/hardware_rev?
    skiq_hw_rev_t hardware_rev;
    hw_iface_vers_t hw_iface_vers;
    uint8_t card_active;
    bool present;
    skiq_chan_mode_t chan_mode;
    skiq_rx_iface rx_list[skiq_rx_hdl_end];
    skiq_tx_iface tx_list[skiq_tx_hdl_end];
    skiq_ref_clock_select_t ref_clock;
    skiq_tx_timestamp_base_t timestamp_base;
    bool accel_enabled;
    bool iq_packed;
    bool has_iq_packed_mode;
    bool iq_complex_mult_enabled;
    bool has_dc_offset; //consider moving this to skiq_rx_iface.skiq_rx_param_t
    skiq_iq_order_t iq_order_mode;
    skiq_tx_callback_t tx_complete;
    skiq_tx_ena_callback_t tx_enabled_cb;
    uint8_t num_send_threads;
    int32_t thread_priority;
    skiq_card_param_t card_params;
    skiq_fpga_param_t fpga_params;
    skiq_fw_param_t fw_params;
    skiq_rf_param_t rf_params;
    skiq_fpga_priv_t fpga_priv;
    skiq_rf_port_config_t rf_port_config;
    skiq_rx_fir_gain_t rx_fir_gain;
    skiq_rx_stream_mode_t rx_stream_mode;
    skiq_part_info_t part_info;
    rfe_functions_t *p_rfe_funcs;
    rfic_functions_t *p_rfic_funcs;
} skiq_card_t;

typedef struct
{
    skiq_card_t card[SKIQ_MAX_NUM_CARDS];
    uint8_t num_cards_avail;
    bool initialized;
    skiq_log_msg_callback_t log_msg_cb;

    bool use_exit_handler;
} skiq_sys;


/***** Extern Functions  *****/
extern void hal_critical_exit( int32_t status );

void hal_select_functions_for_card( uint8_t card,
                                    skiq_xport_type_t type );
void hal_select_functions_for_card_by_part( uint8_t card,
                                            skiq_part_t part );
extern void hal_reset_functions( uint8_t card );

extern int32_t hal_write_i2c( uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes );
extern int32_t hal_read_i2c( uint8_t card, uint8_t addr, uint8_t* p_data, uint8_t num_bytes );
extern int32_t hal_write_then_read_i2c( uint8_t card,
                                        uint8_t periph_addr,
                                        uint8_t reg_addr,
                                        uint8_t* p_data,
                                        uint8_t num_bytes );

/** Read a 16-bit register from an I2C device that presents its data in little endian ordering */
extern int32_t hal_read_le16_i2c_reg( uint8_t card,
                                      uint8_t addr,
                                      uint8_t reg,
                                      int16_t* p_data16 );

/** Read a 16-bit register from an I2C device that presents its data in big endian ordering */
extern int32_t hal_read_be16_i2c_reg( uint8_t card,
                                      uint8_t addr,
                                      uint8_t reg,
                                      int16_t* p_data16 );

/** Read an unsigned 16-bit register from an I2C device that presents its data in big endian ordering */
extern int32_t hal_read_beu16_i2c_reg( uint8_t card,
                                       uint8_t addr,
                                       uint8_t reg,
                                       uint16_t* p_data16 );

extern int32_t hal_read_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

// A lock must be obtained prior to writing using hal_write_eeprom() directly
extern int32_t hal_write_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

extern int32_t hal_write_i2c_by_bus( uint8_t card, uint8_t bus, uint8_t addr, uint8_t data[], uint8_t num_bytes );
extern int32_t hal_read_i2c_by_bus( uint8_t card, uint8_t bus, uint8_t addr, uint8_t data[], uint8_t num_bytes );
extern int32_t hal_write_then_read_i2c_by_bus( uint8_t card,
                                               uint8_t bus,
                                               uint8_t periph_addr,
                                               uint8_t reg_addr,
                                               uint8_t data[],
                                               uint8_t num_bytes );

extern int32_t hal_read_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );
extern int32_t hal_write_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

extern int32_t hal_rfic_write_reg( uint8_t card, uint8_t cs, uint16_t addr, uint8_t data);
extern int32_t hal_rfic_read_reg( uint8_t card, uint8_t cs, uint16_t addr, uint8_t* p_data);

extern int32_t hal_spi_dac_write( uint8_t card, uint16_t data );

extern void hal_register_critical_error_cb( void (*critical_cb)(int32_t status, void* p_user_data),
                                            void* p_user_data );

extern int32_t hal_read_fw_ver( uint8_t card,
                                uint8_t* p_ver_maj,
                                uint8_t* p_ver_min );

extern int32_t hal_prog_fpga_from_file( uint8_t card, FILE *fp );

#endif
