/**
 * @file   xilinx_fpga_bitstream.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Aug  8 11:13:54 CDT 2019
 *
 * @brief Provide functions for parsing / decoding Xilinx FPGA bitstreams as well as operating on
 * bitstream contents.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __XILINX_FPGA_BITSTREAM_H__
#define __XILINX_FPGA_BITSTREAM_H__

#include <stdint.h>

bool xilinx_fpga_bitstream_is_bitfile( uint8_t card,
                                       const void *data,
                                       uint32_t nr_words );

bool xilinx_fpga_bitstream_is_partial( uint8_t card,
                                       const void *data,
                                       uint32_t nr_words );

int32_t xilinx_fpga_bitstream_word_align( uint8_t card,
                                          uint32_t *data,
                                          uint32_t nr_words );

#endif  /* __XILINX_FPGA_BITSTREAM_H__ */
