/**
 * @file   pll_adf4002.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu May 16 10:11:36 CDT 2019
 *
 * @brief This file provides generic access to the ADF4002 Phase Detector / Frequency Synthesizer
 * populated on the Sidekiq M.2-2280.
 *
 * Datasheet is available:
 * - on the web -- https://www.analog.com/media/en/technical-documentation/data-sheets/ADF4002.pdf
 * - on the network -- /mnt/storage/datasheets/AnalogDevices/ADF4002.pdf
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __PLL_ADF4002_H__
#define __PLL_ADF4002_H__

/***** INCLUDES *****/

#include <stdint.h>

/***** DEFINES *****/


/***** TYPEDEFS *****/

typedef enum
{
    pll_adf4002_setting_10MHz,
    pll_adf4002_setting_40MHz,

} pll_adf4002_setting_t;


/***** EXTERN FUNCTIONS  *****/

int32_t pll_adf4002_configure( uint8_t card,
                               pll_adf4002_setting_t setting );


#endif  /* __PLL_ADF4002_H__ */
