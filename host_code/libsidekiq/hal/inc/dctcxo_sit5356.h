#ifndef __DCTCXO_SIT5356_H__
#define __DCTCXO_SIT5356_H__

/**
 * @file   dctcxo_sit5356.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Mon May 13 14:35:56 CDT 2019
 *
 * @brief This file provides access to the TCXO (SiT5356) I2C.
 *
 * Datasheet is available:
 * - on the web -- https://www.sitime.com/datasheet/SiT5356
 * - on the network -- /mnt/storage/datasheets/SiTime/SiT5356-rev0.60_180228.pdf
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <stdint.h>

/***** DEFINES *****/



/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/


extern int32_t sit5356_dctcxo_read_warp_voltage( uint8_t card,
                                                 uint32_t *p_warp_voltage );

extern int32_t sit5356_dctcxo_write_warp_voltage( uint8_t card,
                                                  uint32_t warp_voltage );



#endif  /* __DCTCXO_SIT5356_H__ */
