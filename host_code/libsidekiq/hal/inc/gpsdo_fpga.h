/**
 * @file   gpsdo_fpga.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Fri Mar  6 14:31:49 CST 2020
 *
 * @brief  This captures the interface functions for the GPSDO implemented in the FPGA design
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __GPSDO_FPGA_H__
#define __GPSDO_FPGA_H__

/***** INCLUDES *****/

#include <stdbool.h>
#include <stdint.h>

#include "sidekiq_private.h"    /* for FPGA_VERSION() MACRO */


/***** DEFINES *****/

/* Required FPGA version for the GPSDO functionality */
#define FPGA_VERS_GPSDO                 FPGA_VERSION(3,14,1)

/**
    Default configuration values for the GPSDO control registers

    These values are taken from
    https://confluence.epiq.rocks/pages/viewpage.action?title=GPSDO+FPGA+Register+Definitions&spaceKey=EN

  @{
*/

/** @brief  Default value for the `ki_start` parameter */
#ifndef GPSDO_DEFAULT_KI_START
#   define GPSDO_DEFAULT_KI_START           (5)
#endif

/** @brief  Default value for the `ki_end` parameter */
#ifndef GPSDO_DEFAULT_KI_END
#   define GPSDO_DEFAULT_KI_END             (10)
#endif

/** @brief  Default value for the `thresh_locked` parameter */
#ifndef GPSDO_DEFAULT_THRESH_LOCKED
#   define GPSDO_DEFAULT_THRESH_LOCKED      (5)
#endif

/** @brief  Default value for the `thresh_unlocked` parameter */
#ifndef GPSDO_DEFAULT_THRESH_UNLOCKED
#   define GPSDO_DEFAULT_THRESH_UNLOCKED    (6)
#endif

/** @brief  Default value for the `count_locked` parameter */
#ifndef GPSDO_DEFAULT_COUNT_LOCKED
#   define GPSDO_DEFAULT_COUNT_LOCKED       (4)
#endif

/** @brief  Default value for the `count_unlocked` parameter */
#ifndef GPSDO_DEFAULT_COUNT_UNLOCKED
#   define GPSDO_DEFAULT_COUNT_UNLOCKED     (4)
#endif

/** @brief  Default value for the `time_in_stage` parameter */
#ifndef GPSDO_DEFAULT_TIME_IN_STAGE
#   define GPSDO_DEFAULT_TIME_IN_STAGE      (60)
#endif

/** @brief  Default value for the `warp_set_value` parameter */
#ifndef GPSDO_DEFAULT_WARP_SET_VALUE
#   define GPSDO_DEFAULT_WARP_SET_VALUE     (0)
#endif

/** @} */




/***** TYPEDEFS *****/
/** @brief  Structure definition for the GPSDO configuration settings */
typedef struct
{
    /** @brief  Initial Ki setting to use */
    uint8_t ki_start;
    /** @brief  Maximum Ki value (minimum gain value) */
    uint8_t ki_end;
    /** @brief  Frequency error to consider within lock */
    uint8_t thresh_locked;
    /** @brief  Frequency error to consider "out of lock" */
    uint8_t thresh_unlocked;
    /** @brief  Number of consecutive low errors to see before entering lock state */
    uint8_t count_locked;
    /** @brief  Number of consecutive high frequency counts to see before leaving lock state */
    uint8_t count_unlocked;
    /** @brief  Time to stay at each Ki gain value before reducing gain */
    uint8_t time_in_stage;
    /** @brief  The initial VCO warp value to use */
    int32_t warp_set_value;
} gpsdo_fpga_control_parameters_t;

/** @brief  Initializer macro for the "gpsdo_fpga_control_parameters_t" type. */
#define GPSDO_FPGA_CONTROL_PARAMETERS_T_INITIALIZER \
    {                                               \
        .ki_start = GPSDO_DEFAULT_KI_START, \
        .ki_end = GPSDO_DEFAULT_KI_END, \
        .thresh_locked = GPSDO_DEFAULT_THRESH_LOCKED,   \
        .thresh_unlocked = GPSDO_DEFAULT_THRESH_UNLOCKED,   \
        .count_locked = GPSDO_DEFAULT_COUNT_LOCKED, \
        .count_unlocked = GPSDO_DEFAULT_COUNT_UNLOCKED, \
        .time_in_stage = GPSDO_DEFAULT_TIME_IN_STAGE,   \
        .warp_set_value = GPSDO_DEFAULT_WARP_SET_VALUE, \
    }

/** @brief  Structure to describe the GPSDO status registers */
typedef struct
{
    /** @brief  The raw status register value */
    uint32_t raw_value;
    /** @brief  Flag to indicate if the control loop has converged according to the algorithm */
    bool gpsdo_locked_state;
    /** @brief  Flag to indicate if the GPS has a fix */
    bool gps_has_fix;
    /** @brief  Flag to indicate the lock state of the PLL */
    bool pll_locked_state;
    /** @brief  Number of consecutive high frequency error values detected in a row */
    uint8_t counter_unlocked;
    /** @brief  Number of consecutive low frequency error values detected in a row */
    uint8_t counter_locked;
    /** @brief  Current gain parameter */
    uint8_t ki;
    /** @brief  Time at current gain setting */
    uint8_t stage_timer;
} gpsdo_fpga_status_registers_t;

/** @brief  Initializer macro for the "gpsdo_fpga_status_registers_t" type. */
#define GPSDO_FPGA_STATUS_REGISTERS_T_INITIALIZER   \
    {                                               \
        .raw_value = 0, \
        .gpsdo_locked_state = false,  \
        .gps_has_fix = false, \
        .pll_locked_state = false,  \
        .counter_locked = 0,    \
        .counter_unlocked = 0,  \
        .ki = 0,    \
        .stage_timer = 0,   \
    }

/** @brief  Structure to describe the GPSDO counter registers */
typedef struct
{
    /** @brief  2-bit counter that counts PPS signals (modulo 4) */
    uint8_t seconds_counter;
    /**
        @brief  Latched values of the frequency counter at each PPS edge (e.g. frequency error
                value)
    */
    int32_t freq_counter;
} gpsdo_fpga_counters_t;

/** @brief  Initializer macro for the "gpsdo_fpga_counters_t" type. */
#define GPSDO_FPGA_COUNTERS_T_INITIALIZER   \
    {                                       \
        .seconds_counter = 0,   \
        .freq_counter = 0,  \
    }


/***** EXTERN FUNCTIONS  *****/


/**************************************************************************************************/
/**
    @brief  Configures the GPSDO module with the default control values and enables the GPSDO
            algorithm control of the DCTCXO.

    @param[in]  card             requested Sidekiq card ID

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_configure_and_enable(uint8_t card);

/**************************************************************************************************/
/**
   @brief   Provides indication whether or not the GPSDO is enabled in the FPGA design.

   @param[in]  card             requested Sidekiq card ID
   @param[out] p_is_enabled     if true, the GPSDO is enabled else the GPSDO is disabled

   @return 0 on success, else a negative errno value
   @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
   @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
   @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_is_enabled(uint8_t card, bool *p_is_enabled );

/**************************************************************************************************/
/**
    @brief  Provides the frequency accuracy of the oscillator.

    @attention  This accuracy is only valid if the GPS module has a fix. It is up to the caller to
                verify that is the case.

    @param[in]  card            requested Sidekiq card ID
    @param[out] p_ppm           the calculated frequency accuracy (in ppm)
    @param[out] p_freq_counter  the raw frequency counter value from the GPSDO

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
    @retval -EAGAIN     if the measurement is not available because the GPSDO is disabled
*/
int32_t gpsdo_fpga_get_freq_accuracy(uint8_t card, double *p_ppm, int32_t *p_freq_counter);

/**************************************************************************************************/
/**
    @brief   Read the warp voltage that the GPSDO algorithm has written to the DCTCXO.

    @param[in]  card             requested Sidekiq card ID
    @param[out] p_warp_voltage   the warp voltage currently in use

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_read_warp_voltage(uint8_t card, uint32_t *p_warp_voltage);

/**************************************************************************************************/
/**
    @brief  Read the GPSDO algorithm's operating counter registers

    @param[in]  card            requested Sidekiq card ID
    @param[out] p_counters      If the function returns success, the list of operating counters
                                from the GPSDO algorithm

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_read_counters(uint8_t card, gpsdo_fpga_counters_t *p_counters);

/**************************************************************************************************/
/**
    @brief  Read the GPSDO algorithm's status registers

    @param[in]  card            requested Sidekiq card ID
    @param[out] p_status        If the function returns success, the list of status registers
                                from the GPSDO algorithm

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_read_status(uint8_t card, gpsdo_fpga_status_registers_t *p_status);

/**************************************************************************************************/
/**
    @brief  Set the GPSDO algorithm control parameters

    This function sets the specified control parameters for the specified card.  If the GPSDO
    algorithm is running, it will also write those values to the FPGA.

    @param[in]  card            requested Sidekiq card ID
    @param[in]  p_params        the parameters & settings for the GPSDO algorithm

    @warning    for DEBUG purposes ONLY!  This function is subject to removal

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_set_control_parameters(uint8_t card,
    const gpsdo_fpga_control_parameters_t *p_params);

/**************************************************************************************************/
/**
    @brief  Enable the GPSDO control algorithm on the specified card

    @param[in]  card            requested Sidekiq card ID

    @warning    for DEBUG purposes ONLY!  This function is subject to removal

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_enable(uint8_t card);

/**************************************************************************************************/
/**
    @brief   Disables the GPSDO algorithm's control of the DCTCXO.

    @param[in]  card             requested Sidekiq card ID

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_disable(uint8_t card);

/*****************************************************************************/
/**
    @brief   Indicates whether or not the specified card has a GPSDO module present in the FPGA

    @param[in]  card             requested Sidekiq card ID
    @param[out] p_has_module     If the function returns success, indicates whether or not a
                                 GPSDO module is present in the FPGA
    @param[out] p_detailed_status If module not available, -ENOTSUP if not supported for product
                                  -ENOSYS if bitstream doesn't have GPSDO.  0 in the case module is
                                  available and present.
    @return 0 on success, else a negative errno value
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_has_module( uint8_t card,
                               bool *p_has_module,
                               int32_t *p_detailed_status );


/*****************************************************************************/
/**
    @brief   Updates the reset override settings depending on whether GPSDO is enabled or not.

    @param[in]  card             requested Sidekiq card ID
    @return 0 on success, else a negative errno value
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
int32_t gpsdo_fpga_update_reset_override( uint8_t card );

#endif  /* __GPSDO_FPGA_H__ */

