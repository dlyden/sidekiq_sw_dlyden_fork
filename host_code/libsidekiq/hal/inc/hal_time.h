/**
    @file   hal_time.h
    @brief  Header file for hal_time.c

    <pre>
    Copyright 2019 Epiq Solutions, All Rights Reserved
    </pre>
*/
#ifndef __HAL_TIME_H__
#define __HAL_TIME_H__

#include <stdint.h>
#include <time.h>

/**
    @brief  A representation of time in seconds & nanoseconds

    @note   This is intentionally similar to "struct timespec" but designed to not rely on the
            host's definition.
*/
typedef struct _hal_timespec_t
{
    uint64_t tv_sec;
    uint64_t tv_nsec;
} hal_timespec_t;

/**
    @brief  The type of clock requested
*/
typedef enum
{
    /** @brief  A steadily increasing counter from an arbitrary point in time */
    hal_time_clock_type_monotonic,
    /** @brief  The current wallclock time; can be affected by system time changes */
    hal_time_clock_type_realtime,
    /**
        @brief  The amount of CPU time used by the current process (not supported on all platforms)
    */
    hal_time_clock_type_process_cputime_id,
    /**
        @biref  The amount of CPU time used by the current thread (not supported on all platforms)
    */
    hal_time_clock_type_thread_cputime_id,
    /** @brief  Not a valid clock type; used to indicate an invalid value */
    hal_time_clock_type_invalid,
} hal_time_clock_type_t;


/**
    @brief  Get the current time for a specified clock

    @param[in]  clockType       [::hal_time_type_t] The requested clock type
    @param[out] p_clockVal      The current time; this should not be NULL

    @return 0 on success, else an errno
    @retval -EINVAL     the requested clock type is invalid
    @retval -ENOTSUP    the specified clock type isn't supported on the current platform
    @retval -EFAULT     @p p_clockVal references invalid memory
*/
int32_t hal_time_get(hal_time_clock_type_t clockType, hal_timespec_t *p_clockVal);

/**
    @brief  Get the current time for a specified clock in nanoseconds

    @param[in]  clockType       [::hal_time_type_t] The requested clock type
    @param[out] p_clockVal      The current time in nanoseconds; this should not be NULL

    @note   The underlying time functions used by this function do not guarantee nanosecond
            resolution - see the platform implementations to determine the clock resolution
            provided by this function.

    @return 0 on success, else an errno
    @retval -EINVAL     the requested clock type is invalid
    @retval -ENOTSUP    the specified clock type isn't supported on the current platform
    @retval -EFAULT     @p p_clockVal references invalid memory
*/
int32_t hal_time_get_ns(hal_time_clock_type_t clockType, uint64_t *p_clockVal);

#endif /* __HAL_TIME_H__ */

