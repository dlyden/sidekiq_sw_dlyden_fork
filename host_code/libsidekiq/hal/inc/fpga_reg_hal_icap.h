/**
 * @file   fpga_reg_hal_icap.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Aug  8 12:28:08 CDT 2019
 *
 * @brief  Relevant documentation on Xilinx ICAP (Internal Configuration Access Port)
 *
 * - Xilinx Partial Reconfiguration User Guide (UG702)
 * -- https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_2/ug702.pdf
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __FPGA_REG_HAL_ICAP_H__
#define __FPGA_REG_HAL_ICAP_H__

#include <stdint.h>

int32_t
fpga_icap_prog_fpga_from_file( uint8_t card,
                               FILE *fp );

#endif
