#! /bin/bash

#
# Simple script to test the SDK build
#

BUILD_CONFIG=${BUILD_CONFIG:-x86_64.gcc}

MKT=tmp/build
rm -rf tmp
mkdir -p $MKT

tar -chf - \
    Makefile.for_sdk \
    src/ \
    test_apps/ \
    tools.mk \
    | tar -C $MKT -xvf -

mkdir -p $MKT/lib $MKT/inc $MKT/../lib/support $MKT/../arg_parser/lib
cp -t $MKT/inc ../../../build.${BUILD_CONFIG}/include/*.h
cp -rt $MKT/../lib/support/ ../../../build.${BUILD_CONFIG}/support
mv $MKT/../lib/support/support $MKT/../lib/support/${BUILD_CONFIG}
cp -t $MKT/inc ../../arg_parser/inc/arg_parser.h
cp -t $MKT/../arg_parser/lib ../../arg_parser/arg_parser__${BUILD_CONFIG}.a
cp -t $MKT/../lib ../../../build.${BUILD_CONFIG}/lib/libsidekiq__${BUILD_CONFIG}.a

(cd $MKT; make -f Makefile.for_sdk BUILD_CONFIG=${BUILD_CONFIG})
