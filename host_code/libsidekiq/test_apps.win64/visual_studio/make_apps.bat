@echo OFF
REM
REM Iterates over each subdirectory and calls make_app.bat in each
REM
REM
for /d %%D in (*) do (
   echo %%~fD
   pushd %%~fD
   call make_app.bat
   popd
)

