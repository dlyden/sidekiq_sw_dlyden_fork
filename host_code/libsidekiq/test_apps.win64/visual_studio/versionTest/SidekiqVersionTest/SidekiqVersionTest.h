
// SidekiqVersionTest.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CSidekiqVersionTestApp:
// See SidekiqVersionTest.cpp for the implementation of this class
//

class CSidekiqVersionTestApp : public CWinApp
{
public:
	CSidekiqVersionTestApp();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int  ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CSidekiqVersionTestApp theApp;