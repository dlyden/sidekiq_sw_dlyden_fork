
// SidekiqVersionTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SidekiqVersionTest.h"
#include "SidekiqVersionTestDlg.h"
#include "afxdialogex.h"

#include "sidekiq_api.h"
#include <conio.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define BASIC_LOG	_cprintf

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSidekiqVersionTestDlg dialog



CSidekiqVersionTestDlg::CSidekiqVersionTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSidekiqVersionTestDlg::IDD, pParent)
	, m_strEditLabel(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_activeCard = 0;
	m_maxCards = 0;
}

void CSidekiqVersionTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NUM_CARDS, m_strEditLabel);
	DDX_Control(pDX, IDC_MFCMASKEDEDIT_FREQUENCY, m_frequency);
}

BEGIN_MESSAGE_MAP(CSidekiqVersionTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_GO, &CSidekiqVersionTestDlg::OnBnClickedButtonGo)
	ON_WM_DEVMODECHANGE()
	ON_BN_CLICKED(IDC_BUTTON_SETFREQUENCY, &CSidekiqVersionTestDlg::OnClickedButtonSetfrequency)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_NUM_CARDS, &CSidekiqVersionTestDlg::OnDeltaposSpinNumCards)
END_MESSAGE_MAP()


static void criticalSidekiqErrorHandler(int32_t status, void *p_userData)
{
	CString message;
	message.Format(_T("libsidekiq: Critical Error %d.\nExiting."), status);
	AfxMessageBox(message, MB_OK|MB_ICONEXCLAMATION);
	exit(status);
}

// CSidekiqVersionTestDlg message handlers

BOOL CSidekiqVersionTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_frequency.EnableMask(_T("dddddd.dddd"), // The mask string
		_T("______.____"), // Literal, "_" char = character entry
		_T(' ')); // Backspace replace char
	m_frequency.SetValidChars(_T("1234567890")); // Valid string characters
	m_frequency.SetWindowText(_T("805000.0123"));

	skiq_register_critical_error_callback(criticalSidekiqErrorHandler, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSidekiqVersionTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

//void CSidekiqVersionTestDlg::OnDeviceChange(UINT nEventType,
//	DWORD_PTR dwData) {
//
//}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSidekiqVersionTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSidekiqVersionTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CSidekiqVersionTestDlg::OnBnClickedButtonGo()
{
	// TODO: Add your control notification handler code here
	CString test("probing...");
	CWnd *numCardsLabel = GetDlgItem(IDC_EDIT_NUM_CARDS);
	numCardsLabel->SetWindowTextW(test);

    skiq_xport_type_t init_type = skiq_xport_type_pcie;
    skiq_xport_init_level_t init_level = skiq_xport_init_level_basic;
	uint8_t cards[SKIQ_MAX_NUM_CARDS];
	uint8_t num_cards = 0;

	/* determine how many Sidekiqs are there and their card IDs */
	BASIC_LOG("calling skiq_get_cards!\n");

	/* Probe PCIe but not USB */
    if (skiq_get_cards(init_type, &num_cards, &cards[0]) != 0) {
		BASIC_LOG("Getting cards failed!\n");
		return;
	}

	CString message;
	message.Format(_T("%d card(s) found!\n"), num_cards);
	numCardsLabel->SetWindowTextW(message);

	m_maxCards = num_cards;
	if (m_maxCards > 0) {
		numCardsLabel->EnableWindow(TRUE);
		((CEdit *)numCardsLabel)->SetReadOnly();

		CWnd *numCardsSpin = GetDlgItem(IDC_SPIN_NUM_CARDS);
		numCardsSpin->EnableWindow(TRUE);
		((CSpinButtonCtrl *)numCardsSpin)->SetRange(1, m_maxCards);
		((CSpinButtonCtrl *)numCardsSpin)->SetPos(1);

		CWnd *numCardsSpinEdit = GetDlgItem(IDC_EDIT_ACTIVE_CARD);
		numCardsSpinEdit->EnableWindow(TRUE);
		numCardsSpinEdit->ShowWindow(TRUE);
		m_activeCard = 1;
		((CEdit *)numCardsSpinEdit)->SetWindowText(_T("1"));
		//((CEdit *)numCardsSpinEdit)->SetPos(1);

		DoVersionTest(num_cards, cards);
	}

}

/* Must call free if the return value is non-NULL */
static wchar_t * charTowchar(const char *orig)
{
	// newsize describes the length of the   
	// wchar_t string called wcstring in terms of the number   
	// of wide characters, not the number of bytes.  
	size_t newsize = strlen(orig) + 1;

	// The following creates a buffer large enough to contain   
	// the exact number of characters in the original string  
	// in the new format. If you want to add more characters  
	// to the end of the string, increase the value of newsize  
	// to increase the size of the buffer.  
	wchar_t * wcstring = new wchar_t[newsize];
	
	if (wcstring != NULL) {
		// Convert char* string to a wchar_t* string.  
		size_t convertedChars = 0;
		mbstowcs_s(&convertedChars, wcstring, newsize, orig, _TRUNCATE);
	}
	// Display the result and indicate the type of string that it is.  
	//wcout << wcstring << _T(" (wchar_t *)") << endl;

	return (wcstring);
}

int32_t CSidekiqVersionTestDlg::DoVersionTest(uint8_t num_cards, uint8_t *cards)
{
	int32_t status = 0;

	uint8_t firmware_maj;
	uint8_t firmware_min;
	char* p_serial_num;
	skiq_hw_vers_t hardware_vers;
	skiq_product_t product_vers;
	uint8_t libsidekiq_maj;
	uint8_t libsidekiq_min;
	uint8_t libsidekiq_patch;
	const char *libsidekiq_label;
        skiq_param_t params;
        skiq_fpga_param_t *fpga = &(params.fpga_param);
	skiq_ref_clock_select_t ref_clock = skiq_ref_clock_invalid;
    skiq_xport_type_t init_type = skiq_xport_type_pcie;
    skiq_xport_init_level_t init_level = skiq_xport_init_level_basic;

	uint8_t i = 0;
	bool display_fw = true;
	bool rf_port_fixed_avail = false;
	bool rf_port_tdd_avail = false;


	/* bring up the USB and PCIe interfaces for all the cards */
    status = skiq_init(init_type, init_level, cards, num_cards);
	if (status != 0)
	{
		BASIC_LOG("Error: unable to initialize libsidekiq with status %d\n", status);
		return (-1);
	}
	else {
        BASIC_LOG("Info: initialized libsidekiq with PCIe level %d and USB level %d\n", init_type, init_level);
	}

	/* read the version info for each for each of the cards */
	for (i = 0; i<num_cards; i++)
	{
		/* read the firmware version */
		if ((status = skiq_read_fw_version(cards[i], &firmware_maj, &firmware_min)) != 0)
		{
			BASIC_LOG("Error: unable to read firmware version, status %d\r\n", status);
			display_fw = false;
		}

		/* read the serial number */
		if ((status = skiq_read_serial_string(cards[i], &p_serial_num)) != 0)
		{
			BASIC_LOG("Error: unable to read serial number, status %d\r\n", status);
		}

		/* read the hardware version */
		if ((status = skiq_read_hw_version(cards[i], &hardware_vers)) != 0)
		{
			BASIC_LOG("Error: unable to read hardware version, status %d\r\n", status);
		}

		/* read the product version */
		if ((status = skiq_read_product_version(cards[i], &product_vers)) != 0)
		{
			BASIC_LOG("Error: unable to read hardware version, status %d\r\n", status);
		}

		/* read the ref clock config if the hardware rev supports it */
		if ((status = skiq_read_ref_clock_select(cards[i], &ref_clock) != 0) &&
			( (hardware_vers == skiq_hw_vers_mpcie_c) || (hardware_vers == skiq_hw_vers_mpcie_d) || (hardware_vers == skiq_hw_vers_mpcie_e)))
		{
			BASIC_LOG("Error: unable to read reference clock, status %d\r\n", status);
			ref_clock = skiq_ref_clock_invalid;
		}

		/* read the libsidekiq version */
		skiq_read_libsidekiq_version(&libsidekiq_maj, &libsidekiq_min, &libsidekiq_patch, &libsidekiq_label);

		/* read the Sidekiq parameters */
                skiq_read_parameters( cards[i], &params );

		/* Fill in Window */
		{
			CString strFwVersion(p_serial_num);
			strFwVersion.Format(_T("%u.%u"), firmware_maj, firmware_min);
			CWnd *sidekiqFwVersionLabel = GetDlgItem(IDC_EDIT_SIDEKIQ_FW_VERSION);
			sidekiqFwVersionLabel->SetWindowTextW(strFwVersion);

			CString strSerialNumber(p_serial_num);
			CWnd *numCardsLabel = GetDlgItem(IDC_EDIT_SERIAL_NUMBER);
			numCardsLabel->SetWindowTextW(strSerialNumber);

			CString strHwVersion(skiq_hardware_vers_string(hardware_vers));
			CWnd *hwVersionLabel = GetDlgItem(IDC_EDIT_SIDEKIQ_HW_VERSION);
			hwVersionLabel->SetWindowTextW(strHwVersion);

			CString strProductVersion(skiq_product_vers_string(product_vers));
			CWnd *productVersionLabel = GetDlgItem(IDC_EDIT_PRODUCT_VERSION);
			productVersionLabel->SetWindowTextW(strProductVersion);

			CString strLibsidekiqVersion;
			wchar_t *wchar_label = charTowchar(libsidekiq_label);
			strLibsidekiqVersion.Format(_T("%u.%u.%u%s"),
				libsidekiq_maj, libsidekiq_min, libsidekiq_patch, wchar_label);
			delete wchar_label;
			CWnd *libsidekiqVersionLabel = GetDlgItem(IDC_EDIT_SIDEKIQ_LIBRARY_VERSION);
			libsidekiqVersionLabel->SetWindowTextW(strLibsidekiqVersion);

			CString strFpgaVersion;
			strFpgaVersion.Format(_T("%u.%u.%u"), fpga->version_major, fpga->version_minor, fpga->version_patch);
			CWnd *fpgaVersionLabel = GetDlgItem(IDC_EDIT_FPGA_VERSION);
			fpgaVersionLabel->SetWindowTextW(strFpgaVersion);
		}

		uint64_t lo_freq = 801000000;
		status = skiq_write_rx_LO_freq(cards[i], skiq_rx_hdl_A1, lo_freq);
		//            printf("MSDEBUG %s:%d\n", __FILE__, __LINE__);
		if (status < 0)
		{
			BASIC_LOG("Error: failed to set LO freq (using previous LO freq)...status is %d\r\n",
				status);
		}
		else {
			BASIC_LOG("Info: configured card %d channel %d Rx LO freq to %" PRIu64 " Hz\r\n", cards[i], skiq_rx_hdl_A1, lo_freq);
		}



		BASIC_LOG("************** Sidekiq Card %u ********************\r\n", cards[i]);
		if (display_fw == true)
		{
			BASIC_LOG("Info: sidekiq firmware version is %d.%d\r\n", firmware_maj, firmware_min);
		}
		BASIC_LOG("Info: sidekiq serial number: %s\r\n", p_serial_num);
		BASIC_LOG("Info: sidekiq hardware version: %s\r\n",
			skiq_hardware_vers_string(hardware_vers));
		BASIC_LOG("Info: sidekiq product version: %s\r\n",
			skiq_product_vers_string(product_vers));
		if (ref_clock != skiq_ref_clock_invalid)
		{
			BASIC_LOG("Info: reference clock is %u\r\n", ref_clock);
		}
		BASIC_LOG("Info: libsidekiq version is %d.%d.%d%s\r\n", 
			libsidekiq_maj, libsidekiq_min, libsidekiq_patch, libsidekiq_label);
		BASIC_LOG("Info: FPGA git hash is 0x%04x\r\n", fpga->git_hash);
		BASIC_LOG("Info: FPGA build date (yymmddhh) is %04x\r\n", fpga->build_date);
		BASIC_LOG("Info: FPGA version %u.%u.%u\r\n", fpga->version_major, fpga->version_minor, fpga->version_patch);
		BASIC_LOG("Info: FPGA Tx FIFO size is ");
		switch (fpga->tx_fifo_size)
		{
		case skiq_fpga_tx_fifo_size_unknown:
			BASIC_LOG("unknown\r\n");
			BASIC_LOG("FPGA Tx FIFO size not available in FPGA versions prior to 1.6\r\n");
			break;

		case skiq_fpga_tx_fifo_size_4k:
			BASIC_LOG("4k samples\r\n");
			break;

		case skiq_fpga_tx_fifo_size_8k:
			BASIC_LOG("8k samples\r\n");
			break;

		case skiq_fpga_tx_fifo_size_16k:
			BASIC_LOG("16k samples\r\n");
			break;

		case skiq_fpga_tx_fifo_size_32k:
			BASIC_LOG("32k samples\r\n");
			break;

		case skiq_fpga_tx_fifo_size_64k:
			BASIC_LOG("64k samples\r\n");
			break;
		}
		// check the availability of the RF port configuration
		if (skiq_read_rf_port_config_avail(cards[i], &rf_port_fixed_avail, &rf_port_tdd_avail) == 0)
		{
			if (rf_port_fixed_avail == true)
			{
				BASIC_LOG("Info: Fixed RF port config available\r\n");
			}
			if (rf_port_tdd_avail == true)
			{
				BASIC_LOG("Info: TDD RF port config available\r\n");
			}
		}
		BASIC_LOG("\r\n");
	}

	skiq_exit();

	return (0);
}


void CSidekiqVersionTestDlg::OnDevModeChange(LPTSTR lpDeviceName)
{
	CDialogEx::OnDevModeChange(lpDeviceName);

	// TODO: Add your message handler code here
	CWnd *label = GetDlgItem(IDC_EDIT2);
	CString message;
	message.Format(_T("%s mode change!\n"), lpDeviceName);
	label->SetWindowTextW(message);
}


void CSidekiqVersionTestDlg::OnClickedButtonSetfrequency()
{
	// TODO: Add your control notification handler code here
//	CString newFrequency;
//	m_frequency.GetWindowText(newFrequency);
//	BASIC_LOG("Requested Frequency: %s\n", (LPCTSTR)newFrequency);
	static uint64_t lo_freq = 801000000;
	lo_freq += 1000000;
	int32_t status = skiq_write_rx_LO_freq(0, skiq_rx_hdl_A1, lo_freq);
	//            printf("MSDEBUG %s:%d\n", __FILE__, __LINE__);
	if (status < 0)
	{
		BASIC_LOG("Error: failed to set LO freq %" PRIu64 " (using previous LO freq)...status is %d\r\n",
			lo_freq, status);
	}
	else {
		//BASIC_LOG("Info: configured Rx LO freq to %" PRIu64 " Hz\r\n", lo_freq);
		BASIC_LOG("Info: configured card %d channel %d Rx LO freq to %" PRIu64 " Hz\r\n", 0, skiq_rx_hdl_A1, lo_freq);
	}
}




void CSidekiqVersionTestDlg::OnDeltaposSpinNumCards(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	//BASIC_LOG("Info: updwn pos=%d delta=%d active=%d max=%d\n", pNMUpDown->iPos, pNMUpDown->iDelta, m_activeCard, m_maxCards);

	m_activeCard = m_activeCard + pNMUpDown->iDelta;
	if (m_activeCard > m_maxCards) {
		m_activeCard = 1;
		//BASIC_LOG("Info: adjusted min newvalue=%d\n", m_activeCard);
	}
	else if (m_activeCard < 1) {
		m_activeCard = m_maxCards;
		//BASIC_LOG("Info: adjusted max newvalue=%d\n", m_activeCard);
	}
	else {
		//BASIC_LOG("Info: newvalue=%d\n", m_activeCard);
	}
	CWnd *numCardsSpin = GetDlgItem(IDC_SPIN_NUM_CARDS);
	((CSpinButtonCtrl *)numCardsSpin)->SetPos(m_activeCard);

	CWnd *numCardsSpinEdit = GetDlgItem(IDC_EDIT_ACTIVE_CARD);
	CString temp;
	temp.Format(_T("%d"), m_activeCard);
	((CEdit *)numCardsSpinEdit)->SetWindowText(temp);

	/* allow the change so return 0 */
	*pResult = 0;
}
