
// SidekiqVersionTestDlg.h : header file
//
#include "Resource.h"
#pragma once
#include <inttypes.h>
#include "afxmaskededit.h"

// CSidekiqVersionDlg dialog
class CSidekiqVersionDlg : public CDialogEx
{
// Construction
public:
	CSidekiqVersionDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_VERSION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonGo();
	CString m_strEditLabel;
	afx_msg void OnDevModeChange(LPTSTR lpDeviceName);

	int32_t CSidekiqVersionDlg::DoVersionTest(uint8_t num_cards, uint8_t *cards);
	afx_msg void OnBnClickedOk();
	CMFCMaskedEdit m_frequency;
	afx_msg void OnClickedButtonSetfrequency();
	afx_msg void OnDeltaposSpinNumCards(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	int m_activeCard;
	int m_maxCards;
};
#pragma once


