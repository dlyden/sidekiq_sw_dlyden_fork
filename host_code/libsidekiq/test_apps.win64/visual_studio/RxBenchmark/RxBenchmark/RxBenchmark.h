
// RxBenchmark.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "Resource.h"		// main symbols


// CRxBenchmarkApp:
// See RxBenchmark.cpp for the implementation of this class
//

class CRxBenchmarkApp : public CWinApp
{
public:
	CRxBenchmarkApp();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int  ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CRxBenchmarkApp theApp;