//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RxBenchmark.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_RXBENCHMARK_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_VERSION_DIALOG              132
#define IDD_SIDEKIQVERSIONTEST_DIALOG   134
#define IDC_BUTTON_GO                   1000
#define IDC_BUTTON1                     1000
#define IDC_BUTTON_DEVICE               1000
#define IDC_EDIT_NUM_CARDS              1001
#define IDC_BUTTON_START_RX             1001
#define IDC_EDIT2                       1002
#define IDC_EDIT_SIDEKIQ_HW_VERSION     1002
#define IDC_STATIC_STATUS               1002
#define IDC_EDIT_SERIAL_NUMBER          1003
#define IDC_EDIT_STATUS                 1003
#define IDC_EDIT_                       1004
#define IDC_EDIT_FPGA_VERSION           1004
#define IDC_EDIT3                       1004
#define IDC_EDIT_SAMPLE_RATE            1004
#define IDC_MFCMASKEDEDIT_FREQUENCY     1005
#define IDC_BUTTON_SETFREQUENCY         1006
#define IDC_LIST_TEST_RESULTS           1006
#define IDC_EDIT_SIDEKIQ_FW_VERSION     1007
#define IDC_EDIT_PRODUCT_VERSION        1008
#define IDC_EDIT_SIDEKIQ_LIBRARY_VERSION 1009
#define IDC_SPIN_NUM_CARDS              1010
#define IDC_EDIT_ACTIVE_CARD            1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
