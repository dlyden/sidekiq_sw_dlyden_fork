// RxBenchmarkMonitor.cpp : implementation file
//

#include "stdafx.h"
#include "RxBenchmark.h"
#include "RxBenchmarkDlg.h"
#include "RxBenchmarkMonitor.h"

#include "sidekiq_api.h"

#include <conio.h>
#define BASIC_LOG	_cprintf

// CRxBenchmarkMonitor

IMPLEMENT_DYNCREATE(CRxBenchmarkMonitor, CWinThread)

CRxBenchmarkMonitor::CRxBenchmarkMonitor(void *threadArgs)
{
	RxBenchmarkThreadArgs_t *args = (RxBenchmarkThreadArgs_t *)threadArgs;

	m_pParent = (CWnd *)args->parentWnd;
	m_sampleRate = args->sampleRate;
    m_activeCard = args->cardId - 1;

	// kill event starts out in the signaled state
	m_hEventKill = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hEventDead = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_startTime = CTime::GetCurrentTime();

	benchmarkData.curr_ts_1 = 0;
	benchmarkData.curr_ts_2 = 0;
	benchmarkData.next_ts_1 = 0;
	benchmarkData.next_ts_2 = 0;
	benchmarkData.first_block_1 = true;
	benchmarkData.first_block_2 = true; 
	benchmarkData.ts_errors_1 = 0;
	benchmarkData.ts_errors_2 = 0;
	benchmarkData.totalBytes = 0;
    benchmarkData.bytesInInterval = 0;

}

CRxBenchmarkMonitor::CRxBenchmarkMonitor()
{
	/* don't automatically delete the thread */
	m_bAutoDelete = false;

	m_pParent = NULL;
	m_sampleRate = 0;
	m_hEventKill = INVALID_HANDLE_VALUE;
	m_hEventDead = INVALID_HANDLE_VALUE;
}

CRxBenchmarkMonitor::~CRxBenchmarkMonitor()
{
	CloseHandle(m_hEventKill);
	CloseHandle(m_hEventDead);
}

bool CRxBenchmarkMonitor::InitReceive(uint8_t activeCard)
{
	uint8_t card = activeCard;
	uint8_t num_chans = 1;
	skiq_rx_hdl_t hdl = skiq_rx_hdl_A1;
	uint32_t sample_rate = m_sampleRate;
	// maximize the sample rate and start streaming
	BASIC_LOG("Setting sample rate to %d samples/sec\n", sample_rate);
    if (skiq_write_rx_sample_rate_and_bandwidth(card, hdl, sample_rate, sample_rate) != 0) {
        BASIC_LOG("Failed to set sample rate to %d samples/sec\n", sample_rate);
        return (false);
    }
	if (num_chans == 2)
	{
		if (skiq_write_chan_mode(card, skiq_chan_mode_dual) != 0)
		{
			BASIC_LOG("Error: unable to configure channel mode\r\n");
		}
		//BASIC_LOG("start streaming A2\r\n");
		skiq_start_rx_streaming(card, skiq_rx_hdl_A2);
	}
	skiq_start_rx_streaming(card, hdl);

    m_startTime = CTime::GetCurrentTime();


    return (true);
}


bool CRxBenchmarkMonitor::DoReceive(uint8_t activeCard)
{
	uint8_t card = activeCard;
	uint8_t num_chans = 1;
	skiq_rx_hdl_t hdl = skiq_rx_hdl_A1;
	skiq_rx_hdl_t rx_hdl;
	skiq_rx_block_t* p_data;
	uint32_t data_len;
	bool gotNewData = false;


	static WM_STATUS_UPDATE_DATA uiData; // = { 0 };
	static bool doneOnce = false;
	if (!doneOnce) {
		uiData.totalData = 0;
		doneOnce = true;
        m_startTime = CTime::GetCurrentTime();
	}

	if (skiq_receive(card,
		&rx_hdl,
		&p_data,
		&data_len) == skiq_rx_status_success)
	{
		//BASIC_LOG("%s:%d success\n", __FILE__, __LINE__);

		static bool notYetRun = true;
		if (notYetRun) {
			BASIC_LOG("%s:%d ...\n", __FILE__, __LINE__);
			notYetRun = false;
		}

		if (rx_hdl == skiq_rx_hdl_A1)
		{
			benchmarkData.curr_ts_1 = p_data->rf_timestamp;
			if (benchmarkData.first_block_1 == false)
			{
				if (benchmarkData.curr_ts_1 != benchmarkData.next_ts_1)
				{
					benchmarkData.ts_errors_1++;
				}
			}
			else
			{
				benchmarkData.first_block_1 = false;
			}

			benchmarkData.next_ts_1 = benchmarkData.curr_ts_1 + (data_len / 4) - SKIQ_RX_HEADER_SIZE_IN_WORDS;
		}
		else
		{
            ASSERT(true); // don't handle two channels at the moment
			benchmarkData.curr_ts_2 = *((uint64_t*)(p_data));
			if (benchmarkData.first_block_2 == false)
			{
				if (benchmarkData.curr_ts_2 != benchmarkData.next_ts_2)
				{
					benchmarkData.ts_errors_2++;
				}
			}
			else
			{
				benchmarkData.first_block_2 = false;
			}

			benchmarkData.next_ts_2 = benchmarkData.curr_ts_2 + (data_len / 4) - SKIQ_RX_HEADER_SIZE_IN_WORDS;
		}
		benchmarkData.totalBytes += data_len;
        benchmarkData.bytesInInterval += data_len;


        /* determine if sufficient time has elapsed to warrant a UI update */
        CTime nowTime = CTime::GetCurrentTime();
        CTimeSpan elapsedTime = nowTime - m_startTime;
        if (elapsedTime.GetSeconds() > 1) {
            uiData.seconds = (int)elapsedTime.GetSeconds();
            uiData.totalData = benchmarkData.totalBytes;
            uiData.totalErrors = benchmarkData.ts_errors_1;
            /* calculate rate, in MB/s */
            uiData.rate = ((benchmarkData.bytesInInterval / (1024*1024)) / uiData.seconds);

            m_pParent->PostMessage(UWM_STATUS_UPDATE, (WPARAM)&uiData, 0);

            m_startTime = nowTime;
            benchmarkData.bytesInInterval = 0;
        }

		gotNewData = true;
	}

	return (gotNewData);
}

void CRxBenchmarkMonitor::ShutdownReceive(uint8_t activeCard)
{
	uint8_t card = activeCard;
	uint8_t num_chans = 1;
	skiq_rx_hdl_t hdl = skiq_rx_hdl_A1;

	if (num_chans == 2)
	{
		skiq_stop_rx_streaming(card, skiq_rx_hdl_A2);
	}
	skiq_stop_rx_streaming(card, hdl);
}

BOOL CRxBenchmarkMonitor::InitInstance()
{
	BASIC_LOG("%s:%d\n", __FUNCTION__, __LINE__);


    skiq_xport_type_t init_type = skiq_xport_type_pcie;
    skiq_xport_init_level_t init_level = skiq_xport_init_level_full;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
	uint8_t num_cards = 0;
	uint8_t m_maxCards = 0;
	//int8_t  m_activeCard = -1;
	bool initOk = false;

#if 1
	/* determine how many Sidekiqs are there and their card IDs */
	BASIC_LOG("calling skiq_get_cards!\n");

	/* Probe PCIe but not USB */
		BASIC_LOG("skiq_probe complete!\n");

		if (skiq_get_cards(init_type, &num_cards, cards) != 0) {
			BASIC_LOG("Getting cards failed!\n");
		}
		else {
			m_maxCards = num_cards;
			if (m_maxCards > 0) {
				//m_activeCard = 0;

				/* bring up the USB and PCIe interfaces for all the cards */
                int32_t status = skiq_init(init_type, init_level, cards, num_cards);
				if (status != 0)
				{
					BASIC_LOG("Error: unable to initialize libsidekiq with status %d\n", status);
				}
				else {
					initOk = true;
                    BASIC_LOG("Info: initialized libsidekiq with type %d and level %d\n", init_type, init_level);
				}
			}
		}
#else
    /* we use the version dialog to select the card, so don't probe above and instead only skiq_init() */
    //m_activeCard = 0;

    /* bring up the USB and PCIe interfaces for the desired card */
    num_cards = 1;
    /* activeCard is not zero-based like skiq_XXX is, so adjust */
    cards[0] = (m_activeCard - 1);
//    cards[0] = m_activeCard;
    BASIC_LOG("Info: attempt to initialize libsidekiq with PCIe level %d and USB level %d and card[0]=%d and num_cards=%d\n", pcie_level, usb_level, cards[0], num_cards);
    int32_t status = skiq_init(pcie_level, usb_level, cards, num_cards);
    if (status != 0)
    {
        BASIC_LOG("Error: unable to initialize libsidekiq with status %d\n", status);
    }
    else {
        initOk = true;
        BASIC_LOG("Info: initialized libsidekiq with PCIe level %d and USB level %d\n", pcie_level, usb_level);
    }

#endif /* 0 */

    if (!InitReceive(m_activeCard)) {
        return (false);
    }

	//SetThreadPriority(THREAD_PRIORITY_ABOVE_NORMAL);

	// wait for the kill notification.  service the 
	// message queue.
	BASIC_LOG("%s:%d waiting for kill...\n", __FUNCTION__, __LINE__);
	bool continueTest = true;
	while (continueTest)
	{
		if (initOk) {
			if (!DoReceive(m_activeCard)) {
				/* didn't get new data */
			}
			else {
				DWORD waitStatus = WaitForSingleObject(m_hEventKill, 0);
				if (waitStatus == WAIT_TIMEOUT) {

				}
				else if (waitStatus == WAIT_OBJECT_0) {
					BASIC_LOG("%s:%d received kill event...\n", __FUNCTION__, __LINE__);
					/* kill event was signalled, so exit */
					continueTest = false;
				}
			}
		}
	}

	
	BASIC_LOG("%s:%d preparing exit...\n", __FUNCTION__, __LINE__);
	if (initOk) {
		BASIC_LOG("%s:%d shutting down...\n", __FUNCTION__, __LINE__);

		CRxBenchmarkMonitor::ShutdownReceive(m_activeCard);
		BASIC_LOG("%s:%d skiq_exit...\n", __FUNCTION__, __LINE__);
		skiq_exit();
	}
	BASIC_LOG("%s:%d returning...\n", __FUNCTION__, __LINE__);

	// avoid entering standard message loop by returning FALSE
	return FALSE;
}

int CRxBenchmarkMonitor::ExitInstance()
{
	BASIC_LOG("%s:%d\n", __FUNCTION__, __LINE__);
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

void CRxBenchmarkMonitor::KillThread(const int reason)
{
	BASIC_LOG("%s:%d\n", __FUNCTION__, __LINE__);
	// reset the m_hEventKill which signals the thread 
	// to shutdown
    //m_bRun = false;
	VERIFY(SetEvent(m_hEventKill));
	BASIC_LOG("%s:%d set kill event\n", __FUNCTION__, __LINE__);

	// allow thread to run at higher priority during 
	// kill process
	//SetThreadPriority(THREAD_PRIORITY_ABOVE_NORMAL);
	BASIC_LOG("%s:%d waiting for dead event\n", __FUNCTION__, __LINE__);
	//WaitForSingleObject(m_hEventDead, INFINITE);
	WaitForSingleObject(m_hEventDead, 5000);
	BASIC_LOG("%s:%d waiting for m_hThread event\n", __FUNCTION__, __LINE__);
	//WaitForSingleObject(m_hThread, INFINITE);
	WaitForSingleObject(m_hThread, 1000);
	BASIC_LOG("%s:%d deleting object\n", __FUNCTION__, __LINE__);

	// now delete CWinThread object since no longer necessary
	delete this;

}
void CRxBenchmarkMonitor::Delete()
{
	BASIC_LOG("%s:%d\n", __FUNCTION__, __LINE__);

	// calling the base here won't do anything but it is a 
	// good habit
//	CWinThread::Delete();
	BASIC_LOG("%s:%d base class called\n", __FUNCTION__, __LINE__);

	// acknowledge receipt of kill notification
	VERIFY(SetEvent(m_hEventDead));
	BASIC_LOG("%s:%d set dead event\n", __FUNCTION__, __LINE__);

}

BEGIN_MESSAGE_MAP(CRxBenchmarkMonitor, CWinThread)
END_MESSAGE_MAP()


// CRxBenchmarkMonitor message handlers
