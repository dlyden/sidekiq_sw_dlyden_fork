
// RxBenchmarkDlg.h : header file
//

#pragma once
#include <inttypes.h>

#define UWM_STATUS_UPDATE	(WM_APP + 1)
#define UWM_CARD_SELECTION  (WM_APP + 2)

typedef struct _tagWM_STATUS_UPDATE_DATA {
    int      seconds;
    uint64_t totalData;
    uint64_t totalErrors;
    uint32_t rate; // in MB/s
} WM_STATUS_UPDATE_DATA;

typedef struct _tagWM_CARD_SELECTION_DATA {
    uint8_t  card;   
} WM_CARD_SELECTION_DATA;


// CRxBenchmarkDlg dialog
class CRxBenchmarkDlg : public CDialogEx
{
// Construction
public:
	CRxBenchmarkDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_RXBENCHMARK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	bool m_start;
    uint8_t m_activeCard;
	CTime m_startTime;
	CTime m_nowTime;


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClickedButtonDevice();
	afx_msg void OnClickedButtonStartStopRx();
    LRESULT CRxBenchmarkDlg::OnUwmStatusUpdate(WPARAM wparam, LPARAM lparam);
    LRESULT CRxBenchmarkDlg::OnUwmCardSelection(WPARAM wparam, LPARAM lparam);

};
