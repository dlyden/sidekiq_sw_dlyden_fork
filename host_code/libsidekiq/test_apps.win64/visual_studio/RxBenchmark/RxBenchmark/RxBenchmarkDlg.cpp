
// RxBenchmarkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "RxBenchmark.h"
#include "RxBenchmarkDlg.h"
#include "afxdialogex.h"
#include "SidekiqVersionDlg.h"
#include "RxBenchmarkMonitor.h"
#include <conio.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define BASIC_LOG	_cprintf



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CRxBenchmarkDlg dialog



CRxBenchmarkDlg::CRxBenchmarkDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(CRxBenchmarkDlg::IDD, pParent), m_start(true), m_activeCard(-1), m_startTime(), m_nowTime()

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRxBenchmarkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CRxBenchmarkDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_DEVICE, &CRxBenchmarkDlg::OnClickedButtonDevice)
	ON_BN_CLICKED(IDC_BUTTON_START_RX, &CRxBenchmarkDlg::OnClickedButtonStartStopRx)
    ON_MESSAGE(UWM_STATUS_UPDATE, OnUwmStatusUpdate)
    ON_MESSAGE(UWM_CARD_SELECTION, OnUwmCardSelection)
END_MESSAGE_MAP()


LRESULT CRxBenchmarkDlg::OnUwmCardSelection(WPARAM wparam, LPARAM lparam)
{
    WM_CARD_SELECTION_DATA *pCardSelection = (WM_CARD_SELECTION_DATA *)wparam;

    if (pCardSelection) {
        BASIC_LOG("in OnUwmCardSelection, selecting card %d!\n", pCardSelection->card);

        CWnd *buttonStartStop = GetDlgItem(IDC_BUTTON_START_RX);
        buttonStartStop->EnableWindow(true);
        m_activeCard = pCardSelection->card;
    }
    else {
        BASIC_LOG("in OnUwmCardSelection, no card selection made!\n");
    }

    return (0);
}

// CRxBenchmarkDlg message handlers
LRESULT CRxBenchmarkDlg::OnUwmStatusUpdate(WPARAM wparam, LPARAM lparam)
{
	WM_STATUS_UPDATE_DATA *pStatusData = (WM_STATUS_UPDATE_DATA *)wparam;

	CWnd *editStatusLabel = GetDlgItem(IDC_EDIT_STATUS);
	static CString status;
	CString units("");
    int seconds = pStatusData->seconds;
    uint64_t totalBytes = pStatusData->totalData;
    uint64_t totalErrors = pStatusData->totalErrors;
    uint32_t rate = pStatusData->rate;

/*
	if (pStatusData->totalData < (1024)) {
		units = "Bytes";
	}
	else if (pStatusData->totalData < (1024 * 1024)) {
		units = "KBytes";
		totalBytes /= (1024);
	}
	else if (pStatusData->totalData < (1024 * 1024 * 1024)) {
		units = "MBytes";
		totalBytes /= (1024 * 1024);
	} // cast to 64-bit type to avoid overflow 
	else if (pStatusData->totalData < ((uint64_t)1024 * 1024 * 1024 * 1024)) {
		units = "GBytes";
		totalBytes /= (1024 * 1024 * 1024);
	}
*/
	m_nowTime = CTime::GetCurrentTime();
	CTimeSpan elapsedTime = m_nowTime - m_startTime;

    status.Format(_T("%d MB/s"), rate);
    CListCtrl *resultsListCtrl = (CListCtrl *)GetDlgItem(IDC_LIST_TEST_RESULTS);
    int nIndex = resultsListCtrl->InsertItem(0, status);
    status.Format(_T("%d"), totalErrors);
    resultsListCtrl->SetItemText(nIndex, 1, status);
    status = elapsedTime.Format("%Dd %Hh %Mm %Ss");
    resultsListCtrl->SetItemText(nIndex, 2, status);
    resultsListCtrl->EnsureVisible(0, true);

    
//    status.Format(_T("%d MB/s %d %s %d errors in "), rate, totalBytes, units, totalErrors);
    status.Format(_T("%d MB/s %s %d errors in "), rate, units, totalErrors);
    status += elapsedTime.Format("%Dd %Hh %Mm %Ss");
	((CEdit *)editStatusLabel)->SetWindowText(status);



	return 0;
}

BOOL CRxBenchmarkDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
    CWnd *sampleRateEdit = GetDlgItem(IDC_EDIT_SAMPLE_RATE);
    sampleRateEdit->SetWindowTextW(_T("10000000"));

    CListCtrl *resultsListCtrl = (CListCtrl *)GetDlgItem(IDC_LIST_TEST_RESULTS);
    resultsListCtrl->EnsureVisible(0, true);
    resultsListCtrl->InsertColumn(0, _T("Rate"), LVCFMT_LEFT, 90);
    resultsListCtrl->InsertColumn(1, _T("Cumulative Errors"), LVCFMT_LEFT, 120);
    resultsListCtrl->InsertColumn(2, _T("Time"), LVCFMT_LEFT, 120);

    m_activeCard = -1;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CRxBenchmarkDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CRxBenchmarkDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CRxBenchmarkDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CRxBenchmarkDlg::OnClickedButtonDevice()
{
	BASIC_LOG("in OnClickedButtonDevice!\n");
	// TODO: Add your control notification handler code here
	CSidekiqVersionDlg *dlg = new CSidekiqVersionDlg(this);
	BASIC_LOG("in OnClickedButtonDevice 2!\n");
	dlg->Create(CSidekiqVersionDlg::IDD);
	dlg->ShowWindow(SW_SHOW);
	BASIC_LOG("in OnClickedButtonDevice 3!\n");
}


void CRxBenchmarkDlg::OnClickedButtonStartStopRx()
{
	// TODO: Add your control notification handler code here
	CString startText("&Start");
	CString stopText("&Stop");
	CWnd *buttonLabel = GetDlgItem(IDC_BUTTON_START_RX);

	CWnd *sampleRateEdit = GetDlgItem(IDC_EDIT_SAMPLE_RATE);
	uint32_t requestedSampleRate;
	CString strRequestedSampleRate;

	if (m_start) {

		sampleRateEdit->GetWindowTextW(strRequestedSampleRate);

		int numConverted = swscanf_s(strRequestedSampleRate.GetString(), _T("%u"), &requestedSampleRate);
		if (numConverted != 1) {
			AfxMessageBox(_T("Invalid Sample Rate"));
			return;
		}
//		else {
//			CString msg;
//			msg.Format(_T("%u"), requestedSampleRate);
//			AfxMessageBox(msg);
//		}
	}


	static CRxBenchmarkMonitor *pThread = NULL;
	if (m_start) {
        /* Clear out any old data */
        CListCtrl *resultsListCtrl = (CListCtrl *)GetDlgItem(IDC_LIST_TEST_RESULTS);
        int nCount = resultsListCtrl->GetItemCount();

        // Delete all of the items from the list view control.
        for (int i = 0; i < nCount; i++)
        {
            resultsListCtrl->DeleteItem(0);
        }



		BASIC_LOG("starting thread!\n");
		buttonLabel->SetWindowTextW(stopText);

		RxBenchmarkThreadArgs_t args;
		args.parentWnd = (void *)this;
		args.sampleRate = requestedSampleRate;
        args.cardId = m_activeCard;
        BASIC_LOG("starting thread with active card %d!\n", m_activeCard);

		pThread = new CRxBenchmarkMonitor((void *)&args);
		pThread->CreateThread();
		m_start = false;

		m_startTime = CTime::GetCurrentTime();

	}
	else {
		BASIC_LOG("stopping thread!\n");
		buttonLabel->SetWindowTextW(startText);
		if (pThread != NULL) {
			pThread->KillThread(0 /* reason: user cancelled */);
			//WaitForSingleObject(pThread->m_hThread, INFINITE);
			//delete(pThread);
		}
		m_start = true;
	}
}
