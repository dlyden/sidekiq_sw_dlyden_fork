#pragma once

#include <inttypes.h>

typedef struct {
	void *parentWnd;
	uint32_t sampleRate;
    uint8_t cardId;
} RxBenchmarkThreadArgs_t;



// CRxBenchmarkMonitor

class CRxBenchmarkMonitor : public CWinThread
{
	DECLARE_DYNCREATE(CRxBenchmarkMonitor)
public:
	CRxBenchmarkMonitor(void *threadArgs);
	virtual ~CRxBenchmarkMonitor();
	virtual void Delete();
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	void KillThread(const int reason);

protected:
	CRxBenchmarkMonitor();           // protected constructor used by dynamic creation
//	virtual ~CRxBenchmarkMonitor();

	CWnd *m_pParent;
	uint32_t m_sampleRate;
    uint8_t m_activeCard;
	HANDLE m_hEventKill;
	HANDLE m_hEventDead;
    CTime m_startTime;

	typedef struct {
		uint64_t curr_ts_1;
		uint64_t curr_ts_2;
		uint64_t next_ts_1;
		uint64_t next_ts_2;
		bool first_block_1;
		bool first_block_2;
		uint64_t ts_errors_1; // # timestamp errors (A1)
		uint64_t ts_errors_2; // # timestamp errors (A2)
		uint64_t totalBytes;
        uint64_t bytesInInterval; // # bytes over a timed interval, used to track a rate
	} benchmarkData_t;
	benchmarkData_t benchmarkData;

	bool InitReceive(uint8_t m_activeCard);
	bool DoReceive(uint8_t m_activeCard);
	void ShutdownReceive(uint8_t m_activeCard);

protected:
	DECLARE_MESSAGE_MAP()
};


