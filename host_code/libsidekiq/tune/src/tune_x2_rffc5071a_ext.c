/*****************************************************************************/
/** @file tune_x2_rffc5071a_ext.c

 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

/***** INCLUDES *****/

#include "tune.h"
#include "tune_private.h"
#include "tune_x2_rffc5071a_ext.h"
#include "rffc5071a_api.h"
#include "sidekiq_rffc5071a_private.h"

/***** DEFINES *****/

/** @brief The default intermediate frequency. */
#define DEFAULT_MIX_FREQ (1000e6) // 1000Mhz / 1Ghz

/***** MACROS *****/

/** @brief Helper macro to determine if a Rx frequency is low band. */
#define X2_IS_RX_LOWBAND(freq) \
    ((0 != (freq)) && (X2_RX_LOW_BAND_MAX_FREQ > (freq)))

/** @brief Helper macro to determine if a Rx frequency is high band. */
#define X2_IS_RX_HIGHBAND(freq) \
    (!X2_IS_RX_LOWBAND(freq))

/** @brief Helper macro to determine if a Tx frequency is low band. */
#define X2_IS_TX_LOWBAND(freq) \
    ((0 != (freq)) && (X2_TX_LOW_BAND_MAX_FREQ > (freq)))

/** @brief Helper macro to determine if a Tx frequency is high band. */
#define X2_IS_TX_HIGHBAND(freq) \
    (!X2_IS_TX_LOWBAND(freq))

/***** LOCAL DATA *****/

/** @brief Local data for holding the intermediate frequency used for mixing. */
static float _mix_freq[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = DEFAULT_MIX_FREQ,
};

/***** LOCAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Initializes the tune module for Sidekiq X2.

    @param card         requested Sidekiq card ID
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _x2_rffc_init(uint8_t card)
{
    uint8_t maj = 0, min = 0, patch = 0;
    (void) card;

    rffc5071a_get_lib_version(&maj, &min, &patch);
    _skiq_log(SKIQ_LOG_INFO, "librffc5071a v%u.%u.%u\n", maj, min, patch);

    // MREUTMAN: Initialization of the RFFC is currently done in the RFE module
    // since it is also used to switch filter paths. Should we move that code
    // in here or just leave it as is and only call the LO tuning function
    // from that library?

    tune_cache_mix_freq(card, _mix_freq[card]);

    return 0;
}

/*****************************************************************************/
/** @brief Initializes the tune module for Sidekiq X2.

    @param card         requested Sidekiq card ID
    @return void
*/
static void _x2_rffc_exit(uint8_t card)
{
    uint64_t id = 0;

    // MREUTMAN: The RFE code doesn't have an exit function, so we have to
    // clean up the rffc5071a code over here.
    id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, skiq_rx_hdl_A1);
    rffc5071a_exit(id);

    id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, skiq_rx_hdl_B1);
    rffc5071a_exit(id);
}

static int32_t _read_rx_LO_freq_range(uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min)
{
    (void) card;

    if( (skiq_rx_hdl_A1 == hdl) ||
        (skiq_rx_hdl_A2 == hdl) ||
        (skiq_rx_hdl_B1 == hdl) )
    {
        *p_max = X2_RX_HIGH_BAND_MAX_FREQ;
        *p_min = X2_RX_LOW_BAND_MIN_FREQ;
    }
    else
    {
        *p_max = 0;
        *p_min = 0;
    }

    return(0);
}

static int32_t _read_tx_LO_freq_range(uint8_t card,
                                   skiq_tx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min)
{
    (void) card;

    if( skiq_tx_hdl_A1 == hdl )
    {
        // uses RFFC for frequency mixing, allows low band tuning
        *p_max = X2_TX_HIGH_BAND_MAX_FREQ;
        *p_min = X2_TX_LOW_BAND_MIN_FREQ;
    }
    else if( skiq_tx_hdl_A2 == hdl )
    {
        // tx A2 does not have an RFFC for frequency mixing
        *p_max = X2_TX_HIGH_BAND_MAX_FREQ;
        *p_min = X2_TX_HIGH_BAND_MIN_FREQ;
    }
    else
    {
        *p_max = 0;
        *p_min = 0;
    }

    return(0);
}

/*****************************************************************************/
/** @brief Helper function used to validate that a given frequency will not
    result in any configuration errors.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @return void
*/
static int32_t _x2_verify_tx_low_band_freq(uint8_t card,
                                           skiq_tx_hdl_t hdl,
                                           uint64_t freq)
{
#ifdef ATE_SUPPORT
    // TODO: remove this when sample rate can be set properly to allow
    // Ken to perform all of the testing he needs.
    (void) card;
    (void) hdl;
    (void) freq;
#else
    uint32_t rate = 0, bw = 0, actual_bw = 0;
    double actual_rate = 0.0;
    int32_t status = 0;

    status = skiq_read_tx_sample_rate_and_bandwidth(card,
                                                    hdl,
                                                    &rate,
                                                    &actual_rate,
                                                    &bw,
                                                    &actual_bw);
    if( 0 != status )
    {
        return status;
    }
    else if( (0 != rate) && (((rate / 2) > freq) || ((bw / 2) > freq)) )
    {
        // we don't allow collection of I/Q samples at negative frequencies
        return -1;
    }
#endif
    return 0;
}

/*****************************************************************************/
/** @brief Routine for tuning the Tx LO for Sidekiq X2.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _x2_rffc_write_tx_LO_freq(uint8_t card,
                                         skiq_tx_hdl_t hdl,
                                         uint64_t freq,
                                         skiq_freq_tune_mode_t mode,
                                         uint64_t rf_timestamp)
{
    rfic_t rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    rfe_t rfe_instance = _skiq_get_tx_rfe_instance(card, hdl);
    skiq_rx_hdl_t rx_hdl = skiq_rx_hdl_end;
    uint64_t ad9371_tx_freq = 0;
    uint64_t rffc5071a_freq = 0;
    uint64_t rx_freq = 0;
    double actual = 0.0;
    uint64_t id = 0;
    int32_t status = -1;

    // X2 doesn't support freq hopping
    if( mode == skiq_freq_tune_mode_hop_on_timestamp )
    {
        skiq_error("RF IC unable to support hopping on timestamp tuning mode (card=%u", card);
        status = -ENOTSUP;
        return (status);
    }

    VALIDATE_FREQ(freq, X2_TX_HIGH_BAND_MAX_FREQ, X2_TX_LOW_BAND_MIN_FREQ);

    if( freq < X2_TX_LOW_BAND_MAX_FREQ )
    {
        if( skiq_tx_hdl_A1 != hdl )
        {
            // Only TxA1 has low band capabilities.
            return -1;
        }

        // make sure the sample rate and bandwidth won't go into negative freq
        status = _x2_verify_tx_low_band_freq(card, hdl, freq);
        if( 0 != status )
        {
            return status;
        }

        // RxB1 shares the same LO for low frequency tuning, we need to
        // coordinate its use when tuning.
        rx_hdl = skiq_rx_hdl_B1;
        tune_read_rx_LO_freq(card, rx_hdl, &rx_freq, &actual);
        id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, rx_hdl);
        status = rffc5071a_get_freq(id, &rffc5071a_freq);
        if( 0 != status )
        {
            return -1;
        }

        // Use low side injection mixing (i.e. RFFC5071A tuned below AD9371) to
        // achieve the desired frequency.
        if( (0 != rffc5071a_freq) && (X2_IS_RX_LOWBAND(rx_freq)) )
        {
            // RxB1 is actively tuned to receive in low band.
            ad9371_tx_freq = rffc5071a_freq + freq;
        }
        else
        {
            // RxB1 is not using the RFFC5071a, we can tune it to what we want.
            rffc5071a_freq = _mix_freq[card];
            ad9371_tx_freq = rffc5071a_freq + freq;

            status = rffc5071a_set_freq(id, rffc5071a_freq);
            if( 0 != status )
            {
                _skiq_log(SKIQ_LOG_ERROR,
                          "failed to set rffc5071a frequency\n");
                return -1;
            }
        }
    }
    else
    {
        // high band
        if( skiq_tx_hdl_A1 == hdl )
        {
            // RxB1 shares RFFC, check to see if it is being used
            rx_hdl = skiq_rx_hdl_B1;
            tune_read_rx_LO_freq(card, rx_hdl, &rx_freq, &actual);
            if( !X2_IS_RX_LOWBAND(rx_freq) )
            {
                // RxB1 is not using RFFC, disable it until it is needed
                id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, rx_hdl);
                status = rffc5071a_disable(id);
                if( 0 != status )
                {
                    _skiq_log(SKIQ_LOG_ERROR, "failed to disable rffc5071a\n");
                    return status;
                }
            }
        }
        ad9371_tx_freq = freq;
    }

    /* update RFE prior to re-tuning based on the passed in freq */
    status = rfe_update_before_tx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s before Tx LO frequency tune on card %u\n",
                   tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    if( mode == skiq_freq_tune_mode_standard )
    {
        status = rfic_write_tx_freq(&rfic_instance, ad9371_tx_freq, &actual);
    }
    else
    {
        status = rfic_tx_hop(&rfic_instance, rf_timestamp, &actual);
    }
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to set Tx LO frequency\n");
        return status;
    }

    tune_cache_tx_LO_freq(card, hdl, freq, actual);

    /* update RFE after re-tuning based on the passed in freq */
    status = rfe_update_after_tx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s after Tx LO frequency tune on card %u\n",
                   tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    return 0;
}

/*****************************************************************************/
/** @brief Helper function used to validate that a given frequency will not
    result in any configuration errors.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @return void
*/
static int32_t _x2_verify_rx_low_band_freq(uint8_t card,
                                           skiq_rx_hdl_t hdl,
                                           uint64_t freq)
{
#ifdef ATE_SUPPORT
    // TODO: remove this when sample rate can be set properly to allow
    // Ken to perform all of the testing he needs.
    (void) card;
    (void) hdl;
    (void) freq;
#else
    uint32_t rate = 0, bw = 0, actual_bw = 0;
    double actual_rate = 0.0;
    int32_t status = 0;

    status = skiq_read_rx_sample_rate_and_bandwidth(card,
                                                    hdl,
                                                    &rate,
                                                    &actual_rate,
                                                    &bw,
                                                    &actual_bw);
    if( 0 != status )
    {
        return status;
    }
    else if( (0 != rate) && (((rate / 2) > freq) || ((bw / 2) > freq)) )
    {
        // we don't allow collection of I/Q samples at negative frequencies
        return -1;
    }
#endif
    return 0;
}

/*****************************************************************************/
/** @brief Routine for tuning the Rx LO for Sidekiq X2.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _x2_rffc_write_rx_LO_freq(uint8_t card,
                                         skiq_rx_hdl_t hdl,
                                         uint64_t freq,
                                         skiq_freq_tune_mode_t mode,
                                         uint64_t rf_timestamp)
{
    rfic_t rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    rfe_t rfe_instance = _skiq_get_rx_rfe_instance(card, hdl), rfe_instance_other;
    skiq_rx_hdl_t hdl_other = rfic_rx_hdl_map_other(&rfic_instance);
    uint64_t id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, hdl);
    uint64_t ad9371_rx_freq = 0;
    uint64_t rffc5071a_freq = 0;
    uint64_t tx_freq = 0;
    double actual = 0.0;
    int32_t status = -1;

    // X2 doesn't support freq hopping on timestamp
    if( mode == skiq_freq_tune_mode_hop_on_timestamp )
    {
        skiq_error("RF IC unable to support hopping on timestamp tuning mode (card=%u", card);
        status = -ENOTSUP;
        return (status);
    }

    VALIDATE_FREQ(freq, X2_RX_HIGH_BAND_MAX_FREQ, X2_RX_LOW_BAND_MIN_FREQ);

    if( freq < X2_RX_LOW_BAND_MAX_FREQ )
    {
        // make sure the sample rate and bandwidth won't go into negative freq
        status = _x2_verify_rx_low_band_freq(card, hdl, freq);
        if( 0 != status )
        {
            return status;
        }

        // Use low side injection mixing (i.e. RFFC5071A tuned below AD9371) to
        // achieve the desired frequency.
        // TODO: There is a 400Mhz filter in the Rx line up that will attenuate
        // the signal. I don't think we can avoid the attenuation in order to
        // maintain low side injection, avoiding spectral inversion, given the
        // tuning limitations on the AD9371 and the RFFC5071a chip.
        rffc5071a_freq = _mix_freq[card];
        ad9371_rx_freq = rffc5071a_freq + freq;

        status = rffc5071a_set_freq(id, rffc5071a_freq);
        if( 0 != status )
        {
            _skiq_log(SKIQ_LOG_ERROR, "failed to set rffc5071a frequency\n");
            return status;
        }
    }
    else
    {
        // Tuning highband, use the AD9371 for tuning directly.
        ad9371_rx_freq = freq;
    }

    if( (skiq_rx_hdl_A1 == hdl) || (skiq_rx_hdl_A2 == hdl) )
    {
        // These RF interfaces share the same tuning frequency, so tuning
        // one of these is the same as tuning the other as well.
        rfe_instance_other = _skiq_get_rx_rfe_instance(card, hdl_other);
    }
    else
    {
        hdl_other = skiq_rx_hdl_end;
    }

    /* update RFE prior to re-tuning based on the passed in freq */
    status = rfe_update_before_rx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s before Rx LO frequency tune on card %u\n",
                   rx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    if ( hdl_other != skiq_rx_hdl_end )
    {
        /* update RFE (other hdl) prior to re-tuning based on the passed in freq */
        status = rfe_update_before_rx_lo_tune(&rfe_instance_other, freq);
        if ( ( 0 != status ) && ( -ENOTSUP != status ) )
        {
            skiq_error("Failed to update RF frontend on %s before Rx LO frequency tune on card %u\n",
                       rx_hdl_cstr( rfe_instance_other.p_id->hdl ), card);
            return status;
        }
    }

    if( mode == skiq_freq_tune_mode_standard )
    {
        status = rfic_write_rx_freq(&rfic_instance, ad9371_rx_freq, &actual);
    }
    else
    {
        status = rfic_rx_hop( &rfic_instance, rf_timestamp, &actual );
    }
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to write Rx LO frequency\n");
        return status;
    }

    /* update RFE after re-tuning based on the passed in freq */
    status = rfe_update_after_rx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s after Rx LO frequency tune on card %u\n",
                   rx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    if ( hdl_other != skiq_rx_hdl_end )
    {
        /* update RFE (other hdl) after re-tuning based on the passed in freq */
        status = rfe_update_after_rx_lo_tune(&rfe_instance_other, freq);
        if ( ( 0 != status ) && ( -ENOTSUP != status ) )
        {
            skiq_error("Failed to update RF frontend on %s after Rx LO frequency tune on card %u\n",
                       rx_hdl_cstr( rfe_instance_other.p_id->hdl ), card);
            return status;
        }
    }

    tune_cache_rx_LO_freq(card, hdl, freq, (double) freq);

    if ( hdl_other != skiq_rx_hdl_end )
    {
        // Update the cached frequency for the other handle.
        tune_cache_rx_LO_freq(card, hdl_other, freq, (double) freq);

        if( X2_IS_RX_HIGHBAND(freq) )
        {
            // not using frequency mixing, disable RFFC to minimize noise
            status = rffc5071a_disable(id);
            if( 0 != status )
            {
                _skiq_log(SKIQ_LOG_ERROR, "failed to disable rffc5071a\n");
                return status;
            }
        }
    }
    else if( skiq_rx_hdl_B1 == hdl )
    {
        tune_read_tx_LO_freq(card, skiq_tx_hdl_A1, &tx_freq, &actual);
        if( X2_IS_TX_LOWBAND(tx_freq) && X2_IS_RX_LOWBAND(freq) )
        {
            // RxB1 shares the same RFFC as TxA1 on the X2. We need to update
            // the AD9371 Tx LO if transmitting low band to make sure it is
            // tuned to the correct frequency since the RFFC changed.
            status = _x2_rffc_write_tx_LO_freq(card, skiq_tx_hdl_A1, tx_freq,
                                               skiq_freq_tune_mode_standard, 0);
            if( 0 != status )
            {
                return status;
            }
        }
        else if( X2_IS_TX_HIGHBAND(tx_freq) && X2_IS_RX_HIGHBAND(freq) )
        {
            // Not using frequency mixing for Rx or Tx, disable RFFC to
            // minimize noise and spurs.
            status = rffc5071a_disable(id);
            if( 0 != status )
            {
                _skiq_log(SKIQ_LOG_ERROR, "failed to disable rffc5071a\n");
                return status;
            }
        }
        // else if Rx is low band and Tx is high band, nothing to do
        // else if Rx is high band and Tx is low band, nothing to do
    }

    return status;
}

/*****************************************************************************/
/** @brief Sets the intermediate frequency to tune the RFFC5071a for mixing.

    @param card         requested Sidekiq card ID
    @param freq         frequency in hertz of intermediate frequency
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _x2_rffc_write_mix_freq(uint8_t card,
                                       uint64_t freq)
{
    _mix_freq[card] = freq;
    tune_cache_mix_freq(card, freq);

    return 0;
}

/*****************************************************************************/
/** @brief Provide a notification that the Tx sample rate and bandwidth have
    changed in the event that they impact LO tuning. For X2, this is just a
    check to make sure that the sample rate and/or bandwidth doesn't cross
    into negative frequencies.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _notify_rx_sample_rate_and_bandwidth(uint8_t card,
                                                    skiq_rx_hdl_t hdl,
                                                    uint32_t rate,
                                                    uint32_t bw)
{
#ifdef ATE_SUPPORT
    (void) card;
    (void) hdl;
    (void) rate;
    (void) bw;
#else
    uint64_t rx_freq = 0;
    double actual = 0.0;

    tune_read_rx_LO_freq(card, hdl, &rx_freq, &actual);

    if( ((rate / 2) > rx_freq) && (rx_freq != 0) )
    {
        // we don't allow collection of I/Q samples at negative frequencies
        return -1;
    }
#endif
    return 0;
}

/*****************************************************************************/
/** @brief Provide a notification that the Tx sample rate and bandwidth have
    changed in the event that they impact LO tuning. For X2, this is just a
    check to make sure that the sample rate and/or bandwidth doesn't cross
    into negative frequencies.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _notify_tx_sample_rate_and_bandwidth(uint8_t card,
                                                    skiq_tx_hdl_t hdl,
                                                    uint32_t rate,
                                                    uint32_t bw)
{
#ifdef ATE_SUPPORT
    (void) card;
    (void) hdl;
    (void) rate;
    (void) bw;
#else
    uint64_t tx_freq = 0;
    double actual = 0.0;

    tune_read_tx_LO_freq(card, hdl, &tx_freq, &actual);

    if( (((rate / 2) > tx_freq) || ((bw / 2) > tx_freq)) && (tx_freq != 0) )
    {
        // we don't allow collection of I/Q samples at negative frequencies
        return -1;
    }

#endif
    return 0;
}

/***** GLOBAL DATA *****/

/** @brief Functions used for Sidekiq X2 LO tuning, using the AD9371 for
    highband and the AD9371 mixed with the RFFC5071a for lowband. */
tune_functions_t tune_x2_rffc5071_ext = {
    .init = _x2_rffc_init,
    .exit = _x2_rffc_exit,
    .read_rx_LO_freq_range = _read_rx_LO_freq_range,
    .read_tx_LO_freq_range = _read_tx_LO_freq_range,
    .write_rx_LO_freq = _x2_rffc_write_rx_LO_freq,
    .write_tx_LO_freq = _x2_rffc_write_tx_LO_freq,
    .write_mix_freq = _x2_rffc_write_mix_freq,
    .notify_rx_sample_rate_and_bandwidth = _notify_rx_sample_rate_and_bandwidth,
    .notify_tx_sample_rate_and_bandwidth = _notify_tx_sample_rate_and_bandwidth,
};
