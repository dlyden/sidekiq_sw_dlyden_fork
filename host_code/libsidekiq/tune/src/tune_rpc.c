/*****************************************************************************/
/** @file tune_rpc.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

/***** INCLUDES *****/
#include "tune.h"
#include "tune_private.h"
#include "tune_rpc.h"
#include "sidekiq_rpc_client_api.h"
#include "card_services.h"

/***** LOCAL FUNCTIONS *****/
static int32_t _read_rx_LO_freq_range(uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min);

static int32_t _read_tx_LO_freq_range(uint8_t card,
                                   skiq_tx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min);

static int32_t _write_rx_LO_freq(uint8_t card,
                                 skiq_rx_hdl_t hdl,
                                 uint64_t freq,
                                 skiq_freq_tune_mode_t mode,
                                 uint64_t rf_timestamp);

static int32_t _write_tx_LO_freq(uint8_t card,
                                 skiq_tx_hdl_t hdl,
                                 uint64_t freq,
                                 skiq_freq_tune_mode_t mode,
                                 uint64_t rf_timestamp);

static int32_t _read_rx_LO_freq_range(uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min)
{
    int32_t status = -1;
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfe_capabilities);
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfic_capabilities);
    rfic_t rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    rfe_t rfe_instance   = _skiq_get_rx_rfe_instance(card, hdl);

    // If the rf_capabilities call return !0, the functionality is not supported.
    // This would be a critical error.
    status = rfe_read_rf_capabilities(&rfe_instance, &rfe_capabilities);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to get RFE rf capabilities\n");
        return status;
    }   

    status = rfic_read_rf_capabilities(&rfic_instance, &rfic_capabilities);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to get RFIC rf capabilities\n");
        return status;
    }   

    if( status == 0 )
    {
        // The RFE and RFIC layers each return different LO frequency ranges.
        // A safe range is the overlap of the ranges.
        *p_max = MIN(rfe_capabilities.maxRxFreqHz,rfic_capabilities.maxRxFreqHz);
        *p_min = MAX(rfe_capabilities.minRxFreqHz,rfic_capabilities.minRxFreqHz);
    }

    return(status);
}

static int32_t _read_tx_LO_freq_range(uint8_t card,
                                   skiq_tx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min)
{
    int32_t status = -1;
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfe_capabilities);
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfic_capabilities);
    rfic_t rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    rfe_t rfe_instance   = _skiq_get_rx_rfe_instance(card, hdl);

    // If the rf_capabilities call return !0, the functionality is not supported.
    // This would be a critical error.
    status = rfe_read_rf_capabilities(&rfe_instance, &rfe_capabilities);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to get RFE rf capabilities\n");
        return status;
    }   

    status = rfic_read_rf_capabilities(&rfic_instance, &rfic_capabilities);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to get RFIC rf capabilities\n");
        return status;
    }   

    if( status == 0 )
    {
        // The RFE and RFIC layers each return different LO frequency ranges.
        // A safe range is the overlap of the ranges.
        *p_max = MIN(rfe_capabilities.maxTxFreqHz,rfic_capabilities.maxTxFreqHz);
        *p_min = MAX(rfe_capabilities.minTxFreqHz,rfic_capabilities.minTxFreqHz);
    }

    return(status);
}


int32_t _write_rx_LO_freq(uint8_t card,
                          skiq_rx_hdl_t hdl,
                          uint64_t freq,
                          skiq_freq_tune_mode_t mode,
                          uint64_t rf_timestamp)
{
    int32_t status=0;
    rfic_t rfic_instance;
    skiq_rx_hdl_t hdl_other;
    skiq_chan_mode_t chan_mode;
    double actual = 0.0;

    status = skiq_read_chan_mode(card, &chan_mode);
    rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    hdl_other = rfic_rx_hdl_map_other(&rfic_instance);

    if( status == 0 )
    {
        // do RPC
        status = rpc_client_tune_write_rx_LO_freq( card,
                                                   hdl,
                                                   freq,
                                                   mode,
                                                   rf_timestamp );
        // update cached freq
        if( status == 0 )
        {
            tune_cache_rx_LO_freq(card, hdl, freq, actual);
            if( (chan_mode == skiq_chan_mode_dual) && (hdl_other != skiq_rx_hdl_end) )
            {
                tune_cache_rx_LO_freq(card, hdl_other, freq, actual);
            }
        }
    }

    return (status);
}

int32_t _write_tx_LO_freq(uint8_t card,
                                 skiq_tx_hdl_t hdl,
                                 uint64_t freq,
                                 skiq_freq_tune_mode_t mode,
                                 uint64_t rf_timestamp)
{
    int32_t status=0;
    skiq_tx_hdl_t hdl_other;
    skiq_chan_mode_t chan_mode;
    double actual = 0.0;
    rfic_t rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    status = skiq_read_chan_mode(card, &chan_mode);
    hdl_other = rfic_tx_hdl_map_other(&rfic_instance);

    if( status == 0 )
    {
        // do RPC
        status = rpc_client_tune_write_tx_LO_freq( card,
                                                   hdl,
                                                   freq,
                                                   mode,
                                                   rf_timestamp );
        // update cache
        if( status == 0 )
        {
            tune_cache_tx_LO_freq(card, hdl, freq, actual);
            if( (chan_mode == skiq_chan_mode_dual) && (hdl_other != skiq_tx_hdl_end) )
            {
                tune_cache_tx_LO_freq(card, hdl_other, freq, actual);
            }
        }
    }
    
    return (status);
}

/***** GLOBAL DATA *****/

/** @brief Functions used for a card that had no additional frequency 
    extensions used for LO tuning. */
tune_functions_t tune_rpc = {
    .init = NULL,
    .exit = NULL,
    .read_rx_LO_freq_range = _read_rx_LO_freq_range,
    .read_tx_LO_freq_range = _read_tx_LO_freq_range,
    .write_rx_LO_freq = _write_rx_LO_freq,
    .write_tx_LO_freq = _write_tx_LO_freq,
    .write_mix_freq = NULL,
    .notify_rx_sample_rate_and_bandwidth = NULL,
    .notify_tx_sample_rate_and_bandwidth = NULL,
};
