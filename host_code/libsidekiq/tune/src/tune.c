/*****************************************************************************/
/** @file tune.c
 
 <pre>
 Copyright 2017-2021 Epiq Solutions, All Rights Reserved
 </pre>
*/

/***** INCLUDES *****/

#include "tune.h"
#include "tune_private.h"
#include "tune_no_ext.h"
#include "tune_mpcie_dkiq_ext.h"
#include "tune_rpc.h"
#include "tune_x2_rffc5071a_ext.h"
#include "dropkiq_api.h"
#include "sidekiq_rpc_client_api.h"

/***** DEFINES *****/

/** @brief Initializer for the tune_LO_params_t struct. */
#define TUNE_LO_PARAMS_INITIALIZER  \
    {                               \
        .freq = 0,                  \
        .actual_freq = 0.0,         \
        .cached = false,            \
    }

/** @brief Initializer for the tune_t struct. */
#define TUNE_INITIALIZER                                \
    {                                                   \
        .funcs = TUNE_FUNCTIONS_INITIALIZER,            \
        .rx_LO_params = {TUNE_LO_PARAMS_INITIALIZER},   \
        .tx_LO_params = {TUNE_LO_PARAMS_INITIALIZER},   \
        .mix_freq = 0,                                  \
        .has_mix_freq = false,                          \
    }

/***** TYPEDEFS *****/

/** @brief Holds cached parameters for a given LO handle. */
typedef struct
{
    double actual_freq;
    uint64_t freq;
    bool cached;

} tune_LO_params_t;

/** @brief Holds cached parameters and functions for a given card's tuning 
    configuration. */
typedef struct
{
    tune_functions_t funcs;
    tune_LO_params_t rx_LO_params[skiq_rx_hdl_end];
    tune_LO_params_t tx_LO_params[skiq_tx_hdl_end];
    bool has_mix_freq;
    uint64_t mix_freq;
} tune_t;

/***** LOCAL VARIABLES *****/

/** @brief Function pointers for a given card's LO tuning method. */
static tune_t _tune[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = TUNE_INITIALIZER,
};

/***** GLOBAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Selects the appropriate LO tuning functions for a given card.
    
    @param card         requested Sidekiq card ID
    @return void
*/
void tune_select_functions_for_card(uint8_t card)
{
    skiq_part_t part = _skiq_get_part(card);
    bool dkiq_present = false;

    // RPC should always be used if avail
    if( sidekiq_rpc_is_client_avail(card) )
    {
        _tune[card].funcs = tune_rpc;
    }
    else
    {
        switch( part )
        {
            case skiq_mpcie:
                // check to see if a Dropkiq has been mated to the given card
                dkiq_probe(card, &dkiq_present);
                if( dkiq_present )
                {
                    _tune[card].has_mix_freq = true;
                    _tune[card].funcs = tune_dkiq_freq_ext;
                }
                else
                {
                    _tune[card].funcs = tune_no_freq_ext;
                }
                
                break;
                
            case skiq_m2:
            case skiq_z2:
            case skiq_x4:
            case skiq_m2_2280:
            case skiq_z2p:
            case skiq_z3u:
            case skiq_nv100:
                _tune[card].funcs = tune_no_freq_ext;
                break;
                
            case skiq_x2:
                _tune[card].has_mix_freq = true;
                _tune[card].funcs = tune_x2_rffc5071_ext;
                break;
                
            case skiq_part_invalid:
                break;
        }
    }
}

/*****************************************************************************/
/** @brief Initializes all resources used for tuning the LO for a given card.
    
    @param card         requested Sidekiq card ID
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_card_init(uint8_t card)
{
    // some configurations don't need to init anything
    int32_t status = 0;
    
    if( NULL != _tune[card].funcs.init )
    {
        status = _tune[card].funcs.init(card);
    }
    
    return status;
}

/*****************************************************************************/
/** @brief Cleans up all resources used for tuning the LO for a given card.
    
    @param card         requested Sidekiq card ID
    @return void
*/
void tune_card_exit(uint8_t card)
{
    if( NULL != _tune[card].funcs.exit )
    {
        _tune[card].funcs.exit(card);
    }
    
    // reset to default
    _tune[card] = (tune_t) TUNE_INITIALIZER;
}

/*****************************************************************************/
/** @brief Tune the Rx LO to a given frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_write_rx_LO_freq(uint8_t card,
                              skiq_rx_hdl_t hdl,
                              uint64_t freq,
                              skiq_freq_tune_mode_t mode,
                              uint64_t rf_timestamp)
{
    int32_t status = -ENOTSUP;
    
    if( NULL != _tune[card].funcs.write_rx_LO_freq )
    {
        status = _tune[card].funcs.write_rx_LO_freq(card, hdl, freq, mode, rf_timestamp);
    }
    
    return status;
}


/*****************************************************************************/
/** @brief Tune the Tx LO to a given frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_write_tx_LO_freq(uint8_t card,
                              skiq_tx_hdl_t hdl,
                              uint64_t freq,
                              skiq_freq_tune_mode_t mode,
                              uint64_t rf_timestamp)
{
    int32_t status = -ENOTSUP;
    
    if( NULL != _tune[card].funcs.write_tx_LO_freq )
    {
        status = _tune[card].funcs.write_tx_LO_freq(card, hdl, freq, mode, rf_timestamp);
    }
    
    return status;
}

/*****************************************************************************/
/** @brief Set the intermediate frequency used for mixing.
    
    @param card         requested Sidekiq card ID
    @param freq         frequency in hertz of intermediate frequency
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_write_mix_freq(uint8_t card,
                            uint64_t freq)
{
    int32_t status = -ENOTSUP;
    
    if( NULL != _tune[card].funcs.write_mix_freq )
    {
        status = _tune[card].funcs.write_mix_freq(card, freq);
    }
    
    return status;
}

/*****************************************************************************/
/** @brief Provide a notification that the Rx sample rate and bandwidth have
    changed in the event that they impact LO tuning.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_notify_rx_sample_rate_and_bandwidth(uint8_t card,
                                                 skiq_rx_hdl_t hdl,
                                                 uint32_t rate,
                                                 uint32_t bw)
{
    // some configurations don't need to be notified
    int32_t status = 0;
    
    if( NULL != _tune[card].funcs.notify_rx_sample_rate_and_bandwidth )
    {
        status = _tune[card].funcs.notify_rx_sample_rate_and_bandwidth(card, 
                                                                       hdl,
                                                                       rate,
                                                                       bw);
    }
    
    return status;
}

/*****************************************************************************/
/** @brief Provide a notification that the Tx sample rate and bandwidth have
    changed in the event that they impact LO tuning.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_notify_tx_sample_rate_and_bandwidth(uint8_t card,
                                                 skiq_tx_hdl_t hdl,
                                                 uint32_t rate,
                                                 uint32_t bw)
{
    // some configurations don't need to be notified
    int32_t status = 0;
    
    if( NULL != _tune[card].funcs.notify_tx_sample_rate_and_bandwidth )
    {
        status = _tune[card].funcs.notify_tx_sample_rate_and_bandwidth(card, 
                                                                       hdl,
                                                                       rate,
                                                                       bw);
    }
    
    return status;
}

/*****************************************************************************/
/** @brief Obtain the Rx LO's current frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with current frequency in hertz
    @param p_actual     pointer to be updated with actual frequency in hertz
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_read_rx_LO_freq(uint8_t card,
                             skiq_rx_hdl_t hdl,
                             uint64_t* p_freq,
                             double* p_actual)
{
    int32_t status = -ENODATA;
    
    if( _tune[card].rx_LO_params[hdl].cached == true )
    {
        status = 0;
        *p_freq = _tune[card].rx_LO_params[hdl].freq;
        *p_actual = _tune[card].rx_LO_params[hdl].actual_freq;
    }

    return (status);
}

/*****************************************************************************/
/** @brief Obtain the Tx LO's current frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with current frequency in hertz
    @param p_actual     pointer to be updated with actual frequency in hertz
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t tune_read_tx_LO_freq(uint8_t card,
                             skiq_tx_hdl_t hdl,
                             uint64_t* p_freq,
                             double* p_actual)
{
    int32_t status = -ENODATA;
    
    if( _tune[card].tx_LO_params[hdl].cached == true )
    {
        status=0;
        *p_freq = _tune[card].tx_LO_params[hdl].freq;
        *p_actual = _tune[card].tx_LO_params[hdl].actual_freq;
    }

    return (status);
}

/*****************************************************************************/
/** @brief Get the intermediate frequency used for mixing.
    
    @param card         requested Sidekiq card ID
    @param p_freq       pointer to be updated with mix frequency in hertz
    @return void
*/
void tune_read_mix_freq(uint8_t card,
                        uint64_t* p_freq)
{
    *p_freq = _tune[card].mix_freq;
}

/*****************************************************************************/
/** @brief Obtains the maximum Rx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
void tune_read_rx_LO_max_freq(uint8_t card,
                              skiq_rx_hdl_t hdl,
                              uint64_t* p_freq)
{
    uint64_t tmp;

    if( NULL != _tune[card].funcs.read_rx_LO_freq_range )
    {
        _tune[card].funcs.read_rx_LO_freq_range(card, hdl, p_freq, &tmp);
    }
    else
    {
        *p_freq = 0;
    }
}

/*****************************************************************************/
/** @brief Obtains the minimum Rx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
void tune_read_rx_LO_min_freq(uint8_t card,
                              skiq_rx_hdl_t hdl,
                              uint64_t* p_freq)
{
    uint64_t tmp;

    if( NULL != _tune[card].funcs.read_rx_LO_freq_range )
    {
        _tune[card].funcs.read_rx_LO_freq_range(card, hdl, &tmp, p_freq);
    }
    else
    {
        *p_freq = 0;
    }
}

/*****************************************************************************/
/** @brief Obtains the maximum Tx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
void tune_read_tx_LO_max_freq(uint8_t card,
                              skiq_tx_hdl_t hdl,
                              uint64_t* p_freq)
{
    uint64_t tmp;

    if( NULL != _tune[card].funcs.read_tx_LO_freq_range )
    {
        _tune[card].funcs.read_tx_LO_freq_range(card, hdl, p_freq, &tmp);
    }
    else
    {
        *p_freq = 0;
    }
}

/*****************************************************************************/
/** @brief Obtains the minimum Tx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
void tune_read_tx_LO_min_freq(uint8_t card,
                              skiq_tx_hdl_t hdl,
                              uint64_t* p_freq)
{
    uint64_t tmp;

    if( NULL != _tune[card].funcs.read_tx_LO_freq_range )
    {
        _tune[card].funcs.read_tx_LO_freq_range(card, hdl, &tmp, p_freq);
    }
    else
    {
        *p_freq = 0;
    }
}

/*****************************************************************************/
/** @brief Stores the current Rx tuning values for a given card and handle to
    be used when tune_read_rx_LO_freq is called to prevent the need to
    backwards calculate the values.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         current frequency in hertz
    @param actual       actual frequency in hertz
    @return void
*/
void tune_cache_rx_LO_freq(uint8_t card,
                           skiq_rx_hdl_t hdl,
                           uint64_t freq,
                           double actual_freq)
{
    _tune[card].rx_LO_params[hdl].freq = freq;
    _tune[card].rx_LO_params[hdl].actual_freq = actual_freq;
    _tune[card].rx_LO_params[hdl].cached = true;
}

/*****************************************************************************/
/** @brief Stores the current Tx tuning values for a given card and handle to
    be used when tune_read_rx_LO_freq is called to prevent the need to
    backwards calculate the values.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         current frequency in hertz
    @param actual       actual frequency in hertz
    @return void
*/
void tune_cache_tx_LO_freq(uint8_t card,
                           skiq_tx_hdl_t hdl,
                           uint64_t freq,
                           double actual_freq)
{
    _tune[card].tx_LO_params[hdl].freq = freq;
    _tune[card].tx_LO_params[hdl].actual_freq = actual_freq;
    _tune[card].tx_LO_params[hdl].cached = true;
}

/*****************************************************************************/
/** @brief Stores the current tuning value used for frequency mixing.
    
    @param card         requested Sidekiq card ID
    @param freq         current frequency in hertz
    @return void
*/
void tune_cache_mix_freq(uint8_t card,
                         uint64_t freq)
{
    if( _tune[card].has_mix_freq )
    {
        _tune[card].mix_freq = freq;
    }
}
