/*****************************************************************************/
/** @file tune_no_ext.c
 
 <pre>
 Copyright 2021 Epiq Solutions, All Rights Reserved
 </pre>
*/

/***** INCLUDES *****/
#include <inttypes.h>  // For PRIu8
#include "tune.h"
#include "tune_private.h"

#include "card_services.h"

/***** LOCAL FUNCTIONS *****/

static int32_t _read_rx_LO_freq_range(uint8_t card,
                                      skiq_rx_hdl_t hdl,
                                      uint64_t* p_max,
                                      uint64_t* p_min)
{
    int32_t status = -1;
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfe_capabilities);
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfic_capabilities);
    rfic_t rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    rfe_t rfe_instance   = _skiq_get_rx_rfe_instance(card, hdl);

    // If the rf_capabilities call return !0, the functionality is not supported.
    // This would be a critical error.
    status = rfe_read_rf_capabilities(&rfe_instance, &rfe_capabilities);
    if( 0 != status )
    {
        skiq_error( "failed to get RFE rf capabilities on card %" PRIu8 "\n", card);
        return status;
    }   

    status = rfic_read_rf_capabilities(&rfic_instance, &rfic_capabilities);
    if( 0 != status )
    {
        skiq_error( "failed to get RFIC rf capabilities on card %" PRIu8 "\n", card);
        return status;
    }   

    if( status == 0 )
    {
        // The RFE and RFIC layers each return different LO frequency ranges.
        // A safe range is the overlap of the ranges.
        *p_max = MIN(rfe_capabilities.maxRxFreqHz,rfic_capabilities.maxRxFreqHz);
        *p_min = MAX(rfe_capabilities.minRxFreqHz,rfic_capabilities.minRxFreqHz);
    }

    return(status);
}

static int32_t _read_tx_LO_freq_range(uint8_t card,
                                      skiq_tx_hdl_t hdl,
                                      uint64_t* p_max,
                                      uint64_t* p_min)
{
    int32_t status = -1;
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfe_capabilities);
    SKIQ_RF_CAPABILITIES_INITIALIZER(rfic_capabilities);
    rfic_t rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    rfe_t rfe_instance   = _skiq_get_tx_rfe_instance(card, hdl);

    // If the rf_capabilities call return !0, the functionality is not supported.
    // This would be a critical error.
    status = rfe_read_rf_capabilities(&rfe_instance, &rfe_capabilities);
    if( 0 != status )
    {
        skiq_error( "failed to get RFE rf capabilities on card %" PRIu8 "\n", card);
        return status;
    }   

    status = rfic_read_rf_capabilities(&rfic_instance, &rfic_capabilities);
    if( 0 != status )
    {
        skiq_error( "failed to get RFIC rf capabilities on card %" PRIu8 "\n", card);
        return status;
    }   

    if( status == 0 )
    {
        // The RFE and RFIC layers each return different LO frequency ranges.
        // A safe range is the overlap of the ranges.
        *p_max = MIN(rfe_capabilities.maxTxFreqHz,rfic_capabilities.maxTxFreqHz);
        *p_min = MAX(rfe_capabilities.minTxFreqHz,rfic_capabilities.minTxFreqHz);
    }

    return(status);
}

/*****************************************************************************/
/** @brief Default routine for Rx LO tuning.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _write_rx_LO_freq(uint8_t card,
                                 skiq_rx_hdl_t hdl,
                                 uint64_t freq,
                                 skiq_freq_tune_mode_t mode,
                                 uint64_t rf_timestamp)
{
    int32_t status = -1;
    double actual = 0.0;
    skiq_chan_mode_t chan_mode;
    skiq_rx_hdl_t hdl_other = skiq_rx_hdl_end;
    rfe_t rfe_instance, other_rfe_instance;
    rfic_t rfic_instance;
    uint64_t min_freq = 0ULL;
    uint64_t max_freq = 0ULL;

    status = skiq_read_chan_mode(card, &chan_mode);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read channel mode");
        return status;
    }

    rfe_instance = _skiq_get_rx_rfe_instance(card, hdl);
    rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    hdl_other = rfic_rx_hdl_map_other(&rfic_instance);

    status = _read_rx_LO_freq_range(card, hdl, &max_freq, &min_freq);
    if(0 != status)
    {
        return status;
    }

    VALIDATE_FREQ(freq, max_freq, min_freq);

    /* update RFE prior to re-tuning based on the passed in freq */
    if( mode != skiq_freq_tune_mode_hop_on_timestamp )
    {
        status = rfe_update_before_rx_lo_tune(&rfe_instance, freq);
        if ( ( 0 != status ) && ( -ENOTSUP != status ) )
        {
            skiq_error("Failed to update RF frontend on %s before Rx LO frequency tune on card %u\n",
                       rx_hdl_cstr( rfe_instance.p_id->hdl ), card);
            return status;
        }

        if( (chan_mode == skiq_chan_mode_dual) && (hdl_other != skiq_rx_hdl_end) )
        {
            other_rfe_instance = _skiq_get_rx_rfe_instance(card, hdl_other);
            status = rfe_update_before_rx_lo_tune(&other_rfe_instance, freq);
            if ( ( 0 != status ) && ( -ENOTSUP != status ) )
            {
                skiq_error("Failed to update RF frontend on %s before Rx LO frequency tune on card %u\n",
                        rx_hdl_cstr( other_rfe_instance.p_id->hdl ), card);
                return status;
            }
        }
    }

    /* configure the LO for the requested frequency */
    if( mode == skiq_freq_tune_mode_standard )
    {
        status = rfic_write_rx_freq(&rfic_instance, freq, &actual);
    }
    else
    {
        status = rfic_rx_hop( &rfic_instance, rf_timestamp, &actual );
    }
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to write Rx LO frequency (tune_mode=%u)\n", mode);
        if (status == -EPERM)
        {
            skiq_error("Unable to write rx_freq for %s on card %" PRIu8 ".  This operation " \
                       "must occur after calling skiq_write_rx_sample_rate_and_bandwidth() " \
                       "on the requested handle\n" , rx_hdl_cstr( hdl ), card);
        }
        return status;
    }
    tune_cache_rx_LO_freq(card, hdl, freq, actual);

    if( mode != skiq_freq_tune_mode_hop_on_timestamp )
    {
        /* update RFE after re-tuning based on the passed in freq */
        status = rfe_update_after_rx_lo_tune(&rfe_instance, freq);
        if ( ( 0 != status ) && ( -ENOTSUP != status ) )
        {
            skiq_error("Failed to update RF frontend on %s after Rx LO frequency tune on card %u\n",
                       rx_hdl_cstr( rfe_instance.p_id->hdl ), card);
            return status;
        }
    }

    // TODO: we may need different table when operating in freq hopping mode
    status = card_update_iq_complex_multiplier( card, hdl, freq );
    if ( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to update I/Q complex multiplier on card %u and hdl %u", card, hdl);
        return status;
    }
    
    if( (chan_mode == skiq_chan_mode_dual) && (hdl_other != skiq_rx_hdl_end) )
    {
        if( mode != skiq_freq_tune_mode_hop_on_timestamp )
        {
            /* update RFE after re-tuning based on the passed in freq */
            status = rfe_update_after_rx_lo_tune(&other_rfe_instance, freq);
            if ( ( 0 != status ) && ( -ENOTSUP != status ) )
            {
                skiq_error("Failed to update RF frontend on %s after Rx LO frequency tune on card "
                           "%u\n", rx_hdl_cstr( other_rfe_instance.p_id->hdl ), card);
                return status;
            }
        }

        status = card_update_iq_complex_multiplier( card, hdl_other, freq );
        if ( 0 != status )
        {
            _skiq_log(SKIQ_LOG_ERROR, "Unable to update I/Q complex multiplier on card %u and hdl %u", card, hdl_other);
            return status;
        }

        tune_cache_rx_LO_freq(card, hdl_other, freq, actual);
    }
    
    return 0;
}

/*****************************************************************************/
/** @brief Default routine for Tx LO tuning.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _write_tx_LO_freq(uint8_t card,
                                 skiq_tx_hdl_t hdl,
                                 uint64_t freq,
                                 skiq_freq_tune_mode_t mode,
                                 uint64_t rf_timestamp)
{
    int32_t status = -1;
    double actual = 0.0;
    skiq_chan_mode_t chan_mode;
    skiq_tx_hdl_t hdl_other;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    uint64_t min_freq = 0ULL;
    uint64_t max_freq = 0ULL;

    status = skiq_read_chan_mode(card, &chan_mode);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read channel mode");
        return status;
    }
    
    rfe_instance = _skiq_get_tx_rfe_instance(card, hdl);
    rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    hdl_other = rfic_tx_hdl_map_other(&rfic_instance);

    status = _read_tx_LO_freq_range(card, hdl, &max_freq, &min_freq);
    if(0 != status)
    {
        return status;
    }

    VALIDATE_FREQ(freq, max_freq, min_freq);

    /* update pre-select filter based on the passed in freq */
    if( mode != skiq_freq_tune_mode_hop_on_timestamp )
    {
        /* update RFE prior to re-tuning based on the passed in freq */
        status = rfe_update_before_tx_lo_tune(&rfe_instance, freq);
        if ( ( 0 != status ) && ( -ENOTSUP != status ) )
        {
            skiq_error("Failed to update RF frontend on %s before Tx LO frequency tune on card "
                       "%u\n", tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
            return status;
        }
    }

    /* configure the LO for the requested frequency */
    if( mode == skiq_freq_tune_mode_standard )
    {
        status = rfic_write_tx_freq(&rfic_instance, freq, &actual);
    }
    else
    {
        status = rfic_tx_hop(&rfic_instance, rf_timestamp, &actual);
    }
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to set Tx LO frequency (tune_mode=%u)\n", mode);
        return status;
    }

    tune_cache_tx_LO_freq(card, hdl, freq, actual);

    /* update RFE after re-tuning based on the passed in freq */
    status = rfe_update_after_tx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s after Tx LO frequency tune on card %u\n",
                   tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }
    
    /* only update the other path if we're in dual channel mode */
    if( (chan_mode == skiq_chan_mode_dual) && (hdl_other != skiq_tx_hdl_end) )
    {
        rfe_instance = _skiq_get_tx_rfe_instance(card, hdl_other);
        if( mode != skiq_freq_tune_mode_hop_on_timestamp )
        {
            /* update RFE prior to re-tuning based on the passed in freq */
            status = rfe_update_before_tx_lo_tune(&rfe_instance, freq);
            if ( ( 0 != status ) && ( -ENOTSUP != status ) )
            {
                skiq_error("Failed to update RF frontend on %s before Tx LO frequency tune on card "
                           "%u\n", tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
                return status;
            }
        }
        tune_cache_tx_LO_freq(card, hdl_other, freq, actual);

        /* update RFE after re-tuning based on the passed in freq */
        status = rfe_update_after_tx_lo_tune(&rfe_instance, freq);
        if ( ( 0 != status ) && ( -ENOTSUP != status ) )
        {
            skiq_error("Failed to update RF frontend on %s after Tx LO frequency tune on card %u\n",
                       tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
            return status;
        }
    }

    return 0;
}

/***** GLOBAL DATA *****/

/** @brief Functions used for a card that had no additional frequency 
    extensions used for LO tuning. */
tune_functions_t tune_no_freq_ext = {
    .init = NULL,
    .exit = NULL,
    .read_rx_LO_freq_range = _read_rx_LO_freq_range,
    .read_tx_LO_freq_range = _read_tx_LO_freq_range,
    .write_rx_LO_freq = _write_rx_LO_freq,
    .write_tx_LO_freq = _write_tx_LO_freq,
    .write_mix_freq = NULL,
    .notify_rx_sample_rate_and_bandwidth = NULL,
    .notify_tx_sample_rate_and_bandwidth = NULL,
};
