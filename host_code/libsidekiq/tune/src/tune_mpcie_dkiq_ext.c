/*****************************************************************************/
/** @file tune_mpcie_dkiq_ext.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

/***** INCLUDES *****/

#include "tune.h"
#include "tune_private.h"
#include "dropkiq_api.h"

/***** LOCAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Initialization routine for card's using Dropkiq.
    
    @param card         requested Sidekiq card ID
    @return void
*/
static int32_t _dkiq_init(uint8_t card)
{
    skiq_ref_clock_select_t ref_clock = skiq_ref_clock_invalid;
    uint8_t maj = 0, min = 0, patch = 0;
    int32_t status = 0;
    
    status = skiq_read_ref_clock_select(card, &ref_clock);
    if( 0 != status )
    {
        return status;
    }
    
    // external ref clock required for DKIQ
    if( ref_clock != skiq_ref_clock_external )
    {
        _skiq_log(SKIQ_LOG_ERROR, "not configured for external clock\n");
        return status;
    }

    // Initialize Dropkiq early on since it provides the reference 
    // clock to the AD9361.
    if( 0 != dkiq_get_lib_version(&maj, &min, &patch) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read Dropkiq library version\n");
        return status;
    }
    
    _skiq_log(SKIQ_LOG_INFO, "libdropkiq v%u.%u.%u\n", maj, min, patch);
    
    if( 0 != dkiq_init(card) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to initialize Dropkiq\n");
        return status;
    }
    
    return 0;
}

/*****************************************************************************/
/** @brief Exit routine for cards using Dropkiq.
    
    @param card         requested Sidekiq card ID
    @return void
*/
static void _dkiq_exit(uint8_t card)
{
    dkiq_free(card);
}

static int32_t _read_rx_LO_freq_range(uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min)
{
    if( skiq_rx_hdl_A1 == hdl )
    {
        dkiq_get_max_min_rx_lo_freq(card, p_max, p_min);
    }
    else
    {
        *p_max = 0;
        *p_min = 0;
    }

    return(0);
}

static int32_t _read_tx_LO_freq_range(uint8_t card,
                                   skiq_tx_hdl_t hdl,
                                   uint64_t* p_max,
                                   uint64_t* p_min)
{
    if( skiq_tx_hdl_A1 == hdl )
    {
        dkiq_get_max_min_tx_lo_freq(card, p_max, p_min);
    }
    else
    {
        *p_max = 0;
        *p_min = 0;
    }

    return(0);
}

/*****************************************************************************/
/** @brief Routine for confguring Rx (AD9361 and RFE) on MPCIE.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _write_rx_LO_freq(uint8_t card,
                                 skiq_rx_hdl_t hdl,
                                 uint64_t freq,
                                 bool invert)
{
    rfic_t rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    rfe_t rfe_instance = _skiq_get_rx_rfe_instance(card, hdl);
    double actual = 0.0;
    int32_t status = 0;
    
    /* update RFE prior to re-tuning based on the passed in freq */
    status = rfe_update_before_rx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s before Rx LO frequency tune on card %u\n",
                   rx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }
        
    /* configure the LO for the requested frequency */
    status = rfic_write_rx_freq(&rfic_instance, freq, &actual);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to set Rx LO frequency\n");
        return status;
    }
    
    // For certain frequency mixing configurations, Dropkiq requires the
    // spectrum to be inverted in order for information to appear at the
    // correct frequency.
    status = rfic_write_rx_spectrum_invert(&rfic_instance, invert);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to invert Rx spectrum\n");
        return status;
    }

    /* update RFE after re-tuning based on the passed in freq */
    status = rfe_update_after_rx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s after Rx LO frequency tune on card %u\n",
                   rx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    return 0;
}

/*****************************************************************************/
/** @brief Routine for confguring Tx (AD9361 and RFE) on MPCIE.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _write_tx_LO_freq(uint8_t card,
                                 skiq_tx_hdl_t hdl,
                                 uint64_t freq,
                                 bool invert)
{
    rfic_t rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    rfe_t rfe_instance = _skiq_get_tx_rfe_instance(card, hdl);
    double actual = 0.0;
    int32_t status = 0;
    
    /* update RFE prior to re-tuning based on the passed in freq */
    status = rfe_update_before_tx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s before Tx LO frequency tune on card %u\n",
                   tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    /* configure the LO for the requested frequency */
    status = rfic_write_tx_freq(&rfic_instance, freq, &actual);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to set Tx LO frequency\n");
        return status;
    }
    
    // For certain frequency mixing configurations, Dropkiq requires the
    // spectrum to be inverted in order for information to appear at the
    // correct frequency.
    status = rfic_write_tx_spectrum_invert(&rfic_instance, invert);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to invert Tx spectrum\n");
        return status;
    }

    /* update RFE after re-tuning based on the passed in freq */
    status = rfe_update_after_tx_lo_tune(&rfe_instance, freq);
    if ( ( 0 != status ) && ( -ENOTSUP != status ) )
    {
        skiq_error("Failed to update RF frontend on %s after Tx LO frequency tune on card %u\n",
                   tx_hdl_cstr( rfe_instance.p_id->hdl ), card);
        return status;
    }

    return 0;
}

/*****************************************************************************/
/** @brief Utility function for configuring the AD9361 after tuning the Dropkiq
    unit to a given frequency. This is required due to the frequency mixing
    design implemented on a combined Sidekiq and Dropkiq system.
    
    @param card             requested Sidekiq card ID
    @param rx_freq          actual Rx frequency achieved by mixing in hertz
    @param tx_freq          actual Tx frequency achieved by mixing in hertz
    @param rx_ad9361_freq   frequency to tune the AD9361 Rx LO to in hertz
    @param tx_ad9361_freq   frequency to tune the AD9361 Tx LO to in hertz
    @param rx_invert        true to invert the Rx spectrum on the AD9361
    @param tx_invert        true to invert the Tx spectrum on the AD9361
    @return void
*/
static int32_t _dkiq_write_ad9361_LO_freq(uint8_t card,
                                          uint64_t rx_freq,
                                          uint64_t tx_freq,
                                          uint64_t rx_ad9361_freq,
                                          uint64_t tx_ad9361_freq,
                                          bool rx_invert,
                                          bool tx_invert)
{
    // Dropkiq will only use these two handles.
    skiq_rx_hdl_t rx_hdl = skiq_rx_hdl_A1;
    skiq_tx_hdl_t tx_hdl = skiq_tx_hdl_A1;
    int32_t status = 0;
    
    // Update AD9361 LOs if the frequency returned is non-zero.

    if( 0 != rx_ad9361_freq )
    {
        status = _write_rx_LO_freq(card, rx_hdl, rx_ad9361_freq, rx_invert);
        if( 0 != status )
        {
            return status;
        }
        
        tune_cache_rx_LO_freq(card, rx_hdl, rx_freq, (double) rx_freq);
    }

    if( 0 != tx_ad9361_freq )
    {
        status = _write_tx_LO_freq(card, tx_hdl, tx_ad9361_freq, tx_invert);
        if( 0 != status )
        {
            return status;
        }
        
        tune_cache_tx_LO_freq(card, tx_hdl, tx_freq, (double) tx_freq);
    }
    
    return 0;
}

/*****************************************************************************/
/** @brief Routine for tuning the Rx LO for systems using a Dropkiq.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _dkiq_write_rx_LO_freq(uint8_t card,
                                      skiq_rx_hdl_t hdl,
                                      uint64_t freq,
                                      skiq_freq_tune_mode_t mode,
                                      uint64_t rf_timestamp)
{
    uint64_t tx_freq = 0;
    uint64_t rx_ad9361_freq = 0;
    uint64_t tx_ad9361_freq = 0;
    uint64_t hmc832_freq = 0;
    bool rx_invert = false;
    bool tx_invert = false;
    int32_t status = 0;

    // only standard tune mode supported with Dropkiq currently
    if( mode != skiq_freq_tune_mode_standard )
    {
        skiq_error("Only standard tune mode is supported with Dropkiq (card=%u)", card);
        status = -ENOTSUP;
        return (status);
    }

    status = dkiq_set_rx_lo_freq(card,
                                 freq,
                                 &rx_ad9361_freq,
                                 &tx_ad9361_freq,
                                 &rx_invert);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to write Dropkiq Rx LO\n");
        return status;
    }
    
    status = dkiq_get_tx_lo_freq(card,
                                 &tx_freq,
                                 &rx_ad9361_freq,
                                 &tx_ad9361_freq,
                                 &tx_invert);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read Dropkiq Tx LO\n");
        return status;
    }
    
    status = dkiq_get_hmc832_freq(card, &hmc832_freq);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read Dropkiq HMC832 LO\n");
        return status;
    }
    tune_cache_mix_freq(card, hmc832_freq);
    
    status = _dkiq_write_ad9361_LO_freq(card,
                                        freq,
                                        tx_freq,
                                        rx_ad9361_freq,
                                        tx_ad9361_freq,
                                        rx_invert,
                                        tx_invert);
    if( 0 != status )
    {
        return status;
    }
    
    return 0;
}

/*****************************************************************************/
/** @brief Routine for tuning the Tx LO for systems using a Dropkiq.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _dkiq_write_tx_LO_freq(uint8_t card,
                                      skiq_tx_hdl_t hdl,
                                      uint64_t freq,
                                      skiq_freq_tune_mode_t mode,
                                      uint64_t rf_timestamp)
{
    uint64_t rx_freq = 0;
    uint64_t rx_ad9361_freq = 0;
    uint64_t tx_ad9361_freq = 0;
    uint64_t hmc832_freq = 0;
    bool rx_invert = false;
    bool tx_invert = false;
    int32_t status = 0;

    // only standard tune mode supported with Dropkiq currently
    if( mode != skiq_freq_tune_mode_standard )
    {
        skiq_error("Only standard tune mode is supported with Dropkiq (card=%u)", card);
        status = -ENOTSUP;
        return (status);
    }

    status = dkiq_set_tx_lo_freq(card,
                                 freq,
                                 &rx_ad9361_freq,
                                 &tx_ad9361_freq,
                                 &tx_invert);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to write Dropkiq Tx LO\n");
        return status;
    }
    
    status = dkiq_get_rx_lo_freq(card,
                                 &rx_freq,
                                 &rx_ad9361_freq,
                                 &tx_ad9361_freq,
                                 &rx_invert);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read Dropkiq Tx LO\n");
        return status;
    }
    
    status = dkiq_get_hmc832_freq(card, &hmc832_freq);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read Dropkiq HMC832 LO\n");
        return status;
    }
    tune_cache_mix_freq(card, hmc832_freq);
    
    status = _dkiq_write_ad9361_LO_freq(card,
                                        rx_freq,
                                        freq,
                                        rx_ad9361_freq,
                                        tx_ad9361_freq,
                                        rx_invert,
                                        tx_invert);
    if( 0 != status )
    {
        return status;
    }
    
    return 0;
}

/*****************************************************************************/
/** @brief Provide a notification that the Rx sample rate and bandwidth have
    changed. This is important for Dropkiq since a change in the sample rate
    and bandwidth can change the intermediate frequency used for mixing
    purposes, necessitating a full retune of the LO.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _notify_rx_sample_rate_and_bandwidth(uint8_t card,
                                                    skiq_rx_hdl_t hdl,
                                                    uint32_t rate,
                                                    uint32_t bw)
{
    uint64_t lo_freq = 0;
    double actual = 0.0;
    int32_t status = 0;
    
    // Returns error if sample rate yields invalid spectral data.
    status = dkiq_set_rx_sample_rate_and_bandwidth(card, rate, bw);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR,
                  "failed to notify Dropkiq Rx sample rate and bandwidth\n");
        return status;
    }
    
    // Changing the Rx bandwidth can cause a different filter to be
    // selected on Dropkiq. Each filter has its own "tuning", so go
    // ahead and reconfigure the Rx LO again. We can probably be smarter
    // about this in the future (i.e. only retune if new filter is
    // chosen), but until then, we'll retune every time.
    
    // MREUTMAN: If the LO has not been tuned previously, lo_freq=0 and
    // will return an error. We currently will assume if it is non-zero
    // that Sidekiq has a valid LO frequency and needs to be retuned.
    // Otherwise, we will skip this step.
    tune_read_rx_LO_freq(card, hdl, &lo_freq, &actual);
    if( 0 != lo_freq )
    {
        status = tune_write_rx_LO_freq(card, hdl, lo_freq, skiq_freq_tune_mode_standard, 0);
        if( 0 != status )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "failed to retune Sidekiq and Dropkiq Rx LO\n");
            return status;
        }
    }
    
    return 0;
}

/*****************************************************************************/
/** @brief Provide a notification that the Tx sample rate and bandwidth have
    changed.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
static int32_t _notify_tx_sample_rate_and_bandwidth(uint8_t card,
                                                    skiq_tx_hdl_t hdl,
                                                    uint32_t rate,
                                                    uint32_t bw)
{
    int32_t status = 0;
    
    // We don't need to worry about retuning the Tx LO on change since there
    // are no filters being changed that can affect which intermediate
    // frequency being used by Dropkiq.
    status = dkiq_set_tx_sample_rate_and_bandwidth(card, rate, bw);
    if (0 != status)
    {
        _skiq_log(SKIQ_LOG_ERROR,
                  "failed to set Dropkiq Tx sample rate and bandwidth\n");
        return status;
    }
    
    return 0;
}

/***** GLOBAL DATA *****/

/** @brief Functions used for a card that makes use of a Dropkiq for additonal
    low frequency tuning by means of frequency mixing with the Dropkiq's own
    LO. */
tune_functions_t tune_dkiq_freq_ext = {
    .init = _dkiq_init,
    .exit = _dkiq_exit,
    .read_rx_LO_freq_range = _read_rx_LO_freq_range,
    .read_tx_LO_freq_range = _read_tx_LO_freq_range,
    .write_rx_LO_freq = _dkiq_write_rx_LO_freq,
    .write_tx_LO_freq = _dkiq_write_tx_LO_freq,
    .write_mix_freq = NULL,
    .notify_rx_sample_rate_and_bandwidth = _notify_rx_sample_rate_and_bandwidth,
    .notify_tx_sample_rate_and_bandwidth = _notify_tx_sample_rate_and_bandwidth,
};
