#ifndef __TUNE_RPC_H__
#define __TUNE_RPC_H__

/*! \file tune_rpc.h
 * \brief 
 *  
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** EXTERN VARIABLES *****/

extern tune_functions_t tune_rpc;

#endif
