#ifndef __TUNE_MPCIE_DKIQ_EXT_H__
#define __TUNE_MPCIE_DKIQ_EXT_H__

/*! \file tune_mpcie_dkiq_ext.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** EXTERN VARIABLES *****/

extern tune_functions_t tune_dkiq_freq_ext;

#endif
