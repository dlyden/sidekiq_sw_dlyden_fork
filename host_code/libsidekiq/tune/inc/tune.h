#ifndef __TUNE_H__
#define __TUNE_H__

/*! \file tune.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** INCLUDES *****/

#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include "sidekiq_api.h"
#include "sidekiq_types.h"
#include "sidekiq_private.h"

/***** EXTERN FUNCTIONS  *****/

/*****************************************************************************/
/** @brief Selects the appropriate LO tuning functions for a given card.
    
    @param card         requested Sidekiq card ID
    @return void
*/
extern void tune_select_functions_for_card(uint8_t card);

/*****************************************************************************/
/** @brief Initializes all resources used for tuning the LO for a given card.
    
    @param card         requested Sidekiq card ID
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_card_init(uint8_t card);

/*****************************************************************************/
/** @brief Cleans up all resources used for tuning the LO for a given card.
    
    @param card         requested Sidekiq card ID
    @return void
*/
extern void tune_card_exit(uint8_t card);

/*****************************************************************************/
/** @brief Tune the Rx LO to a given frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_write_rx_LO_freq(uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     uint64_t freq,
                                     skiq_freq_tune_mode_t mode,
                                     uint64_t rf_timestamp);

/*****************************************************************************/
/** @brief Tune the Tx LO to a given frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         frequency in hertz to tune to
    @param mode         tune mode
    @param rf_timestamp RF timestamp to execute tune, 0 for immediate
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_write_tx_LO_freq(uint8_t card,
                                     skiq_tx_hdl_t hdl,
                                     uint64_t freq,
                                     skiq_freq_tune_mode_t mode,
                                     uint64_t rf_timestamp);

/*****************************************************************************/
/** @brief Set the intermediate frequency used for mixing.
    
    @param card         requested Sidekiq card ID
    @param freq         frequency in hertz of intermediate frequency
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_write_mix_freq(uint8_t card,
                                   uint64_t freq);

/*****************************************************************************/
/** @brief Obtain the Rx LO's current frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with current frequency in hertz
    @param p_actual     pointer to be updated with actual frequency in hertz
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_read_rx_LO_freq(uint8_t card,
                                    skiq_rx_hdl_t hdl,
                                    uint64_t* p_freq,
                                    double* p_actual);

/*****************************************************************************/
/** @brief Obtain the Tx LO's current frequency.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with current frequency in hertz
    @param p_actual     pointer to be updated with actual frequency in hertz
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_read_tx_LO_freq(uint8_t card,
                                    skiq_tx_hdl_t hdl,
                                    uint64_t* p_freq,
                                    double* p_actual);

/*****************************************************************************/
/** @brief Get the intermediate frequency used for mixing.
    
    @param card         requested Sidekiq card ID
    @param p_freq       pointer to be updated with mix frequency in hertz
    @return int32_t     status where 0=success, anything else is an error
*/
extern void tune_read_mix_freq(uint8_t card,
                               uint64_t* p_freq);

/*****************************************************************************/
/** @brief Provide a notification that the Rx sample rate and bandwidth have
    changed in the event that they impact LO tuning.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_notify_rx_sample_rate_and_bandwidth(uint8_t card,
                                                        skiq_rx_hdl_t hdl,
                                                        uint32_t rate,
                                                        uint32_t bw);

/*****************************************************************************/
/** @brief Provide a notification that the Tx sample rate and bandwidth have
    changed in the event that they impact LO tuning.

    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param rate         new sample rate (in Hertz)
    @param bw           new bandwidth (in Hertz)
    @return int32_t     status where 0=success, anything else is an error
*/
extern int32_t tune_notify_tx_sample_rate_and_bandwidth(uint8_t card,
                                                        skiq_tx_hdl_t hdl,
                                                        uint32_t rate,
                                                        uint32_t bw);

/*****************************************************************************/
/** @brief Obtains the maximum Rx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
extern void tune_read_rx_LO_max_freq(uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     uint64_t* p_freq);

/*****************************************************************************/
/** @brief Obtains the minimum Rx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
extern void tune_read_rx_LO_min_freq(uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     uint64_t* p_freq);

/*****************************************************************************/
/** @brief Obtains the maximum Tx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
extern void tune_read_tx_LO_max_freq(uint8_t card,
                                     skiq_tx_hdl_t hdl,
                                     uint64_t* p_freq);

/*****************************************************************************/
/** @brief Obtains the minimum Tx LO frequency for a given card.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param p_freq       pointer to be updated with frequency limit
    @return void
*/
extern void tune_read_tx_LO_min_freq(uint8_t card,
                                     skiq_tx_hdl_t hdl,
                                     uint64_t* p_freq);

#endif
