#ifndef __TUNE_NO_EXT_H__
#define __TUNE_NO_EXT_H__

/*! \file tune_no_ext.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** EXTERN VARIABLES *****/

extern tune_functions_t tune_no_freq_ext;

#endif
