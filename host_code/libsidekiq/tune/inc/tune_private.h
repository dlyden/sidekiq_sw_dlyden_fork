#ifndef __TUNE_PRIVATE_H__
#define __TUNE_PRIVATE_H__

/*! \file tune_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** DEFINES *****/

/** @brief Initializer for tune_functions_t struct. */
#define TUNE_FUNCTIONS_INITIALIZER                      \
    {                                                   \
        .init = NULL,                                   \
        .exit = NULL,                                   \
        .read_rx_LO_freq_range = NULL,                  \
        .read_tx_LO_freq_range = NULL,                  \
        .write_rx_LO_freq = NULL,                       \
        .write_tx_LO_freq = NULL,                       \
        .write_mix_freq = NULL,                         \
        .notify_rx_sample_rate_and_bandwidth = NULL,    \
        .notify_tx_sample_rate_and_bandwidth = NULL,    \
    }

/** @brief Macro used to check that a given frequency is in bounds. */
#define VALIDATE_FREQ(freq, max, min)   \
    do {                                \
        if( (freq) > (max) )            \
        {                               \
            return -EINVAL;             \
        }                               \
        else if( (freq) < (min) )       \
        {                               \
            return -EINVAL;             \
        }                               \
    } while( 0 )

/***** TYPEDEFS *****/

/** @brief Function pointers related to LO tuning. */
typedef struct
{
    int32_t (*init)(uint8_t card);

    void    (*exit)(uint8_t card);

    int32_t (*read_rx_LO_freq_range)(uint8_t card,
                                  skiq_rx_hdl_t hdl,
                                  uint64_t* p_max,
                                  uint64_t* p_min);

    int32_t (*read_tx_LO_freq_range)(uint8_t card,
                                  skiq_tx_hdl_t hdl,
                                  uint64_t* p_max,
                                  uint64_t* p_min);

    int32_t (*write_rx_LO_freq)(uint8_t card,
                                skiq_rx_hdl_t hdl,
                                uint64_t freq,
                                skiq_freq_tune_mode_t mode,
                                uint64_t rf_timestamp);
    
    int32_t (*write_tx_LO_freq)(uint8_t card,
                                skiq_tx_hdl_t hdl,
                                uint64_t freq,
                                skiq_freq_tune_mode_t mode,
                                uint64_t rf_timestamp);

    int32_t (*write_mix_freq)(uint8_t card,
                              uint64_t freq);

    int32_t (*notify_rx_sample_rate_and_bandwidth)(uint8_t card,
                                                   skiq_rx_hdl_t hdl,
                                                   uint32_t rate,
                                                   uint32_t bw);

    int32_t (*notify_tx_sample_rate_and_bandwidth)(uint8_t card,
                                                   skiq_tx_hdl_t hdl,
                                                   uint32_t rate,
                                                   uint32_t bw);
} tune_functions_t;

/***** EXTERN FUNCTIONS  *****/

/*****************************************************************************/
/** @brief Stores the current Rx tuning values for a given card and handle to
    be used when tune_read_rx_LO_freq is called to prevent the need to
    backwards calculate the values.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         current frequency in hertz
    @param actual       actual frequency in hertz
    @return void
*/
extern void tune_cache_rx_LO_freq(uint8_t card,
                                  skiq_rx_hdl_t hdl,
                                  uint64_t freq,
                                  double actual_freq);

/*****************************************************************************/
/** @brief Stores the current Tx tuning values for a given card and handle to
    be used when tune_read_rx_LO_freq is called to prevent the need to
    backwards calculate the values.
    
    @param card         requested Sidekiq card ID
    @param hdl          requested RF interface
    @param freq         current frequency in hertz
    @param actual       actual frequency in hertz
    @return void
*/
extern void tune_cache_tx_LO_freq(uint8_t card,
                                  skiq_tx_hdl_t hdl,
                                  uint64_t freq,
                                  double actual_freq);

/*****************************************************************************/
/** @brief Stores the current tuning value used for frequency mixing.
    
    @param card         requested Sidekiq card ID
    @param freq         current frequency in hertz
    @return void
*/
extern void tune_cache_mix_freq(uint8_t card,
                                uint64_t freq);
#endif
