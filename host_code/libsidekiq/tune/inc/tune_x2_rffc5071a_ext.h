#ifndef __TUNE_X2_RFFC5071A_EXT_H__
#define __TUNE_X2_RFFC5071A_EXT_H__

/*! \file tune_x2_rffc5071a_ext.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** INCLUDES *****/

#include "tune_private.h"

/***** DEFINES *****/

/** @brief Minimum Rx frequency for X2 using the RFFC and AD9371. */
#define X2_RX_LOW_BAND_MIN_FREQ (1000000) // 1 Mhz

/** @brief Minimum Rx frequency for X2 using the RFFC and AD9371. */
#define X2_TX_LOW_BAND_MIN_FREQ (1000000) // 1 Mhz

/** @brief Maximum Rx frequency for X2 using the RFFC and AD9371. */
#define X2_RX_LOW_BAND_MAX_FREQ (440000000) // 440 Mhz

/** @brief Maximum Tx frequency for X2 using the RFFC and AD9371. */
#define X2_TX_LOW_BAND_MAX_FREQ (300000000) // 300 Mhz

/** @brief Minimum Rx frequency for X2 using the AD9371 only. */
#define X2_RX_HIGH_BAND_MIN_FREQ (440000000) // 440 Mhz

/** @brief Minimum Tx frequency for X2 using the AD9371 only. */
#define X2_TX_HIGH_BAND_MIN_FREQ (300000000) // 300 Mhz

/** @brief Maximum Rx frequency for X2 using the AD9371 only. */
#define X2_RX_HIGH_BAND_MAX_FREQ (6000000000) // 6 Ghz

/** @brief Maximum Tx frequency for X2 using the AD9371 only. */
#define X2_TX_HIGH_BAND_MAX_FREQ (6000000000) // 6 Ghz

/** @brief Minimum tuning frequency for X2. */
#define X2_TUNE_MIN_FREQ (X2_RX_LOW_BAND_MIN_FREQ) // 1 Mhz

/** @brief Maximum tuning frequency for X2. */
#define X2_TUNE_MAX_FREQ (X2_RX_HIGH_BAND_MAX_FREQ) // 6 Ghz

/***** EXTERN VARIABLES *****/

extern tune_functions_t tune_x2_rffc5071_ext;

#endif
