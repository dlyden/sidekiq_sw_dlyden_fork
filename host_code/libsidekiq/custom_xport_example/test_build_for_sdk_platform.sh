#! /bin/bash

#
# Simple script to test the SDK build
#

PLATFORM=${PLATFORM:-z2}

MKT=tmp/build
rm -rf tmp
mkdir -p $MKT

tar -chf - \
    Makefile.for_sdk \
    inc/ \
    src/ \
    test_apps/ \
    tools.mk \
    | tar -C $MKT -xvf -

mkdir -p $MKT/lib $MKT/inc $MKT/../lib/support $MKT/../arg_parser/lib
cp -t $MKT/inc ../../../build.${PLATFORM}/include/*.h
cp -rt $MKT/../lib/support/ ../../../build.${PLATFORM}/support
mv $MKT/../lib/support/support $MKT/../lib/support/${PLATFORM}
cp -t $MKT/inc ../../arg_parser/inc/arg_parser.h
cp -t $MKT/inc ../../../libraries/nw_logic_dma/dma_interface/dma_interface_api.h
cp -t $MKT/inc ../../../libraries/pci_manager/driver/pci_manager.h
cp -vt $MKT/lib ../../../libraries/nw_logic_dma/dma_interface/libdmainterface__${PLATFORM}.a
cp -t $MKT/../arg_parser/lib ../../arg_parser/arg_parser__${PLATFORM}.a
cp -t $MKT/../lib ../../../build.${PLATFORM}/lib/libsidekiq__${PLATFORM}.a

(cd $MKT; make -f Makefile.for_sdk PLATFORM=${PLATFORM})
