#ifndef __PCIE_TRANSPORT_PRIVATE_H__
#define __PCIE_TRANSPORT_PRIVATE_H__

/*! \file pcie_transport_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include "sidekiq_api.h"

/***** DEFINES *****/

// PCI Manager Version Requirement
#define PCI_MANAGER_MAJ_REQ (1)
#define PCI_MANAGER_MIN_REQ (2)
#define PCI_MANAGER_SUBMINOR_REQ (0)

// DMA Driver Version Requirement
#define DMA_DRIVER_MAJ_REQ (5)
#define DMA_DRIVER_MIN_REQ (0)
#define DMA_DRIVER_SUBMINOR_REQ (0)

/** @brief A simple macro for comparing driver version (major, minor, patch) */
#ifndef DRIVER_VERSION
#define DRIVER_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))
#endif

/***** TYPEDEFS *****/

typedef struct
{
    uint32_t pci_bus_num;
    uint32_t pci_devfn;
    uint32_t pci_domain;
} pci_bus_info_t;


/***** EXTERN DATA  *****/

extern pci_bus_info_t pcie_transport_pci_bus[SKIQ_MAX_NUM_CARDS];
extern skiq_xport_init_level_t pcie_transport_init_level;

/* transport functions */
extern skiq_xport_fpga_functions_t pcie_xport_fpga_funcs;
extern skiq_xport_rx_functions_t pcie_xport_rx_funcs;
extern skiq_xport_tx_functions_t pcie_xport_tx_funcs;


/***** EXTERN FUNCTIONS  *****/




#endif  /* __PCIE_TRANSPORT_PRIVATE_H__ */
