/**
 * @file   pcie_fpga_registers.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016,2017 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include "sidekiq_xport_types.h"
#include "pcie_transport_private.h"

#include <dma_interface_api.h>

#ifndef __MINGW32__
#include <pci_manager.h>
#endif /* __MINGW32__ */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef __MINGW32__
#include <sys/ioctl.h>
#endif /* __MINGW32__ */

#include <unistd.h>

int32_t _pcie_card_fpga_down( uint64_t uid )
{
    int32_t status=0;
#ifndef __MINGW32__
    int32_t fd;
    uint32_t name_len = sizeof(PCI_MANAGER_PATH)+2+sizeof(PCI_MANAGER_FILENAME);
    char pci_mgr_name[name_len];
    pci_manager_bus_t bus;
    pci_manager_vers_t vers;

    bus.pci_bus = pcie_transport_pci_bus[uid].pci_bus_num;
    bus.pci_domain = pcie_transport_pci_bus[uid].pci_domain;
    bus.pci_devfn = pcie_transport_pci_bus[uid].pci_devfn;

    // open the PCI manager file to do the ioctl calls
    snprintf(pci_mgr_name, name_len, "%s/%s", PCI_MANAGER_PATH, PCI_MANAGER_FILENAME);
    fd = open(pci_mgr_name, O_RDWR);
    if( fd < 0 )
    {
	fprintf(stderr, "Error: unable to access pci_manager\n");
	return (-1);
    }

    // get the driver version and ensure it matches ok
    if( ioctl(fd, PCI_MANAGER_VERSION, &vers) != 0 )
    {
        fprintf(stderr, "Error: unable to determine driver version\n");
        status = -2;
        goto pci_device_stop_exit;
    }
    if ( DRIVER_VERSION(vers.maj_vers, vers.min_vers, vers.patch_vers) <
         DRIVER_VERSION(PCI_MANAGER_MAJ_REQ, PCI_MANAGER_MIN_REQ, PCI_MANAGER_SUBMINOR_REQ) )
    {
        fprintf(stderr, "Error: driver version does not meet minimum requirement\n");
        status = -3;
        goto pci_device_stop_exit;
    }

    if( pcie_transport_init_level == skiq_xport_init_level_basic )
    {
        DmaInterface_close( uid );
    }
    else
    {
        DmaInterfaceShutdown( uid );
    }

    // send the ioctl to stop
    status = ioctl( fd, PCI_MANAGER_STOP_AND_REMOVE_IOCTL, &bus );

pci_device_stop_exit:
    // close the descriptor
    close(fd);

#else
    status = -1;
#endif /* __MINGW32__ */

    return (status);
}

int32_t _pcie_card_fpga_up( uint64_t uid )
{
    int32_t status=0;
#ifndef __MINGW32__
    int32_t fd;
    uint32_t name_len = sizeof(PCI_MANAGER_PATH)+2+sizeof(PCI_MANAGER_FILENAME);
    char pci_mgr_name[name_len];
    pci_manager_bus_t bus;
    pci_manager_vers_t vers;

    bus.pci_bus = pcie_transport_pci_bus[uid].pci_bus_num;
    bus.pci_domain = pcie_transport_pci_bus[uid].pci_domain;
    bus.pci_devfn = pcie_transport_pci_bus[uid].pci_devfn;

    // open the PCI manager file to do the ioctl calls
    snprintf(pci_mgr_name, name_len, "%s/%s", PCI_MANAGER_PATH, PCI_MANAGER_FILENAME);
    fd = open(pci_mgr_name, O_RDWR);

    if( fd < 0 )
    {
        fprintf(stderr, "Error: unable to access pci_manager\n");
        return (-1);
    }

    // get the driver version and ensure it matches ok
    if( ioctl(fd, PCI_MANAGER_VERSION, &vers) != 0 )
    {
        fprintf(stderr, "Error: unable to determine driver version\n");
        status = -2;
        goto pci_device_rescan_exit;
    }
    if ( DRIVER_VERSION(vers.maj_vers, vers.min_vers, vers.patch_vers) <
         DRIVER_VERSION(PCI_MANAGER_MAJ_REQ, PCI_MANAGER_MIN_REQ, PCI_MANAGER_SUBMINOR_REQ) )
    {
        fprintf(stderr, "Error: driver version does not meet minimum requirement\n");
        status = -3;
        goto pci_device_rescan_exit;
    }

    status = ioctl( fd, PCI_MANAGER_FORCE_RESCAN_IOCTL, &bus );
    // test the FPGA interface to make sure the rescan actually worked
    if( status >= 0 )
    {
        // make sure there's a driver entry for the card
        status = DmaInterface_open( uid );
        if( status == 0 )
        {
            DmaInterface_close( uid );
        }
    }

    if( pcie_transport_init_level == skiq_xport_init_level_basic )
    {
        status=DmaInterface_open( uid );
    }
    else
    {
        status=DmaInterfaceInit( uid );
    }

pci_device_rescan_exit:
    // close the descriptor
    close(fd);

#else
    status = -1;
#endif /* __MINGW32__ */

    return (status);
}

int32_t _pci_card_fpga_reg_read( uint64_t xport_uid,
                                 uint32_t addr,
                                 uint32_t* p_data )
{
    int32_t status=0;
    
    status = DmaInterfaceReadRegister( (uint8_t)(xport_uid),
                                       addr,
                                       p_data );

    return (status);
}

int32_t _pci_card_fpga_reg_write( uint64_t xport_uid,
                                  uint32_t addr,
                                  uint32_t data )
{
    int32_t status=0;
    
    status = DmaInterfaceWriteRegister( (uint8_t)(xport_uid),
                                        addr,
                                        data );

    return (status);
}

skiq_xport_fpga_functions_t pcie_xport_fpga_funcs = {
    .fpga_reg_read = _pci_card_fpga_reg_read,
    .fpga_reg_write = _pci_card_fpga_reg_write,
    .fpga_down = _pcie_card_fpga_down,
    .fpga_up = _pcie_card_fpga_up,
};
