/**
 * @file   pcie_tx.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/ 

#include <stdint.h>
#include <stdlib.h>

#include "sidekiq_xport_types.h"

#include <dma_interface_api.h>

/***** DEFINES *****/

#define UNUSED(_x)                      do { (void)(_x); } while (0)

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/

static int32_t pcie_tx_initialize( uint64_t uid,
                                   skiq_tx_transfer_mode_t tx_transfer_mode,
                                   uint32_t num_bytes_to_send,
                                   uint8_t num_send_threads,
                                   int32_t priority,
                                   skiq_tx_callback_t tx_complete_cb )
{
    int32_t status=0;

    status = DmaInterfaceInitSend( (uint8_t)(uid),
                                   tx_transfer_mode,
                                   num_bytes_to_send,
                                   num_send_threads,
                                   priority,
                                   (dma_tx_callback_t)tx_complete_cb );

    return (status);
}

static int32_t pcie_tx_stop_streaming( uint64_t uid,
                                       skiq_tx_hdl_t hdl )
{
    int32_t status=0;

    UNUSED(hdl);

    status = DmaInterfaceSendComplete((uint8_t)(uid));

    return (status);
}

static int32_t pcie_tx_transmit( uint64_t uid,
                                 skiq_tx_hdl_t hdl,
                                 int32_t *p_samples,
                                 void *p_private )
{
    int32_t status=0;

    UNUSED(hdl);

    status = DmaInterfaceSend( (uint8_t)(uid), (uint32_t*)(p_samples), p_private );

    return (status);
}

/***** GLOBAL DATA *****/

skiq_xport_tx_functions_t pcie_xport_tx_funcs = {
    .tx_initialize      = pcie_tx_initialize,
    .tx_start_streaming = NULL,
    .tx_stop_streaming  = pcie_tx_stop_streaming,
    .tx_transmit        = pcie_tx_transmit,
};
