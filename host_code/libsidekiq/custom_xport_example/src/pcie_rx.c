/**
 * @file   pcie_rx.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2017 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/ 

#include <stdint.h>
#include <stdlib.h>

#include "sidekiq_api.h"
#include "sidekiq_xport_types.h"

#include <dma_interface_api.h>

/***** DEFINES *****/

#define UNUSED(_x)                      do { (void)(_x); } while (0)

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static uint32_t card_data_rate[SKIQ_MAX_NUM_CARDS] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = 0,
};

static int32_t card_timeout[SKIQ_MAX_NUM_CARDS] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = RX_TRANSFER_NO_WAIT,
};

/***** LOCAL FUNCTIONS *****/


static int32_t _pcie_rx_flush( uint64_t uid )
{
    int32_t status=0;

    status = DmaInterfaceRxFlush( (uint8_t)(uid) );

    return (status);
}

static int32_t _pcie_rx_receive( uint64_t uid, uint8_t **pp_data, uint32_t *p_data_len )
{
    int32_t status = 0;
    uint64_t user_status = 0;

    status = DmaInterfaceReceive( (uint8_t)(uid), pp_data, p_data_len, &user_status );

    return (status);
}

static int32_t _pcie_rx_set_transfer_timeout( uint64_t uid, const int32_t timeout_us )
{
    int32_t status = 0;
    int32_t poll_minimum, poll_maximum;
    uint32_t blocks_per_seconds, microseconds_per_block;

    card_timeout[uid] = timeout_us;
    if ( RX_TRANSFER_NO_WAIT == timeout_us )
    {
        /* skip the math, poll periods do not matter with non-blocking mode */
        status = DmaInterfaceSetRxTimeout( (uint8_t)(uid),
                                  timeout_us,
                                  RX_POLL_PERIOD_NO_CHANGE,
                                  RX_POLL_PERIOD_NO_CHANGE );
    }
    else
    {
        /* a card_data_rate of 0 could mean no cards are streaming yet, so skip poll period updates */
        if ( 0 == card_data_rate[uid] )
        {
            status = DmaInterfaceSetRxTimeout( (uint8_t)(uid),
                                      timeout_us,
                                      RX_POLL_PERIOD_NO_CHANGE,
                                      RX_POLL_PERIOD_NO_CHANGE );
        }
        else
        {
            /* calculate blocks per second and microseconds per block */
            blocks_per_seconds = card_data_rate[uid] / SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES;

            /* convert to time per block and make certain it does not go lower than 10 */
            microseconds_per_block = 1000000 / blocks_per_seconds;
            if ( microseconds_per_block < 10 ) microseconds_per_block = 10;

            /* set the poll periods to 1-2 blocks unless timeout is less than poll_minimum */
            poll_minimum = microseconds_per_block;
            poll_maximum = 2 * poll_minimum;
            if ( ( RX_TRANSFER_WAIT_FOREVER != timeout_us ) && ( timeout_us < poll_minimum ) )
            {
                poll_minimum = timeout_us;
                poll_maximum = 1 + poll_minimum;
            }

            status = DmaInterfaceSetRxTimeout( (uint8_t)(uid), timeout_us, poll_minimum, poll_maximum );
        }
    }

    return (status);
}


static int32_t _pcie_rx_configure( uint64_t uid, uint32_t aggregate_data_rate )
{
    int32_t status = 0;

    /* if the rate changed, prod the _pcie_rx_set_transfer_timeout to update
     * poll periods */
    if ( card_data_rate[uid] != aggregate_data_rate )
    {
        card_data_rate[uid] = aggregate_data_rate;
        status = _pcie_rx_set_transfer_timeout( uid, card_timeout[uid] );
    }

    return (status);
}


static int32_t
_pcie_rx_set_buffered( uint64_t uid,
                       bool buffered )
{
    UNUSED(uid);
    UNUSED(buffered);

    /* In the PCIe transport, there's no difference between buffered and non-buffered, so always
     * return success */
    return 0;
}


/***** GLOBAL DATA *****/

skiq_xport_rx_functions_t pcie_xport_rx_funcs = {
    .rx_configure            = _pcie_rx_configure,
    .rx_set_block_size       = NULL,
    .rx_set_buffered         = _pcie_rx_set_buffered,
    .rx_start_streaming      = NULL,
    .rx_stop_streaming       = NULL,
    .rx_pause_streaming      = NULL,
    .rx_resume_streaming     = NULL,
    .rx_flush                = _pcie_rx_flush,
    .rx_set_transfer_timeout = _pcie_rx_set_transfer_timeout,
    .rx_receive              = _pcie_rx_receive,
};
