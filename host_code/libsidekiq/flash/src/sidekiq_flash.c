/**
 * @file   sidekiq_flash.c
 * @date   Wed Apr  3 15:14:24 2019
 * 
 * @brief Implements a Sidekiq agnostic interface to the flash that can contain FPGA bitstreams and
 * calibration
 * 
 * <pre>
 * Copyright 2017-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include "sidekiq_flash.h"
#include "sidekiq_flash_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_private.h"
#include "flash_defines.h"

#include "sidekiq_fpga_ctrl.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_flash_types.h"
#include "fpga_reg_flash.h"
#include "usb_flash.h"
#include "portable_endian.h"    /* for htole64() and le64toh() */

/* enable debug_print and debug_print_plain when DEBUG_FLASH is defined */
#if (defined DEBUG_FLASH)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/** @brief  Macro used to display Flash addresses in print functions (assumes 32-bit address) */
#define ADDR_SPECIFIER                        "0x%08" PRIx32

#define MPCIE_FLASH_TOTAL_SIZE                (0x00400000)
#define MPCIE_FLASH_SECTOR_SIZE               (65536)
#define MPCIE_FLASH_PAGE_SIZE                 (256)

#define M2_FLASH_TOTAL_SIZE                   (0x00800000)
#define M2_FLASH_SECTOR_SIZE                  (65536)
#define M2_FLASH_PAGE_SIZE                    (256)

/* The flash sizes across the HTG-K800 and HTG-K810 are the same */
#define HTG_FLASH_TOTAL_SIZE                  (0x04000000)
#define HTG_FLASH_SECTOR_SIZE                 (256 * 1024)
#define HTG_FLASH_PAGE_SIZE                   (512)

/* The user and recovery bitstream addresses depend on the FPGA model and their maximum bitstream
 * file size */
#define HTG_XCKU060_USER_BITSTREAM_ADDR       (0x00800000)
#define HTG_XCKU060_RECOVERY_BITSTREAM_ADDR   (0x00000000)

#define HTG_XCKU115_USER_BITSTREAM_ADDR       (0x00C00000)
#define HTG_XCKU115_RECOVERY_BITSTREAM_ADDR   (0x00000000)


/*
  M.2 2280 rev A/B -- MT25QU128ABA
  NV100 rev A/B/C -- MT25QU128ABA
 */
#define M2_2280_FLASH_TOTAL_SIZE              (0x01000000)
#define M2_2280_FLASH_SECTOR_SIZE             (65536)
#define M2_2280_FLASH_PAGE_SIZE               (256)

#define M2_2280_USER_BITSTREAM_ADDR           (0x00200000)
#define M2_2280_RECOVERY_BITSTREAM_ADDR       (0x00000000)


#define FLASH_TRANSFER_INITIALIZER              \
    {                                           \
        .addr = 0,                              \
        .len = 0,                               \
        .buf = NULL,                            \
    }

#define FLASH_FUNCTIONS_INITIALIZER             \
    {                                           \
        .init = NULL,                           \
        .identify_chip = NULL,                  \
        .get_unique_id = NULL,                  \
        .erase_sector = NULL,                   \
        .read_page = NULL,                      \
        .write_page = NULL,                     \
        .lock_golden_sectors = NULL,            \
        .unlock_golden_sectors = NULL,          \
        .is_golden_locked = NULL,               \
        .load_fpga = NULL,                      \
    }

#define PLATFORM_FLASH_PARAMS_INITIALIZER               \
    {                                                   \
        .skiq_part = skiq_part_invalid,                 \
        .skiq_fmc_carrier = skiq_fmc_carrier_unknown,   \
        .skiq_fpga_device = skiq_fpga_device_unknown,   \
        .flash_params = NULL,                           \
    }

#define FLASH_CARD_INFORMATION_INITIALIZER                  \
    {                                                       \
        .initialized = false,                               \
        .fpga_ver = 0,                                      \
        .fpga_caps = FPGA_CAPABILITIES_INITIALIZER,         \
        .fw_ver = 0,                                        \
        .part_id = FLASH_PART_INFORMATION_INITIALIZER,      \
        .page_size = 0,                                     \
        .sector_size = 0,                                   \
        .total_size = 0,                                    \
        .params = PLATFORM_FLASH_PARAMS_INITIALIZER,        \
        .p_functions = NULL,                                \
    }


/***** MACROS *****/

#define RETURN_IF_NOT_INITIALIZED(card) \
    do  \
    {   \
        if (!flashInfo[(card)].initialized) \
        { \
            skiq_error("Flash chip on card %" PRIu8 " has not been initialized", (card)); \
            return (-ENOENT); \
        } \
    } while (0)


/***** LOCAL TYPE DEFINITIONS *****/


/***** LOCAL VARIABLES *****/

static const uint8_t mpcie_golden_fpga_compare[] =
        {0x32, 0x81, 0x03, 0x20};

static const uint8_t golden_fpga_compare[] =
        {'G', 'O', 'L', 'D', 'E', 'N'};

/**
    @brief  An empty set of Flash functions to specify when Flash is not supported.
*/
static const flash_functions_t flash_funcs_none = FLASH_FUNCTIONS_INITIALIZER;


static flash_params_t mpcie_flash_params =
{
    /* flash geometry */
    .default_page_size = MPCIE_FLASH_PAGE_SIZE,
    .default_sector_size = MPCIE_FLASH_SECTOR_SIZE,
    .default_total_size = MPCIE_FLASH_TOTAL_SIZE,
    .slot_size = (MPCIE_FLASH_TOTAL_SIZE -
                  FLASH_MPCIE_USER_BITSTREAM_ADDR), /* note: user at offset */

    /* addresses */
    .user_addr = FLASH_MPCIE_USER_BITSTREAM_ADDR,
    .recovery_addr = FLASH_MPCIE_RECOVERY_BITSTREAM_ADDR,

    /* recovery identifier */
    .recovery_compare_len = 4,
    .recovery_compare_offset = 28,
    .recovery_compare_pattern = mpcie_golden_fpga_compare,

    /* FPGA SPI capabilities */
    .read_fifo_depth = 511,     /* note: FIFO capacity is (2 ** SPI_READ_COUNT_LEN) - 1 */
    .write_fifo_depth = 511,    /* note: FIFO capacity is (2 ** SPI_WRITE_COUNT_LEN) - 1 */

    /* flash driver capabilities */
    .has_fx2_negotiation = true,
    .has_4byte_addr = false,
    .has_quad_read = false,
    .has_quad_write = false,
    .has_32bit_read_fifo = false,
    .has_spi_delay_counter = false,
    .has_flash_read_flag_register = true,
    .has_write_enable_latch_verify = false,

    /* timing / sleep parameters */
    .fpga_spi_transfer_delay_ns = 100,
    .fpga_flash_status_delay_ns = 100,
    .pci_fpga_delay_to_first_rescan_sec = 10,
    .pci_fpga_delay_to_first_rescan_sec_self_reload = 10,
    .pci_fpga_max_rescan_period_sec = 15,
};

static flash_params_t m2_flash_params =
{
    /* flash geometry */
    .default_page_size = M2_FLASH_PAGE_SIZE,
    .default_sector_size = M2_FLASH_SECTOR_SIZE,
    .default_total_size = M2_FLASH_TOTAL_SIZE,
    .slot_size = (FLASH_M2_RECOVERY_BITSTREAM_ADDR -
                  FLASH_M2_USER_BITSTREAM_ADDR), /* note: recovery at offset */

    /* addresses */
    .user_addr = FLASH_M2_USER_BITSTREAM_ADDR,
    .recovery_addr = FLASH_M2_RECOVERY_BITSTREAM_ADDR,

    /* recovery identifier */
    .recovery_compare_len = 6,
    .recovery_compare_offset = 23,
    .recovery_compare_pattern = golden_fpga_compare,

    /* FPGA SPI capabilities */
    .read_fifo_depth = 511,     /* note: FIFO capacity is (2 ** SPI_READ_COUNT_LEN) - 1 */
    .write_fifo_depth = 511,    /* note: FIFO capacity is (2 ** SPI_WRITE_COUNT_LEN) - 1 */

    /* flash driver capabilities */
    .has_fx2_negotiation = true,
    .has_4byte_addr = false,
    .has_quad_read = false,
    .has_quad_write = false,
    .has_32bit_read_fifo = false,
    .has_spi_delay_counter = false,
    .has_flash_read_flag_register = true,
    .has_write_enable_latch_verify = false,

    /* timing / sleep parameters */
    .fpga_spi_transfer_delay_ns = 100,
    .fpga_flash_status_delay_ns = 100,
    .pci_fpga_delay_to_first_rescan_sec = 10,
    .pci_fpga_delay_to_first_rescan_sec_self_reload = 1,
    .pci_fpga_max_rescan_period_sec = 15,
};


/**
    @brief This parameter set applies to flash parts on the HTG-K800 and HTG-K810 and defines
    addresses for use with the XCKU115
*/
static flash_params_t htg_xcku115_flash_params =
{
    /* flash geometry */
    .default_page_size = HTG_FLASH_PAGE_SIZE,
    .default_sector_size = HTG_FLASH_SECTOR_SIZE,
    .default_total_size = HTG_FLASH_TOTAL_SIZE,
    .slot_size = (HTG_FLASH_TOTAL_SIZE -
                  HTG_XCKU115_USER_BITSTREAM_ADDR), /* note: user at offset */

    /* addresses */
    .user_addr = HTG_XCKU115_USER_BITSTREAM_ADDR,
    .recovery_addr = HTG_XCKU115_RECOVERY_BITSTREAM_ADDR,

    /* recovery identifier */
    .recovery_compare_len = 6,
    .recovery_compare_offset = 0,
    .recovery_compare_pattern = golden_fpga_compare,

    /* FPGA SPI capabilities */
    .read_fifo_depth = 512,     /* note: FIFO capacity is not 2 ** SPI_READ_COUNT_LEN, it is 2 ** (SPI_READ_COUNT_LEN - 1) */
    .write_fifo_depth = 1024,   /* note: FIFO capacity is not 2 ** SPI_WRITE_COUNT_LEN, it is 2 ** (SPI_WRITE_COUNT_LEN - 1) */

    /* flash driver capabilities */
    .has_fx2_negotiation = false,
    .has_4byte_addr = true,
    .has_quad_read = true,
    .has_quad_write = true,
    .has_32bit_read_fifo = true,
    .has_spi_delay_counter = true,
    .has_flash_read_flag_register = false,
    .has_write_enable_latch_verify = true,

    /* timing / sleep parameters */
    .fpga_spi_transfer_delay_ns = 0,
    .fpga_flash_status_delay_ns = 100,
    .pci_fpga_delay_to_first_rescan_sec = 1,
    .pci_fpga_delay_to_first_rescan_sec_self_reload = 1,
    .pci_fpga_max_rescan_period_sec = 15,
};


/**
    @brief This parameter set applies to flash parts on the HTG-K800 and HTG-K810 and defines
    addresses for use with the XCKU060
*/
static flash_params_t htg_xcku060_flash_params =
{
    /* flash geometry */
    .default_page_size = HTG_FLASH_PAGE_SIZE,
    .default_sector_size = HTG_FLASH_SECTOR_SIZE,
    .default_total_size = HTG_FLASH_TOTAL_SIZE,
    .slot_size = (HTG_FLASH_TOTAL_SIZE -
                  HTG_XCKU060_USER_BITSTREAM_ADDR), /* note: user at offset */

    /* addresses */
    .user_addr = HTG_XCKU060_USER_BITSTREAM_ADDR,
    .recovery_addr = HTG_XCKU060_RECOVERY_BITSTREAM_ADDR,

    /* recovery identifier */
    .recovery_compare_len = 6,
    .recovery_compare_offset = 0,
    .recovery_compare_pattern = golden_fpga_compare,

    /* FPGA SPI capabilities */
    .read_fifo_depth = 512,     /* note: FIFO capacity is not 2 ** SPI_READ_COUNT_LEN, it is 2 ** (SPI_READ_COUNT_LEN - 1) */
    .write_fifo_depth = 1024,   /* note: FIFO capacity is not 2 ** SPI_WRITE_COUNT_LEN, it is 2 ** (SPI_WRITE_COUNT_LEN - 1) */

    /* flash driver capabilities */
    .has_fx2_negotiation = false,
    .has_4byte_addr = true,
    .has_quad_read = true,
    .has_quad_write = true,
    .has_32bit_read_fifo = true,
    .has_spi_delay_counter = true,
    .has_flash_read_flag_register = false,
    .has_write_enable_latch_verify = true,

    /* timing / sleep parameters */
    .fpga_spi_transfer_delay_ns = 0,
    .fpga_flash_status_delay_ns = 100,
    .pci_fpga_delay_to_first_rescan_sec = 1,
    .pci_fpga_delay_to_first_rescan_sec_self_reload = 1,
    .pci_fpga_max_rescan_period_sec = 15,
};


static flash_params_t m2_2280_flash_params =
{
    /* flash geometry */
    .default_page_size = M2_2280_FLASH_PAGE_SIZE,
    .default_sector_size = M2_2280_FLASH_SECTOR_SIZE,
    .default_total_size = M2_2280_FLASH_TOTAL_SIZE,
    .slot_size = (M2_2280_FLASH_TOTAL_SIZE -
                  M2_2280_USER_BITSTREAM_ADDR), /* note: user at offset */

    /* addresses */
    .user_addr = M2_2280_USER_BITSTREAM_ADDR,
    .recovery_addr = M2_2280_RECOVERY_BITSTREAM_ADDR,

    /* recovery identifier */
    .recovery_compare_len = 6,
    .recovery_compare_offset = 0,
    .recovery_compare_pattern = golden_fpga_compare,

    /* FPGA SPI capabilities */
    .read_fifo_depth = 511,     /* note: FIFO capacity is (2 ** SPI_READ_COUNT_LEN) - 1 */
    .write_fifo_depth = 511,    /* note: FIFO capacity is (2 ** SPI_WRITE_COUNT_LEN) - 1 */

    /* flash driver capabilities */
    .has_fx2_negotiation = false,
    .has_4byte_addr = false,
    .has_quad_read = false,
    .has_quad_write = false,
    .has_32bit_read_fifo = false,
    .has_spi_delay_counter = false,
    .has_flash_read_flag_register = true,
    .has_write_enable_latch_verify = false,

    /* timing / sleep parameters */
    .fpga_spi_transfer_delay_ns = 100,
    .fpga_flash_status_delay_ns = 100,

    /* rescan parameters */
    .pci_fpga_delay_to_first_rescan_sec = 2,
    .pci_fpga_delay_to_first_rescan_sec_self_reload = 1,
    .pci_fpga_max_rescan_period_sec = 15,
};


#define PROD_FLASH_PARAMS(_part,_fpga,_params)                          \
    FMC_PROD_FLASH_PARAMS(_part,skiq_fmc_carrier_not_applicable,_fpga,_params)

#define FMC_PROD_FLASH_PARAMS(_part,_fmc,_fpga,_params) \
    {                                                   \
        .skiq_part = _part,                             \
        .skiq_fmc_carrier = _fmc,                       \
        .skiq_fpga_device = _fpga,                      \
        .flash_params = &(_params),                     \
    }

static platform_flash_params_t platform_flash_params[] =
{
    PROD_FLASH_PARAMS(skiq_mpcie,      skiq_fpga_device_xc6slx45t, mpcie_flash_params),
    PROD_FLASH_PARAMS(skiq_m2,         skiq_fpga_device_xc7a50t,   m2_flash_params),
    PROD_FLASH_PARAMS(skiq_m2_2280,    skiq_fpga_device_xc7a50t,   m2_2280_flash_params),

    /* The NV100 uses the same flash part as M.2-2280 (Stretch) */
    PROD_FLASH_PARAMS(skiq_nv100,      skiq_fpga_device_xc7a50t,   m2_2280_flash_params),

    /* Flash parameters for the HTG-K800 series of FMC carriers */
    FMC_PROD_FLASH_PARAMS(skiq_x2, skiq_fmc_carrier_htg_k800, skiq_fpga_device_xcku060, htg_xcku060_flash_params),
    FMC_PROD_FLASH_PARAMS(skiq_x2, skiq_fmc_carrier_htg_k800, skiq_fpga_device_xcku115, htg_xcku115_flash_params),
    FMC_PROD_FLASH_PARAMS(skiq_x4, skiq_fmc_carrier_htg_k800, skiq_fpga_device_xcku060, htg_xcku060_flash_params),
    FMC_PROD_FLASH_PARAMS(skiq_x4, skiq_fmc_carrier_htg_k800, skiq_fpga_device_xcku115, htg_xcku115_flash_params),

    /* Flash parameters for the HTG-K810 series of FMC carriers */
    FMC_PROD_FLASH_PARAMS(skiq_x2, skiq_fmc_carrier_htg_k810, skiq_fpga_device_xcku060, htg_xcku060_flash_params),
    FMC_PROD_FLASH_PARAMS(skiq_x2, skiq_fmc_carrier_htg_k810, skiq_fpga_device_xcku115, htg_xcku115_flash_params),
    FMC_PROD_FLASH_PARAMS(skiq_x4, skiq_fmc_carrier_htg_k810, skiq_fpga_device_xcku060, htg_xcku060_flash_params),
    FMC_PROD_FLASH_PARAMS(skiq_x4, skiq_fmc_carrier_htg_k810, skiq_fpga_device_xcku115, htg_xcku115_flash_params),
};
static uint8_t nr_platform_flash_params = ARRAY_SIZE( platform_flash_params );

static ARRAY_WITH_DEFAULTS(flash_card_information_t, flashInfo, SKIQ_MAX_NUM_CARDS,
                           FLASH_CARD_INFORMATION_INITIALIZER);


/**
    @todo   Make a table mapping FlashChips to sizes... even better, use SFDP to determine these
            values.
*/

/***** LOCAL FUNCTIONS *****/

#if defined(WIN32) || defined(__MINGW32__)
// The function memmem is missing from Windows (appears to be in newlib). We'll
// implement it here for the time being.

/* Byte-wise substring search, using the Two-Way algorithm.
 * Copyright (C) 2008 Eric Blake
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
void *memmem(const void *haystack,
             size_t haystacklen,
             const void *needle,
             size_t needlelen)
{
    /* Abstract memory is considered to be an array of 'unsigned char' values,
    not an array of 'char' values.  See ISO C 99 section 6.2.6.1.  */

    if (needlelen == 0)
    {
        /* The first occurrence of the empty string is deemed to occur at
        the beginning of the string.  */
        return (void *) haystack;
    }

    /* Less code size, but quadratic performance in the worst case.  */
    while (needlelen <= haystacklen)
    {
        if (!memcmp (haystack, needle, needlelen))
        {
            return (void *) haystack;
        }

        haystack++;
        haystacklen--;
    }
    return NULL;
}
#endif

/**
    @brief  Based upon the part's JEDEC identification information, determine the total size
            of the Flash part

    @note   The capacity field is only updated if the part is identified and the capacity
            identifier for the manufacturer is recognized

    This information came from experimentation and:

    | Chip | Datasheet | Page | Notes |
    | ISSI IS25 | https://www.mouser.com/datasheet/2/198/IS25WP032-064-128-737458.pdf | 65 | "Memory Type + Capacity" |
    | Macronix MX25 | https://www.macronix.com/Lists/Datasheet/Attachments/7656/MX25U12835F,%201.8V,%20128Mb,%20v1.9.pdf | 38 | "Memory Density" |
    | Micron N25 | https://www.micron.com/-/media/client/global/documents/products/data-sheet/nor-flash/serial-nor/n25q/n25q_32mb_1_8v_65nm.pdf | 35 | "Memory capacity" |
    | Micron MT25Q | https://www.micron.com/-/media/client/global/documents/products/data-sheet/nor-flash/serial-nor/mt25q/die-rev-a/mt25q_qlhs_l_128_aba_0.pdf | 36 | "Memory capacity" |
    | Spansion S25 | https://www.cypress.com/file/177971/download | 117 | "Device ID Least Significant Byte - Density" |
    | Winbond W25 | https://www.mouser.com/datasheet/2/949/w25q128fw_revk_05182017-1489721.pdf | 24 | "Instruction 9Fh" |

    @param[in]  card        the Sidekiq card ID
*/
static void chip_update_capacity( uint8_t card)
{
#define MBITS_TO_BYTES(mb)      ( ((mb) / 8) * (1024 * 1024) )
    flash_card_information_t *p_flashInfo = &(flashInfo[card]);
    flash_part_information_t *p_partInfo = &(p_flashInfo->part_id);
    uint8_t capacity = 0;
    uint16_t newMbitSize = 0;

    capacity = p_partInfo->manufacturer_id_capacity;

    switch (p_partInfo->chip)
    {
    case flash_chip_type_micron_n25:
    case flash_chip_type_micron_mt25q:
        switch (capacity)
        {
        case 0x16:  /* 32Mbit */
            newMbitSize = 32;
            break;
        case 0x17:  /* 64Mbit */
            newMbitSize = 64;
            break;
        case 0x18:  /* 128Mbit */
            newMbitSize = 128;
            break;
        case 0x19:  /* 256Mbit */
            newMbitSize = 256;
            break;
        case 0x20:  /* 512Mbit */
            newMbitSize = 512;
            break;
        case 0x21:  /* 1Gbit */
            newMbitSize = 1024;
            break;
        case 0x22:  /* 2Gbit */
            newMbitSize = 2048;
            break;
        default:
            skiq_info("Unrecognized Micron capacity 0x%" PRIX8 "\n", capacity);
            break;
        }
        break;
    case flash_chip_type_winbond_w25:
        switch (capacity)
        {
        case 0x18:  /* 128Mbit */
            newMbitSize = 128;
            break;
        default:
            skiq_info("Unrecognized Winbond capacity 0x%" PRIX8 "\n", capacity);
            break;
        }
        break;
    case flash_chip_type_issi_is25:
        switch (capacity)
        {
        case 0x16:  /* 32Mbit */
            newMbitSize = 32;
            break;
        case 0x17:  /* 64Mbit */
            newMbitSize = 64;
            break;
        case 0x18:  /* 128Mbit */
            newMbitSize = 128;
            break;
        default:
            skiq_info("Unrecognized ISSI capacity 0x%" PRIX8 "\n", capacity);
            break;
        }
        break;
    case flash_chip_type_macronix_mx25:
        switch (capacity)
        {
        case 0x38:  /* 128Mbit */
            newMbitSize = 128;
            break;
        default:
            skiq_info("Unrecognized Macronix capacity 0x%" PRIX8 "\n", capacity);
            break;
        }
        break;
    case flash_chip_type_spansion_s25:
        switch (capacity)
        {
        case 0x20:  /* 512Mbit */
            newMbitSize = 512;
            break;
        default:
            skiq_info("Unrecognized Spansion capacity 0x%" PRIX8 "\n", capacity);
            break;
        }
        break;
    case flash_chip_type_none:
    case flash_chip_type_unknown:
    default:
        break;
    }

    if (0 != newMbitSize)
    {
        uint32_t newTotalSize = MBITS_TO_BYTES(newMbitSize);

        debug_print("Updating chip total capacity: JEDEC ID 0x%" PRIX8 " Mfr ID 0x%" PRIX8
            " Mfr capacity 0x%" PRIX8 "; was %" PRIu32 " bytes, now %" PRIu32 " bytes\n",
            p_partInfo->jedec_id_manufacturer, p_partInfo->manufacturer_id_type,
            p_partInfo->manufacturer_id_capacity, p_flashInfo->total_size, newTotalSize);
        p_flashInfo->total_size = newTotalSize;
    }
    else
    {
        debug_print("Not updating chip total capacity: JEDEC ID 0x%" PRIX8 " Mfr ID 0x%" PRIX8
            " Mfr capacity 0x%" PRIX8 "; keeping at default value of %" PRIu32 " bytes\n",
            p_partInfo->jedec_id_manufacturer, p_partInfo->manufacturer_id_type,
            p_partInfo->manufacturer_id_capacity, p_flashInfo->total_size);
    }
#undef MBITS_TO_BYTES
}

static platform_flash_params_t * _get_product_flash_params( uint8_t card )
{
    platform_flash_params_t *p_platFlashParams = NULL;
    skiq_part_t part = _skiq_get_part( card );
    skiq_fmc_carrier_t carrier = _skiq_get_fmc_carrier( card );
    skiq_fpga_device_t device = _skiq_get_fpga_device( card );
    uint8_t i;

    for ( i = 0; ( i < nr_platform_flash_params ) && ( p_platFlashParams == NULL ); i++ )
    {
        if ( ( platform_flash_params[i].skiq_part == part ) &&
             ( platform_flash_params[i].skiq_fmc_carrier == carrier ) &&
             ( platform_flash_params[i].skiq_fpga_device == device ) )
        {
            p_platFlashParams = &(platform_flash_params[i]);
        }
    }

    if ( p_platFlashParams == NULL )
    {
        skiq_error("Internal error: unable to determine flash parameter for card %" PRIu8 "\n",
            card);
    }

    return p_platFlashParams;
}

static flash_functions_t *
_select_fpga_functions_for_card( uint8_t card )
{
    flash_functions_t *p_functions = (flash_functions_t *) &(flash_funcs_none);

    /* FLASH_SUPPORT capability bit showed up in FPGA v3.11.0, otherwise use FMC carrier to make a
     * determination.  Use _meets_fpga_version_nocache() here instead of
     * skiq_read_fpga_semantic_version() because there are cases when the FPGA version is not yet
     * cached */
    if ( _skiq_meets_fpga_version_nocache( card, 3, 11, 0 ) )
    {
        struct fpga_capabilities fpga_caps = FPGA_CAPABILITIES_INITIALIZER;

        /* call fpga_ctrl_read_capabilities() here instead of _skiq_get_fpga_caps() because
         * there are cases when the capabilities are not yet cached */
        if ( fpga_ctrl_read_capabilities( card, &fpga_caps ) == 0 )
        {
            if ( fpga_caps.flash_support > 0 )
            {
                p_functions = &(fpga_reg_flash);
                debug_print("Full flash support for card %" PRIu8 " as dictated by FPGA "
                            "capabilities\n", card);
            }
            else
            {
                p_functions = &(fpga_reg_flash_only_load);
                skiq_warning("FPGA capabilities indicate no support for reading/writing flash "
                             "for card %" PRIu8 "\n", card);
            }
        }
        else
        {
            p_functions = (flash_functions_t *) &(flash_funcs_none);
            skiq_error("Unable to query FPGA register for capabilities for card %" PRIu8 "\n",
                       card);
        }
    }
    else
    {
        skiq_part_t part;
        skiq_fmc_carrier_t carrier;

        part = _skiq_get_part( card );
        if ( ( skiq_z2 == part ) ||  ( skiq_z2p == part ) || ( skiq_z3u == part ) )
        {
            p_functions = (flash_functions_t *) &(flash_funcs_none);
            debug_print("Z2 / Z2P / Z3u part found; no support for reading/writing flash nor for "
                " reloading the bitstream for card %" PRIu8 "\n", card);
        }
        else
        {
        carrier = _skiq_get_fmc_carrier_nocache( card );
        switch (carrier)
        {
            case skiq_fmc_carrier_not_applicable:
                    p_functions = &(fpga_reg_flash);
                debug_print("Full flash support for card %" PRIu8 " as determined by non-FMC "
                            "carrier Sidekiq\n", card);
                break;

            case skiq_fmc_carrier_htg_k800:
            case skiq_fmc_carrier_htg_k810:
                    p_functions = &(fpga_reg_flash);
                debug_print("Full flash support for card %" PRIu8 " as determined by FMC "
                            "carrier HTG-K800\n", card);
                break;

            case skiq_fmc_carrier_ams_wb3xzd:
            case skiq_fmc_carrier_ams_wb3xbm:
                    p_functions = &(fpga_reg_flash_only_load);
                skiq_warning("FMC carrier has no support for reading/writing flash for "
                             "card %" PRIu8 ", only reloading the bitstream\n", card);
                break;

            default:
                    p_functions = (flash_functions_t *) &(flash_funcs_none);
                skiq_warning("FMC carrier has no support for reading/writing flash nor for "
                             "reloading the bitstream for card %" PRIu8 "\n", card);
                    break;
            }
        }
    }

    return p_functions;
}


/**
   @brief helper function to determine if part has self reloading capabilities.
 */
static bool
part_has_self_reload( uint8_t card )
{
    flash_card_information_t *p_card = &(flashInfo[card]);
    bool has_self_reload = false;

    /* if the FPGA version is 3.8.0 or later, check the CAPABILITIES flags for
     * self reloading capability */
    if ( p_card->fpga_ver >= FPGA_VERSION(3,8,0) )
    {
        has_self_reload = ( p_card->fpga_caps.self_reload > 0 );
    }
    else
    {
        /* otherwise set has_self_reload according to part and FPGA version */
        switch (p_card->params.skiq_part)
        {
            case skiq_m2:
                /* @note for M.2, since this is the USB implementation of reloading
                 * the FPGA, it means it should use self reload if the FPGA version
                 * is 3.6 or greater */
                if ( p_card->fpga_ver >= FPGA_VERSION(3,6,0) )
                {
                    has_self_reload = true;
                }
                else
                {
                    has_self_reload = false;
                }
                break;

            case skiq_mpcie:
                /* mPCIe will never have self reload capabilities */
                has_self_reload = false;
                break;

            case skiq_x2:
                /* X2 only has self reload capabilities */
                has_self_reload = true;
                break;

            case skiq_x4:
                /* X4 only has self reload capabilities */
                has_self_reload = true;
                break;

            case skiq_m2_2280:
                /* M.2 2280 has self reload capabilities */
                has_self_reload = true;
                break;

            case skiq_nv100:
                /* NV100 has self reload capabilities */
                has_self_reload = true;
                break;

            default:
                /* default case is no self reload */
                has_self_reload = false;
                break;
        }
    }

    /* @warning It seems that self reloading an M.2 on an Intel NUC can be problematic as it doesn't
     * always come back.  So, for the time being, if USB is available, let's use that since we can
     * retry configuring the FPGA from flash multiple times over USB whereas we only get one shot
     * with self-reload */
    if ( card_mgr_is_xport_avail( card, skiq_xport_type_usb ) )
    {
        has_self_reload = false;
    }

    return has_self_reload;
}


/**************************************************************************************************/
/**
    @brief  Performs the actual instructions needed to save a file to flash. If the file is a fpga
            configuration, the sync word in the first page of flash will be cleared out initially
            and then be written down after the rest of the data has been successfully written down.
            Otherwise, all data is written in a linear manner.

    @param[in]  card         requested Sidekiq card ID
    @param[in]  addr         address in flash to write to
    @param[in]  p_file       file to write to flash
    @param[in]  is_fpga      set to true if file is a FPGA configuration

    @return int32_t     status where 0=success, anything else is an error
    @retval -ENOTSUP    if Flash access isn't supported for the card
    @retval -EFBIG      if the specified bitstream exceeds the slot or total Flash chip size
*/
static int32_t _write_file(uint8_t card, uint32_t addr, FILE* p_file, bool is_fpga)
{
    flash_card_information_t *p_flash = &(flashInfo[card]);

    RETURN_IF_NOT_INITIALIZED(card);

    int32_t (*erase_sector)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->erase_sector;
    int32_t (*write_page)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->write_page;

    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;

    uint8_t buf[FLASH_PAGE_MAX_SIZE] = {0};
    uint8_t* p_sync = NULL;
    uint32_t start_addr = addr;
    uint32_t sector_erase = 0;
    uint32_t sector_size = 0;
    uint32_t page_write = 0;
    uint32_t page_size = 0;
    int32_t status = 0;
    long file_size = 0;
    uint32_t n = 0;

    ///////////////////////////////////////////////////////////////////////////
    // 1. Check Input
    if( (NULL == erase_sector) || (NULL == write_page) )
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        return -ENOTSUP;
    }

    page_size = p_flash->page_size;
    sector_size = p_flash->sector_size;

    file_size = hal_file_stream_size(p_file);
    if ( file_size < 0 )
    {
        skiq_error("Failed to determine size of specified file for card %" PRIu8 " (%d: '%s')\n",
                   card, (int32_t)file_size, strerror(-file_size));
        return file_size;
    }

    // For FPGA bitstreams, enforce tighter memory constraint to allow room for
    // multiple bitstreams to exist in peace. We'll allow files to only be
    // checked against the available flash memory and trust that whomever has
    // generic flash access (which should ONLY be internal developers) is being
    // very careful with their privilege.
    if( is_fpga )
    {
        if( file_size > p_flash->params.flash_params->slot_size )
        {
            skiq_error("FPGA bitstream size exceeds slot bounds (%" PRIu32 " > %" PRIu32
                       ") on card %" PRIu8 "\n", (uint32_t)file_size,
                       p_flash->params.flash_params->slot_size, card);
            return -EFBIG;
        }
    }
    else if( file_size > p_flash->total_size )
    {
        skiq_error("Specified file exceeds overall flash size (%" PRIu32 " > %" PRIu32
                   ") on card %" PRIu8 "\n", (uint32_t)file_size, p_flash->total_size, card);
        return -EFBIG;
    }
    else if ( p_flash->sector_size == 0 )
    {
        skiq_error("Flash sector size is zero, expected non-zero for card %" PRIu8 "\n", card);
        return -ENOTSUP;
    }

    ///////////////////////////////////////////////////////////////////////////
    // 2. Erase Flash
    sector_erase = DIV_ROUND_UP(file_size, p_flash->sector_size);
    while( (sector_erase--) && (0 == status) )
    {
        skiq_debug("Erasing flash sector at address " ADDR_SPECIFIER " on card %" PRIu8 "\n",
            addr, card);
        transfer.addr = addr;
        transfer.buf = NULL;
        transfer.len = 0;
        addr += sector_size;

        status = erase_sector(card, &transfer, p_flash);
        if( 0 != status )
        {
            skiq_error( "Failed erase of flash sector at address " ADDR_SPECIFIER " on card %" PRIu8
                " (status %" PRIi32 ")\n", transfer.addr, card, status);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // 3. Write Flash (No Syncword)
    transfer.buf = buf;
    addr = start_addr;
    page_write = DIV_ROUND_UP(file_size, p_flash->page_size);
    while( (page_write--) && (0 == status) )
    {
        n = fread(buf, 1, page_size, p_file);
        if( 0 >= n )
        {
            skiq_error("Failed to read from specified file for card %" PRIu8 " (status %" PRIi32
                ")\n", card, errno);
            status = -EIO;
        }
        else
        {
            // status info displayed per sector
            if( (addr % sector_size) == 0 )
            {
                skiq_debug("Writing to flash sector starting at address " ADDR_SPECIFIER " on card %"
                    PRIu8 "\n", addr, card);
            }

            if( (addr == start_addr) && (is_fpga) )
            {
                // ASSUMPTION: sync word is in the first page of FPGA data
                p_sync = memmem(buf, page_size, fpga_sync_word, FPGA_SYNC_WORD_LEN);
                if( NULL == p_sync )
                {
                    skiq_error("Specified FPGA bitstream file does not contain sync word for "
                               "card %" PRIu8 "\n", card);
                    status = -ENOEXEC;
                }
                else
                {
                    // clear the sync word, we will write this at the end
                    memset(p_sync, 0xFF, FPGA_SYNC_WORD_LEN);
                }
            }

            if( 0 == status )
            {
                transfer.addr = addr;
                transfer.len = n;

                status = write_page(card, &transfer, p_flash);
                if( 0 != status )
                {
                    skiq_error("Failed write to flash page at address " ADDR_SPECIFIER " on card %"
                        PRIu8 " (status %" PRIi32 ")\n", addr, card, status);
                }
                // increment to the next page of flash
                addr += page_size;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // 4. Write Syncword (FPGA only)
    if( (0 == status) && (is_fpga) )
    {
        fseek(p_file, 0, SEEK_SET);
        n = fread(buf, 1, page_size, p_file);
        fseek(p_file, 0, SEEK_END);

        transfer.addr = start_addr;
        transfer.len = n;

        // if we read the complete first page (with sync word) from file, write
        // it down into flash memory
        status = (n == page_size) ? write_page(card, &transfer, p_flash) : -EIO;

        // this is a catch all for any errors writing the sync word
        if( 0 != status )
        {
            skiq_error("Failed to write sync word to flash on card %" PRIu8 " (status %" PRIi32
                ")\n", card, status);
        }
    }

    return status;
}

static int32_t
detect_flash_chip(uint8_t card)
{
    int32_t result = 0;
    flash_card_information_t *p_flash = &(flashInfo[card]);

    if (NULL == p_flash->p_functions->identify_chip)
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        return (-ENOTSUP);
    }
    else
    {
        result = p_flash->p_functions->identify_chip(card, p_flash, &(p_flash->part_id));
        if (-ENOTSUP == result)
        {
            skiq_warning("Card %" PRIu8 " does not support reading the Flash chip identity\n",
                card);
            p_flash->part_id.chip = flash_chip_type_none;
        }
        else if (0 != result)
        {
            skiq_error("Failed to identify Flash chip for card %" PRIu8 " (status %" PRIi32 ")\n",
                card, result);
            /** @todo   What to do here? */
        }
    }

    if ( ( flash_chip_type_none != p_flash->part_id.chip ) &&
         ( flash_chip_type_unknown != p_flash->part_id.chip ) )
    {
        chip_update_capacity(card);
    }

    return (result);
}


/**
    @brief  Re-initialize the per-card data structure after a reprogramming operation

    After reloading a bitfile, the Flash configuration could've change - for example, switching
    from a PCI & USB enabled bitstream to a USB-only bitstream. This function just closes the
    specified card in this layer and attempts to re-initialize it.

    @param[in]  card        The ID of the card that was reprogrammed

    @return 0 on success, else an errno
*/
static int32_t
reload_layer_after_reprogram(uint8_t card)
{
    int32_t status = 0;

    status = flash_exit_card(card);
    if ( 0 != status )
    {
        skiq_error("Failed to close Flash layer before reinitialization on card %" PRIu8
            " (status = %" PRIi32 ")\n", card, status);
    }

    if ( status == 0 )
    {
        skiq_xport_type_t resolved_type;

        /* When the FPGA is reconfigured, call flash_select_functions_for_card() to reconsider what
         * flash support is available.  In order to do that, consider the currently configured
         * transport type (resolving as appropriate) and call to reselect the flash functions
         * accordingly */
        resolved_type = _skiq_resolve_transport_type( card );
        if ( resolved_type != skiq_xport_type_unknown )
        {
            flash_select_functions_for_card( card, resolved_type );
        }
        else
        {
            status = -ESRCH;
        }
    }

    if ( status == 0 )
    {
        status = flash_init_card(card);
        if ( 0 != status )
        {
            skiq_error("Failed to reinitialize Flash layer after FPGA reload on card %" PRIu8
                " (status = %" PRIi32 ")\n", card, status);
        }
    }

    return (status);
}


/***** GLOBAL FUNCTIONS *****/

/*****************************************************************************/
/**
    @brief  Selects the appropriate flash access functions according to the
    provided transport type
*/
void flash_select_functions_for_card( uint8_t card, skiq_xport_type_t type )
{
    // custom uses FPGA registers for flash access
    if ( ( type == skiq_xport_type_custom ) ||
         ( type == skiq_xport_type_pcie ) ||
         ( type == skiq_xport_type_net ) )
    {
        debug_print("Selecting flash functionality based on FPGA support and/or FMC carrier for "
                    "card %" PRIu8 "\n", card);
        flashInfo[card].p_functions = _select_fpga_functions_for_card( card );
    }
    else if ( type == skiq_xport_type_usb )
    {
        debug_print("Selecting flash functionality over USB for card %" PRIu8 "\n", card);
        flashInfo[card].p_functions = &(usb_flash);
    }
    else
    {
        flashInfo[card].p_functions = (flash_functions_t *) &(flash_funcs_none);
        skiq_error("Unsupported transport type (%u) for card %" PRIu8 "\n", type, card);
        hal_critical_exit(-ENOTSUP);
    }
}


/*****************************************************************************/
/**
    @brief  Initialize flash functionality for the specified card
*/
int32_t flash_init_card( uint8_t card )
{
    int32_t result = 0;
    flash_card_information_t *p_flash = NULL;
    platform_flash_params_t *p_platFlashParams = NULL;
    skiq_part_t part = skiq_part_invalid;
    uint8_t maj = 0, min = 0, patch = 0;

    debug_print("Initializing Flash on card %" PRIu8 "...\n", card);

    p_flash = &(flashInfo[card]);

    if ( p_flash->initialized )
    {
        skiq_info("Flash layer already initialized for card %" PRIu8 "; not re-initializing.\n",
            card);
        return ( 0 );
    }

    /* Get information on the Sidekiq card */
    result = skiq_read_fpga_semantic_version(card, &maj, &min, &patch);
    if( 0 != result )
    {
        skiq_error("Failed to read FPGA version on card %" PRIu8 " (result = %" PRIi32 ")\n",
            card, result);
        return result;
    }

    /* Pack FPGA version and capture capabilities */
    p_flash->fpga_ver = FPGA_VERSION(maj, min, patch);
    p_flash->fpga_caps = _skiq_get_fpga_caps(card);

    /* read firmware version for mPCIe or M.2 */
    part = _skiq_get_part( card );
    if ( (part == skiq_mpcie) || (part == skiq_m2) )
    {
        result = skiq_read_fw_version(card, &maj, &min);
        if( 0 != result )
        {
            skiq_error("Failed to read FW version on card %" PRIu8 " (result = %" PRIi32 ")\n",
                card, result);
            return result;
        }

        /* Pack firmware version */
        p_flash->fw_ver = FW_VERSION(maj, min);
    }
    else
    {
        p_flash->fw_ver = 0;
    }

    /*
        As part of the transport init, flash_select_functions_for_card() should've already been
        called... and that assigns the per-transport functions. Based upon its choice of flash
        routines, we can figure out if a Flash chip is present and/or accessible
    */
    if ( ( ( NULL == p_flash->p_functions ) ||
           ( &(flash_funcs_none) == p_flash->p_functions) ) ||
         ( NULL == p_flash->p_functions->read_page ) )
    {
        p_flash->part_id.chip = flash_chip_type_none;
    }

    if ( flash_chip_type_none != p_flash->part_id.chip )
    {
        /* Get per-product Flash parameters */
        p_platFlashParams = _get_product_flash_params(card);
        if (NULL == p_platFlashParams)
        {
            skiq_unknown("Failed to get Flash parameters on card %" PRIu8 "\n", card);
            return -EIO;
        }
        else
        {
            p_flash->params = *p_platFlashParams;

            /* Assign the default size values as a fallback. */
            p_flash->page_size = p_flash->params.flash_params->default_page_size;
            p_flash->sector_size = p_flash->params.flash_params->default_sector_size;
            p_flash->total_size = p_flash->params.flash_params->default_total_size;
        }

        /* The ability to self-reload depends on what the FPGA version is in use */
        p_flash->params.has_self_reload = part_has_self_reload( card );

        result = detect_flash_chip(card);
        if (0 != result)
        {
            skiq_error("Failed to detect Flash part on card %" PRIu8 " (status = %" PRIi32 ")\n",
                card, result);
            return -EIO;
        }

        if (FLASH_PAGE_MAX_SIZE < p_flash->page_size)
        {
            skiq_error("For card %" PRIu8 ", the part's Flash page size (%" PRIu32 " bytes) is"
                " greater than the maximum allowed (%" PRIu32 ")", card, p_flash->page_size,
                (uint32_t) FLASH_PAGE_MAX_SIZE);

            flash_card_information_t blank = FLASH_CARD_INFORMATION_INITIALIZER;
            memcpy(&(flashInfo[card]), &(blank), sizeof(blank));
            p_flash->initialized = false;

            return -E2BIG;
        }

        if (NULL != p_flash->p_functions->init)
        {
            result = p_flash->p_functions->init(card, p_flash);
            if (0 != result)
            {
                skiq_error("Failed Flash per-transport initialization on card %" PRIu8 " (status %"
                    PRIi32 ")\n", card, result);
            }
        }
    }

    if (0 == result)
    {
        p_flash->initialized = true;
    }

    debug_print("Exiting Flash initialization on card %" PRIu8 " (result = %" PRIi32 ")...\n",
        card, result);

    return (result);
}


/*****************************************************************************/
/**
    @brief  Shutdown flash functionality for the specified card
*/
int32_t flash_exit_card( uint8_t card )
{
    int32_t result = 0;

    debug_print("Uninitializing Flash on card %" PRIu8 "...\n", card);

    /** @todo   Should this function do anything if in the middle of a Flash transaction? */

    flash_card_information_t blank = FLASH_CARD_INFORMATION_INITIALIZER;
    memcpy(&(flashInfo[card]), &(blank), sizeof(blank));
    flashInfo[card].initialized = false;

    debug_print("Exiting Flash uninitialization on card %" PRIu8 "...\n", card);

    return (result);
}


/*****************************************************************************/
/**
    @brief  Obtains flash address of the user FPGA configuration.
*/
int32_t flash_get_user_fpga_addr( uint8_t card, uint32_t *p_addr )
{
    int32_t result = 0;
    uint32_t addr = 0;

    RETURN_IF_NOT_INITIALIZED(card);

    if (flashInfo[card].params.flash_params == NULL)
    {
        /** @todo   Should this just be checked on initialization? */
        skiq_error("Unsupported Sidekiq card (%u) for flash access\n", card);
        result = -ENOTSUP;
        hal_critical_exit((int) result);
    }
    else
    {
        addr = flashInfo[card].params.flash_params->user_addr;
        debug_print("user address for card %" PRIu8 " is " ADDR_SPECIFIER "\n", card, addr);
        if (NULL != p_addr)
        {
            *p_addr = addr;
        }
    }

    return (result);
}


/*****************************************************************************/
/**
    @brief  Obtains flash address of the golden FPGA configuration.
*/
int32_t flash_get_golden_fpga_addr( uint8_t card, uint32_t *p_addr )
{
    int32_t result = 0;
    uint32_t addr = 0;

    RETURN_IF_NOT_INITIALIZED(card);

    if (flashInfo[card].params.flash_params == NULL)
    {
        skiq_error("Unsupported Sidekiq card (%u) for flash access\n", card);
        result = -ENOTSUP;
        hal_critical_exit((int) result);
    }
    else
    {
        addr = flashInfo[card].params.flash_params->recovery_addr;
        debug_print("recovery address for card %" PRIu8 " is " ADDR_SPECIFIER "\n", card, addr);
        if (NULL != p_addr)
        {
            *p_addr = addr;
        }
    }

    return (result);
}


/*****************************************************************************/
/**
    @brief Verifies the presence of the golden FPGA in flash memory.
*/
int32_t flash_verify_golden_present(uint8_t card, bool *p_present)
{
    int32_t status = 0;
    uint8_t buf[FLASH_PAGE_MAX_SIZE] = { 0 };
    uint8_t* p_sync_cmp = NULL;
    flash_params_t *p_params = NULL;

    RETURN_IF_NOT_INITIALIZED(card);

    p_params = flashInfo[card].params.flash_params;

    // read a page of flash at the start of the recovery bitstream address, but only if the
    // recovery_compare_len is strictly greater than 0
    if ( ( 0 == status ) && ( p_params->recovery_compare_len > 0 ) )
    {
        status = flash_read(card, p_params->recovery_addr, buf, FLASH_PAGE_MAX_SIZE);
    }

    if ( 0 == status )
    {
        // search for the golden compare
        if ( p_params->recovery_compare_len == 0 )
        {
            /* set p_sync_cmp to something non-NULL to pass the check below */
            p_sync_cmp = buf;
            debug_print("Skipping golden FPGA bitstream pattern checking on card %" PRIu8 "\n",
                        card);
        }
        else if( 0 == memcmp( buf + p_params->recovery_compare_offset,
                              p_params->recovery_compare_pattern,
                              p_params->recovery_compare_len ) )
        {
            // search for the sync word
            p_sync_cmp = memmem(buf, FLASH_PAGE_MAX_SIZE, fpga_sync_word, FPGA_SYNC_WORD_LEN);

            /* specifically check the case where golden pattern and sync word
             * pattern were found, part is mPCIe, and a non-genuine golden image
             * check passes, then warn the user, but don't disable access. */
            if ( ( p_sync_cmp != NULL ) &&
                 ( flashInfo[card].params.skiq_part == skiq_mpcie ) &&
                 ( buf[0x9D] != 0x12 ) )
            {
                skiq_warning("Golden FPGA bitstream needs update on card %" PRIu8 ", please run"
                    " hardware updater\n", card);
            }
        }

        if (NULL != p_present)
        {
            *p_present = (NULL != p_sync_cmp) ? true : false;
        }
    }

    return (status);
}


/*****************************************************************************/
/**
    @brief Reads data from the flash chip.
*/
int32_t flash_read(uint8_t card, uint32_t addr, uint8_t* p_buf, uint32_t len)
{
    int32_t result = 0;
    flash_card_information_t *p_flash = NULL;
    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;
    uint8_t page[FLASH_PAGE_MAX_SIZE] = { 0 };
    uint32_t n = 0, residual;
    uint32_t page_size = 0;

    if (NULL == p_buf)
    {
        return -EINVAL;
    }

    RETURN_IF_NOT_INITIALIZED(card);

    p_flash = &(flashInfo[card]);
    page_size = p_flash->page_size;

    int32_t (*read_page)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->read_page;

    if (NULL == read_page)
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        result = -ENOTSUP;
    }
    else if( (addr + len) > p_flash->total_size )
    {
        skiq_error("Flash read would exceed flash addressing boundaries (" ADDR_SPECIFIER " > "
            ADDR_SPECIFIER ") for card %" PRIu8 "\n", (addr + len), p_flash->total_size, card);
        result = -EFBIG;
    }
    else if ( page_size == 0 )
    {
        skiq_error("Flash page size is zero, expected non-zero for card %" PRIu8 "\n", card);
        result = -ENOTSUP;
    }

    if ( result == 0 )
    {
        /* if address is not on a page boundary, make a special case out of it, thanks a lot USB. */
        residual = addr % page_size;
        if ( residual > 0 )
        {
            /* read from the beginning of the page of either residual+len or a full page */
            transfer.addr = addr - residual;
            transfer.buf = page;
            transfer.len = MIN(residual + len, page_size);

            result = read_page(card, &transfer, p_flash);
            if( 0 != result )
            {
                skiq_error("Failed to read flash page at address " ADDR_SPECIFIER " on card %" PRIu8
                           " (status %" PRIi32 ")\n", transfer.addr, card, result);
            }
            else
            {
                /* copy the page contents starting at the residual offset to the end of the page */
                memcpy( p_buf, &(page[residual]), MIN(len, page_size - residual) );
                len -= MIN(len, page_size - residual);
                addr += (page_size - residual);
            }
        }
    }

    /* read the remainder out page-by-page */
    for( n = 0; (n < len) && (0 == result); n += page_size )
    {
        /* read one page of flash at a time until we have everything */
        transfer.addr = addr + n;
        transfer.buf = p_buf + n;
        transfer.len = MIN(len - n, page_size);

        result = read_page(card, &transfer, p_flash);
        if( 0 != result )
        {
            skiq_error("Failed to read flash page at address " ADDR_SPECIFIER " on card %" PRIu8
                " (status %" PRIi32 ")\n", transfer.addr, card, result);
        }
    }

    return result;
}

/*****************************************************************************/
/**
    @brief Writes data to the flash chip (without erasing it first).
*/
static int32_t flash_write_no_erase(uint8_t card, uint32_t addr, uint8_t* p_buf, uint32_t len)
{
    int32_t result = 0;
    flash_card_information_t *p_flash = NULL;
    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;
    uint32_t n = 0;
    uint32_t page_size = 0;
    uint32_t residual = 0;

    if (NULL == p_buf)
    {
        return -EINVAL;
    }

    RETURN_IF_NOT_INITIALIZED(card);

    p_flash = &(flashInfo[card]);
    page_size = p_flash->page_size;

    int32_t (*read_page)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->read_page;
    int32_t (*write_page)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->write_page;

    if( NULL == write_page )
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        result = -ENOTSUP;
    }
    else if( (addr + len) > p_flash->total_size )
    {
        skiq_error("Flash write would exceed flash addressing boundaries (" ADDR_SPECIFIER " > "
            ADDR_SPECIFIER ") for card %" PRIu8 "\n", (addr + len), p_flash->total_size, card);
        result = -EFBIG;
    }
    else if ( page_size == 0 )
    {
        skiq_error("Flash page size is zero, expected non-zero for card %" PRIu8 "\n", card);
        result = -ENOTSUP;
    }
    else if ( p_flash->sector_size == 0 )
    {
        skiq_error("Flash sector size is zero, expected non-zero for card %" PRIu8 "\n", card);
        result = -ENOTSUP;
    }

    if ( result == 0 )
    {
        /* if address is not on a page boundary, make a special case out of it since writing to
         * flash happens page-by-page */
        residual = addr % page_size;
        if ( residual > 0 )
        {
            uint8_t page[FLASH_PAGE_MAX_SIZE];

            /* read the full page, then memcpy the "new" contents into an offset (residual) into the
             * page, then write-back the full page */
            transfer.addr = addr - residual;
            transfer.buf = page;
            transfer.len = page_size;

            result = read_page(card, &transfer, p_flash);
            if ( result == 0 )
            {
                memcpy( &(page[residual]), p_buf, MIN(len, page_size - residual) );

                skiq_debug("Writing to flash sector starting at address " ADDR_SPECIFIER
                           " on card %" PRIu8 "\n", transfer.addr, card);
                result = write_page(card, &transfer, p_flash);
            }

            if ( result == 0 )
            {
                len -= MIN(len, page_size - residual);
                addr += (page_size - residual);
            }
        }
    }

    for( n = 0; (n < len) && (0 == result); n+= page_size )
    {
        transfer.addr = addr + n;
        transfer.buf = p_buf + n;
        transfer.len = MIN(len - n, page_size);

        // status info displayed per sector
        if( (transfer.addr % p_flash->sector_size) == 0 )
        {
            skiq_debug("Writing to flash sector starting at address " ADDR_SPECIFIER " on card %"
                       PRIu8 "\n", addr, card);
        }

        result = write_page(card, &transfer, p_flash);
        if( 0 != result )
        {
            skiq_error( "Failed write to flash page at address " ADDR_SPECIFIER " on card %" PRIu8
                        ", status %d\n", addr + n, card, result);
        }
    }

    return result;
}

/*****************************************************************************/
/**
    @brief Writes data to the flash chip.
*/
int32_t flash_write(uint8_t card, uint32_t addr, uint8_t* p_buf, uint32_t len)
{
    int32_t result = 0;
    flash_card_information_t *p_flash = NULL;
    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;
    uint32_t m = 0;
    uint32_t n = 0;

    if (NULL == p_buf)
    {
        return -EINVAL;
    }

    RETURN_IF_NOT_INITIALIZED(card);

    p_flash = &(flashInfo[card]);

    int32_t (*erase_sector)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->erase_sector;

    if( NULL == erase_sector )
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        result = -ENOTSUP;
    }
    else if( (addr + len) > p_flash->total_size )
    {
        skiq_error("Flash write would exceed flash addressing boundaries (" ADDR_SPECIFIER " > "
            ADDR_SPECIFIER ") for card %" PRIu8 "\n", (addr + len), p_flash->total_size, card);
        result = -EFBIG;
    }

    // "m" holds the byte length of erased sectors
    m = DIV_ROUND_UP(len, p_flash->sector_size);
    m *= p_flash->sector_size;
    for( n = 0; (n < m) && (0 == result); n += p_flash->sector_size )
    {
        transfer.addr = addr + n;
        transfer.buf = NULL;
        transfer.len = 0;

        skiq_debug("Erasing flash sector at address " ADDR_SPECIFIER " on card %" PRIu8 "\n",
            transfer.addr, card);
        result = erase_sector(card, &transfer, p_flash);
        if( 0 != result )
        {
            skiq_error( "Failed erase of flash sector at address " ADDR_SPECIFIER " on card %" PRIu8
                " (status %" PRIi32 ")\n", transfer.addr, card, result);
        }
    }

    if ( 0 == result )
    {
        result = flash_write_no_erase( card, addr, p_buf, len );
    }

    return result;
}

/*****************************************************************************/
/** @brief Writes the contents of a file to flash.

    @param card         requested Sidekiq card ID
    @param addr         address in flash to write to
    @param p_file       file to write to flash
    @return int32_t     status where 0=success, anything else is an error
*/
int32_t flash_write_file(uint8_t card, uint32_t addr, FILE* p_file)
{
    if (NULL == p_file)
    {
        return -EINVAL;
    }

    RETURN_IF_NOT_INITIALIZED(card);

    return _write_file(card, addr, p_file, false);
}


/*****************************************************************************/
/**
    @brief  Writes the contents of a FPGA configuration file to flash. This function takes extra
            precautions in comparison to ::flash_write_file by writing the FPGA sync word as the
            very last action. This prevents partial writes of FPGA configurations from appearing
            to be valid due to the appearance of the sync word.
*/
int32_t flash_write_file_fpga(uint8_t card, uint32_t addr, FILE* p_file)
{
    int32_t status = 0;
    uint8_t sync[FPGA_SYNC_WORD_LEN] = {0};
    int32_t tmp = 0;
    int32_t cmp = -1;
    uint32_t m = 0;
    uint32_t n = 0;

    if (NULL == p_file)
    {
        return -EINVAL;
    }

    RETURN_IF_NOT_INITIALIZED(card);

    // Search the first 1024 bytes of the file to ensure the sync word is
    // present before accessing flash memory. Note, this is not the most
    // efficient method, but it saves memory since there is a possiblity that
    // we could have multiple large buffers pushed onto the stack the further
    // down we go in this code.
    for( n = 0; (n < 1024) && (cmp != 0) && (tmp != EOF); n++ )
    {
        tmp = fgetc(p_file);
        if( EOF != tmp )
        {
            sync[FPGA_SYNC_WORD_LEN - 1] = (uint8_t) tmp;
            cmp = memcmp(sync, fpga_sync_word, FPGA_SYNC_WORD_LEN);
            // shift the data down
            for( m = 0; m < (FPGA_SYNC_WORD_LEN-1); m++ )
            {
                sync[m] = sync[m + 1];
            }
        }
    }

    if( 0 != cmp )
    {
        skiq_error("Specified FPGA bitstream file does not contain sync word for card %" PRIu8 "\n", card);
        status = -EFAULT;
    }
    else
    {
        rewind(p_file);
        status = _write_file(card, addr, p_file, true);
    }

    return status;
}


static bool same_sector( uint32_t addr1,
                         uint32_t addr2,
                         uint32_t sector_size )
{
    bool same = false;

    debug_print("addr1: " ADDR_SPECIFIER " --> %u, addr2: " ADDR_SPECIFIER " --> %u, sector_size: "
                "%u\n", addr1, addr1 / sector_size, addr2, addr2 / sector_size, sector_size);

    if ( sector_size > 0 )
    {
        /* NOTE: this assumes that a flash's sectors are all the same size for a given flash
         * device */
        if ( ( addr1 / sector_size ) == ( addr2 / sector_size ) )
        {
            same = true;
        }
    }
    else
    {
        skiq_error("Internal Error: sector_size is zero\n");
    }

    return same;
}


static int32_t get_sector_size( uint8_t card,
                                uint32_t *p_sector_size )
{
    flash_card_information_t *p_flash = NULL;

    RETURN_IF_NOT_INITIALIZED(card);

    p_flash = &(flashInfo[card]);

    *p_sector_size = p_flash->sector_size;

    return 0;
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_write_file_fpga_with_metadata( uint8_t card,
                                             uint32_t addr,
                                             FILE* p_file,
                                             uint32_t metadata_addr,
                                             uint64_t metadata )
{
    int32_t status = 0;
    uint32_t sector_size;
    long file_size;

    CHECK_NULL_PARAM( p_file );

    file_size = hal_file_stream_size( p_file );
    if ( file_size < 0 )
    {
        status = -EIO;
    }

    if ( status == 0 )
    {
        status = get_sector_size( card, &sector_size );
    }

    if ( status == 0 )
    {
        status = flash_write_file_fpga( card, addr, p_file );
    }

    if ( status == 0 )
    {
        /* store bitstream metadata as little endian */
        metadata = htole64(metadata);

        debug_print("Writing metadata 0x%016" PRIx64 " to " ADDR_SPECIFIER "\n", metadata,
                    metadata_addr);

        /* if the metadata and the last byte of the bitstream are stored in the same underlying
         * flash sector, do not erase flash before writing. */
        if ( same_sector( addr + (uint32_t)file_size - 1, metadata_addr, sector_size ) )
        {
            status = flash_write_no_erase( card, metadata_addr, (uint8_t *)(&metadata),
                                           sizeof(metadata) );
        }
        else
        {
            status = flash_write( card, metadata_addr, (uint8_t *)(&metadata), sizeof(metadata) );
        }
    }

    return status;
}


/*****************************************************************************/
/**
    @brief Verifies the contents of a file against that found in flash.
*/
int32_t flash_verify_file(uint8_t card, uint32_t addr, FILE* p_file)
{
    flash_card_information_t *p_flash = NULL;
    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;
    uint8_t fbuf[FLASH_PAGE_MAX_SIZE] = {0};
    uint8_t rbuf[FLASH_PAGE_MAX_SIZE] = {0};
    uint32_t page_size = 0;
    uint32_t sector_size = 0;
    int32_t status = 0;
    int64_t size = 0;
    uint32_t m = 0;
    uint32_t n = 0;
    int r = 0;

    if (NULL == p_file)
    {
        return -EINVAL;
    }

    RETURN_IF_NOT_INITIALIZED(card);

    p_flash = &(flashInfo[card]);

    int32_t (*read_page)(uint8_t, flash_transfer_t*, flash_card_information_t * const) = \
        p_flash->p_functions->read_page;

    ///////////////////////////////////////////////////////////////////////////
    // 1. Check Input
    if( NULL == read_page )
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        return -ENOTSUP;
    }

    page_size = p_flash->page_size;
    sector_size = p_flash->sector_size;

    errno = 0;
    r = fseek(p_file, 0, SEEK_END);
    if( 0 != r )
    {
        skiq_error("Failed to seek to end of specified file for card %" PRIu8 " (%d: '%s')\n",
            card, errno, strerror(errno));
    }
    errno = 0;
    size = ftell(p_file);
    r = errno;
    rewind(p_file);

    if( size < 0 )
    {
        skiq_error("Failed to determine size of specified file for card %" PRIu8 " (%d: '%s')\n",
            card, r, strerror(r));
        return -EIO;
    }
    else if( (uint32_t) size > p_flash->total_size )
    {
        skiq_error("Size of specified file exceeds flash capacity for card %" PRIu8 "\n", card);
        return -EFBIG;
    }
    else if ( sector_size == 0 )
    {
        skiq_error("Flash sector size is zero, expected non-zero for card %" PRIu8 "\n", card);
        return -ENOTSUP;
    }

    ///////////////////////////////////////////////////////////////////////////
    // 2. Read and Compare
    transfer.buf = rbuf;
    while( (size) && (0 == status) )
    {
        // status info displayed per sector
        if( (addr % sector_size) == 0 )
        {
            skiq_debug("Verifying flash sector " ADDR_SPECIFIER " on card %" PRIu8 "\n", addr, card);
        }

        n = (size > page_size) ? page_size : size;

        transfer.addr = addr;
        transfer.len = n;

        status = read_page(card, &transfer, p_flash);
        if( 0 != status )
        {
            skiq_error("Failed read of flash page at address " ADDR_SPECIFIER " on card %" PRIu8
                " (status %" PRIi32 ")\n", addr, card, status);
        }
        else if( n != fread(fbuf, sizeof(uint8_t), n, p_file) )
        {
            skiq_error("Failed to read from specified file for card %" PRIu8 " (status %" PRIi32
                ")\n", card, status);
            status = -EIO;
        }
        else if( 0 != memcmp(fbuf, rbuf, n) )
        {
            // There's an error in here somewhere, lets find it!
            for( m = 0; m < n; m++ )
            {
                if( fbuf[m] != rbuf[m] )
                {
                    skiq_error("Failed flash verification at offset %d (" ADDR_SPECIFIER "), file "
                               "byte 0x%02x does not match flash byte 0x%02x\n", addr + m, addr + m,
                               fbuf[m], rbuf[m]);
                    status = -EXDEV;
                }
            }
        }

        addr += page_size;
        size -= n;
    }

    return status;
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_verify_file_with_metadata( uint8_t card,
                                         uint32_t addr,
                                         FILE* p_file,
                                         uint32_t metadata_addr,
                                         uint64_t metadata )
{
    int32_t status = 0;

    ///////////////////////////////////////////////////////////////////////////
    // Verify bitstream
    status = flash_verify_file( card, addr, p_file );

    ///////////////////////////////////////////////////////////////////////////
    // Verify metadata
    if ( status == 0 )
    {
        uint64_t stored_metadata = 0;

        status = flash_read( card, metadata_addr, (uint8_t *)&stored_metadata,
                             sizeof(stored_metadata) );
        if ( status == 0 )
        {
            debug_print("stored metadata: 0x%016" PRIX64 ", metadata: 0x%016" PRIX64 "\n",
                        stored_metadata, metadata);

            /* metadata is stored little endian */
            stored_metadata = le64toh(stored_metadata);
            if ( stored_metadata != metadata )
            {
                status = -EXDEV;
            }
        }
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief  Loads the FPGA with the contents stored in flash memory from the default (user) address.
*/
int32_t flash_load_fpga_default(uint8_t card)
{
    int32_t status = 0;
    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;

    RETURN_IF_NOT_INITIALIZED(card);

    if( NULL == flashInfo[card].p_functions->load_fpga )
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        status = -ENOTSUP;
    }

    if ( 0 == status )
    {
        transfer.addr = flashInfo[card].params.flash_params->user_addr;
        status = flashInfo[card].p_functions->load_fpga(card, &transfer, &(flashInfo[card]));
    }

    /* Re-initialize the Flash layer just to ensure that the config is valid */
    if ( 0 == status )
    {
        status = reload_layer_after_reprogram(card);
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief  Loads the FPGA with the contents stored in flash memory from a specified non-volatile
            memory address.
*/
int32_t flash_load_fpga_by_address(uint8_t card, uint32_t load_address)
{
    int32_t status = 0;
    flash_transfer_t transfer = FLASH_TRANSFER_INITIALIZER;

    RETURN_IF_NOT_INITIALIZED(card);

    if( NULL == flashInfo[card].p_functions->load_fpga )
    {
        skiq_error("Flash access not supported for card %" PRIu8 "\n", card);
        status = -ENOTSUP;
    }

    if ( 0 == status )
    {
        debug_print("Configuring FPGA on card %u from flash address " ADDR_SPECIFIER "\n", card,
                    load_address);
        transfer.addr = load_address;
        status = flashInfo[card].p_functions->load_fpga(card, &transfer, &(flashInfo[card]));
    }

    /* Re-initialize the Flash layer just to ensure that the config is valid */
    if ( 0 == status )
    {
        status = reload_layer_after_reprogram(card);
    }

    return status;
}


/*****************************************************************************/
/**
    @brief Queries the flash lock status.
*/
int32_t flash_is_golden_locked( uint8_t card, bool *p_is_locked )
{
    int32_t status = 0;

    RETURN_IF_NOT_INITIALIZED(card);

    if ( NULL == flashInfo[card].p_functions->is_golden_locked )
    {
        skiq_error("Querying flash lock status not supported for card %" PRIu8 "\n", card);
        status = -ENOTSUP;
    }

    if ( 0 == status )
    {
        status = flashInfo[card].p_functions->is_golden_locked( card, &(flashInfo[card]),
                    p_is_locked );
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief  Locks the flash sectors associated with the golden FPGA bitstream, only on Sidekiq
            parts that support it.
*/
int32_t flash_lock_golden_sectors( uint8_t card )
{
    int32_t status = 0;

    RETURN_IF_NOT_INITIALIZED(card);

    if ( NULL == flashInfo[card].p_functions->lock_golden_sectors )
    {
        skiq_error("Locking flash sectors is not supported for card %" PRIu8 "\n", card);
        status = -ENOTSUP;
    }

    if ( 0 == status )
    {
        status = flashInfo[card].p_functions->lock_golden_sectors( card, &(flashInfo[card]) );
    }

    return status;
}


/**************************************************************************************************/
/**
    @brief  Unlocks the flash sectors associated with the golden FPGA bitstream, only on Sidekiq
            parts that support it.
*/
int32_t flash_unlock_golden_sectors( uint8_t card )
{
    int32_t status = 0;

    RETURN_IF_NOT_INITIALIZED(card);

    if ( NULL == flashInfo[card].p_functions->unlock_golden_sectors )
    {
        skiq_error("Unlocking flash sectors is not supported for card %" PRIu8 "\n", card);
        status = -ENOTSUP;
    }

    if ( 0 == status )
    {
        status = flashInfo[card].p_functions->unlock_golden_sectors( card, &(flashInfo[card]) );
    }

    return status;
}



/*****************************************************************************/
/**
    @brief  Gets information about the Flash chip on the specified Sidekiq card
*/
int32_t flash_get_information( uint8_t card, flash_part_information_t *p_part )
{
    int32_t status = 0;
    flash_card_information_t *p_flash = NULL;

    RETURN_IF_NOT_INITIALIZED(card);

    if (NULL == p_part)
    {
        /* The user isn't interested in the information, so just return success. */
        return 0;
    }

    p_flash = &(flashInfo[card]);

    if (NULL != p_part)
    {
        *p_part = p_flash->part_id;
    }

    return status;
}


/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
#if (defined ATE_SUPPORT)

/**
    @brief  Retrieve the Flash chip's manufacturing unique ID
*/
int32_t flash_get_unique_id( uint8_t card, uint8_t *p_id, uint8_t *p_id_length )
{
    int32_t status = 0;

    RETURN_IF_NOT_INITIALIZED(card);

    if (NULL == p_id)
    {
        /* The user isn't interested in the information, so just return success. */
        if (NULL != p_id_length)
        {
            *p_id_length = 0;
        }

        return 0;
    }

    if ( NULL == flashInfo[card].p_functions->get_unique_id )
    {
        skiq_error("Getting Flash unique ID is not supported for card %" PRIu8 "\n", card);
        status = -ENOTSUP;
    }

    if ( 0 == status )
    {
        status = flashInfo[card].p_functions->get_unique_id( card, &(flashInfo[card]), p_id,
                    p_id_length );
    }

    return status;
}

#else   /* (defined ATE_SUPPORT) */

int32_t flash_get_unique_id( uint8_t card, uint8_t *p_id, uint8_t *p_id_length )
{
    (void) card;
    (void) p_id;
    (void) p_id_length;

    return -ENOTSUP;
}

#endif /* ATE_SUPPORT */
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */

