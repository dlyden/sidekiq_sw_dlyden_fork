/*! \file fpga_reg_flash.c
 * \brief This file contains the implementation for interfacing the on board
 * flash through the fpga.
 *
 * <pre>
 * Copyright 2015 - 2021 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 04/29/2015   MJR   Created file
 * 05/24/2017   MJR   Revision and clean up for flash abstraction
 * 06/05/2017   JBB   Added support for X2
 *
 * </pre>
 */

#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include "fpga_reg_flash.h"
#include "sidekiq_xport.h"
#include "sidekiq_hal.h"
#include "flash_defines.h"
#include "bit_ops.h"

#include "hal_delay.h"
#include "hal_time.h"

/* enable debug_print and debug_print_plain when DEBUG_FPGA_FLASH is defined */
#if (defined DEBUG_FPGA_FLASH)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

// 120 seconds is the longest any flash command should take (bulk erase)
#define FLASH_DONE_BIT_TIMEOUT_NS       (uint64_t)(130 * (uint64_t)SEC)

// 5 seconds is more than enough time to finish a SPI transaction to flash
#define FPGA_SPI_DONE_BIT_TIMEOUT_NS    (uint64_t)(5 * (uint64_t)SEC)

#define PCI_RESCAN_DELAY_SECONDS        (15)

// bits used to signal control and commands to the FX2
#define FPGA_FX2_CMD_BIT_0              (BIT9)
#define FPGA_FX2_CMD_BIT_1              (BIT10)

#define CMD_DEFAULT                     (0)
#define CMD_FPGA_FLASH_SPI_MASTER       (1)
#define CMD_FPGA_LOAD_FROM_FLASH        (2)

#define FLASH_CMD_MAX_SIZE              (5) /* 1 command byte + 4 address bytes */

/* Time delay to allow the FPGA to reload using the PCI only method.  Value
 * depends on whether self reload is available */
#define PCI_FPGA_DELAY_TO_FIRST_RESCAN_SEC              (p_params->pci_fpga_delay_to_first_rescan_sec)
#define PCI_FPGA_DELAY_TO_FIRST_RESCAN_SEC_SELF_RELOAD  (p_params->pci_fpga_delay_to_first_rescan_sec_self_reload)
#define PCI_FPGA_MAX_RESCAN_PERIOD_SEC                  (p_params->pci_fpga_max_rescan_period_sec)

// *** Micron N25 / MT25Q specific commands and flags
// The Micron N25Q032A11EF640E data sheet can be found (at time of writing) at
// https://www.micron.com/-/media/client/global/documents/products/data-sheet/nor-flash/serial-nor/n25q/n25q_32mb_1_8v_65nm.pdf
#define FLASH_CMD_MICRON_N25_CLEAR_FLAG_REG (FLASH_CMD_CLEAR_FLAG_REG)
#define FLASH_CMD_MICRON_N25_READ_FLAG_REG  (FLASH_CMD_READ_FLAG_REG)
#define FLASH_MASK_MICRON_N25_FLAGSTATUS_BUSY   (FLASH_CMD_READ_FLAG_BUSY_MASK)

/*
    Per page 35 of the N25 datasheet, the "READ ID" command returns 20 bytes:
        1B  - JEDEC Manufacturer ID
        2B  - Manufacturer Specific ID (Memory Type and Memory Capacity)
        17B - Unique ID
              1B    - Length of data to follow
              2B    - Extended device ID
              14B   - Factory unique ID
*/
/* The total number of bytes to read for the unique ID. */
#define FLASH_LEN_MICRON_N25_READ_ID_TOTAL      (20)
/* The offset into the read buffer where the unique ID starts. */
#define FLASH_LEN_MICRON_N25_READ_ID_OFFSET     (6)
/* The length (in bytes) of the unique ID. */
#define FLASH_LEN_MICRON_N25_READ_ID_LEN        (14)


// *** Macronix MX25 specific commands and flags
// The Macronix MX25U12835F data sheet can be found (at time of writing) at
// https://www.macronix.com/Lists/Datasheet/Attachments/7656/MX25U12835F,%201.8V,%20128Mb,%20v1.9.pdf

/* The total number of bytes to read for the unique ID (see pg. 11 of the datasheet). */
#define FLASH_LEN_MACRONIX_MX25_READ_ID_TOTAL   (16)


// *** Winbond W25 specific commands and flags
// The Winbond W25Q128FW data sheet can be found (at time of writing) at
// https://www.mouser.com/datasheet/2/949/w25q128fw_revk_05182017-1489721.pdf

/* The total number of bytes to read for the unique ID (see pg. 65 of the datasheet). */
#define FLASH_LEN_WINBOND_W25_READ_ID_TOTAL         (8)

/*
    The total number of dummy bytes needed for the "Read Unique ID" command (see pg. 65 of the
    datasheet)
*/
#define FLASH_LEN_WINBOND_W25_READ_ID_DUMMY_BYTES   (4)

#define FLASH_CMD_WINBOND_W25_READ_STATUS_REG_2     (0x35)
#define FLASH_CMD_WINBOND_W25_READ_STATUS_REG_3     (0x15)


// *** ISSI IS25 specific commands and flags
// The ISSI IS25WP032 data sheet can be found (at time of writing) at
// https://www.mouser.com/datasheet/2/198/IS25WP032-064-128-737458.pdf

/* The total number of bytes to read for the unique ID (see pg. 69 of the datasheet). */
#define FLASH_LEN_ISSI_IS25_READ_ID_TOTAL           (16)

/*
    The total number of dummy bytes needed for the "Read Unique ID" command; this is a 3 byte
    address (which should be zeros) followed by a dummy cycle (see pg. 69 of the datasheet).
*/
#define FLASH_LEN_ISSI_IS25_READ_ID_DUMMY_BYTES     (4)

#define FLASH_CMD_ISSI_IS25_READ_RXT_READ_REG       (0x81)

/***** definitions for the Flash IC on X2 *****/

/* flash commands for 4 byte addressing */
#define FLASH_CMD_SECTOR_ERASE_4BYTE       (0xDC)
#define FLASH_CMD_PAGE_PROGRAM_4BYTE       (0x12)
#define FLASH_CMD_QUAD_PAGE_PROGRAM_4BYTE  (0x34)
#define FLASH_CMD_PAGE_READ_4BYTE          (0x13)
#define FLASH_CMD_QUAD_PAGE_READ_4BYTE     (0x6C)
#define FLASH_CMD_WRITE_REGISTERS          (0x01)

// *** Spansion S25 specific commands and flags.
// The Spansion S25FL512S data sheet can be found (at time of writing) at
//    https://www.cypress.com/file/177971/download
// Write Registers - writes both the Status Register and Configuration Register 1
#define FLASH_CMD_SPANSION_S25_WRITE_REGISTERS          (0x01)
// Read Configuration Register 1 (CR1)
#define FLASH_CMD_SPANSION_S25_READ_CONFIG_REG_1        (0x35)
// The bit to set in the status register to set the write latch
#define FLASH_BIT_SPANSION_S25_SR1_WRITE_LATCH_ENABLE   (1 << 1)
// The bit to set in Configuration Register 1 to enable Quad I/O operation (see page 49 of the
// above listed data sheet)
#define FLASH_BIT_SPANSION_S25_CR1_QUAD_OPERATION_BIT   (1 << 1)
#define FLASH_CMD_SPANSION_S25_READ_STATUS_REG_2    (0x07)
#define FLASH_CMD_SPANSION_S25_OTP_READ             (0x4B)

/* The total length (in bytes) of the unique ID (see p. 45 of the datasheet) */
#define FLASH_LEN_SPANSION_S25_READ_ID_TOTAL        (16)

#define CR_QUAD_OPERATION_BIT              (1 << 1)
#define SR_WRITE_LATCH_ENABLE_BIT          (1 << 1)


/***** Flash protection bits for Micron used on Sidekiq mPCIe *****/
/* PDF: 09005aef84566617
   n25q_32mb_1_8v_65nm.pdf - Rev. F 01/13 EN

   Fortunately, the protection bits are forwards compatible, the identification mask then doesn't
   care about capacity identification, just manufacturer and memory type

   MICRON_FLASH_BLOCK_* definitions derived from page 20, table 9: Status Register Bit Definitions,
   specifically bits 2 through 5 in conjunction with table 6 (page 15): Protected Area Sizes - Lower
   Area
 */
#define MICRON_FLASH_BLOCK_PROTECT_VALUE          0x38 /* Bottom=1, BP2=1, BP1=1, BP0=0 */
#define MICRON_FLASH_BLOCK_UNPROTECT_VALUE        0x00 /* Bottom=0, BP2=0, BP1=0, BP0=0 */

/***** MACROS *****/

// macros for setting out the command bytes and address for read & write
#define FLASH_CMD_PREPARE(buffer, len, cmd, addr)        \
    do {                                                 \
        len = 0;                                         \
        buffer[len++] = cmd;                             \
        buffer[len++] = (addr >> 16) & 0xFF;             \
        buffer[len++] = (addr >>  8) & 0xFF;             \
        buffer[len++] = (addr >>  0) & 0xFF;             \
    } while( 0 )

#define FLASH_CMD_PREPARE_4BYTE(buffer, len, cmd, addr)  \
    do {                                                 \
        len = 0;                                         \
        buffer[len++] = cmd;                             \
        buffer[len++] = (addr >> 24) & 0xFF;             \
        buffer[len++] = (addr >> 16) & 0xFF;             \
        buffer[len++] = (addr >>  8) & 0xFF;             \
        buffer[len++] = (addr >>  0) & 0xFF;             \
    } while( 0 )

#define ROUND_UP(_numerator, _denominator)     (_ROUND_UP((_numerator),(_denominator)))
#define _ROUND_UP(_numerator, _denominator)    (_numerator + (_denominator - 1)) / _denominator

/* Close an open Flash interface */
#define CLOSE_INTERFACE(card, p_params, status)                                                    \
    do                                                                                             \
    {                                                                                              \
        int32_t free_status = 0;                                                                   \
        free_status = _free_flash_interface((card), (p_params));                                   \
        if ( 0 != free_status )                                                                    \
        {                                                                                          \
            skiq_error("Failed to free flash interface on card %" PRIu8 " (status %" PRIi32        \
                ")\n", (card), free_status);                                                       \
            if ( 0 == (status) )                                                                   \
            {                                                                                      \
                (status) = -EIO;                                                                   \
            }                                                                                      \
        }                                                                                          \
    } while ( 0 )

/***** TYPE DEFINITIONS *****/

typedef struct
{
    uint32_t fpga_min_version;
    uint32_t fw_min_version;

} version_req_t;


/***** LOCAL VARIABLES *****/

static version_req_t mpcie_versions = {
    .fpga_min_version = FPGA_VERSION(1,6,0),
    .fw_min_version =   FW_VERSION(1,14),
};

static version_req_t m2_versions = {
    .fpga_min_version = FPGA_VERSION(3,6,0),
    .fw_min_version =   FW_VERSION(2,6),
};

static version_req_t x2_versions = {
    .fpga_min_version = FPGA_VERSION(3,4,0),
    .fw_min_version =   0,
};

static version_req_t x4_versions = {
    .fpga_min_version = FPGA_VERSION(3,10,0),
    .fw_min_version =   0,
};

static version_req_t m2_2280_versions = {
    .fpga_min_version = FPGA_VERSION(3,12,0),
    .fw_min_version =   0,
};

static version_req_t all_versions_supported = {
    .fpga_min_version = 0,
    .fw_min_version =   0,
};


/***** LOCAL FUNCTIONS *****/

#if (defined DEBUG_FPGA_FLASH)
static int32_t
display_status_registers ( uint8_t card, flash_card_information_t * const p_card_info );
#endif


/**
    @brief  Verify that the running FPGA meets the required version for Flash access

    @param[in]  p_card_info     Flash information about the selected card

    @return true if the FPGA version is valid for Flash access, else false
*/
static bool _is_flash_supported(flash_card_information_t * const p_card_info)
{
    const uint32_t fpga_golden = FPGA_VERS_GOLDEN;
    version_req_t required_versions;
    bool supported = false;

    switch ( p_card_info->params.skiq_part )
    {
        case skiq_mpcie:
            required_versions = mpcie_versions;
            break;

        case skiq_m2:
            required_versions = m2_versions;
            break;

        case skiq_x2:
            required_versions = x2_versions;
            break;

        case skiq_x4:
            required_versions = x4_versions;
            break;

        case skiq_m2_2280:
            required_versions = m2_2280_versions;
            break;

        case skiq_nv100:
            required_versions = all_versions_supported;
            break;

        default:
            // all other parts aren't supported at the moment
            skiq_warning("Unsupported Sidekiq part\n");
            return false;
    }

    if( (fpga_golden == (p_card_info->fpga_ver)) ||
        (required_versions.fpga_min_version <= (p_card_info->fpga_ver)) )
    {
        if( required_versions.fw_min_version <= (p_card_info->fw_ver) )
        {
            // meets FPGA and firmware requirements
            supported = true;
        }
        else
        {
            skiq_warning("Flash access over FPGA registers not supported, firmware version v%u.%u "
                         "or later required\n", FW_VERSION_MAJOR(required_versions.fw_min_version),
                         FW_VERSION_MINOR(required_versions.fw_min_version));
        }
    }
    else
    {
        if ( p_card_info->fpga_ver < FPGA_VERSION(3,8,0) )
        {
            skiq_warning("Flash access over FPGA registers not supported, FPGA version v%u.%u or "
                         "later required\n", FPGA_VERSION_MAJOR(required_versions.fpga_min_version),
                         FPGA_VERSION_MINOR(required_versions.fpga_min_version));
        }
        else
        {
            skiq_warning("Flash access over FPGA registers not supported, FPGA bitstream version "
                         "v%u.%u.%u or later required\n",
                         FPGA_VERSION_MAJOR(required_versions.fpga_min_version),
                         FPGA_VERSION_MINOR(required_versions.fpga_min_version),
                         FPGA_VERSION_PATCH(required_versions.fpga_min_version));
        }
    }

    return supported;
}

/*****************************************************************************/
/** The _init_fx2_interface function sets up the GPIF pins on the FPGA to
    outputs such that they can be utilized to communicate to the FX2 in cases
    where the USB interface is unavailable. Note that pullups should be enabled
    on the GPIF pins 9 through 15 on the FPGA by default to keep the FX2 in a
    passive state. As a further note, only GPIF pins 9 and 10 are currently in
    use for sending commands.

    @param card the sidekiq card to be selected

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _init_fx2_interface(uint8_t card)
{
    uint32_t read = 0;
    int32_t status = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIF_WRITE, &read);
    if( status != 0 )
    {
        return( status );
    }

    BIT_SET(read, (1 << FPGA_FX2_CMD_BIT_0) | (1 << FPGA_FX2_CMD_BIT_1));
    status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_WRITE, read);
    if( status != 0 )
    {
        return( status );
    }

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIF_TRISTATE, &read);
    if( status != 0 )
    {
        return( status );
    }
    // configure FPGA GPIF9 and GPIF10 as output
    BIT_SET(read, (1 << FPGA_FX2_CMD_BIT_0) | (1 << FPGA_FX2_CMD_BIT_1));
    status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_TRISTATE, read);

    return( status );
}

/*****************************************************************************/
/** The _free_fx2_interface function tristates the GPIF pins on the
    FPGA such that they can no longer be driven by the FPGA. The pullups on the
    GPIF pins 9 - 15 will return the FX2 back to its default state.

    @param card the sidekiq card to be selected

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _free_fx2_interface(uint8_t card)
{
    int32_t status;
    uint32_t read;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIF_TRISTATE, &read);
    if( status != 0 )
    {
        return( status );
    }
    // tristate FPGA GPIF9 and GPIF10
    BIT_CLEAR(read, (1 << FPGA_FX2_CMD_BIT_0) | (1 << FPGA_FX2_CMD_BIT_1));
    status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_TRISTATE, read);

    return( status );
}

/*****************************************************************************/
/** The _send_fx2_command function sets the state GPIF pins 9 and 10 such
    that the FPGA can issue commands to the FX2 in cases where USB support is
    unavailable. This is primarily needed to allow sharing of the flash pins
    without bus contention. Sidekiq mPCIe is unique though in that the FX2
    controls the "enable" state of flash through hardware.

    @param card the sidekiq card to be selected
    @param command the command to send to the FX2, can be either
           CMD_FPGA_FLASH_SPI_MASTER, CMD_FPGA_LOAD_FROM_FLASH, or
           CMD_DEFAULT

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _send_fx2_command(uint8_t card, uint8_t command)
{
    uint32_t read = 0;
    int32_t status = 0;

    // get the current state of the gpif pins
    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIF_WRITE, &read);
    if( status != 0 )
    {
        return( status );
    }

    switch( command )
    {
        case CMD_FPGA_FLASH_SPI_MASTER:
            // active low
            BIT_CLEAR(read, (1 << FPGA_FX2_CMD_BIT_1));
            break;

        case CMD_FPGA_LOAD_FROM_FLASH:
            // active low, have FPGA tell the FX2 to load bitstream from flash
            BIT_CLEAR(read, (1 << FPGA_FX2_CMD_BIT_0) |
                            (1 << FPGA_FX2_CMD_BIT_1));
            break;

        case CMD_DEFAULT:
            BIT_SET(read, (1 << FPGA_FX2_CMD_BIT_0) |
                          (1 << FPGA_FX2_CMD_BIT_1));
            break;

        default:
            status = -1;
            return( status );
    }
    // write the state of the gpif pins
    status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_WRITE, read);
    // brief delay to ensure that the FX2 has had a chance to see the pin change
    hal_nanosleep(1*MILLISEC);

    return( status );
}


/*****************************************************************************/
/**
    @brief  Wait for a SPI transaction to finish

    @param[in]  card        card number of the Sidekiq of interest
    @param[in]  p_params    pointer to flash access parameters; this should not be NULL

    @return int32_t  status where 0=success, anything else is an error
    @retval -EFAULT     if the start or stop time couldn't be read
    @retval -ETIMEDOUT  if the function timed out waiting for the transaction to finish
                        (::FPGA_SPI_DONE_BIT_TIMEOUT_NS nanoseconds)
*/
static int32_t _wait_fpga_spi_done(uint8_t card, flash_params_t * const p_params)
{
    int32_t status = 0;
    bool done = false;
    uint32_t flash_data = 0;
    uint64_t nowNs = 0;
    uint64_t endNs = 0;
    /**
        @brief  A flag to indicate if one last register check should be done after the deadline
                has been reached (or exceeded)
    */
    bool timeoutLastTime = false;

    status = hal_time_get_ns(hal_time_clock_type_monotonic, &nowNs);
    if (0 != status)
    {
        skiq_error("Failed to get start time on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        return (-EFAULT);
    }
    endNs = nowNs + FPGA_SPI_DONE_BIT_TIMEOUT_NS;

    while (!done)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_FLASH_SPI_STATUS, &flash_data);
        if (0 != status)
        {
            skiq_error("Failed to read SPI status register on card %" PRIu8 " (status %" PRIi32
                ")\n", card, status);
            done = true;
        }
        else if (0 != (flash_data & (1 << FLASH_SPI_DONE_OFFSET)) )
        {
            done = true;
        }
        else
        {
            if (timeoutLastTime)
            {
                /*
                    The "one last chance" timeout has failed - so now officially declare the
                    timeout.
                */
                status = -ETIMEDOUT;
                done = true;
            }
            else
            {
                hal_nanosleep(p_params->fpga_spi_transfer_delay_ns);

                status = hal_time_get_ns(hal_time_clock_type_monotonic, &nowNs);
                if (0 != status)
                {
                    skiq_error("Failed to get current time on card %" PRIu8 " (status %" PRIi32
                        ")\n", card, status);
                    done = true;
                    status = -EFAULT;
                }
                else
                {
                    if (endNs <= nowNs)
                    {
                        /*
                            If the deadline is upon us (or already past), try one more time to
                            check the register just to be safe - maybe the register changed
                            during the last sleep...
                        */
                        timeoutLastTime = true;
                    }
                }
            }
        }
    }

    return (status);
}


/*****************************************************************************/
/** The _clear_fpga_spi function is used to clear out any and all stale data
    in the fpga's read fifo.

    @param card card number of the Sidekiq of interest
    @param p_params pointer to flash access parameters

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _clear_fpga_spi(uint8_t card, flash_params_t * const p_params)
{
    const uint32_t mask = BF_MASK(FLASH_READ_FIFO_EMPTY_OFFSET,
                                  FLASH_READ_FIFO_EMPTY_LEN);
    uint32_t tmp = 0;
    uint32_t n = 0;
    int32_t r = 0;

    r = sidekiq_fpga_reg_read(card, FPGA_REG_FLASH_SPI_STATUS, &tmp);
    if( (0 == r) && (!(tmp & mask)) )
    {
        // nothing fancy here, just read out the entire length of the FIFO
        for( n = 0; (n < p_params->read_fifo_depth) && (0 == r); n++ )
        {
            // clock the next byte out
            sidekiq_fpga_reg_write(card, FPGA_REG_FLASH_SPI_READ_ACK, 1);
            r = sidekiq_fpga_reg_verify(card, FPGA_REG_FLASH_SPI_READ_ACK, 1);
        }
    }

    return r;
}


/*****************************************************************************/
/** The _do_flash_write_read function is used to actually perform the
    read and write operations needed to interface with the flash IC by means of
    the fpga. The write operation is performed first since to actually read data
    from flash, a command byte must be sent first. If no data is to be read or
    written, the appropriate "len" variable can be set to 0 and that part of
    the code will be skipped.

    @param card card number of the Sidekiq of interest
    @param write_data pointer to data to be written
    @param write_len number of bytes to write
    @param read_data pointer to where read data is to be stored
    @param read_len number of bytes to read
    @param p_params pointer to flash access parameters

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _do_flash_write_read(uint8_t card, uint32_t* write_data, uint32_t write_len,
    uint32_t* read_data, uint32_t read_len, flash_card_information_t * const p_card_info)
{
    uint32_t tmp = 0;
    uint32_t n;
    int32_t status = 0;
    uint32_t fpga_read_len;
    flash_params_t *p_params = p_card_info->params.flash_params;

    // make sure we aren't writing more data than the fifo can hold
    if ( p_params->read_fifo_depth < read_len )
    {
        skiq_error("Read request of %d bytes exceeds SPI read FIFO depth of %" PRIu32 " bytes\n",
                   read_len, p_params->read_fifo_depth);
        return( -E2BIG );
    }
    else if ( p_params->write_fifo_depth < write_len )
    {
        skiq_error("Write request of %d bytes exceeds SPI write FIFO depth of %" PRIu32 " bytes\n",
                  write_len, p_params->write_fifo_depth);
        return( -E2BIG );
    }

    /* enable SPI interface, albeit in standby mode, before the transaction is started */
    BF_SET(tmp, 1, FLASH_SPI_ENABLE_OFFSET, FLASH_SPI_ENABLE_LEN);
    sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_FLASH_SPI_CTRL, tmp);

    /* make sure the SPI controller is in a "ready" state */
    status = _wait_fpga_spi_done(card, p_params);
    if (0 != status)
    {
        return( status );
    }

    /* if FPGA has 32-bit wide read FIFO, must round to multiple of four */
    if ( p_params->has_32bit_read_fifo )
    {
        fpga_read_len = 4 * ROUND_UP( read_len, 4 );
    }
    else
    {
        fpga_read_len = read_len;
    }

    // set the read and write count
    BF_SET(tmp, write_len, FLASH_WRITE_BYTE_COUNT_OFFSET, FLASH_WRITE_BYTE_COUNT_LEN);
    BF_SET(tmp, fpga_read_len, FLASH_READ_BYTE_COUNT_OFFSET, FLASH_READ_BYTE_COUNT_LEN);

    if ( p_params->has_spi_delay_counter )
    {
        /* sets the SPI clock to its second fastest (0 is fastest, 1 is second fastest) for these
         * commands */
        BF_SET(tmp, 1, FLASH_DELAY_COUNT_OFFSET, FLASH_DELAY_COUNT_LEN);
    }

    sidekiq_fpga_reg_write( card, FPGA_REG_FLASH_SPI_CTRL, tmp);
    status = sidekiq_fpga_reg_verify( card, FPGA_REG_FLASH_SPI_CTRL, tmp );
    if( 0 != status )
    {
        skiq_error("Failed to write FPGA SPI control register on card %u\n", card);
        return( -EIO );
    }

    if( 0 != write_len )
    {
        for( n = 0; n < write_len; n++ )
        {
            sidekiq_fpga_reg_write( card,
                                    FPGA_REG_FLASH_SPI_WRITE_DATA,
                                    write_data[n] );
        }

        // send off the command to flash by toggling high......
        BIT_FLIP(tmp, (1 << FLASH_SPI_START_OFFSET));
        sidekiq_fpga_reg_write( card, FPGA_REG_FLASH_SPI_CTRL, tmp );
        status = sidekiq_fpga_reg_verify( card, FPGA_REG_FLASH_SPI_CTRL, tmp );
        if( 0 != status )
        {
            skiq_error("Failed to assert FPGA SPI control start bit on card %" PRIu8 " (status %"
                PRIi32 ")\n", card, status);
            return( -EIO );
        }

        // and then toggle it low
        BIT_FLIP(tmp, (1 << FLASH_SPI_START_OFFSET));
        sidekiq_fpga_reg_write( card, FPGA_REG_FLASH_SPI_CTRL, tmp );
        status = sidekiq_fpga_reg_verify( card, FPGA_REG_FLASH_SPI_CTRL, tmp );
        if( 0 != status )
        {
            skiq_error("Failed to clear FPGA SPI control start bit on card %" PRIu8 " (status %"
                PRIi32 ")\n", card, status);
            return( -EIO );
        }

        // block until the spi transaction finishes before continuing
        status = _wait_fpga_spi_done(card, p_params);
        if( 0 != status )
        {
            return( status );
        }
    }

    if ( 0 != fpga_read_len )
    {
        uint32_t dword = 0, read_increment;

        /* increment by 4 if FPGA has 32-bit wide read FIFO */
        read_increment = (p_params->has_32bit_read_fifo) ? 4 : 1;

        for( n = 0; n < fpga_read_len; n += read_increment )
        {
            status = sidekiq_fpga_reg_read( card,
                                            FPGA_REG_FLASH_SPI_READ_DATA,
                                            &dword );
            if( 0 != status )
            {
                return( -EIO );
            }

            /* unpack the data word */
            if ( p_params->has_32bit_read_fifo )
            {
                /* only unpack into at most the number of bytes that the caller
                 * requested, otherwise a stack overflow can occur.
                 * read_data[n] is always okay otherwise the loop condition
                 * would have failed.  assumes MSB ordering from the FPGA. */
                read_data[n] = (dword >> 24) & 0xFF;
                if ( n+1 < read_len )
                {
                    read_data[n+1] = (dword >> 16) & 0xFF;
                }
                if ( n+2 < read_len )
                {
                    read_data[n+2] = (dword >>  8) & 0xFF;
                }
                if ( n+3 < read_len )
                {
                    read_data[n+3] = (dword >>  0) & 0xFF;
                }
            }
            else
            {
                read_data[n] = dword;
            }

            // clock the next byte out
            sidekiq_fpga_reg_write( card,
                                    FPGA_REG_FLASH_SPI_READ_ACK,
                                    0x00000001 );
            status = sidekiq_fpga_reg_verify( card,
                                              FPGA_REG_FLASH_SPI_READ_ACK,
                                              0x00000001 );
            if( 0 != status )
            {
                return( -EFAULT );
            }
        }
    }

    // disable spi interface, albeit in standby mode,
    tmp = 0;
    sidekiq_fpga_reg_write( card, FPGA_REG_FLASH_SPI_CTRL, tmp);
    status = sidekiq_fpga_reg_verify( card, FPGA_REG_FLASH_SPI_CTRL, tmp );
    if( 0 != status )
    {
        skiq_error("Failed to set FPGA SPI control register on card %" PRIu8 "\n", card);
        return( -EIO );
    }

    return( 0 );
}


/**
    @note This function assumes that _init_flash_interface has been called prior.
*/
static int32_t
read_status_register(uint8_t card, flash_card_information_t * const p_card_info,
    uint8_t *p_statusReg)
{
    int32_t status = 0;
    uint32_t write_data[1] = { FLASH_CMD_READ_STATUS_REG };
    uint32_t read_data = 0;
    uint32_t write_len = 1;
    uint32_t read_len = 1;

    status = _do_flash_write_read(card, write_data, write_len, &read_data, read_len, p_card_info);
    if (0 == status)
    {
        if (NULL != p_statusReg)
        {
            *p_statusReg = (uint8_t) (read_data & 0xFF);
        }
    }

    return (status);
}


/*****************************************************************************/
/** The flash_wel_enable function is used to set the write enable latch. See
    page 47 of the data sheet for the N25Q032A11EF640E flash IC for mPCIe and
    m.2.  See page 75 of the data sheet for the S25FL512S flash IC for X2.

    @param card card number of the Sidekiq of interest
    @param p_params pointer to flash access parameters

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t
flash_wel_enable(uint8_t card, flash_card_information_t * const p_card_info)
{
#define MAX_NR_TRIES 5
    uint32_t cmd_write_enable = FLASH_CMD_WRITE_ENABLE;
    flash_params_t *p_params = p_card_info->params.flash_params;
    int32_t status = 0;
    bool done = false;
    uint8_t nr_tries = MAX_NR_TRIES;

    /* On the HTG-K800's Flash IC, the Write Enable (WREN) command is sometimes not decoded
     * correctly or is not successful.  If the flash has the ability to verify this field, the
     * following code will read Status Register 1 (SR-1) and verify that the Write Enable Latch
     * (WEL) is asserted, otherwise it will retry the WREN command up to 'nr_tries'.  */
    do
    {
        /* try sending FLASH_CMD_WRITE_ENABLE */
        status = _do_flash_write_read(card, &cmd_write_enable, 1, NULL, 0, p_card_info);
        if ( p_params->has_write_enable_latch_verify )
        {
            if ( 0 == status )
            {
                uint8_t sr = 0xFF;

                /* check SR-1 to see that WEL is asserted */
                status = read_status_register(card, p_card_info, &sr);
                if ( 0 == status )
                {
                    if ( ( 0xFF != sr ) &&
                         ( ( sr & FLASH_CMD_READ_STATUS_WRITE_LATCH_MASK ) == FLASH_CMD_READ_STATUS_WRITE_LATCH_MASK ) )
                    {
                        /* WEL is asserted in SR-1, so WREN was successful */
                        done = true;
                    }
                }
            }
        }
        else
        {
            /* no ability to verify means the function is done after WREN is sent */
            done = true;
        }
    }
    while ( !done && ( status == 0 ) && ( --nr_tries > 0 ) );

    if ( nr_tries < MAX_NR_TRIES )
    {
        skiq_warning("Retried write enable (WE) command for flash device on card %" PRIu8 "\n",
            card);
    }

    if ( nr_tries == 0 )
    {
        skiq_error("Failed to issue successful write enable (WE) for flash device on card %"
            PRIu8 "\n", card);
        status = -EIO;
    }

    return( status );
#undef MAX_NR_TRIES
}

/*****************************************************************************/
/**
    The flash_write_read_with_wel function is used to both read and write data
    to the flash chip by means of the fpga's spi interface. Before performing
    any command though, the write enable latch (WEL) must first be set. Only then can
    a flash command, such as erase or program, be executed.

    @param[in]  card        card number of the Sidekiq of interest
    @param[in]  write_data  pointer to data to be written
    @param[in]  write_len   number of bytes to write in @p write_data
    @param[in]  read_data   pointer to where read data is to be stored
    @param[in]  read_len    number of bytes to read into @p read_data
    @param[in]  p_params    pointer to flash access parameters

    @return int32_t  status where 0=success, anything else is an error
    @retval -EIO    if the Write Enable Latch couldn't be set before doing the desired read/write
*/
static int32_t
flash_write_read_with_wel(uint8_t card, uint32_t* write_data, uint32_t write_len,
    uint32_t* read_data, uint32_t read_len, flash_card_information_t * const p_card_info)
{
    int32_t status = 0;

    // set the write enable latch
    status = flash_wel_enable(card, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to set write enable latch (WEL) on flash memory on card %" PRIu8
            " (status = %" PRIi32 ")\n", card, status);
    }

    // go and perform the actual read and/or write
    if( 0 == status )
    {
        status = _do_flash_write_read(card, write_data, write_len, read_data, read_len,
                    p_card_info);
    }

    /**
        @todo   Should the WEL be explicitly disabled (on most chips 04h - WRITE DISABLE cmd) if
                the caller's Flash command isn't sent properly?
    */

    return( status );
}

/*****************************************************************************/
/**
    The _flash_wait_until_idle function is used to block code flow until flash says that it is
    done with executing the previous command. This is done by polling both the flash's status and
    flag registers.

    @param card card number of the Sidekiq of interest
    @param p_params pointer to flash access parameters

    @return int32_t  status where 0=success, anything else is an error
    @retval -EFAULT     if the start or stop time couldn't be read
    @retval -ETIMEDOUT  if the operation timed out
*/
static int32_t
_flash_wait_until_idle(uint8_t card, flash_card_information_t * const p_card_info)
{
    int32_t status = 0;
    uint32_t write_data = 0;
    uint32_t read_data = 0;
    bool done = false;
    flash_params_t *p_params = p_card_info->params.flash_params;
    uint32_t flash_status_delay_ns = p_params->fpga_flash_status_delay_ns;

    uint64_t nowNs = 0;
    uint64_t endNs = 0;
    /**
        @brief  A flag to indicate if one last register check should be done after the deadline
                has been reached (or exceeded)
    */
    bool timeoutLastTime = false;

    status = hal_time_get_ns(hal_time_clock_type_monotonic, &nowNs);
    if (0 != status)
    {
        skiq_error("Failed to get start time on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        return (-EFAULT);
    }
    endNs = nowNs + (uint64_t) FLASH_DONE_BIT_TIMEOUT_NS;

    while( !done )
    {
        uint8_t sr = 0xFF;

        status = read_status_register(card, p_card_info, &sr);
        if( 0 != status )
        {
            // Error!
            skiq_error("Failed to read Flash status register on card %" PRIu8 " (status %"
                PRIi32 ")\n", card, status);
            done = true;
        }
        else if( 0 == (sr & FLASH_BIT_GENERIC_STATUS_WIP) )
        {
            // Done!
            done = true;
        }
        else
        {
            if (timeoutLastTime)
            {
                /*
                   The "one last chance" timeout has failed - so now officially declare the
                   timeout.
                */
                status = -ETIMEDOUT;
                done = true;
            }
            else
            {
                hal_nanosleep(flash_status_delay_ns);

                status = hal_time_get_ns(hal_time_clock_type_monotonic, &nowNs);
                if (0 != status)
                {
                    skiq_error("Failed to get current time on card %" PRIu8 " (status %" PRIi32
                        ")\n", card, status);
                    done = true;
                    status = -EFAULT;
                }
                else
                {
                    if (endNs <= nowNs)
                    {
                        /*
                            If the deadline is upon us (or already past), try one more time to
                            check the register just to be safe - maybe the register changed
                            during the last sleep...
                        */
                        timeoutLastTime = true;
                    }
                }
            }
        }
    }

    if ( p_params->has_flash_read_flag_register && ( 0 == status ) )
    {
        if ((flash_chip_type_micron_n25 == p_card_info->part_id.chip) ||
            (flash_chip_type_micron_mt25q == p_card_info->part_id.chip))
        {
            done = false;
            timeoutLastTime = false;
            write_data = FLASH_CMD_MICRON_N25_READ_FLAG_REG;

            status = hal_time_get_ns(hal_time_clock_type_monotonic, &nowNs);
            if (0 != status)
            {
                skiq_error("Failed to get start time on card %" PRIu8 " (status %" PRIi32 ")\n",
                    card, status);
                return (-EFAULT);
            }
            endNs = nowNs + FLASH_DONE_BIT_TIMEOUT_NS;

            while( !done )
            {
                status = _do_flash_write_read(card, &write_data, 1, &read_data, 1, p_card_info);
                if( 0 != status )
                {
                    // Error!
                    skiq_error("Failed to read Flash Flag register on card %" PRIu8 " (status %"
                        PRIi32 ")\n", card, status);
                    done = true;
                }
                else if( 0 != ( read_data & FLASH_MASK_MICRON_N25_FLAGSTATUS_BUSY ) )
                {
                    // Done!
                    done = true;
                }
                else
                {
                    if (timeoutLastTime)
                    {
                        /*
                            The "one last chance" timeout has failed - so now officially declare the
                            timeout.
                        */
                        status = -ETIMEDOUT;
                        done = true;
                    }
                    else
                    {
                        hal_nanosleep(flash_status_delay_ns);

                        status = hal_time_get_ns(hal_time_clock_type_monotonic, &nowNs);
                        if (0 != status)
                        {
                            skiq_error("Failed to get current time on card %" PRIu8 " (status %"
                                PRIi32 ")\n", card, status);
                            done = true;
                            status = -EFAULT;
                        }
                        else
                        {
                            if (endNs <= nowNs)
                            {
                                /*
                                    If the deadline is upon us (or already past), try one more time
                                    to check the register just to be safe - maybe the register
                                    changed during the last sleep...
                                */
                                timeoutLastTime = true;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            debug_print("Flash FLAG register not supported by Flash part %" PRIu8 " on card %"
                PRIu8 "\n", (int) p_card_info->part_id.chip, card);
        }
    }

    return( status );
}

/*****************************************************************************/
/** The _init_flash_interface function performs all of the necessary actions
    to ensure that flash access can occur over FPGA registers.

    @param card card number of the Sidekiq of interest
    @param p_params pointer to flash access paramters

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 success
    @retval -ENOTSUP flash access through FPGA registers is not supported for the specified card
*/
static int32_t _init_flash_interface(uint8_t card, flash_card_information_t * const p_card_info)
{
    flash_params_t *p_params = p_card_info->params.flash_params;
    int32_t r = 0;

    // make sure flash interface is supported
    if( false == _is_flash_supported(p_card_info))
    {
        return -ENOTSUP;
    }

    // FX2 controls flash SPI interface
    if( p_params->has_fx2_negotiation )
    {
        // set GPIF pins to interface with the FX2
        if( 0 != (r = _init_fx2_interface(card)) )
        {
            return r;
        }
        // have FPGA claim flash SPI interface
        if( 0 != (r = _send_fx2_command(card, CMD_FPGA_FLASH_SPI_MASTER)) )
        {
            // the FX2 GPIF pins are still set; go ahead and clear them
            (void) _free_fx2_interface(card);

            return r;
        }
    }

    // flush FPGA's internal SPI FIFO
    if( 0 != (r = _clear_fpga_spi(card, p_params)) )
    {
        return r;
    }

    return 0;
}

/*****************************************************************************/
/** The _free_flash_interface function performs all of the necessary actions
    to ensure that the flash interface is cleaned up after use.

    @param card card number of the Sidekiq of interest
    @param p_params pointer to flash access paramters

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _free_flash_interface(uint8_t card, flash_params_t * const p_params)
{
    int32_t r = 0;

    // set GPIF pins to default
    if( p_params->has_fx2_negotiation )
    {
        r = _free_fx2_interface(card);
    }

    return r;
}

/*****************************************************************************/
/**
    The _quad_operation_check_and_enable function is used to set enable Quad SPI operation
    (if needed and supported). Generally this is a non-volatile setting, but needs to be checked
    in case the flash IC still has its factory configuration (or if setting is volatile).

    @param[in]  card        card number of the Sidekiq of interest
    @param[in]  p_card_info information about the card and its flash

    @return int32_t  status where 0=success, anything else is an error
    @retval -ENOSUP     if Quad SPI isn't supported on this Flash chip
*/
static int32_t
_quad_operation_check_and_enable( uint8_t card, flash_card_information_t * const p_card_info )
{
    uint32_t write_len = 0;
    uint32_t read_len = 1;
    uint32_t write_data[FLASH_CMD_MAX_SIZE] = { 0 };
    uint32_t read_data;
    int32_t status = 0;
    bool quadEnableNeeded = false;
    uint32_t quadMask = 0;
    uint32_t quadExpectedValue = 0;
    flash_chip_type_t chip = p_card_info->part_id.chip;
    /*
        Flag to indicate if the chip needs to set the Write Enable Latch (WEL) before enabling
        quad operation.
    */
    bool welNeeded = false;

    write_len = 0;

    // Check if Quad Operation is enabled on this card
    if (flash_chip_type_spansion_s25 == chip)
    {
        // The bits to mask off in Configuration Register 1 that tell the Quad I/O state
        quadMask = FLASH_BIT_SPANSION_S25_CR1_QUAD_OPERATION_BIT;
        // After masking, this is the expected value of CR1; if not, then the Quad I/O needs to be
        // enabled
        quadExpectedValue = FLASH_BIT_SPANSION_S25_CR1_QUAD_OPERATION_BIT;
        write_data[write_len++] = FLASH_CMD_SPANSION_S25_READ_CONFIG_REG_1;
    }
    else
    {
        status = -ENOTSUP;
        skiq_error("Cannot check for quad operation on this Flash chip (%d) on card %" PRIu8,
            p_card_info->part_id.chip, card);
    }

    if ( 0 == status )
    {
        status = _do_flash_write_read(card, write_data, write_len, &read_data, read_len,
                    p_card_info);
    }

    if ( 0 == status )
    {
        if ( ( read_data & quadMask ) != quadExpectedValue )
        {
            skiq_info("Quad SPI operation bit needed assertion (expected on very first flash "
                      "access) on card %" PRIu8 "\n", card);
            quadEnableNeeded = true;
        }
    }

    // Enable Quad SPI if needed
    if (( 0 == status ) && ( quadEnableNeeded ))
    {
        write_len = 0;

        if (flash_chip_type_spansion_s25 == chip)
        {
            welNeeded = true;

            // See page 74 of the Spansion data sheet (listed above) about the Write Registers
            // command
            write_data[write_len++] = FLASH_CMD_SPANSION_S25_WRITE_REGISTERS;
            write_data[write_len++] = FLASH_BIT_SPANSION_S25_SR1_WRITE_LATCH_ENABLE;
            write_data[write_len++] = FLASH_BIT_SPANSION_S25_CR1_QUAD_OPERATION_BIT;
        }

        if ( 0 < write_len )
        {
            if ( welNeeded )
            {
                status = flash_write_read_with_wel(card, write_data, write_len, NULL, 0,
                            p_card_info);
            }
            else
            {
                status = _do_flash_write_read(card, write_data, write_len, NULL, 0, p_card_info);
            }

            /* setting the quad SPI mode operation bit can be a non-volatile
             * configuration, so wait until the flash IC indicates completion */
            if ( 0 == status)
            {
                status = _flash_wait_until_idle( card, p_card_info );
            }
        }
    }

    return( status );
}


/***** GLOBAL FUNCTIONS *****/

/**
    @brief  Function used to initialize the Flash chip on a per-transport basis
*/
int32_t fpga_reg_init(uint8_t card, flash_card_information_t * const p_card_info)
{
    flash_params_t *p_params = p_card_info->params.flash_params;
    int32_t status = 0;
    bool interfaceEnabled = false;

    if ( p_params->has_4byte_addr && p_params->has_quad_read )
    {
        status = _init_flash_interface( card, p_card_info );
        if ( 0 != status )
        {
            if ( -ENOTSUP != status )
            {
                skiq_error("Failed to initialize flash interface on card %" PRIu8 " (status %"
                    PRIi32 ")\n", card, status);
            }
        }
        else
        {
            interfaceEnabled = true;
        }

        if ( 0 == status )
        {
            status = _quad_operation_check_and_enable( card, p_card_info );
            if ( ( 0 != status ) && ( -ENOTSUP != status ) )
            {
                skiq_error("Failed to check and/or enable quad SPI operation for card %" PRIu8
                    " (status %" PRIi32 ")\n", card, status);
                status = -EIO;
            }
        }
    }

    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return status;
}


/**
    @brief  Function used to identify the Flash chip (part) used on a Sidekiq card
*/
int32_t fpga_reg_identify_chip(uint8_t card, flash_card_information_t* const p_card_info,
    flash_part_information_t *p_part)
{
#define MAX_NR_TRIES    (5)
    int32_t status = 0;
    uint32_t write_data[FLASH_CMD_MAX_SIZE] = { 0 };
    uint32_t write_len = 0;
    uint32_t read_data[FLASH_CMD_MAX_SIZE] = { 0 };
    uint32_t read_len = FLASH_LEN_JEDEC_ID;
    flash_params_t* p_params = p_card_info->params.flash_params;
    flash_chip_type_t tmpChip = flash_chip_type_unknown;
    flash_part_information_t localPart = FLASH_PART_INFORMATION_INITIALIZER;
    bool interfaceEnabled = false;

    uint8_t nr_tries = MAX_NR_TRIES;
    bool done = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for chip identification on card %"
                PRIu8 " (status %" PRIi32 ")\n", card, status);
        }
        goto fpga_reg_identify_chip_exit;
    }
    interfaceEnabled = true;

    /* Don't use the FLASH_CMD_PREPARE_* macros as no address bytes are needed for this command */
    write_len = 0;
    write_data[write_len++] = FLASH_CMD_READ_JEDEC_ID;

    do
    {
        status = _do_flash_write_read(card, write_data, write_len, read_data, read_len, p_card_info);
        if( 0 != status )
        {
            skiq_error("Failed to read from flash on card %" PRIu8 " (status %" PRIi32 ")\n",
                card, status);
            goto fpga_reg_identify_chip_exit;
        }

        status = _flash_wait_until_idle(card, p_card_info);
        if( 0 != status )
        {
            skiq_error("Failed to obtain flash idle state on card %" PRIu8 " (status %" PRIi32 ")\n",
                card, status);
            goto fpga_reg_identify_chip_exit;
        }

        localPart.jedec_id_manufacturer = (uint8_t) (read_data[0] & 0xFF);
        localPart.manufacturer_id_type = (uint8_t) (read_data[1] & 0xFF);
        localPart.manufacturer_id_capacity = (uint8_t) (read_data[2] & 0xFF);

        /*
            Look for valid data - it's unlikely that there would be a JEDEC Manufacturer ID of 0xFF,
            but even more unlikely that the Manufacturer would also have an ID of 0xFF - in this
            case, assume that the read wasn't successful.
        */
        done = ((0xFF != localPart.jedec_id_manufacturer) &&
                (0xFF != localPart.manufacturer_id_type));
    } while ( !done && ( status == 0 ) && ( --nr_tries > 0 ) );

    if ( nr_tries < MAX_NR_TRIES )
    {
        skiq_warning("Retried identification command for flash device on card %" PRIu8 "\n",
            card);
    }

    if ( 0 == nr_tries )
    {
        skiq_error("Failed to get successful identification of flash device on card %" PRIu8
            "; declaring as 'unknown' type.\n", card);
        status = -EIO;
        goto fpga_reg_identify_chip_exit;
    }

    switch(localPart.jedec_id_manufacturer)
    {
    case FLASH_CODE_JEDEC_MFR_SPANSION_1:
        if (FLASH_CODE_MEMORY_TYPE_SPANSION_S25 == localPart.manufacturer_id_type)
        {
            tmpChip = flash_chip_type_spansion_s25;
        }
        break;
    case FLASH_CODE_JEDEC_MFR_MICRON_1:
        if (FLASH_CODE_MEMORY_TYPE_MICRON_MT25Q == localPart.manufacturer_id_type)
        {
            tmpChip = flash_chip_type_micron_mt25q;
        }
        else if (FLASH_CODE_MEMORY_TYPE_MICRON_N25 == localPart.manufacturer_id_type)
        {
            tmpChip = flash_chip_type_micron_n25;
        }
        break;
    case FLASH_CODE_JEDEC_MFR_ISSI_1:
        if (FLASH_CODE_MEMORY_TYPE_ISSI_IS25 == localPart.manufacturer_id_type)
        {
            tmpChip = flash_chip_type_issi_is25;
        }
        break;
    case FLASH_CODE_JEDEC_MFR_MACRONIX_1:
        if (FLASH_CODE_MEMORY_TYPE_MACRONIX_MX25 == localPart.manufacturer_id_type)
        {
            tmpChip = flash_chip_type_macronix_mx25;
        }
        break;
    case FLASH_CODE_JEDEC_MFR_WINBOND_1:
        if (FLASH_CODE_MEMORY_TYPE_WINBOND_W25 == localPart.manufacturer_id_type)
        {
            tmpChip = flash_chip_type_winbond_w25;
        }
        break;
    default:
        tmpChip = flash_chip_type_unknown;
        break;
    }

    if (flash_chip_type_unknown == tmpChip)
    {
        skiq_unknown("Unrecognized Flash (JEDEC Manufacturer ID 0x%" PRIx8 ", Manufacturer type ID"
            " 0x%" PRIx8 ", Manufacturer Capacity ID 0x%" PRIx8 ") on card %" PRIu8
            "... setting to 'unknown'\n", localPart.jedec_id_manufacturer,
            localPart.manufacturer_id_type, localPart.manufacturer_id_capacity, card);
    }

    localPart.chip = tmpChip;

    if (NULL != p_part)
    {
        *p_part = localPart;
    }

#if (defined DEBUG_FPGA_FLASH)
    display_status_registers(card, p_card_info);
#endif

fpga_reg_identify_chip_exit:
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return (status);
#undef MAX_NR_TRIES
}


int32_t fpga_reg_erase_sector(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    flash_params_t* p_params = p_card_info->params.flash_params;
    uint32_t write_data[FLASH_CMD_MAX_SIZE] = { 0 };
    uint32_t write_len = 0;   /* write_len is set by FLASH_CMD_PREPARE or FLASH_CMD_PREPARE_4BYTE */
    int32_t status = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if (0 != status)
    {
        if ( status != -ENOTSUP )
        {
            skiq_error("Failed to initialize flash interface for sector erase on card %" PRIu8
                " (status %" PRIi32 ")\n", card, status);
        }
        goto fpga_reg_erase_sector_exit;
    }
    interfaceEnabled = true;

    /* 4 byte addressing requires a different command and command structure */
    if ( p_params->has_4byte_addr )
    {
        FLASH_CMD_PREPARE_4BYTE(write_data, write_len, FLASH_CMD_SECTOR_ERASE_4BYTE,
                                p_transfer->addr);
    }
    else
    {
        FLASH_CMD_PREPARE(write_data, write_len, FLASH_CMD_SECTOR_ERASE, p_transfer->addr);
    }

    status = flash_write_read_with_wel(card, write_data, write_len, NULL, 0, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to erase flash sector on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        goto fpga_reg_erase_sector_exit;
    }

    status = _flash_wait_until_idle(card, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to obtain flash idle state on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        goto fpga_reg_erase_sector_exit;
    }

fpga_reg_erase_sector_exit:
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return status;
}

int32_t fpga_reg_read_page(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    flash_params_t* p_params = p_card_info->params.flash_params;
    uint32_t read_data[FLASH_PAGE_MAX_SIZE] = {0};
    uint32_t read_len = p_transfer->len;
    uint32_t write_data[FLASH_CMD_MAX_SIZE + 1] = {0}; /* add one for dummy write in case of 4byte +
                                                        * quad-read */
    uint32_t write_len;         /* write_len is set by FLASH_CMD_PREPARE or
                                 * FLASH_CMD_PREPARE_4BYTE */
    int32_t status = 0;
    uint32_t n = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for page read on card %" PRIu8
                " (status %" PRIi32 ")\n", card, status);
        }
        goto fpga_reg_read_page_exit;
    }
    interfaceEnabled = true;

    if ( p_params->has_4byte_addr && p_params->has_quad_read )
    {
        FLASH_CMD_PREPARE_4BYTE(write_data, write_len, FLASH_CMD_QUAD_PAGE_READ_4BYTE,
                                p_transfer->addr);

        /* add a dummy byte to the write_len for read latency */
        write_len++;
    }
    else if ( p_params->has_4byte_addr )
    {
        FLASH_CMD_PREPARE_4BYTE(write_data, write_len, FLASH_CMD_PAGE_READ_4BYTE, p_transfer->addr);
    }
    else
    {
        FLASH_CMD_PREPARE(write_data, write_len, FLASH_CMD_PAGE_READ, p_transfer->addr);
    }

    status = _do_flash_write_read(card, write_data, write_len, read_data, read_len, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to read from flash on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        goto fpga_reg_read_page_exit;
    }

    status = _flash_wait_until_idle(card, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to obtain flash idle state on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        goto fpga_reg_read_page_exit;
    }

    // copy UINT32 data (FPGA register read) to UINT8 buffer (flash data)
    for( n = 0; n < read_len; n++ )
    {
        p_transfer->buf[n] = (uint8_t) read_data[n];
    }

fpga_reg_read_page_exit:
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return status;
}

int32_t fpga_reg_write_page(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    flash_params_t* p_params = p_card_info->params.flash_params;
    uint32_t write_data[FLASH_CMD_MAX_SIZE + FLASH_PAGE_MAX_SIZE] = {0};
    uint32_t write_len = 0;   /* write_len is set by FLASH_CMD_PREPARE or FLASH_CMD_PREPARE_4BYTE */
    int32_t status = 0;
    uint32_t n = 0;
    bool interfaceEnabled = false;

    if ( FLASH_PAGE_MAX_SIZE < p_transfer->len )
    {
        status = -E2BIG;
        skiq_error("Data size %" PRIu32 " exceeds maximum write buffer size %" PRIu32
            " on card %" PRIu8 " (status %" PRIi32 ")\n", p_transfer->len, FLASH_PAGE_MAX_SIZE,
            card, status);
        goto fpga_reg_write_page_exit;
    }

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for page write on card %" PRIu8
                " (status %" PRIi32 ")\n", card, status);
        }
        goto fpga_reg_write_page_exit;
    }
    interfaceEnabled = true;

    if ( p_params->has_4byte_addr && p_params->has_quad_write )
    {
        FLASH_CMD_PREPARE_4BYTE(write_data, write_len, FLASH_CMD_QUAD_PAGE_PROGRAM_4BYTE,
                                p_transfer->addr);
    }
    else if ( p_params->has_4byte_addr )
    {
        FLASH_CMD_PREPARE_4BYTE(write_data, write_len, FLASH_CMD_PAGE_PROGRAM_4BYTE,
                                p_transfer->addr);
    }
    else
    {
        FLASH_CMD_PREPARE(write_data, write_len, FLASH_CMD_PAGE_PROGRAM, p_transfer->addr);
    }

    // copy UINT8 buffer (flash data) to UINT32 buffer (fpga register)
    for( n = 0; n < p_transfer->len; n++ )
    {
        write_data[write_len++] = p_transfer->buf[n];
    }

    status = flash_write_read_with_wel(card, write_data, write_len, NULL, 0, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to write to flash on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        goto fpga_reg_write_page_exit;
    }

    status = _flash_wait_until_idle(card, p_card_info);
    if( 0 != status )
    {
        skiq_error("Failed to obtain flash idle state on card %" PRIu8 " (status %" PRIi32 ")\n",
            card, status);
        goto fpga_reg_write_page_exit;
    }

fpga_reg_write_page_exit:
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return status;
}


static int32_t
_delay_and_rescan( uint8_t card,
                   uint32_t delay_to_first_rescan_sec,
                   uint32_t max_rescan_period_sec )
{
    uint8_t i;
    int32_t status = INT32_MAX;

    skiq_info("Starting transport rescan of card %u in %d second(s)\n", card,
              delay_to_first_rescan_sec);

    /* loop to avoid potential overflow of hal_nanosleep's uint32_t argument: SEC < UINT32_MAX */
    for( i = 0; i < delay_to_first_rescan_sec; i++ )
    {
        hal_nanosleep(SEC);
    }

    skiq_info("Performing periodic rescan of transport bus for up to %d second(s)\n",
              max_rescan_period_sec);

    for( i = 0; (i < max_rescan_period_sec) && (status != 0); i++ )
    {
        hal_nanosleep(SEC);
        status = sidekiq_xport_fpga_up(card);
    }

    if ( 0 == status )
    {
        skiq_info("Found card %u, transport is up\n", card);
    }
    else
    {
        skiq_error("Failed to find card %u, transport is down, status %d\n", card, status);
    }

    return status;
}


static int32_t
_load_fpga( uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info )
{
    flash_params_t* p_params = p_card_info->params.flash_params;
    int32_t status = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if ( status != 0 )
    {
        skiq_error("Failed to initialize flash interface on card %" PRIu8 " (status %" PRIi32
            ")\n", card, status);
    }
    else
    {
        interfaceEnabled = true;
    }

    /* `has_fx2_negotiation` here actually means "is an FX2 present". */
    if ( ( 0 == status ) && ( p_params->has_fx2_negotiation ) )
    {
        status = _send_fx2_command(card, CMD_FPGA_LOAD_FROM_FLASH);
    }

    /* take down the FPGA transport */
    if ( status == 0 )
    {
        /* FX2 will handle the reload, so just use standard fpga_down, rescan
         * the transport on the host to detect the FPGA bitstream change */
        status = sidekiq_xport_fpga_down(card);
    }

    /* perform the delay to first rescan and bringing up the FPGA transport */
    if ( status == 0 )
    {
        status = _delay_and_rescan( card,
                                    PCI_FPGA_DELAY_TO_FIRST_RESCAN_SEC,
                                    PCI_FPGA_MAX_RESCAN_PERIOD_SEC );
    }

    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return (status);
}


static int32_t
_load_fpga_by_address( uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info, uint32_t load_address )
{
    flash_params_t* p_params = p_card_info->params.flash_params;
    int32_t status = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if ( status != 0 )
    {
        skiq_error("Failed to initialize flash interface on card %" PRIu8 " (status %" PRIi32
            ")\n", card, status);
    }
    else
    {
        interfaceEnabled = true;
    }

    /* `has_fx2_negotiation` here actually means "is an FX2 present". */
    if ( ( 0 == status ) && ( p_params->has_fx2_negotiation ) )
    {
        status = _send_fx2_command(card, CMD_FPGA_LOAD_FROM_FLASH);
    }

    /* take down the FPGA transport */
    if ( status == 0 )
    {
        /* DMA driver will handle the reload, so use fpga_down_reload, rescan
         * the transport on the host to detect the FPGA bitstream change */
        status = sidekiq_xport_fpga_down_reload(card, load_address);
    }

    /* perform the delay to first rescan and bringing up the FPGA transport */
    if ( status == 0 )
    {
        status = _delay_and_rescan( card,
                                    PCI_FPGA_DELAY_TO_FIRST_RESCAN_SEC_SELF_RELOAD,
                                    PCI_FPGA_MAX_RESCAN_PERIOD_SEC );
    }

    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return (status);
}


int32_t fpga_reg_load_fpga(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    uint32_t status;

    if ( p_card_info->params.has_self_reload )
    {
        status = _load_fpga_by_address( card, p_transfer, p_card_info, p_transfer->addr );
    }
    else
    {
        status = _load_fpga( card, p_transfer, p_card_info );
    }

    return (status);
}

/* =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= */
#if (defined DEBUG_FPGA_FLASH)

static int32_t
display_status_registers ( uint8_t card, flash_card_information_t * const p_card_info )
{
#define MAX_STATUS_REGISTERS    (8)
    int32_t status = 0;
    flash_params_t* p_params = p_card_info->params.flash_params;
    uint32_t write_data = 0;
    uint32_t read_data = 0;
    flash_chip_type_t chip = p_card_info->part_id.chip;
    struct
    {
        const char *name;
        uint8_t regValue;
    } statusRegisters[MAX_STATUS_REGISTERS] = \
        { [0 ... (MAX_STATUS_REGISTERS-1)] = { .name = NULL, .regValue = UINT8_MAX } };
    uint8_t statusRegIdx = 0;
    uint8_t i = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for unlocking golden sectors on"
                " card %" PRIu8 " (status %" PRIi32 ")\n", card, status);
        }
        return (status);
    }
    interfaceEnabled = true;

    switch (chip)
    {
    case flash_chip_type_micron_n25:
    case flash_chip_type_micron_mt25q:
        statusRegisters[statusRegIdx].name = "Status Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_READ_STATUS_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Flag Status Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_READ_FLAG_REG;
        statusRegIdx++;
        break;
    case flash_chip_type_winbond_w25:
        statusRegisters[statusRegIdx].name = "Status Register-1";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_READ_STATUS_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Status Register-2";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_WINBOND_W25_READ_STATUS_REG_2;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Status Register-3";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_WINBOND_W25_READ_STATUS_REG_3;
        statusRegIdx++;
        break;
    case flash_chip_type_issi_is25:
        statusRegisters[statusRegIdx].name = "Status Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_READ_STATUS_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Read Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_ISSI_IS25_READ_FUNC_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Extended Read Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_ISSI_IS25_READ_RXT_READ_REG;
        statusRegIdx++;
        break;
    case flash_chip_type_macronix_mx25:
        statusRegisters[statusRegIdx].name = "Status Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_READ_STATUS_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Config Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_MACRONIX_MX25_READ_CONFIG_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Security Register";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_MACRONIX_MX25_READ_SECURITY_REG;
        statusRegIdx++;
        break;
    case flash_chip_type_spansion_s25:
        statusRegisters[statusRegIdx].name = "Status Register-1";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_READ_STATUS_REG;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Status Register-2";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_SPANSION_S25_READ_STATUS_REG_2;
        statusRegIdx++;

        statusRegisters[statusRegIdx].name = "Configuration Register-1";
        statusRegisters[statusRegIdx].regValue = FLASH_CMD_SPANSION_S25_READ_CONFIG_REG_1;
        statusRegIdx++;
        break;
    case flash_chip_type_none:
        skiq_info("No Flash chip present on card %" PRIu8 "; not displaying status registers",
            card);
        break;
    case flash_chip_type_unknown:
    default:
        status = -ENOTSUP;
        skiq_error("Unrecognized Flash chip on card %" PRIu8 "; not attempting a read", card);
        break;
    }

    if ( 0 == status )
    {
        for (i = 0; i < statusRegIdx; i++)
        {
            if (NULL == statusRegisters[i].name)
            {
                continue;
            }

            write_data = statusRegisters[i].regValue;
            status = _do_flash_write_read(card, &(write_data), 1, &(read_data), 1, p_card_info);
            if (0 != status)
            {
                skiq_error("Failed to read Flash register '%s' on card %" PRIu8 " (status = %"
                    PRIi32 ")\n", statusRegisters[i].name, card, status);
            }
            else
            {
                skiq_info("    [card %" PRIu8 "]: %s = 0x%" PRIx8 "\n", card,
                    statusRegisters[i].name, ( (uint8_t) (read_data & 0xFF ) ));
            }
        }
    }

    /* always free the flash interface, but preserve any non-zero status from above */
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return status;
}

#endif /* DEBUG_FPGA_FLASH */
/* =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= DEBUG =-=-=-= */


/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
#if (defined ATE_SUPPORT)

static int32_t
get_unique_id( uint8_t card, flash_card_information_t * const p_card_info, uint8_t *p_id,
    uint8_t *p_id_length)
{
    int32_t status = 0;
    flash_params_t* p_params = p_card_info->params.flash_params;
    flash_chip_type_t chip = p_card_info->part_id.chip;
    uint32_t write_data[FLASH_CMD_MAX_SIZE] = {0};
    uint32_t write_len = 0;
    uint32_t follow_on_command_data[FLASH_CMD_MAX_SIZE] = { 0 };
    uint32_t follow_on_command_len = 0;
    uint32_t read_data[FLASH_CMD_MAX_SIZE + SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN] = { 0 };
    uint32_t read_len = 0;
    uint8_t read_id_offset = 0;
    uint8_t read_id_len = 0;
    uint8_t i = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for unlocking golden sectors on"
                " card %" PRIu8 " (status %" PRIi32 ")\n", card, status);
        }
        return (status);
    }
    interfaceEnabled = true;

    write_len = 0;
    if ( flash_chip_type_macronix_mx25 == chip)
    {
        /*
            The Macronix MX25 stores its Flash identifier in the OTP sector, which means that
            the address space must be switched before and after the read - see pg. 11 of the
            data sheet
        */
        write_len = 0;
        write_data[write_len++] = FLASH_CMD_MACRONIX_MX25_ENTER_OTP;

        status = _do_flash_write_read(card, &(write_data[0]), write_len, NULL, 0, p_card_info);
        if ( 0 != status )
        {
            skiq_warning("Failed to select OTP sector on card %" PRIu8 " (status %" PRIi32 ")\n",
                card, status);
        }
        else
        {
            /* Set up the follow-on command to exit OTP sector access */
            follow_on_command_len = 0;
            follow_on_command_data[follow_on_command_len++] = FLASH_CMD_MACRONIX_MX25_EXIT_OTP;

            read_id_offset = 0;
            read_len = FLASH_LEN_MACRONIX_MX25_READ_ID_TOTAL;
            read_id_len = FLASH_LEN_MACRONIX_MX25_READ_ID_TOTAL;

            /*
                Set up the command to read the unique ID - see pg. 11 of the Macronix datasheet.
                The trailing zero writes indicate the read address, which should be at address 0
                and three bytes long.
            */
            write_len = 0;
            write_data[write_len++] = FLASH_CMD_MACRONIX_MX25_READ;
            write_data[write_len++] = 0x00;
            write_data[write_len++] = 0x00;
            write_data[write_len++] = 0x00;
        }
    }
    else if ( flash_chip_type_spansion_s25 == chip )
    {
        read_id_offset = 0;
        read_id_len = FLASH_LEN_SPANSION_S25_READ_ID_TOTAL;
        read_len = FLASH_LEN_SPANSION_S25_READ_ID_TOTAL;

        /*
            Set up the command to read the unique ID - see pg. 102 of the Spansion datasheet. The
            trailing zero writes indicate the read address, which should be at address 0 and four
            bytes long.
        */
        write_len = 0;
        write_data[write_len++] = FLASH_CMD_SPANSION_S25_OTP_READ;
        write_data[write_len++] = 0x00;
        write_data[write_len++] = 0x00;
        write_data[write_len++] = 0x00;
        write_data[write_len++] = 0x00;

    }
    else if ( ( flash_chip_type_micron_n25 == chip ) || ( flash_chip_type_micron_mt25q == chip ) )
    {
        read_id_offset = FLASH_LEN_MICRON_N25_READ_ID_OFFSET;
        read_id_len = FLASH_LEN_MICRON_N25_READ_ID_LEN;
        read_len = FLASH_LEN_MICRON_N25_READ_ID_TOTAL;

        write_data[write_len++] = FLASH_CMD_READ_ID;
    }
    else
    {
        uint8_t num_dummy_bytes = 0;

        /* Generic settings */
        read_id_offset = 0;
        read_id_len = 16;

        if ( flash_chip_type_winbond_w25 == chip )
        {
            num_dummy_bytes = FLASH_LEN_WINBOND_W25_READ_ID_DUMMY_BYTES;
            read_id_len = FLASH_LEN_WINBOND_W25_READ_ID_TOTAL;
            read_len = FLASH_LEN_WINBOND_W25_READ_ID_TOTAL;
        }
        else if ( flash_chip_type_issi_is25 == chip )
        {
            num_dummy_bytes = FLASH_LEN_ISSI_IS25_READ_ID_DUMMY_BYTES;
            read_id_len = FLASH_LEN_ISSI_IS25_READ_ID_TOTAL;
            read_len = FLASH_LEN_ISSI_IS25_READ_ID_TOTAL;
        }

        write_data[write_len++] = FLASH_CMD_READ_UNIQUE_ID;
        for ( i = 0; i < num_dummy_bytes; i++ )
        {
            write_data[write_len++] = 0x00;
        }
    }

    if ( 0 == status )
    {
        /* Execute the "read unique ID" command assembled above */

        status = _do_flash_write_read(card, &(write_data[0]), write_len, &(read_data[0]),
                    read_len, p_card_info);
        if ( 0 != status )
        {
            skiq_warning("Failed to read unique ID on card %" PRIu8 " (status %" PRIi32 ")\n",
                card, status);
        }
    }

    if ( 0 < follow_on_command_len )
    {
        /*
            Execute the follow-on command; always execute it in case the chip was put into
            a special mode and needs to exit it, regardless of the success of reading the ID
        */

        int32_t tmpStatus = 0;

        tmpStatus = _do_flash_write_read(card, &(follow_on_command_data[0]), follow_on_command_len,
                        NULL, 0, p_card_info);
        if ( 0 != tmpStatus )
        {
            skiq_warning("Failed to execute follow-on command %" PRIx32 " on card %" PRIu8
                " (status %" PRIi32 "); attempting to continue...\n",
                (uint8_t) (follow_on_command_data[0] & 0xFF), card, tmpStatus);
        }
    }

    if ( 0 == status )
    {
        /*
            At this point the ID should be in read_data; the read_id_offset and read_id_len
            variables should point to the bytes that need to be copied out.
        */

        uint8_t j = 0;
        if ( NULL != p_id )
        {
            for (i = read_id_offset; i < ( read_id_offset + read_id_len ); i++ )
            {
                p_id[j++] = (uint8_t) ( read_data[i] & 0xFF );
            }
        }

        if ( NULL != p_id_length )
        {
            *p_id_length = j;
        }
    }

    /* always free the flash interface, but preserve any non-zero status from above */
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_params, status);
        interfaceEnabled = false;
    }

    return status;
}
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */


/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
/** @todo   Expand support for this to other flash chips. */
static int32_t
is_locking_supported( uint8_t card, flash_card_information_t * const p_card_info )
{
    int32_t status = 0;

    if (((skiq_mpcie == p_card_info->params.skiq_part) ||
         (skiq_m2_2280 == p_card_info->params.skiq_part) ||
         (skiq_nv100 == p_card_info->params.skiq_part)) &&
        ((flash_chip_type_micron_n25 == p_card_info->part_id.chip) ||
         (flash_chip_type_micron_mt25q == p_card_info->part_id.chip)))
    {
        status = 0;
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */


/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
static int32_t
are_golden_sectors_locked( uint8_t card, flash_card_information_t * const p_card_info,
    bool *p_is_locked )
{
    int32_t status = 0;
    bool interfaceEnabled = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for unlocking golden sectors on"
                " card %" PRIu8 " (status %" PRIi32 ")\n", card, status);
        }
        return (status);
    }
    interfaceEnabled = true;

    if ( status == 0 )
    {
        status = is_locking_supported( card, p_card_info );
        if (0 != status)
        {
            skiq_warning("Flash locking is not supported on card %" PRIu8 "\n", card);
        }
    }

    if ( status == 0 )
    {
        uint8_t sr = 0;

        status = read_status_register(card, p_card_info, &sr);
        if ( status == 0 )
        {
            debug_print("Status register: 0x%02" PRIx8 "\n", sr);

            switch (p_card_info->part_id.chip)
            {
            case flash_chip_type_micron_n25:
            case flash_chip_type_micron_mt25q:
                *p_is_locked = (bool) (0 != (sr & FLASH_MASK_MICRON_N25_BLOCKS_LOCKED));
                break;
            case flash_chip_type_winbond_w25:
            case flash_chip_type_issi_is25:
            case flash_chip_type_macronix_mx25:
                /** @todo   Add support for these Flash parts in a future release. */
                skiq_warning("Locking on this Flash part (%u) is not currently supported"
                    " on card %" PRIu8 "\n", (int) p_card_info->part_id.chip, card);
                status = -ENOTSUP;
                break;
            case flash_chip_type_spansion_s25:
                skiq_warning("Flash locking on the Spansion Flash part not currently supported"
                    " on card %" PRIu8 "\n", card);
                status = -ENOTSUP;
                break;
            case flash_chip_type_none:
                skiq_warning("no Flash part detected on Sidekiq card %" PRIu8 "\n", card);
                status = -ENOTSUP;
                break;
            case flash_chip_type_unknown:
            default:
                skiq_unknown("Error: unknown Flash part (%d) on card %" PRIu8 "; cannot determine"
                    " locking\n", (int) p_card_info->part_id.chip, card);
                status = -ENOTSUP;
                break;
            }

            if (0 == status)
            {
                if (*p_is_locked)
                {
                    skiq_info("Golden FPGA flash sectors are protected on card %" PRIu8 "\n", card);
                }
                else
                {
                    skiq_warning("Golden FPGA flash sectors are NOT protected on card %" PRIu8 "\n",
                        card);
                }
            }
        }
    }

    if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    /* always free the flash interface, but preserve any non-zero status from above */
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_card_info->params.flash_params, status);
        interfaceEnabled = false;
    }

    return status;
}
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */



/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
static int32_t
lock_golden_sectors( uint8_t card, flash_card_information_t * const p_card_info)
{
    int32_t status = 0;
    uint32_t write_len = 0;
    uint32_t write_data[FLASH_CMD_MAX_SIZE] = { [0 ... (FLASH_CMD_MAX_SIZE-1)] = 0, };
    bool interfaceEnabled = false;
    bool welNeeded = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status)
        {
            skiq_error("Failed to initialize flash interface for unlocking golden sectors on card %"
                PRIu8 " (status %" PRIi32 ")\n", card, status);
        }

        return (status);
    }
    interfaceEnabled = true;

    if ( status == 0 )
    {
        status = is_locking_supported( card, p_card_info );
    }

    if ( status == 0 )
    {
        bool locked = false;

        /* check to see if sectors are already locked or need locking */
        status = are_golden_sectors_locked( card, p_card_info, &locked );
        if ( ( status == 0 ) && locked )
        {
            status = -EEXIST;
        }
    }

    if ( status == 0 )
    {
        /* write the Block Protect bits of the Status Register */
        write_len = 0;

        if ((flash_chip_type_micron_n25 == p_card_info->part_id.chip) ||
            (flash_chip_type_micron_mt25q == p_card_info->part_id.chip))
        {
            welNeeded = true;

            write_data[write_len++] = FLASH_CMD_WRITE_STATUS_REG;
            write_data[write_len++] = FLASH_VALUE_MICRON_N25_MPCIE_GOLDEN_IMAGE_LOCK;
        }
        else
        {
            skiq_warning("Flash locking not currently implemented for this Flash chip (%d)\n",
                (int) p_card_info->part_id.chip);
            status = -ENOTSUP;
        }

        if ( ( 0 < write_len ) && ( 0 == status ) )
        {
            skiq_info("Protecting golden FPGA flash sectors on card %u\n", card);

            if ( welNeeded )
            {
                status = flash_write_read_with_wel(card, write_data, write_len, NULL, 0,
                            p_card_info);
            }
            else
            {
                status = _do_flash_write_read(card, write_data, write_len, NULL, 0, p_card_info);
            }
        }

        if ( 0 == status )
        {
            /*
                the above operation could change the non-volatile configuration, so wait until the
                flash IC indicates completion
            */
            status = _flash_wait_until_idle( card, p_card_info );
        }
    }

    if ( status == -EEXIST )
    {
        /* rewrite an EEXIST error (aka sectors are already locked) to a success */
        status = 0;
    }
    else if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    /* always free the flash interface, but preserve any non-zero status from above */
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_card_info->params.flash_params, status);
        interfaceEnabled = false;
    }

    return( status );
}
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */


/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
static int32_t
unlock_golden_sectors( uint8_t card, flash_card_information_t * const p_card_info )
{
    int32_t status = 0;
    uint32_t write_len = 0;
    uint32_t write_data[FLASH_CMD_MAX_SIZE] = { [0 ... (FLASH_CMD_MAX_SIZE-1)] = 0, };
    bool interfaceEnabled = false;
    bool welNeeded = false;

    status = _init_flash_interface(card, p_card_info);
    if ( 0 != status )
    {
        if ( -ENOTSUP != status )
        {
            skiq_error("Failed to initialize flash interface for unlocking golden sectors on"
                " card %" PRIu8 " (status %" PRIi32 ")\n", card, status);
        }

        return ( status );
    }
    interfaceEnabled = true;

    if ( status == 0 )
    {
        status = is_locking_supported( card, p_card_info );
    }

    if ( status == 0 )
    {
        bool locked = false;

        /* check to see if sectors are already unlocked */
        status = are_golden_sectors_locked( card, p_card_info, &locked );
        if ( ( status == 0 ) && !locked )
        {
            status = -EEXIST;
        }
    }

    if ( status == 0 )
    {
        /* write the Block Protect bits of the Status Register */
        write_len = 0;

        if ((flash_chip_type_micron_n25 == p_card_info->part_id.chip) ||
            (flash_chip_type_micron_mt25q == p_card_info->part_id.chip))
        {
            uint8_t sr = 0;

            /*
                Get the Status Register value - the lock bits (found in
                FLASH_MASK_MICRON_N25_MPCIE_GOLDEN_IMAGE_LOCK_SR) need to be cleared.
            */
            status = read_status_register(card, p_card_info, &sr);
            if( 0 != status )
            {
                // Error!
                skiq_error("Failed to read Flash status register on card %" PRIu8 " (status %"
                    PRIi32 ")\n", card, status);
            }

            if ( 0 == status )
            {
                welNeeded = true;

                write_data[write_len++] = FLASH_CMD_WRITE_STATUS_REG;
                write_data[write_len++] = (sr & FLASH_MASK_MICRON_N25_MPCIE_GOLDEN_IMAGE_LOCK_SR);
            }
        }
        else
        {
            skiq_warning("Flash unlocking not currently implemented for this Flash chip (%d)\n",
                (int) p_card_info->part_id.chip);
            status = -ENOTSUP;
        }

        if  ( ( 0 < write_len ) && ( 0 == status ) )
        {
            skiq_info("Removing protection on golden FPGA flash sectors on card %" PRIu8 "\n",
                card);

            if ( welNeeded )
            {
                status = flash_write_read_with_wel(card, write_data, write_len, NULL, 0,
                            p_card_info);
            }
            else
            {
                status = _do_flash_write_read(card, write_data, write_len, NULL, 0, p_card_info);
            }
        }

        if ( 0 == status )
        {
            /*
                the above operation could change the non-volatile configuration, so wait until the
                flash IC indicates completion
            */
            status = _flash_wait_until_idle( card, p_card_info );
        }
    }

    if ( status == -EEXIST )
    {
        /* rewrite an EEXIST error (aka sectors are already unlocked) to a success */
        status = 0;
    }
    else if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    /* always free the flash interface, but preserve any non-zero status from above */
    if ( interfaceEnabled )
    {
        CLOSE_INTERFACE(card, p_card_info->params.flash_params, status);
        interfaceEnabled = false;
    }

    return( status );
}

#endif  /* ATE_SUPPORT */
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */



/***** GLOBAL VARIABLES *****/

flash_functions_t fpga_reg_flash = {
    .init = fpga_reg_init,
    .identify_chip = fpga_reg_identify_chip,
    .erase_sector = fpga_reg_erase_sector,
    .read_page = fpga_reg_read_page,
    .write_page = fpga_reg_write_page,
#if (defined ATE_SUPPORT)
    .get_unique_id = get_unique_id,
    .lock_golden_sectors = lock_golden_sectors,
    .unlock_golden_sectors = unlock_golden_sectors,
    .is_golden_locked = are_golden_sectors_locked,
#else
    .get_unique_id = NULL,
    .lock_golden_sectors = NULL,
    .unlock_golden_sectors = NULL,
    .is_golden_locked = NULL,
#endif  /* defined ATE_SUPPORT */
    .load_fpga = fpga_reg_load_fpga,
};

flash_functions_t fpga_reg_flash_only_load = {
    .init = NULL,
    .identify_chip = NULL,
    .get_unique_id = NULL,
    .erase_sector = NULL,
    .read_page = NULL,
    .write_page = NULL,
    .lock_golden_sectors = NULL,
    .unlock_golden_sectors = NULL,
    .is_golden_locked = NULL,
    .load_fpga = fpga_reg_load_fpga,
};
