/**
 * @file   sidekiq_flash_config_slots.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec 11 09:59:59 2019
 *
 * @brief This file and its functions manage Sidekiq cards and their flash config slots.  A flash
 * config slot is an area of flash memory (resident on the Sidekiq or carrier) that stores an FPGA
 * configuration bitstream (and metadata).  A product may have 0 to N config slots available for use
 * by the user.
 *
 * A user can specify metadata to associate with a particular flash config slot.  That 64-bit
 * metadata field is stored as the last 8 bytes of the slot.
 *
 * A flash config slot MUST lie on a flash sector boundary so that these functions can operate on a
 * single slot without interfering with other slots during erasure.
 *
 * <pre>
 * Copyright 2019-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */


/***** INCLUDES *****/

#include <inttypes.h>

#include "sidekiq_flash_config_slots.h"
#include "sidekiq_flash.h"
#include "hal_file_ops.h"
#include "sidekiq_private.h"
#include "portable_endian.h"    /* for htole64() and le64toh() */

/* enable debug_print and debug_print_plain when DEBUG_FLASH_CONFIG_SLOTS is defined */
#if (defined DEBUG_FLASH_CONFIG_SLOTS)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/** @brief a macro that (potentially) rounds up to the nearest whole number of sectors */
#define NR_SECTOR_ROUND_UP(_sz, _sect_sz)      ((_sect_sz)*ROUND_UP((_sz)+METADATA_SZ,_sect_sz))

#define METADATA_SZ                     sizeof(uint64_t)


/** @brief Definitions for the maximum configuration bitstream size (in bytes) for a given FPGA part
 * (sidekiq_fpga_device_t).  The Xilinx User Guides provide the size in bits, so the definitions
 * then div-by-8 to get the maximum number of bytes. */

 /* from Xilinx UG380, Table 5-5: Spartan-6 FPGA Bitstream Length */
#define MAX_6SLX45T_BS_SIZE             (11939296 / 8)

 /* from Xilinx UG470, Table 1-1: Bitstream Length */
#define MAX_7A50T_BS_SIZE               (17536096 / 8)

 /* from Xilinx UG570, Table 1-4: Bitstream Length for UltraScale Architecture-based FPGAs */
#define MAX_KU060_BS_SIZE               (192999264 / 8)
#define MAX_KU115_BS_SIZE               (386012288 / 8)


/** @brief Definitions for the number of sectors that a configuration bitstream consumes for a given
 * flash sector size and product
 *
 * @warning These must not be changed once software is publicly released as it can mess with
 * backward compatibility
 */
#define SLOT_SIZE_MPCIE                 NR_SECTOR_ROUND_UP(MAX_6SLX45T_BS_SIZE, 0x10000)
#define SLOT_SIZE_M2                    NR_SECTOR_ROUND_UP(MAX_7A50T_BS_SIZE, 0x10000)
#define SLOT_SIZE_M2_2280               NR_SECTOR_ROUND_UP(MAX_7A50T_BS_SIZE, 0x10000)
#define SLOT_SIZE_HTG_KU060             NR_SECTOR_ROUND_UP(MAX_KU060_BS_SIZE, 0x40000)
#define SLOT_SIZE_HTG_KU115             NR_SECTOR_ROUND_UP(MAX_KU115_BS_SIZE, 0x40000)

/** @brief Definitions for the number of flash config slots available for a given product (and
 * carrier?).
 *
 * @warning These must not be changed once software is publicly released as it can mess with
 * backward compatibility
 */
#define NR_SLOTS_MPCIE                  1 /* doesn't support self reload (WBSTAR) in any fashion */
#define NR_SLOTS_M2                     1 /* golden and user are flipped in flash, so only logical
                                           * space for one without having the split slots around the
                                           * golden bitstream location*/

/*
  manually calculated for Sidekiq Stretch (and shared with NV100)

  - total size = 16MB
  - calibration storage = 128KB
  - user FPGA storage starts at 2MB
  --> available space = 16MB - 2MB - 128KB = 14208KB = 222 sectors available

  - sector size is 64KB
  - max bitstream size is 2192012 bytes
  - metadata size is 8 bytes
  --> # of sectors for a bitstream = ceil( ( 2192012 + 8 ) / 65536 ) = 34 sectors

  --> number of slots = floor( 222 / 34 ) = 6 slots
*/
#define NR_SLOTS_M2_2280                6

/*
  manually calculated for Sidekiq X2 / X4 on HTG (KU060)

  - total size = 64MB
  - calibration storage = 0KB
  - user FPGA storage starts at 8MB
  - sector size is 256KB
  --> available space = 64MB - 8MB - 0KB = 56MB / 256KB = 224 sectors available

  - max bitstream size is 24124908 bytes
  - metadata size is 8 bytes
  --> # of sectors for a bitstream = ceil( ( 24124908 + 8 ) / 256KB ) = 93 sectors

  --> number of slots = floor( 224 / 93 ) = 2 slots
*/
#define NR_SLOTS_HTG_KU060              2

/*
  manually calculated for Sidekiq X2 / X4 on HTG (KU115)

  - total size = 64MB
  - calibration storage = 0KB
  - user FPGA storage starts at 12MB
  - sector size is 256KB
  --> available space = 64MB - 12MB - 0KB = 52MB / 256KB = 208 sectors available

  - max bitstream size is 48251536 bytes
  - metadata size is 8 bytes
  --> # of sectors for a bitstream = ceil( ( 48251536 + 8 ) / 256KB ) = 185 sectors

  --> number of slots = floor( 208 / 185 ) = 1 slot
*/
#define NR_SLOTS_HTG_KU115              1


#define FLASH_CONFIG(_part,_fpga,_carrier,_nr,_sz) \
    {                                              \
        .part = _part,                             \
        .fpga_device = _fpga,                      \
        .fmc_carrier = _carrier,                   \
        .nr_slots = _nr,                           \
        .slot_size = _sz,                          \
    }

/** @brief helper MACRO for those configurations that do not have a carrier */
#define FLASH_CONFIG_NC(_part,_fpga,_nr,_sz)       \
    FLASH_CONFIG(_part,_fpga,skiq_fmc_carrier_not_applicable,_nr,_sz)


/***** TYPEDEFS *****/




/***** STRUCTS *****/

struct flash_config
{
    skiq_part_t part;
    skiq_fpga_device_t fpga_device;
    skiq_fmc_carrier_t fmc_carrier;

    uint8_t nr_slots;
    uint32_t slot_size;
};


/***** LOCAL VARIABLES *****/

static struct flash_config flash_configs[] =
{
    /* flash configurations for Sidekiq cards that do not have a carrier */
    FLASH_CONFIG_NC(skiq_mpcie,      skiq_fpga_device_xc6slx45t, NR_SLOTS_MPCIE,   SLOT_SIZE_MPCIE),
    FLASH_CONFIG_NC(skiq_m2,         skiq_fpga_device_xc7a50t,   NR_SLOTS_M2,      SLOT_SIZE_M2),

    /**
       @note Re-use the M.2-2280 definitions since M.2-2280 and NV100 have the same FPGA and same
       QSPI flash capacity
    */
    FLASH_CONFIG_NC(skiq_m2_2280,    skiq_fpga_device_xc7a50t,   NR_SLOTS_M2_2280, SLOT_SIZE_M2_2280),
    FLASH_CONFIG_NC(skiq_nv100,      skiq_fpga_device_xc7a50t,   NR_SLOTS_M2_2280, SLOT_SIZE_M2_2280),

    /* flash configurations for those with carriers */
    FLASH_CONFIG(skiq_x2, skiq_fpga_device_xcku060, skiq_fmc_carrier_htg_k800, NR_SLOTS_HTG_KU060, SLOT_SIZE_HTG_KU060),
    FLASH_CONFIG(skiq_x4, skiq_fpga_device_xcku060, skiq_fmc_carrier_htg_k800, NR_SLOTS_HTG_KU060, SLOT_SIZE_HTG_KU060),
    FLASH_CONFIG(skiq_x2, skiq_fpga_device_xcku115, skiq_fmc_carrier_htg_k800, NR_SLOTS_HTG_KU115, SLOT_SIZE_HTG_KU115),
    FLASH_CONFIG(skiq_x4, skiq_fpga_device_xcku115, skiq_fmc_carrier_htg_k800, NR_SLOTS_HTG_KU115, SLOT_SIZE_HTG_KU115),

    /* flash configurations for those with HTG-K810 carriers */
    FLASH_CONFIG(skiq_x2, skiq_fpga_device_xcku060, skiq_fmc_carrier_htg_k810, NR_SLOTS_HTG_KU060, SLOT_SIZE_HTG_KU060),
    FLASH_CONFIG(skiq_x4, skiq_fpga_device_xcku060, skiq_fmc_carrier_htg_k810, NR_SLOTS_HTG_KU060, SLOT_SIZE_HTG_KU060),
    FLASH_CONFIG(skiq_x2, skiq_fpga_device_xcku115, skiq_fmc_carrier_htg_k810, NR_SLOTS_HTG_KU115, SLOT_SIZE_HTG_KU115),
    FLASH_CONFIG(skiq_x4, skiq_fpga_device_xcku115, skiq_fmc_carrier_htg_k810, NR_SLOTS_HTG_KU115, SLOT_SIZE_HTG_KU115),
};
static uint8_t nr_flash_configs = ARRAY_SIZE(flash_configs);


/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
/** For a given card index, provide the caller with the number of accessible slots and the slot
    size.

    @param[in]    card        Sidekiq card index of interest
    @param[out]   p_nr_slots  (optional) Number of accessible slots for the specified card
    @param[out]   p_slot_size (optional) Size of flash config slot for the specified card

    @return int32_t
    @retval 0 Success
    @retval -ENODEV No entry in flash configuration array
 */
static int32_t slot_info( uint8_t card,
                          uint8_t *p_nr_slots,
                          uint32_t *p_slot_size )
{
    skiq_part_t part;
    skiq_fpga_device_t fpga_device;
    skiq_fmc_carrier_t fmc_carrier;
    uint32_t slot_size = 0;
    uint8_t i, nr_slots = 0;
    int32_t status = -ENODEV;

    part = _skiq_get_part(card);
    fpga_device = _skiq_get_fpga_device(card);
    fmc_carrier = _skiq_get_fmc_carrier(card);

    debug_print("Looking up slot info for a Sidekiq '%s' with FPGA '%s' and FMC carrier '%s' on "
                "card %u\n", part_cstr(part), fpga_device_cstr(fpga_device),
                fmc_carrier_cstr(fmc_carrier), card);

    for (i = 0; ( i < nr_flash_configs ) && ( status != 0 ); i++)
    {
        if ( ( part == flash_configs[i].part ) &&
             ( fpga_device == flash_configs[i].fpga_device ) &&
             ( fmc_carrier == flash_configs[i].fmc_carrier ) )
        {
            slot_size = flash_configs[i].slot_size;
            nr_slots = flash_configs[i].nr_slots;
            status = 0;
        }
    }

    if ( status == 0 )
    {
        if ( p_nr_slots != NULL )
        {
            *p_nr_slots = nr_slots;
        }

        if ( p_slot_size != NULL )
        {
            *p_slot_size = slot_size;
        }
    }

    return status;
}


/**************************************************************************************************/
/** For the provided (card,slot,nr_slots,slot_size)-tuple, calculate the bitstream's associated
    flash address.

    @param[in]  card             Sidekiq card index of interest
    @param[in]  slot             Flash config slot index of interest
    @param[in]  nr_slots         Number of accessible slots for the card
    @param[in]  slot_size        Slot size for the card
    @param[out] p_bitstream_addr Resulting bitstream flash address

    @return int32_t
    @retval 0 Success
    @retval -EINVAL Slot index exceed number of accessible slots
 */
static int32_t slot_to_bitstream_address( uint8_t card,
                                          uint8_t slot,
                                          uint8_t nr_slots,
                                          uint32_t slot_size,
                                          uint32_t *p_bitstream_addr )
{
    int32_t status = 0;

    CHECK_NULL_PARAM(p_bitstream_addr);

    if ( slot < nr_slots )
    {
        uint32_t address;

        status = flash_get_user_fpga_addr( card, &address );
        if ( 0 != status )
        {
            skiq_error("failed to get user FPGA image address for card %" PRIu8
                       " (status = %" PRIi32 ")\n", card, status);
        }
        else
        {
            *p_bitstream_addr = address + slot * slot_size;
            debug_print("Slot %u corresponds to flash address 0x%08X on card %u\n", slot,
                        *p_bitstream_addr, card );
        }
    }
    else
    {
        debug_print("Slot index %u exceeds number of accessible flash config slots %u for card %"
                    PRIu8 "\n", slot, nr_slots, card);
        status = -EINVAL;
    }

    return status;
}


/**************************************************************************************************/
/** For the provided (card,slot,nr_slots,slot_size)-tuple, calculate the metadata's associated flash
    address.

    @param[in]  card            Sidekiq card index of interest
    @param[in]  slot            Flash config slot index of interest
    @param[in]  nr_slots        Number of accessible slots for the card
    @param[in]  slot_size       Slot size for the card
    @param[out] p_metadata_addr Resulting metadata flash address

    @return int32_t
    @retval 0 Success
    @retval -EINVAL Slot index exceed number of accessible slots
 */
static int32_t slot_to_metadata_address( uint8_t card,
                                         uint8_t slot,
                                         uint8_t nr_slots,
                                         uint32_t slot_size,
                                         uint32_t *p_metadata_addr )
{
    int32_t status = 0;

    CHECK_NULL_PARAM(p_metadata_addr);

    if ( slot < nr_slots )
    {
        uint32_t address;

        status = flash_get_user_fpga_addr( card, &address );
        if ( 0 != status )
        {
            skiq_error("failed to get user FPGA image address for card %" PRIu8
                       " (status = %" PRIi32 ")\n", card, status);
        }
        else
        {
            /* metadata is placed at the END of the slot */
            *p_metadata_addr = address + (slot + 1) * slot_size - METADATA_SZ;
            debug_print("Metadata for slot %u corresponds to flash address 0x%08X on card %u\n",
                        slot, *p_metadata_addr, card );
        }
    }
    else
    {
        debug_print("Slot index %u exceeds number of accessible flash config slots %u for card %"
                    PRIu8 "\n", slot, nr_slots, card);
        status = -EINVAL;
    }

    return status;
}


/**************************************************************************************************/
/** A helper function that checks the file size is within bounds and also looks up the associated
    flash addresses for the bitstream and metadata for the specified card and slot.

    @param[in]    card             Sidekiq card index of interest
    @param[in]    slot             Flash config slot index of interest
    @param[in]    fp               FILE stream reference to check
    @param[out]   p_bitstream_addr (Optional) Resulting bitstream flash address
    @param[out]   p_metadata_addr  (Optional) Resulting metadata flash address

    @return int32_t
    @retval 0 Success
    @retval -EBADF FILE stream references a bad file descriptor
    @retval -EFBIG FILE stream reference points to a file that exceeds the flash config slot's size
    @retval -EINVAL Slot index exceed number of accessible slots
    @retval -ENODEV No entry in flash configuration array
 */
static int32_t check_file_and_get_flash_addresses( uint8_t card,
                                                   uint8_t slot,
                                                   FILE *fp,
                                                   uint32_t *p_bitstream_addr,
                                                   uint32_t *p_metadata_addr )
{
    int32_t status = 0;
    uint32_t slot_size;
    uint8_t nr_slots;

    /* 0. Grab slot information for card */
    status = slot_info( card, &nr_slots, &slot_size );

    /* 1. Limit file size + metadata to slot size */
    if ( status == 0 )
    {
        long stream_size = hal_file_stream_size(fp);

        if ( stream_size < 0 )
        {
            skiq_error("File stream (%p) references a bad file descriptor for card %u\n", fp, card);
            status = -EBADF;
        }
        else if ( stream_size + METADATA_SZ > slot_size )
        {
            skiq_error("File (%lu bytes) too large for slot size (%lu bytes) on card %u\n",
                       stream_size, (long)(slot_size - METADATA_SZ), card);
            status = -EFBIG;
        }
    }

    /* 2. Look up bitstream and metadata flash address by slot */
    if ( ( status == 0 ) && ( p_bitstream_addr != NULL ) )
    {
        status = slot_to_bitstream_address( card, slot, nr_slots, slot_size, p_bitstream_addr );
    }

    if ( ( status == 0 ) && ( p_metadata_addr != NULL ) )
    {
        status = slot_to_metadata_address( card, slot, nr_slots, slot_size, p_metadata_addr );
    }

    return status;
}


/**************************************************************************************************/
/** Checks the associated flash memory for the presence of the FPGA fallback configuration bitstream
    (golden).  This function is used here to protect against users storing a bitstream without
    having a way to fallback to a usable configuration.

    @param[in]   card   Sidekiq card index of interest

    @return int32_t
    @retval 0 Success
    @retval -EACCES No golden FPGA bitstream found in flash memory
    @retval -EIO Failed to read from flash memory
 */
static int32_t check_golden_presence( uint8_t card )
{
    int32_t status;
    bool present;

    status = flash_verify_golden_present(card, &present);
    if ( status == 0 )
    {
        if ( !present )
        {
            skiq_error("Golden FPGA bitstream not present in flash, cannot update user FPGA image "
                       "on card %u!\n", card);
            status = -EACCES;
        }
    }
    else
    {
        skiq_error("Reading golden image from flash failed on card %u\n", card);
        status = -EIO;
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_save_fpga_config( uint8_t card,
                                FILE *p_file )
{
    int32_t status = 0;
    uint32_t bitstream_addr = 0;

    status = check_file_and_get_flash_addresses( card, 0, p_file, &bitstream_addr, NULL );

    /* Check for the presence of the FPGA fallback bitstream before committing anything to flash */
    if ( status == 0 )
    {
        status = check_golden_presence( card );
    }

    /* Store file contents ONLY -- legacy method */
    if ( status == 0 )
    {
        status = flash_write_file_fpga( card, bitstream_addr, p_file );
    }

    return status;

} /* flash_save_fpga_config */


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_save_fpga_config_to_flash_slot( uint8_t card,
                                              uint8_t slot,
                                              FILE *p_file,
                                              uint64_t metadata )
{
    int32_t status = 0;
    uint32_t bitstream_addr = 0, metadata_addr = 0;

    status = check_file_and_get_flash_addresses( card, slot, p_file, &bitstream_addr,
                                                 &metadata_addr );

    /* Check for the presence of the FPGA fallback bitstream before committing anything to flash */
    if ( status == 0 )
    {
        status = check_golden_presence( card );
    }

    /* Store metadata and file contents */
    if ( status == 0 )
    {
        status = flash_write_file_fpga_with_metadata( card, bitstream_addr, p_file,
                                                      metadata_addr, metadata );
    }

    return status;

} /* flash_save_fpga_config_to_flash_slot */


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_verify_fpga_config( uint8_t card,
                                  FILE *p_file )
{
    int32_t status = 0;
    uint32_t bitstream_addr = 0;

    status = check_file_and_get_flash_addresses( card, 0, p_file, &bitstream_addr, NULL );

    /* Verify file contents ONLY -- legacy method */
    if ( status == 0 )
    {
        status = flash_verify_file( card, bitstream_addr, p_file );
    }

    return status;

} /* flash_verify_fpga_config */


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_verify_fpga_config_in_flash_slot( uint8_t card,
                                                uint8_t slot,
                                                FILE *p_file,
                                                uint64_t metadata )
{
    int32_t status = 0;
    uint32_t bitstream_addr = 0, metadata_addr = 0;

    status = check_file_and_get_flash_addresses( card, slot, p_file, &bitstream_addr,
                                                 &metadata_addr );

    /* Verify metadata and file contents */
    if ( status == 0 )
    {
        status = flash_verify_file_with_metadata( card, bitstream_addr, p_file,
                                                  metadata_addr, metadata );
    }

    return status;

} /* flash_verify_fpga_config_in_flash_slot */


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_read_fpga_config_flash_slot_metadata( uint8_t card,
                                                    uint8_t slot,
                                                    uint64_t *p_metadata )
{
    int32_t status = 0;
    uint32_t metadata_addr = 0, slot_size;
    uint8_t nr_slots;

    /* 0. Grab slot information for card */
    status = slot_info( card, &nr_slots, &slot_size );

    /* 1. Look up metadata's flash address by slot */
    if ( status == 0 )
    {
        status = slot_to_metadata_address( card, slot, nr_slots, slot_size, &metadata_addr );
    }

    /* 2. Read metadata from flash address */
    if ( status == 0 )
    {
        status = flash_read( card, metadata_addr, (uint8_t *)p_metadata, METADATA_SZ );
    }

    /* 3. Translate from little endian to host */
    if ( status == 0 )
    {
        *p_metadata = le64toh(*p_metadata);
    }

    return status;

} /* flash_read_fpga_config_flash_slot_metadata */


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_read_fpga_config_flash_slots_avail( uint8_t card,
                                                  uint8_t *p_nr_slots )
{
    int32_t status = 0;

    /* 1. Look up number of slots based on flash size and FPGA type, default to 0 slots if
       `slot_info` returns -ENODEV */
    status = slot_info( card, p_nr_slots, NULL );
    if ( status == -ENODEV )
    {
        *p_nr_slots = 0;
        status = 0;
    }

    return status;

} /* flash_read_fpga_config_flash_slots_avail */


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t flash_load_fpga_by_slot ( uint8_t card,
                                  uint8_t slot )
{
    int32_t status = 0;
    uint32_t bitstream_addr = 0, slot_size;
    uint8_t nr_slots;

    /* 0. Grab slot information for card */
    status = slot_info( card, &nr_slots, &slot_size );

    /* 1. Look up bitstreams's flash address by slot */
    if ( status == 0 )
    {
        status = slot_to_bitstream_address( card, slot, nr_slots, slot_size, &bitstream_addr );
    }

    /* 2. Load FPGA bitstream by address */
    if ( status == 0 )
    {
        status = flash_load_fpga_by_address( card, bitstream_addr );
    }

    return status;

} /* flash_load_fpga_by_slot */
