/**
 * @file   usb_flash.c
 * @author <mreutman@epiqsolutions.com>
 * @date   2017-05-23 17:37:49
 *
 * @brief
 *
 * <pre>
 * Copyright 2017 - 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <errno.h>

#include <inttypes.h>

#include "usb_flash.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_xport.h"
#include "usb_private.h"
#include "usb_interface.h"

#include "sidekiq_flash_types.h"
#include "sidekiq_flash_private.h"

/* enable debug_print and debug_print_plain when DEBUG_FLASH is defined */
#if (defined DEBUG_FLASH)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/***** DEFINES *****/

/* determined empirically, the Intel NUC with a Sidekiq M.2 has been observed to take up to 6
 * retries before succeeding */
#define MAX_NUM_LOAD_RETRIES 10


/***** EXTERN FUNCTIONS *****/

int32_t usb_identify_chip(uint8_t card, flash_card_information_t * const p_card_info,
    flash_part_information_t * p_part)
{
    int32_t status = 0;
    uint8_t index = 0;
    FlashChip chipId = FLASH_CHIP_UNKNOWN;
    flash_part_information_t localPart = FLASH_PART_INFORMATION_INITIALIZER;

    (void) p_card_info;

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_read_flash_manufacturer_info(index, &(chipId),
                    &(localPart.jedec_id_manufacturer), &(localPart.manufacturer_id_type),
                    &(localPart.manufacturer_id_capacity));
    }

    if ((-ENOTSUP == status) && (NULL != p_part))
    {
        debug_print("Sidekiq firmware on card %" PRIu8 " does not support reading Flash"
            " information; assuming standard Flash chip.\n", card);

        /**
            @todo   For now, assume the 32Mbit Micron N25 as it was the legacy part that most USB
                    capable cards (such as the mPCIe and m.2) were using; the MT25Q should be
                    compatible.

            @todo   The 0x16 represents 32Mbit for the Micron N25; this is a magic number as it
                    isn't being defined anywhere. Think about making constants for the manufacturer
                    specific size constants?
        */
        localPart.chip = flash_chip_type_micron_n25;
        localPart.jedec_id_manufacturer = FLASH_CODE_JEDEC_MFR_MICRON_1;
        localPart.manufacturer_id_type = FLASH_CODE_MEMORY_TYPE_MICRON_N25;
        localPart.manufacturer_id_capacity = 0x16;

        status = 0;
    }

    if ((0 == status) && (NULL != p_part))
    {
        /*
            Translate the FlashChip type (found in the FX2 firmware and thus a private enum) into
            a flash_chip_type_t (which is safe to pass up to upper layers)
        */
        /**
            @todo   This code assumes that the FX2 is always attached to a Sidekiq that has Flash
                    (as there's no "no flash" type currently defined).
        */
        switch (chipId)
        {
        case FLASH_CHIP_MICRON_N25:
            localPart.chip = flash_chip_type_micron_n25;
            break;
        case FLASH_CHIP_MICRON_MT25Q:
            localPart.chip = flash_chip_type_micron_mt25q;
            break;
        case FLASH_CHIP_WINBOND_W25:
            localPart.chip = flash_chip_type_winbond_w25;
            break;
        case FLASH_CHIP_ISSI_IS25:
            localPart.chip = flash_chip_type_issi_is25;
            break;
        case FLASH_CHIP_MACRONIX_MX25:
            localPart.chip = flash_chip_type_macronix_mx25;
            break;
        case FLASH_CHIP_SPANSION_S25:
            localPart.chip = flash_chip_type_spansion_s25;
            break;
        case FLASH_CHIP_UNKNOWN:
        default:
            localPart.chip = flash_chip_type_unknown;
            if (FLASH_CHIP_UNKNOWN != chipId)
            {
                skiq_unknown("Invalid Flash chip ID (%d) in use by card %" PRIu8 "; returning"
                    " 'unknown'\n", (int) chipId, card);
            }
            break;
        }

        *p_part = localPart;
    }

    return status;
}

int32_t usb_read_page(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    int32_t status=0;
    uint8_t index=0;

    // TODO: this translation layer can probably be refactored...

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_read_flash(index, p_transfer->buf, p_transfer->addr, p_transfer->len);
    }

    return (status);
}

int32_t usb_write_page(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    int32_t status = 0;
    uint8_t index = 0;

    // TODO: this translation layer can probably be refactored...

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_write_flash(index, 
                                 p_transfer->buf,
                                 p_transfer->addr, 
                                 p_transfer->len);
    }

    return (status);
}

int32_t usb_erase_sector(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    int32_t status = 0;
    uint8_t index = 0;

    // TODO: this translation layer can probably be refactored...

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_erase_flash_sector(index, p_transfer->addr);
    }

    return (status);
}


/** Poll the state of the FPGA configuration for up to a second. */
static int32_t
_poll_fpga_config( uint8_t index )
{
    const uint32_t poll_timeout_ms = 1000;
    const uint32_t poll_delay_ms = 10;
    uint32_t poll_elapsed_ms = 0;
    uint8_t fpga_config = 0;
    int32_t status = 0;

    status = usb_read_fpga_config(index, &fpga_config);
    while ( (0 == status) &&
            (poll_elapsed_ms < poll_timeout_ms) &&
            (1 != fpga_config) )
    {
        /* sleep before checking FPGA config status */
        hal_millisleep(poll_delay_ms);
        poll_elapsed_ms += poll_delay_ms;

        status = usb_read_fpga_config(index, &fpga_config);
    }

    /* if the calls to usb_read_fpga_config() all returned 0 (success), but the loop timed out,
     * indicate that case by returning -ETIMEDOUT */
    if ( ( status == 0 ) && ( fpga_config != 1 ) )
    {
        status = -ETIMEDOUT;
    }

    return status;
}


static int32_t
_load_fpga( uint8_t card,
            flash_transfer_t* p_transfer )
{
    int32_t status = 0;
    uint8_t load_retries = MAX_NUM_LOAD_RETRIES;
    const int32_t sleep_before_up_ms = 100;
    uint8_t index = 0;

    // get the USB index from the card
    if ( ( status = usb_get_index(card, &index) ) != 0 )
    {
        return (status);
    }

    /* take down FPGA transport and try usb_load_fpga_from_flash up to MAX_NUM_LOAD_RETRIES */
    status = sidekiq_xport_fpga_down(card);
    if( 0 == status )
    {
        do
        {
            /* trigger a "load from flash", then poll the configuration state */
            status = usb_load_fpga_from_flash(index);
            if ( 0 == status )
            {
                status = _poll_fpga_config( index );
            }

            if ( 0 == status )
            {
                /* sleep before bringing up FPGA transport */
                hal_millisleep( sleep_before_up_ms );

                /* try to bring up FPGA transport */
                status = sidekiq_fpga_bring_up(card);
            }
            load_retries--;

        } while ( ( 0 != status ) && ( load_retries > 0 ) );

        if ( status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "Loading FPGA from flash failed after %d tries\n",
                      MAX_NUM_LOAD_RETRIES);
        }
        else if ( load_retries < (MAX_NUM_LOAD_RETRIES - 1) )
        {
            _skiq_log(SKIQ_LOG_DEBUG,
                      "Loading FPGA from flash took %d tries\n",
                      MAX_NUM_LOAD_RETRIES - load_retries);
        }
    }
    else
    {
        _skiq_log(SKIQ_LOG_WARNING,
                  "FPGA transport did not stop properly, a host reboot "
                  "may be required to restore FPGA transport\n");
    }

    return (status);
}


static int32_t
_load_fpga_by_address( uint8_t card,
                         flash_transfer_t* p_transfer,
                         uint32_t load_address )
{
    int32_t status = 0;

    /* take down FPGA transport and provide a load address */
    status = sidekiq_xport_fpga_down_reload(card, load_address);
    if( 0 == status )
    {
        /* bring up transport, emits its own error messages */
        status = sidekiq_fpga_bring_up(card);
    }
    else
    {
        _skiq_log(SKIQ_LOG_WARNING,
                  "FPGA transport did not stop properly, a host reboot "
                  "may be required to restore FPGA transport\n");
    }

    return (status);
}


/** @brief dispatch to functions depending on self reloading capabilities */
int32_t usb_load_fpga(uint8_t card, flash_transfer_t* p_transfer,
    flash_card_information_t * const p_card_info)
{
    int32_t status = 0;

    /* dispatch based on has_self_reload flag */
    if ( p_card_info->params.has_self_reload )
    {
        status = _load_fpga_by_address( card, p_transfer, p_transfer->addr );
    }
    else
    {
        status = _load_fpga( card, p_transfer );
    }

    return (status);
}

/** @brief Queries the flash lock status. */
int32_t usb_flash_is_golden_locked(uint8_t card, flash_card_information_t * const p_card_info,
        bool *p_is_locked)
{
    int32_t status = 0;
    uint8_t index = 0;
    bool is_locked = false;

    (void) p_card_info;

    // TODO: this translation layer can probably be refactored...

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        status = usb_is_golden_locked(index, &is_locked);

        if (-ENODEV == status)
        {
            status = -EIO;
        }
        else
        {
            if ((0 == status) && (is_locked))
            {
                skiq_info("Golden FPGA flash sectors are protected on card %u\n", card);
            }
            else if ((0 == status) && (!is_locked))
            {
                skiq_warning("Golden FPGA flash sectors are NOT protected on card %u\n", card);
            }
            else
            {
                is_locked = true;
                skiq_warning("Golden FPGA flash sector protection is not expected on card %u\n",
                    card);
            }

            if (NULL != p_is_locked)
            {
                *p_is_locked = is_locked;
            }
        }
    }
    else
    {
        skiq_warning("Failed to find USB index for card %u (status = %" PRIi32 ")\n", card, status);
    }

    if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    return (status);
}

/**
    @brief  Locks the flash sectors associated with the golden FPGA bitstream, only on
            Sidekiq parts that support it.
*/
int32_t usb_flash_lock_golden_sectors(uint8_t card, flash_card_information_t * const p_card_info)
{
    int32_t status = 0;
    uint8_t index = 0;

    (void) p_card_info;

    // TODO: this translation layer can probably be refactored...

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        skiq_info("Protecting golden FPGA flash sectors on card %u\n", card);
        status = usb_lock_golden_sectors(index);

        if (-ENOSYS == status)
        {
            skiq_warning("The firmware on card %u reports that it does not support protecting"
                " sectors\n", card);
        }
        else if (-ENOTSUP == status)
        {
            skiq_warning("Card %u reports that it does not support protecting sectors\n", card);
        }
    }
    else
    {
        skiq_warning("Failed to find USB index for card %u (status = %" PRIi32 ")\n", card, status);
    }

    if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    return (status);
}

/**
    @brief  Unlocks the flash sectors associated with the golden FPGA bitstream, only
            on Sidekiq parts that support it.
*/
int32_t usb_flash_unlock_golden_sectors(uint8_t card, flash_card_information_t * const p_card_info)
{
    int32_t status = 0;
    uint8_t index = 0;

    (void) p_card_info;

    // TODO: this translation layer can probably be refactored...

    if( (status=usb_get_index(card, &index)) == 0 )
    {
        skiq_info("Removing protection on golden FPGA flash sectors on card %u\n", card);
        status = usb_unlock_golden_sectors(index);

        if (-ENOSYS == status)
        {
            skiq_warning("The firmware on card %u reports that it does not support protecting"
                " sectors\n", card);
        }
        else if (-ENOTSUP == status)
        {
            skiq_warning("Card %u reports that it does not support protecting sectors\n", card);
        }
    }
    else
    {
        skiq_warning("Failed to find USB index for card %u (status = %" PRIi32 ")\n", card, status);
    }

    if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    return (status);
}

/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
#if (defined ATE_SUPPORT)

/**
    @brief  Read the Flash chip's manufacturing unique ID (if present)
*/
static int32_t
usb_get_unique_id( uint8_t card, flash_card_information_t * const p_card_info, uint8_t *p_id,
    uint8_t *p_id_length)
{
    int32_t status = 0;
    uint8_t index = 0;

    (void) p_card_info;

    status = usb_get_index(card, &index);
    if ( 0 == status )
    {
        status = usb_read_flash_id(index, p_id, p_id_length);

        if ( -EIO == status )
        {
            skiq_warning("Failed to read unique ID from Flash chip on card %" PRIu8 "\n", card);
        }
    }

    if ( ( status != 0 ) && ( status != -ENOTSUP ) )
    {
        /* rewrite any other error that's not an ENOTSUP as a generic I/O error */
        status = -EIO;
    }

    return (status);
}

#endif  /* ATE_SUPPORT */
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */

/***** GLOBAL VARIABLES *****/

flash_functions_t usb_flash = {
    .init = NULL,
    .identify_chip = usb_identify_chip,
#if (defined ATE_SUPPORT)
    .get_unique_id = usb_get_unique_id,
#else
    .get_unique_id = NULL,
#endif /* defined ATE_SUPPORT */
    .erase_sector = usb_erase_sector,
    .read_page = usb_read_page,
    .write_page = usb_write_page,
    .lock_golden_sectors = usb_flash_lock_golden_sectors,
    .unlock_golden_sectors = usb_flash_unlock_golden_sectors,
    .is_golden_locked = usb_flash_is_golden_locked,
    .load_fpga = usb_load_fpga,
};

