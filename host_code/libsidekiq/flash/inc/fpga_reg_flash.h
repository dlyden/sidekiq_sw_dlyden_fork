#ifndef __FPGA_REG_FLASH_H__
#define __FPGA_REG_FLASH_H__

/*! \file sidekiq_flash.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** INCLUDES *****/

#include <stdint.h>
#include "sidekiq_flash.h"
#include "sidekiq_flash_private.h"

/***** EXTERN VARIABLES *****/

extern flash_functions_t fpga_reg_flash;
extern flash_functions_t fpga_reg_flash_only_load;

#endif

