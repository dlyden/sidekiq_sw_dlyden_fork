#ifndef __SIDEKIQ_FLASH_CONFIG_SLOTS_H__
#define __SIDEKIQ_FLASH_CONFIG_SLOTS_H__

/**
 * @file   sidekiq_flash_config_slots.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec 11 10:46:59 2019
 * 
 * @brief Provides an interface for accessing a Sidekiq card's available FPGA configuration slots
 * resident in Flash memory.
 * 
 * <pre>
 * Copyright 2019-2020 Epiq Solutions, All Rights Reserved
 *</pre>
 */

/***** INCLUDES *****/

#include <stdio.h>
#include <stdint.h>


/***** DEFINES *****/

#define FLASH_CONFIG_DEFAULT_SLOT       0
#define FLASH_CONFIG_DEFAULT_METADATA   UINT64_MAX


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/


/**************************************************************************************************/
/**
   This function stores the FPGA bitstream referenced by @p p_file in the default (slot index = 0)
   flash configuration slot.

   @param[in] card     requested Sidekiq card ID
   @param[in] p_file   FILE stream reference for the requested FPGA bitstream

   @return 0 on success, else a negative errno value
   @retval -EBADF      if the FILE stream references a bad file descriptor
   @retval -ENODEV     if no entry is found in the flash configuration array
   @retval -EACCES     if no golden FPGA bitstream is found in flash memory
   @retval -EIO        if the transport failed to read from flash memory
   @retval -EINVAL     if @p p_file is NULL
   @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
   @retval -ENOTSUP    if Flash access isn't supported for this card
   @retval -EFBIG      if the write would exceed Flash address boundaries and/or flash config slots's size
   @retval -EFAULT     if the file specified by @p p_file doesn't contain an FPGA sync word
   @retval -ENOEXEC    if the specified file doesn't contain a sync word
*/
int32_t flash_save_fpga_config( uint8_t card,
                                FILE *p_file );


/**************************************************************************************************/
/**
   This function stores the FPGA bitstream referenced by @a p_file in the flash configuration slot
   specified by @a slot.

   @param[in] card      requested Sidekiq card ID
   @param[in] slot      requested flash configuration slot
   @param[in] p_file    FILE stream reference for the requested FPGA bitstream
   @param[in] metadata  metadata to associate with the FPGA bitstream at the specified slot

   @return 0 on success, else a negative errno value
   @retval -EBADF      if the FILE stream references a bad file descriptor
   @retval -ENODEV     if no entry is found in the flash configuration array
   @retval -EACCES     if no golden FPGA bitstream is found in flash memory
   @retval -EIO        if the transport failed to read from flash memory
   @retval -EINVAL     if @p p_file is NULL
   @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
   @retval -ENOTSUP    if Flash access isn't supported for this card
   @retval -EFBIG      if the write would exceed Flash address boundaries and/or flash config slots's size
   @retval -EFAULT     if the file specified by @p p_file doesn't contain an FPGA sync word
   @retval -ENOEXEC    if the specified file doesn't contain a sync word
 */
int32_t flash_save_fpga_config_to_flash_slot( uint8_t card,
                                              uint8_t slot,
                                              FILE *p_file,
                                              uint64_t metadata );


/**************************************************************************************************/
/**
   This function verifies the contents of the FPGA bitstream referenced by @p p_file against the
   stored FPGA bitstream in the default (slot index = 0) flash configuration slot.

   @param[in] card     requested Sidekiq card ID
   @param[in] p_file   FILE stream reference for the requested FPGA bitstream

   @return 0 on success, else a negative errno value
   @retval -EBADF      if the FILE stream references a bad file descriptor
   @retval -EFBIG      if the FILE stream reference points to a file that exceeds the flash config slot's size
   @retval -ENODEV     if no entry is found in the flash configuration array
   @retval -ENOTSUP    if Flash access isn't supported for this card
*/
int32_t flash_verify_fpga_config( uint8_t card,
                                  FILE *p_file );


/**************************************************************************************************/
/**
   This function verifies the contents of the FPGA bitstream referenced by @p p_file against the
   stored FPGA bitstream in the flash configuration slot specified by @p slot.

   @param[in] card      requested Sidekiq card ID
   @param[in] slot      requested flash configuration slot
   @param[in] p_file    FILE stream reference for the requested FPGA bitstream
   @param[in] metadata  metadata to verify at the specified slot

   @return 0 on success, else a negative errno value
   @retval -EBADF      if the FILE stream references a bad file descriptor
   @retval -EFBIG      if the FILE stream reference points to a file that exceeds the flash config slot's size
   @retval -EINVAL     if the @p slot index exceed number of accessible slots
   @retval -ENODEV     if no entry is found in the flash configuration array
   @retval -ENOTSUP    if Flash access isn't supported for this card
*/
int32_t flash_verify_fpga_config_in_flash_slot( uint8_t card,
                                                uint8_t slot,
                                                FILE *p_file,
                                                uint64_t metadata );


/**************************************************************************************************/
/**
   This function provides the metadata associated with the flash configuration slot specified by @p
   slot.

   @param[in] card         requested Sidekiq card ID
   @param[in] slot         requested flash configuration slot
   @param[out] p_metadata  populated with retrieved metadata when return value indicates success

   @return 0 on success, else a negative errno value
   @retval -ENODEV     if no entry is found in the flash configuration array
   @retval -EINVAL     if @p p_metadata is NULL
   @retval -EINVAL     if the @p slot index exceed number of accessible slots
   @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
   @retval -ENOTSUP    if Flash access isn't supported for this card
   @retval -EFBIG      if the read would exceed Flash address boundaries
*/
int32_t flash_read_fpga_config_flash_slot_metadata( uint8_t card,
                                                    uint8_t slot,
                                                    uint64_t *p_metadata );


/**************************************************************************************************/
/**
   This function provides the number of available flash configuration slots for the requested
   Sidekiq card.

   @param[in] card         requested Sidekiq card ID
   @param[out] p_nr_slots  populated with the number of flash configuration slots when return value indicates success

   @return 0 on success, else a negative errno value
   @retval -ENODEV     if no entry is found in the flash configuration array
 */
int32_t flash_read_fpga_config_flash_slots_avail( uint8_t card,
                                                  uint8_t *p_nr_slots );


/**************************************************************************************************/
/**
   Loads the FPGA with the contents stored in flash memory from a specified slot

   @param[in] card     requested Sidekiq card ID
   @param[in] slot     requested FPGA image slot

   @return 0 on success, else a negative errno value
   @retval -ENODEV     if no entry is found in the flash configuration array
   @retval -EINVAL     if the @p slot index exceed number of accessible slots
   @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
   @retval -ENOTSUP    if Flash access isn't supported for this card
*/
int32_t flash_load_fpga_by_slot ( uint8_t card,
                                  uint8_t slot );


#endif  /* __SIDEKIQ_FLASH_CONFIG_SLOTS_H__ */
