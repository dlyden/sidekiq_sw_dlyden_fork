#ifndef __USB_FLASH_H__
#define __USB_FLASH_H__

/*! \file sidekiq_flash.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017,2019 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** INCLUDES *****/

#include <stdint.h>
#include "sidekiq_flash.h"
#include "sidekiq_flash_private.h"

/***** EXTERN VARIABLES *****/

extern flash_functions_t usb_flash;

#endif
