#ifndef __SIDEKIQ_FLASH_H__
#define __SIDEKIQ_FLASH_H__

/*! \file sidekiq_flash.h
 * \brief 
 *  
 * <pre>
 * Copyright 2017 - 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "sidekiq_api.h"
#include "sidekiq_xport_api.h"
#include "sidekiq_private.h"

#include "sidekiq_flash_types.h"
#include "sidekiq_flash_config_slots.h"

/***** EXTERN FUNCTIONS  *****/

/*****************************************************************************/
/**
    @brief  Selects the appropriate flash access functions according to the provided transport type
    
    @param[in]  card         Sidekiq card ID of interest
    @param[in]  type         transport type to use for flash access
*/
void flash_select_functions_for_card(uint8_t card, skiq_xport_type_t type);

/*****************************************************************************/
/**
    @brief  Initialize flash functionality for the specified card
    
    @param[in]  card        Sidekiq card ID of interest

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -EIO        if the Flash part or parameters couldn't be determined for this card
    @retval -E2BIG      if the flash parameters are unsupported (for example, the page size
                        exceeds ::FLASH_PAGE_MAX_SIZE)
*/
int32_t flash_init_card( uint8_t card );

/*****************************************************************************/
/**
    @brief  Shutdown flash functionality for the specified card
    
    @param[in]  card        Sidekiq card ID of interest

    @return 0 on success, else an errno
*/
int32_t flash_exit_card( uint8_t card );

/*****************************************************************************/
/**
    @brief Obtains flash address of the user FPGA configuration.
    
    @param[in]  card        Sidekiq card ID of interest
    @param[out] p_addr      If not NULL, the address of user FPGA

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if the card part information couldn't be determined
*/
int32_t flash_get_user_fpga_addr( uint8_t card, uint32_t *p_addr );

/*****************************************************************************/
/**
    @brief Obtains flash address of the golden FPGA configuration.

    @param[in]  card        Sidekiq card ID of interest
    @param[out] p_addr      If not NULL, the address of golden FPGA

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if the card part information couldn't be determined
*/
int32_t flash_get_golden_fpga_addr( uint8_t card, uint32_t *p_addr );

/*****************************************************************************/
/**
    @brief Verifies the presence of the golden FPGA in flash memory.

    @param[in]  card        Sidekiq card ID of interest
    @param[out] p_present   If not NULL, true if present

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
*/
int32_t flash_verify_golden_present(uint8_t card, bool *p_present);

/*****************************************************************************/
/**
    @brief Reads data from the flash chip.

    @param[in]  card        Sidekiq card ID of interest
    @param[in]  addr        address in flash to read from
    @param[out] p_buf       pointer to buffer to store data; this should not be NULL
    @param[in]  len         the number of bytes to read at the given address

    @return 0 on success, else an errno
    @retval -EINVAL     if @p p_buf is NULL
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
    @retval -EFBIG      if the read would exceed Flash address boundaries
*/
int32_t flash_read(uint8_t card, uint32_t addr, uint8_t* p_buf, uint32_t len);

/*****************************************************************************/
/**
    @brief Writes data to the flash chip.

    @param[in]  card        Sidekiq card ID of interest
    @param[in]  addr        address in flash to write to
    @param[in]  p_buf       pointer to buffer to write data; this should not be NULL
    @param[in]  len         the number of bytes in @p p_buf to write

    @return 0 on success, else an errno
    @retval -EINVAL     if @p p_buf is NULL
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
    @retval -EFBIG      if the write would exceed Flash address boundaries
*/
int32_t flash_write(uint8_t card, uint32_t addr, uint8_t* p_buf, uint32_t len);

/*****************************************************************************/
/**
    @brief Writes the contents of a file to flash.
    
    @param[in]  card        Sidekiq card ID of interest
    @param[in]  addr        address in flash to write to
    @param[in]  p_file      pointer to file to write to flash; this must already be open

    @return 0 on success, else an errno
    @retval -EINVAL     if @p p_file is NULL
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
    @retval -EFBIG      if the write would exceed Flash address boundaries
    @retval -EIO        if the file could not be read from
    @retval -ENOEXEC    if the specified file doesn't contain a sync word
*/
int32_t flash_write_file(uint8_t card, uint32_t addr, FILE* p_file);

/*****************************************************************************/
/**
    @brief  Writes the contents of a FPGA configuration file to flash. This function takes extra
            precautions in comparison to ::flash_write_file by writing the FPGA sync word as the
            very last action. This prevents partial writes of FPGA configurations from appearing
            to be valid due to the appearance of the sync word.
    
    @param[in]  card        Sidekiq card ID of interest
    @param[in]  addr        address in flash to write to
    @param[in]  p_file      FPGA configuration file to write to flash; this must already be open

    @return 0 on success, else an errno
    @retval -EINVAL     if @p p_file is NULL
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
    @retval -EFBIG      if the write would exceed Flash address boundaries
    @retval -EFAULT     if the file specified by @p p_file doesn't contain an FPGA sync word
    @retval -ENOEXEC    if the specified file doesn't contain a sync word
*/
int32_t flash_write_file_fpga(uint8_t card, uint32_t addr, FILE* p_file);


/**************************************************************************************************/
/**
   @brief Writes the contents of a FPGA configuration file and associated metadata to flash. This
   function takes extra precautions in comparison to ::flash_write_file by writing the FPGA sync
   word as the very last action. This prevents partial writes of FPGA configurations from appearing
   to be valid due to the appearance of the sync word.

   @param[in] card           requested Sidekiq card ID
   @param[in] addr           address in flash to write file contents to
   @param[in] p_file         FPGA configuration file to write to flash
   @param[in] metadata_addr  address in flash to write metadata to
   @param[in] metadata       metadata to write

   @return 0 on success, else a negative errno value
   @retval -EINVAL     if @p p_file is NULL
   @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
   @retval -ENOTSUP    if Flash access isn't supported for this card
   @retval -EFBIG      if the write would exceed Flash address boundaries
   @retval -EIO        if the file could not be read from
   @retval -EFAULT     if the file specified by @p p_file doesn't contain an FPGA sync word
   @retval -ENOEXEC    if the specified file doesn't contain a sync word
*/
int32_t flash_write_file_fpga_with_metadata( uint8_t card,
                                             uint32_t addr,
                                             FILE* p_file,
                                             uint32_t metadata_addr,
                                             uint64_t metadata );


/*****************************************************************************/
/**
    @brief Verifies the contents of a file against that found in flash.
    
    @param[in]  card        Sidekiq card ID of interest
    @param[in]  addr        address to begin verification
    @param[in]  p_file      pointer to file to perform verification against; this must already be
                            open

    @return 0 on success, else an errno
    @retval -EINVAL     if @p p_file is NULL
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
    @retval -EFBIG      if the file exceeds the Flash address boundaries
    @retval -EIO        if the file could not be read from
    @retval -EXDEV      if the verification failed
*/
int32_t flash_verify_file(uint8_t card, uint32_t addr, FILE* p_file);


/**************************************************************************************************/
/**
   @brief Verifies the contents of a file and specified metadata against those found in flash.

   @param[in] card           requested Sidekiq card ID
   @param[in] addr           address to begin verification of file contents
   @param[in] p_file         file to verify
   @param[in] metadata_addr  address to begin verification of metadata
   @param[in] p_file         metadata to verify

   @return 0 on success, else a negative errno value
   @retval -EINVAL     if @p p_file is NULL
   @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
   @retval -ENOTSUP    if Flash access isn't supported for this card
   @retval -EFBIG      if the file exceeds the Flash address boundaries
   @retval -EIO        if the file could not be read from
   @retval -EXDEV      if the verification failed
*/
int32_t flash_verify_file_with_metadata( uint8_t card,
                                         uint32_t addr,
                                         FILE* p_file,
                                         uint32_t metadata_addr,
                                         uint64_t metadata );


/*****************************************************************************/
/**
    @brief Loads the FPGA with the contents stored in flash memory.

    @param[in]  card        Sidekiq card ID of interest

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
*/
int32_t flash_load_fpga_default(uint8_t card);


/*****************************************************************************/
/**
    @brief  Loads the FPGA with the contents stored in flash memory from a specified slot

    @param[in] card     Sidekiq card ID of interest
    @param[in] slot     requested FPGA image slot

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card or if the function
                        isn't implemented
*/
int32_t flash_load_fpga_by_slot ( uint8_t card, uint8_t slot );

/*****************************************************************************/
/**
    @brief  Loads the FPGA with the contents stored in flash memory from a specified non-volatile
            memory address.

    @param[in] card         Sidekiq card ID of interest
    @param[in] load_address requested FPGA image address
    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if Flash access isn't supported for this card
*/
int32_t flash_load_fpga_by_address ( uint8_t card, uint32_t load_address );

/*****************************************************************************/
/**
    @brief  Queries the flash lock status.

    @param[in]  card        Sidekiq card ID of interest
    @param[out] p_is_locked If not NULL, true if the flash sectors are locked

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP Sidekiq card does not support locking flash sectors
    @retval -EIO an I/O error occurred while communicating with the flash
*/
int32_t flash_is_golden_locked( uint8_t card, bool *p_is_locked );

/**************************************************************************************************/
/**
    @brief  Locks the flash sectors associated with the golden FPGA bitstream, only on Sidekiq
            parts that support it.

    @param[in] card         Sidekiq card ID of interest

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP Sidekiq card does not support locking flash sectors
    @retval -EIO an I/O error occurred while communicating with the flash
*/
int32_t flash_lock_golden_sectors( uint8_t card );


/**************************************************************************************************/
/**
    @brief  Unlocks the flash sectors associated with the golden FPGA bitstream, only on Sidekiq
    parts that support it.

    @param[in] card         Sidekiq card ID of interest

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    Sidekiq card does not support locking flash sectors
    @retval -EIO an I/O error occurred while communicating with the flash
*/
int32_t flash_unlock_golden_sectors( uint8_t card );


/**************************************************************************************************/
/**
    @brief  Gets information about the Flash chip on the specified Sidekiq card

    @param[in]  card            Sidekiq card ID of interest
    @param[out] p_part          If not NULL, information about the Flash chip in use on the
                                specified card

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -EIO        if the Flash card wasn't detected for this card
*/
int32_t flash_get_information( uint8_t card, flash_part_information_t *p_part );


/**************************************************************************************************/
/**
    @brief  Retrieve the Flash chip's unique ID

    @param[in]  card            Sidekiq card ID of interest
    @param[out] p_id            If not NULL, the Flash chip's manufacturing unique ID (if present);
                                this array should be at least ::SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN
                                bytes long
    @param[out] p_id_length     If not NULL, the actual length of the unique ID in @p p_id (in
                                bytes)

    @return 0 on success, else an errno
    @retval -ENOENT     if the Flash data structure hasn't been initialized for this card
    @retval -ENOTSUP    if getting the Flash unique ID isn't supported for this card or this
                        function isn't implemented
*/
int32_t flash_get_unique_id( uint8_t card, uint8_t *p_id, uint8_t *p_id_length );


#endif /* __SIDEKIQ_FLASH_H__ */
