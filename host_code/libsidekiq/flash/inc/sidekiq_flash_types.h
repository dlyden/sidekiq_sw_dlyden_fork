/**
    @file   sidekiq_flash_types.h
    @brief  Header file defining types and constants used in the flash subsystem

    <pre>
    Copyright 2019 - 2020 Epiq Solutions, All Rights Reserved
    </pre>
*/

#ifndef __SIDEKIQ_FLASH_TYPES_H_
#define __SIDEKIQ_FLASH_TYPES_H_

/***** DEFINES *****/

/** @brief  The maximum length (in bytes) of a Flash chip's manufacturing unique ID */
#ifndef SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN
#   define SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN      (32)
#endif

/***** TYPES *****/

/**
    @brief  An enumeration to identify supported Flash chips by manufacturer and part number
*/
typedef enum
{
    flash_chip_type_micron_n25 = 0,
    flash_chip_type_micron_mt25q,
    flash_chip_type_winbond_w25,
    flash_chip_type_issi_is25,
    flash_chip_type_macronix_mx25,
    flash_chip_type_spansion_s25,
    flash_chip_type_none,
    flash_chip_type_unknown,
} flash_chip_type_t;

/**
    @brief  Structure used to store information about the detected Flash part
*/
typedef struct
{
    /** @brief  The detected Flash chip information */
    flash_chip_type_t chip;
    /** @brief  The JEDEC Manufacturer code for the Flash part */
    uint8_t jedec_id_manufacturer;
    /** @brief  The Manufacturer type code for the Flash part */
    uint8_t manufacturer_id_type;
    /** @brief  The Manufacturer capacity code for the Flash part */
    uint8_t manufacturer_id_capacity;

    /** @todo   Add page size, sector size, total chip capacity; get via SFDP? */
} flash_part_information_t;

/** @brief  Static initializer macro for ::flash_part_information_t */
#define FLASH_PART_INFORMATION_INITIALIZER  \
{   \
    .chip = flash_chip_type_unknown, \
    .jedec_id_manufacturer = 0, \
    .manufacturer_id_type = 0, \
    .manufacturer_id_capacity = 0, \
}

#endif /* ifndef __SIDEKIQ_FLASH_TYPES_H_ */

