#ifndef __SIDEKIQ_FLASH_PRIVATE_H__
#define __SIDEKIQ_FLASH_PRIVATE_H__

/*! \file sidekiq_flash_private.h
 * \brief
 *
 * <pre>
 * Copyright 2017-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include "sidekiq_api.h"
#include "sidekiq_xport_api.h"
#include "sidekiq_private.h"
#include "sidekiq_fpga_ctrl.h"

#include "sidekiq_flash_types.h"
#include "flash_defines.h"

/***** DEFINES *****/

#define FLASH_PAGE_MAX_SIZE             (512)


/***** TYPEDEFS *****/

/* Forward define the Flash functions to avoid circular definitions. */
typedef struct flash_functions_t_ flash_functions_t;

/**
    @brief  Structure used to convey data regarding a given flash transaction. Note that functions
            receiving this struct may use some or all of the fields for a given transaction. Refer
            to documentation in flash_functions_t for details.
*/
typedef struct
{
    /** @brief  The Flash address associated with the transaction */
    uint32_t addr;
    /** @brief  Length of the data buffer (in bytes) */
    uint32_t len;
    /** @brief  The data buffer to read / write */
    uint8_t* buf;
} flash_transfer_t;



/** @brief Structure used to track parameters and capabilities of the flash
    chip selected.
*/
typedef struct
{
    /**
        @todo   These values don't really belong in this definition - the Flash identification
                routines should determine these values. However, the current Flash identification
                code doesn't automatically do this - so in the interim, just have these default
                values to help the Flash chip identification code out.
    */
    /** @brief  The size of a page (in bytes) for this card's Flash chip */
    uint32_t default_page_size;
    /** @brief  The size of a sector (in bytes) for this card's Flash chip */
    uint32_t default_sector_size;
    /** @brief  The total storage capacity (in bytes) of this card's Flash chip */
    uint32_t default_total_size;


    uint32_t slot_size;         /// Size allocated for one FPGA bitstream in bytes.

    uint32_t user_addr;         /// address of the 'user' FPGA image
    uint32_t recovery_addr;     /// address of the 'recovery' FPGA image

    uint32_t recovery_compare_len;             /// length of the 'recovery' FPGA image identifier
    uint32_t recovery_compare_offset;          /// address of the 'recovery' FPGA image identifier
    const uint8_t* recovery_compare_pattern;   /// pattern of the 'recovery' FPGA image identifier

    uint32_t read_fifo_depth;   /// depth (in bytes) of the FPGA SPI read FIFO
    uint32_t write_fifo_depth;  /// depth (in bytes) of the FPGA SPI write FIFO

    bool has_fx2_negotiation;           /// Support for calling FX2 functions for flash access
    bool has_4byte_addr;                /// Support for 4 byte addressing
    bool has_quad_read, has_quad_write; /// Support for quad SPI reads / writes
    bool has_32bit_read_fifo;           /// Support for reading data from FPGA at 32bits at a request
    bool has_spi_delay_counter;         /// Support for changing the SPI clock speed by using a delay counter
    bool has_flash_read_flag_register;  /// Support for flash ICs that have a 'read flag register'
    bool has_write_enable_latch_verify; /// Support for flash ICs that can verify WEL is asserted after a WREN

    uint32_t fpga_spi_transfer_delay_ns; /// Delay (in ns) to poll for SPI transfer completion
    uint32_t fpga_flash_status_delay_ns; /// Delay (in ns) to poll for Flash status

   /* Time delay to allow the FPGA to reload using the PCI only method.  This
    * field is ignored for USB access to flash. The time delay to first rescan
    * may be different depending on whether self reload is available */
    uint32_t pci_fpga_delay_to_first_rescan_sec;
    uint32_t pci_fpga_delay_to_first_rescan_sec_self_reload;
    uint32_t pci_fpga_max_rescan_period_sec;      /// does NOT include delay to first rescan

} flash_params_t;


/**
   @brief This structure is meant to be copied for a given card instance if it matches
   (skiq_part,skiq_fmc_carrier,skiq_fpga_device).  The flash_params pointer _reference_ should also
   be copied and its contents should be treated read-only.
 */
typedef struct
{
    /**
        @brief  The Sidekiq part
    */
    skiq_part_t skiq_part;

    /**
        @brief  The FMC carrier
    */
    skiq_fmc_carrier_t skiq_fmc_carrier;

    /**
        @brief  The FPGA device associated with the FMC carrier (if applicable) or the Sidekiq
                product
    */
    skiq_fpga_device_t skiq_fpga_device;

    /**
        @brief Reference to the flash parameters associated with the (part,FMC,FPGA) tuple.  Treat
        the referenced contents as read-only
    */
    flash_params_t *flash_params;

    /**
        @brief This field is card specific (depends on FPGA bitstream version) and reflects presence
        of support for FPGA to reload itself through a register transaction
    */
    bool has_self_reload;

} platform_flash_params_t;


typedef struct
{
    /** @brief  Flag to indicate that this data structure is initialized. */
    bool initialized;

    /** @brief  Lock to protect the card from being interrupted during Flash transactions. */
    /**
        @todo   Enable this in a future release to make sure that Flash calls are serialized;
                be sure to update FLASH_CARD_INFORMATION_INITIALIZER in sidekiq_flash.c.
    */
    //pthread_mutex_t cardLock;

    /**
        @todo   In a future release, add volatile shutdown variable (to indicate that
                `flash_exit_card()` was called and that pending Flash transactions should stop...
                or not begin at all); this should probaly work in conjunction with ::cardLock.
    */

    /** @brief  FPGA version during transaction */
    uint32_t fpga_ver;
    /** @brief  FPGA capabilities if fpga_ver >= 0x030800 */
    struct fpga_capabilities fpga_caps;
    /** @brief  Firmware version during transaction. */
    uint32_t fw_ver;

    /** @brief  Manufacturer information about the Flash chip available to the Sidekiq card */
    flash_part_information_t part_id;

    /** @brief  The size of a page (in bytes) for this card's Flash chip */
    uint32_t page_size;
    /** @brief  The size of a sector (in bytes) for this card's Flash chip */
    uint32_t sector_size;
    /** @brief  The total storage capacity (in bytes) of this card's Flash chip */
    uint32_t total_size;

    /** @brief  Transport / product specific information and settings */
    platform_flash_params_t params;

    /** @brief  Flash functions for this card (based upon transport, product, etc.) */
    /** @todo   Make const? */
    flash_functions_t *p_functions;

} flash_card_information_t;


/**
    @brief  Function pointers to flash transactions implemented over various transports.
*/
struct flash_functions_t_
{
    /**
        @brief  Function used to initialize the Flash chip on a per-transport basis

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_card_info The per-card Flash information

        @note   This call is generally made last by flash_card_init(), so @p p_card_info
                should be largely populated by the time this function is called

        @return 0 on success, else an errno
    */
    int32_t (*init)(uint8_t card, flash_card_information_t * const p_card_info);

    /**
        @brief  Function used to identify the Flash chip (part) used on a Sidekiq card

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_card_info The per-card Flash information
        @param[out] p_chip_id   If not NULL, the Flash identification information (see
                                ::flash_part_information_t)

        @return 0 on success, else an errno
        @retval -ENOTSUP    if Flash support is not present on this card
        @retval -EIO        if the Flash chip (part) couldn't be successfully read
    */
    int32_t (*identify_chip)(uint8_t card, flash_card_information_t * const p_card_info,
            flash_part_information_t * p_chip_id);

    /**
        @brief  Function used to get the Flash chip's manufacturing unique ID (if present)

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_card_info The per-card Flash information
        @param[out] p_id        If not NULL, the Flash chip's manufacturing unique ID (if present);
                                this array should be at least ::SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN
                                bytes long
        @param[out] p_id_length If not NULL, the actual length of the unique ID in @p p_id (in
                                bytes)

        @return 0 on success, else an errno
        @retval -ENOTSUP    if Flash support is not present on this card
    */
    int32_t (*get_unique_id)(uint8_t card, flash_card_information_t * const p_card_info,
            uint8_t *p_id, uint8_t *p_id_length);

    /**
        @brief  Function used to erase a sector in flash memory.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_transfer  Information about the sector to erase.
        @param[in]  p_card_info A pointer to the per-card Flash information entry

        For @p p_transfer, the following fields should be populated:
            addr:   the starting address of the sector to erase

        @return 0 on success, else an errno
        @retval -ENOTSUP    if Flash support is not present on this card
    */
    int32_t (*erase_sector)(uint8_t card, flash_transfer_t* p_transfer,
            flash_card_information_t * const p_card_info);

    /**
        @brief  Function used to read a page from flash memory.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_transfer  Information about the page to read.
        @param[in]  p_card_info A pointer to the per-card Flash information entry

        For @p p_transfer, the following fields should be populated:
            addr:   address of page to be read
            len:    number of bytes to read (up to p_card_info->page_size)

        @return 0 on success, else an errno
        @retval -ENOTSUP    if Flash support is not present on this card
    */
    int32_t (*read_page)(uint8_t card, flash_transfer_t* p_transfer,
            flash_card_information_t * const p_card_info);

    /**
        @brief  Function used to write a page of data to flash memory.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_transfer  Information about the page to read.
        @param[in]  p_card_info A pointer to the per-card Flash information entry

        For @p p_transfer, the following fields should be populated:
            addr:   address of page to be written
            len:    number of bytes to write (up to p_card_info->page_size)

        @return 0 on success, else an errno
        @retval -ENOTSUP    if Flash support is not present on this card
        @retval -E2BIG      if the write length exceeds the page size
    */
    int32_t (*write_page)(uint8_t card, flash_transfer_t* p_transfer,
            flash_card_information_t * const p_card_info);

    /**
        @brief  Function used to lock the flash sectors associated with the golden FPGA bitstream.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_card_info A pointer to the per-card Flash information entry

        @return 0 on success, else an errno
        @retval -EIO        Error communicating with flash part
        @retval -ENOTSUP    Sidekiq part does not support locking flash sectors
    */
    int32_t (*lock_golden_sectors)(uint8_t card, flash_card_information_t * const p_card_info);

    /**
        @brief  Function used to unlock the flash sectors associated with the golden FPGA bitstream.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_card_info A pointer to the per-card Flash information entry

        @return 0 on success, else an errno
        @retval -EIO        Error communicating with flash part
        @retval -ENOTSUP    Sidekiq part does not support locking flash sectors
    */
    int32_t (*unlock_golden_sectors)(uint8_t card, flash_card_information_t * const p_card_info);

    /**
        @brief  Function used to query whether or not the flash sectors associated with the golden
                FPGA bitstream are locked.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_card_info A pointer to the per-card Flash information entry
        @param[out] p_is_locked Boolean flag indicating lock status (if return value is 0)

        @return 0 on success, else an errno
        @retval -EIO     Error communicating with flash part
        @retval -ENOTSUP Sidekiq part does not support querying lock status
    */
    int32_t (*is_golden_locked)(uint8_t card, flash_card_information_t * const p_card_info,
            bool *p_is_locked);

    /**
        @brief Function used to program the FPGA with data stored in flash memory.

        @param[in]  card        The Sidekiq card ID
        @param[in]  p_transfer  Information about the image to program.
        @param[in]  p_card_info A pointer to the per-card Flash information entry

        For @p p_transfer, the following fields should be populated:
            addr:   if the card address supports it, the address of the image to program the
                    FPGA

        @return 0 on success, else an errno
        @retval -EIO     Error communicating with flash part
        @retval -ENOTSUP Sidekiq part does not support querying lock status
    */
    int32_t (*load_fpga)(uint8_t card, flash_transfer_t* p_transfer,
            flash_card_information_t * const p_card_info);
};


#endif

