#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**
    @brief  Convert an array into its ASCII hex string equivalent.

    @param[in]  str         The array to convert. This should not be NULL.
    @param[in]  strLen      The length of @str in bytes.
    @param[out] outStr      The buffer to place the encoded string into. This
                            should not be NULL.
    @param[in]  outStrLen   The length of @outStr in bytes.

    @return The length of the encoded string in @outStr in bytes if successful,
            else 0.
*/
extern uint32_t
hexlify(char *str, uint32_t strLen, char *outStr, uint16_t outStrLen);

/**
    @brief  Convert an ASCII hex string into its equivalent integer values.

    @param[in]  str         The ASCII hex string to convert. This should not
                            be NULL.
    @param[in]  strLen      The length of @str in bytes.
    @param[out] outStr      The buffer to place the decoded bytes into. This
                            should not be NULL.
    @param[in]  outStrLen   The length of @outStr in bytes.

    @return The length of the decoded data in bytes if successful, else 0.
*/
extern uint32_t
unhexlify(char *str, uint32_t strLen, char *outStr, uint16_t outStrLen);

/**
    @brief  This is essentially strtol() with a bunch of error checking.

    @param[in]  strPtr      A pointer to the string to decode.
    @param[out] endPtr      A pointer to the end of the decoded string. This
                            can be NULL if uninterested in the result.
    @param[in]  base        The base (10, 16, etc.) of the number to decode.
    @param[out] out         The location to put the decoded variable.

    @return true on success, else false.
*/
extern bool
convertToLong(const char *strPtr, char **endPtr, int base, long *out);

#ifdef __cpluscplus
}
#endif

#endif /* ifndef UTIL_H */

