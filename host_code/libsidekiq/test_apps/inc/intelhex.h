#ifndef INTELHEX_H
#define INTELHEX_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define INTEL_RECORD_HEADER_LEN       (5)
#define INTEL_MAX_RECORD_SIZE         (INTEL_RECORD_HEADER_LEN + 255)

typedef enum ResultCode
{
    FileReadError = -10,
    MemoryError = -9,
    InvalidFilename = -8,
    InvalidChecksum = -7,
    InvalidRecordType = -6,
    InvalidSizeField = -5,
    InvalidHexString = -4,
    InvalidStartCode = -3,
    InvalidStringSize = -2,
    InvalidParam = -1,
    Success = 0,
} ResultCode;

typedef enum RecordType
{
    Data = 0,
    Eof = 1,
    ExtendedSegment = 2,
    StartSegment = 3,
    ExtendedLinearAddr = 4,
    StartLinear = 5,
    MaxValue = 6,
    Unknown = 0xFF,
} RecordType;

struct Record
{
    uint8_t length;
    uint16_t addr;
    RecordType type;
    uint8_t data[INTEL_MAX_RECORD_SIZE];

    struct Record *next;
};

struct RecordList
{
    struct Record *head;
    uint32_t nmemb;
    bool complete;
    /** @todo   Add lock to make multi-thread safe? */
};


extern ResultCode
intel_loadHexFile(char *filename, struct RecordList *list);

extern void
intel_sortRecordList(struct RecordList *list);

extern void
intel_cleanRecordList(struct RecordList *list);

extern void
intel_displayRecordList(struct RecordList *list);


#ifdef __cplusplus
}
#endif

#endif /* INTELHEX_H */

