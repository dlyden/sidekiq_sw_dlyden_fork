#ifndef EEPROM_H
#define EEPROM_H

#include <stdint.h>
#include <stdbool.h>

#include "intelhex.h"

#ifndef EEPROM_MAX_BUFFER_LEN
#   define EEPROM_MAX_BUFFER_LEN    (16384)
#endif

#ifdef __cplusplus
extern "C"
{
#endif

extern uint32_t
eeprom_convertRecords(struct RecordList *list, uint8_t *outBuffer,
        uint32_t outBufferLen);

#ifdef __cplusplus
}
#endif

#endif /* ifdef EEPROM_H */

