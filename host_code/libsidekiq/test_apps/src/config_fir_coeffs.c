/*! \file tx_samples.c
 * \brief This file contains a basic application for configuring
 * the FIR filter coefficients.
 *  
 * <pre>
 * Copyright 2014 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sidekiq_api.h>
#include <ad9361_driver.h>

#define BUF_SIZE                        (5000)

FILE *input_fp;

static void print_usage(void);

int main(int argc, char *argv[] )
{
    bool rx_fir=false;
    char buf[BUF_SIZE];
    char *num;
    int16_t coeffs[128];
    uint8_t num_taps=0;
    uint32_t sample_rate=0;
    uint8_t card=0;
    int32_t status=0;
    size_t n;

    if( argc != 5 )
    {
        printf("Error: invalid # arguments\n");
        print_usage();
        _exit(-1);
    }

    if( strcasecmp(argv[1], "rx") == 0 )
    {
        rx_fir = true;
    }
    else if( strcasecmp(argv[1], "tx") == 0 )
    {
        rx_fir = false;
    }
    else
    {
        printf("Error: invalid FIR specified\n");
        print_usage();
        exit(-2);
    }

    input_fp=fopen(argv[2],"rb");
    if( input_fp == NULL )
    {
        printf("Error: unable to open input file %s\n", argv[1]);
        _exit(-2);
    }
    sscanf(argv[3], "%u", &sample_rate);
#if (defined __MINGW32__)
    sscanf(argv[4], "%2" SCNu8, &card);
#else
    sscanf(argv[4], "%hhu", &card);
#endif

    if ( ( n = fread( buf, sizeof(uint8_t), BUF_SIZE-1, input_fp ) ) <= 0 )
    {
        printf("Error reading from file\n");
        return (-1);
    }
    buf[n] = '\0';

    num = strtok( buf, ",\n");
    while( (num != NULL) && (num_taps<128) )
    {
        coeffs[num_taps++] = atoi(num);
        num=strtok( NULL, ",\n");
    }
    printf("Info: # of filter taps is %u\n", num_taps);
    fclose( input_fp );

    status = skiq_init(skiq_xport_type_pcie, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    // Since configuring either Tx or Rx sample rate actually configures both, it
    // doesn't matter which we write to, so just always call the rx_sample_rate
    if( (status=skiq_write_rx_sample_rate_and_bandwidth(card, skiq_rx_hdl_A1, sample_rate, sample_rate)) != 0 )
    {
        printf("Error: unable to configure sample rate\n");
    }

    if( rx_fir )
    {
        if( ad9361_write_tx_fir_coeffs(card, num_taps, coeffs) == 0 )
        {
            ad9361_save_custom_tx_fir_coeffs( card, num_taps, coeffs );
        }
    }
    else
    {
        if( ad9361_write_rx_fir_coeffs(card, num_taps, coeffs) == 0 )
        {
            ad9361_save_custom_rx_fir_coeffs( card, num_taps, coeffs );
        }
    }

    skiq_exit();

    return (status);
}

void print_usage(void)
{
    printf("Usage: config_fir_coeffs <rx|tx> <path to  coefficients file> <sample_rate> <card>\n");
    printf("   Configure the the Rx or Tx FIR filter coefficients with the values contained in\n");
    printf("   file specified.  The coefficient file should be a comma separated list of 16-bit\n");
    printf("   signed values\n");
}
