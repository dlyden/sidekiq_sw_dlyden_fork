/*! \file timestamp_read_test.c
 * \brief This file contains a basic application that reads the two timestamp
 * FPGA registers and checks for decreasing values.
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <stdio.h>
#include <inttypes.h>
#include <signal.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "sidekiq_api.h"
#include "arg_parser.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#define ITERATIONS                      100000

#define DEFAULT_SAMPLE_RATE (15360000)    // 15.36 Msps
#define DEFAULT_BANDWIDTH (9000000)       // 9 MHz - doesn't really matter
struct test_data
{
    uint8_t card;
    char *p_sernum;
    int64_t sys_ts_errors;
    int64_t rf_ts_errors;
    int64_t nr_iter;
};

static uint64_t iterations = ITERATIONS;

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- reads timestamp registers and checks for decreasing values";
static const char* p_help_long =
    "Calls skiq_read_curr_rx_timestamp and skiq_read_curr_sys_timestamp repeatedly\n"
    "and checks for the case where values from two subsequent reads are\n"
    "non-monotonic.\n"
    "\n"
    "Defaults:\n"
    "  --iterations=" xstr(ITERATIONS);

/* command line argument variables */
static char* p_serial = NULL;
static uint8_t single_card = SKIQ_MAX_NUM_CARDS;
static char* p_rfic_file_path = NULL;
static bool verbose = false;

/* global running flag */
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("serial",
                'S',
                "Target card by serial number",
                "SERIAL",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("card",
                'c',
                "Target specified Sidekiq card",
                "ID",
                &single_card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Display individual timestamp errors",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("iterations",
                'n',
                "Read registers N times",
                "N",
                &iterations,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("rfic-config",
                0,
                "Input filename of RFIC configuration",
                NULL,
                &p_rfic_file_path,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


static void sigint_function(int signo)
{
    running = false;
}


static int64_t run_rf_ts_read_test( uint8_t card, int64_t nr_iter )
{
    uint64_t last_ts = 0, curr_ts = 0;
    int64_t i, ts_errors = 0;
    bool retry=0;
    int32_t status=0;

    for (i = 0; running && (i < nr_iter); i++)
    {
        /* if we're retrying the transaction, we want to undo the increment at the completion of
           the last loop since it wasn't successful */
        if( retry == true )
        {
            i--;
        }
        status=skiq_read_curr_rx_timestamp( card, skiq_rx_hdl_A1, &curr_ts );
        if( status == 0 )
        {
            if ( (curr_ts <= last_ts) )
            {
                if( (retry == false) && (curr_ts==last_ts) )
                {
                    if( verbose )
                    {
                        fprintf(stderr, "Warning: RX timestamp did not increment %" PRIu64 " <= %" PRIu64 " at iteration %" PRId64 " on card %u\n",
                                curr_ts, last_ts, i, card);
                    }
                    retry = true;
                }
                else
                {
                    if (verbose)
                    {
                        fprintf(stderr, "Error: RX Timestamp: %" PRIu64 " <= %" PRIu64 " at iteration %" PRId64 " on card %u\n", curr_ts, last_ts, i, card);
                    }
                    ts_errors++;
                }
            }
            else
            {
                retry = false;
            }
            last_ts = curr_ts;
        }
        else
        {
            if( verbose )
            {
                fprintf(stderr, "Warning: Unable to read timestamp at iteration %"
                    PRId64 " on card %u (status %" PRIi32 ")\n", i, card, status);
            }
            // not really a timestamp error, but an error none the less
            ts_errors++;
        }
    }

    return ts_errors;
}


static int64_t run_sys_ts_read_test( uint8_t card, int64_t nr_iter )
{
    uint64_t last_ts = 0, curr_ts;
    int64_t i, ts_errors = 0;

    for (i = 0; running && (i < nr_iter); i++)
    {
        skiq_read_curr_sys_timestamp( card, &curr_ts );
        if ( curr_ts <= last_ts )
        {
            if (verbose)
            {
                fprintf(stderr, "Error: System Timestamp: %" PRIu64 " <= %" PRIu64 " at iteration %" PRId64 " on card %u\n", curr_ts, last_ts, i, card);
            }
            ts_errors++;
        }
        last_ts = curr_ts;
    }

    return ts_errors;
}


/* pthread function for each card to execute */
static void *timestamp_test(void *data)
{
    struct test_data *p_td = ((struct test_data *)data);
    uint8_t card = p_td->card;
    int32_t status;

    status = skiq_read_serial_string( card, &(p_td->p_sernum) );
    if ( status != 0 )
    {
        p_td->p_sernum = "UNKNOWN";
    }

    p_td->sys_ts_errors = run_sys_ts_read_test( card, p_td->nr_iter );
    p_td->rf_ts_errors = run_rf_ts_read_test( card, p_td->nr_iter );

    return NULL;
}


/*****************************************************************************/
/** This is the main function for executing timestamp_read_test command.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;

    struct test_data cards_td[SKIQ_MAX_NUM_CARDS];
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t num_cards = 0;
    uint8_t i = 0;

    pthread_t threads[SKIQ_MAX_NUM_CARDS];

    /* register signal handler */
    signal( SIGINT, sigint_function );

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    /* check that 'iterations' makes sense */
    if ( iterations < 2 )
    {
        fprintf(stderr, "Error: Must read registers at least 2 times for fault detection\n");
        return (-1);
    }
    else
    {
        fprintf(stderr, "INFO: Reading registers %" PRId64 " times for fault detection\n", iterations);
    }

    /* check card specifiers */
    if ( ( SKIQ_MAX_NUM_CARDS != single_card ) && ( NULL != p_serial ) )
    {
        fprintf(stderr, "Error: either --card OR --serial must be specified, not both\n");
        return (-1);
    }

    /* check for serial number existence */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string( p_serial, &single_card );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: unable to find Sidekiq with serial number %s\n", p_serial);
            return (-1);
        }
        cards[0] = single_card;
        num_cards = 1;
    }
    else if ( SKIQ_MAX_NUM_CARDS > single_card )
    {
        cards[0] = single_card;
        num_cards = 1;
    }
    else
    {
        /* determine how many Sidekiqs are there and their card IDs */
        skiq_get_cards( skiq_xport_type_auto, &num_cards, cards );
    }

    /* initialize the card list */
    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_full, cards, num_cards);
    if ( status != 0 )
    {
        fprintf(stderr, "Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    if ( p_rfic_file_path != NULL )
    {
        FILE *p_rfic_file = fopen(p_rfic_file_path, "r");
        if ( p_rfic_file == NULL )
        {
            fprintf(stderr, "Error: unable to open specified RFIC configuration file %s "
                    "(errno %d)\n", p_rfic_file_path, errno);
            skiq_exit();
            exit(-1);
        }
        else
        {
            for ( i = 0; ( i < num_cards ) && ( status == 0 ); i++ )
            {
                rewind(p_rfic_file);
                printf("Info: configuring RFIC on card %u with configuration from %s\n", cards[i],
                       p_rfic_file_path);
                status = skiq_prog_rfic_from_file(p_rfic_file, cards[i]);
            }
            fclose( p_rfic_file );
            p_rfic_file = NULL;
        }

        if ( status != 0 )
        {
            fprintf(stderr, "Error: unable to program RFIC on card %u from file with error %d\n",
                    cards[i], status);
            skiq_exit();
            exit(-1);
        }
    }
    else
    {
        // default the sample rate
        if( (status=skiq_write_rx_sample_rate_and_bandwidth( cards[0],
                                                             skiq_rx_hdl_A1,
                                                             DEFAULT_SAMPLE_RATE,
                                                             DEFAULT_BANDWIDTH )) != 0 )
        {
            fprintf(stderr, "Error: unable to configure sample rate / bandwidth of card %u (status=%d)\n",
                    cards[i], status);
            skiq_exit();
            exit(-1);
        }
    }

    /* spin up a thread for each of the cards */
    for ( i = 0; i < num_cards; i++ )
    {
        cards_td[i].card = cards[i];
        cards_td[i].p_sernum = NULL;
        cards_td[i].nr_iter = iterations;
        cards_td[i].sys_ts_errors = 0;
        cards_td[i].rf_ts_errors = 0;
        pthread_create( &(threads[i]), NULL, timestamp_test, (void *)&(cards_td[i]) );
    }

    /* join all the threads and display the results */
    for ( i = 0; i < num_cards; i++ )
    {
        pthread_join( threads[i], NULL );

        fprintf(stderr, "INFO: %6" PRId64 " System Timestamp errors and %6" PRId64
                " RX Timestamp errors over %" PRId64 " register transactions on"
                " card %u (s/n %s)\n", cards_td[i].sys_ts_errors, cards_td[i].rf_ts_errors,
                cards_td[i].nr_iter, cards_td[i].card, cards_td[i].p_sernum);

        if ( ( cards_td[i].sys_ts_errors > 0 ) || ( cards_td[i].rf_ts_errors > 0 ) )
        {
            status = -1;
        }
    }

    skiq_exit();

    return status;
}
