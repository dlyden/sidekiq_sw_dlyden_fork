/*! \file reg_poke.c
 * \brief This file contains a basic application that reads and writes
 * registers in the Sidekiq FPGA.  These registers are accessed over
 * PCIe.
 *  
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <bit_ops.h>
#include <libgen.h>
#include <unistd.h>
#include <sidekiq_hal.h>
#include <dma_interface_api.h>
#include <sidekiq_xport.h>
#include <inttypes.h>
#include <sidekiq_card_mgr.h>

static char* app_name;

static void print_usage(void);

#define print_usage_and_exit    do { print_usage(); _exit(-1); } while (0)

extern int32_t _pcie_card_init( skiq_xport_init_level_t level, uint64_t xport_uid );
extern int32_t _pcie_card_exit( skiq_xport_init_level_t level, uint64_t xport_uid );

/*****************************************************************************/
/** This is the main function for executing reg_test commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    uint32_t addr;
    uint32_t data;
    uint8_t chip_id;
    uint8_t card, card_idx;
    int32_t status = -1;

    app_name = argv[0];

    if ( ( argc < 5 ) || ( argc > 7 ) )
    {
        print_usage();
        _exit(-1);
    }

    card = (uint8_t)(strtoul(argv[3],NULL,10));
    if ( card >= SKIQ_MAX_NUM_CARDS )
    {
        printf("Error: invalid card specified (%u)\n", card);
        return (-1);
    }

    /* initialize access to the card */
    card_mgr_init();
    status = _pcie_card_init( skiq_xport_init_level_basic, card );
    if( status != 0 )
    {
        printf("Error: card %d initialization failed (status=%d)\n", card, status);
        _exit(-1);
    }

    card_mgr_get_card( card, skiq_xport_type_pcie, &card_idx );

    /* should be read, but confirm */
    if (strcasecmp(argv[2],"r") == 0)
    {
        if (strcasecmp(argv[1],"fpga") == 0)
        {
            if ( 5 != argc ) print_usage_and_exit;

            /* reading an FPGA reg, proceed */
            addr=(uint32_t)strtoul(argv[4],NULL,16);

            status = DmaInterfaceReadRegister(card,addr,(uint32_t*)&data);
            if ( 0 == status )
            {
                printf("Info: FPGA read: card=%u addr=0x%08" PRIx32 ", data=0x%08" PRIx32 "\n",
                       card,addr,data);
            }
            else
            {
                printf("Error: FPGA read failed (status=%d)\n", status);
            }
        }
        else if (strcasecmp(argv[1], "rfic") == 0)
        {
            if ( 6 != argc ) print_usage_and_exit;

            /* reading an rfic reg, proceed */
            if ((strcasecmp(argv[4], "0") == 0) ||
                (strcasecmp(argv[4], "1") == 0) ||
                (strcasecmp(argv[4], "2") == 0) )
            {
                chip_id = atoi(argv[4]);
            }
            else
            {
                printf("Error: invalid chip id %s\n",argv[4]);
                return(-1);
            }
            addr=(uint16_t)strtoul(argv[5],NULL,16);
            status = hal_rfic_read_reg(card_idx,chip_id,addr,(uint8_t*)&data);
            if ( 0 == status )
            {
                printf("Info: rfic read: card=%u cs=%u addr=0x%04x, data=0x%02x\n",
                       card,chip_id, (uint32_t)addr,(uint8_t)data);
            }
            else
            {
                printf("Error: rfic read failed (status=%d)\n", status);
            }
        }
        else
        {
            printf("Error: invalid device for reading\n");
            print_usage();
            return(-2);
        }
    }
    
    else if (strcasecmp(argv[2], "w") == 0)
    {
        if (strcasecmp(argv[1], "fpga") == 0)
        {
            if ( 6 != argc ) print_usage_and_exit;

            /* writing an FPGA reg, proceed */
            addr=(uint32_t)strtoul(argv[4],NULL,16);
            data=(uint32_t)strtoul(argv[5],NULL,16);

            status = sidekiq_fpga_reg_write(card,addr,data);
            if ( 0 == status )
            {
                printf("Info: FPGA write: card=%u addr=0x%08" PRIx32 ", data=0x%08" PRIx32"\n",
                       card,addr,data);
            }
            else
            {
                printf("Error: FPGA read failed (status=%d)\n", status);
            }
        }
        else if (strcasecmp(argv[1], "rfic") == 0)
        {
            if ( 7 != argc ) print_usage_and_exit;

            if ((strcasecmp(argv[4], "0") == 0) ||
                (strcasecmp(argv[4], "1") == 0) ||
                (strcasecmp(argv[4], "2") == 0) )
            {
                chip_id = atoi(argv[4]);
            }
            else
            {
                printf("Error: invalid chip id %s\n",argv[3]);
                return(-1);
            }
            addr=(uint16_t)strtoul(argv[5],NULL,16);
            data=(uint8_t)strtoul(argv[6],NULL,16);
            status = hal_rfic_write_reg(card_idx,chip_id,addr,data);
            if ( 0 == status )
            {
                printf("Info: rfic write: card=%u cs=%u addr=0x%04x, data=0x%02x\n",
                       card,chip_id,(uint16_t)addr,(uint8_t)data);
            }
            else
            {
                printf("Error: rfic read failed (status=%d)\n", status);
            }
        }
        else if (strcasecmp(argv[1], "dac") == 0)
        {
            if( 5 != argc ) print_usage_and_exit;
            data=(uint32_t)strtoul(argv[4],NULL,16);
            status = hal_spi_dac_write( card, data );
            if( 0 == status )
            {
                printf("Info: dac write: card=%u data=0x%04x\n",
                       card,(uint16_t)data);
            }
            else
            {
                printf("Error: dac write failed (status=%d)\n", status);
            }
        }
        else
        {
            printf("Error: invalid device for writing\n");
            print_usage();
            return(-2);
        }
    }
    else
    {
        printf("Error: invalid command\n");
        print_usage();
        return(-3);
    }

    _pcie_card_exit( skiq_xport_init_level_basic, card );
    card_mgr_exit();

    return(0);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    char *bn = basename( app_name );

    printf("Usage: %s   fpga r <card>           <addr in hex>\n",bn);
    printf("       %s   fpga w <card>           <addr in hex> <val in hex>\n",bn);
    printf("       %s rfic r <card> <chip_id> <addr in hex>\n",bn);
    printf("       %s rfic w <card> <chip_id> <addr in hex> <val in hex>\n",bn);
    printf("       %s dac  w <card>                         <val in hex>\n",bn);
    printf("\n");
    printf("   read or write a registers in specified Sidekiq card\n");
    printf("   if fpga, read/write the actual FPGA register\n");
    printf("   if rfic, read/write the addr/data for the specified rfic chip_id (which\n");
    printf("   is either 'a'\n");
}
