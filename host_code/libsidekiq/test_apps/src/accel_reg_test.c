/*! \file read_accel.c
 * \brief This file contains a basic application that reads the
 * accelerometer of the specified Sidekiq.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include <sidekiq_hal.h>
#include <sidekiq_api.h>

#include "card_services.h"

static char* app_name;

static void print_usage(void);

/*****************************************************************************/
/** This is the main function for executing read_temp application

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;
    uint8_t card=0;
    uint8_t val=0;
    uint8_t reg=0;
    bool b_write = false;
    pid_t owner = 0;
    app_name = argv[0];

    if( (argc == 4) || (argc == 5) )
    {
        card = atoi(argv[1]);
        reg = strtoul(argv[3], NULL, 16);

        if( strcasecmp(argv[2], "r") == 0 )
        {
            b_write = false;
        }
        else if( strcasecmp(argv[2], "w") == 0 )
        {
            b_write = true;
        }
        else
        {
            printf("Error: invalid read/write option specified\n");
            print_usage();
            return(-2);
        }

        printf("Info: initializing card %" PRIu8 "...\n", card);

        /* initialize Sidekiq */
        status = skiq_init(type, level, &card, 1);
        if( status != 0 )
        {
            if ( ( EBUSY == status ) &&
                 ( 0 != skiq_is_card_avail(card, &owner ) ) )
            {
                printf("Error: card %" PRIu8 " is already in use (by process"
                        " ID %u); cannot initialize card.\n", card,
                        (unsigned int) owner);
            }
            else if ( -EINVAL == status )
            {
                printf("Error: unable to initialize libsidekiq; was a valid"
                        " card specified? (result code %" PRIi32 ")\n", status);
            }
            else
            {
                printf("Error: unable to initialize libsidekiq with status %"
                        PRIi32 "\n", status);
            }
            return (-1);
        }

        if( b_write == true )
        {
            val = strtoul(argv[4], NULL, 16);
            status = card_write_accel_reg( card, reg, &val, 1 );
            if( status == 0 )
            {
                printf("Info: wrote 0x%x to register 0x%x\n", val, reg);
                status = card_read_accel_reg( card, reg, &val, 1 );
                printf("Info: read 0x%x from register 0x%x\n", val, reg);
            }
            else
            {
                printf("Error: unable to write register 0x%x (%" PRIi32 ")\n",
                        reg, status);
            }
        }
        else
        {
            status = card_read_accel_reg( card, reg, &val, 1 );
            if( status == 0 )
            {
                printf("Info: read 0x%x from register 0x%x\n", val, reg);
            }
            else
            {
                printf("Error: unable to read register 0x%x (%" PRIi32 ")\n",
                        reg, status);
            }
        }
        skiq_exit();
    }
    else
    {
        printf("Invalid # of args\n");
        print_usage();
        return (-2);
    }

    return (0);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
void print_usage(void)
{
    printf("Usage: %s <card> <r|w> <reg> [val]\n", app_name);
    printf("   reads or writes the accelerometer register\n");
}

