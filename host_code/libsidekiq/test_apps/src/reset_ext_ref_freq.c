/*! \file reset_ext_ref_freq.c
 * \brief This file contains a basic application that erases reference
 * clock frequency.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_hal.h>
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <sidekiq_private.h>

#include "hardware.h"
#include "card_services.h"
#include "sidekiq_usb_cmds.h"
#include "sidekiq_card_mgr_private.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <inttypes.h>

static void print_usage(void);

int main( int argc, char *argv[] )
{
    int32_t status=0;
    uint8_t card=0;
    uint8_t blank_eeprom[REFERENCE_CLOCK_EXTERNAL_FREQ_LEN]; 


    if( argc != 2 )
    {
        printf("Error: invalid # args (%d)\n", argc);
        print_usage();
        return (-1);
    }
    card=atoi(argv[1]);

    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_basic,
                        &card, 1 );
    if (status == 0)
    {
        memset( blank_eeprom, 0xFF, REFERENCE_CLOCK_EXTERNAL_FREQ_LEN );

        // write reference clock frequency to "unconfigured"
        printf("Wiping reference clock frequency\n");
        if( (status=card_write_eeprom(card,
                                      REFERENCE_CLOCK_EXTERNAL_FREQ_ADDR,
                                      blank_eeprom,
                                      REFERENCE_CLOCK_EXTERNAL_FREQ_LEN)) == 0 )
        {
            printf("Successfully wiped ref clock freq\n");
        }
        else
        {
            printf("Error writing to eeprom (status=%d)\n", status);
        }
        status = skiq_exit();
    }

    return (status);
}

void print_usage()
{
    printf("Usage: reset_ext_ref_freq <card>\n");
    printf("   resets the reference clock frequency in EEPROM\n");
}
