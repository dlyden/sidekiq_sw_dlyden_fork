/*! \file init_and_exit.c
 * \brief This file contains a basic application that
 * reads and prints the various version strings for
 * libsidekiq and the FPGA.
 *
 * <pre>
 * Copyright 2011-2017 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include "sidekiq_api.h"
#include "sidekiq_params.h"
#include <stdio.h>

#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include <arg_parser.h>

#define DIVIDER_STR \
    "***********************************************************\n"

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- initialize, optionally wait, and exit";
static const char* p_help_long = "- initialize, optionally wait, and exit";

/* command line argument variables */
static uint8_t single_card = SKIQ_MAX_NUM_CARDS;
static uint8_t sleep_delay = 0;
static bool do_full_init = false;
static uint32_t repeat = 0;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Target specified Sidekiq card",
                "ID",
                &single_card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("sleep-duration",
                0,
                "Delay in seconds between init and exit",
                "seconds",
                &sleep_delay,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("full",
                0,
                "Perform full RF initialization",
                NULL,
                &do_full_init,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("repeat",
                0,
                "Call skiq_init / skiq_exit N additional times",
                "N",
                &repeat,
                UINT32_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the main function for executing version_test command.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;
    uint8_t all_cards[SKIQ_MAX_NUM_CARDS];
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t all_num_cards = 0;
    uint8_t num_cards = 0;
    pid_t card_pid = 0;
    int32_t status = 0;
    uint32_t i = 0;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    /* perform full init to obtain RF parameters */
    if( do_full_init )
    {
        level = skiq_xport_init_level_full;
    }

    /* determine how many Sidekiqs are there and their card IDs */
    skiq_get_cards( skiq_xport_type_auto, &all_num_cards, all_cards );
    printf( "%d card(s) found!\n", all_num_cards );
    for( i=0; i<all_num_cards; i++ )
    {
        if( skiq_is_card_avail(all_cards[i], &card_pid) != 0 )
        {
            printf("Warning: card %u locked by another process (PID=%u)\n",
                    all_cards[i], (unsigned int)(card_pid));
        }
        else
        {
            if( single_card == SKIQ_MAX_NUM_CARDS )
            {
                cards[num_cards] = all_cards[i];
                num_cards++;
            }
            else if( all_cards[i] == single_card )
            {
                cards[num_cards] = single_card;
                num_cards++;
            }
        }
    }

    do
    {
        /* bring up the interfaces for all the cards */
        status = skiq_init(skiq_xport_type_auto, level, cards, num_cards);
        if( status != 0 )
        {
            printf("Error: unable to initialize libsidekiq, status=%d\n", status);
            return status;
        }

        if (sleep_delay)
        {
            printf("Sleeping for %d seconds.\n", sleep_delay);
            sleep(sleep_delay);
        }
        else
        {
            printf("No sleeping.\n");
        }

        skiq_exit();

    } while (repeat-- > 0);

    return status;
}
