/*! \file i2c_test.c
 * \brief This file contains a basic application that reads or
 * writes the I2C bus
 *  
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sidekiq_hal.h>
#include <sidekiq_api.h>

static char* app_name;

static void print_usage(void);


/*****************************************************************************/
/** This is the main function for executing read_temp commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    uint8_t slave_addr;
    unsigned long data_hack;
    uint8_t data_buff[4];
    uint8_t data_len;
    uint8_t i;
    int32_t status=0;
    uint8_t card=0;

    app_name = argv[0];
    
    if (argc != 5)
    {
        printf("Error: incorrect # of args\n");
        print_usage();
    }
    else
    {
        card = (uint8_t)(strtoul(argv[2],NULL,10));
        status = skiq_init(skiq_xport_type_pcie, skiq_xport_init_level_basic, &card, 1);
        if( status != 0 )
        {
            printf("Error: unable to initialize libsidekiq with status %d\n", status);
            return (-1);
        }

        if (strcasecmp(argv[1],"r") == 0)
        {
            /* read operation...grab the slave addr */
            slave_addr=(uint8_t)strtoul(argv[3],NULL,16);
            data_len = atoi(argv[4]);
            if (data_len > 4)
            {
                printf("Error: data length must be <= 4 bytes\n");
                skiq_exit();
                exit(-1);
            }
            printf("Info: I2C slave addr is 0x%02x\n",slave_addr);
            printf("Info: I2C data length to read is %d\n",data_len);
            memset(data_buff,0x00,4);
            data_buff[0] = (uint8_t)strtoul(argv[4],NULL,16);
            if ((status=hal_write_i2c(card,slave_addr,data_buff,1)) == 0)
            {
                printf("Info: Write success\n");
            }
            if ((status=hal_read_i2c(card,slave_addr,data_buff,data_len)) == 0)
            {           
                printf("Info: Success: I2C data read: ");
                for (i=0; i<data_len; i++)
                {
                    printf("0x%02x ",data_buff[i]);
                }
                printf("\n");
            }
            else
            {
                printf("Error: I2C read transaction failed\n");
            }
        }
        else if (strcasecmp(argv[1],"w") == 0)
        {
            /* write operation...grab the slave addr */
            memset(data_buff,0x00,4);
            slave_addr=(uint8_t)strtoul(argv[3],NULL,16);
            data_len = (strlen(argv[4]) - 2) / 2; /* -2 to remove leading 0x */
            data_hack = strtoul(argv[4],NULL,16);
            memcpy(data_buff,(void*)&data_hack,4);
            printf("Info: I2C slave addr is 0x%02x\n",slave_addr);
            printf("Info: I2C data length is %d\n",data_len);
            printf("Info: I2C data to write: ");
            for (i=0; i<data_len; i++)
            {
                printf("0x%02x ",data_buff[i]);
            }
            printf("\n");
            if ((status=hal_write_i2c(card,slave_addr,data_buff,data_len)) == 0)
            {
                printf("Info: Success\n");
            }
            else
            {
                printf("Error: I2C write transaction failed\n");
            }
        }
        else
        {
            printf("Error: need to specify 'r' or 'w'\n");
            print_usage();
        }           
    }

    skiq_exit();
    return(status);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <r|w> <card> <I2C slave addr> \n", app_name);
    printf("   <# bytes to read>|<hex sequence to write>\n");
    printf("   read or write to the I2C device of the Sidekiq card specified\n");
}

    
