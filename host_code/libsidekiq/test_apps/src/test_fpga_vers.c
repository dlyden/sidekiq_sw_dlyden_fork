/*! \file test_fpga_vers.c
 * \brief This file contains a basic application that tests the FPGA version of
 * the Sidekiq.
 *
 * <pre>
 * Copyright 2014 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#include <arg_parser.h>

/* these are used to provide help strings for the application when running itq
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- determine if FPGA versions match";
static const char* p_help_long =
"\
Reads from the Sidekiq FPGA to see if the various versions (major, minor, build\n\
date, and GIT hash) match.\n\
\n\
Defaults:\n\
  --mode=PCIE";

/* command line argument variables */
static char* p_mode = "PCIE";
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;

static uint8_t major_version = 0, minor_version = 0, patch_version = 0;
static uint32_t builddate = 0, githash = 0;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Use specified Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Use specified Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("mode",
                'm',
                "Transport mode to use, either USB or PCIE",
                "TRANSPORT",
                &p_mode,
                STRING_VAR_TYPE),
    APP_ARG_REQ("major",
                'j',
                "FPGA major version to match",
                "MAJOR",
                &major_version,
                UINT8_VAR_TYPE),
    APP_ARG_REQ("minor",
                'n',
                "FPGA minor version to match",
                "MINOR",
                &minor_version,
                UINT8_VAR_TYPE),
    APP_ARG_REQ("patch",
                'p',
                "FPGA patch version to match",
                "PATCH",
                &patch_version,
                UINT8_VAR_TYPE),
    APP_ARG_REQ("build-date",
                'b',
                "FPGA build date to match",
                "0xYYMMDDHH",
                &builddate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("git-hash",
                'g',
                "FPGA GIT hash to match",
                "0xAABBCCDD",
                &githash,
                UINT32_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

int main( int argc, char *argv[] )
{
    skiq_hw_vers_t hardware_vers;
    skiq_xport_type_t type;
    int32_t status = 0;
    pid_t owner = 0;

    bool match_git_hash = false;
    skiq_param_t params;
    skiq_fpga_param_t *fpga = &(params.fpga_param);

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    /* disable messages */
    skiq_register_logging(NULL);

    if( 0 == strncasecmp(p_mode, "PCIE", 5) )
    {
        type = skiq_xport_type_pcie;
    }
    else if( 0 == strncasecmp(p_mode, "USB", 4) )
    {
        type = skiq_xport_type_usb;
    }
    else
    {
        fprintf(stderr, "Error: invalid mode specified\n");
        return (-1);
    }

    if ( ( UINT8_MAX == card ) && ( NULL == p_serial ) )
    {
        fprintf(stderr, "Error: one of --card or --serial MUST be specified\n");
        return (-1);
    }
    else if ( ( UINT8_MAX != card ) && ( NULL != p_serial ) )
    {
        fprintf(stderr, "Error: EITHER --card OR --serial must be specified,"
                " not both\n");
        return (-1);
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string( p_serial, &card );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: unable to find Sidekiq with serial number"
                    " %s (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(type, skiq_xport_init_level_basic, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        return (-1);
    }

    /* before programming FPGA from flash, check to see that we are using the
     * 'correct' transport by looking for a non-zero major version */
    status = skiq_read_parameters( card, &params );
    if ( status != 0 )
    {
        fprintf(stderr, "Error: unable to read FPGA version, status %d\n",
                status);
        skiq_exit();
        return (status);
    }
    else if ( 0 == fpga->version_major )
    {
        fprintf(stderr, "Error: unable to read FPGA version using specified"
                " transport %s\n", p_mode);
        skiq_exit();
        return (-1);
    }

    /* read the hardware version */
    if ( ( status = skiq_read_hw_version(card, &hardware_vers) ) != 0 )
    {
        fprintf(stderr, "Error: unable to read hardware version, status %d\n",
                status);
    }

    if ( ( status == 0 ) && ( hardware_vers != skiq_hw_vers_reserved ) )
    {
        /* program the FPGA from flash */
        status = skiq_prog_fpga_from_flash( card );
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to program FPGA from flash with"
                    " status %d\n", status);
            skiq_exit();
            return (status);
        }

        /* finally read the FPGA version */
        status = skiq_read_parameters( card, &params );
        if ( status != 0 )
        {
            fprintf(stderr, "Error: unable to read FPGA version, status %d\n",
                    status);
            skiq_exit();
            return (status);
        }
    }

    /* try to match the githash if it was specified as non-zero */
    match_git_hash = (githash > 0);

    /* traditionally a return status of 0 indicates success */
    if ( (status == 0) &&
         (fpga->version_major == major_version) &&
         (fpga->version_minor == minor_version) &&
         (fpga->version_patch == patch_version) &&
         (fpga->build_date == builddate) &&
         ( !match_git_hash || ( fpga->git_hash == githash ) ) )
    {
        printf("Info: FPGA versions match\n");
        status = 0;
    }
    else
    {
        printf("Info: FPGA versions do not match\n");
        status = 1;
    }

    skiq_exit();

    return (status);
}

