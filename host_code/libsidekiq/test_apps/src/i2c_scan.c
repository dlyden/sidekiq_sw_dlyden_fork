/*! \file i2c_scan.c
 *
 * \brief This file contains a basic application that scans the I2C bus through
 * FPGA register writes
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <inttypes.h>
#include <unistd.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "sidekiq_hal.h"

#include "card_services.h"

#include "rfe_z2_b.h"

#include "arg_parser.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER          0
#endif

#ifndef DEFAULT_BUS_NUMBER
#   define DEFAULT_BUS_NUMBER          0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- scan Sidekiq's I2C bus";
static const char* p_help_long =
"\
Scans the Sidekiq's I2C bus and provides a matrix of available devices.  On\n\
Sidekiq Z2, a user may optionally enable the external I2C bus available through\n\
the PCIe edge connector.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --bus=" xstr(DEFAULT_BUS_NUMBER);

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static uint8_t bus = UINT8_MAX;
static char* p_serial = NULL;
static bool enable_external = false;
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("bus",
                'b',
                "Specify I2C bus",
                "BUS",
                &bus,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("enable-external",
                0,
                "Enable access to the external I2C bus (Z2 only)",
                NULL,
                &enable_external,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}

/*****************************************************************************/
/** This is the main function for executing read_temp commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])

{
    int32_t status = 0;
    uint8_t addr, data = 0;
    pid_t owner = 0;

    bool skiq_initialized = false;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        status = -1;
        goto finished;
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    if (UINT8_MAX == bus)
    {
        bus = DEFAULT_BUS_NUMBER;
    }

    /* turn off logging messages */
    skiq_register_logging(NULL);

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    if ( 0 == skiq_is_xport_avail( card, skiq_xport_type_pcie ) )
    {
        hal_select_functions_for_card( card, skiq_xport_type_pcie );
    }
    else if ( 0 == skiq_is_xport_avail( card, skiq_xport_type_usb ) )
    {
        /* if PCIe is not available, but USB is, the underlying hal_i2c_write()
         * will always "succeed", so we can't use it for device detection */
        fprintf(stderr, "Error: cannot support USB only with this application, need either PCIe or a "
                "custom transport\n");
        goto finished;
    }

    if ( ( _skiq_get_part( card ) == skiq_z2 ) ||
         ( _skiq_get_fmc_carrier( card ) == skiq_fmc_carrier_htg_k800 ) )
    {
        if ( enable_external )
        {
            status = skiq_fact_i2c_enable_external_bus_access( card );
        }
        else
        {
            status = skiq_fact_i2c_disable_external_bus_access( card );
        }

        if ( status != 0 )
        {
            fprintf(stderr, "Error: could not set external I2C with status %d\n", status);
            goto finished;
        }
    }
    else if ( enable_external )
    {
        fprintf(stderr, "Error: --enable-external is only valid with a Sidekiq Z2\n");
        status = EINVAL;
        goto finished;
    }

    fprintf(stderr, "     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\n");
    fprintf(stderr, "00:                        ");
    for (addr = 0x08; ( addr < 0x78 ) && running; addr++)
    {
        if ( ( addr % 16 ) == 0 ) fprintf(stderr, "\n%02x:", addr);

        status = skiq_fact_i2c_write(card, bus, addr, &data, 1);
        if ( status == 0 )
        {
            fprintf(stderr, " YY");
        }
        else
        {
            fprintf(stderr, " --");
        }
    }
    fprintf(stderr, "\n");

    status = 0;

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return ((int) status);
}
