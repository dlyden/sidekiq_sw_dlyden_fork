/*! \file ref_clock_stamp.c
 * \brief This file contains the ability to write a default reference clock
 * setting if one does not already exit.
 *
 * <pre>
 * Copyright 2016 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <bit_ops.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <inttypes.h>

#include <arg_parser.h>

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- 'stamp' the Sidekiq with a default ref clock setting if not set";
static const char* p_help_long =
"\
Reads the reference clock configuration and writes a valid setting if one does\n\
not exist.";

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

int main( int argc, char *argv[] )
{
    skiq_xport_type_t type;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;
    int32_t status = 0;
    skiq_ref_clock_select_t ref_clock = skiq_ref_clock_invalid;
    pid_t owner = 0;

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    /* disable messages */
    skiq_register_logging(NULL);

    if ( ( UINT8_MAX == card ) && ( NULL == p_serial ) )
    {
        fprintf(stderr, "Error: one of --card or --serial MUST be specified\n");
        return (-1);
    }
    else if ( ( UINT8_MAX != card ) && ( NULL != p_serial ) )
    {
        fprintf(stderr, "Error: EITHER --card OR --serial must be specified,"
                " not both\n");
        return (-1);
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string( p_serial, &card );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: unable to find Sidekiq with serial number"
                    "%s (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card"
                " ID (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    if( skiq_is_xport_avail(card, skiq_xport_type_pcie) != 0 )
    {
        type = skiq_xport_type_usb;
    }
    else
    {
        type = skiq_xport_type_pcie;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(type, level, &card, 1);
    if ( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        return (-1);
    }

    status = skiq_read_ref_clock_select( card, &ref_clock );
    if ( status == 0 )
    {
        if ( ref_clock == skiq_ref_clock_invalid )
        {
            printf("Info: invalid reference clock configuration, stamping as internal\n");
            status = skiq_fact_write_ref_clock(card, skiq_ref_clock_internal);
            if ( status == 0 )
            {
                printf("Info: succesfully configured reference clock\n");
            }
            else
            {
                fprintf(stderr, "Error: failed writing reference clock"
                        " configuration with status %d\n", status);
                status = -1;
            }
        }
        else
        {
            printf("Info: valid reference clock configuration\n");
        }
    }
    else
    {
        printf("Info: Sidekiq does not support reference clock configuration, ignoring\n");
    }

    skiq_exit();

    return (int) status;
}

