/*! \file tx_timestamp_tests.c
 * \brief This file tests various aspects of how timestamps and transmitting
 * data interact.
 *
 * <pre>
 * Copyright 2018-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <sidekiq_private.h>
#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

/**
    @brief  The default card index to use.
*/
#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#ifndef DEFAULT_TIMESTAMP_BASE
#   define DEFAULT_TIMESTAMP_BASE   "rf"
#endif

/**
    @brief  The default timestamp value to use when transmitting.
*/
#ifndef DEFAULT_TIMESTAMP_VALUE
#   define DEFAULT_TIMESTAMP_VALUE      (100000)
#endif

/**
    @brief  For the TX late timestamp test, the amount to subtract off the
            current TX timestamp to guarantee the transmitted data is late.
*/
#ifndef LATE_TIMESTAMP_OFFSET
#   define LATE_TIMESTAMP_OFFSET        (500)
#endif

/**
    @brief  For the TX late FIFO test, the amount of time to sleep before
            checking the late TX counter again.
*/
#ifndef EMPTY_FIFO_SLEEP_TIME_SEC
#   define EMPTY_FIFO_SLEEP_TIME_SEC    (15)
#endif

#define NO_DELAY                       0
#define STOP_AFTER_TX                  true
#define NO_STOP_AFTER_TX               false

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- perform some TX tests";
static const char* p_help_long =
"\
If none of the above tests are specified, all available tests are run\n\
by default.\n\
\n\
--run-late-test:\n\
Test if the current bitfile on the specified card supports the ability\n\
to transmit late timestamps and, if so, verify that it is able to do so.\n\
\n\
--run-empty-fifo-test:\n\
Test if the TX late timestamp increments if the TX FIFO becomes empty (\n\
it shouldn't).\n\
\n\
\n\
The input file specified via the '-s' or '--source' options provides\n\
raw I/Q data; it should contain 16-bit signed twos-complement little-endian\n\
I/Q samples formatted as follows:\n\
\n\
    <16-bit Q0> <16-bit I0> <16-bit Q1> <16-bit I1> ... etc\n\
\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --frequency=850000000\n\
  --handle=A1\n\
  --rate=10000000\n\
  --timestamp-base=" xstr(DEFAULT_TIMESTAMP_BASE) "\n\
  maximum attenuation\n\
";

/* variables used for all command line arguments */
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static uint64_t lo_freq = 850000000;
static char* p_file_path = NULL;
static char* p_hdl = "A1";
static char* p_timestamp_base = DEFAULT_TIMESTAMP_BASE;
static uint32_t sample_rate = 1000000;
static bool run_late_test = false;
static bool run_fifo_empty_test = false;
static uint32_t repeat = 0;
static uint16_t attenuation = UINT16_MAX;

/* application variables  */
static uint32_t block_size_in_words = 16380;

/* Tx packet size must be 2k words less than or equal to the FPGA FIFO size:

             FIFO size = 16384
        Tx packet size = 16384 - 2048 = 14366
            Per handle = 14366 / 2    = 7168
 After shared metadata = 7168 - 2     = 7166
*/
static uint32_t block_size_in_words_dual = 7166;

static FILE *input_fp = NULL;
static skiq_tx_hdl_t hdl = skiq_tx_hdl_A1;
static skiq_tx_timestamp_base_t timestamp_base = skiq_tx_rf_timestamp;
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("frequency",
                'f',
                "Frequency to transmit samples at in Hertz",
                "Hz",
                &lo_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("rate",
                'r',
                "Sample rate in Hertz",
                "Hz",
                &sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("attenuation",
                'a',
                "Output attenuation in quarter dB steps",
                "dB",
                &attenuation,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("handle",
                0,
                "Tx handle to use, either A1 or A2",
                "Tx",
                &p_hdl,
                STRING_VAR_TYPE),
    APP_ARG_OPT("timestamp-base",
                0,
                "Timestamps based on rf or system free running clock, either 'rf' or 'system'",
                NULL,
                &p_timestamp_base,
                STRING_VAR_TYPE),
    APP_ARG_REQ("source",
                's',
                "Input file to source for I/Q data",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("repeat",
                0,
                "Repeat the specified test(s) N time(s)",
                "N",
                &repeat,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("run-late-test",
                0,
                "Run the late TX timestamp test",
                NULL,
                &run_late_test,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("run-empty-fifo-test",
                0,
                "Run the TX FIFO empty test",
                NULL,
                &run_fifo_empty_test,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/**
    @brief  Transmit given data blocks on a specified card & handle.

    @param[in]  local_card          The card index to use.
    @param[in]  local_hdl           The handle to transmit on.
    @param[in]  tx_blocks           A pointer to the list of blocks that should
                                    be transmitted; this should not be NULL.
    @param[in]  num_blocks          The number of blocks to transmit (the number
                                    of blocks in tx_blocks).
    @param[in]  flow_mode           The TX flow mode to use when transmitting;
                                    this can only be
                                    ::skiq_tx_with_timestamps_data_flow_mode or
                                    ::skiq_tx_with_timestamps_allow_late_data_flow_mode.
    @param[in]  interblock_delay_us Time in microseconds to delay between
                                    transmitting blocks
    @param[in]  stop_when_finished  After completing transmission, choose
                                    whether or not the transmit stream should be
                                    shut down.
    @param[out] tx_errors           The number of transmit errors incurred while
                                    trying to transmit the given data.
    @param[out] late_errors         The number of late timestamp errors incurred
                                    while trying to transmit the given data.

    @return 0 on success, else an errno representing the failure.
*/
static int32_t
perform_tx_test( uint8_t local_card, skiq_tx_hdl_t local_hdl,
        skiq_tx_block_t ***tx_blocks, uint32_t num_blocks,
        skiq_tx_flow_mode_t flow_mode, uint32_t interblock_delay_us,
        bool stop_when_finished, uint32_t *tx_errors,
        uint32_t *late_errors)
{
    int32_t status = 0;
    int32_t tmp_status = 0;
    uint32_t local_tx_errors = 0;
    uint32_t local_late_errors = 0;
    uint32_t current_block = 0;
    uint32_t timestamp_increment = block_size_in_words;
    uint64_t timestamp = DEFAULT_TIMESTAMP_VALUE;
    skiq_tx_block_t **local_tx_blocks = NULL;

    if( NULL == tx_blocks )
    {
        fprintf(stderr, "Error: invalid transmit data pointer!\n");
        return (-EINVAL);
    }
    local_tx_blocks = *tx_blocks;

    if( flow_mode == skiq_tx_with_timestamps_data_flow_mode )
    {
        if ( interblock_delay_us == 0 )
        {
            /* Reset the timestamp */
            status = skiq_reset_timestamps(local_card);
            if( status != 0 )
            {
                fprintf(stderr, "Error: unable to reset timestamps (result code %"
                        PRIi32 ")\n", status);
                return (status);
            }
        }
        else
        {
            /* when a TX test is executed with timestamps_data_flow_mode and a
             * non-zero interblock delay, the blocks are required to be late, so
             * set the timestamp base that is definitely late. */
            timestamp = 1;
        }
    }
    else if( flow_mode == skiq_tx_with_timestamps_allow_late_data_flow_mode )
    {
        /*
            Get the current timestamp and ensure that the frames to be sent are
            already late.
        */
        status = skiq_read_curr_tx_timestamp(local_card, local_hdl, &timestamp);
        if( status != 0 )
        {
            fprintf(stderr, "Error: failed to get current TX timestamp"
                    " (result code %" PRIi32 ")\n", status);
            return (status);
        }

        printf("Info: current timestamp is %" PRIu64 "\n", timestamp);
        if( LATE_TIMESTAMP_OFFSET <= timestamp )
        {
            timestamp -= LATE_TIMESTAMP_OFFSET;
        }
        else
        {
            timestamp = 0;
        }
        printf("Info:    late timestamp is %" PRIu64 "\n", timestamp);
    }
    else
    {
        return -ENOTSUP;
    }
    printf("Info: using initial timestamp value of %" PRIu64 ".\n",
            timestamp);

    status = skiq_write_tx_data_flow_mode(local_card, local_hdl, flow_mode);
    if( status != 0 )
    {
        if( (-ENOTSUP == status) &&
            (skiq_tx_with_timestamps_allow_late_data_flow_mode == flow_mode) )
        {
            fprintf(stderr, "Error: the currently loaded bitfile doesn't"
                    " support late timestamp mode (result code %" PRIi32
                    ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to configure Tx data flow mode"
                    " (result code %" PRIi32 ")\n", status);
        }
        return (status);
    }

    if ( interblock_delay_us > 0 )
    {
        printf("Info: Requested to delay %u microseconds between sample blocks.\n",
               interblock_delay_us);
    }

    // enable the Tx streaming
    status = skiq_start_tx_streaming(local_card, local_hdl);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to start streaming (result code %"
                PRIi32 ")\n", status);
        return (status);
    }
    printf("Info: successfully started streaming.\n");

    while( (current_block < num_blocks) && (running) )
    {
        skiq_tx_set_block_timestamp(local_tx_blocks[current_block], timestamp);

        status = skiq_transmit(local_card, local_hdl,
                    local_tx_blocks[current_block], NULL);
        if( 0 != status )
        {
            fprintf(stderr, "Error: failed to transmit data block %" PRIu32
                    " of %" PRIu32 " (result code %" PRIi32 ")\n",
                    current_block, num_blocks, status);
            local_tx_errors++;
        }
        current_block++;

        if ( interblock_delay_us > 0 )
        {
            usleep( interblock_delay_us );
        }

        /* update the timestamp */
        timestamp += timestamp_increment;
    }

    if( !running )
    {
        status = -EINTR;
    }

    status = skiq_read_tx_num_late_timestamps(local_card, local_hdl,
                &local_late_errors);
    if( status != 0 )
    {
        fprintf(stderr, "Error: failed to read number of late timestamps"
                " (result code %" PRIi32 ")\n", status);
    }

    if( stop_when_finished )
    {
        printf("Info: stopping streaming.\n");
        tmp_status = skiq_stop_tx_streaming(local_card, local_hdl);
        if( 0 != tmp_status )
        {
            fprintf(stderr, "Error: failed to stop streaming (result code %"
                    PRIi32 ")\n", tmp_status);
        }
    }

    if( 0 == status )
    {
        if( NULL != tx_errors )
        {
            *tx_errors = local_tx_errors;
        }
        if( NULL != late_errors )
        {
            *late_errors = local_late_errors;
        }
    }

    return (status);
}


/**
    @brief  Run TX late timestamp test.

    @param[in]  local_card      The card index of the card to use.
    @param[in]  local_hdl       The handle to transmit on.
    @param[in]  tx_blocks       A pointer to the list of blocks that should
                                be transmitted; this should not be NULL.
    @param[in]  num_blocks      The number of blocks to transmit (the number
                                of blocks in tx_blocks).

    This test does several things. It:
    a) Checks for the presence of the "late timestamp" capability flag and
       return if it doesn't exist on the current bitfile.
    b) Tests transmitting the user-specified file using
       ::skiq_tx_with_timestamps_data_flow_mode and reports the number of
       TX errors and late timestamp errors (for a baseline).
    c) Tests transmitting the user-specified file using
       ::skiq_tx_with_timestamps_allow_late_data_flow_mode, verifies that no
       late timestamps were noted (as this mode shouldn't have any), and
       reports the number of TX errors and late timestamp errors.

    @return 0 on success, else an errno representing the failure.
*/
static int32_t
run_late_timestamp_test(uint8_t local_card, skiq_tx_hdl_t local_hdl,
        skiq_tx_block_t ***tx_blocks, uint32_t num_blocks)
{
    int32_t status = 0;
    uint32_t tx_errors = 0;
    uint32_t late_errors = 0;
    uint8_t major = 0, minor = 0, patch = 0;

    printf("*** Running late timestamp capability test\n");

    /*
        Check that late timestamp mode is supported before continuing on with
        the test; this setting doesn't need to be returned to its previous value
        as the later TX tests will change this variable.
    */
    status = skiq_write_tx_data_flow_mode(local_card, local_hdl,
                skiq_tx_with_timestamps_allow_late_data_flow_mode);
    if( 0 != status )
    {
        if( -ENOTSUP == status )
        {
            fprintf(stderr, "Error: the currently loaded bitfile doesn't"
                    " support late timestamp mode (result code %" PRIi32
                    ")\n", status);
            status = -2;
        }
        else
        {
            fprintf(stderr, "Error: unable to configure Tx data flow mode"
                    " (result code %" PRIi32 ")\n", status);
        }

        goto finished;
    }

    /* Run the normal TX timestamp test. */
    if( running )
    {
        printf("*** Running transmit test with TX timestamp mode\n");
        status = perform_tx_test(local_card, local_hdl, tx_blocks,
                    num_blocks, skiq_tx_with_timestamps_data_flow_mode, NO_DELAY,
                    STOP_AFTER_TX, &tx_errors, &late_errors);
        if( 0 != status )
        {
            fprintf(stderr, "Error: failed to run timestamp mode test (result"
                    " code %" PRIi32 ")\n", status);
            goto finished;
        }
        printf("Info: (normal timestamp mode): number of transmit errors is %"
                PRIu32 "; number of late timestamp errors is %" PRIu32 "\n",
                tx_errors, late_errors);
        tx_errors = 0;
        late_errors = 0;
    }

    status = skiq_read_fpga_semantic_version( card, &major, &minor, &patch );
    if(status == 0)
    {
        /* Write the timestamp base for the TX timestamp tests */
        status = skiq_write_tx_timestamp_base(card, timestamp_base);
        if((FPGA_VERSION(major, minor, patch) < FPGA_VERSION(3,15,1)) && (status == -ENOSYS))
        {
            printf("Info: attempted to set non-default system timestamp base on card %" PRIu8 " "
                    "with unsupported FPGA version: (%s) - (result code %" PRIi32 ")\n",
                    card, FPGA_VERSION_STR(major,minor,patch), status);
            status = 0;
        }
        else if(status != 0)
        {
            fprintf(stderr, "Error: unable to write the timestamp base on card %" PRIu8 ""
                            " (result code %" PRIi32 ")\n", card, status);
            goto finished;
        }
    }
    else
    {
        fprintf(stderr, "Error: unable to read FPGA version when configuring the transmit timestamp base"
                "on card %" PRIu8 " (result code %" PRIi32 ")\n", card, status);
        goto finished;
    }

    /* Run the late TX timestamp test. */
    if( running )
    {
        printf("*** Running transmit test with TX timestamp mode"
                " (allow late)\n");
        status = perform_tx_test(card, hdl, tx_blocks, num_blocks,
                    skiq_tx_with_timestamps_allow_late_data_flow_mode, NO_DELAY,
                    STOP_AFTER_TX, &tx_errors, &late_errors);
        if( 0 != status )
        {
            fprintf(stderr, "Error: failed to run timestamp mode test (result"
                    " code %" PRIi32 ")\n", status);
            goto finished;
        }
        printf("Info: (late timestamp mode)  : number of transmit errors is %"
                PRIu32 "; number of late timestamp errors is %" PRIu32 "\n",
                tx_errors, late_errors);
        if( 0 != late_errors )
        {
            fprintf(stderr, "Error: running in late timestamp mode should"
                    " produce no late timestamp errors!\n");
            status = -2;
        }
        else
        {
            printf("Info: late timestamp test succeeded!\n");
            status = 0;
        }
    }

finished:
    printf("Info: finished with late timestamp test.\n");

    if(status == 0)
    {
        printf("Info: resetting the timestamp base to default RF timestamp base.\n");
        status = skiq_write_tx_timestamp_base(card, skiq_tx_rf_timestamp);
        if((FPGA_VERSION(major, minor, patch) < FPGA_VERSION(3,15,1)) && (status == -ENOSYS))
        {
            printf("Info: attempted to set non-default system timestamp base on card %" PRIu8 " "
                    "with unsupported FPGA version: (%s) - (result code %" PRIi32 ")\n",
                    card, FPGA_VERSION_STR(major,minor,patch), status);
            status = 0;
        }
        else if(status != 0)
        {
            fprintf(stderr, "Error: unable to write the timestamp base on card %" PRIu8 ""
                            " (result code %" PRIi32 ")\n", card, status);
        }
    }

    return status;
}


/**
    @brief  Run TX empty FIFO test.

    @param[in]  local_card      The card index of the card to use.
    @param[in]  local_hdl       The handle to transmit on.
    @param[in]  tx_blocks       A pointer to the list of blocks that should
                                be transmitted; this should not be NULL.
    @param[in]  num_blocks      The number of blocks to transmit (the number
                                of blocks in tx_blocks).

    This test attempts to check if the late timestamp counter is incremented
    incorrectly when the FIFO reaches the empty state by transmitting the
    user-specified file and waiting for the TX FIFO to become empty. The number
    of late timestamps shouldn't increment once the TX FIFO is empty...

    @return 0 on success, else an errno representing the failure.
*/
static int32_t
run_tx_empty_fifo_test(uint8_t local_card, skiq_tx_hdl_t local_hdl,
        skiq_tx_block_t ***tx_blocks, uint32_t num_blocks)
{
    int32_t status = 0;
    int32_t stop_status = 0;
    uint32_t tx_errors = 0;
    uint32_t before_late_errors = 0;
    uint32_t after_late_errors = 0;
    uint32_t interblock_delay_us = 0;
    struct timespec tm = {0, 0};
    bool tx_streaming = true;

    printf("*** Running TX Empty FIFO timestamp test\n");

    printf("Info: transmitting file...\n");

    /* To make certain each block is sent to the FPGA and then marked as late,
       make the interblock delay:

       (2 x time_to_tx_block_in_us) = \
       (2 x block_size_in_samples x 1e6 us_in_sec / sample_rate_in_samples_per_sec)
    */
    interblock_delay_us = (uint32_t)((2 * block_size_in_words * 1000000.0) / sample_rate);

    status = perform_tx_test(card, hdl, tx_blocks, num_blocks,
                skiq_tx_with_timestamps_data_flow_mode, interblock_delay_us,
                NO_STOP_AFTER_TX, &tx_errors, &before_late_errors);
    if( 0 != status )
    {
        fprintf(stderr, "Error: failed to run timestamp mode test (result"
                " code %" PRIi32 ")\n", status);
        goto finished;
    }
    printf("Info: number of transmit errors before stopping streaming is %"
            PRIu32 "; number of late timestamp errors is %" PRIu32 "\n",
            tx_errors, before_late_errors);

    if( !running )
    {
        status = -1;
        goto finished;
    }

    tm.tv_sec = EMPTY_FIFO_SLEEP_TIME_SEC;
    tm.tv_nsec = 0;
    printf("Info: sleeping for %" PRIu64 " seconds...\n", (uint64_t) tm.tv_sec);
    do
    {
        errno = 0;
        status = nanosleep(&tm, &tm);
        if( 0 != status )
        {
            status = (int32_t) errno;
            if( EINTR != status )
            {
                fprintf(stderr, "Error: failed to sleep for specified time"
                        " (result code %" PRIi32 ")\n", status);
                goto finished;
            }
        }
    } while ((status != 0) && (running));

    status = skiq_read_tx_num_late_timestamps(local_card, local_hdl,
                &after_late_errors);
    if( status != 0 )
    {
        fprintf(stderr, "Error: failed to read number of late timestamps"
                " (result code %" PRIi32 ")\n", status);
        goto finished;
    }
    printf("Info: number of transmit errors after  stopping streaming is %"
            PRIu32 "; number of late timestamp errors is %" PRIu32 "\n",
            tx_errors, after_late_errors);

    status = skiq_stop_tx_streaming(local_card, local_hdl);
    if( 0 != status )
    {
        fprintf(stderr, "Error: failed to stop TX streaming (result code %"
            PRIi32 ")\n", status);
        goto finished;
    }
    tx_streaming = false;

    if( before_late_errors != num_blocks )
    {
        fprintf(stderr, "Error: the number of late timestamps don't match"
                " the number of submitted blocks (%u); test failed.\n", num_blocks);
        status = -2;
    }
    else if( before_late_errors != after_late_errors )
    {
        fprintf(stderr, "Error: the number of late timestamps don't match"
                " before and after stopping streaming; test failed.\n");
        status = -2;
    }
    else
    {
        printf("Info: TX FIFO empty test succeeded!\n");
    }

finished:
    printf("Info: finished with TX empty FIFO test.\n");

    if( tx_streaming )
    {
        stop_status = skiq_stop_tx_streaming(local_card, local_hdl);
        if( 0 != stop_status )
        {
            fprintf(stderr, "Error: failed to stop TX streaming (result code %"
                    PRIi32 ").\n", stop_status);
        }
        tx_streaming = false;
    }

    return status;
}



/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
static void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}


/*****************************************************************************/
/** This is the main function for executing the application.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ASCII string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    int32_t status = 0;
    uint32_t j = 0;
    int result = 0;
    uint32_t num_bytes_in_file = 0;
    uint32_t num_blocks = 0;
    ssize_t bytes_read = 0;
    bool skiq_initialized = false;
    pid_t owner = 0;
    uint32_t num_runs = 1;

    /* reference to an array of transmit block references */
    skiq_tx_block_t **p_tx_blocks = NULL;

    uint32_t read_bandwidth = 0;
    uint32_t actual_bandwidth = 0;
    uint32_t read_sample_rate = 0;
    double actual_sample_rate = 0.0;
    skiq_chan_mode_t chan_mode = skiq_chan_mode_single;

    int32_t overall_status = 0;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( (!run_late_test) && (!run_fifo_empty_test) )
    {
        printf("Info: no tests specified; running all tests.\n");
        run_late_test = true;
        run_fifo_empty_test = true;
    }

    if( NULL == p_file_path )
    {
        fprintf(stderr, "Error: no input file specified\n");
        status = -1;
        goto finished;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number,"
            " not both\n");
        status = -1;
        goto finished;
    }
    if( UINT8_MAX == card )
    {
        card = DEFAULT_CARD_NUMBER;
    }

    if( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if( 0 != status )
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error : card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    if( 0 == strncasecmp(p_hdl, "A1", 3) )
    {
        hdl = skiq_tx_hdl_A1;
        /* set chan mode to single */
        chan_mode = skiq_chan_mode_single;
        printf("Info: using Tx handle A1\n");
    }
    else if( 0 == strncasecmp(p_hdl, "A2", 3) )
    {
        hdl = skiq_tx_hdl_A2;
        /* set chan mode to dual */
        chan_mode = skiq_chan_mode_dual;
        block_size_in_words = block_size_in_words_dual;
        printf("Info: using Tx handle A2\n");
    }
    else
    {
        fprintf(stderr, "Error: invalid handle %s specified\n", p_hdl);
        status = -1;
        goto finished;
    }

    if((0 == strncasecmp(p_timestamp_base, "rf", 10)) ||
       (0 == strncasecmp(p_timestamp_base, "'rf'", 10)))
    {
        timestamp_base = skiq_tx_rf_timestamp;
        printf("Info: using RF free running clock for transmit timestamp base\n");
    }
    else if((0 == strncasecmp(p_timestamp_base, "system", 10)) ||
            (0 == strncasecmp(p_timestamp_base, "'system'", 10)))
    {
        timestamp_base = skiq_tx_system_timestamp;
        printf("Info: using system free running clock for transmit timestamp base\n");
    }
    else
    {
        fprintf(stderr, "Error: invalid free running clock '%s' specified\n", p_timestamp_base);
        status = -EINVAL;
        goto finished;
    }

    input_fp = fopen(p_file_path, "rb");
    if( input_fp == NULL )
    {
        fprintf(stderr, "Error: unable to open input file %s\n", p_file_path);
        status = -1;
        goto finished;
    }

    // determine how large the file is and how many blocks we'll need to send
    errno = 0;
    result = fseek(input_fp, 0, SEEK_END);
    if( -1 == result)
    {
        fprintf(stderr, "Error: failed to seek in input file %s (result"
                " code %d: '%s')\n", p_file_path, errno, strerror(errno));
        status = -1;
        goto finished;
    }
    errno = 0;
    num_bytes_in_file = ftell(input_fp);
    if( -1 == num_bytes_in_file )
    {
        fprintf(stderr, "Error: failed to get file size for input file %s"
                " (result code %d: '%s')\n", p_file_path, errno,
                strerror(errno));
        status = -1;
        goto finished;
    }
    rewind(input_fp);

    // setup the sample buffer based on the samples in file
    num_blocks = (num_bytes_in_file / (block_size_in_words * 4));

    // if we don't end on an even block boundary, we need an extra block
    if ( (num_bytes_in_file % (block_size_in_words * 4)) != 0 )
    {
        num_blocks++;
    }

    p_tx_blocks = calloc(num_blocks, sizeof(skiq_tx_block_t *));
    if( NULL == p_tx_blocks )
    {
        fprintf(stderr, "Error: unable to allocate %u bytes to hold transmit"
                " block descriptors\n",
                (uint32_t)(num_blocks * sizeof(skiq_tx_block_t *)));
        status = -ENOMEM;
        goto finished;
    }

    for( j = 0; j < num_blocks; j++ )
    {
        if( chan_mode == skiq_chan_mode_dual )
        {
            /*
                For dual channel mode, allocate a transmit block by doubling
                the number of words.
            */
            p_tx_blocks[j] = skiq_tx_block_allocate(2 * block_size_in_words);
        }
        else
        {
            p_tx_blocks[j] = skiq_tx_block_allocate(block_size_in_words);
        }

        if( NULL == p_tx_blocks[j] )
        {
            fprintf(stderr, "Error: unable to allocate transmit block data\n");
            status = -ENOMEM;
            goto finished;
        }

        bytes_read = fread(p_tx_blocks[j]->data, sizeof(int32_t),
                        block_size_in_words, input_fp);
        result = ferror(input_fp);
        if( 0 != result )
        {
            fprintf(stderr, "Error: unable to read from input file %s (result"
                    " code %d: '%s')\n", p_file_path, result, strerror(result));
            status = -1;
            goto finished;
        }
        if( bytes_read != (ssize_t) block_size_in_words )
        {
            fprintf(stderr, "Error: failed to read entire block from input"
                    " file %s (read %" PRIi64 "/ %" PRIi64 " bytes);"
                    " continuing.\n", p_file_path, (int64_t) bytes_read,
                    (int64_t) block_size_in_words);
        }
        if( skiq_chan_mode_dual == chan_mode )
        {
            /*
               Duplicate the block of samples into the second half of the
               transmit block's data array.
            */
            memcpy(&(p_tx_blocks[j]->data[block_size_in_words*2]),
                p_tx_blocks[j]->data, block_size_in_words * 4);
        }

        if( feof(input_fp) )
        {
            printf("Info: reached unexpected end-of-file at block %" PRIu32
                    "; continuing.\n", j);
            num_blocks = j;
            break;
        }
    }

    fclose(input_fp);
    input_fp = NULL;

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full,
                &card, 1);
    if( status != 0 )
    {
        if( ( EBUSY == status ) &&
            ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    /* set the channel mode */
    status = skiq_write_chan_mode(card, chan_mode);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to set %s channel mode (result"
                " code %" PRIi32 ")\n",
                (skiq_chan_mode_single == chan_mode) ? "single" : "dual",
                status);
        status = -1;
        goto finished;
    }

    // configure our Tx parameters
    
    /* pick at most 80% of the sample rate as the bandwidth since the 9371 cannot go any higher */
    status = skiq_write_tx_sample_rate_and_bandwidth(card, hdl,
                                                     sample_rate, (sample_rate * 8) / 10 );
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to configure TX sample rate (result code"
                " %" PRIi32 ")\n", status);
        goto finished;
    }
    status = skiq_read_tx_sample_rate_and_bandwidth(card, hdl,
                &read_sample_rate, &actual_sample_rate,
                &read_bandwidth, &actual_bandwidth);
    if( 0 == status )
    {
        if( actual_sample_rate != (double) sample_rate )
        {
            fprintf(stderr, "Error: failed to set sample rate & bandwidth; wanted sample rate %"
                    PRIu32 " Hz with bandwidth %" PRIu32 " Hz, read back sample rate %.0f Hz with "
                    "bandwidth %" PRIu32 " Hz.\n", sample_rate, (sample_rate * 8) / 10,
                    actual_sample_rate, actual_bandwidth);
            status = -1;
            goto finished;
        }
        else
        {
            /* save sample_rate to what libsidekiq says it actually is */
            sample_rate = (uint32_t)actual_sample_rate;
            printf("Info: set sample rate to %.0f Hz and bandwidth to %" PRIu32 " Hz.\n",
                    actual_sample_rate, actual_bandwidth);
        }
    }
    else
    {
        fprintf(stderr, "Error: failed to read TX sample rate and bandwidth"
                " (result code %" PRIi32 "); continuing.\n", status);
    }

    status = skiq_write_tx_LO_freq(card, hdl, lo_freq);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to configure Tx LO frequency (result"
                " code %" PRIi32 ")\n", status);
        goto finished;
    }
    printf("Info: configured Tx LO freq to %" PRIu64 " Hz.\n", lo_freq);

    if( UINT16_MAX == attenuation )
    {
        /* No TX attenuation was specified; go ahead and max it out. */
        skiq_param_t card_params;
        memset(&card_params, 0, sizeof(card_params));

        status = skiq_read_parameters(card, &card_params);
        if( 0 != status )
        {
            fprintf(stderr, "Error: failed to get Sidekiq parameters (result"
                    " code %" PRIi32 ")\n", status);
            status = -1;
            goto finished;
        }

        attenuation = card_params.tx_param[(int) hdl].atten_quarter_db_max;
        printf("Info: setting attenuation to maximum (%" PRIu16 ").\n",
                attenuation);
    }
    status = skiq_write_tx_attenuation(card, hdl, attenuation);
    if( 0 != status )
    {
        fprintf(stderr, "Error: failed to set TX attenuation to %" PRIu16
                " (result code %" PRIi32 ")\n", attenuation, status);
        status = -1;
        goto finished;
    }

    status = skiq_write_tx_block_size(card, hdl, block_size_in_words);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to configure TX block size (result"
                " code %" PRIi32 ")\n", status);
        goto finished;
    }

    /* Run the actual tests. */
    num_runs += repeat;
    do
    {
        if( (0 != num_runs) && (0 == (num_runs % 25)) )
        {
            printf("Test iteration %" PRIu32 ".\n", num_runs);
        }

        if( run_late_test )
        {
            printf("\n\n");
            status = run_late_timestamp_test(card, hdl, &p_tx_blocks,
                        num_blocks);
            if( 0 != status )
            {
                fprintf(stderr, "Error: late timestamp test failed (result"
                        " code %" PRIi32 ")\n", status);
                if( 0 == overall_status )
                {
                    overall_status = status;
                }
            }
        }

        if( run_fifo_empty_test )
        {
            printf("\n\n");
            status = run_tx_empty_fifo_test(card, hdl, &p_tx_blocks,
                        num_blocks);
            if( 0 != status )
            {
                fprintf(stderr, "Error: TX FIFO empty test failed (result"
                        " code %" PRIi32 ")\n", status);
                if( 0 == overall_status )
                {
                    overall_status = status;
                }
            }
        }

        if( 0 < num_runs  )
        {
            num_runs--;
        }
    } while( (num_runs > 0) && (running) );

finished:
    printf("Info: shutting down application...\n");

    if( skiq_initialized )
    {
        // stop sending samples
        skiq_stop_tx_streaming(card, hdl);

        /* Deallocate the transmit block memory. */
        if( NULL != p_tx_blocks )
        {
            for (j = 0; j < num_blocks; j++)
            {
                skiq_tx_block_free(p_tx_blocks[j]);
            }

            free(p_tx_blocks);
            p_tx_blocks = NULL;
        }

        skiq_exit();
        skiq_initialized = false;
    }

    if( NULL != input_fp )
    {
        fclose(input_fp);
        input_fp = NULL;
    }

    if( 0 != overall_status )
    {
        status = overall_status;
    }

    return (status);
}

