/*! \file usb_rx_test.c
 * \brief This file contains a basic receive across USB test app.
 *  
 * <pre>
 * Copyright 2013-2017 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sidekiq_api.h>
#include <sidekiq_hal.h>
#include <dma_interface_api.h>

int main( void )
{
    uint8_t card = 0;
    skiq_rx_block_t* p_rx_block;
    uint32_t len;
    int32_t status;
    skiq_rx_status_t rx_status;
    uint32_t count = 0;
    
    uint32_t n;
    
    skiq_rx_hdl_t hdl = skiq_rx_hdl_A1; 
    
    status = skiq_init(skiq_xport_type_usb, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    printf( "CARD: %d\n", card );
    
    status=skiq_write_rx_sample_rate_and_bandwidth( card, 
                                                    hdl,
                                                    1000000,
                                                    500000 );
    if (0 != status )
    {
        printf("Error: failed to set Rx sample rate or bandwidth(using default from last config file)...status is %d\n",
                status);
            return( -1 );
    }

    status = skiq_write_rx_data_src( card, hdl, skiq_data_src_counter );
    if( 0 != status ) return( -1 );
    
    status = skiq_start_rx_streaming( card, hdl );
    if( 0 != status ) return( -1 );
    
    while( count < 4 )
    {
        do
        {
            rx_status = skiq_receive( card, &hdl, &p_rx_block, &len );
        } while ( skiq_rx_status_success != rx_status ); 

        {
            uint8_t *data = (uint8_t*)(p_rx_block);
            printf( "DATA LEN: %d\n", len );
            for( n = 0; n < 64; n+=8 )
            {
                printf( "DATA[]: %x %x %x %x %x %x %x %x\n", data[n], data[n+1], 
                                                             data[n+2], data[n+3],
                                                             data[n+4], data[n+5], 
                                                             data[n+6], data[n+7] );
            }
        }
        count++;
        printf("\n\n");
    }
    
    skiq_stop_rx_streaming( card, hdl );
    
    skiq_exit();
    
    return( 0 );
}
