/**
 * @file   card_mgr_uninit.c
 * @date   Wed Mar  1 15:23:29 CST 2017
 *
 * @brief Programmatically attempts to lock access to all available cards, then destroys card
 * manager structures in order to start fresh.
 *
 * <pre>
 * Copyright 2017-2020 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <inttypes.h>
#include <stdio.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "sidekiq_card_mgr_private.h"
#include "sidekiq_api_factory.h"


/***** GLOBAL FUNCTIONS *****/

int main( int argc, char* argv[] )
{
    uint8_t cards[SKIQ_MAX_NUM_CARDS] =
    {
        [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = SKIQ_MAX_NUM_CARDS,
    };
    uint8_t num_cards = 0;
    int32_t status = 0;
    uint8_t i;

    printf("libsidekiq API %" PRIu8 ".%" PRIu8 ".%" PRIu8 "%s (githash %s)\n",
        (uint8_t) LIBSIDEKIQ_VERSION_MAJOR,
        (uint8_t) LIBSIDEKIQ_VERSION_MINOR,
        (uint8_t) LIBSIDEKIQ_VERSION_PATCH,
#ifdef LIBSIDEKIQ_VERSION_LABEL
        LIBSIDEKIQ_VERSION_LABEL,
#else
        "",
#endif
        _GIT_HASH);

    // initialize card_mgr to obtain all cards in system
    status = card_mgr_init();
    if ( status != 0 )
    {
        fprintf(stderr, "Error: failed to initialize card manager (status code = %" PRIi32 ")\n",
                status);
        return 1;
    }

    // grab any and all cards regardless of transport type
    status = card_mgr_get_cards(cards, &num_cards);
    if( status != 0 )
    {
        fprintf(stderr, "Error: failed to get cards from card manager (status code = %" PRIi32
                ")\n", status);

        card_mgr_exit();
        return 1;
    }
    else if( 0 != num_cards )
    {
        // print out any cards detected by card_mgr
        card_mgr_display_info();
    }

    // run through each card detected and ensure it isn't in use
    for (i = 0; ( i < num_cards ) && ( 0 == status ); i++)
    {
        pid_t owner = 0;

        // if we can lock the card, assume that the card is not in use
        status = card_mgr_card_trylock(cards[i], &owner);
        if( 0 != status )
        {
            if ( status == EBUSY )
            {
                fprintf(stderr, "Warning: card %" PRIu8 " is in use by process %jd\n", cards[i],
                        (intmax_t)owner);
            }
            else
            {
                fprintf(stderr, "Error: failed to lock card %" PRIu8 " (status code = %" PRIi32
                        ")\n", cards[i], status);
            }
        }
        else
        {
            printf("Info: Successfully acquired card %" PRIu8 "\n", cards[i]);
        }
    }

    // if no errors, free the card manager shared memory
    if ( 0 == status )
    {
        status = skiq_fact_card_mgr_destroy();
        if ( 0 != status )
        {
            fprintf(stderr, "Error: failed to free card manager shared memory (status code = %"
                    PRIi32 ")\n", status);

            card_mgr_exit();
        }
        else
        {
            printf("Info: Successfully freed card manager shared memory\n");
        }
    }
    else
    {
        fprintf(stderr, "Error: Unable to acquire all cards, bailing\n");

        /* the attempt to lock all of the cards has failed, unlock everything that may have been
         * locked by us */
        for (i = 0; i < num_cards; i++)
        {
            card_mgr_card_unlock(cards[i]);
        }

        card_mgr_exit();
    }

    return (0 == status) ? 0 : 1;
}
