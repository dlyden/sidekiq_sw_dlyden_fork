/*! \file check_fpga_config.c
 * \brief This file contains a basic application that checks the
 * configuration state of the FPGA.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <usb_interface.h>
#include <usb_private.h>

#include <arg_parser.h>

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- check the FPGA configuration status";
static const char* p_help_long =
"\
Checks the configuration status of the FPGA to determine if it has a valid\n\
bitstream currently loaded onto it.\n\
\n\
Defaults:\n\
  --mode=PCIE";

/* command line argument variables */
static char* p_mode = "PCIE";
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("mode",
                'm',
                "Transport mode to use, either USB or PCIE",
                "TRANSPORT",
                &p_mode,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the main function for checking the FPGA configuration state.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    skiq_xport_type_t type;
    skiq_xport_init_level_t level=skiq_xport_init_level_basic;
    uint8_t config=0;
    int32_t status = 0;
    uint8_t index=0;
    pid_t owner = 0;

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if ( (UINT8_MAX == card) && (NULL == p_serial) )
    {
        fprintf(stderr, "Error: must specify card ID or serial number\n");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }
    else if ( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number,"
                " not both\n");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if (NULL != p_serial)
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    if( 0 == strncasecmp(p_mode, "PCIE", 5) )
    {
        type = skiq_xport_type_pcie;
    }
    else if( 0 == strncasecmp(p_mode, "USB", 4) )
    {
        type = skiq_xport_type_usb;
    }
    else
    {
        fprintf(stderr, "Error: invalid mode specified\n");
        return (-1);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    // initialize just the USB level and read the DONE bit of the FPGA
    status = skiq_init(skiq_xport_type_usb, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        return (-1);
    }

    usb_get_index(card, &index);
    usb_read_fpga_config( index, &config );

    skiq_exit();

    if( config==0 || config==0xFF )
    {
        printf("Info: FPGA not programmed.\n");
    }
    else
    {
        if( type == skiq_xport_type_pcie)
        {
            /*
                try to init across PCIe to see if the driver/FPGA are coming up
                correctly
            */
            if( skiq_init(type, level, &card, 1) == 0 )
            {
                printf("Info: FPGA programmed and ready.\n");
                    skiq_exit();
            }
            else
            {
                printf("Info: FPGA programmed but not ready.  A reboot is of"
                        " the host is required\n");
            }
        }
        else
        {
            printf("Info: FPGA programmed and ready.\n");
        }
    }

    return 0;
}

