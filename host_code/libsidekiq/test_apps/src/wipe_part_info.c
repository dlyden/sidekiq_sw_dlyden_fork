/*! \file wipe_part_info.c
 * \brief This file contains a basic application that erases the various
 * part information of a Sidekiq.
 *
 * <pre>
 * Copyright 2013 - 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_hal.h>
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <sidekiq_private.h>

#include "hardware.h"
#include "card_services.h"
#include "sidekiq_usb_cmds.h"
#include "sidekiq_card_mgr_private.h"

#include <arg_parser.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <inttypes.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "wipes part info";
static const char* p_help_long = "Wipes the part information stored in EEPROM";

static uint8_t card = UINT8_MAX;
static char* p_part_num = NULL;
static char* p_rev  = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_REQ("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_REQ("part-num",
                0,
                "Part number string",
                "PN",
                &p_part_num,
                STRING_VAR_TYPE),    
    APP_ARG_REQ("rev",
                0,
                "Revision string",
                "REV",
                &p_rev,
                STRING_VAR_TYPE),    
    APP_ARG_TERMINATOR,
};
    
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    pid_t owner = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, blank_eeprom_version_legacy, SKIQ_FACT_SERIAL_NUM_LEGACY_BYTE_LEN, 0xFF);
    ARRAY_WITH_DEFAULTS(uint8_t, blank_eeprom_version_alphanum, SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN, 0xFF);
    ARRAY_WITH_DEFAULTS(uint8_t, blank_eeprom_hw_iface_vers, HW_IFACE_VERS_EEPROM_LEN, 0xFF);

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return status;
    }

    if((SKIQ_MAX_NUM_CARDS - 1) < card)
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return -ERANGE;
    }

    status = skiq_is_card_avail(card, &owner);
    if(status != 0)
    {
        printf("Warning: card %u is currently locked by another process"
                    " (PID=%u)\n", card, (unsigned int)(owner));
        return status;
    }

    status = skiq_fact_init_for_config(card);
    if (status == 0)
    {        
        // mPCIe and m2 are legacy and don't use the part info
        printf("Wiping part info\n");
        status=skiq_fact_wipe_part_info(card, p_part_num, p_rev);
        if(status != 0)
        {
            fprintf(stderr, "Error: failed to wipe part info on card %" PRIu8 " (status=%d)\n", card, status);
        }

        // write legacy serial number and legacy hw vers
        printf("Wiping legacy serial # / hw vers / pcb\n");
        status=card_write_eeprom(card, VERSION_INFO_LEGACY_ADDR, blank_eeprom_version_legacy, SKIQ_FACT_SERIAL_NUM_LEGACY_BYTE_LEN);
        if(status != 0)
        {
            fprintf(stderr, "Error: wiping legacy info on card %" PRIu8 " (status=%d)\n", status, card);
        }

        // write alphanumeric serial number
        printf("Wiping alphanumeric serial #\n");
        status=card_write_eeprom(card, VERSION_INFO_ALPHANUMERIC_ADDR, blank_eeprom_version_alphanum, SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN);
        if(status != 0)
        {
            fprintf(stderr, "Error: wiping version info on card %" PRIu8 " (status=%d)\n", card, status);
        }

        skiq_part_t part = _skiq_get_part(card);

        // wiping hardware interface version is not supported for Sidekiq mPCIe or M.2
        if((part != skiq_mpcie) && (part != skiq_m2))
        {
            // write hardware interface version
            printf("Wiping hardware interface version\n");
            status=card_write_eeprom(card, HW_IFACE_VERS_EEPROM_ADDR, blank_eeprom_hw_iface_vers, HW_IFACE_VERS_EEPROM_LEN);
            if(status != 0)
            {
                fprintf(stderr, "Error: wiping hardware interface version on card %" PRIu8 " (status=%d)\n", card, status);
            }
        }

        // clear out cached card mgr values
        status = card_mgr_destroy_card_mgr();
        if(status != 0)
        {
            fprintf(stderr, "Error: clearing cached card mgr values on card %" PRIu8 " (status=%d)\n", card, status);
        }

        status = skiq_fact_exit_for_config(card);
        if(status != 0)
        {
            fprintf(stderr, "Error: failed to exit factory config on card %" PRIu8 " (status=%d)\n", card, status);
        }
    }
    else
    {
        fprintf(stderr, "Error: unable to initialize libsidekiq for factory config on card %" PRIu8 " (status=%d)\n", card, status);
    }

    return status;
}

