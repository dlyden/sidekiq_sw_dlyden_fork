/*! \file verify_golden_fpga_and_lock_multicard.c
 *
 * \brief This file contains a basic application that checks golden FPGA bitstream, optionally
 * stores golden FPGA bitstream, then locks the associated flash sectors on to multiple cards in
 * parallel.
 *
 * <pre>
 * Copyright 2017-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>

#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "arg_parser.h"

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- verify / lock golden FPGA bitstream into flash for multiple cards";
static const char* p_help_long = "\
Scan the system for Sidekiq cards, check golden FPGA presence and store image if not detected,\n\
then finally lock the associated flash sectors";

/* command line argument variables */
static char* p_serial = NULL;

/* file path to golden FPGA bitstream */
static char* p_golden_file_path = NULL;
static FILE* golden_fp = NULL;

static bool detect_only = false;

/*
    Flag to indicate if the shutdown signals (SIGINT & SIGTERM) are currently
    masked (suspended).
*/
static bool masked = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("serial",
                'S',
                "Target card by serial number",
                "SERIAL",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("golden",
                0,
                "Golden bitstream file to source for programming",
                "PATH",
                &p_golden_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("detect-only",
                0,
                "Print results of golden FPGA checks without actually programming",
                NULL,
                &detect_only,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


#if (defined __MINGW32__)
/* Unfortunately Mingw does not support sigset_t, so define a fake type definition and stub out the
   related functions. */
typedef int sigset_t;
static inline int mask_signals(sigset_t *old_mask) { return 0; }
static inline int unmask_signals(sigset_t *old_mask) { return 0; }
static inline bool shutdown_signal_pending(void) { return false; }
#else
/**
    @brief  Mask (block) the SIGINT and SIGTERM signals during critical
            operations

    @param[out]  old_mask   The original signal mask, needed by
                            ::unmask_signals().

    @note   Pending signals will be raised when ::unmask_signals() is called.

    @return 0 on success, else an errno.
*/
static int
mask_signals(sigset_t *old_mask)
{
    int result = 0;
    sigset_t new_mask;

    sigemptyset(&new_mask);
    sigaddset(&new_mask, SIGINT);
    sigaddset(&new_mask, SIGTERM);

    errno = 0;
    result = pthread_sigmask(SIG_BLOCK, &new_mask, old_mask);

    return (0 == result) ? result : errno;
}

/**
    @brief  Unmask (unblock) the signals blocked by ::mask_signals().

    @param[in]  old_mask    The original signal mask, as generated by
                            ::mask_signals().

    @note   Pending signals will be processed after calling this function.

    @return 0 on success, else an errno.
*/
static int
unmask_signals(sigset_t *old_mask)
{
    int result = 0;

    errno = 0;
    result = pthread_sigmask(SIG_SETMASK, old_mask, NULL);

    return (0 == result) ? result : errno;
}

/**
    @brief  Check if a shutdown signal (SIGINT or SIGTERM) is pending (while
            masked).

    @return true if a signal is pending, else false.
*/
static bool
shutdown_signal_pending(void)
{
    int result = 0;
    sigset_t pending_mask;
    bool pending = false;

    errno = 0;
    result = sigpending(&pending_mask);
    if (0 == result)
    {
        pending = ((1 == sigismember(&pending_mask, SIGINT)) ||
                   (1 == sigismember(&pending_mask, SIGTERM)));
    }
    else
    {
        printf("Debug: sigpending() failed (%d: '%s')\n",
                errno, strerror(errno));
    }

    return pending;
}
#endif	/* __MINGW32__ */


static int32_t verify_then_lock( uint8_t card,
                                 char *p_sernum,
                                 FILE *fp )
{
    int32_t status;

    status = skiq_fact_verify_golden_fpga_from_flash( card, fp );
    if ( status == 0 )
    {
        fprintf(stderr, "Info: FPGA bitstream verification succeeded on card %u "
                "(s/n %s)\n", card, p_sernum);

        /* lock the sectors */
        status = skiq_fact_lock_golden_sectors( card );

        if ( status != 0 )
        {
            fprintf(stderr, "Error: Failed to lock flash sectors (status = %d) on card "
                    "%u (s/n %s)\n", status, card, p_sernum);
        }
    }
    else
    {
        /* verification failure indicated with -ENOEXEC, Exec format error */
        status = -ENOEXEC;
    }

    return (status);
}


static void *program_card(void *data)
{
    uint8_t card = *((uint8_t *)data);
    bool locked = false;
    char *p_sernum;
    int32_t status;
    FILE *fp = NULL;

    status = skiq_read_serial_string( card, &p_sernum );
    if ( status != 0 )
    {
        p_sernum = "UNKNOWN";
    }

    status = skiq_fact_is_golden_locked( card, &locked );
    if ( status == 0 )
    {
        fprintf(stderr, "Info: flash sectors %slocked on card %u (s/n %s)\n", locked ? "" : "NOT ",
                card, p_sernum);
    }
    else
    {
        fprintf(stderr, "ERROR: Failure to query flash lock status (%d) on card %u (s/n %s)\n",
                status, card, p_sernum);
    }

    if ( !detect_only && ( status == 0 ) )
    {
        if ( golden_fp )
        {
            fp = fopen( p_golden_file_path, "r" );
            if ( fp == NULL )
            {
                perror("fopen()");
                status = -ENOENT;
            }
        }

        if ( fp )
        {
            if ( !locked )
            {
                /*
                    If the signals were successfully masked, check if one of
                    them was received before attempting the below critical
                    operations. This check allows the user to safely interrupt
                    the program (and not randomly while attempting to verify
                    the card and lock the golden image sectors).
                */
                if( ( masked ) && ( shutdown_signal_pending() ) )
                {
                    goto thread_finished;
                }

                status = verify_then_lock( card, p_sernum, fp );
                if ( status == -ENOEXEC )
                {
                    fprintf(stderr, "Info: FPGA bitstream verification FAILED on card %u "
                            "(s/n %s)\n", card, p_sernum);

                    /*
                        If the signals were successfully masked, check if one
                        of them was received before storing the golden image;
                        this check allows the user to safely interrupt the
                        program (and not randomly while attempting to program
                        the card).
                    */
                    if( ( masked ) && ( shutdown_signal_pending() ) )
                    {
                        goto thread_finished;
                    }

                    status = skiq_fact_save_golden_fpga_to_flash( card, fp );
                    if ( status == 0 )
                    {
                        printf("Info: successfully stored bitstream for card %u (s/n %s)\n",
                               card, p_sernum);

                        /*
                            If the signals were successfully masked, check if one of
                            them was received before attempting the below critical
                            operations. This check allows the user to safely interrupt
                            the program (and not randomly while attempting to verify
                            the card and lock the golden image sectors).
                        */
                        if( ( masked ) && ( shutdown_signal_pending() ) )
                        {
                            goto thread_finished;
                        }

                        status = verify_then_lock( card, p_sernum, fp );
                        if ( status == -ENOEXEC )
                        {
                            fprintf(stderr, "Info: FPGA bitstream verification FAILED AGAIN on "
                                    "card %u (s/n %s)\n", card, p_sernum);
                        }
                    }
                    else
                    {
                        fprintf(stderr, "ERROR: failed to store bitstream for card %u (s/n %s) "
                                "with status %d\n", card, p_sernum, status);
                    }
                }
            }
        }
    }

thread_finished:
    /* cleanup */
    if ( NULL != fp )
    {
        fclose( fp );
        fp = NULL;
    }

    pthread_exit( (void *)(intptr_t)status );

    return NULL;
}

/*****************************************************************************/
/** This is the main function for executing store_golden_fpga_multicard command.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status=0;
    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;

    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t num_cards=0;
    uint8_t i=0;

    int result = 0;
    sigset_t old_mask;

    pthread_t threads[SKIQ_MAX_NUM_CARDS];
    bool thread_started[SKIQ_MAX_NUM_CARDS] = { false };
    int failed_thread_result = 0;

    bool skiq_initialized = false;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if ( !detect_only )
    {
        if ( NULL == p_golden_file_path )
        {
            fprintf(stderr, "Error: no golden file specified\n");
            arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
            return (-1);
        }

        golden_fp = fopen( p_golden_file_path, "r" );
        if ( golden_fp == NULL )
        {
            fprintf(stderr, "Error opening golden file: %s\n", strerror(errno));
            arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
            return (-1);
        }
        else
        {
            fclose(golden_fp);
        }
    }

    /* determine how many Sidekiqs are there and their card IDs */
    skiq_get_cards( type, &num_cards, cards );

    /* limit the available cards if --serial is specified */
    /* If specified, attempt to find the card with a matching serial number. */
    if (NULL != p_serial)
    {
        status = skiq_get_card_from_serial_string(p_serial, &(cards[0]));
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, cards[0]);
        num_cards = 1;
    }

    /* no harm, no foul, no cards detected */
    if ( 0 == num_cards )
    {
        printf("Info: no cards detected.\n");
        return (0);
    }

    /*
        While not all of the following operations are criticla and should not
        be interrupted, the blocking of shutdown signals (SIGINT & SIGTERM)
        must be done here as skiq_init() can spin up threads, any of which can
        receive & handle these signals, and threads inherit the signal mask of
        the parent on creation. The functions should periodically check the
        pending signals to see if the user wants to shut things down.
    */
    result = mask_signals(&old_mask);
    if (0 != result)
    {
        printf("Warning: failed to block shutdown signals (return code %d);"
                " continuing but please do not press Ctrl-C during"
                " programming.\n", result);
    }
    else
    {
        masked = true;
    }

    printf("Info: initializing %" PRIu8 " card(s)...\n", num_cards);

    /* Initialize libsidekiq using the specified card(s). */
    status = skiq_init(type, level, cards, num_cards);
    if ( status != 0 )
    {
        if ( EBUSY == status )
        {
            fprintf(stderr, "Error: at least one card is already in use; cannot"
                    " initialize card(s).\n");
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }

        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    /*
        If the signals were successfully masked, check if one of them was
        received and, if so, shut down the program. This check allows the
        user to interrupt the program before doing work that shouldn't be
        interrupted.
    */
    if( ( masked ) && ( shutdown_signal_pending() ) )
    {
        printf("Info: got shutdown signal\n");
        goto finished;
    }

    /*
        Note that from this point, the shutdown signals are masked so SIGINT
        and SIGTERM won't immediately kill the program (which is good so the
        threads can't be killed while attempting to program cards). The
        individual threads will check at certain points if either of these
        signals have been received and, if detected, will cleanly stop. The code
        below, however, does not check the signals, so it will create all the
        threads and then wait for them to exit (which they should do upon
        success / failure receiving a signal).
    */

    /* program all of the cards in separate threads */
    for ( i=0; i<num_cards; i++ )
    {
        result = pthread_create( &(threads[i]), NULL, program_card,
                    (void *)&(cards[i]) );
        if ( 0 != result )
        {
            fprintf(stderr, "Error: FAILED to start thread for card %" PRIu8
                " (result code %d)\n", cards[i], result);
            failed_thread_result = result;
        }
        else
        {
            thread_started[i] = true;
        }
    }

    /* join all the threads */
    for ( i = 0; i < num_cards; i++ )
    {
        void *retval;
        int32_t thread_status;

        if ( !thread_started[i] )
        {
            continue;
        }

        if ( 0 == pthread_join( threads[i], &retval ) )
        {
            thread_status = (int32_t)(intptr_t)retval;
            if ( thread_status == 0 )
            {
                fprintf(stderr, "Info: Thread %ld returned with status %d\n", threads[i],
                        thread_status);
            }
            else
            {
                fprintf(stderr, "Error: Thread %ld FAILED with status %d\n", threads[i],
                        thread_status);
            }

            if ( status == 0 )
            {
                status = thread_status;
            }

            thread_started[i] = false;
        }
        else
        {
            perror("pthread_join");
        }
    }

finished:
    /*
        If the shutdown signals are still masked, restore them; this is done
        as clean up.
    */
    if( masked )
    {
        (void) unmask_signals(&old_mask);
        masked = false;
    }

    /* cleanup */
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    if ( 0 != failed_thread_result )
    {
        status = failed_thread_result;
    }

    return status;
}

