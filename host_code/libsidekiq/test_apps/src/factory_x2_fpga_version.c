/**
 * @file   factory_x2_fpga_version.c
 * @author Jeremy Baugher <jeremy@epiq-solutions.com>
 * @date   Fri Sep  8 11:00:55 2017
 *
 * @brief
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sidekiq_api.h"
#include "arg_parser.h"
#include "sidekiq_api_factory.h"
#include "sidekiq_types_private.h"
#include "sidekiq_card_mgr.h"
#include "hardware.h"
#include "sidekiq_flash.h"
#include "sidekiq_xport.h"
#include "sidekiq_hal.h"

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- print the FPGA version information on a bare HTG-K800 card";
static const char* p_help_long =
"\
Displays the FPGA version information of the currently executing FPGA image of a\n\
HTG-K800 card that does NOT have an X2 FMC populated.  This test application is\n\
for FACTORY USE ONLY.  It creates a fake \"card\" and tricks card manager.\n\
";

/* command line argument variables */
static uint64_t xport_uid = 0;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("dmad-index",
                'd',
                "Use specified DMAD index",
                "ID",
                &xport_uid,
                UINT64_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/* reference some innards of libsidekiq to do the dirty work */
extern bool _probing;
extern void flash_select_functions_for_card(uint8_t card, skiq_xport_type_t type);
extern int32_t _pcie_card_init( skiq_xport_init_level_t level, uint64_t xport_uid );
extern int32_t _pcie_card_exit( skiq_xport_init_level_t level, uint64_t xport_uid );
extern skiq_sys skiq;

int main( int argc, char *argv[] )
{
    uint8_t card;
    int32_t status = 0;

    if ( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
    }

    if ( (status == 0) && (xport_uid >= SKIQ_MAX_NUM_CARDS) )
    {
        fprintf(stderr, "Error: specified DMAD index out of bounds\n");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
    }

    if ( status == 0 )
    {
        /* enable libsidekiq logging */
        openlog( "SKIQ", (LOG_PID | LOG_NDELAY | LOG_CONS | LOG_PERROR), LOG_USER );
    }

    if ( status == 0 )
    {
        fprintf(stderr, "calling skiq_fact_card_mgr_destroy");
        status = skiq_fact_card_mgr_destroy();
        fprintf(stderr, " --> status = %d\n", status);
    }

    /* this will fail hal_read_eeprom, but that's okay */
    if ( status == 0 )
    {
        fprintf(stderr, "calling card_mgr_init");
        status = card_mgr_init();
        fprintf(stderr, " --> status = %d\n", status);
    }

    if ( status == 0 )
    {
        uint32_t serial;
        skiq_part_info_t spi;
        uint16_t hw_version;

        serial = ( 'F' << 24 ) | ( 'A' << 16 ) | ( 'K' << 8 ) | ( 'E' << 0 );

        memcpy(spi.number_string, "020201", SKIQ_PART_NUM_STRLEN);
        memcpy(spi.revision_string, "A0", SKIQ_REVISION_STRLEN);
        memcpy(spi.variant_string, "00", SKIQ_VARIANT_STRLEN);

        hw_version = (skiq_eeprom_hw_vers_ext << 0) | (skiq_eeprom_product_ext << 12);

        fprintf(stderr, "calling card_mgr_register_xport");
        _probing = true;
        status = card_mgr_register_xport( xport_uid, skiq_xport_type_pcie, serial, hw_version, &spi, 0, NULL );
        fprintf(stderr, " --> status = %d\n", status);
        _probing = false;

        fprintf(stderr, "calling card_mgr_get_card");
        status = card_mgr_get_card( xport_uid, skiq_xport_type_pcie, &card );
        fprintf(stderr, " --> status = %d\n", status);

        skiq.card[card].card_params.part_type = skiq_x2;
        skiq.card[card].card_active = 1;
        skiq.card[card].card_params.card = card;
    }

    if ( status == 0 )
    {
        fprintf(stderr, "_pcie_card_init");
        status = _pcie_card_init( skiq_xport_init_level_basic, xport_uid );
        fprintf(stderr, " --> status = %d\n", status);
    }

    if ( status == 0 )
    {
        skiq_fpga_param_t* p_param = &(skiq.card[card].fpga_params);
        fprintf(stderr, "fpga_ctrl_read_version");
        status = fpga_ctrl_read_version(card,
                                        &(p_param->git_hash),
                                        &(p_param->build_date),
                                        &(p_param->version_major),
                                        &(p_param->version_minor),
                                        &(p_param->version_patch),
                                        &(p_param->baseline_hash),
                                        &(p_param->tx_fifo_size));
        fprintf(stderr, " --> status = %d\n", status);
    }

    if( 0 == status )
    {
        fprintf(stderr, "fpga_ctrl_read_board_id");
        status = fpga_ctrl_read_board_id(card, &skiq.card[card].fpga_priv.board_id);
        fprintf(stderr, " --> status = %d\n", status);
    }

    if( 0 == status )
    {
        fprintf(stderr, "fpga_ctrl_read_board_id_ext");
        status = fpga_ctrl_read_board_id_ext(card, &skiq.card[card].fpga_priv.board_id_ext);
        fprintf(stderr, " --> status = %d\n", status);
    }

    if( 0 == status )
    {
        fprintf(stderr, "fpga_ctrl_read_capabilities");
        status = fpga_ctrl_read_capabilities(card, &skiq.card[card].fpga_priv.caps);
        fprintf(stderr, " --> status = %d\n", status);
    }

    if( 0 == status )
    {
        // Cache the FPGA device
        skiq.card[card].fpga_params.fpga_device = _skiq_get_fpga_device_nocache( card );

        // Cache the FMC carrier here now that .board_id has been cached
        skiq.card[card].card_params.part_fmc_carrier = _skiq_get_fmc_carrier_nocache( card );
    }

    if ( status == 0 )
    {
        fprintf(stderr, "flash_select_functions_for_card\n");
        flash_select_functions_for_card( card, skiq_xport_type_pcie );
    }

    if ( status == 0 )
    {
        skiq_param_t params;
        skiq_fpga_param_t *fpga = &(params.fpga_param);

        fprintf(stderr, "calling skiq_read_parameters");
        status = skiq_read_parameters( card, &params );
        fprintf(stderr, " --> status = %d\n", status);
        if ( 0 == status )
        {
            printf("Info: FPGA git hash is 0x%04x\n", fpga->git_hash);
            printf("Info: FPGA build date (yymmddhh) is %04x\n", fpga->build_date);
            if ( FPGA_VERSION(fpga->version_major,fpga->version_minor,fpga->version_patch) < FPGA_VERSION(3,8,0) )
            {
                printf("Info: FPGA version %u.%u\n", fpga->version_major, fpga->version_minor);
            }
            else
            {
                printf("Info: FPGA version %u.%u.%u\n", fpga->version_major,
                       fpga->version_minor, fpga->version_patch);
            }
        }
    }

    if ( status == 0 )
    {
        uint8_t present;

        fprintf(stderr, "calling skiq_fact_read_genuine_golden_fpga_present_in_flash");
        status = skiq_fact_read_genuine_golden_fpga_present_in_flash( card, &present );
        fprintf(stderr, " --> status = %d\n", status);

        if ( 0 == present )
        {
            printf("Info: Golden FPGA IS NOT present\n");
        }
        else
        {
            printf("Info: Golden FPGA IS present\n");
        }
    }

    if ( status == 0 )
    {
        fprintf(stderr, "calling skiq_fact_card_mgr_destroy");
        status = skiq_fact_card_mgr_destroy();
        fprintf(stderr, " --> status = %d\n", status);
    }

    return (status);
}
