/*! \file i2c_rw_test.c
 *
 * \brief This file contains a basic application that reads/writes/verifies to both the temp sensor
 * and accelerometer (if available).  The intention here is to stress test the I2C bus.
 * 
 * <pre>
 * Copyright 2015-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "card_services.h"
#include "sidekiq_api_factory.h"
#include "sidekiq_hal.h"
#include "sidekiq_usb_cmds.h"
#include "elapsed.h"

#define TLOW_REG_ADDR                   0x02
#define FREE_FALL_REG_ADDR              0x28
#define X_GYRO_OFFSET_ADJ_REG_ADDR      0x13

/** @brief A simple macro for comparing semantic FPGA versions (major, minor, patch) */
#define FPGA_VERSION(a,b,c)   (((a) << 16) + ((b) << 8) + (c))
#define PARAM_FPGA_VERSION(_param)      FPGA_VERSION((_param).version_major,(_param).version_minor,(_param).version_patch)

static char* app_name;
static void print_usage(void);

static uint8_t rand_u8(void)
{
    return (uint8_t)(rand() & UINT8_MAX);
}


/*****************************************************************************/
/** This is the main function for executing read_temp commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])

{
    uint8_t card=0;
    uint8_t temp_write_data[2];
    uint8_t temp_read_data[1];
    uint8_t temp_i2c_address = TEMP_I2C_ADDRESS;
    uint8_t accel_reg_addr = FREE_FALL_REG_ADDR;
    uint32_t num_times=0;
    uint32_t error_count=0;
    uint32_t i=0;
    uint8_t accel_write_reg=0;
    uint8_t accel_read_reg=0;
    int32_t status = 0;
    uint8_t bus = 0;
    bool has_accel = false;
    bool can_test_temp_sensor = true;

    ELAPSED(i2c_temperature_access);
    ELAPSED(i2c_accel_access);

    app_name = argv[0];
    if( argc != 3 )
    {
        printf("Error: invalid # command line args\n");
        print_usage();
        _exit(-1);
    }
    card = (uint8_t)(strtoul(argv[1], NULL, 10));
    num_times = (uint32_t)(strtoul(argv[2], NULL, 10));

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);

    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    /* The FX2 firmware prohibits pass-through I2C access, so this test application fails if the HAL
     * functions are selected to use USB.  In the case where a device has both PCIe and USB,
     * override the HAL function selection to use the PCIe.  If there's only USB available, there's
     * not much we can do. */
    if ( ( skiq_is_xport_avail( card, skiq_xport_type_pcie ) == 0 ) &&
         ( skiq_is_xport_avail( card, skiq_xport_type_usb ) == 0 ) )
    {
        fprintf(stderr, "Info: Overriding HAL functions to use PCIe in place of USB\n");
        hal_select_functions_for_card(card, skiq_xport_type_pcie );
    }
    else if ( skiq_is_xport_avail( card, skiq_xport_type_usb ) == 0 )
    {
        fprintf(stderr, "cannot support USB only\n");
        return (-1);
    }

    /* seed the PNG */
    srand(time(NULL));

    /* check the part type to see if an alternate I2C address is needed for the temperature
     * sensor */
    {
        skiq_param_t params;
        status = skiq_read_parameters( card, &params );
        if ( 0 == status )
        {
            if ( params.card_param.part_type == skiq_x2 )
            {
                /* alternate temperature sensor I2C slave address for X2 */
                temp_i2c_address = 0x48;
            }
            else if ( params.card_param.part_type == skiq_x4 )
            {
                /* alternate temperature sensor I2C slave address for X4 */
                temp_i2c_address = 0x49;
            }
            else if ( ( params.card_param.part_type == skiq_m2_2280 ) &&
                      ( PARAM_FPGA_VERSION(params.fpga_param) >= FPGA_VERSION(3,13,1) ) )
            {
                can_test_temp_sensor = false;
            }
            else if ( params.card_param.part_type == skiq_nv100 )
            {
                /* on NV100, there are no temperature sensors available over I2C */
                can_test_temp_sensor = false;
            }

            /* assign accelerometer register based on part.  For Z2, let skiq_is_accel_supported()
             * tell us if the accelerometer is supported or not instead of trying to do a part
             * revision determination here */
            if ( ( params.card_param.part_type == skiq_mpcie ) ||
                 ( params.card_param.part_type == skiq_m2 ) )
            {
                accel_reg_addr = FREE_FALL_REG_ADDR;
            }
            else if ( ( params.card_param.part_type == skiq_z2 ) ||
                      ( params.card_param.part_type == skiq_m2_2280 ) ||
                      ( params.card_param.part_type == skiq_z2p ) ||
                      ( params.card_param.part_type == skiq_z3u ) ||
                      ( params.card_param.part_type == skiq_nv100 ) )
            {
                accel_reg_addr = X_GYRO_OFFSET_ADJ_REG_ADDR;
            }

            /* assign a bus index of 2 for temperature sensor access when working on an M.2 2280 / Z2p / Z3u */
            if ( (params.card_param.part_type == skiq_m2_2280) ||
                 (params.card_param.part_type == skiq_z2p) ||
                 (params.card_param.part_type == skiq_z3u) )
            {
                bus = 2;
            }
        }
        else
        {
            printf("Error: unable to access card %u parameters status %d\n", card, status);
            return (-1);
        }
    }

    if ( 0 != skiq_is_accel_supported( card, &has_accel ) )
    {
        fprintf(stderr, "Error: cannot determine accelerometer support, assuming false\n");
        has_accel = false;
    }

    if ( can_test_temp_sensor )
    {
        fprintf(stderr, "Info: Testing temperature sensor access\n");
    }
    else
    {
        fprintf(stderr, "Warning: SKIPPING temperature sensor access\n");
    }

    if ( has_accel )
    {
        fprintf(stderr, "Info: Testing accelerometer access on register 0x%02X\n", accel_reg_addr);
    }
    else
    {
        fprintf(stderr, "Warning: SKIPPING accelerometer access\n");
    }

    temp_write_data[0] = TLOW_REG_ADDR;

    for ( i = 0; ( i < num_times ) && ( status == 0 ); i++ )
    {
        if ( can_test_temp_sensor )
        {
            temp_write_data[1] = rand_u8();

            // setup pointer reg and write a val to Tlow
            status = skiq_fact_i2c_write(card, bus, temp_i2c_address, temp_write_data, 2);

            // do the read of the Tlow
            if ( status == 0 )
            {
                elapsed_start(&i2c_temperature_access);
                status = skiq_fact_i2c_write_before_read(card, bus, temp_i2c_address, TLOW_REG_ADDR,
                                                         temp_read_data, 1);
                elapsed_end(&i2c_temperature_access);
            }

            if ( ( status == 0 ) && ( temp_write_data[1] != temp_read_data[0] ) )
            {
                /* purposefully do not set `status`, just increment error_count.  the API functions
                 * did not fail, the verification failed and we want to count how many failures we
                 * get */
                error_count++;
                fprintf(stderr, "Error: temp sensor failed (iteration %" PRIu32 ", errors %" PRIu32
                        "): wrote 0x%0" PRIx8 ", read 0x%0" PRIx8 "\n", i, error_count,
                        temp_write_data[1], temp_read_data[0]);
            }
        }

        if ( has_accel && ( status == 0 ) )
        {
            accel_write_reg = rand_u8();
            status = card_write_accel_reg(card, accel_reg_addr, &accel_write_reg, 1);
            if ( status == 0 )
            {
                elapsed_start(&i2c_accel_access);
                status = card_read_accel_reg(card, accel_reg_addr, &accel_read_reg, 1);
                elapsed_end(&i2c_accel_access);
            }

            if ( ( status == 0 ) && ( accel_write_reg != accel_read_reg ) )
            {
                /* purposefully do not set `status`, just increment error_count.  the API functions
                 * did not fail, the verification failed and we want to count how many failures we
                 * get */
                error_count++;
                fprintf(stderr, "Error: accel failed (iteration %" PRIu32 ", errors %" PRIu32
                        "): wrote 0x%0" PRIx8 ", read 0x%0" PRIx8 "\n", i, error_count,
                        accel_write_reg, accel_read_reg);
            }
        }

        if ( ( (i + 1) % 200) == 0 )
        {
            printf("Completed iteration %u, errors %u\n", i + 1, error_count);
        }
    }

    printf("Completed %u iterations, errors %u\n", i, error_count);
    if( error_count == 0 )
    {
        printf("Successfully completed!\n");
    }

    if ( can_test_temp_sensor && ( status == 0 ) )
    {
        // restore temp sensor back to reading temps
        temp_write_data[0] = 0;
        status = skiq_fact_i2c_write(card, bus, temp_i2c_address, temp_write_data, 1);
    }

    skiq_exit();

    if ( can_test_temp_sensor )
    {
        printf("=======================================================================\n");
        printf("             Total time for temperature reading: "),print_total(&i2c_temperature_access);
        printf("                       Total number of readings: "),print_nr_calls(&i2c_temperature_access);
        printf("  Minimum time for a single temperature reading: "),print_minimum(&i2c_temperature_access);
        printf("  Average time for a single temperature reading: "),print_average(&i2c_temperature_access);
        printf("  Maximum time for a single temperature reading: "),print_maximum(&i2c_temperature_access);
    }

    if ( has_accel )
    {
        printf("=======================================================================\n");
        printf("            Total time for accelerometer reading: "),print_total(&i2c_accel_access);
        printf("                        Total number of readings: "),print_nr_calls(&i2c_accel_access);
        printf(" Minimum time for a single accelerometer reading: "),print_minimum(&i2c_accel_access);
        printf(" Average time for a single accelerometer reading: "),print_average(&i2c_accel_access);
        printf(" Maximum time for a single accelerometer reading: "),print_maximum(&i2c_accel_access);
    }

    /* if all transactions passed, but verification found non-zero number of errors, return the
     * status as a failure */
    if ( ( status == 0 ) && ( error_count > 0 ) )
    {
        status = 1;
    }

    return status;
}

/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <card> <# transactions>\n", app_name);
    printf("   stress test of I2C of the Sidekiq card specified\n");
}
