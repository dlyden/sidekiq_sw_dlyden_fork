#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static void print_usage(void);

int main( int argc, char *argv[] )
{
    int32_t status=0;
    uint8_t card=0;
    uint8_t filter_config;

    if( argc != 3 )
    {
        printf("Error: invalid # args (%u)\n", argc);
        print_usage();
        _exit(-1);
    }

    card=atoi(argv[1]);
    filter_config = atoi(argv[2]);

    status=skiq_init( skiq_xport_type_pcie,
                      skiq_xport_init_level_full,
                      &card,
                      1 );
    if( status == 0 )
    {
        // TODO: support multiple handles at some point?
        if( (status=skiq_fact_write_rx_preselect_filter_config(card,
                                                               skiq_rx_hdl_A1,
                                                               (skiq_rx_filter_config_t)(filter_config))) == 0 )
        {
            printf("Successfully wrote filter configuration\n");
        }
    }
    return (status);
}

void print_usage()
{
    printf("Usage: config_filters <card> <filters>\n");
    printf("   writes the filter config to EEPROM\n");
}
