/*! \file calibration_date.c
 * \brief This file contains the ability to read and write
 * and reset the calibration date.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>

#include <arg_parser.h>

#include <inttypes.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = \
    "- configures, queries, or resets calibration date";
static const char* p_help_long =
"\
Configures, queries or resets the calibration date. If the date\n\
or reset option is not specified, the current date is reported.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
";

static uint8_t card = UINT8_MAX;
static uint16_t cal_year = UINT16_MAX;
static uint8_t cal_week = UINT8_MAX;
static uint8_t cal_interval = UINT8_MAX;
static bool reset=false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("year",
                'y',
                "Specify year of calibration (must specify week/interval too)",
                NULL,
                &cal_year,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("week",
                'w',
                "Specify week number of calibration (must specify year/interval too)",
                NULL,
                &cal_week,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("interval",
                'i',
                "Specify calibration interval (in years); (must specify year/week too)",
                NULL,
                &cal_interval,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("reset",
                0,
                "Resets the calibration date",
                NULL,
                &reset,
                BOOL_VAR_TYPE),    
    APP_ARG_TERMINATOR,
};

int main( int argc, char *argv[] )
{
    int32_t status=0;
    pid_t owner;
    bool skiq_initialized=false;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( UINT8_MAX == card )
    {
        card = DEFAULT_CARD_NUMBER;
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if ( status != 0 )
    {
        if ( ( EBUSY == status) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }

        status = -1;
        goto finished;
    }
    else
    {
        skiq_initialized = true;

        // write the data if year/week/interval specified
        if( (cal_year != UINT16_MAX) &&
            (cal_week != UINT8_MAX) &&
            (cal_interval != UINT8_MAX) )
        {
            printf("Info: saving calibration date: year=%u, week=%u, interval=%u\n",
                   cal_year, cal_week, cal_interval);
            if( (status=skiq_fact_write_calibration_date(card,
                                                         cal_year,
                                                         cal_week,
                                                         cal_interval)) == 0 )
            {
                printf("Info: calibration date successfully stored (status=%d)\n", status);
            }
        }
        if( reset == true )
        {
            printf("Info: resetting calibration date\n");
            status=skiq_fact_reset_calibration_date(card);
            if( status != 0 )
            {
                printf("Error: unable to reset data (status=%d)\n", status);
            }
        }
        // now try to read the date
        if( (status=skiq_read_calibration_date( card,
                                                &cal_year,
                                                &cal_week,
                                                &cal_interval )) == 0 )
        {
            printf("Info: read calibration date: year=%u, week=%u, interval=%u\n",
                   cal_year, cal_week, cal_interval);
        }
        else if ( status == -ENOENT )
        {
            printf("Warning: calibration date is unconfigured\n");
        }
        else
        {
            printf("Error: unable to read calibration date (status=%d)\n", status);
        }
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return (status);
}
