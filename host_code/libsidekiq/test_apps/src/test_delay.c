/**
 * @file   test_delay.c
 * @author  <info@epiq-solutions.com>
 * @date   Wed Dec  5 21:21:03 2018
 *
 * @brief  Performs delay tests over different values and measures performance
 *
 * <pre>
 * Copyright 2018, 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>

#include "elapsed.h"
#include "hal_delay.h"
#include "hal_time.h"
#include "arg_parser.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

/** @brief  The default amount of time (in seconds) to run a test. */
#define DEFAULT_TEST_DURATION_SEC           5
/** @brief  The default duration (in seconds) to print out status updates. */
#define DEFAULT_UPDATE_RATE_SEC             1
/** @brief  The default choice to run the 'hal_nanosleep' delay tests. */
#define DEFAULT_RUN_HAL_SLEEP_TESTS         false
/** @brief  The default choice to run the 'hal_time_get' delay tests. */
#define DEFAULT_RUN_HAL_TIME_TESTS          false

static const char *p_help_short = " - test the HAL time & delay functions";
static const char *p_help_long = "\
A test to characterize the time and delay functions used in libsidekiq.\n\
\n\
If the '--sleeptests' flag is specified, then the timing tests for the libsidekiq\n\
HAL nanosleep functions will be run. This is also the case if no command line\n\
arguments are given (to match the previous default behavior).\n\
If the '--timetests' flag is specified, then the timing tests for the libsidekiq\n\
HAL time functions will be run.\n\
\n\
Defaults:\n\
    --duration=" xstr(DEFAULT_TEST_DURATION_SEC) "\n\
    --update=" xstr(DEFAULT_UPDATE_RATE_SEC) "\n\
    --sleeptests=" xstr(DEFAULT_RUN_HAL_SLEEP_TESTS) "\n\
    --timetests=" xstr(DEFAULT_RUN_HAL_TIME_TESTS) "\n";

static uint32_t testDurationSec = DEFAULT_TEST_DURATION_SEC;
static uint32_t updateSec = DEFAULT_UPDATE_RATE_SEC;
static bool runHalSleepTests = DEFAULT_RUN_HAL_SLEEP_TESTS;
static bool runHalTimeTests = DEFAULT_RUN_HAL_TIME_TESTS;

/* The command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("duration", 'd', "Duration of each time test (in seconds)", "SECS",
            &testDurationSec, UINT32_VAR_TYPE),
    APP_ARG_OPT("update", 'u', "How often updates are displayed (in seconds)", "SECS", &updateSec,
            UINT32_VAR_TYPE),
    APP_ARG_OPT("sleeptests", 's', "If set, run the 'hal_nanosleep' delay tests", NULL,
            &runHalSleepTests, BOOL_VAR_TYPE),
    APP_ARG_OPT("timetests", 't', "If set, run the 'hal_time_get' delay tests", NULL,
            &runHalTimeTests, BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

static int hal_timespec_gt( hal_timespec_t * const a, hal_timespec_t * const b )
{
    if (a->tv_sec == b->tv_sec)
    {
        return ( a->tv_nsec > b->tv_nsec );
    }
    else
    {
        return ( a->tv_sec > b->tv_sec );
    }
}

static void
run_hal_time_test(void)
{
    struct timespec ts_before, ts_after, ts_measured;
    hal_timespec_t ts_now, ts_deadline, ts_update;
    ELAPSED(delay_time);

    printf("======================================================================\n");
    fprintf(stderr, "Info: [HAL Time] Testing for %" PRIu32 " seconds", testDurationSec);

    /*
        The below loop tests hal_time_get() but also uses it to determine the timing for
        the test... this minimizes the extra number of "what's the current high resolution time?"
        calls needed. However, bugs in this custom time function could cause the test timing to be
        absolutely off and skew the results. Therefore, ts_before and ts_after use the traditional
        time measurement functions (shown in the results as "Measured tests duration") to also
        measure the duration of the test; any discrepancies between the intended and the measured
        could tip-off the user that something's not quite right.
    */
    clock_gettime(CLOCK_MONOTONIC, &ts_before);
    (void) hal_time_get(hal_time_clock_type_monotonic, &ts_now);

    ts_deadline = ts_now;
    ts_deadline.tv_sec += testDurationSec;
    ts_update = ts_now;
    ts_update.tv_sec += updateSec;
    do
    {
        elapsed_start(&delay_time);
        (void) hal_time_get(hal_time_clock_type_monotonic, &ts_now);
        elapsed_end(&delay_time);

        if ( hal_timespec_gt( &ts_now, &ts_update ) )
        {
            fprintf(stderr, ".");
            ts_update = ts_now;
            ts_update.tv_sec += updateSec;
        }
    }
    while ( hal_timespec_gt( &ts_deadline, &ts_now ) );
    clock_gettime(CLOCK_MONOTONIC, &ts_after);
    fprintf(stderr, "\n");

    ts_measured = timespec_sub(&ts_before, &ts_after);

    printf("           Intended test duration : %13" PRIu32 " seconds\n", testDurationSec);
    printf("           Measured test duration : %3" PRId64 ".%09lu seconds%s\n",
        (int64_t) ts_measured.tv_sec, ts_measured.tv_nsec,
        ((uint64_t) ts_measured.tv_sec != (uint64_t) testDurationSec) ? " (!!!)" : "");
    printf("  Total execution time for gettime: "),print_total(&delay_time);
    printf("      Number of gettime calls made: "),print_nr_calls(&delay_time);
    printf(" Minimum time for a single gettime: "),print_minimum(&delay_time);
    printf(" Average time for a single gettime: "),print_average(&delay_time);
    printf("  Stddev time for a single gettime: "),print_stddev(&delay_time);
    printf(" Maximum time for a single gettime: "),print_maximum(&delay_time);
}

static void
run_hal_sleep_test(uint64_t nr_nanoseconds)
{
    struct timespec ts_now, ts_deadline, ts_update;
    ELAPSED(delay_time);

    printf("======================================================================\n");
    fprintf(stderr, "Info: [HAL Sleep] Testing %" PRIu64 " ns delay ", nr_nanoseconds);
    clock_gettime(CLOCK_MONOTONIC, &ts_now);

    ts_deadline = ts_now;
    ts_deadline.tv_sec += testDurationSec;
    ts_update = ts_now;
    ts_update.tv_sec += updateSec;
    do
    {
        elapsed_start(&delay_time);
        hal_nanosleep(nr_nanoseconds);
        elapsed_end(&delay_time);

        clock_gettime(CLOCK_MONOTONIC, &ts_now);

        if ( timespec_gt( &ts_now, &ts_update ) )
        {
            fprintf(stderr, ".");
            ts_update = ts_now;
            ts_update.tv_sec += updateSec;
        }
    }
    while ( timespec_gt( &ts_deadline, &ts_now ) );
    fprintf(stderr, "\n");

    printf("          Delay value under test: %13.3f uS\n", (double)nr_nanoseconds / 1000.0);
    printf("         Total time for delaying: "),print_total(&delay_time);
    printf("          Total number of delays: "),print_nr_calls(&delay_time);
    printf(" Minimum time for a single delay: "),print_minimum(&delay_time);
    printf(" Average time for a single delay: "),print_average_and_error(&delay_time, nr_nanoseconds);
    printf("  Stddev time for a single delay: "),print_stddev(&delay_time);
    printf(" Maximum time for a single delay: "),print_maximum(&delay_time);
}


int main( int argc, char *argv[])
{
    int32_t status = 0;
    uint32_t i = 0;

    if (1 == argc)
    {
        /*
            No arguments were provided; revert to the old test behavior where only the sleep
            test is run.

            If the "sleep" test is enabled by default then you can't run the "time" test by itself.
        */
        runHalSleepTests = true;
    }
    else
    {
        status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
        if (0 != status)
        {
            perror("Command Line");
            arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
            return (-EINVAL);
        }
    }

    const uint64_t testValuesNs[] =
    {
        10, 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000,
        10000000, 50000000, 100000000, 500000000, 1000000000, 5000000000,
    };
    uint32_t lenTestValuesNs = sizeof(testValuesNs) / sizeof(testValuesNs[0]);

    if (runHalSleepTests)
    {
        for (i = 0; i < lenTestValuesNs; i++)
        {
            run_hal_sleep_test(testValuesNs[i]);
        }
    }

    if (runHalTimeTests)
    {
        run_hal_time_test();
    }

    return 0;
}

