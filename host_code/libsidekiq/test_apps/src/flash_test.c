/*! \file flash_test.c
 * \brief This file tests the flash interface by reading
 * and/or writing a file from the flash.
 *
 * <pre>
 * Copyright 2013-2019 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_flash.h"


#define M2_FLASH_NUM_BYTES               (8 << 20) /* 8 MB */
#define X2_FLASH_NUM_BYTES              (64 << 20) /* 64 MB */
#define X4_FLASH_NUM_BYTES              (64 << 20) /* 64 MB */

#define NUM_BYTES_IN_FLASH               (4 << 20) /* 4 MB */
#define FLASH_PAGE_SIZE (256)


static char *app_name;

/* local functions */
static void print_usage( void );

/*****************************************************************************/
/** This is the main function for executing the flash_test app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    FILE *pFile = NULL;
    uint8_t buf[FLASH_PAGE_SIZE];
    uint32_t num_bytes = 0;
    uint32_t flash_addr = 0;
    uint32_t tot_bytes = 0;
    uint32_t curr_num_bytes = 0;
    uint8_t card = 0;
    bool skiq_initialized = false;
    app_name = argv[0];

    // make sure he appropriate # parameters were provided
    if ( argc != 4 && argc != 5 )
    {
        printf( "Error: invalid # arguments provided\n" );
        print_usage(  );
        return ( -1 );
    }

    card = atoi( argv[1] );

    // initialize the sidekiq interface
    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_basic, &card, 1 );
    if ( status != 0 )
    {
        fprintf( stderr, "Error: unable to initialize libsidekiq with status %d\n", status );
        return ( -1 );
    }
    skiq_initialized = true;

    // read the flash contents
    if ( strcasecmp( argv[2], "r") == 0 )
    {
        // # of bytes to read was specified
        if ( argc == 5 )
        {
            sscanf( argv[4], "%u", &num_bytes );
            if ( num_bytes > NUM_BYTES_IN_FLASH )
            {
                printf( "Error: requested read of %u bytes when max is %u\n", num_bytes, NUM_BYTES_IN_FLASH );
                status = -1;
                goto finished;
            }
        }
        else
        {
            skiq_param_t params;
            status = skiq_read_parameters( card, &params );
            if ( status == 0 )
            {
                switch (params.card_param.part_type)
                {
                    case skiq_m2:
                        num_bytes = M2_FLASH_NUM_BYTES;
                        break;
                    case skiq_x2:
                        num_bytes = X2_FLASH_NUM_BYTES;
                        break;
                    case skiq_x4:
                        num_bytes = X4_FLASH_NUM_BYTES;
                        break;
                    default:
                        num_bytes = NUM_BYTES_IN_FLASH;
                        break;
                }
            }
            else
            {
                fprintf(stderr, "Error: unable to read skiq_part_t from card %u\n", card);
                num_bytes = 0;
            }
        }

        // open the file to save the contents to
        pFile = fopen( argv[3], "wb" );
        if ( pFile == NULL )
        {
            printf( "Error: unable to open file %s to store flash contents\n", argv[2] );
            status = -1;
            goto finished;
        }

        printf( "Info: reading flash and storing to the file...this may take some time\n" );
        // read the bytes in a page at a time until the specified # bytes was reached
        while ( tot_bytes < num_bytes )
        {
            // determine # bytes to read in
            if ( ( num_bytes - tot_bytes ) < FLASH_PAGE_SIZE )
            {
                curr_num_bytes = num_bytes - tot_bytes;
            }
            else
            {
                curr_num_bytes = FLASH_PAGE_SIZE;
            }

            // actually read the flash
            if ( ( status = flash_read( card, flash_addr, buf, curr_num_bytes ) ) == 0 )
            {
                fwrite( buf, 1, curr_num_bytes, pFile );
            }
            else
            {
                printf( "Error: failed to read from flash with status %d\n", status );
            }
            tot_bytes += curr_num_bytes;
            flash_addr += curr_num_bytes;

            // feedback to user that we're still running
            if ( ( tot_bytes % 40960 ) == 0 )
            {
                printf( "." );
                fflush( stdout );
            }
        }
        printf( "\n" );
        printf( "Info: successfully read %u bytes from flash and stored to %s\n", tot_bytes, argv[3] );
        fclose( pFile );
    }
    // write the file specified to flash
    else if ( strcasecmp( argv[2], "w") == 0 )
    {
        pFile = fopen( argv[3], "rb" );
        if ( pFile == NULL )
        {
            printf( "Error: unable to open file %s to read from\n", argv[2] );
            status = -1;
            goto finished;
        }

        status = flash_write_file( card, flash_addr, pFile );
        fclose( pFile );

        if ( status == 0 )
        {
            printf( "Info: successfully wrote file to flash\n" );
        }
        else
        {
            fprintf( stderr, "Error: failed to write file to flash (status = %" PRIi32 ")\n", status );
            status = -1;
            goto finished;
        }
    }
    else
    {
        fprintf( stderr, "Error: invalid operation specified\n" );
        print_usage(  );
        status = -1;
        goto finished;
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return status;
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
void print_usage( void )
{
    printf( "Usage: %s <card> <r|w> <filename> [# bytes]\n", app_name );
    printf( "    reads or writes the contents of the specified Sidekiq's flash to/from\n" );
    printf( "    the specified file.  If reading a file, the number of bytes to read can\n" );
    printf( "    be specified or the entire flash contents will be read.\n\n" );
    printf( "    Note: writing to flash will perform a bulk erase prior to the write.\n" );
    printf( "    This will result in the entire flash being cleared.\n" );
}
