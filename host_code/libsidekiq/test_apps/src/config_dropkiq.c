/*! \file config_dropkiq.c
 * \brief Program hardware configuration data for a specified Dropkiq.
 *
 * <pre>
 * Copyright 2016 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <dropkiq_api.h>
#include <dropkiq_api_factory.h>
#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  (0)
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- configure Dropkiq";
static const char* p_help_long = "\
Program and configure the specified Dropkiq with version information, clock\n\
selection, and/or warp voltage.\n\
\n\
Defaults:\n\
    --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
";

/* command line argument variables */
static uint8_t _card = UINT8_MAX;
static char* p_serial = NULL;
static char* _p_clock = NULL;
static char* _p_option_num = NULL;
static char* _p_part_num = NULL;
static char* _p_revision_num = NULL;
static char* _p_serial_num = NULL;
static uint16_t _warp = 0xFFFF;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &_card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("clock",
                0,
                "Reference clock, either INTERNAL or EXTERNAL",
                "SELECT",
                &_p_clock,
                STRING_VAR_TYPE),
    APP_ARG_OPT("option",
                0,
                "Option number, 2 characters",
                "NUM",
                &_p_option_num,
                STRING_VAR_TYPE),
    APP_ARG_OPT("part",
                0,
                "Part number, 6 characters",
                "NUM",
                &_p_part_num,
                STRING_VAR_TYPE),
    APP_ARG_OPT("revision",
                0,
                "Revision number, 2 characters",
                "NUM",
                &_p_revision_num,
                STRING_VAR_TYPE),
    APP_ARG_OPT("serial",
                0,
                "Serial number, 4 characters",
                "NUM",
                &_p_serial_num,
                STRING_VAR_TYPE),
    APP_ARG_OPT("warp",
                0,
                "Reference clock warp voltage, 16 bit value",
                "NUM",
                &_warp,
                UINT16_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


int main(int argc, char* argv[])
{
    dkiq_hw_version_info_t ver;
    dkiq_ref_clock_t clock = dkiq_ref_clock_invalid;
    int32_t status = 0;
    pid_t owner = 0;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return -1;
    }

    if( (UINT8_MAX != _card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == _card)
    {
        _card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &_card);
        if (0 != status)
        {
            printf("Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            return -1;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, _card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < _card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", _card, (SKIQ_MAX_NUM_CARDS - 1));
        return -1;
    }

    // Parse reference clock string into proper type value.
    if( NULL != _p_clock )
    {
        if( 0 == strncasecmp(_p_clock, "INTERNAL", 9) )
        {
            clock = dkiq_ref_clock_internal;
        }
        else if( 0 == strncasecmp(_p_clock, "EXTERNAL", 9) )
        {
            clock = dkiq_ref_clock_external;
        }
        else
        {
            printf("Error: %s is an invalid clock configuration\n", _p_clock);
            return -1;
        }
    }

    printf("Info: initializing card %" PRIu8 "...\n", _card);

    /* initialize access to the card */
    status = skiq_init(skiq_xport_type_pcie,
                       skiq_xport_init_level_basic,
                       &_card,
                       1);
    if( 0 != status )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(_card, &owner) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", _card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }
        return -1;
    }

    if( NULL != _p_option_num ||
        NULL != _p_part_num ||
        NULL != _p_revision_num ||
        NULL != _p_serial_num )
    {
        // Get hardware version info.
        status = dkiq_get_hw_version(_card, &ver);
        if( 0 != status )
        {
            printf("Error: failed to read Dropkiq hardware version info\n");
            goto main_exit;
        }

        // Update the fields passed in via command line.
        if( NULL != _p_option_num )
        {
            memcpy(&(ver.p_option_num),
                   _p_option_num,
                   DKIQ_VERSION_OPTION_NUMBER_LENGTH);
        }
        if( NULL != _p_part_num )
        {
            memcpy(&(ver.p_part_num),
                   _p_part_num,
                   DKIQ_VERSION_PART_NUMBER_LENGTH);
        }
        if( NULL != _p_revision_num )
        {
            memcpy(&(ver.p_revision_num),
                   _p_revision_num,
                   DKIQ_VERSION_REVISION_NUMBER_LENGTH);
        }
        if( NULL != _p_serial_num )
        {
            memcpy(&(ver.p_serial_num),
                   _p_serial_num,
                   DKIQ_VERSION_SERIAL_NUMBER_LENGTH);
        }

        // Set the hardware version info with the updated values.
        status = dkiq_fact_set_hw_version(_card, &ver);
        if( 0 != status )
        {
            printf("Error: failed to write Dropkiq hardware version info\n");
            goto main_exit;
        }
    }

    // Configure the reference clock.
    if( dkiq_ref_clock_invalid != clock )
    {
        status = dkiq_fact_set_ref_clock(_card, clock);
        if( 0 != status )
        {
            printf("Error: failed to write Dropkiq reference clock\n");
            goto main_exit;
        }
    }

    // Configure default warp voltage.
    if( 0xFFFF != _warp )
    {
        status = dkiq_fact_set_default_tcvcxo_warp_voltage(_card, _warp);
        if( 0 != status )
        {
            printf("Error: failed to write Dropkiq default warp voltage\n");
            goto main_exit;
        }
    }

    // Read back hardware configuration info.
    status = dkiq_get_hw_version(_card, &ver);
    if( 0 != status )
    {
        goto main_exit;
    }
    status = dkiq_get_default_tcvcxo_warp_voltage(_card, &_warp);
    if( 0 != status )
    {
        printf("Error: failed to read Dropkiq default warp voltage\n");
        goto main_exit;
    }
    status = dkiq_get_ref_clock(_card, &clock);
    if( 0 != status )
    {
        printf("Error: failed to read Dropkiq reference clock\n");
        goto main_exit;
    }

    printf("Dropkiq %u\n", _card);
    printf("\tOption    %c%c\n",
           (0 != isprint(ver.p_option_num[0])) ? ver.p_option_num[0] : '.',
           (0 != isprint(ver.p_option_num[1])) ? ver.p_option_num[1] : '.');
    printf("\tPart      %c%c%c%c%c%c\n",
           (0 != isprint(ver.p_part_num[0])) ? ver.p_part_num[0] : '.',
           (0 != isprint(ver.p_part_num[1])) ? ver.p_part_num[1] : '.',
           (0 != isprint(ver.p_part_num[2])) ? ver.p_part_num[2] : '.',
           (0 != isprint(ver.p_part_num[3])) ? ver.p_part_num[3] : '.',
           (0 != isprint(ver.p_part_num[4])) ? ver.p_part_num[4] : '.',
           (0 != isprint(ver.p_part_num[5])) ? ver.p_part_num[5] : '.');
    printf("\tRevision  %c%c\n",
           (0 != isprint(ver.p_revision_num[0])) ? ver.p_revision_num[0] : '.',
           (0 != isprint(ver.p_revision_num[1])) ? ver.p_revision_num[1] : '.');
    printf("\tSerial    %c%c%c%c\n",
           (0 != isprint(ver.p_serial_num[0])) ? ver.p_serial_num[0] : '.',
           (0 != isprint(ver.p_serial_num[1])) ? ver.p_serial_num[1] : '.',
           (0 != isprint(ver.p_serial_num[2])) ? ver.p_serial_num[2] : '.',
           (0 != isprint(ver.p_serial_num[3])) ? ver.p_serial_num[3] : '.');
    printf("\tWarp      %u\n", _warp);
    printf("\t%s Reference Clock\n",
           (dkiq_ref_clock_external == clock) ? "External" :
           (dkiq_ref_clock_internal == clock) ? "Internal" :
                                                "Invalid");
main_exit:

    skiq_exit();
    return status;
}

