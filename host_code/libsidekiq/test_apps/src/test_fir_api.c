/*! \file test_fir_api.c
 * \brief This file contains a basic application that tests the API
 * of reading/writting the FIR coefficients
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <inttypes.h>
#include <strings.h>

#include <sidekiq_api.h>
#include <arg_parser.h>

#define ARRAY_SIZE(_x)                  (sizeof(_x) / sizeof((_x)[0]))
#define GHZ                             (uint64_t)(1000000000)
#define MHZ                             (uint64_t)(1000000)

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- test custom FIR coefficient API";
static const char* p_help_long = "\
Verify the custom FIR coefficient API is functioning properly.\n\
\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --rate=10000000\n\
  --handle=A1";

static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static uint32_t rate=10000000;
static char* p_hdl = "A1";
static bool skip_transmit = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &(card),
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("rate",
                'r',
                "Sample rate",
                "Hz",
                &rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("handle",
                'h',
                "Handle to use, either A1, A2, B1, or ALL",
                NULL,
                &p_hdl,
                STRING_VAR_TYPE),
    APP_ARG_OPT("skip-transmit",
                0,
                "Skip the TX FIR coefficient tests",
                NULL,
                &skip_transmit,
                BOOL_VAR_TYPE),

    APP_ARG_TERMINATOR,
};

uint64_t lo_freq_test[] = {
    1 * GHZ,
    500 * MHZ,
    2 * GHZ,
    200 * MHZ,
    3 * GHZ,
    100 * MHZ,
    4 * GHZ,
    75 * MHZ,
};
uint16_t nr_lo_freq = ARRAY_SIZE( lo_freq_test );

int16_t rx_test[128] =
{ 0, 1, 2, 3,
  4, 5, 6, 7,
  8, 9, 10, 11,
  12, 13, 14, 15,
  16, 17, 18, 19,
  20, 21, 22, 23,
  24, 25, 26, 27,
  28, 29, 30, 31,
  32, 33, 34, 35,
  36, 37, 38, 39,
  40, 41, 42, 43,
  44, 45, 46, 47,
  48, 49, 50, 51,
  52, 53, 54, 55,
  56, 57, 58, 59,
  60, 61, 62, 63,
  0, -1, -2, -3,
  -4, -5, -6, -7,
  -8, -9, -10, -11,
  -12, -13, -14, -15,
  -16, -17, -18, -19,
  -20, -21,- 22, -23,
  -24, -25, -26, -27,
  -28, -29, -30, -31,
  -32, -33, -34, -35,
  -36, -37, -38, -39,
  -40, -41, -42, -43,
  -44, -45, -46, -47,
  -48, -49, -50, -51,
  -52, -53, -54, -55,
  -56, -57, -58, -59,
  -60, -61, -62, -63 };

int16_t tx_test[128] =
{   0, -1, -2, -3,
  -4, -5, -6, -7,
  -8, -9, -10, -11,
  -12, -13, -14, -15,
  -16, -17, -18, -19,
  -20, -21,- 22, -23,
  -24, -25, -26, -27,
  -28, -29, -30, -31,
  -32, -33, -34, -35,
  -36, -37, -38, -39,
  -40, -41, -42, -43,
  -44, -45, -46, -47,
  -48, -49, -50, -51,
  -52, -53, -54, -55,
  -56, -57, -58, -59,
  -60, -61, -62, -63,
    0, 1, 2, 3,
    4, 5, 6, 7,
    8, 9, 10, 11,
    12, 13, 14, 15,
    16, 17, 18, 19,
    20, 21, 22, 23,
    24, 25, 26, 27,
    28, 29, 30, 31,
    32, 33, 34, 35,
    36, 37, 38, 39,
    40, 41, 42, 43,
    44, 45, 46, 47,
    48, 49, 50, 51,
    52, 53, 54, 55,
    56, 57, 58, 59,
    60, 61, 62, 63 };

#define VERIFY_RX_COEFFICIENTS_MATCH(_rx_test,_num_rx_taps)             \
    do {                                                                \
        if ( verify_rx_coefficients( _rx_test, _num_rx_taps, false ) == false ) \
        {                                                               \
            printf("Error: verify RX coefficients failed to match on line %u!\n", __LINE__); \
            status = -6;                                                \
            goto finished;                                              \
        }                                                               \
    } while (0)

#define VERIFY_RX_COEFFICIENTS_MISMATCH(_rx_test,_num_rx_taps)          \
    do {                                                                \
        if ( verify_rx_coefficients( _rx_test, _num_rx_taps, true ) == true ) \
        {                                                               \
            printf("Error: verify RX coefficients failed to mismatch on line %u!\n", __LINE__); \
            status = -6;                                                \
            goto finished;                                              \
        }                                                               \
    } while (0)

#define VERIFY_TX_COEFFICIENTS_MATCH(_tx_test,_num_tx_taps,_dec_interp_tx) \
    do {                                                                \
        if ( verify_tx_coefficients( _tx_test, _num_tx_taps, _dec_interp_tx, false ) == false ) \
        {                                                               \
            printf("Error: verify TX coefficients failed to match on line %u!\n", __LINE__); \
            status = -6;                                                \
            goto finished;                                              \
        }                                                               \
    } while (0)

#define VERIFY_TX_COEFFICIENTS_MISMATCH(_tx_test,_num_tx_taps,_dec_interp_tx) \
    do {                                                                \
        if ( verify_tx_coefficients( _tx_test, _num_tx_taps, _dec_interp_tx, true ) == true ) \
        {                                                               \
            printf("Error: verify TX coefficients failed to mismatch on line %u!\n", __LINE__); \
            status = -6;                                                \
            goto finished;                                              \
        }                                                               \
    } while (0)


static bool
verify_coefficients( int16_t rfic_coeffs[],
                     int16_t test_coeffs[],
                     uint8_t num_taps,
                     uint8_t dec_interp,
                     const char *type,
                     bool expect_mismatch )
{
    uint8_t i=0;
    bool matches = true;
    uint8_t mult = 1;

    if ( dec_interp == 4 ) mult = 2;

    printf("Info: verifying %s coefficients across %u taps and mult = %u ... ", type, num_taps, mult);
    for ( i = 0; (i < num_taps) && (matches); i++ )
    {
        if ( rfic_coeffs[i] != mult * test_coeffs[i] )
        {
            if ( !expect_mismatch )
            {
                printf("\nError: FIR coefficients (RFIC %d != TEST %d) at index %u do not match!\n",
                       rfic_coeffs[i], mult * test_coeffs[i], i);
            }
            matches = false;
        }
    }
    printf("done\n");

    return (matches);
}


static bool
verify_rx_coefficients( int16_t test_coeffs[],
                        uint8_t num_taps,
                        bool expect_mismatch )
{
    int16_t rfic_coeffs[128];

    if( skiq_read_rfic_rx_fir_coeffs(card, rfic_coeffs) != 0 )
    {
        printf("Error: unable to read Rx FIR coeffs\n");
        return false;
    }
    else
    {
        return verify_coefficients( rfic_coeffs, test_coeffs, num_taps, 1, "RX", expect_mismatch );
    }
}

static bool
verify_tx_coefficients( int16_t test_coeffs[],
                        uint8_t num_taps,
                        uint8_t dec_interp,
                        bool expect_mismatch )
{
    int16_t rfic_coeffs[128];

    if( skip_transmit == false )
    {
        if( skiq_read_rfic_tx_fir_coeffs(card, rfic_coeffs) != 0 )
        {
            printf("Error: unable to read Tx FIR coeffs\n");
            return false;
        }
        else
        {
            return verify_coefficients( rfic_coeffs, test_coeffs, num_taps, dec_interp, "TX", expect_mismatch );
        }
    }
    else
    {
        printf("Info: skipping TX verification\n");
        return (true);
    }
}

static bool
test_coeffs_after_rx_tune( uint64_t rx_lo_freq,
                           skiq_rx_hdl_t rx_hdl,
                           int16_t rx_coeffs[], uint8_t num_rx_taps,
                           int16_t tx_coeffs[], uint8_t num_tx_taps, uint8_t dec_interp_tx )
{
    int32_t status;

    printf("Info: updating RX LO frequency to %" PRIu64 " Hz\n", rx_lo_freq);
    status = skiq_write_rx_LO_freq(card, rx_hdl, rx_lo_freq);

    if ( status == 0 )
    {
        /* expect to coefficients to match (expect_mismatch = false) */
        if ( verify_tx_coefficients( tx_test, num_tx_taps, dec_interp_tx, false ) == false )
        {
            printf("Error: verifying TX coefficients failed after RX tune to %" PRIu64 " Hz!\n", rx_lo_freq);
            status = -1;
        }
    }

    if ( status == 0 )
    {
        /* expect to coefficients to match (expect_mismatch = false) */
        if( verify_rx_coefficients( rx_test, num_rx_taps, false ) == false )
        {
            printf("Error: verifying RX coefficients failed after RX tune to %" PRIu64 " Hz!\n", rx_lo_freq);
            status = -1;
        }
    }

    return (status == 0);
}

static bool
test_coeffs_after_tx_tune( uint64_t tx_lo_freq,
                           skiq_tx_hdl_t tx_hdl,
                           int16_t rx_coeffs[], uint8_t num_rx_taps,
                           int16_t tx_coeffs[], uint8_t num_tx_taps, uint8_t dec_interp_tx )
{
    int32_t status;

    printf("Info: updating TX LO frequency to %" PRIu64 " Hz\n", tx_lo_freq);
    status = skiq_write_tx_LO_freq(card, tx_hdl, tx_lo_freq);

    if ( status == 0 )
    {
        /* expect to coefficients to match (expect_mismatch = false) */
        if ( verify_tx_coefficients( tx_test, num_tx_taps, dec_interp_tx, false ) == false )
        {
            printf("Error: verifying TX coefficients failed after TX tune to %" PRIu64 " Hz!\n", tx_lo_freq);
            status = -1;
        }
    }

    if ( status == 0 )
    {
        /* expect to coefficients to match (expect_mismatch = false) */
        if( verify_rx_coefficients( rx_test, num_rx_taps, false ) == false )
        {
            printf("Error: verifying RX coefficients failed after TX tune to %" PRIu64 " Hz!\n", tx_lo_freq);
            status = -1;
        }
    }

    return (status == 0);
}

/*****************************************************************************/
/** This is the main function for executing the test_fir_api app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    pid_t owner = 0;

    uint8_t num_rx_taps = 0, num_tx_taps = 0;
    uint8_t dec_interp_rx = 0, dec_interp_tx = 0;
    skiq_rx_hdl_t rx_hdl;
    skiq_tx_hdl_t tx_hdl;
    skiq_chan_mode_t chan_mode;
    uint16_t i;

    if( arg_parser(argc, argv, p_help_short, p_help_long, p_args) != 0 )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if ( 0 != status )
        {
            printf("Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 "\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    /* map argument values to sidekiq specific variable values */
    if( 0 == strncasecmp(p_hdl, "A1", 3) )
    {
        rx_hdl = skiq_rx_hdl_A1;
        tx_hdl = skiq_tx_hdl_A1;
        /* set chan mode to single */
        chan_mode = skiq_chan_mode_single;
        printf("Info: using handle A1\n");
    }
    else if( 0 == strncasecmp(p_hdl, "A2", 3) )
    {
        rx_hdl = skiq_rx_hdl_A2;
        tx_hdl = skiq_tx_hdl_A2;
        /* set chan mode to dual */
        chan_mode = skiq_chan_mode_dual;
        printf("Info: using handle A2\n");
    }
    else
    {
        printf("Error: invalid handle specified\n");
        return (-1);
    }


    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full,
                &card, 1);
    if (status != 0)
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %d\n",
                    status);
        }
        return (-1);
    }

    /* only write the channel mode if it's dual */
    if( chan_mode == skiq_chan_mode_dual )
    {
        if( skiq_write_chan_mode(card,chan_mode) != 0 )
        {
            printf("Error: failed to set Rx channel mode\n");
            status = -1;
            goto finished;
        }
    }


    // write Tx SR/BW
    if( skip_transmit == false )
    {
        if( skiq_write_tx_sample_rate_and_bandwidth(card,
                                                    tx_hdl,
                                                    rate,
                                                    rate) != 0 )
        {
            printf("Error: unable to configure Tx sample rate/bandwidth\n");
            status = -2;
            goto finished;
        }
    }

    // write Rx SR/BW
    if( skiq_write_rx_sample_rate_and_bandwidth(card,
                                                rx_hdl,
                                                rate,
                                                rate) != 0 )
    {
        printf("Error: unable to configure Rx sample rate/bandwidth\n");
        status = -3;
        goto finished;
    }

    if( skiq_read_rfic_rx_fir_config(card, &num_rx_taps, &dec_interp_rx) != 0 )
    {
        printf("Error: unable to read Rx FIR config\n");
        status = -4;
        goto finished;
    }
    printf("Info: Rx FIR config taps=%" PRIu8 "\n", num_rx_taps);

    if( skip_transmit == false )
    {
        if( skiq_read_rfic_tx_fir_config(card, &num_tx_taps, &dec_interp_tx) != 0 )
        {
            printf("Error: unable to read Tx FIR config\n");
            status = -4;
            goto finished;
        }
        printf("Info: Tx FIR config taps=%" PRIu8 ", interp=%" PRIu8 "\n", num_tx_taps, dec_interp_tx);
    }

    /* first verify that both RX and TX FIR coefficients do NOT match the test sets */
    if( skip_transmit == false )
    {
        VERIFY_TX_COEFFICIENTS_MISMATCH(tx_test, num_tx_taps, dec_interp_tx);
    }
    VERIFY_RX_COEFFICIENTS_MISMATCH(rx_test, num_rx_taps);

    /* write TX FIR coefficients and verify that TX matches, but RX does NOT match */
    if( skip_transmit == false )
    {
        printf("Info: Writing new Tx FIR config taps=%" PRIu8 "\n", num_tx_taps);
        if( skiq_write_rfic_tx_fir_coeffs(card, tx_test) != 0 )
        {
            printf("Error: unable to write Tx FIR coeffs\n");
            status = -5;
            goto finished;
        }
        VERIFY_TX_COEFFICIENTS_MATCH(tx_test, num_tx_taps, dec_interp_tx);
    }
    VERIFY_RX_COEFFICIENTS_MISMATCH(rx_test, num_rx_taps);

    /* write RX FIR coefficients and verify that TX matches and RX matches */
    printf("Info: Writing new Rx FIR config taps=%" PRIu8 "\n", num_rx_taps);
    if( skiq_write_rfic_rx_fir_coeffs(card, rx_test) != 0 )
    {
        printf("Error: unable to write Tx FIR coeffs\n");
        status = -5;
        goto finished;
    }
    VERIFY_TX_COEFFICIENTS_MATCH(tx_test, num_tx_taps, dec_interp_tx);
    VERIFY_RX_COEFFICIENTS_MATCH(rx_test, num_rx_taps);

    /* iterate over frequencies and test TX tuning */
    if( skip_transmit == false )
    {
        for ( i = 0; i < nr_lo_freq; i++ )
        {
            // modify TX LO and verify coefficients again
            if ( test_coeffs_after_tx_tune( lo_freq_test[i], tx_hdl,
                                            rx_test, num_rx_taps,
                                            tx_test, num_tx_taps, dec_interp_tx ) == false )
            {  
                status = -6;
                goto finished;
            }
        }
    }

    /* iterate over frequencies and test RX tuning */
    for ( i = 0; i < nr_lo_freq; i++ )
    {
        // modify RX LO and verify coefficients again
        if ( test_coeffs_after_rx_tune( lo_freq_test[i], rx_hdl,
                                        rx_test, num_rx_taps,
                                        tx_test, num_tx_taps, dec_interp_tx ) == false )
        {
            status = -6;
            goto finished;
        }
    }

finished:
    skiq_exit();

    return ((int) status);
}
