/**
 * @file   unlock_flash.c
 * @author  <info@epiqsolutions.com>
 * @date   Mon Dec 10 09:11:21 2018
 * 
 * @brief  This basic application unlocks the SPI flash for a given Sidekiq unit
 *
 * @note   Not all Sidekiq parts support this feature
 * 
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_flash.h"
#include "arg_parser.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- unlock SPI flash blocks associated with the golden FPGA bitstream storage";
static const char* p_help_long =
"\
Unlocks SPI flash blocks associated with the golden FPGA bitstream storage for a\n\
specified Sidekiq unit.  Note that not all Sidekiq parts support this feature.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n";

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
}

/*****************************************************************************/
/** This is the main function for executing unlock_flash application

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ASCII string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    pid_t owner = 0;

    bool skiq_initialized = false;

    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not both\n");
        status = -1;
        goto finished;
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s (result code %"
                    PRIi32 ")\n", p_serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID (%" PRIu8 ")\n",
                card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* initialize Sidekiq */
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID %u); cannot "
                    "initialize card.\n", card, (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a valid card specified? "
                    "(result code %" PRIi32 ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with status %d\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    status = flash_unlock_golden_sectors( card );
    if( 0 != status )
    {
        fprintf(stderr, "Error: failed to unlock flash (result code %" PRIi32 ")\n", status);
    }
    else
    {
        printf("Info: Flash sectors unlocked!\n");
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return ((int) status);
}

