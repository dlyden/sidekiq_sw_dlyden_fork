/*! \file prbs_test.c
 * \brief This file contains a basic application that performs the
 * PRBS test to validate the FPGA/AD9361 interface.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "sidekiq_hal.h"
#include "arg_parser.h"


/***** DEFINES *****/

#define PRBS_MIN_DATA_DELAY             (0)
#define PRBS_MAX_DATA_DELAY             (15)
#define PRBS_MIN_CLOCK_DELAY            (0)
#define PRBS_MAX_CLOCK_DELAY            (15)

#define MAX_LOG_MSG_SIZE                (128)

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- test AD9361 to FPGA";
static const char* p_help_long = "\
Verify the AD9361 to FPGA connection using a pseudo random binary sequence.\n\
\n\
Note, if no card or serial number is specified, test will be performed on\n\
all cards detected on the system.\n\
\n\
Defaults:\n\
  --channels=1\n\
  --start=250000\n\
  --stop=61000000\n\
  --step=500000\n\
  --time=3";

/* command line argument variables */
static char* p_file_path = NULL;
static uint32_t num_sec_req = 3;
static uint32_t start_rate = 250000;
static uint32_t end_rate = 61000000;
static uint32_t step = 500000;
static uint8_t card = SKIQ_MAX_NUM_CARDS;
static uint8_t num_chans = 1;
static char* p_serial = NULL;
static bool sweep_data_delay = false;
static bool sweep_clock_delay = false;

/* global FILE pointer */
static FILE *output_fp = NULL;

/* global running state */
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("channels",
                0,
                "Number of channels to test, 1 or 2",
                "ID",
                &num_chans,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("destination",
                'd',
                "Output file to store results",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("start",
                0,
                "Starting sample rate",
                "Hz",
                &start_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("stop",
                0,
                "End sample rate",
                "Hz",
                &end_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("step",
                0,
                "Sample rate step size",
                "Hz",
                &step,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("time",
                0,
                "During per step",
                "Sec",
                &num_sec_req,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("sweep-data-delay",
                0,
                "Sweep data delay setting of the 9361 for each sample rate",
                false,
                &sweep_data_delay,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("sweep-clock-delay",
                0,
                "Sweep clock delay settings of the 0361 for each sample rate",
                false,
                &sweep_clock_delay,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


/***** LOCAL FUNCTIONS *****/

void app_log_close( void )
{
    if ( output_fp )
    {
        fclose(output_fp);
        output_fp = NULL;
    }
}

void app_log( int32_t priority,
              const char *message )
{
    fprintf(stderr, "%s", message);
    if ( output_fp )
    {
        fprintf(output_fp, "%s", message);
        fflush(output_fp);
    }
}

void app_logf( const char *format, ... )
{
    char str[MAX_LOG_MSG_SIZE];
    int num_bytes=0;
    va_list args;

    va_start( args, format );
    vsnprintf( &(str[num_bytes]), MAX_LOG_MSG_SIZE-num_bytes, format, args );
    va_end( args );

    /* call the logging function */
    app_log( 0, str );
}


/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
   does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    app_logf("<INFO> received signal %d, cleaning up libsidekiq\n", signum);

    skiq_fact_prbs_cancel_test();
    running = false;
}


/**************************************************************************************************/
/** The run_prbs_test function runs a PRBS test across different sample rates and verifies the
    AD9361-to-FPGA connection using a pseudo random binary sequence

    @param[in] cards array of card indices to test
    @param[in] nr_cards number of card indices in @a cards
    @param[in] start_sample_rate starting sample rate for test
    @param[in] end_sample_rate ending sample rate for test
    @param[in] sample_rate_step sample rate increment for test when sweeping sample rates
    @param[in] nr_seconds_requested number of seconds to poll for PRBS errors at every tested sample rate
    @param[in] should_sweep_data_delay flag to indicate that sweeping over possible data delay values (if false, use current setting only)
    @param[in] should_sweep_clock_delay flag to indicate that sweeping over possible clock delay values (if false, use current setting only)
    @param[out] prbs_errors array of uint32_t values to indicate number of PRBS errors encountered

    @return int32_t  status where 0=success, anything else is an error
 */
static int32_t run_prbs_test( uint8_t cards[],
                              uint8_t nr_cards,
                              uint8_t nr_handles,
                              uint32_t start_sample_rate,
                              uint32_t end_sample_rate,
                              uint32_t sample_rate_step,
                              uint32_t nr_seconds_requested,
                              bool should_sweep_data_delay,
                              bool should_sweep_clock_delay,
                              uint32_t prbs_errors[] )
{
    int32_t status = 0;
    uint8_t start_data_delay, stop_data_delay, curr_data_delay;
    uint8_t start_clock_delay, stop_clock_delay, curr_clock_delay;
    uint32_t current_sample_rate;
    uint8_t i, delay_reg;
    bool sweeping = false;

    /* check passed parameters */
    if ( ( nr_cards == 0 ) || ( nr_cards > SKIQ_MAX_NUM_CARDS ) )
    {
        return -EINVAL;
    }

    /* set start_data_delay and stop_clock_delay based on whether sweeping is requested for the data delay */
    if ( should_sweep_data_delay )
    {
        start_data_delay = PRBS_MIN_DATA_DELAY;
        stop_data_delay = PRBS_MAX_DATA_DELAY;
        sweeping = true;
    }
    else
    {
        hal_rfic_read_reg(cards[0], 0, 0x006, &delay_reg);
        start_data_delay = delay_reg & 0xF;
        stop_data_delay = start_data_delay;
    }

    /* set start_clock_delay and stop_clock_delay based on whether sweeping is requested for the clock delay */
    if ( should_sweep_clock_delay )
    {
        start_clock_delay = PRBS_MIN_CLOCK_DELAY;
        stop_clock_delay = PRBS_MAX_CLOCK_DELAY;
        sweeping = true;
    }
    else
    {
        hal_rfic_read_reg(cards[0], 0, 0x006, &delay_reg);
        start_clock_delay = (delay_reg & 0xF0) >> 4;
        stop_clock_delay = start_clock_delay;
    }

    app_logf("<INFO> Starting sample rate %u, ending rate %u, step size %u, duration %u seconds\n",
              start_sample_rate, end_sample_rate, sample_rate_step, nr_seconds_requested);

    current_sample_rate = start_sample_rate;
    while ( ( current_sample_rate <= end_sample_rate ) &&
            ( (status == 0) || (sweeping == true) ) &&
            running )
    {
        for ( curr_data_delay = start_data_delay;
              ( curr_data_delay <= stop_data_delay ) &&
                  ( (status == 0) || (sweeping == true) ) &&
                  running;
              curr_data_delay++ )
        {
            for ( curr_clock_delay = start_clock_delay;
                  ( curr_clock_delay <= stop_clock_delay ) &&
                      ( (status == 0) || (sweeping == true) ) &&
                      running;
                  curr_clock_delay++ )
            {
                for ( i = 0; i < nr_cards; i++ )
                {
                    delay_reg = curr_data_delay | (curr_clock_delay << 4);
                    app_logf("<INFO> Setting delay register for card %u to 0x%02x (clock_delay=%u, data_delay=%u)\n",
                            cards[i], delay_reg, curr_clock_delay, curr_data_delay);
                    hal_rfic_write_reg(cards[i], 0, 0x006, delay_reg);
                }
                status = skiq_fact_prbs_run_test_multi(cards, nr_cards, current_sample_rate, nr_handles, nr_seconds_requested, prbs_errors);
            }
	}
	current_sample_rate += sample_rate_step;
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/

/*****************************************************************************/
/** This is the main function for executing the PRBS test.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    uint8_t cards[SKIQ_MAX_NUM_CARDS], nr_cards = 0;
    uint32_t prbs_errors[SKIQ_MAX_NUM_CARDS];
    int32_t status = 0;

    signal(SIGINT, app_cleanup);

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if ( (SKIQ_MAX_NUM_CARDS != card) &&
         (NULL != p_serial) )
    {
        app_logf("<ERROR> must specify EITHER card ID or serial number, not"
                 " both.\n");
        return (-1);
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if (NULL != p_serial)
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            app_logf("<ERROR> cannot find card with serial number %s (result"
                     " code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        app_logf("<INFO> found serial number %s as card ID %" PRIu8 "\n",
                 p_serial, card);
    }

    /* only probe if user omitted card number and serial number */
    if( ( SKIQ_MAX_NUM_CARDS == card ) &&
        ( NULL == p_serial ) )
    {
        // get Sidekiq cards
        skiq_get_cards( skiq_xport_type_auto, &nr_cards, cards );
    }
    else
    {
        cards[0] = card;
        nr_cards = 1;
    }

    /* open the summary file */
    output_fp = fopen( p_file_path, "wb" );
    if ( output_fp == NULL )
    {
        app_logf("<ERROR> unable to open file %s for logging\n",
                 p_file_path);
        return (-1);
    }

    /* register a custom logging function that writes to both output_fp and stderr */
    skiq_register_logging( app_log );

    app_logf("<INFO> Initializing PRBS test for %u card(s)\n", nr_cards);
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, cards, nr_cards);
    if ( status != 0 )
    {
        if ( EBUSY == status )
        {
            app_logf("<ERROR> Unable to initialize libsidekiq; one or more cards"
                     " seem to be in use (result code %" PRIi32 ")\n", status);
        }
        else if ( -EINVAL == status )
        {
            app_logf("<ERROR> Unable to initialize libsidekiq; was a valid card"
                     " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            app_logf("<ERROR> Unable to initialize libsidekiq with status %" PRIi32
                     "\n", status);
        }
        app_log_close();
        return (-1);
    }

    status = run_prbs_test( cards, nr_cards, num_chans,
                            start_rate, end_rate, step,
                            num_sec_req,
                            sweep_data_delay, sweep_clock_delay,
                            prbs_errors );

    skiq_exit();

    if ( status == -ENOLCK )
    {
        app_logf("<ERROR> There was at least one sample rate that never got lock\n");
        app_log_close();
        return (-1);
    }

    if ( status == -EFAULT )
    {
        uint8_t i;
        app_logf("<ERROR> There was at least one sample rate on one card that had errors\n");
        for ( i = 0; i < nr_cards; i++ )
        {
            app_logf("<ERROR> \tCard %u had %u errors\n", cards[i], prbs_errors[cards[i]]);
        }
        app_log_close();
        return (-2);
    }

    if ( status == -ECANCELED )
    {
        app_logf("<ERROR> PRBS test was canceled\n");
        app_log_close();
        return (-3);
    }

    app_log_close();
    return status;
}

