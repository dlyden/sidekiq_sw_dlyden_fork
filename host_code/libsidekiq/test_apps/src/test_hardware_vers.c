/*! \file test_hardware_vers.c
 * \brief This file contains a basic application that
 * tests whether the supplied hardware / product specifications match the specified Sidekiq.
 *
 * <pre>
 * Copyright 2016 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <strings.h>
#include <errno.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <dropkiq_api.h>
#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- obtain serial number information";
static const char* p_help_long = "\
Scan the system for Sidekiq cards, displaying a list of serial numbers\n\
\n\
Defaults:\n\
  --mode=PCIE";

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;

static char* p_addon = NULL;
static char* p_hw = NULL;
static char* p_mode = NULL;
static char* p_prod = NULL;
static char* p_rev = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Use specified Sidekiq card",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Query card by serial number",
                "SERIAL",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("addon",
                'a',
                "test hardware addons (NONE, DROPKIQ)",
                "ITEM",
                &p_addon,
                STRING_VAR_TYPE),
    APP_ARG_OPT("hardware",
                'H',
                "test hardware form factor (MPCIE, M2)",
                "TYPE",
                &p_hw,
                STRING_VAR_TYPE),
    APP_ARG_OPT("mode",
                'm',
                "test transport mode (PCIE, USB)",
                "MODE",
                &p_mode,
                STRING_VAR_TYPE),
    APP_ARG_OPT("product",
                'p',
                "test product type (001, 002)",
                "TYPE",
                &p_prod,
                STRING_VAR_TYPE),
    APP_ARG_OPT("revision",
                'r',
                "test hardware revision (A, B, C, D)",
                "ID",
                &p_rev,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

int main( int argc, char *argv[] )
{
    int32_t status=0;
    bool m2 = false, mpcie = false;
    bool revA = false, revB = false, revC = false, revD = false, revE = false;
    bool oh_oh_one = false, oh_oh_two = false;
    bool dkiq_avail = false, dkiq_req = false, skiq_only = false;
    skiq_hw_vers_t hardware_vers;
    skiq_product_t product_vers;
    skiq_xport_type_t type;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;
    bool skiq_initialized = false;
    pid_t owner = 0;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    skiq_register_logging(NULL);

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            printf("Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            status = -1;
            goto main_exit;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto main_exit;
    }

    if( NULL == p_hw )
    {
        m2 = true;
        mpcie = true;
    }
    else if( 0 == strncasecmp(p_hw, "m2", 3) )
    {
        m2 = true;
    }
    else if( 0 == strncasecmp(p_hw, "mpcie", 6) )
    {
        mpcie = true;
    }
    else
    {
        printf("invalid hardware\n");
        return 1;
    }

    if( NULL == p_rev )
    {
        revA = true;
        revB = true;
        revC = true;
        revD = true;
        revE = true;
    }
    else if( 0 == strncasecmp(p_rev, "a", 2) )
    {
        revA = true;
    }
    else if( 0 == strncasecmp(p_rev, "b", 2) )
    {
        revB = true;
    }
    else if( 0 == strncasecmp(p_rev, "c", 2) )
    {
        revC = true;
    }
    else if( 0 == strncasecmp(p_rev, "d", 2) )
    {
        revD = true;
    }
    else if( 0 == strncasecmp(p_rev, "e", 2) )
    {
        revE = true;
    }
     else
    {
        printf("invalid revision\n");
        return 1;
    }

    if( NULL == p_prod )
    {
        oh_oh_one = true;
        oh_oh_two = true;
    }
    else if ( 0 == strncasecmp(p_prod, "001", 4) )
    {
        oh_oh_one = true;
    }
    else if ( 0 == strncasecmp(p_prod, "002", 4) )
    {
        oh_oh_two = true;
    }
    else
    {
        printf("invalid product\n");
        return 1;
    }

    if( NULL == p_addon )
    {
        dkiq_req = true;
        skiq_only = true;
    }
    else if( 0 == strncasecmp(p_addon, "none", 5) )
    {
        skiq_only = true;
    }
    else if( 0 == strncasecmp(p_addon, "dropkiq", 8) )
    {
        dkiq_req = true;
    }
    else
    {
        printf("invalid addon\n");
        return 1;
    }

    if( NULL == p_mode )
    {
        type = skiq_xport_type_auto;
    }
    else
    {
        if( (0 == strncasecmp(p_mode, "pcie", 5)) &&
            (0 == skiq_is_xport_avail(card, skiq_xport_type_pcie)) )
        {
            type = skiq_xport_type_pcie;
        }
        else if( (0 == strncasecmp(p_mode, "usb", 4)) &&
                 (0 == skiq_is_xport_avail(card, skiq_xport_type_usb)) )
        {
            type = skiq_xport_type_usb;
        }
        else
        {
            printf("invalid mode\n");
            status = 1;
            goto main_exit;
        }
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(type,
                       level,
                       &card,
                       1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }
        goto main_exit;
    }
    skiq_initialized = true;

    /* read the hardware version */
    if( (status=skiq_read_hw_version(card,&hardware_vers)) != 0 )
    {
        printf("Error: unable to read hardware version, status %d\n", status);
    }

    /* read the product version */
    if( (status=skiq_read_product_version(card,&product_vers)) != 0 )
    {
        printf("Error: unable to read product version, status %d\n", status);
    }

    /* probe for Dropkiq */
    if( (status=dkiq_probe(card, &dkiq_avail)) != 0 )
    {
        printf("Error: unable to probe Dropkiq, status %d\n", status);
    }

    /* handle the mPCIe case */
    status = 1;
    if ( mpcie && ( ( revA && ( hardware_vers == skiq_hw_vers_mpcie_a ) ) ||
                    ( revB && ( hardware_vers == skiq_hw_vers_mpcie_b ) ) ||
                    ( revC && ( hardware_vers == skiq_hw_vers_mpcie_c ) ) ||
                    ( revD && ( hardware_vers == skiq_hw_vers_mpcie_d ) ) ||
                    ( revE && ( hardware_vers == skiq_hw_vers_mpcie_e ) ) ))
    {
        if ( ( oh_oh_one && ( product_vers == skiq_product_mpcie_001 ) ) ||
             ( oh_oh_two && ( product_vers == skiq_product_mpcie_002 ) ) )
        {
            status = 0;
        }
    }

    /* handle the m.2 case */
    if ( m2 && ( ( revB && ( hardware_vers == skiq_hw_vers_m2_b ) )||
                 ( revC && ( hardware_vers == skiq_hw_vers_m2_c ) ) ) )
    {
        if ( ( oh_oh_one && ( product_vers == skiq_product_m2_001 ) ) ||
             ( oh_oh_two && ( product_vers == skiq_product_m2_002 ) ) )
        {
            status = 0;
        }
    }

    /* handle Dropkiq addon */
    if( (dkiq_avail && skiq_only && !dkiq_req) ||
        (!dkiq_avail && !skiq_only && dkiq_req) )
    {
        status = 1;
    }

main_exit:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return ((int) status);
}

