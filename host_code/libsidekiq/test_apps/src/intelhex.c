#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>

#include "intelhex.h"
#include "util.h"

#define LINE_DEBUG              (0)


#define START_CODE              ':'
#define MIN_RECORD_STR_LEN      (11)
#define MAX_RECORD_STR_LEN      (2 * INTEL_MAX_RECORD_SIZE + 5)

static void
insertIntoList(struct RecordList *list, struct Record *rec)
{
    rec->next = list->head;
    list->head = rec;

    list->nmemb++;
}

static inline uint8_t
calculateChecksum(uint8_t *data, uint32_t len, bool includesChksum)
{
    uint32_t i = 0;
    uint32_t totalLen = len;
    uint32_t chksum = 0;

    if (includesChksum)
    {
        totalLen--;
    }

    for (i = 0; i < totalLen; i++)
    {
        chksum += data[i];
    }

    return (~(chksum & 0xFF) + 1);
}

/** @todo   Get rid of magic numbers in function. */
static ResultCode
parseLine(char *line, uint32_t lineLen, struct Record *rec)
{
    uint32_t len;
    uint8_t chksum;
    uint8_t buffer[INTEL_MAX_RECORD_SIZE];

    if (lineLen < MIN_RECORD_STR_LEN)
    {
        printf("ERROR %s : record length is too small (must be at least"
               " %u bytes))!\n",
                __FUNCTION__, MIN_RECORD_STR_LEN);
        return InvalidStringSize;
    }

    if (START_CODE != line[0])
    {
        printf("ERROR %s : invalid starting character (expecting '%c',"
                " got '%c').\n",
                __FUNCTION__, START_CODE, line[0]);
        return InvalidStartCode;
    }

    len = unhexlify(&(line[1]), (lineLen - 1), (char *) buffer,
                INTEL_MAX_RECORD_SIZE);
    if (0 == len)
    {
        printf("ERROR %s : hex string '%s' is invalid!\n",
                __FUNCTION__, (char *) &(line[1]));
        return InvalidHexString;
    }

    /* Verify that the length field is correct. */
    rec->length = buffer[0];
    if ((len - INTEL_RECORD_HEADER_LEN) != rec->length)
    {
        printf("ERROR %s : length field doesn't match amount of data read"
                " (read %u bytes, record says %u).\n",
                __FUNCTION__, (len - INTEL_RECORD_HEADER_LEN), rec->length);
        return InvalidSizeField;
    }

    /* Verify that the record type is correct. */
    rec->type = (enum RecordType) buffer[3];
    if (MaxValue <= rec->type)
    {
        printf("ERROR %s : invalid record type %u.\n",
                __FUNCTION__, rec->type);
        rec->type = Unknown;
        return InvalidRecordType;
    }
    if ((Eof == rec->type) && (0 != rec->length))
    {
        printf("ERROR %s : invalid length %u for EOF record.\n",
                __FUNCTION__, rec->length);
        return InvalidSizeField;
    }

    /* Verify the checksum */
    chksum = calculateChecksum(buffer, len, true);
    if (chksum != buffer[len - 1])
    {
        printf("ERROR %s : invalid checksum (found %x, calculated %x).\n",
                __FUNCTION__, buffer[len - 1], chksum);
        return InvalidChecksum;
    }

    rec->addr = (buffer[1] << 8) | buffer[2];

    if (0 < rec->length)
    {
        memcpy((void *) rec->data, &(buffer[4]), rec->length);
    }

    return Success;
}

static uint32_t
readline(FILE *fp, char *buffer, uint32_t bufLen)
{
    char ch[1];
    ssize_t readLen = 0;
    ssize_t read = 0;
    bool foundEol = false;

    /** @todo   Optimize. */
    while ((!foundEol) && (readLen < (ssize_t) bufLen))
    {
        read = fread((void *) ch, 1, 1, fp);
        if (0 == read)
        {
            readLen = 0;
            if (feof(fp))
            {
                printf("ERROR %s : reached EOF before EOL!\n", __FUNCTION__);
            }
            else if (ferror(fp))
            {
                printf("ERROR %s : file read error!\n", __FUNCTION__);
            }
            else
            {
                printf("ERROR %s : hmmm, unknown error.\n", __FUNCTION__);
            }
            goto finished;
        }
        else
        {
            if (('\r' == ch[0]) || ('\n' == ch[0]))
            {
                if (0 == readLen)
                {
                    /*
                        This is a leading newline - either it's from the
                        previous line or the hex file has extras; we don't
                        really care which, we're just gonna skip it.
                    */
                    continue;
                }
                else
                {
                    foundEol = true;
                }
            }
            else
            {
                buffer[readLen] = ch[0];
            }
        }

        if (!foundEol)
        {
            readLen += read;
        }
    }

finished:
    return readLen;
}

ResultCode
intel_loadHexFile(char *filename, struct RecordList *list)
{
    FILE *fp = NULL;
    ResultCode result = Success;
    struct Record *newRec = NULL;
    char lineBuf[MAX_RECORD_STR_LEN];
    uint32_t i = 0;
    uint32_t lineLen = 0;
    uint32_t lineNum = 1;
    bool foundEof = false;

    if ((NULL == filename) || (NULL == list))
    {
        printf("ERROR %s : invalid parameter provided!\n", __FUNCTION__);
        return InvalidParam;
    }

    list->nmemb = 0;
    list->head = NULL;
    list->complete = false;

    fp = fopen(filename, "rb");
    if (NULL == fp)
    {
        printf("ERROR %s : couldn't load file '%s'!\n", __FUNCTION__, filename);
        return InvalidFilename;
    }

    /** @todo Optimize. */
    while (!foundEof)
    {
        newRec = (struct Record *) calloc(1, sizeof(struct Record));
        if (NULL == newRec)
        {
            printf("ERROR %s : couldn't allocate memory for new record!\n",
                    __FUNCTION__);
            result = MemoryError;
            goto finished;
        }

        newRec->next = NULL;

        lineLen = readline(fp, lineBuf, MAX_RECORD_STR_LEN);
        if (0 == lineLen)
        {
            printf("ERROR %s : couldn't read line from file!\n",
                    __FUNCTION__);
            result = FileReadError;
            goto finished;
        }

        result = parseLine(lineBuf, lineLen, newRec);
        if (Success != result)
        {
            printf("ERROR %s : couldn't parse line %u:\n\t",
                    __FUNCTION__, lineNum);
            for (i = 0; i < lineLen; i++)
            {
                printf("%c", lineBuf[i]);
            }
            printf("\n");
            goto finished;
        }

#if (LINE_DEBUG == 1)
        {
            lineBuf[lineLen] = '\0';
            printf("\tRead line '%s' (%u)!\n", lineBuf, lineLen);
            printf("\t\tCount = %02X Addr = %04X Type = %02X\n",
                    newRec->length, newRec->addr, newRec->type);
        }
#endif

        lineNum++;

        switch (newRec->type)
        {
        case Eof:
            foundEof = true;
            list->complete = true;
            continue;
            break;

        case Data:
            insertIntoList(list, newRec);
            newRec = NULL;
            break;

        default:
            printf("WARN %s : skipping record type %u.\n",
                    __FUNCTION__, newRec->type);
            free(newRec);
            newRec = NULL;
            break;
        }
    }

finished:
    fclose(fp);
    fp = NULL;

    if (NULL != newRec)
    {
        free(newRec);
        newRec = NULL;
    }

    return result;
}

void
intel_sortRecordList(struct RecordList *list)
{
    struct Record *left = NULL;
    struct Record *right = NULL;
    struct Record *tempList = NULL;
    struct Record *item = NULL;
    struct Record *head = NULL;
    uint32_t listSize = 1;
    uint32_t leftSize = 0;
    uint32_t rightSize = 0;

    /* Keep looping until the entire list is sorted (no more sublists). */
#ifdef SORT_DEBUG
    printf ("%s : list->head = %p.\n", __FUNCTION__, list->head);
#endif
    while(true)
    {
        head = NULL;
        left = list->head;
        right = left;
        leftSize = 0;
        rightSize = 0;
        tempList = NULL;

#ifdef SORT_DEBUG
        printf("\tleft = %p [%u], right = %p [%u].\n",
                left, leftSize, right, rightSize);
#endif

        /*
            Loop through until there are no more unsorted sublists for this
            round.
        */
        while (left)
        {
            /*
                Move the right pointer listSize elements past the left pointer
                (or until we reach the end of the list).
            */
            while ((leftSize < listSize) && right)
            {
                right = right->next;
#ifdef SORT_DEBUG
                printf("\tright = %p.\n", right);
#endif
                leftSize++;
                if (NULL == right)
                {
#ifdef SORT_DEBUG
                    printf("\tright = NULL!\n");
#endif
                    break;
                }
            }

            rightSize = leftSize;

            /*
                Loop through all of the entries in the left and right sublists.
            */
            while ((leftSize > 0) || ((rightSize > 0) && right))
            {
#ifdef SORT_DEBUG
                printf("\tleftSize = %u rightSize = %u.\n", leftSize, rightSize);
#endif
                item = NULL;
                if (0 == leftSize)
                {
                    /* The left list is empty. */
                    item = right;
                    rightSize--;
                    right = right->next;
                }
                else if ((0 == rightSize) || (NULL == right) ||
                         (right->addr > left->addr))
                {
                    /*
                       The left side is lesser or the right list is empty.
                    */
                    item = left;
                    leftSize--;
                    left = left->next;
                }
                else
                {
                    /*
                       The right side is lesser or the left list is empty.
                    */
                    item = right;
                    rightSize--;
                    right = right->next;
                }
#ifdef SORT_DEBUG
                printf("\titem = %p leftSize = %u rightSize = %u.\n",
                        item, leftSize, rightSize);
#endif

                if (NULL == tempList)
                {
                    /*
                        This round's sorted list is empty, so we've got a new
                        head.
                    */
                    tempList = item;
                    tempList->next = NULL;
                    head = tempList;
#ifdef SORT_DEBUG
                    printf("\ttempList was empty; now is %p.\n", tempList);
#endif
                }
                else
                {
                    /* Append this item to this round's sorted list. */
                    tempList->next = item;
                    tempList = tempList->next;
#ifdef SORT_DEBUG
                    printf("\ttempList is %p.\n", tempList);
#endif
                }
            }

            /* Move onto the next unsorted portion of the list. */
#ifdef SORT_DEBUG
            printf("\tAdvancing left...\n");
#endif
            left = right;
        }
        /* Terminated this round's sorted list. */
        tempList->next = NULL;

#ifdef SORT_DEBUG
        {
            struct Record *blah = head;
            printf("\tTemp list:");
            while (blah != NULL)
            {
                printf(" %04X", blah->addr);
                blah = blah->next;
            }
            printf("\n");
        }
#endif

        /* Use this round's sorted list as the next round's starting list. */
        list->head = head;
#ifdef SORT_DEBUG
        printf("\tlist->head now is %p.\n", tempList);
#endif

        /* Double the length of the left / right lists. */
        listSize *= 2;
#ifdef SORT_DEBUG
        printf("\tLeft/right size is now %u.\n", listSize);
#endif

        /*
           If the left / right list sizes exceed the total length of the list,
           the list has been sorted.
        */
        if (listSize > list->nmemb)
        {
#ifdef SORT_DEBUG
            printf("\tlistSize is %u, nmemb is %u... bailing.\n",
                    listSize, list->nmemb);
#endif
            break;
        }
    }

#ifdef SORT_DEBUG
    printf("%s : done.\n", __FUNCTION__);
#endif
}

void
intel_cleanRecordList(struct RecordList *list)
{
    struct Record *curRec = NULL;
    struct Record *nextRec = NULL;

    if (NULL == list)
    {
        return;
    }

    curRec = list->head;
    while (NULL != curRec)
    {
        nextRec = curRec->next;
        free(curRec);
        curRec = nextRec;
        list->nmemb--;
    }

    if (0 != list->nmemb)
    {
        printf("WARN %s : somehow didn't entirely clean the list..."
                "%d members remaining.\n",
                __FUNCTION__, list->nmemb);
    }

    list->nmemb = 0;
    list->head = NULL;
}

void
intel_displayRecordList(struct RecordList *list)
{
    uint32_t i = 0;
    uint8_t j = 0;
    struct Record *rec = NULL;
    uint32_t totalLen = 0;

    if (NULL == list)
    {
        printf("ERROR %s : empty list provided!\n", __FUNCTION__);
        return;
    }

    printf("Displaying list %p (%u records)...\n", list, list->nmemb);
    if (!list->complete)
    {
        printf("\tThis doesn't seem to be a complete record list (the final"
                " record wasn't found)!\n");
    }

    rec = list->head;
    while (rec != NULL)
    {
        printf("%04u. %04X [%2uB]:", i, rec->addr, rec->length);
        if (Data != rec->type)
        {
            printf("\t*** Unexpected record type %u.\n", rec->type);
        }
        for (j = 0; j < rec->length; j++)
        {
            printf(" %02X", rec->data[j]);
        }
        printf("\n");

        totalLen += rec->length;

        rec = rec->next;
        i++;
    }

    printf("Total length of read data: %u.\n", totalLen);
}

