/*! \file rfic_rw_test.c
 * \brief This file contains a basic application that reads and writes
 * registers to the RF IC as verifies that the data is correct.
 *  
 * <pre>
 * Copyright 2013,2018 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>

#include <bit_ops.h>
#include <sidekiq_hal.h>
#include <sidekiq_api.h>
#include <sidekiq_private.h>

#include "elapsed.h"

static char* app_name;
static uint8_t card;

static void print_usage(void);

#define NUM_LEGACY_CS (1)
uint16_t legacy_skiq_reg[] = {0x3FC};

#define NUM_X2_CS (2)
uint16_t x2_skiq_reg[] = {0x022, 0x7};

#define NUM_X4_CS (3)
uint16_t x4_skiq_reg[] = {0x3EE0, 0x7, 0x3EE0};

#define NUM_NV100_CS                    1
uint16_t nv100_skiq_reg[] = { 0x0009 };


/***** DEFINES *****/

#define WRITE_REG(_card,_cs,_addr,_data)                        \
    do {                                                        \
        elapsed_start(&write_time);                             \
        hal_rfic_write_reg( _card, _cs, _addr, _data );         \
        elapsed_end(&write_time);                               \
    } while (0)

#define READ_REG(_card,_cs,_addr,_data)                         \
    do {                                                        \
        elapsed_start(&read_time);                              \
        hal_rfic_read_reg( _card, _cs, _addr, _data );          \
        elapsed_end(&read_time);                                \
    } while (0)


/*****************************************************************************/
/** This is the main function for executing the ad9361_rw test app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    uint8_t data = 0;
    uint32_t random = 0;
    uint32_t i=0;
    uint8_t read_data = 0;
    uint8_t prev_data = 0;
    uint32_t num_fails = 0;
    uint32_t num_fails_per_cs = 0;
    uint8_t num_reads = 1;
    int status=0;
    uint32_t num_times=0;
    skiq_xport_init_level_t level = skiq_xport_init_level_full;
    uint8_t j=0;
    uint8_t num_cs=0;
    uint16_t *p_scratch_reg;

    app_name = argv[0];
    if( argc != 3 )
    {
        printf("Error: invalid # of command line args\n");
        print_usage();
        exit(-1);
    }

    /* restrict card to less than SKIQ_MAX_NUM_CARDS - 1 */
    {
        unsigned long int n = strtoul(argv[1], NULL, 10);
        if ( n >= SKIQ_MAX_NUM_CARDS )
        {
            printf( "Error: invalid card specified (%lu)\n", n );
            return -1;
        }
        else
        {
            card = n;
        }
    }

    /* restrict num_times to less than UINT32_MAX */
    {
        unsigned long int n = strtoul(argv[2], NULL, 10);
        num_times = (n < UINT32_MAX) ? n : UINT32_MAX;
    }

    status = skiq_init(skiq_xport_type_auto, level, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    switch (_skiq_get_part(card))
    {
        case skiq_x2:
            num_cs = NUM_X2_CS;
            p_scratch_reg = x2_skiq_reg;
            break;

        case skiq_x4:
            num_cs = NUM_X4_CS;
            p_scratch_reg = x4_skiq_reg;
            break;

        case skiq_nv100:
            num_cs = NUM_NV100_CS;
            p_scratch_reg = nv100_skiq_reg;
            break;

        default:
            num_cs = NUM_LEGACY_CS;
            p_scratch_reg = legacy_skiq_reg;
            break;
    }
    

    // write a random value and read it back several times to see if it changes
    for ( j = 0; j < num_cs; j++ )
    {
        ELAPSED(read_time);
        ELAPSED(write_time);

        num_fails_per_cs = 0;
        for( i=0; i<num_times; i++ )
        {
            prev_data = data;
            random = rand();
            data = random % 255;

            WRITE_REG( card, j, p_scratch_reg[j], data );
            READ_REG( card, j, p_scratch_reg[j], &read_data );
            
            if( data != read_data )
            {
                status = -1;
                num_fails++;
                num_fails_per_cs++;
            }
            while( (data != read_data) && (num_reads < 20) )
            {
                READ_REG( card, j, p_scratch_reg[j], &read_data );
                num_reads++;
            }
            if( num_reads > 1 )
            {
                printf("\nFailure at iteration %u (cs %u, reg 0x%x), num_reads %u\n",
                       i, j, p_scratch_reg[j], num_reads);
                if( num_reads >= 20 )
                {
                    printf("Failed maximum number of times, write was %u, read was %u, prev data %u\n",
                           data, read_data, prev_data);
                }
                num_reads = 1;
            }
        }
        printf("Total number of tests %u for cs %u, total failures %u, chip select failures %u\n", \
               i, j, num_fails, num_fails_per_cs );

        printf("======================================================================\n");
        printf("           Total time for register reads: "),print_total(&read_time);
        printf("          Total number of register reads: "),print_nr_calls(&read_time);
        printf(" Minimum time for a single register read: "),print_minimum(&read_time);
        printf(" Average time for a single register read: "),print_average(&read_time);
        printf(" Maximum time for a single register read: "),print_maximum(&read_time);
        printf("======================================================================\n");
        printf("          Total time for register writes: "),print_total(&write_time);
        printf("         Total number of register writes: "),print_nr_calls(&write_time);
        printf("Minimum time for a single register write: "),print_minimum(&write_time);
        printf("Average time for a single register write: "),print_average(&write_time);
        printf("Maximum time for a single register write: "),print_maximum(&write_time);
        printf("======================================================================\n");
    }

    skiq_exit();

    return (status);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <card> <# transactions>\n",app_name);
    printf("   Performs read/write tests to a scratch register on the AD9361\n");
}

