/*! \file fpga_rw_test.c
 * \brief This file contains a basic application that reads and writes
 * registers to the FPGA as verifies that the data is correct.
 *  
 * <pre>
 * Copyright 2016,2018 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <bit_ops.h>
#include <sys/time.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <sidekiq_hal.h>
#include <sidekiq_xport.h>
#include <arg_parser.h>

#include "elapsed.h"

/***** DEFINES *****/
/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#ifndef DEFAULT_NUM_TIMES
#   define DEFAULT_NUM_TIMES  10
#endif

#ifndef MAX_NUM_READS
#   define MAX_NUM_READS      (20)
#endif

#define READ_REG(_card,_addr,_data, _use_reg64)                         \
    do {                                                                \
        elapsed_start(&read_time);                                      \
        if( _use_reg64 == false )                                       \
       {                                                               \
            sidekiq_fpga_reg_read( _card, _addr, (uint32_t*)(_data) );  \
        }                                                               \
        else                                                            \
        {                                                               \
            sidekiq_fpga_reg_read_64( _card, _addr+4, _addr, (uint64_t*)(_data) ); \
        }                                                               \
        elapsed_end(&read_time);                                        \
        stats_num_read++;                                               \
    } while (0)


#define WRITE_REG(_card,_addr,_data,_use_reg64)                         \
    do {                                                                \
        elapsed_start(&write_time);                                     \
        if( _use_reg64 == false )                                       \
        {                                                               \
            sidekiq_fpga_reg_write( _card, _addr, (uint32_t)_data );    \
        }                                                               \
        else                                                            \
        {                                                               \
            sidekiq_fpga_reg_write_64( _card, _addr+4, _addr, (uint64_t)(_data )); \
        }                                                               \
        elapsed_end(&write_time);                                       \
        stats_num_write++;                                              \
    } while (0)


/* From How to generate random 64-bit unsigned integer in C -- https://stackoverflow.com/a/33021408
 *
 * rand() does not always provide a 64-bit random number on all platforms.  Use RAND_MAX and some
 * looping to get a random(ish) number across all 64-bits
 */
#if RAND_MAX/256 >= 0xFFFFFFFFFFFFFF
  #define LOOP_COUNT 1
#elif RAND_MAX/256 >= 0xFFFFFF
  #define LOOP_COUNT 2
#elif RAND_MAX/256 >= 0x3FFFF
  #define LOOP_COUNT 3
#elif RAND_MAX/256 >= 0x1FF
  #define LOOP_COUNT 4
#else
  #define LOOP_COUNT 5
#endif

static uint64_t rand_uint64(void)
{
    int i;
    uint64_t r = 0;
    for (i = 0; i < LOOP_COUNT; i++)
    {
        r = r * (RAND_MAX + (uint64_t)1) + rand();
    }
    return r;
}


/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "FPGA register verification";
static const char* p_help_long = "\
Performs read/write tests to a scratch register on the FPGA.  If\n\
testing 64-bit access, aligned or unaligned access can be tested\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --num-times=" xstr(DEFAULT_NUM_TIMES) "\n";


/***** LOCAL DATA *****/

static uint8_t card;
static char* p_serial = NULL;
static uint32_t num_times=DEFAULT_NUM_TIMES;
static char* p_reg64_alignment = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("num-times",
                'n',
                "Number of times (N) to read/write register",
                "N",
                &num_times,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("64bit",
                0,
                "64-bit register transactions, `aligned` or `unaligned` on 64-bit address boundary",
                "ALIGN",
                &p_reg64_alignment,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,    
};
/***** LOCAL FUNCTION PROTOTYPES *****/

/*****************************************************************************/
/** This is the main function for executing the fpga_rw test app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    uint64_t data = 0;
    uint64_t read_data = 0;
    uint64_t prev_data = 0;
    uint64_t random = 0;
    uint32_t i=0;
    uint32_t num_fails = 0;
    uint8_t num_reads = 1;
    int status=0;
    bool use_reg64 = false;
    uint32_t scratch_reg = FPGA_REG_SET_ALL_TIMESTAMP_LOW;

    ELAPSED(read_time);
    ELAPSED(write_time);
    uint32_t stats_num_read = 0, stats_num_write = 0;

    srand(time(NULL));

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if ( 0 != status )
        {
            printf("Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 "\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    if( p_reg64_alignment != NULL )
    {
        use_reg64 = true;
        if( strcmp(p_reg64_alignment, "aligned") == 0 )
        {
            scratch_reg = FPGA_REG_1PPS_RESET_TIMESTAMP_LOW;
        }
        else if( strcmp(p_reg64_alignment, "unaligned") == 0 )
        {
            scratch_reg = FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_LOW;
        }
        else
        {
            printf("Error: invalid 64bit register alignment (must be `aligned` or `unaligned`\n");
            return (-1);
        }
    }

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_basic, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    // write a random value and read it back several times to see if it changes
    for( i=1; i<=num_times; i++ )
    {
	prev_data = data;

        random = rand_uint64();
        if( use_reg64 == true )
        {
            data = random % UINT64_MAX;
        }
        else
        {
            data = random % UINT32_MAX;
        }
        WRITE_REG( card, scratch_reg, data, use_reg64 );
        READ_REG( card, scratch_reg, &read_data, use_reg64 );

	if( (data != read_data) )
	{
            status = -1;
	    num_fails++;
	}
	while( (data != read_data) && (num_reads<MAX_NUM_READS) )
	{
            READ_REG( card, scratch_reg, &read_data, use_reg64 );
	    num_reads++;
	}
	if( num_reads > 1 )
	{
	    printf("\nFailure at iteration %u, num_reads %u\n", i, num_reads);
	    if( num_reads >= MAX_NUM_READS )
	    {
                printf("Failed maximum number of times, write was 0x%" PRIx64 ", read was 0x%" PRIx64 ", prev data 0x%" PRIx64 "\n",
                       data, read_data, prev_data);
	    }
	    num_reads = 1;
	}
    }
    printf("Total number of tests %u, failures %u\n", i-1, num_fails);

    printf("======================================================================\n");
    printf("           Total time for register reads: "),print_total(&read_time);
    printf("          Total number of register reads: "),print_nr_calls(&read_time);
    printf(" Minimum time for a single register read: "),print_minimum(&read_time);
    printf(" Average time for a single register read: "),print_average(&read_time);
    printf(" Maximum time for a single register read: "),print_maximum(&read_time);
    printf("======================================================================\n");
    printf("          Total time for register writes: "),print_total(&write_time);
    printf("         Total number of register writes: "),print_nr_calls(&write_time);
    printf("Minimum time for a single register write: "),print_minimum(&write_time);
    printf("Average time for a single register write: "),print_average(&write_time);
    printf("Maximum time for a single register write: "),print_maximum(&write_time);
    printf("======================================================================\n");

    skiq_exit();

    return (status);
}
