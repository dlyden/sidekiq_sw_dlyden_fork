/*! \file lna_bias.c
 * \brief This file contains the ability to read and write
 * the LNA bias current configuration.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>

#include <arg_parser.h>

#include <inttypes.h>

#include "rfe_z2_b.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = \
    "- configures or queries LNA bias current configuration";
static const char* p_help_long =
"\
Configures or queries the LNA bias current configurations. If a bias\n\
index is not specified, the current configuration is reported.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
";

static uint8_t card = UINT8_MAX;
static uint8_t config_index = UINT8_MAX;
static bool sweep=false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("index",
                'i',
                "Specify bias current setting to save by index " xstr(Z2_BIAS_INDEX_MIN) "-" xstr(Z2_BIAS_INDEX_MAX),
                "ID",
                &config_index,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("sweep",
                0,
                "Sweep the bias current index",
                NULL,
                &sweep,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

int main( int argc, char *argv[] )
{
    int32_t status=0;
    uint8_t read_index=0;
    pid_t owner;
    bool skiq_initialized=false;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( UINT8_MAX == card )
    {
        card = DEFAULT_CARD_NUMBER;
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if ( status != 0 )
    {
        if ( ( EBUSY == status) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }

        status = -1;
        goto finished;
    }
    else
    {
        skiq_initialized = true;
    }

    if( UINT8_MAX != config_index )
    {
        if( (status=skiq_fact_write_default_bias_index( card, config_index )) == 0 )
        {
            printf("Info: successfully configured index to %" PRIu8 "\n", config_index);
        }
        else
        {
            printf("Error: unable to configure bias index (status=%d)\n", status);
        }
    }


    if( status == 0 )
    {
        // read the stored configuration
        if( skiq_fact_read_default_bias_index(card, &read_index) == 0 )
        {
            printf("Info: currently stored bias index %" PRIu8 "\n", read_index);
        }
        else
        {
            printf("Error: unable to read stored bias index (status=%d)\n", status);
        }

        // read the active configuration
        if( skiq_fact_read_bias_index(card, &read_index) == 0 )
        {
            printf("Info: current bias index %" PRIu8 "\n", read_index);
        }
        else
        {
            printf("Error: unable to read current bias index (status=%d)\n", status);
        }
    }

    if( sweep == true )
    {
        uint8_t bias=Z2_BIAS_INDEX_MIN;

        // make sure the LNA state is enabled
        printf("Info: enabling LNA\n");
        if( skiq_fact_write_rx_lna(card, skiq_rx_hdl_A1, true) != 0 )
        {
            printf("Error: unable to enable LNA\n");
        }

        for( bias=Z2_BIAS_INDEX_MIN;
             bias<=Z2_BIAS_INDEX_MAX;
             bias++ )
        {
            printf("Info: updating bias to %u\n", bias);
            if( skiq_fact_write_bias_index(card, bias) != 0 )
            {
                printf("Error: failed to write bias to %u\n", bias);
            }
        }
    }
finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return (status);
}
