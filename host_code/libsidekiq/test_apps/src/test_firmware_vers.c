/*! \file version_test.c
 * \brief This file contains a basic application that
 * reads the firmware version of the Sidekiq.
 *  
 * <pre>
 * Copyright 2014 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <sidekiq_api.h>
#include <stdio.h>
#include <stdlib.h>

static char* app_name;
static void print_usage(void);

int main( int argc, char *argv[] )
{
    int32_t status=0;

    uint8_t firmware_maj;
    uint8_t firmware_min;
    
    uint8_t firmware_maj_in;
    uint8_t firmware_min_in;
    uint8_t card=0;

    app_name = argv[0];
    if( argc != 4 )
    {
        printf("Error: invalid # args\n");
        print_usage();
        return (-1);
    }

    card = (uint8_t)(strtoul(argv[1],NULL,10));
    firmware_maj_in = (uint8_t)(strtoul(argv[2],NULL,10));
    firmware_min_in = (uint8_t)(strtoul(argv[3],NULL,10));

    status = skiq_init(skiq_xport_type_usb, skiq_xport_init_level_basic, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    if( (status=skiq_read_fw_version(card, &firmware_maj, &firmware_min)) != 0 )
    {
        printf("Error: unable to read firmware version, status %d\n", status);
        skiq_exit();
        return (status);
    }

    if( (firmware_maj == firmware_maj_in) && (firmware_min == firmware_min_in) )
    {
        printf("Info: firmware versions match\n");
        status=0;
    }
    else
    {
        printf("Info: firmware version do not match\n");
        status=1;
    }
    skiq_exit();

    return (status);
}

static void print_usage(void)
{
    printf("Usage: %s <card> <major> <minor>\n", app_name);
    printf("   Queries the current firmware version and determines if it\n");
    printf("   matches the version information provided\n");
}
