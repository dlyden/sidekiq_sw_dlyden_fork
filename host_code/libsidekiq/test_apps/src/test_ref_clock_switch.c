/*! \file ref_clock_switch.c
 * \brief This file contains the ability to write the
 * reference clock configuration using the public API
 * without writing the configuration to EEPROM.
 *
 * <pre>
 * Copyright 2013 - 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#define DEFAULT_INIT_LEVEL  "basic"
#define DEFAULT_REF_CLOCK_FREQ_40M  40000000

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = \
    "- test application that configures reference clock configuration using the public API";
static const char* p_help_long =
"\
Test application that configures the reference clock configurations.\n\
If a reference clock source is not specified, a usage message will\n\
be supplied.\n\
\n\
Any external reference clock sources should be disconnected before\n\
running this test application.\n\
\n\
This API is not supported on Sidekiq mPCIe or M.2.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --init-level=" xstr(DEFAULT_INIT_LEVEL) "\n\
";

static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static int32_t crit_error_status = 0;
static bool ext_refclk_connect = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("ext-ref-clk",
                0,
                "The external ref clock is connected ",
                "NULL",
                &ext_refclk_connect,
                BOOL_VAR_TYPE),        
    APP_ARG_TERMINATOR,
};

static void critErrorCallback(int32_t status, void *p_user_data)
{
    (void) p_user_data;

    printf("Warning: received critical error from libsidekiq (result code %"
            PRIi32 ")\n", status);
    crit_error_status = status;
}

int main( int argc, char *argv[] )
{
    int32_t status=0;
    skiq_ref_clock_select_t read_ref_clock=skiq_ref_clock_invalid;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number, not"
                " both\n");
        status = -EPERM;
        goto finished;
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            status = -ENODEV;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -ERANGE;
        goto finished;
    }

    /* register an error handler, to catch invalid or missing reference clock errors */
    /* crit_error_status will be set by the callback method */
    skiq_register_critical_error_callback(critErrorCallback, NULL);

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if (0 != status )
    {
        fprintf(stderr, "Error: unable to initialize card %" PRIu8 "(result code %"
                PRIi32 ")\n", card, status);
        goto finished;
    }

    // Test case: switch to reference clock that is already selected
    status = skiq_read_ref_clock_select( card, &read_ref_clock );
    if ( 0 != status )
    {
        fprintf(stderr, "Error: failed to read reference clock source (result code %"
                PRIi32 ") on card %" PRIu8 "\n", status, card);
        goto finished;
    }
    status = skiq_write_ref_clock_select(card, read_ref_clock);
    if ( 0 != status )
    {
        fprintf(stderr, "Error: failed to write reference clock source (result code %"
                PRIi32 ") on card %" PRIu8 "\n", status, card);
        goto finished;
    }

    // determine critical error based on part
    skiq_param_t param;
    int32_t expected_crit_error = -EPERM;

    status = skiq_read_parameters(card, &param);
    if (status != 0)
    {
        fprintf(stderr, "Error: failed to get part type on card %" PRIu8 " (result code %" PRIi32 ")\n", card, status);
        goto finished;
    }

    // On an NV100 when an ARM critical error occurs the error is caught and an errno of -EPROTO is returned
    skiq_part_t part = param.card_param.part_type;
    if ( part == skiq_nv100 )
    {
        expected_crit_error = -EPROTO;

    }
    
    // There is a hardware defect on sidekiq stretch: pll lock detect didn't connect to io expander,
    // libsidekiq can't read the pll lock status, we will skip external ref clock test on stretch for now.
    if ( part != skiq_m2_2280 )
    {
        // Test case: switch to external reference clock when external reference clock is connected
        if ( ext_refclk_connect )  
        {      
            if ( ( part == skiq_nv100 ) )
            {
                // when writing the external ref clock frequency an ext ref clock is automatically selected
                status = skiq_write_ext_ref_clock_freq(card, DEFAULT_REF_CLOCK_FREQ_40M);
            }
            else
            {
                status = skiq_write_ref_clock_select(card, skiq_ref_clock_external);
            }
            if ( status != 0 )
            {
                fprintf(stderr, "Error: failed to configure to external reference clock %" PRIu8 " (result code %" PRIi32 ")\n", card, status);
                goto finished;
            }
        }
        // Test case: switch to external reference clock when none exists
        else  
        {   
            skiq_write_ref_clock_select(card, skiq_ref_clock_external);
            if ( expected_crit_error != crit_error_status )
            {
                if ( crit_error_status == 0)
                {
                    crit_error_status = -EFAULT;
                    fprintf(stderr, "Error: switching to external was expected to fail w/o setting ext-ref-clk flag when external ref clock connected %" PRIi32 ", ", crit_error_status);                

                }
                else
                {
                    fprintf(stderr, "Error: switching to external was expected to fail with status %" PRIi32 ", but returned %" PRIi32", "
                                "please disconnect external reference clock\n", expected_crit_error, crit_error_status);
                }
                status = crit_error_status;
                goto finished;
            }
            else
            {
                printf("Info: expectedly failed to configure external reference clock selection "
                        "when no external reference clock source present (result code %" PRIi32 ")"
                        " on card %" PRIu8 "\n", crit_error_status, card);
            }
        }
    }
    // Test case: switch to invalid reference clock
    status = skiq_write_ref_clock_select(card, skiq_ref_clock_invalid);
    if ( -EINVAL != status )
    {
        fprintf(stderr, "Error: expected failure, attempted to set reference clock source to invalid value\n");
    }
    else
    {
        printf("Info: expectedly failed to configure invalid reference clock selection (result code %" PRIi32 ")"
        " on card %" PRIu8 "\n", status, card);
        status = 0;
    }

finished:
    skiq_exit();
    return (status);
}
