/*! \file fdd_rx_tx_samples.c
 * \brief This file contains a basic application for configuring
 * a Sidekiq to do the following:
 *   -Configure the Rx interface
 *   -Configure the Tx interface
 *   -Start the Rx interface in the main thread, storing samples to a file
 *   -Start a separate Tx thread and loop:
 *       -sleep for a short duration (~1 second)
 *       -transmit samples from a file 
 *
 * Many of the RF configuration parameters are defaulted to sane 
 * values for this application to minimize the command line args 
 * required to run both rx and tx interfaces.  These can always be 
 * tweaked if needed.
 *
 * <pre>
 * Copyright 2014 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/
#ifdef __MINGW32__
/* we use MinGW's stdio since the msvcrt.dll version of sscanf won't support "%hhu" (aka SCNu8) */
#undef __USE_MINGW_ANSI_STDIO
#define __USE_MINGW_ANSI_STDIO 1
#endif /* __MINGW32__ */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <inttypes.h>

#include <sidekiq_api.h>

static char* app_name;

// parameters read from command line
static uint32_t sample_rate;
static uint8_t card;

static bool running=true;

uint32_t num_blocks=100;  // # full blocks to send
uint32_t packet_gap=0;
bool packed=false;

uint64_t start_ts=1000000;
uint64_t curr_tx_ts;
uint64_t curr_rx_ts;

uint32_t num_rx_bytes_to_alloc=0;

uint32_t* p_rx_iq;  // pointer to the Rx data
skiq_tx_block_t **p_tx_blocks = NULL;  /* reference to an array of transmit block references */

pthread_t tx_thread; // transmit thread

/* local functions */
static void print_usage(void);
static void process_cmd_line_args(int argc, char *argv[]);
static int32_t prepare_rx(void);
static int32_t prepare_tx(void);
static void recv_samples(void);
static void* send_samples(void*);
static void validate_loopback(void);

static void unpack_data(int16_t *packed_data, int16_t *unpacked_data, uint32_t num_unpacked_samples);
static void pack_data(int16_t *unpacked_data, int16_t *packed_data, uint32_t num_unpacked_samples);
static int16_t sign_extend( int16_t in );

#define NUM_RX_PAYLOAD_WORDS_IN_BLOCK (SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS-SKIQ_RX_HEADER_SIZE_IN_WORDS)
#define NUM_RX_PACKED_PAYLOAD_WORDS_IN_BLOCK ((1017*4)/3)
#define NUM_TX_PAYLOAD_WORDS_IN_BLOCK (1020)
#define NUM_TX_PACKED_PAYLOAD_WORDS_IN_BLOCK ((NUM_TX_PAYLOAD_WORDS_IN_BLOCK*4)/3)

#define SOP_NUM_PACKETS (3)

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: cleaning up libsidekiq\n");
    // set the flag for everything to cleanup
    running=false;
}

/*****************************************************************************/
/** This is the main function for executing the fdd_rx_tx_samples app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status=0;

    uint8_t reg;

    app_name = argv[0];

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    /* initialize everything based on the arguments provided */
    process_cmd_line_args( argc, argv );

    /* initialize libsidekiq for the card specified */
    status = skiq_init(skiq_xport_type_pcie, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    /* set the mode (packed or unpacked) */
    if( (status=skiq_write_iq_pack_mode(card, packed)) != 0 )
    {
        printf("Error: unable to set the packed mode\n");
    }

    //////////////////////////////////////////
    // TODO: put this code in ad9361 or sidekiq?
    printf("Enabling loopback\n");
    /* Read RX channel and filter configuration */
    skiq_read_rfic_reg(card, 0x3, &reg);
    
    /* Disable RX1 and enable RX2 */
    reg &= 0x3F;
    reg |= 0x80;
    skiq_write_rfic_reg(card, 0x3, reg);
    /* Read BIST Config2 */
    skiq_read_rfic_reg(card, 0x3f5, &reg);

    /* Enable loop test (digital loopback mode) */
    reg |= 1;

    //(MZ -- should we also set the channel bit to chan 2)
    skiq_write_rfic_reg(card, 0x3f5, reg);
    //////////////////////////////////

    /* prepare the interfaces */
    if( (status=prepare_rx()) != 0 )
    {
        printf("Error: unable to initialize Rx parameters\n");
        return (status);
    }
    if( (status=prepare_tx()) != 0 )
    {
        printf("Error: unable to initialize Tx parameters\n");
        return (status);
    }

    skiq_reset_timestamps( card );

    /* fire off a thread to handle transmit tasks */
    pthread_create(&tx_thread, NULL, send_samples, NULL);

    printf("Info: start Recv samples\n");
    recv_samples();
    printf("Info: done receiving samples\n");

    if (pthread_join(tx_thread,NULL))
    {
	printf("Error: failed to join Tx thread\n");
	exit(-1);
    }

    validate_loopback();

    free(p_rx_iq);

    skiq_exit();

    printf("Info: Success\n");

    return 0;
}

/*****************************************************************************/
/** The prepare_rx function does all configuration of the Rx interface,
    without actually starting the interface.

    @param void
    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t prepare_rx(void)
{
    int32_t status=0;

    if( (status=skiq_write_rx_dc_offset_corr(card, 
                                             skiq_rx_hdl_A1, 
                                             false)) != 0 )
    {
        printf("Error: unable to disable DC offset\n");
        return (status);
    }

    if( (status=skiq_write_rx_sample_rate_and_bandwidth(card, 
                                                        skiq_rx_hdl_A1, 
                                                        sample_rate, 
                                                        sample_rate)) != 0 )
    {
        printf("Error: unable to configure Rx sample rate and bandwidth\n");
        return (status);
    }

    // allocate memory to save all the receive data
    if( packed == false )
    {
        num_rx_bytes_to_alloc = (((num_blocks+1)*(NUM_RX_PAYLOAD_WORDS_IN_BLOCK+packet_gap))*sizeof(uint32_t));
    }
    else
    {
        num_rx_bytes_to_alloc = (((num_blocks+1)*(NUM_RX_PACKED_PAYLOAD_WORDS_IN_BLOCK+packet_gap))*sizeof(uint32_t));
    }
    printf("Rx alloc %u\n", num_rx_bytes_to_alloc);
    p_rx_iq = malloc( num_rx_bytes_to_alloc );
    if( p_rx_iq == NULL )
    {
        printf("Error: didn't successfully allocate %d bytes to hold unpacked iq\n", 
               num_rx_bytes_to_alloc);
    }

    return (status);
}

/*****************************************************************************/
/** The prepare_tx function does all configuration of the Tx interface,
    without actually starting the interface.

    @param void
    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t prepare_tx(void)
{
    int32_t status=0;
    uint32_t curr_block=0;
    uint32_t i=0;
    uint64_t tx_timestamp=0;
    int16_t *p_tmp;
    int16_t sample=0;
    uint32_t payload_size=0;

    if( (status=skiq_write_tx_data_flow_mode(card, 
                                             skiq_tx_hdl_A1, 
                                             skiq_tx_with_timestamps_data_flow_mode)) != 0 )
    {
        printf("Error: unable to configure Tx data flow mode\n");
        return (status);
    }
    if( (status=skiq_write_tx_block_size(card,
                                         skiq_tx_hdl_A1,
                                         NUM_TX_PAYLOAD_WORDS_IN_BLOCK)) != 0 )
    {
        printf("Error: unable to configure Tx block size\n");
        return (status);
    }
    if( (status=skiq_write_tx_sample_rate_and_bandwidth(card, 
                                                        skiq_tx_hdl_A1, 
                                                        sample_rate, 
                                                        sample_rate)) != 0 )
    {
        printf("Error: unable to configure Tx sample rate and bandwidth\n");
        return (status);
    }

    if( packed == true )
    {
        payload_size = NUM_TX_PACKED_PAYLOAD_WORDS_IN_BLOCK;
    }
    else
    {
        payload_size = NUM_TX_PAYLOAD_WORDS_IN_BLOCK;
    }

    uint32_t tx_packed_buf[1360];

    // allocate memory for all blocks
    p_tx_blocks = calloc( num_blocks, sizeof( skiq_tx_block_t * ) );
    tx_timestamp = start_ts;
    for( curr_block=0; curr_block<num_blocks; curr_block++ )
    {
        p_tx_blocks[curr_block] = skiq_tx_block_allocate_by_bytes( 4 * NUM_TX_PAYLOAD_WORDS_IN_BLOCK );
        skiq_tx_set_block_timestamp( p_tx_blocks[curr_block], tx_timestamp );

        if( packed == false )
        {
            p_tmp = (int16_t*)(p_tx_blocks[curr_block]->data);
        }
        else
        {
            p_tmp = (int16_t*)(tx_packed_buf);
        }

        // put our SOP at the beginning of the first block
        if( curr_block==0 )
        {
            for( i=0; i<3; i++ )
            {
                *p_tmp = -2048;
                p_tmp++;
                *p_tmp = -2048;
                p_tmp++;
            }
        }
        else
        {
            i=0;
        }
        // Note: i set immediately above in i statement
        for( ; i<(payload_size*2); i=i+2 )
        {
            *p_tmp = sample++;
            p_tmp++;
            *p_tmp = sample++;
            p_tmp++;
            // deal with the 12-bit wrap
            if( sample == 0x800 )
            {
                // wrap it to -2046 (instead of -2048...which is our SOP)
                //sample = -2046;
                sample = -2048;
            }
        }
        // if we're running in packed, we need to pack it now
        if( packed == true )
        {
            pack_data( (int16_t*)(tx_packed_buf), 
                       (int16_t*)(p_tx_blocks[curr_block]->data),
                       1360 );
        }
        tx_timestamp += (payload_size+packet_gap);
    }

    return (status);
}

/*****************************************************************************/
/** The recv_samples function is responsible for receiving the requested
    # of I/Q samples and storing them in a file.

    @param none
    @return void
*/
static void recv_samples(void)
{
    skiq_rx_hdl_t hdl;
    skiq_rx_block_t *p_rx_block;
    uint32_t len;
    uint64_t curr_timestamp=0;
    skiq_rx_status_t status=0;
    bool done=false;
    bool first_timestamp = true;
    uint32_t* p_next_rx_write;
    uint64_t next_timestamp = 0;   

    uint32_t tot_num_bytes=0;
    uint32_t num_bytes=0;
    uint32_t offset;
    uint32_t payload_data[1356];

    uint32_t payload_size=0;

    p_next_rx_write = p_rx_iq;

    printf("Info: receiving samples\n");
    skiq_start_rx_streaming(card, skiq_rx_hdl_A1);

    if( packed == true )
    {
        payload_size = NUM_RX_PACKED_PAYLOAD_WORDS_IN_BLOCK;
    }
    else
    {
        payload_size = NUM_RX_PAYLOAD_WORDS_IN_BLOCK;
    }
    
    // call receive until all of the bytes are received
    while( (done==false) && (running==true) )
    {
        status = skiq_receive(card, &hdl, &p_rx_block, &len);
        if( skiq_rx_status_success == status )
        {
            if( hdl != skiq_rx_hdl_A1 )
            {
                printf("Error: received unexpected data from hdl %u\n", hdl);
            }
            // check the timestamp if it isn't the first block
            curr_timestamp = p_rx_block->rf_timestamp; /* peek at timestamp */
            if( first_timestamp == true )
            {
                first_timestamp = false;
                next_timestamp = curr_timestamp;
            }
            else
            {
                if( curr_timestamp != next_timestamp )
                {
                    printf("Error: timestamp error...expected 0x%016" PRIx64 " but got 0x%016" PRIx64 "\n",
                           next_timestamp, curr_timestamp);
                }
            }
            next_timestamp += payload_size;

            // if the next block has our loopback data, start storing
            if( next_timestamp >= start_ts )
            {
                // our first write
                if( p_next_rx_write == p_rx_iq )
                {
                    bool bFound=false;
                    uint32_t *p_tmp;
                    uint32_t latency=0;
                    // figure out the offset into the data we need to start at
                    offset = start_ts-curr_timestamp;
                    if( packed == false ) 
                    {
                        memcpy( payload_data,
                                (void *)p_rx_block->data,
                                SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES - SKIQ_RX_HEADER_SIZE_IN_BYTES );
                        p_tmp = payload_data;
                    }
                    else
                    {
                        offset = ((start_ts-curr_timestamp));
                        // unpack the data on the fly
                        unpack_data( (int16_t *)p_rx_block->data,
                                     (int16_t *)(payload_data), 
                                     1356 );
                    }
                    // look for the SOP
                    for( ; (offset<(payload_size-2)) && (bFound==false); offset++ )
                    {
                        p_tmp = payload_data+offset;
                        printf("looking for SOP: 0x%x 0x%x\n", p_tmp[0], p_tmp[1]);
                        if( (p_tmp[0] == 0xF800F800) && (p_tmp[1] == 0xF800F800) )
                        {
                            // need to increment our offset 1 more here
                            offset++;
                            printf("Found SOP at offset %u, latency %u\n", offset, latency);
                            bFound = true;
                        }
                        else
                        {
                            latency++;
                        }
                    }
                    if( bFound == false )
                    {
                        printf("Error: unable to find SOP\n");
                        _exit(-2);
                    }
                    else
                    {
                        printf("Info: latency is %u\n", latency);
                    } 
                    num_bytes = (payload_size-offset)*4;
                    memcpy( p_next_rx_write, 
                            payload_data+offset,
                            num_bytes ); 
                    p_next_rx_write += (num_bytes/4);
                }
                else
                {
                    if( packed == true )
                    {
                        unpack_data( (int16_t *)p_rx_block->data,
                                     (int16_t *)(payload_data), 
                                     payload_size );//1356 );
                    }
                    else
                    {
                        memcpy( payload_data,
                                (void *)p_rx_block->data,
                                payload_size*4 );
                    }
                    num_bytes = (payload_size*4);
                    memcpy( p_next_rx_write, 
                            payload_data,
                            num_bytes );
                    p_next_rx_write += (num_bytes/4);
                }
                tot_num_bytes += num_bytes;
                // just skip the last packet by checking what the total size would be
                if( (tot_num_bytes+num_bytes) >= num_rx_bytes_to_alloc )
                {
                    done = true;
                    printf("Done receiving, tot_bytes %u!\n", tot_num_bytes);
                }
            }
        }
    }

    skiq_stop_rx_streaming(card, skiq_rx_hdl_A1);
}

/*****************************************************************************/
/** The send_samples function is responsible for sending the samples from
    the user specified input file. 

    @param none
    @return: void
*/
static void* send_samples(void* data)
{
    uint32_t num_tx_blocks=0;
    uint32_t num_late=0;

    printf("Info: sending samples\n");

    skiq_start_tx_streaming(card, skiq_tx_hdl_A1);
    for( num_tx_blocks=0; (num_tx_blocks<num_blocks) && (running==true); num_tx_blocks++ )
    {    
        if( ((num_tx_blocks%10) == 0) )
        {
            skiq_read_tx_num_late_timestamps(card, skiq_tx_hdl_A1, &num_late);
            printf("Info: num late timestamps %u before block %u\n", num_late, num_tx_blocks);
        }
        skiq_transmit(card, skiq_tx_hdl_A1, p_tx_blocks[num_tx_blocks], NULL );
    }
    // wait for transmission to complete before stopping (resetting fifo)
    skiq_read_tx_num_late_timestamps(card, skiq_tx_hdl_A1, &num_late);
    printf("Info: num late timestamps %u before sleep\n", num_late);
    sleep(1);
 
    skiq_read_tx_num_late_timestamps(card, skiq_tx_hdl_A1, &num_late);
    printf("Info: num late timestamps %u\n", num_late);

    skiq_stop_tx_streaming(card, skiq_tx_hdl_A1);

    return NULL;
}

void validate_loopback(void)
{
    int16_t *p_rx;
    uint32_t i=0;
    uint32_t j=0;
    int16_t curr_sample=0;
    uint32_t pkt_length=0;
    bool bValid=true;
    uint32_t tx_payload_size;
    uint32_t err_count=0;

    if( packed == true )
    {
        tx_payload_size = NUM_TX_PACKED_PAYLOAD_WORDS_IN_BLOCK;
    }
    else
    {
        tx_payload_size = NUM_TX_PAYLOAD_WORDS_IN_BLOCK;
    }

    p_rx = (int16_t*)(p_rx_iq);
    for( i=0; (i<num_blocks); i++ )
    {
        if( i == 0 )
        {
#if 1
            printf("!!!!!!!!!!!!!skipping 2 pkts!!!!!!!!!\n");
            i+=2;
            p_rx = p_rx + (2*((tx_payload_size+packet_gap-SOP_NUM_PACKETS+1)*2))+SOP_NUM_PACKETS;
            curr_sample = *p_rx;
            pkt_length = tx_payload_size;
#else
            pkt_length = tx_payload_size-SOP_NUM_PACKETS-1; 
#endif
        }
        else if( i == (num_blocks-1) )
        {
            printf("!!!!!!!!!!!!!!skipping last 4 samples\n");
            pkt_length = tx_payload_size-4;
        }
        else
        {
            pkt_length = tx_payload_size;
        }
        for( j=0; j<pkt_length; j++ )
        {
            if( *p_rx != curr_sample )
            {
                printf("Error: Q counter does not match at offset %u (block %u), expected 0x%x, got 0x%x, diff %u\n", 
                       j, i, curr_sample, *p_rx, *(p_rx)-curr_sample);
                curr_sample = *p_rx;
                err_count++;
            }
            p_rx++;
            curr_sample++;
            // deal with the 12-bit wrap
            if( curr_sample == 0x800 )
            {
                // wrap it to -2046 (instead of -2048...which is our SOP)
                curr_sample = -2048;
            }

            if( *p_rx != curr_sample )
            {
                printf("Error: I counter does not match at offset %u (block %u), expected 0x%x, got 0x%x, diff %u\n", 
                       j, i, curr_sample, *p_rx, *(p_rx)-curr_sample);
                curr_sample = *p_rx;
                err_count++;
            }
            p_rx++;
            curr_sample++;

            // deal with the 12-bit wrap
            if( curr_sample == 0x800 )
            {
                // wrap it to -2046 (instead of -2048...which is our SOP)
                curr_sample = -2048;
            }
        }
        p_rx = p_rx + ((packet_gap)*2);
    }

    if( bValid == true )
    {
        printf("!!!!!!!!!!!!!!!!!!!!!WOOT, errors %u!!!!!!!!!!!!\n", err_count);
    }
}

/*****************************************************************************/
/** This function performs sign extension for the 12-bit value passed in.

    @param in: 12-bit value to be sign extended
    @return: sign extended representation of value passed in
*/
int16_t sign_extend( int16_t in )
{
    int16_t out=0;
    
    if( in & 0x800 )
    {
        out = in | 0xF000;
    }
    else
    {
        out = in;
    }

    return out;
}

/*****************************************************************************/
/** This unpacks the sample data, packed as 12-bits and unpacked to 16-bits.

    @param packed_data: pointer to packed data
    @param unpacked_data: pointer to unpacked data
    @param num_unpacked_samples: number of samples contained in sample data
    @return: sign extended representation of value passed in
*/
void unpack_data(int16_t *packed_data, int16_t *unpacked_data, uint32_t num_unpacked_samples )
{
    int16_t i0;
    int16_t q0;
    int16_t i1;
    int16_t q1;
    int16_t i2;
    int16_t q2;
    int16_t q3;
    int16_t i3;

    uint32_t num_samples=0;
    uint32_t packed_offset=0;

    int32_t *data;

    // loop through all the samples and unpack them
    for( num_samples=0; num_samples < (num_unpacked_samples*2); num_samples+=8 )
    {
        data = (int32_t*)(&(packed_data[packed_offset*2]));

        // unpack the data
        i0 = (data[0] & 0xFFF00000) >> 20;
        i0 = sign_extend(i0);
        q0 = (data[0] & 0x000FFF00) >> 8;
        q0 = sign_extend(q0);
        i1 = ((data[0] & 0x000000FF) << 4) | ((data[1] & 0xF0000000)>>28);
        i1 = sign_extend(i1);
        q1 = (data[1] & 0x0FFF0000) >> 16;
        q1 = sign_extend(q1);
        i2 = (data[1] & 0x0000FFF0) >> 4;
        i2 = sign_extend(i2);
        q2 = ((data[1] & 0x0000000F) << 8) | ((data[2] & 0xFF000000) >> 24);
        q2 = sign_extend(q2);
        i3 = ((data[2] & 0x00FFF000) >> 12);
        i3 = sign_extend(i3);
        q3 = (data[2] & 0x00000FFF);
        q3 = sign_extend(q3);

        // save the unpacked data
        unpacked_data[num_samples] = q0;
        unpacked_data[num_samples+1] = i0;
        unpacked_data[num_samples+2] = q1;
        unpacked_data[num_samples+3] = i1;
        unpacked_data[num_samples+4] = q2;
        unpacked_data[num_samples+5] = i2;
        unpacked_data[num_samples+6] = q3;
        unpacked_data[num_samples+7] = i3;

        // every 3 words of data contain 4 samples in packed mode
        packed_offset += 3;
    }
}

void pack_data(int16_t *unpacked_data, int16_t *packed_data, uint32_t num_unpacked_samples)
{
    int32_t sample1;
    int32_t sample2;
    int32_t sample3;
    uint32_t num_samples;

    int32_t *p_data = (int32_t*)(packed_data);

    for( num_samples=0; num_samples < (num_unpacked_samples*2); num_samples++ )
    {
        sample1 = (uint32_t)(((unpacked_data[num_samples+1]) & 0xFFF)) << 20;
        sample1 |= (uint32_t)(((unpacked_data[num_samples]) & 0xFFF)) << 8;
        num_samples+=2;                         
        sample1 |= (uint32_t)(((unpacked_data[num_samples+1]) & 0xFFF)) >> 4;
        *p_data = sample1;
        p_data++;
    
        sample2 = (uint32_t)(((unpacked_data[num_samples+1]) & 0xFFF)) << 28;
        sample2 |= (uint32_t)(((unpacked_data[num_samples]) & 0xFFF)) << 16;
        num_samples+=2;
        sample2 |= (uint32_t)(((unpacked_data[num_samples+1]) & 0xFFF)) << 4;
        sample2 |= (uint32_t)(((unpacked_data[num_samples]) & 0xFFF)) >> 8;
        *p_data = sample2;
        p_data++;
    
        sample3 = (uint32_t)(((unpacked_data[num_samples]) & 0xFFF)) << 24;
        num_samples+=2;
        sample3 |= (uint32_t)(((unpacked_data[num_samples+1]) & 0xFFF)) << 12;
        sample3 |= (uint32_t)(((unpacked_data[num_samples]) & 0xFFF));
        *p_data = sample3;
        p_data++;

        num_samples++;
    }
}

/*****************************************************************************/
/** This function extracts all cmd line args

    @param argc: the # of args to be processed
    @param argv: a pointer to the vector of arg strings
    @return: void
*/
static void process_cmd_line_args(int argc, char* argv[])
{    
    if( argc != 6 )
    {
        printf("Error: invalid # arguments\n");
        print_usage();
        _exit(-1);
    }

#ifdef __MINGW32__
    sscanf(argv[1], "%2" SCNu8, &card);
#else
    sscanf(argv[1], "%hhu", &card);
#endif /* __MINGW32__ */
    sscanf(argv[2], "%u", &sample_rate);
    sscanf(argv[3], "%u", &num_blocks);
    printf("Number of blocks to send is %u\n", num_blocks);
    sscanf(argv[4], "%u", &packet_gap);
    printf("Packet gap is %u\n", packet_gap);

    if ((strcasecmp(argv[5],"packed")) == 0)
    {
        packed = true;
        printf("Info: using packed IQ mode\n");
    }
    else if ((strcasecmp(argv[5],"unpacked")) == 0)
    {
        packed = false;
        printf("Info: using unpacked IQ mode\n");
    }
    else
    {
        printf("Error: invalid packed mode %s\n",argv[10]);
        print_usage();
        exit(-1);
    }
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: loopback_test <card> <sample rate>\n");
    printf("    <number of blocks> <Tx gap in samples> <packed | unpacked>\n");
    printf("Enables loopback and verifies the data transmitted correlates with the data received\n");
}

