/*! \file tx_samples.c
 * \brief This file contains a basic application for transmitting
 * sample data using factory API.
 *
 * <pre>
 * Copyright 2012 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <arg_parser.h>

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- transmit I/Q data";
static const char* p_help_long =
"\
Configure the Tx lineup according to the specified parameters and transmit\n\
the entire contents of a provided file.\n\
The file should contain 16-bit\n\
signed twos-complement little-endian I/Q samples formatted as follows:\n\
\n\
    <16-bit Q0> <16-bit I0> <16-bit Q1> <16-bit I1> ... etc\n\
\n\
\n\
Defaults:\n\
  --card=0\n\
  --frequency=850000000\n\
  --handle=A1\n\
  --duration=1";

/* variables used for all command line arguments */
static uint8_t card = 0;
static char* p_serial = NULL;
static uint64_t lo_freq = 850000000;
static uint32_t duration = 1;
static char* p_file_path = NULL;
static char* p_hdl = "A1";

/* application variables  */
static FILE *input_fp = NULL;
static skiq_tx_hdl_t hdl = skiq_tx_hdl_A1;
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("frequency",
                'f',
                "Frequency to transmit samples at in Hertz",
                "Hz",
                &lo_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("handle",
                0,
                "Tx handle to use, either A1 or A2",
                "Tx",
                &p_hdl,
                STRING_VAR_TYPE),
    APP_ARG_OPT("duration",
                'd',
                "Transmit the file for duration seconds",
                "d",
                &duration,
                UINT32_VAR_TYPE),
    APP_ARG_REQ("source",
                's',
                "Input file to source for I/Q data",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}

/*****************************************************************************/
/** This is the main function for executing the tx_samples app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ASCII string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    int32_t status=0;
    uint32_t num_bytes_in_file=0;
    uint8_t *p_buf = NULL;
    bool skiq_initialized = false;
    pid_t owner = 0;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error : card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    if( 0 == strncasecmp(p_hdl, "A1", 2) )
    {
        hdl = skiq_tx_hdl_A1;
        printf("Info: using Tx handle A1\n");
    }
    else if( 0 == strncasecmp(p_hdl, "A2", 2) )
    {
        hdl = skiq_tx_hdl_A2;
        printf("Info: using Tx handle A2\n");
    }
    else
    {
        fprintf(stderr, "Error: invalid handle specified\n");
        status = -1;
        goto finished;
    }

    input_fp = fopen(p_file_path, "rb");
    if( input_fp == NULL )
    {
        fprintf(stderr, "Error: unable to open input file %s\n", argv[1]);
        status = -1;
        goto finished;
    }

    // determine how large the file is and how many blocks we'll need to send
    fseek(input_fp, 0, SEEK_END);
    num_bytes_in_file = ftell(input_fp);
    rewind(input_fp);
    // setup the sample buffer based on the samples in file
    p_buf = malloc( num_bytes_in_file );
    if (p_buf == NULL)
    {
        fprintf(stderr, "Error: unable to allocate %d bytes\n",
                num_bytes_in_file);
        status = -1;
        goto finished;
    }

    if( fread( p_buf, 1, num_bytes_in_file, input_fp ) != num_bytes_in_file )
    {
        fprintf(stderr, "Error: unable to read in # bytes\n");
        status = -2;
        goto finished;
    }

    fclose(input_fp);
    input_fp = NULL;

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(skiq_xport_type_pcie, skiq_xport_init_level_full,
                &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    // configure our Tx parameters
    status = skiq_write_tx_LO_freq(card, hdl, lo_freq);
    if (status != 0)
    {
        fprintf(stderr, "Error: unable to configure Tx LO frequency (result"
                " code %" PRIi32 ")\n", status);
        goto finished;
    }
    printf("Info: configured Tx LO freq to %" PRIu64 " Hz\n", lo_freq);

    // start sending samples
    printf("Info: enabling factory tx_samples\n");
    status = skiq_fact_enable_tx_samples(card, hdl, (int16_t*)(p_buf),
                num_bytes_in_file / sizeof(int32_t));
    if (status != 0)
    {
        fprintf(stderr, "Error: unable to enable factory tx_samples (result"
                " code %" PRIi32 ")\n", status);
        goto finished;
    }

    while ((duration > 0) && running)
    {
       sleep(1);
       duration--;
    }

    status = 0;

finished:
    if (skiq_initialized)
    {
        // stop sending samples
        printf("Info: disabling factory tx_samples\n");
        skiq_fact_disable_tx_samples( card, hdl );

        skiq_exit();

        skiq_initialized = false;
    }

    if (NULL != input_fp)
    {
        fclose(input_fp);
        input_fp = NULL;
    }

    // deallocate memory
    if (NULL != p_buf)
    {
        free(p_buf);
        p_buf = NULL;
    }

    return (status);
}

