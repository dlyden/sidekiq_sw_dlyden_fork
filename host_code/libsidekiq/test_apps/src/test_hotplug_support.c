/*! \file test_hotplug_support.c
 * \brief This file contains test automation for removing
 * and adding Sidekiq cards to a host machine.
 *  
 * <pre>
 * Copyright 2014-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <fcntl.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>

#include <dma_interface_api.h>
#include <sidekiq_api.h>
#include <arg_parser.h>

#include "pci_manager.h"
#include "sidekiq_card_mgr.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

/* these defines will be used to build up the default card list that will
   be used to test the hotplug feature */
#ifndef DEFAULT_CARD_0
#   define DEFAULT_CARD_0  0
#endif

#ifndef DEFAULT_CARD_1
#   define DEFAULT_CARD_1  1
#endif

#ifndef DEFAULT_CARD_2
#   define DEFAULT_CARD_2  2
#endif

#ifndef DEFAULT_CARD_LIST
#   define DEFAULT_CARD_LIST {DEFAULT_CARD_0,DEFAULT_CARD_1,DEFAULT_CARD_2}
#endif

/* this is the maximum number of cards to be used for this test */
#define MAX_NUM_TEST_CARDS  3

/** @brief A simple macro for comparing driver version (major, minor, patch) */
#ifndef DRIVER_VERSION
#   define DRIVER_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))
#endif

// PCI Manager Version Requirement
#define PCI_MANAGER_MAJ_REQ (1)
#define PCI_MANAGER_MIN_REQ (2)
#define PCI_MANAGER_SUBMINOR_REQ (0)

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "tests card hotplugging";
static const char* p_help_long = "\
For the supplied card list, perform a receive benchmark during\n\
PCI remove/rescan events to validate hotplugging functionality.\n\
\n\
Note: cards inputted into the test must be in ascending order.\n\
\n\
Defaults:\n\
  --cardlist={" xstr(DEFAULT_CARD_0) "," xstr(DEFAULT_CARD_1) "," xstr(DEFAULT_CARD_2) "}";

static uint8_t cardlist[MAX_NUM_TEST_CARDS] = DEFAULT_CARD_LIST;

/* command line argument variables */
static uint8_t cards[MAX_NUM_TEST_CARDS] = DEFAULT_CARD_LIST;

static bool cards_present[MAX_NUM_TEST_CARDS] = {false, false, false};

static pci_manager_bus_t bus_mgr[MAX_NUM_TEST_CARDS] = {
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0}
};

/* global variables shared amongst threads */
static pthread_t receiving_thread;
static pthread_mutex_t thread_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t thread_started = PTHREAD_COND_INITIALIZER;

volatile bool running_receive = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT_PRESENT("card-0",
                0,
                "Specify Sidekiq card 0 by card index",
                "ID",
                &cards[0],
                UINT8_VAR_TYPE,
                &cards_present[0]),
    APP_ARG_OPT_PRESENT("card-1",
                0,
                "Specify Sidekiq card 1 by card index",
                "ID",
                &cards[1],
                UINT8_VAR_TYPE,
                &cards_present[1]),
    APP_ARG_OPT_PRESENT("card-2",
                0,
                "Specify Sidekiq card 2 by card index",
                "ID",
                &cards[2],
                UINT8_VAR_TYPE,
                &cards_present[2]),
    APP_ARG_TERMINATOR,
};


/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    running_receive = false; // clears the running flag so that everything exits
}

/*****************************************************************************/
/** This function verifies the cards in the given card list.

    @param none
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
static int32_t verify_cards(void)
{
    int32_t status = 0;
    int i;

    /* Check to make sure any and all cards that were specified are valid */
    for(i = 0; i < MAX_NUM_TEST_CARDS; i++)
    {
        if(cards_present[i])
        {
            if(cards[i] >= SKIQ_MAX_NUM_CARDS)
            {
                fprintf(stderr, "Error: card-%d ID %" PRIu8 " exceeds the maximum card ID"
                        " (%" PRIu8 ")\n", i, cards[i], (SKIQ_MAX_NUM_CARDS - 1));
                status = -EINVAL;
                goto finished;
            }
            else
            {
                cardlist[i] = cards[i];
            }
        }
    }

    /* Check to make sure that there are no duplicate cards in the card list */
    if(cardlist[0] == cardlist[1] ||
       cardlist[0] == cardlist[2] ||
       cardlist[1] == cardlist[2] )
    {
        fprintf(stderr, "Error: unable to use duplicate cards in this test\n");
        status = -EINVAL;
        goto finished;
    }
    
    

finished:
    return status;
}

/*****************************************************************************/
/** This function creates the pci bus manager entries from the user input.

    @param none
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
static int32_t create_bus_entries(void)
{
    int32_t status = 0;
    int i;

    for(i = 0; i < MAX_NUM_TEST_CARDS; i++)
    {
        status = DmaInterface_open(cardlist[i]);
        if(status != 0)
        {
            fprintf(stderr, "Unable to open the DmaInterface for card: %u\n", cardlist[i]);
            break;
        }
        status = DmaInterface_get_pci_dev_info(cardlist[i], &bus_mgr[i].pci_bus, &bus_mgr[i].pci_devfn, &bus_mgr[i].pci_domain);
        if(status != 0)
        {
            fprintf(stderr, "Unable to get the pci info from the DmaInterface for card: %u\n", cardlist[i]);
            break;
        }
        status = DmaInterface_close(cardlist[i]);
        if(status != 0)
        {
            fprintf(stderr, "Unable to close the DmaInterface for card: %u\n", cardlist[i]);
            break;
        }
    }

    return status;
}

static int32_t open_and_validate_pci_manager(int *p_fd)
{
    int32_t status = 0;
    pci_manager_vers_t vers = {
        .maj_vers = 0,
        .min_vers = 0,
        .patch_vers = 0,
    };

    /* open the PCI manager file to do the ioctl calls */
    errno = 0;
    *p_fd = open(PCI_MANAGER_FULLPATH, O_RDWR);
    if (*p_fd < 0)
    {
        fprintf(stderr, "Error: unable to access " PCI_MANAGER_FILENAME " module with status=%d\n", errno);
        return -errno;
    }

    /* get the driver version and ensure it meets the minimum requirement */
    if (ioctl(*p_fd, PCI_MANAGER_VERSION, &vers) == -1)
    {
        fprintf(stderr, "Error: unable to determine " PCI_MANAGER_FILENAME " module version with status=%d\n", errno);
        status = -errno;
    }

    if ((status == 0) &&
        (DRIVER_VERSION(vers.maj_vers, vers.min_vers, vers.patch_vers) <
         DRIVER_VERSION(PCI_MANAGER_MAJ_REQ, PCI_MANAGER_MIN_REQ, PCI_MANAGER_SUBMINOR_REQ)))
    {
        fprintf(stderr, "Error: " PCI_MANAGER_FILENAME " version (%d) does not meet minimum requirement (%d)\n",
                DRIVER_VERSION(vers.maj_vers, vers.min_vers, vers.patch_vers),
                DRIVER_VERSION(PCI_MANAGER_MAJ_REQ, PCI_MANAGER_MIN_REQ, PCI_MANAGER_SUBMINOR_REQ));
        status = -ERANGE;
    }

    /* close file descriptor if there was an error */
    if (status != 0)
    {
        close(*p_fd);
    }

    return status;
}


/*****************************************************************************/
/** This function initializes the card under benchmark.

    @param receiving_card: the card that will be used in the receiving thread
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
static int32_t init_receiving_card(uint8_t receiving_card)
{
    int32_t status = 0;
    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_full;
    pid_t owner = 0;
    
    printf("Info: initializing card %" PRIu8 "...\n", receiving_card);

    status = skiq_init(type, level, &receiving_card, 1);
    if(status != 0)
    {
        if(EBUSY == status)
        {
            if(0 != skiq_is_card_avail(receiving_card, &owner))
            {
                fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID"
                        " %u); cannot initialize card.\n", receiving_card,
                        (unsigned int) owner);
            }
        }
        else if (-EINVAL == status)
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }
    }

    return status;
}

/*****************************************************************************/
/** This function probes the available cards and lists the cards in use
 *  and locked.

    @param none
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
static int32_t probe_and_verify(uint8_t* locked_cards_exp, uint8_t* unlocked_cards_exp,
                                uint8_t num_locked, uint8_t num_unlocked)
{
    int32_t status = 0;
    uint8_t locked_cards[MAX_NUM_TEST_CARDS];
    uint8_t unlocked_cards[MAX_NUM_TEST_CARDS];
    uint8_t all_cards[SKIQ_MAX_NUM_CARDS];
    uint8_t num_all_cards = 0;
    int8_t i = 0, j = 0, k = 0;
    pid_t owner = 0;

    status = skiq_get_cards(skiq_xport_type_auto, &num_all_cards, all_cards);
    if(status != 0)
    {
        return status;
    }
    for(i = 0; i < num_all_cards; i++)
    {
        if(skiq_is_card_avail(all_cards[i], &owner) != 0)
        {
            printf("Info: card %u is currently locked by another process"
                        " (PID=%u)\n", all_cards[i], (unsigned int)(owner));
            locked_cards[j++] = all_cards[i];
        }
        else
        {
            printf("Info: card %u is connected and available\n", all_cards[i]);
            unlocked_cards[k++] = all_cards[i];
        }
    }
    printf("\n");

    if(j < num_locked)
    {
        skiq_error("There are not enough locked cards");
        status = -ENODEV;
        return status;
    }
    // verify that the correct locked cards are in the locked cards array
    for(i = 0; i < j; i++)
    {
	if(!(locked_cards[i] == locked_cards_exp[i]))
        {
            skiq_error("The expected cards are not locked");
            status = -ENODEV;
            break;
        }
    }

    if(status == 0)
    {
        if(k < num_unlocked)
        {
            skiq_error("There are not enough unlocked cards");
            status = -ENODEV;
            return status;
        }
        // verify that the correct unlocked cards are in the unlocked cards array
        for(i = 0; i < k; i++)
        {
            if(!(unlocked_cards[i] == unlocked_cards_exp[i]))
            {
                skiq_error("The expected cards are not unlocked");
                status = -ENODEV;
                break;
            }
        }
    }

    return status;
}

/*****************************************************************************/
/** This function updates the pci bus using the given ioctl command, probes
 *  the available cards and lists the cards in use and locked, and verifies
 *  the two lists.

    @param fd: pci manager file descriptor
    @param pci_entry: pci entry that will be re-added to the bus
    @param ioctl_cmd: the integer value of the ioctl command to call
    @param locked_cards_exp: array of expected locked cards
    @param unlocked_cards_exp: array of expected unlocked cards
    @param num_locked: the number of expected locked cards
    @param num_unlocked: the number of expected unlocked cards
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
int32_t update_bus_and_verify(int32_t* p_fd, pci_manager_bus_t* p_pci_entry, int32_t ioctl_cmd,
                              uint8_t* locked_cards_exp, uint8_t* unlocked_cards_exp,
                              uint8_t num_locked, uint8_t num_unlocked)
{
    int32_t status = 0;

    status = ioctl(*p_fd, ioctl_cmd, p_pci_entry);
    if(status == -1)
    {
        skiq_error("Failed to update the pci bus");
        return status;
    }

    status = probe_and_verify(locked_cards_exp, unlocked_cards_exp,
                              num_locked, num_unlocked);
    if(status != 0)
    {
        return status;
    }

    return status;
}


/*****************************************************************************/
/** This is a separate thread that locks a card to be used during hotplug
 * testing.

    @param card: the card that will be used in the receive thread
    @return none
*/
void* receive_utility(void *receive_card)
{
    static skiq_rx_hdl_t handle = skiq_rx_hdl_A1;
    skiq_rx_block_t* p_rx_block;
    uint32_t data_len;
    uint8_t card = 0;
    if(receive_card == NULL)
    {
        skiq_error("No card was used when creating the receive thread");
        running_receive = false;
        return NULL;
    }
    else
    {
        card = *((uint8_t *)receive_card);
    }

    printf("Info: starting RxA1 interface on card %u\n", card);
    if(skiq_start_rx_streaming(card, handle) != 0)
    {
        skiq_error("Unable to start streaming on receiving thread");
        running_receive = false;
        return NULL;
    }
    pthread_mutex_lock(&thread_mutex);
    pthread_cond_broadcast(&thread_started);
    pthread_mutex_unlock(&thread_mutex);

    /* run until the running_receive flag gets cleared */
    while(running_receive)
    {
	    skiq_receive(card, &handle, &p_rx_block, &data_len);
    }

    /* stop receive streaming */
    printf("Info: stopping RxA1 interface(s) on card %u\n", card);
    skiq_stop_rx_streaming(card, handle);

    return NULL;
}

/*****************************************************************************/
/** This initializes the card that will be used in the receiving thread and
 * makes sure that the thread starts before moving on in execution.

    @param card: the card that will be used in the receive thread
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
static int32_t start_receive_thread(uint8_t card)
{
    int32_t status = 0;
    running_receive = true;

    status = init_receiving_card(card);
    if(status != 0)
    {
        return status;
    }

    /* initialize a thread to start the receive utility */
    status = pthread_create(&receiving_thread, NULL, receive_utility, &card);
    if(status != 0)
    {
        fprintf(stderr, "Error: unable to create receive thread (status: %d)\n", status);
        return status;
    }

    struct timespec ts = {0, 0};
    errno = 0;
    status = clock_gettime(CLOCK_REALTIME, &ts);
    if(status == 0)
    {
        ts.tv_sec += 1;
    }
    else
    {
        fprintf(stderr, "Error: failed to get current time (%d: '%s')\n", errno, strerror(errno));
        return status;
    }
    pthread_mutex_lock(&thread_mutex);
    status = pthread_cond_timedwait(&thread_started, &thread_mutex, &ts);
    pthread_mutex_unlock(&thread_mutex);

    if(status == ETIMEDOUT)
    {
        fprintf(stderr, "Error: timed out waiting for receive thread to start\n");
    }

    if(!running_receive)
    {
        fprintf(stderr, "Error: Receive thread exited early\n");
        status = -EIO;
    }

    return status;
}



/*****************************************************************************/
/** This is the main function for executing the test_hotplug_support app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string arguments from the cmd line
    @return status code that indicated the successfulness of the function
    (0 is success, anything else is an error)
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    int fd = -1;
    uint8_t locked_cards_expected[MAX_NUM_TEST_CARDS];
    uint8_t unlocked_cards_expected[MAX_NUM_TEST_CARDS];
    uint8_t num_locked = 0;
    uint8_t num_unlocked = 0;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    if((status = arg_parser(argc, argv, p_help_short, p_help_long, p_args)) != 0)
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        goto sidekiq_exit;
    }

    status = verify_cards();
    if(status != 0)
    {
        goto sidekiq_exit;
    }

    status = create_bus_entries();
    if(status != 0)
    {
        goto sidekiq_exit;
    }

    /* open PCI Manager and validate version */
    status = open_and_validate_pci_manager(&fd);
    if (status != 0)
    {
        goto sidekiq_exit;
    }

    if(!running_receive)
    {
        goto sidekiq_exit;
    }

    /* In the test setups below all connected entries refer to the
       location of the card in cardlist and not the card ID */

    /* Connected: 0, 1, 2
       Card Locked by Receive: N/A
       Test: card 2 is removed and a probe is completed resulting
             in cards 0 and 1 visible. */

    printf("Info: Card 2 is being removed from the PCI bus\n");
    unlocked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[1] = cardlist[1];
    num_unlocked = 2;
    status = update_bus_and_verify(&fd, &bus_mgr[2],
                                    PCI_MANAGER_STOP_AND_REMOVE_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to remove card from PCI bus with status: %" PRIi32 "\n",  status);
        goto sidekiq_exit;
    }

    /* Connected: 0, 1
       Card Locked by Receive: N/A
       Test: card 2 is connected and a probe is completed resulting
             in cards 0, 1, and 2 visible. */

    printf("Info: Card 2 is being reconnected to the PCI bus\n");
    unlocked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[1] = cardlist[1];
    unlocked_cards_expected[2] = cardlist[2];
    num_unlocked = 3;
    status = update_bus_and_verify(&fd, &bus_mgr[2],
                                    PCI_MANAGER_FORCE_RESCAN_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to rescan PCI bus with status: %" PRIi32 "\n",  status);
        goto sidekiq_exit;
    }

    /* Connected: 0, 1, 2
       Card Locked by Receive: N/A
       Test: card 1 is removed and a probe is completed resulting
             in cards 0 and 2 visible. */

    printf("Card 1 is being removed from the PCI bus\n");
    unlocked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[1] = cardlist[2];
    num_unlocked = 2;
    status = update_bus_and_verify(&fd, &bus_mgr[1],
                                    PCI_MANAGER_STOP_AND_REMOVE_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to remove card from PCI bus with status: %" PRIi32 "\n",  status);
        goto sidekiq_exit;
    }

    /* Connected: 0, 2
       Card Locked by Receive: N/A
       Test: card 1 is connected and a probe is completed resulting
             in cards 0, 1, and 2 visible. */

    printf("Card 1 is being reconnected to the PCI bus\n");
    unlocked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[1] = cardlist[1];
    unlocked_cards_expected[2] = cardlist[2];
    num_unlocked = 3;
    status = update_bus_and_verify(&fd, &bus_mgr[1],
                                    PCI_MANAGER_FORCE_RESCAN_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to rescan PCI bus with status: %" PRIi32 "\n",  status);
        goto sidekiq_exit;
    }

    /* Connected: 0, 1, 2
       Card Locked by Receive: 0
       Test: card 2 is removed and a probe is completed resulting
             in cards 0 and 1 visible with card 0 being locked. */
    
    status = start_receive_thread(cardlist[0]);
    if(status != 0)
    {
        goto sidekiq_exit;
    }

    printf("Card 2 is being removed from the PCI bus\n");
    locked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[0] = cardlist[1];
    num_locked = 1;
    num_unlocked = 1;
    status = update_bus_and_verify(&fd, &bus_mgr[2],
                                    PCI_MANAGER_STOP_AND_REMOVE_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to remove card from PCI bus with status: %" PRIi32 "\n",  status);
        goto stop_thread_and_exit;
    }

    /* Connected: 0, 1
       Card Locked by Receive: 0
       Test: card 2 is connected and a probe is completed resulting
             in cards 0, 1, and 2 visible with card 0 being locked. */

    printf("Card 2 is being reconnected to the PCI bus\n");
    locked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[0] = cardlist[1];
    unlocked_cards_expected[1] = cardlist[2];
    num_unlocked = 2;
    status = update_bus_and_verify(&fd, &bus_mgr[2],
                                    PCI_MANAGER_FORCE_RESCAN_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to rescan PCI bus with status: %" PRIi32 "\n",  status);
        goto stop_thread_and_exit;
    }

    /* Connected: 0, 1, 2
       Card Locked by Receive: 0
       Test: card 1 is removed and a probe is completed resulting
             in cards 0 and 2 visible with card 0 being locked. */

    printf("Card 1 is being removed from the PCI bus\n");
    locked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[0] = cardlist[2];
    num_unlocked = 1;
    status = update_bus_and_verify(&fd, &bus_mgr[1],
                                    PCI_MANAGER_STOP_AND_REMOVE_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to remove card from PCI bus with status: %" PRIi32 "\n",  status);
        goto stop_thread_and_exit;
    }

    /* Connected: 0, 2
       Card Locked by Receive: 0
       Test: card 1 is connected and a probe is completed resulting
             in cards 0, 1, and 2 visible with card 0 being locked. */

    printf("Card 1 is being reconnected to the PCI bus\n");
    locked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[0] = cardlist[1];
    unlocked_cards_expected[1] = cardlist[2];
    num_unlocked = 2;
    status = update_bus_and_verify(&fd, &bus_mgr[1],
                                    PCI_MANAGER_FORCE_RESCAN_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to rescan PCI bus with status: %" PRIi32 "\n",  status);
        goto stop_thread_and_exit;
    }

    /* Connected: 0, 1, 2
       Card Locked by Receive: N/A
       Test: card 0 is no longer locked and a probe is completed
             resulting in cards 0, 1, and 2 visible. */

    running_receive = false;
    
    /* wait for the monitor thread to complete */
    pthread_join(receiving_thread, NULL);
    skiq_exit();
    unlocked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[1] = cardlist[1];
    unlocked_cards_expected[2] = cardlist[2];
    num_locked = 0;
    num_unlocked = 3;
    status = probe_and_verify(locked_cards_expected, unlocked_cards_expected,
                              num_locked, num_unlocked);
    if(status != 0)
    {
        goto sidekiq_exit;
    }

    /* Connected: 0, 1, 2
       Card Locked by Receive: 1
       Test: card 0 is removed and a probe is completed resulting
             in cards 1 and 2 visible with card 1 being locked. */

    status = start_receive_thread(cardlist[1]);
    if(status != 0)
    {
        goto sidekiq_exit;
    }

    printf("Card 0 is being removed from the PCI bus\n");
    locked_cards_expected[0] = cardlist[1];
    unlocked_cards_expected[0] = cardlist[2];
    num_locked = 1;
    num_unlocked = 1;
    status = update_bus_and_verify(&fd, &bus_mgr[0],
                                    PCI_MANAGER_STOP_AND_REMOVE_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to remove card from PCI bus with status: %" PRIi32 "\n",  status);
        goto stop_thread_and_exit;
    }

    /* Connected: 0, 2
       Card Locked by Receive: 1
       Test: card 0 is connected and a probe is completed resulting
             in cards 0, 1, and 2 visible with card 1 being locked. */

    printf("Card 0 is being reconnected to the PCI bus\n");
    locked_cards_expected[0] = cardlist[1];
    unlocked_cards_expected[0] = cardlist[0];
    unlocked_cards_expected[1] = cardlist[2];
    num_unlocked = 2;
    status = update_bus_and_verify(&fd, &bus_mgr[0],
                                    PCI_MANAGER_FORCE_RESCAN_IOCTL,
                                    locked_cards_expected,
                                    unlocked_cards_expected,
                                    num_locked,
                                    num_unlocked);
    if(status != 0)
    {
        fprintf(stderr, "Error: Unable to rescan PCI bus with status: %" PRIi32 "\n",  status);
        goto stop_thread_and_exit;
    }

    /* Connected: 0, 1, 2
       Card Locked by Receive: N/A
       Test: card 1 is no longer locked and a probe is completed
             resulting in cards 0, 1, and 2 visible. */

stop_thread_and_exit:
    running_receive = false;
    
    /* wait for the monitor thread to complete */
    pthread_join(receiving_thread, NULL);
    skiq_exit();
    if(status == 0)
    {
        unlocked_cards_expected[0] = cardlist[0];
        unlocked_cards_expected[1] = cardlist[1];
        unlocked_cards_expected[2] = cardlist[2];
        num_locked = 0;
        num_unlocked = 3;
        status = probe_and_verify(locked_cards_expected, unlocked_cards_expected,
                                  num_locked, num_unlocked);
    }

sidekiq_exit:
    errno = 0;
    if(fd >= 0)
    {
        close(fd);
        if(errno != EBADF && errno != 0)
        {
            skiq_warning("The PCI bus manager file descriptor was not closed successfully,"
                            "(errno: %d)\n", errno);
        }
    }
    return status;
}

