/*! \file prog_fpga_and_receive.c
 * \brief This file programs the FPGA with a random bitstream
 * and receives samples in either randomly selected stream mode.
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <arg_parser.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#ifndef DEFAULT_ITERATIONS
#   define DEFAULT_ITERATIONS  10
#endif

#ifndef DEFAULT_NUM_RECEIVE_BLOCKS
#   define DEFAULT_NUM_RECEIVE_BLOCKS  100
#endif

#ifndef DEFAULT_RECEIVE_ITERATIONS
#   define DEFAULT_RECEIVE_ITERATIONS 3
#endif

#ifndef MAX_NUM_BITSTREAMS
#   define MAX_NUM_BITSTREAMS (100) // completely arbitrary size
#endif

#define BITSTREAM_DELIM (",")

/* these are used to provide help strings for the application when running t
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- program the FPGA and receive";
static const char* p_help_long =
"\
Program a randomly selected bitstream and receive samples using \n\
a randomly selected RX stream mode. \n\
\n\
Defaults:\n\
    --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
    --iterations=" xstr(DEFAULT_ITERATIONS) "\n\
    --num_receive_blocks=" xstr(DEFAULT_NUM_RECEIVE_BLOCKS) "\n\
    --receive_iterations=" xstr(DEFAULT_RECEIVE_ITERATIONS) "\n\
";

/* Command Line Argument Variables */
/* program the FPGA using bitstream stored in external flash memory */
static bool b_use_flash = false;
/* sidekiq card number */
static uint8_t card = UINT8_MAX;
/* Sidekiq serial number */
static char* p_serial = NULL;
/* bitstream list */
static char* p_bitstreams = NULL;
/* number of iterations to repeat full test */
static uint32_t num_iterations = DEFAULT_ITERATIONS;
/* sample rate for rx_stream_modes */
static uint32_t sample_rate[skiq_rx_stream_mode_end] =
{ [ 0 ... (skiq_rx_stream_mode_end-1) ] = UINT32_MAX };
/* number of blocks of samples to receive for each receive call */
static uint32_t num_receive_blocks = DEFAULT_NUM_RECEIVE_BLOCKS;
/* number of receive iterations to run */
static uint32_t num_receive_iterations = DEFAULT_RECEIVE_ITERATIONS;
/* flag to verify counter data...not all bitstreams support this */
static bool b_verify_counter = false;
/* Log all output (including noisy debug level) */
static bool verbose = false;

static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("bitstreams",
                'b',
                "Comma separated list of FPGA bitstreams to program",
                "PATH",
                &p_bitstreams,
                STRING_VAR_TYPE),
    APP_ARG_OPT("iterations",
                'i',
                "Number of iterations to run",
                "N",
                &num_iterations,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("flash",
                0,
                "Use bitstream file stored in flash memory",
                NULL,
                &b_use_flash,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verify",
                0,
                "Verify counter data of samples received",
                NULL,
                &b_verify_counter,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("sample-rate-high-tput",
                0,
                "Sample rate for skiq_rx_stream_mode_high_tput (in Hertz)",
                "Hz",
                &(sample_rate[skiq_rx_stream_mode_high_tput]),
                UINT32_VAR_TYPE),
    APP_ARG_OPT("sample-rate-low-latency",
                0,
                "Sample rate for skiq_rx_stream_mode_low_latency (in Hertz)",
                "Hz",
                &(sample_rate[skiq_rx_stream_mode_low_latency]),
                UINT32_VAR_TYPE),
    APP_ARG_OPT("sample-rate-balanced",
                0,
                "Sample rate for skiq_rx_stream_mode_balanced (in Hertz)",
                "Hz",
                &(sample_rate[skiq_rx_stream_mode_balanced]),
                UINT32_VAR_TYPE),
    APP_ARG_OPT("num-receive-blocks",
                0,
                "Number of blocks to receive for each receive configuration",
                NULL,
                &num_receive_blocks,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("receive-iterations",
                0,
                "Number of receive iterations to complete prior to reprogramming FPGA",
                NULL,
                &num_receive_iterations,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Enable logging from libsidekiq to stdout",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),

    APP_ARG_TERMINATOR,
};

/* translate from enum to string */
static const char *
fifo_cstr( skiq_fpga_tx_fifo_size_t fifo )
{
    const char* p_fifo_str =
        (fifo == skiq_fpga_tx_fifo_size_4k) ? "4k samples" :
        (fifo == skiq_fpga_tx_fifo_size_8k) ? "8k samples" :
        (fifo == skiq_fpga_tx_fifo_size_16k) ? "16k samples" :
        (fifo == skiq_fpga_tx_fifo_size_32k) ? "32k samples" :
        (fifo == skiq_fpga_tx_fifo_size_64k) ? "64k samples" :
        "unknown";

    return p_fifo_str;
}

static void
log_msg( int32_t priority, const char* message )
{
    bool log_msg=true;
    if( (verbose==false) && (priority>SKIQ_LOG_INFO) )
    {
        log_msg = false;
    }
    if( log_msg == true )
    {
        printf("%s", message);
    }
}

#if (defined __MINGW32__)
/* Unfortunately Mingw does not support sigset_t, so define a fake type definition and stub out the
   related functions. */
typedef int sigset_t;
static inline int mask_signals(sigset_t *old_mask) { return 0; }
static inline int unmask_signals(sigset_t *old_mask) { return 0; }
static inline bool shutdown_signal_pending(void) { return false; }
#else
/**
    @brief  Mask (block) the SIGINT and SIGTERM signals during critical
            operations

    @param[out]  old_mask   The original signal mask, needed by
                            ::unmask_signals().

    @note   Pending signals will be raised when ::unmask_signals() is called.

    @return 0 on success, else an errno.
*/
static int
mask_signals(sigset_t *old_mask)
{
    int result = 0;
    sigset_t new_mask;

    sigemptyset(&new_mask);
    sigaddset(&new_mask, SIGINT);
    sigaddset(&new_mask, SIGTERM);

    errno = 0;
    result = pthread_sigmask(SIG_BLOCK, &new_mask, old_mask);

    return (0 == result) ? result : errno;
}

/**
    @brief  Unmask (unblock) the signals blocked by ::mask_signals().

    @param[in]  old_mask    The original signal mask, as generated by
                            ::mask_signals().

    @note   Pending signals will be processed after calling this function.

    @return 0 on success, else an errno.
*/
static int
unmask_signals(sigset_t *old_mask)
{
    int result = 0;

    errno = 0;
    result = pthread_sigmask(SIG_SETMASK, old_mask, NULL);

    return (0 == result) ? result : errno;
}

/**
    @brief  Check if a shutdown signal (SIGINT or SIGTERM) is pending (while
            masked).

    @return true if a signal is pending, else false.
*/
static bool
shutdown_signal_pending(void)
{
    int result = 0;
    sigset_t pending_mask;
    bool pending = false;

    errno = 0;
    result = sigpending(&pending_mask);
    if (0 == result)
    {
        pending = ((1 == sigismember(&pending_mask, SIGINT)) ||
                   (1 == sigismember(&pending_mask, SIGTERM)));
    }
    else
    {
        printf("Debug: sigpending() failed (%d: '%s')\n",
                errno, strerror(errno));
    }

    return pending;
}
#endif	/* __MINGW32__ */

static void
verify_data( int16_t* p_data,
             uint32_t num_samps )
{
    uint32_t offset=0;
    int16_t last_data=0;
    uint8_t rx_resolution;
    int16_t max_data=0;

    skiq_read_rx_iq_resolution( card, &rx_resolution );
    max_data = (1 << (rx_resolution-1))-1;

    printf("Info: verifying data contents, num_samps %u (RX resolution %u max"
            " ADC value %d)...\n", num_samps, rx_resolution, max_data);

    last_data = p_data[offset]+1;
    offset++;
    for( ; offset<(num_samps*2); offset++ )
    {
        if( last_data != p_data[offset] )
        {
            printf("Error: at sample %u, expected 0x%x but got 0x%x\n",
                   offset, (uint16_t)last_data, (uint16_t)p_data[offset]);
            skiq_exit();
            exit(-1);
        }
        last_data = p_data[offset]+1;
        /* see if we need to wrap the data */
        if( last_data == (max_data+1) )
        {
            last_data = -(max_data+1);
        }
    }
    printf("done\n");
    printf("-------------------------\n");
}


/**
    @brief  Local function to program the FPGA

    @param[in]  card_id     The libsidekiq card number to use
    @param[in]  use_flash   If true, the Sidekiq should be programmed from the
                            bitstream currently in flash memory
    @param[in]  slot_idx    The flash config slot index (when use_flash is true)
    @param[in]  p_bitfile   File pointer of the bitstream to program

    @return 0 on success, else a negative errno; see error codes from
            skiq_prog_fpga_from_file() and skiq_prog_fpga_from_flash().
    @retval -EBADF  if the file specified by p_bitfile cannot be opened
*/
static int32_t
program_fpga(uint8_t card_id, bool use_flash, uint8_t slot_idx, FILE *p_bitfile)
{
    int32_t status = 0;

    /* Choose how to reprogram the FPGA */
    if( use_flash == false )
    {
        /* Reprogram the FPGA from a file */
        printf("Info: programming FPGA from file for card %u...\n", card_id);
        status = skiq_prog_fpga_from_file(card_id, p_bitfile);
    }
    else
    {
        /* save the bitstream to flash */
        printf("Info: saving FPGA to flash for card %u...\n", card_id);
        status = skiq_save_fpga_config_to_flash_slot( card,
                                                      slot_idx,
                                                      p_bitfile,
                                                      0 /* TODO: metadata? */ );

        /* Reprogram the FPGA from the card's on-board flash memory. */
        if ( status == 0 )
        {
            printf("Info: programming FPGA from flash for card %u (config slot %u)...\n",
                   card_id, slot_idx);
            status = skiq_prog_fpga_from_flash_slot(card_id, slot_idx);
        }
    }

    return status;
}

static int32_t
receive_samples(uint8_t card_id, skiq_rx_stream_mode_t mode,
                uint32_t rx_sample_rate, uint32_t num_blocks)
{
    int32_t status=0;
    int32_t block_size=0;
    skiq_rx_block_t* p_rx_block;
    uint32_t len;
    uint32_t* p_rx_data;
    uint32_t* p_rx_data_start;
    uint32_t num_complete_blocks=0;
    skiq_rx_status_t rx_status;
    uint64_t curr_ts=0;
    uint64_t next_ts=0;
    uint64_t first_ts=0;
    skiq_rx_hdl_t curr_rx_hdl=skiq_rx_hdl_A1;

    // always put it in counter mode
    skiq_write_rx_data_src(card_id, curr_rx_hdl, skiq_data_src_counter);
    
    // set the sample rate
    status=skiq_write_rx_sample_rate_and_bandwidth(card_id,
                                                   curr_rx_hdl,
                                                   rx_sample_rate,
                                                   (uint32_t)(rx_sample_rate*0.8));
    if( status == 0 )
    {
        // set the mode
        status = skiq_write_rx_stream_mode( card_id, mode );
        if( status == 0 )
        {
            // get the number of samples per block
            block_size = skiq_read_rx_block_size( card_id, mode );
            if( block_size > 0 )
            {
                // allocate the data to receive
                p_rx_data = (uint32_t*)
                    (calloc((block_size-SKIQ_RX_HEADER_SIZE_IN_BYTES/sizeof(uint32_t))*num_blocks,
                            sizeof(uint32_t)));
                if( p_rx_data != NULL )
                {
                    printf("Info: sample rate %u configured for stream mode %s (card=%u) (block_size=%u)\n",
                           rx_sample_rate, SKIQ_RX_STREAM_MODE_STRINGS[mode], card_id, block_size);
                    p_rx_data_start = p_rx_data;
                    // finally, we're ready to start streaming and receive data
                    status = skiq_start_rx_streaming_multi_immediate(card_id, &curr_rx_hdl, 1);
                    while( (num_complete_blocks <= num_blocks) && (status == 0) )
                    {
                        rx_status = skiq_receive(card_id, &curr_rx_hdl, &p_rx_block, &len);
                        if ( skiq_rx_status_success == rx_status )
                        {
                            if ( NULL != p_rx_block )
                            {
                                /* verify timestamp  */
                                curr_ts = p_rx_block->rf_timestamp;
                                if( first_ts == 0 )
                                {
                                    first_ts = curr_ts;
                                    /*
                                      will be incremented properly below for next time through
                                    */
                                    next_ts = curr_ts;
                                }
                                else if( curr_ts != next_ts )
                                {
                                    printf("Error: timestamp error in block %d for card %u..."
                                           "expected 0x%016" PRIx64 " but got 0x%016" PRIx64
                                           " (delta %" PRId64 ")\n",    \
                                           num_complete_blocks, card_id,
                                           next_ts, curr_ts,
                                           curr_ts - next_ts);
                                    status = -EIO;
                                }

                                if( status == 0 )
                                {
                                    // copy just the sample data to verify counter later
                                    memcpy( p_rx_data,
                                            ((uint32_t*)(p_rx_block->data)),
                                            len-SKIQ_RX_HEADER_SIZE_IN_BYTES );
                                    p_rx_data = p_rx_data + (len-SKIQ_RX_HEADER_SIZE_IN_BYTES)/4;
                                }
                                next_ts = next_ts + (len-SKIQ_RX_HEADER_SIZE_IN_BYTES)/4;
                                num_complete_blocks++;
                            } // end NULL != p_rx_block
                            else
                            {
                                // shouldn't be possible to have NULL for block
                                status = -EPROTO;
                            }
                        } // end receive success
                        else if( rx_status != skiq_rx_status_no_data )
                        {
                            printf("Error: received rx_status error %d\n", rx_status);
                            status = -EIO;
                        }
                    } // end of receiving all data
                    curr_rx_hdl = skiq_rx_hdl_A1;
                    skiq_stop_rx_streaming_multi_immediate(card_id, &curr_rx_hdl, 1);

                    if( (status == 0) && (b_verify_counter==true) )
                    {
                        // verify counter data
                        verify_data( (int16_t*)(p_rx_data_start),
                                     (num_blocks*(block_size-SKIQ_RX_HEADER_SIZE_IN_BYTES))/4 );
                    }

                    // free up the memory previously allocated
                    free( p_rx_data_start );
                } // end p_rx_data != NULL
                else
                {
                    printf("Error: unable to allocate %d bytes (card=%u)\n",
                           block_size*num_blocks, card_id);
                    status = -ENOMEM;
                }
            } // end valid block size
        } // end setting stream mode successfully
        else
        {
            printf("Error: failed to write Rx stream mode to %u for card %u...status is %d\n",
                   mode, card_id, status);
        }
    }
    else
    {
        printf("Error: failed to set Rx sample rate (%u) or bandwidth (%u) for card %u...status is %d\n",
               rx_sample_rate, (uint32_t)(rx_sample_rate*0.8), card_id, status);
    }

    return (status);
}

/*****************************************************************************/
/** This is the main function for programming the FPGA

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status=0;
    int result=0;
    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_full;
    sigset_t old_mask;
    bool masked = false;
    bool skiq_initialized = false;
    pid_t owner = 0;
    char *p_bitstream_name;
    FILE *p_bitstream_files[MAX_NUM_BITSTREAMS];
    uint8_t num_bitstreams=0;
    uint32_t i=0;
    uint32_t j=0;
    uint8_t num_stream_modes=0;
    skiq_rx_stream_mode_t stream_modes[skiq_rx_stream_mode_end] =
        { [ 0 ... (skiq_rx_stream_mode_end-1) ] = skiq_rx_stream_mode_end };

    uint8_t bitstream_index=0;
    uint8_t num_slots=0;
    uint8_t slot_index=0;
    uint32_t stream_index=0;
    skiq_param_t skiq_params;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if( num_iterations == 0 )
    {
        printf("Error: must specify at least one iteration (num_iterations=%u)\n", num_iterations);
        goto finished;
    }
    if( num_receive_blocks == 0 )
    {
        printf("Error: must specify at least one block to receive (num_receive_blocks=%u)\n",
               num_receive_blocks);
        goto finished;
    }
    if( num_receive_iterations == 0 )
    {
        printf("Error: must specify at least one receive iteration (num_receive_iterations=%u)\n",
               num_receive_iterations);
        goto finished;
    }
    

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not both\n\n");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            printf("Error: cannot find card with serial number %s (status=%" PRIi32 ")\n",
                   p_serial, status);
            status = -1;
            goto finished;
        }
        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    // make sure at least one sample rate is specified
    for( i=skiq_rx_stream_mode_high_tput; i<skiq_rx_stream_mode_end; i++ )
    {
        if( sample_rate[i] != UINT32_MAX )
        {
            stream_modes[num_stream_modes] = i;
            printf("Info: receive mode %s is enabled with sample rate %u Hz for card %u\n",
                   SKIQ_RX_STREAM_MODE_STRINGS[i], sample_rate[i], card);
            num_stream_modes++;
        }
    }
    if( num_stream_modes == 0 )
    {
        printf("Error: sample rate for at least one stream mode must be specified\n");
        status = -1;
        goto finished;
    }
    
    // parse the FPGA bitstream list
    p_bitstream_name = strtok( p_bitstreams, BITSTREAM_DELIM );
    while( (p_bitstream_name != NULL) &&
           (status==0) &&
           (num_bitstreams<MAX_NUM_BITSTREAMS) )
    {
        printf("Opening bitstream=%s\n", p_bitstream_name);
        p_bitstream_files[num_bitstreams] =
            fopen( p_bitstream_name, "rb" );
        if( p_bitstream_files[num_bitstreams] != NULL )
        {
            printf("Info: bitstream %s at index %u\n", p_bitstream_name, num_bitstreams);
            num_bitstreams++;
        }
        else
        {
            printf("unable to open bitstream %s (errno=%u)\n",
                   p_bitstream_name, errno);
            status = -errno;
            goto cleanup_files;
        }

        if( status == 0 )
        {
            p_bitstream_name = strtok(NULL, BITSTREAM_DELIM);
        }
    }

    /*
        Ignore the shutdown signals SIGINT and SIGTERM while programming the
        FPGA so the FPGA can't be only partially programmed. Blocking of these
        signals must be done here as skiq_init() can spin up threads, any of
        which can receive & handle these signals; as threads inherit the signal
        mask of the parent on creation, blocking signals now prevents any
        created threads from receiving the shutdown signals.
    */
    result = mask_signals(&old_mask);
    if (0 != result)
    {
        printf("Warning: failed to block shutdown signals (return code %d);"
                " continuing but please do not press Ctrl-C during"
                " programming.\n", result);
    }
    else
    {
        masked = true;
    }
    // register our own logger
    skiq_register_logging( log_msg );
    
    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* Initialize libsidekiq using the specified card. */
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    // determine number of slots available (if programming from flash)
    if( b_use_flash == true )
    {
        status = skiq_read_fpga_config_flash_slots_avail( card, &num_slots );
        if( status == 0 )
        {
            printf("Info: total number of flash slots available is %u for card %u\n",
                   num_slots, card);
        }
    }

    srand(time(NULL));

    for( i=0; (i<num_iterations) && (status==0); i++ )
    {
        if( b_use_flash == true )
        {
            slot_index = rand() % num_slots;
            printf("Programming card %" PRIu8 " FPGA to slot %" PRIu8 "\n",
                   card, slot_index);
        }
        ///////////////////////////////////////////////////////
        // 1) program random bitstream (either to file or randomly selected config slot)
        ////////////////////////////////
        //     a) randomly select bitstream
        //     b) if programming to flash, randomly select config slot
        //     c) program FPGA
        bitstream_index = rand() % num_bitstreams;
        printf("Programming card %" PRIu8 " FPGA with bitstream at %" PRIu8 "\n",
               card, bitstream_index);
        
        /*
          If the signals were successfully masked, check if one of them was
          received and, if so, shut down the program. This check allows the
          user to interrupt the program before doing the uninterruptable work.
        */
        if( ( masked ) && ( shutdown_signal_pending() ) )
        {
            printf("Info: got shutdown signal\n");
            goto finished;
        }

        /* mask signals before programming the FPGA */
        if( !masked )
        {
            (void) mask_signals(&old_mask);
            masked = false;
        }
        status = program_fpga(card,
                              b_use_flash,
                              slot_index, 
                              p_bitstream_files[bitstream_index]);

        /*
          If the signals were successfully masked, unmask them so that the
          shutdown signals operate normally. This is done here so the user can
          interrupt the rest of the program normally (as none of the rest of the
          program has any critical operations).
        */
        if( masked )
        {
            result = unmask_signals(&old_mask);
            if (0 == result)
            {
                masked = false;
            }
        }

        /* After programming, obtain the current FPGA version info */
        if( status != 0 )
        {
            printf("Error: unable to program FPGA with status %d\n", status);
        }
        else
        {
            if( skiq_read_parameters( card, &skiq_params ) == 0 )
            {
                skiq_fpga_param_t *fpga = &(skiq_params.fpga_param);
                
                printf("Current FPGA version for card %" PRIu8 " v%" PRIu8 ".%" PRIu8 ".%" PRIu8
                       ", (date %x/githash 0x%x, FIFO size %s)\n",
                       card, 
                       fpga->version_major, fpga->version_minor, fpga->version_patch,
                       fpga->build_date, fpga->git_hash, fifo_cstr(fpga->tx_fifo_size));
                
                status = 0;
            }
            else
            {
                printf("Error: unable to determine FPGA version (card=%u)!\n", card);
                status = -1;
            }
        }
        ///////////////////////////////////////////////////////
    
        ///////////////////////////////////////////////////////
        // 2) receive samples
        for( j=0; (j<num_receive_iterations) && (status==0); j++ )
        {
            ////////////////////////////////
            //     a) randomly select mode
            stream_index = rand() % num_stream_modes;
            //     b) receive data
            ///////////////////////////////////////////////////////
            status = receive_samples(card, stream_modes[stream_index],
                                     sample_rate[stream_modes[stream_index]], num_receive_blocks);
            if( status != 0 )
            {
                printf("Error: receive_samples failed with status %d\n", status);
            }
        } // repeat for number of receive iterations
        printf("Info: completed iteration %u for card %u (status=%d)\n", i, card, status);
    } // repeat for number of full iterations
    
finished:
    /*
        If the shutdown signals are still masked, restore them; this is done
        as clean up.
    */
    if( masked )
    {
        (void) unmask_signals(&old_mask);
        masked = false;
    }

    /* Cleanup */
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

cleanup_files:
    for( i=0; i<num_bitstreams; i++ )
    {
        printf("closing file at %u\n", i);
        fclose( p_bitstream_files[i] );
    }

    return ((int)(status));
}
