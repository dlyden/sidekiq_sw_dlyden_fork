/*! \file ref_clock.c
 * \brief This file contains the ability to read and write
 * the reference clock configuration.
 *
 * <pre>
 * Copyright 2013 - 2021 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <sidekiq_hal.h>
#include <sidekiq_xport.h>
#include <bit_ops.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_EXT_FREQ
#   define DEFAULT_EXT_FREQ     40000000
#endif

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = \
    "- configures or queries reference clock configuration";
static const char* p_help_long =
"\
Configures or queries the reference clock configurations.  If a reference\n\
clock source is not specified, the current configuration is reported.\n\
\n\
If using an external reference and the frequency is not specified, it\n\
defaults to 40MHz.\n\
\n\
NOTE: Sidekiq mPCIe, M.2, and Z2 can be configured to have an external reference\n\
frequency of 40MHz or 30.72MHz.  Sidekiq M.2-2280 can be configured for 10MHz,\n\
30.72MHz, or 40MHz.  Sidekiq X4 can be configured for 10MHz or 100MHz. Sidekiq\n\
NV100 can be configured for 10MHz or 40MHz. Other products do not support\n\
frequency configuration.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --ext_freq=" xstr(DEFAULT_EXT_FREQ) "\n\
  (no verification)\n\
";

static uint8_t card = DEFAULT_CARD_NUMBER;
static bool card_is_set = false;
static char* p_serial = NULL;
static char* p_clock = NULL;
static uint32_t ext_freq = DEFAULT_EXT_FREQ;
static bool ext_freq_is_set = false;
static bool verify_init = false;

const static uint32_t allowed_freq[] = {
    10000000,
    30720000,
    40000000,
    100000000,
    0,
};

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT_PRESENT("card",
                        'c',
                        "Specify Sidekiq by card index",
                        "ID",
                        &card,
                        UINT8_VAR_TYPE,
                        &card_is_set),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("source",
                0,
                "Reference clock source, either 'internal', 'external', 'carrier', or"
                " 'host' ('host' not available on all platforms)",
                "Source",
                &p_clock,
                STRING_VAR_TYPE),
    APP_ARG_OPT_PRESENT("frequency",
                'f',
                "External reference clock frequency in Hertz (see NOTE)",
                "Hz",
                &ext_freq,
                UINT32_VAR_TYPE,
                &ext_freq_is_set),
    APP_ARG_OPT("verify",
                0,
                "Verify full initialization with configured clock source",
                NULL,
                &verify_init,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

int main( int argc, char *argv[] )
{
    int32_t status=0;
    bool bRead=true;
    skiq_ref_clock_select_t ref_clock=skiq_ref_clock_invalid;
    skiq_ref_clock_select_t read_ref_clock=skiq_ref_clock_invalid;
    skiq_xport_type_t type = skiq_xport_type_auto;
    uint32_t read_ext_freq = DEFAULT_EXT_FREQ;
    pid_t owner = 0;
    bool skiq_initialized = false;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( p_clock == NULL )
    {
        bRead = true;
    }
    else
    {
        bRead = false;
        if( strcasecmp(p_clock, "internal") == 0 )
        {
            ref_clock = skiq_ref_clock_internal;
        }
        else if( strcasecmp(p_clock, "external") == 0 )
        {
            ref_clock = skiq_ref_clock_external;
        }
        else if( strcasecmp(p_clock, "carrier") == 0 )
        {
            ref_clock = skiq_ref_clock_carrier_edge;
        }
        else if( strcasecmp(p_clock, "host") == 0 )
        {
            ref_clock = skiq_ref_clock_host;
        }
        else
        {
            fprintf(stderr, "Error: invalid reference clock selected; should be one"
                    " of the following: 'internal', 'external', 'carrier', or 'host'\n");
            return (-3);
        }
    }

    /* Validate the ext_freq value. */
    if ( ext_freq_is_set && ( skiq_ref_clock_internal == ref_clock ) )
    {
        fprintf(stderr, "Error: Reference clock frequency setting not valid"
                " when specifying an internal reference clock source.\n");
        return -EINVAL;
    }

    if ( ext_freq_is_set )
    {
        int i = 0;
        bool valid_freq = false;

        while ( ( allowed_freq[i] > 0 ) && !valid_freq )
        {
            if ( ext_freq == allowed_freq[i] )
            {
                valid_freq = true;
            }
            i++;
        }

        if ( !valid_freq )
        {
            fprintf(stderr, "Error: invalid external frequency specified (%" PRIu32
                    " Hz); must be one of the following (some frequencies\n"
                    "are product dependent):\n", ext_freq);

            i = 0;
            while ( allowed_freq[i] > 0 )
            {
                fprintf(stderr, "\t%u\n", allowed_freq[i]);
                i++;
            }
            return (-4);
        }
    }

    if ( card_is_set && (NULL != p_serial) )
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number, not both\n");
        return (-1);
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init(type, skiq_xport_init_level_basic, &card, 1);
    if ( status != 0 )
    {
        if ( ( EBUSY == status) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }

        status = -1;
        goto finished;
    }
    else
    {
        skiq_initialized = true;
    }

    if((skiq_ref_clock_carrier_edge == ref_clock) &&
       (_skiq_get_part(card) != skiq_x4))
    {
        fprintf(stderr, "Error: invalid reference clock selected; can only use 'carrier' clock on Sidekiq X4\n");
        status = -EINVAL;
        goto finished;
    }

    skiq_read_ref_clock_select( card, &read_ref_clock );
    if( bRead == true )
    {
        switch( read_ref_clock )
        {
            case skiq_ref_clock_internal:
                printf("Info: reference clock configured for internal\n");
                break;

            case skiq_ref_clock_external:
            case skiq_ref_clock_carrier_edge:
            case skiq_ref_clock_host:
                if( 0 == skiq_read_ext_ref_clock_freq(card, &read_ext_freq) )
                {
                    printf("Info: reference clock currently configured for"
                            " %" PRIu32 " Hz external source\n", read_ext_freq);
                }
                else
                {
                    fprintf(stderr, "Error: external reference clock configured for "
                            "unknown frequency!\n");
                }
                break;

            default:
                fprintf(stderr, "Error: unknown reference clock configured\n");
                break;
        }
    }
    else
    {
        status = skiq_fact_write_ref_clock(card, ref_clock);
        if ( 0 != status )
        {
            fprintf(stderr, "Error: failed to update reference clock (result code %"
                    PRIi32 ")\n", status);
            goto finished;
        }
        else
        {
            printf("Info: successfully configured reference clock selection\n");
        }

        /* write the external reference clock frequency if it is selected and the user specified a
         * frequency on the command line */
        if ( ext_freq_is_set )
        {
            status = skiq_fact_write_ext_ref_clock_freq(card, ext_freq);
            if( 0 != status )
            {
                printf("Warning: unable to configure reference clock"
                       " frequency (result code %" PRIi32 ")\n", status);
                goto finished;
            }
            printf("Info: external clock configured to %"PRIu32" Hz\n", ext_freq);
        }
    }

    // attempt full init and verify presence (if requested)
    if( verify_init == true )
    {
        if( skiq_initialized == true )
        {
            skiq_exit();
            skiq_initialized = false;
        }

        printf("Info: attempting full initialization to validate reference"
               " clock presence\n");
        status = skiq_init(type, skiq_xport_init_level_full, &card, 1);
        if (0 != status)
        {
            fprintf(stderr, "Error: failed to fully initialize Sidekiq (result code"
                   " %" PRIi32 ")\n", status);
            goto finished;
        }
        else
        {
            skiq_initialized = true;
        }
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return (status);
}
