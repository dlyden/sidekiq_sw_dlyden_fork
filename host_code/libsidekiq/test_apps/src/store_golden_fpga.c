/*! \file store_golden_fpga.c
 * \brief This file copies the provided golden bitstream into flash.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <arg_parser.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <inttypes.h>
#include <signal.h>


/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- store golden FPGA bitstream into flash";
static const char* p_help_long =
"\
Uses either PCIE or USB interface to store a supplied bitstream into flash\n\
memory at offset zero, the golden bitstream slot.";

/* verify flash contents after writing */
static bool do_verify = false;
/* only verify flash contents against file contents */
static bool do_verify_only = false;
/* file path to FPGA bitstream */
static char* p_file_path = NULL;
/* Sidekiq card number */
static uint8_t card = UINT8_MAX;
/* Sidekiq serial number */
static char* p_serial = NULL;
/* Allow libsidekiq log output */
static bool verbose = false;

/* command line arguments available for use with this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("source",
                's',
                "Bitstream file to source for writing to flash",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Enable logging from libsidekiq to stdout",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verify",
                0,
                "Verify the flash memory after it is written",
                NULL,
                &do_verify,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verify-only",
                0,
                "Do not store the bitstream, just verify the flash memory matches the file contents (conflicts with --verify)",
                NULL,
                &do_verify_only,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


#if (defined __MINGW32__)
/* Unfortunately Mingw does not support sigset_t, so define a fake type definition and stub out the
   related functions. */
typedef int sigset_t;
static inline int mask_signals(sigset_t *old_mask) { return 0; }
static inline int unmask_signals(sigset_t *old_mask) { return 0; }
static inline bool shutdown_signal_pending(void) { return false; }
#else
/**
    @brief  Mask (block) the SIGINT and SIGTERM signals during critical
            operations

    @param[out]  old_mask   The original signal mask, needed by
                            ::unmask_signals().

    @note   Pending signals will be raised when ::unmask_signals() is called.

    @return 0 on success, else an errno.
*/
static int
mask_signals(sigset_t *old_mask)
{
    int result = 0;
    sigset_t new_mask;

    sigemptyset(&new_mask);
    sigaddset(&new_mask, SIGINT);
    sigaddset(&new_mask, SIGTERM);

    errno = 0;
    result = pthread_sigmask(SIG_BLOCK, &new_mask, old_mask);

    return (0 == result) ? result : errno;
}

/**
    @brief  Unmask (unblock) the signals blocked by ::mask_signals().

    @param[in]  old_mask    The original signal mask, as generated by
                            ::mask_signals().

    @note   Pending signals will be processed after calling this function.

    @return 0 on success, else an errno.
*/
static int
unmask_signals(sigset_t *old_mask)
{
    int result = 0;

    errno = 0;
    result = pthread_sigmask(SIG_SETMASK, old_mask, NULL);

    return (0 == result) ? result : errno;
}

/**
    @brief  Check if a shutdown signal (SIGINT or SIGTERM) is pending (while
            masked).

    @return true if a signal is pending, else false.
*/
static bool
shutdown_signal_pending(void)
{
    int result = 0;
    sigset_t pending_mask;
    bool pending = false;

    errno = 0;
    result = sigpending(&pending_mask);
    if (0 == result)
    {
        pending = ((1 == sigismember(&pending_mask, SIGINT)) ||
                   (1 == sigismember(&pending_mask, SIGTERM)));
    }
    else
    {
        printf("Debug: sigpending() failed (%d: '%s')\n",
                errno, strerror(errno));
    }

    return pending;
}
#endif	/* __MINGW32__ */



/*****************************************************************************/
/** This is the main function for executing the store_default_fpga app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    FILE *pFile = NULL;
    pid_t owner = 0;

    int result = 0;
    bool masked = false;
    sigset_t old_mask;

    bool skiq_initialized = false;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( !verbose )
    {
        /* disable messages */
        skiq_register_logging(NULL);
    }

    if ( ( UINT8_MAX == card ) && ( NULL == p_serial ) )
    {
        fprintf(stderr, "Error: one of --card or --serial MUST be specified\n");
        return (-1);
    }
    else if ( ( UINT8_MAX != card ) && ( NULL != p_serial ) )
    {
        fprintf(stderr, "Error: either --card OR --serial must be specified,"
                " not both\n");
        return (-1);
    }

    if ( do_verify && do_verify_only )
    {
        fprintf(stderr, "Error: either --verify OR --verify-only must be specified, not both\n");
        return (-1);
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if (NULL != p_serial)
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    /* open the bitstream */
    pFile = fopen( p_file_path, "rb" );
    if( pFile == NULL )
    {
        fprintf(stderr, "Error: unable to open file %s to read from (error code"
                " %d: '%s')\n", p_file_path, errno, strerror(errno));
        return (-1);
    }

    /*
        Ignore the shutdown signals SIGINT and SIGTERM while programming the
        FPGA so the FPGA can't be only partially programmed. Blocking of these
        signals must be done here as skiq_init() can spin up threads, any of
        which can receive & handle these signals; as threads inherit the signal
        mask of the parent on creation, blocking signals now prevents any
        created threads from receiving the shutdown signals.
    */
    result = mask_signals(&old_mask);
    if (0 != result)
    {
        printf("Warning: failed to block shutdown signals (return code %d);"
                " continuing but please do not press Ctrl-C during"
                " programming.\n", result);
    }
    else
    {
        masked = true;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* Initialize libsidekiq using the specified card. */
    status = skiq_init(skiq_xport_type_auto,
                       skiq_xport_init_level_basic,
                       &card,
                       1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }

        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    /*
        If the signals were successfully masked, check if one of them was
        received and, if so, shut down the program.
    */
    if( ( masked ) && ( shutdown_signal_pending() ) )
    {
        printf("Info: got shutdown signal\n");
        goto finished;
    }

    if ( ( status == 0 ) && !do_verify_only )
    {
        /*
            Store contents of the file on the card's on-board flash memory as
            the "golden" image.
        */
        status = skiq_fact_save_golden_fpga_to_flash( card, pFile );

        if( 0 == status )
        {
            printf("Info: flash programming succeeded\n");
        }
        else
        {
            fprintf(stderr, "Error: flash programming failed with status %d\n", status);
        }
    }

    /*
        If the signals were successfully masked, unmask them so that the
        shutdown signals operate normally; this is done here so the user can
        interrupt the verify operation(s) below.
    */
    if (masked)
    {
        result = unmask_signals(&old_mask);
        if (0 == result)
        {
            masked = false;
        }
    }

    if ( ( status == 0 ) && ( do_verify || do_verify_only ) )
    {
        printf("Info: performing flash data verification\n");
        status = skiq_fact_verify_golden_fpga_from_flash(card, pFile);
        if( 0 == status )
        {
            printf("Info: flash verify succeeded\n");
        }
        else
        {
            fprintf(stderr, "Error: flash verify failed with status %d\n", status);
            status = -1;
        }
    }

finished:
    /*
        If the shutdown signals are still masked, restore them; this is done
        as clean up.
    */
    if( masked )
    {
        (void) unmask_signals(&old_mask);
        masked = false;
    }

    /* cleanup */
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }
    if (NULL != pFile)
    {
        fclose( pFile );
        pFile = NULL;
    }

    return status;
}

