/*! \file test_warp_voltage_read.c
 * \brief This file contains the ability to write the
 * reference clock configuration using the public API
 * without writing the configuration to EEPROM.
 *
 * <pre>
 * Copyright 2013 - 2021 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <sidekiq_hal.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#define DEFAULT_INIT_LEVEL  "basic"

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = \
    "- test application that reads warp voltage in different initialization modes and evaluates errors";
static const char* p_help_long =
"\
Test application that reads warp voltage in basic and full init.\n\
When in basic initialization an error is expected when attempting \n\
to read the warp voltage on a Sidekiq card.  The opposite is\n\
expected when the card is in full initialization.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
";

static uint8_t card = DEFAULT_CARD_NUMBER;
static bool card_present = false;
static char* p_serial = NULL;
static bool serial_present = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT_PRESENT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE,
                &card_present),
    APP_ARG_OPT_PRESENT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE,
                &serial_present),
    APP_ARG_TERMINATOR,
};

static int32_t parse_card(void)
{
    int32_t status = 0;

    if(card_present && serial_present)
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number, not"
                " both\n");
        status = -EPERM;
        return status;
    }

    /* Card number specification will overrule any serial specification */
    /* If specified, attempt to find the card with a matching serial number when a card is not also specified. */
    if(serial_present && !card_present)
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            status = -ENODEV;
            return status;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if(card > (SKIQ_MAX_NUM_CARDS - 1))
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -ERANGE;
    }

    return status;
}

static int32_t test_case(skiq_xport_init_level_t init_level, int32_t exp_status)
{
    int32_t status = 0;
    uint16_t warp_voltage = 0;

    if(init_level != skiq_xport_init_level_unknown)
    {
        status = skiq_init(skiq_xport_type_auto, init_level, &card, 1);
        if(status != 0)
        {
            fprintf(stderr, "Error: unable to initialize card %" PRIu8 " (result code %"
                    PRIi32 ")\n", card, status);
            return status;
        }
    }

    status = skiq_read_tcvcxo_warp_voltage(card, &warp_voltage);
    if(status == exp_status)
    {
        printf("Info: expected status %" PRIi32 " was received when reading warp voltage\n", exp_status);
        status = 0;
    }
    else
    {
        fprintf(stderr, "Error: received an unexpected status %" PRIi32 " when reading warp voltage on card %" PRIu8 "\n", status, card);
    }

    skiq_exit();
    return status;
}


int main( int argc, char *argv[] )
{
    int32_t status=0;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -EINVAL;
        return status;
    }

    status = parse_card();
    if(status != 0)
    {
        return status;      // no need to output an error message here, this is accomplished in the function
    }

    /* Test case: reading warp voltage when the card is not initialized should result in an error */
    status = test_case(skiq_xport_init_level_unknown, -ENODEV);
    if(status != 0)
    {
        return status;      // no need to output an error message here, this is accomplished in the function
    }

    /* Test case: reading warp voltage when the card is in basic initialization should result in an error */
    status = test_case(skiq_xport_init_level_basic, -ENODEV);
    if(status != 0)
    {
        return status;      // no need to output an error message here, this is accomplished in the function
    }

    /* Test case: reading warp voltage when the card is in full initialization should not result in an error */
    status = test_case(skiq_xport_init_level_full, 0);

    return status;
}
