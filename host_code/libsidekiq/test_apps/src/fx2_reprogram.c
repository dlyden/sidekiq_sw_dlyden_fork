/**
    @file   fx2_reprogram.c

    @brief  FX2 programming and verification tool.

    <pre>
    Copyright 2016 - 2018 Epiq Solutions, All Rights Reserved
    </pre>
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <sidekiq_hal.h>        /* defines FW_VERSION */
#include <usb_interface.h>
#include <usb_private.h>
#include <arg_parser.h>

#include "util.h"
#include "intelhex.h"
#include "eeprom_records.h"

/** @brief  The first address to write into the EEPROM at. */
#define INITIAL_EEPROM_ADDRESS  (0)

/** @brief  The maximum size of the EEPROM (in bytes). */
#ifndef EEPROM_MAX_SIZE
#   define EEPROM_MAX_SIZE              (0x4000)
#endif

/**
    @defgroup SupportedComms    Bitfield indicating the communications methods
                                supported by a Sidekiq card.
    @{
*/
/** The Sidekiq supports PCI communications. */
#define PCI_SUPPORTED     (1 << 0)
/** The Sidekiq supports USB communications. */
#define USB_SUPPORTED     (1 << 1)
/** @} */

/** @brief  Macro used for checking if the Sidekiq hardware version is mPCIe. */
#define IS_HW_MPCIE(hwVer)                  \
    (((skiq_hw_vers_mpcie_b == (hwVer)) ||  \
      (skiq_hw_vers_mpcie_c == (hwVer)) ||  \
      (skiq_hw_vers_mpcie_d == (hwVer)) ||  \
      (skiq_hw_vers_mpcie_e == (hwVer))) ?  \
       true : false)

/** @brief  Macro used for checking if the Sidekiq hardware version is M2. */
#define IS_HW_M2(hwVer)                     \
    (((skiq_hw_vers_m2_b == (hwVer)) ||     \
      (skiq_hw_vers_m2_c == (hwVer))) ?     \
       true : false)

#define INVALID_ENUMERATION_DELAY (UINT16_MAX)

#ifndef DEFAULT_FX2_COMMS_ATTEMPTS
#   define DEFAULT_FX2_COMMS_ATTEMPTS   (5)
#endif

/** @brief  Information about a Sidekiq card. */
struct SidekiqCard
{
    /** @brief  The serial number of the card. */
    char serial[SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM];

    /** @brief  The hardware version of the Sidekiq card. */
    skiq_hw_vers_t hwVer;

    /**
        @brief  A bitfield indicating the valid communication methods for this
                card. See @ref SupportedComms for a list of supported methods.
    */
    uint8_t supported;

    /** @brief  Flag to indicate if the card's firmware version was found. */
    bool fwVerFound;
    /** @brief  The major version of the card's firmware. */
    uint8_t fwVerMaj;
    /** @brief  The minor version of the card's firmware. */
    uint8_t fwVerMin;

    /** @brief The time in milliseconds that firmware should delay USB
               emumeration on the host. */
    uint16_t enumeration_delay;
};

/** @brief  A structure to track multiple Sidekiqs. */
struct SidekiqCardList
{
    /** @brief  The list of Sidekiqs. */
    struct SidekiqCard entry[SKIQ_MAX_NUM_CARDS];
    /** @brief  The number of valid entries in @ref entry. */
    uint8_t length;
};

/** @brief  Information about the programming operation. */
struct ProgramConfig
{
    /**
        @brief  A list of user-specified serials to program.
        This reuses the SidekiqCardList structure to allow reuse of its
        functions; we don't use the supported field in this case.
    */
    struct SidekiqCardList userSerials;

    /** @brief  True if only listing all discovered cards. */
    bool listOnly;
    /** @brief  True if the write is to be verified. */
    bool verify;
    /** @brief  True to program all of the discovered Sidekiqs. */
    bool all;
    /** @brief  True to program Sidekiqs found over PCI. */
    bool usePci;
    /** @brief  True to program Sidekiqs found over USB. */
    bool useUsb;
    /** @brief  True to program the FX2's EEPROM. */
    bool loadEeprom;
    /** @brief  True to program the FX2's RAM. */
    bool loadRam;

    /**
        @brief  True if the user wants to reprogram any card below the version
                number found below in @ref verMaj and @ref verMin.
    */
    bool fwVerCheck;
    /** @brief  The major version number to check against. */
    uint8_t checkVerMaj;
    /** @brief  The minor version number to check against. */
    uint8_t checkVerMin;

    /**
        @brief  The filename of the FX2 firmware - this file should contain
                a list of Intel hex records.
    */
    const char *mpcieHexFilename;
    const char *m2HexFilename;

    struct RecordList mpcieHexRecords;
    struct RecordList m2HexRecords;

    /** @brief  A buffer containing the parsed and formatted firmware. */
    uint8_t *mpcieFwBuffer;
    uint8_t *m2FwBuffer;
    /** @brief  The length of firmware buffer in bytes. */
    uint16_t mpcieFwLength;
    uint16_t m2FwLength;

    /** @brief The time in milliseconds that firmware should delay USB
               emumeration on the host. */
    uint16_t enumeration_delay;

    /**
        @brief  The number of times to attempt communications with the FX2 after reprogramming; if
                0, do not try to establish communications.
    */
    uint16_t commsAttempts;
};

/**
    @brief  Find a Sidekiq with a specific serial number in a list of Sidekiqs.

    @param[in]  list        The list of Sidekiqs to search.
    @param[in]  serial      The serial number to search for.
    @param[out] idx         The index within the list that the serial number was
                            found. Not valid if the function returns false.

    @return true if the serial number was found, else false.
*/
static bool
findBySerial(struct SidekiqCardList *list, char* serial, uint8_t *idx)
{
    bool found = false;
    uint8_t i = 0;

    for (i = 0; (i < list->length) && (false == found); i++)
    {
        if (0 == strncasecmp(list->entry[i].serial,
                             serial,
                             SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM))
        {
            *idx = i;
            found = true;
        }
    }

    return (found);
}

/**
    @brief  Display a list of Sidekiqs.

    @param[in]  list    The list of Sidekiqs to display.
*/
static void
displayCardList(struct SidekiqCardList *list)
{
    uint8_t i = 0;

    printf("%u eligible card(s) found.\n", list->length);
    if (0 < list->length)
    {
        for (i = 0; i < list->length; i++)
        {
            if (IS_HW_MPCIE(list->entry[i].hwVer))
            {
                printf("\tmPCIe ");
            }
            else if (IS_HW_M2(list->entry[i].hwVer))
            {
                printf("\tM2 ");
            }
            else
            {
                printf("\t??? ");
            }

            printf("Serial %s ", list->entry[i].serial);

            printf("\n\t\tSupports ");
            if (list->entry[i].supported & PCI_SUPPORTED)
            {
                printf("PCI");
            }
            if (list->entry[i].supported & USB_SUPPORTED)
            {
                if (list->entry[i].supported & PCI_SUPPORTED)
                {
                    printf(", ");
                }
                printf("USB");
            }
            if (list->entry[i].fwVerFound)
            {
                printf("\n\t\tFirmware v%u.%u",
                        list->entry[i].fwVerMaj, list->entry[i].fwVerMin);
            }
            if (0 != list->entry[i].enumeration_delay)
            {
                printf("\n\t\tEnumeration Delay %d msec",
                       list->entry[i].enumeration_delay);
            }
            printf("\n");
        }
    }
}

/**
    @brief  Display the version of libsidekiq being used.
*/
static void
displayLibVersion(void)
{
    uint8_t major = 0;
    uint8_t minor = 0;
    uint8_t patch = 0;
    const char * label;

    (void)skiq_read_libsidekiq_version(&major, &minor, &patch, &label);

    printf("Using libsidekiq %u.%u.%u%s\n", major, minor, patch, label);
}

/**
    @brief  Find all of the connected Sidekiqs that supported a specified
            communication method (PCI or USB).

    @param[out]  list   The list of Sidekiqs to append newly discovered cards
                        to.
    @param[in]   usePci true to find only PCI Sidekiqs; false to find only
                        USB Sidekiqs.

    @return false if the probe failed, else true.
*/
static bool
findSkiqsByMethod(struct SidekiqCardList *list, bool usePci)
{
    int32_t res = 0;
    bool found = false;
    uint8_t numCards = 0;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t numUsableCards = 0;
    uint8_t usableCards[SKIQ_MAX_NUM_CARDS];
    uint8_t i = 0;
    uint8_t idx = 0;
    uint16_t delay = 0;
    char* serial;
    skiq_xport_type_t type;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;
    pid_t owner = 0;

    if (usePci)
    {
        type = skiq_xport_type_pcie;
    }
    else
    {
        type = skiq_xport_type_usb;
    }

    res = skiq_get_cards(type, &numCards, (uint8_t *) cards);
    if (0 != res)
    {
        printf("%s [%s] : couldn't get available card list (%" PRIi32 ")!\n",
                __FUNCTION__, (usePci) ? "PCI" : "USB", res);
        return false;
    }

    if ( 0 == numCards )
    {
        /* No Sidekiqs discovered */
        return true;
    }

    /* Only process the cards that aren't already in use. */
    for (i = 0; i < numCards; i++)
    {
        res = skiq_is_card_avail(cards[i], &owner);
        if (0 != res)
        {

            printf("%s [%s] : card %" PRIu8 " is already in use (PID %u);"
                    " skipping.\n", __FUNCTION__,
                    (usePci) ? "PCI" : "USB", cards[i], (unsigned int) owner);
        }
        else
        {
            usableCards[numUsableCards] = cards[i];
            numUsableCards++;
        }
    }

    if ( 0 == numUsableCards )
    {
        /* No usable Sidekiqs discovered */
        return true;
    }

    res = skiq_init(type, level, usableCards, numUsableCards);
    if (0 != res)
    {
        printf("%s [%s] : couldn't start Sidekiq library (result code %" PRIi32
                ")!\n", __FUNCTION__, (usePci) ? "PCI" : "USB", res);
        return false;
    }

    for (i = 0; i < numUsableCards; i++)
    {
        skiq_param_t param;

        res = skiq_read_parameters(usableCards[i], &param);
        if (0 == res)
        {
            if ( (param.card_param.part_type != skiq_mpcie) &&
                 (param.card_param.part_type != skiq_m2) )
            {
                /* not an eligible card, so skip it */
                continue;
            }
        }
        else
        {
            printf("%s : couldn't read card parameters for card %u"
                    " (%" PRIi32 ")!\n", __FUNCTION__, usableCards[i], res);
        }

        res = skiq_read_serial_string(usableCards[i], &serial);
        if (0 != res)
        {
            printf("%s [%s] : couldn't get serial number for card %u"
                    " (%" PRIi32 ")!\n", __FUNCTION__,
                    (usePci) ? "PCI" : "USB", usableCards[i], res);
        }
        else
        {
            idx = 0;
            found = findBySerial(list, serial, &idx);
            if (!found)
            {
                if (SKIQ_MAX_NUM_CARDS < (list->length + 1))
                {
                    printf("%s [%s] : somehow too many cards were found"
                            " (max is currently %u). List may be truncated.\n",
                            __FUNCTION__,
                            (usePci) ? "PCI" : "USB", SKIQ_MAX_NUM_CARDS);
                    continue;
                }
                else
                {
                    idx = list->length;
                    strncpy(list->entry[idx].serial,
                            serial,
                            SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM);
                    list->entry[idx].supported = 0;
                    list->length++;
                }
            }

            list->entry[idx].supported |= (usePci) ? PCI_SUPPORTED :
                                                     USB_SUPPORTED;
            list->entry[idx].fwVerFound = false;
            res = skiq_read_fw_version(usableCards[i],
                                       &(list->entry[idx].fwVerMaj),
                                       &(list->entry[idx].fwVerMin));
            if (0 == res)
            {
                list->entry[idx].fwVerFound = true;
            }
            else if ( -ENOTSUP != res )
            {
                printf("%s [%s] : couldn't get firmware version (%" PRIi32
                        ")!\n", __FUNCTION__, (usePci) ? "PCI" : "USB", res);
            }

            res = skiq_read_hw_version(usableCards[i],
                    &(list->entry[idx].hwVer));
            if (0 != res)
            {
                list->entry[idx].hwVer = skiq_hw_vers_invalid;
                printf("%s [%s] : couldn't get hardware version (%" PRIi32
                        ")!\n", __FUNCTION__, (usePci) ? "PCI" : "USB", res);
            }

            list->entry[idx].enumeration_delay = 0;
            res = skiq_read_usb_enumeration_delay(usableCards[i], &delay);
            if (0 == res)
            {
                list->entry[idx].enumeration_delay = delay;
            }
            else if ( -ENOTSUP != res )
            {
                printf("%s [%s] : couldn't get FX2 enumeration delay (%" PRIi32
                        ")!\n", __FUNCTION__, (usePci) ? "PCI" : "USB", res);
            }
        }
    }

    res = skiq_exit();
    if (0 != res)
    {
        printf("%s [%s] : failed to shut down Sidekiq library (%" PRIi32 ")!\n",
                __FUNCTION__, (usePci) ? "PCI" : "USB", res);
        return false;
    }

    return true;
}

/**
    @brief  Verify that the FX2 responds to commands after programming

    @note   This function attempts to read the firmware version from the FX2 as a means of testing
            communications

    @param[in]  cardId          The card ID of the Sidekiq to verify.
    @param[in]  numAttempts     The number of attempts to try communicating before giving up

    @return 0 on success, else an errno
    @retval ETIMEDOUT   if no response was read within @p numAttempts attempts
*/
static int32_t
waitForFx2Response(uint8_t cardId, uint16_t numAttempts)
{
    int32_t result = 0;
    int32_t status = 0;
    bool responded = false;
    uint8_t majorVer = 0;
    uint8_t minorVer = 0;

    if (0 == numAttempts)
    {
        return 0;
    }

    while ((0 < numAttempts--) && (!responded) && (0 == result))
    {
        status = hal_read_fw_ver(cardId, &majorVer, &minorVer);
        if (0 != status)
        {
            printf("%s: failed to read firmware version from FX2 (status = %" PRIi32 ")\n",
                __FUNCTION__, status);
        }
        else
        {
            responded = true;
            printf("%s: got firmware version response from FX2 (%" PRIu16 " attempts left; read"
                " firmware version %" PRIu8 ".%" PRIu8 ")\n", __FUNCTION__, numAttempts,
                majorVer, minorVer);
        }


        if (0 != status)
        {
            /* Tiny sleep here so we don't constantly spin */
            status = hal_millisleep(250);
            if (0 != status)
            {
                printf("%s: sleep between attempts failed (success = %" PRIi32 ")\n", __FUNCTION__,
                    status);
            }
        }
    }

    if ((0 == numAttempts) && (!responded))
    {
        result = ETIMEDOUT;
        printf("%s: timed out waiting to read firmware version from FX2\n", __FUNCTION__);
    }

    if ((0 == result) && (0 != status))
    {
        /*
            If there was an error code above but we're poised to return "success" then make sure
            the caller knows there was some kind of error
        */
        result = status;
    }

    return result;
}

/**
    @brief  Initialize a list of Sidekiqs.

    @param[in]  list    The list of Sidekiqs to initialize.

    @return false if @ref list is NULL, else true.
*/
bool
skiqListInit(struct SidekiqCardList *list)
{
    uint8_t i = 0;

    if (NULL == list)
    {
        printf("%s : list is NULL!\n", __FUNCTION__);
        return false;
    }

    list->length = 0;
    for (i = 0; i < SKIQ_MAX_NUM_CARDS; i++)
    {
        memset(list->entry[i].serial, 0, SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM);
        list->entry[i].supported = 0;
        list->entry[i].hwVer = skiq_hw_vers_invalid;
        list->entry[i].fwVerMaj = 0;
        list->entry[i].fwVerMin = 0;
        list->entry[i].fwVerFound = false;
    }

    return true;
}

/**
    @brief  Find all of the Sidekiqs attached to a system.

    This function has the possiblity to discover Sidekiqs twice - once using
    PCI and once using USB. This allows us to know what communication methods
    are available to each card. This is important with boards like the
    Gateworks, which has some PCI slots that don't have USB.

    @param[out]  list       The list to place any discovered cards into.
    @param[in]  usePci      Detect Sidekiq's using PCI.
    @param[in]  useUsb      Detect Sidekiq's using USB.

    @return false if @ref list is NULL or the discovery failed, else true.
*/
bool
findSkiqs(struct SidekiqCardList *list, bool usePci, bool useUsb)
{
    bool success = false;
    bool overall = false;

    if (NULL == list)
    {
        printf("%s : list is NULL!\n", __FUNCTION__);
        return false;
    }

    list->length = 0;

    if (usePci)
    {
        /* Find the PCI Sidekiqs first. */
        success = findSkiqsByMethod(list, true);
        if (!success)
        {
            printf("%s : failed to get PCI cards.\n", __FUNCTION__);
        }
        overall = success;
    }

    if (useUsb)
    {
        /* Now find the USB Sidekiqs. */
        success = findSkiqsByMethod(list, false);
        if (!success)
        {
            printf("%s : failed to get USB cards.\n", __FUNCTION__);
        }
        overall |= success;
    }

    return overall;
}

/**
    @brief  Get card information for desired Sidekiq cards from the list of
            detected cards. It is expected that the @ref desiredList is already
            populated with the serial number of the desired card.

    @param[in,out]  desiredList     List of desired cards.
    @param[in]      foundList       List of detected cards.

    @return true if the write succeeds, else false.
*/
bool
findRequestedSkiqs(struct SidekiqCardList *desiredList,
                   struct SidekiqCardList *foundList)
{
    struct SidekiqCard* desired;
    struct SidekiqCard* found;
    uint8_t m = 0;
    uint8_t n = 0;
    uint8_t total = 0;

    if ((NULL == desiredList) || (NULL == foundList))
    {
        return false;
    }

    for (m = 0; m < desiredList->length; m++)
    {
        desired = &(desiredList->entry[m]);
        for (n = 0; n < foundList->length; n++)
        {
            found = &(foundList->entry[n]);
            if (0 == strncmp(desired->serial,
                             found->serial,
                             SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM))
            {
                desired->supported = found->supported;
                desired->hwVer = found->hwVer;
                desired->fwVerMaj = found->fwVerMaj;
                desired->fwVerMin = found->fwVerMin;
                desired->fwVerFound = found->fwVerFound;
                total++;
            }
        }
    }

    return (0 != total) ? true : false;
}

static bool
writeRam(uint8_t cardId, struct RecordList *list)
{
    struct Record* tmp;
    int32_t r = 0;
    uint8_t index=0;

    if( usb_get_index(cardId, &index) != 0 )
    {
        return (false);
    }

    if (0 != usb_hold_firmware_reset(index, true))
    {
        return false;
    }

    tmp = list->head;
    while ((NULL != tmp) && (r == 0))
    {
        if (Data == tmp->type)
        {
            r = usb_write_firmware_ram(index,
                                       tmp->addr,
                                       tmp->data,
                                       tmp->length);
        }
        tmp = tmp->next;
    }

    if (0 != usb_hold_firmware_reset(index, false))
    {
        return false;
    }

    return true;
}

/**
    @brief  Verify the firmware in a Sidekiq EEPROM.

    @param[in]  cardId          The card ID of the Sidekiq to verify.
    @param[in]  fwBuffer        The firmware image to verify.
    @param[in]  fwBufferLen     The length of @ref fwBuffer in bytes.
    @param[in]  startAddress    The starting address in EEPROM to begin the
                                comparison; in most cases, this should be
                                INITIAL_EEPROM_ADDRESS.

    @return true if the verify succeeds, else false.
*/

static bool
verifyEeprom(uint8_t cardId, uint8_t *fwBuf, uint16_t fwBufLen,
        uint16_t startAddress)
{
    bool success = false;
    int32_t res = 0;
    uint16_t addr = (uint16_t) startAddress;
    uint16_t num_bytes = fwBufLen;
    uint16_t len = 0;
    uint16_t n = 0;
    uint8_t *vrfyBuf = NULL;

    vrfyBuf = (uint8_t *) calloc(fwBufLen, 1);
    if (NULL == vrfyBuf)
    {
        printf("%s : couldn't allocate memory for verify buffer!\n",
                __FUNCTION__);
        goto finished;
    }

    while (num_bytes)
    {
        len = (num_bytes > 64) ? 64 : num_bytes;
        res = hal_read_eeprom(cardId, addr, vrfyBuf, len);
        if (0 > res)
        {
            printf("%s : couldn't read contents of EEPROM (%d)!\n",
                    __FUNCTION__, res);
            goto finished;
        }

        for (n = 0; n < len; n++)
        {
            if (fwBuf[addr+n] != vrfyBuf[n])
            {
                printf("%s : verify at address %04X failed (expected 0x%02X,"
                       " read 0x%02X).\n", __FUNCTION__, addr+n, fwBuf[addr+n],
                       vrfyBuf[n]);
                goto finished;
            }
        }
        num_bytes -= len;
        addr += len;
    }

    success = true;

finished:
    if (NULL != vrfyBuf)
    {
        free(vrfyBuf);
        vrfyBuf = NULL;
    }

    return success;
}

/**
    @brief  Write firmware to a Sidekiq.

    @param[in]  cardId      The card ID of the Sidekiq to program.
    @param[in]  fwBuffer    The firmware to write to the Sidekiq.
    @param[in]  fwBufferLen The length of @ref fwBuffer in bytes.

    @return true if the write succeeds, else false.
*/
static bool
writeEeprom(uint8_t cardId, uint8_t *fwBuf, uint16_t fwBufLen)
{
    bool success = false;
    int32_t res = 0;
    uint16_t addr = (uint16_t) INITIAL_EEPROM_ADDRESS;
    uint16_t num_bytes = fwBufLen;
    uint16_t len = 0;

    if (EEPROM_MAX_SIZE <= fwBufLen)
    {
        printf("%s : buffer exceeds EEPROM size (%u bytes).\n",
                __FUNCTION__, EEPROM_MAX_SIZE);

        goto finished;
    }

    while (num_bytes)
    {
        len = (num_bytes > 64) ? 64 : num_bytes;
        res = hal_write_eeprom(cardId, addr, (fwBuf + addr), len);
        if (0 > res)
        {
            printf("%s : failed write to EEPROM at address %04X (%" PRIi32
                    ").\n", __FUNCTION__, addr, res);
            goto finished;
        }
        num_bytes -= len;
        addr += len;
    }

    success = true;

finished:
    return success;
}


/**
    @brief  Iterate through the list of discovered cards and process (this
            is programming EEPROM, verifying EEPROM, or writing to RAM) only
            the user-specified cards.

    @param[in]  selectList      The list of selected (user-specified) Sidekiqs.
    @param[in]  config          The configuration data structure.

    @return true if the specified cards were all processed, else false.
*/
bool
processCardList(struct SidekiqCardList *selectList,
                struct ProgramConfig *config)
{
    skiq_xport_type_t type;
    int32_t res = 0;
    uint8_t card = 0;
    uint16_t delay;
    uint8_t i = 0;
    uint8_t total = 0;
    struct SidekiqCard *target = NULL;
    struct RecordList *fwRec = NULL;
    uint8_t* fwBuf = NULL;
    uint16_t fwBufLen = 0;
    bool initialized = false;

    if (NULL == config)
    {
        printf("%s : config is NULL!\n", __FUNCTION__);
        return false;
    }
    else if (NULL == selectList)
    {
        printf("%s : selectList is NULL; calling this a success.\n",
                __FUNCTION__);
        return true;
    }

    delay = config->enumeration_delay;

    for (i = 0; i < selectList->length; i++) {

        target = &(selectList->entry[i]);

        if (USB_SUPPORTED & target->supported) {
            type = skiq_xport_type_usb;
        }
        else if (PCI_SUPPORTED & target->supported) {
            type = skiq_xport_type_pcie;
        }
        else {
            printf("%s : skipping Sidekiq serial %s, not found\n",
                    __FUNCTION__, target->serial);
            continue;
        }

        fwBuf = NULL;
        fwBufLen = 0;

        if (IS_HW_MPCIE(target->hwVer)) {
            fwRec = &(config->mpcieHexRecords);
            fwBuf = config->mpcieFwBuffer;
            fwBufLen = config->mpcieFwLength;
        }
        else if (IS_HW_M2(target->hwVer)) {
            fwRec = &(config->m2HexRecords);
            fwBuf = config->m2FwBuffer;
            fwBufLen = config->m2FwLength;
        }
        else {
            printf("%s : skipping Sidekiq serial %s, unsupported hardware"
                    " type\n", __FUNCTION__, target->serial);
            continue;
        }

        if ((NULL == fwBuf) || (0 == fwBufLen))
        {
            printf("%s : can't program nor verify Sidekiq serial %s, no hex"
                    " file provided\n", __FUNCTION__, target->serial);
            continue;
        }

        if ((config->fwVerCheck) &&
            (FW_VERSION(config->checkVerMaj, config->checkVerMin) <=
             FW_VERSION(target->fwVerMaj, target->fwVerMin)))
        {
            printf("%s : skipping Sidekiq serial %s, version is at or above "
                   "v%u.%u\n",
                   __FUNCTION__, target->serial, config->checkVerMaj,
                   config->checkVerMin);
            // mark this as a "success"
            total++;
            continue;
        }

        res = skiq_get_card_from_serial_string(target->serial, &card);
        if (0 != res) {
            printf("%s : failed to get card number for Sidekiq serial %s"
                    " (%" PRIi32 ")!\n", __FUNCTION__, target->serial, res);
            continue;
        }

        /* Exit the library if it's already initialized. */
        if (initialized) {
            skiq_exit();
            initialized = false;
        }

        res = skiq_init(type, skiq_xport_init_level_basic, &card, 1);
        if (0 != res) {
            printf("%s : failed to initialize Sidekiq serial %s (%" PRIi32
                    ")!\n", __FUNCTION__, target->serial, res);
            continue;
        }
        initialized = true;

        if (!config->verify && (config->loadEeprom || config->loadRam))
        {
            /* Don't attempt to write anything if just verifying. */
            if (INVALID_ENUMERATION_DELAY != config->enumeration_delay) {
                res = skiq_fact_write_usb_enumeration_delay(card, delay);
                if (0 != res)
                {
                    printf("%s : failed to set enumeration delay (result code"
                            " %" PRIi32 ")\n", __FUNCTION__, res);
                    continue;
                }
            }
        }

        if (config->loadEeprom)
        {
            printf("%s : programming EEPROM for Sidekiq serial %s\n",
                   __FUNCTION__, target->serial);
            res = writeEeprom(card, fwBuf, fwBufLen);
            if (!res)
            {
                printf("%s : failed EEPROM write for Sidekiq serial %s!\n",
                        __FUNCTION__, target->serial);
                continue;
            }
            else
            {
                printf("%s : EEPROM write succeeded for Sidekiq serial %s!\n",
                        __FUNCTION__, target->serial);
            }
        }
        if (config->verify)
        {
            printf("%s : verifying EEPROM for Sidekiq serial %s\n",
                    __FUNCTION__, target->serial);
            res = verifyEeprom(card, fwBuf, fwBufLen,
                    (uint16_t) INITIAL_EEPROM_ADDRESS);
            if (!res)
            {
                printf("%s : failed EEPROM verify on Sidekiq serial %s!\n",
                        __FUNCTION__, target->serial);
                continue;
            }
            else
            {
                printf("%s : EEPROM verify succeeded for Sidekiq serial %s!\n",
                        __FUNCTION__, target->serial);
            }
        }

        if (config->loadRam)
        {
            if (skiq_xport_type_usb != type)
            {
                printf("%s : can't load RAM for Sidekiq serial %s, no USB "
                       "connection present\n",
                       __FUNCTION__, target->serial);
                continue;
            }

            printf("%s : programming RAM for Sidekiq serial %s\n",
                   __FUNCTION__,
                   target->serial);

            res = writeRam(card, fwRec);
            if (!res)
            {
                printf("%s : failed RAM write for Sidekiq serial %s!\n",
                        __FUNCTION__, target->serial);
                continue;
            }
        }

        res = waitForFx2Response(card, config->commsAttempts);
        if (0 != res)
        {
            printf("%s : failed to confirm communications after reprogramming on Sidekiq serial"
                " %s!\n", __FUNCTION__, target->serial);
            continue;
        }

        total++;
    }

    if (initialized)
    {
        skiq_exit();
        initialized = false;
    }

    return (total == selectList->length) ? true : false;
}

/**
    @brief  Add Sidekiq serial number(s) to a Sidekiq card list.

    @param[in]  list        The list to add the card(s) to.
    @param[in]  serials     Text format Sidekiq serial numbers; this can
                            either be a single serial number ("12345") or
                            a comma-separated list of serial numbers
                            ("1,2,3,4").
*/
static void
addSerials(struct SidekiqCardList *list, const char *serials)
{
    char *dup = NULL;
    char *ptr = NULL;
    char *savePtr = NULL;
    char *parseStr = NULL;
    struct SidekiqCard *cardPtr = NULL;

    if (list->length >= SKIQ_MAX_NUM_CARDS)
    {
        printf("%s : reached maximum number of cards (%u)!\n",
                __FUNCTION__, SKIQ_MAX_NUM_CARDS);
        return;
    }

    dup = strdup(serials);
    if (NULL == dup)
    {
        printf("%s : couldn't get memory to duplicate string!\n",
                __FUNCTION__);
        return;
    }

    parseStr = dup;
    do
    {
        ptr = strtok_r(parseStr, (const char *) ",", &savePtr);
        if (NULL != parseStr)
        {
            parseStr = NULL;
        }

        if (NULL == ptr)
        {
            break;
        }

        cardPtr = &(list->entry[list->length]);
        strncpy(cardPtr->serial, ptr, SKIQ_FACT_SERIAL_NUM_STRING_MAX_LEN);
        cardPtr->supported = 0;
        list->length++;

    } while (SKIQ_MAX_NUM_CARDS > list->length);

    if (SKIQ_MAX_NUM_CARDS < list->length)
    {
        printf("%s : reached maximum number of cards (%u)!\n",
                __FUNCTION__, SKIQ_MAX_NUM_CARDS);
    }

    free(dup);
    dup = NULL;
}

/**
    @brief  Add Sidekiq card ID(s) to a Sidekiq card list.

    @param[in]  list        The list to add the card(s) to.
    @param[in]  ids         Text format Sidekiq card IDs; this can
                            either be a single card number ("1") or
                            a comma-separated list of card numbers
                            ("0,1,2,3").
*/
static void
addCards(struct SidekiqCardList *list, const char *ids)
{
    char *dup = NULL;
    char *ptr = NULL;
    char *savePtr = NULL;
    char *parseStr = NULL;
    char* serialStr = NULL;
    uint8_t card = 0;
    struct SidekiqCard *cardPtr = NULL;

    if (list->length >= SKIQ_MAX_NUM_CARDS)
    {
        printf("%s : reached maximum number of cards (%u)!\n",
                __FUNCTION__, SKIQ_MAX_NUM_CARDS);
        return;
    }

    dup = strdup(ids);
    if (NULL == dup)
    {
        printf("%s : couldn't get memory to duplicate string!\n",
                __FUNCTION__);
        return;
    }

    parseStr = dup;
    do
    {
        ptr = strtok_r(parseStr, (const char *) ",", &savePtr);
        if (NULL != parseStr)
        {
            parseStr = NULL;
        }

        if (NULL == ptr)
        {
            break;
        }

        card = atoi(ptr);

        if (0 != skiq_read_serial_string(card, &serialStr))
        {
            printf("%s : failed to get serial number for card %" PRIu8 "!\n",
                   __FUNCTION__, card);
            break;
        }

        cardPtr = &(list->entry[list->length]);
        strncpy(cardPtr->serial, serialStr, SKIQ_FACT_SERIAL_NUM_STRING_MAX_LEN);
        cardPtr->supported = 0;
        list->length++;

    } while (SKIQ_MAX_NUM_CARDS > list->length);

    if (SKIQ_MAX_NUM_CARDS < list->length)
    {
        printf("%s : reached maximum number of cards (%u)!\n",
                __FUNCTION__, SKIQ_MAX_NUM_CARDS);
    }

    free(dup);
    dup = NULL;
}

/**
    @brief  Read, parse, and format a firmware file.

    @param[in]  fileName    The configuration structure to use.
    @param[out] list        List to hold parsed intel records.
    @param[out] fwDest      Buffer to hold flat memory firmware data.
    @param[out] fwLength    The length of the firmware data.

    This function reads in an Intel hex firmware file, parses all of the
    records, and converts it into the format it needs to be on the EEPROM.

    @return The number of bytes parsed from the hex file; a negative value
            is returned on failure.
*/
static int32_t
parseHexFile(const char* fileName, struct RecordList* list, uint8_t** fwDest,
        uint16_t *fwLength)
{
    int32_t success = 0;
    ResultCode result = Success;
    uint32_t total = 0;
    uint8_t* buf = NULL;

    buf = (uint8_t *) calloc(1, EEPROM_MAX_BUFFER_LEN);
    if (NULL == buf)
    {
        printf("%s : can't allocate memory for generated records!\n",
                __FUNCTION__);
        success = -1;
        goto finished;
    }

    result = intel_loadHexFile((char*) fileName, list);
    if (Success != result)
    {
        printf("%s : couldn't load hex file (%d).\n",
                __FUNCTION__, result);
        success = -2;
        goto finished;
    }

    intel_sortRecordList(list);

    total = eeprom_convertRecords(list, buf, EEPROM_MAX_BUFFER_LEN);
    if (0 >= total)
    {
        printf("%s : couldn't convert records!\n", __FUNCTION__);
        success = -3;
        goto finished;
    }

    *fwDest = buf;
    *fwLength = (uint16_t) total;

finished:
    if ((0 >= total) && (NULL != buf))
    {
        free(buf);
    }

    return (success);
}

/**
    @brief  Parse a user-specified version string.

    @param[in]  config      The configuration structure to store the version
                            number in.
    @param[in]  verString   The version string to parse.

    @return true if the parsing succeeded.
*/
static bool
getVersionNum(struct ProgramConfig *config, const char *verString)
{
    bool success = false;
    char *dup = NULL;
    char *ptr = NULL;
    char *savePtr = NULL;
    long major = 0;
    long minor = 0;

    dup = strdup(verString);
    if (NULL == dup)
    {
        printf("%s : couldn't allocate memory for parsing!\n",
                __FUNCTION__);
        goto finished;
    }

    ptr = strtok_r(dup, (const char *) ".", &savePtr);
    if (NULL == ptr)
    {
        printf("%s : couldn't find token!\n", __FUNCTION__);
        goto finished;
    }

    success = convertToLong((const char *) ptr, NULL, 0, &major);
    if (!success)
    {
        printf("%s : couldn't parse version '%s'!\n",
                __FUNCTION__, verString);
        goto finished;
    }

    ptr = strtok_r(NULL, (const char *) "\n", &savePtr);
    if (NULL == ptr)
    {
        printf("%s : couldn't find string end!\n", __FUNCTION__);
        goto finished;
    }

    success = convertToLong((const char *) ptr, NULL, 0, &minor);
    if (!success)
    {
        printf("%s : couldn't parse version '%s'!\n",
                __FUNCTION__, verString);
        goto finished;
    }

    config->checkVerMaj = major;
    config->checkVerMin = minor;
    config->fwVerCheck = true;

finished:
    if (NULL != dup)
    {
        free(dup);
        dup = NULL;
    }

    return success;
}

/**
    @brief  Initialize the configuration data structure.

    @param[in]  config      The configuration structure to initialize.

    @return true if the initialization succeeded, else false.
*/
static bool
initProgramConfig(struct ProgramConfig *config)
{
    bool success = false;

    success = skiqListInit(&(config->userSerials));
    if (!success)
    {
        printf("Failed to initialize Sidekiq list... weird.\n");
        return false;
    }

    config->listOnly = false;
    config->verify = false;
    config->all = false;
    config->usePci = true;
    config->useUsb = true;
    config->loadEeprom = false;
    config->loadRam = false;

    config->fwVerCheck = false;
    config->checkVerMaj = 0;
    config->checkVerMin = 0;

    config->mpcieHexFilename = NULL;
    config->m2HexFilename = NULL;

    config->mpcieFwBuffer = NULL;
    config->mpcieFwLength = 0;
    config->m2FwBuffer = NULL;
    config->m2FwLength = 0;

    config->enumeration_delay = INVALID_ENUMERATION_DELAY;
    config->commsAttempts = DEFAULT_FX2_COMMS_ATTEMPTS;

    return true;
}

/**
    @brief  Parse the command line arguments.

    @param[in]  argc        The number of command line arguments.
    @param[in]  argv        The list of command line arguments.
    @param[out] config      The configuration data structure; this is populated
                            based upon the command line options given.

    @return false if the parsing fails, else true.
*/
static bool
parseCmdLine(int argc, char *argv[], struct ProgramConfig *config)
{
    bool success = false;
    bool doAllCards = false;
    bool doListOnly = false;
    bool doEeprom = false;
    bool doRam = false;
    bool doVerify = false;
    char* serialNums = NULL;
    char* cardNums = NULL;
    char* programMode = NULL;
    char* olderVersion = NULL;
    char* mpcieHexFilename = NULL;
    char* m2HexFilename = NULL;
    uint16_t commsAttempts = DEFAULT_FX2_COMMS_ATTEMPTS;
    uint16_t delay = INVALID_ENUMERATION_DELAY;

    const char* p_help_short = \
"- update Sidekiq FX2 firmware";
    const char* p_help_long = "\
Writes FX2 firmware code to FX2 EEPROM and/or RAM. If written to EEPROM\n\
only, the unit will be need to be power cycled in order for new firmware to\n\
be active. If only written to RAM, the firmware will be active upon exit of\n\
this program, but will default to the firmware in EEPROM after power cycle.\n\
If the --verify flag is specified without an EEPROM or RAM write flag, the\n\
the current contents of EEPROM will be verified against the specified file\n\
name (given using the appropriate --mpcie or --m2 parameter).\n\
";

    struct application_argument p_args[] =
    {
        APP_ARG_OPT("list",
                    'l',
                    "Only list and identify all attached cards",
                    NULL,
                    &doListOnly,
                    BOOL_VAR_TYPE),
        APP_ARG_OPT("card",
                    'c',
                    "Comma-separated list of card IDs",
                    "ID",
                    &cardNums,
                    STRING_VAR_TYPE),
        APP_ARG_OPT("serial",
                    's',
                    "Comma-separated list of serial numbers",
                    "NUMBER",
                    &serialNums,
                    STRING_VAR_TYPE),
        APP_ARG_OPT("all",
                    'a',
                    "Program all discovered Sidekiq devices",
                    NULL,
                    &doAllCards,
                    BOOL_VAR_TYPE),
        APP_ARG_OPT("mode",
                    'm',
                    "Programming mode to use, either USB or PCIE",
                    "MODE",
                    &programMode,
                    STRING_VAR_TYPE),
        APP_ARG_OPT("eeprom",
                    'e',
                    "Write FX2's EEPROM",
                    NULL,
                    &doEeprom,
                    BOOL_VAR_TYPE),
        APP_ARG_OPT("verify",
                    'v',
                    "Verify EEPROM contents",
                    NULL,
                    &doVerify,
                    BOOL_VAR_TYPE),
        APP_ARG_OPT("ram",
                    'r',
                    "Write FX2's RAM, requires USB",
                    NULL,
                    &doRam,
                    BOOL_VAR_TYPE),
        APP_ARG_OPT("older",
                    'o',
                    "Program only if older than specified version",
                    "VERSION",
                    &olderVersion,
                    STRING_VAR_TYPE),
        APP_ARG_OPT("wait",
                    'w',
                    "Attempt up to this many tries to establish\n"
                    "communications with the FX2 before giving\n"
                    "up; if 0, do not wait",
                    "ATTEMPTS",
                    &commsAttempts,
                    UINT16_VAR_TYPE),
        APP_ARG_OPT("mpcie",
                    0,
                    "mPCIe firmware hex file",
                    "HEX",
                    &mpcieHexFilename,
                    STRING_VAR_TYPE),
        APP_ARG_OPT("m2",
                    0,
                    "M2 firmware hex file",
                    "HEX",
                    &m2HexFilename,
                    STRING_VAR_TYPE),
        APP_ARG_OPT("delay",
                    0,
                    "set the usb enumeration delay for firmware",
                    "MILLISECONDS",
                    &delay,
                    UINT16_VAR_TYPE),
        APP_ARG_TERMINATOR,
    };

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("%s :");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if ((!doListOnly) && (!doEeprom) && (!doRam) && (!doVerify))
    {
        printf("%s : nothing to program or verify; neither EEPROM, RAM, nor"
               " verify specified\n", __FUNCTION__);
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        goto finished;
    }

    if (NULL != programMode)
    {
        if((0 == strncasecmp(programMode, "PCIE", 5)) ||
           (0 == strncasecmp(programMode, "PCI", 4)))
        {
            config->usePci = true;
            config->useUsb = false;
        }
        else if( 0 == strncasecmp(programMode, "USB", 4) )
        {
            config->usePci = false;
            config->useUsb = true;
        }
        else
        {
            goto finished;
        }
    }

    if (NULL != serialNums) {
        if (doAllCards) {
            printf("%s : -a/--all and -s/--serial are mutually exclusive\n",
                    __FUNCTION__);
            goto finished;
        }
        else if (NULL != cardNums) {
            printf("%s : -c/--card and -s/--serial are mutually exclusive\n",
                    __FUNCTION__);
            goto finished;
        }
        addSerials(&(config->userSerials), (const char *) serialNums);
    }
    else if (NULL != cardNums) {
        if (doAllCards) {
            printf("%s : -a/--all and -c/--card are mutually exclusive\n",
                    __FUNCTION__);
            goto finished;
        }
        // Note: don't need to check if serialNums was used, checked above
        addCards(&(config->userSerials), (const char *) cardNums);
    }

    if ((NULL != olderVersion) &&
        (!getVersionNum(config, (const char *) olderVersion))) {
        printf("%s : '%s' isn't a valid version\n", __FUNCTION__, olderVersion);
        goto finished;
    }

    config->mpcieHexFilename = mpcieHexFilename;
    config->m2HexFilename = m2HexFilename;
    config->enumeration_delay = delay;
    config->listOnly = doListOnly;
    config->loadEeprom = doEeprom;
    config->loadRam = doRam;
    config->verify = doVerify;
    config->all = doAllCards;
    config->commsAttempts = commsAttempts;

    success = true;

finished:
    return (success);
}

static void
critErrorCallback(int32_t status, void *p_user_data)
{
    (void) p_user_data;

    printf("Warning: received critical error from libsidekiq (result code %"
            PRIi32 ")\n", status);
}


/**
    @brief  El Jefe and Herr Reutmann.

    @param[in]  argc    The number of command line arguments.
    @param[in]  argv    The list of command line arguments.

    @return 0 on success, else a number based upon where it fails.
*/
int
main(int argc, char *argv[])
{
    int result = 0;
    bool success = false;
    struct SidekiqCardList *targetSkiqList = NULL;
    struct SidekiqCardList foundSkiqList;
    struct ProgramConfig config;

    displayLibVersion();

    success = initProgramConfig(&config);
    if (!success)
    {
        result = 1;
        goto finished;
    }

    success = parseCmdLine(argc, argv, &config);
    if (!success)
    {
        result = 1;
        goto finished;
    }

    if (!config.listOnly)
    {
        if (!config.all && (0 == config.userSerials.length))
        {
            printf("No Sidekiqs specified!\n");
            result = 1;
            goto finished;
        }

        if (config.mpcieHexFilename)
        {
            result = parseHexFile(config.mpcieHexFilename,
                                  &(config.mpcieHexRecords),
                                  &(config.mpcieFwBuffer),
                                  &(config.mpcieFwLength));
            if (0 > result)
            {
                printf("Couldn't load mPCIe firmware file '%s' (result"
                        " code %d)!\n", config.mpcieHexFilename, result);
                result = 1;
                goto finished;
            }
        }
        if (config.m2HexFilename)
        {
            result = parseHexFile(config.m2HexFilename,
                                  &(config.m2HexRecords),
                                  &(config.m2FwBuffer),
                                  &(config.m2FwLength));
            if (0 > result)
            {
                printf("Couldn't load m.2 firmware file '%s' (result"
                        " code %d)!\n", config.m2HexFilename, result);
                result = 1;
                goto finished;
            }
        }
    }

    /* disable messages */
    skiq_register_logging(NULL);

    /* register an error handler, just in case. */
    skiq_register_critical_error_callback(critErrorCallback, NULL);

    success = skiqListInit(&foundSkiqList);
    if (!success)
    {
        printf("Failed to initialize Sidekiq list... weird.\n");
        result = 1;
        goto finished;
    }

    success = findSkiqs(&foundSkiqList, config.usePci, config.useUsb);
    if (!success)
    {
        printf("Couldn't find any Sidekiqs!\n");
        result = 1;
        goto finished;
    }

    displayCardList(&foundSkiqList);
    if (config.listOnly)
    {
        goto finished;
    }

    if (0 < foundSkiqList.length)
    {
        if (config.all)
        {
            targetSkiqList = &foundSkiqList;
        }
        else
        {
            success = findRequestedSkiqs(&(config.userSerials), &foundSkiqList);
            if (!success)
            {
                printf("Failed to detect requested Sidekiqs!\n");
                result = 1;
                goto finished;
            }
            targetSkiqList = &(config.userSerials);
        }

        if (config.loadEeprom || config.loadRam || config.verify)
        {
            printf("Programming and/or verifying cards...\n");
            success = processCardList(targetSkiqList, &config);
            if (!success)
            {
                printf("Failed to program and/or verify requested Sidekiqs!\n");
                result = 1;
                goto finished;
            }

            printf("Programming and/or verifying succeeded!\n");
        }
    }
    else if ( 0 == foundSkiqList.length )
    {
        if (!config.all)
        {
            /* !config.all implies that the user specified a serial number, so
               surely the user wanted to do something, right?  fail with an
               non-zero exit code */
            printf("Failed to perform action, no cards found\n");
            result = 1;
        }
    }

finished:
    printf("Done.\n");

    // Memory management, fun stuff.
    if (NULL != config.mpcieFwBuffer)
    {
        intel_cleanRecordList(&(config.mpcieHexRecords));
        free(config.mpcieFwBuffer);
    }
    if (NULL != config.m2FwBuffer)
    {
        intel_cleanRecordList(&(config.m2HexRecords));
        free(config.m2FwBuffer);
    }

    return result;
}

