/*! \file reg_test.c
 * \brief This file contains a basic application that reads, writes
 * and verifies the registers in the Sidekiq FPGA.
 *  
 * <pre>
 * Copyright 2022 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "bit_ops.h"
#include "sidekiq_xport.h"
#include "sidekiq_api.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

static void print_usage(void);

static int32_t test_registers( uint8_t card,
                               uint32_t addr[],
                               uint32_t num_addr,
                               bool use_verify )
{
    int32_t status = 0;
    uint32_t original_val = 0;
    uint32_t random=0;
    uint32_t val=0;
    uint32_t i;

    for (i = 0; (i < num_addr) && (status == 0); i++)
    {
        if ( ( status = skiq_read_user_fpga_reg(card, addr[i], &original_val) ) != 0 )
        {
            fprintf(stderr, "Error: failed to read current value from reg 0x%04x\n", addr[i]);
            goto complete;
        }

        printf("Info: Verifying register 0x%08x\n", addr[i]);

        /* POSIX tells me that rand() is only good between 0 and RAND_MAX (assumed to be 32767).
         * Instead, use mrand48() which returns signed long integers uniformly distributed over the
         * interval [-2^31, 2^31) */
        random = mrand48();

        if( use_verify == false )
        {
            if( (status=skiq_write_user_fpga_reg(card, addr[i], random)) != 0 )
            {
                fprintf(stderr, "Error: failed to write 0x%08x to reg 0x%08x\n", random, addr[i]);
                goto complete;
            }
            /* need to make sure the write actually happened before the read is checked */
            if( (status=sidekiq_fpga_reg_verify(card, addr[i], random)) != 0 )
            {
                fprintf(stderr, "Error: register verify failed for reg 0x%08x\n", addr[i]);
                goto complete;
            }
            /* note: we don't really need to do the read here since we did the verify
               above, but it's good to make sure the read API is working, so leave it in */
            if( (status=skiq_read_user_fpga_reg(card, addr[i], &val)) != 0 )
            {
                fprintf(stderr, "Error: failed to read from reg 0x%08x\n", addr[i]);
                goto complete;
            }
            if( random != val )
            {
                status = -EIO;
                fprintf(stderr, "Error: Value (0x%08x) in register differs from written (0x%08x) "
                        "for reg 0x%08x\n", val, random, addr[i]);
                goto complete;
            }
        }
        else
        {
            if( (status=skiq_write_and_verify_user_fpga_reg(card, addr[i], random)) != 0 )
            {
                fprintf(stderr, "Error: unable to verify user FPGA register 0x%08x, status=%d\n",
                       addr[i], status);
                goto complete;
            }
        }

        if ( ( status = skiq_write_and_verify_user_fpga_reg(card, addr[i], original_val) ) != 0 )
        {
            fprintf(stderr, "Error: failed to restore original value 0x0%08x to reg 0x%04x\n",
                    original_val, addr[i]);
        }
        else
        {
            printf("Info: Restored original value 0x%08x to reg 0x%04x\n", original_val, addr[i]);
        }
    }

complete:
    return status;
}

/*****************************************************************************/
/** This is the main function for executing user_reg_verify commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    uint8_t card=0;
    int32_t status=0;
    uint32_t addr_at_level_1[] = { 0x8700, 0x8704 };
    uint32_t addr_at_level_2[] = { 0x8700, 0x8704, 0x8800, 0x8804 };
    uint32_t sample_rates[2] = {0};  //query the card for mix/max sample rates.
    
    skiq_rx_hdl_t curr_rx_hdl = skiq_rx_hdl_A1;
    uint32_t read_bandwidth;
    uint32_t actual_bandwidth;
    uint32_t read_sample_rate;
    double actual_sample_rate;
    skiq_param_t skiq_params;
    
    uint32_t i;
    if( argc != 2 )
    {
        fprintf(stderr, "Error: invalid # args\n");
        print_usage();
        return (-1);
    }

    card = (uint8_t)(strtoul(argv[1],NULL,10));
    
    /* seed the RNG with the current time */
    srand(time(NULL));

    /* bring up the minimal FPGA interface to allow FPGA register
       reads/writes to work */
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_basic, &card, 1);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to initialize libsidekiq with status %d\n", status);
        goto init_error;
    }
    
    status = test_registers( card, addr_at_level_1, ARRAY_SIZE( addr_at_level_1 ), false );
    if ( status != 0 )
    {
        fprintf(stderr, "Error: Register test at level 1 failed with status %d\n", status);
        goto exit_with_error;
    }

    skiq_exit();

     /* bring up the full FPGA interface to allow FPGA register reads/writes
            to work */
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to initialize libsidekiq with status %d\n", status);
        goto init_error;
    }

    if ( status == 0 )
    {
        status = skiq_read_parameters(card,&skiq_params);
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to read skiq parameters with status %d\n", status);
        }
    }

    if ( status == 0 )
    {
        sample_rates[0] = skiq_params.rx_param[curr_rx_hdl].sample_rate_min;
        sample_rates[1] = skiq_params.rx_param[curr_rx_hdl].sample_rate_max;
    }

    for(i=0; (i < ARRAY_SIZE(sample_rates)) && (status == 0); i++)
    {
        printf("Info: Configuring Rx sample rate and bandwidth to %u samples per second and %u "
               "Hz\n", sample_rates[i], sample_rates[i] );
        status=skiq_write_rx_sample_rate_and_bandwidth(card, curr_rx_hdl, sample_rates[i],
                                                        sample_rates[i]);
        if (status != 0)
        {
            fprintf(stderr, "Warning: failed to set Rx sample rate or bandwidth with status %d\n",
                    status);
        }

        status=skiq_read_rx_sample_rate_and_bandwidth(card,
                                                        curr_rx_hdl,
                                                        &read_sample_rate,
                                                        &actual_sample_rate,
                                                        &read_bandwidth,
                                                        &actual_bandwidth);
        if( status == 0 )
        {
            printf("Info: actual sample rate is %f, actual bandwidth is %u\n",
                    actual_sample_rate, actual_bandwidth);
        }

        status = test_registers( card, addr_at_level_2, ARRAY_SIZE( addr_at_level_2 ), true );
        if ( status != 0 )
        {
            fprintf(stderr, "Error: Register test at level 2 failed with status %d\n", status);
        }
    }

exit_with_error:
    skiq_exit();

init_error:
    return (status);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
void print_usage(void)
{
    printf("Usage: user_reg_verify <card> \n");
    printf("   writes to the FPGA user registers of the specified Sidekiq card\n");
    printf("   and verifies the register reads back correctly\n");
}
