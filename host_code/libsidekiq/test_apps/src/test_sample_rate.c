/*! \file test_sample_rate.c
 * \brief This file contains a basic application that
 * determines the actual sample rate setting based on the requested rate
 *
 * <pre>
 * Copyright 2017 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */
#include <stdio.h>
#include <errno.h>
#include <inttypes.h>
#include <locale.h>

#include <sidekiq_api.h>

#include <calc_ad9528.h>
#include <t_ad9528.h>
#include <fpga_jesd_ctrl.h>
#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- determine actual sample rate\n";
static const char* p_help_long = "\
Determines the actual sample rate based on the requested rate\n\
\n\
Defaults:\n\
    --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
";

static uint32_t req_sample_rate=0;
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_REQ("sample_rate",
                'r',
                "Requested sample rate in Hz",
                "Hz",
                &req_sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),

    APP_ARG_TERMINATOR,
};

static const clk_sel_t _unused_clk_sel =
{
    // just set these to max for undefined/invalid
    .fpga_clk_sel_mask = AD9528_GBT_CLK_OUT_ALL,
    .debug_clk_sel = AD9528_MAX_CLOCK_OUTS,
    .rffc1_clk_sel = AD9528_MAX_CLOCK_OUTS,
    .rffc2_clk_sel = AD9528_MAX_CLOCK_OUTS,
};

int main( int argc, char *argv[] )
{
    int32_t status=0;
    uint32_t actual_sample_rate;
    ad9528pll1Settings_t pll1;
    ad9528pll2Settings_t pll2;
    ad9528outputSettings_t output;
    ad9528sysrefSettings_t sysref;
    clk_div_t fpga_div = CLK_DIV_INVALID;
    uint32_t dev_clk = 0;
    clk_div_t dev_clk_div = CLK_DIV_INVALID;
    uint32_t rffc_freq=0;
    uint64_t fpga_min_freq = 0;
    uint64_t fpga_max_freq = 0;
    uint64_t fpga_gbt_clock = 0;
    pid_t owner = 0;

    /* set locale so that numbers can be printed with commas */
    setlocale(LC_NUMERIC, "");

    /* initialize everything based on the arguments provided */
    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    // TODO: handle all product types...assuming X2/X4
    /* initialize Sidekiq cards */
    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }
        return (-1);
    }


    /* check to see if this card is a Sidekiq X2/X4, bail otherwise */

    skiq_param_t params;

    status = skiq_read_parameters( card, &params );
    if ( 0 == status )
    {
        if ( !((params.card_param.part_type == skiq_x2 ) || ( params.card_param.part_type == skiq_x4 )) )
        {
            fprintf(stderr, "Error: this test application is for use with Sidekiq X2/X4 to "
                    "provide some guidance on how to choose the DEV_CLK for a specified "
                    "sample rate\n");
            return (-1);
        }
    }

    fpga_jesd_qpll_freq_range( card, &fpga_min_freq, &fpga_max_freq );

    status = calc_ad9528_for_sample_rate( req_sample_rate,
                                          &actual_sample_rate,
                                          &pll1,
                                          &pll2,
                                          &output,
                                          &sysref,
                                          &fpga_div,
                                          &dev_clk,
                                          &dev_clk_div,
                                          &rffc_freq,
                                          fpga_min_freq,
                                          fpga_max_freq,
                                          &fpga_gbt_clock,
                                          _unused_clk_sel,
                                          params.card_param.part_type );
    if( status == 0 )
    {
        if( req_sample_rate != actual_sample_rate )
        {
            fprintf( stderr, "Warning: unable to achieve exact sample rate; requested=%" PRIu32
                     " Hz and actual=%" PRIu32 " Hz (difference=%" PRIi32 " Hz)\n",
                     req_sample_rate,
                     actual_sample_rate,
                     ((int32_t)(req_sample_rate) - (int32_t)(actual_sample_rate)) );
        }
        else
        {
            printf("Info: exact sample rate requested (%u Hz) is possible!\n", req_sample_rate);
        }

        // customer needs to know dev clock and dev clock divider to create profile
        printf("\nInfo: Use dev clock frequency %u Hz with a divider of %u\n\n", dev_clk, dev_clk_div);
    }
    else
    {
        fprintf(stderr, "Error: failed to get sample rate (result code %" PRIi32 ")\n", status);
    }

    skiq_exit();

    return ((int) status);
}

