/*! \file timestamp_test.c
 * \brief This file contains a basic application that checks the timestamps
 * after updating the sample rate
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <inttypes.h>
#include <math.h>

#include <sidekiq_api.h>
#include <sidekiq_hal.h>
#include "arg_parser.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#define TOT_NUM_BLOCKS                  10000
#define DEFAULT_CARD                    0
#define START_SAMPLE_RATE               12000000
#define STOP_SAMPLE_RATE                13000000
#define STEP_SAMPLE_RATE                1000000
#define DEFAULT_HANDLE                  "A1"

#define NUM_PAYLOAD_WORDS_IN_BLOCK      (SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS - SKIQ_RX_HEADER_SIZE_IN_WORDS)
#define NUM_SAMPLES_IN_BLOCK            (NUM_PAYLOAD_WORDS_IN_BLOCK)
#define NUM_SAMPLES_IN_PACKED_BLOCK     (SKIQ_NUM_PACKED_SAMPLES_IN_BLOCK(NUM_SAMPLES_IN_BLOCK-1))

/* command line argument variables */
static uint32_t tot_num_blocks = TOT_NUM_BLOCKS;
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static uint32_t repeat = 0;
static uint32_t start_sample_rate = START_SAMPLE_RATE;
static uint32_t stop_sample_rate = STOP_SAMPLE_RATE;
static uint32_t step_sample_rate = STEP_SAMPLE_RATE;
static bool blocking_rx = false;
static bool packed = false;
static char* p_hdl = DEFAULT_HANDLE;
static bool exit_on_fail = false;
static bool validate = false;
static bool verbose = false;

/* global variables */
static bool running = true;

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- sweep sample rates and check timestamps";
static const char* p_help_long =
    "Accumulates the number of blocks specified and verifies the timestamp. Updates\n"
    "the sample rate and verifies the timestamp increments appropriately.\n"
    "\n"
    "Defaults:\n"
    "  --blocks=" xstr(TOT_NUM_BLOCKS) "\n"
    "  --card="   xstr(DEFAULT_CARD) "\n"
    "  --start="  xstr(START_SAMPLE_RATE) "\n"
    "  --stop="   xstr(STOP_SAMPLE_RATE) "\n"
    "  --step="   xstr(STEP_SAMPLE_RATE) "\n"
    "  --handle=" DEFAULT_HANDLE;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("blocks",
                0,
                "Number of Rx sample blocks to acquire",
                "N",
                &tot_num_blocks,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("repeat",
                0,
                "Sweep sample rates an additional N times",
                "N",
                &repeat,
                INT32_VAR_TYPE),
    APP_ARG_OPT("start",
                0,
                "Starting sample rate",
                "Hz",
                &start_sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("stop",
                0,
                "Ending sample rate",
                "Hz",
                &stop_sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("step",
                0,
                "Sample rate step size",
                "Hz",
                &step_sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("blocking",
                0,
                "Perform blocking during skiq_receive call",
                NULL,
                &blocking_rx,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("packed",
                0,
                "Use packed mode for I/Q samples",
                NULL,
                &packed,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("exit-on-fail",
                0,
                "Exit the application on first failure",
                NULL,
                &exit_on_fail,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("counter-validation",
                0,
                "Perform counter data validation",
                NULL,
                &validate,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("handle",
                0,
                "Rx handle to use: one of A1, A2, B1",
                "Rx",
                &p_hdl,
                STRING_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Display individual timestamp errors",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

struct sr_bw {
    uint32_t sample_rate, bandwidth;
};

static struct sr_bw x2_discrete_a1a2_configs[] = {
    { 480000, 480000, },
    { 576000, 576000, },
    { 600000, 600000, },
    { 781250, 781250, },
    { 960000, 960000, },
    { 1152000, 1152000, },
    { 1200000, 1200000, },
    { 1562500, 1562500, },
    { 1920000, 1920000, },
    { 2304000, 2304000, },
    { 2400000, 2400000, },
    { 3125000, 3125000, },
    { 3840000, 3840000, },
    { 4608000, 4608000, },
    { 4800000, 4800000, },
    { 6250000, 6250000, },
    { 7680000, 7680000, },
    { 9216000, 9216000, },
    { 9600000, 9600000, },
    { 12500000, 12500000, },
    { 15360000, 12300000, },
    { 15360000, 15360000, },
    { 18432000, 18432000, },
    { 19200000, 15360000, },
    { 19200000, 19200000, },
    { 25000000, 20000000, },
    { 25000000, 25000000, },
    { 30720000, 12300000, },
    { 30720000, 18000000, },
    { 30720000, 20000000, },
    { 30720000, 25000000, },
    { 30720000, 30720000, },
    { 36864000, 30228000, },
    { 36864000, 36864000, },
    { 38400000, 15360000, },
    { 38400000, 25000000, },
    { 50000000, 20000000, },
    { 50000000, 41000000, },
    { 61440000, 25000000, },
    { 61440000, 50000000, },
    { 73728000, 30228000, },
    { 73728000, 60456000, },
    { 100000000, 82000000, },
    { 122880000, 61440000, },
    { 122880000, 64000000, },
    { 122880000, 72000000, },
    { 122880000, 100000000, },
    { 153600000, 100000000, },
    { 0, 0 },
};

static struct sr_bw x2_discrete_b1_configs[] = {
    { 50000000, 41000000 },
    { 61440000, 25000000 },
    { 61440000, 50000000 },
    { 73728000, 30228000 },
    { 73728000, 60456000 },
    { 100000000, 82000000 },
    { 122880000, 100000000 },
    { 153600000, 100000000 },
#if (!defined __MINGW32__)
    { 245760000, 100000000 },
    { 245760000, 200000000 },
#endif
    { 0, 0 },
};

static struct sr_bw *x2_discrete_configs[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = x2_discrete_a1a2_configs,
    [skiq_rx_hdl_A2] = x2_discrete_a1a2_configs,
    [skiq_rx_hdl_B1] = x2_discrete_b1_configs,
};

static struct sr_bw x4_discrete_a1a2_configs[] = {
   
    { 781250, 781250 },
    { 960000, 960000 },
    { 1152000, 1152000 },
    { 1200000, 1200000 },
    { 1562500, 1562500 },
    { 1920000, 1920000 },
    { 2304000, 2304000 },
    { 2400000, 2400000 },
    { 3125000, 3125000 },
    { 3840000, 3840000 },
    { 4608000, 4608000 },
    { 4800000, 4800000 },
    { 6250000, 6250000 },
    { 7680000, 7680000 },
    { 9216000, 9216000 },
    { 9600000, 9600000 },
    { 12500000, 12500000 },
    { 15360000, 15360000 },
    { 18432000, 18432000 },
    { 19200000, 19200000 },
    { 25000000, 20000000 },
    { 25000000, 25000000 },
    { 30720000, 25000000 },
    { 30720000, 30720000 },
    { 36864000, 30228000 },
    { 36864000, 36864000 },
    { 38400000, 30720000 },
    { 50000000, 20000000 },
    { 50000000, 41000000 },
    { 50000000, 50000000 },
    { 61440000, 25000000 },
    { 61440000, 50000000 },
    { 61440000, 61440000 },
    { 73728000, 30228000 },
    { 73728000, 60456000 },
    { 76800000, 30720000 },
    { 100000000, 82000000 },
    { 122880000, 61440000 },
    { 122880000, 64000000 },
    { 122880000, 72000000 },
    { 122880000, 100000000 },
    { 153600000, 100000000 },

#if (!defined __MINGW32__)

    { 200000000, 164000000 },
    { 245760000, 100000000 },
    { 245760000, 200000000 },
    { 250000000, 100000000 },
    { 250000000, 200000000 },
#endif
    { 0, 0 },
};

static struct sr_bw x4_discrete_b1b2_configs[] = {
    { 50000000, 20000000 },
    { 50000000, 41000000 },
    { 61440000, 25000000 },
    { 61440000, 50000000 },
    { 73728000, 30228000 },
    { 73728000, 60456000 },
    { 76800000, 30720000 },
    { 100000000, 82000000 },
    { 122880000, 61440000 },
    { 122880000, 64000000 },
    { 122880000, 72000000 },
    { 122880000, 100000000 },
    { 153600000, 100000000 },
#if (!defined __MINGW32__)
    { 200000000, 164000000 },
    { 245760000, 100000000 },
    { 245760000, 200000000 },
    { 250000000, 100000000 },
    { 250000000, 200000000 },
#endif
    { 0, 0 },
};

static struct sr_bw x4_discrete_c1d1_configs[] = {
    { 50000000, 20000000 },
    { 50000000, 41000000 },
    { 61440000, 25000000 },
    { 61440000, 50000000 },
    { 73728000, 30228000 },
    { 73728000, 60456000 },
    { 76800000, 30720000 },
    { 100000000, 82000000 },
    { 122880000, 61440000 },
    { 122880000, 64000000 },
    { 122880000, 72000000 },
    { 122880000, 100000000 },
    { 153600000, 100000000 },
#if (!defined __MINGW32__)
    { 200000000, 164000000 },
    { 245760000, 100000000 },
    { 245760000, 200000000 },
    { 250000000, 100000000 },
    { 250000000, 200000000 },
    { 491520000, 400000000 },
    { 491520000, 450000000 },
    { 500000000, 400000000 },
    { 500000000, 450000000 },
#endif
    { 0, 0 },
};

static struct sr_bw *x4_discrete_configs[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = x4_discrete_a1a2_configs,
    [skiq_rx_hdl_A2] = x4_discrete_a1a2_configs,
    [skiq_rx_hdl_B1] = x4_discrete_b1b2_configs,
    [skiq_rx_hdl_B2] = x4_discrete_b1b2_configs,
    [skiq_rx_hdl_C1] = x4_discrete_c1d1_configs,
    [skiq_rx_hdl_D1] = x4_discrete_c1d1_configs,
};

static struct sr_bw nv100_discrete_a1b1_configs[] = {
    {     541667,     230883 },
    {    1400000,    1200000 },
    {    1920000,     960000 },
    {    2457600,    1843200 },
    {    2800000,    1200000 },
    {    3840000,    2000000 },
    {    4000000,    1200000 },
    {    4915200,    1228800 },
    {    5600000,    1200000 },
    {    7680000,    4000000 },
    {    9830400,    9338880 },
    {   10000000,    8000000 },
    {   11200000,    1200000 },
    {   11200000,   10000000 },
    {   15360000,    9000000 },
    {   16000000,    2000000 },
    {   20000000,   16000000 },
    {   21666665,    4116666 },
    {   22000000,   16000000 },
    {   23040000,   13500000 },
    {   30720000,   18000000 },
    {   40000000,   38000000 },
    {   61440000,   30720000 },

    { 0, 0 },
};

static struct sr_bw *nv100_discrete_configs[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = nv100_discrete_a1b1_configs,
    [skiq_rx_hdl_B1] = nv100_discrete_a1b1_configs,
};

static struct sr_bw **discrete_configs = NULL;

static void verify_data(int16_t *p_last_data, int16_t p_data[], uint32_t nr_samples);
static void print_block_contents(skiq_rx_block_t* p_block, uint32_t block_size_in_bytes);

static void sigint_function(int signo)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signo);
    running=false;
}


/*************************************************************************************************/
/* sets, then verifies that the actual sample rate and bandwidth match what was requested */
static int32_t set_and_check_sample_rate( uint8_t card_index,
                                          skiq_rx_hdl_t hdl,
                                          uint32_t sample_rate,
                                          uint32_t bandwidth )
{
    int32_t status;

    status = skiq_write_rx_sample_rate_and_bandwidth( card_index, hdl, sample_rate, bandwidth );
    if ( status == 0 )
    {
        uint32_t configured_rate, configured_bandwidth, actual_bandwidth;
        double actual_rate;

        status = skiq_read_rx_sample_rate_and_bandwidth( card_index, hdl, &configured_rate,
                                                         &actual_rate, &configured_bandwidth,
                                                         &actual_bandwidth );
        if ( status != 0 )
        {
            fprintf(stderr, "Error: unable to read the sample rate and bandwidth\n");
        }
        else
        {
            if ( (uint32_t)actual_rate != sample_rate )
            {
                status = -1;
                fprintf(stderr, "Error: requested sample rate not match (%u != %u)\n",
                        (uint32_t)actual_rate, sample_rate);
            }

            if ( actual_bandwidth != bandwidth )
            {
                status = -1;
                fprintf(stderr, "Error: requested RF bandwidth not match (%u != %u)\n",
                        actual_bandwidth, bandwidth);
            }
        }
    }

    return status;
}


/*****************************************************************************/
/** This is the main function for executing timestamp_test application.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    bool first_block = true;
    bool use_discrete = false;
    uint64_t curr_ts = 0, next_ts = 0;
    uint64_t sys_ts_freq;
    skiq_rx_block_t *p_rx_block;
    uint32_t len;

    uint32_t num_blocks = 0;
    uint32_t count = 0;
    int32_t status=0;
    skiq_rx_status_t rx_status;

    skiq_chan_mode_t chan_mode;
    skiq_rx_hdl_t hdl;

    uint32_t curr_rate = 0, curr_bw = 0, curr_config_index = 0;
    uint32_t loop_count=0;
    pid_t owner = 0;

    signal( SIGINT, sigint_function );

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if ( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    // packed validation is not currently supported
    if ( packed && validate )
    {
        fprintf(stderr, "Error: Packed validation not currently supported!\n");
        return (-1);
    }

    /* map argument values to sidekiq specific variable values */
    if( 0 == strcasecmp(p_hdl, "A1") )
    {
        hdl = skiq_rx_hdl_A1;
        chan_mode = skiq_chan_mode_single;
    }
    else if( 0 == strcasecmp(p_hdl, "A2") )
    {
        hdl = skiq_rx_hdl_A2;
        chan_mode = skiq_chan_mode_dual;
    }
    else if( 0 == strcasecmp(p_hdl, "B1") )
    {
        hdl = skiq_rx_hdl_B1;
        chan_mode = skiq_chan_mode_single;
    }
    else if( 0 == strcasecmp(p_hdl, "B2") )
    {
        hdl = skiq_rx_hdl_B2;
        chan_mode = skiq_chan_mode_single;
    }
    else if( 0 == strcasecmp(p_hdl, "C1") )
    {
        hdl = skiq_rx_hdl_C1;
        chan_mode = skiq_chan_mode_single;
    }
    else if( 0 == strcasecmp(p_hdl, "D1") )
    {
        hdl = skiq_rx_hdl_D1;
        chan_mode = skiq_chan_mode_single;
    }
    else
    {
        fprintf(stderr, "Error: invalid handle specified\n");
        return (-1);
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    /* initialize Sidekiq cards */
    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        return (-1);
    }

    /* check to see if the card is a Sidekiq X2 or X4, then use the discrete list of sample rates /
     * bandwidths */
    {
        skiq_param_t params;
        status = skiq_read_parameters( card, &params );
        if ( status != 0 )
        {
            fprintf(stderr, "Error: unable to read Sidekiq card parameters with status %d\n",
                    status);
            goto exit_with_error;
        }
        else
        {
            if ( params.card_param.part_type == skiq_x2 )
            {
                use_discrete = true;
                discrete_configs = x2_discrete_configs;
            }
            else if ( params.card_param.part_type == skiq_x4 )
            {
                use_discrete = true;
                discrete_configs = x4_discrete_configs;
            }
            else if ( params.card_param.part_type == skiq_nv100 )
            {
                use_discrete = true;
                discrete_configs = nv100_discrete_configs;
            }
        }
    }

    if ( use_discrete && ( ( discrete_configs == NULL ) || ( discrete_configs[hdl] == NULL ) ) )
    {
        status = -EINVAL;
        fprintf(stderr, "Error: unable to determine a set of discrete configurations for hdl %u\n",
                hdl);
        goto exit_with_error;
    }

    if ( blocking_rx )
    {
        /* set a modest rx timeout */
        status = skiq_set_rx_transfer_timeout( card, 10000 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to set RX transfer timeout with status %d\n", status);
            goto exit_with_error;
        }
    }

    /* set the mode (packed or unpacked) */
    if ( ( status = skiq_write_iq_pack_mode( card, packed ) ) != 0 )
    {
        if ( status == -ENOTSUP )
        {
            fprintf(stderr, "Error: packed mode is not supported on this Sidekiq product\n");
        }
        else
        {
            fprintf(stderr, "Error: unable to set the packed mode with status %d\n", status);
        }
        goto exit_with_error;
    }

    if ( ( status = skiq_write_chan_mode( card, chan_mode ) ) != 0 )
    {
        fprintf(stderr, "Error: unable to configure channel mode with status %d\n", status);
        goto exit_with_error;
    }

    if ( ( status = skiq_write_rx_data_src( card, hdl, skiq_data_src_counter ) ) != 0 )
    {
        fprintf(stderr, "Error: unable to configure data source with status %d\n", status);
        goto exit_with_error;
    }

    printf("Info: starting Rx interface\n");

    if ( ( status = skiq_start_rx_streaming( card, hdl ) ) != 0 )
    {
        fprintf(stderr, "Error: unable to start receive streaming with status %d\n", status);
        goto exit_with_error;
    }

    do
    {
        if ( !use_discrete )
        {
            curr_rate = start_sample_rate;
        }
        loop_count++;
        fprintf(stderr, "Starting iteration %u\n", loop_count);

        // loop through the sample rates
        while ( running &&
                ( ( use_discrete && (discrete_configs[hdl][curr_config_index].sample_rate != 0) ) ||
                  ( !use_discrete && (curr_rate <= stop_sample_rate)) ) )
        {
            uint64_t fail_count = 0, sys_ts_fail_count = 0;
            uint64_t last_sys_ts = 0;
            double sys_ts_delta;
            int16_t last_data;

            if ( use_discrete )
            {
                curr_rate = discrete_configs[hdl][curr_config_index].sample_rate;
                curr_bw = discrete_configs[hdl][curr_config_index].bandwidth;
            }
            else
            {
                curr_bw = curr_rate;
            }

            fprintf(stderr, "Info: updating sample rate to %u and bandwidth to %u\n",
                    curr_rate, curr_bw );
            status = set_and_check_sample_rate( card, hdl, curr_rate, curr_bw );
            if ( status != 0 )
            {
                goto exit_with_error;
            }

            /* update the cached system timestamp frequency since it can change after a sample rate
             * change (for some Sidekiq products) */
            if ( ( status = skiq_read_sys_timestamp_freq( card, &sys_ts_freq ) ) != 0 )
            {
                fprintf(stderr, "Error: unable to read the system timestamp frequency\n");
                goto exit_with_error;
            }
            else
            {
                fprintf(stderr, "Info: system timestamp frequency is %" PRIu64 "\n", sys_ts_freq);
            }

            num_blocks = 0;
            last_sys_ts = 0;
            first_block = true;

            /* calculate the expected system timestamp delta for the current sample rate */
            if ( packed )
            {
                sys_ts_delta = NUM_SAMPLES_IN_PACKED_BLOCK * ( (double) sys_ts_freq / (double) curr_rate );
            }
            else
            {
                sys_ts_delta = NUM_SAMPLES_IN_BLOCK * ( (double) sys_ts_freq / (double) curr_rate );
            }
            fprintf(stderr, "Info: expected system timestamp delta between %.0f and %.0f\n",
                    floor(sys_ts_delta - 1), ceil(sys_ts_delta + 1));

            // receive the number of blocks specified
            while( (num_blocks < tot_num_blocks) && (running==true) )
            {
                skiq_rx_hdl_t rx_hdl;

                rx_status = skiq_receive(card, &rx_hdl, &p_rx_block, &len);
                if ( ( rx_status == skiq_rx_status_success ) && ( hdl == rx_hdl ) )
                {
                    if ( p_rx_block != NULL )
                    {
                        // grab the timestamp
                        curr_ts = p_rx_block->rf_timestamp;
                        if ( first_block )
                        {
                            // reset last data
                            last_data = p_rx_block->data[0];
                            first_block = false;
                            if( curr_ts < next_ts )
                            {
                                fprintf(stderr, "Error: Timestamp on new sample rate 0x%016" PRIx64
                                        " , next was 0x%016" PRIx64 "\n",
                                        curr_ts, next_ts);
                                status = -1;
                                goto stop_streaming_with_error;
                            }

                            if ( validate )
                            {
                                verify_data(&last_data, (int16_t *)p_rx_block->data, NUM_SAMPLES_IN_BLOCK);
                            }

                            next_ts = curr_ts;
                            next_ts += (packed) ? NUM_SAMPLES_IN_PACKED_BLOCK : NUM_SAMPLES_IN_BLOCK;
                        }
                        // check for a sample (RF) timestamp error
                        else if( curr_ts != next_ts )
                        {
                            if ( verbose || exit_on_fail )
                            {
                                fprintf(stderr, "Error: timestamp error expected 0x%016" PRIx64 \
                                        " but got 0x%016" PRIx64 ". block %u, count %u diff %" \
                                        PRId64 " , card %u\n",
                                        next_ts, curr_ts, num_blocks,
                                        count, (int64_t)curr_ts - (int64_t)next_ts, card);
                            }

                            if ( exit_on_fail )
                            {
                                print_block_contents( p_rx_block, len );
                                status = -1;
                                goto stop_streaming_with_error;
                            }
                            else
                            {
                                first_block = true;
                                last_sys_ts = 0;
                                fail_count++;
                            }
                        }
                        /* sample (RF) timestamp is correct, now check the system timestamp */
                        else
                        {
                            int64_t sys_ts_diff = (int64_t) p_rx_block->sys_timestamp - (int64_t) last_sys_ts;

                            /* special case check for a zero system timestamp */
                            if ( p_rx_block->sys_timestamp == 0 )
                            {
                                if ( verbose || exit_on_fail )
                                {
                                    fprintf(stderr, "Warning: System timestamp is zero and sample timestamp is %"
                                            PRIu64 "\n", p_rx_block->rf_timestamp);
                                }

                                if ( exit_on_fail )
                                {
                                    print_block_contents( p_rx_block, len );
                                    status = -1;
                                    goto stop_streaming_with_error;
                                }
                            }

                            /* check that system timestamp difference is in-bounds */
                            if ( last_sys_ts > 0 )
                            {
                                /* the accepted bounds are +/- 2 since a unit may not have
                                   its reference calibrated (warp voltage) */
                                if ( ( sys_ts_diff > ceil(sys_ts_delta + 2) ) || ( sys_ts_diff < floor(sys_ts_delta - 2) ) )
                                {
                                    if ( verbose || exit_on_fail )
                                    {
                                        fprintf(stderr, "Error: System timestamp error, expected delta between %.0f"
                                                " and %.0f" " but got %" PRId64 " on block %u, count %u, card %u\n",
                                                floor(sys_ts_delta - 1), ceil(sys_ts_delta + 1), sys_ts_diff, num_blocks, count, card);
                                    }

                                    if ( exit_on_fail )
                                    {
                                        print_block_contents( p_rx_block, len );
                                        status = -1;
                                        goto stop_streaming_with_error;
                                    }
                                    else
                                    {
                                        last_sys_ts = 0;
                                        sys_ts_fail_count++;
                                    }
                                }
                            }
                            last_sys_ts = p_rx_block->sys_timestamp;

                            // validate data if requested
                            if ( validate && ( curr_ts == next_ts ) )
                            {
                                verify_data(&last_data, (int16_t *)p_rx_block->data, NUM_SAMPLES_IN_BLOCK);
                            }

                            /* increment next expected timestamp */
                            next_ts += (packed) ? NUM_SAMPLES_IN_PACKED_BLOCK : NUM_SAMPLES_IN_BLOCK;
                        }
                        num_blocks++;
                    }
                }
            }

            count++;
            if ( use_discrete )
            {
                curr_config_index++;
            }
            else
            {
                curr_rate += step_sample_rate;
            }

            printf( "Info: %" PRIu64 " sample (RF) timestamp errors and %"
                    PRIu64 " system timestamp errors over %" PRIu32
                    " blocks\n", fail_count, sys_ts_fail_count, tot_num_blocks );
        }
    } while ( running && (loop_count <= repeat) );

    printf("Exiting...\n");

stop_streaming_with_error:
    skiq_stop_rx_streaming(card, hdl);

exit_with_error:
    skiq_exit();

    return status;
}


/*****************************************************************************/
/** This function verifies that the received sample data is a monotonically
    increasing counter.  At the conclusion of the function, *p_last_data will be
    assigned to the next logical value for the next block.

    @param p_last_data: a pointer to the expected data value
    @param p_data: a pointer to the first 16-bit sample in the buffer to be verified
    @param nr_samples: the number of samples to verify
*/
static void verify_data( int16_t *p_last_data,
                         int16_t p_data[],
                         uint32_t nr_samples )
{
    uint8_t rx_resolution;
    int16_t max_data=0;
    uint32_t i;

    skiq_read_rx_iq_resolution( card, &rx_resolution );
    max_data = (1 << (rx_resolution-1))-1;

    for (i = 0; i < 2 * nr_samples; i++)
    {
        if ( *p_last_data != p_data[i] )
        {
            fprintf(stderr, "Error: at sample %u, expected 0x%04x but got 0x%04x\n", i, *p_last_data, p_data[i]);
            skiq_exit();
            exit(-1);
        }
        *p_last_data = p_data[i]+1;

        /* see if the counter needs to be wrapped */
        if ( *p_last_data == (max_data + 1) )
        {
            *p_last_data = -(max_data + 1);
        }
    }
}


/*****************************************************************************/
/** This function prints contents of raw data

    @param p_data: a reference to raw bytes
    @param length: the number of bytes references by p_data
    @return: void
*/
static void hex_dump(uint8_t* p_data, uint32_t length)
{
    int i, j;
    uint8_t c;

    for (i = 0; i < length; i += 16)
    {
        /* print offset */
        printf("%06X:", i);

        /* print HEX */
        for (j = 0; j < 16; j++)
        {
            if ( ( j % 2 ) == 0 ) printf(" ");
            if ( ( j % 8 ) == 0 ) printf(" ");
            if ( i + j < length )
                printf("%02X", p_data[i + j]);
            else
                printf("  ");
        }
        printf("    ");

        /* print ASCII (if printable) */
        for (j = 0; j < 16; j++)
        {
            if ( ( j % 8 ) == 0 ) printf(" ");
            if ( i + j < length )
            {
                c = p_data[i + j];
                if ( 0 == isprint(c) )
                    printf(".");
                else
                    printf("%c", c);
            }
        }
        printf("\n");
    }
}


/*****************************************************************************/
/** This function prints contents of a receive block

    @param p_block: a reference to the receive block
    @param num_samples: the # of 32-bit samples to print
    @return: void
*/
static void print_block_contents(skiq_rx_block_t* p_block, uint32_t block_size_in_bytes)
{
    printf("    RF Timestamp: %20" PRIu64 " (0x%016" PRIx64 ")\n", p_block->rf_timestamp, p_block->rf_timestamp);
    printf("System Timestamp: %20" PRIu64 " (0x%016" PRIx64 ")\n", p_block->sys_timestamp, p_block->sys_timestamp);
    printf(" System Metadata: %20u (0x%06" PRIx32 ")\n", p_block->system_meta, p_block->system_meta);
    printf("    RFIC Control: %20u (0x%04" PRIx32 ")\n", p_block->rfic_control, p_block->rfic_control);
    printf("     RF Overload: %20u\n", p_block->overload);
    printf("       RX Handle: %20u\n", p_block->hdl);
    printf("   User Metadata: %20u (0x%08" PRIx32 ")\n", p_block->user_meta, p_block->user_meta);

    printf("Header:\n");
    hex_dump( (uint8_t*)p_block, SKIQ_RX_HEADER_SIZE_IN_BYTES );

    printf("Samples:\n");
    hex_dump( (uint8_t*)p_block->data, block_size_in_bytes - SKIQ_RX_HEADER_SIZE_IN_BYTES );
}

