/*! \file gpsdo_logger.c
 * \brief This file contains a basic application!
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <time.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "sidekiq_api_hwdev.h"
#include "card_services.h"
#include "arg_parser.h"

#include "gpsdo_fpga.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef MAX_BUFFER_SIZE
#   define MAX_BUFFER_SIZE              (1024)
#endif

/**
    @brief  Default values for command-line arguments

    @{
*/
#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER          0
#endif

#ifndef DEFAULT_TX_FREQUENCY
#   define DEFAULT_TX_FREQUENCY         1000000000
#endif

#ifndef DEFAULT_TX_ATTENUATION
#   define DEFAULT_TX_ATTENUATION       359
#endif

#ifndef DEFAULT_SAMPLE_RATE
#   define DEFAULT_SAMPLE_RATE          61.44e6
#endif

#ifndef DEFAULT_BANDWIDTH
#   define DEFAULT_BANDWIDTH            32e6
#endif

#ifndef DEFAULT_SHOW_STATUS_FLAG
#   define DEFAULT_SHOW_STATUS_FLAG     false
#endif

#ifndef DEFAULT_VERBOSE_FLAG
#   define DEFAULT_VERBOSE_FLAG         false
#endif

/** @} */

/* Register definitions */
#define FPGA_REG_GPSDO_READ_COUNTER     (0x86C8)


/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- log GPSDO information";
static const char* p_help_long =
"\
Reads and logs the GPSDO information for a given Sidekiq.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --attenuation=" xstr(DEFAULT_TX_ATTENUATION) "\n\
  --frequency=" xstr(DEFAULT_TX_FREQUENCY) "\n\
  --ki-start=" xstr(GPSDO_DEFAULT_KI_START) "\n\
  --ki-end=" xstr(GPSDO_DEFAULT_KI_END) "\n\
  --thresh-locked=" xstr(GPSDO_DEFAULT_THRESH_LOCKED) "\n\
  --thresh-unlocked=" xstr(GPSDO_DEFAULT_THRESH_UNLOCKED) "\n\
  --count-locked=" xstr(GPSDO_DEFAULT_COUNT_LOCKED) "\n\
  --count-unlocked=" xstr(GPSDO_DEFAULT_COUNT_UNLOCKED) "\n\
  --time-in-stage=" xstr(GPSDO_DEFAULT_TIME_IN_STAGE) "\n\
  --show-status=" xstr(DEFAULT_SHOW_STATUS_FLAG) "\n\
  --verbose=" xstr(DEFAULT_VERBOSE_FLAG) "\n\
";

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static uint16_t tx_atten = DEFAULT_TX_ATTENUATION;
static uint32_t initial_warp_voltage = UINT32_MAX;
static uint64_t lo_freq = (uint64_t)DEFAULT_TX_FREQUENCY;
static bool show_status_flag = DEFAULT_SHOW_STATUS_FLAG;
static bool verbose_flag = DEFAULT_VERBOSE_FLAG;
static char* p_serial = NULL;

/* GPSDO parameters */
static uint8_t ki_start = GPSDO_DEFAULT_KI_START;
static uint8_t ki_end = GPSDO_DEFAULT_KI_END;
static uint8_t thresh_locked = GPSDO_DEFAULT_THRESH_LOCKED;
static uint8_t thresh_unlocked = GPSDO_DEFAULT_THRESH_UNLOCKED;
static uint8_t count_locked = GPSDO_DEFAULT_COUNT_LOCKED;
static uint8_t count_unlocked = GPSDO_DEFAULT_COUNT_UNLOCKED;
static uint8_t time_in_stage = GPSDO_DEFAULT_TIME_IN_STAGE;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("frequency",
                'f',
                "Frequency to receive samples at in Hertz",
                "Hz",
                &lo_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("attenuation",
                'a',
                "Output attenuation in quarter dB steps",
                "dB",
                &tx_atten,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("initial-warp-voltage",
                'w',
                "Initial warp voltage value (26-bit, 0 to 67108864, where 33554432 is mid-rail)",
                "raw",
                &initial_warp_voltage,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("show-status",
                0,
                "If specified, show the GPSDO Status Register as well",
                NULL,
                &show_status_flag,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Show verbose output",
                NULL,
                &verbose_flag,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("ki-start",
                0,
                "Initial Ki setting to use",
                NULL,
                &ki_start,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("ki-end",
                0,
                "Maximum Ki value (minimum gain value)",
                NULL,
                &ki_end,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("thresh-locked",
                0,
                "Frequency error to consider within lock",
                NULL,
                &thresh_locked,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("thresh-unlocked",
                0,
                "Frequency error to consider out of lock",
                NULL,
                &thresh_unlocked,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("count-locked",
                0,
                "Number of consecutive low errors to see before entering lock state",
                NULL,
                &count_locked,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("count-unlocked",
                0,
                "Number of consecutive high frequency counts to see before leaving lock state",
                NULL,
                &count_unlocked,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("time-in-stage",
                0,
                "Time to stay at each Ki gain value before reducing gain",
                NULL,
                &time_in_stage,
                UINT8_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

static bool running = true;


/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}

/*****************************************************************************/
/** This is the main function for executing this application

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ASCII string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    pid_t owner = 0;

    bool skiq_initialized = false;

    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_full;

    gpsdo_fpga_control_parameters_t gpsdoParams = GPSDO_FPGA_CONTROL_PARAMETERS_T_INITIALIZER;
    FILE *fp = NULL;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        status = -1;
        goto finished;
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* initialize Sidekiq */
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    if ( initial_warp_voltage < UINT32_MAX )
    {
        printf("Info: setting an initial warp voltage of %" PRIu32 "\n", initial_warp_voltage);
        status = skiq_fact_write_warp_voltage( card, initial_warp_voltage );
        if ( status != 0 )
        {
            fprintf(stderr, "Failed to write warp voltage on card %" PRIu8
                " (status = %" PRIi32 ")\n", card, status);
        }
    }

    gpsdoParams.ki_start = ki_start;
    gpsdoParams.ki_end = ki_end;
    gpsdoParams.thresh_locked = thresh_locked;
    gpsdoParams.thresh_unlocked = thresh_unlocked;
    gpsdoParams.count_locked = count_locked;
    gpsdoParams.count_unlocked = count_unlocked;
    gpsdoParams.time_in_stage = time_in_stage;

    status = gpsdo_fpga_set_control_parameters(card, &gpsdoParams);
    if (0 != status)
    {
        fprintf(stderr, "Failed to write GPSDO control parameters on card %" PRIu8
                " (status = %" PRIi32 ")\n", card, status);
    }

    status = gpsdo_fpga_enable( card );
    if ( status != 0 )
    {
        fprintf(stderr, "Failed to enable GPSDO in FPGA on card %" PRIu8
            " (status = %" PRIi32 ")\n", card, status);
    }

    skiq_write_rx_sample_rate_and_bandwidth(card, skiq_rx_hdl_A1, (int32_t) DEFAULT_SAMPLE_RATE,
        (int32_t) DEFAULT_BANDWIDTH);
    skiq_write_rx_LO_freq( card, skiq_rx_hdl_A1, lo_freq);
    skiq_write_tx_block_size( card, skiq_tx_hdl_A1, 1020);
    skiq_write_tx_LO_freq(card, skiq_tx_hdl_A1, lo_freq );
    skiq_write_tx_sample_rate_and_bandwidth(card,skiq_tx_hdl_A1, 32000000, 5000000);
    skiq_write_tx_attenuation(card,skiq_tx_hdl_A1, tx_atten);
    skiq_enable_tx_tone(card,skiq_tx_hdl_A1);
    skiq_start_tx_streaming(card,skiq_tx_hdl_A1);

    uint64_t prev_rf_ts = 0, prev_sys_ts = 0;

    {
        char fn[MAX_BUFFER_SIZE];
        snprintf(fn, MAX_BUFFER_SIZE, "gpsdo-%" PRIdMAX ".csv", (intmax_t) time(NULL));

        errno = 0;
        fp = fopen(fn, "w");
        if (NULL == fp)
        {
            fprintf(stderr, "Error: failed to open GPSDO output file '%s' (%d: '%s')\n",
                fn, errno, strerror(errno));
        }
    }

    while (running)
    {
        uint32_t data;
        status = skiq_hwdev_read_fpga_reg( card, FPGA_REG_GPSDO_READ_COUNTER, &data );
        if ( status == 0 )
        {
            uint64_t rf_ts = 0;
            uint64_t sys_ts = 0;

            skiq_read_last_1pps_timestamp( card, &rf_ts, &sys_ts );
            if ( ( prev_rf_ts != rf_ts ) || ( prev_sys_ts != sys_ts ) )
            {
                int8_t t0 = INT8_MIN;
                int8_t t1 = INT8_MIN;
                int8_t t2 = INT8_MIN;
                uint32_t wve = 0;

                struct { int32_t freq_count:30; } reg;
                int32_t freq_count;
                freq_count = reg.freq_count = ( data & ( ( 1 << 30 ) - 1 ) );

                skiq_fact_read_temp_sensor( card, 0, &t0 );
                skiq_fact_read_temp_sensor( card, 1, &t1 );
                skiq_fact_read_temp_sensor( card, 2, &t2 );
                skiq_fact_read_warp_voltage(card, &wve);

                fprintf(stderr, "freq_count: %+d (raw 0x%08x) (%4d,%4d,%4d) (%d) %"
                        PRIu64 " %" PRIu64 " %" PRIu64 " %" PRIu64 "\n", freq_count, data,
                        t0, t1, t2, ((int32_t)wve) - (1 << 25), rf_ts, rf_ts - prev_rf_ts,
                        sys_ts, sys_ts - prev_sys_ts);
                if (NULL != fp)
                {
                    fprintf(fp, "%" PRIdMAX ",%d,%d,%d,%d,%u\n", (intmax_t) time(NULL), freq_count,
                        t0, t1, t2, wve);
                }

                if (show_status_flag)
                {
                    gpsdo_fpga_status_registers_t gpsdoStatus = \
                        GPSDO_FPGA_STATUS_REGISTERS_T_INITIALIZER;

                    status = gpsdo_fpga_read_status(card, &gpsdoStatus);
                    if (0 == status)
                    {
                        if (verbose_flag)
                        {
                            fprintf(stderr, "  pps: sys_ts = %" PRIu64 " rf_ts = %" PRIu64 "\n",
                                sys_ts, rf_ts);

                            {
                                gpsdo_fpga_counters_t gpsdoCounters = \
                                    GPSDO_FPGA_COUNTERS_T_INITIALIZER;
                                status = gpsdo_fpga_read_counters(card, &gpsdoCounters);
                                if (0 == status)
                                {
                                    fprintf(stderr, "  counters: seconds_counter = %" PRIu8
                                        " freq_counter = %" PRIi32 "\n",
                                        gpsdoCounters.seconds_counter, gpsdoCounters.freq_counter);
                                }
                                else
                                {
                                    fprintf(stderr, "  counters: failed to read (%" PRIu32 ")\n",
                                        status);
                                }
                            }

                            {
                                uint32_t warp_voltage = UINT32_MAX;
                                status = gpsdo_fpga_read_warp_voltage(card, &warp_voltage);
                                if (0 == status)
                                {
                                    fprintf(stderr, "  warp: warp_voltage = %" PRIi32 "\n",
                                        warp_voltage);
                                }
                                else
                                {
                                    fprintf(stderr, "  warp: failed to read (%" PRIi32 ")\n",
                                        status);
                                }
                            }

                            fprintf(stderr, "  status: gpsdo_locked_state = %s"
                                " gps_has_fix = %s pll_locked_state = %s"
                                " counter_unlocked = %" PRIu8 " counter_locked = %" PRIu8
                                " ki = %" PRIu8 " stage_timer = %" PRIu8 " (raw 0x%" PRIx32 ")\n",
                                (gpsdoStatus.gpsdo_locked_state) ? "Locked" : "NOT Locked",
                                (gpsdoStatus.gps_has_fix) ? "Has Fix" : "NO Fix",
                                (gpsdoStatus.pll_locked_state) ? "Locked" : "NOT Locked",
                                gpsdoStatus.counter_unlocked, gpsdoStatus.counter_locked,
                                gpsdoStatus.ki, gpsdoStatus.stage_timer, gpsdoStatus.raw_value);
                        }
                        else
                        {
                            fprintf(stderr, "  status register: 0x%" PRIx32 "\n",
                                gpsdoStatus.raw_value);
                        }
                    }
                }

                prev_rf_ts = rf_ts;
                prev_sys_ts = sys_ts;
            }
        }
    }

    skiq_stop_tx_streaming(card,skiq_tx_hdl_A1);
    skiq_disable_tx_tone(card,skiq_tx_hdl_A1);

    status = 0;

finished:
    if (NULL != fp)
    {
        fclose(fp);
        fp = NULL;
    }

    if (skiq_initialized)
    {
        status = gpsdo_fpga_disable(card);
        if (0 != status)
        {
            fprintf(stderr, "Failed to disable GPSDO FPGA algorithm on card %" PRIu8
                " (status = %" PRIi32 ")\n", card, status);
        }

        skiq_exit();
        skiq_initialized = false;
    }

    return ((int) status);
}

