#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <limits.h>

static const char hexTable[] =
{
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'a', 'b', 'c', 'd', 'e', 'f'
};


/**
    @brief  Determine if a character is a valid ASCII hex character (0-9,
            'a'-'f').

    @param[in]  c   The character to check. This can be uppercase or lowercase.

    @return true if the character is valid, else false.
*/
static inline bool
isValidHex(char c)
{
    return (bool) ( (('0' <= c) && ('9' >= c)) ||
                    ( (('a' <= c) && ('f' >= c)) ||
                      (('A' <= c) && ('F' >= c))));
}

/**
    @brief  Convert an ASCII hex nibble to its corresponding hex value.

    @param[in]  c   The character to convert. This can be uppercase or
                    lowercase.

    @return The integer equivalent of the ASCII hex nibble.
*/
static inline uint8_t
charToNibble(char c)
{
    uint8_t nibble = 0;

    if (('0' <= c) && ('9' >= c))
    {
        nibble = c - '0';
    }
    else if (('a' <= c) && ('f' >= c))
    {
        nibble = 0x0a + (c - 'a');
    }
    else if (('A' <= c) && ('F' >= c))
    {
        nibble = 0x0a + (c - 'A');
    }

    return(nibble);
}

/**
    @brief  Convert a nibble into a hex ASCII character.

    @param[in]  nibble  The value to convert.

    @return 'x' if @nibble is invalid, else the corresponding hex character.
*/
static inline char
nibbleToChar(uint8_t nibble)
{
    char chr = 'x';

    if (16 > nibble)
    {
        chr = hexTable[nibble];
    }

    return(chr);
}

/**
    @brief  Convert a byte into its ASCII hex equivalent.

    @param[in]  byte        The value to convert.
    @param[out] outStr      The buffer to place the converted value into.
                            This should not be NULL.

    @return The length of the converted string.
*/
static inline uint8_t
byteToHexChars(uint8_t byte, char *outStr)
{
    outStr[0] = nibbleToChar((byte >> 4) & 0x0f);
    outStr[1] = nibbleToChar(byte & 0x0f);

    return (2);
}

/**
    @brief  Convert an array into its ASCII hex string equivalent.

    @param[in]  str         The array to convert. This should not be NULL.
    @param[in]  strLen      The length of @str in bytes.
    @param[out] outStr      The buffer to place the encoded string into. This
                            should not be NULL.
    @param[in]  outStrLen   The length of @outStr in bytes.

    @return The length of the encoded string in @outStr in bytes if successful,
            else 0.
*/
uint32_t
hexlify(char *str, uint32_t strLen, char *outStr, uint16_t outStrLen)
{
    uint32_t i;
    uint32_t j;
    uint32_t outLen;

    if ((NULL == str) || (NULL == outStr))
    {
        printf("%s: invalid pointer provided!\n", __FUNCTION__);
        return(0);
    }

    if ((UINT32_MAX / 2) < strLen)
    {
        outLen = (UINT32_MAX - 1);
    }
    else
    {
        outLen = strLen * 2;
    }
    /* Make room for a trailing NUL. */
    outLen++;

    if (outStrLen < outLen)
    {
        printf("%s: not enough room in output buffer!\n", __FUNCTION__);
        return(0);
    }

    j = 0;
    for (i = 0; i < strLen; i++)
    {
        j += byteToHexChars(str[i], &(outStr[j]));
    }
    outStr[j++] = '\0';

    return(outLen);
}

/**
    @brief  Convert an ASCII hex string into its equivalent integer values.

    @param[in]  str         The ASCII hex string to convert. This should not
                            be NULL.
    @param[in]  strLen      The length of @str in bytes.
    @param[out] outStr      The buffer to place the decoded bytes into. This
                            should not be NULL.
    @param[in]  outStrLen   The length of @outStr in bytes.

    @return The length of the decoded data in bytes if successful, else 0.
*/
uint32_t
unhexlify(char *str, uint32_t strLen, char *outStr, uint16_t outStrLen)
{
    uint32_t i;
    uint32_t j;
    uint8_t k;
    uint8_t nibble[2];
    uint32_t outLen;
    char c;

    if ((NULL == str) || (NULL == outStr))
    {
        printf("%s: invalid parameters!\n", __FUNCTION__);
        return(0);
    }

    if (0 != (strLen % 2))
    {
        printf("%s: string isn't an even length!\n", __FUNCTION__);
        return(0);
    }

    outLen = (strLen / 2);

    if (outStrLen < outLen)
    {
        printf("%s: not enough room in output buffer!\n", __FUNCTION__);
        return(0);
    }

    j = 0;
    i = 0;
    while (i < strLen)
    {
        for (k = 0; k < 2; k++)
        {
            c = tolower(str[i]);

            if (!isValidHex(c))
            {
                printf("%s: invalid hex character '%c'!\n", __FUNCTION__, c);
                outLen = 0;
                goto finished;
            }

            nibble[k] = charToNibble(c);

            i++;
        }

        outStr[j++] = (nibble[0] << 4) | nibble[1];
    }

finished:
    return(outLen);
}


/**
    @brief  This is essentially strtol() with a bunch of error checking.

    @param[in]  strPtr      A pointer to the string to decode.
    @param[out] endPtr      A pointer to the end of the decoded string. This
                            can be NULL if uninterested in the result.
    @param[in]  base        The base (10, 16, etc.) of the number to decode.
    @param[out] out         The location to put the decoded variable.

    @return true on success, else false.
*/
bool
convertToLong(const char *strPtr, char **endPtr, int base, long *out)
{
    bool return_value = false;
    long value;
    char *tmpEndPtr;

    if ((NULL == strPtr) || (NULL == out))
    {
        goto finished;
    }

    errno = 0;
    value = strtol(strPtr, &tmpEndPtr, base);
    if (tmpEndPtr == strPtr)
    {
        printf("ERROR %s : not a decimal number.\n", __FUNCTION__);
        goto finished;
    }
    else if ('\0' != *tmpEndPtr)
    {
        printf("ERROR %s : extra characters at end of input (original was '%s',"
                " '%s' left over).\n",
                __FUNCTION__, strPtr, tmpEndPtr);
        goto finished;
    }
    else if (((LONG_MIN == value) || (LONG_MAX == value)) && (ERANGE == errno))
    {
        printf("ERROR %s : value '%s' is out of range!\n", __FUNCTION__, strPtr);
        goto finished;
    }
    else if (EINVAL == errno)
    {
        printf("ERROR %s : '%s' is an invalid value.\n", __FUNCTION__, strPtr);
    }

    *out = value;
    if (NULL != endPtr)
    {
        *endPtr = tmpEndPtr;
    }

    return_value = true;

finished:
    return (return_value);
}

