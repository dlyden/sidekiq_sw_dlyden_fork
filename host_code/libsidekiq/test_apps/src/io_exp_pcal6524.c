/*! \file io_exp_pcal6524.c
 * \brief This file contains a basic application that reads and writes
 * the PCAL6524 IO expander.  This assumes access
 *  
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sidekiq_api.h>
#include <io_expander_pcal6524.h>
#include <inttypes.h>
#include <signal.h>
#include <unistd.h>

#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#ifndef DEFAULT_IO_EXP_I2C_BUS
#   define DEFAULT_IO_EXP_I2C_BUS  1        // Stretch and Z3U both use bus 1 (default), NV100 uses bus 0
#endif

#ifndef DEFAULT_IO_EXP_ADDR
#   define DEFAULT_IO_EXP_ADDR  0x22
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- obtain reads/writes IOE pcal6524";
static const char* p_help_long =
"\
Read/write of IO expander pcal6524.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --bus=" xstr(DEFAULT_IO_EXP_I2C_BUS) "\n\
  --address=" xstr(DEFAULT_IO_EXP_ADDR) "\n";


/* command line argument variables */
static uint8_t card = DEFAULT_CARD_NUMBER;
static uint8_t bus=DEFAULT_IO_EXP_I2C_BUS;
static uint8_t address=DEFAULT_IO_EXP_ADDR;
static uint32_t pin_mask=0;
static bool pin_mask_present=false;
static uint32_t hiz_mask=0;
static bool hiz_mask_present=false;
static uint32_t high_mask=0;
static bool high_mask_present=false;

static bool skiq_initialized = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("bus",
                'b',
                "I2C bus of IO expander",
                "ID",
                &bus,
                UINT8_VAR_TYPE), 
    APP_ARG_OPT("address",
                'a',
                "I2C address of IO expander",
                "ID",
                &address,
                UINT8_VAR_TYPE),   
    APP_ARG_OPT_PRESENT("pin_mask",
                        'p',
                        "Pin bitmask that specifies pins on which to operate",
                        NULL,
                        &pin_mask,
                        UINT32_VAR_TYPE,
                        &pin_mask_present),
    APP_ARG_OPT_PRESENT("hiz_mask",
                        'z',
                        "Bitmask that specifies which pins to configure as Hi-Z",
                        NULL,
                        &hiz_mask,
                        UINT32_VAR_TYPE,
                        &hiz_mask_present),
    APP_ARG_OPT_PRESENT("high_mask",
                        'h',
                        "Bitmask that specifies which pins to configure as high outputs",
                        NULL,
                        &high_mask,
                        UINT32_VAR_TYPE,
                        &high_mask_present),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);

    if (skiq_initialized)
    {
        skiq_exit();
    }
    
    _exit(-1);
}


int main( int argc, char *argv[] )
{
    int32_t status = 0;
    pid_t owner = 0;
    
    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;

    uint32_t val=0;
    uint32_t dir=0;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    if( pin_mask_present == true )
    {
        if( high_mask_present == false )
        {
            fprintf(stderr, "Error: if pin mask is specified, high mask must be specified\n");
            goto finished;
        }

        // if hi-z is present, then we need to configure everything
        if( hiz_mask_present == true )
        {
            printf("Info: configuring I2C bus %u, IO exp address 0x%x, pin_mask=0x%x, hiz_mask=0x%x, high_mask=0x%x\n",
                   bus, address, pin_mask, hiz_mask, high_mask);
            status = pcal6524_io_exp_set_as_hiz( card,
                                                 bus,
                                                 address,
                                                 pin_mask,
                                                 hiz_mask,
                                                 high_mask);
        }
        else
        {
            printf("Info: configuring I2C bus %u, IO exp address 0x%x, pin_mask=0x%x, high_mask=0x%x\n",
                   bus, address, pin_mask, high_mask);
            status = pcal6524_io_exp_set_as_output( card,
                                                    bus,
                                                    address,
                                                    pin_mask,
                                                    high_mask);
        }
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to write to IO expander (status=%d)\n", status);
        }
        else
        {
            printf("Info: successfully wrote to IO expander\n");
        }
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* initialize Sidekiq */
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    if( pin_mask_present == true )
    {
        // if hi-z is present, then we need to configure everything
        if( hiz_mask_present == true )
        {
            printf("Info: configuring I2C bus %u, IO exp address 0x%x, pin_mask=0x%x, hiz_mask=0x%x, high_mask=0x%x\n",
                   bus, address, pin_mask, hiz_mask, high_mask);
            status = pcal6524_io_exp_set_as_hiz( card,
                                                 bus,
                                                 address,
                                                 pin_mask,
                                                 hiz_mask,
                                                 high_mask);
        }
        else
        {
            printf("Info: configuring I2C bus %u, IO exp address 0x%x, pin_mask=0x%x, high_mask=0x%x\n",
                   bus, address, pin_mask, high_mask);
            status = pcal6524_io_exp_set_as_output( card,
                                                    bus,
                                                    address,
                                                    pin_mask,
                                                    high_mask);
        }
        if( status != 0 )
        {
            printf("Error: unable to write to IO expander (status=%d)\n", status);
        }
        else
        {
            printf("Info: successfully wrote to IO expander\n");
        }
    }


    status = pcal6524_io_exp_read_direction( card, bus, address, &dir );
    if( status != 0 )
    {
        fprintf(stderr, "Error reading IO expander direction at 0x%x on bus %u(card=%u) (status=%d)\n",
               address, bus, card, status);
    }

    status = pcal6524_io_exp_read_output( card, bus, address, &val );
    if( status != 0 )
    {
        fprintf(stderr, "Error reading IO expander value at 0x%x on bus %u (card=%u) (status=%d)\n",
               address, bus, card, status);
    }

    if( status == 0 )
    {
        printf("Info: IO expander 0x%x on bus %u, direction=0x%x, value=0x%x\n",
               address, bus, dir, val);
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }


    return (status);
}
