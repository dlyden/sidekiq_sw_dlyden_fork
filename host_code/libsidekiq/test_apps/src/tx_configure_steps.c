/*! \file tx_configure_steps.c
 * \brief This file contains a basic application for transmitting a
 * continuous tone across the specified frequencies and sample rates.
 *  
 * <pre>
 * Copyright 2014 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <strings.h>
#include <unistd.h>

#include <arg_parser.h>
#include <sidekiq_api.h>
#include <sidekiq_private.h>

/* storage for all cmd line args */
static char* app_name;

static bool running=true;

/* The absolute difference between a and b without underflowing or truncating intermediate result */
#define ABS_DIFF(a,b)                           \
    ({ __typeof__ (a) _a = (a);                 \
        __typeof__ (b) _b = (b);                \
        _a > _b ? (_a - _b) : (_b - _a); })

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#ifndef DEFAULT_START_FREQUENCY
#   define DEFAULT_START_FREQUENCY  75000000
#endif

#ifndef DEFAULT_END_FREQUENCY
#   define DEFAULT_END_FREQUENCY  6000000000
#endif

#ifndef DEFAULT_FREQUENCY_STEP
#   define DEFAULT_FREQUENCY_STEP  300000000
#endif

#ifndef DEFAULT_ATTENUATION
#   define DEFAULT_ATTENUATION  359
#endif

#ifndef DEFAULT_START_RATE
#   define DEFAULT_START_RATE  1000000
#endif

#ifndef DEFAULT_END_RATE
#   define DEFAULT_END_RATE  2000000
#endif

#ifndef DEFAULT_RATE_STEP
#   define DEFAULT_RATE_STEP  1000000
#endif

#ifndef DEFAULT_DURATION
#   define DEFAULT_DURATION 1
#endif

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: cleaning up libsidekiq\n");
    running = false;
}

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- transmit a tone for the duration specified";
static const char* p_help_long  = "\
Transmit a tone at the user-specified frequency on the specified \n\
Sidekiq card for the duration provided. A tone will be transmitted\n\
for each frequency step and the tone will be transmitted at each\n\
sample rate.\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --start-freq=" xstr(DEFAULT_START_FREQUENCY) "\n\
  --end-freq=" xstr(DEFAULT_END_FREQUENCY) "\n\
  --freq-step=" xstr(DEFAULT_FREQUENCY_STEP) "\n\
  --attenuation=" xstr(DEFAULT_ATTENUATION) "\n\
  --start-rate=" xstr(DEFAULT_START_RATE) "\n\
  --end-rate=" xstr(DEFAULT_END_RATE) "\n\
  --rate-step=" xstr(DEFAULT_RATE_STEP) "\n\
  --time=" xstr(DEFAULT_DURATION) "\n\
  --cal-mode=auto\n";

static uint8_t card=UINT8_MAX;
static char* p_serial = NULL;
static char* p_hdl = "A1";
static uint64_t lo_freq=DEFAULT_START_FREQUENCY;
static uint64_t end_freq=DEFAULT_END_FREQUENCY;
static uint64_t freq_step=DEFAULT_FREQUENCY_STEP;
static uint16_t tx_atten=DEFAULT_ATTENUATION;
static uint32_t start_sample_rate=DEFAULT_START_RATE;
static uint32_t end_sample_rate=DEFAULT_END_RATE;
static uint32_t sample_rate_step=DEFAULT_RATE_STEP;
static uint32_t num_secs=DEFAULT_DURATION;
static char* p_quadcal="auto";
static uint64_t cal_step=UINT64_MAX;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("handle",
                0,
                "Tx handle to use, either A1, A2, B1, or B2",
                "Tx",
                &p_hdl,
                STRING_VAR_TYPE),
    APP_ARG_OPT("start-freq",
                0,
                "Frequency to start transmitting samples at in Hertz",
                "Hz",
                &lo_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("end-freq",
                0,
                "Frequency to finish transmitting samples at in Hertz",
                "Hz",
                &end_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("freq-step",
                0,
                "Frequency step in Hertz",
                "Hz",
                &freq_step,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("attenuation",
                'a',
                "Output attenuation in quarter dB steps",
                "dB",
                &tx_atten,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("start-rate",
                0,
                "Starting sample rate in Hertz",
                "Hz",
                &start_sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("end-rate",
                0,
                "Ending sample rate in Hertz",
                "Hz",
                &end_sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("rate-step",
                0,
                "Sample rate step in Hertz",
                "Hz",
                &sample_rate_step,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("time",
                't',
                "Duration of test tone transmission",
                "seconds",
                &num_secs,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("quadcal-mode",
                'q',
                "TX quadrature calibration mode, either auto or manual",
                "quadcal",
                &p_quadcal,
                STRING_VAR_TYPE),
    APP_ARG_OPT("quadcal-step",
                0,
                "Calibration steps, when in manual tx quadrature calibration mode",
                NULL,
                &cal_step,
                UINT64_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the main function for executing the tx_configure app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    uint32_t sample_rate;
    // filter overflow
    bool int3 = false;
    bool hb3 = false;
    bool hb2 = false;
    bool qec = false;
    bool hb1 = false;
    bool tx_fir = false;
    skiq_tx_hdl_t hdl = skiq_tx_hdl_end;
    skiq_tx_quadcal_mode_t tx_cal_mode;
    uint64_t last_cal = 0;
    bool skiq_initialized = false;
    pid_t owner = 0;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);
    app_name = argv[0];

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        goto cleanup;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        fprintf(stderr,"Error: must specify EITHER card ID or serial number, not both\n");
        status = -EPERM;
        goto cleanup;
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            status = -ENODEV;
            goto cleanup;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if( 0 == strcasecmp(p_hdl, "A1") )
    {
        hdl = skiq_tx_hdl_A1;
        printf("Info: using Tx handle A1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "A2") )
    {
        hdl = skiq_tx_hdl_A2;
        printf("Info: using Tx handle A2\n");
    }
    else if( 0 == strcasecmp(p_hdl, "B1") )
    {
        hdl = skiq_tx_hdl_B1;
        printf("Info: using Tx handle B1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "B2") )
    {
        hdl = skiq_tx_hdl_B2;
        printf("Info: using Tx handle B2\n");
    }
    else
    {
        fprintf(stderr, "Error: invalid handle specified (%s)\n", p_hdl);
        exit(-1);
    }

    if( 0 == strcasecmp(p_quadcal, "auto") )
    {
        tx_cal_mode = skiq_tx_quadcal_mode_auto;
    }
    else if( 0 == strcasecmp(p_quadcal, "manual") )
    {
        tx_cal_mode = skiq_tx_quadcal_mode_manual;
    }
    else
    {
        fprintf(stderr, "Error: invalid quadcal mode specified (%s)\n", p_quadcal);
        status = -EINVAL;
        goto cleanup;
    }

    if( tx_cal_mode == skiq_tx_quadcal_mode_manual &&
        cal_step == UINT64_MAX )
    {
        fprintf(stderr, "Error: manual calibration mode calibration steps not specified\n");
        status = -ERANGE;
        goto cleanup;
    }
    else if( tx_cal_mode == skiq_tx_quadcal_mode_auto &&
             cal_step != UINT64_MAX )
    {
        printf("Warning: auto calibration mode was selected and calibration steps were specified\n"
               "calibration steps will not be used\n");
    }

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        goto cleanup;
    }
    skiq_initialized = true;

    // configure TX quad cal mode
    status = skiq_write_tx_quadcal_mode( card, hdl, tx_cal_mode );
    if( status != 0 )
    {
        printf("Error: unable to configure the TX quad cal mode with status %d\n", status);
        goto cleanup;
    }

    for( ; (lo_freq < end_freq) && (status==0) && (running==true); lo_freq+=freq_step )
    {
        for( sample_rate=start_sample_rate;
             (sample_rate <= end_sample_rate) && (status==0) && (running==true);
             sample_rate+=sample_rate_step )
        {
            printf("disable tone\n");
            skiq_disable_tx_tone(card, hdl);
            skiq_stop_tx_streaming(card, hdl);
            skiq_write_tx_sample_rate_and_bandwidth(card, hdl, sample_rate, sample_rate);
            skiq_write_tx_LO_freq(card, hdl, lo_freq);
            // set the Tx attenuation level
            skiq_write_tx_attenuation(card, hdl, tx_atten);
            // if we're in manual cal, see if we need to run it again
            if( (tx_cal_mode==skiq_tx_quadcal_mode_manual) &&
                (last_cal == 0 || ( ABS_DIFF(lo_freq, last_cal) >= cal_step ) ) )
            {
                printf("Info: running TX quadcal\n");
                last_cal = lo_freq;
                if( (status=skiq_run_tx_quadcal(card, hdl)) != 0 )
                {
                    printf("Error: unable to run TX quadcal algorithm (%d)\n", status);
                    goto cleanup;
                }
                printf("Info: TX quadcal complete\n");
            }
            // start streaming
            skiq_start_tx_streaming(card, hdl);
            printf("enable tone\n");
            skiq_enable_tx_tone(card, hdl);
            printf("Transmitting at %" PRIu64 "; sample rate %u\n", lo_freq, sample_rate);
            sleep(num_secs);
            // check for overflow
            if( _skiq_read_tx_filter_overflow( card, hdl, &int3, &hb3, &hb2, &qec, &hb1, &tx_fir ) == 0 )
            {
                printf("Filter overflow: %u/%u/%u/%u/%u/%u\n",
                       int3, hb3, hb2, qec, hb1, tx_fir);
                if( (int3 == true) || (hb3 == true) || (hb2 == true) || (qec == true) ||
                    (hb1 == true) || (tx_fir == true) )
                {
                    status = -1;
                }
            }
        }
    }

cleanup:
    if(skiq_initialized)
    {
        // stop streaming
        skiq_stop_tx_streaming(card, hdl);
        // disable the tone
        skiq_disable_tx_tone(card, hdl);

        skiq_exit();
    }

    return status;
}

