/*! \file store_golden_fpga_multicard.c
 * \brief This file contains a basic application that checks then stores the
 * golden FPGA bitstream on to multiple cards in parallel.
 *
 * <pre>
 * Copyright 2017-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>

#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <inttypes.h>
#include <pthread.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "arg_parser.h"

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- store golden FPGA bitstream into flash for multiple cards";
static const char* p_help_long = "\
Scan the system for Sidekiq cards, check golden FPGA presence and store bitstream if not detected";

/* command line argument variables */
static char* p_serial = NULL;

/* file path to golden FPGA bitstream */
static char* p_golden_file_path = NULL;
static FILE* golden_fp = NULL;

static bool force_program = false;
static bool detect_only = false;

/* verify flash contents after writing */
static bool do_verify = false;
/* only verify flash contents against file contents */
static bool do_verify_only = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("serial",
                'S',
                "Target card by serial number",
                "SERIAL",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("golden",
                0,
                "Golden bitstream file to source for programming",
                "PATH",
                &p_golden_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("force",
                'f',
                "Force programming",
                NULL,
                &force_program,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("detect-only",
                0,
                "Print results of golden FPGA checks without actually programming (conflicts with --verify and --verify-only)",
                NULL,
                &detect_only,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verify",
                0,
                "Verify the flash memory after it is written",
                NULL,
                &do_verify,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verify-only",
                0,
                "Do not store the bitstream, just verify the flash memory matches the file contents (conflicts with --verify and --detect-only)",
                NULL,
                &do_verify_only,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

static void *program_card(void *data)
{
    uint8_t card = *((uint8_t *)data);
    char *p_sernum;
    uint8_t present;
    int32_t status;
    FILE *fp = NULL;

    status = skiq_read_serial_string( card, &p_sernum );
    if ( status != 0 )
    {
        p_sernum = "UNKNOWN";
    }

    status = skiq_fact_read_genuine_golden_fpga_present_in_flash( card, &present );
    if ( status == 0 )
    {
        if( present == 1 )
        {
            printf("Info: genuine golden FPGA presence detected on card %u (s/n %s)\n", card, p_sernum);
        }
        else
        {
            printf("Info: genuine golden FPGA presence NOT detected on card %u (s/n %s)\n", card, p_sernum);
        }
    }
    else
    {
        fprintf(stderr, "WARNING: genuine golden FPGA presence NOT AVAILABLE, function returned status %d card %u (s/n %s)\n", status, card, p_sernum);
    }

    if ( !detect_only )
    {
        if ( golden_fp )
        {
            fp = fopen( p_golden_file_path, "r" );
        }

        if ( fp )
        {
            if ( !do_verify_only )
            {
                if ( force_program || ( present != 1 ) )
                {
                    printf("Info: storing provided FPGA bitstream to card %u (s/n %s)\n", card, p_sernum);
                    status = skiq_fact_save_golden_fpga_to_flash( card, fp );
                    if ( status == 0 )
                    {
                        printf("Info: successfully stored bitstream for card %u (s/n %s)\n", card, p_sernum);
                    }
                    else
                    {
                        fprintf(stderr, "ERROR: failed to store bitstream for card %u (s/n %s) with status %d\n", card, p_sernum, status);
                    }
                }
            }

            if ( ( do_verify || do_verify_only ) && ( status == 0 ) )
            {
                fprintf(stderr, "Info: verifying provided FPGA bitstream on card %u (s/n %s)\n", card, p_sernum);
                status = skiq_fact_verify_golden_fpga_from_flash( card, fp );
                if ( status == 0 )
                {
                    printf("Info: successfully verified stored bitstream for card %u (s/n %s)\n", card, p_sernum);
                }
                else
                {
                    fprintf(stderr, "ERROR: failed to verify stored bitstream for card %u (s/n %s) with status %d\n", card, p_sernum, status);
                }
            }
        }
        else
        {
            fprintf(stderr, "ERROR: failed to open golden bitstream: %s\n", strerror(errno));
        }
    }

    if ( fp )
    {
        fclose( fp );
    }

    return NULL;
}

/*****************************************************************************/
/** This is the main function for executing store_golden_fpga_multicard command.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status=0;
    skiq_xport_type_t type = skiq_xport_type_pcie;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;

    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t num_cards=0;
    uint8_t i=0;

    pthread_t threads[SKIQ_MAX_NUM_CARDS];

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if ( !detect_only )
    {
        if ( (status == 0) && (NULL == p_golden_file_path) )
        {
            fprintf(stderr, "Error: no golden file specified\n");
            arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
            return (-1);
        }

        if ( status == 0 )
        {
            golden_fp = fopen( p_golden_file_path, "r" );
            if ( golden_fp == NULL )
            {
                fprintf(stderr, "Error opening golden file: %s\n", strerror(errno));
                arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
                return (-1);
            }
        }
    }

    if ( do_verify && ( do_verify_only || detect_only ) )
    {
        fprintf(stderr, "Error: --verify CONFLICTS with --verify-only and --detect-only\n");
        return (-1);
    }

    if ( detect_only && do_verify_only )
    {
        fprintf(stderr, "Error: either --detect-only OR --verify-only must be specified, not both\n");
        return (-1);
    }

    if ( force_program && ( do_verify_only || detect_only ) )
    {
        fprintf(stderr, "Error: --force CONFLICTS with --verify-only and --detect-only\n");
        return (-1);
    }

    /* disable noisy messages, we just want the info */
    if ( detect_only )
    {
        skiq_register_logging(NULL);
    }

    /* determine how many Sidekiqs are there and their card IDs */
    skiq_get_cards( type, &num_cards, cards );

    /* limit the available cards if --serial is specified */
    /* If specified, attempt to find the card with a matching serial number. */
    if (NULL != p_serial)
    {
        status = skiq_get_card_from_serial_string(p_serial, &(cards[0]));
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, cards[0]);
        num_cards = 1;
    }

    /* no harm, no foul, no cards detected */
    if ( 0 == num_cards )
    {
        printf("Info: no cards detected.\n");
        return (0);
    }

    printf("Info: initializing %" PRIu8 " card(s)...\n", num_cards);

    /* bring up the USB and PCIe interfaces for all the cards */
    status = skiq_init(type, level, cards, num_cards);
    if ( status != 0 )
    {
        if ( EBUSY == status )
        {
            fprintf(stderr, "Error: at least one card is already in use; cannot"
                    " initialize card(s).\n");
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        return (-1);
    }

    /* read the serial number for each of the cards */
    for ( i=0; i<num_cards; i++ )
    {
        pthread_create( &(threads[i]), NULL, program_card, (void *)&(cards[i]) );
    }

    /* join all the threads */
    for ( i=0; i<num_cards; i++ )
    {
        pthread_join( threads[i], NULL );
    }

    if ( golden_fp )
    {
        fclose( golden_fp );
        golden_fp = NULL;
    }

    skiq_exit();

    return status;
}
