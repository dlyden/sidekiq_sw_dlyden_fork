/**
 * @file   jesd_sync_test_regression.c
 * @date   Fri May 17 09:55:33 2019
 *
 * @brief This file contains a basic application for running a specified number of JESD sync trials
 * with the default values for preEmphasis, serializer amplitude, diffcontrol, and precursor to
 * report the sync quality and number of retries.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>

#include "sidekiq_api.h"
#include "arg_parser.h"

#include "sidekiq_private.h"    /* for _skiq_syslog */
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "fpga_jesd_ctrl.h"     /* for jesd_sync_result_t */
#include "rfic_x2_private.h"    /* for reset_jesd_x2 and reset_jesd_x2_defaults */
#include "rfic_x4_private.h"    /* for reset_jesd_x4 and reset_jesd_x4_defaults */
#include "hal_delay.h"          /* for hal_millisleep */

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#ifndef DEFAULT_SAMPLE_RATE
#   define DEFAULT_SAMPLE_RATE  61440000
#endif

#ifndef DEFAULT_DELAY_MS
#   define DEFAULT_DELAY_MS  10
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "run JESD sync regression";
static const char* p_help_long = "\
Executes a specified number of JESD sync trials across default JESD parameters\n\
to report on sync quality and number of retries.\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --rate=" xstr(DEFAULT_SAMPLE_RATE) "\n\
  --delay=" xstr(DEFAULT_DELAY_MS) "\n\
  --repeat=0\
";

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static uint32_t sample_rate = DEFAULT_SAMPLE_RATE;
static char* p_file_path = NULL;
static int32_t repeat = 0;
static uint32_t delay_ms = DEFAULT_DELAY_MS;
static bool verbose = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("rate",
                'r',
                "Sample rate in Hertz",
                "Hz",
                &sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_REQ("destination",
                'd',
                "Output file to store trial report",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("repeat",
                0,
                "Run the JESD sync test N additional trials",
                "N",
                &repeat,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("delay",
                0,
                "Delay (in ms) to report FPGA registers after a failed sync",
                "MS",
                &delay_ms,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Show extra information during the test execution",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


static uint32_t trials_with_retries[UINT8_MAX+1];

static void critical_handler( int32_t status, void* p_user_data )
{
    printf("RECEIVED CRITICAL ERROR (status=%d), CONTINUING\n", status);
}

static void log_msg( int32_t priority, const char *msg )
{
    if ( priority != SKIQ_LOG_DEBUG )
    {
        fprintf(stderr, "%s", msg);
    }
}

static void write_header_to_FILE( FILE *fp )
{
    fprintf(fp, "premphasis,amplitude,diffctrl,precursor,synced,jesd_status_imm,jesd_unsync_imm,framer_a_rx,framer_a_orx,deframer_a,framer_b_rx,framer_b_orx,deframer_b,jesd_status_dwell,jesd_unsync_dwell,rx_unsync_a,orx_unsync_a,tx_unsync_a,rx_unsync_b,orx_unsync_b,tx_unsync_b,retries\n");
}

static void write_result_to_FILE( FILE *fp,
                           uint8_t synced,
                           jesd_sync_result_t *p_sync_result )
{
    fprintf(fp,
            "%u,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,%u,%u,%u,%u,%u,%u,%u\n",
            synced,
            p_sync_result->jesd_status_imm,
            p_sync_result->jesd_unsync_imm,
            p_sync_result->framer_a_rx,
            p_sync_result->framer_a_orx,
            p_sync_result->deframer_a,
            p_sync_result->framer_b_rx,
            p_sync_result->framer_b_orx,
            p_sync_result->deframer_b,
            p_sync_result->jesd_status_dwell,
            p_sync_result->jesd_unsync_dwell,
            p_sync_result->unsync_a.rx_unsync,
            p_sync_result->unsync_a.orx_unsync,
            p_sync_result->unsync_a.tx_unsync,
            p_sync_result->unsync_b.rx_unsync,
            p_sync_result->unsync_b.orx_unsync,
            p_sync_result->unsync_b.tx_unsync,
            p_sync_result->num_retries);
}


int main( int argc, char *argv[] )
{
    int32_t status = 0;
    jesd_sync_result_t result;
    uint32_t num_syncs=0;
    FILE *pFile = NULL;
    int32_t tot_sync_err = 0;
    uint32_t tot_num_tests=0;
    uint32_t r=0;
    uint8_t synced=0;
    bool skiq_initialized = false;
    int32_t (*reset_jesd_default)(uint8_t card,
                                  jesd_sync_result_t *p_sync_result) = NULL;

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    /* translate a '-' to mean /dev/stdout */
    if ( 0 == strcasecmp( p_file_path, "-" ) )
    {
        p_file_path = "/dev/stdout";
    }

    pFile = fopen(p_file_path, "w+");
    if( pFile == NULL )
    {
        fprintf(stderr, "Error: failed to open file %s\n", p_file_path);
        status = -1;
        goto finished;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* initialize Sidekiq */
    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        pid_t owner;

        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    status = skiq_write_rx_sample_rate_and_bandwidth( card, skiq_rx_hdl_A1, sample_rate, (uint32_t)((float)sample_rate * 0.8) );
    if ( status != 0 )
    {
        fprintf(stderr, "Error: unable to set sample rate and bandwidth with status %d\n", status);
        goto finished;
    }

    {
        skiq_param_t param;

        status = skiq_read_parameters( card, &param );
        if ( status != 0 )
        {
            fprintf(stderr, "Error: unable to read card parameters with status %d\n", status);
            goto finished;
        }

        if ( param.card_param.part_type == skiq_x2 )
        {
            printf("Info: using X2 JESD reset\n");
            reset_jesd_default = &(reset_jesd_x2_defaults);
        }
        else if( param.card_param.part_type == skiq_x4 )
        {
            printf("Info: using X4 JESD reset\n");
            reset_jesd_default = &(reset_jesd_x4_defaults);
        }
        else
        {
            fprintf(stderr, "Error: product does not support JESD\n");
            status = -1;
            goto finished;
        }
    }

    memset(trials_with_retries, 0, sizeof( trials_with_retries ) );

    skiq_register_critical_error_callback( &critical_handler, NULL );

    skiq_register_logging( log_msg );
    for ( r = 0; r < repeat + 1; r++ )
    {
        tot_num_tests++;
        if ( verbose )
        {
            printf("\n------------------------------------------------------------\n");
        }

        if( tot_num_tests <= 1 )
        {
            write_header_to_FILE(pFile);
            if ( verbose )
            {
                write_header_to_FILE(stdout);
            }
        }

        // reset our results
        memset( &result, 0, sizeof(jesd_sync_result_t) );

        if ( reset_jesd_default( card, &result ) != 0 )
        {
            tot_sync_err++;
            synced = 0;
        }
        else
        {
            synced = 1;
            hal_millisleep(delay_ms);

            sidekiq_fpga_reg_read(card, FPGA_REG_JESD_STATUS, &result.jesd_status_dwell);
            fpga_jesd_read_unsync_counts( card, &result.unsync_a, &result.unsync_b, &result.jesd_unsync_dwell );
            if ( verbose )
            {
                printf("\tA Rx1/2 error count %u\n", result.unsync_a.rx_unsync);
                printf("\tA ObsRx error count %u\n", result.unsync_a.orx_unsync);
                printf("\tA Tx error count %u\n", result.unsync_a.tx_unsync);
                printf("\tB Rx1/2 error count %u\n", result.unsync_b.rx_unsync);
                printf("\tB ObsRx error count %u\n", result.unsync_b.orx_unsync);
                printf("\tB Tx error count %u\n", result.unsync_b.tx_unsync);
            }

            num_syncs++;
            trials_with_retries[result.num_retries]++;
        }

        write_result_to_FILE(pFile, synced, &result);
        printf("trial %u: ", r), write_result_to_FILE(stdout, synced, &result);

        fflush(pFile);

        printf("Info: num_sync_success=%u, sync_errs=%u, total tests %u, repeat=%u\n",
               num_syncs, tot_sync_err, tot_num_tests, r);
    }
    skiq_register_logging( _skiq_syslog );

    printf("Completed testing of %u sample rate: num_sync_success=%u, sync_errs=%u, total tests "
           "%u, repeat=%u\n", sample_rate, num_syncs, tot_sync_err, tot_num_tests, r);
    for (r = 0; r < UINT8_MAX; r++)
    {
        if ( trials_with_retries[r] > 0 )
        {
            printf("%10d trials with %u retries\n", trials_with_retries[r], r);
        }
    }
    if ( tot_sync_err > 0 )
    {
        printf("%10d trials with sync errors\n", tot_sync_err);
    }
    printf("\n");

    if ( tot_sync_err > 0 )
    {
        status = -1;
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    if ( pFile != NULL )
    {
        fclose(pFile);
        pFile = NULL;
    }

    return ((int) status);
}
