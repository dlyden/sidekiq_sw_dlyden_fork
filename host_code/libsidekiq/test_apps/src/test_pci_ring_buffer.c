/**
    @file   test_pci_ring_buffer
    @brief  Test utility to test the PCI DMA ring buffer size (now a module parameter in the
            dmadriver)

    <pre>
    Copyright 2020 Epiq Solutions, All Rights Reserved
    </pre>
*/

/**
    @todo   Add another test that uses timing information to determine buffer size; basically
            you keep increasing a sleep between reads until you find out the largest amount
            of time that can be slept before overflowing the ring buffer.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "sidekiq_api.h"
#include "arg_parser.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER      0
#endif

#ifndef DEFAULT_SAMPLE_RATE_HZ
#   define DEFAULT_SAMPLE_RATE_HZ   20000000
#endif

#ifndef DEFAULT_TIME_SEC
#   define DEFAULT_RUN_TIME_SEC     15
#endif

/**
    @note   This is defined in dma_interface.h but I don't want to try to pull that header into a
            test app
*/
#ifndef DMA_INTERFACE_PACKET_SIZE
#   define DMA_INTERFACE_PACKET_SIZE    (16384)
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- get ring buffer information";
static const char* p_help_long =
"\
At a given sample rate, attempt to determine information about the PCI DMA\n\
ring buffer.\n\n\
For slower sample rates, a larger time value is suggested to ensure that the entire\n\
DMA buffer is filled before calculating its size.\n\n\
NOTE: it should go without saying that this can only be used on cards using\n\
      the PCI transport; running this with other transports may return invalid or\n\
      incorrect information\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --rate=" xstr(DEFAULT_SAMPLE_RATE_HZ) "\n\
  --time=" xstr(DEFAULT_RUN_TIME_SEC) "\n\
";

static uint8_t card = DEFAULT_CARD_NUMBER;
static uint32_t sample_rate = DEFAULT_SAMPLE_RATE_HZ;
static uint32_t run_time = DEFAULT_RUN_TIME_SEC;

static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("rate",
                'r',
                "Sample rate",
                "Hz",
                &sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("time",
                't',
                "Run time",
                "seconds",
                &run_time,
                UINT32_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

static const char *
priorityToStr(int32_t priority)
{
    switch (priority)
    {
    case SKIQ_LOG_DEBUG:
        return "DEBUG";
        break;
    case SKIQ_LOG_INFO:
        return "INFO";
        break;
    case SKIQ_LOG_WARNING:
        return "WARN";
        break;
    case SKIQ_LOG_ERROR:
        return "ERROR";
        break;
    default:
        break;
    }

    return "Unknown";
}

static void
logging_func(int32_t priority, const char *message)
{
    size_t len = strlen(message);
    char lastCh = (len > 0) ? message[len - 1] : '\n';
    printf("    [Sidekiq %s (%" PRIi32 ")]: %s%s", priorityToStr(priority), priority,
        message, (lastCh != '\n') ? "\n" : "");
}

static void
sigHandler(int sigNum)
{
    (void) sigNum;
    running = false;
}


/**
    @brief  Attempt to read the ring buffer packet count from the dmadriver's sysfs entry

    @param[out] p_packetCount   If not NULL, the number of packets the module indicates that it
                                is using

    @return 0 on success, else an errno
*/
static int32_t
sysfsReadBufferSize(uint32_t *p_packetCount)
{
#define READ_BUFFER_SIZE    (128)
    int32_t status = 0;
    const char *p_dmaSysfsPacketCount = "/sys/module/dmadriver/parameters/RingBufferPacketCount";
    int fd = -1;
    char readBuffer[READ_BUFFER_SIZE] = { '\0' };

    ssize_t readCount = 0;

    errno = 0;
    fd = open(p_dmaSysfsPacketCount, O_RDONLY);
    if (-1 == fd)
    {
        fprintf(stderr, "Warn: failed to open dmadriver sysfs entry for RingBufferPacketCount"
            " '%s' (%d: '%s')\n", p_dmaSysfsPacketCount, errno, strerror(errno));
        status = errno;
        goto finished;
    }

    errno = 0;
    readCount = read(fd, &(readBuffer[0]), READ_BUFFER_SIZE);
    if (-1 == readCount)
    {
        fprintf(stderr, "Warn: failed to read ring buffer packet count from sysfs entry"
            " (%d: '%s')\n", errno, strerror(errno));
        status = errno;
        goto finished;
    }

    if ((0 == status) && (NULL != p_packetCount))
    {
        unsigned long int value = 0;

        errno = 0;
        value = strtoul(&(readBuffer[0]), NULL, 10);
        if (0 != errno)
        {
            fprintf(stderr, "Warn: failed to parse ring buffer packet count '%s' (%d: '%s')\n",
                readBuffer, errno, strerror(errno));
            status = errno;
        }
        else
        {
            *p_packetCount = (uint32_t) value;
        }
    }

finished:
    if (-1 != fd)
    {
        int32_t tmpStatus = 0;

        errno = 0;
        tmpStatus = close(fd);
        if (0 != tmpStatus)
        {
            fprintf(stderr, "Warn: failed to close sysfs entry (%d: '%s')\n",
                errno, strerror(errno));
        }
        fd = -1;
    }

    return (status);
#undef READ_BUFFER_SIZE
}

static void
displayEstimatedBufferTime(uint64_t bufferSize, uint32_t sampleRate)
{
    double bufferTimeSec = \
        (double) bufferSize / ((double) sampleRate * sizeof(uint16_t) * 2);
    printf("Info: a %" PRIu64 " byte buffer at a sample rate of %" PRIu32 " Hz stores about"
        " %0.4f seconds of samples\n", bufferSize, sampleRate, bufferTimeSec);
}

/**
    @brief  Attempt to display the ring buffer packet count and buffer size from the dmadriver
*/
static void
displaySysfsPacketCount(uint32_t sampleRate)
{
    int32_t status = 0;
    uint32_t packetCount = 0;

    status = sysfsReadBufferSize(&packetCount);
    if (0 != status)
    {
        printf("Warn: Failed to read ring buffer count from sysfs; skipping.\n");
    }
    else
    {
        /* Again, this knowledge comes from the dmadriver */
        uint32_t allocatedBufferSize = packetCount * DMA_INTERFACE_PACKET_SIZE;
        printf("Info: sysfs reports ring buffer packet count at %" PRIu32 " packets"
            " (estimated %" PRIu32 " bytes allocated)\n", packetCount, allocatedBufferSize);

        displayEstimatedBufferTime(allocatedBufferSize, sampleRate);
    }
    printf("\n");
}

/**
    @brief  Attempt to calculate the ring buffer packet count from DMA addresses

    @param[in]  cardNum     The card number to test
    @param[in]  sampleRate  The sample rate to operate at
*/
static int32_t
calcBufferSizeTest(uint8_t cardNum, uint32_t sampleRate, uint32_t timeSec)
{
    int32_t status = 0;
    uint8_t cardList[1] = { SKIQ_MAX_NUM_CARDS };
    pid_t owner = 0;
    bool skiqInitialized = false;
    bool skiqStreaming = false;
    skiq_rx_hdl_t rxHdl = skiq_rx_hdl_A1;

    uint64_t minAddress = UINT64_MAX;
    uint64_t maxAddress = 0;

    cardList[0] = cardNum;

    skiq_register_logging(logging_func);

    printf("Info: Initializing card %" PRIu8 "...\n", card);
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &(cardList[0]), 1);
    if( 0 != status )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(cardNum, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID %u);"
                " cannot initialize card.\n", cardNum, (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a valid card"
                " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with status %" PRIi32 "\n",
                status);
        }
        goto finished;
    }
    printf("Info: initialized card %d\n", card);
    skiqInitialized = true;

    {
        uint32_t bandwidth = (uint32_t) ((float) sample_rate * 0.80);
        status = skiq_write_rx_sample_rate_and_bandwidth(cardNum, rxHdl, sampleRate,
                    bandwidth);
        if (0 != status)
        {
            fprintf(stderr, "Error: failed to set sample rate to %" PRIu32 "Hz and bandwidth to"
                " %" PRIu32 "Hz (status = %" PRIi32 ")\n", sampleRate, bandwidth, status);
            goto finished;
        }
    }

    /* Set the transfer timeout so we're not constantly polling for samples below */
    status = skiq_set_rx_transfer_timeout(cardNum, 250000);
    if (0 != status)
    {
        fprintf(stderr, "Warn: failed to set RX transfer timeout; test utility may max out CPU\n");
    }

    status = skiq_start_rx_streaming(cardNum, rxHdl);
    if (0 != status)
    {
        fprintf(stderr, "Error: failed to start streaming (status = %" PRIi32 ")\n", status);
        goto finished;
    }
    skiqStreaming = true;

    /* The number of samples left to read */
    uint64_t samplesLeft = (uint64_t)sampleRate * timeSec;
    uint64_t displaySamples = samplesLeft;

    printf("Info: reading %" PRIu64 " samples...\n", samplesLeft);

    skiq_rx_status_t result = skiq_rx_status_success;
    skiq_rx_hdl_t rxRecvHdl = skiq_rx_hdl_A1;
    skiq_rx_block_t *p_rxBlock;
    uint32_t rxLen = 0;
    while ((running) && (0 == status) && (0 < samplesLeft))
    {
        result = skiq_receive(cardNum, &rxRecvHdl, &p_rxBlock, &rxLen);
        if (skiq_rx_status_no_data == result)
        {
            continue;
        }
        else if (skiq_rx_status_error_overrun == result)
        {
            fprintf(stderr, "Warn: overrun detected!\n");
        }
        else if (skiq_rx_status_success != result)
        {
            fprintf(stderr, "Error: failed to read data from radio (result = %" PRIi32 ")\n",
                (int32_t) result);
            status = EIO;
        }
        else if (rxRecvHdl != rxHdl)
        {
            fprintf(stderr, "Error: read data from unexpected receive handle"
                " (expecting %d, got %d)\n", (int) rxHdl, (int) rxRecvHdl);
            status = EIO;
        }
        else
        {
            uint64_t ptrAddr = (uint64_t) (intptr_t) &(p_rxBlock->data[0]);
            if (maxAddress < ptrAddr)
            {
                maxAddress = ptrAddr;
            }
            if (minAddress > ptrAddr)
            {
                minAddress = ptrAddr;
            }
        }

        uint64_t samplesRead = (rxLen / (sizeof(uint16_t) * 2)) - SKIQ_RX_HEADER_SIZE_IN_WORDS;
        if (samplesRead <= samplesLeft)
        {
            samplesLeft -= samplesRead;

            if (sampleRate <= (displaySamples - samplesLeft))
            {
                printf("Info: %" PRIu64 " samples left\n", samplesLeft);
                displaySamples = samplesLeft;
            }
        }
        else
        {
            samplesLeft = 0;
        }
    }
    if (!running)
    {
        printf("Info: test aborted due to signal!\n");
    }

    printf("Info: minAddress = 0x%" PRIx64 " maxAddress = 0x%" PRIx64 " (%" PRIu64 " bytes)\n",
        minAddress, maxAddress, (maxAddress - minAddress));

    {
        double addrDiff = maxAddress - minAddress;
        /*
            DMA_INTERFACE_PACKET_SIZE * RingBufferCount is the rough calculation for the size
            of the DMA ring buffer buffer... which we've just been probing
        */
        addrDiff /= DMA_INTERFACE_PACKET_SIZE;

        uint64_t numEntries = (uint64_t) round(addrDiff);

        printf("Info: this roughly corresponds to %" PRIu64 " ring buffer entries"
            " (%" PRIu64 " bytes)\n", numEntries, (numEntries * DMA_INTERFACE_PACKET_SIZE));
        displayEstimatedBufferTime((numEntries * DMA_INTERFACE_PACKET_SIZE), sampleRate);
    }

finished:
    printf("Info: cleaning up after test...\n");

    if (skiqStreaming)
    {
        int32_t tmpStatus = 0;

        tmpStatus = skiq_stop_rx_streaming(cardNum, rxHdl);
        if (0 != tmpStatus)
        {
            fprintf(stderr, "Warning: failed to stop RX streaming (status = %" PRIi32 ")\n",
                tmpStatus);
        }
    }

    if (skiqInitialized)
    {
        int32_t tmpStatus = 0;

        tmpStatus = skiq_exit();
        if (0 != tmpStatus)
        {
            fprintf(stderr, "Warning: failed to close libsidekiq (status = %" PRIi32 ")\n",
                tmpStatus);
        }
        skiqInitialized = false;
    }

    return (status);
}

int main(int argc, char *argv[])
{
    int32_t status = 0;
    struct sigaction signal_action;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    signal_action.sa_handler = sigHandler;
    sigemptyset(&signal_action.sa_mask);
    signal_action.sa_flags = 0;
    sigaction(SIGINT, &signal_action, NULL);
    sigaction(SIGTERM, &signal_action, NULL);

    printf("Info: running test\n"
           "                 Card %" PRIu8 "\n"
           "          Sample Rate %" PRIu32 " Hz\n"
           "             Run Time %" PRIu32 " seconds\n\n", card, sample_rate, run_time);

    displaySysfsPacketCount(sample_rate);

    status = calcBufferSizeTest(card, sample_rate, run_time);

    return (status);
}

