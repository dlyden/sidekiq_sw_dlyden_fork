#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "intelhex.h"

/**
    @brief  The maximum address in EEPROM that can be written to; this should
            save space for any user data stored at the end of the EEPROM.
*/
#define EEPROM_REC_MAX_ADDR     (0x3FF0)
/** @brief  The maximum length of an EEPROM record. */
#define EEPROM_REC_MAX_LEN      (1023)
#define EEPROM_BOOT_BYTE        (0xC2)

#define EEPROM_REC_HEADER_LEN   (4)

#define CONFIG_BYTE_400KHZ      (0x01)

#define MIN(a,b)                                \
    ({ __typeof__ (a) _a = (a);                 \
        __typeof__ (b) _b = (b);                \
        _a < _b ? _a : _b; })

/** @brief  The EEPROM header - this always needs to be there. */
static const uint8_t eepromHeader[] =
{
    EEPROM_BOOT_BYTE,   /* Header magic value. */
    0x00, 0x00,         /* USB PID */
    0x00, 0x00,         /* USB VID */
    0x00, 0x00,         /* USB DID */
    CONFIG_BYTE_400KHZ  /* Configuration byte. */
};
static const uint8_t eepromHeaderLen = \
        sizeof(eepromHeader) / sizeof(eepromHeader[0]);
/** @brief  The final EEPROM record - this always need to be there. */
static const uint8_t eepromFinalRecord[] =
{
    0x80, 0x01, 0xe6, 0x00, 0x00
};
static const uint8_t eepromFinalRecordLen = \
        sizeof(eepromFinalRecord) / sizeof(eepromFinalRecord[0]);

static inline uint32_t
writeEepromHeader(uint8_t *buffer)
{
    memcpy((void *) buffer, (const void *) eepromHeader, eepromHeaderLen);
    return eepromHeaderLen;
}

static inline uint32_t
writeEepromFooter(uint8_t *buffer)
{
    memcpy((void *) buffer, (const void *) eepromFinalRecord,
            eepromFinalRecordLen);
    return eepromFinalRecordLen;
}

/** @todo   How big should outBuffer be? */
uint32_t
eeprom_convertRecords(struct RecordList *list, uint8_t *outBuffer,
        uint32_t outBufferLen)
{
    struct Record *tmpRec = NULL;
    uint32_t bufferLen = 0;
    uint32_t tmpIdx = 0;
    uint16_t len = 0;
    uint16_t addr = 0;
    uint16_t nextAddr = 0;
    uint16_t curRecLen = 0;

    if (NULL == list)
    {
        printf("ERROR %s : invalid parameters!\n", __FUNCTION__);
        return 0;
    }

    /* Fill in the EEPROM file header. */
    bufferLen = writeEepromHeader(outBuffer);

    printf("\nCreating EEPROM records...\n");

    tmpRec = list->head;
    nextAddr = 0;
    while (NULL != tmpRec)
    {
#if 0
        printf("Creating new record...\n");
#endif

        addr = tmpRec->addr;
        curRecLen = 0;

        if (EEPROM_REC_HEADER_LEN >= (outBufferLen - bufferLen))
        {
            printf("ERROR %s : not enough buffer to complete record"
                   " conversion.\n", __FUNCTION__);
            bufferLen = -1;
            goto finished;
        }

        /*
            Store off the index of the length bytes; we'll go back and
            back-fill them with the correct addresses later.
        */
        tmpIdx = bufferLen;
        outBuffer[bufferLen++] = 0x00;
        outBuffer[bufferLen++] = 0x00;
        /* Now it's the address's turn to shine. */
        outBuffer[bufferLen++] = (addr >> 8) & 0xFF;
        outBuffer[bufferLen++] = (addr) & 0xFF;

        do
        {
            /* Make sure the values are within range. */
            addr = tmpRec->addr;
            len = tmpRec->length;
            if (EEPROM_REC_MAX_ADDR < addr)
            {
                printf("ERROR %s : address %x is out of range (0 - %x)!\n",
                        __FUNCTION__, addr, EEPROM_REC_MAX_ADDR);
                goto finished;
            }
            if (EEPROM_REC_MAX_LEN < len)
            {
                printf("ERROR %s : length %u is out of range (0 - %u)!\n",
                        __FUNCTION__, len, EEPROM_REC_MAX_LEN);
                goto finished;
            }
            nextAddr = addr + len;

            if (EEPROM_REC_MAX_LEN >= (curRecLen + len))
            {
                if (curRecLen >= (outBufferLen - bufferLen))
                {
                    printf("ERROR %s : not enough buffer to complete record"
                            " conversion.\n", __FUNCTION__);
                    bufferLen = -1;
                    goto finished;
                }

#if 0
                printf("DEBUG %s : writing %u bytes to offset %u...\n",
                        __FUNCTION__, len, bufferLen+curRecLen);
#endif

                memcpy((void *) &(outBuffer[bufferLen+curRecLen]),
                       (const void *) tmpRec->data,
                       MIN(len, sizeof(tmpRec->data)));

                curRecLen += len;
                tmpRec = tmpRec->next;
            }
        } while ((tmpRec != NULL) &&
                 (nextAddr == tmpRec->addr) &&
                 (EEPROM_REC_MAX_LEN >= curRecLen));

#if 0
        printf("Record length: %u bytes.\n", curRecLen);
#endif

        /* Fill in the length for the previous record. */
        outBuffer[tmpIdx++] = (curRecLen >> 8) & 0xFF;
        outBuffer[tmpIdx++] = (curRecLen) & 0xFF;

        bufferLen += curRecLen;
    }

    if (eepromFinalRecordLen >= (outBufferLen - bufferLen))
    {
        printf("ERROR %s : not enough buffer to complete record conversion.\n",
                __FUNCTION__);
        bufferLen = 0;
        goto finished;
    }

    /* Append the mandatory final EEPROM record. */
    bufferLen += writeEepromFooter(&(outBuffer[bufferLen]));

finished:
    return bufferLen;
}

