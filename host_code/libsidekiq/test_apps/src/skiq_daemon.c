/*! \file skiq_daemon.c
 * \brief This file contains a server application that allows
 * register access and streaming API through socket(s).  Refer to
 * https://confluence.epiq.rocks/display/EN/RX+Implementation+of+skiq_daemon
 * for details regarding the state machine for the RX streaming.
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include "sidekiq_api.h"
#include "sidekiq_params.h"
#include "sidekiq_daemon_private.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_fpga.h"
#include "sidekiq_rpc_server.h"
#include "net_transport_private.h"
#include "sidekiq_rx.h"
#include "hal_delay.h"
#include "sidekiq_tx.h"
#include "io_expander_cache.h"

#include "glib.h"

#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>

#include <arg_parser.h>

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <signal.h>

#include <sched.h>

#include <rpc/rpc.h>

#define PROBE_SOCK_BACKLOG (5)

#define KEEPALIVE_TIME (10)  // 10 seconds before first probe
#define KEEPALIVE_INVTL (10) // probe every 10 seconds 
#define KEEPALIVE_PROBES (6) // 6 times of no ACK
#define XCV_TIMEOUT_SEC (1) // timeout every second

#define NUM_DUMMY_PKTS (16) // we want to send 16 pkts initally to force everything out buffer

#define NUM_CONSECUTIVE_PKTS (10) // max number of packets to send without a sleep

#define NET_STREAMING_CPU (1) // use 2nd CPU for network streaming (note: CPU_SET starts at 0 index)

#define DEFAULT_MAX_SEND_RETRY (10) // arbitrarily chosen, retry sending a packet 10 times if there's an error on the socket

#define MAX_NUMBER_OF_FLUSHES (100) // a bit arbitrarily selected...typically don't see more than 20 flushes

#define MAX_NUM_MS_RPC_START (500) // it shouldn't take more than 500 ms for RPC thread to startup

// arbitrarily selected default ports, can be overwritten with cmd arguments
#define DEFAULT_CTRL_PORT_NUM (DEFAULT_PROBE_PORT_NUM+1)
#define DEFAULT_RX_STREAM_PORT_NUM (DEFAULT_CTRL_PORT_NUM+1)
#define DEFAULT_TX_STREAM_PORT_NUM (DEFAULT_RX_STREAM_PORT_NUM+1)

///////////////////////////////////////
// Any time we issue a signal and expect a state transition
// we poll the state_update message queue for the 
// expected state transition.  The MAX_*_TRANS is the
// maximum number of times the queue is polled for an update, and 
// the MAX_*_US is the poll interval (in uS)

// RX transitions
#define MAX_RX_SOCK_READY_US    (1000) // 1 ms
#define MAX_RX_SOCK_READY_TRANS (100)  // 100 ms total wait time
#define MAX_RX_STREAMING_US     (1000) // 1 ms
#define MAX_RX_STREAMING_TRANS  (20)   // 20 ms total wait time
#define MAX_RX_STOPPED_US       (5000) // 5 ms
#define MAX_RX_STOPPED_TRANS    (10)   // 50 ms total wait time
#define MAX_RX_PAUSED_US        (1000) // 1 ms
#define MAX_RX_PAUSED_TRANS     (10)   // 10 ms total wait time
#define MAX_RX_FLUSHED_US       (10000) // 10 ms
#define MAX_RX_FLUSHED_TRANS    (100)   // 1 s total wait time
#define MAX_RX_DESTROYED_US     (500)  // 500 us
#define MAX_RX_DESTROYED_TRANS  (10)   // 5 ms total wait time

// TX transitions
#define MAX_TX_STREAMING_US     (1000) // 1 ms
#define MAX_TX_STREAMING_TRANS  (20)   // 20 ms total wait time
#define MAX_TX_STOPPED_US       (5000) // 5 ms
#define MAX_TX_STOPPED_TRANS    (10)   // 50 ms total wait time
#define MAX_TX_DESTROYED_US     (500)  // 500 us
#define MAX_TX_DESTROYED_TRANS  (10)   // 5 ms total wait time
///////////////////////////////////////

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

// An enum used to track the states of the RX thread...prior to the creation
// of the RX thread, we are in the "DESTROYED" state.  Once the thread has
// been created, we move to the "STOPPED" state.
typedef enum
{
    // Note: due to use of GPOINTER_TO_INT, "0" is not a valid value and results in segfault
    RX_THREAD_SOCK_READY=1,  // RX stream socket has been configured and SOM received
    RX_THREAD_STREAMING,     // RX streaming
    RX_THREAD_PAUSED,        // streaming is paused...no data is sent across the socket
    RX_THREAD_FLUSHED,       // skiq receive buffer has been flushed
    RX_THREAD_STOPPED,       // RX streaming stopped
    RX_THREAD_DESTROYED,     // RX thread destroyed
    RX_THREAD_INVALID,       // Error / invalid
} rx_thread_state_t;

// An enum used to send signals the RX stream thread to trigger state transactions
typedef enum 
{
    // Note: due to use of GPOINTER_TO_INT, "0" is not a valid value and results in segfault
    RX_SIGNAL_SETUP_SOCK=1,       // setup the RX socket, wait for SOM
    RX_SIGNAL_START_STREAMING,    
    RX_SIGNAL_STOP_STREAMING,
    RX_SIGNAL_PAUSE_STREAMING,
    RX_SIGNAL_RESUME_STREAMING,
    RX_SIGNAL_FLUSH_STREAMING,
    RX_SIGNAL_RELEASE_CARD,      // done with TX on this card, shut it down
    // all signals above this line
    RX_SIGNAL_MAX,
} rx_signal_t;

// An enum used to track the states of the TX thread...prior to the creation
// of the TX thread, we are in the "DESTROYED" state.  Once the thread has
// been created, we move to the "STOPPED" state.
typedef enum
{
    // Note: due to use of GPOINTER_TO_INT, "0" is not a valid value and results in segfault
    TX_THREAD_STREAMING=1,   // TX streaming
    TX_THREAD_STOPPED,       // TX streaming stopped
    TX_THREAD_DESTROYED,     // TX thread destroyed
    TX_THREAD_INVALID,       // Error / invalid
} tx_thread_state_t;

// An enum used to send signals to the TX stream thread to trigger state transactions
typedef enum 
{
    // Note: due to use of GPOINTER_TO_INT, "0" is not a valid value and results in segfault
    TX_SIGNAL_START_STREAMING=1,
    TX_SIGNAL_STOP_STREAMING,
    TX_SIGNAL_RELEASE_CARD,      // done with RX on this card, shut it down
    // all signals above this line
    TX_SIGNAL_MAX,
} tx_signal_t;

// An enum used to distinguish betwee RX or TX for shared functions
typedef enum
{
    XCV_TYPE_RX=0,
    XCV_TYPE_TX
} xcv_type_t;

// const string to display xcv_type
const char *
xcv_type_cstr( xcv_type_t type )
{
    const char* p_xcv_type_str =
        (type == XCV_TYPE_RX) ? "RX" :
        (type == XCV_TYPE_TX) ? "TX" :
        "unknown";

    return p_xcv_type_str;
}

// tracks the current state of the RX thread
static rx_thread_state_t _rx_thread_state = RX_THREAD_DESTROYED;
// mutex for RX thread changes state
static pthread_mutex_t _rx_thread_state_mutex=PTHREAD_MUTEX_INITIALIZER;
// queue containing messages to RX thread
GAsyncQueue *_p_rx_thread_msg_queue = NULL;
// queue containing updates to rx thread state changes
GAsyncQueue *_p_rx_thread_state_update = NULL;

// tracks the current state of the TX thread
static tx_thread_state_t _tx_thread_state = TX_THREAD_DESTROYED;
// mutex for TX thread changes state
static pthread_mutex_t _tx_thread_state_mutex=PTHREAD_MUTEX_INITIALIZER;
// mutex for TX xport access
static pthread_mutex_t _tx_xport_mutex=PTHREAD_MUTEX_INITIALIZER;
// queue containing messages to RX thread
GAsyncQueue *_p_tx_thread_msg_queue = NULL;
// queue containing updates to rx thread state changes
GAsyncQueue *_p_tx_thread_state_update = NULL;

static pthread_t ctrl_rx_thread;
static pthread_t rpc_thread;
static pthread_t stream_rx_thread;
static pthread_t stream_tx_thread;

static bool rpc_thread_running=false;
static pthread_cond_t rpc_start_cond=PTHREAD_COND_INITIALIZER;
static pthread_mutex_t rpc_start_mutex=PTHREAD_MUTEX_INITIALIZER;

static uint32_t _usleep_per_skiq_pkt=0;

/* command line argument variables */
static uint16_t probe_port_num=DEFAULT_PROBE_PORT_NUM;
static uint16_t control_port_num=DEFAULT_CTRL_PORT_NUM;
static uint16_t rx_stream_port_num=DEFAULT_RX_STREAM_PORT_NUM;
static uint16_t tx_stream_port_num=DEFAULT_TX_STREAM_PORT_NUM;
static char* p_accept_ip = NULL;
static uint32_t max_send_retry=DEFAULT_MAX_SEND_RETRY;

static bool running = true;

static const char* p_help_short = "- daemon to allow remote Sidekiq access";
static const char* p_help_long = "\
Scan the system for Sidekiq cards, allowing card numbers to be queried on probe port.\n\
Defaults:\n\
  --probe-port-num=" xstr(DEFAULT_PROBE_PORT_NUM) "\n\
  --max-send-retry=" xstr(DEFAULT_MAX_SEND_RETRY) "\n\
  --ctrl-port-num=" xstr(DEFAULT_CTRL_PORT_NUM) "\n\
  --rx-stream-port-num=" xstr(DEFAULT_RX_STREAM_PORT_NUM) "\n\
";

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("probe-port-num",
                'p',
                "Port number to report cards in probe",
                "PORTNUM",
                &probe_port_num,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("accept-ip",
                'a',
                "IP address to allow access in probe",
                "IP",
                &p_accept_ip,
                STRING_VAR_TYPE),
    APP_ARG_OPT("max-send-retry",
                'r',
                "Maximum number of times to retry sending a RX packet if a failure occurs",
                "MAXRETRY",
                &max_send_retry,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("ctrl-port-num",
                'c',
                "Port number to use for control messages",
                "CTRL_PORTNUM",
                &control_port_num,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("rx-stream-port-num",
                'r',
                "Port number to use for RX streaming",
                "RX_PORTNUM",
                &rx_stream_port_num,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("tx-stream-port-num",
                't',
                "Port number to use for TX streaming",
                "TX_PORTNUM",
                &tx_stream_port_num,
                UINT16_VAR_TYPE),

    APP_ARG_TERMINATOR,
};

typedef struct
{
    int32_t sock;
    uint8_t card;
} card_socket_t;

/*****************************************************************************/
/** The create_tcp_socket() function creates a listening socket from the provided 
    port number

    @param[in] port port number of the socket

    @return file descriptor of created socket if positive, otherwise an errno
 */
static int create_tcp_socket( uint16_t port );

/*****************************************************************************/
/** The create_udp_socket() function creates a UDP socket from the provided 
    port number

    @param[in] port port number of the socket

    @return file descriptor of created socket if positive, otherwise an errno
 */
static int create_udp_socket( uint16_t port );

/*****************************************************************************/
/** The recv_probe_msg() function attempts to read a probe message and call
    the appropriate handler

    @param[in] conn_sock file descriptor of connected probe socket

    @return 0 if successful, otherwise a errno
*/
static int32_t recv_probe_msg( int conn_sock );

/*****************************************************************************/
/** The recv_ctrl_msg() function attempts to read a control message and call
    the appropriate handler

    @param[in] card card number associated with control socket
    @param[in] conn_sock file descriptor of connected control socket

    @return 0 if successful, otherwise a errno
*/
static int32_t recv_control_msg( uint8_t card, int32_t conn_sock );

/*****************************************************************************/
/** The process_rpc() function intializes the RPC server and runs forever

    @param[in] ptr currently unused

    @return NULL
*/
static void* process_rpc(void *ptr);

/*****************************************************************************/
/** The process_ctrl_rx() function receives and handles messages on the 
    control socket

    @param[in] ptr pointer to card_socket_t that contains the card number and
    socket file descriptor associated with the control connection

    @return NULL
*/
static void* process_ctrl_rx(void *ptr);

/*****************************************************************************/
/**
   The following functions are handlers for messages received on the probe socket

   @param[in] conn_sock file descriptor of connected socket
   @param[in] seq_number sequence number of received message
   @param[in] payload_len length of payload
 */

static int32_t handle_get_api( int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_get_cards( int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_reserve_cards( int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );

/*****************************************************************************/

////////////////////////////////////////////////////
/*
    @param[in] ptr pointer to card_socket_t that contains the card number and
    socket file descriptor associated with the RX stream connection
*/
static void* process_rx_stream(void *ptr);

////////////////////////////////////////////////////
/*
    @param[in] ptr pointer to card_socket_t that contains the card number and
    socket file descriptor associated with the TX stream connection
*/
static void* process_tx_stream(void *ptr);
////////////////////////////////////////////////////

/*****************************************************************************/
/**
   The following functions are handlers for messages received on the control socket

   @oaram[in] card card number associated with the control socket
   @param[in] conn_sock file descriptor of connected socket
   @param[in] seq_number sequence number of received message
   @param[in] payload_len length of payload
 */

static int32_t handle_release_cards( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_read_fpga_reg( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_write_fpga_reg( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_fpga_reg_verify( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_fpga_reg_write_and_verify( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );

static int32_t handle_rx_configure( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_rx_stream_ctrl( uint8_t card, int32_t conn_sock, uint32_t seq_number,  skiq_daemon_req_t rx_ctrl );

static int32_t handle_tx_configure( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len );
static int32_t handle_tx_stream_ctrl( uint8_t card, int32_t conn_sock, uint32_t seq_number, skiq_daemon_req_t tx_ctrl );

/*****************************************************************************/

/*****************************************************************************/
/** The process_rx_stopped_state processes RX signals from the "stopped" state.
 *
 *     @param[in] card card number associated with the RX stream
 * 
 *     @return state transition requested
 */
static rx_thread_state_t process_rx_stopped_state( uint8_t card );

/*****************************************************************************/
/** The process_rx_streaming_state processes RX signals from the "streaming" state.
 *
 *     @param[in] sock_fd file descriptor of streaming socket
 *     @param[in] card card number associated with the RX stream
 * 
 *     @return state transition requested
 */
static rx_thread_state_t process_rx_streaming_state( int sock_fd, uint8_t card );

/*****************************************************************************/
/** The process_tx_stopped_state processes TX signals from the "stopped" state.
 *
 *     @param[in] card card number associated with the TX stream
 * 
 *     @return state transition requested
 */
static tx_thread_state_t process_tx_stopped_state( uint8_t card );

/*****************************************************************************/
/** The process_tx_streaming_state processes TX signals from the "streaming" state.
 *
 *     @param[in] sock_fd file descriptor of streaming socket
 *     @param[in] card card number associated with the RX stream
 * 
 *     @return state transition requested
 */
static tx_thread_state_t process_tx_streaming_state( int sock_fd, uint8_t card );

/*****************************************************************************/
/** This function monitors the _p_tx/rx_thread_state_update queue for thread 
 *  transitions.  If the transition doesn't occur within the specified amount of
 *  time -ETIMEDOUT is returned.
 *
 *     @param[in] req_state state transition expected (requested)
 *     @param[in] max_num_us_per_state maximum number of microseconds to wait for a state change
 *     @param[in] max_state_trans maximum number of state transitions to wait for requested state
 *     @param[in] type indicating whether we're monitoring the TX or RX thread state update
 * 
 *     @return 0 upon success, otherwise negative errno
 */
static int32_t wait_for_state_transition( int32_t req_state, 
                                          uint32_t max_num_us_per_state, 
                                          uint32_t max_state_trans,
                                          xcv_type_t type );

/*****************************************************************************/
/** This function waits for the start streaming signal to be received on the
 *  _p_rx_thread_msg_queue indicating transition to streaming. 
 * 
 *     @return next state to transition to
 */
static rx_thread_state_t wait_for_rx_start_signal( void );

/*****************************************************************************/
/** This function creates either a RX or TX stream thread.
 *
 *     @param[in] card card number associated with the stream
 *     @param[in] type indicates if stream is RX or TX
 *     @param[in] tcp flag indicating if socket should be TCP or UDP
 * 
 *     @return 0 upon success, otherwise negative errno
 */
static int32_t create_stream_thread( uint8_t card, xcv_type_t type, bool tcp );

/*****************************************************************************/
/** This function processes any signals while in the RX streaming state.
 *
 *     @param[in] card card number associated with the RX stream
 *     @param[in] p_rx_signal pointer to RX signal to process
 * 
 *     @return 0 upon success, otherwise negative errno
 */
static rx_thread_state_t process_rx_streaming_signals( uint8_t card, gpointer p_rx_signal );

/*****************************************************************************/
/** This function pushes the rx_signal onto the p_t/rx_thread_msg_queue.
 *
 *     @param[in] signal TX/RX signal to push onto the queue
 *     @param[in] type indicates if signal is RX or TX related
 * 
 *     @return 0 upon success, otherwise negative errno
 */
static void push_signal( int32_t signal, xcv_type_t type );

/*****************************************************************************/
/** This function pushes the thread_state onto the p_t/rx_thread_state_queue.
 *
 *     @param[in] signal signal to push onto the queue
 *     @param[in] type indicates if thread update is RX or TX related
 * 
 *     @return 0 upon success, otherwise negative errno
 */
static void push_thread_state_update( int32_t thread_state, xcv_type_t type );

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq...\n", signum);
    running = false; // clears the running flag so that everything exits
}

/*****************************************************************************/
/** This is the main function of the daemon.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status=0;
    int probe_sock_fd=-1;
    int connected_probe_peer=-1;
    bool skiq_initialized=false;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    // initialize the library with no cards and then use enable/disable as they are requested
    if( running == true )
    {
        if( (status=skiq_init_without_cards()) != 0 )
        {
            fprintf(stderr, "Error: failed to initialize libsidekiq (status=%d)\n", status);
            return (status);
        }
        skiq_initialized = true;
    }
    else
    {
        // no longer running...just return
        return 0;
    }

    if( (status=pthread_create( &(rpc_thread),
                                 NULL,
                                 process_rpc,
                                 NULL )) != 0 )
    {
        fprintf(stderr, "Error: failed to create RPC thread (status=%" PRId32 ")\n", status);
        skiq_exit();
        return (status);
    }

    pthread_mutex_lock( &rpc_start_mutex );
    if( rpc_thread_running == false )
    {
        struct timespec ts;

        printf("Info: Waiting for RPC thread to start\n");
        /* determine a reasonable timespec for the condition wait to timeout */
        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_nsec += (long)MAX_NUM_MS_RPC_START * (long)MILLISEC;
        if ( ts.tv_nsec >= (long)SEC )
        {
            ts.tv_nsec -= (long)SEC;
            ts.tv_sec++;
        }

        if( pthread_cond_timedwait( &rpc_start_cond, &rpc_start_mutex, &ts ) != 0 )
        {
            fprintf(stderr, "Error: RPC thread did not start successfully, exiting\n");
            goto main_error;
        }
    }
    pthread_mutex_unlock( &rpc_start_mutex );
    printf("Info: RPC thread started\n");


    // TODO: forcibly kill any running apps...maybe cmd line option?
    // TODO: force card mgr reinit...maybe cmd line option?

    printf("Info: Creating sidekiq card probe on port %u\n", probe_port_num);

    probe_sock_fd = create_tcp_socket( probe_port_num );
    if( probe_sock_fd >= 0 )
    {
        printf("Info: probe socket created successfully\n");
        // setup the probe socket as passive
        if( listen( probe_sock_fd, PROBE_SOCK_BACKLOG ) >= 0 )
        {
            struct sockaddr_in caddr;
            socklen_t caddr_len = sizeof(caddr);

            printf("Info: now listening on probe socket\n");

            // accept any incoming connections
            while( running==true )
            {
                errno = 0;
                connected_probe_peer = accept( probe_sock_fd, (struct sockaddr*)(&caddr), &caddr_len);
                status = 0;
                if( connected_probe_peer >= 0 )
                {
                    while( ((status == 0) || (status==-ETIMEDOUT)) && (running==true) )
                    {
                        status = recv_probe_msg( connected_probe_peer );
                    }
                    // we're done with the probe port, just go back to accepting connections
                    close( connected_probe_peer );
                    connected_probe_peer = -1;
                }
                else
                {
                    // we expect a timeout if no one is connected immediately
                    if( (errno != EAGAIN) && (errno != EWOULDBLOCK) && (errno != ECONNABORTED) )
                    {
                        fprintf(stderr, "Error: unable to accept probe socket connections (errno=%d)\n", errno);
                        running = false;
                    }
                }
            }

            printf("Info: done accepting connections\n");
        }
        else
        {
            fprintf(stderr, "Error: unable to listen on port %u (errno=%d)\n", probe_port_num, errno);
            running = false;
        }
        printf("Info: Closing probe socket\n");
        close( probe_sock_fd );
        probe_sock_fd = -1;
    }

main_error:
    if( (status=pthread_cancel( rpc_thread )) != 0 )
    {
        fprintf(stderr, "Error: unable to cancel RPC thread with error %" PRId32 "\n", status);
    }
    if( (status=pthread_join( rpc_thread, NULL )) != 0 )
    {
        fprintf(stderr, "Error: unable to join RPC thread with error %" PRId32 "\n", status);
    }
    printf("Info: cleanup complete!\n");

    if( skiq_initialized == true )
    {
        skiq_exit();
    }
    
    return (status);
}

int create_tcp_socket( uint16_t port )
{
    int fd=-1;
    int enable = 1;
    struct sockaddr_in sockaddr;
    int32_t keepalive_time = KEEPALIVE_TIME;
    int32_t keepalive_intvl = KEEPALIVE_INVTL;
    int32_t keepalive_probes = KEEPALIVE_PROBES;
    struct timeval tv;
    int32_t status=0;

    memset( &sockaddr, 0, sizeof(sockaddr) );

    sockaddr.sin_family = AF_INET;
    if( p_accept_ip == NULL )
    {
        sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    else
    {
        if( inet_aton( p_accept_ip, &sockaddr.sin_addr ) == 0 )
        {
            status = -EINVAL;
        }
    }
    
    sockaddr.sin_port = htons(port);

    if( status == 0 )
    {
        errno = 0;
        fd = socket(AF_INET, SOCK_STREAM, 0);

        if( fd >= 0  )
        {
            errno = 0;

            tv.tv_sec = XCV_TIMEOUT_SEC;
            tv.tv_usec = 0;
            /* Configure the socket to send keep-alive packets every KEEPALIVE_INVTL seconds,
             * and disconnect the client if no reply was received after KEEPALIVE_PROBES with
             * no response received: https://www.tldp.org/HOWTO/html_single/TCP-Keepalive-HOWTO/#overview */
            if( (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) >= 0) &&
                (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(enable)) >= 0) &&
                (setsockopt(fd, IPPROTO_TCP, TCP_KEEPCNT,
                            &keepalive_probes, sizeof(keepalive_probes)) >= 0) &&
                (setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE,
                            &keepalive_time, sizeof(keepalive_time)) >= 0) &&
                (setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL,
                            &keepalive_intvl, sizeof(keepalive_intvl)) >= 0) &&
                (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)(&tv), sizeof(tv)) >= 0) &&
                (setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (const char*)(&tv), sizeof(tv)) >= 0) )
            {
                errno = 0;
                if( bind( fd, (struct sockaddr*)(&sockaddr), sizeof(sockaddr) ) < 0 )
                {
                    fprintf(stderr,"Error: unable to bind on port %u (errno=%d)\n", port, errno );
                    // close socket
                    close( fd );
                    fd = -1;
                }
            }
            else
            {
                fprintf(stderr,"Error: unable to set socket options for port %u (errno=%d)\n", port, errno);
                // close socket
                close( fd );
                fd = -1;
            }
        }
        else
        {
            perror("Error: unable to create socket\n");
        }

        if( fd < 0 )
        {
            fd = -errno;
        }
    }
    else
    {
        fd = status;
    }

    return (fd);
}

int create_udp_socket( uint16_t port )
{
    int32_t status=0;
    struct sockaddr_in server_addr;
    int sock_fd=-1;

    errno = 0;
    if( (sock_fd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) != -1 )
    {
        memset(&server_addr, 0, sizeof(server_addr)); 

        if( p_accept_ip == NULL )
        {
            server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        }
        else if( inet_aton( p_accept_ip, &server_addr.sin_addr ) == 0 )
        {
            status = -EINVAL;
            fprintf(stderr, "Error: failed to convert IP address\n");
        }

        if( status == 0 )
        {
            server_addr.sin_family = AF_INET;
            server_addr.sin_port = htons(port);

            // bind it
            errno = 0;
            if( bind(sock_fd, (const struct sockaddr*)(&server_addr), sizeof(server_addr)) < 0 )
            {
                status = errno;
                fprintf(stderr, "Error: failed to bind (errno=%d)\n", errno);
            } // end bind
        } // end status ok, ready to bind
        else
        {
            status = -errno;
            close(sock_fd);
            sock_fd = -1;
        }
    } // end creating socket
    else
    {
        fprintf(stderr, "Error: failed to create socket (errno=%d)\n", errno);
        status = -errno;
    }

    if( sock_fd >= 0 )
    {
        status = sock_fd;
    }

    return status;
}

int32_t create_stream_thread( uint8_t card, xcv_type_t type, bool tcp )
{
    int32_t status=0;
    int sock_fd=-1;
    card_socket_t *p_sock = NULL;
    uint16_t port_num=0;

    if( type == XCV_TYPE_TX )
    {
        port_num = tx_stream_port_num;
    }
    else
    {
        port_num = rx_stream_port_num;
    }
                                    
    printf("Info: Stream port num is %u for %s\n", port_num, xcv_type_cstr(type) );

    if( tcp == true )
    {
        sock_fd = create_tcp_socket( tx_stream_port_num );
        if( sock_fd < 0 )
        {
            status = sock_fd;
        }
    }
    else
    {
        // create the UDP streaming socket
        sock_fd = create_udp_socket( rx_stream_port_num );
    } // end UDP socket create

    // create the stream thread
    if( status == 0 )
    {
        // note that this memory is freed by the thread that is spun up and receives this data
        p_sock = (card_socket_t*)(malloc(sizeof(card_socket_t)));
        if( p_sock != NULL )
        {
            p_sock->card = card;
            p_sock->sock = sock_fd;

            printf("Info: card=%u, stream socket fd=%d\n", p_sock->card, p_sock->sock);

            // create the thread message / state queues now...they need to be created before the thread
            if( type == XCV_TYPE_RX )
            {
                _p_rx_thread_state_update = g_async_queue_new();
                _p_rx_thread_msg_queue = g_async_queue_new();

                // create RX thread
                status = pthread_create( &(stream_rx_thread), NULL,
                                         process_rx_stream,
                                         (void*)(p_sock) );
            }
            else
            {
                _p_tx_thread_state_update = g_async_queue_new();
                _p_tx_thread_msg_queue = g_async_queue_new();

                // create TX thread
                status = pthread_create( &(stream_tx_thread), NULL,
                                         process_tx_stream,
                                         (void*)(p_sock) );
            }

            // free up memory if thread wasn't created successfully
            if( status != 0 )
            {
                free( p_sock );
                goto cleanup_queue;
            }
        }
        else
        {
            fprintf(stderr, "Error: failed to allocate memory for stream socket information (card=%u)\n", card);
            status = -ENOMEM;
            goto cleanup_sockfd;
        }
    }

    return (status);

cleanup_queue:
    if( type == XCV_TYPE_RX )
    {
        g_async_queue_unref( _p_rx_thread_state_update );
        g_async_queue_unref( _p_rx_thread_msg_queue );
    }
    else
    {
        g_async_queue_unref( _p_tx_thread_state_update );
        g_async_queue_unref( _p_tx_thread_msg_queue );            
    }
cleanup_sockfd:
    close(sock_fd);

    return (status);
}

void* process_rpc(void *ptr)
{
    int32_t status=0;
    
    if( (status=sidekiq_rpc_server_init()) == 0 )
    {
        pthread_mutex_lock( &rpc_start_mutex );
        rpc_thread_running = true;
        pthread_cond_signal( &rpc_start_cond );
        pthread_mutex_unlock( &rpc_start_mutex );
        
        svc_run(); // this never returns...
    }
    else
    {
        fprintf(stderr, "Error: unable to initialize RPC server with status %d\n", status);
    }

    return (NULL);
}

void* process_ctrl_rx(void *data)
{
    int32_t status=0;
    card_socket_t *p_ctrl_sock = (card_socket_t*)(data);
    int control_sock_fd=-1;
    uint8_t card=0;
    int connected_ctrl_peer = -1;
    struct sockaddr_in caddr;
    socklen_t caddr_len = sizeof(caddr);
    bool card_released=false;
    int32_t thread_state=0;

    if( p_ctrl_sock != NULL )
    {
        control_sock_fd = p_ctrl_sock->sock;
        card = p_ctrl_sock->card;
        free(p_ctrl_sock);

        errno = 0;
        // accept any incoming connections
        connected_ctrl_peer = accept( control_sock_fd, (struct sockaddr*)(&caddr), &caddr_len);
        if( connected_ctrl_peer >= 0 )
        {
            printf("Info: Peer connected on control (card=%u)!\n", card);
            while( ((status == 0) || (status == -ETIMEDOUT) || (status==-EAGAIN)) && (running==true) )
            {
                status = recv_control_msg( card, connected_ctrl_peer );
                // this means the card was released, not actually an error
                if( status == ECONNRESET )
                {
                    card_released = true;
                    status=0;
                }
            }

            /* close file descriptor of connected control peer */
            close( connected_ctrl_peer );
            connected_ctrl_peer = -1;

            printf("Info: Closing control socket, card %u disabled\n", card);
            close( control_sock_fd );
            control_sock_fd = -1;

            // disable card if it wasn't released already 
            if( card_released == false )
            {
                // generate a STOP signal in case anything is waiting on it
                pthread_mutex_lock( &_tx_thread_state_mutex );
                thread_state = _tx_thread_state;
                pthread_mutex_unlock( &_tx_thread_state_mutex );

                if( thread_state != TX_THREAD_DESTROYED )
                {
                    push_signal( TX_SIGNAL_STOP_STREAMING, XCV_TYPE_TX );    
                }

                // generate a STOP signal in case anything is waiting on it
                pthread_mutex_lock( &_rx_thread_state_mutex );
                thread_state = _rx_thread_state;
                pthread_mutex_unlock( &_rx_thread_state_mutex );
                if( thread_state != RX_THREAD_DESTROYED )
                {
                    push_signal( RX_SIGNAL_STOP_STREAMING, XCV_TYPE_RX );    
                }

                if( (status=skiq_disable_cards( &card, 1 )) != 0 )
                {
                    fprintf(stderr, "Error: unable to disable card with status=%d\n", status);
                }
            }
        }
        else
        {
            fprintf(stderr, "Error: accept failed (errno=%d)\n", errno);
        }
    }
    else
    {
        fprintf(stderr, "Error: control socket descriptor is NULL\n");
    }

    return (NULL);
}

int32_t recv_probe_msg( int conn_sock )
{
    int32_t status=0;
    skiq_daemon_req_msg_t req_msg;
    ssize_t msg_size;
    uint32_t host_som=0;
    uint32_t host_request_type=0;
    uint32_t host_seq_number=0;
    uint32_t host_payload_len=0;

    errno = 0;
    msg_size = read( conn_sock, &(req_msg), SKIQ_DAEMON_REQ_SIZE );
    
    if( msg_size == SKIQ_DAEMON_REQ_SIZE ) 
    {
        // convert ntoh
        host_som = ntohl(req_msg.som);
        host_request_type = ntohl(req_msg.request_type);
        host_seq_number = ntohl(req_msg.seq_number);
        host_payload_len = ntohl(req_msg.payload_len);

        if( host_som == DAEMON_HDR_SOM )
        {
            switch( host_request_type )
            {
                case SKIQ_DAEMON_GET_API:
                    status = handle_get_api( conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_GET_CARDS:
                    status = handle_get_cards( conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_RESERVE_CARD:
                    status = handle_reserve_cards( conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_RELEASE_SOCKET:
                    status = ESRCH;
                    break;

                default:
                    fprintf(stderr, "Error: got unsupported request %u in probe message\n", host_request_type);
                    status = -EINVAL;
                    break;
            }
        }
        else
        {
            status = -EPROTO;
        }
    }
    else
    {
        if( (msg_size >= 0) )
        {
            status = -EPROTO;
        }
        else
        {
            status = -errno;
        }
    }    

    return (status);
}

int32_t recv_control_msg( uint8_t card, int32_t conn_sock )
{
    int32_t status=0;
    skiq_daemon_req_msg_t req_msg;
    ssize_t msg_size;
    uint32_t host_som=0;
    uint32_t host_request_type=0;
    uint32_t host_seq_number=0;
    uint32_t host_payload_len=0;

    errno = 0;
    msg_size = read( conn_sock, &(req_msg), SKIQ_DAEMON_REQ_SIZE );

    if( msg_size == SKIQ_DAEMON_REQ_SIZE )
    {
        // convert ntoh
        host_som = ntohl(req_msg.som);
        host_request_type = ntohl(req_msg.request_type);
        host_seq_number = ntohl(req_msg.seq_number);
        host_payload_len = ntohl(req_msg.payload_len);

        if( host_som == DAEMON_HDR_SOM )
        {
            switch( host_request_type )
            {
                case SKIQ_DAEMON_RELEASE_SOCKET:
                    status = ESRCH;
                    break;

                case SKIQ_DAEMON_RELEASE_CARD:
                    status = handle_release_cards( card, conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_READ_FPGA_REG:
                    status = handle_read_fpga_reg( card, conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_WRITE_FPGA_REG:
                    status = handle_write_fpga_reg( card, conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_FPGA_REG_VERIFY:
                    status = handle_fpga_reg_verify( card, conn_sock, host_seq_number, host_payload_len );
                    break;
                
                case SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY:
                    status = handle_fpga_reg_write_and_verify( card, conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_RX_CONFIGURE:
                    status = handle_rx_configure( card, conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_RX_PREP_SOCK:
                    // intentional fall-thru
                case SKIQ_DAEMON_RX_START_STREAMING:
                    // intentional fall-thru
                case SKIQ_DAEMON_RX_STOP_STREAMING:
                    // intentional fall-thru
                case SKIQ_DAEMON_RX_PAUSE_STREAMING:
                    // intentional fall-thru
                case SKIQ_DAEMON_RX_RESUME_STREAMING:
                    // intentional fall-thru
                case SKIQ_DAEMON_RX_FLUSH:
                    status = handle_rx_stream_ctrl( card, conn_sock, host_seq_number, ntohl(req_msg.request_type) );
                    break;

                case SKIQ_DAEMON_TX_CONFIGURE:
                    status = handle_tx_configure( card, conn_sock, host_seq_number, host_payload_len );
                    break;

                case SKIQ_DAEMON_TX_START_STREAMING:
                    // intentional fall-thru
                case SKIQ_DAEMON_TX_PRE_STOP_STREAMING:
                    // intentional fall-thru
                case SKIQ_DAEMON_TX_STOP_STREAMING:
                    // intentional fall-thru
                    status = handle_tx_stream_ctrl( card, conn_sock, host_seq_number, ntohl(req_msg.request_type) );
                    break;

                default:
                    status = -EINVAL;
                    fprintf(stderr, "Error: got unsupported request %u on control port\n", host_request_type);
                    break;
            }
        }
        else
        {
            status = -EPROTO;
        }
    }
    else
    {
        if( msg_size > 0 )
        {
            status = -EPROTO;
        }
        else if( msg_size == 0 )
        {
            status = -ESRCH;
        }
        else
        {
            // we'll have timeouts if there's nothing going on with the interface
            if( errno != EAGAIN )
            {
                fprintf(stderr, "Error: unable to read control port (errno=%u)\n", errno);
                status = -errno;
            }
        }
    }
    
    return (status);
}

int32_t handle_get_api( int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_api_payload_t api_payload;

    // send the response header
    status = _net_send_resp( conn_sock, 0, seq_number, SKIQ_DAEMON_API_PAYLOAD_SIZE );
    if( status == 0 )
    {
        // now send the payload
        api_payload.maj = SKIQ_DAEMON_API_MAJOR;
        api_payload.min = SKIQ_DAEMON_API_MINOR;
        api_payload.patch = SKIQ_DAEMON_API_PATCH;
        status = _net_send_payload( conn_sock, &api_payload, SKIQ_DAEMON_API_PAYLOAD_SIZE );
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to send payload (status=%d)\n", status);
        }
    }
    else
    {
        fprintf(stderr, "Error: unable to send response message (status=%d)\n", status);
    }

    return (status);
}

int32_t handle_get_cards( int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    uint8_t num_cards=0;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t *p_card_ids;
    uint32_t send_payload_len;
    int32_t send_status=0;

    // determine the card list
    if( (status=skiq_get_cards( skiq_xport_type_auto, &num_cards, cards )) == 0 )
    {
        // allocate memory for card info
        p_card_ids = malloc( sizeof(uint8_t) * num_cards );
        if( p_card_ids != NULL )
        {
            memcpy( p_card_ids, cards, num_cards );

            send_payload_len = sizeof(uint8_t)*num_cards;
            status = _net_send_resp( conn_sock, 0, seq_number, send_payload_len );
            if( status == 0 )
            {
                // now send the payload
                status = _net_send_payload( conn_sock, p_card_ids, send_payload_len );
                if( status != 0 )
                {
                    fprintf(stderr, "Error: failed to send payload for get_cards\n");
                }
            }
            else
            {
                fprintf(stderr, "Error: failed to send response for get_cards\n");
            }
            free( p_card_ids );
        }
        else
        {
            fprintf(stderr, "Error: unable to allocate memory for cards\n");
            if( (send_status=_net_send_resp( conn_sock, -ENOMEM, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send status (%d)\n", send_status );
            }
            status = -ENOMEM;
        }
    }
    else
    {
        fprintf(stderr, "Error: unable to determine cards available (status=%d)\n", status);
        if( (send_status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
        {
            fprintf(stderr, "Error: unable to send status (%d)\n", send_status );
        }
    }

    return (status);
}

int32_t handle_reserve_cards( int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_reserve_card_payload_t reserve_card;
    skiq_daemon_port_resp_payload_t port_payload;
    card_socket_t *p_ctrl_card_sock;
    skiq_xport_init_level_t init_level;
    int32_t send_status=0;
    
    if( payload_len != SKIQ_DAEMON_RESERVE_CARD_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        // receive the reserve card payload
        status = _net_receive_payload( conn_sock, &reserve_card, SKIQ_DAEMON_RESERVE_CARD_PAYLOAD_SIZE );
        if( status == 0 )
        {
            init_level = ntohl( reserve_card.level );

            // attempt to enable card requested
            status = skiq_enable_cards( &(reserve_card.card_num),
                                        1,
                                        init_level );

            // disable the IOE cache
            io_exp_enable_cache( reserve_card.card_num, false );

            // if we successfully enabled it, then setup the control socket
            if( status == 0 )
            {
                // create the control card/sock struct
                p_ctrl_card_sock = (card_socket_t*)(malloc(sizeof(card_socket_t)));
                if( p_ctrl_card_sock != NULL )
                {
                    p_ctrl_card_sock->card = reserve_card.card_num;
                    // if socket created successfully, wait for a connection
                    p_ctrl_card_sock->sock = create_tcp_socket( control_port_num );
                    if( p_ctrl_card_sock->sock >= 0 )
                    {
                        // TODO: is backlog of 0 ok?
                        if( listen( p_ctrl_card_sock->sock, 1 ) >= 0 )
                        {
                            // spawn thread to accept connections and receive messages
                            // note: thread will cleanup p_ctrl_card_sock
                            status = pthread_create( &(ctrl_rx_thread), NULL,
                                                     process_ctrl_rx,
                                                     (void*)(p_ctrl_card_sock) );
                            // check for thread created successfully
                            if( status != 0 )
                            {
                                fprintf(stderr, "Error: unable to create ctrl thread (status=%d)\n", status);
                                skiq_disable_cards( &(reserve_card.card_num), 1 );
                                close( p_ctrl_card_sock->sock );
                                free(p_ctrl_card_sock);
                                p_ctrl_card_sock = NULL;
                            }
                            else
                            {
                                // create the TX/RX thread if it's a full init
                                if( init_level == skiq_xport_init_level_full )
                                {
                                    // if the TX thread doesn't exist, we need to create it here
                                    pthread_mutex_lock( &_tx_thread_state_mutex );
                                    if( _tx_thread_state == TX_THREAD_DESTROYED )
                                    {
                                        pthread_mutex_unlock( &_tx_thread_state_mutex );
                                        // create TX stream thread
                                        status = create_stream_thread( reserve_card.card_num, XCV_TYPE_TX, true );
                                    } // end of thread destroyed
                                    else
                                    {
                                        pthread_mutex_unlock( &_tx_thread_state_mutex );
                                        // make sure it's in the stopped state
                                        push_signal( TX_SIGNAL_STOP_STREAMING, XCV_TYPE_TX );
                                        // wait until we enter the stopped streaming state
                                        status=wait_for_state_transition( TX_THREAD_STOPPED, MAX_TX_STOPPED_US, MAX_TX_STOPPED_TRANS, XCV_TYPE_TX );
                                    }

                                    if( status == 0 )
                                    {
                                        // if the RX thread doesn't exist, we need to create it here
                                        pthread_mutex_lock( &_rx_thread_state_mutex );
                                        if( _rx_thread_state == RX_THREAD_DESTROYED )
                                        {
                                            pthread_mutex_unlock( &_rx_thread_state_mutex );
                                            // create RX stream thread
                                            status = create_stream_thread( reserve_card.card_num, XCV_TYPE_RX, false );
                                        } // end of thread destroyed
                                        else
                                        {
                                            pthread_mutex_unlock( &_rx_thread_state_mutex );
                                            // make sure it's in the stopped state
                                            push_signal( RX_SIGNAL_STOP_STREAMING, XCV_TYPE_RX );
                                            // wait until we enter the stopped streaming state
                                            status=wait_for_state_transition( RX_THREAD_STOPPED, MAX_RX_STOPPED_US, MAX_RX_STOPPED_TRANS, XCV_TYPE_RX );
                                        }
                                    } // end TX stream thread created
                                } // end of full init
                            } // end status==0
                        } // end socket listen ok
                        else
                        {
                            status = -errno;
                            skiq_disable_cards( &(reserve_card.card_num),
                                                1 );
                            close( p_ctrl_card_sock->sock );
                            free(p_ctrl_card_sock);
                            p_ctrl_card_sock = NULL;
                        }
                    } // end socket created successfully
                    else
                    {
                        status = -EIO;
                        skiq_disable_cards( &(reserve_card.card_num),
                                            1 );
                        free(p_ctrl_card_sock);
                        p_ctrl_card_sock = NULL;
                    }
                } // end card/sock struct allocated successfully
                else
                {
                    status = -ENOMEM;
                    skiq_disable_cards( &(reserve_card.card_num),
                                        1 );
                }
            } // end enable cards successful
            else
            {
                fprintf(stderr, "Error: enable cards failed %d\n", status);
            }
            send_status = _net_send_resp( conn_sock, status, seq_number, SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE );
            if( send_status == 0 )
            {
                // send the control port
                port_payload.port = htons( control_port_num );
                if( (send_status = _net_send_payload(conn_sock, &port_payload, SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE)) != 0 )
                {
                    fprintf(stderr, "Error: unable to send port resp (%d)\n", send_status);
                }
            }
            else
            {
                fprintf(stderr, "Error: unable to send net resp (%d)\n", send_status);
            }
        } // end payload received successfully
    } // end payload size ok
    
    return (status);
}

int32_t handle_release_cards( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    
    status = skiq_disable_cards( &(card), 1 );
    if( status != 0 )
    {
        printf("Warning: failed to disable card with status %d\n", status);
    }

    pthread_mutex_lock( &_tx_thread_state_mutex );
    if( _tx_thread_state != TX_THREAD_DESTROYED )
    {
        pthread_mutex_unlock( &_tx_thread_state_mutex );
        push_signal( TX_SIGNAL_RELEASE_CARD, XCV_TYPE_TX );
        // wait until we know the thread is destroyed
        status=wait_for_state_transition( TX_THREAD_DESTROYED, MAX_TX_DESTROYED_US, MAX_TX_DESTROYED_TRANS, XCV_TYPE_TX );
        if( status == 0 )
        {
            pthread_cancel( stream_tx_thread );
            pthread_join( stream_tx_thread, NULL );

            // unreference the queues after the stream thread is joined
            g_async_queue_unref( _p_tx_thread_msg_queue );
            g_async_queue_unref( _p_tx_thread_state_update );
        }
    }
    else
    {
        pthread_mutex_unlock( &_tx_thread_state_mutex );
    }

    if( status == 0 )
    {
        pthread_mutex_lock( &_rx_thread_state_mutex );
        if( _rx_thread_state != RX_THREAD_DESTROYED )
        {
            pthread_mutex_unlock( &_rx_thread_state_mutex );
            push_signal( RX_SIGNAL_RELEASE_CARD, XCV_TYPE_RX );
            // wait until we know the thread is destroyed
            status=wait_for_state_transition( RX_THREAD_DESTROYED, MAX_RX_DESTROYED_US, MAX_RX_DESTROYED_TRANS, XCV_TYPE_RX );
            if( status == 0 )
            {
                pthread_cancel( stream_rx_thread );
                pthread_join( stream_rx_thread, NULL );

                // unreference the queues after the stream thread is joined
                g_async_queue_unref( _p_rx_thread_msg_queue );
                g_async_queue_unref( _p_rx_thread_state_update );
            }
        }
        else
        {
            pthread_mutex_unlock( &_rx_thread_state_mutex );
        }
    }

    status = _net_send_resp( conn_sock, status, seq_number, 0 );    
    if( (status != 0) )
    {
        fprintf(stderr, "Error: unable to send release card response %d\n", status);
    }
    
    return (ECONNRESET);
}

int32_t handle_read_fpga_reg( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_read_fpga_req_payload_t read_fpga;
    skiq_daemon_read_fpga_resp_t resp_msg;
    uint32_t val=0;
    ssize_t msg_size;
    
    if( payload_len != SKIQ_DAEMON_READ_FPGA_REQ_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        status = _net_receive_payload( conn_sock, &read_fpga, payload_len );
        if( status == 0 )
        {
            // for efficiency, we need to send response and payload in one shot
            resp_msg.header.som = htonl( DAEMON_HDR_SOM );
            resp_msg.header.status = htonl( sidekiq_fpga_reg_read( card,
                                                                    ntohl( read_fpga.addr ),
                                                                    &val ) );
            resp_msg.header.seq_number = htonl( seq_number );
            resp_msg.header.payload_len = htonl( SKIQ_DAEMON_READ_FPGA_RESP_PAYLOAD_SIZE );
            resp_msg.payload.val = htonl( val );

            errno = 0;
            msg_size = send( conn_sock, &resp_msg, SKIQ_DAEMON_READ_FPGA_RESP_SIZE, 0 );
            if( msg_size < 0 )
            {
                status = -errno;
            }
            else if( msg_size < SKIQ_DAEMON_READ_FPGA_RESP_SIZE )
            {
                status = -EPROTO;
            }
        }
    }
    
    return (status);    
}

int32_t handle_write_fpga_reg( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_write_fpga_payload_t write_fpga;

    if( payload_len != SKIQ_DAEMON_WRITE_FPGA_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        status = _net_receive_payload( conn_sock, &write_fpga, payload_len );
        if( status == 0 )
        {
            status = sidekiq_fpga_reg_write( card,
                                             ntohl( write_fpga.addr ),
                                             ntohl( write_fpga.val ));
            status = _net_send_resp( conn_sock, status, seq_number, 0 );
        }
    }
    
    return (status);
}

int32_t handle_fpga_reg_verify( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_fpga_reg_verify_payload_t fpga_reg_verify;

    if( payload_len != SKIQ_DAEMON_FPGA_REG_VERIFY_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        status = _net_receive_payload( conn_sock, &fpga_reg_verify, payload_len );
        if( status == 0 )
        {
            status = sidekiq_fpga_reg_verify( card,
                                              ntohl( fpga_reg_verify.addr ),
                                              ntohl( fpga_reg_verify.val ) );
            status = _net_send_resp( conn_sock, status, seq_number, 0 );
        }
    }
    
    return (status);
}

int32_t handle_fpga_reg_write_and_verify( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_fpga_reg_write_and_verify_payload_t write_and_verify;

    if( payload_len != SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        status = _net_receive_payload( conn_sock, &write_and_verify, payload_len );
        if( status == 0 )
        {
            status = sidekiq_fpga_reg_write_and_verify( card,
                                                        ntohl( write_and_verify.addr ),
                                                        ntohl( write_and_verify.val ) );
            status = _net_send_resp( conn_sock, status, seq_number, 0 );
        }
    }
    
    return (status);
}

int32_t handle_rx_configure( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_rx_configure_payload_t rx_config;

    if( payload_len != SKIQ_DAEMON_RX_CONFIGURE_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        status = _net_receive_payload( conn_sock, &rx_config, payload_len );
        if( status == 0 )
        {
            // note that technically we should have mutex protecting this, but it is
            // less than desired to have a mutex in the receive loop, so we just leave
            // this unprotected...worst case, we sleep for an incorrect amount of time
            _usleep_per_skiq_pkt = ntohl( rx_config.usleep_per_skiq_rx );
            if( rx_config.buffered == 1 )
            {
                status = skiq_write_rx_stream_mode( card, skiq_rx_stream_mode_high_tput );
            }
            else
            {
                status = skiq_write_rx_stream_mode( card, skiq_rx_stream_mode_balanced );
            }
            status = _net_send_resp( conn_sock, status, seq_number, 0 );
        }
    }

    return (status);
}

int32_t wait_for_state_transition( int32_t req_state, uint32_t max_num_us_per_state, uint32_t max_state_trans, xcv_type_t type )
{
    int32_t status=0;
    int32_t curr_state;
    int32_t *p_curr_state=NULL;
    uint32_t state_trans_count=0;
    GAsyncQueue *p_state_update;
    int32_t invalid_state;

    if( type == XCV_TYPE_TX )
    {
        p_state_update = _p_tx_thread_state_update;
        invalid_state = TX_THREAD_INVALID;
    }
    else
    {
        p_state_update = _p_rx_thread_state_update;
        invalid_state = RX_THREAD_INVALID;
    }
    curr_state = invalid_state;

    // wait until we get to the requested state or we timeout the maximum number of times
    while( (curr_state != req_state) && (state_trans_count < max_state_trans) && (status == 0) && (running==true) )
    {
        p_curr_state = g_async_queue_timeout_pop( p_state_update, max_num_us_per_state );
        if( p_curr_state != NULL )
        {
            curr_state = (int32_t)(GPOINTER_TO_INT(p_curr_state));
            if( curr_state >= invalid_state )
            {
                // typically a thread error is due to receiving an invalid signal by the streaming state machine
                fprintf(stderr, "Error: thread state reported error when waiting for state %u\n", req_state);
                status = -EINVAL;
            }
        }
        state_trans_count++;
    } // end waiting for streaming

    if( (curr_state != req_state) && (status==0) )
    {
        fprintf(stderr, "Error: timed out waiting for state transition (req_state=%u) (type=%s) (curr=%u)\n", 
            req_state, xcv_type_cstr(type), curr_state);
        status = -ETIMEDOUT;
    }
    return (status);
}

int32_t handle_rx_stream_ctrl( uint8_t card, int32_t conn_sock, uint32_t seq_number, skiq_daemon_req_t rx_ctrl )
{
    int32_t status=-EPROTO;
    skiq_daemon_port_resp_payload_t port_payload;
    rx_thread_state_t curr_state;

    // cache the current thread state
    pthread_mutex_lock( &_rx_thread_state_mutex );
    curr_state = _rx_thread_state;
    pthread_mutex_unlock( &_rx_thread_state_mutex );

    // we can't handle any message if the RX thread is destroyed
    if( curr_state == RX_THREAD_DESTROYED )
    {
        // send the response
        if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
        {
            fprintf(stderr, "Error: unable to send response (status=%d)", status);
        }            
        return (status);
    }

    // we only got here if we're not in the DESTROYED state

    switch( rx_ctrl )
    {
        case SKIQ_DAEMON_RX_PREP_SOCK:
            // save off the RX port number
            port_payload.port = htons( rx_stream_port_num );
            
            // we've already setup the socket, nothing to do here
            if( curr_state == RX_THREAD_SOCK_READY )
            {
                // we're already waiting for a SOM...just send the port num
                status = 0;
            }
            // as long as we're not already streaming, request it setup and wait
            else if( (curr_state != RX_THREAD_STREAMING) )
            {
                // push the start streaming message onto the message queue
                push_signal( RX_SIGNAL_SETUP_SOCK, XCV_TYPE_RX );

                // wait until we have the socket ready
                status=wait_for_state_transition( RX_THREAD_SOCK_READY, MAX_RX_SOCK_READY_US, MAX_RX_SOCK_READY_TRANS, XCV_TYPE_RX );
            }

            // send the response
            status = _net_send_resp( conn_sock, status, seq_number, SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE );
            if( status == 0 )
            {
                // send the RX port
                status = _net_send_payload( conn_sock, &port_payload, SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE );
            }
            break;

        case SKIQ_DAEMON_RX_START_STREAMING:
            if(  curr_state == RX_THREAD_STREAMING )
            {
                // nothing to do, we're already streaming
                status = 0;
            }
            else
            {
                // push the start streaming onto the message queue
                push_signal( RX_SIGNAL_START_STREAMING, XCV_TYPE_RX );
                // wait until we enter the streaming state
                status=wait_for_state_transition( RX_THREAD_STREAMING, MAX_RX_STREAMING_US, MAX_RX_STREAMING_TRANS, XCV_TYPE_RX );
            }    

            // send the response
            if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;
            
        case SKIQ_DAEMON_RX_STOP_STREAMING:
            // if we're already stopped, nothing to do here...
            if( curr_state == RX_THREAD_STOPPED )
            {
                // nothing to do
                status = 0;
            }
            else
            {
                // push the stop streaming onto the message queue
                push_signal( RX_SIGNAL_STOP_STREAMING, XCV_TYPE_RX );
                // wait until we enter the streaming state
                status=wait_for_state_transition( RX_THREAD_STOPPED, MAX_RX_STOPPED_US, MAX_RX_STOPPED_TRANS, XCV_TYPE_RX );
            }

            // send the response
            if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;
            
        case SKIQ_DAEMON_RX_PAUSE_STREAMING:
            // if we're already paused, nothing to do here...
            if( curr_state == RX_THREAD_PAUSED )
            {
                // do nothing
                status = 0;
            }
            else
            {
                // push the stop streaming onto the message queue
                push_signal( RX_SIGNAL_PAUSE_STREAMING, XCV_TYPE_RX );
                // wait until we enter the streaming state
                status=wait_for_state_transition( RX_THREAD_PAUSED, MAX_RX_PAUSED_US, MAX_RX_PAUSED_TRANS, XCV_TYPE_RX );
            }

            if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;
            
        case SKIQ_DAEMON_RX_RESUME_STREAMING:
            // if we're already paused, nothing to do here...
            if( curr_state == RX_THREAD_STREAMING )
            {
                // do nothing
                status = 0;
            }
            else
            {
                // push the stop streaming onto the message queue
                push_signal( RX_SIGNAL_RESUME_STREAMING, XCV_TYPE_RX );
                // wait until we enter the streaming state
                status=wait_for_state_transition( RX_THREAD_STREAMING, MAX_RX_STREAMING_US, MAX_RX_STREAMING_TRANS, XCV_TYPE_RX );
            }

            if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;
            
        case SKIQ_DAEMON_RX_FLUSH:
            // flush RX
            // if we're already stopped, nothing to do here...
            if( curr_state == RX_THREAD_STOPPED )
            {
                // do nothing
                status = 0;
            }
            else
            {
                // push the stop streaming onto the message queue
                push_signal( RX_SIGNAL_FLUSH_STREAMING, XCV_TYPE_RX );
                // wait until we enter the streaming state
                status=wait_for_state_transition( RX_THREAD_FLUSHED, MAX_RX_FLUSHED_US, MAX_RX_FLUSHED_TRANS, XCV_TYPE_RX );
            }

            if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;

        default:
            status = -EPROTO;
            fprintf(stderr, "Error: got unsupported rx_stream_ctrl %u\n", rx_ctrl);
            break;
    }

    return (status);
}

int32_t handle_tx_configure( uint8_t card, int32_t conn_sock, uint32_t seq_number, uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_tx_configure_payload_t tx_config;
    uint32_t num_bytes_to_send;
    int32_t send_status=0;

    if( payload_len != SKIQ_DAEMON_TX_CONFIGURE_PAYLOAD_SIZE )
    {
        status = -EPROTO;
    }
    else
    {
        status = _net_receive_payload( conn_sock, &tx_config, payload_len );
        if( status == 0 )
        {
            num_bytes_to_send = ntohl(tx_config.num_bytes_to_send);
            status = skiq_write_tx_block_size( card, 
                                               skiq_tx_hdl_A1, 
                                               (num_bytes_to_send/sizeof(uint32_t))-SKIQ_TX_HEADER_SIZE_IN_WORDS );
            if( status == 0 )
            {
                status = skiq_xport_tx_initialize( card,
                                                   ntohl(tx_config.transfer_mode),
                                                   ntohl(tx_config.num_bytes_to_send),
                                                   tx_config.num_send_threads,
                                                   ntohl(tx_config.priority ),
                                                   NULL );
            }
            if( (send_status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send status (%d)\n", send_status );
            }
        }
    }

    return (status);
}

int32_t handle_tx_stream_ctrl( uint8_t card, int32_t conn_sock, uint32_t seq_number,  skiq_daemon_req_t tx_ctrl )
{
    int32_t status=-EPROTO;
    skiq_daemon_port_resp_payload_t port_payload;
    tx_thread_state_t curr_state;
    int32_t send_status=0;

    // cache the current thread state
    pthread_mutex_lock( &_tx_thread_state_mutex );
    curr_state = _tx_thread_state;
    pthread_mutex_unlock( &_tx_thread_state_mutex );

    // we can't handle any message if the TX thread is destroyed
    if( curr_state == TX_THREAD_DESTROYED )
    {
        // send the response
        if( (status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
        {
            fprintf(stderr, "Error: unable to send response (status=%d)", status);
        }            
        return (status);
    }

    // we only got here if we're not in the DESTROYED state
    switch( tx_ctrl )
    {
        case SKIQ_DAEMON_TX_START_STREAMING:
            // save off the TX port number
            port_payload.port = htons( tx_stream_port_num );
            
            // we've already setup the socket, nothing to do here
            if( curr_state == TX_THREAD_STREAMING )
            {
                // we're already streaming...just send the port num
                status = 0;
            }
            else
            {
                // push the start streaming message onto the message queue
                push_signal( TX_SIGNAL_START_STREAMING, XCV_TYPE_TX );

                // wait until we have the socket ready
                status=wait_for_state_transition( TX_THREAD_STREAMING, MAX_TX_STREAMING_US, MAX_TX_STREAMING_TRANS, XCV_TYPE_TX );
            }

            // send the response
            send_status = _net_send_resp( conn_sock, status, seq_number, SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE );
            if( send_status == 0 )
            {
                // send the TX port
                send_status = _net_send_payload( conn_sock, &port_payload, SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE );
            }
            break;

        case SKIQ_DAEMON_TX_PRE_STOP_STREAMING:
            // if we're already stopped, nothing to do here...
            if( curr_state == TX_THREAD_STOPPED )
            {
                // nothing to do
                status = 0;
            }
            else
            {
                // no signaling needed, but we need to stop the xport so we can process additional signals
                pthread_mutex_lock( &_tx_xport_mutex );
                status = skiq_xport_tx_stop_streaming( card, skiq_tx_hdl_A1 );
                pthread_mutex_unlock( &_tx_xport_mutex );
            }

            // send the response
            if( (send_status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;

        case SKIQ_DAEMON_TX_STOP_STREAMING:
            // if we're already stopped, nothing to do here...
            if( curr_state == TX_THREAD_STOPPED )
            {
                // nothing to do
                status = 0;
            }
            else
            {
                // push the stop streaming onto the message queue
                push_signal( TX_SIGNAL_STOP_STREAMING, XCV_TYPE_TX );
                // wait until we enter the stopped state
                status=wait_for_state_transition( TX_THREAD_STOPPED, MAX_TX_STOPPED_US, MAX_TX_STOPPED_TRANS, XCV_TYPE_TX );
            }

            // send the response
            if( (send_status=_net_send_resp( conn_sock, status, seq_number, 0 )) != 0 )
            {
                fprintf(stderr, "Error: unable to send response (status=%d)", status);
            }
            break;

        default:
            status = -EPROTO;
            fprintf(stderr, "Error: got unsupported tx_stream_ctrl %u\n", tx_ctrl);
            break;
    }

    return (status);
}

void push_signal( int32_t signal, xcv_type_t type )
{
    GAsyncQueue *p_queue;
    if( type == XCV_TYPE_TX )
    {
        p_queue = _p_tx_thread_msg_queue;
    }
    else
    {
        p_queue = _p_rx_thread_msg_queue;
    }

    g_async_queue_push( p_queue,
                        GINT_TO_POINTER(signal) );
}

void push_thread_state_update( int32_t thread_state, xcv_type_t type )
{
    GAsyncQueue *p_queue;
    if( type == XCV_TYPE_TX )
    {
        p_queue = _p_tx_thread_state_update;
    }
    else
    {
        p_queue = _p_rx_thread_state_update;
    }

    g_async_queue_push( p_queue,
                        GINT_TO_POINTER(thread_state) );
}

// returns the new state to transition to
rx_thread_state_t process_rx_stopped_state( uint8_t card )
{
    int32_t status=0;
    gpointer p_rx_signal = NULL;
    rx_signal_t rx_signal;
    rx_thread_state_t new_state = RX_THREAD_STOPPED;

    pthread_mutex_lock( &_rx_thread_state_mutex );
    // if we're not already stopped or destroyed, the stream needs to be stopped
    if( (_rx_thread_state != RX_THREAD_STOPPED) &&
        (_rx_thread_state != RX_THREAD_DESTROYED) )
    {
        // call stop streaming upon entry to the stop state
        // TODO: Do the xport calls need to be protected with a mutex
        status=skiq_xport_rx_stop_streaming( card, skiq_rx_hdl_A1 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to stop streaming (%d)\n", status);
        }
    }
    _rx_thread_state = RX_THREAD_STOPPED;
    pthread_mutex_unlock( &_rx_thread_state_mutex );

    // notify we're in the stopped state
    push_thread_state_update( RX_THREAD_STOPPED, XCV_TYPE_RX );
    
    // now we just wait for a message to do something...
    p_rx_signal = g_async_queue_pop( _p_rx_thread_msg_queue );

    if( p_rx_signal != NULL )
    {
        rx_signal = (rx_signal_t)(GPOINTER_TO_INT(p_rx_signal));

        switch( rx_signal )
        {
            case RX_SIGNAL_SETUP_SOCK:
                new_state = RX_THREAD_STREAMING;
                break;

            case RX_SIGNAL_RELEASE_CARD:
                // card release requested
                new_state = RX_THREAD_DESTROYED;
                break;

            case RX_SIGNAL_START_STREAMING:
                // intentional fall-thru
            case RX_SIGNAL_STOP_STREAMING:
                // intentional fall-thru
            case RX_SIGNAL_PAUSE_STREAMING:
                // intentional fall-thru
            case RX_SIGNAL_RESUME_STREAMING:
                // intentional fall-thru
            case RX_SIGNAL_FLUSH_STREAMING:
                // intentional fall-thru
            default:
                fprintf(stderr, "Error: received unexpected state request in RX signal=%u\n", rx_signal );
                push_thread_state_update( RX_THREAD_INVALID, XCV_TYPE_RX );
                new_state = RX_THREAD_DESTROYED;
                break;
        }
    }

    return (new_state);
}


void send_dummy_pkts(int sock_fd, 
                    struct sockaddr* p_client_addr,
                    int client_addr_len,
                    uint32_t num_pkts )
{
    uint32_t dummy_block[SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS];
    uint32_t i=0;
    uint32_t pkt_length =  SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES;

    memset( dummy_block, 0, pkt_length );

    for( i=0; (i<num_pkts) && (running == true); i++ )
    {
        errno = 0;
        if( sendto(sock_fd, dummy_block, pkt_length, MSG_CONFIRM,
                    p_client_addr, client_addr_len ) != pkt_length )
        {
            fprintf(stderr, "Error: unable to send dummy packet %u, errno %d\n", i, errno);
        }
    }
}

rx_thread_state_t wait_for_rx_start_signal( void )
{
    rx_thread_state_t new_state = RX_THREAD_INVALID;
    gpointer p_rx_signal=NULL;
    rx_signal_t rx_signal;

    // We've already received SOM, the only thing we expect is to either be stopped or started
    /* wait forever for our next signal */
    p_rx_signal = g_async_queue_pop( _p_rx_thread_msg_queue );
    if( p_rx_signal != NULL )
    {
        rx_signal = (rx_signal_t)(GPOINTER_TO_INT(p_rx_signal));
        switch( rx_signal )
        {
            case RX_SIGNAL_START_STREAMING:
                new_state = RX_THREAD_STREAMING;
                break;

            case RX_SIGNAL_STOP_STREAMING:
                // switch to the stopped state
                new_state = RX_THREAD_STOPPED;
                break;

             case RX_SIGNAL_RELEASE_CARD:
                // card release requested
                new_state = RX_THREAD_DESTROYED;
                break;

            case RX_SIGNAL_SETUP_SOCK:
                // intentional fall-thru
            case RX_SIGNAL_PAUSE_STREAMING:
                // intentional fall-thru
            case RX_SIGNAL_RESUME_STREAMING:
                // intentional fall-thru
            case RX_SIGNAL_FLUSH_STREAMING:
                // intentional fall-thru
            default:
                // error: unknown / invalid request report error and cleanup
                fprintf(stderr, "Error: received unexpected RX state request signal=%u\n", rx_signal );
                push_thread_state_update( RX_THREAD_INVALID, XCV_TYPE_RX );
                new_state = RX_THREAD_DESTROYED;
                break;
        }
        ////////////////////////////////////////////////////
    }

    return (new_state);
}

rx_thread_state_t process_rx_streaming_signals( uint8_t card, gpointer p_rx_signal )
{
    rx_thread_state_t new_state = RX_THREAD_STREAMING;
    skiq_rx_status_t rx_status=skiq_rx_status_no_data;
    skiq_rx_block_t *p_rx_block=NULL;
    uint32_t data_len=0;
    uint32_t flush_count=0;
    rx_signal_t rx_signal;

    while( (p_rx_signal != NULL) && (running == true) )
    {
        rx_signal = (rx_signal_t)(GPOINTER_TO_INT( p_rx_signal ));
        switch( rx_signal )
        {
            case RX_SIGNAL_START_STREAMING:
                // we're already doing it...nothing to do
                p_rx_signal = NULL;
                break;

            case RX_SIGNAL_STOP_STREAMING:
                p_rx_signal = NULL;
                // switch to the stopped state
                new_state = RX_THREAD_STOPPED;
                break;

            case RX_SIGNAL_PAUSE_STREAMING:
                // acknowledge we're paused 
                pthread_mutex_lock( &_rx_thread_state_mutex );
                _rx_thread_state = RX_THREAD_PAUSED;
                pthread_mutex_unlock( &_rx_thread_state_mutex );
                push_thread_state_update( RX_THREAD_PAUSED, XCV_TYPE_RX );
                // wait for the next transition
                p_rx_signal = g_async_queue_pop( _p_rx_thread_msg_queue );
                break;

            case RX_SIGNAL_RESUME_STREAMING:
                // acknowledge that we're resumed
                pthread_mutex_lock( &_rx_thread_state_mutex );
                _rx_thread_state = RX_THREAD_STREAMING;
                pthread_mutex_unlock( &_rx_thread_state_mutex );
                push_thread_state_update( RX_THREAD_STREAMING, XCV_TYPE_RX );
                // we want to fall through to start streaming
                p_rx_signal = NULL;
                break;

            case RX_SIGNAL_FLUSH_STREAMING:
                // just receive data until we get no data returned or max flushes reached
                do
                {
                    // TODO: Do the xport calls need to be protected with a mutex
                    rx_status = (skiq_rx_status_t)(skiq_xport_rx_receive( card, 
                                                                          (uint8_t **)&p_rx_block,
                                                                          &data_len ));
                    flush_count++;
                } while( (rx_status == skiq_rx_status_success) && (flush_count<MAX_NUMBER_OF_FLUSHES) );

                // acknowledge that we're resumed
                pthread_mutex_lock( &_rx_thread_state_mutex );
                _rx_thread_state = RX_THREAD_FLUSHED;
                pthread_mutex_unlock( &_rx_thread_state_mutex );
                push_thread_state_update( RX_THREAD_FLUSHED, XCV_TYPE_RX );
                // wait for the next transition
                p_rx_signal = g_async_queue_pop( _p_rx_thread_msg_queue );
                break;

            case RX_SIGNAL_RELEASE_CARD:
                // card release requested
                new_state = RX_THREAD_DESTROYED;
                p_rx_signal = NULL;
                break;

            case RX_SIGNAL_SETUP_SOCK:
                // intentional fall-thru
            default:
                // error: unknow / invalid request...stay in the stopped state
                fprintf(stderr, "Error: received unknown request type %u in streaming state\n", rx_signal);
                push_thread_state_update( RX_THREAD_INVALID, XCV_TYPE_RX );
                new_state = RX_THREAD_DESTROYED;
                break;
        } // end switch of processing signals
    } // end of running or new events to process

    return new_state;
}

bool receive_and_send( uint8_t card, int sock_fd, struct sockaddr* p_client_addr, int client_addr_len )
{
    static bool wait_for_next_rx = false;
    static uint32_t consecutive_bytes=0;
    
    skiq_rx_block_t *p_rx_block=NULL;
    uint32_t data_len=0;
    uint32_t usleep_per_pkt=0;
    bool critical_error=false;
    int32_t send_count=0;
    skiq_rx_status_t rx_status=skiq_rx_status_no_data;
    bool send_success=false;
    uint32_t retry_count=0;

    // if we previously had no data and we're still trying to stream, let's wait a bit and get the data
    if( wait_for_next_rx == true )
    {
        usleep_per_pkt = _usleep_per_skiq_pkt;
        hal_microsleep( usleep_per_pkt );
        consecutive_bytes = 0;
        wait_for_next_rx = false;
    }

    // receive data
    // TODO: Do the xport calls need to be protected with a mutex
    if( (rx_status = (skiq_rx_status_t)(skiq_xport_rx_receive( card, 
                                                               (uint8_t **)&p_rx_block,
                                                               &data_len ))) == skiq_rx_status_success )
    {
                
        // if we've sent a lot of packets in a row, sleep for a bit to allow the
        // queue to clear up before sending more
        consecutive_bytes += data_len;
        if( consecutive_bytes >= 
            (NUM_CONSECUTIVE_PKTS*SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES) ) 
        {
            usleep_per_pkt = _usleep_per_skiq_pkt;
            hal_microsleep( usleep_per_pkt );
            // reset our consecutive count
            consecutive_bytes = 0;
        }

        // reset the send success flag
        send_success = false;

        // retry sending the packet if there's an error
        for( retry_count=0; (retry_count<max_send_retry) && (send_success==false) && (running==true); retry_count++ )
        {
            errno = 0;
            send_count += sendto(sock_fd, p_rx_block+send_count, data_len-send_count, MSG_DONTWAIT,
                            p_client_addr, client_addr_len);
            if( send_count != data_len )
            {
                if( send_count >= 0 )
                {
                    fprintf(stderr, "Error: expected to send %u bytes, but only sent %u (errno=%d)\n", data_len, send_count, errno );
                }
                /* EAGAIN or EWOULDBLOCK indicates that we should retry which is handled in the else */
                else if( (errno != EAGAIN) && (errno != EWOULDBLOCK) )
                {
                    fprintf(stderr, "Error trying to send packet (errno=%d)\n", errno );
                    // set send_count to 0 since no bytes sent
                    send_count = 0;
                }
                else
                {
                    // set send_count to 0 since no bytes sent
                    send_count = 0;
                    if( ((retry_count+1) >= max_send_retry) )
                    {
                        fprintf(stderr, "Error: tried to send packet %u times, discarding\n", retry_count);
                    }
                    else
                    {
                        usleep_per_pkt = _usleep_per_skiq_pkt;
                        usleep(usleep_per_pkt);
                        consecutive_bytes = 0;
                    }
                }
            }
            else
            {
                send_success = true;
            }
        }
    }
    else
    {
        // if we didn't get data, just wait a bit
        if( rx_status == skiq_rx_status_no_data )
        {
            wait_for_next_rx = true;
        }
        else if( rx_status == skiq_rx_status_error_overrun )
        {
            printf("Warning: overrun detected on card %u\n", card);
        }
        else
        {
            fprintf(stderr, "Error: received critical error %d when receiving samples, ending receive loop\n", rx_status);
            critical_error = true;
        }
    }
    return (critical_error);
}

rx_thread_state_t process_rx_streaming_state( int sock_fd, uint8_t card )
{
    int32_t status=0;
    uint32_t som=0;
    gpointer p_rx_signal=NULL;
    struct sockaddr_in client_addr;
    socklen_t sock_addr_len=sizeof(client_addr);
    rx_thread_state_t new_state = RX_THREAD_DESTROYED;
    bool critical_error = false;
    
    // we just entered the streaming state...wait for the SOM
    pthread_mutex_lock( &_rx_thread_state_mutex );
    _rx_thread_state = RX_THREAD_SOCK_READY;
    pthread_mutex_unlock( &_rx_thread_state_mutex );

    push_thread_state_update( RX_THREAD_SOCK_READY, XCV_TYPE_RX );

    printf("Info: trying to receive SOM (card=%u, fd=%d)\n", card, sock_fd);
    errno = 0;
    status = recvfrom(sock_fd, &som, sizeof(uint32_t), 0,
                      (struct sockaddr*)(&client_addr), &sock_addr_len);
    if( (status == sizeof(uint32_t)) &&
        (ntohl(som) == DAEMON_HDR_SOM) )
    {
        printf("Info: got SOM on streaming socket (running=%u)!\n", running);

        status = 0;
    }
    else
    {
        status = -errno;
        fprintf(stderr, "Error: Got no data or SOM incorrect, errno=%d, status=%d, som 0x%x\n",
               errno, status, ntohl(som));
    }

    if( status == 0 )
    {
        new_state = wait_for_rx_start_signal();
    }

    if( new_state == RX_THREAD_STREAMING )
    {
        send_dummy_pkts(sock_fd, 
                        (struct sockaddr*)(&client_addr),
                        sizeof(client_addr),
                        NUM_DUMMY_PKTS);

        /* we don't actually care about the handle here...the handle should already have
          been enabled in the FPGA by the client...we're just calling start here to get
          the data flowing at the transport layer */
        // TODO: Do the xport calls need to be protected with a mutex
        status = skiq_xport_rx_start_streaming( card, skiq_rx_hdl_A1 );
        if( status == 0 )
        {
            pthread_mutex_lock( &_rx_thread_state_mutex );
            _rx_thread_state = RX_THREAD_STREAMING;
            pthread_mutex_unlock( &_rx_thread_state_mutex );
            push_thread_state_update( RX_THREAD_STREAMING, XCV_TYPE_RX );
        }
        else
        {
            fprintf(stderr, "Error: failed to start streaming with status %d\n", status);
            push_thread_state_update( RX_THREAD_INVALID, XCV_TYPE_RX );
            new_state = RX_THREAD_DESTROYED;
        }
    
        while( (running == true) && (critical_error==false) && (new_state == RX_THREAD_STREAMING) )
        {
            // now we just wait for a message to do something...
            p_rx_signal = g_async_queue_try_pop( _p_rx_thread_msg_queue );

            if( p_rx_signal != NULL )
            {
                new_state = process_rx_streaming_signals( card, p_rx_signal );
            }

            // if we're still streaming, continue on...
            if( new_state == RX_THREAD_STREAMING )
            {
                critical_error = receive_and_send( card, sock_fd, (struct sockaddr*)(&client_addr), sock_addr_len );
            }
        }

        if( critical_error == true )
        {
            new_state = RX_THREAD_DESTROYED;
        }
    }

    return (new_state);
}

tx_thread_state_t process_tx_stopped_state( uint8_t card )
{
    int32_t status=0;
    tx_thread_state_t new_state = RX_THREAD_INVALID;
    gpointer p_tx_signal=NULL;
    tx_signal_t tx_signal;

    pthread_mutex_lock( &_tx_thread_state_mutex );
    // if we're not already stopped or destroyed, the stream needs to be stopped
    if( (_tx_thread_state != TX_THREAD_STOPPED) &&
        (_tx_thread_state != TX_THREAD_DESTROYED) )
    {
        // call stop streaming upon entry to the stop state
        pthread_mutex_lock( &_tx_xport_mutex );
        status = skiq_xport_tx_stop_streaming( card, skiq_tx_hdl_A1 );
        pthread_mutex_unlock( &_tx_xport_mutex );
        if( status != 0 )
        {
            fprintf(stderr, "Warning: unable to stop streaming (%d)\n", status);
        }
    }
    _tx_thread_state = TX_THREAD_STOPPED;
    pthread_mutex_unlock( &_tx_thread_state_mutex );

    // notify we're in the stopped state
    push_thread_state_update( TX_THREAD_STOPPED, XCV_TYPE_TX );

    /* wait forever for our next signal */
    p_tx_signal = g_async_queue_pop( _p_tx_thread_msg_queue );
    if( p_tx_signal != NULL )
    {
        tx_signal = (tx_signal_t)(GPOINTER_TO_INT(p_tx_signal));
        switch( tx_signal )
        {
            case TX_SIGNAL_START_STREAMING:
                // go to streaming state
                new_state = TX_THREAD_STREAMING;
                break;

            case TX_SIGNAL_RELEASE_CARD:
                new_state = TX_THREAD_DESTROYED;
                break;

            case TX_SIGNAL_STOP_STREAMING:
                // intentional fall-thru
            default:
                // error: unknown / invalid request report error and cleanup
                fprintf(stderr, "Error: received unexpected TX state request signal=%u\n", tx_signal );
                push_thread_state_update( TX_THREAD_INVALID, XCV_TYPE_TX );
                new_state = TX_THREAD_DESTROYED;
                break;
        }
    }
    return (new_state);
}

tx_thread_state_t process_tx_streaming_state( int sock_fd, uint8_t card )
{
    int32_t status=0;
    tx_thread_state_t new_state = TX_THREAD_STREAMING;
    gpointer p_tx_signal=NULL;
    tx_signal_t tx_signal;
    uint32_t num_bytes_to_send = 0;
    uint16_t block_size = 0;
    uint8_t *p_pkt = NULL;
    struct sockaddr_in client_addr;
    socklen_t sock_addr_len=sizeof(client_addr);
    int connected_tx_peer = -1;
    uint32_t offset=0;
    uint32_t num_bytes_remaining=0;
    skiq_daemon_tx_header_t *p_header;
    skiq_daemon_tx_resp_msg_t resp;

    if( (status=skiq_read_tx_block_size( card, skiq_tx_hdl_A1, &block_size )) != 0 )
    {
        fprintf(stderr, "Error: unable to read TX block size (status=%u), (card=%u)", status, card);
        new_state = TX_THREAD_DESTROYED;
    }
    else
    {
        // allocate memory for packet received on socket
        num_bytes_to_send = (block_size + SKIQ_TX_HEADER_SIZE_IN_WORDS)*sizeof(int32_t) + SKIQ_DAEMON_TX_HEADER_SIZE;
        p_pkt = malloc( num_bytes_to_send );
        num_bytes_remaining = num_bytes_to_send;
        p_header = (skiq_daemon_tx_header_t*)(p_pkt);
    }

    if( (p_pkt == NULL) && (new_state != TX_THREAD_DESTROYED) )
    {
        fprintf(stderr, "Error: unable to allocate memory for TX streaming (card=%u)\n", card);
        new_state = TX_THREAD_DESTROYED;
    }


    // if we haven't hit an error yet, start streaming
    if( new_state != TX_THREAD_DESTROYED )
    {
       /* we don't actually care about the handle here...the handle should already have
        been enabled in the FPGA by the client...we're just calling start here to get
        the data flowing at the transport layer */
        pthread_mutex_lock( &_tx_xport_mutex );
        status = skiq_xport_tx_start_streaming( card, skiq_tx_hdl_A1 );
        pthread_mutex_unlock( &_tx_xport_mutex );

        if( status == 0 )
        {
            pthread_mutex_lock( &_tx_thread_state_mutex );
            _tx_thread_state = TX_THREAD_STREAMING;
            pthread_mutex_unlock( &_tx_thread_state_mutex );
            push_thread_state_update( TX_THREAD_STREAMING, XCV_TYPE_TX );
        }
        else
        {
            fprintf(stderr, "Error: failed to start streaming with status %d\n", status);
            push_thread_state_update( TX_THREAD_INVALID, XCV_TYPE_TX );
            new_state = TX_THREAD_DESTROYED;
        }
    }

    if( new_state != TX_THREAD_DESTROYED )
    {
        errno = 0;
        // accept any incoming connections
        if( listen( sock_fd, 1 ) >= 0 )
        {
            errno = 0;
            connected_tx_peer = accept( sock_fd, (struct sockaddr*)(&client_addr), &sock_addr_len);
            if( connected_tx_peer >= 0 )
            {
                printf("Info: Peer connected on TX (card=%u)!\n", card);
                // close the listen socket
                close( sock_fd );
                sock_fd = -1;
            }
            else
            {
                fprintf(stderr, "Error: no connection TX stream (errno=%d)\n", errno);
                new_state = TX_THREAD_DESTROYED;
            }
        }
        else
        {
            fprintf(stderr, "Error: TX listen failed with errno %d\n", errno);
            new_state = TX_THREAD_DESTROYED;
        }
    }
    
    // keep streaming as long as we're running and no errors are encountered
    while( (running == true) && (new_state == TX_THREAD_STREAMING) )
    {
        errno = 0;
        status = read( connected_tx_peer, &(p_pkt[offset]), num_bytes_remaining );
        if( status > 0 )
        {
            num_bytes_remaining = num_bytes_remaining - status;
            offset = num_bytes_to_send - num_bytes_remaining;
        }

        if( (num_bytes_remaining==0) && (running == true) )
        {
            num_bytes_remaining = num_bytes_to_send;
            offset = 0;

            pthread_mutex_lock( &_tx_xport_mutex );
            status = skiq_xport_tx_transmit( card, skiq_tx_hdl_A1, (int32_t*)(&p_pkt[SKIQ_DAEMON_TX_HEADER_SIZE]), NULL );
            pthread_mutex_unlock( &_tx_xport_mutex );

            // send status back
            resp.status = htonl( status );
            resp.seq_number = p_header->seq_number;

            if( (status=send( connected_tx_peer, &resp, SKIQ_DAEMON_TX_RESP_SIZE, 0 )) != SKIQ_DAEMON_TX_RESP_SIZE )
            {
                fprintf(stderr, "Error: unable to send TX response with status %d\n", status);
            }
        }

        /* check for a signal to process */
        p_tx_signal = g_async_queue_try_pop( _p_tx_thread_msg_queue );
        if( p_tx_signal != NULL )
        {
            tx_signal = (tx_signal_t)(GPOINTER_TO_INT(p_tx_signal));
            switch( tx_signal )
            {
                case TX_SIGNAL_STOP_STREAMING:
                    // go to streaming state
                    new_state = TX_THREAD_STOPPED;
                    break;

                case TX_SIGNAL_RELEASE_CARD:
                    new_state = TX_THREAD_DESTROYED;
                    break;

                case TX_SIGNAL_START_STREAMING:
                    // intentional fall-thru
                default:
                    // error: unknown / invalid request report error and cleanup
                    fprintf(stderr, "Error: received unexpected TX state request signal=%u\n", tx_signal );
                    push_thread_state_update( TX_THREAD_INVALID, XCV_TYPE_TX );
                    new_state = TX_THREAD_DESTROYED;
                    break;
            }
        }
    }

    return (new_state);
}


///////////////////////////////////////////
// RX thread
void* process_rx_stream(void *data)
{
    card_socket_t *p_rx_sock = (card_socket_t*)(data);
    int sock_fd=0;
    uint8_t card=0;
    rx_thread_state_t trans_state=RX_THREAD_STOPPED;
    bool critical_error=false;
    bool cleanup=false;
    
    if( p_rx_sock != NULL )
    {
        sock_fd = p_rx_sock->sock;
        card = p_rx_sock->card;
        free(p_rx_sock);
        p_rx_sock = NULL;
    }
    else
    {
        fprintf(stderr, "Error: no socket/card information provided to RX stream thread\n");
        return NULL;
    }

    //////////////////////////////////////////////////////////////
    // we want to make sure that we get high priority for this thread, so setup the 
    // priority and lock it to a core
    struct sched_param sched;
    errno = 0;
    sched.sched_priority = sched_get_priority_min(SCHED_RR);
    if( sched.sched_priority < 0 )
    {
        printf("Warning: unable to get minimum scheduled priority, defaulting to 0 (errno=%d)\n", errno);
        sched.sched_priority = 0;
    }

    // set the scheduling to SCHED_RR...this ensures that this thread will
    // be run until it is blocked as long as a higher priority thread doesn't
    // need to run
    errno = 0;
    if( sched_setscheduler(0, SCHED_RR, &sched) != 0 )
    {
        printf("Warning: unable to set realtime priority for receive thread, falling back to other (errno=%d)\n", errno);
    }

    cpu_set_t  mask;
    CPU_ZERO(&mask);
    CPU_SET(NET_STREAMING_CPU, &mask);
    errno = 0;
    if( sched_setaffinity(0, sizeof(mask), &mask) != 0 )
    {
        printf("Warning: failed to set CPU affinity, possible reduced performance (errno=%d)\n", errno);
    }

    // increase our ref count
    g_async_queue_ref( _p_rx_thread_msg_queue );
    g_async_queue_ref( _p_rx_thread_state_update );
    //////////////////////////////////////////////////////////////

    // the thread has been created and setup, signal to anyone who cares that we're ready (in the stopped state)
    pthread_mutex_lock( &_rx_thread_state_mutex );
    _rx_thread_state = RX_THREAD_STOPPED;
    pthread_mutex_unlock( &_rx_thread_state_mutex );
    push_thread_state_update( RX_THREAD_STOPPED, XCV_TYPE_RX );

    while( (running == true) && (critical_error == false) && (cleanup==false) )
    {
        // process messages based on state
        switch( trans_state )
        {
            case RX_THREAD_STOPPED:
                trans_state = process_rx_stopped_state( card );
                break;

            case RX_THREAD_STREAMING:
                trans_state = process_rx_streaming_state( sock_fd, card );
                break;

            case RX_THREAD_DESTROYED:
                // we want to be destroyed and cleanup...set the cleanup flag
                cleanup = true;
                break;

            default:
                critical_error = true;
                fprintf(stderr, "Error: received unexpected state transition %u\n", trans_state);
                break;
        }
    }

    // our receive thread is done running, close the socket
    close( sock_fd );
    sock_fd = -1;

    pthread_mutex_lock( &_rx_thread_state_mutex );
    _rx_thread_state = RX_THREAD_DESTROYED;
    pthread_mutex_unlock( &_rx_thread_state_mutex );
    push_thread_state_update( RX_THREAD_DESTROYED, XCV_TYPE_RX );

    return NULL;
}
///////////////////////////////////////////

///////////////////////////////////////////
// TX thread
void* process_tx_stream(void *data)
{
    card_socket_t *p_tx_sock = (card_socket_t*)(data);
    int sock_fd=0;
    uint8_t card=0;
    bool critical_error=false;
    bool cleanup=false;
    tx_thread_state_t trans_state=TX_THREAD_STOPPED;
    
    if( p_tx_sock != NULL )
    {
        sock_fd = p_tx_sock->sock;
        card = p_tx_sock->card;
        free(p_tx_sock);
        p_tx_sock = NULL;
    }
    else
    {
        fprintf(stderr, "Error: no socket/card information provided to TX stream thread\n");
        return NULL;
    }

    while( (running == true) && (critical_error == false) && (cleanup==false) )
    {
        // process messages based on state
        switch( trans_state )
        {
            case TX_THREAD_STOPPED:
                trans_state = process_tx_stopped_state( card );
                break;

            case TX_THREAD_STREAMING:
                trans_state = process_tx_streaming_state( sock_fd, card );
                break;

            case TX_THREAD_INVALID:
                // intentional fall-through
            case TX_THREAD_DESTROYED:
                // we want to be destroyed and cleanup...set the cleanup flag
                cleanup = true;
                break;

            default:
                critical_error = true;
                fprintf(stderr, "Error: received unexpected state transition %u\n", trans_state);
                break;
        }
    }

    // our transmit thread is done running, close the socket
    close( sock_fd );
    sock_fd = -1;

    pthread_mutex_lock( &_tx_thread_state_mutex );
    _tx_thread_state = TX_THREAD_DESTROYED;
    pthread_mutex_unlock( &_tx_thread_state_mutex );
    push_thread_state_update( TX_THREAD_DESTROYED, XCV_TYPE_TX );

    return NULL;
}
///////////////////////////////////////////
