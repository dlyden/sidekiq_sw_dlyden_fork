/*! \file config_vers.c
 * \brief This file contains a basic application that configures
 * the hardware version and serial number of the Sidekiq.
 *  
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <sidekiq_hal.h>
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>

#include "hardware.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static char* app_name;

static void print_usage(void);

/*****************************************************************************/
/** This is the main function for configuring the version information.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status;
    skiq_hw_vers_t hardware_ver;
    skiq_product_t product;
    uint32_t serial_num;
    char* p_serial = NULL;
    uint8_t card;
    bool b_mpcie = true;
    bool b_serial_legacy=false;

    app_name = argv[0];

    if( argc == 3 )
    {
        if( strcasecmp(argv[1],"r") == 0 )
        {
            card = atoi(argv[2]);

            /* bring up just the usb interface */
            status = skiq_init(skiq_xport_type_usb, skiq_xport_init_level_basic, &card, 1);
            if( status != 0 )
            {
                printf("Error: unable to initialize libsidekiq with status %d\n", status);
                return (-1);
            }

            // serial #
            if( (status=skiq_read_serial_string(card, &p_serial)) != 0 )
            {
                printf("Error: failed to read serial number\n");
            }
            else
            {
                printf("Info: serial number = %s\n", p_serial);
            }
            // product
            if( (status=skiq_read_product_version(card, &product)) != 0 )
            {
                printf("Error: failed to read product version\n");
            }
            else
            {
                printf("Info: product ID = %s\n", skiq_product_vers_string(product));
            }
            // hardware rev
            if( (status=skiq_read_hw_version(card, &hardware_ver)) != 0 )
            {
                printf("Error: failed to read hardware version\n");
            }
            else
            {
                printf("Info: hardware version = %s\n", skiq_hardware_vers_string(hardware_ver));
            }
            skiq_exit();
        }
        else
        {
            printf("Error: invalid option\n");
            print_usage();
        }
    }
    else if( argc == 7 )
    {
        if( strcasecmp(argv[1],"w") == 0 )
        {
            card = atoi(argv[2]);

            /* bring up just the usb interface */
            status = skiq_fact_init_for_config(card);
            if( status != 0 )
            {
                printf("Error: unable to initialize libsidekiq with status %d\n", status);
                return (-1);
            }

            if( strcasecmp(argv[3], "mpcie") == 0 )
            {
                b_mpcie = true;
            }
            else if( strcasecmp(argv[3], "m2") == 0 )
            {
                b_mpcie = false;
            }
            else
            {
                printf("Error: invalid product specified\n");
                print_usage();
                _exit(-1);
            }

            if( strcasecmp(argv[4], "a") == 0 )
            {
                if( b_mpcie == false )
                {
                    printf("Error: m2 A no longer supported!\n");
                    status=-1;
                    goto exit_for_config;
                }
                else
                {
                    hardware_ver = skiq_hw_vers_mpcie_a;
                }
            }
            else if( strcasecmp(argv[4], "b") == 0 )
            {
                if( b_mpcie == false )
                {
                    hardware_ver = skiq_hw_vers_m2_b;
                }
                else
                {
                    hardware_ver = skiq_hw_vers_mpcie_b;
                }
            }
            else if( strcasecmp(argv[4], "c") == 0 )
            {
                if( b_mpcie == false )
                {
                    hardware_ver = skiq_hw_vers_m2_c;
                }
                else
                {
                    hardware_ver = skiq_hw_vers_mpcie_c;
                }
            }
            else if( strcasecmp(argv[4], "d") == 0 )
            {
                if( b_mpcie == false )
                {
                    printf("Error: m2 D is not supported!\n");
                    status=-1;
                    goto exit_for_config;
                }
                else
                {
                    hardware_ver = skiq_hw_vers_mpcie_d;
                }
            }
            else if( strcasecmp(argv[4], "e") == 0 )
            {
                if( b_mpcie == false )
                {
                    printf("Error: m2 E is not supported!\n");
                    status=-1;
                    goto exit_for_config;
                }
                else
                {
                    hardware_ver = skiq_hw_vers_mpcie_e;
                }
            }
             else
            {
                printf("Error: invalid board rev specified\n");
                print_usage();
                status=-2;
                goto exit_for_config;
            }

            if( strcasecmp(argv[5], "1") == 0 )
            {
                if( b_mpcie == false )
                {
                    product = skiq_product_m2_001;
                }
                else
                {
                    product = skiq_product_mpcie_001;
                }
            }
            else if( strcasecmp(argv[5], "2") == 0 )
            {
                if( b_mpcie == false )
                {
                    product = skiq_product_m2_002;
                }
                else
                {
                    product = skiq_product_mpcie_002;
                }
            }
            else
            {
                printf("Error: invalid product specified\n");
                print_usage();
                status=-3;
                goto exit_for_config;
            }
            
            p_serial = argv[6];
            if( SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN != 
                strnlen(p_serial, SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM) )
            {
                b_serial_legacy = true;
                serial_num = strtol(p_serial, NULL, 0);
                if( (serial_num > SKIQ_FACT_SERIAL_NUM_LEGACY_MAX) || 
                    (serial_num < SKIQ_FACT_SERIAL_NUM_LEGACY_MIN) )
                {
                    printf("Error: invalid legacy serial number\n");
                    print_usage();
                    status = -4;
                    goto exit_for_config;
                }
            }

            printf("legacy is %d serial is %s\n", b_serial_legacy, p_serial);
            if( (status=skiq_fact_write_serial_string(card, p_serial, b_serial_legacy)) != 0 )
            {
                printf("Error: failed to write serial number, status %d\n", status);
            }
            else
            {
                printf("Info: serial number successfully configured to %s\n", p_serial);
            }
            
            if( (status=skiq_fact_write_product_and_hw_vers( card, hardware_ver, product )) == 0 )
            {
                printf("Info: successfully wrote product and hardware version!\n");
            }
            else
            {
                printf("Error: unable to write product / hardware version\n");
                print_usage();
                status = -4;
            }
exit_for_config:            
            skiq_fact_exit_for_config(card);
            return (status);
        }
        else
        {
            printf("Error: invalid option\n");
            print_usage();
        }
    }
    else
    {
	printf("Error: invalid # args\n");
	print_usage();
    }
    return 0;
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <r|w> <card> <mpcie|m2> <hardware version [a|b|c|d|e]> <product version [1|2]> <serial #> \n", app_name);
    printf("   reads or writes the serial #, and hardware/product version of the specified Sidekiq\n");
}
