/**
    \file   test_fpga_soft_reset.c

    \brief  This application tests the FPGA soft reset feature

    <pre>
    Copyright 2020-2021 Epiq Solutions, All Rights Reserved
    </pre>
*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/time.h>
#include <string.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_xport.h"
#include "arg_parser.h"

/** @brief  The default uint32_t value to use if none is specified. */
#define DEFAULT_UINT32_VALUE    (0xDEADBEEF)

typedef struct
{
    /** @brief  If true, this entry is "in use" (has been updated) */
    bool inUse;

    /** @brief  The address of the register */
    uint16_t address;
    /** @brief  The value of the register */
    uint32_t value;

    /** @brief  Flag to indicate if this register can be read */
    bool canRead;
    /** @brief  Flag to indicate if this register can be written */
    bool canWrite;
    /**
        @brief  Flag to indicate if the register is considered an "extended" test register
    */
    bool extended;
} FpgaRegister;

/** @brief  Static initializer for the FpgaRegister struct */
#define FPGA_REGISTER_INITIALIZER   \
    {                               \
        .inUse = false,             \
        .address = 0,               \
        .value = 0,                 \
        .canRead = false,           \
        .canWrite = false,          \
        .extended = false,          \
    }

/** @brief  Helper initializer macro for initializing an FpgaRegister */
#define FPGA_REGISTER(address, read, write, extended) \
    { true, (address), 0, (read), (write), (extended) }
/** @brief  Special helper initializer macro for read/write FpgaRegisters */
#define FPGA_REGISTER_RW(address)           FPGA_REGISTER((address), true, true, false)
/** @brief  Special helper initializer macro for read-only FpgaRegisters */
#define FPGA_REGISTER_RO(address)           FPGA_REGISTER((address), true, false, false)
/** @brief  Special helper initializer macro for "extended" read/write FpgaRegisters */
#define FPGA_REGISTER_EXT_RW(address)       FPGA_REGISTER((address), true, true, true)
/** @brief  Special helper initializer macro for "extended" read-only FpgaRegisters */
#define FPGA_REGISTER_EXT_RO(address)       FPGA_REGISTER((address), true, false, true)

/**
    @brief  A list of all of the registers to test
*/
static const FpgaRegister testRegisters[] =
{
    /** @brief  Recommended test registers */
    FPGA_REGISTER_RW(FPGA_REG_1PPS_RESET_TIMESTAMP_LOW),
    FPGA_REGISTER_RW(FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH),
    FPGA_REGISTER_RW(FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_LOW),
    FPGA_REGISTER_RW(FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_HIGH),
    FPGA_REGISTER_RW(FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_B_LOW),
    FPGA_REGISTER_RW(FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_B_HIGH),
    FPGA_REGISTER_RW(FPGA_REG_TEST_WRITE),
    FPGA_REGISTER_RW(FPGA_REG_SET_ALL_TIMESTAMP_LOW),
    FPGA_REGISTER_RW(FPGA_REG_SET_ALL_TIMESTAMP_HIGH),
    FPGA_REGISTER_RW(FPGA_REG_TX_MEM_DATA),
    /** @brief  Experimental test registers */
    FPGA_REGISTER_RW(FPGA_REG_TX_RAM_WRITE),

    FPGA_REGISTER_EXT_RW(FPGA_REG_FPGA_CTRL),
    FPGA_REGISTER_EXT_RW(FPGA_REG_FMC_CTRL),
    FPGA_REGISTER_RO(FPGA_REG_JESD_STATUS),
    FPGA_REGISTER_EXT_RW(FPGA_REG_JESD_PRBS_CTRL),
};
/** @brief  The number of entries in ::testRegisters */
static const uint32_t testRegisterCount = sizeof(testRegisters) / sizeof(testRegisters[0]);

/** @brief  A list of the iterations to perform per-test. */
typedef enum
{
    /** @brief  Iteration where 0x55555555 is programmed into the registers */
    CurrentTestIteration_fives,
    /** @brief  Iteration where 0xAAAAAAAA (~0x55) is programmed into the registers */
    CurrentTestIteration_not_fives,
    /** @brief  Iteration where a random values is programmed into the registers */
    CurrentTestIteration_random,
    /** @brief  Iteration where a custom value is programmed into the registers */
    CurrentTestIteration_custom,
    /** @brief  Special marker to mark the last entry */
    CurrentTestIteration_end,
} CurrentTestIteration;


/* command line argument variables */
static uint8_t card = UINT8_MAX;
static bool verbose = false;
static bool customIterOnly = false;
static bool noExtended = false;
static bool regression = false;
static uint32_t randomSeed = 0;
static bool randomSeedPresent = false;
static uint32_t customValue = 0;
static bool customValuePresent = false;
static bool randomizeRegisters = false;

static const char *p_help_short = "- test the FPGA soft reset feature";
static const char *p_help_long = "\
Test that the FPGA soft reset feature can successfully reset the FPGA. The soft reset \
functionality is tested two ways - first, it is tested directly, by calling the soft \
reset function (shown as the 'factory soft reset' test). Secondly, libsidekiq should \
be calling the soft reset function when initializing a card, so the indirect method \
verifies that the registers are reset after re-initializing a card (shown as the \
'API enable / disable' test).";


static struct application_argument p_args[] =
{
    APP_ARG_REQ("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "If specified, show more information while running",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("custom-only",
                0,
                "If specified, only run the 'custom' iteration for each test (set with the"
                " '--custom-value' option)",
                NULL,
                &customIterOnly,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("no-extended",
                0,
                "If specified, 'extended' registers will not be tested",
                NULL,
                &noExtended,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("regression",
                0,
                "If specified, skip the second test if the soft-reset capability isn't available",
                NULL,
                &regression,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("randomize-registers",
                0,
                "If specified, randomize the order of the test registers (done at the beginning"
                " of each iteration)",
                NULL,
                &randomizeRegisters,
                BOOL_VAR_TYPE),
    APP_ARG_OPT_PRESENT("random-seed",
                0,
                "The seed to use for the random number generator (used by the 'random'"
                " iteration of each test)",
                "SEED",
                &randomSeed,
                UINT32_VAR_TYPE,
                &randomSeedPresent),
    APP_ARG_OPT_PRESENT("custom-value",
                0,
                "The register value to write & verify in the 'custom' iteration for each test",
                "VALUE",
                &customValue,
                UINT32_VAR_TYPE,
                &customValuePresent),
    APP_ARG_TERMINATOR,
};


/**
    @brief  Get the name of a test iteration

    @param[in]  iteration   The iteration to get the name of

    @return The name of the specified test iteration; if @p iteration isn't valid, then "OTHER"
            is returned
*/
static const char *
getTestIterationName(CurrentTestIteration iteration)
{
    switch(iteration)
    {
    case CurrentTestIteration_fives:
        return "0x55";
        break;
    case CurrentTestIteration_not_fives:
        return "0xAA";
        break;
    case CurrentTestIteration_random:
        return "RANDOM";
        break;
    case CurrentTestIteration_custom:
        return "CUSTOM";
        break;
    case CurrentTestIteration_end:
    default:
        break;
    }

    return "OTHER";
}

/**
    @brief  Enable a card

    @param[in]  card    The Sidekiq card of interest

    @return 0 on success, else an errno
*/
static int32_t
enableCard(uint8_t cardId)
{
    int32_t status = 0;
    pid_t owner = 0;

    status = skiq_enable_cards(&cardId, 1, skiq_xport_init_level_full);
    if (0 != status)
    {
        if (((EBUSY == status) || (-EBUSY == status)) &&
            (0 != skiq_is_card_avail(cardId, &owner)))
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID %u);"
                " cannot initialize card.\n", cardId, (unsigned int) owner);
        }
        else if (-EINVAL == status)
        {
            fprintf(stderr, "Error: unable to initialize card %" PRIu8 "; was a valid card"
                " specified? (status %" PRIi32 ")\n", cardId, status);
        }
        else
        {
            fprintf(stderr, "Error: failed to initialize card %" PRIu8 " (status = %" PRIi32
                ")\n", cardId, status);
        }
    }

    return (status);
}

/**
    @brief  Disable a card

    @param[in]  card    The Sidekiq card of interest

    @return 0 on success, else an errno
*/
static int32_t
disableCard(uint8_t cardId)
{
    int32_t status = 0;

    status = skiq_disable_cards(&cardId, 1);
    if (0 != status)
    {
        fprintf(stderr, "Warning: failed to disable card %" PRIu8 " (status = %" PRIi32
            "); possible resource leak)\n", cardId, status);
    }

    return (status);
}


/**
    @brief  A test that attempts to write to registers and verify that the soft reset changes them

    This test attempts to perform multiple write / reset passes using different values in an
    attempt to ensure that all bits are being reset.

    @param[in]  card                The Sidekiq card number
    @param[in]  useEnableDisable    Testing flag: if true, use the `skiq_enable_cards()` /
                                    `skiq_disable_cards()` functions to soft-reset the FPGA; if
                                    false, use the factory function `skiq_fact_soft_reset_fpga()`
                                    to soft-reset the FPGA.
    @param[in]  customOnly          If true, only run the 'custom' iteration
    @param[in]  randomRegisters     If true, randomize the list of registers before using them
    @param[in]  randomTestSeed      For the random test, the seed used to initialize the random
                                    number generator
    @param[in]  customTestValue     For the custom test, the custom value to write to the registers
    @param[in]  skipExtended        If true, don't run the "extended" tests

    @return 0 on success, else a negative errno value
*/
static int32_t
readWriteTest(uint8_t cardId, bool useEnableDisable, bool customOnly, bool randomRegisters,
    uint32_t randomTestSeed, uint32_t customTestValue, bool skipExtended)
{
#define MAX_REGISTER_COUNT      (1024)
    int32_t status = 0;
    bool skiqInitialized = false;
    bool cardsEnabled = false;
    uint32_t i = 0;
    uint32_t writeIdx = 0;
    uint32_t changedCount = 0;
    uint32_t sameCount = 0;
    int32_t tmpStatus = 0;
    FpgaRegister originalRegisters[MAX_REGISTER_COUNT] = \
        {[0 ... (MAX_REGISTER_COUNT-1)] = FPGA_REGISTER_INITIALIZER, };
    CurrentTestIteration currentIteration = CurrentTestIteration_fives;
    uint32_t registerIndexList[MAX_REGISTER_COUNT] = { UINT32_MAX };
    /* The number of changed registers needed to declare the test a success */
    uint32_t successThreshold = testRegisterCount;

    printf("=> Running '%s' test (using %s functions)\n", __FUNCTION__,
        (useEnableDisable) ? "API enable / disable" : "factory soft reset");

    status = skiq_init_without_cards();
    if (0 != status)
    {
        fprintf(stderr, "Error: failed to initialize libsidekiq (status = %" PRIi32 ")\n", status);
        goto finished;
    }
    skiqInitialized = true;

    status = enableCard(cardId);
    if (0 != status)
    {
        goto finished;
    }
    cardsEnabled = true;

    if (!useEnableDisable)
    {
        /*
            If using the factory APIs, attempt to do a soft-reset first to determine if the FPGA
            supports it.
        */
        status = skiq_fact_soft_reset_fpga(cardId);
        if (-ENOTSUP == status)
        {
            /*
                The soft-reset isn't supported, so skip over the "extended" register tests so
                the riskier FPGA registers aren't modified (written to)
            */
            noExtended = true;
            goto finished;
        }
    }

    /* Make a list of the valid register indices - this can be iterated over or randomized later */
    for (i = 0; i < testRegisterCount; i++)
    {
        registerIndexList[i] = i;
    }
    if (randomRegisters)
    {
        srand((unsigned int) randomTestSeed);
    }

    for (currentIteration = CurrentTestIteration_fives;
         (currentIteration != CurrentTestIteration_end) && (0 == status);
         currentIteration++)
    {
        const FpgaRegister *p_register = NULL;
        uint32_t value = 0;

        sameCount = 0;
        changedCount = 0;

        if ((customOnly) && (CurrentTestIteration_custom != currentIteration))
        {
            continue;
        }

        printf("= Running '%s' iteration...\n", getTestIterationName(currentIteration));

        /* Set up register numbers & values for this iteration */
        if (randomRegisters)
        {
            uint32_t a_idx = 0;

            /*
                Sattolo's algorithm (see https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle)
            */
            a_idx = testRegisterCount;
            while (a_idx > 0)
            {
                /* http://c-faq.com/lib/randrange.html */
                uint32_t b_idx = (uint32_t) (rand() / (RAND_MAX / ((testRegisterCount - 1) + 1) + 1));
                uint32_t tmp = registerIndexList[b_idx];

                a_idx--;
                registerIndexList[b_idx] = registerIndexList[a_idx];
                registerIndexList[a_idx] = tmp;
            }
        }

        switch(currentIteration)
        {
        case CurrentTestIteration_fives:
            value = 0x55555555;
            break;
        case CurrentTestIteration_not_fives:
            value = 0xAAAAAAAA;
            break;
        case CurrentTestIteration_random:
            srand((unsigned int) randomTestSeed);
            break;
        case CurrentTestIteration_custom:
            value = customTestValue;
            break;
        default:
            value = DEFAULT_UINT32_VALUE;
            break;
        }

        /* Write a bunch of registers and save off their values */
        i = 0;
        while (testRegisterCount > i)
        {
            p_register = &(testRegisters[registerIndexList[i]]);

            if ((skipExtended) && (p_register->extended))
            {
                i++;
                continue;
            }

            if (p_register->canWrite)
            {
                /*
                    Register is read / write; write a value to the register and expect that
                    it will change on reset
                */
                if (CurrentTestIteration_random == currentIteration)
                {
                    value = (uint32_t) (rand() & 0xFFFFFFFF);
                }

                if (verbose)
                {
                    printf("   Writing 0x%08" PRIX32 " to   register 0x%" PRIX16 "...\n",
                        value, p_register->address);
                }

                tmpStatus = sidekiq_fpga_reg_write_and_verify(cardId, p_register->address, value);
                if (0 != tmpStatus)
                {
                    fprintf(stderr, "Warning: failed to write register 0x%" PRIX16
                        " on card %" PRIu8 " (status = %" PRIi32 "); ignoring and attempting"
                        " to continue...\n", p_register->address, cardId, tmpStatus);
                }
                else
                {
                    originalRegisters[writeIdx].inUse = true;
                    originalRegisters[writeIdx].address = p_register->address;
                    originalRegisters[writeIdx].value = value;
                    writeIdx++;
                }
            }
            else
            {
                /*
                    Read only register; read in the current value and hopefully it changes
                    on reset
                */
                uint32_t tmpValue = 0;

                if (verbose)
                {
                    printf("   Reading            from register 0x%" PRIX16 " [R/O]...\n",
                        p_register->address);
                }

                tmpStatus = sidekiq_fpga_reg_read(cardId, p_register->address, &tmpValue);
                if (0 != tmpStatus)
                {
                    fprintf(stderr, "Warning: failed to read register 0x%" PRIX16
                        " on card %" PRIu8 " (status = %" PRIi32 "); ignoring and attempting"
                        " to continue...\n", p_register->address, cardId, tmpStatus);
                }
                else
                {
                    originalRegisters[writeIdx].inUse = true;
                    originalRegisters[writeIdx].address = p_register->address;
                    originalRegisters[writeIdx].value = tmpValue;
                    writeIdx++;
                }
            }

            i++;
        }

        /* Disable & re-enable the card in order to reset the registers */
        if (cardsEnabled)
        {
            if (useEnableDisable)
            {
                printf("== Disabling card...\n");
                tmpStatus = disableCard(cardId);
                if (0 != tmpStatus)
                {
                    goto finished;
                }
                cardsEnabled = false;

                printf("== Re-enabling card...\n");
                tmpStatus = enableCard(cardId);
                if (0 != tmpStatus)
                {
                    goto finished;
                }
                cardsEnabled = true;
            }
            else
            {
                printf("== Soft resetting FPGA...\n");
                tmpStatus = skiq_fact_soft_reset_fpga(cardId);
                if (0 != tmpStatus)
                {
                    fprintf(stderr, "Error: failed to reset soft-reset FPGA on card %" PRIu8 "using"
                        " the factory soft reset function (status = %" PRIi32 ")\n", cardId,
                        tmpStatus);
                    goto finished;
                }
            }
        }

        /* Loop through all of the registers and check if the registers match */
        i = 0;
        while (testRegisterCount > i)
        {
            if (originalRegisters[i].inUse)
            {
                tmpStatus = sidekiq_fpga_reg_read(cardId, originalRegisters[i].address, &value);
                if (0 != tmpStatus)
                {
                    fprintf(stderr, "Warning: failed to read register 0x%" PRIX16 " on card %"
                        PRIu8 " (status = %" PRIi32 "); not verifying...\n",
                        originalRegisters[i].address, cardId, tmpStatus);
                }
                else
                {
                    if (verbose)
                    {
                        printf("   Read    0x%08" PRIX32 " from register 0x%" PRIX16 "...\n",
                            value, originalRegisters[i].address);
                    }

                    if (value == originalRegisters[i].value)
                    {
                        if ((0 == value) && (0 == originalRegisters[i].value))
                        {
                            fprintf(stderr, "Info: register 0x%" PRIX16 " on card %" PRIu8
                                " did not change, but assuming success as both the original"
                                " value and reset value are 0\n", originalRegisters[i].address,
                                cardId);
                            /* Lower the success threshold so this isn't counted as a failure */
                            if (0 < successThreshold)
                            {
                                successThreshold--;
                            }
                            /*
                                Skip incrementing the "same" count so the user doesn't mistake
                                this for a failure
                            */
                        }
                        else
                        {
                            fprintf(stderr, "Info: register 0x%" PRIX16 " on card %" PRIu8
                                " did not change (original value 0x%08" PRIX32 ", read 0x%08" PRIX32
                                ")\n", originalRegisters[i].address, cardId,
                                originalRegisters[i].value, value);
                            sameCount++;
                        }
                    }
                    else
                    {
                        changedCount++;
                    }
                }
            }

            i++;
        }

        printf("=== %s %s test %s: %" PRIu32 " registers changed value after reset, %" PRIu32
            " stayed the same\n\n", __FUNCTION__, getTestIterationName(currentIteration),
            (changedCount >= successThreshold) ? "PASSED" : "FAILED", changedCount, sameCount);
        if (changedCount < successThreshold)
        {
            status = -EXDEV;
        }
    }

finished:
    if (cardsEnabled)
    {
        (void) disableCard(cardId);
        cardsEnabled = false;
    }

    if (skiqInitialized)
    {
        tmpStatus = skiq_exit();
        if (0 != tmpStatus)
        {
            fprintf(stderr, "Error: failed to exit libsidekiq (status = %" PRIi32 "); possible"
                " resource leak\n", status);
        }
        skiqInitialized = false;
    }

    printf("\n\n");

    return (status);
#undef MAX_REGISTER_COUNT
}


int main(int argc, char *argv[])
{
    int32_t tmp_status = 0;
    int32_t status = 0;
    int res = 0;
    bool runTest = true;

    if (0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args))
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (1);
    }

    if (!randomSeedPresent)
    {
        struct timeval now = {0, 0};

        res = gettimeofday(&now, NULL);
        if (0 != res)
        {
            randomSeed = DEFAULT_UINT32_VALUE;
        }
        else
        {
            randomSeed = (uint32_t) ((uint32_t) now.tv_sec ^ (uint32_t) now.tv_usec);
        }
    }

    if (!customValuePresent)
    {
        customValue = DEFAULT_UINT32_VALUE;
    }

    if (verbose)
    {
        printf("Info: Using random seed 0x%" PRIX32 " and custom value 0x%" PRIX32 "\n",
            randomSeed, customValue);
    }

    /*
        Run the tests.

        Now, this application is designed to test the "FPGA soft reset" functionality...
        so if the FPGA doesn't support the capability (which should become evident in
        the factory API test) then it would be reasonable to not run the second test.
        However, running the second test (the test that verifies that registers are reset
        during an enable / disable) allows the user to demonstrate that this functionality
        doesn't work if the "soft reset" feature isn't present - yeah, it sounds obvious,
        but it's another way to prove that the "soft reset" is working. This behavior
        can be bypassed with the `--regression` command-line option, which skips the
        enable / disable test if the capability isn't present - allowing the test app
        to work on older systems (read 'mPCIe') during regression.
    */
    status = readWriteTest(card, false, customIterOnly, randomizeRegisters, randomSeed,
                customValue, noExtended);
    if (-ENOTSUP == status)
    {
        printf("Info: FPGA on card %" PRIu8 " does not support soft-reset functionality;"
            " marking test as passed and skipping any further 'extended' register"
            " tests...\n", card);
        if (regression)
        {
            runTest = false;
        }
    }
    else if (0 != status)
    {
        fprintf(stderr, "Error: failed %s test [using factory function] (status = %"
            PRIi32 ")\n", "readWriteTest", status);
    }

    if (runTest)
    {
        tmp_status = readWriteTest(card, true, customIterOnly, randomizeRegisters, randomSeed,
                        customValue, noExtended);
    }
    else
    {
        printf("Info: Soft reset capability not present, skipping enable / disable test and"
            " marking as passed\n");
        status = 0;
    }
    if (0 != tmp_status)
    {
        fprintf(stderr, "Error: failed %s test [using enable / disable card] (status = %"
            PRIi32 ")\n", "readWriteTest", tmp_status);
        if (0 == status)
        {
            status = tmp_status;
        }
    }

    res = (int) status;
    if (0 > res)
    {
        res = 0 - res;
    }

    return (res);
}

