/**
 * @file   store_fmc_eeprom.c
 * @author  <info@epiq-solutions.com>
 * @date   Mon Nov 19 12:54:28 CST 2018
 *
 * @brief This application stores file contents to FMC EEPROM
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_api_factory.h"
#include "arg_parser.h"

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- store file contents into FMC EEPROM";
static const char* p_help_long =
    "This private test application showcases how to store file contents into a Sidekiq card's FMC EEPROM.";

/* Command Line Argument Variables */

/* file path to calibration files */
static char* p_file_path = NULL;
/* Sidekiq card index */
static uint8_t card = SKIQ_MAX_NUM_CARDS;
/* Sidekiq serial number */
static char* p_serial = NULL;
/* Allow libsidekiq log output */
static bool verbose = false;
/* Verify instead of writing */
static bool verify = false;

/* command line arguments available for use with this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("source",
                's',
                "Input file to store in FMC EEPROM",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("verify",
                0,
                "Verify FMC EEPROM contents against specified file instead of writing",
                NULL,
                &verify,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Enable logging from libsidekiq to stdout",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


/*****************************************************************************/
/** This is the main function for executing the application.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;
    FILE* pFile = NULL;
    pid_t owner = 0;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        // won't return from the above function
    }

    if( !verbose )
    {
        /* disable messages */
        skiq_register_logging(NULL);
    }

    pFile = fopen( p_file_path, "rb" );
    if ( pFile == NULL )
    {
        fprintf(stderr, "Error: unable to open file %s to read from\n", p_file_path);
        status = -1;
        goto close_files_and_exit;
    }

    if ( ( SKIQ_MAX_NUM_CARDS == card ) && ( NULL == p_serial ) )
    {
        fprintf(stderr, "Error: one of --card or --serial MUST be specified\n");
        status = -1;
        goto close_files_and_exit;
    }
    else if ( ( SKIQ_MAX_NUM_CARDS != card ) && ( NULL != p_serial ) )
    {
        fprintf(stderr, "Error: EITHER --card OR --serial must be specified,"
                " not both\n");
        status = -1;
        goto close_files_and_exit;
    }

    /* check for serial number */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string( p_serial, &card );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: unable to find Sidekiq with serial number"
                    " %s\n", p_serial);
            status = -1;
            goto close_files_and_exit;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto close_files_and_exit;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_basic, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int)owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        status = -1;
        goto close_files_and_exit;
    }

    if ( verify )
    {
        printf("Info: verifying file contents of FMC EEPROM\n");
        status = skiq_fact_verify_fmc_contents_from_file( card, pFile );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: failed to verify FMC EEPROM contents from %s to card %u,"
                    " status = %d\n", p_file_path, card, status);
        }
        else
        {
            printf("Info: success!\n");
        }
    }
    else
    {
        printf("Info: writing file contents to FMC EEPROM\n");
        status = skiq_fact_write_fmc_contents_from_file( card, pFile );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: failed to write FMC EEPROM contents from %s to card %u,"
                    " status = %d\n", p_file_path, card, status);
        }
        else
        {
            printf("Info: success!\n");
        }
    }

    skiq_exit();

close_files_and_exit:
    if ( pFile != NULL )
    {
        fclose(pFile);
        pFile = NULL;
    }

    return (int) status;
}

