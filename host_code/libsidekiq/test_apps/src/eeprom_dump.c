/**
 * @file   eeprom_dump.c
 * @author  <info@epiq-solutions.com>
 * @date   Thu Nov 15 15:47:56 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2013,2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_hal.h"
#include "arg_parser.h"

#define X2_EEPROM_NUM_BYTES             (32*1024) /* 256kbit = 32kB */
#define X2_FMC_EEPROM_NUM_BYTES_REVB3   (16*1024) /* 128kbit = 16kB */
#define X2_FMC_EEPROM_NUM_BYTES_REVB4   (256)     /*   2kbit = 256 bytes */
#define X2_FMC_EEPROM_NUM_BYTES         (256)     /*   2kbit = 256 bytes */
#define X4_EEPROM_NUM_BYTES             (64*1024) /* 256kbit = 64kB */
#define X4_FMC_EEPROM_NUM_BYTES_REVA    (32*1024) /* 256kbit = 32kB */
#define X4_FMC_EEPROM_NUM_BYTES         (256)     /*   2kbit = 256 bytes */
#define EEPROM_NUM_BYTES                (16*1024) /* 128kbit = 16kB */

#define BUF_SIZE (64)

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- dump EEPROM contents";
static const char* p_help_long =
"\
Dumps the EEPROM contents of a given Sidekiq, either the configuration or the FMC EEPROM, depending\n\
on the supplied arguments\n\
\n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n";

/* command line argument variables */
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static char* p_file_path = NULL;
static bool fmc_eeprom = false;
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("destination",
                'd',
                "Output file to store EEPROM contents",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("fmc",
                0,
                "Dump the FMC EEPROM contents",
                NULL,
                &fmc_eeprom,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}

/*****************************************************************************/
/** This is the main function for dumping the EEPROM contents.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    uint8_t buf[BUF_SIZE];
    uint16_t address = 0, line, i;
    FILE *pFile = NULL;
    int32_t status = 0;
    uint32_t eeprom_num_bytes;
    pid_t owner = 0;

    bool skiq_initialized = false;

    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        status = -1;
        goto finished;
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        status = -1;
        goto finished;
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", p_serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    /* initialize Sidekiq */
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
        }
        status = -1;
        goto finished;
    }
    skiq_initialized = true;

    /* translate a '-' to mean /dev/stdout */
    if ( 0 == strcasecmp( p_file_path, "-" ) )
    {
        p_file_path = "/dev/stdout";
    }

    pFile = fopen(p_file_path, "w+");
    if( pFile == NULL )
    {
        fprintf(stderr, "Error: failed to open file %s\n", p_file_path);
        status = -1;
        goto finished;
    }

    {
        skiq_param_t params;
        status = skiq_read_parameters( card, &params );
        if ( status == 0 )
        {
            switch (params.card_param.part_type)
            {
                case skiq_x2:
                    if ( fmc_eeprom )
                    {
                        if ( params.card_param.part_info.revision_string[0] == 'B' )
                        {
                            if ( params.card_param.part_info.revision_string[1] < '4' )
                            {
                                eeprom_num_bytes = X2_FMC_EEPROM_NUM_BYTES_REVB3;
                            }
                            else
                            {
                                eeprom_num_bytes = X2_FMC_EEPROM_NUM_BYTES_REVB4;
                            }
                        }
                        else
                        {
                            eeprom_num_bytes = X2_FMC_EEPROM_NUM_BYTES;
                        }
                    }
                    else
                    {
                        eeprom_num_bytes = X2_EEPROM_NUM_BYTES;
                    }
                    break;
                case skiq_x4:
                    if ( fmc_eeprom )
                    {
                        if ( params.card_param.part_info.revision_string[0] == 'A' )
                        {
                            eeprom_num_bytes = X4_FMC_EEPROM_NUM_BYTES_REVA;
                        }
                        else
                        {
                            eeprom_num_bytes = X4_FMC_EEPROM_NUM_BYTES;
                        }
                    }
                    else
                    {
                        eeprom_num_bytes = X4_EEPROM_NUM_BYTES;
                    }
                    break;
                default:
                    if ( fmc_eeprom )
                    {
                        fprintf(stderr, "Error: No FMC EEPROM on Sidekiq card %u\n", card);
                        status = -1;
                        goto finished;
                    }
                    else
                    {
                        eeprom_num_bytes = EEPROM_NUM_BYTES;
                    }
                    break;
            }
        }
        else
        {
            fprintf(stderr, "Error: unable to read skiq_part_t from card %u\n", card);
            eeprom_num_bytes = 0;
        }
    }

    // read the EEPROM contents
    for ( address = 0; (address < eeprom_num_bytes) && running && (status == 0); address+=BUF_SIZE )
    {
        uint16_t nr_bytes = MIN(eeprom_num_bytes - address, BUF_SIZE);

        if ( fmc_eeprom )
        {
            status = hal_read_fmc_eeprom( card, address, buf, nr_bytes );
        }
        else
        {
            status = hal_read_eeprom( card, address, buf, nr_bytes );
        }

        if ( status == 0 )
        {
            size_t n;

            for (line = 0; line < (BUF_SIZE / 16); line++)
            {
                printf("[card %u] 0x%06x:", card, address + line * 16);
                for (i = 0; i < 16; i++)
                {
                    if ((i % 2) == 0) printf(" ");
                    if ((i % 8) == 0) printf(" ");
                    printf("%02x", buf[line * 16 + i]);
                }
                printf("  ");
                for (i = 0; i < 16; i++)
                {
                    if ((i % 8) == 0) printf(" ");
                    if (isprint(buf[line * 16 + i]) == 0)
                        printf(".");
                    else
                        printf("%c", buf[line * 16 + i]);
                }
                printf("\n");
            }

            n = fwrite(buf, 1, nr_bytes, pFile);
            if ( n != nr_bytes )
            {
                status = -EIO;
                fprintf(stderr, "Error: got error writing to file, errno = %d, status = %d\n",
                        errno, status);
            }
        }
        else
        {
            fprintf(stderr, "Error: got error reading from EEPROM, status = %d\n", status);
        }
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    if ( pFile != NULL )
    {
        fclose(pFile);
        pFile = NULL;
    }

    return ((int) status);
}
