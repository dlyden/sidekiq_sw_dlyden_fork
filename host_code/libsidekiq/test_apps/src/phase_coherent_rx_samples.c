/*! \file phase_coherent_rx_samples.c
 * \brief This file contains an application for configuring Rx phase coherent
 * pair to a parameters, and receives data from both channels and generates
 * output files that are phase algined across channels.
 *  
 * <pre>
 * Copyright 2016-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <inttypes.h>

#include <sidekiq_api.h>
#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#define NUM_PAYLOAD_WORDS_IN_BLOCK (SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS-SKIQ_RX_HEADER_SIZE_IN_WORDS)

#define MAX_TEST_HDL_INDEX (skiq_rx_hdl_A2+1)

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

static const char* p_file_suffix[MAX_TEST_HDL_INDEX] = { ".a1", ".a2" };

/* these are used to provide help strings for the application when running it 
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- record phase coherent Rx data";

static const char* p_help_long =
"\
Tune to the user-specifed Rx frequency and acquire the specified number of\n\
words at the requested sample rate. Two output files are generated, one from\n\
each receive channel.  The sample data is phase aligned.\n\
\n\
The data is stored in the file as 16-bit I/Q pairs with 'I' samples\n\
stored in the the upper 16-bits of each word, and 'Q' samples stored\n\
in the lower 16-bits of each word, resulting in the following format:\n\
\n\
             -31-------------------------------------------------------0-\n\
             |         12-bit I0_A1        |       12-bit Q0_A1         |\n\
    word 0   | (sign extended to 16 bits   | (sign extended to 16 bits) |\n\
             ------------------------------------------------------------\n\
             |         12-bit I1_A1        |       12-bit Q1_A1         |\n\
    word 1   | (sign extended to 16 bits   | (sign extended to 16 bits) |\n\
             ------------------------------------------------------------\n\
             |         12-bit I1_A1        |       12-bit Q1_A1         |\n\
    word 2   | (sign extended to 16 bits   | (sign extended to 16 bits) |\n\
             ------------------------------------------------------------\n\
             |           ...               |          ...               |\n\
             -31-------------------------------------------------------0-\n\
\n\
Each sample is little-endian, twos-complement, signed, and sign-extended\n\
from 12 to 16-bits. \n\
Defaults:\n\
  --card=" xstr(DEFAULT_CARD_NUMBER) "\n\
  --frequency=850000000\n\
  --mode=PCIE\n\
  --rate=1000000\n\
  --words=100000\
";

static uint32_t tot_num_words = 100000;
static uint64_t lo_freq = 850000000;
static uint32_t sample_rate = 1000000;
static uint32_t bandwidth = UINT32_MAX;
static uint16_t warp_voltage = UINT16_MAX;
static uint32_t rx_gain = 50;
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static char* p_mode = "PCIE";
static char* p_file_path = NULL;
static uint8_t source=0;
static uint8_t rxquadcal=1;
static uint8_t dcoffset=1;

/* variable used to signal force quit of application */
static bool running = true;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("bandwidth",
                'b',
                "Bandwidth in hertz",
                "Hz",
                &bandwidth,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("card",
                'c',
                "Use specified Sidekiq card",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("destination",
                'd',
                "Output file to store Rx data",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_OPT("frequency",
                'f',
                "Frequency to receive samples at in Hertz",
                "Hz",
                &lo_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("gain",
                'g',
                "Manually configure the gain by index rather than using automatic",
                "index",
                &rx_gain,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("warp",
                0,
                "Configure the TCVCXO warp voltage",
                "DAC",
                &warp_voltage,
                UINT16_VAR_TYPE),
    APP_ARG_OPT("mode",
                'm',
                "Transport mode to use, either USB or PCIE",
                "MODE",
                &p_mode,
                STRING_VAR_TYPE),
    APP_ARG_OPT("rate",
                'r',
                "Sample rate in Hertz",
                "Hz",
                &sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("words",
                'w',
                "Number of sample words to acquire",
                "N",
                &tot_num_words,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("source",
                's',
                "Source of data (0: RF, 1: Tone, 2: PRBS)",
                "[0,1,2]",
                &source,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("dcoffset",
                'o',
                "DC Offset Tracking (0: off, 1: on)",
                "[0,1]",
                &dcoffset,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("rxquadcal",
                'q',
                "Rx Quad Cal Tracking (0: off, 1: on)",
                "[0,1]",
                &rxquadcal,
                UINT8_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

/* local functions */
static void app_cleanup(int signum);
static int32_t configure_rf_params(void);

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}

/*****************************************************************************/
/** This is the main function for executing the rx_samples app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    int32_t status=0;
    skiq_xport_type_t type;
    skiq_xport_init_level_t level = skiq_xport_init_level_full;
    uint8_t num_active_chans=2;
    pid_t owner = 0;
    bool skiq_initialized = false;

    // used to receive a packet of data
    skiq_rx_status_t rx_status;
    skiq_rx_hdl_t curr_hdl;
    skiq_rx_block_t* p_rx_block;
    uint32_t len;

    bool first_block[MAX_TEST_HDL_INDEX];
    uint64_t curr_ts[MAX_TEST_HDL_INDEX];
    uint64_t next_ts[MAX_TEST_HDL_INDEX];

    /* keep track of the specified handles to start streaming */
    skiq_rx_hdl_t handles[MAX_TEST_HDL_INDEX];
    uint8_t nr_handles = 0;

    // used in saving the results
    FILE* output_fp[MAX_TEST_HDL_INDEX];
    uint32_t num_words_remain[MAX_TEST_HDL_INDEX];
    char p_filename[100];
    uint32_t filename_len=0;
    uint32_t *p_samples[MAX_TEST_HDL_INDEX];
    uint32_t *p_start_samples[MAX_TEST_HDL_INDEX];

    // RxA1 tmp buffer and timestamp (until we start receiving data from A2)
    uint32_t tmp_samples[NUM_PAYLOAD_WORDS_IN_BLOCK];
    uint64_t tmp_ts=0;
    bool synced = false;

    /* always install a handler for proper cleanup */
    signal(SIGINT, app_cleanup);

    for( curr_hdl=skiq_rx_hdl_A1; curr_hdl<MAX_TEST_HDL_INDEX; curr_hdl++ )
    {
        output_fp[curr_hdl] = NULL;
        p_samples[curr_hdl] = NULL;
    }

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    /* map argument values to sidekiq specific variable values */
    if( 0 == strncasecmp(p_mode, "PCIE", 5) )
    {
        type = skiq_xport_type_pcie;
        printf("Info: using pcie interface\n");
    }
    else if( 0 == strncasecmp(p_mode, "USB", 4) )
    {
        type = skiq_xport_type_usb;
        printf("Info: using usb interface\n");
    }
    else
    {
        printf("Error: invalid mode specified\n");
        return (-1);
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return (-1);
    }
    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    /* If specified, attempt to find the card with a matching serial number. */
    if (NULL != p_serial)
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (0 != status)
        {
            printf("Error: cannot find card with serial number %s (result"
                    " code %" PRIi32 ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    // initialize some book keeping
    for( curr_hdl=skiq_rx_hdl_A1; curr_hdl<MAX_TEST_HDL_INDEX; curr_hdl++ )
    {
        first_block[curr_hdl] = true;
        num_words_remain[curr_hdl] = tot_num_words;
        curr_ts[curr_hdl] = 0;
        next_ts[curr_hdl] = 0;

        p_filename[0] = '\0';
        if( strlen(p_file_path) > 90 )
        {
            printf("Error: filename is too long\n");
            exit(-1);
        }
        strncpy( p_filename, p_file_path, 100 );
        filename_len = strlen(p_filename);
        strncpy( &(p_filename[filename_len]),
                 p_file_suffix[curr_hdl],
                 100-filename_len );
        p_filename[99] = '\0';
        output_fp[curr_hdl]=fopen(p_filename,"wb");
        if (output_fp[curr_hdl] == NULL)
        {
            printf("Error: unable to open output file %s\n",p_filename);
            exit(-1);
        }
        printf("Info: opened file %s for output\n",p_filename);
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    // initialize the Sidekiq card
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            printf("Error: card %" PRIu8 " is already in use (by process ID"
                    " %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            printf("Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            printf("Error: unable to initialize libsidekiq with status %d\n",
                    status);
        }
        return (-1);
    }
    skiq_initialized = true;

    // make sure the card is capable of dual channel receive
    status = skiq_write_chan_mode( card, skiq_chan_mode_dual );
    if( status != 0 )
    {
        printf("Error: unable to configure dual channel mode with status %d\n", status);
        status = -2;
        goto finished;
    }
    printf("Info: configured dual channel mode\n");

    // set the RF parameters
    status = configure_rf_params();
    if( status != 0 )
    {
        printf("Error: failed to configure RF parameters, exiting\n");
        status = -1;
        goto finished;
    }

    // setup the data source
    if( source == 0 )
    {
        skiq_write_rfic_reg(card, 0x3F4, 0x00);
        skiq_write_rfic_reg(card, 0x03D, 0x00);
        skiq_write_rfic_reg(card, 0x03E, 0x00);
    }
    else
    {
        if( source == 1 )
        {
            // setup for a tone
            skiq_write_rfic_reg(card, 0x3F4, 0x0B);
        }
        else
        {
            // setup PRBS
            skiq_write_rfic_reg(card, 0x3F4, 0x09);
        }
        skiq_write_rfic_reg(card, 0x03D, 0xFF);
        skiq_write_rfic_reg(card, 0x03E, 0x0F);
    }

    // allocate memory to save the samples
    for( curr_hdl=skiq_rx_hdl_A1; curr_hdl<MAX_TEST_HDL_INDEX; curr_hdl++ )
    {
        p_samples[curr_hdl] = malloc(tot_num_words*sizeof(uint32_t));
        if( p_samples[curr_hdl] == NULL )
        {
            printf("Error: unable to allocate memory to store received samples\n");
            status = -1;
            goto finished;
        }
        p_start_samples[curr_hdl] = p_samples[curr_hdl];
    }

    // start streaming
    for( curr_hdl=skiq_rx_hdl_A1; curr_hdl<MAX_TEST_HDL_INDEX; curr_hdl++ )
    {
        handles[nr_handles++] = curr_hdl;
    }

    printf("Info: starting %u Rx interface(s) on card %u\n", nr_handles, card);
    status = skiq_start_rx_streaming_multi_immediate(card, handles, nr_handles);
    if ( status != 0 )
    {
        fprintf(stderr, "Error: starting %u Rx interface(s) on card %u failed with status %d\n", nr_handles, card, status);
        goto finished;
    }

    while ( (num_active_chans > 0) && (running==true) )
    {
        rx_status = skiq_receive(card,&curr_hdl, &p_rx_block, &len);
        if ( skiq_rx_status_success == rx_status )
        {
            if ( NULL != p_rx_block )
            {
                /* verify timestamp and data */
                curr_ts[curr_hdl] = p_rx_block->rf_timestamp;

                if (first_block[curr_hdl] == true)
                {
                    first_block[curr_hdl] = false;
                    /* will be incremented properly below for next time through */
                    next_ts[curr_hdl] = curr_ts[curr_hdl];
                    printf("Got first block for hdl %u, ts 0x%016" PRIx64 "\n",
                           curr_hdl, curr_ts[curr_hdl]);
                    // Note: we're assuming that we'll get data from a1 first, and the first
                    // time we get data from a2 is when we need to align our samples.  Based
                    // on how we're starting the stream and how the FPGA is structured,
                    // that should be a safe assumption
                    if( curr_hdl == skiq_rx_hdl_A2 )
                    {
                        uint32_t offset = curr_ts[curr_hdl] - tmp_ts;
                        uint32_t words_to_copy = NUM_PAYLOAD_WORDS_IN_BLOCK-offset;
                        // copy the intermediate data into a1 samples
                        memcpy( p_samples[skiq_rx_hdl_A1],
                                (uint32_t*)(tmp_samples)+offset,
                                words_to_copy*sizeof(uint32_t) );
                        p_samples[skiq_rx_hdl_A1] += words_to_copy;
                        num_words_remain[skiq_rx_hdl_A1] = num_words_remain[skiq_rx_hdl_A1] - words_to_copy;
                        synced = true;
                    }
                }
                else if (curr_ts[curr_hdl] != next_ts[curr_hdl])
                {
                    printf("Error: timestamp error for %u...expected 0x%016" PRIx64 " but got 0x%016" PRIx64 " num words left %u\n",
                           curr_hdl, next_ts[curr_hdl], curr_ts[curr_hdl], num_words_remain[curr_hdl]);
                    status = -1;
                    goto finished;
                }
                next_ts[curr_hdl] += NUM_PAYLOAD_WORDS_IN_BLOCK;
                if( synced == true )
                {
                    if( num_words_remain[curr_hdl] > 0 )
                    {
                        // update the remain word count
                        if( num_words_remain[curr_hdl] >= NUM_PAYLOAD_WORDS_IN_BLOCK )
                        {
                            // copy the entire block
                            memcpy( p_samples[curr_hdl],
                                    (void *)p_rx_block->data,
                                    NUM_PAYLOAD_WORDS_IN_BLOCK*sizeof(uint32_t) );
                            p_samples[curr_hdl] += NUM_PAYLOAD_WORDS_IN_BLOCK;
                            num_words_remain[curr_hdl] = num_words_remain[curr_hdl] - NUM_PAYLOAD_WORDS_IN_BLOCK;
                        }
                        else
                        {
                            printf("copy last block %u for hdl %u\n", num_words_remain[curr_hdl], curr_hdl);
                            memcpy( p_samples[curr_hdl],
                                    (void *)p_rx_block->data,
                                    num_words_remain[curr_hdl]*sizeof(uint32_t) );
                            // last block
                            num_words_remain[curr_hdl] = 0;
                            skiq_stop_rx_streaming(card, curr_hdl);
                            num_active_chans--;
                            printf("Last block for hdl %u, num active %u\n", curr_hdl, num_active_chans);
                        }
                    }
                }
                else
                {
                    // temporarily save off the timestamp and sample data
                    tmp_ts = curr_ts[curr_hdl];
                    memcpy( tmp_samples,
                            (void *)p_rx_block->data,
                            NUM_PAYLOAD_WORDS_IN_BLOCK*sizeof(uint32_t) );
                }
            } // null buf
        } // rx status ok
    } // receive loop

    // write the results out to the files
    /********************* write output file and clean up **********************/
    /* all file data has been verified, write off to our output file */
    for( curr_hdl=skiq_rx_hdl_A1; (curr_hdl<MAX_TEST_HDL_INDEX) && (running==true); curr_hdl++ )
    {
        printf("Info: done receiving, start write to file\n");
        if( fwrite(p_start_samples[curr_hdl],
                               sizeof(uint32_t),
                               tot_num_words,
                   output_fp[curr_hdl]) != tot_num_words )
        {
            printf("Info: attempt to write %d words to output file failed\n",
                   tot_num_words);
        }
        else
        {
            printf("Info: successful write of %u words\n", tot_num_words);
        }
        fclose(output_fp[curr_hdl]);
        free(p_start_samples[curr_hdl]);
    }
    printf("Info: Done without errors!\n");
    status = 0;

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    for( curr_hdl=skiq_rx_hdl_A1; curr_hdl<MAX_TEST_HDL_INDEX; curr_hdl++ )
    {
        if (output_fp[curr_hdl] != NULL)
        {
            fclose(output_fp[curr_hdl]);
            output_fp[curr_hdl] = NULL;
        }

        if (p_samples[curr_hdl] != NULL)
        {
            free(p_samples[curr_hdl]);
            p_samples[curr_hdl] = NULL;
        }
    }

    return ((int) status);
}

static int32_t configure_rf_params(void)
{
    int32_t status=0;
    skiq_rx_hdl_t curr_hdl;
    uint8_t reg=0;

    /* initialize the warp voltage here to allow time for it to settle */
    if ( warp_voltage != UINT16_MAX )
    {
        if( (status=skiq_write_tcvcxo_warp_voltage(card,warp_voltage)) != 0 )
        {
            printf("Error: unable to set the warp voltage, using previous value\n");
        }
        printf("Info: tcvcxo warp voltage configured to %u\n", warp_voltage);
    }
    else
    {
        printf("Info: tcvcxo warp voltage left at factory setting\n");
    }

    for( curr_hdl=skiq_rx_hdl_A1; curr_hdl<MAX_TEST_HDL_INDEX; curr_hdl++ )
    {
        if( (status=skiq_write_rx_gain(card, curr_hdl, rx_gain)) != 0 )
        {
            printf("Error: failed to set Rx gain\n");
            goto EXIT_CONFIG_PARAMS;
        }
        if( (status=skiq_write_rx_sample_rate_and_bandwidth(card,
                                                            curr_hdl,
                                                            sample_rate,
                                                            bandwidth)) != 0 )
        {
            printf("Error: failed to set Rx sample rate or bandwidth\n");
            goto EXIT_CONFIG_PARAMS;
        }
        /* tune the Rx chain to the requested freq */
        if( (status=skiq_write_rx_LO_freq(card, curr_hdl, lo_freq)) != 0 )
        {
            printf("Error: failed to set LO freq\n");
            goto EXIT_CONFIG_PARAMS;
        }
        // we want to make sure our DC offset removal in the FPGA is disabled for phase coherent testing
        skiq_write_rx_dc_offset_corr(card, curr_hdl, false);
    }

    // TODO: add API calls to disable DC offset tracking and QEC tracking

    // disable DC offset tracking in the RF IC
    skiq_read_rfic_reg(card, 0x18B, &reg);
    if( dcoffset == 0 )
    {
        printf("dc off\n");
        // clear bits 5 (BB DC) and 3 (RF DC) of reg 0x18B
        reg &= 0xD7;
        skiq_write_rfic_reg(card, 0x18B, reg);
    }
    else
    {
        printf("dc on\n");
        // enable DC offset tracking
        reg |= 0x28;
        skiq_write_rfic_reg(card, 0x18B, reg);
    }

    // rx quad tracking
    skiq_read_rfic_reg(card, 0x169, &reg);
    // disable Rx Quadrature tracking in the RF IC
    if( rxquadcal == 0 )
    {
        printf("qec off\n");
        // clear bits 1 (Rx2) and 0 (Rx1) of cal config reg
        reg &= 0xFC;
        skiq_write_rfic_reg(card, 0x169, reg);
    }
    else
    {
        printf("qec on\n");
        reg |= 0x03;
        skiq_write_rfic_reg(card, 0x169, reg);
    }

EXIT_CONFIG_PARAMS:
    return (status);
}
