/**
 * @file   multi.c
 * @author  <info@epiq-solutions.com>
 * @date   Thu Nov 15 13:36:59 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <stdio.h>
#include <string.h>
#include <libgen.h>


/***** DEFINES *****/

#define MULTICALL                       "skiq_multi_call"

#define _app_with_underscores(_app)     (#_app "__")

#define APP(_app)                                                       \
    do {                                                                \
        extern int main_##_app(int argc, char *argv[]);                 \
        applets[nr_applets].name = #_app;                               \
        applets[nr_applets++].fn = main_##_app;                         \
        if (( 0 == strcmp(s, #_app)) ||                                 \
            ( 0 == strncmp(s, _app_with_underscores(_app),              \
                              strlen(_app_with_underscores(_app)))))    \
        {                                                               \
            return main_##_app(argc, argv);                             \
        }                                                               \
                                                                        \
    } while (0)


/***** TYPEDEFS *****/

typedef int (*applet_fn_t)(int argc, char *argv[]);


/***** STRUCTS *****/

struct applet
{
    char *name;
    applet_fn_t fn;
};


/***** LOCAL VARIABLES *****/

static struct applet applets[256];


/***** LOCAL FUNCTIONS *****/

void show_help( int nr_applets )
{
    int i, col = 0;

    printf("Sidekiq multi-call binary\n\n");
    printf("Usage: " MULTICALL " [function [arguments]...]\n");
    printf("   or: " MULTICALL " --list\n");
    printf("   or: function [arguments]...\n\n");
    printf("\tSidekiq's '" MULTICALL "' is a multi-call binary that combines several "
           "Sidekiq test\n\tapplications into a single executable.\n\n");

    printf("Currently defined functions:\n");
    col += printf("\t%s", applets[0].name);
    for ( i = 1; i < nr_applets; i++ )
    {
        if ( col > 80 )
        {
            col = printf("\n\t%s", applets[i].name);
        }
        else
        {
            col += printf(", %s", applets[i].name);
        }
    }
    printf("\n");
}


/***** GLOBAL FUNCTIONS *****/

int main(int argc, char *argv[])
{
    int i, status = 0, nr_applets = 0;
    char *s = basename(argv[0]);

    APP(fdd_rx_tx_samples);
    APP(prog_fpga);
    APP(read_accel);
    APP(read_imu);
    APP(read_temp);
    APP(rx_benchmark);
    APP(rx_samples);
    APP(sidekiq_probe);
    APP(sweep_receive);
    APP(tdd_rx_tx_samples);
    APP(tx_benchmark);
    APP(tx_configure);
    APP(tx_samples);
    APP(user_reg_test);
    APP(version_test);
    APP(xcv_benchmark);

    if ( 0 == strcmp(s, MULTICALL) )
    {
        if ( argc > 1 )
        {
            if ( 0 == strcmp( argv[1], "--list" ) )
            {
                /* list each applet, one line each */
                for (i = 0; i < nr_applets; i++)
                {
                    printf("%s\n", applets[i].name);
                }
            }
            else if ( ( 0 == strcmp( argv[1], "--help" ) ) ||
                      ( 0 == strcmp( argv[1], "-h" ) ) )
            {
                show_help( nr_applets );
            }
            else
            {
                /* search the list of registered applets to see if argv[1] matches one */
                for (i = 0; i < nr_applets; i++)
                {
                    if ( 0 == strcmp( argv[1], applets[i].name ) )
                    {
                        return applets[i].fn(argc - 1, &(argv[1]));
                    }
                }

                printf("%s: applet not found\n", argv[1]);
                status = 1;
            }
        }
        else
        {
            show_help( nr_applets );
        }
    }
    else
    {
        printf("%s: applet not found\n", argv[0]);
        status = 1;
    }

    return status;
}
