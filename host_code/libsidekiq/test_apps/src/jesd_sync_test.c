/*! \file jesd_sync_test.c
 * \brief This file contains a basic application for sweeping
 * the preEmphasis and amplitude settings and detecting the sync quality.
 *  
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>

#include <sidekiq_api.h>
#include <sidekiq_xport.h>
#include <sidekiq_fpga_reg_defs.h>
#include <fpga_jesd_ctrl.h>

#define NUM_USEC_IN_MS (1000)

static void critical_handler( int32_t status, void* p_user_data );

static void print_usage(const char *progName);

// defined in rfic_x2.c
extern int32_t reset_jesd_x2( uint8_t card,
                              uint8_t rx_preEmphasis,       // 0..7, 4 is default
                              uint8_t rx_serialAmplitude,   // 18..26, 22 is default
                              uint8_t tx_diffctrl,          // 0..16
                              uint8_t tx_precursor,         // 0..20
                              jesd_sync_result_t *p_sync_result );       
// defined in rfic_x4.c
extern int32_t reset_jesd_x4( uint8_t card,
                              uint8_t rx_preEmphasis,       // 0..4, 1 is default
                              uint8_t rx_serialAmplitude,   // 0..15, 15 is default
                              uint8_t tx_diffctrl,          // 0..16
                              uint8_t tx_precursor,         // 0..20
                              jesd_sync_result_t *p_sync_result );       


void critical_handler( int32_t status, void* p_user_data )
{
    printf("RECEIVED CRITICAL ERROR (status=%d), CONTINUING\n", status);
}

int main( int argc, char *argv[] )
{
    uint8_t card=0;
    uint8_t start_pre=0;
    uint8_t end_pre=0;
    uint8_t start_amp=0;
    uint8_t end_amp=0;
    uint8_t start_diffctrl=0;
    uint8_t end_diffctrl=0;
    uint8_t start_precursor=0;
    uint8_t end_precursor=0;
    uint32_t i=0;
    uint32_t j=0;
    uint32_t k=0;
    uint32_t l=0;
    uint8_t delay_ms=0;
    jesd_sync_result_t result;
    uint32_t num_syncs=0;
    FILE *p_results_file=NULL;
    int32_t tot_sync_err = 0;
    skiq_param_t param;
    uint32_t sample_rate=0;
    uint32_t tot_num_tests=0;
    uint32_t repeat=1;
    uint32_t r=0;
    uint8_t synced=0;
    int32_t (*reset_jesd)(uint8_t card,
                          uint8_t rx_preEmphasis,
                          uint8_t rx_serialAmplitude,
                          uint8_t tx_diffctrl,
                          uint8_t tx_precursor,
                          jesd_sync_result_t *p_sync_result) = NULL;
    
    if( argc != 14 )
    {
        printf("Error: invalid # args\n");
        print_usage(argv[0]);
        return (-1);
    }

    sscanf(argv[1], "%hhu", &card);
    sscanf(argv[2], "%u", &sample_rate);
    sscanf(argv[3], "%hhu", &start_pre);
    sscanf(argv[4], "%hhu", &end_pre);
    sscanf(argv[5], "%hhu", &start_amp);
    sscanf(argv[6], "%hhu", &end_amp);
    sscanf(argv[7], "%hhu", &start_diffctrl);
    sscanf(argv[8], "%hhu", &end_diffctrl);
    sscanf(argv[9], "%hhu", &start_precursor);
    sscanf(argv[10], "%hhu", &end_precursor);
    sscanf(argv[11], "%hhu", &delay_ms);
    sscanf(argv[12], "%u", &repeat);
    p_results_file = fopen( argv[13], "w" );
    if( p_results_file == NULL )
    {
        printf("Error: unable to open results file %s\n", argv[7]);
        return (-1);
    }

    skiq_init( skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1 );
    skiq_write_rx_sample_rate_and_bandwidth( card, skiq_rx_hdl_A2, sample_rate, sample_rate );
    skiq_read_parameters( card, &param );
    
    if( param.card_param.part_type == skiq_x2 )
    {
        printf("Info: using X2 JESD reset\n");
        reset_jesd = &(reset_jesd_x2);
    }
    else if( param.card_param.part_type == skiq_x4 )
    {
        printf("Info: using X4 JESD reset\n");
        reset_jesd = &(reset_jesd_x4);
    }
    else
    {
        printf("Error: product does not support JESD\n");
        _exit(-1);
    }

    skiq_register_critical_error_callback( &critical_handler, NULL );

    for( r=0; r<repeat; r++ )
    {
        for( i=start_pre; i<=end_pre; i++ )
        {
            for( j=start_amp; j<=end_amp; j++ )
            {
                for( k=start_diffctrl; k<=end_diffctrl; k++ )
                {
                    for( l=start_precursor; l<=end_precursor; l++ )
                    {
                        tot_num_tests++;
                        printf("\n---------------\n");
                        printf("preEmphasis %" PRIu8 ", amplitude %" PRIu8 ", diffctrl %" PRIu8 ", precursor %" PRIu8 ", \n",
                               i, j, k, l);

                        if( tot_num_tests <= 1 )
                        {
                            fprintf(p_results_file,"premphasis,amplitude,diffctrl,precursor,synced,jesd_status_imm,jesd_unsync_imm,framer_a_rx,framer_a_orx,deframer_a,framer_b_rx,framer_b_orx,deframer_b,jesd_status_dwell,jesd_unsync_dwell,rx_unsync_a,orx_unsync_a,tx_unsync_a,rx_unsync_b,orx_unsync_b,tx_unsync_b,retries\n");
                            fprintf(stdout,"premphasis,amplitude,diffctrl,precursor,synced,jesd_status_imm,jesd_unsync_imm,framer_a_rx,framer_a_orx,deframer_a,framer_b_rx,framer_b_orx,deframer_b,jesd_status_dwell,jesd_unsync_dwell,rx_unsync_a,orx_unsync_a,tx_unsync_a,rx_unsync_b,orx_unsync_b,tx_unsync_b,retries\n");
                        }

                        // reset our results 
                        memset( &result, 0, sizeof(jesd_sync_result_t) );
                                
                        if( reset_jesd( card, i, j, k, l, &result ) != 0 )
                        {
                            tot_sync_err++;
                            synced = 0;
                        }
                        else
                        {
                            synced = 1;
                            usleep(delay_ms*NUM_USEC_IN_MS);
                            
                            sidekiq_fpga_reg_read(card, FPGA_REG_JESD_STATUS, &result.jesd_status_dwell);
                            fpga_jesd_read_unsync_counts( card, &result.unsync_a, &result.unsync_b, &result.jesd_unsync_dwell );
                            printf("\tA Rx1/2 error count %u\n", result.unsync_a.rx_unsync);
                            printf("\tA ObsRx error count %u\n", result.unsync_a.orx_unsync);
                            printf("\tA Tx error count %u\n", result.unsync_a.tx_unsync);
                            printf("\tB Rx1/2 error count %u\n", result.unsync_b.rx_unsync);
                            printf("\tB ObsRx error count %u\n", result.unsync_b.orx_unsync);
                            printf("\tB Tx error count %u\n", result.unsync_b.tx_unsync);
                            
                            num_syncs++;
                        }
                        fprintf(p_results_file,
                                "%u,%u,%u,%u,%u,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,%u,%u,%u,%u,%u,%u,%u\n",
                                i, // preemp
                                j, // amp
                                k, // diffctrl
                                l, // precurs
                                synced,
                                result.jesd_status_imm,
                                result.jesd_unsync_imm,
                                result.framer_a_rx,
                                result.framer_a_orx,
                                result.deframer_a,
                                result.framer_b_rx,
                                result.framer_b_orx,
                                result.deframer_b,
                                result.jesd_status_dwell,
                                result.jesd_unsync_dwell,
                                result.unsync_a.rx_unsync,
                                result.unsync_a.orx_unsync,
                                result.unsync_a.tx_unsync,
                                result.unsync_b.rx_unsync,
                                result.unsync_b.orx_unsync,
                                result.unsync_b.tx_unsync,
                                result.num_retries);
                        fprintf(stdout,
                                "%u,%u,%u,%u,%u,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,%u,%u,%u,%u,%u,%u,%u\n",
                                i, // preemp
                                j, // amp
                                k, // diffctrl
                                l, // precurs
                                synced,
                                result.jesd_status_imm,
                                result.jesd_unsync_imm,
                                result.framer_a_rx,
                                result.framer_a_orx,
                                result.deframer_a,
                                result.framer_b_rx,
                                result.framer_b_orx,
                                result.deframer_b,
                                result.jesd_status_dwell,
                                result.jesd_unsync_dwell,
                                result.unsync_a.rx_unsync,
                                result.unsync_a.orx_unsync,
                                result.unsync_a.tx_unsync,
                                result.unsync_b.rx_unsync,
                                result.unsync_b.orx_unsync,
                                result.unsync_b.tx_unsync,
                                result.num_retries);

                        fflush(p_results_file);

                        printf("num_sync_success=%u, sync_errs=%u, total tests %u, repeat=%u\n\n",
                               num_syncs, tot_sync_err, tot_num_tests, r);
                    }
                }
            }
        }
    }

    fclose(p_results_file);

    return (0);
}

void print_usage(const char *progName)
{
    printf("Description:\n");
    printf("   Sweep the JESD sync state for the start/end values entered\n");
    printf("\n");
    printf("Usage: %s <card> <sample_rate> \n", progName);
    printf("   <start RX preEmphasis> <end RX preEmphasis>\n");
    printf("   <start RX amplitude> <end RX amplitude>\n");
    printf("   <start TX diffctrl> <end TX diffctrl>\n");
    printf("   <start TX precursor> <end TX precursor>\n");    
    printf("   <check sync status delay in ms> <repeat> <results filename>\n\n");
    
    printf("   X2 RX preEmphasis can vary 0:7, RX amplitude 18:26\n");
    printf("   X4 RX preEmphasis can vary 0:4, RX amplitude 0:15\n");
    printf("   X2/X4 TX diffctrl can vary 0:15, TX precursor 0:20\n");
    printf("\n");
}
