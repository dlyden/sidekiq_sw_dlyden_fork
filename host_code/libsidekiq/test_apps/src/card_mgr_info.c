/**
 * @file   card_mgr_info.c
 * @date   Tue Apr 21 14:25:02 CDT 2020
 *
 * @brief  Display all the information contained in the card manager shared memory.
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <inttypes.h>
#include <stdio.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "sidekiq_card_mgr_private.h"
#include "sidekiq_api_factory.h"


/***** GLOBAL FUNCTIONS *****/

int main( int argc, char* argv[] )
{
    int32_t status = 0;

    // initialize card_mgr to obtain all cards in system
    status = card_mgr_init();
    if ( status != 0 )
    {
        fprintf(stderr, "Error: failed to initialize card manager (status code = %" PRIi32 ")\n",
                status);
        return 1;
    }

    // print out any cards detected by card_mgr
    card_mgr_display_info();

    // exit card manager
    card_mgr_exit();

    return 0;
}
