/*! \file rfic_ctrl_in_test.c
 * \brief This file contains a basic application that tests the
 * the RFIC_CTRL_IN pins of the AD9361 are properly connected to the FPGA
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "sidekiq_api_factory.h"
#include "sidekiq_api.h"

static char* app_name;

static void print_usage(void);

/*****************************************************************************/
/** This is the main function for executing gpif_test commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    uint8_t card= 0;
    int32_t status = 0;
    app_name = argv[0];

    if( argc < 2 )
    {
        fprintf(stderr, "Error: invalid # args, %u\n", argc);
        print_usage();
        return -1;
    }

    card = (uint8_t)(strtoul(argv[1],NULL,10));

    if ( card >= SKIQ_MAX_NUM_CARDS )
    {
        fprintf(stderr, "Error: invalid card specified (%u)\n", card);
        return (-1);
    }

    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to initialize libsidekiq with status: %d and card: %u\n",
                status, card);
        return (-1);
    }

    status = skiq_fact_test_rfic_ctrl(card);
    if( 0 == status )
    {
        printf("Info: skiq_fact_test_rfic_ctrl completed without error\n");
    }
    else
    {
        printf("Info: skiq_fact_test_rfic_ctrl completed with error %d\n", status);
    }

    skiq_exit();

    return status;
}


/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <card>\n",app_name);
    printf("   test RFIC_CTRL_IN pins of the AD9361 are properly connected to the FPGA\n");
}
