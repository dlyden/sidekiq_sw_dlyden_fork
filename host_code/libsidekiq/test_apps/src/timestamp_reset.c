/*! \file pps_timestamp_test.c
 * \brief This file contains the implementation of testing
 * various PPS and timestamp reset features
 *  
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include <sidekiq_api.h>

#include <sidekiq_hal.h>

static char* app_name;

static void print_usage(void);

#define SYS_TIMESTAMP_DIFF (80000000) // 2 seconds = 80M
#define RF_TIMESTAMP_DIFF  (20000000) // 2 seconds with 10M sample rate = 20M

int main( int argc, char *argv[])
{
    uint8_t card=0;
    int32_t status=0;

    uint64_t last_pps_rf_timestamp;
    uint64_t last_pps_sys_timestamp;

    uint64_t pps_rf_timestamp;
    uint64_t pps_sys_timestamp;

    uint64_t rf_timestamp_diff;
    uint64_t sys_timestamp_diff;

    uint64_t new_timestamp=0x0000200000001000;
    uint64_t rf_timestamp=0;
    uint64_t sys_timestamp=0;

    uint8_t test_pps=0;
    skiq_1pps_source_t pps_source;

    app_name = argv[0];

    if( argc != 4 )
    {
	printf("Error: invalid # arguments\n");
	print_usage();
	return (-1);
    }

    card = atoi(argv[1]);
    test_pps = atoi(argv[2]);
    pps_source = atoi(argv[3]);

    // need to do a full init
    status = skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    // write the sample rate to a well known value
    skiq_write_rx_sample_rate_and_bandwidth(card, skiq_rx_hdl_A1, 10000000, 10000000);

    if( test_pps == 1 )
    {
        skiq_write_1pps_source( card, pps_source );

        skiq_read_1pps_source( card, &pps_source );
        printf("Read 1PPS source as %u\n", pps_source );

        // wait 2 sec for pps to occur
        sleep(2);
 
        skiq_read_last_1pps_timestamp(card, &last_pps_rf_timestamp, &last_pps_sys_timestamp);
        printf("Last 1PPS timestamp 0x%016" PRIx64 " (RF) / 0x%016" PRIx64 " (SYS)\n", 
               last_pps_rf_timestamp, last_pps_sys_timestamp);
    
        printf("sleeping for 2 seconds to wait for a PPS\n");
        sleep(2);
        printf("\n");

        skiq_read_last_1pps_timestamp(card, &pps_rf_timestamp, &pps_sys_timestamp);
        printf("1PPS timestamp 0x%016" PRIx64 " (RF) / 0x%016" PRIx64 " (SYS)\n", 
               pps_rf_timestamp, pps_sys_timestamp);

        rf_timestamp_diff = (pps_rf_timestamp-last_pps_rf_timestamp);
        sys_timestamp_diff = (pps_sys_timestamp-last_pps_sys_timestamp);
        
        printf("rf diff %" PRIu64 ", sys diff %" PRIu64 "\n\n", 
               rf_timestamp_diff, sys_timestamp_diff);

        // make sure the timestamp diff is correct
        if( ((rf_timestamp_diff != RF_TIMESTAMP_DIFF) &&
             (rf_timestamp_diff != RF_TIMESTAMP_DIFF+1) &&
             (rf_timestamp_diff != RF_TIMESTAMP_DIFF-1)) ||
             ((sys_timestamp_diff != SYS_TIMESTAMP_DIFF) &&
             (sys_timestamp_diff != SYS_TIMESTAMP_DIFF+1) &&
              (sys_timestamp_diff != SYS_TIMESTAMP_DIFF-1)) )
        {
            printf("Error: PPS timestamps did not align properly\n");
            status = -1;
            skiq_exit();
            _exit(status);
        }
    }

    // reset the timestamp immediately
    printf("resetting timestamp\n");
    skiq_reset_timestamps(card);
    skiq_read_curr_rx_timestamp(card, skiq_rx_hdl_A1, &rf_timestamp);
    skiq_read_curr_sys_timestamp(card, &sys_timestamp);
    printf("timestamp 0x%016" PRIx64 " (RF) / 0x%016" PRIx64 " (SYS)\n\n", 
           rf_timestamp, sys_timestamp);
    // make sure both timestamps are less than 0x1000
    if( (rf_timestamp > 0x1000) || (sys_timestamp > 0x3000) )
    {
        printf("Error: immediate timestamp reset must have failed\n");
        status = -2;
        skiq_exit();
        _exit(status);
    }

    // reset the timestamp immediately to a value
    printf("updating timestamp to 0x%016" PRIx64 "\n", new_timestamp);
    skiq_update_timestamps(card, new_timestamp);
    skiq_read_curr_rx_timestamp(card, skiq_rx_hdl_A1, &rf_timestamp);
    skiq_read_curr_sys_timestamp(card, &sys_timestamp);
    printf("timestamp 0x%016" PRIx64 " (RF) / 0x%016" PRIx64 " (SYS)\n\n", 
           rf_timestamp, sys_timestamp);
    // since we're resetting to a large value, we can just make sure the current 
    // timestamp is larger than what we should have reset it to
    if( (rf_timestamp < new_timestamp) || (sys_timestamp < new_timestamp) )
    {
        printf("Error: immediate timestamp reset to a specific value must have failed\n");
        status = -3;
        skiq_exit();
        _exit(status);
    }

    if( test_pps == 1 )
    {
        // reset the timestamp on PPS
        printf("resetting timestamp on pps\n");
        skiq_write_timestamp_reset_on_1pps(card, 0);
        printf("sleeping for 2 seconds to wait for a PPS\n");
        sleep(2);
        skiq_read_curr_rx_timestamp(card, skiq_rx_hdl_A1, &rf_timestamp);
        skiq_read_curr_sys_timestamp(card, &sys_timestamp);
        printf("timestamp 0x%016" PRIx64 " (RF) / 0x%016" PRIx64 " (SYS)\n\n", 
               rf_timestamp, sys_timestamp);
        // since we're resetting back to 0 (from a large #) and waiting 2 seconds, 
        // we just need to make sure that the timestamp is less than the timestamp diff
        if( (rf_timestamp > RF_TIMESTAMP_DIFF) || (sys_timestamp > SYS_TIMESTAMP_DIFF) )
        {
            printf("Error: reset timestamp on PPS must have failed\n");
            status = -4;
            skiq_exit();
            _exit(status);
        }

        // update the timestamp on PPS
        printf("resetting timestamp on pps to a specific value\n");
        skiq_write_timestamp_update_on_1pps(card, 0, new_timestamp);
        printf("sleeping for 2 seconds to wait for a PPS\n");
        sleep(2);
        skiq_read_curr_rx_timestamp(card, skiq_rx_hdl_A1, &rf_timestamp);
        skiq_read_curr_sys_timestamp(card, &sys_timestamp);
        printf("timestamp 0x%016" PRIx64 " (RF) / 0x%016" PRIx64 " (SYS)\n\n", 
               rf_timestamp, sys_timestamp);
        // since we're resetting to a large value, we can just make sure the current 
        // timestamp is larger than what we should have reset it to
        if( (rf_timestamp < new_timestamp) || (sys_timestamp < new_timestamp) )
        {
            printf("Error: immediate timestamp reset on PPS to a specific value must have failed\n");
            status = -5;
            skiq_exit();
            _exit(status);
        }
        printf("Info: timestamp resetting with PPS is functioning\n");
    }

    printf("Info: timestamp resetting is functioning\n");
    skiq_exit();

    return (status);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <card> <test PPS 0|1> <pps source 0=external, 1=host>\n", app_name);
    printf("\tResets timestamps, optionally using using PPS if specified.\n");
}
