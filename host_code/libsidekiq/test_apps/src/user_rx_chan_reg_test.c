/*! \file user_reg_test.c
 * \brief This file contains a basic application that reads and writes
 * the user-definable Rx channel based registers in the FPGA using libsidekiq.
 *  
 * <pre>
 * Copyright 2014 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sidekiq_api.h>

static char* app_name;

static void print_usage(void);


/*****************************************************************************/
/** This is the main function for executing reg_test commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    uint32_t offset,data;
    int32_t status;
    skiq_rx_hdl_t hdl=skiq_rx_hdl_end;
    uint8_t card=0;

    app_name = argv[0];
    card = (uint8_t)(strtoul(argv[2],NULL,10));

    status = skiq_init(skiq_pcie_init_level_1, skiq_usb_init_level_0, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    if( (argc != 6) && (argc != 7) )
    {
	printf("Error: invalid # args\n");
	print_usage();
	return (-1);
    }

    if( strcasecmp(argv[3], "a") == 0 )
    {
	if( strcasecmp(argv[4], "1") == 0 )
	{
	    hdl = skiq_rx_hdl_A1;
	}
	else if( strcasecmp(argv[4], "2") == 0 )
	{
	    hdl = skiq_rx_hdl_A2;
	}
	else
	{
	    printf("Error: invalid chip ID specified\n");
	    print_usage();
	    return (-2);
	}
    }
    else if( strcasecmp(argv[3], "b") == 0 )
    {
	if( strcasecmp(argv[4], "1") == 0 )
	{
	    hdl = skiq_rx_hdl_B1;
	}
        else
        {
            printf("Error: invalid chip ID specified\n");
            print_usage();
            return (-2);
        }
    }    
    else
    {
	printf("Error: invalid Rx path specified\n");
	print_usage();
	return (-3);
    }
    
    if (argc == 6)
    {
	/* should be read, but confirm */
	if (strcasecmp(argv[1],"r") == 0)
	{	    
	    /* convert offset to read to unsigned long */
	    offset=(uint32_t)strtoul(argv[5],NULL,16);

	    if ((status = skiq_read_rx_chan_user_fpga_reg(card, hdl, offset, &data)) != 0)
	    {
		printf("Error: failed to read FPGA address at offset 0x%08x, status is %d\n",offset,status);
	    }
	    else
	    {
		printf("Info: read at offset=0x%08x, data=0x%08x for Rx handle %u on card %u\n",
		       offset,data,hdl,card);
	    }
	}
	else
	{
	    printf("Error: invalid command type %s\n",argv[1]);
	}
    }
    else if (argc == 7)
    {
	/* should be a write, but confirm */
	if (strcasecmp(argv[1],"w") == 0)
	{
	    /* convert offset and val to unsigned long */
	    offset=(uint32_t)strtoul(argv[5],NULL,16);
	    data=(uint32_t)strtoul(argv[6],NULL,16);

	    if ((status = skiq_write_rx_chan_user_fpga_reg(card, hdl, offset, data)) != 0)
	    {
		printf("Error: failed to read FPGA address at offset 0x%08x, status is %d\n",offset,status);
	    }
	    else
	    {
		printf("Info: wrote at offset=0x%08x, data=0x%08x for Rx handle %u on card %u\n",
		       offset,data,hdl,card);
	    }
	}
	else
	{
	    printf("Error: invalid command type %s\n",argv[1]);
	}
    }
    else
    {
	print_usage();
    }

    return(0);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <r|w> <card> <chip ID a> <path within chip ID 1|2> <offset in hex> <32-bit val in hex>\n",app_name);
    printf("   read or write a user-defined register in the Rx channel FPGA space for the specified Sidekiq\n");
    printf("   User accessible rx channel regs start at offset 0x%04x and end at offset 0x%04x\n",
	   SKIQ_START_RX_CHAN_USER_REG_OFFSET,SKIQ_END_RX_CHAN_USER_REG_OFFSET);
}

    
