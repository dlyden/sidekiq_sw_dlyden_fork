/**
 * @file   store_calibration.c
 * @author  <info@epiq-solutions.com>
 * @date   Thu Nov  9 09:37:29 2017
 *
 * @brief This application stores calibration data to non-volatile storage
 *
 * <pre>
 * Copyright 2017 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include "sidekiq_api_factory.h"
#include <arg_parser.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- store calibration into non-volatile storage";
static const char* p_help_long =
    "This private test application showcases how to load / clear / add / commit\n"
    "receive calibration files for a Sidekiq card.";

/* Command Line Argument Variables */
/* file path to calibration files */
static char* p_file_paths[] = {
    [ 0 ... (skiq_rx_hdl_end-1) ] = NULL,
};
/* file path to calibration files for Z2 and M.2-2280 */
static char* p_file_paths_by_port[] = {
    [ 0 ... (skiq_rf_port_max-1) ] = NULL,
};
/* Sidekiq card index */
static uint8_t card = SKIQ_MAX_NUM_CARDS;
/* Sidekiq serial number */
static char* p_serial = NULL;
/* Allow libsidekiq log output */
static bool verbose = false;
/* call card_calibration_clear() before adding tables */
static bool clear_first = true;
/* force commit of calibration tables, even if none are provided */
static bool no_files = true;

/* command line arguments available for use with this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("rxa1",
                0,
                "Calibration file for RxA1",
                "PATH",
                &(p_file_paths[skiq_rx_hdl_A1]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("j1",
                0,
                "Calibration file for handle RxA1 on RF port J1 (Z2/Z2+/M.2-2280 only)",
                "PATH",
                &(p_file_paths_by_port[skiq_rf_port_J1]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("j2",
                0,
                "Calibration file for handle RxA1 on RF port J2 (Z2/Z2+/M.2-2280 only)",
                "PATH",
                &(p_file_paths_by_port[skiq_rf_port_J2]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("j3",
                0,
                "Calibration file for handle RxA1 on RF port J3 (Z2 only)",
                "PATH",
                &(p_file_paths_by_port[skiq_rf_port_J3]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("j300",
                0,
                "Calibration file for handle RxA1 on RF port J300 (Z2 only)",
                "PATH",
                &(p_file_paths_by_port[skiq_rf_port_J300]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("rxa2",
                0,
                "Calibration file for RxA2",
                "PATH",
                &(p_file_paths[skiq_rx_hdl_A2]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("rxb1",
                0,
                "Calibration file for RxB1 (X2 only)",
                "PATH",
                &(p_file_paths[skiq_rx_hdl_B1]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("rxb2",
                0,
                "Calibration file for RxB2 (X4 only)",
                "PATH",
                &(p_file_paths[skiq_rx_hdl_B2]),
                STRING_VAR_TYPE),
    APP_ARG_OPT("verbose",
                'v',
                "Enable logging from libsidekiq to stdout",
                NULL,
                &verbose,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("clear",
                0,
                "Clear existing calibration tables before adding new tables",
                NULL,
                &clear_first,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("no-files",
                0,
                "Force committing calibration tables when no files are provided (implies --clear and conflicts with --rxa1, --rxa2, --rxb1, --rxb2, --j1, --j2, --j3, and --j300)",
                NULL,
                &no_files,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


/*****************************************************************************/
/** This is the main function for executing the store_default_fpga app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    skiq_rx_hdl_t hdl;
    skiq_rf_port_t port;
    int32_t status = 0;
    FILE* pFiles[] = {
        [ 0 ... (skiq_rx_hdl_end-1) ] = NULL,
    };
    FILE* pFiles_by_port[] = {
        [ 0 ... (skiq_rf_port_max-1) ] = NULL,
    };
    int nr_files = 0;
    pid_t owner = 0;

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        // won't return from the above function
    }

    if( !verbose )
    {
        /* disable messages */
        skiq_register_logging(NULL);
    }

    for (hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++)
    {
        if ( p_file_paths[hdl] != NULL )
        {
            pFiles[hdl] = fopen( p_file_paths[hdl], "rb" );
            if( pFiles[hdl] == NULL )
            {
                fprintf(stderr, "Error: unable to open file %s to read from\n",
                        p_file_paths[hdl]);
                status = -1;
                goto close_files_and_exit;
            }
            nr_files++;
        }
    }

    for (port = skiq_rf_port_J1; port < skiq_rf_port_max; port++)
    {
        if ( p_file_paths_by_port[port] != NULL )
        {
            pFiles_by_port[port] = fopen( p_file_paths_by_port[port], "rb" );
            if( pFiles_by_port[port] == NULL )
            {
                printf("Error: unable to open file %s to read from\n", p_file_paths_by_port[port]);
                status = -1;
                goto close_files_and_exit;
            }
            nr_files++;
        }
    }

    if ( ( nr_files == 0 ) && ( !no_files ) )
    {
        fprintf(stderr, "Error: at least one calibration file must be"
                " specified\n");
        return (-1);
    }

    if ( ( nr_files > 0 ) && ( no_files ) )
    {
        fprintf(stderr, "Error: no calibration files may be specified when"
                " --no-files is used\n");
        return (-1);
    }

    if ( no_files )
    {
        /* --no-files implies --clear */
        clear_first = true;
    }

    if ( ( SKIQ_MAX_NUM_CARDS == card ) && ( NULL == p_serial ) )
    {
        fprintf(stderr, "Error: one of --card or --serial MUST be specified\n");
        status = -1;
        goto close_files_and_exit;
    }
    else if ( ( SKIQ_MAX_NUM_CARDS != card ) && ( NULL != p_serial ) )
    {
        fprintf(stderr, "Error: EITHER --card OR --serial must be specified,"
                " not both\n");
        status = -1;
        goto close_files_and_exit;
    }

    /* check for serial number */
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string( p_serial, &card );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: unable to find Sidekiq with serial number"
                    " %s\n", p_serial);
            status = -1;
            goto close_files_and_exit;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID"
                " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto close_files_and_exit;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    status = skiq_init( skiq_xport_type_auto, skiq_xport_init_level_full,
                &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int)owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %" PRIi32 "\n", status);
        }
        status = -1;
        goto close_files_and_exit;
    }

    /* RF port validation */
    {
        uint8_t nr_fixed_ports, nr_trx_ports;
        skiq_rf_port_t fixed_ports[skiq_rf_port_max], trx_ports[skiq_rf_port_max];
        bool has_port[skiq_rf_port_max] = { [0 ... (skiq_rf_port_max-1)] = false };

        status = skiq_read_rx_rf_ports_avail_for_hdl( card, skiq_rx_hdl_A1,
                                                      &nr_fixed_ports, fixed_ports,
                                                      &nr_trx_ports, trx_ports );
        if ( status == 0 )
        {
            while ( nr_fixed_ports-- > 0 )
            {
                has_port[fixed_ports[nr_fixed_ports]] = true;
            }

            while ( nr_trx_ports-- > 0 )
            {
                has_port[trx_ports[nr_trx_ports]] = true;
            }

            for (port = skiq_rf_port_J1; port < skiq_rf_port_max; port++)
            {
                if ( !has_port[port] && ( pFiles_by_port[port] != NULL ) )
                {
                    fprintf(stderr, "Error: Sidekiq card %u does not have port %u\n", card, port);
                    status = -1;
                    goto close_files_and_exit;
                }
            }
        }
        else
        {
            fprintf(stderr, "Error: Failed to read available RF ports for skiq_rx_hdl_A1 on card "
                    "%u (result code %" PRIi32 ")\n", card, status);
            status = -1;
            goto close_files_and_exit;
        }
    }

    /* clear the calibration first */
    if ( clear_first )
    {
        skiq_fact_calibration_clear(card);
    }

    /* add the calibration tables by handle if specified */
    for (hdl = skiq_rx_hdl_A1; (status == 0) && (hdl < skiq_rx_hdl_end); hdl++)
    {
        if ( pFiles[hdl] != NULL )
        {
            printf("Info: adding RX calibration in %s to card %u hdl %u\n",
                    p_file_paths[hdl], card, hdl);
            status = skiq_fact_calibration_add_rx_by_file( card, hdl,
                        skiq_rf_port_J1, pFiles[hdl] );
            if ( 0 != status )
            {
                fprintf(stderr, "Error: failed to add RX calibration in %s to"
                        " card %u hdl %u, status = %d\n",
                        p_file_paths[hdl], card, hdl, status);
            }
            else
            {
                printf("Info: success!\n");
            }
        }
    }

    /* add the calibration tables by port if specified (default to hdl = skiq_rx_hdl_A1) */
    hdl = skiq_rx_hdl_A1;
    for (port = skiq_rf_port_J1; (status == 0) && (port < skiq_rf_port_max); port++)
    {
        if ( pFiles_by_port[port] != NULL )
        {
            printf("Info: adding RX calibration in %s to card %u hdl %u port %u\n", p_file_paths_by_port[port], card, hdl, port);
            status = skiq_fact_calibration_add_rx_by_file( card, hdl, port, pFiles_by_port[port] );
            if ( 0 != status )
            {
                fprintf(stderr, "Error: failed to add RX calibration in %s to card %u hdl %u port %u, status = %d\n",
                        p_file_paths_by_port[port], card, hdl, port, status);
            }
            else
            {
                printf("Info: success!\n");
            }
        }
    }

    if ( 0 == status )
    {
        printf("Info: committing calibration to card %u\n", card);
        status = skiq_fact_calibration_commit( card );
        if ( 0 != status )
        {
            fprintf(stderr, "Error: failed to commit calibration to NVM for"
                    " card %u, status = %d ('%s')\n", card, status,
                    strerror(-status));
        }
    }

    skiq_exit();

close_files_and_exit:
    for (hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++)
    {
        if ( pFiles[hdl] != NULL )
        {
            fclose(pFiles[hdl]);
            pFiles[hdl] = NULL;
        }
    }

    for (port = skiq_rf_port_J1; port < skiq_rf_port_max; port++)
    {
        if ( pFiles_by_port[port] != NULL )
        {
            fclose(pFiles_by_port[port]);
        }
    }

    return (int) status;
}

