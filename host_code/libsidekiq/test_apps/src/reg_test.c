/*! \file reg_test.c
 * \brief This file contains a basic application that reads and writes
 * registers in the Sidekiq FPGA.  These registers are accessed over
 * PCIe.
 *  
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <bit_ops.h>
#include <sidekiq_hal.h>
#include <sidekiq_api.h>

/* internal interface */
#include <sidekiq_xport.h>

static char* app_name;

static void print_usage(void);


/*****************************************************************************/
/** This is the main function for executing reg_test commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    unsigned long addr,data;
    uint8_t chip_sel;
    uint8_t card;
    int32_t status = 0;
    skiq_xport_init_level_t level=skiq_xport_init_level_basic;

    app_name = argv[0];

    if( argc < 5 || argc > 7 )
    {
        printf("Error: invalid # args\n");
        print_usage();
        return (-1);
    }

    card = (uint8_t)(strtoul(argv[3],NULL,10));

    /* bring up the minimal FPGA interface to allow FPGA register
       reads/writes to work */
    status = skiq_init(skiq_xport_type_auto, level, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    /* should be read, but confirm */
    if (strcasecmp(argv[2],"r") == 0)
    {       
        if (strcasecmp(argv[1],"fpga") == 0) 
        {
            /* reading an FPGA reg, proceed */
            addr=(uint32_t)strtoul(argv[4],NULL,16);
            status=sidekiq_fpga_reg_read(card,addr,(uint32_t*)&data);
            printf("Info: FPGA read: card=%u addr=0x%08lx, data=0x%08lx\n",
                   card,addr,data);
        }
        if( (strcasecmp(argv[1],"fpga64") == 0) )
        {
            uint64_t data64=0xDEADBEEFBEEFDEAD;
            /* reading an FPGA reg, proceed */
            addr=(uint32_t)strtoul(argv[4],NULL,16);
            status=sidekiq_fpga_reg_read_64(card,addr+4,addr,&data64);
            printf("Info: FPGA read: card=%u addr=0x%08lx, data=0x%" PRIx64 "\n",
                   card,addr,data64);
        }
        
        else if (strcasecmp(argv[1],"rfic") == 0)
        {
            chip_sel = (uint8_t)(strtoul(argv[4],NULL,10));
            if( chip_sel != 0 && chip_sel != 1 )
            {
                printf("Error: invalid chip select specified (%u)\n", chip_sel);
                return (-1);
            }                    
            addr=(uint16_t)strtoul(argv[5],NULL,16);
            status=hal_rfic_read_reg(card, chip_sel, addr,(uint8_t*)&data);
            printf("Info: rfic read: card=%u cs=%u addr=0x%04x, data=0x%02x\n",
                   card, chip_sel, (uint32_t)addr, (uint8_t)data);
        }
        else
        {
            printf("Error: invalid device for reading\n");
            print_usage();
            return(-2);
        }
    }
    
    else if (strcasecmp(argv[2],"w") == 0)
    {
        if (strcasecmp(argv[1],"fpga") == 0)
        {
            /* writing an FPGA reg, proceed */
            addr=(uint32_t)strtoul(argv[4],NULL,16);
            data=(uint32_t)strtoul(argv[5],NULL,16);
            status=sidekiq_fpga_reg_write(card,addr,data);
            printf("Info: FPGA write: card=%u addr=0x%08lx, data=0x%08lx\n",
                   card,addr,data);
        }
        if (strcasecmp(argv[1],"fpga64") == 0)
        {
            uint64_t data64;
            /* writing an FPGA reg, proceed */
            addr=(uint32_t)strtoul(argv[4],NULL,16);
            data64=(uint64_t)strtoul(argv[5],NULL,16);
            status=sidekiq_fpga_reg_write_64(card,addr+4,addr,data64);
            printf("Info: FPGA write: card=%u addr=0x%08lx, data=0x%" PRIx64 "\n",
                   card,addr,data64);
        }
        else if (strcasecmp(argv[1],"rfic") == 0)
        {
            chip_sel = (uint8_t)(strtoul(argv[4],NULL,10));
            if( chip_sel != 0 && chip_sel != 1 )
            {
                printf("Error: invalid chip select specified (%u)\n", chip_sel);
                return (-1);
            }                    
            addr=(uint16_t)strtoul(argv[5],NULL,16);
            data=(uint8_t)strtoul(argv[6],NULL,16);
            status=hal_rfic_write_reg(card, chip_sel, addr,data);
            printf("Info: rfic write: card=%u cs=%u addr=0x%04x, data=0x%02x\n",
                   card, chip_sel, (uint16_t)addr, (uint8_t)data);
        }
        else
        {
            printf("Error: invalid device for writing\n");
            print_usage();
            return(-2);
        }
    }       
    else
    {
        printf("Error: invalid command\n");
        print_usage();
        return(-3);
    }

    skiq_exit();

    return(status);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <fpga|fpga64|rfic> <r|w> <card> {chip_sel 0 | 1} <addr in hex> <val in hex>\n",app_name);
    printf("   read or write a register to the specified Sidekiq card\n");
    printf("   if fpga, read/write the actual FPGA register\n");
    printf("   if RFIC, read/write the addr/data for the specified ad9361 chip_id (which\n");
    printf("   is 'a'\n");
}

    
