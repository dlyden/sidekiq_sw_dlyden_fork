/*! \file gpif_test.c
 * \brief This file contains a basic application that reads and writes
 * the pins in the GPIF interface.
 *  
 * <pre>
 * Copyright 2014 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <bit_ops.h>
#include "sidekiq_api_factory.h"
#include "usb_interface.h"
#include "sidekiq_api.h"
#include "usb_private.h"

static char* app_name;

static void print_usage(void);

#define GPIF_M2_BITMASK (0x2800)

/*****************************************************************************/
/** This is the main function for executing gpif_test commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    int k;
    int error_flag =0;
    uint8_t card=0;
    uint16_t data=0;
    uint16_t read=0;
    uint8_t fpga_status = 0;
    int32_t status = 0;
    skiq_hw_vers_t hw_vers;
    bool is_fpga_write = false;
    app_name = argv[0];

    if( argc < 4 )
    {
        printf("Error: invalid # args, %u\n", argc);
        print_usage();
        return -1;
    }

    card = (uint8_t)(strtoul(argv[1],NULL,10));

    if ( card >= SKIQ_MAX_NUM_CARDS )
    {
        printf("Error: invalid card specified (%u)\n", card);
        return (-1);
    }

    /* bring up the minimal FPGA interface to allow FPGA register
       reads/writes to work and USB access to work */
    status = skiq_init(skiq_xport_type_pcie, skiq_xport_init_level_basic, &card, 1);
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    skiq_read_hw_version(card, &hw_vers);

    if( strcasecmp(argv[2],"fpga") == 0 )
    {
        is_fpga_write = true;
    }
    
    for( k = 3; (k < argc) && (error_flag == 0); k++ )
    {
        data = (uint16_t)strtoul(argv[k],NULL,0);
        status = skiq_fact_test_gpif(card, is_fpga_write, data, &read);
        if( 0 == status )
        {
            printf("Wrote 0x%x from FPGA to USB, read back 0x%x\n",data, read);
        }
        if( read != data )
        {
            error_flag = 1;
        }
    }

    if( error_flag )
    {
        printf("Error: value(s) written do not match value(s) read!\n");
    }

    // restore the LED based on FPGA configured if we're on m2
    if( (hw_vers == skiq_hw_vers_m2_b) || (hw_vers == skiq_hw_vers_m2_c) )
    {
        uint8_t index=0;
        usb_get_index(card, &index);
        usb_read_fpga_config(index, &fpga_status);
    }
    skiq_exit();

    return 0;
}


/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <card> <fpga|usb> <value(s) to write>\n",app_name);
    printf("   write a value with FPGA or USB as the master and read the result\n");
}
