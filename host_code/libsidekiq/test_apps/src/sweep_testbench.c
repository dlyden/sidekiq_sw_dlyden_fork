/*! \file sweep_testbench.c
 * \brief This file contains a basic application for sweeping a
 * specified frequency range via various parameters and performing and log
 * retune times. A CSV file is create with metadata of the various settings used
 * for transmit as well as receive (assuming the user does not disable one
 * direction, followed by x lines of <start freq>, <stop freq>, and <tune time>.
 *
 * <pre>
 * Copyright 2015 - 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <inttypes.h>
#include <sys/time.h>

#if (defined __MINGW32__)
#define OUTPUT_PATH_MAX                 260
#else
#include <linux/limits.h>
#define OUTPUT_PATH_MAX                 PATH_MAX
#endif

#include "arg_parser.h"
#include "sidekiq_api.h"
#include "elapsed.h"

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */

#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef DEFAULT_CARD_NUMBER
#   define DEFAULT_CARD_NUMBER  0
#endif

#define DUAL_TX_BLOCKSIZE               1022
#define NUM_BLOCKS                      100

// these are used to provide help strings for the application when running it
// with either the "-h" or "--help" flags
static const char* p_help_short = "- time retunes for TX and RX";
static const char* p_help_long = "\
The LO frequency is updated and timed for the change. Data is ignored, the\n\
timing of the retune is all that is monitored.  Various metrics are logged to\n\
a specified logfile.\n\
\n\
Defaults:\n\
  --card=" str(DEFAULT_CARD_NUMBER) "\n\
  --handle=A1\n\
  --rate=10000000\n\
  --start=75000000\n\
  --stop=6000000000\n\
  --step=30000000\n\
  --repeat=0\n\
";

// command line argument variables
static uint8_t card = UINT8_MAX;
static char* p_serial = NULL;
static char* p_hdl = "A1";
static uint32_t sample_rate = 10000000;
static bool quadcal_auto = false;
static uint64_t start_freq = 75000000;
static uint64_t stop_freq = 6000000000;
static uint64_t step_size = 30000000;
static uint32_t repeat = 0;
static bool freq_hop = false;
static bool run_rx = false;
static bool run_tx = false;

static char filename[OUTPUT_PATH_MAX];

// file path to save logs
static char* p_file_path = NULL;

// boolean used to track status of application
static bool running = true;

// the command line arguments available to this application
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("handle",
                0,
                "Tx handle to use, either A1, A2, B1, or B2",
                "Tx",
                &p_hdl,
                STRING_VAR_TYPE),
    APP_ARG_OPT("rate",
                'r',
                "Sample rate in Hertz",
                "Hz",
                &sample_rate,
                UINT32_VAR_TYPE),
    APP_ARG_OPT("quadcal_auto",
                0,
                "Auto TX quadrature calibration mode",
                0,
                &quadcal_auto,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("repeat",
                0,
                "Sweep frequency an additional N times",
                "N",
                &repeat,
                INT32_VAR_TYPE),
    APP_ARG_OPT("start",
                0,
                "Starting LO frequency",
                "Hz",
                &start_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("stop",
                0,
                "End LO frequency",
                "Hz",
                &stop_freq,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("step",
                0,
                "LO frequency step size",
                "Hz",
                &step_size,
                UINT64_VAR_TYPE),
    APP_ARG_OPT("freq-hop",
                0,
                "Use frequency hopping API instead of traditional tuning API",
                0,
                &freq_hop,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("rx",
                0,
                "Run a receive test",
                NULL,
                &run_rx,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("tx",
                0,
                "Run a transmit test",
                NULL,
                &run_tx,
                BOOL_VAR_TYPE),
    APP_ARG_REQ("filename",
                'f',
                "logfile name",
                "PATH",
                &p_file_path,
                STRING_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


/***** LOCAL FUNCTIONS *****/

static void app_cleanup(int signum);
static int32_t receive_data(void);
static void dump_packet_contents( uint32_t block_num,
                                  uint8_t *p_data,
                                  uint32_t num_to_dump );
int receive_sweep(void);
int transmit_sweep(void);
void clean_exit(void);
struct timespec diff(struct timespec start, struct timespec end);

/*****************************************************************************/
/** This is the cleanup handler to ensure that the app properly exits and
    does the needed cleanup if it ends unexpectedly.

    @param signum: the signal number that occurred
    @return void
*/
void app_cleanup(int signum)
{
    printf("Info: received signal %d, cleaning up libsidekiq\n", signum);
    running = false;
}


/*****************************************************************************/
/** This is the main function for executing the sweep_receive app.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status=0;
    pid_t owner = 0;

    // install a signal handler for proper cleanup
    signal(SIGINT, app_cleanup);

    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( status != 0 )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    if( (UINT8_MAX != card) && (NULL != p_serial) )
    {
        fprintf(stderr, "Error: must specify EITHER card ID or serial number, not both\n");
        return (-1);
    }

    if (UINT8_MAX == card)
    {
        card = DEFAULT_CARD_NUMBER;
    }

    // If specified, attempt to find the card with a matching serial number.
    if ( NULL != p_serial )
    {
        status = skiq_get_card_from_serial_string(p_serial, &card);
        if (status != 0)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s (result code %" PRIi32
                    ")\n", p_serial, status);
            return (-1);
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n", p_serial, card);
    }
    else
    {
        status = skiq_read_serial_string(card, &p_serial);
        if (status != 0)
        {
            fprintf(stderr, "Error: cannot read the serial number %s (result code %" PRIi32 ")\n",
                    p_serial, status);
            return (-1);
        }

        printf("Info: Serial number is %s for card ID %" PRIu8 "\n", p_serial, card);
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID (%" PRIu8 ")\n",
                card, (SKIQ_MAX_NUM_CARDS - 1));
        return (-1);
    }

    /* if neither RX nor TX is specified by the user, the default is to run both */
    if ( !run_rx && !run_tx )
    {
        run_rx = true;
        run_tx = true;
    }

    printf("Info: initializing card %" PRIu8 "...\n", card);

    // initialize libsidekiq
    status = skiq_init(skiq_xport_type_auto,
                       skiq_xport_init_level_full,
                       &card,
                       1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail(card, &owner) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by process ID %u); cannot "
                    "initialize card.\n", card, (unsigned int) owner);
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a valid card"
                    " specified? (result code %" PRIi32 ")\n", status);
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with status %" PRIi32
                    "\n", status);
        }
        clean_exit();
        return (-1);
    }

    //*******************************************
    // Receive portion of the sweep benchmark ***
    //*******************************************
    if ( ( status == 0 ) && run_rx )
    {
        status = receive_sweep();
        if ( status != 0 )
        {
            fprintf(stderr, "Error: failed to run receive sweep with status %d\n", status);
            clean_exit();
            return(-1);
        }
    }

    //*******************************************
    // Transmit portion of the sweep benchmark **
    //*******************************************
    if ( ( status == 0 ) && run_tx )
    {
        status = transmit_sweep();
        if ( status != 0 )
        {
            fprintf(stderr, "Error: failed to run transmit sweep with status %d\n", status);
            clean_exit();
            return(-1);
        }
    }

    skiq_exit();
    return (0);
}


int receive_sweep()
{
    skiq_rx_hdl_t hdl;
    skiq_chan_mode_t chan_mode = skiq_chan_mode_single;
    int32_t status = 0;
    uint64_t curr_freq = start_freq;
    uint32_t num_receive_errors = 0;
    struct timespec start, stop, result;
    uint64_t num_ns = 0;
    FILE *fp = NULL;
    int32_t dec_loc = 0;
    char rx_insert[3] = {'_','r','x'};
    uint16_t num_hop_freqs = (stop_freq-start_freq)/step_size + 1;
    uint64_t freq_list [num_hop_freqs];
    uint64_t prev_freq = start_freq + (num_hop_freqs-1)*step_size;
    uint64_t base_ts = 0;
    uint64_t curr_ts;

    //copy filename string into a buffer and append the _rx suffix at the end of
    //the filename and before the file extension (if it exists)
    memset(filename, 0, OUTPUT_PATH_MAX);
    strncpy(filename, p_file_path, OUTPUT_PATH_MAX-1);
    dec_loc = (strrchr(filename,'.') == NULL) ? strlen(filename) : (uint32_t)(strrchr(filename,'.')-filename);
    memmove (filename+dec_loc+3,filename+dec_loc,strlen(filename)-3);
    memcpy(&filename[dec_loc], &rx_insert, 3);

    //open the logfile
    fp = fopen( filename, "w" );
    if( fp == NULL )
    {
        fprintf(stderr, "Error: unable to open file %s to read from (error code"
                " %d: '%s')\n", filename, errno, strerror(errno));
        return -errno;
    }

    //write the meta data line to the beginning of the logfile
    fprintf(fp, "%s,%s,%" PRIu32",%" PRIu64 ",%" PRIu64 ",%"
            PRIu64 ",%d,%" PRIu32 ",%d\n", p_serial, p_hdl, sample_rate,
            start_freq, stop_freq, step_size, freq_hop, repeat, quadcal_auto);

    //map argument values to sidekiq specific variable values
    if( 0 == strcasecmp(p_hdl, "A1") )
    {
        hdl = skiq_rx_hdl_A1;
        printf("Info: using Rx handle A1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "A2") )
    {
        hdl = skiq_rx_hdl_A2;
        chan_mode = skiq_chan_mode_dual;
        printf("Info: using Rx handle A2\n");
    }
    else if( 0 == strcasecmp(p_hdl, "B1") )
    {
        hdl = skiq_rx_hdl_B1;
        printf("Info: using Rx handle B1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "B2") )
    {
        hdl = skiq_rx_hdl_B2;
        chan_mode = skiq_chan_mode_dual;
        printf("Info: using Rx handle B2\n");
    }
    else if( 0 == strcasecmp(p_hdl, "C1") )
    {
        hdl = skiq_rx_hdl_C1;
        printf("Info: using Rx handle C1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "D1") )
    {
        hdl = skiq_rx_hdl_D1;
        printf("Info: using Rx handle D1\n");
    }
    else
    {
        fprintf(stderr, "Error: invalid handle specified (%s)\n", p_hdl);
        status = -EINVAL;
        goto exit;
    }

    // write the channel mode (dual if A2/B2 is being used)
    status = skiq_write_chan_mode(card, chan_mode);
    if ( status != 0 )
    {
        fprintf(stderr, "Error: failed to set Rx channel mode to %u with status %d\n", chan_mode,
                status);
        goto exit;
    }

    // set the sample rate
    status = skiq_write_rx_sample_rate_and_bandwidth(card, hdl,
                sample_rate, sample_rate);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to write Rx sample rate (result code %" PRIi32 ")\n",
                status);
        goto exit;
    }

    // set the frequency to some default value
    status = skiq_write_rx_LO_freq(card, hdl, curr_freq);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to write Rx LO frequency (result code %" PRIi32 ")\n",
                status);
        goto exit;
    }

    // set counter mode for easier debug if it breaks
    status = skiq_write_rx_data_src(card, hdl,
                skiq_data_src_counter);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to set counter mode (result code %" PRIi32 ")\n",
                status);
        goto exit;
    }

    // tune to the "end" frequency to start without metrics
    status = skiq_write_rx_LO_freq(card, hdl, prev_freq);
    if ( status != 0 )
    {
        fprintf(stderr, "Error: failed to change RX LO frequency to %" PRIu64 " Hz (result code %"
                PRIi32 ")\n", curr_freq, status);
        goto exit;
    }

    printf("Info: starting RX sweep\n");

    if(freq_hop)
    {
        //create frequency hopping list
        uint64_t freq_val = start_freq;

        if (num_hop_freqs > SKIQ_MAX_NUM_FREQ_HOPS)
        {
            fprintf(stderr, "Error: too many freq hops. Requested %d, but max is %d\n",
                    num_hop_freqs, SKIQ_MAX_NUM_FREQ_HOPS);
            status = -EINVAL;
            goto exit;
        }

        for (uint32_t i = 0; i < num_hop_freqs; i++)
        {
            freq_list[i] = freq_val;
            freq_val += step_size;
        }
        prev_freq = freq_list[num_hop_freqs-1];

        // configure frequency hopping
        if( (status=skiq_write_rx_freq_tune_mode( card, hdl,
                skiq_freq_tune_mode_hop_immediate )) != 0 )
        {
            fprintf(stderr, "Error: failed to set tune mode status=%d\n", status);
            goto exit;
        }

        // configure the hopping list
        status = skiq_write_rx_freq_hop_list( card, hdl, num_hop_freqs,
                    freq_list, 0 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: failed to set hop list %d\n", status);
            goto exit;
        }

        // tune to the "end" frequency to start without metrics
        status = skiq_write_next_rx_freq_hop( card, hdl, (num_hop_freqs -1) );
        if( status != 0 )
        {
            fprintf(stderr, "Error: failed to write hop with status %d\n", status);
            goto exit;
        }

        status = skiq_perform_rx_freq_hop( card, hdl, 0 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: failed to perform hop with status %d\n", status);
            goto exit;
        }

        // reset the timestamps and wait for the reset to complete
        if( skiq_read_curr_rx_timestamp( card, hdl, &base_ts ) == 0 )
        {
            //printf("Resetting timestamps (base=%" PRIu64 ")\n", base_ts);

            printf("Resetting async\n");
            if( skiq_reset_timestamps( card ) != 0 )
            {
                fprintf(stderr, "Error: unable to reset timestamps\n");
            }

            if( skiq_read_curr_rx_timestamp( card, hdl, &curr_ts ) != 0 )
            {
                //fprintf(stderr, "Error: unable to read current timestamp\n");
            }
        }

        while( (base_ts < curr_ts) && (running==true) )
        {
            // just sleep 100 uS and read the timestamp again...we should be
            //close based on the initial sleep calculation
            usleep(100);
            skiq_read_curr_tx_timestamp(card, hdl, &curr_ts);
        }


        // start streaming
        status = skiq_start_rx_streaming_multi_immediate( card,
                                                          &hdl, 1 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: receive streaming failed to start with status code %d\n",
                    status);
            goto exit;
        }

        // grab some data
        status = receive_data();
        if ( status != 0 )
        {
            num_receive_errors++;
        }

        status = skiq_stop_rx_streaming_multi_immediate(card, &hdl, 1);
        if ( status != 0 )
        {
            fprintf(stderr, "Error: failed to stop RX streaming (result code %" PRIi32 ")\n",
                    status);
            goto exit;
        }

        for(uint32_t curr_iteration=0;
             (curr_iteration <= repeat) && (running==true);
             curr_iteration++ )
        {
            // receive data for each frequency
            for(int32_t i = 0; ((i < num_hop_freqs) && (running == true)); i++ )
            {
                int32_t status_temp = 0;

                clock_gettime(CLOCK_MONOTONIC, &start);
                status_temp = skiq_write_next_rx_freq_hop( card, hdl, i );
                status = skiq_perform_rx_freq_hop( card, hdl, 0 );
                clock_gettime(CLOCK_MONOTONIC, &stop);

                if(( status_temp != 0 ) || ( status != 0 ))
                {
                    fprintf(stderr, "Error: failed to write hop with status %d\n", status);
                    goto exit;
                }

                result = diff(start, stop);
                num_ns = (uint64_t)result.tv_sec * (uint64_t)1e9 +
                    (uint64_t)result.tv_nsec;
                fprintf(fp, "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ".%03" PRIu64
                        "\n", prev_freq, curr_freq, num_ns / (uint64_t)1000,
                        num_ns % (uint64_t)1000);

                prev_freq = freq_list[i];

                // start streaming
                status = skiq_start_rx_streaming_multi_immediate( card,
                            &hdl, 1 );
                if( status != 0 )
                {
                    fprintf(stderr, "Error: receive streaming failed to start with status code %"
                            PRIi32 "\n", status);
                    goto exit;
                }

                // grab some data
                status = receive_data();
                if ( status != 0 )
                {
                    num_receive_errors++;
                }

                status = skiq_stop_rx_streaming_multi_immediate(card, &hdl, 1);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to stop RX streaming (result code %" PRIi32
                            ")\n", status);
                    goto exit;
                }
                curr_freq += step_size;
            }
            curr_freq = start_freq;
        }
    }
    else
    {
        // sweep the frequency range the # of times specified
        for(uint32_t curr_iteration=0;
             (curr_iteration <= repeat) && (running==true);
             curr_iteration++ )
        {
            while( (curr_freq <= stop_freq) && (running==true) )
            {
                clock_gettime(CLOCK_MONOTONIC, &start);
                status = skiq_write_rx_LO_freq(card, hdl, curr_freq);
                clock_gettime(CLOCK_MONOTONIC, &stop);

                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to change RX LO frequency to %"
                            PRIu64 " Hz (result code %" PRIi32 ")\n",
                            curr_freq, status);
                    goto exit;
                }

                //compute the time diff and write to file
                result = diff(start, stop);
                num_ns = (uint64_t)result.tv_sec * (uint64_t)1e9 +
                    (uint64_t)result.tv_nsec;
                fprintf(fp, "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ".%03" PRIu64
                        "\n", prev_freq, curr_freq, num_ns / (uint64_t)1000,
                        num_ns % (uint64_t)1000);

                prev_freq = curr_freq;

                // start streaming
                status = skiq_start_rx_streaming(card, hdl);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to start RX streaming (result code %" PRIi32
                            ")\n", status);
                    goto exit;
                }

                // grab some data
                status = receive_data();
                if ( status != 0 )
                {
                    num_receive_errors++;
                }

                // stop streaming to ensure we don't get any "stale" data after
                // retuning
                status = skiq_stop_rx_streaming(card, hdl);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to stop RX streaming (result code %" PRIi32
                            ")\n", status);
                    goto exit;
                }

                curr_freq += step_size;
            }
            curr_freq = start_freq;
        }

    }

exit:
    fclose(fp);
    fp = NULL;

    return (status);
}






int transmit_sweep(void)
{
    skiq_tx_hdl_t hdl;
    skiq_tx_hdl_t hdl_other = skiq_tx_hdl_end;
    skiq_chan_mode_t chan_mode = skiq_chan_mode_single;
    int32_t status = 0;
    uint64_t curr_freq = start_freq;
    struct timespec start, stop, result;
    uint64_t num_ns = 0;
    //uint64_t prev_freq = stop_freq;
    uint16_t block_size = 1020;
    skiq_tx_quadcal_mode_t tx_cal_mode;
    FILE* fp = NULL;
    int32_t dec_loc = 0;
    char tx_insert[3] = {'_','t','x'};
    uint16_t num_hop_freqs = (stop_freq-start_freq)/step_size + 1;
    uint64_t prev_freq = start_freq + (num_hop_freqs-1)*step_size;
    uint64_t freq_list [num_hop_freqs];
    uint64_t base_ts = 0;
    uint64_t curr_ts;

    //copy filename string into a buffer and append the _rx suffix at the end of
    //the filename and before the file extension (if it exists)
    memset(filename, 0, OUTPUT_PATH_MAX);
    strncpy(filename, p_file_path, OUTPUT_PATH_MAX-1);
    dec_loc = (strrchr(filename,'.') == NULL) ? strlen(filename) : (uint32_t)(strrchr(filename,'.')-filename);
    memmove (filename+dec_loc+3,filename+dec_loc,strlen(filename)-3);
    memcpy(&filename[dec_loc], &tx_insert, 3);

    //open the logfile
    fp = fopen( filename, "w" );
    if( fp == NULL )
    {
        fprintf(stderr, "Error: unable to open file %s to write to (error code"
                " %d: '%s')\n", filename, errno, strerror(errno));
        return -errno;
    }

    //write the meta data line to the beginning of the logfile
    fprintf(fp, "%s,%s,%" PRIu32",%" PRIu64 ",%" PRIu64 ",%"
            PRIu64 ",%d,%" PRIu32 ",%d\n", p_serial, p_hdl,
            sample_rate, start_freq, stop_freq, step_size, freq_hop, repeat,
            quadcal_auto);

    //map argument values to sidekiq specific variable values
    if( 0 == strcasecmp(p_hdl, "A1") )
    {
        hdl = skiq_tx_hdl_A1;
        printf("Info: using Tx handle A1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "A2") )
    {
        hdl = skiq_tx_hdl_A2;
        hdl_other = skiq_tx_hdl_A1;
        chan_mode = skiq_chan_mode_dual;
        block_size = DUAL_TX_BLOCKSIZE;
        printf("Info: using Tx handle A2\n");
    }
    else if( 0 == strcasecmp(p_hdl, "B1") )
    {
        hdl = skiq_tx_hdl_B1;
        printf("Info: using Tx handle B1\n");
    }
    else if( 0 == strcasecmp(p_hdl, "B2") )
    {
        hdl = skiq_tx_hdl_B2;
        hdl_other = skiq_tx_hdl_B1;
        chan_mode = skiq_chan_mode_dual;
        block_size = DUAL_TX_BLOCKSIZE;
        printf("Info: using Tx handle B2\n");
    }
    else
    {
        fprintf(stderr, "Error: invalid handle specified (%s)\n", p_hdl);
        goto exit;
    }

    //Configure tx_quadcal mode
    if( quadcal_auto )
    {
        tx_cal_mode = skiq_tx_quadcal_mode_auto;
    }
    else
    {
        tx_cal_mode = skiq_tx_quadcal_mode_manual;
    }

    // write the channel mode (dual if A2/B2 is being used)
    status = skiq_write_chan_mode(card, chan_mode);
    if ( status != 0 )
    {
        fprintf(stderr, "Error: failed to set Tx channel mode to %u with status %d\n",
            chan_mode, status);
        goto exit;
    }

    //Set the tx block size
    status = skiq_write_tx_block_size( card, hdl, block_size );
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to configure block size (result code %"
               PRIi32 ")\n", status);
        goto exit;
    }

    status = skiq_write_tx_sample_rate_and_bandwidth(card, hdl, sample_rate,
             sample_rate);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to write Tx sample rate (result code %" PRIi32
                ")\n", status);
        goto exit;
    }

    // initialize the start frequency
    curr_freq = start_freq;

    // tune to the "end" frequency to start without metrics
    status = skiq_write_tx_LO_freq(card, hdl, prev_freq);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to write Tx LO frequency (result code %" PRIi32
                ")\n", status);
        goto exit;
    }

    // enable the tone
    status = skiq_enable_tx_tone(card, hdl);
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to enable tone (result code %" PRIi32 ")\n",
                status);
        goto exit;
    }

    // enable the tone on the other associated handle when using dual channel
    //mode (i.e. other handle is valid)
    if ( hdl_other != skiq_tx_hdl_end )
    {
        status = skiq_enable_tx_tone(card, hdl);
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to enable tone on other hdl (result code %" PRIi32
                    ")\n", status);
            goto exit;
        }
    }

    // configure the TX quadcal mode
    status = skiq_write_tx_quadcal_mode( card, hdl, tx_cal_mode );
    if( status != 0 )
    {
        fprintf(stderr, "Error: unable to disable TX quad cal (result code %" PRIi32 ")\n", status);
        goto exit;
    }

    // set the transmit quadcal mode on the other associated handle when using
    //dual channel mode (i.e. other handle is valid )
    if ( hdl_other != skiq_tx_hdl_end )
    {
        status = skiq_write_tx_quadcal_mode( card, hdl_other, tx_cal_mode );
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to configure TX quadcal mode status=%" PRIu32 "\n",
                    status);
            goto exit;
        }
    }

    // tune to the "end" frequency to start without metrics
    skiq_write_tx_LO_freq(card, hdl, prev_freq);

    printf("Info: starting TX sweep\n");


    if(freq_hop)
    {
        //create frequency hopping list
        uint64_t freq_val = start_freq;

        if (num_hop_freqs > SKIQ_MAX_NUM_FREQ_HOPS)
        {
            fprintf(stderr, "Error: too many freq hops. Requested %d, but max is %d\n",
                    num_hop_freqs, SKIQ_MAX_NUM_FREQ_HOPS);
            goto exit;
        }

        for (uint32_t i = 0; i < num_hop_freqs; i++)
        {
            freq_list[i] = freq_val;
            freq_val += step_size;
        }
        prev_freq = freq_list[num_hop_freqs-1];

        // configure frequency hopping
        if( (status=skiq_write_tx_freq_tune_mode( card, hdl,
            skiq_freq_tune_mode_hop_immediate )) != 0 )
        {
            fprintf(stderr, "Error: failed to set tune mode status=%d\n", status);
            goto exit;
        }

        // configure the hopping list
        status = skiq_write_tx_freq_hop_list( card, hdl, num_hop_freqs, freq_list, 0 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: failed to set hop list %d\n", status);
            goto exit;
        }

        // tune to the "end" frequency to start without metrics
        status = skiq_write_next_tx_freq_hop( card, hdl, (num_hop_freqs -1) );
        status = skiq_perform_tx_freq_hop( card, hdl, 0 );
        if( status != 0 )
        {
            fprintf(stderr, "Error: failed to write hop with status %d\n", status);
            goto exit;
        }

        // reset the timestamps and wait for the reset to complete
        if( skiq_read_curr_rx_timestamp( card, hdl, &base_ts ) == 0 )
        {
            printf("Resetting async\n");
            if( skiq_reset_timestamps( card ) != 0 )
            {
                fprintf(stderr, "Error: unable to reset timestamps\n");
            }

            if( skiq_read_curr_rx_timestamp( card, hdl, &curr_ts ) != 0 )
            {
                fprintf(stderr, "Error: unable to read current timestamp\n");
            }
        }

        while( (base_ts < curr_ts) && (running==true) )
        {
            // just sleep 100 uS and read the timestamp again...we should be
            //close based on the initial sleep calculation
            usleep(100);
            skiq_read_curr_tx_timestamp(card, hdl, &curr_ts);
        }

        // start streaming
        status = skiq_start_tx_streaming(card, hdl);
        if( status != 0 )
        {
            fprintf(stderr, "Error: unable to start streaming (result code %" PRIi32 ")\n", status);
            goto exit;
        }

        //transmit for 1ms
        usleep(1000);

        // stop streaming to ensure we don't get any "stale" data after
        // retuning
        status = skiq_stop_tx_streaming(card, hdl);
        if ( status != 0 )
        {
            fprintf(stderr, "Error: failed to stop TX streaming (result code %" PRIi32 ")\n",
                    status);
            goto exit;
        }

        for(uint32_t curr_iteration=0;
             (curr_iteration <= repeat) && (running==true);
             curr_iteration++ )
        {
            // receive data for each frequency
            for(int32_t i = 0; ((i < num_hop_freqs) && (running == true)); i++ )
            {

                int32_t status_temp = 0;

                clock_gettime(CLOCK_MONOTONIC, &start);
                status_temp = skiq_write_next_tx_freq_hop( card, hdl, i );
                status = skiq_perform_tx_freq_hop( card, hdl, 0 );
                clock_gettime(CLOCK_MONOTONIC, &stop);

                if(( status_temp != 0 ) || ( status != 0 ))
                {
                    fprintf(stderr, "Error: failed to write hop with status %d\n", status);
                    goto exit;
                }

                result = diff(start, stop);
                num_ns = (uint64_t)result.tv_sec * (uint64_t)1e9 +
                            (uint64_t)result.tv_nsec;
                fprintf(fp, "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ".%03" PRIu64
                        "\n", prev_freq, curr_freq, num_ns / (uint64_t)1000,
                        num_ns % (uint64_t)1000);

                prev_freq = freq_list[i];

                // start streaming
                status = skiq_start_tx_streaming(card, hdl);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to start RX streaming (result code %" PRIi32
                            ")\n", status);
                    goto exit;
                }

                //transmit for 1ms
                usleep(1000);

                // stop streaming to ensure we don't get any "stale" data after
                // retuning
                status = skiq_stop_tx_streaming(card, hdl);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to stop TX streaming (result code %"
                            PRIi32 ")\n", status);
                    goto exit;
                }
                curr_freq += step_size;
            }
            curr_freq = start_freq;
        }
    }
    else
    {
        // sweep the frequency range the # of times specified
        for(uint32_t curr_iteration=0;
             (curr_iteration <= repeat) && (running==true);
             curr_iteration++ )
        {
            while( (curr_freq <= stop_freq) && (running==true) )
            {
                clock_gettime(CLOCK_MONOTONIC, &start);
                status = skiq_write_tx_LO_freq(card, hdl, curr_freq);
                clock_gettime(CLOCK_MONOTONIC, &stop);

                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to change tX LO frequency to %"
                        PRIu64 " Hz (result code %" PRIi32 ")\n", curr_freq,
                        status);
                    goto exit;
                }

                result = diff(start, stop);
                num_ns = (uint64_t)result.tv_nsec;

                fprintf(fp, "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ".%03" PRIu64
                        "\n", prev_freq, curr_freq, num_ns / (uint64_t)1000,
                        num_ns % (uint64_t)1000);
                prev_freq = curr_freq;

                // start streaming
                status = skiq_start_tx_streaming(card, hdl);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to start TX streaming (result code %" PRIi32
                            ")\n", status);
                    goto exit;
                }

                //transmit for 1ms
                usleep(1000);

                // stop streaming to ensure we don't get any "stale" data after
                // retuning
                status = skiq_stop_tx_streaming(card, hdl);
                if ( status != 0 )
                {
                    fprintf(stderr, "Error: failed to stop TX streaming (result code %" PRIi32
                            ")\n", status);
                    goto exit;
                }

                curr_freq += step_size;
            }
            curr_freq = start_freq;
        }
    }

exit:
    fclose(fp);
    fp = NULL;

    return status;
}


/*****************************************************************************/
/** This function receives data and verifies the timestamp increment.

    @return int-0 if received successfully, negative if there was an error
*/
static int32_t receive_data(void)
{
    uint32_t num_rx_blocks = NUM_BLOCKS;
    uint32_t curr_num_blocks=0;
    skiq_rx_hdl_t hdl;
    uint32_t len=0;
    uint64_t curr_ts=0;
    uint64_t next_ts=0;
    int32_t status=0;
    skiq_rx_status_t rx_status;
    skiq_rx_block_t *p_rx_block;
    uint32_t ts_offset = SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS - SKIQ_RX_HEADER_SIZE_IN_WORDS;

    // receive the number of blocks requested per iteration
    while( (curr_num_blocks<num_rx_blocks) && (running==true) )
    {
        rx_status = skiq_receive(card, &hdl, &p_rx_block, &len);
        if ( rx_status == skiq_rx_status_success )
        {
            // Ensure that the handle is correct
            if( hdl != skiq_rx_hdl_A1 )
            {
                fprintf(stderr, "Error: invalid handle %u returned\n", (int) hdl);
                dump_packet_contents(curr_num_blocks, (uint8_t*)p_rx_block, 60);
                clean_exit();
                _exit(-1);
            }

            // Ensure the correct length was returned
            if( len != SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES )
            {
                fprintf(stderr, "Error: wrong data length of %u received"
                        " (expected %" PRIu32 ")\n", len,
                        (uint32_t) SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES);
                dump_packet_contents(curr_num_blocks, (uint8_t*)p_rx_block, 60);
                clean_exit();
                _exit(-2);
            }

            // Ensure that the timestamp is non-zero
            curr_ts = p_rx_block->rf_timestamp;

            if( curr_ts == 0 )
            {
                fprintf(stderr, "Error: rx timestamp is 0 (%" PRIu64 ", %" PRIu64 ")\n",
                        p_rx_block->rf_timestamp, curr_ts);
                dump_packet_contents(curr_num_blocks, (uint8_t*)p_rx_block, 60);
                clean_exit();
                _exit(-3);
            }

            // look at the RF pair timestamp to make sure we didn't drop data
            if( (curr_num_blocks != 0) && (curr_ts != next_ts) )
            {
                fprintf(stderr, "Error: timestamp error in block %d....expected"
                        " 0x%016" PRIx64 " but got 0x%016" PRIx64
                        "\n", curr_num_blocks, next_ts, curr_ts);
                status = -1;
                return (status);
            }
            next_ts = curr_ts + ts_offset;
            curr_num_blocks++;
        }
        else if ( rx_status == skiq_rx_status_error_overrun )
        {
            printf("Warning: overrun detected on block %" PRIu32 " of %"
                    PRIu32 " (result code %" PRIi32 "); continuing.\n",
                    curr_num_blocks, num_rx_blocks, (int32_t) rx_status);
        }
        else if ( rx_status == skiq_rx_status_error_generic )
        {
            printf("Warning: possible RX error detected on block %" PRIu32
                    " of %" PRIu32 " (result code %" PRIi32 "); continuing.\n",
                    curr_num_blocks, num_rx_blocks, (int32_t) rx_status);
        }
        else if ( rx_status == skiq_rx_status_error_packet_malformed )
        {
            fprintf(stderr, "Error: failed to receive data on block %" PRIu32 " of %"
                    PRIu32 " (result code %" PRIi32 ")\n", curr_num_blocks,
                    num_rx_blocks, (int32_t) rx_status);
            clean_exit();
            _exit(-4);
        }
        else if ( ( rx_status != skiq_rx_status_no_data ) &&
                ( rx_status != skiq_rx_status_error_not_streaming ) )
        {
            printf("Warning: unknown error detected on block %" PRIu32
                    " of %" PRIu32 " (result code %" PRIi32 "); continuing.\n",
                    curr_num_blocks, num_rx_blocks, (int32_t) rx_status);
        }
    }

    return (status);
}

/*****************************************************************************/
/** This function dumps the contents of the packets.  This is useful for
    debugging an unexpected error in packet received.

    @param block_num number of block where error occurred
    @param p_data pointer to data to dump
    @param num_to_dump number of bytes to dump
    @return int-0 if received successfully, negative if there was an error
*/
static void dump_packet_contents( uint32_t block_num,
                           uint8_t *p_data,
                           uint32_t num_to_dump )
{
    uint32_t i=0;
    fprintf(stderr, "Error: at block %u, dumping packet\n", block_num);
    for( i=0; i<num_to_dump;  )
    {
        printf("@%u: 0x%x 0x%x 0x%x 0x%x\n",
               i, p_data[i], p_data[i+1], p_data[i+2], p_data[i+3]);
        i = i+4;
    }
}

void clean_exit(void)
{
    skiq_exit();
}

struct timespec diff(struct timespec start, struct timespec end)
{
        struct timespec temp;
        if ((end.tv_nsec - start.tv_nsec) < 0)
        {
                temp.tv_sec = end.tv_sec - start.tv_sec - 1;
                temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
        }
        else
        {
                temp.tv_sec = end.tv_sec - start.tv_sec;
                temp.tv_nsec = end.tv_nsec - start.tv_nsec;
        }
        return temp;
}
