/*! \file read_modules.c
 * \brief This file contains a basic application that reads the
 *        version number of modules (if they are loaded).
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <sidekiq_api.h>
#include <sidekiq_private.h>
#include <sidekiq_fpga_ctrl.h>
#include <dma_interface_api.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifndef __MINGW32__
#include "pci_manager.h"
#include <sys/ioctl.h>
#endif /* __MINGW32__ */

#include <arg_parser.h>

#ifndef DEFAULT_BUFFER_SIZE
#   define DEFAULT_BUFFER_SIZE      (4096)
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- read module information";
static const char* p_help_long = "\
Displays information about the DMA and PCI modules (if loaded); if a card ID\n\
or serial number is specified, information about the FPGA capabilities and\n\
USB availability is also displayed.\n\
";

/* command line argument variables */
static uint8_t cards[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = UINT8_MAX,
};
static uint8_t num_cards = 1;
static char* p_serial = NULL;
static bool pci_check = false;
static bool dma_check = false;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_OPT("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &(cards[0]),
                UINT8_VAR_TYPE),
    APP_ARG_OPT("serial",
                'S',
                "Specify Sidekiq by serial number",
                "SERNUM",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_OPT("pci",
                0,
                "Display information about the PCI Manager",
                NULL,
                &pci_check,
                BOOL_VAR_TYPE),
    APP_ARG_OPT("dma",
                0,
                "Display information about the DMA Driver",
                NULL,
                &dma_check,
                BOOL_VAR_TYPE),
    APP_ARG_TERMINATOR,
};


/*
    @brief  Lookup up a card by serial, if specified, and verified the given
            card ID is valid.

    @param[in]  serial      The serial number string to lookup; this should
                            not be NULL.
    @param[out] p_card      If not NULL, the card number of the card with the
                            given serial number.

    @return 0 on success, else an error code.
*/
static int32_t
lookupCard(const char *serial, uint8_t *p_card)
{
    int32_t status = -1;
    static bool initialized = false;
    uint8_t localCard = UINT8_MAX;

    if (initialized)
    {
        return 0;
    }

    if (NULL != serial)
    {
        status = skiq_get_card_from_serial_string((char *) serial, &localCard);
        if (0 != status)
        {
            fprintf(stderr, "Error: cannot find card with serial number %s"
                    " (result code %" PRIi32 ")\n", serial, status);
            status = -1;
            goto finished;
        }

        printf("Info: found serial number %s as card ID %" PRIu8 "\n",
                serial, localCard);
        initialized = true;
    }

    if ( (SKIQ_MAX_NUM_CARDS - 1) < localCard )
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card"
                " ID (%" PRIu8 ")\n", localCard, (SKIQ_MAX_NUM_CARDS - 1));
        status = -1;
        goto finished;
    }

    if ( NULL != p_card )
    {
        *p_card = localCard;
    }

    status = 0;

finished:
    return status;
}

#ifdef __MINGW32__

static bool
getDmaModuleInfo(bool *isLoaded, char *version, uint32_t versionLen)
{
    (void) isLoaded;
    (void) version;
    (void) versionLen;

    return false;
}

static bool
getPciModuleInfo(bool *isLoaded, char *version, uint32_t versionLen)
{
    (void) isLoaded;
    (void) version;
    (void) versionLen;

    return false;
}

#else

static bool
getDmaDriverVersion(uint8_t cardId, bool *isLoaded, char *version,
        uint32_t versionLen)
{
    int32_t result = 0;
    uint8_t major = 0;
    uint8_t minor = 0;
    uint8_t subminor = 0;
    bool loaded = false;
    bool foundVersion = false;

    result = DmaInterface_open(cardId);
    if (DMA_INTERFACE_STATUS_SUCCESSFUL != result)
    {
        fprintf(stderr, "Error: failed to open DMA Driver interface (result"
                " code %" PRIi32 ")\n", result);
    }
    else
    {
        loaded = true;

        result = DmaInterfaceDriverVers(cardId, &major, &minor, &subminor);
        if (DMA_INTERFACE_STATUS_SUCCESSFUL != result)
        {
            fprintf(stderr, "Error: failed to get DMA Driver version"
                    " information (result code %" PRIi32 ")\n", result);
        }
        else
        {
            foundVersion = true;
        }

        result = DmaInterface_close(cardId);
        if (DMA_INTERFACE_STATUS_SUCCESSFUL != result)
        {
            fprintf(stderr, "Warn: failed to close DMA Driver interface (result"
                    " code %" PRIi32 ")\n", result);
        }
    }

    if (NULL != isLoaded)
    {
        *isLoaded = loaded;
    }

    if (NULL != version)
    {
        if (foundVersion)
        {
            snprintf(version, (size_t) versionLen, "%u.%u.%u",
                    major, minor, subminor);
        }
        else
        {
            snprintf(version, (size_t) versionLen, "unknown");
        }
    }

    return true;
}

/**
    @brief  Retrieve information about a kernel module.

    @param[in]  moduleName      The name of the kernel module; should not be
                                NULL.
    @param[out] isLoaded        True if the module is currently loaded, else
                                false; if NULL, this value will not be written.
                                Valid only if the function succeeds.
    @param[out] version         The module's version string; if NULL, this value
                                will not be written. Valid only if the function
                                succeeds.
    @param[in]  versionLen      The length of @ref version in bytes.

    @return true if the module information could be read, else false.
*/
static bool
getModuleInfo(const char *moduleName, bool *isLoaded, char *version,
        uint32_t versionLen)
{
    bool success = false;
    int result = 0;
    size_t toCopy = 0;
    char name[DEFAULT_BUFFER_SIZE] = { '\0' };
    char readBuffer[DEFAULT_BUFFER_SIZE] = { '\0' };
    size_t readCount = 0;
    FILE *file = NULL;
    struct stat dirInfo;
    bool loaded = false;

    snprintf(name, DEFAULT_BUFFER_SIZE, "/sys/module/%s", moduleName);

    errno = 0;
    result = stat((const char *) name, &dirInfo);
    if (0 != result)
    {
        fprintf(stderr, "Error: failed to open module %s directory"
                " (%d: '%s')\n", moduleName, errno, strerror(errno));
        return false;
    }
    if (S_ISDIR(dirInfo.st_mode))
    {
        loaded = true;
    }

    if (loaded)
    {
        snprintf(name, DEFAULT_BUFFER_SIZE, "/sys/module/%s/version",
                moduleName);

        errno = 0;
        file = fopen(name, "rb");
        if (NULL == file)
        {
            fprintf(stderr, "Error: failed to open module %s version file"
                    " (%d: '%s')\n", moduleName, errno, strerror(errno));
            loaded = false;
        }
        else
        {
            errno = 0;
            readCount = fread(readBuffer, sizeof(char), DEFAULT_BUFFER_SIZE,
                            file);
            result = errno;
            if (0 != ferror(file))
            {
                fprintf(stderr, "Error: failed to read from module %s version"
                        " file (%d: '%s')\n", moduleName, errno,
                        strerror(errno));
                loaded = false;
            }

            fclose(file);
            file = NULL;

            success = true;
        }
    }

    if (success)
    {
        if (NULL != isLoaded)
        {
            *isLoaded = loaded;
        }
        if ((NULL != version) && (loaded))
        {
            if (readCount > (size_t) versionLen)
            {
                toCopy = (size_t) versionLen;
            }
            else
            {
                toCopy = readCount;
            }

            memcpy((void *) version, (const void *) readBuffer, toCopy);
            if ((0 < toCopy) &&
                (('\r' == version[toCopy - 1]) ||
                 ('\n' == version[toCopy - 1])))
            {
                version[toCopy - 1] = '\0';
            }
        }
    }

    return success;
}

/**
    @brief  Get information about the DMA driver.

    @param[out] isLoaded    Flag to indicate if the module is loaded; if NULL,
                            this value will not be written.
    @param[out] version     The DMA version string; if NULL, this value will not
                            be written.
    @param[in]  versionLen  The length of @ref version in bytes.

    @note   This function attempts to get information about the module via
            sysfs; if that fails, it attempts to use the DMA Driver interface
            to get the information.

    @return true if the information could be queried, else false.
*/
static bool
getDmaModuleInfo(bool *isLoaded, char *version, uint32_t versionLen)
{
    bool success = getModuleInfo("dmadriver", isLoaded, version, versionLen);

    if (!success)
    {
        success = getDmaDriverVersion(0, isLoaded, version, versionLen);
    }

    return success;
}

/**
    @brief  Get information about the PCI Manager.

    @param[out] isLoaded    Flag to indicate if the module is loaded; if NULL,
                            this value will not be written.
    @param[out] version     The PCI Manager version string; if NULL, this value
                            will not be written.
    @param[in]  versionLen  The length of @ref version in bytes.

    @return true if the information could be queried, else false.
*/
static bool
getPciModuleInfo(bool *isLoaded, char *version, uint32_t versionLen)
{
    bool success = false;
    int fd = -1;
    int result = 0;

    pci_manager_vers_t versionInfo = {0, 0, 0};
    bool loaded = false;

    errno = 0;
    fd = open(PCI_MANAGER_FULLPATH, O_RDWR);
    if (-1 == fd)
    {
        fprintf(stderr, "Error: failed to open PCI Manager path (%d: '%s')\n",
                errno, strerror(errno));
        return false;
    }

    errno = 0;
    result = ioctl(fd, PCI_MANAGER_VERSION, &versionInfo);
    if (-1 == result)
    {
        fprintf(stderr, "Error: failed to query PCI Manager (%d: '%s')\n",
                errno, strerror(errno));
    }
    else
    {
        loaded = true;
        success = true;
    }

    close(fd);
    fd = -1;

    if (success)
    {
        if (NULL != isLoaded)
        {
            *isLoaded = loaded;
        }
        if ((NULL != version) && (loaded))
        {
            snprintf(version, versionLen, "%u.%u.%u", versionInfo.maj_vers,
                    versionInfo.min_vers, versionInfo.patch_vers);
        }
    }

    return success;
}

#endif /* ifdef __MINGW32__ */

/**
    @brief  Query if the USB transport is available on a card.

    @param[out]  isAvailable    Flag to indicate if the card has USB available;
                                if NULL, this value will not be written. Valid
                                only if the function succeeds.

    @return 0 on success, else a negative error code.
*/
static int32_t
isUsbAvailable(const uint8_t card, bool *isAvailable)
{
    int32_t status = 0;
    bool available = false;

    status = skiq_is_xport_avail(card, skiq_xport_type_usb);
    available = (0 == status);

    if (NULL != isAvailable)
    {
        *isAvailable = available;
    }

    return 0;
}


/**
    @brief  Verify if a part's FPGA has the "self reload" capability.

    @param[in]  part        The Sidekiq part number.
    @param[in]  fpgaCaps    The list of previously gathered FPGA capabilities.
    @param[in]  pMajor      The major version number of the FPGA bitstream.
    @param[in]  pMinor      The minor version number of the FPGA bitstream.

    @todo   This is taken from sidekiq_flash.c where it's a private function;
            if that function was made more public this copy should go away.

    @return true if the part has the ability to self-reload the FPGA, else
            false.
*/
static bool
part_has_self_reload(skiq_part_t part, struct fpga_capabilities fpgaCaps,
        uint8_t pMajor, uint8_t pMinor)
{
    bool has_self_reload = false;

    if ((pMajor >= 3) && (pMinor >= 8))
    {
        has_self_reload = fpgaCaps.self_reload > 0;
    }
    else
    {
        switch (part)
        {
        case skiq_m2:
            has_self_reload = ((pMajor > 3) && (pMinor > 6));
            break;
        case skiq_mpcie:
            has_self_reload = false;
            break;
        case skiq_x2:
            has_self_reload = true;
            break;
        default:
            has_self_reload = false;
            break;
        }
    }

    return has_self_reload;
}


/**
    @brief  Check the FPGA's capabilities.

    @param[out] canSelfReload       Flag to indicate if the FPGA is capable
                                    of self-reload. If NULL this value will not
                                    be written. Valid only if the function
                                    succeeds.

    @return 0 on success, else an error code.
*/
static int32_t
checkFpgaCapabilities(uint8_t card, bool *canSelfReload)
{
    int32_t status = -1;
    skiq_xport_type_t type = skiq_xport_type_auto;
    skiq_xport_init_level_t level = skiq_xport_init_level_basic;
    pid_t owner = 0;
    bool skiq_initialized = false;

    bool reload = false;

    struct fpga_capabilities fpgaCaps;
    skiq_part_t skiqPart;
    uint8_t pMajor = 0;
    uint8_t pMinor = 0;
    uint8_t pPatch = 0;

    /* initialize Sidekiq */
    status = skiq_init(type, level, &card, 1);
    if( status != 0 )
    {
        if ( ( EBUSY == status ) &&
             ( 0 != skiq_is_card_avail( card, &owner ) ) )
        {
            fprintf(stderr, "Error: card %" PRIu8 " is already in use (by"
                    " process ID %u); cannot initialize card.\n", card,
                    (unsigned int) owner);
            status = -1;
        }
        else if ( -EINVAL == status )
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq; was a"
                    " valid card specified? (result code %" PRIi32 ")\n",
                    status);
            status = -2;
        }
        else
        {
            fprintf(stderr, "Error: unable to initialize libsidekiq with"
                    " status %d\n", status);
            status = -2;
        }
        goto finished;
    }
    skiq_initialized = true;

    status = fpga_ctrl_read_capabilities(card, &fpgaCaps);
    if (0 != status)
    {
        fprintf(stderr, "Error: failed to read capabilities from FPGA"
                " (result code %" PRIi32 ")\n", status);
        goto finished;
    }
    skiqPart = _skiq_get_part(card);
    status = skiq_read_fpga_semantic_version(card, &pMajor, &pMinor, &pPatch);
    if (0 != status)
    {
        fprintf(stderr, "Error: failed to read information from FPGA"
                " (result code %" PRIi32 ")\n", status);
        goto finished;
    }

    reload = part_has_self_reload(skiqPart, fpgaCaps, pMajor, pMinor);
    status = 0;

    if ((0 == status) && (NULL != canSelfReload))
    {
        *canSelfReload = reload;
    }

finished:
    if (skiq_initialized)
    {
        skiq_exit();
        skiq_initialized = false;
    }

    return status;
}


/******************************************************************************/
/** This is the main function.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ASCII string arguments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[] )
{
    int32_t status = 0;

    bool success = false;
    bool isLoaded = false;
    char buffer[DEFAULT_BUFFER_SIZE];
    bool canSelfReload = false;
    bool usbPresent = false;
    uint8_t i = 0;

    if( 0 != arg_parser(argc, argv, p_help_short, p_help_long, p_args) )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return -1;
    }

    if ( (UINT8_MAX != cards[0]) && (NULL != p_serial) )
    {
        printf("Error: must specify EITHER card ID or serial number, not"
                " both\n");
        return -1;
    }

    if ( UINT8_MAX != cards[0] )
    {
        if ( (SKIQ_MAX_NUM_CARDS - 1) < cards[0] )
        {
            fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card"
                    " ID (%" PRIu8 ")\n", cards[0], (SKIQ_MAX_NUM_CARDS - 1));
            return -1;
        }
    }

    if (pci_check)
    {
        success = getPciModuleInfo(&isLoaded, &(buffer[0]),
                    DEFAULT_BUFFER_SIZE);
        if (!success)
        {
            printf("*** %16s: loaded: %s version: %s\n",
                    "PCI Manager", "unknown", "unknown");
        }
        else
        {
            printf("*** %16s: loaded: %s version: %s\n",
                    "PCI Manager",
                    (isLoaded) ? "yes" : "no",
                    (isLoaded) ? buffer : "none");
        }
    }

    if (dma_check)
    {
        success = getDmaModuleInfo(&isLoaded, &(buffer[0]),
                    DEFAULT_BUFFER_SIZE);
        if (!success)
        {
            fprintf(stderr, "Error: failed to get information on DMA module\n");
            printf("*** %16s: loaded: %s version: %s\n",
                    "DMA Driver", "unknown", "unknown");
        }
        else
        {
            printf("*** %16s: loaded: %s version: %s\n",
                    "DMA Driver",
                    (isLoaded) ? "yes" : "no",
                    (isLoaded) ? buffer : "none");
        }
    }

    status = 0;

    if ( (UINT8_MAX == cards[0]) && (NULL == p_serial) )
    {
        skiq_get_cards( skiq_xport_type_auto, &num_cards, cards );
    }
    else if ( NULL != p_serial )
    {
        status = lookupCard(p_serial, &(cards[0]));
        if (0 != status)
        {
            return status;
        }

        num_cards = 1;
    }

    printf("Checking %" PRIu8 " card(s)...\n", num_cards);

    for (i = 0; i < num_cards; i++)
    {
        snprintf(buffer, DEFAULT_BUFFER_SIZE, "Card %u", cards[i]);
        status = checkFpgaCapabilities(cards[i], &canSelfReload);
        if (0 != status)
        {
            fprintf(stderr, "Error: failed to get capabilities from FPGA on"
                    " card %" PRIu8 " (result code %" PRIi32 ")\n", cards[i],
                    status);
            printf("*** %16s: FPGA can self reload: %s\n",
                    buffer, "unknown");
        }
        else
        {
            printf("*** %16s: FPGA can self reload: %s\n",
                    buffer, (canSelfReload) ? "yes" : "no");
        }

        if (-2 != status)
        {
            status = isUsbAvailable(cards[i], &usbPresent);
            if (0 != status)
            {
                fprintf(stderr, "Error: failed to get USB state for card %"
                        PRIu8 " (result code %" PRIi32 ")\n", cards[i], status);
            }
            else
            {
                 printf("*** %16s: USB present: %s\n",
                        buffer, (usbPresent) ? "yes" : "no");
            }
        }
        if (0 != status)
        {
            printf("*** %16s: USB present: unknown\n", buffer);
        }
    }

    return ((int) status);
}

