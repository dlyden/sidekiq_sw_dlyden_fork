/*! \file warp_voltage_stamp.c
 * Test application that allows read/write access to the warp voltage
 * value stored in EEPROM.
 *  
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static char* app_name;

/* local functions */
static void print_usage(void);
static const char* usage_string = \
"   Reads the default warp voltage. Optionally, can read back the user warp\n\
    voltage value and/or store a new default warp voltage value.\n";

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
void print_usage(void)
{
    printf("Usage: %s <card> [print user warp] [int16_t value]\n", app_name);
    printf("%s\n", usage_string);
}

int main( int argc, char *argv[] )
{
    int32_t status = 0;
    uint16_t warp_voltage = 0;
    uint8_t card;
    bool b_write_val = false;
    bool b_read_user = false;
    app_name = argv[0];

    // make sure the appropriate # parameters were provided
    if( (argc < 2) || (argc > 4) )
    {
	    printf("Error: invalid # arguments provided\n");
	    print_usage();
	    return (-1);
    }
    if( (argc > 2) && ('1' == argv[2][0]) )
    {
        b_read_user = true;
    }
    if( argc > 3 )
    {
        b_write_val = true;
    }
    
    card = atoi(argv[1]);
    if( b_write_val == true )
    {
        warp_voltage = atoi(argv[3]);
        printf("Info: configuring for pcie\n");
    }
    status = skiq_init( skiq_xport_type_pcie, skiq_xport_init_level_basic, &card, 1 );
    if( status != 0 )
    {
        printf("Error: unable to initialize libsidekiq with status %d\n", status);
        return (-1);
    }

    if( b_write_val == true )
    {
        status = skiq_fact_write_default_tcvcxo_warp_voltage(card,
                                                             warp_voltage);
        if( status != 0 )
        {
            printf("Error: card_read_warp_voltage_calibration failed with status %d\n", 
                   status);
            return (-1);
        }
    }
    warp_voltage = 0x0000;
    
    status = skiq_read_default_tcvcxo_warp_voltage(card, &warp_voltage);
    if( status != 0 )
    {
        printf("Error: skiq_read_default_tcvcxo_warp_voltage failed with status %d\n", 
               status);
        return (-1);
    }
    
    printf("Info: default warp voltage %d\n", warp_voltage);
    
    if( b_read_user == true )
    {
        warp_voltage = 0x0000;
        status = skiq_read_user_tcvcxo_warp_voltage(card, &warp_voltage);
        if( status != 0 )
        {
            printf("Error: skiq_read_user_tcvcxo_warp_voltage failed with status %d\n", 
                   status);
            return (-1);
        }
        else {
            printf("Info: user warp voltage %d\n", warp_voltage);
        }
    }
    
    skiq_exit();
    
    return 0;
}
