#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <stdio.h>
#include <unistd.h>

int main( int argc, char *argv[] )
{
    uint8_t card=0;
    char *p_string;

skiq_reinit:    
    printf("skiq_exit\n");
    skiq_exit();

    printf("destroy\n");
    skiq_fact_card_mgr_destroy();

    printf("skiq_exit\n");
    skiq_exit();

    skiq_read_serial_string( card, &p_string );
    printf("got serial %s\n", p_string);

    printf("sleeping...\n");
    sleep(10);
    printf("sleep done\n");

    goto skiq_reinit;
    
    
    return (0);
}
