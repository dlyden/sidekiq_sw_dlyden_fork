/*! \file part_info.c
 * \brief This file contains a basic application that configures
 * the part info of the Sidekiq.
 *  
 * <pre>
 * Copyright 2013-2023 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <sidekiq_hal.h>
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>

#include "hardware.h"
#include "card_services.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

#include <arg_parser.h>

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#ifndef STR_MAX_LENGTH
#   define  STR_MAX_LENGTH     (4096)
#endif

/* these are used to provide help strings for the application when running it
   with either the "-h" or "--help" flags */
static const char* p_help_short = "- writes part info and hardware interface version to EEPROM";
static const char* p_help_long = "\
Writes a combination of part info and hardware interface version to EEPROM\n\
on the specified card. Any information that is not provided, will cause a\n\
failure.";

/* command line argument variables */
uint8_t card = UINT8_MAX;
char *p_part_num = NULL;
char *p_revision = NULL;
char *p_variant = NULL;
char *p_serial = NULL;
uint8_t hw_iface_minor;
uint8_t hw_iface_major;

/* the command line arguments available to this application */
static struct application_argument p_args[] =
{
    APP_ARG_REQ("card",
                'c',
                "Specify Sidekiq by card index",
                "ID",
                &card,
                UINT8_VAR_TYPE),
    APP_ARG_REQ("part-num",
                'p',
                "Part number string",
                "PN",
                &p_part_num,
                STRING_VAR_TYPE),
    APP_ARG_REQ("rev",
                'r',
                "Revision string",
                "REV",
                &p_revision,
                STRING_VAR_TYPE),
    APP_ARG_REQ("variant",
                'v',
                "Variant string",
                "VARIANT",
                &p_variant,
                STRING_VAR_TYPE),
    APP_ARG_REQ("serial",
                's',
                "Serial string",
                "SERIAL",
                &p_serial,
                STRING_VAR_TYPE),
    APP_ARG_REQ("hw-iface-minor",
                0,
                "Hardware interface minor version",
                "HIMiV",
                &hw_iface_minor,
                UINT8_VAR_TYPE),
    APP_ARG_REQ("hw-iface-major",
                0,
                "Hardware interface major version",
                "HIMaV",
                &hw_iface_major,
                UINT8_VAR_TYPE),
    APP_ARG_TERMINATOR,
};

int main(int argc, char *argv[])
{
    int32_t status=0;
    pid_t owner=0;

    /* initialize everything based on the arguments provided */
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if(status != 0)
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return status;
    }

    if((SKIQ_MAX_NUM_CARDS - 1) < card)
    {
        fprintf(stderr, "Error: card ID %" PRIu8 " exceeds the maximum card ID (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
        return -ERANGE;
    }

    status = skiq_is_card_avail(card, &owner);
    if(status != 0)
    {
        printf("Warning: card %u is currently locked by another process"
                    " (PID=%u)\n", card, (unsigned int)(owner));
        return status;
    }

    status = skiq_fact_init_for_config(card);
    if(status != 0)
    {

        fprintf(stderr, "Error: unable to initialize libsidekiq for factory config on card %" PRIu8 " (status=%d)\n", card, status);
        return status;
    }

    status = skiq_fact_write_part_info(card, p_part_num, p_revision, p_variant, p_serial);
    if(status == 0)
    {
        printf("Succesfully wrote part info and serial string\n");
    }
    else
    {
        fprintf(stderr, "Error: saving part info failed on card %" PRIu8 " (status=%d)\n", card, status);
        return status;
    }

    status = skiq_fact_write_hw_iface_vers(card, hw_iface_major, hw_iface_minor, p_part_num, p_revision);
    if(status == 0)
    {
        printf("Succesfully wrote hardware interface version\n");
    }
    else
    {
        fprintf(stderr, "Error: saving hardware interface version failed on card %" PRIu8 " info (status=%d)\n", card, status);
        return status;
    }

    status = skiq_fact_exit_for_config(card);
    if(status != 0)
    {
        fprintf(stderr, "Error: unable to exit factory config on card %" PRIu8 " (status=%d)\n", card, status);
    }

    return status;
}
