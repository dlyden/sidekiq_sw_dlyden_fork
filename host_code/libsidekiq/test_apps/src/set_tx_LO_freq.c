/*! \file set_tx_LO_freq.c
 * \brief This file contains a basic application for tuning the
 * Tx interface to the requested LO freq.
 *
 * <pre>
 * Copyright 2013 - 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <sidekiq_api.h>

static char* app_name;

static void print_usage(void);


/*****************************************************************************/
/** This is the main function for executing set_tx_LO_freq commands.

    @param argc-the # of arguments from the cmd line
    @param argv-a vector of ascii string aruments from the cmd line
    @return int-indicating status
*/
int main( int argc, char *argv[])
{
    int32_t status;
    int result=0;
    uint64_t lo_freq;
    uint32_t hdl;
    double read_freq;
    uint8_t card=0;
    pid_t owner = 0;

    app_name = argv[0];

    if (argc != 3)
    {
        printf("Error: incorrect # of args\n");
        print_usage();
    }
    else
    {
        card = (uint8_t)(strtoul(argv[2],NULL,10));

        if ( (SKIQ_MAX_NUM_CARDS - 1) < card )
        {
            printf("Error: card ID %" PRIu8 " exceeds the maximum card ID"
                    " (%" PRIu8 ")\n", card, (SKIQ_MAX_NUM_CARDS - 1));
            return (-1);
        }

        printf("Info: initializing card %" PRIu8 "...\n", card);

        status = skiq_init(skiq_xport_type_pcie, skiq_xport_init_level_full,
                    &card, 1);
        if( status != 0 )
        {
            if ( ( EBUSY == status ) &&
                 ( 0 != skiq_is_card_avail(card, &owner) ) )
            {
                printf("Error: card %" PRIu8 " is already in use (by process ID"
                        " %u); cannot initialize card.\n", card,
                        (unsigned int) owner);
            }
            else if ( -EINVAL == status )
            {
                printf("Error: unable to initialize libsidekiq; was a valid"
                        " card specified? (result code %" PRIi32 ")\n", status);
            }
            else
            {
                printf("Error: unable to initialize libsidekiq with status %"
                        PRIi32 "\n", status);
            }
            return (-1);
        }

        read_freq = atof(argv[1]);
        printf("Read freq %f, %" PRIu64 "\n", read_freq, (uint64_t)(read_freq));
        lo_freq = (uint64_t)(read_freq);

        hdl = skiq_tx_hdl_A1;

        printf("Info: tuning Tx LO to %" PRIu64 " Hz\n",lo_freq);
        status=skiq_write_tx_LO_freq(card,hdl,lo_freq);
        if (status < 0)
        {
            printf("Error: failed to set LO freq...status is %d\n", status);
            result = -1;
        }

        skiq_exit();
    }

    return(result);
}

/*****************************************************************************/
/** This function prints the main usage of the function.

    @param void
    @return void
*/
static void print_usage(void)
{
    printf("Usage: %s <LO freq in Hz> <card>\n",app_name);
    printf("   Tune the Tx LO freq on specified Sidekiq to the requested frequency.\n");
}

