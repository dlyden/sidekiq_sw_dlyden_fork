/**
 * @file   net_tx.c
 *
 * @brief Implements TX network transport
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/ 
#include <inttypes.h>

#include "sidekiq_xport_types.h"
#include "sidekiq_private.h"
#include "net_transport_private.h"

static int32_t _tx_initialize( uint64_t xport_uid,
                               skiq_tx_transfer_mode_t tx_transfer_mode,
                               uint32_t num_bytes_to_send,
                               uint8_t num_send_threads,
                               int32_t priority,
                               skiq_tx_callback_t tx_complete_cb );


static int32_t _tx_start_streaming( uint64_t xport_uid,
                                    skiq_tx_hdl_t hdl );

static int32_t _tx_pre_stop_streaming( uint64_t xport_uid,
                                       skiq_tx_hdl_t hdl );


static int32_t _tx_stop_streaming( uint64_t xport_uid,
                                   skiq_tx_hdl_t hdl );

static int32_t _tx_transmit( uint64_t xport_uid,
                             skiq_tx_hdl_t hdl,
                             int32_t *p_samples,
                             void *p_private );

skiq_xport_tx_functions_t net_xport_tx_funcs = {
    .tx_initialize      = _tx_initialize,
    .tx_start_streaming = _tx_start_streaming,
    .tx_pre_stop_streaming = _tx_pre_stop_streaming,
    .tx_stop_streaming  = _tx_stop_streaming,
    .tx_transmit        = _tx_transmit,
};

int32_t _tx_initialize( uint64_t xport_uid,
                        skiq_tx_transfer_mode_t tx_transfer_mode,
                        uint32_t num_bytes_to_send,
                        uint8_t num_send_threads,
                        int32_t priority,
                        skiq_tx_callback_t tx_complete_cb )
{
    int32_t status=0;

    // we don't support async mode or multiple threads, so return an error if requested
    if( tx_transfer_mode != skiq_tx_transfer_mode_sync )
    {
        skiq_error( "Invalid TX transfer mode specified %u\n", tx_transfer_mode );
        status = -EINVAL;
    }
    else
    {
        status = _net_tx_initialize( xport_uid, tx_transfer_mode, num_bytes_to_send, num_send_threads, priority );
    }

    return (status);
}

int32_t _tx_start_streaming( uint64_t xport_uid,
                             skiq_tx_hdl_t hdl )
{
    int32_t status=-EINVAL;

    if( hdl != skiq_tx_hdl_A1 )
    {
        skiq_error( "Only TX streaming with A1 supported with network transport (hdl=%s)", tx_hdl_cstr(hdl) );
    }
    else
    {
        status = _net_tx_start_streaming( xport_uid );
    }

    return (status);
}

static int32_t _tx_pre_stop_streaming( uint64_t xport_uid,
                                       skiq_tx_hdl_t hdl )
{
    int32_t status=-EINVAL;

    if( hdl != skiq_tx_hdl_A1 )
    {
        skiq_error( "Only TX streaming with A1 supported with network transport (hdl=%s)", tx_hdl_cstr(hdl) );
    }
    else
    {
        status = _net_tx_pre_stop_streaming( xport_uid );
    }

    return (status);
}


int32_t _tx_stop_streaming( uint64_t xport_uid,
                            skiq_tx_hdl_t hdl )
{
    int32_t status=-EINVAL;

    if( hdl != skiq_tx_hdl_A1 )
    {
        skiq_error( "Only TX streaming with A1 supported with network transport (hdl=%s)", tx_hdl_cstr(hdl) );
    }
    else
    {
        status = _net_tx_stop_streaming( xport_uid );
    }

    return (status);
}

int32_t _tx_transmit( uint64_t xport_uid,
                      skiq_tx_hdl_t hdl,
                      int32_t *p_samples,
                      void *p_private )
{
    int32_t status=0;

    // note: no need to check handle here since we check it on start
    status = _net_tx_transmit( xport_uid, p_samples );

    return (status);
}
