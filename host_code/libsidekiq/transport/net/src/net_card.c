/**
 * @file   net_card.c
 *
 * @brief  Network transport card related functions
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "sidekiq_xport_api.h"
#include "sidekiq_daemon_private.h"
#include "sidekiq_card_mgr.h"
#include "net_transport.h"
#include "net_transport_private.h"

#include "sidekiq_rpc_client_api.h"

/* enable debug_print and debug_print_plain when DEBUG_NET_TRANSPORT is defined */
#if (defined DEBUG_NET_TRANSPORT)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

// environment variable names to define where to locate port and IP of daemon
#define SKIQ_DAEMON_PORT "SKIQ_DAEMON_PORT"
#define SKIQ_DAEMON_IP "SKIQ_DAEMON_IP"

// ex setting environment variables
// $ export SKIQ_DAEMON_PORT=3417
// $ export SKIQ_DAEMON_IP=127.0.0.1
// unset with $ unset SKIQ_DAEMON_IP

static int32_t _net_xport_probe( uint64_t *p_uid,
                                 uint8_t *p_num_cards )
{
    int32_t status = 0;
    char *p_ip_addr;
    char *p_port_str;
    uint16_t port_num = DEFAULT_PROBE_PORT_NUM;
    skiq_daemon_req_msg_t req_msg;
    skiq_daemon_api_payload_t api_payload;
    uint8_t *p_card_ids;
    uint8_t i=0;
    int sock;
    uint32_t seq_num=0;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    long port_num_long=0;
    
    // read the environment
    p_port_str = getenv(SKIQ_DAEMON_PORT);
    p_ip_addr = getenv(SKIQ_DAEMON_IP);

    if( p_ip_addr == NULL )
    {
        // if no IP address is specified, just skip
        return (0);
    }
    else
    {
        struct sockaddr_in sa;
        // make sure it's a valid IP
        if( inet_pton( AF_INET, p_ip_addr, &sa.sin_addr ) != 1 )
        {
            skiq_error("Invalid IP address of %s specified", p_ip_addr);
            return (-EINVAL);
        }
    }

    if( p_port_str != NULL )
    {
        port_num_long = strtol( p_port_str, NULL, 10 );
        if( (port_num_long < 0) || (port_num_long > UINT16_MAX) )
        {
            skiq_error("Invalid port number specified (%s)", p_port_str);
            return (-ERANGE);
        }
        else
        {
            port_num = (uint16_t)(port_num_long);
        }
    }
    
    // TODO: eventually, we should support a colon deliminated list in our environment

    debug_print("net_xport_probe: %s %u\n", p_ip_addr, port_num);

    // TODO: when we support multiple remotes, we'll need to update this function
    if( (sock=_connect_daemon_socket( p_ip_addr,
                                      port_num )) >= 0 )
    {
        //////////////////////////////////////////
        // Get API
        status = _net_send_request( sock, SKIQ_DAEMON_GET_API, seq_num, &req_msg, 0 );
        if( status == 0 )
        {
            // now wait for a response
            status = _net_receive_resp( sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            
            if( (status == 0) &&
                (resp_status == 0) &&
                (payload_len == SKIQ_DAEMON_API_PAYLOAD_SIZE) )
            {
                // get the actual payload
                status = _net_receive_payload( sock, &api_payload, SKIQ_DAEMON_API_PAYLOAD_SIZE );
                if( status == 0 )
                {
                    // make sure the payload matches
                    if( api_payload.maj != SKIQ_DAEMON_API_MAJOR ||
                        api_payload.min != SKIQ_DAEMON_API_MINOR ||
                        api_payload.patch != SKIQ_DAEMON_API_PATCH )
                    {
                        skiq_error("Server daemon API (%u.%u.%u) doesn't match client API (%u.%u.%u)",
                                   api_payload.maj,
                                   api_payload.min,
                                   api_payload.patch,
                                   SKIQ_DAEMON_API_MAJOR,
                                   SKIQ_DAEMON_API_MINOR,
                                   SKIQ_DAEMON_API_PATCH);
                        status = -EPROTO;
                    }
                }
            }
            else
            {
                if( resp_status != 0 )
                {
                    skiq_error("Received the daemon API response error %d (IP=%s, port=%u)\n", 
                        resp_status, p_ip_addr, port_num);
                    status = resp_status;
                }
                else
                {
                    skiq_error("Did not receive the daemon API response with error %d (IP=%s, port=%u)\n", 
                        status, p_ip_addr, port_num);
                    status = -EPROTO;
                }
            }
        }
        ///////////////////        
        seq_num++;

        //////////////////
        // Get Cards
        if( status == 0 )
        {
            status = _net_send_request( sock,
                                        SKIQ_DAEMON_GET_CARDS,
                                        seq_num,
                                        &req_msg,
                                        0 );

            if( status == 0 )
            {
                status = _net_receive_resp( sock,
                                            &resp_status,
                                            seq_num,
                                            &payload_len );
                if( (status == 0) && (resp_status == 0) )
                {
                    *p_num_cards = payload_len/sizeof(uint8_t);
                    if( *p_num_cards < SKIQ_MAX_NUM_CARDS )
                    {
                        // allocate memory
                        p_card_ids = malloc( payload_len );
                        
                        if( p_card_ids != NULL )
                        {
                            // get the actual payload
                            status = _net_receive_payload( sock, p_card_ids, payload_len );
                            if( status == 0 )
                            {                            
                                for( i=0; i<(*p_num_cards); i++ )
                                {
                                    // generate a UID for each card detected
                                    p_uid[i] = _net_get_uid( p_ip_addr, port_num, p_card_ids[i] );
                                    debug_print("Adding %" PRIu64 " for IP=%s, port=%u, card=%u",
                                               p_uid[i], p_ip_addr, port_num, p_card_ids[i]);
                                }
                            } // got payload ok
                            free( p_card_ids );
                        } // card malloc ok
                    }
                    else
                    {
                        skiq_error( "Number of cards available (%u) > maximum number of cards (%u)",
                                    *p_num_cards, SKIQ_MAX_NUM_CARDS);
                    }
                } // response header ok
                else
                {
                    if( resp_status != 0 )
                    {
                        skiq_error("Did not receive a response from the net card request with error=%d (resp_error=%d), (IP=%s, port=%u)", 
                            status, resp_status, p_ip_addr, port_num);
                        status = -EPROTO;
                    }
                }
            }
        }
        seq_num++;

        // we finished with our basic probe info, release the socket and close
        status = _net_send_request( sock,
                                    SKIQ_DAEMON_RELEASE_SOCKET,
                                    seq_num,
                                    &req_msg,
                                    0 );
        close( sock );
    }

    return (status);
}

static int32_t _net_xport_hotplug( uint64_t *uid_list,
                                   uint8_t *p_nr_uids,
                                   uint64_t no_probe_uids[],
                                   uint8_t nr_no_probe_uids )
{    
    int32_t status=0;
    uint64_t probe_uids[SKIQ_MAX_NUM_CARDS];
    uint8_t nr_probe_cards=0;
    bool ignore_card=false;
    uint8_t i,j=0;

    status = _net_xport_probe( probe_uids, &nr_probe_cards );
    if( status == 0 )
    {
        // we need to ignore any duplicate card
        for( i=0; i<nr_probe_cards; i++ )
        {
            // reset ignore_cards falg
            ignore_card = false;

            for( j=0; (j<nr_no_probe_uids) && (ignore_card==false); j++ )
            {
                if( probe_uids[i] == no_probe_uids[j] )
                {
                    ignore_card = true;
                }
            }
            if( ignore_card == false )
            {
                uid_list[*p_nr_uids] = probe_uids[i];
                *p_nr_uids = *p_nr_uids + 1;
            }
        }
    }

    return (status);
}

static int32_t _net_xport_init( skiq_xport_init_level_t level,
                                uint64_t xport_uid )
{
    int32_t status = 0;
    int sock = UNINITIALIZED_SOCK;
    skiq_daemon_req_msg_t req_msg;
    skiq_xport_id_t xport_id;
    skiq_daemon_reserve_card_t reserve_card;
    skiq_daemon_port_resp_payload_t resp_payload;
    int32_t seq_num=0;
    int ctrl_sock=0;
    int32_t resp_status;
    uint32_t payload_len;
    
    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_net;

    debug_print("got port %u, ip %s card=%u\n",
                _net_get_port_from_uid(xport_uid),
                _net_get_ip_from_uid(xport_uid),
                _net_get_card_num_from_uid(xport_uid));

    // connect the probe socket to reserve the card
    if( (sock=_connect_daemon_socket( _net_get_ip_from_uid(xport_uid),
                                      _net_get_port_from_uid(xport_uid) )) >= 0 )
    {
        reserve_card.payload.card_num = _net_get_card_num_from_uid(xport_uid);
        reserve_card.payload.level = htonl(level);
        
        status = _net_send_request( sock,
                                    SKIQ_DAEMON_RESERVE_CARD,
                                    seq_num,
                                    &reserve_card,
                                    SKIQ_DAEMON_RESERVE_CARD_PAYLOAD_SIZE );
            
        if( status == 0 )
        {
            status = _net_receive_resp( sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( (status == 0) &&
                (resp_status == 0) &&
                (payload_len == SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE) )
            {
                // get the control port
                status = _net_receive_payload( sock, &resp_payload, payload_len );
                if( status == 0 )
                {
                    seq_num++;
                    // we have a control port, release the probe socket
                    status = _net_send_request( sock,
                                                SKIQ_DAEMON_RELEASE_SOCKET,
                                                seq_num,
                                                &req_msg,
                                                0 );
                    // close the probe socket
                    close( sock );
                    sock = UNINITIALIZED_SOCK;
                    if( status == 0 )
                    {
                        if( (ctrl_sock =
                             _connect_daemon_socket( _net_get_ip_from_uid(xport_uid),
                                                     ntohs( resp_payload.port ) )) >= 0 )
                        {
                            xport_register_fpga_functions(&xport_id, &net_xport_fpga_funcs);
                            if( skiq_xport_init_level_full ==  level )
                            {
                                xport_register_rx_functions(&xport_id, &net_xport_rx_funcs);
                                xport_register_tx_functions(&xport_id, &net_xport_tx_funcs);
                            }                      
                        } // connected to control socket
                    } // release sent successfully
                } // reserve payload good
            } // got resp ok
            else
            {
                if( resp_status != 0 )
                {
                    status = resp_status;
                }
                if( payload_len != SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE )
                {
                    status = -EPROTO;
                }
            }
        } // sent reserve card ok
    } // probe socket connected

    if( sock >= 0 )
    {
        // close the probe socket
        close( sock );
    }

    // if everything's good so far, add the connection and intialize RPC
    if( status == 0 )
    {
        // add connection
        status = _net_add_conn( xport_uid );
        if ( status == 0 )
        {
            status = _net_set_ctrl_sock( xport_uid, ctrl_sock );
            if ( status == 0 )
            {
                status = _net_set_ctrl_seq_num( xport_uid, seq_num );
            }

            if ( status == 0 )
            {
                status = sidekiq_rpc_client_init( &xport_id );
            }

            if ( status != 0 )
            {
                skiq_error("Unable to configure network control interface");

                /* Configuring the network control interface failed, so release the control socket
                 * descriptor here */
                close( ctrl_sock );
                ctrl_sock = -1;
            }
        }
        else
        {
            skiq_error("Unable to add network connection");
        }
    }
    
    return (status);
}

static int32_t _net_xport_exit( skiq_xport_init_level_t level,
                                uint64_t xport_uid )
{
    int32_t status=0;
    skiq_daemon_req_msg_t req_msg;
    skiq_xport_id_t xport_id;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    int ctrl_sock=0;
    uint32_t seq_num;

    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_net;

    if( (status=_net_get_ctrl_sock( xport_uid, &ctrl_sock )) == 0 &&
        (status=_net_get_ctrl_seq_num( xport_uid, &seq_num )) == 0 )
    {
        // release RPC
        status = sidekiq_rpc_client_exit( &xport_id );

        if( status == 0 )
        {
            // release the card
            status = _net_send_request( ctrl_sock,
                                        SKIQ_DAEMON_RELEASE_CARD,
                                        seq_num,
                                        &req_msg,
                                        0 );

            if( status == 0 )
            {
                status = _net_receive_resp( ctrl_sock,
                                            &resp_status,
                                            seq_num,
                                            &payload_len );
                if( (status == 0) && (resp_status == 0) )
                {
                    seq_num++;
                    xport_unregister_fpga_functions(&xport_id);
                    xport_unregister_rx_functions(&xport_id);
                    // TODO: TX functions

                    // release the control socket
                    status = _net_send_request( ctrl_sock,
                                                SKIQ_DAEMON_RELEASE_SOCKET,
                                                seq_num,
                                                &req_msg,
                                                0 );
                    
                    close( ctrl_sock );
                    // we don't need to update the ctrl sock or seq # since we're just removing the connection
                    status=_net_remove_conn( xport_uid );
                } // end release card response ok
                else
                {
                    if( resp_status != 0 )
                    {
                        status = resp_status;
                    }
                }
            } // end release card request sent
        }
    }    
    
    return (status);
}

/***** GLOBAL VARIABLES *****/
skiq_xport_card_functions_t net_xport_card_funcs =
{
    .card_probe = _net_xport_probe,
    .card_hotplug = _net_xport_hotplug,
    .card_init  = _net_xport_init,
    .card_exit  = _net_xport_exit,
    .card_read_priv_data = NULL,
    .card_write_priv_data = NULL,
};
