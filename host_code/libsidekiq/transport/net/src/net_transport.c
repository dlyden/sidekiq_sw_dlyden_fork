/**
 * @file   net_transport.c
 *
 * @brief  Network transport utility functions
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <glib.h>
#include <pthread.h>

#include "sidekiq_private.h"
#include "net_transport.h"
#include "net_transport_private.h"
#include "bit_ops.h"
#include "sidekiq_api.h"

// Generic connection info
typedef struct
{
    pthread_mutex_t stream_mutex;
    pthread_t stream_thread;
    bool b_stream_running;
    uint16_t port;
    struct sockaddr_in server_addr;
    socklen_t socklen;
    int sock_fd;
} net_stream_conn_t;

// RX streaming connection info
struct net_rx_conn_t
{
    net_stream_conn_t rx_stream_conn;
    bool hdl_streaming[skiq_rx_hdl_end];
};

// TX streaming connection info
struct net_tx_conn_t
{
    net_stream_conn_t tx_stream_conn;
    uint32_t num_bytes_to_send;
};

// Card number stored in bits 48-56
#define NET_CARD_NUM_OFFSET    (48)
#define NET_CARD_NUM_MASK      (0x00FF000000000000)
// network port stored in bits 32-48
#define NET_PORT_NUM_OFFSET    (32)
#define NET_PORT_NUM_MASK      (0x0000FFFF00000000)

// IP address in lower 32 bits...note: this will only work with IPv4 (must be updated for IPv6)
#define NET_IP_ADDR_OFFSET     (0)
#define NET_IP_ADDR_MASK       (0x00000000FFFFFFFF)

// Note: we're expecting that we're always using a 64-bit number when we're shifting
#define NET_TRANSPORT_SHIFT (56)

#define MAX_NUM_NET_PKTS (256) // completely arbitrary

static GHashTable *_p_net_conn = NULL;
static pthread_mutex_t _net_hash_mutex = PTHREAD_MUTEX_INITIALIZER;

static uint8_t _rx_buf[SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES*MAX_NUM_NET_PKTS];
static uint32_t _curr_pkt_index=0;

static struct net_rx_conn_t* _init_rx_conn( void );
static struct net_tx_conn_t* _init_tx_conn( void );

static int32_t _init_rx_stream_conn(struct daemon_conn_t *p_rx_conn);
static int32_t _init_tx_stream_conn(struct daemon_conn_t *p_tx_conn);

static int32_t _send_full_packet( int sock_fd,
                                  uint8_t *p_pkt,
                                  uint32_t num_bytes,
                                  int send_flags );

struct net_rx_conn_t* _init_rx_conn( void )
{
    struct net_rx_conn_t *p_rx_conn = NULL;
    uint8_t i=0;

    p_rx_conn = malloc( sizeof(struct net_rx_conn_t) );
    if( p_rx_conn != NULL )
    {
        pthread_mutex_init( &p_rx_conn->rx_stream_conn.stream_mutex, NULL );
    
        // no need to initialize the thread / port since we're not running
        p_rx_conn->rx_stream_conn.b_stream_running = false;
        p_rx_conn->rx_stream_conn.port = 0;
        p_rx_conn->rx_stream_conn.socklen = 0;
        p_rx_conn->rx_stream_conn.sock_fd = -1;
        memset(&(p_rx_conn->rx_stream_conn.server_addr), 0, sizeof(struct sockaddr_in)); 
        // default all the handles to not streaming
        for( i=0; i<skiq_rx_hdl_end; i++ )
        {
            p_rx_conn->hdl_streaming[i] = false;
        }
    }

    return (p_rx_conn);
}

static struct net_tx_conn_t* _init_tx_conn( void )
{
    struct net_tx_conn_t *p_tx_conn = NULL;

    p_tx_conn = malloc( sizeof(struct net_tx_conn_t) );
    if( p_tx_conn != NULL )
    {
        pthread_mutex_init( &p_tx_conn->tx_stream_conn.stream_mutex, NULL );
    
        // no need to initialize the thread / port since we're not running
        p_tx_conn->tx_stream_conn.b_stream_running = false;
        p_tx_conn->tx_stream_conn.port = 0;
        p_tx_conn->tx_stream_conn.socklen = 0;
        p_tx_conn->tx_stream_conn.sock_fd = 0;
        memset(&(p_tx_conn->tx_stream_conn.server_addr), 0, sizeof(struct sockaddr_in)); 
    }

    return (p_tx_conn);    
}

uint64_t _net_get_uid( const char* p_ip, uint16_t port, uint8_t server_card_num )
{
    uint64_t uid=0;
    struct in_addr ip_addr_num;

    if( inet_aton( p_ip, &ip_addr_num ) != 0 )
    {
        uid = ((uint64_t)(ip_addr_num.s_addr) << NET_IP_ADDR_OFFSET) |
            ((uint64_t)(port) << NET_PORT_NUM_OFFSET) |
            ((uint64_t)(server_card_num) << NET_CARD_NUM_OFFSET) |
            ((uint64_t)(SUBCLASS_NET) << NET_TRANSPORT_SHIFT);
    }
    else
    {
        skiq_error("Invalid IP address specified (%s)", p_ip);
    }
    
    return (uid);
}

const char* _net_get_ip_from_uid( uint64_t uid )
{
    struct in_addr ip_addr_num;

    ip_addr_num.s_addr = (uid & NET_IP_ADDR_MASK) >> NET_IP_ADDR_OFFSET;

    return (inet_ntoa(ip_addr_num));
}

uint16_t _net_get_port_from_uid( uint64_t uid )
{
    uint16_t port_num=0;

    port_num = (uid & NET_PORT_NUM_MASK) >> NET_PORT_NUM_OFFSET;

    return (port_num);
}

uint8_t _net_get_card_num_from_uid( uint64_t uid )
{
    uint8_t card=0;

    card = (uid & NET_CARD_NUM_MASK) >> NET_CARD_NUM_OFFSET;

    return (card);
}

int32_t _net_add_conn( uint64_t uid )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn=NULL;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        _p_net_conn = g_hash_table_new( g_direct_hash,
                                        g_direct_equal );
    }

    if( _p_net_conn != NULL )
    {
        // see if the connection already exists
        if( g_hash_table_contains(_p_net_conn, GUINT_TO_POINTER(uid)) == false )
        {
            p_conn = (struct daemon_conn_t*)(malloc( sizeof(struct daemon_conn_t) ));
            if( p_conn != NULL )
            {
                p_conn->uid = uid;
                // initialize the socket and port
                p_conn->ctrl_sock = UNINITIALIZED_SOCK;
                p_conn->ctrl_seq_num = 0;
                // initialize the RX connection
                p_conn->p_rx_conn = _init_rx_conn();
                p_conn->p_tx_conn = _init_tx_conn();
                if( (p_conn->p_rx_conn != NULL) && (p_conn->p_tx_conn != NULL) )
                {
                    // insert it
                    g_hash_table_insert( _p_net_conn, GUINT_TO_POINTER(uid), p_conn );
                }
                else
                {
                    skiq_error("Unable to allocate memory for T/RX connection");
                    if( p_conn->p_rx_conn != NULL )
                    {
                        pthread_mutex_destroy( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
                    }
                    if( p_conn->p_tx_conn != NULL )
                    {
                        pthread_mutex_destroy( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
                    }
                    FREE_IF_NOT_NULL( p_conn->p_rx_conn );
                    FREE_IF_NOT_NULL( p_conn->p_tx_conn );

                    status = -ENOMEM;
                    FREE_IF_NOT_NULL( p_conn );
                }
            }
            else
            {
                skiq_error("Unable to allocate memory for new connection");
                status = -ENOMEM;
            }
        }
        else
        {
            skiq_error("Connection already exists for UID 0x%" PRIx64, uid);
            status = -EBUSY;
        }
    }
    else
    {
        skiq_error("Unable to initialize network hash table");
        status = -ENOMEM;
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _net_remove_conn( uint64_t uid )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        p_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( p_conn == NULL )
        {
            skiq_error("Trying to remove connection that does not exist (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
        else
        {
            // remove it
            g_hash_table_remove( _p_net_conn, GUINT_TO_POINTER(uid) );
            if( p_conn->p_rx_conn != NULL )
            {
                pthread_mutex_destroy( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
            }
            if( p_conn->p_tx_conn != NULL )
            {
                pthread_mutex_destroy( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
            }
            FREE_IF_NOT_NULL( p_conn->p_rx_conn );
            FREE_IF_NOT_NULL( p_conn->p_tx_conn );
            // free the memory
            FREE_IF_NOT_NULL( p_conn );
            // free the table if nothing left in it
            if( g_hash_table_size( _p_net_conn ) == 0 )
            {
                g_hash_table_destroy( _p_net_conn );
                _p_net_conn = NULL;
            }
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );
    
    return (status);
}

int32_t _net_get_ctrl_sock( uint64_t uid, int *p_ctrl_sock )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        p_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( p_conn == NULL )
        {
            skiq_error("No net entry registered (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
        else
        {
            *p_ctrl_sock = p_conn->ctrl_sock;
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _net_set_ctrl_sock( uint64_t uid, int ctrl_sock )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        p_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( p_conn == NULL )
        {
            skiq_error("No net entry registered (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
        else
        {
            p_conn->ctrl_sock = ctrl_sock;
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _net_get_ctrl_seq_num( uint64_t uid, uint32_t *p_seq_num )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        p_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( p_conn == NULL )
        {
            skiq_error("No net entry registered (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
        else
        {
            *p_seq_num = p_conn->ctrl_seq_num;
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _net_set_ctrl_seq_num( uint64_t uid, uint32_t seq_num )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        p_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( p_conn == NULL )
        {
            skiq_error("No net entry registered (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
        else
        {
            p_conn->ctrl_seq_num = seq_num;
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _net_get_daemon_conn( uint64_t uid, struct daemon_conn_t **pp_conn )
{
    int32_t status=0;

    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        *pp_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( *pp_conn == NULL )
        {
            skiq_error("No net entry registered (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _connect_daemon_socket( const char* p_ip_addr, uint16_t port )
{
    struct sockaddr_in serv_addr;
    int32_t status=0;
    int sock=0;
    struct timeval tv;

    errno = 0;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    { 
        status = -errno; 
    }

    if( status == 0 )
    {
        serv_addr.sin_family = AF_INET; 
        serv_addr.sin_port = htons(port); 
       
        // Convert IPv4 and IPv6 addresses from text to binary form 
        errno = 0;
        if(inet_pton(AF_INET, p_ip_addr, &serv_addr.sin_addr)<=0)  
        {
            status = -errno;
        }

        if( status == 0 )
        {
            errno = 0;
            if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
            {
                status = -errno;
            }
            else
            {
                tv.tv_sec = RESPONSE_TIMEOUT_SEC; 
                tv.tv_usec = 0;
                errno = 0;
                if( setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)(&tv), sizeof(tv)) != 0 )
                {
                    skiq_error("Unable to set socket options for daemon, errno %d", errno);
                }
            }
        }
    }
    if( status == 0 )
    {
        status = sock;
    }
    else
    {
        close(sock);
    }

    return (status);
}

int32_t _net_send_request( int32_t sock,
                           uint32_t type,
                           uint32_t seq_num,
                           void* p_msg,
                           uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_req_msg_t *p_req = (skiq_daemon_req_msg_t*)(p_msg);
    ssize_t msg_sent;
    uint32_t tot_msg_size = SKIQ_DAEMON_REQ_SIZE+payload_len;
    
    p_req->som = htonl( DAEMON_HDR_SOM );
    p_req->request_type = htonl( type );
    p_req->seq_number = htonl( seq_num );
    p_req->payload_len = htonl( payload_len );

    errno = 0;

    msg_sent = send( sock,
                     p_msg,
                     tot_msg_size,
                     0 );
    if( msg_sent > 0 )
    {
        if( msg_sent != tot_msg_size )
        {
            skiq_error( "Attempted to send message of %u bytes, only sent %u bytes",
                        tot_msg_size, (uint32_t)(msg_sent) );
            status = -EIO;
        }
    }
    else
    {
        status = -errno;
    }
    
    return (status);
}

int32_t _net_send_resp( int32_t sock,
                        int32_t resp_status,
                        uint32_t seq_num,
                        uint32_t payload_len )
{
    int32_t status=0;
    skiq_daemon_resp_msg_t resp_msg;
    ssize_t msg_size;

    resp_msg.som = htonl( DAEMON_HDR_SOM );
    resp_msg.status = htonl( resp_status );
    resp_msg.seq_number = htonl( seq_num );
    resp_msg.payload_len = htonl( payload_len );

    errno = 0;
    
    msg_size = send( sock, &resp_msg, SKIQ_DAEMON_RESP_SIZE, 0 );
    if( msg_size < 0 )
    {
        status = -errno;
    }
    else if( msg_size != SKIQ_DAEMON_RESP_SIZE )
    {
        skiq_error("Unable to send %" PRIu32 " bytes, only sent %" PRIu32,
                   (uint32_t)(SKIQ_DAEMON_RESP_SIZE), (uint32_t)(msg_size));
        status = -EPROTO;
    }
    
    return (status);
}

int32_t _net_send_payload( int32_t sock, void *p_payload, uint32_t payload_len )
{
    int32_t status=0;
    ssize_t msg_size;

    errno = 0;
    
    msg_size = send( sock, p_payload, payload_len, 0 );
    if( msg_size < 0 )
    {
        status = -errno;
    }
    else if( msg_size < payload_len )
    {
        status = -EPROTO;
        skiq_error("Unable to send %" PRIu32 " bytes, only sent %" PRIu32,
                   payload_len, (uint32_t)(msg_size));
    }

    return (status);
}

int32_t _net_receive_resp( int32_t sock,
                           int32_t *p_resp_status,
                           uint32_t expected_seq_num,
                           uint32_t *p_payload_len )
{
    int32_t status=0;
    ssize_t msg_read;
    skiq_daemon_resp_msg_t resp_msg;

    errno = 0;

    if( (msg_read=read(sock, &resp_msg, SKIQ_DAEMON_RESP_SIZE )) == SKIQ_DAEMON_RESP_SIZE )
    {
        // check for SOM and seq number
        if( (ntohl( resp_msg.som ) != DAEMON_HDR_SOM) ||
            (ntohl( resp_msg.seq_number ) != expected_seq_num) )
        {
            skiq_error("Did not receive properly formatted response (som=%u(%u) / sn=%u(%u))",
                       ntohl(resp_msg.som), DAEMON_HDR_SOM, ntohl(resp_msg.seq_number), expected_seq_num);
            status = -EPROTO;
        }        
        else
        {
            // set payload len
            *p_payload_len = ntohl( resp_msg.payload_len );
            // set the response status
            *p_resp_status = ntohl( resp_msg.status );
        }
    }
    else
    {
        if( msg_read < 0 )
        {
            status = -errno;
            skiq_error("Socket receive failed (errno=%d)", errno);
        }
        else
        {
            skiq_error("Did not receive expected response size (%d bytes read)", (int32_t)(msg_read));
            status = -EPROTO;
        }
    }

    return (status);
}

int32_t _net_receive_payload( int32_t sock,
                              void *p_payload,
                              uint32_t payload_len )
{
    int32_t status=0;
    ssize_t msg_read;

    errno = 0;

    if( (msg_read = read(sock, p_payload, payload_len)) < 0 )
    {
        status = -errno;
    }
    else if( msg_read != payload_len )
    {
        skiq_error("Did not receive expected payload size (%d bytes read)", (int32_t)(msg_read));
        status = -EPROTO;
    }
    
    return (status);
}

int32_t _net_rx_set_config( uint64_t uid, skiq_daemon_rx_configure_payload_t *p_rx_config )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;
    bool update_daemon=false;
    skiq_daemon_rx_configure_t rx_config;
    int32_t resp_status=0;
    uint32_t payload_len=0;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );
 
    if( status == 0 )
    {
        // lock the RX mutex and update the config
        pthread_mutex_lock( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
        // notify the daemon of the update if we're streaming
        if( p_conn->p_rx_conn->rx_stream_conn.b_stream_running == true )
        {
            rx_config.payload.usleep_per_skiq_rx = htonl( p_rx_config->usleep_per_skiq_rx );
            rx_config.payload.num_bytes_per_trans = htonl( p_rx_config->num_bytes_per_trans );
            rx_config.payload.buffered = p_rx_config->buffered;
            update_daemon = true;
        }
        pthread_mutex_unlock( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );

        if( update_daemon == true )
        {
            status = _net_send_request( p_conn->ctrl_sock,
                                        SKIQ_DAEMON_RX_CONFIGURE,
                                        p_conn->ctrl_seq_num,
                                        &rx_config,
                                        SKIQ_DAEMON_RX_CONFIGURE_PAYLOAD_SIZE );
            if( status == 0 )
            {
                status = _net_receive_resp( p_conn->ctrl_sock,
                                            &resp_status,
                                            p_conn->ctrl_seq_num,
                                            &payload_len );
                if( status == 0 )
                {
                    status = resp_status;
                }
            }
            if( status == 0 )
            {
                status = _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
            }
        }
    }
    return (status);
}

int32_t _net_rx_start_streaming_for_hdl( uint64_t uid, skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn=NULL;
    skiq_daemon_req_msg_t req_msg;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    skiq_daemon_port_resp_payload_t resp_payload;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );

    if( status == 0 )
    {
        // lock the RX mutex and update the config
        pthread_mutex_lock( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
        p_conn->p_rx_conn->hdl_streaming[hdl] = true;

        if( p_conn->p_rx_conn->rx_stream_conn.b_stream_running == false )
        {
            // send SKIQ_DAEMON_RX_START_STREAMING
            status = _net_send_request( p_conn->ctrl_sock,
                                        SKIQ_DAEMON_RX_PREP_SOCK,
                                        p_conn->ctrl_seq_num,
                                        &req_msg,
                                        0 );
            if( status == 0 )
            {
                status = _net_receive_resp( p_conn->ctrl_sock,
                                            &resp_status,
                                            p_conn->ctrl_seq_num,
                                            &payload_len );
                if( status == 0 )
                {
                    status = resp_status;
                }
            }
            if( status == 0 )
            {
                // receive skiq_daemon_port_payload_t
                if( (resp_status == 0) &&
                    (payload_len == SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE) )
                {
                    // get the control port
                    status = _net_receive_payload( p_conn->ctrl_sock, &resp_payload, payload_len );
                }
                else
                {
                    status = -EPROTO;
                }
                _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
            }

            if( status == 0 )
            {
                p_conn->p_rx_conn->rx_stream_conn.port = ntohs(resp_payload.port);
                status = _init_rx_stream_conn( p_conn );

                if( status == 0 )
                {
                    // now start the stream
                    status = _net_send_request( p_conn->ctrl_sock,
                                                SKIQ_DAEMON_RX_START_STREAMING,
                                                p_conn->ctrl_seq_num,
                                                &req_msg,
                                                0 );
                    if( status == 0 )
                    {
                        status = _net_receive_resp( p_conn->ctrl_sock,
                                                    &resp_status,
                                                    p_conn->ctrl_seq_num,
                                                    &payload_len );
                        if( status == 0 )
                        {
                            status = resp_status;
                        }
                        else
                        {
                            skiq_warning("Failed to receive response to start streaming call (status=%d)", status);
                        }
                    }
                    else
                    {
                        skiq_warning("Unable to send start streaming message (status=%d)", status);
                    }
                }
            }
        }
        pthread_mutex_unlock( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
    }
    
    return (status);
}

int32_t _net_rx_stop_streaming_for_hdl( uint64_t uid, skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;
    bool hdl_streaming=false;
    uint8_t i=0;
    skiq_daemon_req_msg_t req_msg;
    int32_t resp_status=0;
    uint32_t payload_len=0;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );

    if( status == 0 )
    {
        // lock the RX mutex and update the config
        pthread_mutex_lock( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
        p_conn->p_rx_conn->hdl_streaming[hdl] = false;

        if( p_conn->p_rx_conn->rx_stream_conn.b_stream_running == true )
        {
            // loop through all the handles and see if anything is streaming...we don't want
            // to tear down the RX streaming socket if any handle is still streamin
            for( i=0; (i<skiq_rx_hdl_end) && (hdl_streaming==false); i++ )
            {
                // there's stil a handle streaming
                if( p_conn->p_rx_conn->hdl_streaming[i] == true )
                {
                    hdl_streaming = true;
                }
            }
            // see if we should still be streaming if no handles are streaming
            if( hdl_streaming == false )
            {

                // send SKIQ_DAEMON_RX_STOP_STREAMING
                status = _net_send_request( p_conn->ctrl_sock,
                                            SKIQ_DAEMON_RX_STOP_STREAMING,
                                            p_conn->ctrl_seq_num,
                                            &req_msg,
                                            0 );
                if( status == 0 )
                {
                    status = _net_receive_resp( p_conn->ctrl_sock,
                                                &resp_status,
                                                p_conn->ctrl_seq_num,
                                                &payload_len );
                    if( status == 0 )
                    {
                        p_conn->p_rx_conn->rx_stream_conn.b_stream_running = false;
                        close( p_conn->p_rx_conn->rx_stream_conn.sock_fd );
                        p_conn->p_rx_conn->rx_stream_conn.sock_fd = -1;
                        status = resp_status;
                    }
                }
                if( status == 0 )
                {
                    _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
                }
            }
        }
        pthread_mutex_unlock( &p_conn->p_rx_conn->rx_stream_conn.stream_mutex );
    }
    
    return (status);
}

int32_t _init_rx_stream_conn(struct daemon_conn_t *p_conn)
{
    int32_t status=0;
    uint32_t som = htonl( DAEMON_HDR_SOM );
    struct net_rx_conn_t *p_rx_conn = p_conn->p_rx_conn;
    const char *p_ip = NULL;

    p_ip = _net_get_ip_from_uid( p_conn->uid );

    errno = 0;
    if( (p_rx_conn->rx_stream_conn.sock_fd=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) != -1 )
    {
        memset(&(p_rx_conn->rx_stream_conn.server_addr), 0, sizeof(struct sockaddr_in)); 

        p_rx_conn->rx_stream_conn.server_addr.sin_family = AF_INET; 
        p_rx_conn->rx_stream_conn.server_addr.sin_port = htons(p_rx_conn->rx_stream_conn.port);

        errno = 0;
        if(inet_pton(AF_INET, p_ip, &p_rx_conn->rx_stream_conn.server_addr.sin_addr)<=0)  
        {
            status = -errno;
        }

        if( status == 0 )
        {
            p_rx_conn->rx_stream_conn.socklen = sizeof(struct sockaddr_in);
            
            skiq_debug("Sending SOM handshake\n");

            errno = 0;
            status = sendto( p_rx_conn->rx_stream_conn.sock_fd, &som, sizeof(uint32_t), MSG_CONFIRM,
                             (struct sockaddr*)(&p_rx_conn->rx_stream_conn.server_addr), p_rx_conn->rx_stream_conn.socklen);
            if( status == sizeof(uint32_t) )
            {
                p_rx_conn->rx_stream_conn.b_stream_running = true;
                status = 0;
            }
            else
            {
                skiq_debug("Failed to send, errno %d\n", errno);
                status = -errno;
            }
        }
    }
    else
    {
        status = - errno;
    }

    return (status);
}

int32_t _init_tx_stream_conn(struct daemon_conn_t *p_conn)
{
    int32_t status=0;
    struct net_tx_conn_t *p_tx_conn = p_conn->p_tx_conn;
    const char *p_ip = NULL;

    p_ip = _net_get_ip_from_uid( p_conn->uid );

    // connect to the port
    status = _connect_daemon_socket( p_ip, p_tx_conn->tx_stream_conn.port );
    if( status >= 0 )
    {
        // save the sock fd
        p_tx_conn->tx_stream_conn.sock_fd = status;
        status = 0;
    }

    return (status);
}

int32_t _net_rx_get_pkt( uint64_t uid, uint8_t **pp_data, uint32_t *p_data_len )
{
    int32_t status = skiq_rx_status_no_data;
    struct daemon_conn_t *p_conn = NULL;
    struct net_rx_conn_t *p_rx_conn = NULL;
    
    // check if there's a packet ready
    pthread_mutex_lock( &_net_hash_mutex );
    if( _p_net_conn == NULL )
    {
        skiq_error("Network hash not initialized");
        status = -ENODEV;
    }
    else
    {
        p_conn = g_hash_table_lookup( _p_net_conn, GUINT_TO_POINTER(uid) );
        if( p_conn == NULL )
        {
            skiq_error("No net entry registered (uid=0x%" PRIx64 ")", uid);
            status = -ENODEV;
        }
        else
        {
            p_rx_conn = p_conn->p_rx_conn;
            if( p_rx_conn->rx_stream_conn.b_stream_running == true )
            {
                errno = 0;
                status = recvfrom(p_rx_conn->rx_stream_conn.sock_fd,
                                  &(_rx_buf[_curr_pkt_index*SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES]),
                                  SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES,  
                                  /*MSG_WAITALL,*/MSG_DONTWAIT,
                                  (struct sockaddr*)(&p_rx_conn->rx_stream_conn.server_addr),
                                  &(p_rx_conn->rx_stream_conn.socklen));
                if( status == SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES )
                {
                    *pp_data = (uint8_t*)(&_rx_buf[_curr_pkt_index*SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES]);
                    *p_data_len = status;
                    status = skiq_rx_status_success;
                    _curr_pkt_index++;
                    // reset out index back to 0
                    if( _curr_pkt_index >= MAX_NUM_NET_PKTS )
                    {
                        _curr_pkt_index = 0;
                    }
                }
                else if( (errno == EAGAIN) || (errno == EWOULDBLOCK) )
                {
                    status = skiq_rx_status_no_data;
                }
                else
                {
                    skiq_error("unexpected error from receive call errno=%d\n", errno);
                    status = skiq_rx_status_error_generic;
                }
            }
        }
    }
    pthread_mutex_unlock( &_net_hash_mutex );

    return (status);
}

int32_t _net_tx_initialize( uint64_t uid, 
                            skiq_tx_transfer_mode_t transfer_mode,
                            uint32_t num_bytes_to_send,
                            uint8_t num_send_threads,
                            int32_t priority )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;
    skiq_daemon_tx_configure_t tx_config;
    int32_t resp_status=0;
    uint32_t payload_len=0;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );
 
    if( status == 0 )
    {
        tx_config.payload.transfer_mode = htonl( (uint32_t)(transfer_mode) );
        tx_config.payload.num_bytes_to_send = htonl( num_bytes_to_send );
        tx_config.payload.num_send_threads = num_send_threads;
        tx_config.payload.priority = htonl( priority );

        status = _net_send_request( p_conn->ctrl_sock,
                                    SKIQ_DAEMON_TX_CONFIGURE,
                                    p_conn->ctrl_seq_num,
                                    &tx_config,
                                    SKIQ_DAEMON_TX_CONFIGURE_PAYLOAD_SIZE );
        if( status == 0 )
        {
            status = _net_receive_resp( p_conn->ctrl_sock,
                                        &resp_status,
                                        p_conn->ctrl_seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
            }
        }
        if( status == 0 )
        {
            status = _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
            pthread_mutex_lock( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
            // update the num_bytes_to_send
            p_conn->p_tx_conn->num_bytes_to_send = num_bytes_to_send;
            pthread_mutex_unlock( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );

        }
    }
    return (status);
}

int32_t _net_tx_start_streaming( uint64_t uid )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn=NULL;
    skiq_daemon_req_msg_t req_msg;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    skiq_daemon_port_resp_payload_t resp_payload;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );

    if( status == 0 )
    {
         // send SKIQ_DAEMON_TX_START_STREAMING
         status = _net_send_request( p_conn->ctrl_sock,
                                     SKIQ_DAEMON_TX_START_STREAMING,
                                     p_conn->ctrl_seq_num,
                                     &req_msg,
                                     0 );
        if( status == 0 )
        {
            status = _net_receive_resp( p_conn->ctrl_sock,
                                        &resp_status,
                                        p_conn->ctrl_seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
            }
        }
        if( status == 0 )
        {
            // receive skiq_daemon_port_payload_t
            if( (resp_status == 0) &&
                (payload_len == SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE) )
            {
                // get the TX streaming port
                status = _net_receive_payload( p_conn->ctrl_sock, &resp_payload, payload_len );
                if( status == 0 )
                {
                    skiq_debug("TX streaming port is %u", ntohs(resp_payload.port));
                    p_conn->p_tx_conn->tx_stream_conn.port = ntohs(resp_payload.port);
                }
            }
            else
            {
                status = -EPROTO;
            }
            _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
        }

        if( status == 0 )
        {
            status = _init_tx_stream_conn( p_conn );
            if( status == 0 )
            {
                pthread_mutex_lock( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
                p_conn->p_tx_conn->tx_stream_conn.b_stream_running = true;
                pthread_mutex_unlock( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
            }
        }
    }
    return (status);
}

int32_t _net_tx_pre_stop_streaming( uint64_t uid )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;
    skiq_daemon_req_msg_t req_msg;
    int32_t resp_status=0;
    uint32_t payload_len=0;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );

    if( status == 0 )
    {
        // send SKIQ_DAEMON_TX_PRE_STOP_STREAMING
        status = _net_send_request( p_conn->ctrl_sock,
                                    SKIQ_DAEMON_TX_PRE_STOP_STREAMING,
                                    p_conn->ctrl_seq_num,
                                    &req_msg,
                                    0 );
        if( status == 0 )
        {
            status = _net_receive_resp( p_conn->ctrl_sock,
                                        &resp_status,
                                        p_conn->ctrl_seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
                close( p_conn->p_tx_conn->tx_stream_conn.sock_fd );
                p_conn->p_tx_conn->tx_stream_conn.sock_fd = -1;
                pthread_mutex_lock( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
                p_conn->p_tx_conn->tx_stream_conn.b_stream_running = false;
                pthread_mutex_unlock( &p_conn->p_tx_conn->tx_stream_conn.stream_mutex );
            }
        }
        if( status == 0 )
        {
            _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
        }
    }
    return (status);
}

int32_t _net_tx_stop_streaming( uint64_t uid )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;
    skiq_daemon_req_msg_t req_msg;
    int32_t resp_status=0;
    uint32_t payload_len=0;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );

    if( status == 0 )
    {
        // send SKIQ_DAEMON_TX_STOP_STREAMING
        status = _net_send_request( p_conn->ctrl_sock,
                                    SKIQ_DAEMON_TX_STOP_STREAMING,
                                    p_conn->ctrl_seq_num,
                                    &req_msg,
                                    0 );
        if( status == 0 )
        {
            status = _net_receive_resp( p_conn->ctrl_sock,
                                        &resp_status,
                                        p_conn->ctrl_seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
            }
        }
        if( status == 0 )
        {
            _net_set_ctrl_seq_num( uid, p_conn->ctrl_seq_num+1 );
        }
    }
    
    return (status);
}

int32_t _net_tx_transmit( uint64_t uid, int32_t *p_samples )
{
    int32_t status=0;
    struct daemon_conn_t *p_conn;
    struct net_tx_conn_t *p_tx_conn;
    static uint32_t seq_num=0;
    skiq_daemon_tx_header_t header;
    skiq_daemon_tx_resp_msg_t resp;

    // get the connection info
    status = _net_get_daemon_conn( uid, &p_conn );
    if( status == 0 )
    {
        p_tx_conn = p_conn->p_tx_conn;

        // send header
        header.seq_number = htonl( seq_num );

        status = _send_full_packet( p_tx_conn->tx_stream_conn.sock_fd,
                                    (uint8_t*)(&header),
                                    SKIQ_DAEMON_TX_HEADER_SIZE,
                                    MSG_MORE );
        if( status == 0 )
        {
            status = _send_full_packet( p_tx_conn->tx_stream_conn.sock_fd,
                                       (uint8_t*)(p_samples),
                                       p_tx_conn->num_bytes_to_send,
                                       0 );

            if( status == 0 )
            {
                errno = 0;
                // wait for an ACK
                if( (status=read(p_tx_conn->tx_stream_conn.sock_fd, &resp, SKIQ_DAEMON_TX_RESP_SIZE )) == SKIQ_DAEMON_TX_RESP_SIZE )
                {
                    // make sure the sequence number matches what we're expecting
                    if( ntohl(resp.seq_number) == seq_num )
                    {
                        // update the status
                        status = ntohl( resp.status );
                    }
                    else
                    {
                        skiq_error("Received TX response sequence %u (expected %u)",
                                    ntohl(resp.seq_number), seq_num);
                        status = -EPROTO;
                    }
                }
                else
                {
                    if( status < 0 )
                    {
                        status = -errno;
                    }
                    else
                    {
                        status = -EPROTO;
                    }
                }
            }
        }
        seq_num++;
    }

    return (status);
}

int32_t _send_full_packet( int sock_fd,
                           uint8_t *p_pkt,
                           uint32_t num_bytes,
                           int send_flags )
{
    int32_t status=0;
    int send_status;
    uint32_t num_bytes_remaining = num_bytes;
    uint32_t offset = 0;

    while( (num_bytes_remaining > 0) && (status==0) )
    {
        errno = 0;
        send_status = send( sock_fd, &(p_pkt[offset]), num_bytes, send_flags );
        if( send_status > 0 )
        {
            num_bytes_remaining = num_bytes_remaining - send_status;
            offset = num_bytes - num_bytes_remaining;
        }
        else
        {
            status = -errno;
        }
    }

    return (status);
}
