/**
 * @file   net_fpga_registers.c
 *
 * @brief  Network transport FPGA register related functions
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */
#include "net_transport_private.h"
#include "sidekiq_daemon_private.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>

// TODO: FPGA RMWV? (this will need to be added to the transport)
// TODO: write and verify (this will need to be added to the transport)

static int32_t _fpga_reg_read( uint64_t xport_uid, uint32_t addr, uint32_t* p_data );
static int32_t _fpga_reg_write( uint64_t xport_uid, uint32_t addr, uint32_t data );
static int32_t _fpga_reg_verify( uint64_t xport_uid, uint32_t addr, uint32_t data );
static int32_t _fpga_reg_write_and_verify( uint64_t xport_uid, uint32_t addr, uint32_t data );

int32_t _fpga_reg_read( uint64_t xport_uid, uint32_t addr, uint32_t* p_data )
{ 
    int32_t status=0;
    skiq_daemon_resp_msg_t resp_msg;
    skiq_daemon_read_fpga_req_t read_fpga;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    uint32_t seq_num=0;
    int ctrl_sock=-1;

    if( ((status=_net_get_ctrl_sock( xport_uid, &ctrl_sock)) == 0) &&
        ((status=_net_get_ctrl_seq_num( xport_uid, &seq_num))==0) )
    {
        read_fpga.payload.addr = htonl( addr );
        status = _net_send_request( ctrl_sock,
                                    SKIQ_DAEMON_READ_FPGA_REG,
                                    seq_num,
                                    &read_fpga,
                                    SKIQ_DAEMON_READ_FPGA_REQ_PAYLOAD_SIZE );
        if( status == 0 )
        {
            resp_msg.status=0; // default value to get rid of compiler warning...it gets set in the call below
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( (status == 0) && (resp_status == 0) && (payload_len == sizeof(uint32_t)) )
            {
                // read the value
                status = _net_receive_payload( ctrl_sock, p_data, payload_len );
                if( status == 0 )
                {
                    *p_data = ntohl( *p_data );
                }
            }
            else
            {
                if( status == 0 )
                {
                    if( resp_status != 0 )
                    {
                        status = resp_msg.status;
                    }
                    else
                    {
                        // payload_len not valid
                        status = -EPROTO;
                    }
                }
            }
        }
        else
        {
            status = -ENODEV;
        }
        seq_num++;
        if( status == 0 )
        {
            status = _net_set_ctrl_seq_num( xport_uid, seq_num );
        }
    }

    return (status);
}

int32_t _fpga_reg_write( uint64_t xport_uid, uint32_t addr, uint32_t data )
{
    int32_t status=0;
    skiq_daemon_write_fpga_t write_fpga;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    uint32_t seq_num=0;
    int ctrl_sock=-1;

    if( ((status=_net_get_ctrl_sock( xport_uid, &ctrl_sock)) == 0) &&
        ((status=_net_get_ctrl_seq_num( xport_uid, &seq_num))==0) )
    {
        write_fpga.payload.addr = htonl( addr );
        write_fpga.payload.val = htonl( data );

        status = _net_send_request( ctrl_sock,
                                    SKIQ_DAEMON_WRITE_FPGA_REG,
                                    seq_num,
                                    &write_fpga,
                                    SKIQ_DAEMON_WRITE_FPGA_PAYLOAD_SIZE );
        if( status == 0 )
        {
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
            }
        }
        seq_num++;
        if( status == 0 )
        {
            status = _net_set_ctrl_seq_num( xport_uid, seq_num );
        }
    }

    return (status);
}

int32_t _fpga_reg_verify( uint64_t xport_uid, uint32_t addr, uint32_t data )
{
    int32_t status=0;
    skiq_daemon_fpga_reg_verify_t fpga_reg_verify;
    int32_t resp_status;
    uint32_t payload_len=0;
    uint32_t seq_num;
    int ctrl_sock;

    if( ((status=_net_get_ctrl_sock( xport_uid, &ctrl_sock)) == 0) &&
        ((status=_net_get_ctrl_seq_num( xport_uid, &seq_num))==0) )
    {
        fpga_reg_verify.payload.addr = htonl( addr );
        fpga_reg_verify.payload.val = htonl( data );

        status = _net_send_request( ctrl_sock,
                                    SKIQ_DAEMON_FPGA_REG_VERIFY,
                                    seq_num,
                                    &fpga_reg_verify,
                                    SKIQ_DAEMON_FPGA_REG_VERIFY_PAYLOAD_SIZE );
        if( status == 0 )
        {
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
            }
        }
        seq_num++;
        if( status == 0 )
        {
            status = _net_set_ctrl_seq_num( xport_uid, seq_num );
        }
    }

    return (status);
}

int32_t _fpga_reg_write_and_verify( uint64_t xport_uid, uint32_t addr, uint32_t data )
{
    int32_t status=0;
    skiq_daemon_fpga_reg_write_and_verify_t fpga_reg_write_and_verify;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    uint32_t seq_num;
    int ctrl_sock;

    if( ((status=_net_get_ctrl_sock( xport_uid, &ctrl_sock)) == 0) &&
        ((status=_net_get_ctrl_seq_num( xport_uid, &seq_num))==0) )
    {
        fpga_reg_write_and_verify.payload.addr = htonl( addr );
        fpga_reg_write_and_verify.payload.val = htonl( data );

        status = _net_send_request( ctrl_sock,
                                    SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY,
                                    seq_num,
                                    &fpga_reg_write_and_verify,
                                    SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY_PAYLOAD_SIZE );
        if( status == 0 )
        {
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( status == 0 )
            {
                status = resp_status;
            }
            else
            {
                status = -ENODEV;
            }
        }
        seq_num++;
        if( status == 0 )
        {
            status = _net_set_ctrl_seq_num( xport_uid, seq_num );
        }
    }

    return (status);
}

skiq_xport_fpga_functions_t net_xport_fpga_funcs = {
    .fpga_reg_read = _fpga_reg_read,
    .fpga_reg_write = _fpga_reg_write,
    .fpga_down = NULL,
    .fpga_down_reload = NULL,
    .fpga_up = NULL,
    .fpga_reg_verify = _fpga_reg_verify,
    .fpga_reg_write_and_verify = _fpga_reg_write_and_verify,
};
