/**
 * @file   net_rx.c
 *
 * @brief
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
/***** INCLUDES *****/ 

#include <stdint.h>
#include <stdbool.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <inttypes.h>

#include "sidekiq_types.h"
#include "sidekiq_xport_types.h"
#include "sidekiq_private.h"
#include "sidekiq_daemon_private.h"
#include "net_transport_private.h"
#include "sidekiq_card_mgr.h"

#define RX_CONFIG_INITIALIZER          \
    {                                  \
       .usleep_per_skiq_rx = 1000000,  \
       .num_bytes_per_trans = SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES, \
       .buffered = 1,                  \
    }
    
static ARRAY_WITH_DEFAULTS(skiq_daemon_rx_configure_payload_t, _rx_config, SKIQ_MAX_NUM_CARDS, RX_CONFIG_INITIALIZER);

#define MAX_NUM_PKTS_FLUSHED (20) // in debugging, never seen this more than 5, so make it really large

static int32_t _rx_configure( uint64_t xport_uid, uint32_t aggregate_data_rate );
static int32_t _rx_set_buffered( uint64_t xport_uid, bool buffered );

static int32_t _rx_start_streaming( uint64_t xport_uid, skiq_rx_hdl_t hdl );
static int32_t _rx_stop_streaming( uint64_t xport_uid, skiq_rx_hdl_t hdl );

static int32_t _rx_pause_streaming( uint64_t xport_uid );
static int32_t _rx_resume_streaming( uint64_t xport_uid );

static int32_t _rx_flush( uint64_t xport_uid );

static int32_t _rx_set_transfer_timeout( uint64_t xport_uid, const int32_t timeout_us );

static int32_t _rx_receive( uint64_t xport_uid, uint8_t **pp_data, uint32_t *p_data_len );

skiq_xport_rx_functions_t net_xport_rx_funcs = {
    .rx_configure            = _rx_configure, // this can be used to adjust our receive timeout?
    .rx_set_block_size       = NULL,
    .rx_set_buffered         = _rx_set_buffered, 
    .rx_start_streaming      = _rx_start_streaming,
    .rx_stop_streaming       = _rx_stop_streaming,
    .rx_pause_streaming      = _rx_pause_streaming,
    .rx_resume_streaming     = _rx_resume_streaming,
    .rx_flush                = _rx_flush,
    .rx_set_transfer_timeout = _rx_set_transfer_timeout, // configure our socket receive timeout
    .rx_receive              = _rx_receive,
};

int32_t _rx_configure( uint64_t xport_uid, uint32_t aggregate_data_rate )
{
    int32_t status=0;
    uint32_t usleep_per_pkt=0;
    uint8_t client_card=0;

    if( aggregate_data_rate != 0 )
    {
        status = card_mgr_get_card( xport_uid, skiq_xport_type_net, &client_card );
        if( status == 0 )
        {
            usleep_per_pkt = (uint32_t)((double)(SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES) / ((double)(aggregate_data_rate)/1000000.0));
            _rx_config[client_card].usleep_per_skiq_rx = usleep_per_pkt;

            // update the RX configuration (this will send a message to the daemon too)
            status = _net_rx_set_config( xport_uid, &(_rx_config[client_card]) );
        }
    }

    return (status);
}

int32_t _rx_set_buffered( uint64_t xport_uid, bool buffered )
{
    int32_t status=0;
    uint32_t num_bytes_per_trans = SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES;
    uint8_t client_card=0;

    status = card_mgr_get_card( xport_uid, skiq_xport_type_net, &client_card );
    if( status == 0 )
    {
        if( buffered == true )
        {
            // TODO: handle more bytes per trans...we need to be able to receive multiple pkts per receive call
            num_bytes_per_trans = SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES;
        }
        _rx_config[client_card].num_bytes_per_trans = num_bytes_per_trans;
        _rx_config[client_card].buffered = buffered;

        status = _net_rx_set_config( xport_uid, &(_rx_config[client_card]) );
    }

    return (status);
}

int32_t _rx_start_streaming( uint64_t xport_uid, skiq_rx_hdl_t hdl )
{
    int32_t status=0;

    skiq_debug("Starting network streaming\n");

    // the handle should be started in the normal mechanisms in the FPGA, but
    // we need to startup threads and notify daemon if we weren't already running

    status = _net_rx_start_streaming_for_hdl( xport_uid, hdl );

    return (status);
}

int32_t _rx_stop_streaming( uint64_t xport_uid, skiq_rx_hdl_t hdl )
{
    int32_t status=0;

    // notify that we're done streaming for this handle

    status = _net_rx_stop_streaming_for_hdl( xport_uid, hdl );

    return (status);
}

int32_t _rx_pause_streaming( uint64_t xport_uid )
{
    int32_t status=0;
    int ctrl_sock=0;
    uint32_t seq_num;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    skiq_daemon_req_msg_t req_msg;

    if( (status=_net_get_ctrl_sock( xport_uid, &ctrl_sock )) == 0 &&
        (status=_net_get_ctrl_seq_num( xport_uid, &seq_num )) == 0 )
    {
        // SKIQ_DAEMON_RX_PAUSE_STREAMING
        status = _net_send_request( ctrl_sock, SKIQ_DAEMON_RX_PAUSE_STREAMING, seq_num, &req_msg, 0 );
        if( status == 0 )
        {
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( (status == 0) && (resp_status != 0) )
            {
                status = resp_status;
            }
            else if( status != 0 )
            {
                skiq_error("Failed to receive response to pause streaming (status=%d) (UID=%" PRIu64 ")\n",
                    status, xport_uid);
            }
        }

    }

    return (status);
}

int32_t _rx_resume_streaming( uint64_t xport_uid )
{
    int32_t status=0;
    int ctrl_sock=0;
    uint32_t seq_num;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    skiq_daemon_req_msg_t req_msg;

    if( (status=_net_get_ctrl_sock( xport_uid, &ctrl_sock )) == 0 &&
        (status=_net_get_ctrl_seq_num( xport_uid, &seq_num )) == 0 )
    {
        // SKIQ_DAEMON_RX_RESUME_STREAMING
        status = _net_send_request( ctrl_sock, SKIQ_DAEMON_RX_RESUME_STREAMING, seq_num, &req_msg, 0 );
        if( status == 0 )
        {
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( (status == 0) && (resp_status != 0) )
            {
                status = resp_status;
            }
            else if( status != 0 )
            {
                skiq_error("Failed to receive response to resume streaming(status=%d) (UID=%" PRIu64 ")\n",
                    status, xport_uid);
            }
        }
    }

    return (status);
}

int32_t _rx_flush( uint64_t xport_uid )
{
    int32_t status=0;
    int ctrl_sock=0;
    uint32_t seq_num;
    int32_t resp_status=0;
    uint32_t payload_len=0;
    skiq_daemon_req_msg_t req_msg;
    uint32_t num_flushed=0;
    uint8_t *p_data;
    uint32_t data_len;

    if( (status=_net_get_ctrl_sock( xport_uid, &ctrl_sock )) == 0 &&
        (status=_net_get_ctrl_seq_num( xport_uid, &seq_num )) == 0 )
    {    
        // SKIQ_DAEMON_RX_FLUSH
        status = _net_send_request( ctrl_sock, SKIQ_DAEMON_RX_FLUSH, seq_num, &req_msg, 0 );
        if( status == 0 )
        {
            status = _net_receive_resp( ctrl_sock,
                                        &resp_status,
                                        seq_num,
                                        &payload_len );
            if( (status == 0) && (resp_status != 0) )
            {
                status = resp_status;
            }
            else if( status != 0 )
            {
                skiq_error("Failed to receive response to flush(status=%d) (UID=%" PRIu64 ")\n",
                    status, xport_uid);
            }
            else
            {
                /* the daemon has flushed...let's make sure there's nothing left in the socket */
                do
                {
                    status = _net_rx_get_pkt( xport_uid, &p_data, &data_len );
                    if( status == skiq_rx_status_success )
                    {
                        num_flushed++;
                    }
                } while( (status == skiq_rx_status_success) && (num_flushed<MAX_NUM_PKTS_FLUSHED) );
                if( num_flushed == MAX_NUM_PKTS_FLUSHED )
                {
                    skiq_warning( "Maximum number of packets flushed on network transport, stale data may exist" );
                }         
            }
        }
    }

    return (status);
}

int32_t _rx_set_transfer_timeout( uint64_t xport_uid, const int32_t timeout_us )
{
    int32_t status=-ENOTSUP;

    // TODO: set our read socket timeout?

    return (status);
}

int32_t _rx_receive( uint64_t xport_uid, uint8_t **pp_data, uint32_t *p_data_len )
{
    int32_t status=skiq_rx_status_no_data; 
    skiq_rx_block_t *p_rx_block;

    status = _net_rx_get_pkt( xport_uid, pp_data, p_data_len );
    if( status == 0 )
    {
        p_rx_block = (skiq_rx_block_t*)(*pp_data);
        /* the daemon can send dummy packets...if the timestamps and first samples
            are all 0s, assume its a dummy and drop it */
        if( (p_rx_block->sys_timestamp == 0) &&
            (p_rx_block->rf_timestamp ==0) &&
            (p_rx_block->data[0] == 0) &&
            (p_rx_block->data[1] == 0) )
        {
            status = skiq_rx_status_no_data;
        }
        else
        {
            status = skiq_rx_status_success;
        }
    }

    return (status);
}
