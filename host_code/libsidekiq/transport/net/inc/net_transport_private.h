#ifndef __NET_TRANSPORT_PRIVATE_H__
#define __NET_TRANSPORT_PRIVATE_H__

/*! \file net_transport_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */
/***** INCLUDES *****/

#include "sidekiq_api.h"
#include "sidekiq_daemon_private.h"

/* transport functions */
extern skiq_xport_fpga_functions_t net_xport_fpga_funcs;
extern skiq_xport_rx_functions_t net_xport_rx_funcs;
extern skiq_xport_tx_functions_t net_xport_tx_funcs;

#define UNINITIALIZED_SOCK (-1) // represents an unitialized socket
#define MAX_IP_ADDR_LEN (46) // +1 for \0  https://stackoverflow.com/questions/1076714/max-length-for-client-ip-address
    
// contains information on active connect to daemon control port
struct daemon_conn_t
{
    uint64_t uid;
    int ctrl_sock;
    uint32_t ctrl_seq_num;
    struct net_rx_conn_t *p_rx_conn;
    struct net_tx_conn_t *p_tx_conn;
};

/*****************************************************************************/
/** The _connect_daemon_socket function is responsible for connecting a socket
    to the parameters specified.

    @param[in] p_ip_addr IP address
    @param[in] port port number

    @return int32_t  negative is an errno, positive is the socket descriptor
*/
int32_t _connect_daemon_socket( const char* p_ip_addr, uint16_t port );

/*****************************************************************************/
/** The _net_get_uid function generates a UID based on IP / port / server card.

    @param[in] p_ip string of IP address 
    @param[in] port port number
    @param[in] server_card_num card number on server

    @return uint64_t uid representing input parameters
*/
uint64_t _net_get_uid( const char* p_ip, uint16_t port, uint8_t server_card_num );

/*****************************************************************************/
/** The _net_get_ip_from_uid function provides the IP address contained in 
    the UID.

    @param[in] uid ID to map IP address from

    @return const char* string form of IP address
*/
const char* _net_get_ip_from_uid( uint64_t uid );

/*****************************************************************************/
/** The _net_get_port_from_uid function provides the port number contained in 
    the UID.

    @param[in] uid ID to map port number from

    @return uint16_t port number
*/
uint16_t _net_get_port_from_uid( uint64_t uid );

/*****************************************************************************/
/** The _net_get_card_num_from_uid function provides the server card contained in 
    the UID.

    @param[in] uid ID to map server card number from

    @return uint8_t server card number
*/
uint8_t _net_get_card_num_from_uid( uint64_t uid );

/*****************************************************************************/
/** The _net_add_conn adds the connection information based on the UID.

    @param[in] uid unique identifier of connection

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_add_conn( uint64_t uid );

/*****************************************************************************/
/** The _net_remove_conn removes the connection information based on the UID.

    @param[in] uid unique identifier of connection

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_remove_conn( uint64_t uid );

/*****************************************************************************/
/** The _net_get_ctrl_sock function provides the control socket descriptor 
    based on the UID.

    @param[in] uid unique identifier of connection
    @param[out] p_ctrl_sock pointer to where to store control socket 

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_get_ctrl_sock( uint64_t uid, int *p_ctrl_sock );

/*****************************************************************************/
/** The _net_set_ctrl_sock function sets the control socket descriptor to the 
    value provided for the UID.

    @param[in] uid unique identifier of connection
    @param[out] ctrl_sock control socket descriptor

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_set_ctrl_sock( uint64_t uid, int ctrl_sock );

/*****************************************************************************/
/** The _net_get_ctrl_seq_num function provides the control sequence number
    based on the UID.

    @param[in] uid unique identifier of connection
    @param[out] p_seq_num pointer to where to store control sequence number

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_get_ctrl_seq_num( uint64_t uid, uint32_t *p_seq_num );

/*****************************************************************************/
/** The _net_set_ctrl_seq_num function sets the control sequence number to the 
    value provided for the UID.

    @param[in] uid unique identifier of connection
    @param[out] seq_num value to update the sequence number to

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_set_ctrl_seq_num( uint64_t uid, uint32_t seq_num );

/*****************************************************************************/
/** The _net_get_ctrl_sock_and_seq_num function gets the control sequence number 
    to the and socket descriptor for the UID.

    @param[in] uid unique identifier of connection
    @param[out] p_conn pointer to daemon_conn for the UID provided

    @return int32_t 0 if successful, otherwise negative errno
*/
int32_t _net_get_daemon_conn( uint64_t uid, struct daemon_conn_t **pp_conn );

/*****************************************************************************/
/** The _net_send_request function is responsible for sending a request to 
    the socket specified.

    @param[in] sock FD for the socket to send to
    @param[in] type request type to send
    @param[in] seq_num sequence number of message to send
    @param[in] p_msg pointer to contents of message to send
    @param[in] payload_len length of payload 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _net_send_request( int32_t sock,
                           uint32_t type,
                           uint32_t seq_num,
                           void* p_msg,
                           uint32_t payload_len );

/*****************************************************************************/
/** The _net_send_resp function is responsible for sending a response message to 
    the socket specified.

    @param[in] sock FD for the socket to send to
    @param[in] resp_status status contained in response message
    @param[in] seq_num sequence number of message to send
    @param[in] payload_len length of payload 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _net_send_resp( int32_t sock,
                        int32_t resp_status,
                        uint32_t seq_num,
                        uint32_t payload_len );

/*****************************************************************************/
/** The _net_send_payload function is responsible for sending a raw payload
    to the socket specified.

    @param[in] sock FD for the socket to send to
    @param[in] p_payload pointer to payload contents to send
    @param[in] payload_len length of payload 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _net_send_payload( int32_t sock,
                           void *p_payload,
                           uint32_t payload_len );

/*****************************************************************************/
/** The _net_receive_resp function is responsible for receiving a response 
    message from the socket specified.

    @param[in] sock FD for the socket to send to
    @param[out] p_resp_status pointer to where to store the response status
    @param[in] expected_seq_num expected sequence number of response message
    @param[out] p_payload_len length of payload contained in response message

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _net_receive_resp( int32_t sock,
                           int32_t *p_resp_status,
                           uint32_t expected_seq_num,
                           uint32_t *p_payload_len );

/*****************************************************************************/
/** The _net_receive_payload function is responsible for receiving the payload
    from the socket specified.

    @param[in] sock FD for the socket to send to
    @param[out] p_payload pointer to where to store the payload contents
    @param[in] payload_len length of payload to receive

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _net_receive_payload( int32_t sock,
                              void *p_payload,
                              uint32_t payload_len );

int32_t _net_rx_set_config( uint64_t uid, skiq_daemon_rx_configure_payload_t *p_rx_config );
                           
int32_t _net_rx_start_streaming_for_hdl( uint64_t uid, skiq_rx_hdl_t hdl );
int32_t _net_rx_stop_streaming_for_hdl( uint64_t uid, skiq_rx_hdl_t hdl );

int32_t _net_rx_get_pkt( uint64_t uid, uint8_t **pp_data, uint32_t *p_data_len );

int32_t _net_tx_initialize( uint64_t uid, 
                            skiq_tx_transfer_mode_t transfer_mode,
                            uint32_t num_bytes_to_send,
                            uint8_t num_send_threads,
                            int32_t priority );
int32_t _net_tx_pre_stop_streaming( uint64_t uid );
int32_t _net_tx_start_streaming( uint64_t uid );
int32_t _net_tx_stop_streaming( uint64_t uid );
int32_t _net_tx_transmit( uint64_t uid, int32_t *p_samples );



#endif  /* __NET_TRANSPORT_PRIVATE_H__ */
