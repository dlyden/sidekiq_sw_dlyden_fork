#ifndef __NET_TRANSPORT_H__
#define __NET_TRANSPORT_H__

/*! \file net_transport.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/
#include "sidekiq_api.h"

#define SUBCLASS_NET 'N'

/***** GLOBAL VARIABLES  *****/

/** @brief The exported transport card functions to be used by libsidekiq. */
extern skiq_xport_card_functions_t net_xport_card_funcs;


#endif  /* __NET_TRANSPORT_H__ */
