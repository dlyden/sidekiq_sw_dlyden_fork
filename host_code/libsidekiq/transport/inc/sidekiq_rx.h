/**
 * @file   sidekiq_rx.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __SIDEKIQ_RX_H__
#define __SIDEKIQ_RX_H__

#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_types.h"

extern int32_t skiq_xport_rx_configure( uint8_t card, uint32_t aggregate_data_rate );
extern int32_t skiq_xport_rx_set_block_size( uint8_t card, uint32_t block_size );
extern int32_t skiq_xport_rx_set_buffered( uint8_t card, bool buffered );

extern int32_t skiq_xport_rx_start_streaming( uint8_t card, skiq_rx_hdl_t hdl );
extern int32_t skiq_xport_rx_stop_streaming( uint8_t card, skiq_rx_hdl_t hdl );

extern int32_t skiq_xport_rx_pause_streaming( uint8_t card );
extern int32_t skiq_xport_rx_resume_streaming( uint8_t card );

extern int32_t skiq_xport_rx_flush( uint8_t card );

extern int32_t skiq_xport_rx_set_transfer_timeout( const uint8_t card, const int32_t timeout_us );
extern int32_t skiq_xport_rx_get_transfer_timeout( const uint8_t card, int32_t *p_timeout_us );

extern int32_t skiq_xport_rx_receive( uint8_t card, uint8_t **pp_data, uint32_t *p_data_len );

#endif  /* __SIDEKIQ_RX_H__ */
