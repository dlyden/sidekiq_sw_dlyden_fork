/**
 * @file   sidekiq_fpga.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016,2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __SIDEKIQ_FPGA_H__
#define __SIDEKIQ_FPGA_H__

#include <stdint.h>
#include "bit_ops.h"

/* Helper MACRO that performs a read, modify, write, verify sequence for a given
 * card, first reading the named register, setting the value of the bitfield
 * name, then writing and verifying the register contents
 *
 * See https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html
 */
#define sidekiq_fpga_reg_RMWV(_card,_val,_bfname,_regname)              \
    ({                                                                  \
        uint32_t _reg_data = 0, _status;                                \
        _status = sidekiq_fpga_reg_read(_card, _regname, &_reg_data);   \
        RBF_SET(_reg_data,_val,_bfname);                                \
        if ( _status == 0) _status = sidekiq_fpga_reg_write(_card, _regname, _reg_data); \
        if ( _status == 0) _status = sidekiq_fpga_reg_verify(_card, _regname, _reg_data); \
        _status;                                                        \
    })

/* Helper MACRO that performs a read, modify, write (NO verify) sequence for a given
 * card, first reading the named register, setting the value of the bitfield
 * name, then writing the register contents
 *
 * See https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html
 */
#define sidekiq_fpga_reg_RMW(_card,_val,_bfname,_regname)               \
    ({                                                                  \
        uint32_t _reg_data = 0, _status;                                \
        _status = sidekiq_fpga_reg_read(_card, _regname, &_reg_data);   \
        RBF_SET(_reg_data,_val,_bfname);                                \
        if ( _status == 0) _status = sidekiq_fpga_reg_write(_card, _regname, _reg_data); \
        _status;                                                        \
    })

extern int32_t sidekiq_xport_fpga_down( uint8_t card );
extern int32_t sidekiq_xport_fpga_down_reload( uint8_t card, uint32_t addr );
extern int32_t sidekiq_xport_fpga_up( uint8_t card );

extern int32_t sidekiq_fpga_reg_read(uint8_t card, uint32_t addr, uint32_t* p_data);
extern int32_t sidekiq_fpga_reg_write(uint8_t card, uint32_t addr, uint32_t data);
extern int32_t sidekiq_fpga_reg_verify(uint8_t card, uint32_t addr, uint32_t data);
extern int32_t sidekiq_fpga_reg_write_and_verify(uint8_t card, uint32_t addr, uint32_t data);

extern int32_t sidekiq_fpga_reg_read_64(uint8_t card,
                                        uint32_t addr_high, uint32_t addr_low,
                                        uint64_t* p_data);
extern int32_t sidekiq_fpga_reg_write_64(uint8_t card,
                                         uint32_t addr_high, uint32_t addr_low,
                                         uint64_t data);

extern int32_t sidekiq_fpga_bring_up( uint8_t card );

#endif  /* __SIDEKIQ_FPGA_H__ */
