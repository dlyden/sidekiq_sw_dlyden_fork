#ifndef __SIDEKIQ_CARD_H__
#define __SIDEKIQ_CARD_H__

/*! \file xport_card.h
 * \brief This file contains the public interface of the xport card
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 5/31/2016   MEZ   Created file
 *
 * </pre>
 */


#include <stdint.h>

#include "sidekiq_xport_types.h"

/* custom transport subclass is the uppermost byte of its UID */
#define SUBCLASS(_uid)                  ((uint8_t)(((_uid) >> 56) & 0xFF))
#define SUBCLASS_ANY                    (UINT8_MAX)

extern skiq_xport_type_t skiq_xport_card_get_type( uint8_t card );
extern skiq_xport_init_level_t skiq_xport_card_get_level( uint8_t card );

extern int32_t skiq_xport_card_probe( skiq_xport_type_t type );

extern int32_t skiq_xport_card_hotplug( skiq_xport_type_t type,
                                        uint8_t no_probe_cards[],
                                        uint8_t nr_no_probe_cards );

extern int32_t skiq_xport_card_init( skiq_xport_type_t type,
                                     skiq_xport_init_level_t level,
                                     uint8_t card );

extern int32_t skiq_xport_card_exit( uint8_t card );

#endif  /* __SIDEKIQ_CARD_H__ */
