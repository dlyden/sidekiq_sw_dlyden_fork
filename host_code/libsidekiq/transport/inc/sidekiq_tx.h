/**
 * @file   sidekiq_tx.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __SIDEKIQ_TX_H__
#define __SIDEKIQ_TX_H__

#include <stdint.h>

#include "sidekiq_types.h"

extern int32_t skiq_xport_tx_initialize( uint8_t card,
                                         skiq_tx_transfer_mode_t tx_transfer_mode,
                                         uint32_t num_bytes_to_send,
                                         uint8_t num_send_threads,
                                         int32_t priority,
                                         skiq_tx_callback_t tx_complete_cb );

extern int32_t skiq_xport_tx_start_streaming( uint8_t card, skiq_tx_hdl_t hdl );
extern int32_t skiq_xport_tx_pre_stop_streaming( uint8_t card, skiq_tx_hdl_t hdl );
extern int32_t skiq_xport_tx_stop_streaming( uint8_t card, skiq_tx_hdl_t hdl );

extern int32_t skiq_xport_tx_transmit( uint8_t card, skiq_tx_hdl_t hdl, int32_t *p_samples, void *p_private );

#endif  /* __SIDEKIQ_TX_H__ */

