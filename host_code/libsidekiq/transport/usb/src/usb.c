/**
 * @file   usb.c
 * @author  <info@epiq-solutions>
 * @date   Tue Nov 27 14:05:00 2018
 * 
 * @brief  Common USB transport / OOB functions
 * 
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE                 /* See feature_test_macros(7) */
#endif

#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "libusb.h"

#include "usb_interface.h"
#include "usb_private.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_private.h"

/***** DEFINES *****/


/***** MACROS *****/

/** @brief Macro used for identifying if the USB is initialized/valid. */
#define IS_USB_VALID(card_ctrl_rx_tx) \
    ((NULL == card_ctrl_rx_tx.p_handle) ? false : true)

/** @brief Macro used to identify a Sidekiq USB descriptor. */
#define IS_SKIQ_DESCRIPTOR(desc) \
    (((desc.idVendor == VENDOR_ID) && (desc.idProduct == PRODUCT_ID)) ? \
    true :                                                              \
    false)


/***** LOCAL VARIABLES *****/

/** @brief USB context for all cards. */
static struct libusb_context* _p_context = NULL;

/** @brief Local storage for USB cards. */
static struct usb_card _usb[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = USB_CARD_INITIALIZER,
};

/** @brief Counter used for tracking the total number of active cards. This is
    primarily used to decide when to spawn/close the event thread. */
static uint32_t _active_cards = 0;

/** @brief Thread dedicated to polling for libusb events. */
static pthread_t _event_thread;


/***** LOCAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Reclaim any and all USB based transfers back from libusb and direct
    the transfer to its specified call back function. A timeout value is
    specified in order to keep libusb's handle events function from blocking
    forever if no transfers have completed. Note that the calling function
    should have some means of its own to check if the transfer is completed
    (i.e. set some variable in the callback function).

    NOTE: The libusb_handle_events_timeout_completed function returns if either
    a transfer completes, a USB handle is closed, or the timeout occurs. Keep
    this in mind when trying to return from this function and close the thread.

    @param[in] arg      Not used in this function.

    @return void
*/
static void _get_returned_usb_transfers(void* const arg)
{
    struct timeval timeout = {.tv_sec = 1, 0};

    while( (0 != _active_cards) && (NULL != _p_context) )
    {
        libusb_handle_events_timeout_completed(_p_context, &timeout, NULL);
    }
}

/*****************************************************************************/
/** @brief Initialize the USB interface for a given card.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] p_handle     Pointer to the USB handle to associate.
    @param[in] init_level   The desired initialization level.

    @return Nonzero value on error, zero on success.
*/
static int32_t _init_usb_card(
    uint8_t index,
    struct libusb_device_handle* p_handle,
    skiq_xport_init_level_t init_level)
{
    int32_t err = -1;

    switch( init_level )
    {
    case( skiq_xport_init_level_full ):

        // Configure Rx interface.
        err = usb_rx_init(&(_usb[index].rx), p_handle, index);
        if( 0 != err )
        {
            return (err);
        }

        // fall through and initialize control interface next

    case( skiq_xport_init_level_basic ):

        // Configure control interface.
        err = usb_ctrl_init(&(_usb[index].ctrl), p_handle, index);
        if( 0 != err )
        {
            return (err);
        }
        break;

    case( skiq_xport_init_level_unknown ):
    default:
        return (err);
    }

    _active_cards++;
    _usb[index].p_handle = p_handle;
    _usb[index].init_level = init_level;
    _usb[index].id = index;

    if( 1 == _active_cards )
    {
        // TODO: error check
        pthread_create(&(_event_thread),
                       NULL,
                       (void*) &_get_returned_usb_transfers,
                       NULL);
    }

    // Disable GPIF FIFO initially until streaming is enabled.
    err = usb_gpif_fifo_disable(index);
    if( 0 != err )
    {
        return (err);
    }

    return (0);
}

/*****************************************************************************/
/** @brief Free the USB interface for a given card.

    @param[in] index         index of Sidekiq

    @return void
*/
static void _free_usb_card(
    uint8_t index)
{
    if( false == IS_USB_VALID(_usb[index]) )
    {
        return;
    }

    switch( _usb[index].init_level )
    {
    case( skiq_xport_init_level_full ):

        usb_rx_free(&(_usb[index].rx));

        // fall through to basic level

    case( skiq_xport_init_level_basic ):

        usb_ctrl_free(&(_usb[index].ctrl));
        break;

    case( skiq_xport_init_level_unknown ):
    default:
        return;
    }

    _active_cards--;
    _usb[index].init_level = skiq_xport_init_level_unknown;
    _usb[index].id = 0;
    libusb_close(_usb[index].p_handle);
    _usb[index].p_handle = NULL;

    if( 0 == _active_cards )
    {
        pthread_join(_event_thread, NULL);
    }
}

/***** GLOBAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Probes the USB interface for the provided serial numbers if
    *p_num_cards=0. Otherwise, searches for any Sidekiq cards on USB.

    @param[out] p_xport_id       list of USB interfacesfound
    @param[out] num_cards        number of cards in list

    @return Nonzero value on error, zero on success.
*/
int32_t usb_probe(
    struct usb_xport_id *p_xport_id,
    uint8_t *p_num_cards )
{
    struct libusb_device **pp_dev_list;     // list of USB devices
    struct libusb_device *p_device;         // current device
    struct libusb_device_descriptor desc;

    ssize_t num_devices = 0;
    uint16_t i = 0;
    int32_t err = 0;

    if(_p_context == NULL)
    {
        errno = 0;
        err = libusb_init(&_p_context);
        if( err != 0 )
        {
            if ( errno == ENOENT )
            {
                *p_num_cards = 0;
                return (0);
            }
            else
            {
                _skiq_log(SKIQ_LOG_ERROR, "libusb initialization failed with errno %d\n", errno);
                return (-1);
            }
        }
    }

    *p_num_cards = 0;

    // get the list of USB devices available
    num_devices = libusb_get_device_list(_p_context, &pp_dev_list);

    // Iterate through each device found and see if it is a Sidekiq.
    for( i = 0; i < num_devices; i++ )
    {
        p_device = pp_dev_list[i];

        // get the descriptor
        err = libusb_get_device_descriptor(p_device, &desc);
        if( (0 == err) && (true == IS_SKIQ_DESCRIPTOR(desc)) )
        {
            p_xport_id[*p_num_cards].port_num = libusb_get_port_number( p_device );
            p_xport_id[*p_num_cards].bus = libusb_get_bus_number( p_device );
            p_xport_id[*p_num_cards].addr = libusb_get_device_address( p_device );

            *(p_num_cards) = *(p_num_cards) + 1;
            if( *(p_num_cards) == SKIQ_MAX_NUM_CARDS )
            {
                skiq_error("Maximum number of Sidekiq cards detected on USB, halting probe\n" );
                break;
            }
        }
    }

    // Free and unreference all the devices.
    libusb_free_device_list(pp_dev_list, 1);

    /* only exit libusb if there are no remaining active cards */
    if(_active_cards == 0)
    {
        libusb_exit(_p_context);
        _p_context = NULL;
    }

    return (err);
}

/*****************************************************************************/
/** @brief Initilizes the USB interface for the provided cards.

    @param[in] level            init level
    @param[in] index            index of card to initialize
    @param[in] p_id             pointer to xport_id of card to init

    @return Nonzero value on error, zero on success.
*/
int32_t usb_init( skiq_xport_init_level_t level, uint8_t index, struct usb_xport_id *p_id )
{
    int32_t err=0;
    ssize_t num_devices = 0;

    struct libusb_device **pp_dev_list;     // list of USB devices
    struct libusb_device *p_device;        // current device
    struct libusb_device_descriptor desc; // USB descriptor of p_device
    struct libusb_device_handle* p_handle;

    uint32_t i=0;
    bool found_req = false;

    if( NULL == _p_context )
    {
        err = libusb_init(&_p_context);
        if( err != 0 )
        {
            return (-1);
        }
    }

    // get the list of USB devices available
    num_devices = libusb_get_device_list(_p_context, &pp_dev_list);
    // look through the USB devices for the one matching our card
    for( i = 0; ((i < num_devices) && (found_req==false)); i++ )
    {
        p_device = pp_dev_list[i];

        // get the descriptor
        err = libusb_get_device_descriptor(p_device, &desc);
        if( (0 == err) && (true == IS_SKIQ_DESCRIPTOR(desc)) )
        {
            // see if the port num match what we're looking for
            if ( ( p_id->port_num == libusb_get_port_number(p_device) ) &&
                 ( p_id->bus == libusb_get_bus_number(p_device) ) &&
                 ( p_id->addr == libusb_get_device_address(p_device) ) )
            {
                found_req = true;
            }
        }
    }

    if( true == found_req )
    {
        // open a handle
        err = libusb_open(p_device, &p_handle);
        if ( 0 == err )
        {
            // claim the USB interface
            err = libusb_claim_interface(p_handle, 0 );
            if( 0 == err )
            {
                _init_usb_card(index, p_handle, level);
            }
        }
    }

    // free dev list and unreference all the devices
    libusb_free_device_list(pp_dev_list, 1);

    return (err);
}

/*****************************************************************************/
/** @brief Frees the USB interface for the provided cards.

    @param[in] index index of card to free

    @return void
*/
void usb_free( uint8_t index )
{

    if( true == IS_USB_VALID(_usb[index]) )
    {
        _free_usb_card(index);
    }

    if( (0 == _active_cards) && (NULL != _p_context) )
    {
        libusb_exit(_p_context);
        _p_context = NULL;
    }
}

/*****************************************************************************/
/** @brief Gets the control interface for a given index. Note, if unititialized,
    this function will return an error.

    @param[in] index        index number of Rx interface to obtain.
    @param[out] pp_ctrl     pointer to be updated with address of ctrl interface

    @return Nonzero value on error, zero on success.
*/
int32_t usb_get_ctrl(
    uint8_t index,
    struct usb_ctrl** pp_ctrl)
{
    if( SKIQ_MAX_NUM_CARDS <= index )
    {
        printf("%s:%d bad card id %d\n", __FILE__, __LINE__, index);
        return -1;
    }
    else
    {
        if( false == IS_USB_VALID(_usb[index].ctrl) )
        {
            return -1;
        }
        *pp_ctrl = &(_usb[index].ctrl);
    }

    return 0;
}

/*****************************************************************************/
/** @brief Gets the receive interface for a given index. Note, if unititialized,
    this function will return an error.

    @param[in] index        index of Rx interface to obtain.
    @param[out] pp_rx       pointer to be updated with address of Rx interface

    @return Nonzero value on error, zero on success.
*/
int32_t usb_get_rx(
    uint8_t index,
    struct usb_rx** pp_rx)
{
    if( SKIQ_MAX_NUM_CARDS <= index )
    {
        return -1;
    }
    else if( false == IS_USB_VALID(_usb[index].rx) )
    {
        return -1;
    }

    *pp_rx = &(_usb[index].rx);

    return 0;
}


/*************************************************************************************************/
/** @brief Signal, using a specified write pipe, that a supplied USB transfer is complete.

    @param[in]  write_pipe     pipe to write to
    @param[in]  p_transfer     pointer of the USB transfer to be sent to the waiting thread

    @return Nonzero value on error, zero on success.
    @retval 0 Successfully submitted signal to write_pipe
    @retval -EPIPE A short write happened while signaling
    @retval -EPROTO An unspecified error (other than a short write) happened while signaling
*/
int32_t usb_signal_event( write_pipe_handle_t write_pipe,
                          struct libusb_transfer* const p_transfer )
{
    int32_t status = 0;
    int len = sizeof( struct libusb_transfer * );

#if (defined __MINGW32__)
    DWORD lenWritten = 0;
    if (!WriteFile(write_pipe, (void*) &p_transfer, len, &lenWritten, NULL))
    {
        status = GetLastError();
        skiq_error("Failed to signal USB event, status = %d\n", status);
        status = -EPROTO;
    }
#else
    int r;

    r = write(write_pipe, (void*) &p_transfer, len);
    if( r != len )
    {
        status = -EPIPE;
    }
    else if ( r == -1 )
    {
        status = -EPROTO;
    }
    else
    {
        status = 0;
    }
#endif /* __MINGW32__ */

    return status;
}


/*****************************************************************************/
/** @brief The callback function for a completed USB transfer. This function
    will "resubmit" itself to the attached write pipe to be obtained later
    by calling usb_poll.

    @param[in] p_transfer   pointer to completed libusb transfer

    @return void.
*/
void usb_callback(
    struct libusb_transfer* const p_transfer )
{
    write_pipe_handle_t write_pipe = *(write_pipe_handle_t*) p_transfer->user_data;
    usb_signal_event( write_pipe, p_transfer );
}


/*****************************************************************************/
/** @brief Poll a given read pipe for completed USB transfers. Note that
    transfers are written to the pipe through usb_callback.

    @param[in]  read_fd         pipe to read from
    @param[out] pp_transfer     pointer to be updated with address of completed
                                libusb transfer
    @param[in]  timeout         maximum amount of time to poll

    @return int32_t
    @retval 0 Success
    @retval -EPIPE A short read happened while signaling
    @retval -EPROTO Unspecified error occurred while polling for USB event
    @retval -ETIMEDOUT A timeout occurred waiting for the USB event
*/
int32_t usb_poll(
    read_pipe_handle_t read_pipe,
    struct libusb_transfer** pp_transfer,
    struct timeval timeout )
{
    int32_t status = 0;
    uint32_t len = sizeof(struct libusb_transfer*);

#if (defined __MINGW32__)
    DWORD numberOfBytesRead;
    if (PeekNamedPipe(read_pipe, NULL, len, NULL, NULL, NULL))
    {
        if (!ReadFile(read_pipe,
                      (void*) pp_transfer,
                      len,
                      &numberOfBytesRead,
                      NULL))
        {
            status = GetLastError();
            skiq_error("Unexpected error occurred reading USB event: %d\n", status);
            status = -EPROTO;
        }
    }
    else
    {
        skiq_warning("Polling not supported on this platform\n");
    }
#else
    ssize_t num_read = 0;
    fd_set read_set;
    int retval;

    FD_ZERO( &read_set );
    FD_SET( read_pipe, &read_set );
    retval = select( read_pipe + 1, &(read_set), NULL, NULL, &timeout );
    if ( retval == -1 )
    {
        skiq_error("Unexpected error occurred polling for USB event: %s\n", strerror(errno));
        status = -EPROTO;
    }
    else if ( retval == 0 )
    {
        status = -ETIMEDOUT;
    }
    else
    {
        status = 0;
    }

    /* return from select() gave indication that data is waiting */
    if ( status == 0 )
    {
        if ( FD_ISSET(read_pipe, &read_set) )
        {
            num_read = read(read_pipe, (void*) pp_transfer, len);
            if ( num_read < 0 )
            {
                skiq_error("Unexpected error occurred reading USB event: %s\n", strerror(errno));
                status = -EPROTO;
            }
            else if ( num_read == len )
            {
                status = 0;
            }
            else
            {
                status = -EPIPE;
            }
        }
    }
#endif /* __MINGW32__ */

    return status;
}

/*****************************************************************************/
/** @brief Returns the connection status of the USB interface for a given
    index

    @param[in] index   index number of the Sidekiq of interest

    @return true if the USB is connected
*/
bool usb_is_connected(
    uint8_t index )
{
    return (IS_USB_VALID(_usb[index]));
}

/*****************************************************************************/
/** @brief Returns the streaming status of the USB receive interface.

    @param[in] index   index number of the Sidekiq of interest

    @return true if the USB Rx interface is currently streaming
*/
bool usb_is_rx_streaming(
    uint8_t index )
{
    return ((USB_STREAM_ACTIVE == _usb[index].rx.cur_stat) ? true : false);
}

/*****************************************************************************/
/** @brief Returns the streaming status of the USB transmit interface.

    @param[in] index   index number of the Sidekiq of interest

    @return true if the USB Tx interface is currently streaming
*/
bool usb_is_tx_streaming(
    uint8_t index )
{
    return (false);
}
