/**
 * @file   usb_ctrl.c
 * @author <info@epiq-solutions.com>
 * @date   Mon Jan 21 13:23:22 2019
 * 
 * @brief  Performs all of the USB control transfers
 * 
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * </pre>
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE             /* See feature_test_macros(7) */
#endif

#include <fcntl.h>              /* Obtain O_* constant definitions */
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include "sidekiq_api_factory.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "flash_defines.h"
#include "libusb.h"

#include "usb_interface.h"
#include "usb_private.h"


/* enable debug_print and debug_print_plain when DEBUG_TRANSPORT is defined */
#if (defined DEBUG_TRANSPORT)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/// @brief Timeout for each control transfer, 60 seconds.
#define USB_CTRL_TIMEOUT 60000 

/** @brief  The major firmware version required for USB Golden Image locking. */
#define USB_GOLDEN_LOCKING_FW_MAJ_REQ       (2)
/** @brief  The minor firmware version required for USB Golden Image locking. */
#define USB_GOLDEN_LOCKING_FW_MIN_REQ       (9)

/** @brief  The major firmware version required for USB Flash Manufacturer information */
#define USB_FLASH_MFR_ID_MAJ_REQ            (2)
/** @brief  The minor firmware version required for USB Flash Manufacturer information */
#define USB_FLASH_MFR_ID_MIN_REQ            (9)

#define GENERIC_VERSION_MSG \
    "Firmware version v%u.%u does not meet the minimum requirement of v%u.%u"

static const char *golden_version_msg = \
    GENERIC_VERSION_MSG " needed to protect or unprotect the golden image\n";

static const char *flash_mfr_id_version_msg = \
    GENERIC_VERSION_MSG " needed to read the Flash manufacturing information\n";

/***** LOCAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Performs a USB control transaction writing up to USB_CTRL_MAX_LENGTH
    bytes.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] cmd      vendor command to be issued to the FX2
    @param[in] p_addr   device address
    @param[in] len      total length in bytes of the message
    @param[in] p_data   pointer to write data
           
    @return total number of bytes transfered
*/
static int32_t _usb_write_cmd(
    uint8_t index,
    SIDEKIQ_USB_CMDS cmd,
    uint8_t* p_addr,
    uint16_t len,
    uint8_t* p_data )
{
    struct libusb_transfer* p_transfer = NULL;
    struct usb_ctrl* p_ctrl = NULL;
    struct timeval poll_timeout = {.tv_sec = 20L, .tv_usec = 0L};
    int32_t status = 0;
    
    if( USB_CTRL_MAX_LENGTH < len )
    {
        return -1;
    }
    else if( 0 != usb_get_ctrl(index, &p_ctrl) )
    {
        return -1;
    }
    
    USB_LOCK(p_ctrl);

    libusb_fill_control_setup(p_ctrl->p_buf,
                              WRITE_REQUEST_TYPE,
                              cmd,
                              *((uint16_t*)(p_addr)),
                              0,
                              len);
    
    memcpy(p_ctrl->p_buf + LIBUSB_CONTROL_SETUP_SIZE, p_data, len);
    
    libusb_fill_control_transfer(p_ctrl->p_transfer, 
                                 p_ctrl->p_handle,
                                 p_ctrl->p_buf,
                                 usb_callback,
                                 &(p_ctrl->write_pipe), 
                                 USB_CTRL_TIMEOUT);
    
    status = libusb_submit_transfer(p_ctrl->p_transfer);
    if( 0 != status )
    {
        USB_UNLOCK_AND_RETURN(p_ctrl, -1);
    }
    
    status = usb_poll(p_ctrl->read_pipe, &p_transfer, poll_timeout);
    if( (0 != status) || (p_ctrl->p_transfer != p_transfer) )
    {
        USB_UNLOCK_AND_RETURN(p_ctrl, -1);
    }
    
    len = p_transfer->actual_length;
    
    USB_UNLOCK(p_ctrl);
    
    return (len);
}

/*****************************************************************************/
/** @brief Performs a USB control transaction reading up to USB_CTRL_MAX_LENGTH
    bytes.

    @param[in] index   index number of the Sidekiq of interest
    @param[in] cmd      vendor command to be issued to the FX2
    @param[in] p_addr   device address
    @param[in] len      total length in bytes of the message
    @param[out] p_data  address to store read data
           
    @return total number of bytes transfered
*/
static int32_t _usb_read_cmd(
    uint8_t index,
    SIDEKIQ_USB_CMDS cmd,
    uint8_t* p_addr,
    uint16_t len,
    uint8_t* p_data )
{
    struct libusb_transfer* p_transfer = NULL;
    struct usb_ctrl* p_ctrl = NULL;
    struct timeval poll_timeout = {.tv_sec = 20L, .tv_usec = 0L};
    int32_t status = 0;
    
    if( USB_CTRL_MAX_LENGTH < len )
    {
        printf("%s:%d:%s bad length\n", __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }
    else if( 0 != usb_get_ctrl(index, &p_ctrl) )
    {
        printf("%s:%d:%s usb_get_ctrl() failed\n", __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }
    
    USB_LOCK(p_ctrl);

    libusb_fill_control_setup(p_ctrl->p_buf,
                              READ_REQUEST_TYPE,
                              cmd,
                              *((uint16_t*)(p_addr)),
                              0,
                              len);
                              
    libusb_fill_control_transfer(p_ctrl->p_transfer, 
                                 p_ctrl->p_handle,
                                 p_ctrl->p_buf,
                                 usb_callback,
                                 &(p_ctrl->write_pipe), 
                                 USB_CTRL_TIMEOUT);
    
    status = libusb_submit_transfer(p_ctrl->p_transfer);
    if( 0 != status )
    {
        printf("%s:%d:%s USB submit failed\n", __FILE__, __LINE__, __FUNCTION__);
        USB_UNLOCK_AND_RETURN(p_ctrl, -1);
    }

    status = usb_poll(p_ctrl->read_pipe, &p_transfer, poll_timeout);
    if( (0 != status) || (p_ctrl->p_transfer != p_transfer) )
    {
        printf("%s:%d:%s usb_poll failed\n", __FILE__, __LINE__, __FUNCTION__);
        USB_UNLOCK_AND_RETURN(p_ctrl, -1);
    }

    memcpy(p_data, 
           p_ctrl->p_buf + LIBUSB_CONTROL_SETUP_SIZE, 
           p_transfer->actual_length);

    len = p_transfer->actual_length;

    USB_UNLOCK(p_ctrl);
    
    return (len);
}

/**
    @brief  Verify that the firmware meets the minimum required version for a specified feature

    @param[in]  index           index number of the Sidekiq of interest
    @param[in]  reqMajVer       the required firmware major version number
    @param[in]  reqMinVer       the required firmware minor version number
    @param[in]  verNotMetMsg    if not NULL, the message to display if the firmware doesn't meet
                                the specified version. the current major & minor firmware version
                                as well as the required major & minor versions are passed as
                                parameters to the error message so these are required in this
                                string; see GENERIC_VERSION_MSG for the expected format
    @param[in]  isDebugMsg      if @p verNotMetMsg is not NULL: if true, display the warning
                                message as a debug message, else display it as an error message

    @return false if the firmware version couldn't be read or if the current firmware doesn't
            meet the minimum version requirement; else true
*/
static bool
meets_fw_version(uint8_t index, uint8_t reqMajVer, uint8_t reqMinVer, const char *verNotMetMsg,
    bool isDebugMsg)
{
    bool meets_version = false;
    uint8_t fw_major = 0;
    uint8_t fw_minor = 0;

    int32_t status = 0;

    status = usb_read_fw_ver( index, &fw_major, &fw_minor );
    if (0 == status)
    {
        meets_version = (FW_VERSION(reqMajVer, reqMinVer) <= FW_VERSION(fw_major, fw_minor));

        if (!meets_version)
        {
            if (NULL != verNotMetMsg)
            {
                if (isDebugMsg)
                {
                    debug_print(verNotMetMsg, fw_major, mw_minor, reqMajVer, reqMinVer);
                }
                else
                {
                    skiq_error(verNotMetMsg, fw_major, fw_minor, reqMajVer, reqMinVer);
                }
            }
        }
    }
    else
    {
        skiq_warning("Failed to get firmware version for index %" PRIu8 " (status = %" PRIi32 ")\n",
            index, status);
    }

    return (meets_version);

}


/**
    @brief  Determine if the firmware version meets the requirement for doing
            golden image protection

    Golden image protection was introduced in firmware v2.9.

    @return true if it does, else false.
*/
static bool
has_golden_protection_firmware(uint8_t index)
{
    return meets_fw_version(index, USB_GOLDEN_LOCKING_FW_MAJ_REQ, USB_GOLDEN_LOCKING_FW_MIN_REQ,
                golden_version_msg, false);
}


/***** GLOBAL FUNCTION DEFINITIONS *****/

/*****************************************************************************/
/** @brief Initializes the USB control interface.

    @param[in] p_ctrl       pointer to control interface to initialize.
    @param[in] p_handle     pointer to the USB handle to use with the interface.
    @param[in] id           a numeric ID to associate with the interface.
 
    @return Nonzero value on error, zero on success.
*/
int32_t usb_ctrl_init(
    struct usb_ctrl* p_ctrl,
    struct libusb_device_handle* p_handle,
    uint8_t id )
{
    int32_t err = 0;

#if (defined __MINGW32__)
    if (!CreatePipe(&p_ctrl->read_pipe, &p_ctrl->write_pipe, NULL, 4096))
    {
        err = -1;
        goto _usb_ctrl_init_exit;
    }
#else
    int fd[2] = { UNOPENED_FD, UNOPENED_FD };
    err = pipe2(fd, O_NONBLOCK);
    if( 0 !=  err )
    {
        err = -1;
        goto _usb_ctrl_init_exit;
    }
    p_ctrl->read_pipe = fd[0];
    p_ctrl->write_pipe = fd[1];
#endif /* __MINGW32__ */

    p_ctrl->id = id;
    p_ctrl->p_handle = p_handle;
    p_ctrl->p_transfer = libusb_alloc_transfer(0);
    
_usb_ctrl_init_exit:
    return (err);
}

/*****************************************************************************/
/** @brief Deallocates all resources associated with a given control interface.

    @param[in] p_ctrl       Pointer to control interface to free.
 
    @return Nonzero value on error, zero on success.
*/
extern void usb_ctrl_free(
    struct usb_ctrl* p_ctrl )
{
    CLOSE_FD_IF_OPEN(p_ctrl->read_pipe);
    CLOSE_FD_IF_OPEN(p_ctrl->write_pipe);
    
    if( NULL != p_ctrl->p_transfer )
    {
        libusb_free_transfer(p_ctrl->p_transfer);
        p_ctrl->p_transfer = NULL;
    }
    
    // Set to NULL to indicate the stream is inactive
    p_ctrl->p_handle = NULL;
}

/*****************************************************************************/
/** @brief Reads the state of the LED connected to USB microcontroller.

    @param[in] index            index number of the Sidekiq of interest
    @param[out] p_led   pointer to where to store the LED (1=ON, 0=OFF)
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_led(
    uint8_t index,
    uint8_t* p_led )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;

    result = _usb_read_cmd(index,
                           CMD_READ_WRITE_LED_STATE,
                           addr,
                           length,
                           p_led);

    if( result == length )
    {
        result = 0; // 0 = success
    }

    return result;
}

/*****************************************************************************/
/** @brief Writes the state of the LED connected to USB microcontroller.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] led      state to set the LED to (1=ON, 0=OFF)
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_led(
    uint8_t index,
    uint8_t led )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;

    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_LED_STATE,
                            addr,
                            length,
                            &led );
    if( result == length )
    {
        result = 0;
    }

    return result;
}

/*****************************************************************************/
/** @brief Reads the firmware version, serial number, and board number.

    @param[in] index        index number of the Sidekiq of interest
    @param[out] p_fw_maj    pointer to be updated with firmware major version
    @param[out] p_fw_maj    pointer to be updated with firmware minor version
    @param[out] p_fw_maj    pointer to be updated with the serial number
    @param[out] p_fw_maj    pointer to be updated with the board number
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_version(
    uint8_t index,
    uint8_t* p_fw_maj,
    uint8_t* p_fw_min,
    uint32_t* p_serial_num, 
    uint16_t* p_board_num )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 6;
    uint8_t buf[length];

    result = _usb_read_cmd(index,
                           CMD_READ_WRITE_VERSION,
                           addr,
                           length,
                           buf);

    // TODO: trans status

    if( result == length )
    {
        result = 0;
        *p_serial_num = buf[0] | (buf[1] << 8);
        *p_board_num = buf[2] | (buf[3] << 8);
        *p_fw_maj = buf[4];
        *p_fw_min = buf[5];

        if( INVALID_SERIAL_NUM == *p_serial_num )
        {
            // Check and see if we are using an extended serial number.
            buf[0] = 0xFF;
            buf[1] = 0xFF;
            buf[2] = 0xFF;
            buf[3] = 0xFF;
            addr[0] = VERSION_INFO_ALPHANUMERIC_ADDR & 0xFF;    // LSB first
            addr[1] = (VERSION_INFO_ALPHANUMERIC_ADDR >> 8) & 0xFF;
            result = _usb_read_cmd(index,
                                   CMD_READ_WRITE_EEPROM,
                                   addr,
                                   4,
                                   buf);
            // Valid ASCII characters will not have the most significant bit set.
            if( (result == 4) &&
                (isalnum( buf[0]) != 0 ) &&
                (isalnum( buf[1]) != 0 ) &&
                (isalnum( buf[2]) != 0 ) &&
                (isalnum( buf[3]) != 0 ) )
            {
                result = 0;
                // Serial number is stored little endian.
                *p_serial_num = (buf[0] <<  0) | 
                                (buf[1] <<  8) |
                                (buf[2] << 16) |
                                (buf[3] << 24);
            }
            else
            {
                // Note: for an invalid serial #, we technically read it, so 
                // just return
                fprintf(stderr, "Warning: invalid serial number read\n");
                result = 0;
            }
        }
    }
    else
    {
        result = -1;
    }

    return result;
}

/*****************************************************************************/
/** @brief Writes the serial and board number to EEPROM.

    @param[in] index            index number of the Sidekiq of interest
    @param[in] serial_num       the serial numner to write
    @param[in]                  the board number to write
    
    @return Nonzero value on error, zero on success.
*/   
int32_t usb_write_version(
    uint8_t index,
    uint32_t serial_num, 
    uint16_t board_num )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 4;
    uint8_t buf[length];

    if( (SKIQ_FACT_SERIAL_NUM_LEGACY_MAX > serial_num) && 
        (SKIQ_FACT_SERIAL_NUM_LEGACY_MIN < serial_num) )
    {
        buf[0] = (serial_num >>  0) & 0xFF;
        buf[1] = (serial_num >>  8) & 0xFF;
    }
    else
    {
        // Mark the "legacy" serial number as invalid.
        buf[0] = 0xFF;
        buf[1] = 0xFF;
    }
    buf[2] = (uint8_t)(board_num);
    buf[3] = (uint8_t)(board_num >> 8);

    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_VERSION,
                            addr,
                            length,
                            buf);

    // TODO: trans status

    if( result != length )
    {
        return -1;
    }
    
    // Store the 4 byte alpha numeric serial number little endian.
    length = 4;
    buf[0] = (serial_num >>  0) & 0xFF;
    buf[1] = (serial_num >>  8) & 0xFF;
    buf[2] = (serial_num >> 16) & 0xFF;
    buf[3] = (serial_num >> 24) & 0xFF;
    addr[0] = VERSION_INFO_ALPHANUMERIC_ADDR & 0xFF;    // LSB first
    addr[1] = (VERSION_INFO_ALPHANUMERIC_ADDR >> 8) & 0xFF;
    
    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_EEPROM,
                            addr,
                            length,
                            buf);
    if( result != length )
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/** @brief  Reads the version of the firmware.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_fw_maj    pointer to where to store the major verson number
    @param[out] p_fw_min    pointer to where to store the minor verson number
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_fw_ver(
    uint8_t index,
    uint8_t* p_fw_maj,
    uint8_t* p_fw_min )
{
    int32_t result = -1;
    uint16_t board;
    uint32_t serial;

    result = usb_read_version(index, p_fw_maj, p_fw_min, &serial, &board );

    return result;
}

/*****************************************************************************/
/** @brief Reads the board number.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_board_num     pointer to where to store the board number
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_board_num(
    uint8_t index,
    uint16_t* p_board_num )
{
    int32_t result = -1;
    uint8_t fw_maj, fw_min;
    uint32_t serial;

    result = usb_read_version(index, &fw_maj, &fw_min, &serial, p_board_num);

    return result;
}

/*****************************************************************************/
/** @brief Writes the board number to EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] board_num    the board number to write
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_board_num(
    uint8_t index,
    uint16_t board_num )
{
    int32_t result = -1;
    uint32_t serial;
    uint16_t tmp_board;
    uint8_t fw_maj, fw_min;

    result = usb_read_version(index, &fw_maj, &fw_min, &serial, &tmp_board);
    if( result == 0 )
    {
        result = usb_write_version(index, serial, board_num);
    }
    
    return result;
}

/*****************************************************************************/
/** @brief Reads the serial number of the board.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_serial_num    pointer to where to store the serial number
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_serial_num(
    uint8_t index,
    uint32_t* p_serial_num )
{
    int32_t result = -1;
    uint8_t fw_maj, fw_min;
    uint16_t board;

    result = usb_read_version(index, &fw_maj, &fw_min, p_serial_num, &board);

    return result;
}

/*****************************************************************************/
/** @brief Writes the serial number of the board to EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] serial_num   pointer serial number to store
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_serial_num(
    uint8_t index,
    uint32_t serial_num )
{
    int32_t result = -1;
    uint32_t tmp_serial;
    uint16_t board;
    uint8_t fw_maj, fw_min;

    result = usb_read_version(index, &fw_maj, &fw_min, &tmp_serial, &board);
    if( result == 0 )
    {
        result = usb_write_version(index, serial_num, board);
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads data from the accelerometer register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg      register of accelerometer to access
    @param[out] p_data  pointer of where to store the data read
    @param[in] length   number of bytes to read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_accel_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t* p_data,
    uint8_t length )
{
    int32_t result = -1;
    uint8_t addr[2];

    addr[0] = ACCEL_I2C_ADDRESS;
    addr[1] = reg;

    result = _usb_read_cmd(index,
                           CMD_READ_WRITE_I2C,
                           addr,
                           (uint16_t)(length),
                           p_data);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Writes data to the accelerometer register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg      register of accelerometer to write to
    @param[in] p_data   pointer to the data to write
    @param[in] length   number of bytes to write
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_accel_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t* p_data,
    uint8_t length )
{
    int32_t result = -1;
    uint8_t addr[2];

    addr[0] = ACCEL_I2C_ADDRESS;
    addr[1] = reg;

    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_I2C,
                            addr,
                            (uint16_t)(length),
                            p_data);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads the specified register from the temperature sensor.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg          register of temp sensor to read
    @param[out] p_data      pointer of where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_temp_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t* p_data )
{
    int32_t result = -1;
    uint16_t length = 1;
    uint8_t addr[2];

    addr[0] = TEMP_I2C_ADDRESS;
    addr[1] = reg;

    result = _usb_read_cmd(index,
                           CMD_READ_WRITE_I2C,
                           addr,
                           length,
                           p_data);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Writes to the specified register of the temperature sensor.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg      register of temp sensor to write to
    @param[in] data     value to write (only single byte access is allowed here)
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_temp_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t data )
{
    int32_t result = -1;
    uint16_t length = 1;
    uint8_t addr[2];

    addr[0] = TEMP_I2C_ADDRESS;
    addr[1] = reg;

    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_I2C,
                            addr,
                            length,
                            &data);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads data from the flash. Note: only a page (256 bytes) at a time 
    can be accessed, so the address provided is the upper 2 bytes of the 
    address, and the lowest byte is always 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_buffer    pointer of where to store the data read
    @param[in] start_addr   upper 2 bytes of the address to read from
    @param[in] length       number of bytes to read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_flash(
    uint8_t index,
    uint8_t* p_buffer,
    uint32_t address,
    uint16_t length )
{
    int32_t result = -1;
    uint8_t addr[2];
    uint16_t num_bytes_left = length;
    uint16_t curr_length = 0;
    
    // MSB first
    addr[0] = (address >> 16) & 0xFF;
    addr[1] = (address >> 8) & 0xFF;

    while( num_bytes_left > 0 )
    {
        if( num_bytes_left > FLASH_PAGE_SIZE )
        {
            curr_length = FLASH_PAGE_SIZE;
        }
        else
        {
            curr_length = num_bytes_left;
        }
        
        result = _usb_read_cmd(index,
                               CMD_READ_WRITE_FLASH,
                               addr,
                               curr_length,
                               &(p_buffer[length-num_bytes_left]));
        // TODO: check status
        if( result > 0 )
        {
            num_bytes_left -= result;
            result = 0;
            addr[1] += 1;
            if( addr[1] == 0 )
            {
                addr[0] += 1;
            }
        }
        else
        {
            break;
        }
    }
    return result;
}

/*****************************************************************************/
/** @brief Writes data to the flash. Note: only a page (256 bytes) at a time 
    can be accessed, so the address provided is the upper 2 bytes of the 
    address, and the lowest byte is always 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] p_buffer     pointer to the data to write
    @param[in] address      upper 2 bytes of the address to write to
    @param[in] length       number of bytes to write
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_flash(
    uint8_t index,
    uint8_t* p_buffer,
    uint32_t address,
    uint16_t length )
{
    int32_t result = -1;
    uint8_t addr[2];
    uint16_t num_bytes_left = length;
    uint16_t curr_length = 0;

    // Assume caller has a valid address...
    
    addr[0] = (address >> 16) & 0xFF;
    addr[1] = (address >> 8) & 0xFF;

    while( num_bytes_left > 0 )
    {
        if( num_bytes_left > FLASH_PAGE_SIZE )
        {
            curr_length = FLASH_PAGE_SIZE;
        }
        else
        {
            curr_length = num_bytes_left;
        }

        result = _usb_write_cmd(index,
                                CMD_READ_WRITE_FLASH,
                                addr,
                                curr_length,
                                &(p_buffer[length-num_bytes_left]));

        // TODO: check status
        if( result > 0 )
        {
            num_bytes_left -= result;
            result = 0;
            addr[1] += 1;
            if( addr[1] == 0 )
            {
                addr[0] += 1;
            }
        }
        else
        {
            break;
        }
    }
    return result;
}

/*****************************************************************************/
/** @brief Erases a sector of flash.  Note: only a page (256 bytes) at a time 
    can be accessed, so the address provided is the upper 2 bytes of the 
    address, and the lowest byte is always 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      upper 2 bytes of the address to erase
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_erase_flash_sector(
    uint8_t index,
    uint32_t address )
{
    // TODO: does this need to be changed to have a 32 bit address????????
    int32_t result = -1;
    uint8_t addr[2];
    uint16_t length = 1; // TODO: can we send 0?
    uint8_t buf[length];
    // 32 bit address sent, usb just wants to seet he middle two bytes
    // ie: addr = XX 20 00 xx
    // MSB first
    addr[0] = (address >> 16) & 0xFF;
    addr[1] = (address >> 8) & 0xFF;

    result = _usb_write_cmd(index,
                            CMD_FLASH_SECTOR_ERASE,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Erases the entire flash. Note that this process may take a very long
    time (20 seconds or longer).

    @param[in] index    index number of the Sidekiq of interest
   
    @return Nonzero value on error, zero on success.
*/
int32_t usb_erase_flash(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1; // TODO: 0?
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_FLASH_BULK_ERASE,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Prepares the FPGA to be programmed.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_init_fpga_for_programming(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1; // TODO: 0?
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_INIT_FPGA_PROGRAM,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Sends the FPGA configuration data to FPGA programming interface.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] p_buffer         pointer to the configuration data to send
    @param[in] length           number of bytes in the buffer
    @param[in] legacy_method    flag indicating whether to use legacy
                                (ctrl transfer) method instead of bulk transfer
           
    @return Nonzero value on error, zero on success.
*/
int32_t usb_send_fpga_config(
    uint8_t index,
    uint8_t* p_buffer,
    uint32_t length,
    uint8_t legacy_method )
{
    int32_t result = -1;
    int32_t num_bytes_sent = 0;
    uint8_t addr[2] = {0};
    struct timeval poll_time = {.tv_sec = 4L, .tv_usec = 0L,};
    struct libusb_transfer* p_transfer = NULL;
    struct usb_ctrl* p_ctrl = NULL;
    
    if( 0 != usb_get_ctrl(index, &p_ctrl) )
    {
        return -1;
    }
    
    // Fast programming
    if( legacy_method == 0 )
    {
        libusb_fill_bulk_transfer(p_ctrl->p_transfer,
                                  p_ctrl->p_handle,
                                  0x06,
                                  p_buffer,
                                  length,
                                  usb_callback,
                                  (void*) &(p_ctrl->write_pipe),
                                  5000);
        
        result = libusb_submit_transfer(p_ctrl->p_transfer);
        if( 0 == result )
        {
            result = usb_poll(p_ctrl->read_pipe, &p_transfer, poll_time);
            if( (0 != result) || (p_transfer != p_ctrl->p_transfer) )
            {
                result = -1;
            }
        }
    }
    // Legacy programming
    else
    {
        num_bytes_sent = _usb_write_cmd(index,
                                        CMD_PROGRAM_FPGA,
                                        addr,
                                        length,
                                        p_buffer);
        
        result = (num_bytes_sent == length) ? 0 : -1;
    }

    return (result);
}

/*****************************************************************************/
/** @brief Finalizes the FPGA configuration process.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_finish_fpga_programming(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_FINISH_FPGA_PROGRAM,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Configures FPGA programming to be loaded from the flash.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_load_fpga_from_flash(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_INIT_FPGA_FROM_FLASH,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads the state of the FPGA.  It is configured if it is set to 1, 
    otherwise, the config flag is set to 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_config    pointer to where to store the config flag
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_fpga_config(
    uint8_t index,
    uint8_t* p_config )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;

    result = _usb_read_cmd(index,
                           CMD_READ_FPGA_STATE,
                           addr,
                           length,
                           p_config);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads the contents of the EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      starting address of where to read from
    @param[in] length       number of bytes to read
    @param[out] p_buffer    pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_eeprom_contents(
    uint8_t index,
    uint16_t address,
    uint16_t length,
    uint8_t* p_buffer )
{
    int32_t result = -1;
    uint8_t addr[2];
    // LSB first
    addr[0] = address;
    addr[1] = address >> 8;

    result = _usb_read_cmd(index,
                           CMD_READ_WRITE_EEPROM,
                           addr,
                           length,
                           p_buffer);
    if( result == length )
    {
        result = 0;
    }

    return result;
}

/*****************************************************************************/
/** @brief Writes data to the EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      starting address of where to write to
    @param[in] length       number of bytes to write
    @param[out] p_buffer    pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_eeprom_contents(
    uint8_t index,
    uint16_t address,
    uint16_t length,
    uint8_t* p_buffer )
{
    int32_t result = -1;
    uint8_t addr[2];
    // LSB first
    addr[0] = address;
    addr[1] = address >> 8;

    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_EEPROM,
                            addr,
                            length,
                            p_buffer);
    if( result == length )
    {
        result = 0;
    }
    return (result);
}


/*****************************************************************************/
/** @brief Reads the FPGA register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      register address to read
    @param[out] p_data      pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_fpga_reg(
    uint8_t index,
    uint32_t address,
    uint32_t* p_data )
{
    int32_t status=0;
    uint8_t addr[2]={0};
    uint16_t length = 4; // always 4 bytes to read

    // MSB first
    addr[0] = address >> 8;
    addr[1] = address;

    status = _usb_read_cmd(index,
                           CMD_READ_WRITE_FPGA_REG,
                           addr,
                           length,
                           (uint8_t*)(p_data));

    if( status == length )
    {
        status = 0;
    }
    return status;
}

/*****************************************************************************/
/** @brief Writes the FPGA register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      register address to write to
    @param[in] data         data to write to the register
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_fpga_reg(
    uint8_t index,
    uint32_t address,
    uint32_t data )
{
    int32_t status=0;
    uint8_t addr[2] = {0};
    uint16_t length = 4;

    // MSB first
    addr[0] = address >> 8;
    addr[1] = address;

    status = _usb_write_cmd(index,
                            CMD_READ_WRITE_FPGA_REG,
                            addr,
                            length,
                            (uint8_t*)(&data));

    if( status == length )
    {
        status = 0;
    }

    return status;
}

/*****************************************************************************/
/** @brief Sends a reset command to the USB chip (this is really only needed if
    we're running in USB only mode but shouldn't hurt anything to do it on 
    other builds).

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      register address to write to
    @param[in] data         data to write to the register
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_fpga_reset(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1; // TODO: 0?
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_RESET_FPGA,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Initializes the USB chip to support either a read or a write across 
    the GPIF interface from the USB chip's perspective.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] write    flag set to true if USB chip is going to write
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_init_gpif_test( 
    uint8_t index,
    bool write )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1; // TODO: 0?
    uint8_t buf[length];

    if( write == true )
    {
        result = _usb_write_cmd(index,
                                CMD_GPIF_INIT_TEST_READ_WRITE,
                                addr,
                                length,
                                buf);
    }
    else
    {
        result = _usb_read_cmd(index,
                               CMD_GPIF_INIT_TEST_READ_WRITE,
                               addr,
                               length,
                               buf);
    }

    if( result == length )
    {
        result = 0;
    }

    return result;
}

/*****************************************************************************/
/** @brief Outputs the data across the GPIF interface from the USB chip.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] write    data to write/output
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_gpif_test_write(
    uint8_t index,
    uint16_t write )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 2;
    uint8_t buf[length];

    buf[0] = write;
    buf[1] = (uint8_t)((write >> 8));

    result = _usb_write_cmd(index,
                            CMD_GPIF_TEST_READ_WRITE,
                            addr,
                            length,
                            buf);

    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads the data on the GPIF interface to the USB chip.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_read      pointer to the data read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_gpif_test_read(
    uint8_t index,
    uint16_t* p_read )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 2;

    result = _usb_read_cmd(index,
                           CMD_GPIF_TEST_READ_WRITE,
                           addr,
                           length,
                           (uint8_t*)(p_read));

    if( result == length )
    {
       result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Exits the GPIF test mode (configuring the pins appropriately)

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_exit_gpif_test(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1; // TODO: 0?
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_GPIF_END_TEST,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Turns on the slave fifo interface on the FX2's GPIF interface.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_gpif_fifo_enable(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;
    uint8_t buf[length];

    buf[0] = 1;

    result = _usb_write_cmd(index,
                            CMD_GPIF_FIFO_READ_WRITE,
                            addr,
                            length,
                            buf);

    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Turns off the slave fifo interface on the FX2's GPIF interface.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_gpif_fifo_disable(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;
    uint8_t buf[length];

    buf[0] = 0;

    result = _usb_write_cmd(index,
                            CMD_GPIF_FIFO_READ_WRITE,
                            addr,
                            length,
                            buf);

    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Resets endpoint 2 on the FX2 microcontroller that is used for 
    sending bulk transfer in towards the host.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_reset_bulk_in_endpoint(
    uint8_t index )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;
    uint8_t buf[length];

    result = _usb_write_cmd(index,
                            CMD_BULK_IN_RESET,
                            addr,
                            length,
                            buf);
    if( result == length )
    {
        result = 0;
    }
    return result;
}

/*****************************************************************************/
/** @brief Reads the io expander register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg register     address to read
    @param[out] p_data          pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_read_io_expander_reg(
    uint8_t index,
    uint8_t address,
    uint8_t* p_data )
{
    int32_t status=0;
    uint8_t addr[2]={0};
    uint16_t length = 1; // always 1 byte to read

    // MSB first
    addr[0] = 0x00;
    addr[1] = address;

    status = _usb_read_cmd(index,
                           CMD_READ_WRITE_IO_EXPANDER_REG,
                           addr,
                           length,
                           p_data);

    if( status == length )
    {
        status = 0;
    }
    return status;
}

/*****************************************************************************/
/** @brief Writes the io expander register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address  register address to read
    @param[in] p_data   pointer to the data to write
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_io_expander_reg(
    uint8_t index,
    uint8_t address,
    uint8_t* p_data )
{
    int32_t status=0;
    uint8_t addr[2]={0};
    uint16_t length = 1; // always 1 byte to read

    // MSB first
    addr[0] = 0x00;
    addr[1] = address;

    status = _usb_write_cmd(index,
                           CMD_READ_WRITE_IO_EXPANDER_REG,
                           addr,
                           length,
                           p_data);

    if( status == length )
    {
        status = 0;
    }
    return status;
}

/*****************************************************************************/
/** @brief Writes the new reference clock configuration.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] ref_clock    pointer reference clock configuration to push
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_ref_clock(
    uint8_t index,
    skiq_ref_clock_select_t ref_clk )
{
    int32_t result = -1;
    uint8_t addr[2] = {0};
    uint16_t length = 1;
    uint8_t buf[length];

    // set the value of the command to send based on the enum
    if( ref_clk == skiq_ref_clock_external )
    {
        buf[0] = REFERENCE_CLOCK_EXTERNAL;
        result = 0;
    }
    else if( ref_clk == skiq_ref_clock_carrier_edge )
    {
        buf[0] = REFERENCE_CLOCK_CARRIER_EDGE;
        result = 0;
    }
    else if( ref_clk == skiq_ref_clock_internal )
    {
        buf[0] = REFERENCE_CLOCK_INTERNAL;
        result = 0;
    }

    if( result == 0 )
    {
        result = _usb_write_cmd(index,
                                CMD_READ_WRITE_REFERENCE,
                                addr,
                                length,
                                buf);
    }

    if( result == length )
    {
        result = 0;
    }

    return (result);
}

/*****************************************************************************/
/** @brief Performs an I2C write transaction on a Sidekiq card over USB.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] i2c_addr     I2C slave address of device to read from.
    @param[in] p_data       Buffer address to write data.
    @param[in] num_bytes    Number of bytes to write to device from buffer.
 
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_i2c(
    uint8_t index,
    uint8_t i2c_addr,
    uint8_t *p_data,
    uint8_t num_bytes )
{
    int32_t result = -1;
    uint8_t addr[2];

    addr[0] = i2c_addr;
    addr[1] = 0;

    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_I2C,
                            addr,
                            (uint16_t)(num_bytes),
                            &(p_data[0]));

    if( result == (num_bytes) )
    {
        result = 0;
    }
    else
    {
        result = -EIO;
    }

    return result;
}

/*****************************************************************************/
/** @brief Performs an I2C write then read transaction on a Sidekiq card over
    USB.

    @warning _usb_read_cmd does not only read data.  Initially it writes the
    register that it will be reading from and then reads.
*/
int32_t usb_write_then_read_i2c(
    uint8_t index,
    uint8_t i2c_addr,
    uint8_t reg_addr,
    uint8_t *p_data,
    uint8_t num_bytes )
{
    int32_t result = -1;
    uint8_t addr[2];

    addr[0] = i2c_addr;
    addr[1] = reg_addr;

    result = _usb_read_cmd(index,
                        CMD_READ_WRITE_I2C,
                        addr,
                        (uint16_t)(num_bytes),
                        &(p_data[0]));

    if( result == num_bytes )
    {
        result = 0;
    }
    else
    {
        result = -EIO;
    }

    return result;
}

/*****************************************************************************/
/** @brief Allows software to place the FX2 into a reset state.
    
    @param[in] index    index number of the Sidekiq of interest.
    @param[in] hold_reset   Set to true to place into reset, false to allow
                            firmware to resume operation.
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_hold_firmware_reset(
    uint8_t index,
    bool hold_reset)
{
    int32_t result = -1;
    // Address to the FX2's CPUCS register. 
    uint16_t addr = 0xE600;
    // Bit 0 maps to 8051RES bit; 1 to reset, 0 for normal operation.
    uint8_t data = (hold_reset) ? 1 : 0;
    
    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_RAM,
                            (uint8_t*) &addr,
                            1,
                            &data);
    if( 1 == result )
    {
        result = 0;
    }
    
    return result;
}

/*****************************************************************************/
/** @brief Directly write to the FX2's RAM. Extreme caution should be used with
    with function as it is possible to make the firmware unoperable.
    
    @param[in] index    index number of the Sidekiq of interest
    @param[in] addr         RAM address to write.
    @param[in] data         Pointer to data to write.
    @param[in] data_len     Length of data to write.
    
    @return Nonzero value on error, zero on success.
*/
int32_t usb_write_firmware_ram(
    uint8_t index,
    uint16_t addr,
    uint8_t* data,
    uint16_t data_len)
{
    int32_t result = -1;
    
    result = _usb_write_cmd(index,
                            CMD_READ_WRITE_RAM,
                            (uint8_t*) &addr,
                            data_len,
                            data);
    if( data_len == result )
    {
        result = 0;
    }

    return result;
}

/*****************************************************************************/
/**
    @brief  Query the Flash chip's manufacturer info
*/
int32_t usb_read_flash_manufacturer_info(uint8_t index, FlashChip *p_detected_flash,
    uint8_t *p_manufacturer_id, uint8_t *p_memory_type, uint8_t *p_memory_capacity)
{
    int32_t result = 0;
    uint8_t addr[2] = { 0 };
    uint16_t length = 4;
    uint8_t buf[length];
    FlashChip tmpChip = FLASH_CHIP_UNKNOWN;

    memset(&(buf[0]), 0xFF, length);

    if (!meets_fw_version(index, USB_FLASH_MFR_ID_MAJ_REQ, USB_FLASH_MFR_ID_MIN_REQ,
            flash_mfr_id_version_msg, true))
    {
        return -ENOTSUP;
    }

    result = _usb_read_cmd(index, CMD_FLASH_READ_MANUFACTURER, addr, length, buf);

    if ( result == length )
    {
        result = 0;

        if (NULL != p_detected_flash)
        {
            tmpChip = (FlashChip) buf[0];
            if ( FLASH_CHIP_UNKNOWN < tmpChip )
            {
                tmpChip = FLASH_CHIP_UNKNOWN;
            }

            *p_detected_flash = tmpChip;
        }

        if ( NULL != p_manufacturer_id )
        {
            *p_manufacturer_id = buf[1];
        }
        if ( NULL != p_memory_type )
        {
            *p_memory_type = buf[2];
        }
        if ( NULL != p_memory_capacity )
        {
            *p_memory_capacity = buf[3];
        }
    }
    else
    {
        result = -EIO;
    }

    return result;
}

/*****************************************************************************/
/**
    @brief  Query if the sectors that store the Golden FPGA image are locked.
*/
int32_t usb_is_golden_locked(uint8_t index, bool *p_is_locked)
{
    int32_t result = 0;
    uint8_t addr[2] = { 0 };
    uint8_t buf[USB_CMD_FLASH_QUERY_GOLDEN_SECTORS_LOCK_RESPONSE_LEN] = { 0xFF };

    if (!has_golden_protection_firmware(index))
    {
        return -ENOTSUP;
    }

    result = _usb_read_cmd(index, CMD_FLASH_QUERY_GOLDEN_SECTORS_LOCK,
                addr, USB_CMD_FLASH_QUERY_GOLDEN_SECTORS_LOCK_RESPONSE_LEN, buf);

    if ( result == USB_CMD_FLASH_QUERY_GOLDEN_SECTORS_LOCK_RESPONSE_LEN )
    {
        switch( buf[0] )
        {
        case 0:
        case 1:
            result = 0;
            if ( NULL != p_is_locked )
            {
                *p_is_locked = (buf[0] == 1);
            }
            break;
        case ENODEV:
            // The Flash chip couldn't be identified so the lock status
            // couldn't be queried.
            result = -ENODEV;
            break;
        case ENOTSUP:
            result = -ENOTSUP;
            break;
        default:
            result = -EIO;
            break;
        }
    }
    else
    {
        result = -EIO;
    }

    return result;
}

/*****************************************************************************/
/**
    @brief  Lock the Flash sectors for the Golden FPGA image
*/
int32_t usb_lock_golden_sectors(uint8_t index)
{
    int32_t result = 0;
    uint8_t addr[2] = { 0 };
    uint8_t buf[USB_CMD_FLASH_LOCK_GOLDEN_SECTORS_RESPONSE_LEN] = { 0xFF };

    if (!has_golden_protection_firmware(index))
    {
        return -ENOTSUP;
    }

    result = _usb_read_cmd(index, CMD_FLASH_LOCK_GOLDEN_SECTORS,
                addr, USB_CMD_FLASH_LOCK_GOLDEN_SECTORS_RESPONSE_LEN, buf);

    if ( result == USB_CMD_FLASH_LOCK_GOLDEN_SECTORS_RESPONSE_LEN )
    {
        switch( buf[0] )
        {
        case 0:
            result = 0;
            break;
        case ENODEV:
            result = -ENODEV;
            break;
        case ENOSYS:
            result = -ENOSYS;
            break;
        case ENOTSUP:
            result = -ENOTSUP;
            break;
        default:
            result = -EIO;
            break;
        }
    }
    else
    {
        result = -EIO;
    }

    return result;
}

/*****************************************************************************/
/**
    @brief  Unlock the Flash sectors for the Golden FPGA image
*/
int32_t usb_unlock_golden_sectors(uint8_t index)
{
    int32_t result = 0;
    uint8_t addr[2] = { 0 };
    uint8_t buf[USB_CMD_FLASH_UNLOCK_GOLDEN_SECTORS_RESPONSE_LEN] = { 0xFF };

    if (!has_golden_protection_firmware(index))
    {
        return -ENOTSUP;
    }

    result = _usb_read_cmd(index, CMD_FLASH_UNLOCK_GOLDEN_SECTORS,
                addr, USB_CMD_FLASH_UNLOCK_GOLDEN_SECTORS_RESPONSE_LEN, buf);

    if ( result == USB_CMD_FLASH_UNLOCK_GOLDEN_SECTORS_RESPONSE_LEN )
    {
        switch( buf[0] )
        {
        case 0:
            result = 0;
            break;
        case ENODEV:
            result = -ENODEV;
            break;
        case ENOSYS:
            result = -ENOSYS;
            break;
        case ENOTSUP:
            result = -ENOTSUP;
            break;
        default:
            result = -EIO;
            break;
        }
    }
    else
    {
        result = -EIO;
    }

    return result;
}


int32_t usb_read_flash_id(uint8_t index, uint8_t *p_id, uint8_t *p_id_length)
{
/**
    @todo   This should really use a constant in the firmware definition file; until then, we
            operate on the firmware's documentation for the CMD_READ_FLASH_ID command.
*/
#define USB_CMD_READ_FLASH_ID_RESPONSE_LEN  (10)
    int32_t result = 0;
    uint8_t addr[2] = { 0 };
    uint8_t buf[USB_CMD_READ_FLASH_ID_RESPONSE_LEN] = { 0xFF };

    result = _usb_read_cmd(index, CMD_READ_FLASH_ID, addr, USB_CMD_READ_FLASH_ID_RESPONSE_LEN, buf);
    if ( result == USB_CMD_READ_FLASH_ID_RESPONSE_LEN )
    {
        if (NULL != p_id)
        {
            memcpy(p_id, &(buf[0]), USB_CMD_READ_FLASH_ID_RESPONSE_LEN * sizeof(buf[0]));
        }
        if (NULL != p_id_length)
        {
            *p_id_length = USB_CMD_READ_FLASH_ID_RESPONSE_LEN;
        }
    }
    else
    {
        result = -EIO;
    }

    return result;
#undef USB_CMD_READ_FLASH_ID_RESPONSE_LEN
}

