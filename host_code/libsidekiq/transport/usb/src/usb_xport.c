/**
 * @file   usb_xport.c
 * @author  <info@epiq-solutions>
 * @date   Tue Nov 27 13:56:09 2018
 *
 * @brief  USB transport card related functions
 *
 * <pre>
 * Copyright 2016-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <inttypes.h>

#include "sidekiq_xport_api.h"
#include "sidekiq_util.h"       /* for u64_is_not_in_list() */

#include "usb_interface.h"
#include "usb_private.h"
#include "usb_transport.h"

#include "bit_ops.h"
#include "sidekiq_private.h"
#include "sidekiq_card_mgr.h"

/***** DEFINES *****/

#define USB_BUS_OFFSET                  16
#define USB_BUS_LEN                     8
#define USB_PORT_OFFSET                 8
#define USB_PORT_LEN                    8
#define USB_ADDR_OFFSET                 0
#define USB_ADDR_LEN                    8

#define USB_INVALID_ID                  (uint64_t)0xFFFFFFFFFFFFFFFF

static ARRAY_WITH_DEFAULTS(bool, avail_indexes, SKIQ_MAX_NUM_CARDS, true);
static ARRAY_WITH_DEFAULTS(uint64_t, uids, SKIQ_MAX_NUM_CARDS, USB_INVALID_ID);

static uint64_t _get_usb_uid( struct usb_xport_id *p_xport_id )
{
    uint64_t uid=0;

    RBF_SET(uid, p_xport_id->port_num, USB_PORT );
    RBF_SET(uid, p_xport_id->bus, USB_BUS );
    RBF_SET(uid, p_xport_id->addr, USB_ADDR );

    return (uid);
}

static void _get_usb_ids( uint64_t uid, struct usb_xport_id *p_xport_id )
{
    p_xport_id->port_num = RBF_GET( uid, USB_PORT );
    p_xport_id->bus = RBF_GET( uid, USB_BUS );
    p_xport_id->addr = RBF_GET( uid, USB_ADDR );
}

static int32_t _get_next_avail_index( uint8_t *p_index )
{
    int32_t status=-1;
    uint8_t i=0;

    for( i=0; (i<SKIQ_MAX_NUM_CARDS) && (status!=0); i++ )
    {
        if ( avail_indexes[i] )
        {
            status=0;
            *p_index = i;
            avail_indexes[i] = false;
        }
    }

    return (status);
}

static int32_t _free_index( uint8_t index )
{
    int32_t status=-1;

    if( index<SKIQ_MAX_NUM_CARDS )
    {
        avail_indexes[index] = true;
    }

    return (status);
}

static int32_t _insert_uid( uint64_t uid, uint8_t index )
{
    int32_t status=-1;

    if( index<SKIQ_MAX_NUM_CARDS )
    {
        status=0;
        uids[index] = uid;
    }

    return (status);
}

static int32_t _remove_uid( uint8_t index )
{
    int32_t status=-1;

    if( index<SKIQ_MAX_NUM_CARDS )
    {
        status=0;
        uids[index] = USB_INVALID_ID;
    }

    return (status);
}

static int32_t _find_index_by_uid( uint64_t uid, uint8_t *p_index )
{
    int32_t status=-1;
    uint8_t i=0;

    for( i=0; (i<SKIQ_MAX_NUM_CARDS) && (status!=0); i++ )
    {
        if( uids[i] == uid )
        {
            *p_index = i;
            status=0;
        }
    }

    return (status);
}


/***** LOCAL FUNCTIONS *****/
static int32_t _usb_xport_read_fpga_reg( uint64_t xport_uid,
                                         uint32_t addr,
                                         uint32_t *p_data );

static int32_t _usb_xport_write_fpga_reg( uint64_t xport_uid,
                                          uint32_t addr,
                                          uint32_t data );

static int32_t _usb_xport_rx_start_streaming( uint64_t xport_uid,
                                              skiq_rx_hdl_t hdl );

static int32_t _usb_xport_rx_stop_streaming( uint64_t xport_uid,
                                             skiq_rx_hdl_t hdl );

static int32_t _usb_xport_rx_pause_streaming( uint64_t xport_uid );

static int32_t _usb_xport_rx_resume_streaming( uint64_t xport_uid );

static int32_t _usb_xport_rx_receive( uint64_t xport_uid,
                                      uint8_t **pp_data,
                                      uint32_t *p_data_len );

static int32_t _usb_xport_rx_flush( uint64_t xport_uid );

static int32_t _usb_xport_rx_set_transfer_timeout( uint64_t xport_uid,
                                                   const int32_t timeout_us );


/***** LOCAL VARIABLES *****/

/** @brief USB transport functions related to the FPGA. */
static skiq_xport_fpga_functions_t _xport_fpga_funcs =
{
    .fpga_reg_read  = _usb_xport_read_fpga_reg,
    .fpga_reg_write = _usb_xport_write_fpga_reg,
    .fpga_down = NULL,
    .fpga_down_reload = NULL,
    .fpga_up = NULL,
};

/** @brief USB transport functions related to Rx. */
static skiq_xport_rx_functions_t _xport_rx_funcs =
{
    .rx_configure            = NULL,
    .rx_set_block_size       = NULL,
    .rx_set_buffered         = NULL,
    .rx_start_streaming      = _usb_xport_rx_start_streaming,
    .rx_stop_streaming       = _usb_xport_rx_stop_streaming,
    .rx_pause_streaming      = _usb_xport_rx_pause_streaming,
    .rx_resume_streaming     = _usb_xport_rx_resume_streaming,
    .rx_flush                = _usb_xport_rx_flush,
    .rx_set_transfer_timeout = _usb_xport_rx_set_transfer_timeout,
    .rx_receive              = _usb_xport_rx_receive,
};

/*****************************************************************************/
/**
   Function tied to the libsidekiq's transport hotplug function.

   @param[out] uid_list          list to be populated with detected transport UIDs
   @param[out] p_nr_uids         the number of cards detected by probe
   @param[in] no_probe_uids      list of transport UIDs not to probe
   @param[in] nr_no_probe_uids   the number of cards in the no_probe_uids array

   @return Nonzero value on error, zero on success.
*/
static int32_t _usb_xport_hotplug( uint64_t *uid_list,
                                   uint8_t *p_nr_uids,
                                   uint64_t no_probe_uids[],
                                   uint8_t nr_no_probe_uids )
{
    int32_t status = 0;
    struct usb_xport_id xport_ids[SKIQ_MAX_NUM_CARDS];
    uint8_t nr_discovered_cards = 0, i = 0;

    /* libusb is unique in that it can probe cards without adversely affecting their behavior (see
     * lsusb as proof), so use usb_probe() to find everything that matches our VID:PID, then filter
     * out what's reported back */
    status = usb_probe( xport_ids, &nr_discovered_cards );
    if ( status == 0 )
    {
        *p_nr_uids = 0;

        /* iterate over discovered cards and weed out those in the no probe list */
        for ( i = 0; i < nr_discovered_cards; i++ )
        {
            uint64_t uid = _get_usb_uid( &(xport_ids[i]) );

            if ( u64_is_not_in_list( uid, no_probe_uids, nr_no_probe_uids ) )
            {
                uid_list[(*p_nr_uids)++] = uid;
            }
        }
    }

    return (status);
}

/*****************************************************************************/
/** @brief Function tied to the libsidekiq's card_probe function.

    @param[out] p_uid_list     list to be populated with detected card numbers
    @param[out] p_nr_uids     the number of cards detected by probe

    @return Nonzero value on error, zero on success.
*/
static int32_t _usb_xport_probe( uint64_t *p_uid_list,
                                 uint8_t *p_nr_uids )
{
    return _usb_xport_hotplug( p_uid_list, p_nr_uids, NULL, 0 );
}

/*****************************************************************************/
/** @brief Function tied to the libsidekiq's card_init function.

    @param[in] level            initialization level
    @param[in] p_card_list      list of card numbers to initialize
    @param[in] num_cards        length of card list

    @return Nonzero value on error, zero on success

    @retval 0 success
    @retval -EIO error occurred during communication with the hardware
    @retval -ENOENT no record of card at specified transport UID
    @retval -ENOSPC insufficient resources to allocate to device
    @retval -ENOTSUP hardware does not meet minimum requirements to support request init level
*/
static int32_t _usb_xport_init(
    skiq_xport_init_level_t level,
    uint64_t xport_uid )
{
    int32_t status = 0;
    uint8_t index=0;

    skiq_xport_id_t id = SKIQ_XPORT_ID_INITIALIZER;
    struct usb_xport_id usb_bus;

    id.xport_uid = xport_uid;
    id.type = skiq_xport_type_usb;

    // get the USB bus info from the UID
    _get_usb_ids( xport_uid, &usb_bus );

    status=_get_next_avail_index( &index );
    if( status != 0 )
    {
        skiq_error("Unable to obtain next index for USB (%d, %" PRIu64 "\n", level, xport_uid);
        return (-ENOSPC);
    }

    status = usb_init( level, index, &usb_bus );
    if( status == 0 )
    {
        _insert_uid(xport_uid, index);

        // TODO: check for USB-capable FPGA before registering FPGA read/writes
        if( skiq_xport_init_level_full == level )
        {
            uint8_t card;

            /* Check for a compatible FX2 firmware version (>2.8) on Sidekiq M.2 before allowing
             * caller to initialize at skiq_xport_init_level_full */
            status = card_mgr_get_card( xport_uid, skiq_xport_type_usb, &card );
            if ( ( status == 0 ) && ( _skiq_get_part( card ) == skiq_m2 ) )
            {
                uint8_t fw_major, fw_minor;

                status = usb_read_fw_ver( index, &fw_major, &fw_minor );
                if ( status == 0 )
                {
                    if ( FW_VERSION(fw_major, fw_minor) < FW_VERSION(2,8) )
                    {
                        skiq_error("Firmware version v%u.%u does not meet minimum requirement of"
                                   " v2.8 necessary to use USB transport at"
                                   " skiq_xport_init_level_full on card %u\n",
                                   fw_major, fw_minor, card);
                        status = -ENOTSUP;
                    }
                }
                else
                {
                    skiq_error("Unable to query firmware version on card %u, cannot verify "
                               "capability is met\n", card);
                    status = -EIO;
                }
            }
            else if ( status != 0 )
            {
                skiq_error("Card manager has no record of Sidekiq unit at card index %u\n", card);
                status = -ENOENT;
            }

            if ( status == 0 )
            {
                xport_register_rx_functions(&id, &_xport_rx_funcs);
                xport_unregister_tx_functions(&id);
                xport_register_fpga_functions(&id, &_xport_fpga_funcs);
            }
        }
        else if( skiq_xport_init_level_basic == level )
        {
            xport_unregister_rx_functions(&id);
            xport_unregister_tx_functions(&id);

            xport_register_fpga_functions(&id,
                                          &_xport_fpga_funcs);
        }
    }

    return (status);
}

/*****************************************************************************/
/** @brief Function tied to the libsidekiq's card_exit function.

    @param[in] level            initialization level
    @param[in] p_card_list      list of card numbers to release
    @param[in] num_cards        length of card list

    @return Nonzero value on error, zero on success.
*/
static int32_t _usb_xport_exit(
    skiq_xport_init_level_t level,
    uint64_t xport_uid )
{
    int32_t status = 0;
    uint8_t index = 0;
    skiq_xport_id_t xport_id = SKIQ_XPORT_ID_INITIALIZER;

    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_usb;

    // suppress unused variable error, init level is tracked internally
    (void) level;

    status = _find_index_by_uid( xport_uid, &index );
    if ( status == 0 )
    {
        usb_free(index);

        /* unregister FPGA and RX functions unconditionally */
        (void) xport_unregister_fpga_functions(&xport_id);
        (void) xport_unregister_rx_functions(&xport_id);

        /* remove the UID from the associated index and make that index available to other cards */
        (void) _remove_uid( index );
        (void) _free_index( index );
    }

    return (status);
}

int32_t _usb_xport_read_fpga_reg( uint64_t xport_uid,
                                  uint32_t addr,
                                  uint32_t *p_data )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_read_fpga_reg( index, addr, p_data );
    }

    return (status);
}

int32_t _usb_xport_write_fpga_reg( uint64_t xport_uid,
                                   uint32_t addr,
                                   uint32_t data )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_write_fpga_reg( index, addr, data );
    }

    return (status);
}

int32_t _usb_xport_rx_start_streaming( uint64_t xport_uid,
                                       skiq_rx_hdl_t hdl )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_rx_start_streaming( index, hdl );
    }

    return (status);
}

int32_t _usb_xport_rx_stop_streaming( uint64_t xport_uid,
                                      skiq_rx_hdl_t hdl )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_rx_stop_streaming( index, hdl );
    }

    return (status);
}

int32_t _usb_xport_rx_pause_streaming( uint64_t xport_uid )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_rx_pause_streaming( index );
    }

    return (status);
}

int32_t _usb_xport_rx_resume_streaming( uint64_t xport_uid )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_rx_resume_streaming( index );
    }

    return (status);
}

int32_t _usb_xport_rx_receive( uint64_t xport_uid,
                               uint8_t **pp_data,
                               uint32_t *p_data_len )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_rx_receive( index, pp_data, p_data_len );
    }

    return (status);
}

int32_t _usb_xport_rx_flush( uint64_t xport_uid )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_index_by_uid( xport_uid, &index )) == 0 )
    {
        status = usb_rx_flush( index );
    }

    return (status);
}

int32_t _usb_xport_rx_set_transfer_timeout( uint64_t xport_uid,
                                            const int32_t timeout_us )
{
    int32_t status = -EINVAL;
    uint8_t index = 0;

    if ( ( status = _find_index_by_uid( xport_uid, &index ) ) == 0 )
    {
        status = usb_rx_set_transfer_timeout( index, timeout_us );
    }

    return status;
}


/*****************************************************************************/
/** @brief Gets the index based on the Sidekiq card # provided

    @param[in]  card            card ID
    @param[out] p_index         pointer to where to store the USB index

    @return Nonzero value on error, zero on success.
*/
int32_t usb_get_index( uint8_t card,
                       uint8_t *p_index )
{
    int32_t status=0;
    uint64_t uid=0;

    // we need to figure out our UID from card manager
    status = card_mgr_get_xport_id( card, skiq_xport_type_usb, &uid );
    if ( status == 0 )
    {
        // now we can get our index
        status = _find_index_by_uid( uid, p_index );
    }

    return (status);
}


/***** GLOBAL VARIABLES *****/

skiq_xport_card_functions_t usb_xport_card_funcs =
{
    .card_probe   = _usb_xport_probe,
    .card_hotplug = _usb_xport_hotplug,
    .card_init    = _usb_xport_init,
    .card_exit    = _usb_xport_exit,
};
