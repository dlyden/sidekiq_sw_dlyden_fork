/**
 * @file   usb_rx.c
 * @author <info@epiq-solutions.com>
 * @date   Wed Jan  9 13:50:11 2019
 * 
 * @brief  Provides receive functionality for the USB transport
 * 
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * </pre>
 * 
 */


/***** INCLUDES *****/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE                 /* See feature_test_macros(7) */
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>

#include "libusb.h"

#include "sidekiq_private.h"
#include "usb_interface.h"
#include "usb_private.h"
#include "hal_delay.h"

/***** DEFINES *****/
/// @brief The maximum number of USB bulk transfers issued by the host.
#define USB_BULK_TRANSFER_MAX_SUBMITTED (16)

/// @brief Request 4 blocks of Rx data per transfer to maximize throughput.
#define USB_BULK_TRANSFER_MAX_DATA_LEN (4 * SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES)

/// @brief Number of Rx data bytes we buffer on the host side.
#define RX_BUFFER_SIZE_BYTES (USB_BULK_TRANSFER_MAX_DATA_LEN * 1000)

/// @brief The total number of entries need in linked list.
#define RX_BUFFER_LINKED_LIST_LEN \
    (RX_BUFFER_SIZE_BYTES/SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES)

/// @brief The default size of the buffer for a Windows pipe
#define MINGW32_PIPE_NSIZE              4096


/***** LOCAL DATA *****/

ARRAY_WITH_DEFAULTS(static int32_t, timeout, SKIQ_MAX_NUM_CARDS, RX_TRANSFER_NO_WAIT);


/***** LOCAL FUNCTIONS*****/

/*****************************************************************************/
/** @brief Requests samples over USB by libusb.

    @param[in] p_rx     pointer to Rx interface to stream from

    @return Nonzero value on error, zero on success.
*/
static int32_t _stream_run(
    struct usb_rx* p_rx )
{
    struct timeval poll_time = {.tv_sec = 1L, .tv_usec = 0L,};
    struct libusb_transfer* p_transfer = NULL;
    uint32_t block_size = SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES;
    int32_t index = 0;
    uint32_t n = 0;
    int32_t r = 0;

    p_rx->cur_stat = USB_STREAM_ACTIVE;

    for( n = 0; n < p_rx->transfer_total; n++ )
    {
        p_transfer = p_rx->pp_transfer[n];
        p_transfer->buffer = NULL;

        // Prime the FIFO queue of libusb bulk transfers.
        (void) usb_signal_event( p_rx->write_pipe, p_transfer );
    }

    // MREUTMAN: We wait for the transfers to end "naturally" via their
    // predefined timeout value as opposed to calling libusb_cancel_transfer.
    // Looks like there is some sort of hardware related issue that may occur
    // on certain platforms if libusb_cancel_transfer is called multiple times.
    while( (USB_STREAM_ACTIVE == p_rx->req_stat) ||
           (0 != p_rx->transfer_pending) )
    {
        // Get the next available USB transfer that we can use.
        p_transfer = NULL;
        do
        {
            r = usb_poll(p_rx->read_pipe, &p_transfer, poll_time);
            if( 0 != r )
            {
                p_transfer = NULL;
            }
        } while( NULL == p_transfer );

        // Check to see if the transfer we now have access to has data
        // associated with it. If so, that means we have new data from the FPGA
        // and we need to add the new data to our linked list of Sidekiq Rx
        // data block that the user obtains when calling skiq_receive().
        if( p_transfer->buffer != NULL )
        {
            p_rx->transfer_pending -= 1;

            USB_LOCK(p_rx);
            if( LIBUSB_TRANSFER_COMPLETED == p_transfer->status )
            {
                for( n = 0; n < p_transfer->length; n += block_size )
                {
                    // Set pointer for Rx block to address of data.
                    p_rx->p_write->p_data = p_transfer->buffer + n;
                    p_rx->p_write->data_len = block_size;
                    // Move the write head of the linked list.
                    p_rx->p_write = p_rx->p_write->p_next;

                    /* notify any process blocking in usb_rx_receive() */
                    (void) usb_signal_event( p_rx->blocking_read_wpipe, p_transfer );
                }
            }
            // Zero out the next block to stop data from being read out by the
            // user when they call skiq_receive().
            p_rx->p_write->data_len = 0;
            USB_UNLOCK(p_rx);
        }

        if( USB_STREAM_ACTIVE == p_rx->req_stat )
        {
            // This is the address to where we want new data from the FGPA to
            // be placed.
            p_transfer->buffer = &(p_rx->p_buf[index]);
            index += p_transfer->length;

            // Wrap our buffer index if we have exceeded our memory bounds.
            if( RX_BUFFER_SIZE_BYTES <= index ) index = 0;

            r = libusb_submit_transfer(p_transfer);
            if( 0 != r )
            {
                // Reset buffer pointer so that next time we use it, we know
                // that it didn't return with any new data.
                p_transfer->buffer = NULL;
                (void) usb_signal_event( p_rx->write_pipe, p_transfer );
            }
            else
            {
                p_rx->transfer_pending += 1;
            }
        }
    }

    return 0;
}

/*****************************************************************************/
/** @brief Thread entry function used to start USB Rx streaming.

    @param[in] arg     pointer to Rx interface to start stream from

    @return Nonzero value on error, zero on success.
*/
static void _stream(
    void* const arg )
{
    struct usb_rx* p_rx = (struct usb_rx*) arg;
    uint32_t n;

    // TODO: I think we can grab the handle using usb_card_get_handle function,
    // preventing us from needing to cache it in the struct.

    // Prepare the the stream by initializing the needed parameters to default
    for( n = 0; n < p_rx->transfer_total; n++ )
    {
        // allocate the physical memory in libusb for the transfer
        p_rx->pp_transfer[n] = libusb_alloc_transfer(0);
        // Populate the transfer such that it can be used for bulk transfers.
        // Note, most all of these parameters stay constant, we only need to
        // update a few of these fields (such as the memory address) when
        // actively collecting data
        libusb_fill_bulk_transfer(p_rx->pp_transfer[n],
                                  p_rx->p_handle,
                                  USB_BULK_IN,
                                  NULL,
                                  p_rx->transfer_data_len,
                                  usb_callback,
                                  (void*) &(p_rx->write_pipe),
                                  10000);
    }

    p_rx->cur_stat= USB_STREAM_ACTIVE;
    p_rx->transfer_pending = 0;

    while( USB_STREAM_CLOSE != p_rx->req_stat )
    {
        if( USB_STREAM_ACTIVE == p_rx->req_stat )
        {
            _stream_run(p_rx);
        }
        else
        {
            p_rx->cur_stat = USB_STREAM_STOP;
            USB_LOCK(p_rx);
            {
                // Sleeps until the condition variable is asserted.
                pthread_cond_wait(&(p_rx->wait), &(p_rx->lock));
            }
            USB_UNLOCK(p_rx);
        }
    }

    // Before exiting thread, we need to free the libusb transfers.
    for( n = 0; n < p_rx->transfer_total; n++ )
    {
        libusb_free_transfer(p_rx->pp_transfer[n]);
    }

    p_rx->cur_stat = USB_STREAM_CLOSE;
}


static int32_t
create_fd_pair( read_pipe_handle_t *p_read_fd,
                write_pipe_handle_t *p_write_fd )
{
    int32_t status = 0;

#if (defined __MINGW32__)
    if (!CreatePipe(p_read_fd, p_write_fd, NULL, MINGW32_PIPE_NSIZE))
    {
        status = -1;
    }
#else
    int fd[2] = { UNOPENED_FD, UNOPENED_FD };

    status = pipe2(fd, O_NONBLOCK);
    if ( status == 0 )
    {
        *p_read_fd = fd[0];
        *p_write_fd = fd[1];
    }
    else
    {
        status = -1;
    }
#endif /* __MINGW32__ */

    return (status);
}


/***** GLOBAL FUNCTIONS *****/

/*****************************************************************************/
/** @brief Initilizes a given Rx interface.

    @param[in] p_rx         pointer to Rx interface to initialize
    @param[in] p_handle     USB handle to associate with the interface
    @param[in] index        index to associate with the interface

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_init(
    struct usb_rx* p_rx,
    struct libusb_device_handle* p_handle,
    uint8_t index)
{
    int32_t err = 0;
    uint32_t n = 0;

    USB_LOCK(p_rx);
    p_rx->id = index;
    p_rx->transfer_total = USB_BULK_TRANSFER_MAX_SUBMITTED;
    p_rx->transfer_data_len = USB_BULK_TRANSFER_MAX_DATA_LEN;

    // safety checks to ensure buffer alignment and boundaries are good
    assert(0 == (RX_BUFFER_SIZE_BYTES % p_rx->transfer_total));
    assert(0 == (RX_BUFFER_SIZE_BYTES % p_rx->transfer_data_len));

    // align memory to word boundary to prevent pointer address misalignment
#if (defined __MINGW32__)
    p_rx->p_buf = (uint8_t *)__mingw_aligned_malloc((size_t)RX_BUFFER_SIZE_BYTES,
                                                    (size_t)SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES);
#else
    err = posix_memalign((void **)&(p_rx->p_buf), SKIQ_MAX_RX_BLOCK_SIZE_IN_BYTES,
                         RX_BUFFER_SIZE_BYTES);
#endif /* __MINGW32__ */

    p_rx->p_list = malloc(sizeof(struct linked_list) *
                          RX_BUFFER_LINKED_LIST_LEN);

    p_rx->pp_transfer = malloc(sizeof(struct libusb_transfer*) *
                               p_rx->transfer_total);

    // Check to make sure memory was actually allocated in the case of
    // memory constrained systems. Also ensure that the buffer memory was
    // aligned correctly using posix_memalign.
    if( (NULL == p_rx->p_buf) ||
        (NULL == p_rx->p_list) ||
        (NULL == p_rx->pp_transfer) ||
        (err != 0) )
    {
        err = -1;
        goto _init_card_exit;
    }

    /* create communication pair between receive thread and USB callback */
    err = create_fd_pair( &p_rx->read_pipe, &p_rx->write_pipe );
    if ( err != 0 )
    {
        goto _init_card_exit;
    }

    /* create communication pair between skiq_receive() and receive thread to implement blocking
     * receive */
    err = create_fd_pair( &p_rx->blocking_read_rpipe, &p_rx->blocking_read_wpipe );
    if ( err != 0 )
    {
        goto _init_card_exit;
    }

    // Set up the linked list of Skiq Rx data blocks.
    for( n = 0; n < RX_BUFFER_LINKED_LIST_LEN-1; n++ )
    {
        p_rx->p_list[n].p_next = &(p_rx->p_list[n+1]);
        p_rx->p_list[n].p_data = NULL;
        p_rx->p_list[n].data_len = 0;
    }
    // link the tail to the head to form a circular linked list
    p_rx->p_list[n].p_next = &(p_rx->p_list[0]);
    p_rx->p_list[n].p_data = NULL;
    p_rx->p_list[n].data_len = 0;
    p_rx->p_read =  &(p_rx->p_list[0]);
    p_rx->p_write =  &(p_rx->p_list[0]);

    // assign libusb handle, this will mark it as "active"
    p_rx->p_handle = p_handle;

_init_card_exit:
    USB_UNLOCK(p_rx);
    if( 0 != err )
    {
        // Failed to initialize, free up anything that was allocated during
        // this call and return an error.
        usb_rx_free(p_rx);
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/** @brief Frees a given Rx interface.

    @param[in] p_rx         pointer to Rx interface to free

    @return void
*/
void usb_rx_free(
    struct usb_rx* p_rx )
{
    // Close the Rx streaming thread.
    if( USB_STREAM_ACTIVE == p_rx->cur_stat )
    {
        // USB Rx stream is active, signal it to close down.
        USB_LOCK(p_rx);
        {
            p_rx->req_stat = USB_STREAM_CLOSE;
        }
        USB_UNLOCK(p_rx);
        pthread_join(p_rx->thread, NULL);
    }
    else if( USB_STREAM_STOP == p_rx->cur_stat )
    {
        // The stream thread is stoped and has gone to sleep, wake it up and
        // signal it to close down.
        USB_LOCK(p_rx);
        {
            p_rx->req_stat = USB_STREAM_CLOSE;
            pthread_cond_signal(&(p_rx->wait));
        }
        USB_UNLOCK(p_rx);
        pthread_join(p_rx->thread, NULL);
    }

    // Free all allocated data.
    if( NULL != p_rx->p_buf )
    {
#if (defined __MINGW32__)
        __mingw_aligned_free(p_rx->p_buf);
#else
        free(p_rx->p_buf);
#endif /* __MINGW32__ */
        p_rx->p_buf = NULL;
    }

    FREE_IF_NOT_NULL( p_rx->p_list );
    FREE_IF_NOT_NULL( p_rx->pp_transfer );

    p_rx->p_read =  NULL;
    p_rx->p_write = NULL;

    CLOSE_FD_IF_OPEN( p_rx->read_pipe );
    CLOSE_FD_IF_OPEN( p_rx->write_pipe );
    CLOSE_FD_IF_OPEN( p_rx->blocking_read_rpipe );
    CLOSE_FD_IF_OPEN( p_rx->blocking_read_wpipe );

    // Set to NULL to indicate the stream is inactive
    p_rx->p_handle = NULL;
}

/*****************************************************************************/
/** @brief Starts Rx streaming on a given card.

    @param[in] index   index number of the Sidekiq of interest
    @param[in] hdl      receive handle to use

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_start_streaming(
    uint8_t index,
    skiq_rx_hdl_t hdl )
{
    struct usb_rx* p_rx = NULL;
    int32_t r = 0;

    // TODO: Support dual handles someday...
    if( skiq_rx_hdl_A1 != hdl )
    {
        return -1;
    }
    else if( 0 != usb_get_rx(index, &p_rx) )
    {
        return -1;
    }
    else if( USB_STREAM_ACTIVE == p_rx->cur_stat )
    {
        return 0;
    }
    else if( USB_STREAM_STOP == p_rx->cur_stat )
    {
        return usb_rx_resume_streaming(index);
    }

    USB_LOCK(p_rx);
    r = usb_gpif_fifo_enable(index);
    if( 0 != r )
    {
        USB_UNLOCK_AND_RETURN(p_rx, r);
    }

    p_rx->req_stat = USB_STREAM_ACTIVE;

    r = pthread_create(&(p_rx->thread), NULL, (void*) &_stream, (void*) p_rx);
    if( 0 != r )
    {
        // TODO: Check errno
        p_rx->req_stat = USB_STREAM_CLOSE;
        USB_UNLOCK_AND_RETURN(p_rx, r);
    }
    USB_UNLOCK(p_rx);

    return 0;
}

/*****************************************************************************/
/** @brief Stops Rx streaming on a given card.

    @param[in] index   index number of the Sidekiq of interest
    @param[in] hdl      receive handle to use

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_stop_streaming(
    uint8_t index,
    skiq_rx_hdl_t hdl )
{
    // TODO: Support dual handles someday...
    (void) hdl;

    // Pause and stop are essentially the same thing in USB land.
    return usb_rx_pause_streaming(index);
}

/*****************************************************************************/
/** @brief Pauses Rx streaming on a given card.

    @param[in] index   index number of the Sidekiq of interest

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_pause_streaming(
    uint8_t index )
{
    struct usb_rx* p_rx = NULL;
    int32_t r = 0;

    if( 0 != usb_get_rx(index, &p_rx) )
    {
        return -1;
    }
    else if( USB_STREAM_CLOSE == p_rx->cur_stat )
    {
        // The thread has already been stopped and returned, this is bad...
        return -1;
    }

    USB_LOCK(p_rx);
    p_rx->req_stat = USB_STREAM_STOP;
    USB_UNLOCK(p_rx);

    // TODO: pipe or pthread_cond_signal should probably go here to save on
    // CPU cycles and time delay.
    while( USB_STREAM_STOP != p_rx->cur_stat )
    {
        if( USB_STREAM_CLOSE == p_rx->cur_stat )
        {
            // Error: RX stream thread closed for card
            return -1;
        }

        /* portable way to sleep with subsecond precision */
        hal_microsleep( 100 );
    }

    // Disable GPIF FIFO to allow alternate use of pins on FX2.
    if( false == usb_is_tx_streaming(index) )
    {
        r = usb_gpif_fifo_disable(index);
        if( 0 != r )
        {
            return (r);
        }
    }

    return 0;
}

/*****************************************************************************/
/** @brief Resumes Rx streaming on a given card.

    @param[in] index   index number of the Sidekiq of interest

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_resume_streaming(
    uint8_t index )
{
    struct usb_rx* p_rx = NULL;
    int32_t r = 0;

    if( 0 != usb_get_rx(index, &p_rx) )
    {
        return -1;
    }

    USB_LOCK(p_rx);
    r = usb_gpif_fifo_enable(index);
    if( 0 != r )
    {
        USB_UNLOCK_AND_RETURN(p_rx, r);
    }

    p_rx->req_stat = USB_STREAM_ACTIVE;
    pthread_cond_signal(&(p_rx->wait));
    USB_UNLOCK(p_rx);

    // TODO: Wait here until stream thread reports USB_STREAM_ACTIVE?

    return 0;
}

/*****************************************************************************/
/** @brief Obtains one data block of received I/Q data.

    @param[in] index   index number of the Sidekiq of interest
    @param[in] pp_data      pointer to be updated with address of I/Q data block
    @param[in] p_data_len   length of I/Q data block

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_receive(
    uint8_t index,
    uint8_t **pp_data,
    uint32_t *p_data_len )
{
    struct usb_rx* p_rx = NULL;
    int32_t status = 0;

    if( 0 != usb_get_rx(index, &p_rx) )
    {
        status = skiq_rx_status_error_generic;
    }

    if ( status == 0 )
    {
        /* perform polling on blocking_read_rpipe */
        {
            struct libusb_transfer *p_transfer = NULL;
            struct timeval poll_timeout;

            if ( timeout[index] == RX_TRANSFER_WAIT_FOREVER )
            {
                poll_timeout.tv_sec = 1;
                poll_timeout.tv_usec = 0;
            }
            else if ( timeout[index] == RX_TRANSFER_NO_WAIT )
            {
                /* from select(2) man page: If both fields of the timeval structure are zero, then
                 * select() returns immediately. */
                poll_timeout.tv_sec = 0;
                poll_timeout.tv_usec = 0;
            }
#if (defined __MINGW32__)
            else if ( timeout[index] >= (long)1e6 )
            {
                poll_timeout.tv_sec = ( timeout[index] / (long)1e6 );
                poll_timeout.tv_usec = ( timeout[index] % (long)1e6 );
            }
#else
            else if ( timeout[index] >= (suseconds_t)1e6 )
            {
                poll_timeout.tv_sec = ( timeout[index] / (suseconds_t)1e6 );
                poll_timeout.tv_usec = ( timeout[index] % (suseconds_t)1e6 );
            }
#endif
            else
            {
                poll_timeout.tv_sec = 0;
                poll_timeout.tv_usec = timeout[index];
            }

            /* call usb_poll at least one with poll_timeout.  Loop if timeout[index] is
             * RX_TRANSFER_WAIT_FOREVER instead of trying to pick some large timeval timeout */
            do
            {
                status = usb_poll( p_rx->blocking_read_rpipe, &p_transfer, poll_timeout );

            } while ( ( status == -ETIMEDOUT ) && ( timeout[index] == RX_TRANSFER_WAIT_FOREVER ) );

            /* if usb_poll() indicates a timeout occurred, that's okay since the next block of code
             * will determine if there's data present or not and return an appropriate code
             * accordingly */
            if ( status == -ETIMEDOUT )
            {
                status = 0;
            }
        }

        if ( status == 0 )
        {
            USB_LOCK(p_rx);
            if( USB_STREAM_ACTIVE != p_rx->cur_stat )
            {
                status = skiq_rx_status_error_generic;
            }
            else if( 0 == p_rx->p_read->data_len )
            {
                status = skiq_rx_status_no_data;
            }
            else
            {
                // update the passed in pointers
                *pp_data = p_rx->p_read->p_data;
                *p_data_len = p_rx->p_read->data_len;

                // Set data length for this block to zero to prevent reading old data.
                p_rx->p_read->data_len = 0;

                // Move the read pointer to the next rx data block in the linked list.
                p_rx->p_read = p_rx->p_read->p_next;

                /* success */
                status = 0;
            }
            USB_UNLOCK(p_rx);
        }
    }

    return status;
}


/*****************************************************************************/
/** @brief Clears out all received data for a given card.

    @param[in] index   index number of the Sidekiq of interest

    @return Nonzero value on error, zero if function completed successfully.
*/
int32_t usb_rx_flush(
    uint8_t index )
{
    struct usb_rx* p_rx = NULL;

    if( 0 != usb_get_rx(index, &p_rx) )
    {
        return -1;
    }

    // TODO: Make sure usb streaming has paused/stopped?
    USB_LOCK(p_rx);
    if( 0 != usb_reset_bulk_in_endpoint(index) )
    {
        USB_UNLOCK_AND_RETURN(p_rx, -1);
    }

    // This really doesn't clear out all data since iterating through all
    // entries would take a bit of time. Instead, we just reset the read/write
    // heads and set the first entry to zero data. This allows old data to get
    // overwritten by new data when available while keeping the user waiting
    // until the first entry has non-zero data.
    p_rx->p_list[0].data_len = 0;
    p_rx->p_write = &(p_rx->p_list[0]);
    p_rx->p_read = &(p_rx->p_list[0]);
    USB_UNLOCK(p_rx);

    return 0;
}


/*****************************************************************************/
/** @brief Sets the receive transfer timeout for a given uid

    @param[in] index      index number of the Sidekiq of interest
    @param[in] timeout_us timeout (in microseconds or RX_TRANSFER_WAIT_FOREVER or RX_TRANSFER_NO_WAIT)

    @return int32_t
    @retval 0 Success
    @retval -EINVAL Specified timeout value is invalid (outside of the acceptable range or negative)
*/
int32_t
usb_rx_set_transfer_timeout( uint8_t index,
                             const int32_t timeout_us )
{
    int32_t status = 0;

    if ( ( RX_TRANSFER_WAIT_FOREVER == timeout_us ) ||
         ( RX_TRANSFER_NO_WAIT == timeout_us ) ||
         ( ( 20 <= timeout_us ) && ( timeout_us <= 1000000 ) ) )
    {
        timeout[index] = timeout_us;
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}
