#ifndef __USB_TRANSPORT_H__
#define __USB_TRANSPORT_H__

/*! \file usb_transport.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_api.h"
#include "sidekiq_xport_types.h"


/***** GLOBAL VARIABLES  *****/

/** @brief The exported transport card functions to be used by libsidekiq. */
extern skiq_xport_card_functions_t usb_xport_card_funcs;


#endif  /* __USB_TRANSPORT_H__ */
