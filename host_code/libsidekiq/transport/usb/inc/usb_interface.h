#ifndef __USB_INTERFACE_H__
#define __USB_INTERFACE_H__

/*! \file usb_interface.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_api.h"
#include "flash_defines.h"

/***** GLOBAL FUNCTIONS  *****/
struct usb_xport_id;

/*****************************************************************************/
/** @brief Probes the USB interface for the provided serial numbers if 
    *p_num_cards=0. Otherwise, searches for any Sidekiq cards on USB.

    @param[out] p_xport_id       list of USB interfacesfound
    @param[out] num_cards        number of cards in list
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_probe(
    struct usb_xport_id *p_xport_id,
    uint8_t *p_num_cards );

/*****************************************************************************/
/** @brief Initilizes the USB interface for the provided cards.

    @param[in] level            init level
    @param[in] index            index of card to initialize
    @param[in] p_id             pointer to xport_id of card to init
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_init( skiq_xport_init_level_t level, uint8_t index, struct usb_xport_id *p_id );

/*****************************************************************************/
/** @brief Frees the USB interface for the provided cards.

    @param[in] index index of card to free
    
    @return void
*/
extern void usb_free(
    uint8_t index );

/*****************************************************************************/
/** @brief Returns the connection status of the USB interface for a given
    index

    @param[in] index   index number of the Sidekiq of interest
    
    @return true if the USB is connected
*/
extern bool usb_is_connected(
    uint8_t index );

/*****************************************************************************/
/** @brief Returns the streaming status of the USB receive interface.
    
    @param[in] index   index number of the Sidekiq of interest
    
    @return true if the USB Rx interface is currently streaming
*/
extern bool usb_is_rx_streaming(
    uint8_t index);

/*****************************************************************************/
/** @brief Returns the streaming status of the USB transmit interface.

    @param[in] index   index number of the Sidekiq of interest
    
    @return true if the USB Tx interface is currently streaming
*/
extern bool usb_is_tx_streaming(
    uint8_t index );

/*****************************************************************************/
/** @brief Starts Rx streaming on a given card.

    @param[in] index   index number of the Sidekiq of interest
    @param[in] hdl      receive handle to use
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_start_streaming(
    uint8_t index,
    skiq_rx_hdl_t hdl );

/*****************************************************************************/
/** @brief Stops Rx streaming on a given card.
  
    @param[in] index   index number of the Sidekiq of interest
    @param[in] hdl      receive handle to use
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_stop_streaming(
    uint8_t index,
    skiq_rx_hdl_t hdl );

/*****************************************************************************/
/** @brief Pauses Rx streaming on a given card.

    @param[in] index   index number of the Sidekiq of interest  
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_pause_streaming(
    uint8_t index );

/*****************************************************************************/
/** @brief Resumes Rx streaming on a given card.
  
    @param[in] index   index number of the Sidekiq of interest
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_resume_streaming( 
    uint8_t index );

/*****************************************************************************/
/** @brief Obtains one data block of received I/Q data.
  
    @param[in] index   index number of the Sidekiq of interest
    @param[in] pp_data      pointer to be updated with address of I/Q data block
    @param[in] p_data_len   length of I/Q data block
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_receive( 
    uint8_t index,
    uint8_t **pp_data,
    uint32_t *p_data_len );

/*****************************************************************************/
/** @brief Clears out all received data for a given card.
 
    @param[in] index   index number of the Sidekiq of interest 
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_flush( 
    uint8_t index );

/*****************************************************************************/
/** @brief Sets the receive transfer timeout for a given uid

    @param[in] index      index number of the Sidekiq of interest
    @param[in] timeout_us timeout (in microseconds or RX_TRANSFER_WAIT_FOREVER or RX_TRANSFER_NO_WAIT)

    @return int32_t
    @retval 0 Success
    @retval -EINVAL Specified timeout value is invalid (outside of the acceptable range or negative)
*/
extern int32_t usb_rx_set_transfer_timeout(
    uint8_t index,
    const int32_t timeout_us );

/*****************************************************************************/
/** @brief Reads the firmware version, serial number, and board number.

    @param[in] index            index number of the Sidekiq of interest
    @param[out] p_fw_maj        pointer to be updated with firmware major 
                                version
    @param[out] p_fw_min        pointer to be updated with firmware minor 
                                version
    @param[out] p_serial_num    pointer to be updated with the serial number
    @param[out] p_board_num     pointer to be updated with the board number
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_version(
    uint8_t index,
    uint8_t* p_fw_maj,
    uint8_t* p_fw_min,
    uint32_t* p_serial_num,
    uint16_t* p_board_num );

/*****************************************************************************/
/** @brief Writes the serial and board number to EEPROM.

    @param[in] index            index number of the Sidekiq of interest
    @param[in] serial_num       the serial numner to write
    @param[in] board_num        the board number to write
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_version(
    uint8_t index,
    uint32_t serial_num,
    uint16_t board_num );

/*****************************************************************************/
/** @brief Reads the state of the LED connected to USB microcontroller.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_led   pointer to where to store the LED (1=ON, 0=OFF)
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_led(
    uint8_t index,
    uint8_t* p_led );

/*****************************************************************************/
/** @brief Writes the state of the LED connected to USB microcontroller.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] led      state to set the LED to (1=ON, 0=OFF)
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_led( 
    uint8_t index,
    uint8_t led );

/*****************************************************************************/
/** @brief  Reads the version of the firmware.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_fw_maj    pointer to where to store the major verson number
    @param[out] p_fw_min    pointer to where to store the minor verson number
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_fw_ver(
    uint8_t index,
    uint8_t* p_fw_maj,
    uint8_t* p_fw_min );

/*****************************************************************************/
/** @brief Reads the board number.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_board_num     pointer to where to store the board number
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_board_num(
    uint8_t index,
    uint16_t* p_board_num );

/*****************************************************************************/
/** @brief Writes the board number to EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] board_num    the board number to write
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_board_num(
    uint8_t index,
    uint16_t board_num );

/*****************************************************************************/
/** @brief Reads the serial number of the board.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_serial_num    pointer to where to store the serial number
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_serial_num(
    uint8_t index,
    uint32_t* p_serial_num );

/*****************************************************************************/
/** @brief Writes the serial number of the board to EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] serial_num   pointer serial number to store
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_serial_num(
    uint8_t index,
    uint32_t serial_num );

/*****************************************************************************/
/** @brief Reads data from the accelerometer register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg      register of accelerometer to access
    @param[out] p_data  pointer of where to store the data read
    @param[in] length   number of bytes to read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_accel_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t* p_data,
    uint8_t length );

/*****************************************************************************/
/** @brief Writes data to the accelerometer register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg      register of accelerometer to write to
    @param[in] p_data   pointer to the data to write
    @param[in] length   number of bytes to write
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_accel_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t* p_data,
    uint8_t length );

/*****************************************************************************/
/** @brief Reads the specified register from the temperature sensor.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg          register of temp sensor to read
    @param[out] p_data      pointer of where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_temp_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t* p_data );

/*****************************************************************************/
/** @brief Writes to the specified register of the temperature sensor.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg      register of temp sensor to write to
    @param[in] data     value to write (only single byte access is allowed here)
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_temp_reg(
    uint8_t index,
    uint8_t reg,
    uint8_t data );

/*****************************************************************************/
/** @brief Reads data from the flash. Note: only a page (256 bytes) at a time 
    can be accessed, so the address provided is the upper 2 bytes of the 
    address, and the lowest byte is always 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_buffer    pointer of where to store the data read
    @param[in] start_addr   upper 2 bytes of the address to read from
    @param[in] length       number of bytes to read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_flash(
    uint8_t index,
    uint8_t* p_buffer,
    uint32_t address,
    uint16_t length );

/*****************************************************************************/
/** @brief Writes data to the flash. Note: only a page (256 bytes) at a time 
    can be accessed, so the address provided is the upper 2 bytes of the 
    address, and the lowest byte is always 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] p_buffer     pointer to the data to write
    @param[in] address      upper 2 bytes of the address to write to
    @param[in] length       number of bytes to write
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_flash(
    uint8_t index,
    uint8_t* p_buffer,
    uint32_t address,
    uint16_t length );

/*****************************************************************************/
/** @brief Erases a sector of flash.  Note: only a page (256 bytes) at a time 
    can be accessed, so the address provided is the upper 2 bytes of the 
    address, and the lowest byte is always 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      upper 2 bytes of the address to erase
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_erase_flash_sector(
    uint8_t index,
    uint32_t address );

/*****************************************************************************/
/** @brief Erases the entire flash. Note that this process may take a very long
    time (20 seconds or longer).

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_erase_flash(
    uint8_t index );

/*****************************************************************************/
/** @brief Prepares the FPGA to be programmed.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_init_fpga_for_programming(
    uint8_t index );

/*****************************************************************************/
/** @brief Sends the FPGA configuration data to FPGA programming interface.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] p_buffer         pointer to the configuration data to send
    @param[in] length           number of bytes in the buffer
    @param[in] legacy_method    flag indicating whether to use legacy
                                (ctrl transfer) method instead of bulk transfer
           
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_send_fpga_config(
    uint8_t index,
    uint8_t* p_buffer,
    uint32_t length,
    uint8_t legacy_method );

/*****************************************************************************/
/** The usb_finish_fpga_programming finalizes the FPGA configuration process.

    @param[in] index    index number of the Sidekiq of interest

    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_finish_fpga_programming(
    uint8_t index );

/*****************************************************************************/
/** @brief Configures FPGA programming to be loaded from the flash.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_load_fpga_from_flash(
    uint8_t index );

/*****************************************************************************/
/** @brief Reads the state of the FPGA.  It is configured if it is set to 1, 
    otherwise, the config flag is set to 0.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_config    pointer to where to store the config flag
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_fpga_config(
    uint8_t index,
    uint8_t *p_config );

/*****************************************************************************/
/** @brief Reads the contents of the EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      starting address of where to read from
    @param[in] length       number of bytes to read
    @param[out] p_buffer    pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_eeprom_contents( 
    uint8_t index,
    uint16_t address,
    uint16_t length,
    uint8_t* p_buffer );

/*****************************************************************************/
/** @brief Writes data to the EEPROM.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      starting address of where to write to
    @param[in] length       number of bytes to write
    @param[out] p_buffer    pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_eeprom_contents( 
    uint8_t index,
    uint16_t address,
    uint16_t length,
    uint8_t* p_buffer );

/*****************************************************************************/
/** @brief Reads the FPGA register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      register address to read
    @param[out] p_data      pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_fpga_reg(
    uint8_t index,
    uint32_t address,
    uint32_t* p_data );

/*****************************************************************************/
/** @brief Writes the FPGA register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      register address to write to
    @param[in] data         data to write to the register
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_fpga_reg( 
    uint8_t index,
    uint32_t address,
    uint32_t data );

/*****************************************************************************/
/** @brief Sends a reset command to the USB chip (this is really only needed if
    we're running in USB only mode but shouldn't hurt anything to do it on 
    other builds).

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address      register address to write to
    @param[in] data         data to write to the register
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_fpga_reset( 
    uint8_t index );

/*****************************************************************************/
/** @brief Initializes the USB chip to support either a read or a write across 
    the GPIF interface from the USB chip's perspective.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] write    flag set to true if USB chip is going to write
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_init_gpif_test(
    uint8_t index,
    bool write );

/*****************************************************************************/
/** @brief Outputs the data across the GPIF interface from the USB chip.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] write    data to write/output
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_gpif_test_write(
    uint8_t index,
    uint16_t write );

/*****************************************************************************/
/** The usb_gpif_test_read reads the data on the GPIF interface to the
    USB chip.

    @param[in] index    index number of the Sidekiq of interest
    @param p_read pointer to the data read
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_gpif_test_read(
    uint8_t index,
    uint16_t* p_read );

/*****************************************************************************/
/** @brief Reads the data on the GPIF interface to the USB chip.

    @param[in] index    index number of the Sidekiq of interest
    @param[out] p_read      pointer to the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_exit_gpif_test(
    uint8_t index );

/*****************************************************************************/
/** @brief Turns on the slave fifo interface on the FX2's GPIF interface.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_gpif_fifo_enable(
    uint8_t index );

/*****************************************************************************/
/** @brief Turns off the slave fifo interface on the FX2's GPIF interface.

    @param[in] index    index number of the Sidekiq of interest
        
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_gpif_fifo_disable(
    uint8_t index );

/*****************************************************************************/
/** @brief Resets endpoint 2 on the FX2 microcontroller that is used for 
    sending bulk transfer in towards the host.

    @param[in] index    index number of the Sidekiq of interest
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_reset_bulk_in_endpoint(
    uint8_t index );

/*****************************************************************************/
/** @brief Reads the io expander register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] reg register     address to read
    @param[out] p_data          pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_read_io_expander_reg(
    uint8_t index, uint8_t address,
    uint8_t* p_data );

/*****************************************************************************/
/** @brief Writes the io expander register specified.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] address  register address to read
    @param[in] p_data   pointer to where to store the data read
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_io_expander_reg(
    uint8_t index,
    uint8_t address,
    uint8_t* p_data );

/*****************************************************************************/
/** @brief Writes the new reference clock configuration.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] ref_clock    pointer reference clock configuration to push
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_ref_clock( 
    uint8_t index,
    skiq_ref_clock_select_t ref_clk );

/*****************************************************************************/
/** @brief Performs an I2C write transaction on a Sidekiq card over USB.

    @param[in] index    index number of the Sidekiq of interest
    @param[in] i2c_addr     I2C slave address of device to read from.
    @param[in] p_data       Buffer address to write data.
    @param[in] num_bytes    Number of bytes to write to device from buffer.
 
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_i2c( 
    uint8_t index,
    uint8_t i2c_addr,
    uint8_t *p_data,
    uint8_t num_bytes );

/*****************************************************************************/
/** @brief Performs an I2C write then read transaction on a Sidekiq card over
    USB.

    @warning _usb_read_cmd does not only read data.  Initially it writes the
    register that it will be reading from and then reads.

    @param[in] index        index number of the Sidekiq of interest
    @param[in] i2c_addr     I2C slave address of device to read from.
    @param[in] reg_addr     Register address of where to read data.
    @param[out] p_data      Buffer address to store read data.
    @param[in] num_bytes    Number of bytes to read into the buffer.
 
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_then_read_i2c( 
    uint8_t index,
    uint8_t i2c_addr,
    uint8_t reg_addr,
    uint8_t *p_data,
    uint8_t num_bytes );

/*****************************************************************************/
/** @brief Allows software to place the FX2 into a reset state.
    
    @param[in] index    index number of the Sidekiq of interest
    @param[in] hold_reset   Set to true to place into reset, false to allow
                            firmware to resume operation.
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_hold_firmware_reset(
    uint8_t index,
    bool hold_reset);

/*****************************************************************************/
/** @brief Directly write to the FX2's RAM. Extreme caution should be used with
    with function as it is possible to make the firmware unoperable.
    
    @param[in] index    index number of the Sidekiq of interest
    @param[in] addr         RAM address to write.
    @param[in] data         Pointer to data to write.
    @param[in] data_len     Length of data to write.
    
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_write_firmware_ram(
    uint8_t index,
    uint16_t addr,
    uint8_t* data,
    uint16_t data_len);

/*****************************************************************************/
/**
    @brief  Query the Flash chip's manufacturer info

    @param[in]  index               index number of the Sidekiq of interest
    @param[out] p_detected_flash    if not NULL, the detected Flash chip type (see FlashChip type)
    @param[out] p_manufacturer_id   if not NULL, the Flash's JEDEC manufacturer ID
    @param[out] p_memory_type       if not NULL, the Flash's JEDEC memory type ID
    @param[out] p_memory_capacity   if not NULL, the Flash's JEDEC memory capacity ID

    @return 0 on success, else an error code
    @retval 0       if successful
    @retval -EIO    if the USB command couldn't be issued
*/
extern int32_t usb_read_flash_manufacturer_info(uint8_t index, FlashChip *p_detected_flash,
    uint8_t *p_manufacturer_id, uint8_t *p_memory_type, uint8_t *p_memory_capacity);

/*****************************************************************************/
/**
    @brief  Query if the sectors that store the Golden FPGA image are locked.

    @param[in]  index           index number of the Sidekiq of interest
    @param[out] p_is_locked     pointer to be updated with the lock status
                                (if pointer is not NULL)

    @return 0 on success, else an error code
    @retval 0           if successful
    @retval -EIO        if the USB command couldn't be issued or the result code
                        is invalid
    @retval -ENODEV     if the Flash chip type is unknown and the lock status
                        couldn't be queried
    @retval -ENOTSUP    if the Sidekiq card does not support locking / unlocking
                        Flash sectors
*/
extern int32_t usb_is_golden_locked(uint8_t index, bool *p_is_locked);

/*****************************************************************************/
/**
    @brief  Lock the Flash sectors for the Golden FPGA image

    @param[in]  index           index number of the Sidekiq of interest

    @return 0 on success, else an error code
    @retval 0           if successful
    @retval -EIO        if the USB command couldn't be issued or the result code
                        is invalid
    @retval -ENODEV     if the Flash chip type is unknown and the lock status
                        couldn't be queried
    @retval -ENOSYS     if the Sidekiq card's firmware does not support
                        locking / unlocking Flash sectors
    @retval -ENOTSUP    if the Sidekiq card does not support locking / unlocking
                        Flash sectors
*/
extern int32_t usb_lock_golden_sectors(uint8_t index);

/*****************************************************************************/
/**
    @brief  Unlock the Flash sectors for the Golden FPGA image

    @param[in]  index           index number of the Sidekiq of interest

    @return 0 on success, else an error code
    @retval 0           if successful
    @retval -EIO        if the USB command couldn't be issued or the result code
                        is invalid
    @retval -ENODEV     if the Flash chip type is unknown and the sectors
                        couldn't be unlocked
    @retval -ENOSYS     if the Sidekiq card's firmware does not support
                        locking / unlocking Flash sectors
    @retval -ENOTSUP    if the Sidekiq card does not support locking / unlocking
                        Flash sectors
*/
extern int32_t usb_unlock_golden_sectors(uint8_t index);

/**
    @brief  Read the Flash chip's manufacturing unique ID (if present)

    @param[in]  index           index number of the Sidekiq of interest
    @param[out] p_id            If not NULL, the read unique ID; this should be at least 16 bytes
                                long
    @param[out] p_id_length     If not NULL, the length of the ID read into @p p_id (in bytes)

    @return 0 on success, else an error code
    @retval 0           if successful
    @retval -EIO        if the USB command couldn't be issued or the result code is invalid
*/
extern int32_t usb_read_flash_id(uint8_t index, uint8_t *p_id, uint8_t *p_id_length);

#endif  /* __USB_H__ */

