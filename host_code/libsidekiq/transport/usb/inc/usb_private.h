#ifndef __USB_PRIVATE_H__
#define __USB_PRIVATE_H__

/*! \file usb_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>

#include "libusb.h"

#include "sidekiq_api.h"
#include "sidekiq_usb_cmds.h"
#include "sidekiq_xport_types.h"


/***** DEFINES *****/

#define USB_CTRL_MAX_LENGTH (256)

#define USB_CTRL_BUFFER_LENGTH (LIBUSB_CONTROL_SETUP_SIZE + USB_CTRL_MAX_LENGTH) 

/** @brief Initializer for the struct usb_rx. */
#define USB_RX_INITIALIZER                          \
    {                                               \
        .p_handle = NULL,                           \
        .pp_transfer = NULL,                        \
        .transfer_data_len = 0,                     \
        .transfer_pending = 0,                      \
        .transfer_total = 0,                        \
        .read_pipe = UNOPENED_FD,                   \
        .write_pipe = UNOPENED_FD,                  \
        .blocking_read_rpipe = UNOPENED_FD,          \
        .blocking_read_wpipe = UNOPENED_FD,         \
        .p_list = NULL,                             \
        .p_write = NULL,                            \
        .p_read = NULL,                             \
        .p_buf = NULL,                              \
        .req_stat = USB_STREAM_CLOSE,               \
        .cur_stat = USB_STREAM_CLOSE,               \
        .id = 0                                     \
    }

/** @brief Initializer for the struct usb_ctrl */
#define USB_CTRL_INITIALIZER                        \
    {                                               \
        .p_handle = NULL,                           \
        .p_transfer = NULL,                         \
        .read_pipe = UNOPENED_FD,                   \
        .write_pipe = UNOPENED_FD,                  \
        .lock = PTHREAD_MUTEX_INITIALIZER,          \
        .id = 0,                                    \
    }

/** @brief Initializer for usb_card struct */
#define USB_CARD_INITIALIZER                        \
    {                                               \
      .p_handle = NULL,                             \
      .init_level = skiq_xport_init_level_unknown,  \
      .ctrl = USB_CTRL_INITIALIZER,                 \
      .rx = USB_RX_INITIALIZER,                     \
      .id = 0,                                      \
    }
    

#if (defined __MINGW32__)
typedef HANDLE read_pipe_handle_t;
typedef HANDLE write_pipe_handle_t ;
#else
typedef int read_pipe_handle_t;
typedef int write_pipe_handle_t;
#endif /* __MINGW32__ */

/***** MACROS *****/

#define USB_LOCK(_p)                    LOCK(_p->lock)
#define USB_UNLOCK(_p)                  UNLOCK(_p->lock)
#define USB_UNLOCK_AND_RETURN(_p,_r)    UNLOCK_AND_RETURN(_p->lock,_r)


/***** ENUMS *****/

/** @brief The valid USB Rx/Tx streaming states. */
enum usb_stream_status
{
    ///< Stream thread is actively collecting data.
    USB_STREAM_ACTIVE,
    /// Stream thread is paused/stopped and is waiting to be resumed.
    USB_STREAM_STOP,
    ///< Stream is closed and all data has been deallocated.
    USB_STREAM_CLOSE
};


/***** STRUCTS *****/

struct usb_xport_id
{
    uint8_t bus;
    uint8_t port_num;
    uint8_t addr;
};

/** @brief A simple singularly linked list. */
struct linked_list
{
    ///< Pointer to data payload of linked list entry.
    uint8_t* p_data;
    ///< Length of data in bytes pointed at by p_data.
    uint32_t data_len;
    ///< The next linked list entry.
    struct linked_list* p_next;
};

/** @brief Struct of all USB Rx interface parameters. */
struct usb_rx
{
    ///< USB handle used to talk to Sidekiq/FX2.
    struct libusb_device_handle* p_handle;
    ///< Array of pointers to libusb transfer.
    struct libusb_transfer** pp_transfer;
    ///< Length in bytes of data requested by USB bulk transfer.
    int32_t transfer_data_len;
    ///< Number of USB bulk transfers waiting to be serviced.
    volatile int32_t transfer_pending;
    ///< Total number of USB bulk transfers available for use.
    int32_t transfer_total;
    ///< Pipe to obtain the next USB transfer to use. 
    read_pipe_handle_t read_pipe;
    ///< Pipe to allow USB transfer to submit itself for reuse.
    write_pipe_handle_t write_pipe;

    ///< Pipe to performing blocking receive.
    read_pipe_handle_t blocking_read_rpipe;
    ///< Pipe to signal to blocking read pipe that a transfer is now available
    write_pipe_handle_t blocking_read_wpipe;

    ///< Linked list head used for keeping track of available Rx data.
    struct linked_list* p_list;
    ///< Linked list entry to read from, points to oldest data.
    struct linked_list* p_read;
    ///< Linked list entry to write to, points to newest data.
    struct linked_list* p_write;
    ///< Host side data buffer of Rx data.
    uint8_t* p_buf;
    ///< The requested next status of the stream thread.
    volatile enum usb_stream_status req_stat;
    ///< The current status of the stream thread.
    volatile enum usb_stream_status cur_stat;
    ///< Thread used to manage the Rx stream of data.
    pthread_t thread;
    ///< Mutex used for allowing shared data access with thread.
    pthread_mutex_t lock;
    ///< Wait variable used to allow thread to sleep.
    pthread_cond_t wait;
    ///< Rx stream id, this should be the same value as the card number.
    int32_t id;
};

/** @brief Struct of all USB control interface parameters. */
struct usb_ctrl
{           
    ///< USB handle used to talk to Sidekiq/FX2.
    struct libusb_device_handle* p_handle;
    ///< The libusb transfer to use for control reads/writes.
    struct libusb_transfer* p_transfer;
    ///< Pipe to obtain the next USB transfer to use.
    read_pipe_handle_t read_pipe;
    ///< Pipe to allow USB transfer to submit itself for reuse.
    write_pipe_handle_t write_pipe;

    ///< Buffer used to contain control transfer data.
    uint8_t p_buf[USB_CTRL_BUFFER_LENGTH];
    ///< Mutex lock to serialize software issuing control transfers.
    pthread_mutex_t lock;
    ///< Control id, this should be the same value as the card number.
    int32_t id;
};

/** @brief Struct holding all USB parameters for a given card. */
struct usb_card {
    ///< Handle used to communicate with the Sidekiq card over USB.
    struct libusb_device_handle* p_handle;
    ///< The initialization level for the card.
    skiq_xport_init_level_t init_level;
    ///< USB control interface for a given card.
    struct usb_ctrl ctrl;
    ///< USB Rx interface for a given card.
    struct usb_rx rx;
    ///< Card id mapping to a particular Sidekiq card.
    uint8_t id;
};


/***** GLOBAL FUNCTIONS  *****/

/*****************************************************************************/
/** @brief Gets the control interface for a given index. Note, if unititialized,
    this function will return an error.
  
    @param[in] index        index number of Rx interface to obtain.
    @param[out] pp_ctrl     pointer to be updated with address of ctrl interface
 
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_get_ctrl(
    uint8_t index,
    struct usb_ctrl** pp_ctrl );

/*****************************************************************************/
/** @brief Gets the receive interface for a given card. Note, if unititialized,
    this function will return an error.
  
    @param[in] index        index number of Rx interface to obtain.
    @param[out] pp_rx       pointer to be updated with address of Rx interface
 
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_get_rx(
    uint8_t index,
    struct usb_rx** pp_rx );

/*****************************************************************************/
/** @brief Initialize a given USB control interface.

    @param[in] p_ctrl       pointer to control interface to initialize
 
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_ctrl_init(
    struct usb_ctrl* p_ctrl,
    struct libusb_device_handle* p_handle,
    uint8_t id );

/*****************************************************************************/
/** @brief Free a given USB control interface.

    @param[in] p_rx         pointer to control interface to free
 
    @return Nonzero value on error, zero on success.
*/
extern void usb_ctrl_free(
    struct usb_ctrl* p_ctrl );

/*****************************************************************************/
/** @brief Initilizes a given Rx interface.
  
    @param[in] p_rx         pointer to Rx interface to initialize
    @param[in] p_handle     USB handle to associate with the interface
    @param[in] index        index to associate with the interface
 
    @return Nonzero value on error, zero if function completed successfully.
*/
extern int32_t usb_rx_init(
    struct usb_rx* p_rx,
    struct libusb_device_handle* p_handle,
    uint8_t index );

/*****************************************************************************/
/** @brief Frees a given Rx interface.
  
    @param[in] p_rx         pointer to Rx interface to free
 
    @return void
*/
extern void usb_rx_free(
    struct usb_rx* p_rx );

/*****************************************************************************/
/** @brief The callback function for a completed USB transfer. This function
    will "resubmit" itself to the attached write pipe to be obtained later
    by calling usb_poll.
  
    @param[in] p_transfer   pointer to completed libusb transfer
 
    @return void.
*/
extern void usb_callback(
    struct libusb_transfer* const p_transfer );

/*************************************************************************************************/
/** @brief Signal, using a specified write pipe, that a supplied USB transfer is complete.

    @param[in]  write_pipe     pipe to write to
    @param[in]  p_transfer     pointer of the USB transfer to be sent to the waiting thread

    @return Nonzero value on error, zero on success.
    @retval 0 Successfully submitted signal to write_pipe
    @retval -EPIPE A short write happened while signaling
    @retval -EPROTO An unspecified error (other than a short write) happened while signaling
*/
extern int32_t
usb_signal_event( write_pipe_handle_t write_pipe,
                  struct libusb_transfer* const p_transfer );

/*****************************************************************************/
/** @brief Poll a given read pipe for completed USB transfers. Note that
    transfers are written to the pipe through usb_callback.
  
    @param[in]  read_pipe       the pipe's read handle
    @param[out] pp_transfer     pointer to be updated with address of completed
                                libusb transfer
    @param[in]  timeout         maximum amount of time to poll
 
    @return int32_t
    @retval 0 Success
    @retval -EPIPE A short read happened while signaling
    @retval -EPROTO Unspecified error occurred while polling for USB event
    @retval -ETIMEDOUT A timeout occurred waiting for the USB event
*/
extern int32_t usb_poll(
    read_pipe_handle_t read_pipe,
    struct libusb_transfer** pp_transfer,
    struct timeval timeout );

/*****************************************************************************/
/** @brief Gets the index based on the Sidekiq card # provided
  
    @param[in]  card            card ID
    @param[out] p_index         pointer to where to store the USB index
 
    @return Nonzero value on error, zero on success.
*/
extern int32_t usb_get_index( uint8_t card,
                              uint8_t *p_index );
    
#endif  /* __USB_TRANSPORT_PRIVATE_H__ */
