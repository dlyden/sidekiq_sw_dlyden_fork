/**
 * @file   sidekiq_rx.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <errno.h>

#include "sidekiq_rx.h"
#include "sidekiq_xport_types.h"
#include "sidekiq_api.h"        /* for SKIQ_MAX_NUM_CARDS */
#include "sidekiq_card_mgr.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static skiq_xport_rx_functions_t rx_funcs[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {
        .rx_configure            = NULL,
        .rx_set_block_size       = NULL,
        .rx_set_buffered         = NULL,
        .rx_start_streaming      = NULL,
        .rx_stop_streaming       = NULL,
        .rx_pause_streaming      = NULL,
        .rx_resume_streaming     = NULL,
        .rx_flush                = NULL,
        .rx_set_transfer_timeout = NULL,
        .rx_receive              = NULL,
    },
};

static ARRAY_WITH_DEFAULTS(int32_t, rx_timeout_us, SKIQ_MAX_NUM_CARDS, RX_TRANSFER_NO_WAIT);

static ARRAY_WITH_DEFAULTS(skiq_xport_id_t, rx_xport_id, SKIQ_MAX_NUM_CARDS,
                           SKIQ_XPORT_ID_INITIALIZER);

/***** LOCAL FUNCTIONS *****/


/* this function is called by libtransport implementations to register rx functions for a particular card */
EPIQ_API int32_t xport_register_rx_functions( skiq_xport_id_t *p_xport_id,
					      skiq_xport_rx_functions_t *p_rx_funcs )
{
    int32_t status = -1;
    uint8_t card=0;

    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &card );
    
    /* make sure the requested card index is in range */
    if( card >= SKIQ_MAX_NUM_CARDS ||
        status != 0 )
    {
        status = -1;
        errno = ENXIO;          /* No such device or address */

        return status;
    }

    /* .rx_set_transfer_timeout in skiq_xport_rx_functions_t is required if you
     * plan to offer blocking receive support, all other functions are
     * optional. */
    if( p_rx_funcs != NULL )
    {
        // save off the UID and type at the card index 
        rx_xport_id[card].xport_uid = p_xport_id->xport_uid;
        rx_xport_id[card].type = p_xport_id->type;

        rx_funcs[card].rx_configure            = p_rx_funcs->rx_configure;
        rx_funcs[card].rx_set_block_size       = p_rx_funcs->rx_set_block_size;
        rx_funcs[card].rx_set_buffered         = p_rx_funcs->rx_set_buffered;
        rx_funcs[card].rx_start_streaming      = p_rx_funcs->rx_start_streaming;
        rx_funcs[card].rx_stop_streaming       = p_rx_funcs->rx_stop_streaming;
        rx_funcs[card].rx_pause_streaming      = p_rx_funcs->rx_pause_streaming;
        rx_funcs[card].rx_resume_streaming     = p_rx_funcs->rx_resume_streaming;
        rx_funcs[card].rx_flush                = p_rx_funcs->rx_flush;
        rx_funcs[card].rx_set_transfer_timeout = p_rx_funcs->rx_set_transfer_timeout;
        rx_funcs[card].rx_receive              = p_rx_funcs->rx_receive;
    }

    return (status);
}


EPIQ_API int32_t xport_unregister_rx_functions( skiq_xport_id_t *p_xport_id )
{
    int32_t status = 0;
    uint8_t card=0;
    
    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &card );

    /* make sure the requested card index is in range */
    if( card >= SKIQ_MAX_NUM_CARDS ||
        status != 0 )
    {
        status = -1;
        errno = ENXIO;          /* No such device or address */
    }
    else
    {
        // save off the UID and type at the card index 
        rx_xport_id[card].xport_uid = SKIQ_XPORT_UID_INVALID;
        rx_xport_id[card].type = skiq_xport_type_unknown;

        rx_funcs[card].rx_configure = NULL;
        rx_funcs[card].rx_set_block_size = NULL;
        rx_funcs[card].rx_set_buffered = NULL;
        rx_funcs[card].rx_start_streaming = NULL;
        rx_funcs[card].rx_stop_streaming = NULL;
        rx_funcs[card].rx_pause_streaming = NULL;
        rx_funcs[card].rx_resume_streaming = NULL;
        rx_funcs[card].rx_flush = NULL;
        rx_funcs[card].rx_set_transfer_timeout = NULL;
        rx_funcs[card].rx_receive = NULL;
    }
    
    return (status);
}


/* Internal functions follow.  Assume card is in range and active.  */

int32_t skiq_xport_rx_configure( uint8_t card, uint32_t aggregate_data_rate )
{
    int32_t status=0;

    if( rx_funcs[card].rx_configure != NULL )
    {
        status = rx_funcs[card].rx_configure( rx_xport_id[card].xport_uid, aggregate_data_rate );
    }

    return (status);
}

int32_t skiq_xport_rx_set_block_size( uint8_t card, uint32_t block_size )
{
    int32_t status = -ENOTSUP;

    if( rx_funcs[card].rx_set_block_size != NULL )
    {
        status = rx_funcs[card].rx_set_block_size( rx_xport_id[card].xport_uid, block_size );
    }
    /* by default, all transports support high throughput block size so if there's no
       specific handler, just consider this a success */
    else if( block_size == RX_BLOCK_SIZE_STREAM_HIGH_TPUT )
    {
        status = 0;
    }

    return (status);
}

int32_t skiq_xport_rx_set_buffered( uint8_t card, bool buffered )
{
    int32_t status = -ENOTSUP;

    if( rx_funcs[card].rx_set_buffered != NULL )
    {
        status = rx_funcs[card].rx_set_buffered( rx_xport_id[card].xport_uid, buffered );
    }
    /* by default, all transports support buffered so if there's no
       specific handler, just consider this a success */
    else if( buffered == true )
    {
        status = 0;
    }

    return (status);
}

int32_t skiq_xport_rx_start_streaming( uint8_t card, skiq_rx_hdl_t hdl )
{
    int32_t status=0;

    if( rx_funcs[card].rx_start_streaming != NULL )
    {
        status = rx_funcs[card].rx_start_streaming( rx_xport_id[card].xport_uid, hdl );
    }

    return (status);
}

int32_t skiq_xport_rx_stop_streaming( uint8_t card, skiq_rx_hdl_t hdl )
{
    int32_t status=0;

    if( rx_funcs[card].rx_stop_streaming != NULL )
    {
        status = rx_funcs[card].rx_stop_streaming( rx_xport_id[card].xport_uid, hdl );
    }

    return (status);
}

int32_t skiq_xport_rx_pause_streaming( uint8_t card )
{
    int32_t status=0;

    if( rx_funcs[card].rx_pause_streaming != NULL )
    {
        status = rx_funcs[card].rx_pause_streaming( rx_xport_id[card].xport_uid );
    }

    return (status);
}

int32_t skiq_xport_rx_resume_streaming( uint8_t card )
{
    int32_t status=0;

    if( rx_funcs[card].rx_resume_streaming != NULL )
    {
        status = rx_funcs[card].rx_resume_streaming( rx_xport_id[card].xport_uid );
    }

    return (status);
}

int32_t skiq_xport_rx_flush( uint8_t card )
{
    int32_t status=0;

    if( rx_funcs[card].rx_flush != NULL )
    {
        status = rx_funcs[card].rx_flush( rx_xport_id[card].xport_uid );
    }

    return (status);
}

int32_t skiq_xport_rx_set_transfer_timeout( const uint8_t card, const int32_t timeout_us )
{
    int32_t status = -1;

    if ( rx_funcs[card].rx_set_transfer_timeout != NULL )
    {
        status = rx_funcs[card].rx_set_transfer_timeout( rx_xport_id[card].xport_uid, timeout_us );
        if ( 0 == status )
        {
            /* success, cache the value */
            rx_timeout_us[card] = timeout_us;
        }
    }
    else
    {
        errno = ENOTSUP;
    }

    return (status);
}

int32_t skiq_xport_rx_get_transfer_timeout( const uint8_t card, int32_t *p_timeout_us )
{
    int32_t status = 0;

    if ( rx_funcs[card].rx_set_transfer_timeout == NULL )
    {
        *p_timeout_us = RX_TRANSFER_WAIT_NOT_SUPPORTED;
    }
    else
    {
        /* provide the cached value from the last rx_set_transfer_timeout()
         * call */
        *p_timeout_us = rx_timeout_us[card];
    }

    return (status);
}

int32_t skiq_xport_rx_receive( uint8_t card, uint8_t **pp_data, uint32_t *p_data_len )
{
    int32_t status = -ENOTSUP;

    if( rx_funcs[card].rx_receive != NULL )
    {
        status = rx_funcs[card].rx_receive( rx_xport_id[card].xport_uid, pp_data, p_data_len );
    }

    return (status);
}
