/**
 * @file   sidekiq_tx.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <errno.h>

#include "sidekiq_tx.h"
#include "sidekiq_xport_api.h"
#include "sidekiq_api.h"        /* for SKIQ_MAX_NUM_CARDS */
#include "sidekiq_card_mgr.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static skiq_xport_tx_functions_t tx_funcs[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {
        .tx_initialize      = NULL,
        .tx_start_streaming = NULL,
        .tx_pre_stop_streaming = NULL,
        .tx_stop_streaming  = NULL,
        .tx_transmit        = NULL,
    },
};

static ARRAY_WITH_DEFAULTS(skiq_xport_id_t, tx_xport_id, SKIQ_MAX_NUM_CARDS,
                           SKIQ_XPORT_ID_INITIALIZER);

/***** LOCAL FUNCTIONS *****/


/* this function is called by libtransport implementations to register rx functions for a particular card */
EPIQ_API int32_t xport_register_tx_functions( skiq_xport_id_t *p_xport_id,
					      skiq_xport_tx_functions_t *p_tx_funcs )
{
    int32_t status = -1;
    uint8_t card=0;

    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &card );

    /* make sure the requested card index is in range */
    if( card >= SKIQ_MAX_NUM_CARDS ||
        status != 0 )
    {
        status = -1;
        errno = ENXIO;          /* No such device or address */

        return status;
    }

    /* All function pointers in skiq_xport_tx_functions_t are optional */
    if( p_tx_funcs != NULL )
    {
        // save off the UID and type at the card index 
        tx_xport_id[card].xport_uid = p_xport_id->xport_uid;
        tx_xport_id[card].type = p_xport_id->type;

        tx_funcs[card].tx_initialize      = p_tx_funcs->tx_initialize;
        tx_funcs[card].tx_start_streaming = p_tx_funcs->tx_start_streaming;
        tx_funcs[card].tx_pre_stop_streaming = p_tx_funcs->tx_pre_stop_streaming;
        tx_funcs[card].tx_stop_streaming  = p_tx_funcs->tx_stop_streaming;
        tx_funcs[card].tx_transmit        = p_tx_funcs->tx_transmit;
    }

    return (status);
}


EPIQ_API int32_t xport_unregister_tx_functions( skiq_xport_id_t *p_xport_id )
{
    int32_t status = 0;
    uint8_t card = 0;

    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &card );

    /* make sure the requested card index is in range */
    if( card >= SKIQ_MAX_NUM_CARDS ||
        status != 0 )
    {
        status = -1;
        errno = ENXIO;          /* No such device or address */
    }
    else
    {
        // save off the UID and type at the card index 
        tx_xport_id[card].xport_uid = SKIQ_XPORT_UID_INVALID;
        tx_xport_id[card].type = skiq_xport_type_unknown;

        tx_funcs[card].tx_initialize = NULL;
        tx_funcs[card].tx_start_streaming = NULL;
        tx_funcs[card].tx_pre_stop_streaming = NULL;
        tx_funcs[card].tx_stop_streaming = NULL;
        tx_funcs[card].tx_transmit = NULL;
    }
    
    return (status);
}


int32_t skiq_xport_tx_initialize( uint8_t card,
                                  skiq_tx_transfer_mode_t tx_transfer_mode,
                                  uint32_t num_bytes_to_send,
                                  uint8_t num_send_threads,
                                  int32_t priority,
                                  skiq_tx_callback_t tx_complete_cb )
{
    int32_t status = 0;

    if( tx_funcs[card].tx_initialize != NULL )
    {
        status = tx_funcs[card].tx_initialize( tx_xport_id[card].xport_uid,
                                               tx_transfer_mode,
                                               num_bytes_to_send,
                                               num_send_threads,
                                               priority,
                                               tx_complete_cb );
    }

    return (status);
}

/* Internal functions follow.  Assume card is in range and active.  */

int32_t skiq_xport_tx_start_streaming( uint8_t card, skiq_tx_hdl_t hdl )
{
    int32_t status=0;

    if( tx_funcs[card].tx_start_streaming != NULL )
    {
        status = tx_funcs[card].tx_start_streaming( tx_xport_id[card].xport_uid, hdl );
    }

    return (status);
}

int32_t skiq_xport_tx_pre_stop_streaming( uint8_t card, skiq_tx_hdl_t hdl )
{
    int32_t status=0; // implementation of "pre stop" is optional, so just return if function not defined

    if( tx_funcs[card].tx_pre_stop_streaming != NULL )
    {
        status = tx_funcs[card].tx_pre_stop_streaming( tx_xport_id[card].xport_uid, hdl );
    }

    return (status);
}

int32_t skiq_xport_tx_stop_streaming( uint8_t card, skiq_tx_hdl_t hdl )
{
    int32_t status=0;

    if( tx_funcs[card].tx_stop_streaming != NULL )
    {
        status = tx_funcs[card].tx_stop_streaming( tx_xport_id[card].xport_uid, hdl );
    }

    return (status);
}

int32_t skiq_xport_tx_transmit( uint8_t card, skiq_tx_hdl_t hdl, int32_t *p_samples, void *p_private )
{
    int32_t status=0;

    if( tx_funcs[card].tx_transmit != NULL )
    {
        status = tx_funcs[card].tx_transmit( tx_xport_id[card].xport_uid, hdl, p_samples, p_private );
    }

    return (status);
}
