/**
 * @file   sidekiq_card.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>

#include "sidekiq_card.h"
#include "sidekiq_util.h"       /* for u8_is_not_in_list() */
#include "sidekiq_xport_types.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_card_mgr_private.h"
#include "sidekiq_hal.h"
#include "card_services.h"

/* card_funcs for various existing transports */
#include "pcie_transport.h"
#include "usb_transport.h"

#if (defined USE_AXI_XPORT)
#include "axi_transport.h"
#endif

#if (defined USE_NET_XPORT)
#include "net_transport.h"
#endif

/***** DEFINES *****/

#define CARD_FUNCS_INITIALIZER                    \
    {                                             \
        .card_probe   = NULL,                     \
        .card_hotplug = NULL,                     \
        .card_init    = NULL,                     \
        .card_exit    = NULL,                     \
        .card_read_priv_data = NULL,            \
        .card_write_priv_data = NULL,           \
    }

/***** MACROS *****/

#if (defined DEBUG_TRANSPORT)
#include <ctype.h>
#include <inttypes.h>
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/* resolves character argument to itself if it is printable or '.' if not */
#define PRINTABLE(_c)                   ((isprint(_c) != 0) ? (_c) : '.')

/***** TYPEDEFS *****/

typedef struct
{
    skiq_xport_init_level_t level;
    skiq_xport_card_functions_t card_funcs;

} card_xport_t;

/* structure to hold the transport type, subclass designation, and transport card functions */
typedef struct
{
    skiq_xport_type_t type;
    uint8_t subclass;
    skiq_xport_card_functions_t *card_funcs;

} transport_t;


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static card_xport_t cards[SKIQ_MAX_NUM_CARDS][skiq_xport_type_max] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {
        [ 0 ... (skiq_xport_type_max-1) ] = {
            .level      = skiq_xport_init_level_unknown,
            .card_funcs = CARD_FUNCS_INITIALIZER,
        },
    },
};

/* create a mutex to use while registering or unregistering custom xport card functions */
static pthread_mutex_t custom_card_func_lock = PTHREAD_MUTEX_INITIALIZER;

static skiq_xport_card_functions_t external_custom_xport_card_funcs = CARD_FUNCS_INITIALIZER;

static transport_t custom_transport =
{
    /* external custom transport */
    /* has a .subclass of SUBCLASS_ANY to be a catch-all, this means no
        * other custom transport (internal or external) can have that subclass,
        * the code in this module depends on this entry's pairing of
        * (skiq_xport_type_custom, SUBCLASS_ANY) to be unique */
    .type = skiq_xport_type_custom,
    .subclass = SUBCLASS_ANY,
    .card_funcs = NULL,
};

static transport_t transports[] =
{
    /* PCIe transport */
    {
        .type = skiq_xport_type_pcie,
        .subclass = SUBCLASS_ANY,
        .card_funcs = &pcie_xport_card_funcs,
    },

    /* USB transport */
    {
        .type = skiq_xport_type_usb,
        .subclass = SUBCLASS_ANY,
        .card_funcs = &usb_xport_card_funcs,
    },

    /****************************************************/
    /* ATTENTION: new transports are added here */
    /****************************************************/

#if (defined USE_AXI_XPORT)
    /* AXI transport (custom subclass) */
    {
        .type = skiq_xport_type_custom,
        .subclass = SUBCLASS_AXI,
        .card_funcs = &axi_xport_card_funcs,
    },
#endif

#if (defined USE_NET_XPORT)
    /* network transport (custom subclass) */
    {
        .type = skiq_xport_type_net,
        .subclass = SUBCLASS_ANY,
        .card_funcs = &net_xport_card_funcs,
    },
#endif

    /* list terminator */
    {
        .type = skiq_xport_type_unknown,
        .card_funcs = NULL,
    },
};

/***** LOCAL FUNCTIONS *****/
static void
store_transport_info( uint8_t card,
                      skiq_xport_type_t type,
                      skiq_xport_init_level_t level,
                      skiq_xport_card_functions_t *functions )
{
    if ( ( type != skiq_xport_type_unknown ) && ( level != skiq_xport_init_level_unknown ) )
    {
        debug_print("Storing transport info for card %u: %s %s [%p]\n", card, xport_type_cstr(type),
                    xport_level_cstr(level), functions);
    }

    cards[card][type].level = level;
    cards[card][type].card_funcs = *functions;

}


#ifndef __MINGW32__
static void
clear_transport_info( uint8_t card,
                      skiq_xport_type_t type )
{
    skiq_xport_card_functions_t empty = CARD_FUNCS_INITIALIZER;

    debug_print("Clearing transport info for card %u\n", card);

    store_transport_info( card, type, skiq_xport_init_level_unknown, &empty );
}
#else
static void
clear_transport_info( uint8_t card,
                      skiq_xport_type_t type )
{
    debug_print("Clearing transport info not supported\n");
}
#endif


/**************************************************************************************************/
/**
   For a specified card index and transport type, fill in the provided references to serial number,
   hardware version, and part information.
 */
static int32_t
gather_card_info( uint8_t card,
                  skiq_xport_type_t type,
                  uint32_t *p_serial_num,
                  uint16_t *p_hardware_vers,
                  skiq_part_info_t *p_part_info )
{
    int32_t status = 0;
    uint32_t serial_num;
    uint16_t hardware_vers;

    CHECK_NULL_PARAM(p_serial_num);
    CHECK_NULL_PARAM(p_hardware_vers);
    CHECK_NULL_PARAM(p_part_info);

    /* select HAL functions for the card and type to enable reading card information */
    hal_select_functions_for_card( card, type );

    status = card_read_serial_num( card, &serial_num );
    if ( status != 0 )
    {
        skiq_warning("Failed to read serial number (status = %" PRIi32 ") for card %" PRIu8
                     " while probing\n", status, card);
    }

    if ( status == 0 )
    {
        status = card_read_hw_version( card, &hardware_vers );
        if ( status != 0 )
        {
            skiq_warning("Failed to read hardware version (status = %" PRIi32 ") for card %"
                         PRIu8 " while probing\n", status, card);
        }
    }

    if ( status == 0 )
    {
        status = card_read_part_info( card,
                                      hardware_vers,
                                      p_part_info->number_string,
                                      p_part_info->revision_string,
                                      p_part_info->variant_string );
        if ( status != 0 )
        {
            skiq_warning( "Failed to read part info (status = %" PRIi32 ") for card %" PRIu8
                          " while probing\n", status, card);
        }
    }

    if ( status == 0 )
    {
        /* copy results only if all calls were successful.  The last call (card_read_part_info) uses
         * the p_part_info directly, so no need to copy it here. */
        *p_serial_num = serial_num;
        *p_hardware_vers = hardware_vers;
    }

    /* unconditionally reset the HAL functions */
    hal_reset_functions( card );

    return status;
}


/**************************************************************************************************/
/**
   For each transport UID in the @p uids list, first gather card information using the specified
   transport @p type, then register the transport with card manager.
 */
static int32_t
_xport_register_cards( skiq_xport_type_t type,
                       uint64_t uids[],
                       uint8_t nr_uids,
                       skiq_xport_card_functions_t *functions )
{
    int32_t status = 0;
    skiq_xport_id_t xport_id = SKIQ_XPORT_ID_INITIALIZER;
    uint8_t card = 0;
    uint8_t i = 0;
    uint8_t priv_num_bytes = 0;
    uint8_t private_data[CARD_MGR_XPORT_PRIVATE_SIZE];

    xport_id.type = type;

    /* for each UID detected, get card info and register xport with card mgr */
    for ( i = 0; ( i < nr_uids ) && ( status == 0 ); i++ )
    {
        uint32_t serial_num=0;
        uint16_t hardware_vers=0;
        skiq_part_info_t part_info;

        debug_print("--> registering UID %016" PRIx64 " with %s transport\n", uids[i],
                    xport_type_cstr( type ));

        xport_id.xport_uid = uids[i];
        card_mgr_xport_probe_card( &xport_id ); /* This is a trick used during probe to redirect
                                                 * xport UIDs to a card index for use in functions
                                                 * that expect a card index.  This way the
                                                 * underlying card manager structures are
                                                 * untouched. */
        card_mgr_get_card( xport_id.xport_uid, xport_id.type, &card );
        /* get the private data */
        if( functions->card_read_priv_data != NULL )
        {
            functions->card_read_priv_data( uids[i],
                                            CARD_MGR_XPORT_PRIVATE_SIZE,
                                            &priv_num_bytes,
                                            &(private_data[0]) );
        }

        /* perform basic init so we can get some info about the card */
        status = skiq_xport_card_init( xport_id.type,
                                       skiq_xport_init_level_basic,
                                       card );
        if ( status != 0 )
        {
            skiq_warning("Failed to initialize transport (status = %" PRIi32 ") for card %"
                         PRIu8 " while probing\n", status, card);
        }

        if ( status == 0 )
        {
            status = gather_card_info( card, xport_id.type, &serial_num, &hardware_vers,
                                       &part_info );
        }

        if ( status == 0 )
        {
            debug_print("\nCard %" PRIu8 " on %s transport\n", card,
                        xport_type_cstr( xport_id.type ));
            debug_print_plain("\tSerial:     0x%08X (%u)\n", serial_num, serial_num);
            debug_print_plain("\tHW version: 0x%04X\n", hardware_vers);
            debug_print_plain("\tPart:       ES%s-%s-%s\n", part_info.number_string,
                              part_info.revision_string, part_info.variant_string);
        }
        else
        {
            /* We don't want to consider an invalid a product an error in the case that this is a fresh
               board where we need to actually allow an invalid product to still have a transport registered
               so that we can actually program the product info */
            skiq_warning("Card part information is not valid, unexpected behavior may occur if continued");
        }

        /* now register the XPORT based on the xport UID and serial num */
        status = card_mgr_register_xport( uids[i],
                                          xport_id.type,
                                          serial_num,
                                          hardware_vers,
                                          &part_info,
                                          priv_num_bytes,
                                          &(private_data[0]));
        if ( status == -ENOSPC )
        {
            /* maybe we could implement a LRU eviction policy? */
            skiq_warning("Failed to register transport, no space available in card manager "
                         "cache.  There is a limit of %" PRIu8 " entries in the cache and "
                         "entries are not reused.\n", SKIQ_MAX_NUM_CARDS);
            
            /* set the status here to 0 since other UIDs may find an existing home in the
             * cache */
            status = 0;
        }
        else if (0 != status)
        {
            skiq_warning("Failed to register transport with card manager (status = %"
                         PRIi32 ") for card %" PRIu8 "\n", status, card);
        }

        /* unconditionally exit the card's transport */
        skiq_xport_card_exit( card );
    }

    return status;
}

/* call the card_probe function and identify enough information using a
 * skiq_xport_init_level_basic init level to register the card with Card
 * Manager.
 *
 * NOTE: The assumption is that a given physical Sidekiq card can have at most
 * ONE custom transport.  It may not have a custom transport (if it's using PCIe
 * or USB).  For example, a Sidekiq card is assumed to have a ZAP transport, but
 * not also an AXI transport.  However, this does not prevent different cards
 * from having different custom transports, just one card from having multiple
 * custom transports.
 */
static int32_t
_xport_card_probe( skiq_xport_type_t type,
                   skiq_xport_card_functions_t *functions )
{
    int32_t status = 0;
    uint64_t uids[SKIQ_MAX_NUM_CARDS];
    uint8_t nr_uids = 0;

    if ( NULL == functions->card_probe )
    {
        status = -ENOTSUP;
    }

    if ( status == 0 )
    {
        status = functions->card_probe( uids, &nr_uids );
    }

    if ( status == 0 )
    {
        debug_print("probe of %s transport [%p] found %u cards\n", xport_type_cstr(type),
                    functions, nr_uids);
        status = _xport_register_cards( type, uids, nr_uids, functions );
    }

    return status;
}


/**************************************************************************************************/
/**
   call the card_hotplug function and identify enough information using a
   skiq_xport_init_level_basic init level to register the card with Card Manager.

   If a transport defines a hotplug function and the hotplug scan was successful, tell card manager
   to clear out all instances of the specified transport across all locked cards.

   If a transport does not define a hotplug function or the hotplug scan was not successful, do not
   clear out instances of the specified transport.
 */
static int32_t
_xport_card_hotplug( skiq_xport_type_t type,
                     skiq_xport_card_functions_t *functions,
                     uint8_t no_probe_cards[],
                     uint8_t nr_no_probe_cards )
{
    int32_t status = 0;
    uint64_t uids[SKIQ_MAX_NUM_CARDS], no_probe_uids[SKIQ_MAX_NUM_CARDS];
    uint8_t nr_uids = 0, nr_no_probe_uids = 0;

    if ( NULL == functions->card_hotplug )
    {
        status = -ENOTSUP;
    }

    if ( status == 0 )
    {
        uint8_t i;

        /* convert 'no probe' card indices to UIDs, not every card necessarily has the transport
         * type, so the resulting no_probe_uids[] array may be smaller than the no_probe_cards[]
         * array */
        for ( i = 0; ( i < nr_no_probe_cards ) && ( status == 0 ); i++ )
        {
            uint64_t uid;

            status = card_mgr_get_xport_id( no_probe_cards[i], type, &uid );
            if ( status == 0 )
            {
                no_probe_uids[nr_no_probe_uids++] = uid;
                debug_print("[#%" PRIu8 "] (status=%" PRIi32 ") UID is 0x%016" PRIx64 "\n",
                            no_probe_cards[i], status, uid);
            }
            else if ( status == -ENXIO )
            {
                /* a card not having the specified transport is okay */
                status = 0;
                debug_print("[#%" PRIu8 "] No %s transport present for card\n", no_probe_cards[i],
                            xport_type_cstr(type));
            }
        }
    }

    if ( status == 0 )
    {
        status = functions->card_hotplug( uids, &nr_uids,
                                          no_probe_uids, nr_no_probe_uids );
    }

    /* Now that hotplug was successful, deregister the transport across locked cards (e.g. cards not
     * in the no_probe_cards array) */
    if ( status == 0 )
    {
        uint8_t i;

        for ( i = 0; ( i < SKIQ_MAX_NUM_CARDS ) && ( status == 0 ); i++ )
        {
            if ( u8_is_not_in_list( i, no_probe_cards, nr_no_probe_cards ) )
            {
                debug_print("Deregistering %s transport on card %" PRIu8 "\n",
                            xport_type_cstr(type), i);
                status = card_mgr_deregister_xport( i, type );
                if ( status != 0 )
                {
                    debug_print("Deregistration failed on card %" PRIu8 " with status %" PRIi32
                                "\n", i, status);
                    /* Squash ENODEV error from deregistration.  This error may occur in the 
                       case when the transport type is in the extended range and an entry 
                       for the extended range doesn't exist, which is ok.  It just means that
                       the a card for the transport has never previously been registered */
                    if( status == -ENODEV )
                    {
                        status = 0;
                    }
                }
            }
        }
    }

    /* finally, register all discovered (new or existing) cards that have the specified transport
     * with card manager */
    if ( status == 0 )
    {
        debug_print("Hotplug scan of %s transport [%p] found %u cards\n", xport_type_cstr(type),
                    functions, nr_uids);
        status = _xport_register_cards( type, uids, nr_uids, functions );
    }

    return status;
}


/* call the card_init function and store the transport info locally for the given card index */
static int32_t
_xport_card_init( uint8_t card,
                  skiq_xport_type_t type,
                  skiq_xport_init_level_t level,
                  uint64_t uid,
                  skiq_xport_card_functions_t *functions )
{
    int32_t status = 0;
    uint8_t priv_num_bytes = 0;
    uint8_t private_data[CARD_MGR_XPORT_PRIVATE_SIZE];

    if ( NULL != functions->card_init )
    {
        debug_print("Calling card_init for card %u (UID 0x%016" PRIx64 ") for %s transport [%p] at "
                    "level %s\n", card, uid, xport_type_cstr(type), functions,
                    xport_level_cstr(level));

        /* make private data is written before initializing */
        if( functions->card_write_priv_data != NULL )
        {
            /* get the private data from the card_mgr and write it to the xport */
            if( card_mgr_read_xport_private( uid,
                                             type,
                                             &priv_num_bytes,
                                             &(private_data[0]) ) == 0 )
            {
                status = functions->card_write_priv_data( uid,
                                                          priv_num_bytes,
                                                          &(private_data[0]) );
            }
        }

        if( status == 0 )
        {
            status = functions->card_init( level, uid );
            if ( status == 0 )
            {
                store_transport_info( card, type, level, functions );
            }
        }
    }
    else
    {
        /* having no card_init registered is not supported */
        status = -ENOTSUP;
    }

    return status;
}


/* call the card_exit function and clear out the local transport info for the card index and type */
static int32_t
_xport_card_exit( uint8_t card,
                  skiq_xport_type_t type,
                  skiq_xport_init_level_t level,
                  uint64_t uid,
                  skiq_xport_card_functions_t *functions )
{
    int32_t status = -1;

    if ( NULL != functions->card_exit )
    {
        debug_print("Calling card_exit for card %u (UID 0x%016" PRIx64 ") for %s transport [%p] at "
                    "level %s\n", card, uid, xport_type_cstr(type), functions,
                    xport_level_cstr(level));

        status = functions->card_exit( level, uid );
        if ( 0 == status )
        {
            clear_transport_info( card, type );
        }
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
skiq_xport_init_level_t skiq_xport_card_get_level( uint8_t card )
{
    if ( card < SKIQ_MAX_NUM_CARDS )
    {
        skiq_xport_type_t type;

        /* to maintain backward compatible with sidekiq_core, return the first
         * "well known" level from the 'cards' array */
        for ( type = skiq_xport_type_pcie; type < skiq_xport_type_max; type++ )
        {
            if ( cards[card][type].level != skiq_xport_init_level_unknown )
            {
                return cards[card][type].level;
            }
        }
    }

    return skiq_xport_init_level_unknown;
}


/**************************************************************************************************/
skiq_xport_type_t skiq_xport_card_get_type( uint8_t card )
{
    if ( card < SKIQ_MAX_NUM_CARDS )
    {
        skiq_xport_type_t type;

        /* to maintain backward compatible with sidekiq_core, return the first
         * 'type' with a well defined level from the 'cards' array */
        for ( type = skiq_xport_type_pcie; type < skiq_xport_type_max; type++ )
        {
            if ( cards[card][type].level != skiq_xport_init_level_unknown )
            {
                return type;
            }
        }
    }

    return skiq_xport_type_unknown;
}


/******************************************************************************/
/** skiq_xport_card_probe() calls all card_probe functions in the 'transports'
 * array that match the provided skiq_xport_type_t.  Typically, in the cases of
 * PCIe and USB transports, there will only be one card_probe available.  In the
 * case of a custom transport, more than one card_probe function may be
 * available (multiple internal and at most one external).
 *
 * At the conclusion of this function, Card Manager will know all available
 * cards for a given skiq_xport_type_t.
 *
 * NOTE: transports are specified in the 'transports' array at the top of this
 * source file.
 *
 * @param type desired transport type to be probed
 *
 * @return int32_t - 0 if successful in all probe calls (even if nothing is
 * discovered), non-zero otherwise
 */
int32_t skiq_xport_card_probe( skiq_xport_type_t type )
{
    int32_t status = 0;

    debug_print("Probing for cards with %s transport\n", xport_type_cstr(type));
    /* if the xport type is custom and there are custom xport card function, no need to
       iterate over the other xport types */
    pthread_mutex_lock(&custom_card_func_lock);
    if ( ( type == skiq_xport_type_custom ) && ( custom_transport.card_funcs != NULL ) )
    {
        status = _xport_card_probe( type, custom_transport.card_funcs );

        pthread_mutex_unlock(&custom_card_func_lock);
    }
    else
    {
        pthread_mutex_unlock(&custom_card_func_lock);
        transport_t *p_transport = transports;

        /* iterate over available internal transports */
        while ( ( status == 0 ) && ( p_transport->type != skiq_xport_type_unknown ) )
        {
            if ( ( p_transport->type == type ) && ( p_transport->card_funcs != NULL ) )
            {
                status = _xport_card_probe( type, p_transport->card_funcs );
            }
            p_transport++;
        }
    }

    return (status);
}


/**************************************************************************************************/
/**
   skiq_xport_card_hotplug() calls all card_hotplug functions in the 'transports' array that match
   the provided skiq_xport_type_t.  In the cases of PCIe and USB transports, there is only one
   card_hotplug available.  In the case of a custom transport, more than one card_hotplug function
   may be available (multiple internal and at most one external).

   At the conclusion of this function, Card Manager will know all available cards for a given
   skiq_xport_type_t.

   @note Transports are specified in the 'transports' array at the top of this source file.

   @param[in] type                 desired transport type to perform a hotplug scan
   @param[in] no_probe_cards       array of card indices not to be scanned (i.e. owned by another process)
   @param[in] nr_no_probe_cards    number of valid entries in the @p no_probe_cards

   @return int32_t - 0 if successful in all probe calls (even if nothing is
   discovered), non-zero otherwise
 */
int32_t skiq_xport_card_hotplug( skiq_xport_type_t type,
                                 uint8_t no_probe_cards[],
                                 uint8_t nr_no_probe_cards )
{
    int32_t status = 0;

    debug_print("Hotplugging cards with %s transport\n", xport_type_cstr(type));
    /* if the xport type is custom and there are custom xport card function, no need to
       iterate over the other xport types */
    pthread_mutex_lock(&custom_card_func_lock);
    if ( ( type == skiq_xport_type_custom ) && ( custom_transport.card_funcs != NULL ) )
    {
        status = _xport_card_hotplug( type, custom_transport.card_funcs, no_probe_cards,
                                        nr_no_probe_cards );

        if ( status == -ENOTSUP )
        {
            /* it's okay for a transport not to support hotplug */
            debug_print("Hotplugging not supported with %s transport\n", xport_type_cstr(type));
            status = 0;
        }

        pthread_mutex_unlock(&custom_card_func_lock);
    }
    else
    {
        pthread_mutex_unlock(&custom_card_func_lock);
        transport_t *p_transport = transports;

        /* iterate over available internal transports */
        while ( ( status == 0 ) && ( p_transport->type != skiq_xport_type_unknown ) )
        {
            if ( ( p_transport->type == type ) && ( p_transport->card_funcs != NULL ) )
            {
                status = _xport_card_hotplug( type, p_transport->card_funcs, no_probe_cards,
                                                nr_no_probe_cards );

                if ( status == -ENOTSUP )
                {
                    /* it's okay for a transport not to support hotplug */
                    debug_print("Hotplugging not supported with %s transport\n", xport_type_cstr(type));
                    status = 0;
                }
            }
            p_transport++;
        }
    }

    return (status);
}


/******************************************************************************/
/** skiq_xport_card_init() initializes a card for a given skiq_xport_type_t and
 * skiq_xport_init_level_t.  The referenced card must have been previously
 * discovered through skiq_xport_card_probe (even if by another process) and is
 * presently registered with Card Manager.  The transport type and transport
 * subclass are used to match which transport implementation is used.
 *
 * NOTE: transports are specified in the 'transports' array at the top of this
 * source file.
 *
 * @param type desired transport type to use
 * @param level desired transport initialization level to employ
 * @param card card index of the desired Sidekiq
 *
 * @return int32_t - 0 if successful initializing, negative errno otherwise
 */
int32_t skiq_xport_card_init( skiq_xport_type_t type,
                              skiq_xport_init_level_t level,
                              uint8_t card )
{
    int32_t status = 0;
    uint64_t uid = 0;

    debug_print("Initializing card %u with %s transport at level %s\n", card, xport_type_cstr(type),
                xport_level_cstr(level));

    /* look up uid by (card,type) */
    status = card_mgr_get_xport_id( card, type, &uid );
    if ( status != 0 )
    {
        status = -EDOM;
        return status;
    }

    /* set status to represent unmatched transport type or unmatched subclass before looking for a match */
    status = -EINVAL;

    pthread_mutex_lock(&custom_card_func_lock);
    if ( ( type == skiq_xport_type_custom ) && ( custom_transport.card_funcs != NULL ) )
    {
        debug_print("Considering %s transport with subclass 0x%02X for card %u initialization\n",
                    xport_type_cstr( custom_transport.type ), custom_transport.subclass, card);

        /* if the subclass matches, go ahead and call _xport_card_init */
        if ( ( SUBCLASS_ANY == custom_transport.subclass ) || ( SUBCLASS(uid) == custom_transport.subclass ) )
        {
            debug_print("--> Calling %s transport with subclass 0x%02X for card %" PRIu8
                        " initialization\n", xport_type_cstr( custom_transport.type ),
                        custom_transport.subclass, card);
        
            status = _xport_card_init( card, type, level, uid, custom_transport.card_funcs );
        }

        pthread_mutex_unlock(&custom_card_func_lock);
    }
    else
    {
        pthread_mutex_unlock(&custom_card_func_lock);
        transport_t *p_transport = transports;

        /* iterate over available transports */
        while ( p_transport->type != skiq_xport_type_unknown )
        {
            debug_print("Considering %s transport with subclass 0x%02X for card %u initialization\n",
                        xport_type_cstr( p_transport->type ), p_transport->subclass, card);

            /* if transport type matches */
            if ( ( p_transport->type == type ) && ( p_transport->card_funcs != NULL ) )
            {
                /* if the subclass matches, go ahead and call _xport_card_init */
                if ( ( SUBCLASS_ANY == p_transport->subclass ) || ( SUBCLASS(uid) == p_transport->subclass ) )
                {
                    debug_print("--> Calling %s transport with subclass 0x%02X for card %" PRIu8
                                " initialization\n", xport_type_cstr( p_transport->type ),
                                p_transport->subclass, card);

                    status = _xport_card_init( card, type, level, uid, p_transport->card_funcs );
                    return (status);
                }
            }

            p_transport++;
        }
    }

    return (status);
}


/******************************************************************************/
/** skiq_xport_card_exit() closes down access to the card specified by the
 * passed index argument.  The referenced card must have been previously discovered
 * through skiq_xport_card_probe (even if by another process) and is presently
 * registered with Card Manager.    The transport type and transport
 * subclass are used to match which transport implementation is used.
 *
 * NOTE: It is expected that only one card_exit per card index is called since
 * the cards array knows the type / level of the card at the index.
 *
 * NOTE: internal custom transports are specified in the
 * internal_custom_transports array at the top of this source file.
 *
 * @param card card index of the desired Sidekiq
 *
 * @return int32_t - 0 if successful closing down access, negative errno otherwise
 */
int32_t skiq_xport_card_exit( uint8_t card )
{
    transport_t *p_transport;
    skiq_xport_type_t type;
    skiq_xport_init_level_t level = skiq_xport_card_get_level( card );
    uint32_t exit_count = 0;
    int32_t status = 0;
    uint64_t uid = 0;

    debug_print("Closing card %u\n", card);

    pthread_mutex_lock(&custom_card_func_lock);
    if ( ( custom_transport.card_funcs != NULL ) &&
         ( 0 == card_mgr_get_xport_id( card, skiq_xport_type_custom, &uid ) ) )
    {
        /* if the subclass matches, go ahead and call _xport_card_exit */
        if ( ( SUBCLASS_ANY == custom_transport.subclass ) || ( SUBCLASS(uid) == custom_transport.subclass ) )
        {
            debug_print("--> Calling %s transport with subclass 0x%02X for card %"
                        PRIu8 " exit\n", xport_type_cstr( custom_transport.type ),
                        custom_transport.subclass, card);

            status = _xport_card_exit( card, custom_transport.type, level, uid, custom_transport.card_funcs );
            if ( status == 0 )
            {
                exit_count++;
            }
        }

        pthread_mutex_unlock(&custom_card_func_lock);
    }
    else
    {
        pthread_mutex_unlock(&custom_card_func_lock);
        /* to maintain backward compatibility with sidekiq_core, iterate over all
         * transport types and call exit from each one. */
        for ( type = skiq_xport_type_pcie; ( status == 0 ) && ( type < skiq_xport_type_max ); type++ )
        {
            /* look up uid by (card,type) */
            if ( 0 == card_mgr_get_xport_id( card, type, &uid ) )
            {
                /* iterate over available transports */
                p_transport = transports;
                while ( ( status == 0 ) && ( p_transport->type != skiq_xport_type_unknown ) )
                {
                    debug_print("Considering %s transport with subclass 0x%02X for card %u exit\n",
                                xport_type_cstr( p_transport->type ), p_transport->subclass, card);

                    /* if transport type matches */
                    if ( ( p_transport->type == type ) && ( p_transport->card_funcs != NULL ) )
                    {
                        /* if the subclass matches, go ahead and call _xport_card_exit */
                        if ( ( SUBCLASS_ANY == p_transport->subclass ) || ( SUBCLASS(uid) == p_transport->subclass ) )
                        {
                            debug_print("--> Calling %s transport with subclass 0x%02X for card %"
                                        PRIu8 " exit\n", xport_type_cstr( p_transport->type ),
                                        p_transport->subclass, card);

                            status = _xport_card_exit( card, type, level, uid, p_transport->card_funcs );
                            if ( status == 0 )
                            {
                                exit_count++;
                            }
                        }
                    }

                    p_transport++;
                }
            }
        }
    }

    if ( exit_count == 0 )
    {
        /* unmatched transport type or unmatched subclass */
        status = -EDOM;
    }

    return (status);
}


EPIQ_API int32_t skiq_register_custom_transport( skiq_xport_card_functions_t *functions )
{
    int32_t status = 0;

    debug_print("Custom transport registered [%p]\n", functions);

    /* *_probe, *_init, and *_exit card functions must be non-NULL */
    if ( ( NULL == functions ) ||
         ( NULL == functions->card_probe ) ||
         ( NULL == functions->card_init ) ||
         ( NULL == functions->card_exit ) )
    {
        status = -EINVAL;
    }
    else
    {
        external_custom_xport_card_funcs.card_probe = functions->card_probe;
        external_custom_xport_card_funcs.card_hotplug = functions->card_hotplug;
        external_custom_xport_card_funcs.card_init  = functions->card_init;
        external_custom_xport_card_funcs.card_exit  = functions->card_exit;
        external_custom_xport_card_funcs.card_read_priv_data = functions->card_read_priv_data;
        external_custom_xport_card_funcs.card_write_priv_data = functions->card_write_priv_data;

        pthread_mutex_lock(&custom_card_func_lock);

        /* set card_funcs of custom_transport to external_custom_xport_card_funcs to indicate registered */
        custom_transport.card_funcs = &external_custom_xport_card_funcs;

        pthread_mutex_unlock(&custom_card_func_lock);
    }

    return status;
}


EPIQ_API int32_t skiq_unregister_custom_transport( void )
{
    int32_t status = 0;

    debug_print("Custom transport unregistered\n");

    /* set the card functions back to default */
    external_custom_xport_card_funcs = (skiq_xport_card_functions_t) CARD_FUNCS_INITIALIZER;

    pthread_mutex_lock(&custom_card_func_lock);

    /* set the 'external custom transport' card_funcs to NULL to indicate unregistered */
    custom_transport.card_funcs = NULL;

    pthread_mutex_unlock(&custom_card_func_lock);

    return status;
}
