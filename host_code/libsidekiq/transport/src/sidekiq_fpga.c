/**
 * @file   sidekiq_fpga.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/ 

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <inttypes.h>

#include "sidekiq_private.h"

#include "sidekiq_fpga.h"
#include "sidekiq_xport_types.h"
#include "sidekiq_api.h"        /* for SKIQ_MAX_NUM_CARDS */
#include "sidekiq_hal.h"
#include "sidekiq_card_mgr.h"

/* enable debug_print and debug_print_plain when DEBUG_SIDEKIQ_FPGA_TRACE is defined */
#if (defined DEBUG_SIDEKIQ_FPGA_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

/* enable debug_print and debug_print_plain when DEBUG_SIDEKIQ_FPGA is defined */
#if (defined DEBUG_SIDEKIQ_FPGA)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/***** DEFINES *****/

/* worse case sample rate = 233kHz, which means a register transaction could take
   up to 4.3 uS = 4300 ns (our current sleep interval), so bump this up to 
   5000.  Note that this change shouldn't impact performance of those transactions
   that would have completed successfully under the previous limit of 5.  However,
   it should hopefully cover us in the case of low sample rates, at the cost of
   having a very high poll rate.
 */
#define MAX_NUM_VERIFY_ATTEMPTS (5000)


/* Some platforms like the NVIDIA Jetson TX2 take more time (~150mS) for the DMAD procfs entry to
 * appear so more time is needed between retries.  The overall time used to be 100mS total, now the
 * overall timeout is 500mS. */
#define MAX_NUM_FPGA_UP_RETRIES         (10)
#define FPGA_UP_RETRY_DELAY_MS          (50) /* 50 milliseconds */

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static skiq_xport_fpga_functions_t fpga_funcs[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {
        .fpga_reg_read = NULL,
        .fpga_reg_write = NULL,
        .fpga_down = NULL,
        .fpga_down_reload = NULL,
        .fpga_up = NULL,
        .fpga_reg_verify = NULL,
        .fpga_reg_write_and_verify = NULL,
        .fpga_reg_read_64 = NULL,
        .fpga_reg_write_64 = NULL,
    },
};

static ARRAY_WITH_DEFAULTS(skiq_xport_id_t, fpga_xport_id, SKIQ_MAX_NUM_CARDS,
                           SKIQ_XPORT_ID_INITIALIZER);

/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/

/* this function is called by libtransport implementations to register FPGA functions for a particular card */
EPIQ_API int32_t xport_register_fpga_functions( skiq_xport_id_t *p_xport_id,
						skiq_xport_fpga_functions_t *p_fpga_reg_funcs )
{
    int32_t status=0;
    uint8_t card=0;

    TRACE_ENTER;

    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &card );

    /* make sure the requested card index is in range */
    if( card >= SKIQ_MAX_NUM_CARDS ||
        status != 0 )
    {
        status = -1;
        errno = ENXIO;          /* No such device or address */

        goto trace_exit;
    }

    if ( ( NULL == p_fpga_reg_funcs ) ||
         ( NULL == p_fpga_reg_funcs->fpga_reg_read ) ||
         ( NULL == p_fpga_reg_funcs->fpga_reg_write ) )
    {
        status = -1;
        errno = EINVAL;
    }
    else
    {
        debug_print("Registering FPGA functions at %p for UID 0x%016" PRIx64 " of type %s for "
		    "card %" PRIu8 "\n", p_fpga_reg_funcs, p_xport_id->xport_uid,
                    xport_type_cstr(p_xport_id->type), card);

        // save off the UID and type at the card index 
        fpga_xport_id[card].xport_uid = p_xport_id->xport_uid;
        fpga_xport_id[card].type = p_xport_id->type;
        
        fpga_funcs[card].fpga_reg_read = p_fpga_reg_funcs->fpga_reg_read;
        fpga_funcs[card].fpga_reg_write = p_fpga_reg_funcs->fpga_reg_write;
        fpga_funcs[card].fpga_down = p_fpga_reg_funcs->fpga_down;
        fpga_funcs[card].fpga_down_reload = p_fpga_reg_funcs->fpga_down_reload;
        fpga_funcs[card].fpga_up = p_fpga_reg_funcs->fpga_up;
        fpga_funcs[card].fpga_reg_verify = p_fpga_reg_funcs->fpga_reg_verify;
        fpga_funcs[card].fpga_reg_write_and_verify = p_fpga_reg_funcs->fpga_reg_write_and_verify;
        fpga_funcs[card].fpga_reg_read_64 = p_fpga_reg_funcs->fpga_reg_read_64;
        fpga_funcs[card].fpga_reg_write_64 = p_fpga_reg_funcs->fpga_reg_write_64;
    }

trace_exit:
    TRACE_EXIT_STATUS(status);

    return (status);
}


EPIQ_API int32_t xport_unregister_fpga_functions( skiq_xport_id_t *p_xport_id )
{
    int32_t status=0;
    uint8_t card=0;

    TRACE_ENTER;

    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &card );

    /* make sure the requested card index is in range */
    if( card >= SKIQ_MAX_NUM_CARDS ||
        status != 0 )
    {
        status = -1;
        errno = ENXIO;          /* No such device or address */
    }
    else
    {
        debug_print("Deregistering FPGA functions for UID 0x%016" PRIx64 " of type %s for card %"
		    PRIu8 "\n", p_xport_id->xport_uid, xport_type_cstr(p_xport_id->type), card);

        // save off the UID and type at the card index 
        fpga_xport_id[card].xport_uid = SKIQ_XPORT_UID_INVALID;
        fpga_xport_id[card].type = skiq_xport_type_unknown;

        fpga_funcs[card].fpga_reg_read = NULL;
        fpga_funcs[card].fpga_reg_write = NULL;
        fpga_funcs[card].fpga_down = NULL;
        fpga_funcs[card].fpga_down_reload = NULL;
        fpga_funcs[card].fpga_up = NULL;
        fpga_funcs[card].fpga_reg_verify = NULL;
        fpga_funcs[card].fpga_reg_write_and_verify = NULL;
        fpga_funcs[card].fpga_reg_read_64 = NULL;
        fpga_funcs[card].fpga_reg_write_64 = NULL;
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/* Internal functions follow.  Assume card is in range and active.  */

int32_t sidekiq_xport_fpga_down( uint8_t card )
{
    int32_t status=0;

    TRACE_ENTER;

    if( fpga_funcs[card].fpga_down != NULL )
    {
        status = fpga_funcs[card].fpga_down( fpga_xport_id[card].xport_uid );
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}

int32_t sidekiq_xport_fpga_down_reload( uint8_t card, uint32_t addr )
{
    int32_t status=0;

    TRACE_ENTER;

    if( fpga_funcs[card].fpga_down_reload != NULL )
    {
        status = fpga_funcs[card].fpga_down_reload( fpga_xport_id[card].xport_uid, addr );
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}

int32_t sidekiq_xport_fpga_up( uint8_t card )
{
    int32_t status=0;

    TRACE_ENTER;

    if( fpga_funcs[card].fpga_up != NULL )
    {
        status = fpga_funcs[card].fpga_up( fpga_xport_id[card].xport_uid );
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}

int32_t sidekiq_fpga_reg_read(uint8_t card, uint32_t addr, uint32_t* p_data)
{
    int32_t status = -ENOSYS;

    TRACE_ENTER;
    debug_print("Register address: 0x%" PRIx32 "\n", addr);


    if ( (addr % 4) != 0)
    {
        _skiq_log(SKIQ_LOG_ERROR,"Invalid FPGA register address 0x%08x. FPGA register reads must occur on 32-bit boundaries\n",addr);
        status = -EINVAL;
        goto trace_exit;
    }

    if( fpga_funcs[card].fpga_reg_read != NULL )
    {
        status = fpga_funcs[card].fpga_reg_read(fpga_xport_id[card].xport_uid,addr,p_data);
    }

trace_exit:
    TRACE_EXIT_STATUS(status);

    return (status);
}

int32_t sidekiq_fpga_reg_write(uint8_t card, uint32_t addr, uint32_t data)
{
    int32_t status = -ENOSYS;

    TRACE_ENTER;
    debug_print("Register address: 0x%" PRIx32 "\n", addr);

    if ( (addr % 4) != 0)
    {
        _skiq_log(SKIQ_LOG_ERROR,"Invalid FPGA register address 0x%08x. FPGA register writes must occur on 32-bit boundaries\n", addr);
        status = -EINVAL;
        goto trace_exit;
    }

    if( fpga_funcs[card].fpga_reg_write != NULL )
    {
        status = fpga_funcs[card].fpga_reg_write(fpga_xport_id[card].xport_uid,addr,data);
    }

trace_exit:
    TRACE_EXIT_STATUS(status);

    return (status);
}

int32_t sidekiq_fpga_reg_verify(uint8_t card, uint32_t addr, uint32_t data)
{
    int32_t status = 0;
    uint32_t verify = 0;
    uint32_t num_attempts = 0;

    TRACE_ENTER;

    if( fpga_funcs[card].fpga_reg_verify != NULL )
    {
        status = fpga_funcs[card].fpga_reg_verify( fpga_xport_id[card].xport_uid,
                                                   addr, data );
    }
    else
    {
        status = sidekiq_fpga_reg_read(card, addr, &verify);
        while ( ( 0 == status ) && ( verify != data ) && ( num_attempts < MAX_NUM_VERIFY_ATTEMPTS ) )
        {
            hal_nanosleep(1);
            num_attempts++;
            status = sidekiq_fpga_reg_read(card, addr, &verify);
        }
        
        if ( ( 0 == status ) && ( verify != data ) )
        {
            status = -EIO;
            _skiq_log(SKIQ_LOG_WARNING,"max num read attempts reached for verify, reg 0x%08x, expected 0x%08x, got 0x%08x\n",
                      addr, data, verify);
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

int32_t sidekiq_fpga_reg_write_and_verify(uint8_t card, uint32_t addr, uint32_t data)
{
    int32_t status = 0;

    TRACE_ENTER;

    if( fpga_funcs[card].fpga_reg_write_and_verify != NULL )
    {
        status = fpga_funcs[card].fpga_reg_write_and_verify( fpga_xport_id[card].xport_uid,
                                                             addr, data );
    }
    else
    {
        status = sidekiq_fpga_reg_write(card, addr, data);
        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_verify(card, addr, data);
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

int32_t sidekiq_fpga_reg_read_64(uint8_t card,
                                 uint32_t addr_high, uint32_t addr_low,
                                 uint64_t* p_data)
{
    int32_t status = 0;

    TRACE_ENTER;

    /* we only want to call the 64-bit register access if aligned on 64-bit
     boundary and addr_low is exactly 4 off from addr high */
    if( (fpga_funcs[card].fpga_reg_read_64 != NULL) &&
        (addr_low+sizeof(uint32_t) == addr_high) &&
        (addr_low % sizeof(uint64_t) == 0) )
    {
        status = fpga_funcs[card].fpga_reg_read_64( fpga_xport_id[card].xport_uid,
                                                    addr_low, p_data );
    }
    else
    {
        uint32_t data_high1 = 0, data_high2 = 0, data_low1 = 0, data_low2 = 0;

        /* This code handles three different cases:

           The HIx mean that HI1 and HI2 are equal, so it doesn't matter which one is chosen.  The
           same applies for LOW1 and LOW2 in the following description.

           - Read LOW1, Read HI1, Read LOW2, LOWs agree -> return (HI1, LOWx)
           - Read LOW1, Read HI1, Read LOW2, LOWs disagree, read HI2, HIs agree -> return (HIx, LOW2)
           - Read LOW1, Read HI1, Read LOW2, LOWs disagree, read HI2, HIs disagree, read LOW2 -> return (HI2, LOW2)
         */


        status = sidekiq_fpga_reg_read(card, addr_low, &data_low1);
        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_read(card, addr_high, &data_high1);
        }

        if ( status == 0 )
        {
            /* now here is the tricky part: we re-read the low 32-bits again
               to see if they've changed...this is because there are several
               values (timestamps and the such) that are infrequently updated,
               but could have changed between read of the low word and the
               high word.  If, on the second read of the low word, a different
               value is found, then we need to re-read the high word, which
               couldn't have changed again, since all of these 64-bit regs
               change, at most, once per second (ok, this isn't entirely true:
               the actual timestamp reg changes constantly, so for these
               cases, we'll simply do some unnecessary extra reads */
            status = sidekiq_fpga_reg_read(card, addr_low, &data_low2);
        }

        if ( ( status == 0 ) && ( data_low2 != data_low1 ) )
        {
            /* re-read the high word in case it has too updated */
            status = sidekiq_fpga_reg_read(card, addr_high, &data_high2);

            /* if the upper changed, we should re-read the lower again */
            if ( ( status == 0 ) && ( data_high1 != data_high2 ) )
            {
                status = sidekiq_fpga_reg_read(card, addr_low, &data_low2);
            }

            if ( status == 0 )
            {
                data_low1 = data_low2;
                data_high1 = data_high2;
            }
        }

        if ( status == 0 )
        {
            *p_data = (uint64_t)((uint64_t)data_high1<<32) | (uint64_t)(data_low1);
        }
        else
        {
            /* squash messages from sidekiq_fpga_reg_read() calls into -EBADMSG ( */
            status = -EBADMSG;
        }
    }

    TRACE_EXIT_STATUS(status);

    return(status);
}

int32_t sidekiq_fpga_reg_write_64(uint8_t card,
                                  uint32_t addr_high, uint32_t addr_low,
                                  uint64_t data)
{
    int32_t status=0;
    uint32_t data_high, data_low;

    TRACE_ENTER;

    /* we only want to call the 64-bit register access if aligned on 64-bit
       boundary and addr_low is exactly 4 off from addr high */
    if( (fpga_funcs[card].fpga_reg_write_64 != NULL) &&
        (addr_low+sizeof(uint32_t) == addr_high) &&
        (addr_low % sizeof(uint64_t) == 0) )
    {
        status = fpga_funcs[card].fpga_reg_write_64( fpga_xport_id[card].xport_uid,
                                                     addr_low, data );
    }
    else
    {
        data_high = (uint32_t)(data>>32);
        data_low = (uint32_t)(data & 0x00000000FFFFFFFFllu);
        if ((status=sidekiq_fpga_reg_write(card,addr_high,0xFFFFFFFF)) < 0)
        {
            status = -1;
            goto trace_exit;
        }
        
        if ((status=sidekiq_fpga_reg_write(card,addr_low,data_low)) < 0)
        {
            status = -2;
            goto trace_exit;	
        }

        if ((status=sidekiq_fpga_reg_write(card,addr_high,data_high)) < 0)
        {
            status = -3;
            goto trace_exit;
        }
    }

trace_exit:
    TRACE_EXIT_STATUS(status);

    return(status);
}

/** 
 * sidekiq_fpga_bring_up calls sidekiq_xport_fpga_up periodically (sleeping
 * FPGA_UP_RETRY_DELAY_MS in between attempts) for at most
 * MAX_NUM_FPGA_UP_RETRIES before giving up.
 * 
 * @param[in] card 
 * 
 * @return int32_t - returns 0 if transport is now up, non-zero if transport
 * failed to become available after MAX_NUM_FPGA_UP_RETRIES retries.
 */
int32_t sidekiq_fpga_bring_up( uint8_t card )
{
    uint8_t fpga_up_retries = MAX_NUM_FPGA_UP_RETRIES;
    int32_t status;

    TRACE_ENTER;

    /* try up to MAX_NUM_FPGA_UP_RETRIES to bring the transport back up */
    do
    {
        hal_nanosleep(FPGA_UP_RETRY_DELAY_MS * MILLISEC);
        status = sidekiq_xport_fpga_up( card );
        fpga_up_retries--;

    } while ( ( status != 0 ) && ( fpga_up_retries > 0 ) );

    if ( status != 0 )
    {
        skiq_warning("Transport for card %" PRIu8 " is not available after %d tries\n", card,
                     MAX_NUM_FPGA_UP_RETRIES);
    }
    else if ( fpga_up_retries < MAX_NUM_FPGA_UP_RETRIES - 1 )
    {
        _skiq_log(SKIQ_LOG_DEBUG, "Transport for card %u took %d tries to become available\n", card, MAX_NUM_FPGA_UP_RETRIES - fpga_up_retries);
    }

    TRACE_EXIT_STATUS(status);

    return status;

} 


