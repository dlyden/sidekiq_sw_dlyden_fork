/**
 * @file   iio_rx_thread.c
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>

#include "iio_rx_thread.h"
#include "axi_transport_private.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"

#include "glib.h"

/* enable debug_print and debug_print_plain when DEBUG_IIO_RX_THREAD is defined */
#if (defined DEBUG_IIO_RX_THREAD)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/
/** 
 Nearly all Sidekiq products should have a "epiq_adc_core".  However, for Z2p,
 we could not have anything indicating "Epiq" or "Sidekiq" in the base image.
 As a result, we used the "pcg" name instead of the "epiq" name to ID the radio.
**/
static const char *_EPIQ_RADIO_RX_STREAM_DEV = "epiq_adc_core";
static const char *_PCG_RADIO_RX_STREAM_DEV = "pcg_adc_core";
static const char *_RADIO_RX_STREAM_I = "voltage0";
static const char *_RADIO_RX_STREAM_Q = "voltage1";

#define NUM_PACKETS (160)
#define PACKET_SIZE_IN_WORDS (1024)
#define PACKET_SIZE_IN_BYTES (PACKET_SIZE_IN_WORDS*sizeof(uint32_t))

/* maximum amount of time to wait for a 'write' packet to become available in the p_write_queue
 * GAsyncQueue */
#define WRITE_PKT_AVAIL_TIMEOUT_US      100000

/* poll timeout to balance between waiting for a 'write' packet to become available and to check if
 * the `discard` state should be entered */
#define WRITE_PKT_AVAIL_POLL_US         1000


/* This value is used in iio_rx_pause() as the maximum time to wait for process_iio_rx() to return
 * from iio_buffer_refill() and detect a change in _b_discard[index] to enter the discard state.
 *
 * At a minimum sample rate of 233kSps and a block size of 16 x 1018 samples, the maximum
 * inter-block time is roughly 70 milliseconds, so 100 ms gives the thread enough margin to fill the
 * buffer on its own.  In cases where the FPGA is not providing samples, but is providing "bursty"
 * information (like in Skylight or 802.11), this timeout is used to "unstick" the thread from
 * blocking on iio_buffer_refill() without forcing the FPGA to dummy fill blocks.
 */
#define IIO_REFILL_TIMEOUT_MS           100

/** It seems as though sometimes it takes a bit of time before we're able to create the IIO buffer
    even when everything else looks good.  This mainly seems to occur after reprogramming, when the 
    buffer needs to be re-created / registered with the kernel.  During development, the max amount
    of time this has ever taken is 80ms, and the minimum is 0-10ms, so we'll poll every 10ms, a 
    maximum of 20 times (for a total of 200 ms) */
#define MAX_IIO_BUF_CREATE_RETRY       (20)
#define IIO_CREATE_WAIT_TIME_MS        (10)

/** @brief  The number of times to attempt to wait for the RX thread to shut down */
#define NUM_THREAD_SHUTDOWN_RETRIES (128)
/**
    @brief  The amount of time (in microseconds) to wait between checks to see if the RX
            thread actually shut down
*/
#define TIME_BETWEEN_THREAD_SHUTDOWN_RETRIES_US (200)

//#define DEBUG_CHECK_META (1)

/** Macros to operate on a pthread mutex (_flush_ts_mutex) */
#define LOCK_FLUSH_TS(_index)           LOCK(_flush_state[_index].lock)
#define UNLOCK_FLUSH_TS(_index)         UNLOCK(_flush_state[_index].lock)

/** Macros to operate on a pthread mutex (_buf_mutex) */
#define LOCK_BUFFER(_index)             LOCK(_buf_mutex[_index])
#define UNLOCK_BUFFER(_index)           UNLOCK(_buf_mutex[_index])

/** Macros to operate on a pthread mutex (_read_pkt_mutex) */
#define LOCK_READ_PKT(_index)           LOCK(_read_pkt_mutex[_index])
#define UNLOCK_READ_PKT(_index)         UNLOCK(_read_pkt_mutex[_index])

#define INIT_ARRAY(size, value)         { [0 ... ((size)-1)] = value }


/***** TYPEDEFS *****/

#ifdef USE_OCM
#define NUM_KERNEL_BUFS (4)
#else
#define NUM_KERNEL_BUFS (128) // arbitrarily chosen...default is 4, which is not enough
#endif

#ifdef USE_ISA_ARMV8
#define MEMCPY(DST, SRC, LEN, INDEX)  memcpy256(DST, SRC, LEN, INDEX)
#else
#define MEMCPY(DST, SRC, LEN, INDEX)  memcpy(DST, SRC, LEN)
#endif

/***** STRUCTS *****/

/***** LOCAL VARIABLES *****/
// we have X packets that we return on a receive call for every IIO buffer refill request
uint32_t _num_read_packets_per_write[AXI_MAX_INDEX] =
    { [0 ... (AXI_MAX_INDEX-1)] = AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED };
// this is the number of packets contained in a single IIO buffer refill request
uint32_t _axi_num_packets_in_iio_buffer[AXI_MAX_INDEX] =
    { [0 ... (AXI_MAX_INDEX-1)] = AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED };

// IIO context
struct iio_context *_p_context[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = NULL };

// IIO buffer that is filled in the background
static struct iio_buffer *_p_buffer[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = NULL };
// number of samples (including the metadata header) contained in each IIO buffer 
static uint32_t _num_samples_per_buf[AXI_MAX_INDEX] =
    { [0 ... (AXI_MAX_INDEX-1)] = 1024*AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED };

// temporary holder of data returned from the IIO buffer refill request.  Align the buffer at 16
// bytes to meet the memcpy256() requirements on platforms that use it
static uint32_t _tmp_packet[AXI_MAX_INDEX][PACKET_SIZE_IN_WORDS*NUM_PACKETS] __attribute__ ((aligned (16)));

// tracks the packets we've pulled from the read queue
static uint32_t _num_read_pkts[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = 0 };
// points to the very first read packet (when _num_read_pkts=0)...this is the pointer
// that is placed back on the write queue when we've read enough times
static uint32_t *_p_pkt_head[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = NULL };

static ARRAY_WITH_DEFAULTS(int32_t, rx_timeout, AXI_MAX_INDEX, RX_TRANSFER_NO_WAIT);

// background thread that keeps the IIO buffer full
static pthread_t _rx_thread[AXI_MAX_INDEX];
// flag indicating if background thread should be running
static bool _b_running[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = false };
// flag indicating if the background thread is still running
static bool _b_is_running[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = false };
// flag indicating if we should be discarding samples (like when we're in a flushing state4)
static ARRAY_WITH_DEFAULTS(bool, _b_discard, AXI_MAX_INDEX, true);
// memcpy256 out of alignment warning
static ARRAY_WITH_DEFAULTS(bool, _memcpy256_unaligned, AXI_MAX_INDEX, false);

/*
  Variables to keep track of the flushing state by index and receive handle

  .lock is used to protect access to .is_flushing[] and .flush_timestamp[] after starting the
        process_iio_rx() thread

  .is_flushing[hdl] tracks whether or not to drop a receive block based on its timestamp when
                    compared to .flush_timestamp[hdl]

  .flush_timestamp[hdl] keeps track of the "current time" RF timestamp.  Any receive block with a
                        timestamp less than this variable should be thrown away, but only if
                        .is_flushing[hdl] is true.  This is used to remove stale data from the IIO
                        buffer as well as the async queues.
 */
struct flush_state
{
    pthread_mutex_t lock;

    bool is_flushing[skiq_rx_hdl_end];
    uint64_t flush_timestamp[skiq_rx_hdl_end];
};
#define FLUSH_STATE_INITIALIZER                                 \
    {                                                           \
        .lock = PTHREAD_MUTEX_INITIALIZER,                      \
        .is_flushing = INIT_ARRAY(skiq_rx_hdl_end, false),      \
        .flush_timestamp = INIT_ARRAY(skiq_rx_hdl_end, 0),      \
    }

static ARRAY_WITH_DEFAULTS( struct flush_state, _flush_state, AXI_MAX_INDEX,
                            FLUSH_STATE_INITIALIZER );

// mutex array (indexed by AXI index) for IIO buffer
static MUTEX_ARRAY(_buf_mutex, AXI_MAX_INDEX);

// mutex array (indexed by AXI index) for anything associated with the read packet count or head
static MUTEX_ARRAY(_read_pkt_mutex, AXI_MAX_INDEX);

// mutex and condition variable that indicates if we should actively be accumulating samples
static MUTEX_ARRAY(_streaming_mutex, AXI_MAX_INDEX);
static COND_ARRAY(_streaming_cond, AXI_MAX_INDEX);

// mutex and condition variable to indicate if the IIO buffer is allocated and ready to be filled
static MUTEX_ARRAY(_buf_ready_mutex, AXI_MAX_INDEX);
static COND_ARRAY(_buf_ready_cond, AXI_MAX_INDEX);

/*
  Variables to keep track of the process_iio_rx thread READY state by index.  The thread is READY
  (.is_ready is true) if its IIO buffer has been created successfully and can handle stream
  requests.  If the thread is NOT READY, the caller must block until it is READY.

  .lock is used to protect access to .cond and .is_ready

  .cond is a pthread_cond_t used for waiting and signalling between the primary thread and the
        process_iio_rx thread

  .is_ready keeps track of the ready state of the process_iio_rx thread.
 */
struct thread_ready_state
{
    pthread_mutex_t lock;
    pthread_cond_t cond;

    bool is_ready;
};
#define THREAD_READY_STATE_INITIALIZER                          \
    {                                                           \
        .lock = PTHREAD_MUTEX_INITIALIZER,                      \
        .cond = PTHREAD_COND_INITIALIZER,                       \
        .is_ready = false,                                      \
    }

static ARRAY_WITH_DEFAULTS( struct thread_ready_state, _thread_ready_state, AXI_MAX_INDEX,
                            THREAD_READY_STATE_INITIALIZER );

/* Protected by @a _streaming_mutex, this count represents the number of streaming handles */
static ARRAY_WITH_DEFAULTS(uint8_t, _streaming_count, AXI_MAX_INDEX, 0);


struct hdl_pause_state
{
    /**
       This boolean array keeps track of the pause/resume state for EACH of the handles.  This is
       used to determine which _flush_state entries to populate when a flush is requested
    */
    bool is_hdl_paused[skiq_rx_hdl_end];
};
#define HDL_PAUSE_STATE_INITIALIZER                                 \
    {                                                               \
        .is_hdl_paused = INIT_ARRAY( skiq_rx_hdl_end, false ),      \
    }

/* Protected by @a _streaming_mutex, this represents the pause/resume state of a card's handles */
static ARRAY_WITH_DEFAULTS( struct hdl_pause_state, _hdl_pause_state, AXI_MAX_INDEX,
                            HDL_PAUSE_STATE_INITIALIZER );

/* Wrapper function to look-up if a (index,hdl) handle is marked as paused or not */
static inline
bool is_hdl_paused( uint8_t index,
                    skiq_rx_hdl_t hdl )
{
    return _hdl_pause_state[index].is_hdl_paused[hdl];
}

/* Wrapper function to mark a (index,hdl) handle as paused */
static inline
void mark_hdl_as_paused( uint8_t index,
                         skiq_rx_hdl_t hdl )
{
    _hdl_pause_state[index].is_hdl_paused[hdl] = true;
}

/* Wrapper function to mark a (index,hdl) handle as "resumed" */
static inline
void mark_hdl_as_resumed( uint8_t index,
                          skiq_rx_hdl_t hdl )
{
    _hdl_pause_state[index].is_hdl_paused[hdl] = false;
}

// queue that we write sample data from the IIO buffer into
GAsyncQueue *p_write_queue[AXI_MAX_INDEX] = { [0 ... (AXI_MAX_INDEX-1)] = NULL };

// queue that we pull packets that contain our receive data
GAsyncQueue *p_read_queue[AXI_MAX_INDEX] = { [0 ... (AXI_MAX_INDEX-1)] = NULL };

static void _check_read_pkts_and_push( uint8_t index );

// Copy 256bits (32Bytes) at a time to burst from RAM
// For the copy to work len must be 32 byte aligned and dst must be 16 byte aligned$
// For maximum OCM read efficiency src must be 32 byte aligned$
// https://confluence.epiq.rocks/display/~egregori/NEON+memcpy256+Testing
// https://confluence.epiq.rocks/display/~egregori/Stand-Alone+MEMCPY+and+NEON+Performance+Test
static inline void 
memcpy256( void *dst, const void *src, size_t len, uint8_t index )
{
    if( ((len % 32) != 0) || (((uintptr_t)src % 32) != 0) || (((uintptr_t)dst % 16) !=0) )
    {
        // Just use normal memcpy for out of alignment requests
        memcpy(dst, src, len);
        _memcpy256_unaligned[index] = true;
    }
    else
    {
        asm volatile("1: ldp q0,q1,[%1],32; sub %2,%2,32; stp q0,q1,[%0],32; cbnz %2,1b"
            : "+r" (dst), "+r" (src), "+r" (len) :: "q0", "q1", "memory");
    }
}

static int32_t
cancel_destroy_init_iio_buffer( uint8_t index )
{
    int32_t status = 0;

    if( _p_buffer[index] != NULL )
    {
        iio_buffer_cancel( _p_buffer[index] );
    }

    // note that the buffer could have gotten freed / set to NULL before we got the mutex, so
    // make sure we check for NULL before destroying
    LOCK_BUFFER(index);
    if( _p_buffer[index] != NULL )
    {
        iio_buffer_destroy( _p_buffer[index] );
        _p_buffer[index] = NULL;
        status = init_iio_buffer(index);
        UNLOCK_BUFFER(index);

        if ( status != 0 )
        {
            skiq_error("Unable to create IIO sample buffer for index %u\n", index);
            // this is a critical error...we can't receive if we can't create the buffer
            hal_critical_exit(status);
        }
    }
    else
    {
        UNLOCK_BUFFER( index );
    }

    // signal that the buffer is now ready in case anything was waiting on it
    pthread_mutex_lock( &_buf_ready_mutex[index] );
    pthread_cond_signal( &_buf_ready_cond[index] );
    pthread_mutex_unlock( &_buf_ready_mutex[index] );

    return status;
}


int32_t init_iio_buffer( uint8_t index )
{
    int32_t status=0;
    struct iio_device *p_rxstream;  /// radio RX stream device
    struct iio_channel *p_rx_i_chan; /// RX I channel
    struct iio_channel *p_rx_q_chan; /// RX Q channel
    uint32_t i=0;
    uint32_t *p_data;
    uint32_t *p_pkt;
    const char *p_radio_dev = _EPIQ_RADIO_RX_STREAM_DEV;
    uint32_t buf_create_retry=0;
    bool buf_created = false;
    
    if( _skiq_get_part(index) == skiq_z2p )
    {
        p_radio_dev = _PCG_RADIO_RX_STREAM_DEV;
    }

    // clear out all the write packets until empty
    do
    {
        p_data = (uint32_t*)(g_async_queue_try_pop(p_write_queue[index]));
    }
    while( p_data != NULL );

    // clear out all of the read packets until it's empty
    do
    {
        p_data = (uint32_t*)(g_async_queue_try_pop(p_read_queue[index]));
    }
    while( p_data != NULL );

    LOCK_READ_PKT(index);
    {
        _num_read_pkts[index] = 0; // reset the read pkts
        _p_pkt_head[index] = NULL;
    }
    UNLOCK_READ_PKT(index);

    if( _p_context[index] == NULL )
    {
        if( (_p_context[index] = iio_create_default_context()) == NULL )
        {
            skiq_error("Unable to create IIO context");
            status = -ENODEV;
            return (status);
        }
        if( iio_context_set_timeout( _p_context[index], 0 ) != 0 )
        {
            status = -EINVAL;
            return (status);
        }
    }
    
    if( (p_rxstream =
         iio_context_find_device( _p_context[index],
                                  p_radio_dev )) != NULL )
    {
        if( (p_rx_i_chan = iio_device_find_channel( p_rxstream,
                                                    _RADIO_RX_STREAM_I,
                                                    false /* not output */)) == NULL ||
            (p_rx_q_chan = iio_device_find_channel( p_rxstream,
                                                    _RADIO_RX_STREAM_Q,
                                                    false /* not output */ )) == NULL )
        {
            skiq_error("Unable to find I/Q channels");
            status = -ENOENT;
        }
        else
        {
            if( iio_device_set_kernel_buffers_count( p_rxstream, NUM_KERNEL_BUFS ) != 0 )
            {
                skiq_error("Configuring the buffer size unavailable");
                status = -ENODEV;
            }

            if( (iio_channel_is_scan_element(p_rx_i_chan) == true) &&
                (iio_channel_is_scan_element(p_rx_q_chan) == true) )
            {
                // ad9361-iiostream.c disables the chans after destroying buf
                iio_channel_disable(p_rx_i_chan);
                iio_channel_disable(p_rx_q_chan);
                iio_channel_enable(p_rx_i_chan);
                iio_channel_enable(p_rx_q_chan);

                do
                {
                    errno = 0;
                    if( (_p_buffer[index] = iio_device_create_buffer( p_rxstream,
                                                                      _num_samples_per_buf[index],
                                                                      false )) != NULL )
                    {
                        status=iio_buffer_set_blocking_mode( _p_buffer[index], true );
                        if( status != 0 )
                        {
                            skiq_error("Unable to configure blocking mode with status %d", status);
                            iio_buffer_destroy( _p_buffer[index] );
                            _p_buffer[index] = NULL;
                        }
                    }
                    else
                    {
                        // sometimes the buffer is not immediately there and we need to retry
                        // For Z3u, it may take some additional time to update the IIO buffer permissions
                        // access from when the buffer is created, so allow for a retry
                        if( (errno == ENOENT) || (errno == EACCES) )
                        {
                            buf_create_retry++;
                            hal_millisleep(IIO_CREATE_WAIT_TIME_MS);
                            status = -ENOENT;
                        }
                        else
                        {
                            skiq_error("Unable to create receive buffer: errno %d\n", errno);
                            status = -ENOMEM;
                        }
                    }
                } while( (buf_created == false) && (buf_create_retry < MAX_IIO_BUF_CREATE_RETRY) && (status==-ENOENT) );
            }
            else
            {
                skiq_error("I/Q channels are not configured properly");
                status = -ENOENT;
            }
        }
    }
    else
    {
        skiq_error("Unable to find RX stream device");
        status = -ENODEV;
    }

    if( status == 0 )
    {
        p_pkt = &(_tmp_packet[index][0]);
        // loop through the all the packets and push the pointers on to the write queue
        for( i=0; i<NUM_PACKETS; i+=_num_read_packets_per_write[index] )
        {
            g_async_queue_push( p_write_queue[index], (gpointer)(p_pkt) );
            p_pkt += (_num_read_packets_per_write[index]*PACKET_SIZE_IN_WORDS);
        }
    }

    return (status);    
}


int32_t init_iio_packet_buffered( uint8_t index, bool buffered )
{
    int32_t status=0;

    if( buffered == true )
    {
        _num_read_packets_per_write[index] = AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED;
        _axi_num_packets_in_iio_buffer[index] = AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED;
    }
    else
    {
        _num_read_packets_per_write[index] = AXI_NUM_PACKETS_IN_IIO_BUFFER_UNBUFFERED;
        _axi_num_packets_in_iio_buffer[index] = AXI_NUM_PACKETS_IN_IIO_BUFFER_UNBUFFERED;
    }
    _num_samples_per_buf[index] = 1024*_axi_num_packets_in_iio_buffer[index];

    status = cancel_destroy_init_iio_buffer( index );

    return (status);
}   


int32_t iio_rx_set_transfer_timeout( uint8_t index,
                                     const int32_t timeout_us )
{
    debug_print("Setting receive transfer timeout from %d to %d\n", rx_timeout[index], timeout_us);
    rx_timeout[index] = timeout_us;

    return 0;
}


/***** LOCAL FUNCTIONS *****/

// initializes our background thread
int32_t init_iio_rx_thread( uint8_t index )
{
    int32_t status=0;

    p_write_queue[index] = g_async_queue_new();
    p_read_queue[index] = g_async_queue_new();

    _b_running[index] = true;
    _b_discard[index] = true;

    LOCK(_streaming_mutex[index]);
    {
        skiq_rx_hdl_t hdl;

        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            /* Track each handle's initial state as PAUSED and not flushing */
            _flush_state[index].is_flushing[hdl] = false;
            _flush_state[index].flush_timestamp[hdl] = 0;
            mark_hdl_as_paused(index, hdl);
        }
        _streaming_count[index] = 0;
    }
    UNLOCK(_streaming_mutex[index]);

    _thread_ready_state[index].is_ready = false;
    _memcpy256_unaligned[index] = false;

    status = pthread_create( &(_rx_thread[index]), NULL, process_iio_rx,
                             (void *) (intptr_t) index );

    /* wait here on a condition variable to indicate that the IIO RX thread is ready */
    if ( status == 0 )
    {
        LOCK(_thread_ready_state[index].lock);
        while ( !_thread_ready_state[index].is_ready )
        {
            pthread_cond_wait( &(_thread_ready_state[index].cond), &(_thread_ready_state[index].lock) );
        }
        UNLOCK(_thread_ready_state[index].lock);
    }

    return (status);
}


void _check_read_pkts_and_push( uint8_t index )
{
    LOCK_READ_PKT(index);
    if( _num_read_pkts[index] >= _num_read_packets_per_write[index] )
    {
        // add the pointer back to the write queue
        g_async_queue_push( p_write_queue[index], (gpointer)(_p_pkt_head[index]) );
        
        _num_read_pkts[index] = 0;
        _p_pkt_head[index] = NULL;
    }
    UNLOCK_READ_PKT(index);
}

#if (defined DEBUG_CHECK_META)
void debug_check_meta( uint8_t index,
                       uint8_t i,
                       skiq_rx_block_t *p_rx_block )
{
    uint32_t ts_offset = SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS-SKIQ_RX_HEADER_SIZE_IN_WORDS;
    skiq_rx_hdl_t curr_hdl;
    static uint64_t curr_ts=0;
    static uint64_t next_ts=0;

    curr_ts = p_rx_block->rf_timestamp;
    if( (curr_ts != next_ts) )
    {
        uint64_t flush_ts = 0;

        LOCK_FLUSH_TS(index);
        curr_hdl = p_rx_block->hdl;
        flush_ts = _flush_state[index].flush_timestamp[curr_hdl];

        // only report as an error if we shouldn't drop this packet
        if ( next_ts >= flush_ts )
        {
            UNLOCK_FLUSH_TS(index);

            skiq_error("TIMESTAMP GAP at packet %u....expected 0x%016" PRIx64 " but got 0x%016"
                       PRIx64 "\n", i, next_ts, curr_ts);
            hal_critical_exit(-1);
        }
        else
        {
            UNLOCK_FLUSH_TS(index);
        }

        if( curr_ts < next_ts )
        {
            skiq_debug("BACKWARDS TIMESTAMP discard=%s curr=0x%016" PRIx64 "flush=0x%016" PRIx64
                       "\n", bool_cstr(_b_discard[index]), curr_ts, flush_ts);
            hal_critical_exit(-1);
        }
    }
    next_ts = curr_ts + ts_offset;
}
#else
#define debug_check_meta(...)           do { } while (0)
#endif

// grab some data from the read queue
int32_t iio_rx_thread_get_data( uint8_t index, uint8_t **pp_data, uint32_t *p_data_len )
{
    int32_t status=skiq_rx_status_no_data;
    uint32_t *p_data = NULL;
    skiq_rx_block_t *p_rx_block;
    skiq_rx_hdl_t hdl;
    int32_t timeout;

    _check_read_pkts_and_push(index);

    if( _b_running[index] == false )
    {
        status = skiq_rx_status_no_data;
        return (status);
    }

    timeout = rx_timeout[index];
    if ( timeout == RX_TRANSFER_NO_WAIT )
    {
        p_data = (uint32_t*)(g_async_queue_try_pop(p_read_queue[index]));
    }
    else if ( timeout == RX_TRANSFER_WAIT_FOREVER )
    {
        p_data = (uint32_t*)(g_async_queue_pop(p_read_queue[index]));
    }
    else if ( timeout > 0 )
    {
        /* @todo Fix this so that blocking receive is handled properly while in a flushing state

           Details captured in LIB-289: AXI/IIO Transport: blocking receive and flush
           conflicting behavior
        */
        p_data = (uint32_t*)(g_async_queue_timeout_pop(p_read_queue[index], timeout));
    }

    if ( p_data != NULL )
    {
        status=0;
        *p_data_len = PACKET_SIZE_IN_BYTES;
        *pp_data = (uint8_t*)(p_data);

        // we need to make sure the timestamp of our packet is past the flush ts
        p_rx_block = (skiq_rx_block_t*)(p_data);
        hdl = p_rx_block->hdl;

        LOCK_FLUSH_TS(index);
        if ( (( _flush_state[index].is_flushing[hdl] ) &&
              ( p_rx_block->rf_timestamp < _flush_state[index].flush_timestamp[hdl] )) ||
             (is_hdl_paused(index, hdl)) )
        {
            debug_print("Flushing block with timestamp %" PRIu64 "\n",
                        p_rx_block->rf_timestamp);
            *pp_data = NULL;
            *p_data_len = 0;
            status = skiq_rx_status_no_data;
        }
        else
        {
            /* The block's timestamp is greater than the one in
             * _flush_state[index].flush_timestamp[hdl], that must mean the stream is back to
             * "current time", so mark _flush_state[index].is_flushing[hdl] to false so it can
             * be ignored in the future */
            if ( _flush_state[index].is_flushing[hdl] )
            {
                debug_print("Disabling _flush_state[%u].is_flushing[%s] since block with "
                            "timestamp %" PRIu64 " arrived\n", index, rx_hdl_cstr( hdl ),
                            p_rx_block->rf_timestamp);
                _flush_state[index].is_flushing[hdl] = false;
            }

            status = skiq_rx_status_success;
        }
        UNLOCK_FLUSH_TS(index);

        if ( status == skiq_rx_status_success )
        {
            debug_check_meta(index, 0, p_rx_block);
        }

        // note: we still need to do our normal packet count maintenance even if we're saying we
        // don't have any data to ensure the queues stay synced up
        LOCK_READ_PKT(index);
        {
            if( _num_read_pkts[index] == 0 )
            {
                _p_pkt_head[index] = (uint32_t*)(p_data);
            }
            _num_read_pkts[index]++;
        }
        UNLOCK_READ_PKT(index);
    }

    return (status);
}


// background thread to read the IIO buffer and fill up the read queue
void* process_iio_rx( void *data )
{
    int32_t status=0;
    int32_t num_bytes = 0;
    uint32_t *p_tmp_buf;
    uint32_t *p_tmp_pkt;
    uint32_t i=0;
    uint8_t index = (uint8_t) (intptr_t) data;
    char threadname[100];

    debug_print("Starting skiq_rx_%u thread\n", index);

    // signal that the thread has started
    _b_is_running[index] = true;

    snprintf( threadname, 100, "skiq_rx_%u", index );
    pthread_setname_np( _rx_thread[index], threadname);
    pthread_getname_np( _rx_thread[index], threadname, 100 );

    struct sched_param sched;
    sched.sched_priority = sched_get_priority_min(SCHED_RR);

    // set the scheduling to SCHED_RR...this ensures that this thread will
    // be run until it is blocked as long as a higher priority thread doesn't
    // need to run
    if( sched_setscheduler(0, SCHED_RR, &sched) != 0 )
    {
        skiq_error("unable to set realtime priority for receive thread, falling back to other");
    }
    
    // increase the ref count for our receive thread
    g_async_queue_ref( p_write_queue[index] );
    g_async_queue_ref( p_read_queue[index] );

    // upon creation of the thread, we need to initialize the IIO buffer to receive samples
    LOCK_BUFFER(index);
    if( _p_buffer[index] == NULL )
    {
        status = init_iio_buffer(index);
        UNLOCK_BUFFER( index );

        if ( status != 0 )
        {
            skiq_error("Unable to create IIO sample buffer");
            _b_is_running[index] = false;
            // this is a critical error...we can't receive if we can't create the buffer
            hal_critical_exit(status);
        }
    }
    else
    {
        UNLOCK_BUFFER(index);
    }

    /* IIO context and buffer(s) have been initialized, signal that the thread is ready */
    if ( status == 0 )
    {
        LOCK(_thread_ready_state[index].lock);
        {
            _thread_ready_state[index].is_ready = true;
            pthread_cond_signal( &(_thread_ready_state[index].cond) );
        }
        UNLOCK(_thread_ready_state[index].lock);
    }

    while ( ( _b_running[index] == true ) && ( status == 0 ) )
    {
        if( _b_discard[index] == true )
        {
            pthread_mutex_lock( &(_streaming_mutex[index]) );
            if( _b_discard[index] == true )
            {
                debug_print("Entering discard state on card %u\n", index);

                /* signal to any waiting caller to indicate that this thread is now successfully in
                 * the discard state */
                pthread_cond_signal( &_streaming_cond[index] );

                /* wait until a caller signals this thread to check for a chance to the `b_discard`
                 * state */
                pthread_cond_wait( &_streaming_cond[index], &_streaming_mutex[index] );

                debug_print("Leaving discard state on card %u\n", index);
            }
            pthread_mutex_unlock( &_streaming_mutex[index] );
        }

        if (_b_running[index] == false)
        {
            debug_print("No longer running, continue to end of thread function on card %u\n",
                        index);
            continue;
        }

        LOCK_BUFFER(index);
        if( _p_buffer[index] != NULL )
        {
            num_bytes = iio_buffer_refill( _p_buffer[index] );
            if( num_bytes == (_num_samples_per_buf[index]*sizeof(int16_t)*2) )
            {
                // get the start buffer pointer
                p_tmp_buf = (uint32_t*)(iio_buffer_start(_p_buffer[index]));

                if( _b_discard[index] == false )
                {
                    p_tmp_pkt = (uint32_t*)(g_async_queue_try_pop(p_write_queue[index]));
                    if( p_tmp_pkt == NULL )
                    {
                        // try to add write packets if avail
                        _check_read_pkts_and_push( index );

                        // wait to get a write packet off the queue if we didn't already get one
                        // (unless we're heading toward the discard state)
                        if ( _b_discard[index] == false )
                        {
                            int32_t timeout = WRITE_PKT_AVAIL_TIMEOUT_US;

                            while ( ( timeout > 0 ) && ( p_tmp_pkt == NULL ) &&
                                    ( _b_discard[index] == false ) )
                            {
                                p_tmp_pkt = \
                                    (uint32_t*)(g_async_queue_timeout_pop( p_write_queue[index],
                                                                           WRITE_PKT_AVAIL_POLL_US ));
                                timeout -= WRITE_PKT_AVAIL_POLL_US;
                            }
                        }
                    }

                    if( p_tmp_pkt == NULL )
                    {
                        debug_print("No write packet available in queue on card %u\n", index);
                        debug_print(" Read Queue Length: %d\n", g_async_queue_length( p_read_queue[index] ));
                        debug_print("Write Queue Length: %d\n", g_async_queue_length( p_write_queue[index] ));
                    }
                    else
                    {
                        // copy the data from our IIO buffer to our internal buffer to be placed on
                        // the queue
                        MEMCPY( p_tmp_pkt, p_tmp_buf, num_bytes, index );
                    }

                    /* now that the memory copying has taken place, allow others to operate on the
                     * buffer */
                    UNLOCK_BUFFER(index);

                    if ( p_tmp_pkt != NULL )
                    {
                        for( i=0; i<_num_read_packets_per_write[index]; i++ )
                        {
                            debug_check_meta( index, i, (skiq_rx_block_t *)p_tmp_pkt );

                            // no matter if we're discarding, we need to push the packet on the
                            // queue and it will get ignored on the receive call
                            g_async_queue_push( p_read_queue[index], (gpointer)(p_tmp_pkt) );
                            p_tmp_pkt += (PACKET_SIZE_IN_WORDS);
                        }
                    }
                }
                else
                {
                    UNLOCK_BUFFER(index);
                }
            }
            else
            {
                UNLOCK_BUFFER(index);
                debug_print("iio_buffer_refill returned %d number of bytes\n", num_bytes);

                // if we're supposed to be running and not discarding samples, we must have
                // had a buffer error, let's wait until we're signaled that the buffer
                // is available before trying to refill it again
                if( _b_running[index] == true && _b_discard[index] == false )
                {
                    debug_print("Waiting for buffer to become ready on index %u\n", index);
                    pthread_mutex_lock( &_buf_ready_mutex[index] );
                    pthread_cond_wait( &_buf_ready_cond[index], &_buf_ready_mutex[index] );
                    pthread_mutex_unlock( &_buf_ready_mutex[index] );
                    debug_print("Waited for buffer to become ready on index %u\n", index);
                }
            }
        }
        else
        {
            UNLOCK_BUFFER(index);
        }
    }

    // unreference the queues
    g_async_queue_unref( p_write_queue[index] );
    p_write_queue[index] = NULL;
    g_async_queue_unref( p_read_queue[index] );
    p_read_queue[index] = NULL;

    // signal that the thread has completed
    _b_is_running[index] = false;

    debug_print("Stopping skiq_rx_%u thread\n", index);

    return NULL;
}


/* Pause servicing transport for one specific handle (@p hdl) or all handles (@p hdl ==
 * AXI_ALL_RX_HANDLES) by setting the discard state to 'true' and signalling the IIO RX thread */
int32_t iio_rx_pause( uint8_t index,
                      skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    struct timespec ts;
    bool cancel_and_destroy = false;

    /* The `all_hdls_paused` is an alias for `hdl == AXI_ALL_RX_HANDLES` and means the caller is
     * requesting that all handles are to be paused regardless of individual handle state */
    static bool all_hdls_paused = false;

    debug_print("Pausing stream for %s on index %u\n", ( hdl == AXI_ALL_RX_HANDLES ) ? "ALL" :
                rx_hdl_cstr( hdl ), index);

    /* set discard state to 'true', signal the thread to tell it to check the `_b_discard`
     * condition, and finally perform a timed wait until the receive thread has entered the
     * discarding state.  If the wait times out, proceed to cancel and destroy the IIO buffer */
    LOCK( _streaming_mutex[index] );
    {
        if ( hdl == AXI_ALL_RX_HANDLES )
        {
            /* all handles are being paused */
            all_hdls_paused = true;
        }
        else if ( ( hdl < skiq_rx_hdl_end ) && !is_hdl_paused(index, hdl) )
        {
            /* a specific handle is being paused */
            mark_hdl_as_paused(index, hdl);
            if ( _streaming_count[index] == 0 )
            {
                UNLOCK(_streaming_mutex[index]);
                status = -EPROTO;
                hal_critical_exit(status);
                return status;
            }
            else
            {
                /* track the number of handles that are streaming */
                _streaming_count[index]--;
            }
        }

        /* if all handles are paused (as requested by the call) or if no specific handles are
         * streaming, enter the discard state and signal the thread */
        if ( all_hdls_paused || ( _streaming_count[index] == 0 ) )
        {
            _b_discard[index] = true;
            pthread_cond_signal( &_streaming_cond[index] );

            /* determine a reasonable timespec for the condition wait to timeout */
            status = clock_gettime(CLOCK_REALTIME, &ts);
            ts.tv_nsec += (long)IIO_REFILL_TIMEOUT_MS * (long)MILLISEC;
            if ( ts.tv_nsec >= (long)SEC )
            {
                ts.tv_nsec -= (long)SEC;
                ts.tv_sec++;
            }

            status = pthread_cond_timedwait( &_streaming_cond[index], &_streaming_mutex[index], &ts );
            if ( status == ETIMEDOUT )
            {
                debug_print("Timed out waiting for streaming condition to be satisfied on index %u\n", index);
                cancel_and_destroy = true;
                status = 0;
            }
        }
    }
    UNLOCK( _streaming_mutex[index] );

    if ( cancel_and_destroy )
    {
        debug_print("Trying to cancel / re-allocate buffer on index %u\n", index);
        status = cancel_destroy_init_iio_buffer( index );
    }

    debug_print("Paused stream on index %u\n", index);

    return (status);
}


/* Resume servicing transport for one specific handle (@p hdl) or all paused handles (@p hdl ==
 * AXI_ALL_RX_HANDLES) by setting the discard state to 'false' and signaling the IIO RX thread as
 * long as _streaming_count[index] > 0 */
int32_t iio_rx_resume( uint8_t index,
                       skiq_rx_hdl_t hdl )
{
    int32_t status=0;

    /* The `all_hdls_resumed` is an alias for `hdl == AXI_ALL_RX_HANDLES` and means the caller is
     * requesting that all paused handles are to be resumed regardless of individual handle state.
     * It may be the case that the transport is shutting down and the IIO RX thread needs to be
     * signaled to exit the discard state in order to exit cleanly (see call in
     * stop_iio_rx_thread) */
    static bool all_hdls_resumed = false;

    debug_print("Resuming stream for %s on index %u\n", ( hdl == AXI_ALL_RX_HANDLES ) ? "ALL" :
                rx_hdl_cstr( hdl ), index);

    LOCK( _streaming_mutex[index] );
    {
        if ( hdl == AXI_ALL_RX_HANDLES )
        {
            /* All paused handles are being resumed */
            all_hdls_resumed = true;
        }
        else if ( ( hdl < skiq_rx_hdl_end ) && is_hdl_paused(index, hdl) )
        {
            /* one specific handle is being resumed (aka started) */
            mark_hdl_as_resumed(index, hdl);
            _streaming_count[index]++;

            /* update the flush timestamp for the specified handle */
            LOCK_FLUSH_TS(index);
            {
                status = skiq_read_curr_rx_timestamp( index, hdl,
                                                      &(_flush_state[index].flush_timestamp[hdl]) );
                if ( status == 0 )
                {
                    _flush_state[index].is_flushing[hdl] = true;
                    debug_print("Captured flush timestamp on handle %s as %" PRIu64 "\n",
                                rx_hdl_cstr( hdl ), _flush_state[index].flush_timestamp[hdl]);
                }
            }
            UNLOCK_FLUSH_TS(index);
        }

        /* if all handles are resumed OR at least one specific handle is streaming, exit the discard
         * state and signal the thread */
        if ( all_hdls_resumed || ( _streaming_count[index] > 0 ) )
        {
            _b_discard[index] = false;
            pthread_cond_signal( &_streaming_cond[index] );
        }
    }
    UNLOCK( _streaming_mutex[index] );

    debug_print("Resumed stream for %s on index %u\n", ( hdl == AXI_ALL_RX_HANDLES ) ? "ALL" :
                rx_hdl_cstr( hdl ), index);

    return (status);
}


/* NOTE: this assumes that the receive thread is in a discarding state and not producing sample
 * blocks */
int32_t iio_rx_flush( uint8_t index )
{
    int32_t status=0;

    debug_print("Flushing stream on index %u\n", index);
    {
        uint8_t *p_data = NULL;
        uint32_t data_len;
        uint32_t nr_flushed = 0;

        while ( 0 == iio_rx_thread_get_data( index, &p_data, &data_len ) )
        {
            nr_flushed++;
        }
        debug_print("Flushed %u block(s) on index %u\n", nr_flushed, index);
    }
    debug_print("Flushed stream on index %u\n", index);

    LOCK_FLUSH_TS(index);
    {
        skiq_rx_hdl_t hdl;

        for ( hdl = skiq_rx_hdl_A1; ( hdl < skiq_rx_hdl_end ) && ( status == 0 ); hdl++ )
        {
            if ( !is_hdl_paused( index, hdl ) )
            {
                status = skiq_read_curr_rx_timestamp( index, hdl,
                                                      &(_flush_state[index].flush_timestamp[hdl]) );
                if ( status == 0 )
                {
                    _flush_state[index].is_flushing[hdl] = true;
                    debug_print("Captured flush timestamp on handle %s as %" PRIu64 "\n",
                                rx_hdl_cstr( hdl ), _flush_state[index].flush_timestamp[hdl]);
                }
            }
            else
            {
                _flush_state[index].is_flushing[hdl] = true;
                _flush_state[index].flush_timestamp[hdl] = 0;
            }
        }
    }
    UNLOCK_FLUSH_TS(index);

    return (status);
}


int32_t stop_iio_rx_thread( uint8_t index )
{
    int32_t status=0;
    uint32_t shutdownCounter = NUM_THREAD_SHUTDOWN_RETRIES;
    int result = 0;

    _b_discard[index] = true;
    _b_running[index] = false;

    /* Since the receive thread holds the buffer lock while blocked on iio_buffer_refill(), try
     * cancelling prior to requesting the buffer lock */
    if( _p_buffer[index] != NULL )
    {
        iio_buffer_cancel( _p_buffer[index] );
    }

    LOCK_BUFFER(index);
    {
        // cancel any pending IIO transaction
        if( _p_buffer[index] != NULL )
        {
            iio_buffer_cancel( _p_buffer[index] );

            // note that the buffer could have gotten freed / set to NULL before we got the mutex,
            // so make sure we check for NULL before destroying
            if( _p_buffer[index] != NULL )
            {
                iio_buffer_destroy( _p_buffer[index] );
                _p_buffer[index] = NULL;
            }
        }
    }
    UNLOCK_BUFFER(index);

    // cancel/interrupt and join the thread
    // use iio_rx_resume() to ensure that the thread isn't blocking waiting for the streaming
    // condition (and hopefully releases the streaming mutex)
    iio_rx_resume(index, AXI_ALL_RX_HANDLES);

    /* in case the thread is waiting on the buffer to be ready, throw a signal its way */
    pthread_mutex_lock( &_buf_ready_mutex[index] );
    pthread_cond_signal( &_buf_ready_cond[index] );
    pthread_mutex_unlock( &_buf_ready_mutex[index] );

    // wait for the thread to (hopefully) signal that it's shut down - this means that it should've
    // freed all of its resources
    while ((0 < shutdownCounter) && (_b_is_running[index]))
    {
        if (_b_is_running[index])
        {
            hal_microsleep(TIME_BETWEEN_THREAD_SHUTDOWN_RETRIES_US);
        }
        shutdownCounter--;
    }
    if (0 == shutdownCounter)
    {
        skiq_debug("Timed out waiting for RX thread to gracefully exit\n");
    }
    result = pthread_cancel( _rx_thread[index] );
    if ((0 != result) && (ESRCH != result))
    {
        skiq_warning("Failed while shutting down RX thread on index %" PRIu8 " (result = %" PRIi32 ")\n",
            index, result);
    }

    debug_print("Joining receive thread on index %u\n", index);
    result = pthread_join( _rx_thread[index], NULL );
    debug_print("Joined receive thread on index %u\n", index);

    if (0 != result)
    {
        skiq_warning("Failed while joining RX thread on index %" PRIu8 " (result = %" PRIi32 ")\n",
            index, result);
    }

    if( _p_context[index] != NULL )
    {
        iio_context_destroy( _p_context[index] );
        _p_context[index] = NULL;
    }

    if( _memcpy256_unaligned[index] == true )
    {
        skiq_debug("Memcpy256 disabled due to unaligned buffers or unaligned length\n");
    }

    return (status);
}
