/**
 * @file   axi_fpga_registers.c
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include "sidekiq_xport_types.h"
#include "axi_transport_private.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"

#define FPGA_REG_MEMMAP_SIZE ((int)(0x10000))

#define CHECK_FPGA_REG(_addr)                                           \
    do {                                                                \
        __typeof__ (_addr) __addr = (_addr);                            \
        if ( __addr >= FPGA_REG_MEMMAP_SIZE )                           \
        {                                                               \
            skiq_error("Invalid register addr 0x%08X\n", __addr);       \
            hal_critical_exit(-ERANGE);                                 \
            return -ERANGE;                                             \
        }                                                               \
    } while (0)


// Z2 is expecting registers to start at 0, but Sidekiq starts at 0x8000...
// so we need to convert the register access to handle this offset
#define SKIQ_FPGA_REG_OFFSET            (0x8000)
#define SKIQ_TO_AXI_REG(_reg)           ((_reg) - SKIQ_FPGA_REG_OFFSET)

#ifndef SYS_UIO_PATH
#   define SYS_UIO_PATH     "/sys/class/uio"
#endif

#ifndef UIO_NAME
#   define UIO_NAME         "name"
#endif

#ifndef DEV_BASE
#   define DEV_BASE         "/dev"
#endif

#ifndef AXI_REG_IF
#   define AXI_REG_IF  "axi_to_reg_if"
#endif

#ifndef AXI_REG_IOPE_IF
#   define AXI_REG_IOPE_IF  "axi_iope0_user"
#endif

// Filename can only be as large as path is allowed to be
#if (defined __MINGW32__)
#define MAX_UIO_FILENAME (260)
#else
#include <linux/limits.h>
#define MAX_UIO_FILENAME (PATH_MAX)
#endif

int32_t _fpga_reg_read( uint64_t xport_uid, uint32_t addr, uint32_t* p_data );
int32_t _fpga_reg_write( uint64_t xport_uid, uint32_t addr, uint32_t data );
int32_t _fpga_reg_read_64( uint64_t xport_uid, uint32_t addr, uint64_t* p_data );
int32_t _fpga_reg_write_64( uint64_t xport_uid, uint32_t addr, uint64_t data );

// Note: if we ever support more than 1 card, this will have to be expanded
static uint8_t* _reg_ptr = NULL;

#if (defined USER_AXI_IF)
#ifndef AXI_NUM_IF
#   define AXI_NUM_IF 3
#endif
static char* AXI_IF_NAMES[AXI_NUM_IF] = {AXI_REG_IF, AXI_REG_IOPE_IF, USER_AXI_IF};
#else
#ifndef AXI_NUM_IF
#   define AXI_NUM_IF 2
#endif
static char* AXI_IF_NAMES[AXI_NUM_IF] = {AXI_REG_IF, AXI_REG_IOPE_IF};
#endif

int32_t _open_axi_reg_iface(void)
{
    int32_t status = 0;
    uint8_t i = 0;
    int fd;
    DIR *p_uio_sys;
    struct dirent *p_dir_entry;
    char buf[MAX_UIO_FILENAME];
    bool b_found_reg=false;
    FILE *p_uio_name;

    errno = 0;
    p_uio_sys = opendir(SYS_UIO_PATH);
    if( p_uio_sys != NULL )
    {
        // we need to look for our AXI register interface
        errno = 0;
        p_dir_entry = readdir(p_uio_sys);
        while( (b_found_reg == false) && (p_dir_entry != NULL) )
        {
            // get the full path of the UIO name file
            if((size_t)snprintf( buf, MAX_UIO_FILENAME, "%s/%s/%s",
                      SYS_UIO_PATH, p_dir_entry->d_name, UIO_NAME ) >= sizeof(buf))
            {
                skiq_info("The full path of the UIO name was larger than the buffer and truncated");
            }
            // open the name and read it
            p_uio_name = fopen( buf, "r" );
            if( p_uio_name != NULL )
            {
                // now read the name
                if( fread( buf, 1, MAX_UIO_FILENAME, p_uio_name ) > 0 )
                {
                    for(i = 0; (b_found_reg == false && i < AXI_NUM_IF); i++)
                    {
                        if( strncmp(buf, AXI_IF_NAMES[i], strlen(AXI_IF_NAMES[i])) == 0 )
                        {
                            b_found_reg = true;
                            if((size_t)snprintf( buf, MAX_UIO_FILENAME, "%s/%s", DEV_BASE, p_dir_entry->d_name) >= sizeof(buf))
                            {
                                skiq_info("The full path of the UIO name was larger than the buffer and truncated");
                            }
                        }
                    }
                }
                fclose( p_uio_name );
            }
            errno = 0;
            p_dir_entry = readdir(p_uio_sys);
        }
        if( errno != 0 )
        {
            skiq_info("Errno is %d from readdir", errno);
        }
        if( b_found_reg == false )
        {
            status = -ENOENT;
        }

        if( status == 0 )
        {
            errno = 0;
            fd = open(buf, O_RDWR);
            if( fd >= 0 )
            {
                errno = 0;
                _reg_ptr = mmap(NULL, FPGA_REG_MEMMAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
                if( _reg_ptr == MAP_FAILED )
                {
                    skiq_error("Failed to map register device file %s into memory (errno=%d)",
                               buf, errno);
                    status = -errno;
                }
                
                close(fd);
            }
            else if ( errno == ENOENT )
            {
                /* if the file doesn't exist, this may be a platform that doesn't use it, so keep
                 * quiet (e.g. no call to _skiq_log()) so as not to confuse the user. */
                status = -ENOENT;
            }
            else
            {
                skiq_error("Failed to open %s (errno=%d)", buf, errno);
                status = -errno;
            }
        }
        closedir( p_uio_sys );
    }
    else if ( errno == ENOENT )
    {
        /* if the directory doesn't exist, this may be a platform that doesn't use it, so keep quiet
         * (e.g. no call to _skiq_log()) so as not to confuse the user. */
        status = -errno;
    }
    else
    {
        skiq_error("Failed to open directory %s (errno=%d)", SYS_UIO_PATH, errno);
        status = -errno;
    }

    if ( status == 0 )
    {
        /* perform a DMAC IRQ fixup (the called function determines if it is necessary) */
        status = axi_dmac_irq_reg_fixup();
    }

    return (status);
}

int32_t _close_axi_reg_iface(void)
{
    int32_t status = -ENODEV;
    
    if( _reg_ptr != NULL )
    {
        errno = 0;
        status = munmap( _reg_ptr, FPGA_REG_MEMMAP_SIZE );
        if ( status != 0 )
        {
            skiq_error("Failed to unmap register device file from memory (errno=%d)", errno);
            status = -errno;
        }
        _reg_ptr = NULL;
    }

    return (status);
}

int32_t _fpga_reg_read( uint64_t xport_uid, uint32_t addr, uint32_t* p_data )
{
    int32_t status = -ENODEV;
    uint32_t nep_addr=0;
    
    if( _reg_ptr != NULL )
    {
        nep_addr = SKIQ_TO_AXI_REG(addr);
        CHECK_FPGA_REG(nep_addr);
        
        *p_data = *((volatile uint32_t*)(_reg_ptr + nep_addr));
        status = 0;
    }

    return (status);
}

int32_t _fpga_reg_read_64( uint64_t xport_uid, uint32_t addr, uint64_t* p_data )
{
    int32_t status = -ENODEV;
    uint32_t nep_addr=0;

    if( _reg_ptr != NULL )
    {
        nep_addr = SKIQ_TO_AXI_REG(addr);
        CHECK_FPGA_REG(nep_addr);

        *p_data = *((volatile uint64_t*)(_reg_ptr + nep_addr));
        status = 0;
    }

    return (status);
}

int32_t _fpga_reg_write( uint64_t xport_uid, uint32_t addr, uint32_t data )
{
    int32_t status = -ENODEV;
    uint32_t nep_addr=0;

    if( _reg_ptr != NULL )
    {
        nep_addr = SKIQ_TO_AXI_REG(addr);
        CHECK_FPGA_REG(nep_addr);
        
        *((volatile uint32_t*)(_reg_ptr + nep_addr)) = data;
        status = 0;
    }

    return (status);
}

int32_t _fpga_reg_write_64( uint64_t xport_uid, uint32_t addr, uint64_t data )
{
    int32_t status = -ENODEV;
    uint32_t nep_addr=0;
    
    if( _reg_ptr != NULL )
    {
        nep_addr = SKIQ_TO_AXI_REG(addr);
        CHECK_FPGA_REG(nep_addr);

        *((volatile uint64_t*)(_reg_ptr + nep_addr)) = data;
        status = 0;
    }

    return (status);
}

skiq_xport_fpga_functions_t axi_xport_fpga_funcs = {
    .fpga_reg_read = _fpga_reg_read,
    .fpga_reg_write = _fpga_reg_write,
    .fpga_down_reload = NULL,
    .fpga_up = NULL,
    .fpga_down = NULL,
#ifdef USE_REG64
    .fpga_reg_read_64 = _fpga_reg_read_64,
    .fpga_reg_write_64 = _fpga_reg_write_64,
#else
    .fpga_reg_read_64 = NULL,
    .fpga_reg_write_64 = NULL,
#endif    
};
