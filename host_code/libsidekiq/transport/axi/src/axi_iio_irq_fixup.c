/**
 * @file   axi_irq_fixup.c
 *
 * @brief Applies IRQ fixup by ensuring that the IIO DMA engine IRQs are properly enabled in Z2 BSP
 * v3.1.0.
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "sidekiq_private.h"    /* for _skiq_log */

#include "axi_transport_private.h"

/* Filename can only be as large as path is allowed to be */
#if (defined __MINGW32__)
#define MAX_PATH                        260
#else
#include <linux/limits.h>
#define MAX_PATH                        PATH_MAX
#endif

/* In Z2 BSP v3.1.0, the "rx_dma" symbol provides us with a relative device tree path to the receive
 * IIO DMAC's register space, which is described here --
 * https://wiki.analog.com/resources/fpga/docs/axi_dmac#register_map */
#define RX_DMA_SYMBOL_PATH              "/proc/device-tree/__symbols__/rx_dma"

#define AXI_DMAC_IRQ_MASK_REG_OFFSET              0x80
#define AXI_DMAC_IRQ_MASK_TRANSFER_COMPLETED      (1 << 1)
#define AXI_DMAC_IRQ_MASK_TRANSFER_QUEUED         (1 << 0)
#define AXI_DMAC_IRQ_MASK                         (AXI_DMAC_IRQ_MASK_TRANSFER_COMPLETED | \
                                                   AXI_DMAC_IRQ_MASK_TRANSFER_QUEUED)

/**
   @brief This function unmasks two IRQ(s) associated with receive DMAC to ensure they are properly
   enabled in Z2 BSP v3.1.0

   Fixup Steps
   1. Open RX_DMA_SYMBOL_PATH to read its contents as <symbol-path>
   2. Open /proc/device-tree/<symbol-path>/reg to read its contents as (reg_addr, reg_length)
   3. Open /dev/mem and memory map it starting at reg_addr with length reg_length
   4. Perform the fixup by unmasking the transfer IRQ(s) at reg_addr+AXI_DMAC_IRQ_MASK_REG_OFFSET

   @retval -ENODEV if opening the symbol path fails
   @retval -E2BIG  if register file path is too long
   @retval -ENODEV if opening the register file fails
   @retval -EFAULT if fread is short from the symbol entry or register file
   @retval -ENODEV if the memory map call fails
 */
int32_t axi_dmac_irq_reg_fixup( void )
{
    int32_t status = -ENODEV;
    char dt_symbol[MAX_PATH], dt_regpath[MAX_PATH];
    uint32_t reg_addr = 0, reg_length = 0;
    int fd_mem = -1;
    FILE *fp = NULL;
    
    fp = fopen( RX_DMA_SYMBOL_PATH, "r" );
    if ( fp != NULL )
    {
        size_t n;

        memset(dt_symbol, '\0', MAX_PATH);
        n = fread( dt_symbol, 1, MAX_PATH - 1, fp );
        if ( n > 0 )
        {
            status = 0;
        }
        else if ( !ferror( fp ) )
        {
            skiq_warning("IIO DMA device tree entry exists but is empty, receive streaming may "
                         "be affected\n");
            status = -EFAULT;
        }
        else
        {
            skiq_error("Error occurred reading from IIO DMA device tree entry\n");
            status = -ENODEV;
        }

        fclose(fp);
        fp = NULL;
    }
    else
    {
        status = -ENOENT;
    }

    if ( status == 0 )
    {
        ssize_t n;

        /* entry assumption: dt_symbol contains the name of the 'rx_dma' device-tree symbol */

        memset(dt_regpath, '\0', MAX_PATH);
        n = snprintf(dt_regpath, MAX_PATH, "/proc/device-tree%s/reg", dt_symbol);
        if ( n >= MAX_PATH )
        {
            skiq_error("Register file pathname exceeds maximum path length (/proc/device-tree"
                       "%s/reg)\n", dt_symbol);
            status = -E2BIG;
        }
    }

    if ( status == 0 )
    {
        /* entry assumption: dt_regpath contains the full path of the rx_dma symbol register file */

        fp = fopen( dt_regpath, "r" );
        if ( fp == NULL )
        {
            skiq_error("IIO DMA symbol register file (%s) not found, receive streaming may be "
                       "affected\n", dt_regpath);
            status = -ENODEV;
        }
    }

    if ( status == 0 )
    {
        /* entry assumption: 'fp' is open and valid */

        if ( ( fread( &reg_addr, sizeof( reg_addr ), 1, fp ) == 1 ) &&
             ( fread( &reg_length, sizeof( reg_length ), 1, fp ) == 1 ) )
        {
            reg_addr = be32toh(reg_addr);
            reg_length = be32toh(reg_length);
        }
        else if ( !ferror( fp ) )
        {
            skiq_error("IIO DMA symbol register file has unexpected contents, receive streaming "
                       "may be affected\n");
            status = -EFAULT;
        }
        else
        {
            skiq_error("Error occurred reading from IIO DMA symbol register file\n");
            status = -ENODEV;
        }

        /* close 'fp' unconditionally */
        fclose(fp);
        fp = NULL;
    }

    if ( status == 0 )
    {
        /* entry assumption: none */

        errno = 0;
        fd_mem = open("/dev/mem", O_RDWR | O_SYNC);
        if ( fd_mem < 0 )
        {
            skiq_error("Failed to open /dev/mem with errno %d\n", errno);
            status = -ENODEV;
        }
    }

    if ( status == 0 )
    {
        /* entry assumption: reg_addr represents register address offset, reg_length is the size of
         * the register space, fd_mem is open and valid */

        uint8_t *reg_ptr;

        errno = 0;
        reg_ptr = (uint8_t *) mmap( NULL,
                                    reg_length,
                                    PROT_READ | PROT_WRITE,
                                    MAP_SHARED,
                                    fd_mem,
                                    reg_addr );
        if ( reg_ptr != MAP_FAILED )
        {
            uint8_t *irq_reg_ptr = reg_ptr + AXI_DMAC_IRQ_MASK_REG_OFFSET;

            if ( ( *irq_reg_ptr & AXI_DMAC_IRQ_MASK ) != 0 )
            {
                /* perform the fixup by unmasking the transfer IRQ(s) in
                 * AXI_DMAC_IRQ_MASK_REG_OFFSET */
                skiq_info("Unmasking IIO DMA IRQ(s)\n");
                *irq_reg_ptr &= ~AXI_DMAC_IRQ_MASK;
            }

            (void) munmap( reg_ptr, reg_length );
            reg_ptr = NULL;
        }
        else
        {
            skiq_error("Failed to mmap /dev/mem with errno %d\n", errno);
            status = -ENODEV;
        }

        /* unconditionally close fd_mem */
        close( fd_mem );
        fd_mem = -1;
    }

    if ( status == -ENOENT )
    {
        /* BSP v3.1.0 "always" has "rx_dma", so this must be a different BSP and the fix-up is not
         * needed.  overwrite status since having a different BSP is not a crime. */
        status = 0;
    }

    return status;
}
