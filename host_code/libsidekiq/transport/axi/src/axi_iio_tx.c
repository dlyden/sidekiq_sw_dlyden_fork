/**
 * @file axi_iio_tx.c
 * <pre>
 * Copyright 2016-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
#include <stdint.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_private.h"
#include "sidekiq_xport_api.h"
#include "axi_transport.h"
#include "axi_transport_private.h"
#include "sidekiq_hal.h"

#include "iio.h"

#include <string.h>
#include <inttypes.h>

/***** DEFINES *****/
/** 
 Nearly all Sidekiq products should have a "epiq_dac_core".  However, for Z2p,
 we could not have anything indicating "Epiq" or "Sidekiq" in the base image.
 As a result, we used the "pcg" name instead of the "epiq" name to ID the radio.
**/
static const char *_EPIQ_RADIO_TX_STREAM_DEV = "epiq_dac_core";
static const char *_PCG_RADIO_TX_STREAM_DEV = "pcg_dac_core";

static const char *_RADIO_TX_STREAM_I = "voltage0";
static const char *_RADIO_TX_STREAM_Q = "voltage1";

#define NUM_KERNEL_BUFS (64) // arbitrary

/***** TYPEDEFS *****/

/***** STRUCTS *****/

/***** LOCAL VARIABLES *****/
static struct iio_buffer *_p_buffer[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = NULL };
static uint32_t _num_bytes_to_send[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = 1024 };
static struct iio_context *_p_context[AXI_MAX_INDEX] = { [ 0 ... (AXI_MAX_INDEX-1) ] = NULL };  // IIO context

/***** LOCAL FUNCTIONS *****/
int32_t _axi_iio_tx_initialize( uint64_t xport_uid,
                                skiq_tx_transfer_mode_t tx_transfer_mode,
                                uint32_t num_bytes_to_send,
                                uint8_t num_send_threads,
                                int32_t priority,
                                skiq_tx_callback_t tx_complete_cb );

int32_t _axi_iio_tx_start_streaming( uint64_t xport_uid,
                                      skiq_tx_hdl_t hdl );

int32_t _axi_iio_tx_stop_streaming( uint64_t xport_uid,
                                    skiq_tx_hdl_t hdl );

int32_t _axi_iio_tx_transmit( uint64_t xport_uid,
                              skiq_tx_hdl_t hdl,
                              int32_t *p_samples,
                              void *p_private );

/***** GLOBAL DATA *****/
skiq_xport_tx_functions_t axi_iio_xport_tx_funcs = {
    .tx_initialize      = _axi_iio_tx_initialize,
    .tx_start_streaming = NULL, 
    .tx_pre_stop_streaming = NULL,
    .tx_stop_streaming  = _axi_iio_tx_stop_streaming,
    .tx_transmit        = _axi_iio_tx_transmit,
};

int32_t _axi_iio_tx_initialize( uint64_t xport_uid,
                                skiq_tx_transfer_mode_t tx_transfer_mode,
                                uint32_t num_bytes_to_send,
                                uint8_t num_send_threads,
                                int32_t priority,
                                skiq_tx_callback_t tx_complete_cb )
{
    int32_t status=0;
    uint8_t index=0;
    const char *p_radio_dev = _EPIQ_RADIO_TX_STREAM_DEV;
    struct iio_device *p_tx_stream;  // radio TX stream device
    struct iio_channel *p_tx_i_chan; // TX I channel
    struct iio_channel *p_tx_q_chan; // TX Q channel

    /* Z2p is not allowed to have "Epiq" in the driver name, so the PCG driver name needs to be used */
    if( _skiq_get_part(index) == skiq_z2p )
    {
        p_radio_dev = _PCG_RADIO_TX_STREAM_DEV;
    }

    // we don't support async mode or multiple threads, so return an error if requested
    if( tx_transfer_mode != skiq_tx_transfer_mode_sync )
    {
        _skiq_log( SKIQ_LOG_ERROR, "Invalid TX transfer mode specified %u\n", tx_transfer_mode );
        status = -ENOTSUP;
    }
    else
    {
        if( (status=_find_axi_index_by_uid(xport_uid, &index)) != 0 )
        {
            _skiq_log( SKIQ_LOG_ERROR, "Invalid transport index (%" PRIu64 ")\n", xport_uid );
            status = -ENODEV;
        }
        else
        {
            // FPGA version of at least 3.10.0 is required for TX
            if( !_skiq_meets_FPGA_VERSION(index, FPGA_VERS_AXI_TX_SUPPORT ) )
            {
                skiq_warning("FPGA version does not support transmit (minimum is %s)\n",
                             FPGA_VERS_AXI_TX_SUPPORT_CSTR);
                status = -ENOTSUP;
            }
        }
    }

    if( status == 0 )
    {
        _num_bytes_to_send[index] = num_bytes_to_send;
        if( _p_context[index] == NULL )
        {
            // so far, so good, try to create an IIO buffer with the # bytes requested
            if( (_p_context[index] = iio_create_default_context()) == NULL )
            {
                status = -ENODEV;
                goto cleanup;
            }
        }

        if( (p_tx_stream =
             iio_context_find_device( _p_context[index],
                                      p_radio_dev )) != NULL )
        {
            if( iio_context_set_timeout( _p_context[index], 0 ) != 0 )
            {
                status = -EINVAL;
                goto cleanup;
            }
            if( (p_tx_i_chan = iio_device_find_channel( p_tx_stream,
                                                        _RADIO_TX_STREAM_I,
                                                        true /* output */)) == NULL ||
                (p_tx_q_chan = iio_device_find_channel( p_tx_stream,
                                                        _RADIO_TX_STREAM_Q,
                                                        true /* output */ )) == NULL )
            {
                status = -ENOENT;
            }
            else
            {
                if( iio_device_set_kernel_buffers_count( p_tx_stream, NUM_KERNEL_BUFS ) != 0 )
                {
                    _skiq_log(SKIQ_LOG_ERROR, "Configuring the buffer size unavailable");
                    status = -ENODEV;
                }

                if( (iio_channel_is_scan_element(p_tx_i_chan) == true) &&
                    (iio_channel_is_scan_element(p_tx_q_chan) == true) )
                {
                    // ad9361-iiostream.c disables the chans after destroying buf
                    iio_channel_disable(p_tx_i_chan);
                    iio_channel_disable(p_tx_q_chan);
                    iio_channel_enable(p_tx_i_chan);
                    iio_channel_enable(p_tx_q_chan);

                    // make sure to convert from bytes to words since each buffer element is 1 sample
                    if( (_p_buffer[index] = iio_device_create_buffer( p_tx_stream,
                                                                      _num_bytes_to_send[index]/4,
                                                                      false )) != NULL )
                    {
                        status = iio_buffer_set_blocking_mode( _p_buffer[index], true );
                    }
                    else
                    {
                        _skiq_log(SKIQ_LOG_ERROR, "Unable to create transmit buffer\n");
                        status = -ENOMEM;
                    }
                }
                else
                {
                    _skiq_log(SKIQ_LOG_ERROR, "I/Q channels are not configured properly\n");
                    status = -ENOENT;
                }
            }
        }
        else
        {
            _skiq_log(SKIQ_LOG_ERROR, "Unable to find TX device context (%s)\n", p_radio_dev);
            status = -ENOENT;
        }
    }

    // cleanup buffer / context if there was an error
cleanup:
    if( status != 0 )
    {
        if( _p_buffer[index] != NULL )
        {
            iio_buffer_destroy( _p_buffer[index] );
            _p_buffer[index] = NULL;
        }
        if( _p_context[index] != NULL )
        {
            iio_context_destroy( _p_context[index] );
            _p_context[index] = NULL;
        }
    }

    return (status);
}

int32_t _axi_iio_tx_stop_streaming( uint64_t xport_uid,
                                    skiq_tx_hdl_t hdl )
{
    int32_t status=0;
    uint8_t index=0;
    
    if( (status=_find_axi_index_by_uid(xport_uid, &index)) != 0 )
    {
        _skiq_log( SKIQ_LOG_ERROR, "Invalid transport index (%" PRIu64 ")\n", xport_uid );
        status = -ENODEV;
    }
    else
    {
        // destroy the buffer
        if( _p_buffer[index] != NULL )
        {
            iio_buffer_destroy( _p_buffer[index] );
            _p_buffer[index] = NULL;
        }
        // destroy the context
        if( _p_context[index] != NULL )
        {
            iio_context_destroy( _p_context[index] );
            _p_context[index] = NULL;
        }
    }

    return (status);
}

int32_t _axi_iio_tx_transmit( uint64_t xport_uid,
                              skiq_tx_hdl_t hdl,
                              int32_t *p_samples,
                              void *p_private )
{
    int32_t status=0;
    uint8_t index=0;
    int32_t refill_status=0;

    uint32_t *p_start=NULL;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) != 0 )
    {
        status = -ENODEV;
        return (status);
    }
    
    if( _p_buffer[index] == NULL )
    {
        _skiq_log( SKIQ_LOG_ERROR, "Can't transmit, buffer not initialized\n");
        status = -EINVAL;
    }

    if( status == 0 )
    {
        p_start = iio_buffer_start( _p_buffer[index] );
        memcpy( p_start, p_samples, _num_bytes_to_send[index] );
        // buffer_push returns num_bytes sent
        if( (refill_status=iio_buffer_push( _p_buffer[index] )) < 0 )
        {
            status = refill_status;
            _skiq_log( SKIQ_LOG_ERROR, "Unable to push buffer %d\n", status);
        }
        else if( refill_status != _num_bytes_to_send[index] )
        {
            status = -EIO;
        }
    }

    return (status);
}
