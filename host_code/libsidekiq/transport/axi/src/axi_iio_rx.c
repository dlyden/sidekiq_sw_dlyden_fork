/**
 * @file   axi_iio_rx.c
 *
 * @brief
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
/***** INCLUDES *****/ 

#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>

#include "sidekiq_api.h"
#include "sidekiq_xport_types.h"
#include "axi_transport_private.h"
#include "iio_rx_thread.h"
#include "sidekiq_private.h"

#include "iio.h"

/***** DEFINES *****/

/***** TYPEDEFS *****/

/***** STRUCTS *****/

/***** LOCAL VARIABLES *****/
static bool _buffered[AXI_MAX_INDEX] = { [0 ... (AXI_MAX_INDEX-1)] = true };

/***** LOCAL FUNCTIONS *****/
static int32_t _axi_iio_rx_start_streaming( uint64_t xport_uid,
                                            skiq_rx_hdl_t hdl );

static int32_t _axi_iio_rx_stop_streaming( uint64_t xport_uid,
                                           skiq_rx_hdl_t hdl );

static int32_t _axi_iio_rx_pause_streaming( uint64_t xport_uid );
static int32_t _axi_iio_rx_resume_streaming( uint64_t xport_uid );

static int32_t _axi_iio_rx_flush( uint64_t xport_uid );

static int32_t _axi_iio_rx_receive( uint64_t xport_uid,
                                    uint8_t **pp_data,
                                    uint32_t *p_data_len );
int32_t create_rx_thread(void)
{
    int32_t status=0;

    /* display libiio version */
    {
        unsigned int major, minor;
        char git_tag[8];

        iio_library_get_version(&major, &minor, git_tag);
        skiq_info("libiio v%u.%u (tag %s)\n", major, minor, git_tag);
    }

    status = init_iio_rx_thread(0);

    return (status);
}

int32_t destroy_rx_thread(void)
{
    int32_t status=0;

    status = stop_iio_rx_thread(0);

    return (status);
}

int32_t _axi_iio_rx_set_buffered( uint64_t xport_uid, bool buffered )
{
    int32_t status=0;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        // only update if the buffered mode is different
        if( _buffered[index] != buffered )
        {
            status = init_iio_packet_buffered(index, buffered);
            if( status == 0 )
            {
                _buffered[index] = buffered;
            }
        }
    }

    return (status);
}

int32_t _axi_iio_rx_start_streaming( uint64_t xport_uid,
                                     skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        status = iio_rx_resume(index, hdl);
    }

    return (status);
}

int32_t _axi_iio_rx_stop_streaming( uint64_t xport_uid,
                                    skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        // just pause it
        status = iio_rx_pause(index, hdl);
    }

    return (status);
}

int32_t _axi_iio_rx_pause_streaming( uint64_t xport_uid )
{
    int32_t status=0;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        status = iio_rx_pause(index, AXI_ALL_RX_HANDLES);
    }

    return (status);
}

int32_t _axi_iio_rx_resume_streaming( uint64_t xport_uid )
{
    int32_t status=0;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        status = iio_rx_resume(index, AXI_ALL_RX_HANDLES);
    }
    
    return (status);
}

int32_t _axi_iio_rx_flush( uint64_t xport_uid )
{
    int32_t status=0;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        status = iio_rx_flush(index);
    }

    return (status);
}

int32_t _axi_iio_rx_receive( uint64_t xport_uid,
                             uint8_t **pp_data,
                             uint32_t *p_data_len )
{
    int32_t status=-1;
    uint8_t index=0;

    if( (status=_find_axi_index_by_uid(xport_uid, &index)) == 0 )
    {
        status = iio_rx_thread_get_data( index, pp_data, p_data_len );
    }

    return (status);
}


static int32_t _axi_iio_rx_set_transfer_timeout( uint64_t xport_uid,
                                                 const int32_t timeout_us )
{
    int32_t status = 0;
    uint8_t index = 0;

    status = _find_axi_index_by_uid( xport_uid, &index );
    if ( status == 0 )
    {
        status = iio_rx_set_transfer_timeout( index, timeout_us );
    }

    return (status);
}


skiq_xport_rx_functions_t axi_iio_xport_rx_funcs = {
    .rx_configure            = NULL,
    .rx_set_block_size       = NULL,
    .rx_set_buffered         = _axi_iio_rx_set_buffered,
    .rx_start_streaming      = _axi_iio_rx_start_streaming,
    .rx_stop_streaming       = _axi_iio_rx_stop_streaming,
    .rx_pause_streaming      = _axi_iio_rx_pause_streaming,
    .rx_resume_streaming     = _axi_iio_rx_resume_streaming,
    .rx_flush                = _axi_iio_rx_flush,
    .rx_set_transfer_timeout = _axi_iio_rx_set_transfer_timeout,
    .rx_receive              = _axi_iio_rx_receive,
};
