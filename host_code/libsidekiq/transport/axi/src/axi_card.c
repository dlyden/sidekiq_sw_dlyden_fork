/**
 * @file axi_card.c
 * <pre>
 * Copyright 2016-2017 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>

#include "sidekiq_xport_api.h"
#include "axi_transport.h"
#include "axi_transport_private.h"

#define CARD_UID                        _CARD_UID(0)
#define _CARD_UID(_i)                   (((uint64_t)(SUBCLASS_AXI) << 56) | (uint64_t)(_i))

int32_t _axi_card_exit( skiq_xport_init_level_t level, uint64_t xport_uid );

int32_t _find_axi_index_by_uid( uint64_t uid, uint8_t *p_index )
{
    int32_t status = -ENODEV;

    // TODO: update if we ever support more than 1 card
    if ( uid == CARD_UID )
    {
        *p_index = 0;
        status = 0;
    }

    return status;
}

int32_t _axi_card_hotplug( uint64_t *uid_list,
                            uint8_t *p_nr_uids,
                            uint64_t no_probe_uids[],
                            uint8_t nr_no_probe_uids )
{
    int32_t status = 0;

    // Note: we're assuming that we're running on the Zynq itself, so we should
    // only ever detect at most 1 card when probing this interface
    *p_nr_uids = 0;

    /*
      If the number of "no probe" UIDs is zero, it means that the 1 card may still be out there, so
      probe for it.

      If the number of "no probe" UIDs is non-zero, then there's currently nothing left to discover
      since only 1 card can currently exist while running on Zynq
     */
    if ( nr_no_probe_uids == 0 )
    {
        status = _open_axi_reg_iface();
        if ( status == 0 )
        {
            // don't care what the xport UID is since we only ever support one...just set subclass
            // if we ever support multi-card with this xport, we'll need to update this
            uid_list[0] = CARD_UID;
            *p_nr_uids = 1;

            // close the interface since we're just probing
            status = _close_axi_reg_iface();
        }
        else if ( status == -ENOENT )
        {
            /* not finding the AXI register interface is okay, just find zero cards, there are
             * potentially other custom transports that could find something */
            status = 0;
        }
    }

    return status;
}

int32_t _axi_card_probe( uint64_t uid_list[],
                         uint8_t *p_nr_uids )
{
    return _axi_card_hotplug( uid_list, p_nr_uids, NULL, 0 );
}

int32_t _axi_card_init( skiq_xport_init_level_t level, uint64_t xport_uid )
{
    int32_t status=0;
    skiq_xport_id_t xport_id = SKIQ_XPORT_ID_INITIALIZER;

    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_custom;

    // TODO: handle more than 1 card if we ever need it

    if( (status=_open_axi_reg_iface()) == 0 )
    {
        xport_register_fpga_functions( &xport_id, &axi_xport_fpga_funcs );
        if( skiq_xport_init_level_basic == level )
        {
            xport_unregister_rx_functions( &xport_id );
            xport_unregister_tx_functions( &xport_id );
        }
        else if( skiq_xport_init_level_full == level )
        {
#ifdef USE_IIO
            status = create_rx_thread();
            xport_register_rx_functions( &xport_id, &axi_iio_xport_rx_funcs );
            xport_register_tx_functions( &xport_id, &axi_iio_xport_tx_funcs );
#endif
        }
        else
        {
            status = -1;
        }
    }
    if( status != 0 )
    {
        _axi_card_exit( level, xport_uid );
    }

    return (status);
}

int32_t _axi_card_exit( skiq_xport_init_level_t level, uint64_t xport_uid )
{
    int32_t status=0;
    skiq_xport_id_t xport_id = SKIQ_XPORT_ID_INITIALIZER;

    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_custom;

    if( skiq_xport_init_level_full == level )
    {
#ifdef USE_IIO
        status=destroy_rx_thread();
#endif
    }
    if( status == 0 )
    {
        status = _close_axi_reg_iface();
    }

    /* unregister functions regardless of level */
    xport_unregister_fpga_functions( &xport_id );
    xport_unregister_rx_functions( &xport_id );
    xport_unregister_tx_functions( &xport_id );

    return (status);
}

/* these functions are called by sidekiq_core depending on what transport is
 * chosen by the user. */
skiq_xport_card_functions_t axi_xport_card_funcs = {
    .card_probe     = _axi_card_probe,
    .card_hotplug   = _axi_card_hotplug,
    .card_init      = _axi_card_init,
    .card_exit      = _axi_card_exit,
};
