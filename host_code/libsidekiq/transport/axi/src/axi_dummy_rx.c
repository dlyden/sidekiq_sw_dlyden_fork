/**
 * @file   axi_dummy_rx.c
 *
 * @brief
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
/***** INCLUDES *****/ 

#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>

#include "sidekiq_api.h"
#include "sidekiq_xport_types.h"
#include "axi_transport_private.h"
#include "sidekiq_private.h"

/***** LOCAL FUNCTIONS *****/
static int32_t _axi_dummy_rx_start_streaming( uint64_t xport_uid,
                                            skiq_rx_hdl_t hdl );

static int32_t _axi_iio_rx_stop_streaming( uint64_t xport_uid,
                                           skiq_rx_hdl_t hdl );

static int32_t _axi_dummy_rx_pause_streaming( uint64_t xport_uid );
static int32_t _axi_dummy_rx_resume_streaming( uint64_t xport_uid );

static int32_t _axi_dummy_rx_flush( uint64_t xport_uid );

static int32_t _axi_dummy_rx_receive( uint64_t xport_uid,
                                    uint8_t **pp_data,
                                    uint32_t *p_data_len );
int32_t create_rx_thread(void)
{
    int32_t status=0;

    return (status);
}

int32_t destroy_rx_thread(void)
{
    int32_t status=0;

    return (status);
}

int32_t _axi_dummy_rx_set_buffered( uint64_t xport_uid, bool buffered )
{
    int32_t status=0;
    (void) xport_uid;
    (void) buffered;

    return (status);
}

int32_t _axi_dummy_rx_start_streaming( uint64_t xport_uid,
                                     skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    (void) xport_uid;
    (void) hdl;

    return (status);
}

int32_t _axi_dummy_rx_stop_streaming( uint64_t xport_uid,
                                    skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    (void) xport_uid;
    (void) hdl;

    return (status);
}

int32_t _axi_dummy_rx_pause_streaming( uint64_t xport_uid )
{
    int32_t status=0;
    (void) xport_uid;

    return (status);
}

int32_t _axi_dummy_rx_resume_streaming( uint64_t xport_uid )
{
    int32_t status=0;
    (void) xport_uid;
    
    return (status);
}

int32_t _axi_dummy_rx_flush( uint64_t xport_uid )
{
    int32_t status=0;
    (void) xport_uid;

    return (status);
}

int32_t _axi_dummy_rx_receive( uint64_t xport_uid,
                             uint8_t **pp_data,
                             uint32_t *p_data_len )
{
    int32_t status=0;
    (void) xport_uid;
    (void) **pp_data;
    (void) *p_data_len;

    return (status);
}


static int32_t _axi_dummy_rx_set_transfer_timeout( uint64_t xport_uid,
                                                 const int32_t timeout_us )
{
    int32_t status = 0;
    (void) xport_uid;
    (void) timeout_us;

    return (status);
}


skiq_xport_rx_functions_t axi_iio_xport_rx_funcs = {
    .rx_configure            = NULL,
    .rx_set_block_size       = NULL,
    .rx_set_buffered         = _axi_dummy_rx_set_buffered,
    .rx_start_streaming      = _axi_dummy_rx_start_streaming,
    .rx_stop_streaming       = _axi_dummy_rx_stop_streaming,
    .rx_pause_streaming      = _axi_dummy_rx_pause_streaming,
    .rx_resume_streaming     = _axi_dummy_rx_resume_streaming,
    .rx_flush                = _axi_dummy_rx_flush,
    .rx_set_transfer_timeout = _axi_dummy_rx_set_transfer_timeout,
    .rx_receive              = _axi_dummy_rx_receive,
};