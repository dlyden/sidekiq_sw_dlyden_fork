/**
 * @file axi_dummy_tx.c
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
#include <stdint.h>
#include <inttypes.h>

#include "sidekiq_api.h"
#include "sidekiq_private.h"
#include "sidekiq_xport_api.h"
#include "axi_transport.h"
#include "axi_transport_private.h"
#include "sidekiq_hal.h"

/***** LOCAL FUNCTIONS *****/
int32_t _axi_dummy_tx_initialize( uint64_t xport_uid,
                                skiq_tx_transfer_mode_t tx_transfer_mode,
                                uint32_t num_bytes_to_send,
                                uint8_t num_send_threads,
                                int32_t priority,
                                skiq_tx_callback_t tx_complete_cb );

int32_t _axi_dummy_tx_start_streaming( uint64_t xport_uid,
                                      skiq_tx_hdl_t hdl );

int32_t _axi_dummy_tx_stop_streaming( uint64_t xport_uid,
                                    skiq_tx_hdl_t hdl );

int32_t _axi_dummy_tx_transmit( uint64_t xport_uid,
                              skiq_tx_hdl_t hdl,
                              int32_t *p_samples,
                              void *p_private );

/***** GLOBAL DATA *****/
skiq_xport_tx_functions_t axi_iio_xport_tx_funcs = {
    .tx_initialize      = _axi_dummy_tx_initialize,
    .tx_start_streaming = NULL, 
    .tx_stop_streaming  = _axi_dummy_tx_stop_streaming,
    .tx_transmit        = _axi_dummy_tx_transmit,
};

int32_t _axi_dummy_tx_initialize( uint64_t xport_uid,
                                skiq_tx_transfer_mode_t tx_transfer_mode,
                                uint32_t num_bytes_to_send,
                                uint8_t num_send_threads,
                                int32_t priority,
                                skiq_tx_callback_t tx_complete_cb )
{
    int32_t status=0;
    (void) xport_uid;
    (void) tx_transfer_mode;
    (void) num_bytes_to_send;
    (void) num_send_threads;
    (void) priority;
    (void) tx_complete_cb;

    return (status);
}

int32_t _axi_dummy_tx_stop_streaming( uint64_t xport_uid,
                                    skiq_tx_hdl_t hdl )
{
    int32_t status=0;
    (void) xport_uid;
    (void) hdl;

    return (status);
}

int32_t _axi_dummy_tx_transmit( uint64_t xport_uid,
                              skiq_tx_hdl_t hdl,
                              int32_t *p_samples,
                              void *p_private )
{
    int32_t status=0;
    (void) xport_uid;
    (void) hdl;
    (void) *p_samples;
    (void) *p_private;

    return (status);
}
