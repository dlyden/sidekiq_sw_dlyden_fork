#ifndef __AXI_TRANSPORT_PRIVATE_H__
#define __AXI_TRANSPORT_PRIVATE_H__

/*! \file axi_transport_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include "sidekiq_api.h"

/***** DEFINES *****/
#define AXI_MAX_INDEX (1) // we only ever support 1 card for this transport

/* For the iio_rx_resume() and iio_rx_pause() functions, there's a concept of resuming or pausing
 * ALL streaming activity.  This definition is to be used by the developer to indicate the request
 * should affect all handles */
#define AXI_ALL_RX_HANDLES              skiq_rx_hdl_end

/* transport functions */
extern skiq_xport_fpga_functions_t axi_xport_fpga_funcs;

/***** EXTERN FUNCTIONS  *****/
int32_t _find_axi_index_by_uid( uint64_t uid, uint8_t *p_index );

int32_t _open_axi_reg_iface(void);
int32_t _close_axi_reg_iface(void);

int32_t axi_dmac_irq_reg_fixup(void);

/*********************************************/
/* IIO related functions and definitions     */
#ifdef USE_IIO

// These are the # packets requested per IIO refill call.  In order to maximize
// the throughput, we need to request more than one at a time (high_put).  Balanced
// only requests a single packet at a time.

#ifdef USE_OCM
#define AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED (8)
#else
#define AXI_NUM_PACKETS_IN_IIO_BUFFER_BUFFERED (16)
#endif

#define AXI_NUM_PACKETS_IN_IIO_BUFFER_UNBUFFERED (1)

/* IIO transport functions */
extern skiq_xport_rx_functions_t axi_iio_xport_rx_funcs;
extern skiq_xport_tx_functions_t axi_iio_xport_tx_funcs;

// required version for TX FPGA support
#define FPGA_VERS_AXI_TX_SUPPORT        FPGA_VERSION(3,10,0)
#define FPGA_VERS_AXI_TX_SUPPORT_CSTR   FPGA_VERSION_STR(3,10,0)

/***** EXTERN FUNCTIONS  *****/
int32_t create_rx_thread(void);
int32_t destroy_rx_thread(void);
#endif
/* end USE_IIO                              */
/********************************************/

#endif  /* __AXI_TRANSPORT_PRIVATE_H__ */
