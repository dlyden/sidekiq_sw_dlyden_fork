/**
 * @file   iio_rx_thread.h
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
#include <stdint.h>
#include "iio.h"
#include "sidekiq_types.h"

#ifndef __IIO_RX_THREAD_H__
#define __IIO_RX_THREAD_H__

int32_t init_iio_buffer( uint8_t index );

int32_t init_iio_packet_buffered( uint8_t index, bool buffered );

int32_t iio_rx_set_transfer_timeout( uint8_t index,
                                     const int32_t timeout_us );

int32_t init_iio_rx_thread( uint8_t index );

void* process_iio_rx( void *data );

int32_t iio_rx_thread_get_data( uint8_t index, uint8_t **pp_data, uint32_t *p_data_len );

int32_t iio_rx_pause( uint8_t index, skiq_rx_hdl_t hdl );
int32_t iio_rx_flush( uint8_t index );
int32_t iio_rx_resume( uint8_t index, skiq_rx_hdl_t hdl );

int32_t stop_iio_rx_thread( uint8_t index );

#endif  /* __IIO_RX_THREAD_H__ */
