#ifndef __AXI_TRANSPORT_H__
#define __AXI_TRANSPORT_H__

/*! \file axi_transport.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include "sidekiq_api.h"

/***** DEFINES *****/
#define SUBCLASS_AXI                    'A'

/***** TYPEDEFS *****/

/***** EXTERN DATA  *****/

extern skiq_xport_card_functions_t axi_xport_card_funcs;


/***** EXTERN FUNCTIONS  *****/


#endif  /* __AXI_TRANSPORT_H__ */
