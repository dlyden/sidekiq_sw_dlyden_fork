/**
 * @file   pcie_fpga_registers.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016,2017 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "sidekiq_xport_types.h"
#include "pcie_transport_private.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"

#include <dma_interface_api.h>

#if (!defined __MINGW32__)
#include <pci_manager.h>
#include <sys/ioctl.h>
#endif /* __MINGW32__ */

/* choose the function based on Windows / Linux compile time definitions */
#if (defined __MINGW32__)
#define OS_FUNCTION(_function)        _function ## _WINDOWS
#else
#define OS_FUNCTION(_function)        _function ## _LINUX
#endif

#if (!defined __MINGW32__)

static int
open_and_validate_pci_manager( void )
{
    int fd, status = 0;
    pci_manager_vers_t vers = {
        .maj_vers = 0,
        .min_vers = 0,
        .patch_vers = 0,
    };

    /* open the PCI manager file to do the ioctl calls */
    fd = open(PCI_MANAGER_FULLPATH, O_RDWR);
    if ( fd < 0 )
    {
        fprintf(stderr, "Error: unable to access " PCI_MANAGER_FILENAME " module\n");
        return (-1);
    }

    /* get the driver version and ensure it meets the minimum requirement */
    if ( ioctl(fd, PCI_MANAGER_VERSION, &vers) != 0 )
    {
        fprintf(stderr, "Error: unable to determine " PCI_MANAGER_FILENAME " module version\n");
        status = -2;
    }

    if ( ( status == 0 ) &&
         ( DRIVER_VERSION(vers.maj_vers, vers.min_vers, vers.patch_vers) <
           DRIVER_VERSION(PCI_MANAGER_MAJ_REQ, PCI_MANAGER_MIN_REQ, PCI_MANAGER_SUBMINOR_REQ) ) )
    {
        fprintf(stderr, "Error: " PCI_MANAGER_FILENAME " version does not meet minimum requirement\n");
        status = -3;
    }

    /* close file descriptor if there was an error */
    if ( status != 0 )
    {
        close(fd);
    }

    return fd;
}

#endif  /* (!defined __MINGW32__) */


#if (defined __MINGW32__)

/******************************************************************************/
/** _pcie_card_fpga_down_WINDOWS is responsible for bringing down the PCIe
 * transport under Windows and optionally commanding the FPGA to reload itself
 * when removed from the bus by ways of coordination with the DMA kernel module
 * 
 * @param[in] uid unique ID used to identifier the card at the transport layer
 * @param[in] address flash address / offset of the desired FPGA image to load
 * @param[in] should_reload boolean flag indicating whether or not the FPGA should be reloaded
 * 
 * @return int32_t  status where 0=success, anything else is an error
 */
static int32_t
_pcie_card_fpga_down_WINDOWS( uint64_t uid,
                              uint32_t address,
                              bool should_reload )
{
    int32_t status = 0;

    /* @todo figure out how self-reload fits into Windows driver */
    if ( pcie_transport_init_level == skiq_xport_init_level_basic )
    {
        DmaInterface_close( uid );
    }
    else
    {
        DmaInterfaceShutdown( uid );
    }

    return (status);

} /* _pcie_card_fpga_down_WINDOWS */

#else  /* __MINGW32__ */

/******************************************************************************/
/** _pcie_card_fpga_down_LINUX is responsible for bringing down the PCIe transport
 * under Linux and optionally commanding the FPGA to reload itself when removed
 * from the bus by ways of coordination with the DMA kernel module
 * 
 * @param[in] uid unique ID used to identifier the card at the transport layer
 * @param[in] address flash address / offset of the desired FPGA image to load
 * @param[in] should_reload boolean flag indicating whether or not the FPGA should be reloaded
 * 
 * @return int32_t  status where 0=success, anything else is an error
 */
static int32_t
_pcie_card_fpga_down_LINUX( uint64_t uid,
                            uint32_t address,
                            bool should_reload )
{
    int32_t status = 0;
    int32_t fd;
    pci_manager_bus_t bus;

    /* open PCI Manager and validate version */
    fd = open_and_validate_pci_manager();
    if ( fd < 0 )
    {
        return fd;
    }

    /* after kernel module and version checks, prepare the FPGA reload if needed */
    if ( should_reload )
    {
        status = DmaInterfacePrepareFPGAReload( (uint8_t)uid, address );
        if ( status != 0 )
        {
            close( fd );
            return status;
        }
    }

    /* disconnect the DMA interface based on transport init level */
    if ( pcie_transport_init_level == skiq_xport_init_level_basic )
    {
        DmaInterface_close( uid );
    }
    else
    {
        DmaInterfaceShutdown( uid );
    }

    /* send the ioctl to stop and remove */
    bus.pci_bus = pcie_transport_pci_bus[uid].pci_bus_num;
    bus.pci_domain = pcie_transport_pci_bus[uid].pci_domain;
    bus.pci_devfn = pcie_transport_pci_bus[uid].pci_devfn;
    status = ioctl( fd, PCI_MANAGER_STOP_AND_REMOVE_IOCTL, &bus );

    /* disable the FPGA reload if status indicates that something went wrong and
     * it was previously prepared (i.e. should_reload == true) */
    if ( should_reload && ( status != 0 ) )
    {
        DmaInterfaceDisableFPGAReload( (uint8_t)uid );
    }

    // close the descriptor
    close(fd);

    return (status);

} /* _pcie_card_fpga_down_LINUX */

#endif  /* __MINGW32__ */


#if (defined __MINGW32__)

/******************************************************************************/
/** _pcie_card_fpga_up_WINDOWS is responsible for bringing up the PCIe transport
 * under Windows
 * 
 * @param[in] uid unique ID used to identifier the card at the transport layer
 * 
 * @return int32_t  status where 0=success, anything else is an error
 */
static int32_t
_pcie_card_fpga_up_WINDOWS( uint64_t uid )
{
    int32_t status=0;
    int tryCount = 0;

    do
    {
        tryCount++;

        // TODO this should wait for the WM_DEVICE_ARRIVAL notification
        // wait on an event object that is signaled by the PnP message
        // WINLIB-8
        hal_nanosleep(500000 * MICROSEC);

        // make sure there's a driver entry for the card
        status = DmaInterface_open(uid);
        if ( status == DMA_INTERFACE_STATUS_SUCCESSFUL )
        {
            DmaInterface_close(uid);
        }

    } while ((status == DMA_INTERFACE_STATUS_SUCCESSFUL) && (tryCount < 6));

    if (status == DMA_INTERFACE_STATUS_SUCCESSFUL) {
        if ( pcie_transport_init_level == skiq_xport_init_level_basic )
        {
            status = DmaInterface_open(uid);
            if (status != DMA_INTERFACE_STATUS_SUCCESSFUL)
            {
                _skiq_log(SKIQ_LOG_ERROR, "failed to open DMA interface, status %d\n", status);
            }
        }
        else
        {
            status = DmaInterfaceInit(uid);
            if (status != DMA_INTERFACE_STATUS_SUCCESSFUL)
            {
                _skiq_log(SKIQ_LOG_ERROR, "failed to initialize DMA interface, status %d\n", status);
            }
        }
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to open DMA interface, status %d (attempt %d)\n", status, tryCount);
    }
    return (status);

} /* _pcie_card_fpga_up_WINDOWS */

#else  /* __MINGW32__ */

/******************************************************************************/
/** _pcie_card_fpga_up_LINUX is responsible for bringing up the PCIe transport
 * under Linux
 * 
 * @param[in] uid unique ID used to identifier the card at the transport layer
 * 
 * @return int32_t  status where 0=success, anything else is an error
 */
static int32_t
_pcie_card_fpga_up_LINUX( uint64_t uid )
{
    int32_t status=0;
    int32_t fd;
    pci_manager_bus_t bus;

    /* open PCI Manager and validate version */
    fd = open_and_validate_pci_manager();
    if ( fd < 0 )
    {
        return fd;
    }

    /* test the FPGA interface to make sure the rescan actually worked */
    bus.pci_bus = pcie_transport_pci_bus[uid].pci_bus_num;
    bus.pci_domain = pcie_transport_pci_bus[uid].pci_domain;
    bus.pci_devfn = pcie_transport_pci_bus[uid].pci_devfn;
    status = ioctl( fd, PCI_MANAGER_FORCE_RESCAN_IOCTL, &bus );
    if ( status == 0 )
    {
        // make sure there's a driver entry for the card
        status = DmaInterface_open( uid );
        if ( status == 0 )
        {
            DmaInterface_close( uid );
        }
    }

    /* connect to the DMA interface based on transport init level */
    if ( status == 0 )
    {
        if ( pcie_transport_init_level == skiq_xport_init_level_basic )
        {
            status = DmaInterface_open( uid );
        }
        else
        {
            status = DmaInterfaceInit( uid );
        }
    }

    /* close the descriptor */
    close(fd);

    return (status);

} /* _pcie_card_fpga_up_LINUX */

#endif  /* __MINGW32__ */


static int32_t
_fpga_down( uint64_t uid )
{
    return OS_FUNCTION(_pcie_card_fpga_down)( uid, 0,
                                              false /* should_reload */ );
}


static int32_t
_fpga_down_with_reload( uint64_t uid,
                        uint32_t address )
{
    return OS_FUNCTION(_pcie_card_fpga_down)( uid, address,
                                              true /* should_reload */ );
}


static int32_t
_fpga_up( uint64_t xport_uid )
{
    return OS_FUNCTION(_pcie_card_fpga_up)( xport_uid );

} /* _fpga_up */


static int32_t
_fpga_reg_read( uint64_t xport_uid,
                uint32_t addr,
                uint32_t* p_data )
{
    int32_t status=0;
    
    status = DmaInterfaceReadRegister( (uint8_t)(xport_uid),
                                       addr,
                                       p_data );

    return (status);
}


static int32_t
_fpga_reg_write( uint64_t xport_uid,
                 uint32_t addr,
                 uint32_t data )
{
    int32_t status=0;
    
    status = DmaInterfaceWriteRegister( (uint8_t)(xport_uid),
                                        addr,
                                        data );

    return (status);
}


skiq_xport_fpga_functions_t pcie_xport_fpga_funcs = {
    .fpga_reg_read = _fpga_reg_read,
    .fpga_reg_write = _fpga_reg_write,
    .fpga_down = _fpga_down,
    .fpga_down_reload = _fpga_down_with_reload,
    .fpga_up = _fpga_up,
    .fpga_reg_read_64 = NULL,
    .fpga_reg_write_64 = NULL,
};
