/**
 * @file   pcie_card.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  2 15:20:37 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef __MINGW32__
#include <sys/ioctl.h>
#endif /* __MINGW32__ */

#include <unistd.h>
#include <inttypes.h>
#include <errno.h>

#include "sidekiq_xport_api.h"
#include "sidekiq_util.h"       /* for u64_is_not_in_list() */
#include "sidekiq_private.h"    /* for _skiq_log */
#include "pcie_transport_private.h"

#include <dma_interface_api.h>  /* for DmaInterface* functions */

#ifndef __MINGW32__
#include <pci_manager.h>
#endif /* __MINGW32__ */

pci_bus_info_t pcie_transport_pci_bus[SKIQ_MAX_NUM_CARDS];
skiq_xport_init_level_t pcie_transport_init_level;


int32_t _pcie_card_exit( skiq_xport_init_level_t level, uint64_t xport_uid );


int32_t _pcie_card_hotplug( uint64_t *uid_list,
                            uint8_t *p_nr_uids,
                            uint64_t no_probe_uids[],
                            uint8_t nr_no_probe_uids )
{
    int32_t status = 0, n;
    uint8_t card_num = 0, nr_uids = 0;

    for ( card_num = 0; ( card_num < SKIQ_MAX_NUM_CARDS ) && ( status == 0 ); card_num++ )
    {
        if ( u64_is_not_in_list( card_num, no_probe_uids, nr_no_probe_uids ) )
        {
            // try opening the card
            n = DmaInterface_open(card_num);
            if ( n == 0 )
            {
                uid_list[nr_uids++] = (uint64_t)(card_num);

                // close the card now that we know it's there
                DmaInterface_close(card_num);
            }
#ifndef __MINGW32__
            else if ( ( n == DMA_INTERFACE_UNSUPPORTED_CMD ) || ( n == DMA_INTERFACE_ERR_INCOMPAT ) )
#else
            else if ( n == DMA_INTERFACE_UNSUPPORTED_CMD )
#endif /* __MINGW32__ */
            {
                /* unsupported command or incompatibility breaks the loop early */
                status = n;
            }
        }
    }

    if ( status == 0 )
    {
        *p_nr_uids = nr_uids;
    }

    return status;
}


int32_t _pcie_card_probe( uint64_t uid_list[],
                          uint8_t *p_nr_uids )
{
    return _pcie_card_hotplug( uid_list, p_nr_uids, NULL, 0 );
}


int32_t _pcie_card_init( skiq_xport_init_level_t level,
                         uint64_t xport_uid )
{
    int32_t status=0;
    skiq_xport_id_t xport_id = SKIQ_XPORT_ID_INITIALIZER;

    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_pcie;

    pcie_transport_init_level = level;

    // clear out the bus info for all the cards
    pcie_transport_pci_bus[xport_uid].pci_bus_num = 0;
    pcie_transport_pci_bus[xport_uid].pci_devfn = 0;
    pcie_transport_pci_bus[xport_uid].pci_domain = 0;

#ifndef __MINGW32__
    /* check that dmadriver.ko is loaded */
    {
        int fd;
        fd = open("/sys/module/dmadriver/initstate", O_RDONLY);
        if ( fd < 0 )
        {
            _skiq_log( SKIQ_LOG_ERROR, "Sidekiq DMA kernel module (dmadriver.ko) not loaded\n" );
            status = -errno;
        }
        else
        {
            close(fd);
        }
    }
#endif /* __MINGW32__ */

    if( level == skiq_xport_init_level_basic )
    {
        status=DmaInterface_open( (uint8_t)(xport_uid) );
    }
    else if( level == skiq_xport_init_level_full )
    {
        status=DmaInterfaceInit( (uint8_t)(xport_uid) );
    }

#ifndef __MINGW32__
    /* the driver version is checked in dma_interface against a minimum
     * version, this section of code is free to require a different, but
     * likely greater, driver version */
    if (status == 0)
    {
        /* save off the PCI info of that card */
        if( (DmaInterface_get_pci_dev_info(xport_uid,
                                           &(pcie_transport_pci_bus[xport_uid].pci_bus_num),
                                           &(pcie_transport_pci_bus[xport_uid].pci_devfn),
                                           &(pcie_transport_pci_bus[xport_uid].pci_domain)) != 0) )
        {
            skiq_error("Unable to get PCI device info at UID 0x%016" PRIx64 "\n", xport_uid);
            status=-1;
        }
    }
#endif

    if ( 0 == status )
    {
        /* register transport functions here for curr_card based on level */
#ifndef __MINGW32__
        if ( skiq_xport_init_level_basic == level )
        {
            xport_register_fpga_functions( &xport_id, &pcie_xport_fpga_funcs );
            xport_unregister_rx_functions( &xport_id );
            xport_unregister_tx_functions( &xport_id );
        }
        else if ( skiq_xport_init_level_full == level )
        {
            xport_register_fpga_functions( &xport_id, &pcie_xport_fpga_funcs );
            xport_register_rx_functions( &xport_id, &pcie_xport_rx_funcs );
            xport_register_tx_functions( &xport_id, &pcie_xport_tx_funcs );
        }
#else
        xport_register_fpga_functions( &xport_id, &pcie_xport_fpga_funcs );
        xport_register_rx_functions( &xport_id, &pcie_xport_rx_funcs );
        xport_register_tx_functions( &xport_id, &pcie_xport_tx_funcs );
#endif
    }

    if( status !=0 )
    {
        // make sure we cleanup anything that was opened
        _pcie_card_exit( level, xport_uid );
    }
    
    return (status);
}

int32_t _pcie_card_exit( skiq_xport_init_level_t level, uint64_t xport_uid )
{
    int32_t status=0;
    skiq_xport_id_t xport_id = SKIQ_XPORT_ID_INITIALIZER;
    
    xport_id.xport_uid = xport_uid;
    xport_id.type = skiq_xport_type_pcie;
    if( level == skiq_xport_init_level_basic )
    {
        DmaInterface_close( (uint8_t)(xport_uid) );
    }
    else if( level == skiq_xport_init_level_full )
    {
        DmaInterfaceShutdown( (uint8_t)(xport_uid) );
    }

    /* unregister functions regardless of level */
    xport_unregister_fpga_functions( &xport_id );
    xport_unregister_rx_functions( &xport_id );
    xport_unregister_tx_functions( &xport_id );

    return (status);
}


/* these functions are called by sidekiq_core depending on what transport is
 * chosen by the user. */
skiq_xport_card_functions_t pcie_xport_card_funcs = {
    .card_probe     = _pcie_card_probe,
    .card_hotplug   = _pcie_card_hotplug,
    .card_init      = _pcie_card_init,
    .card_exit      = _pcie_card_exit,
};
