#ifndef __PCIE_TRANSPORT_H__
#define __PCIE_TRANSPORT_H__

/*! \file pcie_transport.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

/***** INCLUDES *****/

#include "sidekiq_api.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/



/***** EXTERN DATA  *****/

extern skiq_xport_card_functions_t pcie_xport_card_funcs;


/***** EXTERN FUNCTIONS  *****/


#endif  /* __PCIE_TRANSPORT_H__ */
