#! /usr/bin/env python2.7

#
# corrects calibration data by subtracting 6dB from all ref values
#
# Copyright 2018 Epiq Solutions, All Rights Reserved
#

def parse_cal_file(input_fn,fix_gain=False):
    with open(input_fn.format(input_fn)) as fi:
        data = [ d.strip() for d in fi.readlines() ]

    cal = dict()

    table = None
    for d in data:
        if d.strip()[0] == '@':
            table = d.split()[0].strip()[1:]
            if not cal.has_key(table):
                cal[table] = dict([ ('table',[]), ('columns',[])])
            cal[table]['columns'] = d.split()[3:]
        else:
            if table == 'gain':
                d = map(float, d.split())
                if fix_gain:
                    d = [ d[0], d[2] - 20.0 ] + d[2:] # fix gain table
                cal[table]['table'].append( map(str, d) )
            else:
                cal[table]['table'].append( d.split() )
    return cal

def write_cal_file(output_fn,cal_table):
    with open(output_fn,'w') as fo:
        ref,ref_columns,gain,gain_columns = cal_table
        fo.write('@ref {} {} {}\n'.format(len(ref), len(ref_columns), ' '.join(ref_columns)))
        for r in ref:
            fo.write(' '.join(r) + '\n')
        fo.write('@gain {} {} {}\n'.format(len(gain), len(gain_columns), ' '.join(gain_columns)))
        for g in gain:
            fo.write(' '.join(g) + '\n')

def correct_cal(C):
    ref,ref_columns,gain,gain_columns = C
    new_ref = map(lambda x: [ x[0], str(-6.0 + float(x[1])) ], ref)
    return (new_ref,ref_columns,gain,gain_columns)

def write_corrected_cal_file(input_fn):
    if input_fn.split('.')[-1] == 'txt':
        output_fn = '.'.join(input_fn.split('.')[:-1]) + '.corrected.txt'
    else:
        output_fn = input_fn + '.corrected.txt'

    print 'correcting {} and writing to {}'.format(input_fn,output_fn)

    write_cal_file(output_fn,correct_cal(parse_cal_file(input_fn)))

if __name__ == "__main__":
    pass
