#! /usr/bin/env python2.7

#
# converts the well-known calibration_RX.csv to a set of C arrays
#
# Copyright 2017-2018 Epiq Solutions, All Rights Reserved
#

from datetime import datetime
import pytz
import numpy as np
from calibration import *

MAX_NR_BANDS=32
BAND_INDICES = map(lambda x: 'b{}'.format(x), range(MAX_NR_BANDS))
DEFINES = {
    'ref': 'REF_TABLE_NAME',
    'gain': 'GAIN_TABLE_NAME',
    'bands': 'BANDS_TABLE_NAME',
    }

def bands_present(D):
    return filter(lambda k: k in BAND_INDICES, sorted(D.keys()))

def export_cal_table_elements(f, name, handle, port, cal_table):
    if port is None:
        handle_port_name = '{}'.format(handle)
    elif (len(handle) == 1) and (len(port) == 1):
        handle_port_name = '{}_{}'.format(handle[0],port[0])
    else:
        handle_port_name = '{}_{}_{}_{}'.format(handle[0], port[0], handle[1], port[1])

    columns = cal_table['columns']
    measurements = cal_table['table']
    f.write('static double _{}_table_{}_columns[] = {{ {} }};\n\n'.format(name,handle_port_name, ', '.join(columns)))
    f.write('static double _{}_table_{}_rows[] = {{\n'.format(name,handle_port_name))
    for m in measurements:
        f.write('    %s,\n' % m[0])
    f.write('};\n\n')
    f.write('static double _{}_table_{}_measurements[] = {{\n'.format(name,handle_port_name))
    for m in measurements:
        M = map(lambda x: '%-18s' % (x + ','), m[1:])
        if name == "bands":
            f.write('    %s\n' % (' '.join(M).strip()))
        else:
            f.write('    %s    /* %12s Hz */\n' % (' '.join(M), m[0]))
    f.write('};\n\n')

def export_cal_table(f, name, table_name, handle, port, measurements, indent='', named=True):
    if port is None:
        handle_port_name = '{}'.format(handle)
    else:
        handle_port_name = '{}_{}'.format(handle,port)

    if named:
        if name in BAND_INDICES:
            f.write(indent + '.band_refs[{}] = {{\n'.format(name[1:]))
        else:
            f.write(indent + '.{} = {{\n'.format(name))

    f.write(indent + '    .name = {},\n'.format(table_name))
    f.write(indent + '    .nr_cols = {},\n'.format(len(measurements[0][1:])))
    f.write(indent + '    .nr_rows = {},\n'.format(len(measurements)))
    f.write(indent + '    .column_headers = _{}_table_{}_columns,\n'.format(name,handle_port_name))
    f.write(indent + '    .row_headers = _{}_table_{}_rows,\n'.format(name,handle_port_name))
    f.write(indent + '    .measurements = _{}_table_{}_measurements,\n'.format(name,handle_port_name))

    if named:
        f.write(indent + '},\n')

def export_corr_table(f, name, table_name, handle, port, measurements, indent=''):
    export_cal_table(f, name, table_name, handle, port, measurements, indent=indent, named=False)

def export_card_calibration(f, part, tables, file_end):
    f.write('struct card_calibration {}_default_cal = {{\n'.format(part))
    f.write('    .rx_cal_by_hdl_port = {\n')
    for handle,port,cal in tables:
        if port is None:
            f.write('        [skiq_rx_hdl_{}][skiq_rf_port_J1] = {{\n'.format(handle))
        else:
            f.write('        [skiq_rx_hdl_{}][skiq_rf_port_{}] = {{\n'.format(handle,port))

        for k in sorted(cal.keys()):
            export_cal_table(f, k, DEFINES.get(k,'"%s"' % k), handle, port, cal[k]['table'], ' ' * 12)
        f.write('        },\n')
    f.write('    },\n')    
    if file_end:
        f.write('};\n')

def export_card_correction_calibration(f, part, tables):
    f.write('    .rx_cal_corrections_by_hdl_port = {\n')
    for handle,port,cal in tables:
        f.write('        [skiq_rx_hdl_{}][skiq_rf_port_{}] = {{\n'.format(handle[0],port[0]))
        f.write('            .corrections_by_hdl_port = {\n')
        f.write('                [skiq_rx_hdl_{}][skiq_rf_port_{}] = {{\n'.format(handle[1],port[1]))
        for k in sorted(cal.keys()):
            export_corr_table(f, k, DEFINES.get(k,'"%s"' % k), "{}_{}".format(handle[0],port[0]), "{}_{}".format(handle[1],port[1]), cal[k]['table'], ' ' * 16)
        f.write('                },\n')
        f.write('            },\n')
        f.write('        },\n')
        f.write('    },\n')
    f.write('};\n')

def write_card_calibration(input_cal_list, output_f, hw):
    T = list()
    C = list()
    for cal,handle,port in input_cal_list:
        if cal.has_key('ref'):
            export_cal_table_elements(output_f, "ref", [handle], [port], cal['ref'])
            export_cal_table_elements(output_f, "gain", [handle], [port], cal['gain'])
            T.append((handle,port,cal))
        elif cal.has_key('bands'):
            export_cal_table_elements(output_f, "bands", [handle], [port], cal['bands'])
            export_cal_table_elements(output_f, "gain", [handle], [port], cal['gain'])
            map(lambda k: export_cal_table_elements(output_f, k, [handle], [port], cal[k]), bands_present(cal))
            T.append((handle,port,cal))
        elif cal.has_key('corrections'):
            export_cal_table_elements(output_f, "corrections", handle, port, cal['corrections'])
            C.append((handle,port,cal))

    export_card_calibration(output_f, hw, T, (len(C) == 0))
    if(len(C) != 0):
        export_card_correction_calibration(output_f, hw, C)

def write_source_file(input_cal_list, output_fn, hw):
    with open(output_fn, 'w') as f:
        f.write('#include "calibration_private.h"\n\n')
        f.write('/* file generated on {} */\n\n'.format(datetime.now(tz=pytz.utc).isoformat()))
        write_card_calibration(input_cal_list, f, hw)

    print "    output left in '{}'".format(output_fn)

def write_source_file_from_input_files(input_fn_list, output_fn, hw):
    cal_list = []
    for input_fn,handle in input_fn_list:
        ref,ref_columns,gain,gain_columns = parse_cal_file(input_fn)
        cal_list.append((ref,ref_columns,gain,gain_columns,handle))

    write_source_file(cal_list, output_fn, hw)

def average_measurements(cal_list, columns):
    avg= np.average(cal_list,axis=0)
    avg = map(lambda x: map(str,x), avg.tolist())
    return dict([('table',avg),('columns',columns)])

def generate_average_cal(file_list, hdl, port):
    R = []
    G = []
    B = []
    BR = dict()
    brc = dict()
    for f in file_list:
        C = parse_cal_file(f)
        if C.has_key('ref') and C.has_key('gain'):
            R.append(np.array(map(lambda x: map(float,x),C['ref']['table'])))
            G.append(np.array(map(lambda x: map(float,x),C['gain']['table'])))
            rc = C['ref']['columns']
            gc = C['gain']['columns']
        elif C.has_key('gain') and C.has_key('bands'):
            G.append(np.array(map(lambda x: map(float,x),C['gain']['table'])))
            B.append(np.array(map(lambda x: map(float,x),C['bands']['table'])))
            gc = C['gain']['columns']
            bc = C['bands']['columns']

            for k in BAND_INDICES:
                if C.has_key(k):
                    BR[k] = BR.get(k, [])
                    BR[k].append(np.array(map(lambda x: map(float,x),C[k]['table'])))
                    brc[k] = C[k]['columns']

    C = dict()
    if len(R) > 0:
        C['ref'] = average_measurements(R, rc)
        C['gain'] = average_measurements(G, gc)
    else:
        C['gain'] = average_measurements(G, gc)
        C['bands'] = average_measurements(B, bc)
        for k,v in BR.items():
            C[k] = average_measurements(BR[k], brc[k])

    return tuple([C, hdl, port])

def generate_cal_correction(file_list, hdl, port, r_hdl, r_port):
    R = []
    B = []
    BR = dict()
    brc = dict()
    for f in file_list:
        C = parse_cal_file(f)
        if C.has_key('corrections'):
            R.append(np.array(map(lambda x: map(float,x),C['corrections']['table'])))
            rc = C['corrections']['columns']

    C = dict()
    C['corrections'] = average_measurements(R, rc)

    return tuple([C, [hdl, r_hdl], [port, r_port]])

def prefix(D, L):
    return map(lambda x: D + x, L)

if __name__ == "__main__":

    ############################################################################
    # ATTENTION
    #
    # For all platforms, except for Z2 and M.2-2280, we use J1 as the RF port
    # since the port gets "squashed" to J1 on all other platforms
    ############################################################################

    #######################################################################
    # Calibration files for M.2-2280
    #######################################################################
    files_rxA1_J1 = ["calibration_j1_average.txt"]
    files_rxA1_J1 = prefix('../data/M.2-2280/', files_rxA1_J1)
    avg_cal_A1_J1 = generate_average_cal(files_rxA1_J1, 'A1', 'J1')

    files_rxA1_J2 = ["calibration_j2_average.txt"]
    files_rxA1_J2 = prefix('../data/M.2-2280/', files_rxA1_J2)
    avg_cal_A1_J2 = generate_average_cal(files_rxA1_J2, 'A1', 'J2')

    cal_list = [avg_cal_A1_J1,avg_cal_A1_J2]
    write_source_file(cal_list,'../src/calibration_m2_2280_default.c','m2_2280')

    #######################################################################
    # Calibration files for X4
    #######################################################################
    files_rxA1 = ["calibration_rx0_average_revb.txt"]
    files_rxA1 = prefix('../data/X4/', files_rxA1)
    avg_cal_A1 = generate_average_cal(files_rxA1, 'A1', 'J1')

    files_rxA2 = ["calibration_rx1_average_revb.txt"]
    files_rxA2 = prefix('../data/X4/', files_rxA2)
    avg_cal_A2 = generate_average_cal(files_rxA2, 'A2', 'J1')

    files_rxB1 = ["calibration_rx2_average_revb.txt"]
    files_rxB1 = prefix('../data/X4/', files_rxB1)
    avg_cal_B1 = generate_average_cal(files_rxB1, 'B1', 'J1')

    files_rxB2 = ["calibration_rx3_average_revb.txt"]
    files_rxB2 = prefix('../data/X4/', files_rxB2)
    avg_cal_B2 = generate_average_cal(files_rxB2, 'B2', 'J1')

    files_rxC1 = ["calibration_rx4_average_revb.txt"]
    files_rxC1 = prefix('../data/X4/', files_rxC1)
    avg_cal_C1 = generate_average_cal(files_rxC1, 'C1', 'J1')

    files_rxD1 = ["calibration_rx5_average_revb.txt"]
    files_rxD1 = prefix('../data/X4/', files_rxD1)
    avg_cal_D1 = generate_average_cal(files_rxD1, 'D1', 'J1')

    cal_list = [avg_cal_A1,avg_cal_A2,avg_cal_B1,avg_cal_B2,avg_cal_C1,avg_cal_D1]
    write_source_file(cal_list,'../src/calibration_x4_default.c','x4')

    #######################################################################
    # Calibration files for X2
    #######################################################################
    files_rxA1 = ["calibration_rx0_7T3D_2018-01-30_17-10-30.660691.txt"]
    files_rxA1 = prefix('../data/X2/', files_rxA1)
    avg_cal_A1 = generate_average_cal(files_rxA1, 'A1', 'J1')

    files_rxA2 = ["calibration_rx1_7T3D_2018-01-30_17-10-30.660691.txt"]
    files_rxA2 = prefix('../data/X2/', files_rxA2)
    avg_cal_A2 = generate_average_cal(files_rxA2, 'A2', 'J1')

    files_rxB1 = ["calibration_rx2_7T3D_2018-01-30_17-10-30.660691.txt"]
    files_rxB1 = prefix('../data/X2/', files_rxB1)
    avg_cal_B1 = generate_average_cal(files_rxB1, 'B1', 'J1')

    cal_list = [avg_cal_A1,avg_cal_A2,avg_cal_B1]
    write_source_file(cal_list,'../src/calibration_x2_default.c','x2')

    # all of the unit calibration files upon which the mPCIe default calibration values are averaged
    files_rxA1 = ["calibration_rx0_30420_2017-11-09_14-04-16.334358.corrected.txt",
                  "calibration_rx0_30421_2017-11-09_15-39-17.942579.corrected.txt",
                  "calibration_rx0_30422_2017-11-10_10-53-56.120623.corrected.txt",
                  "calibration_rx0_30423_2017-11-09_14-21-29.409411.corrected.txt",
                  "calibration_rx0_30424_2017-11-09_15-25-48.028508.corrected.txt",
                  "calibration_rx0_30425_2017-11-10_09-59-20.608881.corrected.txt",
                  "calibration_rx0_30426_2017-11-15_12-19-32.004695.txt",
                  "calibration_rx0_30427_2017-11-09_13-08-35.972283.corrected.txt",
                  "calibration_rx0_30428_2017-11-09_14-58-54.395097.corrected.txt",
                  "calibration_rx0_30429_2017-11-09_13-34-46.654679.corrected.txt",
                  "calibration_rx0_30430_2017-11-09_13-30-24.314967.corrected.txt",
                  "calibration_rx0_30431_2017-11-09_13-17-13.768473.corrected.txt",
                  "calibration_rx0_30432_2017-11-10_10-49-38.814058.corrected.txt",
                  "calibration_rx0_30433_2017-11-09_13-21-23.959209.corrected.txt",
                  "calibration_rx0_30434_2017-11-09_13-43-20.519601.corrected.txt",
                  "calibration_rx0_30435_2017-11-09_13-25-38.838285.corrected.txt",
                  "calibration_rx0_30436_2017-11-09_15-21-12.793373.corrected.txt",
                  "calibration_rx0_30437_2017-11-09_16-03-49.223495.corrected.txt",
                  "calibration_rx0_30438_2017-11-09_13-12-58.203244.corrected.txt",
                  "calibration_rx0_47900_2018-01-17_16-27-39.404368.txt", "calibration_rx0_47901_2018-01-17_16-30-40.487332.txt",
                  "calibration_rx0_47902_2018-01-17_16-33-41.242726.txt", "calibration_rx0_47903_2018-01-17_16-36-51.605189.txt",
                  "calibration_rx0_47904_2018-01-17_16-39-58.612846.txt", "calibration_rx0_47905_2018-01-17_16-43-17.923535.txt",
                  "calibration_rx0_47906_2018-01-17_16-46-27.758901.txt", "calibration_rx0_47907_2018-01-17_16-49-25.244314.txt",
                  "calibration_rx0_47908_2018-01-17_16-52-14.386675.txt", "calibration_rx0_47909_2018-01-17_16-55-24.356843.txt",
                  "calibration_rx0_47910_2018-01-17_16-58-29.472780.txt", "calibration_rx0_47911_2018-01-17_17-01-36.020032.txt",
                  "calibration_rx0_47912_2018-01-17_17-04-38.305940.txt", "calibration_rx0_47913_2018-01-17_17-07-43.195326.txt",
                  "calibration_rx0_47914_2018-01-17_17-10-49.164589.txt", "calibration_rx0_47915_2018-01-17_17-14-00.684919.txt",
                  "calibration_rx0_47916_2018-01-17_17-17-29.060449.txt", "calibration_rx0_47917_2018-01-17_17-20-25.406493.txt",
                  "calibration_rx0_47918_2018-01-17_17-23-30.841998.txt", "calibration_rx0_47919_2018-01-17_17-26-37.418728.txt",
                  "calibration_rx0_47920_2018-01-17_17-29-39.378408.txt", "calibration_rx0_47921_2018-01-17_17-32-40.391149.txt",
                  "calibration_rx0_47922_2018-01-17_17-35-39.700962.txt"]
    files_rxA1 = prefix('../data/mPCIe/', files_rxA1)

    files_rxA2 = ["calibration_rx1_30420_2017-11-09_14-04-16.334358.corrected.txt",
                  "calibration_rx1_30421_2017-11-09_15-39-17.942579.corrected.txt",
                  "calibration_rx1_30422_2017-11-10_10-53-56.120623.corrected.txt",
                  "calibration_rx1_30423_2017-11-09_14-21-29.409411.corrected.txt",
                  "calibration_rx1_30424_2017-11-09_15-25-48.028508.corrected.txt",
                  "calibration_rx1_30425_2017-11-10_09-59-20.608881.corrected.txt",
                  "calibration_rx1_30427_2017-11-09_13-08-35.972283.corrected.txt",
                  "calibration_rx1_30428_2017-11-09_14-58-54.395097.corrected.txt",
                  "calibration_rx1_30429_2017-11-09_13-34-46.654679.corrected.txt",
                  "calibration_rx1_30430_2017-11-09_13-30-24.314967.corrected.txt",
                  "calibration_rx1_30431_2017-11-09_13-17-13.768473.corrected.txt",
                  "calibration_rx1_30432_2017-11-10_10-49-38.814058.corrected.txt",
                  "calibration_rx1_30433_2017-11-09_13-21-23.959209.corrected.txt",
                  "calibration_rx1_30434_2017-11-09_13-43-20.519601.corrected.txt",
                  "calibration_rx1_30435_2017-11-09_13-25-38.838285.corrected.txt",
                  "calibration_rx1_30436_2017-11-09_15-21-12.793373.corrected.txt",
                  "calibration_rx1_30437_2017-11-09_16-03-49.223495.corrected.txt",
                  "calibration_rx1_30438_2017-11-09_13-12-58.203244.corrected.txt"]
    files_rxA2 = prefix('../data/mPCIe/', files_rxA2)

    avg_cal_A1 = generate_average_cal(files_rxA1, 'A1', 'J1')
    avg_cal_A2 = generate_average_cal(files_rxA2, 'A2', 'J1')

    cal_list = [avg_cal_A1,avg_cal_A2]
    write_source_file(cal_list,'../src/calibration_mpcie_default.c','mpcie')

    # all of the unit calibration files upon which the M.2 default calibration values are averaged
    files_rxA1 = ["calibration_rx0_7G18_2017-12-19_12-35-21.941408.txt", "calibration_rx0_7G1A_2017-12-19_10-51-55.367768.txt",
                  "calibration_rx0_7G1B_2017-12-19_13-06-13.993643.txt", "calibration_rx0_7G1D_2017-12-19_13-54-04.478980.txt",
                  "calibration_rx0_7G1E_2017-12-19_10-47-48.721026.txt", "calibration_rx0_7G1H_2017-12-19_12-44-28.107961.txt",
                  "calibration_rx0_7G1M_2017-12-19_12-40-24.366046.txt", "calibration_rx0_7G25_2017-12-19_13-20-41.724441.txt",
                  "calibration_rx0_7G26_2017-12-18_14-03-20.534775.txt", "calibration_rx0_7G27_2017-12-19_11-01-31.537813.txt",
                  "calibration_rx0_7G2A_2017-12-19_14-06-54.372578.txt", "calibration_rx0_7G2B_2017-12-19_13-16-45.283398.txt",
                  "calibration_rx0_7G2D_2017-12-19_10-02-35.564330.txt", "calibration_rx0_7G2E_2017-12-19_09-24-22.690080.txt",
                  "calibration_rx0_7G2F_2017-12-19_09-34-35.444427.txt", "calibration_rx0_7G2K_2017-12-18_14-19-39.754172.txt",
                  "calibration_rx0_7G2P_2017-12-19_12-23-44.388444.txt", "calibration_rx0_7G2T_2017-12-19_11-23-43.005023.txt",
                  "calibration_rx0_7G2W_2017-12-19_10-22-17.964435.txt", "calibration_rx0_7G2X_2017-12-19_09-20-08.368953.txt",
                  "calibration_rx0_7G2Z_2017-12-19_10-26-56.993408.txt", "calibration_rx0_7G31_2017-12-19_10-10-18.862016.txt",
                  "calibration_rx0_7G34_2017-12-19_11-05-28.797901.txt", "calibration_rx0_7G35_2017-12-19_13-59-57.013055.txt",
                  "calibration_rx0_7G36_2017-12-19_13-47-18.410351.txt", "calibration_rx0_7G37_2017-12-19_10-18-29.106520.txt",
                  "calibration_rx0_7G38_2017-12-19_11-31-52.191355.txt", "calibration_rx0_7G39_2017-12-19_09-58-43.311435.txt",
                  "calibration_rx0_7G3A_2017-12-18_14-15-43.025871.txt", "calibration_rx0_7G3B_2017-12-19_13-24-46.552215.txt",
                  "calibration_rx0_7G3C_2017-12-18_14-11-46.850247.txt", "calibration_rx0_7G3D_2017-12-19_11-27-49.029449.txt",
                  "calibration_rx0_7G44_2017-12-19_13-38-45.648953.txt", "calibration_rx0_7G47_2017-12-19_10-36-44.471274.txt",
                  "calibration_rx0_7G4J_2017-12-19_09-39-00.710106.txt", "calibration_rx0_7G4W_2017-12-19_11-17-32.672838.txt",
                  "calibration_rx0_7G4X_2017-12-19_10-14-12.639188.txt", "calibration_rx0_7G4Y_2017-12-19_13-28-57.621872.txt",
                  "calibration_rx0_7G52_2017-12-18_14-23-34.142414.txt", "calibration_rx0_7G58_2017-12-18_14-44-18.836984.txt",
                  "calibration_rx0_7G5F_2017-12-19_09-50-23.670999.txt", "calibration_rx0_7G5H_2017-12-19_09-54-41.059397.txt",
                  "calibration_rx0_7G5N_2017-12-19_13-42-51.043970.txt", "calibration_rx0_7G5P_2017-12-18_15-30-47.079281.txt",
                  "calibration_rx0_7G5R_2017-12-18_14-07-39.679227.txt", "calibration_rx0_7G5T_2017-12-18_14-35-08.377147.txt",
                  "calibration_rx0_7G5U_2017-12-18_14-39-00.294271.txt", "calibration_rx0_7G5W_2017-12-19_12-48-59.330728.txt",
                  "calibration_rx0_7G5X_2017-12-18_15-03-11.139456.txt", "calibration_rx0_7G5Y_2017-12-18_14-31-18.391872.txt",
                  "calibration_rx0_7G65_2017-12-18_14-51-32.406443.txt", "calibration_rx0_7G6B_2017-12-19_10-56-26.859797.txt",
                  "calibration_rx0_7G6C_2018-01-02_11-55-48.373502.txt", "calibration_rx0_7G6D_2018-01-02_12-00-18.995614.txt",
                  "calibration_rx0_7G6G_2017-12-18_15-19-12.051843.txt", "calibration_rx0_7G6S_2017-12-19_10-43-41.748689.txt",
                  "calibration_rx0_7G6T_2017-12-18_14-27-17.319449.txt", "calibration_rx0_7G6U_2017-12-19_13-12-18.329468.txt",
                  "calibration_rx0_7G6V_2017-12-18_15-23-15.791521.txt", "calibration_rx0_7G6W_2017-12-19_13-33-21.353652.txt",
                  "calibration_rx0_7G6X_2017-12-18_14-47-51.601497.txt", "calibration_rx0_7G6Z_2017-12-19_09-46-08.641264.txt",
                  "calibration_rx0_7H05_2017-11-28_11-52-20.743697.txt", "calibration_rx0_7H06_2017-11-28_11-56-21.405469.txt"]
    files_rxA1 = prefix('../data/M.2/', files_rxA1)

    files_rxA2 = ["calibration_rx1_7B4M_2018-01-12_09-43-45.473961.txt", "calibration_rx1_7B4N_2018-01-12_09-58-39.480000.txt",
                  "calibration_rx1_7B4P_2018-01-12_11-00-26.513694.txt", "calibration_rx1_7B4R_2018-01-12_10-12-54.031378.txt",
                  "calibration_rx1_7B4S_2018-01-12_11-44-01.656300.txt", "calibration_rx1_7B4T_2018-01-12_10-52-40.907310.txt",
                  "calibration_rx1_7B4U_2018-01-12_10-19-41.952181.txt", "calibration_rx1_7B4V_2018-01-12_09-24-40.877319.txt",
                  "calibration_rx1_7B4W_2018-01-12_09-34-34.355080.txt", "calibration_rx1_7B4X_2018-01-12_10-05-26.483884.txt",
                  "calibration_rx1_7B4Y_2018-01-12_09-17-45.085164.txt", "calibration_rx1_7B4Z_2018-01-12_09-00-30.869515.txt"]
    files_rxA2 = prefix('../data/M.2/', files_rxA2)

    avg_cal_A1 = generate_average_cal(files_rxA1, 'A1', 'J1')
    avg_cal_A2 = generate_average_cal(files_rxA2, 'A2', 'J1')

    cal_list = [avg_cal_A1,avg_cal_A2]
    write_source_file(cal_list,'../src/calibration_m2_default.c','m2')

    ##################################################
    # Z2 calibration files
    ##################################################
    files_rxA1_J1 = ["calibration_j1_rx0_8026_2018-04-02_12-16-40.510231.txt"]
    files_rxA1_J1 = prefix('../data/Z2/', files_rxA1_J1)

    files_rxA1_J2 = ["calibration_j2_rx0_8026_2018-04-02_12-16-40.510231.txt"]
    files_rxA1_J2 = prefix('../data/Z2/', files_rxA1_J2)

    files_rxA1_J3 = ["calibration_j3_rx0_8026_2018-04-02_12-16-40.510231.txt"]
    files_rxA1_J3 = prefix('../data/Z2/', files_rxA1_J3)

    files_rxA1_J300 = ["calibration_j300_rx0_8027_2018-04-02_14-15-35.197193.txt"]
    files_rxA1_J300 = prefix('../data/Z2/', files_rxA1_J300)

    avg_cal_A1_J1 = generate_average_cal(files_rxA1_J1, 'A1', 'J1')
    avg_cal_A1_J2 = generate_average_cal(files_rxA1_J2, 'A1', 'J2')
    avg_cal_A1_J3 = generate_average_cal(files_rxA1_J3, 'A1', 'J3')
    avg_cal_A1_J300 = generate_average_cal(files_rxA1_J300, 'A1', 'J300')

    cal_list = [avg_cal_A1_J1,avg_cal_A1_J2,avg_cal_A1_J3,avg_cal_A1_J300]
    write_source_file(cal_list,'../src/calibration_z2_default.c','z2')

    ##################################################
    # NV100 calibration and correction files
    ##################################################
    files_rxA1_J1 = ["calibration_rx0_j1_average.txt"]
    files_rxA1_J1 = prefix('../data/NV100/', files_rxA1_J1)

    files_rxB1_J1 = ["calibration_rx2_j1_average.txt"]
    files_rxB1_J1 = prefix('../data/NV100/', files_rxB1_J1)

    files_rxB1_J2 = ["calibration_rx2_j2_average.txt"]
    files_rxB1_J2 = prefix('../data/NV100/', files_rxB1_J2)

    files_rxA1_B1_J1 = ["correction_a1_j1_b1_revc.txt"]
    files_rxA1_B1_J1 = prefix('../data/NV100/', files_rxA1_B1_J1)

    avg_cal_A1_J1 = generate_average_cal(files_rxA1_J1, 'A1', 'J1')
    avg_cal_B1_J1 = generate_average_cal(files_rxB1_J1, 'B1', 'J1')
    avg_cal_B1_J2 = generate_average_cal(files_rxB1_J2, 'B1', 'J2')
    cal_correction_A1_B1_J1 = generate_cal_correction(files_rxA1_B1_J1, 'A1', 'J1', 'B1', 'J1')

    cal_list = [avg_cal_A1_J1,avg_cal_B1_J1,avg_cal_B1_J2,cal_correction_A1_B1_J1]
    write_source_file(cal_list,'../src/calibration_nv100_default.c','nv100')

    #######################################################################
    # Calibration files for Z3u
    #######################################################################
    files_rxA1_J1 = ["rx0_j1_average_calibration.txt"]
    files_rxA1_J1 = prefix('../data/Z3u/', files_rxA1_J1)
    avg_cal_A1_J1 = generate_average_cal(files_rxA1_J1, 'A1', 'J1')

    files_rxA2_J2 = ["rx1_j2_average_calibration.txt"]
    files_rxA2_J2 = prefix('../data/Z3u/', files_rxA2_J2)
    avg_cal_A2_J2 = generate_average_cal(files_rxA2_J2, 'A2', 'J2')

    cal_list = [avg_cal_A1_J1,avg_cal_A2_J2]
    write_source_file(cal_list,'../src/calibration_z3u_default.c','z3u')

    #######################################################################
    # Calibration files for Z2p
    #######################################################################
    files_rxA1_J1 = ["rx0_j1_average_calibration.txt"]
    files_rxA1_J1 = prefix('../data/Z2p/', files_rxA1_J1)
    avg_cal_A1_J1 = generate_average_cal(files_rxA1_J1, 'A1', 'J1')

    files_rxA1_J2 = ["rx0_j2_average_calibration.txt"]
    files_rxA1_J2 = prefix('../data/Z2p/', files_rxA1_J2)
    avg_cal_A1_J2 = generate_average_cal(files_rxA1_J2, 'A1', 'J2')

    cal_list = [avg_cal_A1_J1,avg_cal_A1_J2]
    write_source_file(cal_list,'../src/calibration_z2p_default.c','z2p')
