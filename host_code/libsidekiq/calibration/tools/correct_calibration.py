#! /usr/bin/env python2.7

#
# corrects calibration data by subtracting 6dB from all ref values
#
# Copyright 2018 Epiq Solutions, All Rights Reserved
#

from calibration import *
import sys

if __name__ == "__main__":

    for arg in sys.argv[1:]:
        write_corrected_cal_file(arg)
