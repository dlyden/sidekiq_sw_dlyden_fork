#ifndef __CALIBRATION_FUNCS_H__
#define __CALIBRATION_FUNCS_H__

/*****************************************************************************/
/** @file calibration_funcs.h
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the function prototypes for all of the functions
    that the calibration layer provides.
*/

/***** INCLUDES *****/

#include <stdint.h>
#include <complex.h>

#include "sidekiq_types.h"
#include "sidekiq_private.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/
typedef struct
{
    int32_t (*card_calibration_load)( uint8_t card,
                                      skiq_part_t part );
    int32_t (*card_calibration_clear)( uint8_t card );
    bool (*card_is_rx_cal_data_present)( uint8_t card,
                                         skiq_rx_hdl_t hdl,
                                         skiq_rf_port_t port );
    int32_t (*card_read_cal_offset_in_dB)( uint8_t card,
                                           skiq_rx_hdl_t hdl,
                                           skiq_rf_port_t port,
                                           uint64_t lo_freq,
                                           uint8_t gain_index,
                                           double programmable_rxfir_gain_dB,
                                           double *p_cal_rx_gain_dB );
    bool (*card_is_iq_complex_cal_data_present)( uint8_t card,
                                                 skiq_rx_hdl_t hdl );
    int32_t (*card_read_iq_cal_complex_parts)( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              uint64_t lo_freq,
                                              int8_t temper_deg_C,
                                              double *p_real,
                                              double *p_imag );
#if (defined ATE_SUPPORT)    
    int32_t (*card_calibration_commit)( uint8_t card,
                                        skiq_part_t part );
    int32_t (*card_calibration_add_rx_by_file)( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                skiq_rf_port_t port,
                                                FILE* fp );
    int32_t (*card_calibration_add_iq_complex_by_file)( uint8_t card,
                                                        skiq_rx_hdl_t hdl,
                                                        FILE* fp );
#endif
} calibration_funcs_t;

/***** EXTERN DATA  *****/
extern calibration_funcs_t local_cal_funcs;
extern calibration_funcs_t rpc_cal_funcs;

/***** EXTERN FUNCTIONS  *****/

#endif  /* __CALIBRATION_FUNCS+H__ */
