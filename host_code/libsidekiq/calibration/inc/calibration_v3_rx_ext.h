#ifndef __CALIBRATION_V3_RX_EXT_H__
#define __CALIBRATION_V3_RX_EXT_H__
/**
 * @file   calibration_v3_rx_ext.h
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 13:43:49 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 *
 * </pre>
 *
 */

/***** INCLUDES *****/

#include "calibration_private.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

int32_t cal_import_table_v3_rx_ext( uint8_t card,
                                    uint8_t *p_data_start,
                                    uint32_t length );

int32_t cal_import_record_v3_rx_ext( uint8_t card,
                                     uint8_t *p_data,
                                     uint32_t length );

#if (defined ATE_SUPPORT)

int32_t cal_export_record_v3_rx_ext( uint8_t card,
                                     uint8_t *p_data,
                                     uint32_t length );

#endif  /* ATE_SUPPORT */

#endif  /* __CALIBRATION_V3_RX_EXT_H__ */
