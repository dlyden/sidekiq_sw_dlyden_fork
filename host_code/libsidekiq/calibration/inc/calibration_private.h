#ifndef __CALIBRATION_PRIVATE_H__
#define __CALIBRATION_PRIVATE_H__

/**
 * @file   calibration_private.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec  6 14:36:07 CST 2017
 *
 * @brief
 *
 * <pre>
 * Copyright 2017-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 *         NOTICE: Calibration data is stored with Little Endian byte ordering
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 *
 * Non-volatile memory is formatted as such (for the first version):
 *
 * version 1: supported for Sidekiq mPCIe and M.2 only
 *            rx gain calibration only per handle, frequencies 47-6000MHz
 *            if handle is 0xFF, that concludes the calibration data.  otherwise handle may be 0, 1, or 2.
 *            not all handles need be present.
 *            frequencies must be monotonically increasing.
 *            receive gain is coded using little endian, IEEE754 8-byte doubles
 *            both RX_REF and RX_GAIN table types must be present in the record to be valid
 *            LENGTH is measured from KEY to FOOTER
 *
 *  <calibration KEY = E4D0 E54D 3961 54CD (8 bytes)>
 *  <version = 01 (1 byte)>
 *  <length = LENGTH (2 bytes)>
 *
 *  <handle = {0,1,2} (1 byte)>
 *  <type = [RX_REF=1|RX_GAIN=2] (1 byte)>
 *  <number of rows = ROWS (1 byte)>
 *  <number of columns = COLS (1 byte)>
 *  [ <gain index (1 byte)> ]{COLS}
 *  [ <frequency in kHz (4 bytes)>, [ <gain (8 byte float)> ]{COLS} ]{ROWS}
 *
 *  <handle = 0xFF (1 byte)>
 *  <calibration FOOTER = 722B 296E EF06 0686 (8 bytes)>
 *
 * version 2: supported for Sidekiq Z2 only
 *            rx gain calibration only per handle, frequencies 47-6000MHz
 *            if handle is 0xFF, that concludes the calibration data.  otherwise handle may be 0, 1, or 2.
 *            must include a valid RF port designator
 *            not all handles need be present.
 *            frequencies must be monotonically increasing.
 *            receive gain is coded using little endian, IEEE754 8-byte doubles
 *            both RX_REF and RX_GAIN table types must be present in the record to be valid
 *            LENGTH is measured from KEY to FOOTER
 *
 *  <calibration KEY = E4D0 E54D 3961 54CD (8 bytes)>
 *  <version = 01 (1 byte)>
 *  <length = LENGTH (2 bytes)>
 *
 *  <handle = {0,1,2} (1 byte)>
 *  <port = [ skiq_rf_port_J1 ... skiq_rf_port_max ] (1 byte)>
 *  <type = [RX_REF=1|RX_GAIN=2] (1 byte)>
 *  <number of rows = ROWS (1 byte)>
 *  <number of columns = COLS (1 byte)>
 *  [ <gain index (1 byte)> ]{COLS}
 *  [ <frequency in kHz (4 bytes)>, [ <gain (8 byte float)> ]{COLS} ]{ROWS}
 *
 *  <handle = 0xFF (1 byte)>
 *  <calibration FOOTER = 722B 296E EF06 0686 (8 bytes)>
 *
 * version 1 and 2 records must be located in their storage BEFORE a version 3 record
 *
 * version 3: secondary location for extensible records
 *            if handle is 0xFF, that concludes the calibration data.  otherwise handle may be between 0 and 5, inclusive.
 *            not all handles need be present.
 *            row_headers must be monotonically increasing.
 *            measurements are coded using little endian, IEEE754 8-byte doubles
 *            software enforces what table types must be present in the record to be valid
 *            LENGTH is measured from KEY to FOOTER
 *
 *  <calibration KEY = E4D0 E54D 3961 54CD (8 bytes)>
 *  <version = 03 (1 byte)>
 *  <length = LENGTH (2 bytes)>
 *  <record type = RTYPE (1 byte)>
 *
 *  <handle = [0 ... 5] (1 byte)>
 *  <port = skiq_rf_port_t enum value (1 byte)>
 *  <table type = TTYPE (1 byte)>
 *  <number of rows = ROWS (1 byte, max 128)>
 *  <number of columns = COLS (1 byte, max 32)>
 *  [ <column header (8 byte float)> ]{COLS}
 *  [ <row header (8 byte float)>, [ <measurement (8 byte float)> ]{COLS} ]{ROWS}
 *
 *  <handle = 0xFF (1 byte)>
 *  <calibration FOOTER = 722B 296E EF06 0686 (8 bytes)>
 *
 *  RECORD_TYPE of IQ_COMPLEX_CAL (1) allows TABLE_TYPEs of RE_CAL (3) and IM_CAL (4)
 *  RECORD_TYPE of RX_EXT_CAL (2) allows TABLE_TYPEs of RX_GAIN, RX_BANDS, RX_BAND_REF_START .. RX_BAND_REF_END
 *
 */


/***** INCLUDES *****/

#include <stdint.h>
#include "sidekiq_types.h"
#include "sidekiq_private.h"
#include "portable_endian.h"    /* https://gist.github.com/panzi/6856583 */

/* enable debug_print and debug_print_plain when DEBUG_CAL_TRACE is defined */
#if (defined DEBUG_CAL_TRACE)
#define DEBUG_PRINT_ENABLED
#define TRACE_ENTER                     do { debug_print("entered function\n"); } while (0)
#define TRACE_EXIT                      do { debug_print("exited function\n"); } while (0)
#define TRACE_EXIT_STATUS(_x)           do { debug_print("exited function with status %d\n", _x); } while (0)
#define TRACE_ARGS(...)                 do { debug_print(__VA_ARGS__); } while (0)
#else
#define TRACE_ENTER                     do { ; } while (0)
#define TRACE_EXIT                      do { ; } while (0)
#define TRACE_EXIT_STATUS(_x)           do { ; } while (0)
#define TRACE_ARGS(...)                 do { ; } while (0)
#endif

/* enable debug_print and debug_print_plain when DEBUG_CAL_TABLE is defined */
#if (defined DEBUG_CAL_TABLE)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/***** DEFINES *****/

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/* ATTENTION: these values cannot change without breaking backward compatibility */
/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
#define MAX_NR_ROWS                     (128)
#define MAX_NR_COLS                     (32)
#define MAX_NR_BANDS_LEGACY             (12) /* for ::skiq_x2 and ::skiq_x4 */
#define MAX_NR_BANDS                    (32)

#define CAL_TABLE_INITIALIZER                   \
    {                                           \
        .name = NULL,                           \
        .nr_cols = 0,                           \
        .nr_rows = 0,                           \
        .column_headers = NULL,                 \
        .row_headers = NULL,                    \
        .measurements = NULL,                   \
    }

#define CARD_CAL_INITIALIZER                                            \
    {                                                                   \
        .rx_cal_by_hdl_port = {                                         \
            [ 0 ... (skiq_rx_hdl_end-1) ][ 0 ... (skiq_rf_port_max-1) ] = RX_CAL_INITIALIZER, \
        },                                                              \
        .iq_complex_cal_by_hdl = {                                      \
            [ 0 ... (skiq_rx_hdl_end-1) ] = IQ_COMPLEX_CAL_INITIALIZER, \
        },                                                              \
    }

#define RX_CAL_INITIALIZER                      \
    {                                           \
        .ref =  CAL_TABLE_INITIALIZER,          \
        .gain = CAL_TABLE_INITIALIZER,          \
        .bands = CAL_TABLE_INITIALIZER,         \
        .band_refs = { [ 0 ... MAX_NR_BANDS-1 ] = CAL_TABLE_INITIALIZER }, \
    }

#define IQ_COMPLEX_CAL_INITIALIZER                 \
    {                                              \
        .re_cal = CAL_TABLE_INITIALIZER,           \
        .im_cal = CAL_TABLE_INITIALIZER,           \
    }

#define REF_TABLE_NAME                  "ref"
#define GAIN_TABLE_NAME                 "gain"
#define BANDS_TABLE_NAME                "bands"
#define RE_CAL_TABLE_NAME               "real"
#define IM_CAL_TABLE_NAME               "imag"

#define CAL_IDENTIFIER                  { 0xE4, 0xD0, 0xE5, 0x4D, 0x39, 0x61, 0x54, 0xCD }
#define CAL_IDENTIFIER_LENGTH           8
#define CAL_FOOTER                      { 0x72, 0x2B, 0x29, 0x6E, 0xEF, 0x06, 0x06, 0x86 }
#define CAL_FOOTER_LENGTH               8
#define CAL_VERSION_V1                  (0x01)
#define CAL_VERSION_V2                  (0x02)
#define CAL_VERSION_V3                  (0x03)
#define CAL_VERSION_LATEST              (CAL_VERSION_V3)
#define CAL_HANDLE_EOF                  (0xFF)
#define CAL_HEADER_LENGTH               ( 8 + 1 + 2 ) /* KEY + VERSION + LENGTH */

/* shorthand for accessing calibration structures by (card,hdl,port,[band]) */
#define RX_CAL(_card,_hdl,_port)              (calibration_by_card[_card].rx_cal_by_hdl_port[_hdl][_port])
#define P_RX_CAL(_card,_hdl,_port)            &(RX_CAL(_card,_hdl,_port))

#define IQ_COMPLEX_CAL(_card,_hdl)            (calibration_by_card[_card].iq_complex_cal_by_hdl[_hdl])
#define P_IQ_COMPLEX_CAL(_card,_hdl)          &(IQ_COMPLEX_CAL(_card,_hdl))

/* These point to gain and reference tables that are used in V1/V2 records.  Sidekiq mPCIe, M.2, and
 * Z2 units can have these tables populated in their NVM storage locations.  These Sidekiq units
 * could also have V3 records, but are expected not to have any receive calibration record types
 * (e.g. something from the future). */
#define CREF(_card,_hdl,_port)                (RX_CAL(_card,_hdl,_port).ref)
#define CGAIN(_card,_hdl,_port)               (RX_CAL(_card,_hdl,_port).gain)
#define P_CREF(_card,_hdl,_port)              &(CREF(_card,_hdl,_port))
#define P_CGAIN(_card,_hdl,_port)             &(CGAIN(_card,_hdl,_port))

/* These point to a different style of receive calibration set that are used in a type of V3 record.
 * Sidekiq X2 units can have these tables populated in their NVM storage location in addition to a
 * 'gain' table.  These Sidekiq units are expected to have an empty V1/V2 calibration record and the
 * 'gain' table would be populated in the V3 record and not the V1/V2 record. */
#define CBANDS(_card,_hdl,_port)              (RX_CAL(_card,_hdl,_port).bands)
#define CBANDREF(_card,_hdl,_port,_band)      (RX_CAL(_card,_hdl,_port).band_refs[_band])
#define P_CBANDS(_card,_hdl,_port)            &(CBANDS(_card,_hdl,_port))
#define P_CBANDREF(_card,_hdl,_port,_band)    &(CBANDREF(_card,_hdl,_port,_band))

/* shorthand for IQ Complex Calibration */
#define CRECAL(_card,_hdl)                    (IQ_COMPLEX_CAL(_card,_hdl).re_cal)
#define CIMCAL(_card,_hdl)                    (IQ_COMPLEX_CAL(_card,_hdl).im_cal)
#define P_CRECAL(_card,_hdl)                  &(CRECAL(_card,_hdl))
#define P_CIMCAL(_card,_hdl)                  &(CIMCAL(_card,_hdl))

/***** TYPEDEFS *****/

typedef double MEAS_TYPE_v2;
typedef uint8_t MEAS_TYPE_COL_v2;
typedef uint32_t MEAS_TYPE_ROW_v2;

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/* ATTENTION: the table_type enum values cannot change without breaking backward compatibility */
/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

/** @brief The table_type is used in a calibration record to denote what table is represented and
 * effectively translates between the table name and a representative value */
enum table_type
{
    rx_ref_table_type = 1,      /** @brief represents the "ref" calibration table */
    rx_gain_table_type = 2,     /** @brief represents the "gain" calibration table */

    /* currently only valid in V3 calibration record format and in record_type ==
     * iq_complex_cal_record_type */
    re_cal_table_type = 3,      /** @brief represents the "re_cal" calibration table */
    im_cal_table_type = 4,      /** @brief represents the "im_cal" calibration table */

    /* currently only valid in V3 calibration record format and in record_type == rx_ext_cal_record_type */
    rx_bands_table_type = 5,    /** @brief represents the "bands" calibration table */

    /** @brief represents the "b0" calibration table */
    rx_band_ref_start_table_type = 6,

    /** @brief represents the "bN" calibration table where N is the maximum number of supported band
     * entries minus 1 */
    rx_band_ref_end_table_type = rx_band_ref_start_table_type + MAX_NR_BANDS - 1,
};

/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/* ATTENTION: the record_type enum values cannot change without breaking backward compatibility */
/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
enum record_type
{
    unknown_record_type = 0,    /* unknown / unsupported record type */
    iq_complex_cal_record_type = 1, /* IQ_COMPLEX_CAL_RTYPE */
    rx_ext_cal_record_type = 2, /* RX_EXT_CAL_RTYPE */
};

/* generic 2-D table of measurements */
struct cal_table
{
    char *name;                 /* this field is also used to indicate if a valid is populated or
                                 * not */
    uint8_t nr_cols, nr_rows;
    double *column_headers;
    double *row_headers;
    double *measurements;
};


/* for a given receiver, calibration has a reference table and a gain table */
struct rx_cal
{
    struct cal_table ref, gain;
    struct cal_table bands, band_refs[MAX_NR_BANDS];
};


/* for a given receiver, IQ complex calibration has an 'real' correction table and a 'imag'
 * correction table for each RX handle */
struct iq_complex_cal
{
    struct cal_table re_cal, im_cal;
};

/**
   Calibration data is typically based solely on the (hdl,port) in question. However, some newer
   products (e.g. NV100), calibration data is affected by the settings of other handles on other RF
   ports. For example, the calibration data for (A1,J1) needs to be corrected if (B1,J1) is
   configured.  Therefore, there is a corrections table defined at:

   hdl = skiq_rx_hdl_A1
   port = skiq_rf_port_J1
   related_hdl = skiq_rx_hdl_B1
   related_port = skiq_rf_port_J1

   card_calibration.rx_cal_corrections_by_hdl_port[hdl][port].corrections_by_hdl_port[related_hdl][related_port]
 */
struct rx_cal_corrections
{
    struct cal_table corrections_by_hdl_port[skiq_rx_hdl_end][skiq_rf_port_max];
};


/* for a given card, there's a receiver calibration per (handle,port) and I/Q complex calibration
 * per (handle) */
struct card_calibration
{
    /* Sidekiq mPCIe and Sidekiq M.2 have calibration indexed by (hdl,J1) and
     * Sidekiq Z2 has calibration indexed by (hdl,port) */
    struct rx_cal rx_cal_by_hdl_port[skiq_rx_hdl_end][skiq_rf_port_max];
    struct rx_cal_corrections rx_cal_corrections_by_hdl_port[skiq_rx_hdl_end][skiq_rf_port_max];
    struct iq_complex_cal iq_complex_cal_by_hdl[skiq_rx_hdl_end];
};


/***** EXTERN DATA  *****/

extern struct card_calibration calibration_by_card[SKIQ_MAX_NUM_CARDS];

extern struct card_calibration mpcie_default_cal;
extern struct card_calibration m2_default_cal;
extern struct card_calibration z2_default_cal;
#if (defined HAS_X2_SUPPORT)
extern struct card_calibration x2_default_cal;
#endif  /* HAS_X2_SUPPORT */
#if (defined HAS_X4_SUPPORT)
extern struct card_calibration x4_default_cal;
#endif  /* HAS_X4_SUPPORT */
extern struct card_calibration m2_2280_default_cal;
extern struct card_calibration nv100_default_cal;
extern struct card_calibration z3u_default_cal;
extern struct card_calibration z2p_default_cal;

/***** INLINE FUNCTIONS  *****/

/**************************************************************************************************/
/* These functions are shorthand for checking whether or not a table type has a non-NULL name
 * reference given a card, hdl, port, and band index */
/**************************************************************************************************/
static inline bool
has_gain( uint8_t card,
          skiq_rx_hdl_t hdl,
          skiq_rf_port_t port )
{
    return ( NULL != CGAIN(card,hdl,port).name );
}

static inline bool
has_ref( uint8_t card,
         skiq_rx_hdl_t hdl,
         skiq_rf_port_t port )
{
    return ( NULL != CREF(card,hdl,port).name );
}

static inline bool
has_bands( uint8_t card,
           skiq_rx_hdl_t hdl,
           skiq_rf_port_t port )
{
    return ( NULL != CBANDS(card,hdl,port).name );
}

static inline bool
has_band_ref( uint8_t card,
              skiq_rx_hdl_t hdl,
              skiq_rf_port_t port,
              uint8_t band )
{
    return ( NULL != CBANDREF(card,hdl,port,band).name );
}

static inline bool
has_recal( uint8_t card,
           skiq_rx_hdl_t hdl )
{
    return ( NULL != CRECAL(card,hdl).name );
}

static inline bool
has_imcal( uint8_t card,
           skiq_rx_hdl_t hdl )
{
    return ( NULL != CIMCAL(card,hdl).name );
}
/**************************************************************************************************/


/***** EXTERN FUNCTIONS  *****/

/* these functions are available for use by the calibration_factory.c
 * implementation to allow production to commit a calibration record to NVM for
 * a given card. */

struct cal_table *card_init_and_alloc_cal_table( struct cal_table *p_cal_table,
                                                  char *name,
                                                  uint8_t nr_rows,
                                                  uint8_t nr_cols );

void card_free_cal_table_elements( struct cal_table *p_cal_table );
void card_free_rx_cal_tables( struct rx_cal *p_rx_cal );
void card_free_iq_complex_cal_tables( struct iq_complex_cal *p_iq_complex_cal );

struct cal_table *card_free_cal_table( struct cal_table *p_cal_table );

/* port will be squashed for mPCIe and M.2 parts */
int32_t card_calibration_add_rx_cal( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     skiq_rf_port_t port,
                                     struct rx_cal *p_rx_cal );

int32_t card_calibration_add_iq_complex_cal( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             struct iq_complex_cal *p_iq_complex_cal );

bool cal_import_parse_cal_header( uint8_t *p_data,
                                  uint32_t data_length,
                                  uint32_t *p_cal_length );

int32_t cal_import_table( uint8_t card,
                          uint8_t nr_rows,
                          uint8_t nr_cols,
                          uint8_t *p_data_start,
                          int32_t length,
                          struct cal_table *p_cal_table,
                          char *name,
                          uint8_t version );

int32_t cal_import_all_tables_in_record( uint8_t card,
                                         uint8_t *p_data,
                                         uint32_t length,
                                         uint8_t version,
                                         enum record_type rtype );

uint8_t *cal_export_array( uint8_t *output,
                           uint32_t *p_output_length,
                           uint8_t array[],
                           uint32_t array_length );

uint8_t *cal_export_uint8( uint8_t *output,
                           uint32_t *p_output_length,
                           uint8_t value );

uint8_t *cal_export_uint16( uint8_t *output,
                            uint32_t *p_output_length,
                            uint16_t value );

uint8_t *cal_export_uint32( uint8_t *output,
                            uint32_t *p_output_length,
                            uint32_t value );

uint8_t *cal_export_double( uint8_t *output,
                            uint32_t *p_output_length,
                            double value );

int32_t cal_export_cal( uint8_t card,
                        uint8_t *p_data,
                        uint32_t length );

int32_t cal_max_nr_bands( uint8_t card );

void debug_hex_dump( uint8_t* p_data,
                     uint32_t length );

void debug_display_table( struct cal_table *ct );


/**************************************************************************************************/
/**
 *  string translation functions (in calibration_cstr.c)
 */
const char *table_type_cstr( enum table_type type );


#endif  /* __CALIBRATION_PRIVATE_H__ */
