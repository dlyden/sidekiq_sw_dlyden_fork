#ifndef __CALIBRATION_STORAGE_H__
#define __CALIBRATION_STORAGE_H__

/**
 * @file   calibration_storage.h
 * @author  <info@epiqsolutions.com>
 * @date   Wed Dec 19 10:46:30 2018
 * 
 * @brief Defines calibration storage information for each Sidekiq part (::skiq_part_t), provides
 * info look-up based on card index.
 * 
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <stdint.h>


/***** DEFINES *****/

#define CAL_STORAGE_MAX_SIZE            (2 << 20) /* 2 megabytes */


/***** TYPEDEFS *****/

enum cal_storage_type
{
    storage_not_supported = 0,
    flash_storage,              /* Sidekiq mPCIe and M.2 */
    eeprom_storage,             /* Sidekiq X2 and X4 */
    file_storage,               /* Sidekiq Z2 */
};

struct cal_storage_block
{
    uint32_t address;
    uint32_t size;
};

struct cal_storage_blocks
{
    uint8_t nr_blocks;
    const struct cal_storage_block *blocks;
};

struct cal_storage_info
{
    union
    {
        /* used for flash_storage and eeprom_storage */
        struct cal_storage_blocks *storage;

        /* used for file_storage */
        struct
        {
            const char *filepath;
            const char *mountpoint;
            uint32_t size;
        };
    };
    enum cal_storage_type type;
};


/***** EXTERN FUNCTIONS  *****/

struct cal_storage_info *cal_get_storage_info( uint8_t card );


#endif  /* __CALIBRATION_STORAGE_H__ */
