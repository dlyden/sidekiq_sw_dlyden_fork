#ifndef __CALIBRATION_LOAD_H__
#define __CALIBRATION_LOAD_H__

/**
 * @file   calibration_load.h
 * @author  <info@epiqsolutions.com>
 * @date   Wed Dec 19 10:46:30 2018
 * 
 * @brief Implements loading calibration data from various storage mediums as indexed by Sidekiq
 * part (::skiq_part_t).
 * 
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <stdint.h>


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

int32_t cal_load_from_storage( uint8_t card,
                               uint8_t **pp_data,
                               uint32_t *p_data_length );


#endif  /* __CALIBRATION_LOAD_H__ */
