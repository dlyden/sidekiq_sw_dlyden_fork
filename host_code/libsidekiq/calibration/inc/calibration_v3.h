#ifndef __CALIBRATION_V3_H__
#define __CALIBRATION_V3_H__
/**
 * @file   calibration_v3.h
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 13:43:49 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 *
 * </pre>
 *
 */

/***** INCLUDES *****/

#include "calibration_private.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


struct cal_table_v3
{
    uint8_t handle;             /* skiq_rx_hdl_t */
    uint8_t port;               /* skiq_rf_port_t */
    uint8_t type;               /* enum table_type */
    uint8_t nr_rows, nr_cols;
    uint8_t measurements[];

} __attribute__ ((packed));


struct cal_record_v3
{
    uint8_t identifier[CAL_IDENTIFIER_LENGTH];
    uint8_t version;
    uint16_t length;
    uint8_t rec_type;           /* enum record_type */
    struct cal_table_v3 cal_table[];

} __attribute__ ((packed));


/***** EXTERN FUNCTIONS  *****/

int32_t cal_import_table_v3( uint8_t card,
                             uint8_t *p_data_start,
                             uint32_t length,
                             enum record_type rtype );

int32_t cal_import_record_v3( uint8_t card,
                              uint8_t *p_data,
                              uint32_t length,
                              enum record_type rtype );

#if (defined ATE_SUPPORT)

uint8_t *cal_export_table_v3( uint8_t *p_data,
                              uint32_t *p_length,
                              struct cal_table *p_cal_table,
                              uint8_t handle,
                              uint8_t port,
                              enum table_type type );

int32_t cal_export_record_v3( uint8_t card,
                              uint8_t *p_data,
                              uint32_t length );

#endif  /* ATE_SUPPORT */

#endif  /* __CALIBRATION_V3_H__ */
