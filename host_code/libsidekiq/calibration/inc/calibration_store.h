#ifndef __CALIBRATION_STORE_H__
#define __CALIBRATION_STORE_H__

/**
 * @file   calibration_store.h
 * @author  <info@epiqsolutions.com>
 * @date   Wed Dec 19 10:46:30 2018
 * 
 * @brief Implements storing calibration data to various storage mediums indexed by Sidekiq part
 * (::skiq_part_t).
 * 
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <stdint.h>


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

int32_t cal_write_to_storage( uint8_t card,
                              uint8_t *p_data,
                              uint32_t nr_bytes );

#endif  /* __CALIBRATION_STORE_H__ */
