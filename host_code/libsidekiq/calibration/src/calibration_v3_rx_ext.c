/**
 * @file   calibration_v3_rx_ext.c
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 12:20:07 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_types.h"
#include "sidekiq_private.h"    /* for *_cstr() */
#include "calibration_v3.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


static bool
validate_band_ref_existance( uint8_t card,
                             skiq_rx_hdl_t hdl,
                             skiq_rf_port_t port,
                             uint8_t nr_bands )
{
    bool is_error = false;
    uint8_t band;

    /* 'bands' table, so make sure b0 .. bN exist, but bN+1 .. bMAX-1 do not exist */
    for ( band = 0; band < nr_bands; band++ )
    {
        if ( !has_band_ref(card,hdl,port,band) )
        {
            is_error = true;
            skiq_warning("missing 'b%u' calibration table for card %u hdl %s port %s\n",
                         band, card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
        }
    }

    for ( band = nr_bands; band < cal_max_nr_bands( card ); band++ )
    {
        if ( has_band_ref(card,hdl,port,band) )
        {
            is_error = true;
            skiq_warning("out-of-bounds 'b%u' calibration table for card %u hdl %s port %s\n",
                         band, card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
        }
    }

    return is_error;
}


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** The cal_import_table_v3_rx_ext() function attempts to populate the local card_calibration
    structure with the calibration record in memory referenced by p_data_start of length 'length'.
    It decodes the calibration record and can populate any and all information in
    card_calibration[card].

    At the conclusion, either card_calibration[card] reflects the calibration record described by
    (p_data_start,length) or card_calibration[card] remains unpopulated and a non-zero status is
    returned.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.

    returns the number of bytes consumed by the table if positive.  if negative, there was a
    unrecoverable error encountered while importing the table.
 */
int32_t
cal_import_table_v3_rx_ext( uint8_t card,
                            uint8_t *p_data_start,
                            uint32_t length )
{
#define NAME_INDEX_LEN 16
    int32_t status;
    struct cal_table_v3 *p_table_v3 = (struct cal_table_v3 *)p_data_start;
    int32_t hdr_length = p_table_v3->measurements - (uint8_t *)p_table_v3;
    struct cal_table *p_cal_table = NULL;

    debug_print("Parsing V3 table\n");
    debug_hex_dump(p_data_start, MIN(128, length));

    if ( length > hdr_length )
    {
        skiq_rf_port_t port;
        enum table_type type;
        skiq_rx_hdl_t hdl;
        uint8_t band_ref_index;
        char *name = NULL, name_index[NAME_INDEX_LEN];

        hdl = (skiq_rx_hdl_t)p_table_v3->handle;
        type = (enum table_type)p_table_v3->type;
        port = (skiq_rf_port_t)p_table_v3->port;

        /* select cal_table based on type */
        if ( hdl < skiq_rx_hdl_end )
        {
            debug_print("Importing table type %s for card %u hdl %s port %s rtype %u\n",
                        table_type_cstr(type), card, rx_hdl_cstr(hdl), skiq_rf_port_string(port),
                        rx_ext_cal_record_type);

            /* An RX extended calibration record may contain 'gain', 'bands', or 'band refs'
             * tables */
            switch (type)
            {
                case rx_gain_table_type:
                    name = GAIN_TABLE_NAME;
                    p_cal_table = P_CGAIN(card,hdl,port);
                    break;

                case rx_bands_table_type:
                    name = BANDS_TABLE_NAME;
                    p_cal_table = P_CBANDS(card,hdl,port);
                    break;

                case rx_band_ref_start_table_type ... rx_band_ref_end_table_type:
                    band_ref_index = (uint8_t)type - (uint8_t)rx_band_ref_start_table_type;
                    name = name_index;
                    snprintf( name_index, NAME_INDEX_LEN, "b%d", band_ref_index );
                    p_cal_table = P_CBANDREF(card,hdl,port,band_ref_index);
                    break;

                default:
                    skiq_error("Unexpected table type (%u) for calibration record type "
                               "(%u) for card %u\n", type, rx_ext_cal_record_type, card);
                    p_cal_table = NULL;
                    break;
            }
        }

        /* generically import the table based on the version.  If successful, add the number of
         * consumed bytes to those consumed locally to decode the hdl,port, and type.  It's okay
         * to pass NULL as p_cal_table as import_table is expected to handle that gracefully. */
        status = cal_import_table( card, p_table_v3->nr_rows, p_table_v3->nr_cols,
                                   p_table_v3->measurements, length - hdr_length, p_cal_table,
                                   name, CAL_VERSION_V3 );
        if ( status >= 0 )
        {
            status += hdr_length;
        }
    }
    else
    {
        /* not enough data to properly decode header let alone the measurements */
        status = -E2BIG;
    }

    return status;
#undef NAME_INDEX_LEN
}


/**************************************************************************************************/
/** The cal_import_record_v3_rx_ext() function performs an import implementation for V3 records
    (Extended Receive Offset), checking that the calibration record is structured correctly
    (verifying that the KEY / IDENTIFIER, VERSION, handles, and FOOTER are present.  It delegates
    the main decoding the cal_import_table_v3_rx_ext() function for table correctness.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.

    @attention This function assumes that p_data is pointing at the first HANDLE field and length
    reflects the remaining number of bytes available to import.  p_data must NOT reference the
    beginning of the calibration record.

 */
int32_t
cal_import_record_v3_rx_ext( uint8_t card,
                             uint8_t *p_data,
                             uint32_t length )
{
    int32_t status = 0;

    debug_print("Parsing calibration record V3 (rx ext) from %p with length %u\n", p_data, length);
    debug_hex_dump(p_data, MIN(128, length));

    /* import tables, then check that necessary tables are present */
    status = cal_import_all_tables_in_record(card, p_data, length, CAL_VERSION_V3,
                                             rx_ext_cal_record_type);

    if ( 0 == status )
    {
        /* import of V3 calibration record was successful, assert that for each (hdl,port) the
         * rx_cal is well defined */
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
            {
                bool is_error = false;

                /**********************************************************************************/
                /*
                  if 'bands' table is absent:
                     assert that b0 .. b11 are absent
                     assert that gain is absent
                  else:
                     assert that b0 .. bN are present and bN+1 .. b11 are absent
                     assert that gain is present
                */
                /**********************************************************************************/
                if ( !has_bands(card, hdl, port) )
                {
                    /* no 'bands' table, so b0 .. b11 cannot exist */
                    is_error = validate_band_ref_existance( card, hdl, port, 0 );

                    /* assert that 'gain' table is absent */
                    if ( has_gain(card, hdl, port) )
                    {
                        is_error = true;
                        skiq_warning("missing 'bands' table, 'gain' calibration table not allowed "
                                     "for card %u hdl %s port %s\n", card, rx_hdl_cstr(hdl),
                                     skiq_rf_port_string(port));
                    }
                }
                else
                {
                    /* 'bands' table, so make sure b0 .. bN exist, but bN+1 .. b11 do not exist */
                    is_error = validate_band_ref_existance( card, hdl, port,
                                                            CBANDS(card,hdl,port).nr_rows );

                    /* assert that 'gain' table is present */
                    if ( !has_gain(card, hdl, port) )
                    {
                        is_error = true;
                        skiq_warning("missing required 'gain' table for card %u hdl %s port %s\n",
                                     card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
                    }
                }

                /* an rx_cal didn't pass muster, depopulate it */
                if ( is_error )
                {
                    card_free_rx_cal_tables( P_RX_CAL(card,hdl,port) );
                    status = -EINVAL;
                }
            }
        }
    }
    else
    {
        /* non-zero status means something went wrong in the import, free all rx_cal structures
         * across (hdl,port) */
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
            {
                card_free_rx_cal_tables( P_RX_CAL(card,hdl,port) );
            }
        }
    }

    /********************************************************************************************/
    /* display what's been discovered - for debug purposes */
    {
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
            {
                uint8_t band;
                debug_print("card %u hdl %s port %s bands\n", card, rx_hdl_cstr(hdl),
                            skiq_rf_port_string(port) );
                debug_display_table( P_CBANDS(card,hdl,port) );

                for ( band = 0; band < CBANDS(card,hdl,port).nr_rows; band++ )
                {
                    debug_print("card %u hdl %s port %s b%u\n", card, rx_hdl_cstr(hdl),
                                skiq_rf_port_string(port), band );
                    debug_display_table( P_CBANDREF(card,hdl,port,band) );
                }

                debug_print("card %u hdl %s port %s gain\n", card, rx_hdl_cstr(hdl),
                            skiq_rf_port_string(port) );
                debug_display_table( P_CGAIN(card,hdl,port) );
            }
        }
    }
    /********************************************************************************************/

    return status;
}



#if (defined ATE_SUPPORT)

/**************************************************************************************************/
/** The cal_export_record_v3_rx_ext_cal() function is responsible for the overall creation of the V3
    calibration record for a given card.  It populates the memory block referenced by p_data for the
    eventual commit to NVM.  It populates the header and footer of the record and delegates creation
    of the tables to cal_export_table_v3().

    NOTE: This function is specific to extended RX calibration data, but cal_export_table_v3 is a
    generic method for exporting a supplied cal_table.
 */
int32_t
cal_export_record_v3_rx_ext( uint8_t card,
                             uint8_t *p_data,
                             uint32_t length )
{
    int32_t status = 0;
    uint8_t identifier[] = CAL_IDENTIFIER, footer[] = CAL_FOOTER;
    uint8_t *p = p_data;
    uint16_t *p_cal_length, cal_length_placeholder = 0;
    uint32_t total_length = length;
    uint8_t rtype_u8 = (uint8_t)rx_ext_cal_record_type;
    skiq_rx_hdl_t hdl;

    p = cal_export_array( p, &length, identifier, ARRAY_SIZE( identifier ) );
    p = cal_export_uint8( p, &length, CAL_VERSION_V3 );
    p_cal_length = (uint16_t *)p;           /* save a reference to the LENGTH field */
    p = cal_export_uint16( p, &length, cal_length_placeholder );
    p = cal_export_uint8( p, &length, rtype_u8 );

    for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
    {
        skiq_rf_port_t port;
        for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
        {
            if ( has_bands(card,hdl,port) )
            {
                uint8_t band;
                p = cal_export_table_v3( p, &length, P_CGAIN(card,hdl,port), hdl, port, rx_gain_table_type );
                p = cal_export_table_v3( p, &length, P_CBANDS(card,hdl,port), hdl, port, rx_bands_table_type );
                for ( band = 0; band < CBANDS(card,hdl,port).nr_rows; band++ )
                {
                    p = cal_export_table_v3( p, &length, P_CBANDREF(card,hdl,port,band), hdl, port,
                                             rx_band_ref_start_table_type + band );
                }
            }
        }
    }

    p = cal_export_uint8( p, &length, CAL_HANDLE_EOF );
    p = cal_export_array( p, &length, footer, ARRAY_SIZE( footer ));

    if ( p == NULL )
    {
        status = 0;
    }
    else
    {
        uint16_t export_length;

        /* number of bytes exported to p_data */
        status = total_length - length;

        /* store calibration data length in LENGTH field after byte re-ordering */
        export_length = (uint16_t)status;
        export_length = htole16(export_length);
        *p_cal_length = export_length;
    }

    return status;
}


#endif  /* ATE_SUPPORT */
