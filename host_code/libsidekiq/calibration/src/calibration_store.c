/**
 * @file   calibration_store.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec 19 10:41:04 CST 2018
 *
 * @brief Implements storing calibration data to various storage mediums indexed by Sidekiq part
 * (::skiq_part_t).
 *
 * <pre>
 * Copyright 2018-2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#if (!defined __MINGW32__)
#include <sys/mount.h>
#endif

#include "calibration.h"
#include "calibration_private.h"
#include "calibration_versions.h"
#include "calibration_storage.h"
#include "calibration_store.h"

#include "sidekiq_private.h"
#include "sidekiq_flash.h"
#include "sidekiq_hal.h"

#include "card_services.h"


/***** DEFINES *****/



/***** TYPE DEFINITIONS *****/



/***** LOCAL DATA *****/



/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
/** storage_write() translates the request into one or more hal_write_eeprom() or flash_write()
    calls according to the part's info and storage

    @param[in] card Sidekiq card index
    @param[in] data array of unsigned 8-bit values to write
    @param[in] nr_bytes number of valid bytes in 'data' array

    @return int32_t
    @retval 0 Success
    @retval -EINVAL calibration information type is invalid for this function
    @retval -EFBIG number of bytes requested to write is too large for destination
    @retval -EPROTO internal error: number of storage blocks indicated is invalid
    @retval -EIO I/O error writing to destination
*/
static int32_t
storage_write( uint8_t card,
               uint8_t data[],
               uint16_t nr_bytes )
{
    struct cal_storage_info *info = cal_get_storage_info(card);
    struct cal_storage_blocks *storage = NULL;
    int32_t status = 0;
    uint32_t total_size = 0;
    uint8_t *p_data = &(data[0]);
    uint16_t i;

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    /* look-up storage, only allow flash_storage or eeprom_storage */
    if ( ( info->type == flash_storage ) || ( info->type == eeprom_storage ) )
    {
        storage = info->storage;
    }
    else
    {
        return -EINVAL;
    }

    if ( storage->nr_blocks == 0 )
    {
        return -EPROTO;
    }

    /* determine total size available */
    for ( i = 0; i < storage->nr_blocks; i++ )
    {
        total_size += storage->blocks[i].size;
    }

    if ( nr_bytes > total_size )
    {
        /* cannot complete request, too much requested for given offset */
        status = -EFBIG;
    }

    if ( info->type == eeprom_storage )
    {
        skiq_info("Storing calibration into EEPROM may take 10-20 seconds for card %" PRIu8 "\n",
                  card);
    }

    for ( i = 0; ( i < storage->nr_blocks ) && ( nr_bytes > 0 ) && (status == 0 ); i++ )
    {
        uint16_t to_write = MIN(nr_bytes, storage->blocks[i].size);

        switch (info->type)
        {
            case eeprom_storage:
                debug_print("Writing to EEPROM at physical address 0x%08X with length %u\n",
                            storage->blocks[i].address, to_write);
                status = card_write_eeprom( card, storage->blocks[i].address, p_data, to_write );
                break;

            case flash_storage:
                debug_print("Writing to Flash at physical address 0x%08X with length %u\n",
                            storage->blocks[i].address, to_write);
                status = flash_write( card, storage->blocks[i].address, p_data, to_write );
                break;

            default:
                status = -EINVAL;
                break;
        }

        if ( status != 0 )
        {
            /* squash return code from hal_write_eeprom or flash_write to a generic I/O error */
            status = -EIO;
        }

        /* remaining number of bytes to write, advance data pointer */
        nr_bytes -= to_write;
        p_data += to_write;
    }

    return status;
}


/**************************************************************************************************/
/** file_write() translates the request into mount-readwrite, mount-readonly, and fwrite() calls
    according to the part's info

    @param[in] card Sidekiq card index
    @param[in] data array of unsigned 8-bit values to write
    @param[in] nr_bytes number of valid bytes in 'data' array

    @return int32_t
    @retval 0 Success
    @retval -EINVAL calibration information type is invalid for this function
    @retval -EFBIG number of bytes requested to write is too large for destination
    @retval -EIO I/O error writing to destination
    @retval -errno error occurred in call to mount() or fwrite(), see man page for possible errors
*/
static int32_t
file_write( uint8_t card,
            uint8_t data[],
            uint16_t nr_bytes )
{
    int32_t status = 0;
    struct cal_storage_info *info = cal_get_storage_info(card);

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    /* only allow file_storage */
    if ( info->type != file_storage )
    {
        return -EINVAL;
    }

    if ( nr_bytes > info->size )
    {
        status = -EFBIG;
    }
    else
    {
        FILE *cal_file = NULL;

        debug_print("Storing calibration into %s (size up to %u) for card %u\n", info->filepath,
                    info->size, card);

#if (!defined __MINGW32__)
        skiq_info("Re-mounting %s read-write for card %u\n", info->mountpoint, card);
        status = mount("", info->mountpoint, "", MS_REMOUNT, NULL);
#else
        skiq_info("Skip re-mounting %s read-write for card %u\n", info->mountpoint, card);
#endif
        if ( status != 0 )
        {
            skiq_error("Failed to re-mount %s read-write for card %u with error \"%s\"\n",
                       info->mountpoint, card, strerror(errno));
            status = -errno;
        }
        else
        {
            cal_file = fopen( info->filepath, "wb" );
            if ( cal_file == NULL )
            {
                /* if there's a problem opening the file, that's an error */
                perror("open:");
                status = -errno;
            }
        }

        if ( ( status == 0 ) && ( cal_file != NULL ) )
        {
            status = fwrite( data, sizeof(uint8_t), nr_bytes, cal_file );
            if ( status == nr_bytes )
            {
                /* wrote all of the expected number of bytes */
                status = 0;
            }
            else if ( status > 0 )
            {
                /* wrote some of the expected number of bytes */
                status = -EIO;
            }
            else
            {
                /* error occurred trying to write data */
                status = -errno;
            }

            fclose( cal_file );
            cal_file = NULL;
        }

        /* after exporting calibration to file, re-mount to read-only regardless of success
         * / failure */
#if (!defined __MINGW32__)
        skiq_info("Re-mounting %s read-only for card %u\n", info->mountpoint, card);
        if ( mount("", info->mountpoint, "", MS_REMOUNT | MS_RDONLY, NULL) != 0 )
        {
            if ( status == 0 )
            {
                status = -errno;
            }
            skiq_error("Failed to re-mount %s read-only for card %u with error \"%s\"\n",
                       info->mountpoint, card, strerror(-status));
        }
#else
        skiq_info("Skip re-mounting %s read-only for card %u\n", info->mountpoint,
                  card);
#endif
    }

    return status;
}


/**************************************************************************************************/
/** cal_write_to_storage() handles storage calibration data using the various storage types for a
    given Sidekiq part.

    @param[in] card Sidekiq card index
    @param[in] p_data uint8_t pointer that references to buffer of calibration data to store
    @param[in] nr_bytes number of bytes in p_data to write to storage

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP Sidekiq part does not currently support storing calibration data
    @retval -EINVAL calibration information type is invalid for this function
    @retval -EFBIG number of bytes requested to write is too large for destination
    @retval -EPROTO internal error: number of storage blocks indicated is invalid
    @retval -EIO I/O error writing to destination
    @retval -errno error occurred in call to mount() or fwrite(), see man page for possible errors
*/
int32_t cal_write_to_storage( uint8_t card,
                              uint8_t *p_data,
                              uint32_t nr_bytes )
{
    int32_t status = 0;
    struct cal_storage_info *info = cal_get_storage_info(card);
    skiq_part_t part = _skiq_get_part( card );

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    switch (info->type)
    {
        case flash_storage:
        case eeprom_storage:
            status = storage_write( card, p_data, nr_bytes );
            break;

        case file_storage:
            status = file_write( card, p_data, nr_bytes );
            break;

        case storage_not_supported:
            skiq_error("Storing calibration not supported for card %u and part %s\n", card,
                       part_cstr(part));
            status = -ENOTSUP;
            break;
        default:
            skiq_error("Unknown calibration storage type for card %u and part %s\n", card,
                       part_cstr(part));
            status = -EINVAL;
            break;
    }

    return status;
}
