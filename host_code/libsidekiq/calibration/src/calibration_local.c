/**
 * @file   calibration.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Nov  9 12:59:58 2017
 *
 * @brief
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 * @brief This file implements all calibration files for
 * a locally running Sidekiq (i.e. not using RPC).
 *
 */

/***** INCLUDES *****/

#include <ctype.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#if (!defined __MINGW32__)
#include <sys/mount.h>
#endif

#include "calibration.h"
#include "calibration_private.h"
#include "calibration_versions.h"

#include "calibration_storage.h"
#include "calibration_load.h"
#include "calibration_store.h"

#include "sidekiq_api.h"
#include "sidekiq_flash.h"
#include "sidekiq_hal.h"
#include "card_services.h"

#include "calibration_funcs.h"

/***** DEFINES *****/

#define LOCK_CAL(_card)                       CARD_LOCK(calibration_mutex,_card)
#define UNLOCK_CAL(_card)                     CARD_UNLOCK(calibration_mutex,_card)


/***** TYPE DEFINITIONS *****/


/***** LOCAL DATA *****/

/* calibration_mutex protects all access (read/write) of calibration_by_card */
static pthread_mutex_t calibration_mutex[SKIQ_MAX_NUM_CARDS] = {
#ifdef __linux
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP,
#elif _WIN32
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER,
#else
#error target OS not supported cleanly!
#endif
};

/* local copy of each card's calibration, struct card_calibration is defined in
 * calibration_private.h and calibration_by_card[i] is protected by
 * calibration_lock[i]. */
ARRAY_WITH_DEFAULTS(struct card_calibration, calibration_by_card, SKIQ_MAX_NUM_CARDS, CARD_CAL_INITIALIZER);


/***** LOCAL FUNCTIONS *****/


#if (defined DEBUG_CAL_TABLE)
/*****************************************************************************/
/** This function prints contents of raw data

    @param p_data: a reference to raw bytes
    @param length: the number of bytes references by p_data
    @return: void
*/
void debug_hex_dump(uint8_t* p_data, uint32_t length)
{
    int i, j;
    uint8_t c;

    for (i = 0; i < length; i += 16)
    {
        /* print offset */
        debug_print("%06X:", i);

        /* print HEX */
        for (j = 0; j < 16; j++)
        {
            if ( ( j % 2 ) == 0 ) debug_print_plain(" ");
            if ( ( j % 8 ) == 0 ) debug_print_plain(" ");
            if ( i + j < length )
                debug_print_plain("%02X", p_data[i + j]);
            else
                debug_print_plain("  ");
        }
        debug_print_plain("    ");

        /* print ASCII (if printable) */
        for (j = 0; j < 16; j++)
        {
            if ( ( j % 8 ) == 0 ) debug_print_plain(" ");
            if ( i + j < length )
            {
                c = p_data[i + j];
                if ( isprint(c) )
                    debug_print_plain("%c", c);
                else
                    debug_print_plain(".");
            }
        }
        debug_print_plain("\n");
    }
}


/**************************************************************************************************/
/**
 * Used when DEBUG_CAL_TABLE is defined, the debug_display_table() function prints the contents of
 * the referenced struct cal_table.
 */
void debug_display_table( struct cal_table *ct )
{
    int32_t row, col;

    if ( ( ct->name != NULL ) && ( ct->nr_rows > 0 ) && ( ct->nr_cols > 0 ) )
    {
        debug_print("name=%s, nr_rows=%u, nr_cols=%u\n", ct->name, ct->nr_rows, ct->nr_cols);

        debug_print("column headers:\n");
        debug_print("          ");
        for ( col = 0; col < ct->nr_cols; col++ )
        {
            debug_print_plain(" %10f", ct->column_headers[col]);
        }
        debug_print_plain("\n");

        for ( row = 0; row < ct->nr_rows; row++ )
        {
            debug_print("%10f ::", ct->row_headers[row]);
            for ( col = 0; col < ct->nr_cols; col++ )
            {
                debug_print_plain(" %10f", ct->measurements[(row*ct->nr_cols) + col]);
            }
            debug_print_plain("\n");
        }
    }
    else
    {
        debug_print("empty\n");
    }

    return;
}
#else  /* DEBUG_CAL_TABLE */
void debug_hex_dump(uint8_t* p_data, uint32_t length) { return; }
void debug_display_table( struct cal_table *ct )      { return; }
#endif  /* DEBUG_CAL_TABLE */


/**************************************************************************************************/
/* For the time being, for any Sidekiq other than the ones listed, effectively "squash" the RF port
 * to J1.
 */
static inline skiq_rf_port_t port_fixup( uint8_t card,
                                         skiq_rx_hdl_t hdl,
                                         skiq_rf_port_t port )
{
    skiq_part_t part = _skiq_get_part( card );

    if ( ( part != skiq_z2 ) &&
         ( part != skiq_m2_2280 ) &&
         ( part != skiq_z2p ) &&
         ( part != skiq_z3u ) &&
         ( part != skiq_nv100 ) )
    {
        /* effectively ignore RF port on these Sidekiq cards */
        port = skiq_rf_port_J1;
    }

    return port;
}


/**************************************************************************************************/
/* The function import_table parses row headers, column headers, and measurements into the provided
 * cal_table reference from binary data starting at p_data_start.  It can handle V1/V2 or V3 tables.
 *
 * @note Returns either the number of bytes "consumed" by the decoding or an negative errno if an
 * unrecoverable decoding error occurred.
 *
 * @retval >=0 consumed that number of bytes starting at p_data_start
 * @retval -E2BIG  the table size exceeds the remaining size of the memory block and cannot be trusted
 * @retval -EINVAL reference to p_cal_table is invalid (NULL)
 * @retval -ENOMEM failed to allocate memory for a calibration table
 * @retval -ERANGE number of rows or columns out-of-range, data is suspect and cannot be further decoded
 */
int32_t cal_import_table( uint8_t card,
                          uint8_t nr_rows,
                          uint8_t nr_cols,
                          uint8_t *p_data_start,
                          int32_t length,
                          struct cal_table *p_cal_table,
                          char *name,
                          uint8_t version )
{
    uint8_t *p_data = p_data_start;
    int32_t status = 0;

    if ( p_cal_table != NULL )
    {
        /* check nr_rows and nr_cols validity */
        debug_print("Table '%s' for card %u has nr_rows=%u nr_cols=%u\n", name, card, nr_rows, nr_cols);
        if ( ( ( nr_rows > 0 ) && ( nr_rows <= MAX_NR_ROWS ) ) &&
             ( ( nr_cols > 0 ) && ( nr_cols <= MAX_NR_COLS ) ) )
        {
            uint8_t row, col, col_sz, row_sz, meas_sz;
            uint32_t table_sz;

            /* container sizes change based on cal_table version */
            if ( version >= CAL_VERSION_V3 )
            {
                col_sz = sizeof(double);
                row_sz = sizeof(double);
                meas_sz = sizeof(double);
            }
            else
            {
                col_sz = sizeof(MEAS_TYPE_COL_v2);
                row_sz = sizeof(MEAS_TYPE_ROW_v2);
                meas_sz = sizeof(MEAS_TYPE_v2);
            }
            table_sz = nr_rows * row_sz + nr_cols * col_sz + nr_rows * nr_cols * meas_sz;

            if ( table_sz > length )
            {
                skiq_error("Calibration table exceeds memory boundaries (%u > %u) on card %u\n",
                           table_sz, length, card);
                status = -E2BIG;
            }

            /* import a calibration table that won't clobber an existing table */
            if ( ( status == 0 ) && ( p_cal_table->name == NULL ) )
            {
                p_cal_table = card_init_and_alloc_cal_table( p_cal_table, name, nr_rows, nr_cols );
                if ( NULL == p_cal_table )
                {
                    skiq_error("Cannot allocate calibration table memory for card %u\n", card);
                    status = -ENOMEM;
                }
                else
                {
                    /* import column headers */
                    for ( col = 0; col < nr_cols; col++ )
                    {
                        if ( version >= CAL_VERSION_V3 )
                        {
                            /* in some versions of le64toh(), the 64-bit integer is being type
                               converted to a 'double', which is not what is desired for this
                               function.  Instead, use a union avoid type conversion / typecasts */
                            union
                            {
                                uint64_t u64;
                                double lf;
                            } col_value;

                            memcpy( &col_value, p_data, sizeof(col_value) );

                            /* perform necessary byte re-ordering */
                            col_value.u64 = le64toh(col_value.u64);

                            p_cal_table->column_headers[col] = col_value.lf;
                            p_data += sizeof(col_value);
                        }
                        else
                        {
                            MEAS_TYPE_COL_v2 col_value;
                            memcpy( &col_value, p_data, sizeof(col_value) );

                            /* MEAS_TYPE_COL_v2 is 1 byte width, so no byte re-ordering */

                            p_cal_table->column_headers[col] = (double)col_value;
                            p_data += sizeof(col_value);
                        }
                    }

                    /* grok rows, first header, then measurements */
                    for (row = 0; row < nr_rows; row++)
                    {
                        /* import row header */
                        if ( version >= CAL_VERSION_V3 )
                        {
                            /* in some versions of le64toh(), the 64-bit integer is being type
                               converted to a 'double', which is not what is desired for this
                               function.  Instead, use a union avoid type conversion / typecasts */
                            union
                            {
                                uint64_t u64;
                                double lf;
                            } row_value;

                            memcpy( &row_value, p_data, sizeof(row_value) );

                            /* perform necessary byte re-ordering */
                            row_value.u64 = le64toh(row_value.u64);

                            p_cal_table->row_headers[row] = row_value.lf;
                            p_data += sizeof(row_value);
                        }
                        else
                        {
                            MEAS_TYPE_ROW_v2 row_value;
                            memcpy( &row_value, p_data, sizeof(row_value) );

                            /* MEAS_TYPE_ROW_v2 is 4 bytes width, perform byte re-ordering */
                            row_value = le32toh(row_value);

                            p_cal_table->row_headers[row] = (double)row_value * 1000.0;
                            p_data += sizeof(row_value);
                        }

                        for ( col = 0; col < nr_cols; col++ )
                        {
                            /* import row's measurements */
                            if ( version >= CAL_VERSION_V3 )
                            {
                                /* in some versions of le64toh(), the 64-bit integer is being type
                                   converted to a 'double', which is not what is desired for this
                                   function.  Instead, use a union avoid type conversion / typecasts */
                                union
                                {
                                    uint64_t u64;
                                    double lf;
                                } value;

                                memcpy( &value, p_data, sizeof(value) );

                                /* perform necessary byte re-ordering */
                                value.u64 = le64toh(value.u64);

                                p_cal_table->measurements[row * nr_cols + col] = value.lf;
                                p_data += sizeof(value);
                            }
                            else
                            {
                                /* in some versions of le64toh(), the 64-bit integer is being type
                                   converted to a 'double', which is not what is desired for this
                                   function.  Instead, use a union avoid type conversion / typecasts */
                                union
                                {
                                    uint64_t u64;
                                    MEAS_TYPE_v2 lf;
                                } value;

                                memcpy( &value, p_data, sizeof(value) );

                                /* MEAS_TYPE_v2 is 8 bytes width, perform byte re-ordering */
                                value.u64 = le64toh(value.u64);

                                p_cal_table->measurements[row * nr_cols + col] = value.lf;
                                p_data += sizeof(value);
                            }
                        }
                    }

                    /* status becomes the number of bytes consumed by this function */
                    status = p_data - p_data_start;
                }
            }
            else
            {
                skiq_error("Skipping a calibration table '%s' that would clobber an existing "
                           "table for card %u", p_cal_table->name, card);

                /* advance past the table */
                status = nr_rows * row_sz + nr_cols * col_sz + nr_rows * nr_cols * meas_sz;
            }
        }
        else
        {
            skiq_error("Number of rows and/or columns in stored"
                       " calibration out-of-range for card %u, nr_rows %u,"
                       "nr_cols %u\n", card, nr_rows, nr_cols);
            status = -ERANGE;
        }
    }
    else
    {
        status = -EINVAL;
    }

    if ( status >= 0 )
    {
        debug_print("Consumed %d bytes while importing '%s' table for card %u\n", status, name, card);
    }

    return status;
}


/**************************************************************************************************/
/** cal_import_parse_cal_header parses a calibration record header to see how big it is
 */
bool cal_import_parse_cal_header( uint8_t *p_data,
                                  uint32_t data_length,
                                  uint32_t *p_cal_length )
{
    int32_t status = 0;
    uint32_t offset = 0;
    uint8_t identifier[] = CAL_IDENTIFIER;
    uint8_t version;

    debug_print("Parsing calibration header\n");
    debug_hex_dump(p_data, MIN(128, data_length));

    /* find and match identifier */
    status = memcmp( p_data + offset, identifier, sizeof( identifier ) );
    if ( status == 0 )
    {
        offset += sizeof( identifier );
    }

    /* find version */
    if ( status == 0 )
    {
        version = *(p_data + offset);
        if ( version > CAL_VERSION_LATEST )
        {
            status = -1;
        }
        else
        {
            offset++;
        }
    }

    /* find cal_length */
    if ( status == 0 )
    {
        uint16_t length;

        memcpy( &length, p_data + offset, sizeof(length) );

        /* perform necessary byte re-ordering and pass back to caller */
        length = le16toh(length);
        *p_cal_length = length;
    }

    return (status == 0);
}


/**************************************************************************************************/
/** The import_tables function imports multiple V1/V2 tables or multiple V3 tables starting at
    'p_data' up to 'length' bytes

    Returns either 0 for success or a negative number to indicate an unrecoverable decoding failure.
 */
int32_t cal_import_all_tables_in_record( uint8_t card,
                                         uint8_t *p_data,
                                         uint32_t length,
                                         uint8_t version,
                                         enum record_type rtype )
{
    int32_t status = 0, n;
    uint32_t offset = 0;
    uint8_t handle;

    /* keep importing tables until completion or decoding indicates malformation */
    while ( ( status == 0 ) && ( length > offset ) )
    {
        /* peek at the handle to see if it's an CAL_HANDLE_EOF */
        handle = *(p_data + offset);
        if ( handle >= skiq_rx_hdl_end )
        {
            uint8_t footer[] = CAL_FOOTER;

            offset += sizeof( handle );
            if ( handle == CAL_HANDLE_EOF )
            {
                if ( length >= (offset + sizeof( footer ) ) )
                {
                    if ( 0 == memcmp( p_data + offset, footer, sizeof( footer ) ) )
                    {
                        debug_print("Calibration record footer found during import for card %u\n", card);
                        status = 0;
                        break;
                    }
                    else
                    {
                        debug_print("Calibration record footer DOES NOT MATCH during import for card %u, failing import\n", card);
                        status = -EINVAL;
                    }
                }
                else
                {
                    debug_print("Calibration record extends past available memory during import for card %u, failing import\n", card);
                    status = -EFAULT;
                }
            }
            else
            {
                debug_print("Invalid handle (%u) decoded during import for card %u, failing import\n", handle, card);
                status = -EPROTO;
            }
        }

        if ( status == 0 )
        {
            /* import a single table based on specified version */
            switch (version)
            {
                case CAL_VERSION_V3:
                    n = cal_import_table_v3( card, p_data + offset, length - offset, rtype );
                    break;

                case CAL_VERSION_V2:
                    n = cal_import_table_v2( card, p_data + offset, length - offset );
                    break;

                case CAL_VERSION_V1:
                    n = cal_import_table_v1( card, p_data + offset, length - offset );
                    break;

                default:
                    n = 0;
                    break;
            }

            if ( n > 0 )
            {
                offset += n;
            }
            else
            {
                status = -EFAULT;
            }
        }
    }

    return status;
}


/* Return the maximum number of "bandref" records based on part.  The Sidekiq X2 and X4 must work
 * with previous libsidekiq releases, so MAX_NR_BANDS_LEGACY must be enforced.  However, M.2-2280
 * can increase that number since there are no prior libsidekiq releases that support it */
int32_t cal_max_nr_bands( uint8_t card )
{
    skiq_part_t part = _skiq_get_part( card );

    return ( ( part == skiq_x2 ) || ( part == skiq_x4 ) ) ? MAX_NR_BANDS_LEGACY : MAX_NR_BANDS;
}


/******************************************************************************/
/** The _import_cal() function performs the entire import implementation, iterating over any
    discovered calibration records and checking that the calibration record is structured correctly
    (verifying that the KEY / IDENTIFIER, VERSION, handles, and FOOTER are present.  It delegates
    the main decoding the _import_table() function for table correctness.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.
 */
static int32_t _import_cal( uint8_t card,
                            uint8_t *p_data,
                            uint32_t length )
{
    int32_t status = 0;
    uint8_t *p_data_ptr = p_data;
    uint16_t record_length = 0;
    uint8_t identifier[] = CAL_IDENTIFIER;
    uint8_t version = CAL_VERSION_V1;
    enum record_type rtype = unknown_record_type;

    debug_print("Importing cal for card %u with data %p and length %u\n", card, p_data_ptr, length);
    debug_hex_dump(p_data_ptr, MIN(128, length));

    /* find and match identifier */
    while ( status == 0 )
    {
        status = memcmp( p_data_ptr, identifier, sizeof( identifier ) );
        if ( status != 0 )
        {
            if ( p_data_ptr == p_data )
            {
                debug_print("Missing calibration identifier in NVM, skipping import on card %u\n", card);
            }
            else
            {
                debug_print("No more calibration identifiers found in NVM on card %u\n", card);
                status = 0;
                break;
            }
        }
        else if ( length >= sizeof( identifier ) + sizeof( version ) + sizeof( record_length ) )
        {
            /* find version */
            uint32_t offset = sizeof( identifier );
            version = *(p_data_ptr + offset);
            offset += sizeof(version);

            record_length = *(typeof(record_length) *)(p_data_ptr + offset);
            offset += sizeof(record_length);

            if ( length >= record_length )
            {
                switch (version)
                {
                    case CAL_VERSION_V3:
                        /* parse record type, then import a V3 record */
                        rtype = *(uint8_t *)(p_data_ptr + offset);
                        offset++;
                        status = cal_import_record_v3( card, p_data_ptr + offset, length - offset,
                                                       rtype );
                        break;

                    case CAL_VERSION_V2:
                        status = cal_import_record_v2( card, p_data_ptr + offset, length - offset,
                                                       version );
                        break;

                    case CAL_VERSION_V1:
                        status = cal_import_record_v1( card, p_data_ptr + offset, length - offset,
                                                       version );
                        break;

                    default:
                        /* The `version` is stored in a product's calibration records, so it can
                         * represent a future product / feature that this libsidekiq version does
                         * not support or know about */
                        skiq_unknown("Unsupported calibration version %u (%02Xh), skipping record "
                                     "on card %u\n", version, version, card);
                        status = -ENOTSUP;
                        break;
                }
            }
            else
            {
                status = -E2BIG;
            }
        }
        else
        {
            status = -E2BIG;
        }

        if ( status == 0 )
        {
            /* if calibration record is successfully parsed, advance past the record and see if
             * there's another one */
            p_data_ptr += record_length;
            length -= record_length;
        }
    }

    return status;
}


/**************************************************************************************************/
/**
 *   performs a standard linear interpolation
 */
static double interpolate( double lookup_value,
                           double x0, double x1,
                           double y0, double y1 )
{
    double slope, value;

    /* perform linear interpolation given lookup_value */
    slope = ( y1 - y0 ) / ( x1 - x0 );
    value =  slope * ( lookup_value - x0 ) + y0;

    return (value);

} /* interpolate */


/**************************************************************************************************/
/**
 * This function takes a struct cal_table and a corresponding row index and linearly interpolates
 * the measurement from the column value.  If the column look-up value is numerically below the
 * first entry in column_headers, the returned value is the first entry in row_entries.  Likewise if
 * the column look-up value is above the last in column_headers, then the last value in row_entries
 * is returned.
 *
 * @return interpolated measurement
 */
static double interpolate_meas_in_row( struct cal_table *p_table,
                                       uint8_t row_index,
                                       double lookup_value )
{
    double value;
    bool found = false;
    double *column_headers = p_table->column_headers;
    double *row_entries = &(p_table->measurements[row_index * p_table->nr_cols]);
    uint8_t nr_cols = p_table->nr_cols;
    int i;

    /* if the lookup_value is less than the first header, then value is returned
     * as the first entry in the row */
    if ( lookup_value <= column_headers[0] )
    {
        value = row_entries[0];
        debug_print("column: [%f] (%f) <= %f (%f) (start of table)\n",
                    lookup_value, value,
                    column_headers[0], row_entries[0]);
    }
    else
    {
        /* find the index 'i' where column_headers[i-1] < lookup_value <= column_headers[i] */
        for ( i = 1; (i < nr_cols) && !found; i++ )
        {
            if ( lookup_value <= column_headers[i] )
            {
                /* lookup_value is between x0 and x1, corresponding gain
                 * measurement in dB is between y0 and y1 */
                found = true;
                value = interpolate( lookup_value,
                                     column_headers[i-1], /* x0 */
                                     column_headers[i],   /* x1 */
                                     row_entries[i-1],    /* y0 */
                                     row_entries[i]       /* y1 */ );

                debug_print("column: %f (%f) < [%f] (%f) <= %f (%f)\n",
                            column_headers[i-1], row_entries[i-1],
                            lookup_value, value,
                            column_headers[i], row_entries[i]);
            }
        }

        if ( !found )
        {
            /* lookup_value must be greater than column_headers[nr_cols-1], so
             * value is corresponding value in row_entries */
            value = row_entries[nr_cols-1];

            debug_print("column: %f (%f) < [%f] (%f) (end of table)\n",
                        column_headers[nr_cols-1], row_entries[nr_cols-1],
                        lookup_value, value);
        }
    }

    debug_print("Interpolated measurement (value %f) is %f in row %u of %s table [%p]\n",
                lookup_value, value, row_index, p_table->name, p_table);

    return value;

} /* interpolate_meas_in_row */


/**************************************************************************************************/
/**
 * This function performs a reverse look-up by finding the first row index of the measurement table
 * where the passed lo_freq lies between the first and second column measurements.
 *
 * @param[in] p_table reference to a calibration table
 * @param[in] lo_freq LO frequency of interest
 * @param[out] p_band_index reference to container for row index that contains the frequency
 *
 * @return 0 indicates success, -1 if table is invalid, -1 if frequency not found
 */
static int32_t lookup_band_in_calibration( struct cal_table *p_table,
                                           double lo_freq,
                                           uint8_t *p_band_index )
{
    int32_t status = 0;

    if ( ( p_table == NULL ) || ( p_table->name == NULL ) )
    {
        /* table doesn't exist? */
        debug_print("Table [%p] is empty or non-existent\n", p_table);
        status = -1;
    }

    if ( status == 0 )
    {
        uint8_t row = 0;

        while ( row < p_table->nr_rows )
        {
            if ( ( p_table->measurements[ row * p_table->nr_cols ] <= lo_freq ) &&
                 ( lo_freq <= p_table->measurements[ row * p_table->nr_cols + 1 ] ) )
            {
                *p_band_index = row;
                break;
            }

            row++;
        }

        if ( row == p_table->nr_rows )
        {
            status = -1;
        }
    }

    return status;

} /* lookup_band_in_calibration */


/******************************************************************************/
/**
 * This function performs a 2-D linear interpolation of the measurements in
 * referenced struct cal_table given a row_value and column_value.
 *
 * Assumes the calibration tables have monotonically increasing row fields.
 *
 * @param[in] p_table reference to a calibration table
 * @param[in] lo_freq_kHz LO frequency of interest (in kHz)
 * @param[in] gain_index RX gain index of interest
 * @param[out] p_gain_dB reference to container for calculated gain in dB
 *
 * @return 0 indicates success, -EINVAL if hdl is invalid
 */
static int32_t lookup_meas_in_calibration( struct cal_table *p_table,
                                           double row_value,
                                           double column_value,
                                           double *p_meas )
{
    int32_t status = 0;
    uint8_t row;

    if ( ( p_table == NULL ) || ( p_table->name == NULL ) )
    {
        /* table doesn't exist? */
        debug_print("Table [%p] is empty or non-existent\n", p_table);
        status = -1;
    }

    if ( 0 == status )
    {
        /* iterate through the calibration table until either the row field becomes non-monotonic
         * (indicating EOT) or the search 'row_value' is numerically between 'entry' and
         * 'entry+1' */
        row = 0;
        while ( ( row < p_table->nr_rows ) && ( row_value > p_table->row_headers[row] ) ) row++;

        if ( row >= p_table->nr_rows )
        {
            /* at the end of the table */
            debug_print("row: %f < %f (end of table)\n", p_table->row_headers[row - 1], row_value);
            *p_meas = interpolate_meas_in_row( p_table, row - 1, column_value );
        }
        else if ( row == 0 )
        {
            /* at the start of the table */
            debug_print("row: %f <= %f (start of table)\n", row_value, p_table->row_headers[0]);
            *p_meas = interpolate_meas_in_row( p_table, 0, column_value );
        }
        else
        {
            /* perform linear interpolation of two linear interpolations.  :) */
            *p_meas = interpolate( row_value,
                                   p_table->row_headers[row-1], /* x0 */
                                   p_table->row_headers[row],   /* x1 */
                                   interpolate_meas_in_row( p_table, row - 1, column_value ), /* y0 */
                                   interpolate_meas_in_row( p_table, row, column_value ) /* y1 */ );

            debug_print("rows: %f < %f <= %f\n", p_table->row_headers[row-1], row_value, p_table->row_headers[row]);
        }
    }

    if ( status == 0 )
    {
        debug_print("Interpolated measurement (%f,%f) is %f in %s table [%p]\n",
                    row_value, column_value, *p_meas, p_table->name, p_table);
    }
    else
    {
        debug_print("Interpolated measurement (%f,%f) is N/A\n", row_value, column_value);
    }

    return status;

} /* lookup_meas_in_calibration */


/******************************************************************************/
/**
 * This function performs a 1-D interpolation of the measurements in the
 * reference calibration_ref_table_RX given an lo_freq.
 *
 * Assumes the calibration tables have monotonically increasing row header fields.
 *
 * @param[in] hdl Sidekiq RX handle of interest
 * @param[in] lo_freq_kHz LO frequency of interest (in kHz)
 * @param[out] p_gain_dB reference to container for calculated gain in dB
 *
 * @return  0 indicates success, -EINVAL if hdl is invalid
 */
static int32_t lookup_ref_in_calibration( struct cal_table *p_table,
                                          double lo_freq_kHz,
                                          double *p_gain_dB )
{
    /* looking up the ref is a simplified case where gain_index doesn't matter
     * since the ref table is 1-D */
    return lookup_meas_in_calibration( p_table, lo_freq_kHz, 0.0, p_gain_dB );
}


/**************************************************************************************************/
/**
 * The table_by_hdl_port_freq_and_type resolves a cal_table by (card_calibration, skiq_rx_hdl_t,
 * skiq_rf_port_t, LO Frequency, table_type).  This function hides the fact that a receive
 * calibration structure could exist in two forms, a (ref,gain) pair or a (bands,band_refs[])
 * look-up.
 */
static struct cal_table * table_by_hdl_port_freq_and_type( struct card_calibration *p_cal,
                                                           skiq_rx_hdl_t hdl,
                                                           skiq_rf_port_t port,
                                                           uint64_t lo_freq,
                                                           enum table_type type )
{
    struct cal_table *p_table = NULL;
    struct rx_cal *p_rx_cal = &(p_cal->rx_cal_by_hdl_port[hdl][port]);
    struct iq_complex_cal *p_iq_complex_cal = &(p_cal->iq_complex_cal_by_hdl[hdl]);

    switch (type)
    {
        case rx_ref_table_type:
            /*
             * determine what cal structure is present:
             *
             * 1) ref and gain tables are present (mPCIe, M.2, Z2)
             *    -> return ref or gain table pointer
             * 2) band, band_ref, and gain tables are present (X2, X4)
             *    -> look up lo_freq in band table, then return band_ref table pointer
             *
             * calling function is expected to look for either a ref or gain table
             */
            if ( ( p_rx_cal->ref.name != NULL ) && ( p_rx_cal->gain.name != NULL ) )
            {
                p_table = &(p_rx_cal->ref);
            }
            else if ( ( p_rx_cal->bands.name != NULL ) && ( p_rx_cal->gain.name != NULL ) )
            {
                int32_t status;
                uint8_t band=0;

                debug_print("Looking up ref table by band (LO freq %" PRIu64 ")\n", lo_freq);
                status = lookup_band_in_calibration( &(p_rx_cal->bands), lo_freq, &band );
                if ( status == 0 )
                {
                    debug_print("LO freq %" PRIu64 " is in band index %u\n", lo_freq, band);
                    p_table = &(p_rx_cal->band_refs[band]);
                }
            }
            break;

        case rx_gain_table_type:
            p_table = &(p_rx_cal->gain);
            break;

        case re_cal_table_type:
            p_table = &(p_iq_complex_cal->re_cal);
            break;

        case im_cal_table_type:
            p_table = &(p_iq_complex_cal->im_cal);
            break;

        default:
            p_table = NULL;
    }

    return p_table;
}


/******************************************************************************/
/**
 * This function resolves a request for a cal_table reference given a
 * (card,hdl,port,type) tuple with either the unit-specific calibration
 * (previously loaded from NVM) or a default calibration table (if available).
 */
static struct cal_table * lookup_cal_table( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            skiq_rf_port_t port,
                                            uint64_t lo_freq,
                                            enum table_type type )
{
    struct cal_table *p_table = NULL;
    skiq_part_t part = _skiq_get_part(card);

    /* try the per-card calibration by (card,hdl,type) */
    p_table = table_by_hdl_port_freq_and_type(&(calibration_by_card[card]), hdl, port, lo_freq, type);

    /* check for calibration table validity */
    if ( ( p_table == NULL ) || ( ( p_table != NULL ) && ( p_table->name == NULL ) ) )
    {
        /* name is NULL, so not a valid table, fall back to default table */
        debug_print("Falling back to default calibration %s (%d) table for card %u, hdl %s port %s\n",
                    table_type_cstr(type), type, card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
        switch (part)
        {
            case skiq_mpcie:
                p_table = table_by_hdl_port_freq_and_type(&mpcie_default_cal, hdl, port, lo_freq, type);
                break;
            case skiq_m2:
                p_table = table_by_hdl_port_freq_and_type(&m2_default_cal, hdl, port, lo_freq, type);
                break;
            case skiq_z2:
                p_table = table_by_hdl_port_freq_and_type(&z2_default_cal, hdl, port, lo_freq, type);
                break;
            case skiq_x2:
#if (defined HAS_X2_SUPPORT)
                p_table = table_by_hdl_port_freq_and_type(&x2_default_cal, hdl, port, lo_freq, type);
#else
                skiq_warning("Support for X2 hardware not available in this library configuration");
#endif  /* HAS_X2_SUPPORT */
                break;
            case skiq_x4:
#if (defined HAS_X4_SUPPORT)
                p_table = table_by_hdl_port_freq_and_type(&x4_default_cal, hdl, port, lo_freq, type);
#else
                skiq_warning("Support for X4 hardware not available in this library configuration");
#endif  /* HAS_X4_SUPPORT */
                break;
            case skiq_m2_2280:
                p_table = table_by_hdl_port_freq_and_type(&m2_2280_default_cal, hdl, port, lo_freq, type);
                break;
            case skiq_nv100:
                p_table = table_by_hdl_port_freq_and_type(&nv100_default_cal, hdl, port, lo_freq, type);
                break;
            case skiq_z3u:
                p_table = table_by_hdl_port_freq_and_type(&z3u_default_cal, hdl, port, lo_freq, type);
                break;
            case skiq_z2p:
                p_table = table_by_hdl_port_freq_and_type(&z2p_default_cal, hdl, port, lo_freq, type);
                break;
            default:
                skiq_warning("No default RX calibration available for card %u, hdl %s port %s\n",
                             card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
                break;
        }
    }
    else if ( p_table != NULL )
    {
        debug_print("Using unit calibration %s (%d) table for card %u, hdl %s port %s\n",
                    table_type_cstr(type), type, card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
    }

    debug_print("Calibration %s table [%p] for card %u, hdl %s port %s\n",
                table_type_cstr(type), p_table, card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
    return p_table;
}


/**************************************************************************************************/
/**
 * The corr_table_by_hdl_port_freq_type resolves a cal_table by (card_calibration, skiq_rx_hdl_t,
 * skiq_rf_port_t).  This function iterates over the card's available receive handles, looks up the
 * configured RF port, then checks rx_cal_corrections_by_hdl_port to see if a corrections table is
 * defined.  The function returns NULL if no table is found, or it returns the first table found.
 */
static struct cal_table * corr_table_by_hdl_port( uint8_t card,
                                                  struct card_calibration *p_cal,
                                                  skiq_rx_hdl_t hdl,
                                                  skiq_rf_port_t port )
{
    int32_t status = 0;
    struct cal_table *p_table = NULL;
    struct rx_cal_corrections *p_rx_cal_corr = &(p_cal->rx_cal_corrections_by_hdl_port[hdl][port]);
    skiq_rx_hdl_t r_hdl;
    skiq_rf_port_t r_port = skiq_rf_port_max;
    uint8_t r_hdl_i = 0;
    uint8_t nr_rx_hdl = _skiq_get_nr_rx_hdl( card );

    for ( r_hdl_i = 0; ( r_hdl_i < nr_rx_hdl ) && ( p_table == NULL ) && ( status == 0 ); r_hdl_i++)
    {
        r_hdl = _skiq_get_rx_hdl( card, r_hdl_i );
        if ( r_hdl == hdl )
        {
            /* skip if r_hdl and hdl are the same because correction tables only care about "other"
             * handles and not the handle of interest */
            continue;
        }
        else if (r_hdl < skiq_rx_hdl_end)
        {
            rfe_t rfe = _skiq_get_rx_rfe_instance( card, r_hdl );
            status = rfe_read_rx_rf_port( &rfe, &r_port );
            if ( status == 0 )
            {
                p_table = &(p_rx_cal_corr->corrections_by_hdl_port[r_hdl][r_port]);
                if ( p_table->name == NULL )
                {
                    /* If the table name is NULL, then it didn't find a valid table so unset
                     * p_table */
                    p_table = NULL;
                }
            }
            else
            {
                skiq_error("Unable to lookup RF port for hdl %s on card %u with status %d",
                           rx_hdl_cstr(r_hdl), card, status);
            }
        }
        else
        {
            status = -EPROTO;
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n",
                       __LINE__, card, nr_rx_hdl, r_hdl_i);
        }
    }

    return p_table;
}


/**************************************************************************************************/
/**
 * This function resolves a request for a correction_table reference given a (card,hdl,port) tuple
 * with the unit-specific corrections where hdl is the hdl and port are being used.
 */
static struct cal_table * lookup_correction_table( uint8_t card,
                                                   skiq_rx_hdl_t hdl,
                                                   skiq_rf_port_t port )
{
    struct cal_table *p_table = NULL;
    skiq_part_t part = _skiq_get_part(card);

    switch(part)
    {
        case skiq_nv100:
            p_table = corr_table_by_hdl_port( card, &nv100_default_cal, hdl, port );
            break;
        default:
            p_table = NULL;
            break;
    }

    return p_table;
}


static bool _is_rx_cal_data_present_unlocked( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              skiq_rf_port_t port )
{
    bool is_present = false;

    /* perform the port fix-up based on card and handle */
    port = port_fixup(card, hdl, port);

    debug_print("checking if rx cal data is present for card %u hdl %u port %u\n", card, hdl, port);
    if ( ( port >= skiq_rf_port_J1 ) && ( port < skiq_rf_port_max ) )
    {
        /* check to see if (card,hdl,port) is populated */
        if ( ( has_ref(card,hdl,port) ) && ( has_gain(card, hdl, port) ) )
        {
            is_present = true;
        }
        else if ( ( has_bands(card,hdl,port) ) && ( has_gain(card, hdl, port) ) )
        {
            /* it's enough to just check 'bands' at this point */
            is_present = true;
        }
    }

    return (is_present);
}


static bool is_iq_complex_cal_data_present_unlocked( uint8_t card,
                                                     skiq_rx_hdl_t hdl )
{
    bool is_present = false;

    debug_print("checking if IQ complex cal data is present for card %u hdl %u\n", card, hdl);

    /* check to see if (card,hdl) is populated */
    if ( has_recal(card,hdl) && has_imcal(card,hdl) )
    {
        is_present = true;
    }

    return (is_present);
}


/***** GLOBAL FUNCTIONS *****/


static bool _card_is_rx_cal_data_present( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          skiq_rf_port_t port )
{
    bool is_present = false;

    LOCK_CAL(card);
    {
        is_present = _is_rx_cal_data_present_unlocked( card, hdl, port );
    }
    UNLOCK_CAL(card);

    return (is_present);
}


static bool _card_is_iq_complex_cal_data_present( uint8_t card,
                                                  skiq_rx_hdl_t hdl )
{
    bool is_present = false;

    LOCK_CAL(card);
    {
        is_present = is_iq_complex_cal_data_present_unlocked( card, hdl );
    }
    UNLOCK_CAL(card);

    return (is_present);
}


/******************************************************************************/
/**
 * This function initializes a struct cal_table and allocates measurement memory
 * based on the provided number of rows and number of columns.  If the provided
 * p_cal_table is NULL, this function will also allocate memory for the struct
 * cal_table.
 */
struct cal_table * card_init_and_alloc_cal_table( struct cal_table *p_cal_table,
                                                  char *name,
                                                  uint8_t nr_rows,
                                                  uint8_t nr_cols )
{
    bool cal_table_allocd = false;
    uint32_t size_allocd = 0;

    /* allocate p_cal_table IFF provided pointer is NULL */
    if ( p_cal_table == NULL )
    {
        cal_table_allocd = true;
        p_cal_table = calloc( sizeof( struct cal_table ), 1 );
        size_allocd += sizeof( struct cal_table ) * 1;
        if ( p_cal_table == NULL )
            goto no_free;
    }

    size_allocd += sizeof( double ) * nr_cols;
    p_cal_table->column_headers = calloc( sizeof( double ), nr_cols );
    if ( p_cal_table->column_headers == NULL )
        goto free_cal_table;

    size_allocd += sizeof( double ) * nr_rows;
    p_cal_table->row_headers = calloc( sizeof( double ), nr_rows );
    if ( p_cal_table->row_headers == NULL )
        goto free_column_headers;

    size_allocd += sizeof( double ) * nr_rows * nr_cols;
    p_cal_table->measurements = calloc( sizeof( double ), nr_rows * nr_cols );
    if ( p_cal_table->measurements == NULL )
        goto free_row_headers;

    /* all items successfully allocated */
    p_cal_table->name = strdup( name );
    p_cal_table->nr_rows = nr_rows;
    p_cal_table->nr_cols = nr_cols;
    goto no_free;

free_row_headers:
    FREE_IF_NOT_NULL( p_cal_table->row_headers );

free_column_headers:
    FREE_IF_NOT_NULL( p_cal_table->column_headers );

free_cal_table:
    if ( cal_table_allocd )
    {
        free( p_cal_table );
    }

    /* Some allocation of the table failed, so let the caller know by returning
     * NULL.  Returning NULL won't affect a statically allocated table. */
    p_cal_table = NULL;

no_free:
    debug_print("Allocated size of calibration table [%p] is about %u bytes\n", p_cal_table, size_allocd);
    return p_cal_table;
}


/******************************************************************************/
/**
 * This function frees all of the referenced struct cal_table elements
 */
void
card_free_cal_table_elements( struct cal_table *p_cal_table )
{
    if ( p_cal_table != NULL )
    {
        FREE_IF_NOT_NULL( p_cal_table->name );
        FREE_IF_NOT_NULL( p_cal_table->measurements );
        FREE_IF_NOT_NULL( p_cal_table->row_headers );
        FREE_IF_NOT_NULL( p_cal_table->column_headers );
    }
}


/**************************************************************************************************/
/**
 * This function calls card_free_cal_table_elements on all cal_table structures in a struct rx_cal
 */
void card_free_rx_cal_tables( struct rx_cal *p_rx_cal )
{
    if ( p_rx_cal != NULL )
    {
        uint8_t band;

        card_free_cal_table_elements( &p_rx_cal->ref );
        card_free_cal_table_elements( &p_rx_cal->gain );
        card_free_cal_table_elements( &p_rx_cal->bands );
        for ( band = 0; band < MAX_NR_BANDS; band++ )
        {
            card_free_cal_table_elements( &p_rx_cal->band_refs[band] );
        }
    }
}


/**************************************************************************************************/
/**
 * This function calls card_free_cal_table_elements on all cal_table structures in a struct
 * iq_complex_cal
 */
void card_free_iq_complex_cal_tables( struct iq_complex_cal *p_iq_complex_cal )
{
    if ( p_iq_complex_cal != NULL )
    {
        card_free_cal_table_elements( &p_iq_complex_cal->re_cal );
        card_free_cal_table_elements( &p_iq_complex_cal->im_cal );
    }
}


/******************************************************************************/
/**
 * This function frees all of the referenced struct cal_table elements as well
 * as the struct cal_table reference
 */
struct cal_table * card_free_cal_table( struct cal_table *p_cal_table )
{
    if ( p_cal_table != NULL )
    {
        card_free_cal_table_elements( p_cal_table );
        free( p_cal_table );
    }

    return NULL;
}


/******************************************************************************/
/**
 * The card_calibration_load() function attempts to cache calibration data from
 * the Sidekiq's NVM at the specified card index and given a skiq_part_t type.
 * This function will NOT overwrite the local cache of calibration data, it must
 * be available prior to invoking (use card_calibration_clear() if needed).  The
 * function handles reading the calibration record from the associated NVM
 * (based on skiq_part_t), populating the local cache of calibration data, and
 * verifying that all related tables (ref and gain) for a given receive handle
 * are present.  Any error in this process leaves the local cache empty and
 * available, essentially unwinding any partial import of data from NVM.
 */
static int32_t _card_calibration_load( uint8_t card,
                                       skiq_part_t part )
{
    int32_t status = 0;
    uint8_t *p_data = NULL;
    uint32_t cal_length = 0;

    skiq_info("Loading calibration data for Sidekiq %s, card %u\n", part_cstr(part), card);

    LOCK_CAL(card);
    {
        skiq_rx_hdl_t hdl;

        /* 1. Check for populated tables, unlock and set error if so */
        for ( hdl = skiq_rx_hdl_A1; ( status == 0 ) && ( hdl < skiq_rx_hdl_end ); hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; ( status == 0 ) && ( port < skiq_rf_port_max ); port++ )
            {
                if ( _is_rx_cal_data_present_unlocked( card, hdl, port ) )
                {
                    debug_print("Calibration is already populated for card %u hdl %s port %s, "
                                "returning error\n", card, rx_hdl_cstr(hdl),
                                skiq_rf_port_string(port));
                    status = -EEXIST;
                }
            }
        }

        /* 2. Read from NV storage (source and length depending on part) */
        if ( status == 0 )
        {
            status = cal_load_from_storage( card, &p_data, &cal_length );
        }

        /* 3. Import calibration by pointer */
        if ( ( status == 0 ) && ( p_data != NULL ) && ( cal_length > 0 ) )
        {
            status = _import_cal( card, p_data, cal_length );
        }

        /* 4. free temporary memory */
        FREE_IF_NOT_NULL( p_data );
    }
    UNLOCK_CAL(card);

    debug_print("Loading calibration data for Sidekiq %s, card %u --> status = %d\n",
                part_cstr(part), card, status);

    return status;
}


/******************************************************************************/
/** The card_calibration_clear() function completely clears and frees the local
 * calibration cache for a given card index.
 */
static int32_t _card_calibration_clear( uint8_t card )
{
    int32_t status = 0;

    LOCK_CAL(card);
    {
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++)
            {
                /* free the elements for each receive cal table per handle per port */
                card_free_rx_cal_tables( P_RX_CAL(card,hdl,port) );
            }
            card_free_iq_complex_cal_tables( P_IQ_COMPLEX_CAL(card,hdl) );
        }
    }
    UNLOCK_CAL(card);

    return status;
}


/******************************************************************************/
/**
 * The card_calibration_add_rx_cal() function is used by calibration_factory.c
 * to import a struct rx_cal for a given card index and handle.
 *
 * NOTE: the local calibration data must be available for a given card index and
 * handle before calling this function otherwise an error is returned.
 *
 * NOTE: the references are shallow copied from the caller and the caller must
 * relinquish allocation ownership.
 */
int32_t card_calibration_add_rx_cal( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     skiq_rf_port_t port,
                                     struct rx_cal *p_rx_cal )
{
    int32_t status = 0;

    TRACE_ENTER;

    /* perform the port fix-up based on card and handle */
    port = port_fixup(card, hdl, port);

    LOCK_CAL(card);
    {
        /* check that (card,hdl,port) isn't already populated, the shallow copy
         * the references, we own them now */
        if ( _is_rx_cal_data_present_unlocked( card, hdl, port ) )
        {
            debug_print("RX calibration is already populated for card %u hdl %s, returning error\n", card,
                        rx_hdl_cstr(hdl));
            status = -EEXIST;
        }
        else
        {
            uint8_t band;

            CREF(card,hdl,port) = p_rx_cal->ref;
            CGAIN(card,hdl,port) = p_rx_cal->gain;
            CBANDS(card,hdl,port) = p_rx_cal->bands;
            for ( band = 0; band < CBANDS(card,hdl,port).nr_rows; band++ )
            {
                CBANDREF(card,hdl,port,band) = p_rx_cal->band_refs[band];
            }
        }
    }
    UNLOCK_CAL(card);

    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
/**
 * The card_calibration_add_iq_complex_cal() function is used by calibration_factory.c to import a
 * struct iq_complex_cal for a given card index and handle.
 *
 * NOTE: the local calibration data must be available for a given card index and handle before
 * calling this function otherwise an error is returned.
 *
 * NOTE: the references are shallow copied from the caller and the caller must relinquish allocation
 * ownership.
 */
int32_t card_calibration_add_iq_complex_cal( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             struct iq_complex_cal *p_iq_complex_cal )
{
    int32_t status = 0;

    TRACE_ENTER;

    LOCK_CAL(card);
    {
        /* check that (card,hdl) isn't already populated, the shallow copy the references, we own
         * them now */
        if ( is_iq_complex_cal_data_present_unlocked(card, hdl) )
        {
            debug_print("IQ complex calibration is already populated for card %u hdl %s, returning "
                        "error -EEXIST\n", card, rx_hdl_cstr(hdl));
            status = -EEXIST;
        }
        else
        {
            CRECAL(card,hdl) = p_iq_complex_cal->re_cal;
            CIMCAL(card,hdl) = p_iq_complex_cal->im_cal;
        }
    }
    UNLOCK_CAL(card);

    TRACE_EXIT_STATUS(status);

    return (status);
}


#if (defined ATE_SUPPORT)
/**************************************************************************************************/
/** The card_calibration_commit() function takes the locally cached calibration data for a given
    card and stores it in the associated non-volatile memory.  It is advised that this function only
    be accessible through the factory API (and possibly not even delivered publicly).

    @return int32_t
    @retval 0 Success
    @retval -ENOMEM Failure to allocate adequate memory to export calibration data
    @retval -E2BIG Calibration data too big to store in associated storage medium
 */
static int32_t _card_calibration_commit( uint8_t card,
                                         skiq_part_t part )
{
    int32_t status = 0;

    LOCK_CAL(card);
    {
        uint8_t *p_data = NULL;
        int32_t n = 0;

        p_data = calloc(1, CAL_STORAGE_MAX_SIZE);
        if ( p_data != NULL )
        {
            n = cal_export_cal( card, p_data, CAL_STORAGE_MAX_SIZE );
            if ( n < 0 )
            {
                status = n;
            }
            else
            {
                debug_print("Calibration is %u bytes long for card %u\n", n, card);

                /* write memory to non-volatile storage */
                status = cal_write_to_storage(card, p_data, n);

                debug_hex_dump(p_data, n);
            }

            /* free chunk of memory */
            FREE_IF_NOT_NULL( p_data );
        }
        else
        {
            status = -ENOMEM;
        }
    }
    UNLOCK_CAL(card);

    return (status);
}
#endif  /* ATE_SUPPORT */


/**************************************************************************************************/
/**
 * This function calculates and returns a radio gain conversion factor Gradio in
 * units of dB, that satisfies Pbb = Prf + Gradio(f, M, firG) where Pbb is the
 * baseband power in dBm, and Prf is the RF power in dBm, f is the lo_freq, M is
 * the gain index, and firG is the RX FIR gain (in dB).
 *
 * @note If a Sidekiq card does not have measurements resident in flash, it will
 * fallback to using the default set.
 *
 * @param[in] card index of Sidekiq card of interest
 * @param[in] hdl Sidekiq RX handle of interest
 * @param[in] lo_freq LO frequency of interest
 * @param[in] gain_index gain index of interest
 * @param[in] programmable_rxfir_gain_dB value (in dB) of the programmable RX FIR gain of the RFIC
 * @param[out] p_cal_rx_gain_dB reference to container for calculated RX gain in dB
 *
 * @return  0 indicates success, -EINVAL if hdl is invalid
 */
static int32_t _card_read_cal_offset_in_dB( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            skiq_rf_port_t port,
                                            uint64_t lo_freq,
                                            uint8_t gain_index,
                                            double programmable_rxfir_gain_dB,
                                            double *p_cal_rx_gain_dB )
{
    int32_t status = 0;
    double gain = 0.0, ref = 0.0, correction = 0.0;
    struct cal_table *p_table = NULL;

    /* check that `port` is in range */
    if ( ( port < skiq_rf_port_J1 ) || ( port >= skiq_rf_port_max ) )
    {
        status = -EINVAL;
    }

    /* perform the port fix-up based on card and handle */
    port = port_fixup(card, hdl, port);

    /* lock card's calibration set */
    LOCK_CAL(card);
    {
        /* look up gain given LO frequency and RFIC gain index */
        if ( 0 == status )
        {
            p_table = lookup_cal_table(card, hdl, port, lo_freq, rx_gain_table_type);
            status = lookup_meas_in_calibration(p_table, (double)lo_freq,
                                                (double)gain_index, &gain);
        }

        /* look up reference given LO frequency */
        if ( 0 == status )
        {
            p_table = lookup_cal_table(card, hdl, port, lo_freq, rx_ref_table_type);
            status = lookup_ref_in_calibration(p_table, (double)lo_freq, &ref);
        }

        /* look up correction given LO frequency */
        if ( 0 == status )
        {
            /* A correction table is optional: if no table is found, then correction is 0.0 */
            p_table = lookup_correction_table(card, hdl, port);
            if ( p_table != NULL )
            {
                /* can use lookup_ref_in_correction for this table as well since it is not gain
                   index dependent */
                status = lookup_ref_in_calibration(p_table, (double)lo_freq, &correction);
            }
            else
            {
                correction = (double)0.0;
            }
        }
    }
    UNLOCK_CAL(card);
    /* unlock card's calibration set */

    if ( 0 == status )
    {
        /* finally calculate the conversion factor */
        *p_cal_rx_gain_dB = gain + programmable_rxfir_gain_dB + ref + correction;
    }

    return status;
}


/**************************************************************************************************/
/**
 * This function looks up the I/Q complex real/imag calibration factor given an LO frequency and
 * unit temperature.
 *
 * @param[in] card index of Sidekiq card of interest
 * @param[in] hdl Sidekiq RX handle of interest
 * @param[in] lo_freq LO frequency of interest
 * @param[in] temper_deg_C temperature in degrees Celsius of interest
 * @param[out] p_real real factor of complex multiplier
 * @param[out] p_imag imaginary factor of complex multiplier
 *
 * @return  0 indicates success, -EINVAL if hdl is invalid
 */
static int32_t _card_read_iq_cal_complex_parts( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint64_t lo_freq,
                                                int8_t temper_deg_C,
                                                double *p_real,
                                                double *p_imag)
{
    int32_t status = 0;

    /* lock card's calibration set */
    LOCK_CAL(card);
    {
        /* look up real factor given LO frequency and temperature */
        if ( 0 == status )
        {
            struct cal_table *p_table = lookup_cal_table(card, hdl, skiq_rf_port_J1, lo_freq, re_cal_table_type);
            status = lookup_meas_in_calibration(p_table, (double)(lo_freq), (double)temper_deg_C, p_real);
        }

        /* look up imaginary factor given LO frequency and temperature */
        if ( 0 == status )
        {
            struct cal_table *p_table = lookup_cal_table(card, hdl, skiq_rf_port_J1, lo_freq, im_cal_table_type);
            status = lookup_meas_in_calibration(p_table, (double)(lo_freq), (double)temper_deg_C, p_imag);
        }

    }
    UNLOCK_CAL(card);
    /* unlock card's calibration set */

    return status;
}

#if (defined ATE_SUPPORT)
/* defined in calibration_factory.c */
extern int32_t _card_calibration_add_rx_by_file( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          skiq_rf_port_t port,
                                          FILE* fp );
extern int32_t _card_calibration_add_iq_complex_by_file( uint8_t card,
                                                         skiq_rx_hdl_t hdl,
                                                         FILE* fp );
#endif

calibration_funcs_t local_cal_funcs =
{
    .card_calibration_load = _card_calibration_load,
    .card_calibration_clear = _card_calibration_clear,
    .card_is_rx_cal_data_present = _card_is_rx_cal_data_present,
    .card_read_cal_offset_in_dB = _card_read_cal_offset_in_dB,
    .card_is_iq_complex_cal_data_present = _card_is_iq_complex_cal_data_present,
    .card_read_iq_cal_complex_parts = _card_read_iq_cal_complex_parts,
#if (defined ATE_SUPPORT)
    .card_calibration_commit = _card_calibration_commit,
    .card_calibration_add_rx_by_file = _card_calibration_add_rx_by_file,
    .card_calibration_add_iq_complex_by_file = _card_calibration_add_iq_complex_by_file,
#endif    
};
