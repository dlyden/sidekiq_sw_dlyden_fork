/*****************************************************************************/
/** @file calibration_rpc.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the implementation of calibration functions
    that use RPC.
*/

#include "calibration.h"
#include "calibration_funcs.h"

#include "sidekiq_rpc_client_api.h"

static int32_t _card_calibration_load( uint8_t card,
                                       skiq_part_t part )
{
    int32_t status=0;
    
    // do nothing...it'll be loaded on whatever we're connected to during init

    return (status);
}

static int32_t _card_calibration_clear( uint8_t card )
{
    int32_t status=0;
    
    // do nothing...it'll be cleared on whatever we're connected to

    return (status);
}

static bool _card_is_rx_cal_data_present( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          skiq_rf_port_t port )
{
    bool present=false;
    
    // RPC
    present = rpc_client_cal_is_rx_cal_data_present( card,
                                                     hdl,
                                                     port );
    
    return present;
}

static int32_t _card_read_cal_offset_in_dB( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            skiq_rf_port_t port,
                                            uint64_t lo_freq,
                                            uint8_t gain_index,
                                            double programmable_rxfir_gain_dB,
                                            double *p_cal_rx_gain_dB )
{
    int32_t status=0;
    
    // RPC
    status = rpc_client_cal_read_cal_offset_in_dB( card,
                                                   hdl,
                                                   port,
                                                   lo_freq,
                                                   gain_index,
                                                   programmable_rxfir_gain_dB,
                                                   p_cal_rx_gain_dB );

    return status;
}

static bool _card_is_iq_complex_cal_data_present( uint8_t card,
                                                  skiq_rx_hdl_t hdl )
{
    bool present = false;
    
    // RPC
    present = rpc_client_cal_is_iq_complex_cal_data_present( card, hdl );

    return present;
}

static int32_t _card_read_iq_cal_complex_parts( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint64_t lo_freq,
                                                int8_t temper_deg_C,
                                                double *p_real,
                                                double *p_imag )
{
    int32_t status=0;
    
    // RPC
    status = rpc_client_cal_read_iq_complex_cal_parts( card,
                                                       hdl,
                                                       lo_freq,
                                                       temper_deg_C,
                                                       p_real,
                                                       p_imag );

    return (status);
}

#if (defined ATE_SUPPORT)
static int32_t _card_calibration_commit( uint8_t card,
                                         skiq_part_t part )
{
    int32_t status=-ENOTSUP;

    return (status);
}

static int32_t _card_calibration_add_rx_by_file( uint8_t card,
                                                 skiq_rx_hdl_t hdl,
                                                 skiq_rf_port_t port,
                                                 FILE* fp )
{
    int32_t status=-ENOTSUP;

    return (status);
}

static int32_t _card_calibration_add_iq_complex_by_file( uint8_t card,
                                                         skiq_rx_hdl_t hdl,
                                                         FILE* fp )
{
    int32_t status=-ENOTSUP;

    return (status);
}
#endif

calibration_funcs_t rpc_cal_funcs =
{
    .card_calibration_load = _card_calibration_load,
    .card_calibration_clear = _card_calibration_clear,
    .card_is_rx_cal_data_present = _card_is_rx_cal_data_present,
    .card_read_cal_offset_in_dB = _card_read_cal_offset_in_dB,
    .card_is_iq_complex_cal_data_present = _card_is_iq_complex_cal_data_present,
    .card_read_iq_cal_complex_parts = _card_read_iq_cal_complex_parts,
#if (defined ATE_SUPPORT)
    .card_calibration_commit = _card_calibration_commit,
    .card_calibration_add_rx_by_file = _card_calibration_add_rx_by_file,
    .card_calibration_add_iq_complex_by_file = _card_calibration_add_iq_complex_by_file,
#endif    
};
