/**
 * @file   calibration_v2.c
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 12:20:07 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_types.h"
#include "sidekiq_private.h"    /* for *_cstr() */
#include "calibration_v2.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** The cal_import_table_v2() function attempts to populate the local card_calibration structure
    with the calibration record in memory referenced by p_data_start of length 'length'.  It decodes
    the calibration record and can populate any and all information in card_calibration[card].

    At the conclusion, either card_calibration[card] reflects the calibration record described by
    (p_data_start,length) or card_calibration[card] remains unpopulated and a non-zero status is
    returned.

    @note assumes the calibration_lock[card] is being held by thread.

    returns the number of bytes consumed by the table if positive.  if negative, there was a
    unrecoverable error encountered while importing the table.
 */
int32_t
cal_import_table_v2( uint8_t card,
                     uint8_t *p_data_start,
                     uint32_t length )
{
    int32_t status;
    struct cal_table_v2 *p_table_v2 = (struct cal_table_v2 *)p_data_start;
    int32_t hdr_length = p_table_v2->measurements - (uint8_t *)p_table_v2;
    struct cal_table *p_cal_table = NULL;

    debug_print("Parsing V2 table\n");
    debug_hex_dump(p_data_start, MIN(128, length));

    if ( length > hdr_length )
    {
        skiq_rf_port_t port;
        enum table_type type;
        skiq_rx_hdl_t hdl;
        char *name = NULL;

        hdl = (skiq_rx_hdl_t)p_table_v2->handle;
        type = (enum table_type)p_table_v2->type;
        port = (skiq_rf_port_t)p_table_v2->port;

        /* select cal_table based on type */
        if ( hdl < skiq_rx_hdl_end )
        {
            debug_print("V2 table for card %u hdl %s port %s type %s\n", card, rx_hdl_cstr(hdl),
                        skiq_rf_port_string(port), table_type_cstr(type));

            /* A V2 table may only contain 'ref' and 'gain' table types */
            switch (type)
            {
                case rx_ref_table_type:
                    name = REF_TABLE_NAME;
                    p_cal_table = P_CREF(card,hdl,port);
                    break;

                case rx_gain_table_type:
                    name = GAIN_TABLE_NAME;
                    p_cal_table = P_CGAIN(card,hdl,port);
                    break;

                default:
                    p_cal_table = NULL;
                    break;
            }
        }

        /* generically import the table based on the version.  If successful, add the number of
         * consumed bytes to those consumed locally to decode the hdl,port, and type.  It's okay to
         * pass NULL as p_cal_table as import_table is expected to handle that gracefully. */
        status = cal_import_table( card, p_table_v2->nr_rows, p_table_v2->nr_cols,
                                   p_table_v2->measurements, length - hdr_length, p_cal_table,
                                   name, CAL_VERSION_V2 );
        if ( status >= 0 )
        {
            status += hdr_length;
        }
    }
    else
    {
        /* not enough data to properly decode header let alone the measurements */
        status = -E2BIG;
    }

    return status;
}


/**************************************************************************************************/
/** The cal_import_cal_record_v2() function performs an import implementation for V2 records
    (receive calibration), checking that the calibration record is structured correctly (verifying
    that the KEY / IDENTIFIER, VERSION, handles, and FOOTER are present).  It delegates the main
    decoding the cal_import_table_v2() function for table correctness.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.

    @attention This function assumes that p_data is pointing at the first HANDLE field and length
    reflects the remaining number of bytes available to import.  p_data must NOT reference the
    beginning of the calibration record.

 */
int32_t
cal_import_record_v2( uint8_t card,
                      uint8_t *p_data,
                      uint32_t length,
                      uint8_t version )
{
    int32_t status = 0;

    debug_print("Parsing calibration record V%u from %p with length %u\n", version, p_data, length);
    debug_hex_dump(p_data, MIN(128, length));

    /* import tables, then check that necessary tables are present */
    status = cal_import_all_tables_in_record(card, p_data, length, version, 0);

    /* check that each found rx_cal has both tables present otherwise free */
    if ( 0 == status )
    {
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
            {
                bool free_elements = false;

                /* check that both 'ref' and 'gain' tables are present or that neither 'ref' nor
                 * 'gain' tables are present */
                if ( has_ref(card,hdl,port) && !has_gain(card, hdl, port) )
                {
                    free_elements = true;
                    skiq_warning("missing 'gain' calibration table for card %u hdl %s port %s",
                                 card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
                }
                else if ( ( has_ref(card,hdl,port) ) != ( has_gain(card, hdl, port) ) )
                {
                    free_elements = true;
                    skiq_warning("missing 'ref' calibration table for card %u hdl %s port %s",
                                 card, rx_hdl_cstr(hdl), skiq_rf_port_string(port));
                }

                if ( free_elements )
                {
                    card_free_cal_table_elements( P_CREF(card,hdl,port) );
                    card_free_cal_table_elements( P_CGAIN(card,hdl,port) );
                }
            }
        }
    }
    else
    {
        /* non-zero status means something went wrong in the import, free all rx_cal structures
         * across (hdl,port) */
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
            {
                card_free_rx_cal_tables( P_RX_CAL(card,hdl,port) );
            }
        }
    }

    /********************************************************************************************/
    /* display what's been discovered - for debug purposes */
    {
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            skiq_rf_port_t port;
            for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
            {
                debug_print("card %u hdl %s port %s ref\n", card, rx_hdl_cstr(hdl),
                            skiq_rf_port_string(port) );
                debug_display_table( P_CREF(card,hdl,port) );

                debug_print("card %u hdl %s port %s gain\n", card, rx_hdl_cstr(hdl),
                            skiq_rf_port_string(port) );
                debug_display_table( P_CGAIN(card,hdl,port) );
            }
        }
    }
    /********************************************************************************************/

    return status;
}


#if (defined ATE_SUPPORT)
/**************************************************************************************************/
/** The cal_export_table_v2() function is responsible for creating the correct calibration table
    format from the provided p_cal_table, handle, and type.  It populates the memory block
    referenced by p_data for the eventual commit to NVM.
 */
uint8_t *
cal_export_table_v2( uint8_t *p_data,
                     uint32_t *p_length,
                     struct cal_table *p_cal_table,
                     uint8_t handle,
                     uint8_t port,
                     enum table_type type )
{
    int i, j;
    uint8_t type_u8 = (uint8_t)type;
    MEAS_TYPE_v2 exported_value;
    MEAS_TYPE_COL_v2 exported_col_value;
    MEAS_TYPE_ROW_v2 exported_row_value;

    /* don't export empty tables */
    if ( p_cal_table->name != NULL )
    {
        /* follows the calibration record format described in calibration_private.h */
        p_data = cal_export_uint8( p_data, p_length, handle );
        p_data = cal_export_uint8( p_data, p_length, port );
        p_data = cal_export_uint8( p_data, p_length, type_u8 );
        p_data = cal_export_uint8( p_data, p_length, p_cal_table->nr_rows );
        p_data = cal_export_uint8( p_data, p_length, p_cal_table->nr_cols );

        for (j = 0; j < p_cal_table->nr_cols; j++)
        {
            /* MEAS_TYPE_COL_v2 is 1 byte wide */
            exported_col_value = (MEAS_TYPE_COL_v2)p_cal_table->column_headers[j];
            p_data = cal_export_uint8( p_data, p_length, exported_col_value );
        }

        for (i = 0; i < p_cal_table->nr_rows; i++)
        {
            /* MEAS_TYPE_ROW_v2 is 4 bytes wide */
            exported_row_value = (MEAS_TYPE_ROW_v2)(p_cal_table->row_headers[i] / 1000.0);
            p_data = cal_export_uint32( p_data, p_length, exported_row_value );
            for (j = 0; j < p_cal_table->nr_cols; j++)
            {
                /* MEAS_TYPE_v2 is 8 bytes wide */
                exported_value = (MEAS_TYPE_v2)p_cal_table->measurements[i * p_cal_table->nr_cols + j];
                p_data = cal_export_double( p_data, p_length, exported_value );
            }
        }
    }

    return p_data;
}


/**************************************************************************************************/
/** The cal_export_record_v2() function is responsible for the overall creation of the V2
    calibration record for a given card.  It populates the memory block referenced by p_data for the
    eventual commit to NVM.  It populates the header and footer of the record and delegates creation
    of the tables to cal_export_table_v2().
 */
int32_t
cal_export_record_v2( uint8_t card,
                      uint8_t *p_data,
                      uint32_t length )
{
    int32_t status = 0;
    uint8_t identifier[] = CAL_IDENTIFIER, footer[] = CAL_FOOTER;
    uint8_t *p = p_data;
    uint16_t *p_cal_length, cal_length_placeholder = 0;
    uint32_t total_length = length;
    skiq_rx_hdl_t hdl;

    p = cal_export_array( p, &length, identifier, ARRAY_SIZE( identifier ) );
    p = cal_export_uint8( p, &length, CAL_VERSION_V2 );
    p_cal_length = (uint16_t *)p;           /* save a reference to the LENGTH field */
    p = cal_export_uint16( p, &length, cal_length_placeholder );

    for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
    {
        skiq_rf_port_t port;
        for ( port = skiq_rf_port_J1; port < skiq_rf_port_max; port++ )
        {
            p = cal_export_table_v2( p, &length, P_CREF(card,hdl,port), hdl, port, rx_ref_table_type );
            p = cal_export_table_v2( p, &length, P_CGAIN(card,hdl,port), hdl, port, rx_gain_table_type );
        }
    }

    p = cal_export_uint8( p, &length, CAL_HANDLE_EOF );
    p = cal_export_array( p, &length, footer, ARRAY_SIZE( footer ));

    if ( p == NULL )
    {
        status = 0;
    }
    else
    {
        uint16_t export_length;

        /* number of bytes exported to p_data */
        status = total_length - length;

        /* store calibration data length in LENGTH field after byte re-ordering */
        export_length = (uint16_t)status;
        export_length = htole16(export_length);
        *p_cal_length = export_length;
    }

    return status;
}
#endif  /* ATE_SUPPORT */
