#include "calibration_private.h"

/* file generated on 2019-01-31T18:56:38.200737+00:00 */

static double _bands_table_A1_J1_columns[] = { 0, 1 };

static double _bands_table_A1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_A1_J1_measurements[] = {
    1.0,               450000000.0,
    450000001.0,       600000000.0,
    600000001.0,       800000000.0,
    800000001.0,       1200000000.0,
    1200000001.0,      1700000000.0,
    1700000001.0,      2700000000.0,
    2700000001.0,      3700000000.0,
    3700000001.0,      6000000000.0,
};

static double _gain_table_A1_J1_columns[] = { 195.0, 234.0, 255.0 };

static double _gain_table_A1_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_A1_J1_measurements[] = {
    -29.814,           -10.297,           0.0,                  /*  500000000.0 Hz */
    -30.43,            -10.521,           0.0,                  /* 3000000000.0 Hz */
    -30.236,           -10.048,           0.0,                  /* 4999000000.0 Hz */
    -29.988,           -9.966,            0.0,                  /* 5000000000.0 Hz */
    -30.199,           -10.464,           0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_A1_J1_columns[] = { 255.0 };

static double _b0_table_A1_J1_rows[] = {
    75000000.0,
    120000000.0,
    190000000.0,
    400000000.0,
    450000000.0,
};

static double _b0_table_A1_J1_measurements[] = {
    116.29,               /*   75000000.0 Hz */
    115.016,              /*  120000000.0 Hz */
    115.866,              /*  190000000.0 Hz */
    116.118,              /*  400000000.0 Hz */
    116.0,                /*  450000000.0 Hz */
};

static double _b1_table_A1_J1_columns[] = { 255.0 };

static double _b1_table_A1_J1_rows[] = {
    450000001.0,
    560000000.0,
    590000000.0,
    600000000.0,
};

static double _b1_table_A1_J1_measurements[] = {
    111.558,              /*  450000001.0 Hz */
    111.864,              /*  560000000.0 Hz */
    111.117,              /*  590000000.0 Hz */
    110.698,              /*  600000000.0 Hz */
};

static double _b2_table_A1_J1_columns[] = { 255.0 };

static double _b2_table_A1_J1_rows[] = {
    600000001.0,
    740000000.0,
    800000000.0,
};

static double _b2_table_A1_J1_measurements[] = {
    111.894,              /*  600000001.0 Hz */
    112.125,              /*  740000000.0 Hz */
    111.23,               /*  800000000.0 Hz */
};

static double _b3_table_A1_J1_columns[] = { 255.0 };

static double _b3_table_A1_J1_rows[] = {
    800000001.0,
    840000000.0,
    1060000000.0,
    1200000000.0,
};

static double _b3_table_A1_J1_measurements[] = {
    111.736,              /*  800000001.0 Hz */
    112.238,              /*  840000000.0 Hz */
    112.178,              /* 1060000000.0 Hz */
    110.277,              /* 1200000000.0 Hz */
};

static double _b4_table_A1_J1_columns[] = { 255.0 };

static double _b4_table_A1_J1_rows[] = {
    1200000001.0,
    1500000000.0,
    1700000000.0,
};

static double _b4_table_A1_J1_measurements[] = {
    112.694,              /* 1200000001.0 Hz */
    113.254,              /* 1500000000.0 Hz */
    111.818,              /* 1700000000.0 Hz */
};

static double _b5_table_A1_J1_columns[] = { 255.0 };

static double _b5_table_A1_J1_rows[] = {
    1700000001.0,
    1760000000.0,
    1940000000.0,
    2340000000.0,
    2580000000.0,
    2700000000.0,
};

static double _b5_table_A1_J1_measurements[] = {
    112.762,              /* 1700000001.0 Hz */
    113.115,              /* 1760000000.0 Hz */
    113.373,              /* 1940000000.0 Hz */
    112.543,              /* 2340000000.0 Hz */
    111.498,              /* 2580000000.0 Hz */
    109.087,              /* 2700000000.0 Hz */
};

static double _b6_table_A1_J1_columns[] = { 255.0 };

static double _b6_table_A1_J1_rows[] = {
    2700000001.0,
    2900000000.0,
    3184000000.0,
    3300000000.0,
    3400000000.0,
    3600000000.0,
    3601000000.0,
    3700000000.0,
};

static double _b6_table_A1_J1_measurements[] = {
    112.691,              /* 2700000001.0 Hz */
    112.785,              /* 2900000000.0 Hz */
    112.748,              /* 3184000000.0 Hz */
    112.772,              /* 3300000000.0 Hz */
    112.335,              /* 3400000000.0 Hz */
    111.808,              /* 3600000000.0 Hz */
    110.525,              /* 3601000000.0 Hz */
    110.394,              /* 3700000000.0 Hz */
};

static double _b7_table_A1_J1_columns[] = { 255.0 };

static double _b7_table_A1_J1_rows[] = {
    3700000001.0,
    3840000000.0,
    3940000000.0,
    4400000000.0,
    4780000000.0,
    5080000000.0,
    5220000000.0,
    5400000000.0,
    5900000000.0,
    6000000000.0,
};

static double _b7_table_A1_J1_measurements[] = {
    110.403,              /* 3700000001.0 Hz */
    111.055,              /* 3840000000.0 Hz */
    111.816,              /* 3940000000.0 Hz */
    111.202,              /* 4400000000.0 Hz */
    111.109,              /* 4780000000.0 Hz */
    111.736,              /* 5080000000.0 Hz */
    111.891,              /* 5220000000.0 Hz */
    110.649,              /* 5400000000.0 Hz */
    109.917,              /* 5900000000.0 Hz */
    109.676,              /* 6000000000.0 Hz */
};

static double _bands_table_A2_J1_columns[] = { 0, 1 };

static double _bands_table_A2_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_A2_J1_measurements[] = {
    1.0,               450000000.0,
    450000001.0,       600000000.0,
    600000001.0,       800000000.0,
    800000001.0,       1200000000.0,
    1200000001.0,      1700000000.0,
    1700000001.0,      2700000000.0,
    2700000001.0,      3700000000.0,
    3700000001.0,      6000000000.0,
};

static double _gain_table_A2_J1_columns[] = { 195.0, 234.0, 255.0 };

static double _gain_table_A2_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_A2_J1_measurements[] = {
    -29.76,            -10.309,           0.0,                  /*  500000000.0 Hz */
    -30.423,           -10.577,           0.0,                  /* 3000000000.0 Hz */
    -30.151,           -10.029,           0.0,                  /* 4999000000.0 Hz */
    -29.895,           -9.951,            0.0,                  /* 5000000000.0 Hz */
    -30.186,           -10.45,            0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_A2_J1_columns[] = { 255.0 };

static double _b0_table_A2_J1_rows[] = {
    75000000.0,
    120000000.0,
    190000000.0,
    400000000.0,
    450000000.0,
};

static double _b0_table_A2_J1_measurements[] = {
    115.722,              /*   75000000.0 Hz */
    114.381,              /*  120000000.0 Hz */
    115.275,              /*  190000000.0 Hz */
    115.42,               /*  400000000.0 Hz */
    115.282,              /*  450000000.0 Hz */
};

static double _b1_table_A2_J1_columns[] = { 255.0 };

static double _b1_table_A2_J1_rows[] = {
    450000001.0,
    560000000.0,
    590000000.0,
    600000000.0,
};

static double _b1_table_A2_J1_measurements[] = {
    110.781,              /*  450000001.0 Hz */
    111.019,              /*  560000000.0 Hz */
    110.172,              /*  590000000.0 Hz */
    109.702,              /*  600000000.0 Hz */
};

static double _b2_table_A2_J1_columns[] = { 255.0 };

static double _b2_table_A2_J1_rows[] = {
    600000001.0,
    740000000.0,
    800000000.0,
};

static double _b2_table_A2_J1_measurements[] = {
    111.188,              /*  600000001.0 Hz */
    111.319,              /*  740000000.0 Hz */
    110.294,              /*  800000000.0 Hz */
};

static double _b3_table_A2_J1_columns[] = { 255.0 };

static double _b3_table_A2_J1_rows[] = {
    800000001.0,
    840000000.0,
    1060000000.0,
    1200000000.0,
};

static double _b3_table_A2_J1_measurements[] = {
    111.046,              /*  800000001.0 Hz */
    111.493,              /*  840000000.0 Hz */
    111.276,              /* 1060000000.0 Hz */
    109.306,              /* 1200000000.0 Hz */
};

static double _b4_table_A2_J1_columns[] = { 255.0 };

static double _b4_table_A2_J1_rows[] = {
    1200000001.0,
    1500000000.0,
    1700000000.0,
};

static double _b4_table_A2_J1_measurements[] = {
    111.907,              /* 1200000001.0 Hz */
    112.377,              /* 1500000000.0 Hz */
    110.864,              /* 1700000000.0 Hz */
};

static double _b5_table_A2_J1_columns[] = { 255.0 };

static double _b5_table_A2_J1_rows[] = {
    1700000001.0,
    1760000000.0,
    1940000000.0,
    2340000000.0,
    2580000000.0,
    2700000000.0,
};

static double _b5_table_A2_J1_measurements[] = {
    111.871,              /* 1700000001.0 Hz */
    112.156,              /* 1760000000.0 Hz */
    112.393,              /* 1940000000.0 Hz */
    111.536,              /* 2340000000.0 Hz */
    110.479,              /* 2580000000.0 Hz */
    108.265,              /* 2700000000.0 Hz */
};

static double _b6_table_A2_J1_columns[] = { 255.0 };

static double _b6_table_A2_J1_rows[] = {
    2700000001.0,
    2900000000.0,
    3184000000.0,
    3300000000.0,
    3400000000.0,
    3600000000.0,
    3601000000.0,
    3700000000.0,
};

static double _b6_table_A2_J1_measurements[] = {
    111.686,              /* 2700000001.0 Hz */
    111.523,              /* 2900000000.0 Hz */
    111.545,              /* 3184000000.0 Hz */
    111.611,              /* 3300000000.0 Hz */
    111.235,              /* 3400000000.0 Hz */
    110.852,              /* 3600000000.0 Hz */
    109.593,              /* 3601000000.0 Hz */
    109.688,              /* 3700000000.0 Hz */
};

static double _b7_table_A2_J1_columns[] = { 255.0 };

static double _b7_table_A2_J1_rows[] = {
    3700000001.0,
    3840000000.0,
    3940000000.0,
    4400000000.0,
    4780000000.0,
    5080000000.0,
    5220000000.0,
    5400000000.0,
    5900000000.0,
    6000000000.0,
};

static double _b7_table_A2_J1_measurements[] = {
    109.699,              /* 3700000001.0 Hz */
    110.51,               /* 3840000000.0 Hz */
    110.725,              /* 3940000000.0 Hz */
    110.927,              /* 4400000000.0 Hz */
    111.042,              /* 4780000000.0 Hz */
    111.608,              /* 5080000000.0 Hz */
    111.677,              /* 5220000000.0 Hz */
    110.448,              /* 5400000000.0 Hz */
    108.045,              /* 5900000000.0 Hz */
    107.989,              /* 6000000000.0 Hz */
};

static double _bands_table_B1_J1_columns[] = { 0, 1 };

static double _bands_table_B1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_B1_J1_measurements[] = {
    1.0,               450000000.0,
    450000001.0,       600000000.0,
    600000001.0,       800000000.0,
    800000001.0,       1200000000.0,
    1200000001.0,      1700000000.0,
    1700000001.0,      2700000000.0,
    2700000001.0,      3700000000.0,
    3700000001.0,      6000000000.0,
};

static double _gain_table_B1_J1_columns[] = { 195.0, 234.0, 255.0 };

static double _gain_table_B1_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_B1_J1_measurements[] = {
    -29.77,            -10.301,           0.0,                  /*  500000000.0 Hz */
    -30.415,           -10.549,           0.0,                  /* 3000000000.0 Hz */
    -30.305,           -10.03,            0.0,                  /* 4999000000.0 Hz */
    -30.043,           -9.964,            0.0,                  /* 5000000000.0 Hz */
    -30.23,            -10.467,           0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_B1_J1_columns[] = { 255.0 };

static double _b0_table_B1_J1_rows[] = {
    75000000.0,
    120000000.0,
    190000000.0,
    400000000.0,
    450000000.0,
};

static double _b0_table_B1_J1_measurements[] = {
    116.372,              /*   75000000.0 Hz */
    115.155,              /*  120000000.0 Hz */
    115.886,              /*  190000000.0 Hz */
    116.147,              /*  400000000.0 Hz */
    115.996,              /*  450000000.0 Hz */
};

static double _b1_table_B1_J1_columns[] = { 255.0 };

static double _b1_table_B1_J1_rows[] = {
    450000001.0,
    560000000.0,
    590000000.0,
    600000000.0,
};

static double _b1_table_B1_J1_measurements[] = {
    111.52,               /*  450000001.0 Hz */
    111.838,              /*  560000000.0 Hz */
    111.063,              /*  590000000.0 Hz */
    110.608,              /*  600000000.0 Hz */
};

static double _b2_table_B1_J1_columns[] = { 255.0 };

static double _b2_table_B1_J1_rows[] = {
    600000001.0,
    740000000.0,
    800000000.0,
};

static double _b2_table_B1_J1_measurements[] = {
    111.911,              /*  600000001.0 Hz */
    112.079,              /*  740000000.0 Hz */
    111.138,              /*  800000000.0 Hz */
};

static double _b3_table_B1_J1_columns[] = { 255.0 };

static double _b3_table_B1_J1_rows[] = {
    800000001.0,
    840000000.0,
    1060000000.0,
    1200000000.0,
};

static double _b3_table_B1_J1_measurements[] = {
    111.74,               /*  800000001.0 Hz */
    112.268,              /*  840000000.0 Hz */
    112.089,              /* 1060000000.0 Hz */
    110.237,              /* 1200000000.0 Hz */
};

static double _b4_table_B1_J1_columns[] = { 255.0 };

static double _b4_table_B1_J1_rows[] = {
    1200000001.0,
    1500000000.0,
    1700000000.0,
};

static double _b4_table_B1_J1_measurements[] = {
    112.651,              /* 1200000001.0 Hz */
    113.187,              /* 1500000000.0 Hz */
    111.669,              /* 1700000000.0 Hz */
};

static double _b5_table_B1_J1_columns[] = { 255.0 };

static double _b5_table_B1_J1_rows[] = {
    1700000001.0,
    1760000000.0,
    1940000000.0,
    2340000000.0,
    2580000000.0,
    2700000000.0,
};

static double _b5_table_B1_J1_measurements[] = {
    112.604,              /* 1700000001.0 Hz */
    113.123,              /* 1760000000.0 Hz */
    113.259,              /* 1940000000.0 Hz */
    112.502,              /* 2340000000.0 Hz */
    111.232,              /* 2580000000.0 Hz */
    108.788,              /* 2700000000.0 Hz */
};

static double _b6_table_B1_J1_columns[] = { 255.0 };

static double _b6_table_B1_J1_rows[] = {
    2700000001.0,
    2900000000.0,
    3184000000.0,
    3300000000.0,
    3400000000.0,
    3600000000.0,
    3601000000.0,
    3700000000.0,
};

static double _b6_table_B1_J1_measurements[] = {
    112.57,               /* 2700000001.0 Hz */
    112.716,              /* 2900000000.0 Hz */
    112.603,              /* 3184000000.0 Hz */
    112.571,              /* 3300000000.0 Hz */
    112.379,              /* 3400000000.0 Hz */
    111.829,              /* 3600000000.0 Hz */
    110.628,              /* 3601000000.0 Hz */
    110.813,              /* 3700000000.0 Hz */
};

static double _b7_table_B1_J1_columns[] = { 255.0 };

static double _b7_table_B1_J1_rows[] = {
    3700000001.0,
    3840000000.0,
    3940000000.0,
    4400000000.0,
    4780000000.0,
    5080000000.0,
    5220000000.0,
    5400000000.0,
    5900000000.0,
    6000000000.0,
};

static double _b7_table_B1_J1_measurements[] = {
    110.83,               /* 3700000001.0 Hz */
    111.466,              /* 3840000000.0 Hz */
    112.226,              /* 3940000000.0 Hz */
    111.736,              /* 4400000000.0 Hz */
    111.731,              /* 4780000000.0 Hz */
    112.146,              /* 5080000000.0 Hz */
    111.932,              /* 5220000000.0 Hz */
    110.916,              /* 5400000000.0 Hz */
    109.926,              /* 5900000000.0 Hz */
    109.892,              /* 6000000000.0 Hz */
};

static double _bands_table_B2_J1_columns[] = { 0, 1 };

static double _bands_table_B2_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_B2_J1_measurements[] = {
    1.0,               450000000.0,
    450000001.0,       600000000.0,
    600000001.0,       800000000.0,
    800000001.0,       1200000000.0,
    1200000001.0,      1700000000.0,
    1700000001.0,      2700000000.0,
    2700000001.0,      3700000000.0,
    3700000001.0,      6000000000.0,
};

static double _gain_table_B2_J1_columns[] = { 195.0, 234.0, 255.0 };

static double _gain_table_B2_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_B2_J1_measurements[] = {
    -29.715,           -10.301,           0.0,                  /*  500000000.0 Hz */
    -30.509,           -10.589,           0.0,                  /* 3000000000.0 Hz */
    -30.206,           -10.055,           0.0,                  /* 4999000000.0 Hz */
    -29.933,           -9.957,            0.0,                  /* 5000000000.0 Hz */
    -30.186,           -10.429,           0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_B2_J1_columns[] = { 255.0 };

static double _b0_table_B2_J1_rows[] = {
    75000000.0,
    120000000.0,
    190000000.0,
    400000000.0,
    450000000.0,
};

static double _b0_table_B2_J1_measurements[] = {
    115.76,               /*   75000000.0 Hz */
    114.408,              /*  120000000.0 Hz */
    115.167,              /*  190000000.0 Hz */
    115.31,               /*  400000000.0 Hz */
    115.168,              /*  450000000.0 Hz */
};

static double _b1_table_B2_J1_columns[] = { 255.0 };

static double _b1_table_B2_J1_rows[] = {
    450000001.0,
    560000000.0,
    590000000.0,
    600000000.0,
};

static double _b1_table_B2_J1_measurements[] = {
    110.694,              /*  450000001.0 Hz */
    111.01,               /*  560000000.0 Hz */
    110.311,              /*  590000000.0 Hz */
    109.907,              /*  600000000.0 Hz */
};

static double _b2_table_B2_J1_columns[] = { 255.0 };

static double _b2_table_B2_J1_rows[] = {
    600000001.0,
    740000000.0,
    800000000.0,
};

static double _b2_table_B2_J1_measurements[] = {
    111.144,              /*  600000001.0 Hz */
    111.337,              /*  740000000.0 Hz */
    110.416,              /*  800000000.0 Hz */
};

static double _b3_table_B2_J1_columns[] = { 255.0 };

static double _b3_table_B2_J1_rows[] = {
    800000001.0,
    840000000.0,
    1060000000.0,
    1200000000.0,
};

static double _b3_table_B2_J1_measurements[] = {
    110.879,              /*  800000001.0 Hz */
    111.35,               /*  840000000.0 Hz */
    111.391,              /* 1060000000.0 Hz */
    109.601,              /* 1200000000.0 Hz */
};

static double _b4_table_B2_J1_columns[] = { 255.0 };

static double _b4_table_B2_J1_rows[] = {
    1200000001.0,
    1500000000.0,
    1700000000.0,
};

static double _b4_table_B2_J1_measurements[] = {
    111.951,              /* 1200000001.0 Hz */
    112.458,              /* 1500000000.0 Hz */
    110.912,              /* 1700000000.0 Hz */
};

static double _b5_table_B2_J1_columns[] = { 255.0 };

static double _b5_table_B2_J1_rows[] = {
    1700000001.0,
    1760000000.0,
    1940000000.0,
    2340000000.0,
    2580000000.0,
    2700000000.0,
};

static double _b5_table_B2_J1_measurements[] = {
    111.88,               /* 1700000001.0 Hz */
    112.076,              /* 1760000000.0 Hz */
    112.596,              /* 1940000000.0 Hz */
    111.729,              /* 2340000000.0 Hz */
    110.038,              /* 2580000000.0 Hz */
    108.114,              /* 2700000000.0 Hz */
};

static double _b6_table_B2_J1_columns[] = { 255.0 };

static double _b6_table_B2_J1_rows[] = {
    2700000001.0,
    2900000000.0,
    3184000000.0,
    3300000000.0,
    3400000000.0,
    3600000000.0,
    3601000000.0,
    3700000000.0,
};

static double _b6_table_B2_J1_measurements[] = {
    111.701,              /* 2700000001.0 Hz */
    111.337,              /* 2900000000.0 Hz */
    111.309,              /* 3184000000.0 Hz */
    111.41,               /* 3300000000.0 Hz */
    111.315,              /* 3400000000.0 Hz */
    110.688,              /* 3600000000.0 Hz */
    109.219,              /* 3601000000.0 Hz */
    109.844,              /* 3700000000.0 Hz */
};

static double _b7_table_B2_J1_columns[] = { 255.0 };

static double _b7_table_B2_J1_rows[] = {
    3700000001.0,
    3840000000.0,
    3940000000.0,
    4400000000.0,
    4780000000.0,
    5080000000.0,
    5220000000.0,
    5400000000.0,
    5900000000.0,
    6000000000.0,
};

static double _b7_table_B2_J1_measurements[] = {
    109.844,              /* 3700000001.0 Hz */
    110.715,              /* 3840000000.0 Hz */
    111.266,              /* 3940000000.0 Hz */
    110.929,              /* 4400000000.0 Hz */
    110.856,              /* 4780000000.0 Hz */
    111.253,              /* 5080000000.0 Hz */
    110.728,              /* 5220000000.0 Hz */
    110.205,              /* 5400000000.0 Hz */
    108.824,              /* 5900000000.0 Hz */
    108.908,              /* 6000000000.0 Hz */
};

static double _bands_table_C1_J1_columns[] = { 0, 1 };

static double _bands_table_C1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_C1_J1_measurements[] = {
    1.0,               450000000.0,
    450000001.0,       600000000.0,
    600000001.0,       800000000.0,
    800000001.0,       1200000000.0,
    1200000001.0,      1700000000.0,
    1700000001.0,      2700000000.0,
    2700000001.0,      3700000000.0,
    3700000001.0,      6000000000.0,
};

static double _gain_table_C1_J1_columns[] = { 195.0, 234.0, 255.0 };

static double _gain_table_C1_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_C1_J1_measurements[] = {
    -29.909,           -10.304,           0.0,                  /*  500000000.0 Hz */
    -30.3,             -10.469,           0.0,                  /* 3000000000.0 Hz */
    -30.25,            -10.155,           0.0,                  /* 4999000000.0 Hz */
    -30.136,           -9.996,            0.0,                  /* 5000000000.0 Hz */
    -30.135,           -10.42,            0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_C1_J1_columns[] = { 255.0 };

static double _b0_table_C1_J1_rows[] = {
    75000000.0,
    120000000.0,
    190000000.0,
    400000000.0,
    450000000.0,
};

static double _b0_table_C1_J1_measurements[] = {
    115.3,                /*   75000000.0 Hz */
    113.863,              /*  120000000.0 Hz */
    115.163,              /*  190000000.0 Hz */
    115.386,              /*  400000000.0 Hz */
    115.21,               /*  450000000.0 Hz */
};

static double _b1_table_C1_J1_columns[] = { 255.0 };

static double _b1_table_C1_J1_rows[] = {
    450000001.0,
    560000000.0,
    590000000.0,
    600000000.0,
};

static double _b1_table_C1_J1_measurements[] = {
    110.694,              /*  450000001.0 Hz */
    110.946,              /*  560000000.0 Hz */
    110.067,              /*  590000000.0 Hz */
    109.583,              /*  600000000.0 Hz */
};

static double _b2_table_C1_J1_columns[] = { 255.0 };

static double _b2_table_C1_J1_rows[] = {
    600000001.0,
    740000000.0,
    800000000.0,
};

static double _b2_table_C1_J1_measurements[] = {
    111.097,              /*  600000001.0 Hz */
    111.172,              /*  740000000.0 Hz */
    110.154,              /*  800000000.0 Hz */
};

static double _b3_table_C1_J1_columns[] = { 255.0 };

static double _b3_table_C1_J1_rows[] = {
    800000001.0,
    840000000.0,
    1060000000.0,
    1200000000.0,
};

static double _b3_table_C1_J1_measurements[] = {
    110.884,              /*  800000001.0 Hz */
    111.356,              /*  840000000.0 Hz */
    111.083,              /* 1060000000.0 Hz */
    109.099,              /* 1200000000.0 Hz */
};

static double _b4_table_C1_J1_columns[] = { 255.0 };

static double _b4_table_C1_J1_rows[] = {
    1200000001.0,
    1500000000.0,
    1700000000.0,
};

static double _b4_table_C1_J1_measurements[] = {
    111.672,              /* 1200000001.0 Hz */
    112.111,              /* 1500000000.0 Hz */
    110.624,              /* 1700000000.0 Hz */
};

static double _b5_table_C1_J1_columns[] = { 255.0 };

static double _b5_table_C1_J1_rows[] = {
    1700000001.0,
    1760000000.0,
    1940000000.0,
    2340000000.0,
    2580000000.0,
    2700000000.0,
};

static double _b5_table_C1_J1_measurements[] = {
    111.604,              /* 1700000001.0 Hz */
    111.884,              /* 1760000000.0 Hz */
    112.131,              /* 1940000000.0 Hz */
    111.198,              /* 2340000000.0 Hz */
    110.091,              /* 2580000000.0 Hz */
    107.864,              /* 2700000000.0 Hz */
};

static double _b6_table_C1_J1_columns[] = { 255.0 };

static double _b6_table_C1_J1_rows[] = {
    2700000001.0,
    2900000000.0,
    3184000000.0,
    3300000000.0,
    3400000000.0,
    3600000000.0,
    3601000000.0,
    3700000000.0,
};

static double _b6_table_C1_J1_measurements[] = {
    111.267,              /* 2700000001.0 Hz */
    111.049,              /* 2900000000.0 Hz */
    111.229,              /* 3184000000.0 Hz */
    111.272,              /* 3300000000.0 Hz */
    110.956,              /* 3400000000.0 Hz */
    110.655,              /* 3600000000.0 Hz */
    109.367,              /* 3601000000.0 Hz */
    109.532,              /* 3700000000.0 Hz */
};

static double _b7_table_C1_J1_columns[] = { 255.0 };

static double _b7_table_C1_J1_rows[] = {
    3700000001.0,
    3840000000.0,
    3940000000.0,
    4400000000.0,
    4780000000.0,
    5080000000.0,
    5220000000.0,
    5400000000.0,
    5900000000.0,
    6000000000.0,
};

static double _b7_table_C1_J1_measurements[] = {
    109.52,               /* 3700000001.0 Hz */
    110.234,              /* 3840000000.0 Hz */
    110.474,              /* 3940000000.0 Hz */
    110.511,              /* 4400000000.0 Hz */
    110.496,              /* 4780000000.0 Hz */
    110.977,              /* 5080000000.0 Hz */
    111.022,              /* 5220000000.0 Hz */
    109.563,              /* 5400000000.0 Hz */
    107.982,              /* 5900000000.0 Hz */
    107.916,              /* 6000000000.0 Hz */
};

static double _bands_table_D1_J1_columns[] = { 0, 1 };

static double _bands_table_D1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_D1_J1_measurements[] = {
    1.0,               450000000.0,
    450000001.0,       600000000.0,
    600000001.0,       800000000.0,
    800000001.0,       1200000000.0,
    1200000001.0,      1700000000.0,
    1700000001.0,      2700000000.0,
    2700000001.0,      3700000000.0,
    3700000001.0,      6000000000.0,
};

static double _gain_table_D1_J1_columns[] = { 195.0, 234.0, 255.0 };

static double _gain_table_D1_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_D1_J1_measurements[] = {
    -29.905,           -10.296,           0.0,                  /*  500000000.0 Hz */
    -30.414,           -10.503,           0.0,                  /* 3000000000.0 Hz */
    -30.302,           -10.214,           0.0,                  /* 4999000000.0 Hz */
    -30.112,           -9.971,            0.0,                  /* 5000000000.0 Hz */
    -30.148,           -10.404,           0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_D1_J1_columns[] = { 255.0 };

static double _b0_table_D1_J1_rows[] = {
    75000000.0,
    120000000.0,
    190000000.0,
    400000000.0,
    450000000.0,
};

static double _b0_table_D1_J1_measurements[] = {
    115.316,              /*   75000000.0 Hz */
    113.969,              /*  120000000.0 Hz */
    115.146,              /*  190000000.0 Hz */
    115.354,              /*  400000000.0 Hz */
    115.2,                /*  450000000.0 Hz */
};

static double _b1_table_D1_J1_columns[] = { 255.0 };

static double _b1_table_D1_J1_rows[] = {
    450000001.0,
    560000000.0,
    590000000.0,
    600000000.0,
};

static double _b1_table_D1_J1_measurements[] = {
    110.723,              /*  450000001.0 Hz */
    111.036,              /*  560000000.0 Hz */
    110.321,              /*  590000000.0 Hz */
    109.905,              /*  600000000.0 Hz */
};

static double _b2_table_D1_J1_columns[] = { 255.0 };

static double _b2_table_D1_J1_rows[] = {
    600000001.0,
    740000000.0,
    800000000.0,
};

static double _b2_table_D1_J1_measurements[] = {
    111.135,              /*  600000001.0 Hz */
    111.281,              /*  740000000.0 Hz */
    110.358,              /*  800000000.0 Hz */
};

static double _b3_table_D1_J1_columns[] = { 255.0 };

static double _b3_table_D1_J1_rows[] = {
    800000001.0,
    840000000.0,
    1060000000.0,
    1200000000.0,
};

static double _b3_table_D1_J1_measurements[] = {
    110.799,              /*  800000001.0 Hz */
    111.286,              /*  840000000.0 Hz */
    111.272,              /* 1060000000.0 Hz */
    109.458,              /* 1200000000.0 Hz */
};

static double _b4_table_D1_J1_columns[] = { 255.0 };

static double _b4_table_D1_J1_rows[] = {
    1200000001.0,
    1500000000.0,
    1700000000.0,
};

static double _b4_table_D1_J1_measurements[] = {
    111.804,              /* 1200000001.0 Hz */
    112.312,              /* 1500000000.0 Hz */
    110.752,              /* 1700000000.0 Hz */
};

static double _b5_table_D1_J1_columns[] = { 255.0 };

static double _b5_table_D1_J1_rows[] = {
    1700000001.0,
    1760000000.0,
    1940000000.0,
    2340000000.0,
    2580000000.0,
    2700000000.0,
};

static double _b5_table_D1_J1_measurements[] = {
    111.705,              /* 1700000001.0 Hz */
    111.893,              /* 1760000000.0 Hz */
    112.377,              /* 1940000000.0 Hz */
    111.449,              /* 2340000000.0 Hz */
    109.729,              /* 2580000000.0 Hz */
    107.761,              /* 2700000000.0 Hz */
};

static double _b6_table_D1_J1_columns[] = { 255.0 };

static double _b6_table_D1_J1_rows[] = {
    2700000001.0,
    2900000000.0,
    3184000000.0,
    3300000000.0,
    3400000000.0,
    3600000000.0,
    3601000000.0,
    3700000000.0,
};

static double _b6_table_D1_J1_measurements[] = {
    111.356,              /* 2700000001.0 Hz */
    110.93,               /* 2900000000.0 Hz */
    110.98,               /* 3184000000.0 Hz */
    111.115,              /* 3300000000.0 Hz */
    111.03,               /* 3400000000.0 Hz */
    110.492,              /* 3600000000.0 Hz */
    108.968,              /* 3601000000.0 Hz */
    109.691,              /* 3700000000.0 Hz */
};

static double _b7_table_D1_J1_columns[] = { 255.0 };

static double _b7_table_D1_J1_rows[] = {
    3700000001.0,
    3840000000.0,
    3940000000.0,
    4400000000.0,
    4780000000.0,
    5080000000.0,
    5220000000.0,
    5400000000.0,
    5900000000.0,
    6000000000.0,
};

static double _b7_table_D1_J1_measurements[] = {
    109.671,              /* 3700000001.0 Hz */
    110.541,              /* 3840000000.0 Hz */
    111.006,              /* 3940000000.0 Hz */
    110.758,              /* 4400000000.0 Hz */
    110.545,              /* 4780000000.0 Hz */
    110.744,              /* 5080000000.0 Hz */
    110.399,              /* 5220000000.0 Hz */
    109.77,               /* 5400000000.0 Hz */
    108.692,              /* 5900000000.0 Hz */
    108.875,              /* 6000000000.0 Hz */
};

struct card_calibration x4_default_cal = {
    .rx_cal_by_hdl_port = {
        [skiq_rx_hdl_A1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b0_table_A1_J1_columns,
                .row_headers = _b0_table_A1_J1_rows,
                .measurements = _b0_table_A1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_A1_J1_columns,
                .row_headers = _b1_table_A1_J1_rows,
                .measurements = _b1_table_A1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_A1_J1_columns,
                .row_headers = _b2_table_A1_J1_rows,
                .measurements = _b2_table_A1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_A1_J1_columns,
                .row_headers = _b3_table_A1_J1_rows,
                .measurements = _b3_table_A1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_A1_J1_columns,
                .row_headers = _b4_table_A1_J1_rows,
                .measurements = _b4_table_A1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b5_table_A1_J1_columns,
                .row_headers = _b5_table_A1_J1_rows,
                .measurements = _b5_table_A1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b6_table_A1_J1_columns,
                .row_headers = _b6_table_A1_J1_rows,
                .measurements = _b6_table_A1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 10,
                .column_headers = _b7_table_A1_J1_columns,
                .row_headers = _b7_table_A1_J1_rows,
                .measurements = _b7_table_A1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_A1_J1_columns,
                .row_headers = _bands_table_A1_J1_rows,
                .measurements = _bands_table_A1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 3,
                .nr_rows = 5,
                .column_headers = _gain_table_A1_J1_columns,
                .row_headers = _gain_table_A1_J1_rows,
                .measurements = _gain_table_A1_J1_measurements,
            },
        },
        [skiq_rx_hdl_A2][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b0_table_A2_J1_columns,
                .row_headers = _b0_table_A2_J1_rows,
                .measurements = _b0_table_A2_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_A2_J1_columns,
                .row_headers = _b1_table_A2_J1_rows,
                .measurements = _b1_table_A2_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_A2_J1_columns,
                .row_headers = _b2_table_A2_J1_rows,
                .measurements = _b2_table_A2_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_A2_J1_columns,
                .row_headers = _b3_table_A2_J1_rows,
                .measurements = _b3_table_A2_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_A2_J1_columns,
                .row_headers = _b4_table_A2_J1_rows,
                .measurements = _b4_table_A2_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b5_table_A2_J1_columns,
                .row_headers = _b5_table_A2_J1_rows,
                .measurements = _b5_table_A2_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b6_table_A2_J1_columns,
                .row_headers = _b6_table_A2_J1_rows,
                .measurements = _b6_table_A2_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 10,
                .column_headers = _b7_table_A2_J1_columns,
                .row_headers = _b7_table_A2_J1_rows,
                .measurements = _b7_table_A2_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_A2_J1_columns,
                .row_headers = _bands_table_A2_J1_rows,
                .measurements = _bands_table_A2_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 3,
                .nr_rows = 5,
                .column_headers = _gain_table_A2_J1_columns,
                .row_headers = _gain_table_A2_J1_rows,
                .measurements = _gain_table_A2_J1_measurements,
            },
        },
        [skiq_rx_hdl_B1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b0_table_B1_J1_columns,
                .row_headers = _b0_table_B1_J1_rows,
                .measurements = _b0_table_B1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_B1_J1_columns,
                .row_headers = _b1_table_B1_J1_rows,
                .measurements = _b1_table_B1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_B1_J1_columns,
                .row_headers = _b2_table_B1_J1_rows,
                .measurements = _b2_table_B1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_B1_J1_columns,
                .row_headers = _b3_table_B1_J1_rows,
                .measurements = _b3_table_B1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_B1_J1_columns,
                .row_headers = _b4_table_B1_J1_rows,
                .measurements = _b4_table_B1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b5_table_B1_J1_columns,
                .row_headers = _b5_table_B1_J1_rows,
                .measurements = _b5_table_B1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b6_table_B1_J1_columns,
                .row_headers = _b6_table_B1_J1_rows,
                .measurements = _b6_table_B1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 10,
                .column_headers = _b7_table_B1_J1_columns,
                .row_headers = _b7_table_B1_J1_rows,
                .measurements = _b7_table_B1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_B1_J1_columns,
                .row_headers = _bands_table_B1_J1_rows,
                .measurements = _bands_table_B1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 3,
                .nr_rows = 5,
                .column_headers = _gain_table_B1_J1_columns,
                .row_headers = _gain_table_B1_J1_rows,
                .measurements = _gain_table_B1_J1_measurements,
            },
        },
        [skiq_rx_hdl_B2][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b0_table_B2_J1_columns,
                .row_headers = _b0_table_B2_J1_rows,
                .measurements = _b0_table_B2_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_B2_J1_columns,
                .row_headers = _b1_table_B2_J1_rows,
                .measurements = _b1_table_B2_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_B2_J1_columns,
                .row_headers = _b2_table_B2_J1_rows,
                .measurements = _b2_table_B2_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_B2_J1_columns,
                .row_headers = _b3_table_B2_J1_rows,
                .measurements = _b3_table_B2_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_B2_J1_columns,
                .row_headers = _b4_table_B2_J1_rows,
                .measurements = _b4_table_B2_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b5_table_B2_J1_columns,
                .row_headers = _b5_table_B2_J1_rows,
                .measurements = _b5_table_B2_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b6_table_B2_J1_columns,
                .row_headers = _b6_table_B2_J1_rows,
                .measurements = _b6_table_B2_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 10,
                .column_headers = _b7_table_B2_J1_columns,
                .row_headers = _b7_table_B2_J1_rows,
                .measurements = _b7_table_B2_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_B2_J1_columns,
                .row_headers = _bands_table_B2_J1_rows,
                .measurements = _bands_table_B2_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 3,
                .nr_rows = 5,
                .column_headers = _gain_table_B2_J1_columns,
                .row_headers = _gain_table_B2_J1_rows,
                .measurements = _gain_table_B2_J1_measurements,
            },
        },
        [skiq_rx_hdl_C1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b0_table_C1_J1_columns,
                .row_headers = _b0_table_C1_J1_rows,
                .measurements = _b0_table_C1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_C1_J1_columns,
                .row_headers = _b1_table_C1_J1_rows,
                .measurements = _b1_table_C1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_C1_J1_columns,
                .row_headers = _b2_table_C1_J1_rows,
                .measurements = _b2_table_C1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_C1_J1_columns,
                .row_headers = _b3_table_C1_J1_rows,
                .measurements = _b3_table_C1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_C1_J1_columns,
                .row_headers = _b4_table_C1_J1_rows,
                .measurements = _b4_table_C1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b5_table_C1_J1_columns,
                .row_headers = _b5_table_C1_J1_rows,
                .measurements = _b5_table_C1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b6_table_C1_J1_columns,
                .row_headers = _b6_table_C1_J1_rows,
                .measurements = _b6_table_C1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 10,
                .column_headers = _b7_table_C1_J1_columns,
                .row_headers = _b7_table_C1_J1_rows,
                .measurements = _b7_table_C1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_C1_J1_columns,
                .row_headers = _bands_table_C1_J1_rows,
                .measurements = _bands_table_C1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 3,
                .nr_rows = 5,
                .column_headers = _gain_table_C1_J1_columns,
                .row_headers = _gain_table_C1_J1_rows,
                .measurements = _gain_table_C1_J1_measurements,
            },
        },
        [skiq_rx_hdl_D1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b0_table_D1_J1_columns,
                .row_headers = _b0_table_D1_J1_rows,
                .measurements = _b0_table_D1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_D1_J1_columns,
                .row_headers = _b1_table_D1_J1_rows,
                .measurements = _b1_table_D1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_D1_J1_columns,
                .row_headers = _b2_table_D1_J1_rows,
                .measurements = _b2_table_D1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_D1_J1_columns,
                .row_headers = _b3_table_D1_J1_rows,
                .measurements = _b3_table_D1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_D1_J1_columns,
                .row_headers = _b4_table_D1_J1_rows,
                .measurements = _b4_table_D1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b5_table_D1_J1_columns,
                .row_headers = _b5_table_D1_J1_rows,
                .measurements = _b5_table_D1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b6_table_D1_J1_columns,
                .row_headers = _b6_table_D1_J1_rows,
                .measurements = _b6_table_D1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 10,
                .column_headers = _b7_table_D1_J1_columns,
                .row_headers = _b7_table_D1_J1_rows,
                .measurements = _b7_table_D1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_D1_J1_columns,
                .row_headers = _bands_table_D1_J1_rows,
                .measurements = _bands_table_D1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 3,
                .nr_rows = 5,
                .column_headers = _gain_table_D1_J1_columns,
                .row_headers = _gain_table_D1_J1_rows,
                .measurements = _gain_table_D1_J1_measurements,
            },
        },
    },
};
