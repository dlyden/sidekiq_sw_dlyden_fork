/**
 * @file   calibration_storage.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec 19 10:41:04 CST 2018
 *
 * @brief Defines calibration storage information for each Sidekiq part (::skiq_part_t), provides
 * info look-up based on card index.
 *
 * <pre>
 * Copyright 2018-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#if (!defined __MINGW32__)
#include <sys/mount.h>
#endif

#include "calibration.h"
#include "calibration_private.h"
#include "calibration_versions.h"
#include "calibration_storage.h"

#include "sidekiq_private.h"
#include "sidekiq_flash.h"
#include "sidekiq_hal.h"
#include "card_services.h"


/***** DEFINES *****/

#define CAL_STORAGE(_blocks,_type)             { .storage = (_blocks), .type = (_type) }
#define CAL_FILE_STORAGE(_name,_mp,_size)                               \
    {                                                                   \
        .filepath = (_name),                                            \
        .mountpoint = (_mp),                                            \
        .size = (_size),                                                \
        .type = file_storage                                            \
    }

#define CAL_STORAGE_NOSUPP                     CAL_STORAGE(NULL,0,storage_not_supported)

#define STORAGE(_name,_blocks)                  \
    struct cal_storage_blocks _name =           \
    {                                           \
        .nr_blocks = ARRAY_SIZE( _blocks ),     \
        .blocks = _blocks,                      \
    }

/***** PER-PRODUCT NVM STORAGE SETTINGS *****/

#define MPCIE_CAL_BLOCK_ADDR            (0x3F8000)
#define MPCIE_CAL_BLOCK_SIZE            (0x8000)

#define M2_CAL_BLOCK_ADDR               (0x7F8000)
#define M2_CAL_BLOCK_SIZE               (0x8000)

#define M2_2280_CAL_BLOCK_ADDR          (0xFE0000)
#define M2_2280_CAL_BLOCK_SIZE          (0x20000)

#define NV100_CAL_BLOCK_ADDR            (0xFE0000)
#define NV100_CAL_BLOCK_SIZE            (0x20000)

#define Z2_CAL_FILENAME                 "unit-cal.bin"
#define Z2_CAL_MOUNTPOINT               "/mnt/versions"
#define Z2_CAL_FILEPATH                 Z2_CAL_MOUNTPOINT "/" Z2_CAL_FILENAME
#define Z2_CAL_FILE_SIZE_LIMIT          (48 << 10) /* 48 kilobytes (arbitrary) */

#define X2_CAL_BLOCK_ADDR               (0x4000)
#define X2_CAL_BLOCK_SIZE               (0x4000)

#define Z2P_CAL_FILENAME                 "unit-cal.bin"
#define Z2P_CAL_MOUNTPOINT               "/calibration"
#define Z2P_CAL_FILEPATH                 Z2P_CAL_MOUNTPOINT "/" Z2P_CAL_FILENAME
#define Z2P_CAL_FILE_SIZE_LIMIT          (48 << 10) /* 48 kilobytes (arbitrary) */

#define Z3U_CAL_FILENAME                 "unit-cal.bin"
#define Z3U_CAL_MOUNTPOINT               "/calibration"
#define Z3U_CAL_FILEPATH                 Z3U_CAL_MOUNTPOINT "/" Z3U_CAL_FILENAME
#define Z3U_CAL_FILE_SIZE_LIMIT          (48 << 10) /* 48 kilobytes (arbitrary) */


/***** TYPE DEFINITIONS *****/


/***** LOCAL DATA *****/


/**************************************************************************************************/
/* Sidekiq mPCIE calibration storage definition */
static struct cal_storage_block mpcie_blocks[] =
{
    { .address = MPCIE_CAL_BLOCK_ADDR, .size = MPCIE_CAL_BLOCK_SIZE },
};
static STORAGE(mpcie_storage, mpcie_blocks);


/**************************************************************************************************/
/* Sidekiq M.2 calibration storage definition */
static const struct cal_storage_block m2_blocks[] =
{
    { .address = M2_CAL_BLOCK_ADDR, .size = M2_CAL_BLOCK_SIZE },
};
static STORAGE(m2_storage, m2_blocks);


/**************************************************************************************************/
/* Sidekiq M.2-2280 calibration storage definition */
static const struct cal_storage_block m2_2280_blocks[] =
{
    { .address = M2_2280_CAL_BLOCK_ADDR, .size = M2_2280_CAL_BLOCK_SIZE },
};
static STORAGE(m2_2280_storage, m2_2280_blocks);


/**************************************************************************************************/
/* Sidekiq NV100 calibration storage definition */
static const struct cal_storage_block nv100_blocks[] =
{
    { .address = NV100_CAL_BLOCK_ADDR, .size = NV100_CAL_BLOCK_SIZE },
};
static STORAGE(nv100_storage, nv100_blocks);


/**************************************************************************************************/
/* Sidekiq X2 and X4 calibration data is stored in EEPROM.  The structure is outlined on Confluence
   here -- https://confluence.epiq.rocks/display/EN/Sidekiq+EEPROM+Memory
*/
/**************************************************************************************************/


/**************************************************************************************************/
/* Sidekiq X2 calibration storage definition */
static const struct cal_storage_block x2_blocks[] =
{
    { .address = X2_CAL_BLOCK_ADDR, .size = X2_CAL_BLOCK_SIZE },
};
static STORAGE(x2_storage, x2_blocks);


/**************************************************************************************************/
/* Sidekiq X4 calibration storage definition */
static const struct cal_storage_block x4_blocks[] =
{
    { .address =   0x70, .size =  0x3FE0 -   0x70 },
    { .address = 0x4000, .size = 0x10000 - 0x4000 },
};
static STORAGE(x4_storage, x4_blocks);


/* The 'storage_info' structure describes the available storage mechanisms by skiq_part_t.  The
 * mpcie, M.2, M.2-2280, and NV100 parts have their calibration stored in flash.  The X2 and X4
 * parts have their configuration stored in EEPROM.  The Z2 part has its configuration stored in its
 * file system.
 */
static struct cal_storage_info storage_info[skiq_part_invalid] = {
    [skiq_mpcie]      = CAL_STORAGE(&mpcie_storage,   flash_storage),
    [skiq_m2]         = CAL_STORAGE(&m2_storage,      flash_storage),
    [skiq_x2]         = CAL_STORAGE(&x2_storage,      eeprom_storage),
    [skiq_z2]         = CAL_FILE_STORAGE(Z2_CAL_FILEPATH, Z2_CAL_MOUNTPOINT, Z2_CAL_FILE_SIZE_LIMIT),
    [skiq_x4]         = CAL_STORAGE(&x4_storage,      eeprom_storage),
    [skiq_m2_2280]    = CAL_STORAGE(&m2_2280_storage, flash_storage),
    [skiq_nv100]      = CAL_STORAGE(&nv100_storage,   flash_storage),
    [skiq_z2p]        = CAL_FILE_STORAGE(Z2P_CAL_FILEPATH, Z2P_CAL_MOUNTPOINT, Z2P_CAL_FILE_SIZE_LIMIT),
    [skiq_z3u]        = CAL_FILE_STORAGE(Z3U_CAL_FILEPATH, Z3U_CAL_MOUNTPOINT, Z3U_CAL_FILE_SIZE_LIMIT),
};


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/

struct cal_storage_info *cal_get_storage_info( uint8_t card )
{
    skiq_part_t part = _skiq_get_part( card );
    struct cal_storage_info *info = NULL;

    if ( part < skiq_part_invalid )
    {
        info = &(storage_info[part]);
    }

    return info;
}
