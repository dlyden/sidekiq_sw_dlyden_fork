/**
 * @file   calibration_export.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   
 *
 * @brief
 *
 * <pre>
 * Copyright 2018-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <ctype.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#if (!defined __MINGW32__)
#include <sys/mount.h>
#endif

#include "calibration.h"
#include "calibration_private.h"
#include "calibration_versions.h"

#include "sidekiq_api.h"
#include "sidekiq_flash.h"
#include "sidekiq_hal.h"

/***** DEFINES *****/


/***** PER-PRODUCT NVM STORAGE SETTINGS *****/



/***** TYPE DEFINITIONS *****/



/***** LOCAL DATA *****/



/***** LOCAL FUNCTIONS *****/


#if (defined ATE_SUPPORT)
/**************************************************************************************************/
/** The cal_export_value() function copies a value to memory (referenced by 'output') and decrements
    the remaining length of that memory block.  This function is used throughout the
    _export_cal_table_v*() and _export_cal_v*() functions to append items to a memory block for
    eventual commit to NVM.
 */
uint8_t *
cal_export_value( uint8_t *output,
                  uint32_t *p_output_length,
                  void *value,
                  uint32_t value_length )
{
    if ( output == NULL )
    {
        /* destination reference cannot be NULL, indicate failure by returning NULL */
        return NULL;
    }

    if ( value_length > *p_output_length )
    {
        /* 'value' would not fit, indicate failure to export by returning NULL */
        return NULL;
    }
    else
    {
        memcpy( output, value, value_length );
        *p_output_length -= value_length;
        return output + value_length;
    }
}


/**************************************************************************************************/
/** The cal_export_array() function copies N bytes to memory using cal_export_value.
 */
uint8_t *
cal_export_array( uint8_t *output,
                  uint32_t *p_output_length,
                  uint8_t array[],
                  uint32_t array_length )
{
    return cal_export_value( output, p_output_length, array, array_length );
}


/**************************************************************************************************/
/** The cal_export_uint8() function copies 1 byte to memory using cal_export_value.
 */
uint8_t *
cal_export_uint8( uint8_t *output,
                  uint32_t *p_output_length,
                  uint8_t value )
{
    return cal_export_value( output, p_output_length, &value, sizeof(value) );
}


/**************************************************************************************************/
/** The cal_export_uint16() function performs a byte re-ordering, then copies 2 bytes to memory.
 */
uint8_t *
cal_export_uint16( uint8_t *output,
                   uint32_t *p_output_length,
                   uint16_t value )
{
    value = htole16(value);
    return cal_export_value( output, p_output_length, &value, sizeof(value) );
}


/**************************************************************************************************/
/** The cal_export_uint32() function performs a byte re-ordering, then copies 4 bytes to memory.
 */
uint8_t *
cal_export_uint32( uint8_t *output,
                   uint32_t *p_output_length,
                   uint32_t value )
{
    value = htole32(value);
    return cal_export_value( output, p_output_length, &value, sizeof(value) );
}


/**************************************************************************************************/
/** The cal_export_double() function performs a byte re-ordering, then copies 8 bytes to memory.
 */
uint8_t *
cal_export_double( uint8_t *output,
                   uint32_t *p_output_length,
                   double value )
{
    /* in some versions of htole64(), the 'double' was being type
       converted to a 64-bit integer, which is not what is desired for
       this function.  Instead, use a union avoid type conversion /
       typecasts */
    union
    {
        uint64_t u64;
        double lf;
    } conv;

    conv.lf = value;
    conv.u64 = htole64(conv.u64);
    return cal_export_value( output, p_output_length, &conv, sizeof(conv) );
}


/**************************************************************************************************/
/** The cal_export_cal() function is responsible for the overall creation of the set of calibration
    records for a given card.  It populates the memory block referenced by p_data for the eventual
    commit to NVM.
 */
int32_t cal_export_cal( uint8_t card,
                        uint8_t *p_data,
                        uint32_t length )
{
    int32_t status = -ENOTSUP, n, total = 0;
    skiq_part_t part = _skiq_get_part( card );

    /* These Sidekiq products use either a V1 or V2 calibration record */
    if ( ( part == skiq_mpcie ) || ( part == skiq_m2 ) || ( part == skiq_z2 ) )
    {
        /* Sidekiq Z2 uses a V2 calibration record while the others use a V1 */
        if ( part == skiq_z2 )
        {
            n = cal_export_record_v2( card, p_data, length );
        }
        else
        {
            n = cal_export_record_v1( card, p_data, length );
        }

        if ( n == 0 )
        {
            status = -E2BIG;
        }
        else
        {
            debug_print("V1/V2 calibration record is %u bytes long for card %u\n", n, card);
            total += n;
        }
    }
    else if ( ( part == skiq_x2 ) || ( part == skiq_x4 ) ||
              ( part == skiq_m2_2280 ) || ( part == skiq_z2p ) ||
              ( part == skiq_z3u ) || ( part == skiq_nv100 ) )
    {
        /* These Sidekiq products use a V3 calibration record */
        n = cal_export_record_v3( card, p_data + total, length - total);
        if ( n == 0 )
        {
            status = -E2BIG;
        }
        else
        {
            debug_print("V3 calibration record(s) are %u bytes long for card %u\n", n, card);
            total += n;
        }
    }

    if ( status == -E2BIG )
    {
        skiq_error("Calibration is too large (> %u bytes) for destination NVM for card %u\n",
                   length, card);
    }
    else
    {
        status = total;
    }

    return status;
}
#endif  /* ATE_SUPPORT */
