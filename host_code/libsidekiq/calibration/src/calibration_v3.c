/**
 * @file   calibration_v3.c
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 12:20:07 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_types.h"
#include "sidekiq_private.h"    /* for *_cstr() */
#include "calibration_v3.h"
#include "calibration_v3_rx_ext.h"
#include "calibration_v3_iq_complex.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** The cal_import_table_v3() function dispatches a call to the specific V3 record type table
    importing function.

    returns the number of bytes consumed by the table if positive.  if negative, there was a
    unrecoverable error encountered while importing the table.
 */
int32_t
cal_import_table_v3( uint8_t card,
                     uint8_t *p_data_start,
                     uint32_t length,
                     enum record_type rtype )
{
    int32_t status = 0;

    switch (rtype)
    {
        case rx_ext_cal_record_type:
            status = cal_import_table_v3_rx_ext( card, p_data_start, length );
            break;

        case iq_complex_cal_record_type:
            status = cal_import_table_v3_iq_complex( card, p_data_start, length );
            break;

        default:
            /* ignore unknown record type, it's from the future! */
            skiq_unknown("Unknown / unsupported calibration record type %d for this library "
                         "release, skipping table import for card %u", rtype, card);
            break;
    }

    return (status);
}


/**************************************************************************************************/
/** The cal_import_record_v3() function dispatches a call to the specific V3 record type record
    importing function.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.

    @attention This function assumes that p_data is pointing at the first HANDLE field and length
    reflects the remaining number of bytes available to import.  p_data must NOT reference the
    beginning of the calibration record.

 */
int32_t
cal_import_record_v3( uint8_t card,
                      uint8_t *p_data,
                      uint32_t length,
                      enum record_type rtype )
{
    int32_t status = 0;

    switch (rtype)
    {
        case rx_ext_cal_record_type:
            status = cal_import_record_v3_rx_ext( card, p_data, length );
            break;

        case iq_complex_cal_record_type:
            status = cal_import_record_v3_iq_complex( card, p_data, length );
            break;

        default:
            /* ignore unknown record type, it's from the future! */
            skiq_unknown("Unknown / unsupported calibration record type %d for this library "
                         "release, skipping record import for card %u", rtype, card);
            break;
    }

    return (status);
}


#if (defined ATE_SUPPORT)
/**************************************************************************************************/
/** The cal_export_table_v3() function is responsible for creating the correct calibration table
    format from the provided p_cal_table, handle, port, and type.  It populates the memory block
    referenced by p_data for the eventual commit to NVM.
 */
uint8_t *
cal_export_table_v3( uint8_t *p_data,
                     uint32_t *p_length,
                     struct cal_table *p_cal_table,
                     uint8_t handle,
                     uint8_t port,
                     enum table_type type )
{
    int i, j;
    uint8_t type_u8 = (uint8_t)type;
    double exported_value;

    /* don't export empty tables */
    if ( p_cal_table->name != NULL )
    {
        /* follows the calibration record format described in calibration_private.h */
        p_data = cal_export_uint8( p_data, p_length, handle );

        /* version 3 has a port field */
        p_data = cal_export_uint8( p_data, p_length, port );
        p_data = cal_export_uint8( p_data, p_length, type_u8 );
        p_data = cal_export_uint8( p_data, p_length, p_cal_table->nr_rows );
        p_data = cal_export_uint8( p_data, p_length, p_cal_table->nr_cols );

        for (j = 0; j < p_cal_table->nr_cols; j++)
        {
            exported_value = (double)p_cal_table->column_headers[j];
            p_data = cal_export_double( p_data, p_length, exported_value );
        }

        for (i = 0; i < p_cal_table->nr_rows; i++)
        {
            exported_value = (double)p_cal_table->row_headers[i];
            p_data = cal_export_double( p_data, p_length, exported_value );
            for (j = 0; j < p_cal_table->nr_cols; j++)
            {
                exported_value = (double)p_cal_table->measurements[i * p_cal_table->nr_cols + j];
                p_data = cal_export_double( p_data, p_length, exported_value );
            }
        }
    }

    return p_data;
}

/**************************************************************************************************/
/** The cal_export_record_v3() function is responsible for the overall creation of all of the V3
    calibration records for a given card.  It populates the memory block referenced by p_data for
    the eventual commit to NVM.
 */
int32_t
cal_export_record_v3( uint8_t card,
                      uint8_t *p_data,
                      uint32_t length )
{
    int32_t status = 0, n, total = 0;

    if ( status == 0 )
    {
        n = cal_export_record_v3_rx_ext( card, p_data + total, length - total);
        if ( n == 0 )
        {
            status = -E2BIG;
        }
        else
        {
            debug_print("V3 RX EXT calibration record(s) are %u bytes long for card %u\n", n, card);
            total += n;
        }
    }

    if ( status == 0 )
    {
        n = cal_export_record_v3_iq_complex( card, p_data + total, length - total);
        if ( n == 0 )
        {
            status = -E2BIG;
        }
        else
        {
            debug_print("V3 IQ COMPLEX calibration record(s) are %u bytes long for card %u\n", n,
                        card);
            total += n;
        }
    }

    /*********************************************/
    /* NOTE: new V3 record exports would go here */
    /*********************************************/
    /** TEMPLATE CODE:
        if ( status == 0 )
        {
            n = cal_export_record_v3_NEW( card, p_data + total, length - total);
            if ( n == 0 )
            {
                status = -E2BIG;
            }
            else
            {
                debug_print("V3 <NEW> calibration record(s) are %u bytes long for card %u\n", n,
                            card);
                total += n;
            }
        }
    */

    if ( status == 0 )
    {
        status = total;
    }

    return status;
}

#endif  /* ATE_SUPPORT */
