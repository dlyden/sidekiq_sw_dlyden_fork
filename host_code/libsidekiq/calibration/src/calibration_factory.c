/**
 * @file   calibration_factory.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec 13 08:39:02 2017
 *
 * @brief Provides the file parsing implementation of receive calibration files
 * generated in production.
 *
 * <pre>
 * Copyright 2017-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "calibration_private.h"
#include "glib.h"

/* enable debug_print and debug_print_plain when DEBUG_CAL_TRACE is defined */
#if (defined DEBUG_CAL_TRACE)
#define DEBUG_PRINT_ENABLED
#define TRACE_ENTER                     do { debug_print("entered function\n"); } while (0)
#define TRACE_EXIT                      do { debug_print("exited function\n"); } while (0)
#define TRACE_EXIT_STATUS(_x)           do { debug_print("exited function with status %d\n", _x); } while (0)
#define TRACE_ARGS(...)                 do { debug_print(__VA_ARGS__); } while (0)
#else
#define TRACE_ENTER                     do { ; } while (0)
#define TRACE_EXIT                      do { ; } while (0)
#define TRACE_EXIT_STATUS(_x)           do { ; } while (0)
#define TRACE_ARGS(...)                 do { ; } while (0)
#endif

/* enable debug_print and debug_print_plain when DEBUG_CAL_TABLE is defined */
#if (defined DEBUG_CAL_TABLE)
#if (!defined DEBUG_PRINT_ENABLED)
#define DEBUG_PRINT_ENABLED             true
#endif
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/


/***** TYPE DEFINITIONS *****/


/***** LOCAL DATA *****/


/***** LOCAL FUNCTIONS *****/


/** parses a number from one or more tokens yielded by the GScanner.  The caller
 * provides the expected data (measurement) type.  */
static int32_t
_parse_number( GScanner *scanner,
               double *p_meas )
{
    int32_t status = 0;
    bool negate = false;
    GTokenType gtt;

    TRACE_ENTER;
    TRACE_ARGS("scanner=%p,p_meas=%p\n", scanner, p_meas);

    gtt = g_scanner_get_next_token( scanner );
    while ( gtt == '-' )
    {
        negate = !negate;
        gtt = g_scanner_get_next_token( scanner );
    }

    if ( gtt == G_TOKEN_INT )
    {
        *p_meas = (double)(g_scanner_cur_value( scanner ).v_int);
    }
    else if ( gtt == G_TOKEN_FLOAT )
    {
        *p_meas = (double)(g_scanner_cur_value( scanner ).v_float);
    }
    else
    {
        status = -1;
    }

    if ( ( status == 0 ) && ( negate ) )
    {
        *p_meas *= -1;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/** Some of the elements in the calibration file are expected to be unsigned
 * integers.  This function performs the simpler task than _parse_number() when
 * the format dictates as such. */
static int32_t
_parse_unsigned_number( GScanner *scanner,
                        uint32_t *p_int )
{
    int32_t status = 0;
    GTokenType gtt;

    TRACE_ENTER;

    gtt = g_scanner_get_next_token( scanner );
    if ( gtt == G_TOKEN_INT )
    {
        *p_int = g_scanner_cur_value( scanner ).v_int;
    }
    else
    {
        status = -1;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/* a directive takes the form:
 *     @name <number-of-rows> <number-of-columns> <column values...>
 *
 * This function parses the form and populates the corresponding fields in the
 * referenced struct cal_table.
 *
 * Here are a few examples:
 *     @ref 30 1 76.0
 *     @gain 19 21 0.0 5.0 20.0 30.0 31.0 32.0 33.0 34.0 35.0 36.0 37.0 38.0 39.0 40.0 45.0 50.0 55.0 60.0 65.0 70.0 76.0
 *
 */
static int32_t
_parse_directive( GScanner *scanner,
                  struct cal_table **pp_cal_table )
{
    int32_t status = 0, i = 0;
    GTokenType gtt;

    TRACE_ENTER;

    gtt = g_scanner_get_next_token( scanner );
    if ( gtt == G_TOKEN_IDENTIFIER )
    {
        char *table_name;
        uint32_t nr_rows, nr_cols;

        table_name = strdup( scanner->value.v_identifier );
        if ( table_name == NULL )
        {
            skiq_error("Unable to duplicate table name when parsing calibration table\n");
            status = -ENOMEM;
        }
        if ( status == 0 ) status = _parse_unsigned_number( scanner, &nr_rows );
        if ( status == 0 ) status = _parse_unsigned_number( scanner, &nr_cols );

        if ( ( status == 0 ) &&
             ( ( nr_rows > 0 ) && ( nr_rows <= MAX_NR_ROWS ) ) &&
             ( ( nr_cols > 0 ) && ( nr_cols <= MAX_NR_COLS ) ) )
        {
            *pp_cal_table = card_init_and_alloc_cal_table( *pp_cal_table, table_name, nr_rows, nr_cols );
            while ( ( status == 0 ) &&
                    ( i < (*pp_cal_table)->nr_cols ) )
            {
                double m;

                status = _parse_number( scanner, &m );
                if ( 0 == status )
                    (*pp_cal_table)->column_headers[i] = m;
                i++;
            }
        }
        else
        {
            status = -EINVAL;
        }

        free( table_name );
    }
    else
    {
        status = -1;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/* parses the measurement rows to complete the calibration table, the struct
 * cal_table must have the number of rows and number of columns populated and
 * memory allocated before calling this function. */
static int32_t
_parse_table( GScanner *scanner,
              struct cal_table *ct )
{
    int32_t status = 0, row, col;
    uint32_t current_line;

    TRACE_ENTER;
    TRACE_ARGS("scanner=%p, ct=%p\n", scanner, ct);

    for ( row = 0; ( status == 0 ) && ( row < ct->nr_rows ); row++ )
    {
        double m;

        status = _parse_number( scanner, &m );
        if ( status == 0 )
        {
            ct->row_headers[row] = m;
            current_line = scanner->line;
            for ( col = 0; ( status == 0 ) && ( col < ct->nr_cols ); col++ )
            {
                status = _parse_number( scanner, &m );
                if ( status == 0 )
                {
                    ct->measurements[(row*ct->nr_cols) + col] = m;
                    if ( scanner->line != current_line )
                    {
                        status = -1;
                    }
                }
            }
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/* https://stackoverflow.com/a/12340725 - checks that provided file descriptor is valid */
static bool
fd_is_valid( int fd )
{
    return fcntl(fd, F_GETFD) != -1 || errno != EBADF;
}


/**************************************************************************************************/
/** save_table performs a shallow copy of the elements of p_src to those of p_dest only if p_dest is
    not already populated.

    if the p_dest table is populated, this function frees the cal_table and returns -EINVAL.
 */
static int32_t
save_table( uint8_t card,
            skiq_rx_hdl_t hdl,
            struct cal_table *p_dest,
            struct cal_table *p_src )
{
    int32_t status = 0;

    if ( p_dest->name == NULL )
    {
        /* shallow copy the elements, then free just the cal_table */
        debug_print("found '%s' calibration table for card %u, hdl %u\n", p_src->name, card, hdl);
        *p_dest = *p_src;
        free(p_src);
    }
    else
    {
        /* duplicate, so free the duplicated whole table, the original copy is freed below in case
         * of error */
        skiq_error("found duplicate '%s' calibration table for card %u, hdl %u\n",
                   p_src->name, card, hdl);
        card_free_cal_table( p_src );
        status = -EINVAL;
    }

    return (status);
}


/***** GLOBAL FUNCTIONS *****/


/******************************************************************************/
/**
 * The _card_calibration_add_rx_by_file() function takes an open file handle and
 * attempts to parse its contents into a valid pair of receive calibration
 * tables (ref and gain).  If successful, then the calibration table is added to
 * the card's local calibration cache.  At that point, it may be consulted
 * through gain offset API calls or committed to NVM (using
 * card_calibration_commit).
 */
int32_t _card_calibration_add_rx_by_file( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          skiq_rf_port_t port,
                                          FILE* fp )
{
    GScanner *scanner = g_scanner_new(NULL);
    struct cal_table *p_cal_table = NULL;
    struct rx_cal rx_cal = RX_CAL_INITIALIZER;
    int32_t status = 0;
    int fd = fileno(fp);

    TRACE_ENTER;

    /* check that `port` is in range */
    if ( ( port < skiq_rf_port_J1 ) || ( port >= skiq_rf_port_max ) )
    {
        status = -EINVAL;
        return status;
    }

    /* check that provided file descriptor is valid before trying to scan it */
    if ( ( fd == -1 ) || !fd_is_valid( fd ) )
    {
        status = -EIO;
        return status;
    }

    g_scanner_input_file(scanner, fd);
    while (!g_scanner_eof(scanner) && ( status == 0 ))
    {
        GTokenType gtt = g_scanner_get_next_token(scanner);
        if ( gtt == '@' )
        {
            status = _parse_directive( scanner, &p_cal_table );
            if ( status == 0 )
            {
                status = _parse_table( scanner, p_cal_table );
            }

            /* table was parsed, figure out what type it is and store in rx_cal
             * if not already found */
            if ( status == 0 )
            {
                uint8_t band;
                if ( 0 == strcasecmp( REF_TABLE_NAME, p_cal_table->name ) )
                {
                    status = save_table( card, hdl, &rx_cal.ref, p_cal_table );
                }
                else if ( 0 == strcasecmp( GAIN_TABLE_NAME, p_cal_table->name ) )
                {
                    status = save_table( card, hdl, &rx_cal.gain, p_cal_table );
                }
                else if ( 0 == strcasecmp( BANDS_TABLE_NAME, p_cal_table->name ) )
                {
                    status = save_table( card, hdl, &rx_cal.bands, p_cal_table );
                }
                else if ( 1 == sscanf( p_cal_table->name, "b%" SCNu8, &band ) )
                {
                    if ( band < cal_max_nr_bands( card ) )
                    {
                        /* shallow copy the elements, then free just the cal_table */
                        debug_print("found '%s' calibration table for card %u, hdl %u\n",
                                    p_cal_table->name, card, hdl);
                        rx_cal.band_refs[band] = *p_cal_table;
                        free(p_cal_table);
                        p_cal_table = NULL;
                    }
                    else
                    {
                        skiq_error("found out-of-bounds '%s' calibration table for card %u, "
                                   "hdl %u\n", p_cal_table->name, card, hdl);
                        p_cal_table = card_free_cal_table(p_cal_table);
                        status = -EINVAL;
                    }
                }
                else
                {
                    p_cal_table = card_free_cal_table(p_cal_table);
                }
                p_cal_table = NULL;
            }
        }
        else if ( gtt == G_TOKEN_EOF )
        {
            /* NO-OP */
        }
        else
        {
            status = -1;
        }
    }
    g_scanner_destroy( scanner );

    /* add RX calibration if all required tables were discovered / populated.  NOTE:
     * card_calibration_add_rx_cal takes ownership of the cal_table elements */
    if ( 0 == status )
    {
        /* mPCIe, M.2, and Z2 verification, passes if and only if 'ref' and 'gain' are the only
         * tables present */
        if ( (rx_cal.ref.name != NULL) && (rx_cal.gain.name != NULL) )
        {
            uint8_t band;

            /* check that 'bands' and 'b0 .. bMAX' do not exist */
            if ( rx_cal.bands.name != NULL )
            {
                skiq_error("Successful parsing, but 'bands' table is not permitted "
                           "when 'ref' and 'gain' are present for card %u hdl %u\n", card, hdl);
                status = -EINVAL;
            }
            for ( band = 0; band < cal_max_nr_bands( card ); band++ )
            {
                if ( rx_cal.band_refs[band].name != NULL )
                {
                    skiq_error("Successful parsing, but '%s' table is not permitted "
                               "when 'ref' and 'gain' are present for card %u hdl %u\n",
                               rx_cal.band_refs[band].name, card, hdl);
                    status = -EINVAL;
                }
            }
        }
        else if (rx_cal.ref.name != NULL)
        {
            /* 'ref' table exists but 'gain' does not :sad-face: */
            skiq_error("Successful parsing, but required 'gain' table is missing for card "
                       "%u hdl %u\n", card, hdl);
            status = -EINVAL;
        }
        else if ( (rx_cal.gain.name != NULL) && (rx_cal.bands.name != NULL) )
        {
            uint8_t band;

            /* check that 'ref' does not exist, that 'b0 .. bN' do exist, and 'b(N+1) .. bMAX' do
             * not exist */
            if ( rx_cal.ref.name != NULL )
            {
                skiq_error("Successful parsing, but '%s' table is not permitted when 'gain' "
                           "and 'bands' are present for card %u hdl %u\n", rx_cal.ref.name, card, hdl);
                status = -EINVAL;
            }
            for ( band = 0; band < rx_cal.bands.nr_rows; band++ )
            {
                if ( rx_cal.band_refs[band].name == NULL )
                {
                    skiq_error("Successful parsing, but 'b%u' table is required since "
                               "'bands' table indicates it is present for card %u hdl %u\n",
                               band, card, hdl);
                    status = -EINVAL;
                }
            }
            for ( band = rx_cal.bands.nr_rows; band < cal_max_nr_bands( card ); band++ )
            {
                if ( rx_cal.band_refs[band].name != NULL )
                {
                    skiq_error("Successful parsing, but '%s' table is not permitted since "
                               "'bands' table suggests it is absent for card %u hdl %u\n",
                               rx_cal.band_refs[band].name, card, hdl);
                    status = -EINVAL;
                }
            }
        }
        else
        {
            /* no correct sets of tables found, flag it as invalid */
            skiq_error("Successful parsing, but other required table(s) are missing for "
                       "card %u hdl %u\n", card, hdl);
            status = -EINVAL;
        }
    }

    /* all is good to go, add the RX calibration */
    if ( 0 == status )
    {
        debug_print("finally adding RX calibration for card %u hdl %u port %u\n", card, hdl, port);
        status = card_calibration_add_rx_cal( card, hdl, port, &rx_cal );
    }

    /* free cal_table elements when status indicates a failure to add cal_table to calibration set */
    if ( 0 != status )
    {
        card_free_rx_cal_tables( &rx_cal );
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
/**
 * The _card_calibration_add_iq_complex_by_file() function takes an open file handle and attempts to
 * parse its contents into a valid pair of IQ phase and amplitude calibration tables (real and
 * imag).  If successful, then the calibration table is added to the card's local calibration cache.
 * At that point, it may be consulted through IQ complex correction API calls or committed to NVM
 * (using card_calibration_commit).
 */
int32_t _card_calibration_add_iq_complex_by_file( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  FILE* fp )
{
    GScanner *scanner = g_scanner_new(NULL);
    struct cal_table *p_cal_table = NULL;
    struct iq_complex_cal iq_complex_cal = IQ_COMPLEX_CAL_INITIALIZER;
    bool found_re_cal = false, found_im_cal = false;
    int32_t status = 0;
    int fd = fileno(fp);

    TRACE_ENTER;

    /* check that provided file descriptor is valid before trying to scan it */
    if ( ( fd == -1 ) || !fd_is_valid( fd ) )
    {
        status = -EIO;
        return status;
    }

    g_scanner_input_file(scanner, fd);
    while (!g_scanner_eof(scanner) && ( status == 0 ))
    {
        GTokenType gtt = g_scanner_get_next_token(scanner);
        if ( gtt == '@' )
        {
            status = _parse_directive( scanner, &p_cal_table );
            if ( status == 0 )
            {
                status = _parse_table( scanner, p_cal_table );
            }

            /* table was parsed, figure out what type it is and store in iq_complex_cal if not already
             * found */
            if ( status == 0 )
            {
                if ( ( 0 == strcasecmp( RE_CAL_TABLE_NAME, p_cal_table->name ) ) && !found_re_cal )
                {
                    /* shallow copy the elements, then free just the cal_table */
                    found_re_cal = true;
                    iq_complex_cal.re_cal = *p_cal_table;
                    free(p_cal_table);
                    p_cal_table = NULL;
                }
                else if ( ( 0 == strcasecmp( IM_CAL_TABLE_NAME, p_cal_table->name ) ) && !found_im_cal )
                {
                    /* shallow copy the elements, then free just the cal_table */
                    found_im_cal = true;
                    iq_complex_cal.im_cal = *p_cal_table;
                    free(p_cal_table);
                    p_cal_table = NULL;
                }
                else
                {
                    if ( ( 0 == strcasecmp( RE_CAL_TABLE_NAME, p_cal_table->name ) ) ||
                         ( 0 == strcasecmp( IM_CAL_TABLE_NAME, p_cal_table->name ) ) )
                    {
                        /* duplicate, so free the duplicated whole table, the original copy is freed
                         * below in case of error */
                        skiq_error("found duplicate '%s' calibration table for card %u, hdl %s\n",
                                   p_cal_table->name, card, rx_hdl_cstr(hdl));
                        status = -EINVAL;
                    }
                    p_cal_table = card_free_cal_table( p_cal_table );
                }
            }
        }
        else if ( gtt == G_TOKEN_EOF )
        {
            /* NO-OP */
        }
        else
        {
            status = -1;
        }
    }
    g_scanner_destroy( scanner );

    /* add IQ complex calibration if both tables were discovered / populated.  NOTE:
     * card_calibration_add_iq_complex_cal takes ownership of the cal_table elements */
    if ( ( 0 == status ) && found_re_cal && found_im_cal )
    {
        status = card_calibration_add_iq_complex_cal( card, hdl, &iq_complex_cal );
    }
    else if ( ( 0 == status ) && ( !found_re_cal || !found_im_cal ) )
    {
        if ( !found_re_cal )
            debug_print("successful parsing, but required '" RE_CAL_TABLE_NAME "' table is missing "
                        "for card %u hdl %u\n", card, hdl);

        if ( !found_im_cal )
            debug_print("successful parsing, but required '" IM_CAL_TABLE_NAME "' table is missing "
                        "for card %u hdl %u\n", card, hdl);

        status = -EINVAL;
    }

    /* free cal_table elements when status indicates a failure to add cal_table
     * to calibration set */
    if ( 0 != status )
    {
        if ( found_re_cal )
        {
            card_free_cal_table_elements( &iq_complex_cal.re_cal );
        }

        if ( found_im_cal )
        {
            card_free_cal_table_elements( &iq_complex_cal.im_cal );
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}
