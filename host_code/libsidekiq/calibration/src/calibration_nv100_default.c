#include "calibration_private.h"

/* file generated on 2021-09-14T19:11:05.898385+00:00 */

static double _bands_table_A1_J1_columns[] = { 0, 1 };

static double _bands_table_A1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_A1_J1_measurements[] = {
    30000000.0,        449999999.0,
    450000000.0,       599999999.0,
    600000000.0,       799999999.0,
    800000000.0,       1199999999.0,
    1200000000.0,      1699999999.0,
    1700000000.0,      2699999999.0,
    2700000000.0,      3599999999.0,
    3600000000.0,      6000000000.0,
};

static double _gain_table_A1_J1_columns[] = { 187.0, 195.0, 215.0, 235.0, 255.0 };

static double _gain_table_A1_J1_rows[] = {
    30000000.0,
    102500000.0,
    541500000.0,
    701500000.0,
    987500000.0,
    1400500000.0,
    2375500000.0,
    3200000000.0,
    4000000000.0,
    5000000000.0,
    5500500000.0,
    5998900000.0,
};

static double _gain_table_A1_J1_measurements[] = {
    -34.116,           -29.904,           -19.895,           -9.821,            0.0,                  /*   30000000.0 Hz */
    -33.671,           -29.581,           -19.586,           -9.556,            0.0,                  /*  102500000.0 Hz */
    -33.865,           -29.78,            -19.78,            -9.776,            0.0,                  /*  541500000.0 Hz */
    -33.841,           -29.714,           -19.77,            -9.735,            0.0,                  /*  701500000.0 Hz */
    -33.849,           -29.715,           -19.737,           -9.712,            0.0,                  /*  987500000.0 Hz */
    -33.836,           -29.789,           -19.837,           -9.851,            0.0,                  /* 1400500000.0 Hz */
    -33.558,           -29.665,           -19.871,           -9.89,             0.0,                  /* 2375500000.0 Hz */
    -32.939,           -29.33,            -19.662,           -9.728,            0.0,                  /* 3200000000.0 Hz */
    -32.584,           -29.183,           -19.647,           -9.724,            0.0,                  /* 4000000000.0 Hz */
    -32.27,            -29.182,           -19.837,           -9.952,            0.0,                  /* 5000000000.0 Hz */
    -32.147,           -29.176,           -19.953,           -9.989,            0.0,                  /* 5500500000.0 Hz */
    -31.37,            -28.63,            -19.558,           -9.696,            0.0,                  /* 5998900000.0 Hz */
};

static double _b0_table_A1_J1_columns[] = { 255.0 };

static double _b0_table_A1_J1_rows[] = {
    30100000.0,
    90000000.0,
    140000000.0,
    290000000.0,
    410000000.0,
    449900000.0,
};

static double _b0_table_A1_J1_measurements[] = {
    125.978,              /*   30100000.0 Hz */
    122.67,               /*   90000000.0 Hz */
    121.911,              /*  140000000.0 Hz */
    121.342,              /*  290000000.0 Hz */
    118.561,              /*  410000000.0 Hz */
    118.28,               /*  449900000.0 Hz */
};

static double _b1_table_A1_J1_columns[] = { 255.0 };

static double _b1_table_A1_J1_rows[] = {
    450100000.0,
    500500000.0,
    599900000.0,
};

static double _b1_table_A1_J1_measurements[] = {
    119.857,              /*  450100000.0 Hz */
    120.013,              /*  500500000.0 Hz */
    117.82,               /*  599900000.0 Hz */
};

static double _b2_table_A1_J1_columns[] = { 255.0 };

static double _b2_table_A1_J1_rows[] = {
    600100000.0,
    700500000.0,
    799900000.0,
};

static double _b2_table_A1_J1_measurements[] = {
    119.232,              /*  600100000.0 Hz */
    119.423,              /*  700500000.0 Hz */
    117.527,              /*  799900000.0 Hz */
};

static double _b3_table_A1_J1_columns[] = { 255.0 };

static double _b3_table_A1_J1_rows[] = {
    800100000.0,
    880100000.0,
    1100500000.0,
    1199900000.0,
};

static double _b3_table_A1_J1_measurements[] = {
    118.15,               /*  800100000.0 Hz */
    118.417,              /*  880100000.0 Hz */
    116.182,              /* 1100500000.0 Hz */
    113.689,              /* 1199900000.0 Hz */
};

static double _b4_table_A1_J1_columns[] = { 255.0 };

static double _b4_table_A1_J1_rows[] = {
    1200100000.0,
    1475500000.0,
    1699900000.0,
};

static double _b4_table_A1_J1_measurements[] = {
    116.357,              /* 1200100000.0 Hz */
    115.035,              /* 1475500000.0 Hz */
    112.375,              /* 1699900000.0 Hz */
};

static double _b5_table_A1_J1_columns[] = { 255.0 };

static double _b5_table_A1_J1_rows[] = {
    1700100000.0,
    1860100000.0,
    2200500000.0,
    2500500000.0,
    2699900000.0,
};

static double _b5_table_A1_J1_measurements[] = {
    114.345,              /* 1700100000.0 Hz */
    114.972,              /* 1860100000.0 Hz */
    113.96,               /* 2200500000.0 Hz */
    111.921,              /* 2500500000.0 Hz */
    108.328,              /* 2699900000.0 Hz */
};

static double _b6_table_A1_J1_columns[] = { 255.0 };

static double _b6_table_A1_J1_rows[] = {
    2700100000.0,
    3200500000.0,
    3300500000.0,
    3400500000.0,
    3599900000.0,
};

static double _b6_table_A1_J1_measurements[] = {
    112.136,              /* 2700100000.0 Hz */
    111.504,              /* 3200500000.0 Hz */
    111.045,              /* 3300500000.0 Hz */
    110.934,              /* 3400500000.0 Hz */
    110.351,              /* 3599900000.0 Hz */
};

static double _b7_table_A1_J1_columns[] = { 255.0 };

static double _b7_table_A1_J1_rows[] = {
    3600100000.0,
    4380500000.0,
    5100500000.0,
    5800500000.0,
    5998000000.0,
};

static double _b7_table_A1_J1_measurements[] = {
    109.246,              /* 3600100000.0 Hz */
    112.833,              /* 4380500000.0 Hz */
    113.693,              /* 5100500000.0 Hz */
    111.298,              /* 5800500000.0 Hz */
    110.77,               /* 5998000000.0 Hz */
};

static double _bands_table_B1_J1_columns[] = { 0, 1 };

static double _bands_table_B1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_B1_J1_measurements[] = {
    30000000.0,        449999999.0,
    450000000.0,       599999999.0,
    600000000.0,       799999999.0,
    800000000.0,       1199999999.0,
    1200000000.0,      1699999999.0,
    1700000000.0,      2699999999.0,
    2700000000.0,      3599999999.0,
    3600000000.0,      6000000000.0,
};

static double _gain_table_B1_J1_columns[] = { 187.0, 195.0, 200.0, 205.0, 210.0, 215.0, 220.0, 225.0, 230.0, 245.0, 250.0, 255.0 };

static double _gain_table_B1_J1_rows[] = {
    30000000.0,
    102500000.0,
    541500000.0,
    701500000.0,
    987500000.0,
    1400500000.0,
    2375500000.0,
    3200000000.0,
    4000000000.0,
    5000000000.0,
    5500500000.0,
    5998900000.0,
};

static double _gain_table_B1_J1_measurements[] = {
    -34.072,           -29.878,           -27.391,           -24.881,           -22.347,           -19.828,           -17.263,           -14.78,            -12.231,           -4.769,            -2.305,            0.0,                  /*   30000000.0 Hz */
    -33.72,            -29.708,           -27.3,             -24.793,           -22.29,            -19.763,           -17.172,           -14.678,           -12.095,           -4.681,            -2.251,            0.0,                  /*  102500000.0 Hz */
    -33.818,           -29.744,           -27.4,             -24.894,           -22.322,           -19.807,           -17.286,           -14.728,           -12.217,           -4.77,             -2.302,            0.0,                  /*  541500000.0 Hz */
    -33.804,           -29.707,           -27.311,           -24.852,           -22.334,           -19.829,           -17.291,           -14.754,           -12.241,           -4.78,             -2.314,            0.0,                  /*  701500000.0 Hz */
    -33.923,           -29.805,           -27.356,           -24.923,           -22.405,           -19.889,           -17.33,            -14.836,           -12.323,           -4.847,            -2.376,            0.0,                  /*  987500000.0 Hz */
    -33.768,           -29.766,           -27.389,           -24.839,           -22.338,           -19.828,           -17.301,           -14.82,            -12.344,           -4.867,            -2.369,            0.0,                  /* 1400500000.0 Hz */
    -33.57,            -29.689,           -27.33,            -24.871,           -22.399,           -19.89,            -17.378,           -14.901,           -12.401,           -4.954,            -2.421,            0.0,                  /* 2375500000.0 Hz */
    -32.951,           -29.358,           -27.027,           -24.639,           -22.174,           -19.691,           -17.185,           -14.726,           -12.217,           -4.817,            -2.356,            0.0,                  /* 3200000000.0 Hz */
    -32.541,           -29.156,           -26.871,           -24.528,           -22.1,             -19.644,           -17.153,           -14.702,           -12.222,           -4.834,            -2.365,            0.0,                  /* 4000000000.0 Hz */
    -32.155,           -29.104,           -26.901,           -24.582,           -22.239,           -19.782,           -17.319,           -14.879,           -12.422,           -4.991,            -2.486,            0.0,                  /* 5000000000.0 Hz */
    -31.761,           -28.847,           -26.738,           -24.466,           -22.146,           -19.699,           -17.179,           -14.752,           -12.261,           -4.907,            -2.398,            0.0,                  /* 5500500000.0 Hz */
    -31.341,           -28.626,           -26.524,           -24.26,            -21.971,           -19.543,           -17.06,            -14.623,           -12.165,           -4.83,             -2.359,            0.0,                  /* 5998900000.0 Hz */
};

static double _b0_table_B1_J1_columns[] = { 255.0 };

static double _b0_table_B1_J1_rows[] = {
    30100000.0,
    80100000.0,
    130000000.0,
    190000000.0,
    240000000.0,
    290000000.0,
    410000000.0,
    449900000.0,
};

static double _b0_table_B1_J1_measurements[] = {
    121.918,              /*   30100000.0 Hz */
    118.958,              /*   80100000.0 Hz */
    117.815,              /*  130000000.0 Hz */
    117.648,              /*  190000000.0 Hz */
    118.301,              /*  240000000.0 Hz */
    118.048,              /*  290000000.0 Hz */
    114.014,              /*  410000000.0 Hz */
    114.094,              /*  449900000.0 Hz */
};

static double _b1_table_B1_J1_columns[] = { 255.0 };

static double _b1_table_B1_J1_rows[] = {
    450100000.0,
    470100000.0,
    490100000.0,
    510100000.0,
    540100000.0,
    550500000.0,
    599900000.0,
};

static double _b1_table_B1_J1_measurements[] = {
    116.065,              /*  450100000.0 Hz */
    116.702,              /*  470100000.0 Hz */
    116.556,              /*  490100000.0 Hz */
    114.941,              /*  510100000.0 Hz */
    113.932,              /*  540100000.0 Hz */
    114.3,                /*  550500000.0 Hz */
    115.359,              /*  599900000.0 Hz */
};

static double _b2_table_B1_J1_columns[] = { 255.0 };

static double _b2_table_B1_J1_rows[] = {
    600100000.0,
    700500000.0,
    799900000.0,
};

static double _b2_table_B1_J1_measurements[] = {
    116.775,              /*  600100000.0 Hz */
    119.049,              /*  700500000.0 Hz */
    115.989,              /*  799900000.0 Hz */
};

static double _b3_table_B1_J1_columns[] = { 255.0 };

static double _b3_table_B1_J1_rows[] = {
    800100000.0,
    1000500000.0,
    1199900000.0,
};

static double _b3_table_B1_J1_measurements[] = {
    116.968,              /*  800100000.0 Hz */
    112.361,              /* 1000500000.0 Hz */
    109.52,               /* 1199900000.0 Hz */
};

static double _b4_table_B1_J1_columns[] = { 255.0 };

static double _b4_table_B1_J1_rows[] = {
    1200100000.0,
    1500500000.0,
    1699900000.0,
};

static double _b4_table_B1_J1_measurements[] = {
    111.637,              /* 1200100000.0 Hz */
    114.868,              /* 1500500000.0 Hz */
    114.48,               /* 1699900000.0 Hz */
};

static double _b5_table_B1_J1_columns[] = { 255.0 };

static double _b5_table_B1_J1_rows[] = {
    1700100000.0,
    2000100000.0,
    2300500000.0,
    2500500000.0,
    2699900000.0,
};

static double _b5_table_B1_J1_measurements[] = {
    117.045,              /* 1700100000.0 Hz */
    119.115,              /* 2000100000.0 Hz */
    118.517,              /* 2300500000.0 Hz */
    116.795,              /* 2500500000.0 Hz */
    112.816,              /* 2699900000.0 Hz */
};

static double _b6_table_B1_J1_columns[] = { 255.0 };

static double _b6_table_B1_J1_rows[] = {
    2700100000.0,
    3000100000.0,
    3300500000.0,
    3480500000.0,
    3599900000.0,
};

static double _b6_table_B1_J1_measurements[] = {
    115.976,              /* 2700100000.0 Hz */
    114.037,              /* 3000100000.0 Hz */
    110.638,              /* 3300500000.0 Hz */
    109.435,              /* 3480500000.0 Hz */
    109.376,              /* 3599900000.0 Hz */
};

static double _b7_table_B1_J1_columns[] = { 255.0 };

static double _b7_table_B1_J1_rows[] = {
    3600100000.0,
    4300500000.0,
    4400500000.0,
    5100500000.0,
    5300500000.0,
    5400500000.0,
    5840500000.0,
    5998000000.0,
};

static double _b7_table_B1_J1_measurements[] = {
    108.361,              /* 3600100000.0 Hz */
    114.378,              /* 4300500000.0 Hz */
    115.148,              /* 4400500000.0 Hz */
    116.56,               /* 5100500000.0 Hz */
    115.378,              /* 5300500000.0 Hz */
    114.851,              /* 5400500000.0 Hz */
    110.246,              /* 5840500000.0 Hz */
    108.509,              /* 5998000000.0 Hz */
};

static double _bands_table_B1_J2_columns[] = { 0, 1 };

static double _bands_table_B1_J2_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_B1_J2_measurements[] = {
    30000000.0,        449999999.0,
    450000000.0,       599999999.0,
    600000000.0,       799999999.0,
    800000000.0,       1199999999.0,
    1200000000.0,      1699999999.0,
    1700000000.0,      2699999999.0,
    2700000000.0,      3599999999.0,
    3600000000.0,      6000000000.0,
};

static double _gain_table_B1_J2_columns[] = { 187.0, 195.0, 200.0, 205.0, 210.0, 215.0, 220.0, 225.0, 230.0, 245.0, 250.0, 255.0 };

static double _gain_table_B1_J2_rows[] = {
    30000000.0,
    102500000.0,
    541500000.0,
    701500000.0,
    987500000.0,
    1400500000.0,
    2375500000.0,
    3200000000.0,
    4000000000.0,
    5000000000.0,
    5500500000.0,
    5998900000.0,
};

static double _gain_table_B1_J2_measurements[] = {
    -33.962,           -29.83,            -27.384,           -24.859,           -22.354,           -19.833,           -17.29,            -14.802,           -12.275,           -4.81,             -2.336,            0.0,                  /*   30000000.0 Hz */
    -33.655,           -29.584,           -27.131,           -24.762,           -22.259,           -19.738,           -17.163,           -14.661,           -12.075,           -4.665,            -2.254,            0.0,                  /*  102500000.0 Hz */
    -33.813,           -29.698,           -27.361,           -24.868,           -22.33,            -19.812,           -17.282,           -14.744,           -12.23,            -4.784,            -2.324,            0.0,                  /*  541500000.0 Hz */
    -33.772,           -29.667,           -27.307,           -24.833,           -22.304,           -19.824,           -17.266,           -14.753,           -12.218,           -4.78,             -2.313,            0.0,                  /*  701500000.0 Hz */
    -33.798,           -29.763,           -27.275,           -24.906,           -22.359,           -19.844,           -17.289,           -14.808,           -12.282,           -4.84,             -2.347,            0.0,                  /*  987500000.0 Hz */
    -33.702,           -29.716,           -27.317,           -24.885,           -22.347,           -19.831,           -17.313,           -14.834,           -12.307,           -4.884,            -2.388,            0.0,                  /* 1400500000.0 Hz */
    -33.463,           -29.606,           -27.23,            -24.805,           -22.354,           -19.88,            -17.355,           -14.869,           -12.378,           -4.924,            -2.404,            0.0,                  /* 2375500000.0 Hz */
    -32.898,           -29.257,           -26.929,           -24.542,           -22.099,           -19.664,           -17.14,            -14.658,           -12.166,           -4.785,            -2.315,            0.0,                  /* 3200000000.0 Hz */
    -32.487,           -29.092,           -26.803,           -24.463,           -22.073,           -19.638,           -17.123,           -14.676,           -12.208,           -4.807,            -2.36,             0.0,                  /* 4000000000.0 Hz */
    -32.174,           -29.105,           -26.908,           -24.601,           -22.26,            -19.816,           -17.334,           -14.895,           -12.433,           -5.009,            -2.486,            0.0,                  /* 5000000000.0 Hz */
    -31.885,           -28.855,           -26.71,            -24.449,           -22.159,           -19.737,           -17.241,           -14.795,           -12.306,           -4.928,            -2.441,            0.0,                  /* 5500500000.0 Hz */
    -31.182,           -28.554,           -26.477,           -24.214,           -21.953,           -19.536,           -17.062,           -14.644,           -12.171,           -4.819,            -2.364,            0.0,                  /* 5998900000.0 Hz */
};

static double _b0_table_B1_J2_columns[] = { 255.0 };

static double _b0_table_B1_J2_rows[] = {
    30100000.0,
    40100000.0,
    130000000.0,
    270000000.0,
    410000000.0,
    449900000.0,
};

static double _b0_table_B1_J2_measurements[] = {
    114.616,              /*   30100000.0 Hz */
    114.415,              /*   40100000.0 Hz */
    112.828,              /*  130000000.0 Hz */
    112.591,              /*  270000000.0 Hz */
    109.79,               /*  410000000.0 Hz */
    109.52,               /*  449900000.0 Hz */
};

static double _b1_table_B1_J2_columns[] = { 255.0 };

static double _b1_table_B1_J2_rows[] = {
    450100000.0,
    490100000.0,
    550500000.0,
    599900000.0,
};

static double _b1_table_B1_J2_measurements[] = {
    111.139,              /*  450100000.0 Hz */
    111.027,              /*  490100000.0 Hz */
    111.227,              /*  550500000.0 Hz */
    109.409,              /*  599900000.0 Hz */
};

static double _b2_table_B1_J2_columns[] = { 255.0 };

static double _b2_table_B1_J2_rows[] = {
    600100000.0,
    700500000.0,
    799900000.0,
};

static double _b2_table_B1_J2_measurements[] = {
    110.888,              /*  600100000.0 Hz */
    111.192,              /*  700500000.0 Hz */
    109.444,              /*  799900000.0 Hz */
};

static double _b3_table_B1_J2_columns[] = { 255.0 };

static double _b3_table_B1_J2_rows[] = {
    800100000.0,
    880500000.0,
    1060500000.0,
    1199900000.0,
};

static double _b3_table_B1_J2_measurements[] = {
    110.419,              /*  800100000.0 Hz */
    110.967,              /*  880500000.0 Hz */
    110.32,               /* 1060500000.0 Hz */
    107.974,              /* 1199900000.0 Hz */
};

static double _b4_table_B1_J2_columns[] = { 255.0 };

static double _b4_table_B1_J2_rows[] = {
    1200100000.0,
    1480500000.0,
    1600500000.0,
    1699900000.0,
};

static double _b4_table_B1_J2_measurements[] = {
    110.51,               /* 1200100000.0 Hz */
    110.411,              /* 1480500000.0 Hz */
    109.582,              /* 1600500000.0 Hz */
    108.451,              /* 1699900000.0 Hz */
};

static double _b5_table_B1_J2_columns[] = { 255.0 };

static double _b5_table_B1_J2_rows[] = {
    1700100000.0,
    2020500000.0,
    2200500000.0,
    2400500000.0,
    2699900000.0,
};

static double _b5_table_B1_J2_measurements[] = {
    110.572,              /* 1700100000.0 Hz */
    111.875,              /* 2020500000.0 Hz */
    111.663,              /* 2200500000.0 Hz */
    110.989,              /* 2400500000.0 Hz */
    107.223,              /* 2699900000.0 Hz */
};

static double _b6_table_B1_J2_columns[] = { 255.0 };

static double _b6_table_B1_J2_rows[] = {
    2700100000.0,
    3200500000.0,
    3300500000.0,
    3400500000.0,
    3599900000.0,
};

static double _b6_table_B1_J2_measurements[] = {
    110.514,              /* 2700100000.0 Hz */
    110.238,              /* 3200500000.0 Hz */
    109.705,              /* 3300500000.0 Hz */
    109.423,              /* 3400500000.0 Hz */
    108.298,              /* 3599900000.0 Hz */
};

static double _b7_table_B1_J2_columns[] = { 255.0 };

static double _b7_table_B1_J2_rows[] = {
    3600100000.0,
    4000100000.0,
    4360500000.0,
    5000500000.0,
    5500500000.0,
    5998000000.0,
};

static double _b7_table_B1_J2_measurements[] = {
    107.665,              /* 3600100000.0 Hz */
    110.171,              /* 4000100000.0 Hz */
    110.884,              /* 4360500000.0 Hz */
    110.227,              /* 5000500000.0 Hz */
    109.283,              /* 5500500000.0 Hz */
    106.459,              /* 5998000000.0 Hz */
};

static double _corrections_table_A1_J1_B1_J1_columns[] = { 255.0 };

static double _corrections_table_A1_J1_B1_J1_rows[] = {
    30000000.0,
    60000000.0,
    99000000.0,
    149000000.0,
    199000000.0,
    249000000.0,
    299000000.0,
    349000000.0,
    399000000.0,
    449000000.0,
    499000000.0,
    549000000.0,
    599000000.0,
    649000000.0,
    699000000.0,
    749000000.0,
    799000000.0,
    849000000.0,
    899000000.0,
    949000000.0,
    999000000.0,
    1049000000.0,
    1099000000.0,
    1149000000.0,
    1199000000.0,
    1249000000.0,
    1299000000.0,
    1349000000.0,
    1399000000.0,
    1449000000.0,
    1499000000.0,
    1549000000.0,
    1599000000.0,
    1649000000.0,
    1699000000.0,
    1749000000.0,
    1799000000.0,
    1849000000.0,
    1899000000.0,
    1949000000.0,
    1999000000.0,
    2049000000.0,
    2099000000.0,
    2149000000.0,
    2199000000.0,
    2249000000.0,
    2299000000.0,
    2349000000.0,
    2399000000.0,
    2449000000.0,
    2499000000.0,
    2549000000.0,
    2599000000.0,
    2649000000.0,
    2699000000.0,
    2749000000.0,
    2799000000.0,
    2849000000.0,
    2899000000.0,
    2949000000.0,
    2999000000.0,
    3049000000.0,
    3099000000.0,
    3149000000.0,
    3199000000.0,
    3249000000.0,
    3299000000.0,
    3349000000.0,
    3399000000.0,
    3449000000.0,
    3499000000.0,
    3549000000.0,
    3599000000.0,
    3649000000.0,
    3699000000.0,
    3749000000.0,
    3799000000.0,
    3849000000.0,
    3899000000.0,
    3949000000.0,
    3999000000.0,
    4049000000.0,
    4099000000.0,
    4149000000.0,
    4199000000.0,
    4249000000.0,
    4299000000.0,
    4349000000.0,
    4399000000.0,
    4449000000.0,
    4499000000.0,
    4549000000.0,
    4599000000.0,
    4649000000.0,
    4699000000.0,
    4749000000.0,
    4799000000.0,
    4849000000.0,
    4899000000.0,
    4949000000.0,
    4999000000.0,
    5049000000.0,
    5099000000.0,
    5149000000.0,
    5199000000.0,
    5249000000.0,
    5299000000.0,
    5349000000.0,
    5399000000.0,
    5449000000.0,
    5499000000.0,
    5549000000.0,
    5599000000.0,
    5649000000.0,
    5699000000.0,
    5749000000.0,
    5799000000.0,
    5849000000.0,
    5899000000.0,
    5949000000.0,
    5999000000.0,
};

static double _corrections_table_A1_J1_B1_J1_measurements[] = {
    -0.22,                /*   30000000.0 Hz */
    -0.25,                /*   60000000.0 Hz */
    -0.23,                /*   99000000.0 Hz */
    -0.26,                /*  149000000.0 Hz */
    -0.27,                /*  199000000.0 Hz */
    -0.26,                /*  249000000.0 Hz */
    -0.2,                 /*  299000000.0 Hz */
    -0.2,                 /*  349000000.0 Hz */
    -0.22,                /*  399000000.0 Hz */
    -0.25,                /*  449000000.0 Hz */
    -0.21,                /*  499000000.0 Hz */
    -0.22,                /*  549000000.0 Hz */
    -0.22,                /*  599000000.0 Hz */
    -0.18,                /*  649000000.0 Hz */
    -0.2,                 /*  699000000.0 Hz */
    -0.17,                /*  749000000.0 Hz */
    -0.26,                /*  799000000.0 Hz */
    -0.23,                /*  849000000.0 Hz */
    -0.22,                /*  899000000.0 Hz */
    -0.21,                /*  949000000.0 Hz */
    -0.18,                /*  999000000.0 Hz */
    -0.19,                /* 1049000000.0 Hz */
    -0.21,                /* 1099000000.0 Hz */
    -0.24,                /* 1149000000.0 Hz */
    -0.27,                /* 1199000000.0 Hz */
    -0.22,                /* 1249000000.0 Hz */
    -0.26,                /* 1299000000.0 Hz */
    -0.19,                /* 1349000000.0 Hz */
    -0.16,                /* 1399000000.0 Hz */
    -0.19,                /* 1449000000.0 Hz */
    -0.24,                /* 1499000000.0 Hz */
    -0.25,                /* 1549000000.0 Hz */
    -0.3,                 /* 1599000000.0 Hz */
    -0.2,                 /* 1649000000.0 Hz */
    -0.19,                /* 1699000000.0 Hz */
    -0.2,                 /* 1749000000.0 Hz */
    -0.39,                /* 1799000000.0 Hz */
    -0.26,                /* 1849000000.0 Hz */
    -0.27,                /* 1899000000.0 Hz */
    -0.26,                /* 1949000000.0 Hz */
    -0.13,                /* 1999000000.0 Hz */
    -0.18,                /* 2049000000.0 Hz */
    -0.23,                /* 2099000000.0 Hz */
    -0.25,                /* 2149000000.0 Hz */
    -0.66,                /* 2199000000.0 Hz */
    -0.2,                 /* 2249000000.0 Hz */
    -0.23,                /* 2299000000.0 Hz */
    -0.21,                /* 2349000000.0 Hz */
    -0.21,                /* 2399000000.0 Hz */
    -0.24,                /* 2449000000.0 Hz */
    -0.25,                /* 2499000000.0 Hz */
    -0.2,                 /* 2549000000.0 Hz */
    -0.19,                /* 2599000000.0 Hz */
    -0.23,                /* 2649000000.0 Hz */
    -0.23,                /* 2699000000.0 Hz */
    -0.24,                /* 2749000000.0 Hz */
    -0.28,                /* 2799000000.0 Hz */
    -0.21,                /* 2849000000.0 Hz */
    -0.24,                /* 2899000000.0 Hz */
    -0.23,                /* 2949000000.0 Hz */
    -0.18,                /* 2999000000.0 Hz */
    -0.19,                /* 3049000000.0 Hz */
    -0.22,                /* 3099000000.0 Hz */
    -0.17,                /* 3149000000.0 Hz */
    -0.11,                /* 3199000000.0 Hz */
    -0.22,                /* 3249000000.0 Hz */
    -0.19,                /* 3299000000.0 Hz */
    -0.19,                /* 3349000000.0 Hz */
    -0.17,                /* 3399000000.0 Hz */
    -0.14,                /* 3449000000.0 Hz */
    -0.17,                /* 3499000000.0 Hz */
    -0.17,                /* 3549000000.0 Hz */
    -0.21,                /* 3599000000.0 Hz */
    -0.13,                /* 3649000000.0 Hz */
    -0.2,                 /* 3699000000.0 Hz */
    -0.25,                /* 3749000000.0 Hz */
    -0.13,                /* 3799000000.0 Hz */
    -0.24,                /* 3849000000.0 Hz */
    -0.17,                /* 3899000000.0 Hz */
    -0.23,                /* 3949000000.0 Hz */
    -0.33,                /* 3999000000.0 Hz */
    -0.24,                /* 4049000000.0 Hz */
    -0.22,                /* 4099000000.0 Hz */
    -0.21,                /* 4149000000.0 Hz */
    -0.17,                /* 4199000000.0 Hz */
    -0.21,                /* 4249000000.0 Hz */
    -0.19,                /* 4299000000.0 Hz */
    -0.16,                /* 4349000000.0 Hz */
    -0.09,                /* 4399000000.0 Hz */
    -0.21,                /* 4449000000.0 Hz */
    -0.43,                /* 4499000000.0 Hz */
    -0.27,                /* 4549000000.0 Hz */
    -0.36,                /* 4599000000.0 Hz */
    -0.22,                /* 4649000000.0 Hz */
    -0.26,                /* 4699000000.0 Hz */
    -0.21,                /* 4749000000.0 Hz */
    -0.73,                /* 4799000000.0 Hz */
    -0.25,                /* 4849000000.0 Hz */
    -0.2,                 /* 4899000000.0 Hz */
    -0.21,                /* 4949000000.0 Hz */
    -0.3,                 /* 4999000000.0 Hz */
    -0.19,                /* 5049000000.0 Hz */
    -0.22,                /* 5099000000.0 Hz */
    -0.23,                /* 5149000000.0 Hz */
    -0.16,                /* 5199000000.0 Hz */
    -0.23,                /* 5249000000.0 Hz */
    -0.23,                /* 5299000000.0 Hz */
    -0.19,                /* 5349000000.0 Hz */
    -0.09,                /* 5399000000.0 Hz */
    -0.27,                /* 5449000000.0 Hz */
    -0.16,                /* 5499000000.0 Hz */
    -0.22,                /* 5549000000.0 Hz */
    -0.22,                /* 5599000000.0 Hz */
    -0.2,                 /* 5649000000.0 Hz */
    -0.19,                /* 5699000000.0 Hz */
    0.92,                 /* 5749000000.0 Hz */
    1.18,                 /* 5799000000.0 Hz */
    0.96,                 /* 5849000000.0 Hz */
    0.98,                 /* 5899000000.0 Hz */
    0.97,                 /* 5949000000.0 Hz */
    0.91,                 /* 5999000000.0 Hz */
};

struct card_calibration nv100_default_cal = {
    .rx_cal_by_hdl_port = {
        [skiq_rx_hdl_A1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b0_table_A1_J1_columns,
                .row_headers = _b0_table_A1_J1_rows,
                .measurements = _b0_table_A1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b1_table_A1_J1_columns,
                .row_headers = _b1_table_A1_J1_rows,
                .measurements = _b1_table_A1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_A1_J1_columns,
                .row_headers = _b2_table_A1_J1_rows,
                .measurements = _b2_table_A1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_A1_J1_columns,
                .row_headers = _b3_table_A1_J1_rows,
                .measurements = _b3_table_A1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_A1_J1_columns,
                .row_headers = _b4_table_A1_J1_rows,
                .measurements = _b4_table_A1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b5_table_A1_J1_columns,
                .row_headers = _b5_table_A1_J1_rows,
                .measurements = _b5_table_A1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b6_table_A1_J1_columns,
                .row_headers = _b6_table_A1_J1_rows,
                .measurements = _b6_table_A1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b7_table_A1_J1_columns,
                .row_headers = _b7_table_A1_J1_rows,
                .measurements = _b7_table_A1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_A1_J1_columns,
                .row_headers = _bands_table_A1_J1_rows,
                .measurements = _bands_table_A1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 5,
                .nr_rows = 12,
                .column_headers = _gain_table_A1_J1_columns,
                .row_headers = _gain_table_A1_J1_rows,
                .measurements = _gain_table_A1_J1_measurements,
            },
        },
        [skiq_rx_hdl_B1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b0_table_B1_J1_columns,
                .row_headers = _b0_table_B1_J1_rows,
                .measurements = _b0_table_B1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b1_table_B1_J1_columns,
                .row_headers = _b1_table_B1_J1_rows,
                .measurements = _b1_table_B1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_B1_J1_columns,
                .row_headers = _b2_table_B1_J1_rows,
                .measurements = _b2_table_B1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b3_table_B1_J1_columns,
                .row_headers = _b3_table_B1_J1_rows,
                .measurements = _b3_table_B1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b4_table_B1_J1_columns,
                .row_headers = _b4_table_B1_J1_rows,
                .measurements = _b4_table_B1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b5_table_B1_J1_columns,
                .row_headers = _b5_table_B1_J1_rows,
                .measurements = _b5_table_B1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b6_table_B1_J1_columns,
                .row_headers = _b6_table_B1_J1_rows,
                .measurements = _b6_table_B1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 8,
                .column_headers = _b7_table_B1_J1_columns,
                .row_headers = _b7_table_B1_J1_rows,
                .measurements = _b7_table_B1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_B1_J1_columns,
                .row_headers = _bands_table_B1_J1_rows,
                .measurements = _bands_table_B1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 12,
                .nr_rows = 12,
                .column_headers = _gain_table_B1_J1_columns,
                .row_headers = _gain_table_B1_J1_rows,
                .measurements = _gain_table_B1_J1_measurements,
            },
        },
        [skiq_rx_hdl_B1][skiq_rf_port_J2] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b0_table_B1_J2_columns,
                .row_headers = _b0_table_B1_J2_rows,
                .measurements = _b0_table_B1_J2_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b1_table_B1_J2_columns,
                .row_headers = _b1_table_B1_J2_rows,
                .measurements = _b1_table_B1_J2_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 3,
                .column_headers = _b2_table_B1_J2_columns,
                .row_headers = _b2_table_B1_J2_rows,
                .measurements = _b2_table_B1_J2_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b3_table_B1_J2_columns,
                .row_headers = _b3_table_B1_J2_rows,
                .measurements = _b3_table_B1_J2_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 4,
                .column_headers = _b4_table_B1_J2_columns,
                .row_headers = _b4_table_B1_J2_rows,
                .measurements = _b4_table_B1_J2_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b5_table_B1_J2_columns,
                .row_headers = _b5_table_B1_J2_rows,
                .measurements = _b5_table_B1_J2_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b6_table_B1_J2_columns,
                .row_headers = _b6_table_B1_J2_rows,
                .measurements = _b6_table_B1_J2_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b7_table_B1_J2_columns,
                .row_headers = _b7_table_B1_J2_rows,
                .measurements = _b7_table_B1_J2_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_B1_J2_columns,
                .row_headers = _bands_table_B1_J2_rows,
                .measurements = _bands_table_B1_J2_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 12,
                .nr_rows = 12,
                .column_headers = _gain_table_B1_J2_columns,
                .row_headers = _gain_table_B1_J2_rows,
                .measurements = _gain_table_B1_J2_measurements,
            },
        },
    },
    .rx_cal_corrections_by_hdl_port = {
        [skiq_rx_hdl_A1][skiq_rf_port_J1] = {
            .corrections_by_hdl_port = {
                [skiq_rx_hdl_B1][skiq_rf_port_J1] = {
                    .name = "corrections",
                    .nr_cols = 1,
                    .nr_rows = 121,
                    .column_headers = _corrections_table_A1_J1_B1_J1_columns,
                    .row_headers = _corrections_table_A1_J1_B1_J1_rows,
                    .measurements = _corrections_table_A1_J1_B1_J1_measurements,
                },
            },
        },
    },
};
