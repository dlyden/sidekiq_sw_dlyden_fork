#include "calibration_private.h"

/* file generated on 2018-09-17T15:18:24.155012+00:00 */

static double _bands_table_A1_J1_columns[] = { 0, 1 };

static double _bands_table_A1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_A1_J1_measurements[] = {
    1000000.0,         439999999.0,
    440000000.0,       579999999.0,
    580000000.0,       809999999.0,
    810000000.0,       1169999999.0,
    1170000000.0,      1694999999.0,
    1695000000.0,      2539999999.0,
    2540000000.0,      3839999999.0,
    3840000000.0,      6000000000.0,
};

static double _gain_table_A1_J1_columns[] = { 195.0, 234.0, 235.0, 255.0 };

static double _gain_table_A1_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_A1_J1_measurements[] = {
    -29.656,           -10.414,           -9.963,            0.0,                  /*  500000000.0 Hz */
    -29.185,           -10.337,           -9.824,            0.0,                  /* 3000000000.0 Hz */
    -28.669,           -9.864,            -9.428,            0.0,                  /* 4999000000.0 Hz */
    -27.339,           -10.023,           -9.519,            0.0,                  /* 5000000000.0 Hz */
    -28.574,           -9.66,             -9.177,            0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_A1_J1_columns[] = { 255.0 };

static double _b0_table_A1_J1_rows[] = {
    1000000.0,
    2000000.0,
    4000000.0,
    10000000.0,
    75000000.0,
    225000000.0,
    325000000.0,
    440000000.0,
    480000000.0,
};

static double _b0_table_A1_J1_measurements[] = {
    127.705,              /*    1000000.0 Hz */
    127.611,              /*    2000000.0 Hz */
    127.775,              /*    4000000.0 Hz */
    127.783,              /*   10000000.0 Hz */
    127.698,              /*   75000000.0 Hz */
    116.466,              /*  225000000.0 Hz */
    113.666,              /*  325000000.0 Hz */
    112.904,              /*  440000000.0 Hz */
    112.909,              /*  480000000.0 Hz */
};

static double _b1_table_A1_J1_columns[] = { 255.0 };

static double _b1_table_A1_J1_rows[] = {
    400000000.0,
    440000000.0,
    550000000.0,
    580000000.0,
    600000000.0,
    620000000.0,
};

static double _b1_table_A1_J1_measurements[] = {
    120.254,              /*  400000000.0 Hz */
    121.194,              /*  440000000.0 Hz */
    120.942,              /*  550000000.0 Hz */
    119.783,              /*  580000000.0 Hz */
    119.238,              /*  600000000.0 Hz */
    118.145,              /*  620000000.0 Hz */
};

static double _b2_table_A1_J1_columns[] = { 255.0 };

static double _b2_table_A1_J1_rows[] = {
    540000000.0,
    580000000.0,
    600000000.0,
    680000000.0,
    810000000.0,
    850000000.0,
};

static double _b2_table_A1_J1_measurements[] = {
    119.338,              /*  540000000.0 Hz */
    120.219,              /*  580000000.0 Hz */
    120.27,               /*  600000000.0 Hz */
    120.474,              /*  680000000.0 Hz */
    118.3,                /*  810000000.0 Hz */
    117.182,              /*  850000000.0 Hz */
};

static double _b3_table_A1_J1_columns[] = { 255.0 };

static double _b3_table_A1_J1_rows[] = {
    780000000.0,
    810000000.0,
    890000000.0,
    1030000000.0,
    1170000000.0,
    1210000000.0,
};

static double _b3_table_A1_J1_measurements[] = {
    119.043,              /*  780000000.0 Hz */
    119.298,              /*  810000000.0 Hz */
    119.791,              /*  890000000.0 Hz */
    119.411,              /* 1030000000.0 Hz */
    117.703,              /* 1170000000.0 Hz */
    117.197,              /* 1210000000.0 Hz */
};

static double _b4_table_A1_J1_columns[] = { 255.0 };

static double _b4_table_A1_J1_rows[] = {
    1130000000.0,
    1170000000.0,
    1240000000.0,
    1510000000.0,
    1610000000.0,
    1695000000.0,
    1735000000.0,
};

static double _b4_table_A1_J1_measurements[] = {
    118.14,               /* 1130000000.0 Hz */
    118.532,              /* 1170000000.0 Hz */
    119.399,              /* 1240000000.0 Hz */
    119.064,              /* 1510000000.0 Hz */
    117.817,              /* 1610000000.0 Hz */
    117.352,              /* 1695000000.0 Hz */
    116.968,              /* 1735000000.0 Hz */
};

static double _b5_table_A1_J1_columns[] = { 255.0 };

static double _b5_table_A1_J1_rows[] = {
    1665000000.0,
    1695000000.0,
    1800000000.0,
    2400000000.0,
    2500000000.0,
    2540000000.0,
    2580000000.0,
};

static double _b5_table_A1_J1_measurements[] = {
    117.565,              /* 1665000000.0 Hz */
    117.715,              /* 1695000000.0 Hz */
    118.116,              /* 1800000000.0 Hz */
    117.12,               /* 2400000000.0 Hz */
    117.13,               /* 2500000000.0 Hz */
    116.781,              /* 2540000000.0 Hz */
    116.618,              /* 2580000000.0 Hz */
};

static double _b6_table_A1_J1_columns[] = { 255.0 };

static double _b6_table_A1_J1_rows[] = {
    2500000000.0,
    2540000000.0,
    2600000000.0,
    2800000000.0,
    3000000000.0,
    3500000000.0,
    3800000000.0,
    3840000000.0,
    3880000000.0,
};

static double _b6_table_A1_J1_measurements[] = {
    116.778,              /* 2500000000.0 Hz */
    117.003,              /* 2540000000.0 Hz */
    117.006,              /* 2600000000.0 Hz */
    117.509,              /* 2800000000.0 Hz */
    116.63,               /* 3000000000.0 Hz */
    113.378,              /* 3500000000.0 Hz */
    112.54,               /* 3800000000.0 Hz */
    112.471,              /* 3840000000.0 Hz */
    112.36,               /* 3880000000.0 Hz */
};

static double _b7_table_A1_J1_columns[] = { 255.0 };

static double _b7_table_A1_J1_rows[] = {
    3800000000.0,
    3840000000.0,
    4000000000.0,
    5000000000.0,
    6000000000.0,
};

static double _b7_table_A1_J1_measurements[] = {
    112.957,              /* 3800000000.0 Hz */
    113.264,              /* 3840000000.0 Hz */
    114.465,              /* 4000000000.0 Hz */
    109.92,               /* 5000000000.0 Hz */
    109.024,              /* 6000000000.0 Hz */
};

static double _bands_table_A2_J1_columns[] = { 0, 1 };

static double _bands_table_A2_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_A2_J1_measurements[] = {
    1000000.0,         439999999.0,
    440000000.0,       579999999.0,
    580000000.0,       809999999.0,
    810000000.0,       1169999999.0,
    1170000000.0,      1694999999.0,
    1695000000.0,      2539999999.0,
    2540000000.0,      3839999999.0,
    3840000000.0,      6000000000.0,
};

static double _gain_table_A2_J1_columns[] = { 195.0, 234.0, 235.0, 255.0 };

static double _gain_table_A2_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_A2_J1_measurements[] = {
    -29.658,           -10.415,           -9.94,             0.0,                  /*  500000000.0 Hz */
    -29.197,           -10.26,            -9.748,            0.0,                  /* 3000000000.0 Hz */
    -27.653,           -8.602,            -8.004,            0.0,                  /* 4999000000.0 Hz */
    -27.465,           -9.933,            -9.425,            0.0,                  /* 5000000000.0 Hz */
    -27.797,           -9.618,            -9.11,             0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_A2_J1_columns[] = { 255.0 };

static double _b0_table_A2_J1_rows[] = {
    1000000.0,
    2000000.0,
    4000000.0,
    10000000.0,
    75000000.0,
    225000000.0,
    325000000.0,
    440000000.0,
    480000000.0,
};

static double _b0_table_A2_J1_measurements[] = {
    142.101,              /*    1000000.0 Hz */
    142.131,              /*    2000000.0 Hz */
    141.726,              /*    4000000.0 Hz */
    142.121,              /*   10000000.0 Hz */
    142.057,              /*   75000000.0 Hz */
    114.737,              /*  225000000.0 Hz */
    113.678,              /*  325000000.0 Hz */
    113.692,              /*  440000000.0 Hz */
    113.083,              /*  480000000.0 Hz */
};

static double _b1_table_A2_J1_columns[] = { 255.0 };

static double _b1_table_A2_J1_rows[] = {
    400000000.0,
    440000000.0,
    550000000.0,
    580000000.0,
    600000000.0,
    620000000.0,
};

static double _b1_table_A2_J1_measurements[] = {
    120.05,               /*  400000000.0 Hz */
    120.831,              /*  440000000.0 Hz */
    120.541,              /*  550000000.0 Hz */
    119.325,              /*  580000000.0 Hz */
    118.776,              /*  600000000.0 Hz */
    117.585,              /*  620000000.0 Hz */
};

static double _b2_table_A2_J1_columns[] = { 255.0 };

static double _b2_table_A2_J1_rows[] = {
    540000000.0,
    580000000.0,
    600000000.0,
    680000000.0,
    810000000.0,
    850000000.0,
};

static double _b2_table_A2_J1_measurements[] = {
    119.023,              /*  540000000.0 Hz */
    119.927,              /*  580000000.0 Hz */
    120.02,               /*  600000000.0 Hz */
    120.301,              /*  680000000.0 Hz */
    118.575,              /*  810000000.0 Hz */
    117.51,               /*  850000000.0 Hz */
};

static double _b3_table_A2_J1_columns[] = { 255.0 };

static double _b3_table_A2_J1_rows[] = {
    780000000.0,
    810000000.0,
    890000000.0,
    1030000000.0,
    1170000000.0,
    1210000000.0,
};

static double _b3_table_A2_J1_measurements[] = {
    118.778,              /*  780000000.0 Hz */
    119.141,              /*  810000000.0 Hz */
    119.773,              /*  890000000.0 Hz */
    119.419,              /* 1030000000.0 Hz */
    117.463,              /* 1170000000.0 Hz */
    116.748,              /* 1210000000.0 Hz */
};

static double _b4_table_A2_J1_columns[] = { 255.0 };

static double _b4_table_A2_J1_rows[] = {
    1130000000.0,
    1170000000.0,
    1240000000.0,
    1510000000.0,
    1610000000.0,
    1695000000.0,
    1735000000.0,
};

static double _b4_table_A2_J1_measurements[] = {
    118.229,              /* 1130000000.0 Hz */
    118.57,               /* 1170000000.0 Hz */
    119.089,              /* 1240000000.0 Hz */
    117.931,              /* 1510000000.0 Hz */
    116.881,              /* 1610000000.0 Hz */
    116.274,              /* 1695000000.0 Hz */
    115.753,              /* 1735000000.0 Hz */
};

static double _b5_table_A2_J1_columns[] = { 255.0 };

static double _b5_table_A2_J1_rows[] = {
    1665000000.0,
    1695000000.0,
    1800000000.0,
    2400000000.0,
    2500000000.0,
    2540000000.0,
    2580000000.0,
};

static double _b5_table_A2_J1_measurements[] = {
    116.329,              /* 1665000000.0 Hz */
    116.656,              /* 1695000000.0 Hz */
    117.285,              /* 1800000000.0 Hz */
    117.252,              /* 2400000000.0 Hz */
    116.944,              /* 2500000000.0 Hz */
    116.47,               /* 2540000000.0 Hz */
    116.134,              /* 2580000000.0 Hz */
};

static double _b6_table_A2_J1_columns[] = { 255.0 };

static double _b6_table_A2_J1_rows[] = {
    2500000000.0,
    2540000000.0,
    2600000000.0,
    2800000000.0,
    3000000000.0,
    3500000000.0,
    3800000000.0,
    3840000000.0,
    3880000000.0,
};

static double _b6_table_A2_J1_measurements[] = {
    116.523,              /* 2500000000.0 Hz */
    116.544,              /* 2540000000.0 Hz */
    116.166,              /* 2600000000.0 Hz */
    116.367,              /* 2800000000.0 Hz */
    116.395,              /* 3000000000.0 Hz */
    114.399,              /* 3500000000.0 Hz */
    110.889,              /* 3800000000.0 Hz */
    110.595,              /* 3840000000.0 Hz */
    110.212,              /* 3880000000.0 Hz */
};

static double _b7_table_A2_J1_columns[] = { 255.0 };

static double _b7_table_A2_J1_rows[] = {
    3800000000.0,
    3840000000.0,
    4000000000.0,
    5000000000.0,
    6000000000.0,
};

static double _b7_table_A2_J1_measurements[] = {
    111.033,              /* 3800000000.0 Hz */
    110.919,              /* 3840000000.0 Hz */
    111.262,              /* 4000000000.0 Hz */
    109.429,              /* 5000000000.0 Hz */
    107.21,               /* 6000000000.0 Hz */
};

static double _bands_table_B1_J1_columns[] = { 0, 1 };

static double _bands_table_B1_J1_rows[] = {
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    6.0,
    7.0,
};

static double _bands_table_B1_J1_measurements[] = {
    1000000.0,         439999999.0,
    440000000.0,       579999999.0,
    580000000.0,       809999999.0,
    810000000.0,       1169999999.0,
    1170000000.0,      1694999999.0,
    1695000000.0,      2539999999.0,
    2540000000.0,      3839999999.0,
    3840000000.0,      6000000000.0,
};

static double _gain_table_B1_J1_columns[] = { 237.0, 244.0, 245.0, 255.0 };

static double _gain_table_B1_J1_rows[] = {
    500000000.0,
    3000000000.0,
    4999000000.0,
    5000000000.0,
    6000000000.0,
};

static double _gain_table_B1_J1_measurements[] = {
    -17.679,           -10.882,           -9.849,            0.0,                  /*  500000000.0 Hz */
    -17.331,           -10.582,           -9.632,            0.0,                  /* 3000000000.0 Hz */
    -16.077,           -9.687,            -8.747,            0.0,                  /* 4999000000.0 Hz */
    -16.523,           -10.116,           -8.93,             0.0,                  /* 5000000000.0 Hz */
    -15.759,           -9.757,            -8.816,            0.0,                  /* 6000000000.0 Hz */
};

static double _b0_table_B1_J1_columns[] = { 255.0 };

static double _b0_table_B1_J1_rows[] = {
    1000000.0,
    2000000.0,
    4000000.0,
    10000000.0,
    75000000.0,
    225000000.0,
    325000000.0,
    440000000.0,
    480000000.0,
};

static double _b0_table_B1_J1_measurements[] = {
    138.441,              /*    1000000.0 Hz */
    138.415,              /*    2000000.0 Hz */
    138.396,              /*    4000000.0 Hz */
    138.677,              /*   10000000.0 Hz */
    138.58,               /*   75000000.0 Hz */
    115.341,              /*  225000000.0 Hz */
    112.525,              /*  325000000.0 Hz */
    110.808,              /*  440000000.0 Hz */
    110.648,              /*  480000000.0 Hz */
};

static double _b1_table_B1_J1_columns[] = { 255.0 };

static double _b1_table_B1_J1_rows[] = {
    400000000.0,
    440000000.0,
    550000000.0,
    580000000.0,
    600000000.0,
    620000000.0,
};

static double _b1_table_B1_J1_measurements[] = {
    119.615,              /*  400000000.0 Hz */
    120.589,              /*  440000000.0 Hz */
    119.892,              /*  550000000.0 Hz */
    119.058,              /*  580000000.0 Hz */
    118.209,              /*  600000000.0 Hz */
    117.187,              /*  620000000.0 Hz */
};

static double _b2_table_B1_J1_columns[] = { 255.0 };

static double _b2_table_B1_J1_rows[] = {
    540000000.0,
    580000000.0,
    600000000.0,
    680000000.0,
    810000000.0,
    850000000.0,
};

static double _b2_table_B1_J1_measurements[] = {
    118.665,              /*  540000000.0 Hz */
    119.608,              /*  580000000.0 Hz */
    119.666,              /*  600000000.0 Hz */
    119.748,              /*  680000000.0 Hz */
    117.358,              /*  810000000.0 Hz */
    116.171,              /*  850000000.0 Hz */
};

static double _b3_table_B1_J1_columns[] = { 255.0 };

static double _b3_table_B1_J1_rows[] = {
    780000000.0,
    810000000.0,
    890000000.0,
    1030000000.0,
    1170000000.0,
    1210000000.0,
};

static double _b3_table_B1_J1_measurements[] = {
    118.256,              /*  780000000.0 Hz */
    118.522,              /*  810000000.0 Hz */
    118.835,              /*  890000000.0 Hz */
    118.22,               /* 1030000000.0 Hz */
    116.383,              /* 1170000000.0 Hz */
    115.788,              /* 1210000000.0 Hz */
};

static double _b4_table_B1_J1_columns[] = { 255.0 };

static double _b4_table_B1_J1_rows[] = {
    1130000000.0,
    1170000000.0,
    1240000000.0,
    1510000000.0,
    1610000000.0,
    1695000000.0,
    1735000000.0,
};

static double _b4_table_B1_J1_measurements[] = {
    116.867,              /* 1130000000.0 Hz */
    117.26,               /* 1170000000.0 Hz */
    117.95,               /* 1240000000.0 Hz */
    117.751,              /* 1510000000.0 Hz */
    116.761,              /* 1610000000.0 Hz */
    116.155,              /* 1695000000.0 Hz */
    115.762,              /* 1735000000.0 Hz */
};

static double _b5_table_B1_J1_columns[] = { 255.0 };

static double _b5_table_B1_J1_rows[] = {
    1665000000.0,
    1695000000.0,
    1800000000.0,
    2400000000.0,
    2500000000.0,
    2540000000.0,
    2580000000.0,
};

static double _b5_table_B1_J1_measurements[] = {
    116.748,              /* 1665000000.0 Hz */
    117.136,              /* 1695000000.0 Hz */
    117.535,              /* 1800000000.0 Hz */
    116.619,              /* 2400000000.0 Hz */
    116.033,              /* 2500000000.0 Hz */
    115.822,              /* 2540000000.0 Hz */
    115.391,              /* 2580000000.0 Hz */
};

static double _b6_table_B1_J1_columns[] = { 255.0 };

static double _b6_table_B1_J1_rows[] = {
    2500000000.0,
    2540000000.0,
    2600000000.0,
    2800000000.0,
    3000000000.0,
    3500000000.0,
    3800000000.0,
    3840000000.0,
    3880000000.0,
};

static double _b6_table_B1_J1_measurements[] = {
    115.848,              /* 2500000000.0 Hz */
    115.911,              /* 2540000000.0 Hz */
    115.637,              /* 2600000000.0 Hz */
    115.889,              /* 2800000000.0 Hz */
    116.047,              /* 3000000000.0 Hz */
    115.273,              /* 3500000000.0 Hz */
    112.681,              /* 3800000000.0 Hz */
    112.562,              /* 3840000000.0 Hz */
    112.091,              /* 3880000000.0 Hz */
};

static double _b7_table_B1_J1_columns[] = { 255.0 };

static double _b7_table_B1_J1_rows[] = {
    3800000000.0,
    3840000000.0,
    4000000000.0,
    5000000000.0,
    6000000000.0,
};

static double _b7_table_B1_J1_measurements[] = {
    113.611,              /* 3800000000.0 Hz */
    113.517,              /* 3840000000.0 Hz */
    112.846,              /* 4000000000.0 Hz */
    110.955,              /* 5000000000.0 Hz */
    107.93,               /* 6000000000.0 Hz */
};

struct card_calibration x2_default_cal = {
    .rx_cal_by_hdl_port = {
        [skiq_rx_hdl_A1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 9,
                .column_headers = _b0_table_A1_J1_columns,
                .row_headers = _b0_table_A1_J1_rows,
                .measurements = _b0_table_A1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b1_table_A1_J1_columns,
                .row_headers = _b1_table_A1_J1_rows,
                .measurements = _b1_table_A1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b2_table_A1_J1_columns,
                .row_headers = _b2_table_A1_J1_rows,
                .measurements = _b2_table_A1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b3_table_A1_J1_columns,
                .row_headers = _b3_table_A1_J1_rows,
                .measurements = _b3_table_A1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b4_table_A1_J1_columns,
                .row_headers = _b4_table_A1_J1_rows,
                .measurements = _b4_table_A1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b5_table_A1_J1_columns,
                .row_headers = _b5_table_A1_J1_rows,
                .measurements = _b5_table_A1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 9,
                .column_headers = _b6_table_A1_J1_columns,
                .row_headers = _b6_table_A1_J1_rows,
                .measurements = _b6_table_A1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b7_table_A1_J1_columns,
                .row_headers = _b7_table_A1_J1_rows,
                .measurements = _b7_table_A1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_A1_J1_columns,
                .row_headers = _bands_table_A1_J1_rows,
                .measurements = _bands_table_A1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 4,
                .nr_rows = 5,
                .column_headers = _gain_table_A1_J1_columns,
                .row_headers = _gain_table_A1_J1_rows,
                .measurements = _gain_table_A1_J1_measurements,
            },
        },
        [skiq_rx_hdl_A2][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 9,
                .column_headers = _b0_table_A2_J1_columns,
                .row_headers = _b0_table_A2_J1_rows,
                .measurements = _b0_table_A2_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b1_table_A2_J1_columns,
                .row_headers = _b1_table_A2_J1_rows,
                .measurements = _b1_table_A2_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b2_table_A2_J1_columns,
                .row_headers = _b2_table_A2_J1_rows,
                .measurements = _b2_table_A2_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b3_table_A2_J1_columns,
                .row_headers = _b3_table_A2_J1_rows,
                .measurements = _b3_table_A2_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b4_table_A2_J1_columns,
                .row_headers = _b4_table_A2_J1_rows,
                .measurements = _b4_table_A2_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b5_table_A2_J1_columns,
                .row_headers = _b5_table_A2_J1_rows,
                .measurements = _b5_table_A2_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 9,
                .column_headers = _b6_table_A2_J1_columns,
                .row_headers = _b6_table_A2_J1_rows,
                .measurements = _b6_table_A2_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b7_table_A2_J1_columns,
                .row_headers = _b7_table_A2_J1_rows,
                .measurements = _b7_table_A2_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_A2_J1_columns,
                .row_headers = _bands_table_A2_J1_rows,
                .measurements = _bands_table_A2_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 4,
                .nr_rows = 5,
                .column_headers = _gain_table_A2_J1_columns,
                .row_headers = _gain_table_A2_J1_rows,
                .measurements = _gain_table_A2_J1_measurements,
            },
        },
        [skiq_rx_hdl_B1][skiq_rf_port_J1] = {
            .band_refs[0] = {
                .name = "b0",
                .nr_cols = 1,
                .nr_rows = 9,
                .column_headers = _b0_table_B1_J1_columns,
                .row_headers = _b0_table_B1_J1_rows,
                .measurements = _b0_table_B1_J1_measurements,
            },
            .band_refs[1] = {
                .name = "b1",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b1_table_B1_J1_columns,
                .row_headers = _b1_table_B1_J1_rows,
                .measurements = _b1_table_B1_J1_measurements,
            },
            .band_refs[2] = {
                .name = "b2",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b2_table_B1_J1_columns,
                .row_headers = _b2_table_B1_J1_rows,
                .measurements = _b2_table_B1_J1_measurements,
            },
            .band_refs[3] = {
                .name = "b3",
                .nr_cols = 1,
                .nr_rows = 6,
                .column_headers = _b3_table_B1_J1_columns,
                .row_headers = _b3_table_B1_J1_rows,
                .measurements = _b3_table_B1_J1_measurements,
            },
            .band_refs[4] = {
                .name = "b4",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b4_table_B1_J1_columns,
                .row_headers = _b4_table_B1_J1_rows,
                .measurements = _b4_table_B1_J1_measurements,
            },
            .band_refs[5] = {
                .name = "b5",
                .nr_cols = 1,
                .nr_rows = 7,
                .column_headers = _b5_table_B1_J1_columns,
                .row_headers = _b5_table_B1_J1_rows,
                .measurements = _b5_table_B1_J1_measurements,
            },
            .band_refs[6] = {
                .name = "b6",
                .nr_cols = 1,
                .nr_rows = 9,
                .column_headers = _b6_table_B1_J1_columns,
                .row_headers = _b6_table_B1_J1_rows,
                .measurements = _b6_table_B1_J1_measurements,
            },
            .band_refs[7] = {
                .name = "b7",
                .nr_cols = 1,
                .nr_rows = 5,
                .column_headers = _b7_table_B1_J1_columns,
                .row_headers = _b7_table_B1_J1_rows,
                .measurements = _b7_table_B1_J1_measurements,
            },
            .bands = {
                .name = BANDS_TABLE_NAME,
                .nr_cols = 2,
                .nr_rows = 8,
                .column_headers = _bands_table_B1_J1_columns,
                .row_headers = _bands_table_B1_J1_rows,
                .measurements = _bands_table_B1_J1_measurements,
            },
            .gain = {
                .name = GAIN_TABLE_NAME,
                .nr_cols = 4,
                .nr_rows = 5,
                .column_headers = _gain_table_B1_J1_columns,
                .row_headers = _gain_table_B1_J1_rows,
                .measurements = _gain_table_B1_J1_measurements,
            },
        },
    },
};
