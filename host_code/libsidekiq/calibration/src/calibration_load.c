/**
 * @file   calibration_load.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Dec 19 10:41:04 CST 2018
 *
 * @brief Implements loading calibration data from various storage mediums as indexed by Sidekiq
 * part (::skiq_part_t).
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#if (!defined __MINGW32__)
#include <sys/mount.h>
#endif

#include "calibration.h"
#include "calibration_private.h"
#include "calibration_versions.h"
#include "calibration_storage.h"
#include "calibration_load.h"

#include "sidekiq_private.h"
#include "sidekiq_flash.h"
#include "sidekiq_hal.h"


/***** DEFINES *****/


/***** TYPE DEFINITIONS *****/


/***** LOCAL DATA *****/



/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
/** storage_read() translates the (offset,nr_bytes) request into one or more hal_read_eeprom() or
    flash_read() calls according to the blocks available described by 'storage'

    @param[in] card Sidekiq card index
    @param[in] offset offset index into source memory from which to start retrieving
    @param[out] p_data pointer to unsigned 8-bit values to populate with retrieved calibration data
    @param[in] nr_bytes number of valid bytes in 'data' array

    @return int32_t
    @retval 0 Success
    @retval -EINVAL calibration information type is invalid for this function
    @retval -ESPIPE number of bytes requested to read exceeds source memory's capacity
    @retval -EPROTO internal error: number of storage blocks is invalid in internal structures
    @retval -EIO I/O error reading from source
*/
static int32_t
storage_read( uint8_t card,
              uint16_t offset,
              uint8_t *p_data,
              uint16_t nr_bytes )
{
    struct cal_storage_info *info = cal_get_storage_info( card );
    struct cal_storage_blocks *storage = NULL;
    int32_t status = 0;
    uint32_t total_size = 0;
    bool done = false;
    uint16_t i;

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    /* look-up storage, only allow flash_storage or eeprom_storage */
    if ( ( info->type == flash_storage ) || ( info->type == eeprom_storage ) )
    {
        storage = info->storage;
    }
    else
    {
        return -EINVAL;
    }

    if ( storage->nr_blocks == 0 )
    {
        return -EPROTO;
    }

    /* determine total size available */
    for ( i = 0; i < storage->nr_blocks; i++ )
    {
        total_size += storage->blocks[i].size;
    }

    /* insufficient addressable space to satisfy request, return illegal seek */
    if ( offset > total_size )
    {
        return -ESPIPE;
    }

    for ( i = 0; ( i < storage->nr_blocks ) && !done; i++ )
    {
        debug_print("Considering storage block #%u defined as addr=0x%08X, size=%u with offset %u "
                    "for card %" PRIu8 "\n", i, storage->blocks[i].address, storage->blocks[i].size,
                    offset, card);

        /* if 'offset' part of this storage block? */
        if ( offset < storage->blocks[i].size )
        {
            debug_print("Requested offset resides in storage block #%u\n", i);

            /* split into two reads if there's another block, otherwise set error code */
            if ( ( offset + nr_bytes ) > total_size )
            {
                /* cannot complete request, too much requested for given offset */
                status = -ESPIPE;
            }
            else
            {
                uint16_t j;

                /* split into N hal_read_eeprom() or flash_read() calls */
                for ( j = i; ( j < storage->nr_blocks ) && ( nr_bytes > 0 ) && ( status == 0 ); j++)
                {
                    uint16_t to_read = MIN(nr_bytes, storage->blocks[j].size - offset);

                    switch ( info->type )
                    {
                        case eeprom_storage:
                            debug_print("Reading from EEPROM at physical address 0x%08X of length "
                                        "%u\n", storage->blocks[j].address + offset, to_read);
                            status = hal_read_eeprom( card, storage->blocks[j].address + offset,
                                                      p_data, to_read );
                            break;

                        case flash_storage:
                            debug_print("Reading from Flash at physical address 0x%08X of length "
                                        "%u\n", storage->blocks[j].address + offset, to_read);
                            status = flash_read( card, storage->blocks[j].address + offset, p_data,
                                                 to_read );
                            break;

                        default:
                            status = -EINVAL;
                            break;
                    }

                    if ( ( status != 0 ) && ( status != -ENOTSUP ) )
                    {
                        /* squash return code from hal_read_eeprom, flash_read, or invalid info type
                         * to a generic I/O error unless it's a "not supported" errno */
                        status = -EIO;
                    }

                    /* after the first block, reset 'offset' to read from the beginning of each
                     * block */
                    offset = 0;

                    /* remaining number of bytes to read, advance data pointer */
                    nr_bytes -= to_read;
                    p_data += to_read;
                }
            }

            done = true;
        }
        else
        {
            offset -= storage->blocks[i].size;
            total_size -= storage->blocks[i].size;
        }
    }

    return status;
}


/**************************************************************************************************/
/** storage_load() allocates, then retrieves calibration data from the storage specified by the
    associated skiq_part_t.  It is expected that the caller free the allocated memory referenced by
    pp_data if the return value is 0.

    @param[in] card Sidekiq card index
    @param[out] pp_data pointer to a uint8_t pointer that provides reference to allocated buffer
    @param[out] p_data_length number of bytes in pp_data that are valid

    @return int32_t
    @retval 0 Success
    @retval -ENOMEM insufficient memory available to import calibration data
    @retval -EINVAL calibration information type is invalid for this function
    @retval -ESPIPE number of bytes requested to read exceeds source memory's capacity
    @retval -EPROTO internal error: number of storage blocks is invalid in internal structures
    @retval -EIO I/O error writing to destination
 */
static int32_t
storage_load( uint8_t card,
              uint8_t **pp_data,
              uint32_t *p_data_length )
{
    struct cal_storage_info *info = cal_get_storage_info( card );
    struct cal_storage_blocks *storage = NULL;
    uint32_t total_size = 0, offset = 0;
    int32_t status = 0;
    uint16_t i;

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    /* look-up storage, only allow flash_storage or eeprom_storage */
    if ( ( info->type == flash_storage ) || ( info->type == eeprom_storage ) )
    {
        storage = info->storage;
    }
    else
    {
        return -EINVAL;
    }

    if ( storage->nr_blocks == 0 )
    {
        return -EPROTO;
    }

    for ( i = 0; i < storage->nr_blocks; i++ )
    {
        debug_print("Found storage block #%u defined as addr=0x%08X, size=%u for card %" PRIu8 "\n",
                    i, storage->blocks[i].address, storage->blocks[i].size, card);
        total_size += storage->blocks[i].size;
    }
    debug_print("Total size of storage is %u bytes for card %" PRIu8 "\n", total_size, card);

    /* allocate memory as if the full size of the storage medium contains calibration data */
    *pp_data = calloc( 1, total_size );
    if ( *pp_data != NULL )
    {
        uint32_t cal_length = 0;

        while (status == 0)
        {
            debug_print("Loading calibration header from storage at logical offset 0x%08X for card "
                        "%u\n", offset, card);
            status = storage_read( card, offset, *pp_data + offset, CAL_HEADER_LENGTH );
            if ( status == 0 )
            {
                if ( cal_import_parse_cal_header( *pp_data + offset, CAL_HEADER_LENGTH,
                                                  &cal_length ) )
                {
                    cal_length = MIN(cal_length, total_size - offset);

                    debug_print("Loading calibration record from storage at logical offset 0x%08X "
                                "(length = %u) for card %u\n", offset, cal_length, card);
                    status = storage_read( card, offset, *pp_data + offset, cal_length );
                    if ( status == 0 )
                    {
                        offset += cal_length;
                    }
                }
                else
                {
                    if ( offset == 0 )
                    {
                        debug_print("Calibration header missing for card %u, skipping import\n",
                                    card);
                    }
                    else
                    {
                        debug_print("No more calibration headers for card %u\n", card);
                    }
                    status = 0;
                    break;
                }
            }
        }
    }
    else
    {
        status = -ENOMEM;
    }

    if ( status == 0 )
    {
        *p_data_length = offset;
        debug_print("Loaded a total of %d bytes of calibration data for card %u\n",
                    *p_data_length, card);
    }
    else
    {
        FREE_IF_NOT_NULL( *pp_data );
    }

    return status;
}


/**************************************************************************************************/
/** file_load() allocates, then retrieves calibration data from the storage specified by the
    associated skiq_part_t.  It is expected that the caller free the allocated memory referenced by
    pp_data if the return value is 0 and *pp_data is non-NULL.

    @param[in] card Sidekiq card index
    @param[out] pp_data pointer to a uint8_t pointer that provides reference to allocated buffer
    @param[out] p_data_length number of bytes in pp_data that are valid

    @return int32_t
    @retval 0 Success
    @retval -ENOMEM insufficient memory available to import calibration data
    @retval -EINVAL calibration information type is invalid for this function
    @retval -errno error occurred in call to fopen() or fread(), see man page for possible errors
 */
static int32_t
file_load( uint8_t card,
           uint8_t **pp_data,
           uint32_t *p_data_length )
{
    struct cal_storage_info *info = cal_get_storage_info( card );
    int32_t status = 0;

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    debug_print("Loading calibration data from %s, size %u, for card %u\n", info->filepath,
                info->size, card);
    *pp_data = calloc( 1, info->size );
    if ( *pp_data != NULL )
    {
        FILE *cal_file = NULL;

        cal_file = fopen( info->filepath, "rb" );
        if ( cal_file == NULL )
        {
            /* if the file doesn't exist, that's okay, calibration data is not a requirement */
            if ( errno == ENOENT )
            {
                status = 0;
                *p_data_length = 0;
                FREE_IF_NOT_NULL( *pp_data );
            }
            else
            {
                perror("open:");
                status = -errno;
            }
        }
        else
        {
            size_t n = fread( *pp_data, sizeof(uint8_t), info->size, cal_file );

            /* reading any non-zero, positive number of bytes is considered a success, _import_cal
             * will sort out the details regarding correctness of the record */
            status = ( n > 0 ) ? 0 : -errno;
            fclose( cal_file );

            *p_data_length = info->size;
        }
    }
    else
    {
        status = -ENOMEM;
    }

    if ( status != 0 )
    {
        FREE_IF_NOT_NULL( *pp_data );
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** cal_load_from_storage() handles loading calibration data from the various storage types.

    @param[in] card Sidekiq card index
    @param[out] pp_data pointer to a uint8_t pointer that provides reference to allocated buffer
    @param[out] p_data_length number of bytes in pp_data that are valid

    @return int32_t
    @retval 0 Success
    @retval -ENOMEM insufficient memory available to import calibration data
    @retval -EINVAL calibration information type is invalid for this function
    @retval -ESPIPE number of bytes requested to read exceeds source memory's capacity
    @retval -EPROTO internal error: number of storage blocks is invalid in internal structures
    @retval -EPROTO internal error: unknown storage type set in internal structures
    @retval -EIO I/O error writing to destination
    @retval -errno error occurred in call to fopen() or fread(), see man page for possible errors
*/
int32_t cal_load_from_storage( uint8_t card,
                               uint8_t **pp_data,
                               uint32_t *p_data_length )
{
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( card );
    struct cal_storage_info *info = cal_get_storage_info( card );

    /* check for NULL pointer returned by cal_get_storage_info() */
    if ( info == NULL )
    {
        return -EINVAL;
    }

    switch (info->type)
    {
        case flash_storage:
        case eeprom_storage:
            status = storage_load( card, pp_data, p_data_length );
            break;

        case file_storage:
            status = file_load( card, pp_data, p_data_length );
            break;

        case storage_not_supported:
            /* not having calibration storage supported is not an error, so we let it go */
            *pp_data = NULL;
            *p_data_length = 0;
            break;

        default:
            skiq_error("Unknown calibration storage type for card %u and part %s\n", card,
                       part_cstr(part));
            status = -EPROTO;
            break;
    }

    return status;
}
