/**
 * @file   calibration_v3_iq_complex.c
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 12:20:07 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_types.h"
#include "sidekiq_private.h"    /* for *_cstr() */
#include "calibration_v3.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
/** The cal_import_table_v3_iq_complex() function attempts to populate the local card_calibration
    structure with the calibration record in memory referenced by p_data_start of length 'length'.
    It decodes the calibration record and can populate any and all information in
    card_calibration[card].

    At the conclusion, either card_calibration[card] reflects the calibration record described by
    (p_data_start,length) or card_calibration[card] remains unpopulated and a non-zero status is
    returned.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.

    returns the number of bytes consumed by the table if positive.  if negative, there was a
    unrecoverable error encountered while importing the table.
 */
int32_t
cal_import_table_v3_iq_complex( uint8_t card,
                                uint8_t *p_data_start,
                                uint32_t length )
{
    int32_t status;
    struct cal_table_v3 *p_table_v3 = (struct cal_table_v3 *)p_data_start;
    int32_t hdr_length = p_table_v3->measurements - (uint8_t *)p_table_v3;
    struct cal_table *p_cal_table = NULL;

    debug_print("Parsing V3 table\n");
    debug_hex_dump(p_data_start, MIN(128, length));

    if ( length > hdr_length )
    {
        enum table_type type;
        skiq_rx_hdl_t hdl;
        char *name = NULL;

        hdl = (skiq_rx_hdl_t)p_table_v3->handle;
        type = (enum table_type)p_table_v3->type;

        /* select cal_table based on type */
        if ( hdl < skiq_rx_hdl_end )
        {
            debug_print("Importing table type %s for card %u hdl %s rtype %u\n",
                        table_type_cstr(type), card, rx_hdl_cstr(hdl), iq_complex_cal_record_type);

            /* An IQ complex calibration record may contain 'real' or 'imag' tables */
            switch (type)
            {
                case re_cal_table_type:
                    name = RE_CAL_TABLE_NAME;
                    p_cal_table = P_CRECAL(card,hdl);
                    break;

                case im_cal_table_type:
                    name = IM_CAL_TABLE_NAME;
                    p_cal_table = P_CIMCAL(card,hdl);
                    break;

                default:
                    skiq_error("Unexpected table type (%u) for calibration record type "
                               "(%u) for card %u\n", type, iq_complex_cal_record_type, card);
                    p_cal_table = NULL;
                    break;
            }
        }

        /* generically import the table based on the version.  If successful, add the number of
         * consumed bytes to those consumed locally to decode the hdl,port, and type.  It's okay
         * to pass NULL as p_cal_table as import_table is expected to handle that gracefully. */
        status = cal_import_table( card, p_table_v3->nr_rows, p_table_v3->nr_cols,
                                   p_table_v3->measurements, length - hdr_length, p_cal_table,
                                   name, CAL_VERSION_V3 );
        if ( status >= 0 )
        {
            status += hdr_length;
        }
    }
    else
    {
        /* not enough data to properly decode header let alone the measurements */
        status = -E2BIG;
    }

    return status;
}


/**************************************************************************************************/
/** The cal_import_record_v3_iq_complex() function performs an import implementation for V3 records
    (Extended Receive Offset), checking that the calibration record is structured correctly
    (verifying that the KEY / IDENTIFIER, VERSION, handles, and FOOTER are present.  It delegates
    the main decoding the cal_import_table_v3_iq_complex() function for table correctness.

    @note assumes that card_calibration[card] is not currently populated.  Also assumes the
    calibration_lock[card] is being held by thread.

    @attention This function assumes that p_data is pointing at the first HANDLE field and length
    reflects the remaining number of bytes available to import.  p_data must NOT reference the
    beginning of the calibration record.

 */
int32_t
cal_import_record_v3_iq_complex( uint8_t card,
                             uint8_t *p_data,
                             uint32_t length )
{
    int32_t status = 0;

    debug_print("Parsing calibration record V3 (IQ complex) from %p with length %u\n", p_data,
                length);
    debug_hex_dump(p_data, MIN(128, length));

    /* import tables, then check that necessary tables are present */
    status = cal_import_all_tables_in_record(card, p_data, length, CAL_VERSION_V3,
                                             iq_complex_cal_record_type);

    if ( 0 == status )
    {
        /* import of V3 calibration record was successful, assert that for each (hdl,port) the
         * rx_cal is well defined */
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            if ( !has_recal(card,hdl) && has_imcal(card,hdl) )
            {
                skiq_warning("missing required '" RE_CAL_TABLE_NAME "' table for card %u hdl %s\n",
                             card, rx_hdl_cstr(hdl));
                status = -EINVAL;
            }
            else if ( has_recal(card,hdl) && !has_imcal(card,hdl) )
            {
                skiq_warning("missing required '" IM_CAL_TABLE_NAME "' table for card %u hdl %s\n",
                             card, rx_hdl_cstr(hdl));
                status = -EINVAL;
            }
        }
    }
    else
    {
        /* non-zero status means something went wrong in the import, free all iq_cal structures
         * across (hdl) */
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            card_free_iq_complex_cal_tables( P_IQ_COMPLEX_CAL(card,hdl) );
        }
    }

    /********************************************************************************************/
    /* display what's been discovered - for debug purposes */
    {
        skiq_rx_hdl_t hdl;
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            debug_print("card %u hdl %s " RE_CAL_TABLE_NAME "\n", card, rx_hdl_cstr(hdl));
            debug_display_table( P_CRECAL(card,hdl) );

            debug_print("card %u hdl %s " IM_CAL_TABLE_NAME "\n", card, rx_hdl_cstr(hdl));
            debug_display_table( P_CIMCAL(card,hdl) );

        }
    }
    /********************************************************************************************/

    return status;
}



#if (defined ATE_SUPPORT)

/**************************************************************************************************/
/** The cal_export_record_v3_iq_complex_cal() function is responsible for the overall creation of the V3
    calibration record for a given card.  It populates the memory block referenced by p_data for the
    eventual commit to NVM.  It populates the header and footer of the record and delegates creation
    of the tables to cal_export_table_v3().

    NOTE: This function is specific to extended RX calibration data, but cal_export_table_v3 is a
    generic method for exporting a supplied cal_table.
 */
int32_t
cal_export_record_v3_iq_complex( uint8_t card,
                             uint8_t *p_data,
                             uint32_t length )
{
    int32_t status = 0;
    uint8_t identifier[] = CAL_IDENTIFIER, footer[] = CAL_FOOTER;
    uint8_t *p = p_data;
    uint16_t *p_cal_length, cal_length_placeholder = 0;
    uint32_t total_length = length;
    uint8_t rtype_u8 = (uint8_t)iq_complex_cal_record_type;
    skiq_rx_hdl_t hdl;

    p = cal_export_array( p, &length, identifier, ARRAY_SIZE( identifier ) );
    p = cal_export_uint8( p, &length, CAL_VERSION_V3 );
    p_cal_length = (uint16_t *)p;           /* save a reference to the LENGTH field */
    p = cal_export_uint16( p, &length, cal_length_placeholder );
    p = cal_export_uint8( p, &length, rtype_u8 );

    for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
    {
        p = cal_export_table_v3( p, &length, P_CRECAL(card,hdl), hdl, skiq_rf_port_J1, re_cal_table_type );
        p = cal_export_table_v3( p, &length, P_CIMCAL(card,hdl), hdl, skiq_rf_port_J1, im_cal_table_type );
    }

    p = cal_export_uint8( p, &length, CAL_HANDLE_EOF );
    p = cal_export_array( p, &length, footer, ARRAY_SIZE( footer ));

    if ( p == NULL )
    {
        status = 0;
    }
    else
    {
        uint16_t export_length;

        /* number of bytes exported to p_data */
        status = total_length - length;

        /* store calibration data length in LENGTH field after byte re-ordering */
        export_length = (uint16_t)status;
        export_length = htole16(export_length);
        *p_cal_length = export_length;
    }

    return status;
}


#endif  /* ATE_SUPPORT */
