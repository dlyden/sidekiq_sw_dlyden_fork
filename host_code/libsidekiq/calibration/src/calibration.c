/**
 * @file   calibration.c
 *
 * @brief Defines the calibration functions 
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/
#include "calibration.h"
#include "calibration_funcs.h"
#include "sidekiq_rpc_client_api.h"

calibration_funcs_t *_p_cal_funcs[SKIQ_MAX_NUM_CARDS] =
{ [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = &local_cal_funcs };

int32_t card_calibration_load( uint8_t card,
                               skiq_part_t part )
{
    // if RPC is available, use those functions
    if( sidekiq_rpc_is_client_avail(card)==true )
    {
        _p_cal_funcs[card] = &rpc_cal_funcs;
    }
    else
    {
        _p_cal_funcs[card] = &local_cal_funcs;
    }

    return (_p_cal_funcs[card]->card_calibration_load(card, part));
}

int32_t card_calibration_clear( uint8_t card )
{
    int32_t status=0;

    status = _p_cal_funcs[card]->card_calibration_clear(card);
    
    // reset funcs back to local
    _p_cal_funcs[card] = &local_cal_funcs;

    return (status);
}

bool card_is_rx_cal_data_present( uint8_t card,
                                 skiq_rx_hdl_t hdl,
                                  skiq_rf_port_t port )
{
    return (_p_cal_funcs[card]->card_is_rx_cal_data_present(card, hdl, port));
}

int32_t card_read_cal_offset_in_dB( uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   skiq_rf_port_t port,
                                   uint64_t lo_freq,
                                   uint8_t gain_index,
                                   double programmable_rxfir_gain_dB,
                                   double *p_cal_rx_gain_dB )
{
    return (_p_cal_funcs[card]->card_read_cal_offset_in_dB(card,
                                                           hdl,
                                                           port,
                                                           lo_freq,
                                                           gain_index,
                                                           programmable_rxfir_gain_dB,
                                                           p_cal_rx_gain_dB));
}

/***** GLOBAL FUNCTIONS *****/


bool card_is_iq_complex_cal_data_present( uint8_t card,
                                          skiq_rx_hdl_t hdl )
{
    return (_p_cal_funcs[card]->card_is_iq_complex_cal_data_present(card, hdl));
}

int32_t card_read_iq_cal_complex_parts( uint8_t card,
                             skiq_rx_hdl_t hdl,
                                        uint64_t lo_freq,
                                        int8_t temper_deg_C,
                                        double *p_real,
                                        double *p_imag )
{
    return (_p_cal_funcs[card]->card_read_iq_cal_complex_parts(card,
                                                               hdl,
                                                               lo_freq,
                                                               temper_deg_C,
                                                               p_real,
                                                               p_imag ));
}

int32_t card_read_iq_cal_complex_multiplier( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                              uint64_t lo_freq,
                                              int8_t temper_deg_C,
                                              float complex *p_factor )
{
    int32_t status = 0;
    double real = 1.0, imag = 0.0;

    status = card_read_iq_cal_complex_parts( card, hdl, lo_freq, temper_deg_C, &real, &imag );

    if ( status == 0 )
    {
        *p_factor = (float)real + I*(float)imag;
    }
    else
    {
        *p_factor = (float)1.0;
        status = 0;
    }
    return (status);
}

#if (defined ATE_SUPPORT)
int32_t card_calibration_commit( uint8_t card,
                         skiq_part_t part )
{
    return (_p_cal_funcs[card]->card_calibration_commit(card, part));
}

int32_t card_calibration_add_rx_by_file( uint8_t card,
                                    skiq_rx_hdl_t hdl,
                                    skiq_rf_port_t port,
                                         FILE* fp )
{
    return (_p_cal_funcs[card]->card_calibration_add_rx_by_file(card, hdl, port, fp));
}

int32_t card_calibration_add_iq_complex_by_file( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                                 FILE* fp )
{
    return (_p_cal_funcs[card]->card_calibration_add_iq_complex_by_file(card, hdl, fp));
}
#endif
