/**
 * @file   calibration_v1.c
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 12:20:07 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include "calibration_private.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
const char *
table_type_cstr( enum table_type type )
{
    const char* p_type_str =
        (type == rx_ref_table_type) ? "RX ref" :
        (type == rx_gain_table_type) ? "RX gain" :
        (type == re_cal_table_type) ? "Complex (Real)" :
        (type == im_cal_table_type) ? "Complex (Imag)" :
        (type == rx_bands_table_type) ? "RX bands" :
        ((type >= rx_band_ref_start_table_type) &&
         (type <= rx_band_ref_end_table_type)) ? "RX band ref" :
        "unknown";

    return p_type_str;
}
