# full path to root of glib2 pre-built for BUILD_CONFIG
mkfile_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
ifeq ($(BUILD_CONFIG),mingw64)
# use the glib-2.0 installed by MSYS2's pacman
export glib2_ROOT:= /mingw64
else
# if a BUILD_CONFIG has a .native suffix, strip it off since both native and
# non-native builds will use the same build_output_% directory
export glib2_ROOT:= $(mkfile_dir)/glib2/build_output_$(BUILD_CONFIG:%.native=%)
endif

# libraries needed change depending on architecture
ifeq ($(BUILD_CONFIG),arm_cortex-a9.gcc4.8_uclibc_openwrt)
glib2_LIBNAMES= glib-2.0 intl iconv
else
ifeq ($(BUILD_CONFIG),mingw64)
glib2_LIBNAMES_STATIC= glib-2.0.dll intl.dll iconv.dll
glib2_LIBNAMES= glib-2.0
else
glib2_LIBNAMES= glib-2.0
endif # win64
endif

#
# CFLAGS, LDFLAGS for use by modules when dynamically linking
#
# \$$$$ORIGIN is not a typo.  :(
# since glib2_LDFLAGS is used at levels of dereferencing in make here, $$$$ => $$, then $$ => $
# then bash needs \ to stop it from dereference, it removes \
# linker then sees $ORIGIN as required
#
# this is likely not a portable solution and will need more investigation
#
ifeq ($(BUILD_CONFIG),mingw64)
export glib2_CFLAGS:= -I$(glib2_ROOT)/include/glib-2.0 -I$(glib2_ROOT)/lib/glib-2.0/include
else
export glib2_CFLAGS:= -I$(glib2_ROOT)/usr/include/glib-2.0 -I$(glib2_ROOT)/usr/lib/epiq/glib-2.0/include
endif

ifeq ($(BUILD_CONFIG),arm_cortex-a9.gcc4.8_uclibc_openwrt)
export glib2_LDFLAGS:= -L$(glib2_ROOT)/usr/lib/epiq -Wl,-rpath=/usr/lib/epiq $(foreach lib,$(glib2_LIBNAMES),-l$(lib))
else
ifeq ($(BUILD_CONFIG),mingw64)
#export glib2_LDFLAGS:= -L$(glib2_ROOT) -Wl,-rpath=/usr/lib/epiq $(foreach lib,$(glib2_LIBNAMES),-l$(lib))
export glib2_LDFLAGS:= -L$(glib2_ROOT)/lib $(foreach lib,$(glib2_LIBNAMES),-l$(lib))
else
export glib2_LDFLAGS:= -L$(glib2_ROOT)/usr/lib/epiq -Wl,-rpath=\$$$$ORIGIN:/usr/lib/epiq $(foreach lib,$(glib2_LIBNAMES),-l$(lib))
endif # win64
endif

# LDFLAGS for static linking
ifeq ($(BUILD_CONFIG),mingw64)
export glib2_LDFLAGS_STATIC:= $(foreach lib,$(glib2_LIBNAMES_STATIC),$(glib2_ROOT)/lib/lib$(lib).a)
else
export glib2_LDFLAGS_STATIC:= $(foreach lib,$(glib2_LIBNAMES),$(glib2_ROOT)/usr/lib/epiq/lib$(lib).a)
endif
