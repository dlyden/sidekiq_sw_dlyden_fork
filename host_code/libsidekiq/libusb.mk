export libusb_ROOT:= $(SIDEKIQ_ROOT)/libraries/libusb/build.$(SUFFIX)/support

#
# CFLAGS, LDFLAGS for use by modules when dynamically linking
#
# \$$$$ORIGIN is not a typo.  :(
# since libusb_LDFLAGS is used at levels of dereferencing in make here, $$$$ => $$, then $$ => $
# then bash needs \ to stop it from dereference, it removes \
# linker then sees $ORIGIN as required
#
# this is likely not a portable solution and will need more investigation
#
export libusb_CFLAGS:= -I$(libusb_ROOT)/usr/include/libusb-1.0

ifeq ($(BUILD_CONFIG),arm_cortex-a9.gcc4.8_uclibc_openwrt)
export libusb_LDFLAGS:= -L$(libusb_ROOT)/usr/lib/epiq -Wl,-rpath=/usr/lib/epiq -lusb-1.0
else ifeq ($(BUILD_CONFIG),mingw64)
export libusb_LDFLAGS:= -L$(libusb_ROOT)/usr/lib/bin -Wl,-rpath=\$$$$ORIGIN:/usr/lib/bin -lusb-1.0
else
export libusb_LDFLAGS:= -L$(libusb_ROOT)/usr/lib/epiq -Wl,-rpath=\$$$$ORIGIN:/usr/lib/epiq -lusb-1.0
endif

# LDFLAGS for static linking
export libusb_LDFLAGS_STATIC:= $(libusb_ROOT)/usr/lib/epiq/libusb-1.0.a
