#!/bin/sh


rm -f skiq_rpc_server.c
rm -f Makefile.skiq_rpc
rm -f skiq_rpc_client.c

echo "Generating files"
rpcgen -M -a -i 0 -C skiq_rpc.x

if [ $? -eq 0 ]; then
    echo "Moving autogen files"
    mv skiq_rpc_clnt.c ../src/skiq_rpc_clnt.c
    mv skiq_rpc_xdr.c ../src/skiq_rpc_xdr.c
    mv skiq_rpc.h ../inc/skiq_rpc.h
else
    echo "RPC Gen failed"
fi

echo "Don't forget to update skiq_rpc_server.c with implementation!!!"
