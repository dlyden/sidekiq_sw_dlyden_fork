struct rf_params
{
    uint8_t card;
    uint32_t hdl;
};

struct gpio_params
{
    uint8_t card;
    uint32_t class;
    uint32_t offset;
};

struct write_rx_lo
{
    rf_params config_params;
    uint64_t freq;
    uint32_t tune_mode;
    uint64_t rf_timestamp;
};

struct write_tx_lo
{
    rf_params config_params;
    uint64_t freq;
    uint32_t tune_mode;
    uint64_t rf_timestamp;
};

struct cal_is_rx_cal_data_present
{
    rf_params card_params;
    uint32_t port;
};

struct cal_read_offset_in_dB
{
    rf_params card_params;
    uint32_t port;
    uint64_t lo_freq;
    uint8_t gain_index;
    double rxfir_gain_dB;
};

struct cal_offset_in_dB_result
{
    int32_t status;
    double cal_rx_gain_dB;
};

struct cal_read_iq_cal_complex_parts
{
    rf_params card_params;
    uint64_t lo_freq;
    int8_t temp_in_C;
};

struct cal_read_iq_cal_complex_parts_result
{
    int32_t status;
    double real;
    double imag;
};

struct gpio_direction
{
    gpio_params params;
    uint32_t direction;
};

struct gpio_value
{
    gpio_params params;
    uint8_t value;
};

struct gpio_read_value
{
    int32_t status;
    uint8_t value;
};

struct rfic_id
{
    uint8_t card;
    uint32_t hdl;
};

struct rfic_write_uint32
{
    struct rfic_id id;
    uint32_t val;
};

struct rfic_read_uint32
{
    int32_t status;
    uint32_t val;
};

struct rfic_write_sr_and_bw
{
    struct rfic_id id;
    uint32_t sample_rate;
    uint32_t bandwidth;
};

struct rfic_read_sr
{
    int32_t status;
    uint32_t req_sample_rate;
    double act_sample_rate;
};

struct rfic_read_chan_bw
{
    int32_t status;
    uint32_t req_bandwidth;
    uint32_t act_bandwidth;
};


program SIDEKIQ
{
    version SIDEKIQ_API_VER
    {
        int32_t WRITE_RX_LO_FREQ(write_rx_lo)=1;
        int32_t WRITE_TX_LO_FREQ(write_tx_lo)=2;
        
        bool CAL_IS_RX_DATA_PRESENT(cal_is_rx_cal_data_present)=3;
        cal_offset_in_dB_result CAL_READ_OFFSET_IN_DB(cal_read_offset_in_dB)=4;
        bool CAL_IS_IQ_COMPLEX_PRESENT(rf_params)=5;
        cal_read_iq_cal_complex_parts_result CAL_READ_IQ_CAL_COMPLEX_PARTS(cal_read_iq_cal_complex_parts)=6;

        int32_t GPIO_SYSFS_ENABLE_CTRL(gpio_params)=7;
        int32_t GPIO_SYSFS_DISABLE_CTRL(gpio_params)=8;
        int32_t GPIO_SYSFS_CONFIGURE_DIRECTION(gpio_direction)=9;
        gpio_read_value GPIO_SYSFS_READ_VALUE(gpio_params)=10;
        int32_t GPIO_SYSFS_WRITE_VALUE(gpio_value)=11;

        int32_t RFIC_RESET_AND_INIT(rfic_id)=12;
        int32_t RFIC_WRITE_RX_GAIN_MODE(rfic_write_uint32)=13;
        rfic_read_uint32 RFIC_READ_RX_GAIN_MODE(rfic_id)=14;
        int32_t RFIC_WRITE_RX_SAMPLE_RATE_AND_BANDWIDTH(rfic_write_sr_and_bw)=15;
        rfic_read_sr RFIC_READ_RX_SAMPLE_RATE(rfic_id)=16;
        rfic_read_chan_bw RFIC_READ_RX_CHAN_BANDWIDTH(rfic_id)=17;
        int32_t RFIC_WRITE_TX_SAMPLE_RATE_AND_BANDWIDTH(rfic_write_sr_and_bw)=18;
        rfic_read_sr RFIC_READ_TX_SAMPLE_RATE(rfic_id)=19;
        rfic_read_chan_bw RFIC_READ_TX_CHAN_BANDWIDTH(rfic_id)=20;
        int32_t RFIC_WRITE_RX_CAL_MODE(rfic_write_uint32)=21;
        int32_t RFIC_WRITE_TX_QUADCAL_MODE(rfic_write_uint32)=22;
        int32_t RFIC_WRITE_RX_CAL_MASK(rfic_write_uint32)=23;
        rfic_read_uint32 RFIC_READ_RX_CAL_MASK(rfic_id)=24;

    }=9;
}=0x2FFFFFFF;
