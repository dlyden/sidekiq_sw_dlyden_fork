#include "sidekiq_rpc_server.h"
#include "skiq_rpc.h"

/////////////////////
// from skiq_rpc_svc.c
#include <rpc/pmap_clnt.h>
#include <netinet/in.h>

#ifndef SIG_PF
#define SIG_PF void(*)(int)
#endif
/////////////////////

/////////////////////
// from skiq_rpc_svc.c
static void
sidekiq_rpc(struct svc_req *rqstp, register SVCXPRT *transp)
{
	union {
		write_rx_lo write_rx_lo_freq_9_arg;
		write_tx_lo write_tx_lo_freq_9_arg;
		cal_is_rx_cal_data_present cal_is_rx_data_present_9_arg;
		cal_read_offset_in_dB cal_read_offset_in_db_9_arg;
		rf_params cal_is_iq_complex_present_9_arg;
		cal_read_iq_cal_complex_parts cal_read_iq_cal_complex_parts_9_arg;
		gpio_params gpio_sysfs_enable_ctrl_9_arg;
		gpio_params gpio_sysfs_disable_ctrl_9_arg;
		gpio_direction gpio_sysfs_configure_direction_9_arg;
		gpio_params gpio_sysfs_read_value_9_arg;
		gpio_value gpio_sysfs_write_value_9_arg;
		rfic_id rfic_reset_and_init_9_arg;
		rfic_write_uint32 rfic_write_rx_gain_mode_9_arg;
		rfic_id rfic_read_rx_gain_mode_9_arg;
		rfic_write_sr_and_bw rfic_write_rx_sample_rate_and_bandwidth_9_arg;
		rfic_id rfic_read_rx_sample_rate_9_arg;
		rfic_id rfic_read_rx_chan_bandwidth_9_arg;
		rfic_write_sr_and_bw rfic_write_tx_sample_rate_and_bandwidth_9_arg;
		rfic_id rfic_read_tx_sample_rate_9_arg;
		rfic_id rfic_read_tx_chan_bandwidth_9_arg;
		rfic_write_uint32 rfic_write_rx_cal_mode_9_arg;
		rfic_write_uint32 rfic_write_tx_quadcal_mode_9_arg;
		rfic_write_uint32 rfic_write_rx_cal_mask_9_arg;
		rfic_id rfic_read_rx_cal_mask_9_arg;
	} argument;
	union {
		int32_t write_rx_lo_freq_9_res;
		int32_t write_tx_lo_freq_9_res;
		bool_t cal_is_rx_data_present_9_res;
		cal_offset_in_dB_result cal_read_offset_in_db_9_res;
		bool_t cal_is_iq_complex_present_9_res;
		cal_read_iq_cal_complex_parts_result cal_read_iq_cal_complex_parts_9_res;
		int32_t gpio_sysfs_enable_ctrl_9_res;
		int32_t gpio_sysfs_disable_ctrl_9_res;
		int32_t gpio_sysfs_configure_direction_9_res;
		gpio_read_value gpio_sysfs_read_value_9_res;
		int32_t gpio_sysfs_write_value_9_res;
		int32_t rfic_reset_and_init_9_res;
		int32_t rfic_write_rx_gain_mode_9_res;
		rfic_read_uint32 rfic_read_rx_gain_mode_9_res;
		int32_t rfic_write_rx_sample_rate_and_bandwidth_9_res;
		rfic_read_sr rfic_read_rx_sample_rate_9_res;
		rfic_read_chan_bw rfic_read_rx_chan_bandwidth_9_res;
		int32_t rfic_write_tx_sample_rate_and_bandwidth_9_res;
		rfic_read_sr rfic_read_tx_sample_rate_9_res;
		rfic_read_chan_bw rfic_read_tx_chan_bandwidth_9_res;
		int32_t rfic_write_rx_cal_mode_9_res;
		int32_t rfic_write_tx_quadcal_mode_9_res;
		int32_t rfic_write_rx_cal_mask_9_res;
    rfic_read_uint32 rfic_read_rx_cal_mask_9_res;
	} result;
	bool_t retval;
	xdrproc_t _xdr_argument, _xdr_result;
	bool_t (*local)(char *, void *, struct svc_req *);

	switch (rqstp->rq_proc) {
	case NULLPROC:
		(void) svc_sendreply (transp, (xdrproc_t) xdr_void, (char *)NULL);
		return;

	case WRITE_RX_LO_FREQ:
		_xdr_argument = (xdrproc_t) xdr_write_rx_lo;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))write_rx_lo_freq_9_svc;
		break;

	case WRITE_TX_LO_FREQ:
		_xdr_argument = (xdrproc_t) xdr_write_tx_lo;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))write_tx_lo_freq_9_svc;
		break;

	case CAL_IS_RX_DATA_PRESENT:
		_xdr_argument = (xdrproc_t) xdr_cal_is_rx_cal_data_present;
		_xdr_result = (xdrproc_t) xdr_bool;
		local = (bool_t (*) (char *, void *,  struct svc_req *))cal_is_rx_data_present_9_svc;
		break;

	case CAL_READ_OFFSET_IN_DB:
		_xdr_argument = (xdrproc_t) xdr_cal_read_offset_in_dB;
		_xdr_result = (xdrproc_t) xdr_cal_offset_in_dB_result;
		local = (bool_t (*) (char *, void *,  struct svc_req *))cal_read_offset_in_db_9_svc;
		break;

	case CAL_IS_IQ_COMPLEX_PRESENT:
		_xdr_argument = (xdrproc_t) xdr_rf_params;
		_xdr_result = (xdrproc_t) xdr_bool;
		local = (bool_t (*) (char *, void *,  struct svc_req *))cal_is_iq_complex_present_9_svc;
		break;

	case CAL_READ_IQ_CAL_COMPLEX_PARTS:
		_xdr_argument = (xdrproc_t) xdr_cal_read_iq_cal_complex_parts;
		_xdr_result = (xdrproc_t) xdr_cal_read_iq_cal_complex_parts_result;
		local = (bool_t (*) (char *, void *,  struct svc_req *))cal_read_iq_cal_complex_parts_9_svc;
		break;

	case GPIO_SYSFS_ENABLE_CTRL:
		_xdr_argument = (xdrproc_t) xdr_gpio_params;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))gpio_sysfs_enable_ctrl_9_svc;
		break;

	case GPIO_SYSFS_DISABLE_CTRL:
		_xdr_argument = (xdrproc_t) xdr_gpio_params;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))gpio_sysfs_disable_ctrl_9_svc;
		break;

	case GPIO_SYSFS_CONFIGURE_DIRECTION:
		_xdr_argument = (xdrproc_t) xdr_gpio_direction;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))gpio_sysfs_configure_direction_9_svc;
		break;

	case GPIO_SYSFS_READ_VALUE:
		_xdr_argument = (xdrproc_t) xdr_gpio_params;
		_xdr_result = (xdrproc_t) xdr_gpio_read_value;
		local = (bool_t (*) (char *, void *,  struct svc_req *))gpio_sysfs_read_value_9_svc;
		break;

	case GPIO_SYSFS_WRITE_VALUE:
		_xdr_argument = (xdrproc_t) xdr_gpio_value;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))gpio_sysfs_write_value_9_svc;
		break;

	case RFIC_RESET_AND_INIT:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_reset_and_init_9_svc;
		break;

	case RFIC_WRITE_RX_GAIN_MODE:
		_xdr_argument = (xdrproc_t) xdr_rfic_write_uint32;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_write_rx_gain_mode_9_svc;
		break;

	case RFIC_READ_RX_GAIN_MODE:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_rfic_read_uint32;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_read_rx_gain_mode_9_svc;
		break;

	case RFIC_WRITE_RX_SAMPLE_RATE_AND_BANDWIDTH:
		_xdr_argument = (xdrproc_t) xdr_rfic_write_sr_and_bw;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_write_rx_sample_rate_and_bandwidth_9_svc;
		break;

	case RFIC_READ_RX_SAMPLE_RATE:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_rfic_read_sr;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_read_rx_sample_rate_9_svc;
		break;

	case RFIC_READ_RX_CHAN_BANDWIDTH:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_rfic_read_chan_bw;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_read_rx_chan_bandwidth_9_svc;
		break;

	case RFIC_WRITE_TX_SAMPLE_RATE_AND_BANDWIDTH:
		_xdr_argument = (xdrproc_t) xdr_rfic_write_sr_and_bw;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_write_tx_sample_rate_and_bandwidth_9_svc;
		break;

	case RFIC_READ_TX_SAMPLE_RATE:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_rfic_read_sr;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_read_tx_sample_rate_9_svc;
		break;

	case RFIC_READ_TX_CHAN_BANDWIDTH:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_rfic_read_chan_bw;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_read_tx_chan_bandwidth_9_svc;
		break;

	case RFIC_WRITE_RX_CAL_MODE:
		_xdr_argument = (xdrproc_t) xdr_rfic_write_uint32;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_write_rx_cal_mode_9_svc;
		break;

	case RFIC_WRITE_TX_QUADCAL_MODE:
		_xdr_argument = (xdrproc_t) xdr_rfic_write_uint32;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_write_tx_quadcal_mode_9_svc;
		break;

	case RFIC_WRITE_RX_CAL_MASK:
		_xdr_argument = (xdrproc_t) xdr_rfic_write_uint32;
		_xdr_result = (xdrproc_t) xdr_int32_t;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_write_rx_cal_mask_9_svc;
		break;

case RFIC_READ_RX_CAL_MASK:
		_xdr_argument = (xdrproc_t) xdr_rfic_id;
		_xdr_result = (xdrproc_t) xdr_rfic_read_uint32;
		local = (bool_t (*) (char *, void *,  struct svc_req *))rfic_read_rx_cal_mask_9_svc;
		break;

	default:
		svcerr_noproc (transp);
		return;
	}
	memset ((char *)&argument, 0, sizeof (argument));
	if (!svc_getargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
		svcerr_decode (transp);
		return;
	}
	retval = (bool_t) (*local)((char *)&argument, (void *)&result, rqstp);
	if (retval > 0 && !svc_sendreply(transp, (xdrproc_t) _xdr_result, (char *)&result)) {
		svcerr_systemerr (transp);
	}

	if (!svc_freeargs (transp, (xdrproc_t) _xdr_argument, (caddr_t) &argument)) {
		fprintf (stderr, "%s", "unable to free arguments");
		exit (1);
	}
	if (!sidekiq_9_freeresult (transp, _xdr_result, (caddr_t) &result))
		fprintf (stderr, "%s", "unable to free results");

	return;
}


/////////////////////

int32_t sidekiq_rpc_server_init( void )
{
    int32_t status=0;
    register SVCXPRT *transp = NULL;

    //////////////////////////////
    // from skiq_rpc_sv.c main function
    
    pmap_unset (SIDEKIQ, SIDEKIQ_API_VER);
    
    transp = svcudp_create(RPC_ANYSOCK);
    if (transp == NULL)
    {
        fprintf (stderr, "%s", "cannot create udp service.");
        status=-1;
    }
    if (!svc_register(transp, SIDEKIQ, SIDEKIQ_API_VER, sidekiq_rpc, IPPROTO_UDP))
    {
        fprintf (stderr, "%s", "unable to register (SIDEKIQ, SIDEKIQ_API_VER, udp).");
        status=-2;
    }
    transp = svctcp_create(RPC_ANYSOCK, 0, 0);
    if (transp == NULL)
    {
        fprintf (stderr, "%s", "cannot create tcp service.");
        status=-3;
    }
    if (!svc_register(transp, SIDEKIQ, SIDEKIQ_API_VER, sidekiq_rpc, IPPROTO_TCP))
    {
        fprintf (stderr, "%s", "unable to register (SIDEKIQ, SIDEKIQ_API_VER, tcp).");
        status=-4;
    }

    return (status);
}
