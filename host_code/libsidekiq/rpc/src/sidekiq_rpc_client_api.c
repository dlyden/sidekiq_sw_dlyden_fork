/*****************************************************************************/
/** @file sidekiq_rpc_client_api.c

 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

 @brief This file contains the implementation for Sidekiq RPC client API.

*/

#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include "sidekiq_rpc_client_api.h"
#ifdef HAS_RPC
#include "skiq_rpc.h"
#endif

#include "sidekiq_api.h"
#include "gpio_sysfs.h"
#include "sidekiq_hal.h"
#include "net_transport.h"
#include "net_transport_private.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_card.h"

#ifdef HAS_RPC
typedef struct
{
    CLIENT *p_client;
    uint64_t uid;
    uint8_t server_card; // we can derive the card from the UID, but
                         // we'll cache it to support quick/easy access
} skiq_rpc_client_t;

static skiq_rpc_client_t _skiq_client[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] =
    {
        .p_client = NULL,
        .uid = 0,
        .server_card = INVALID_CARD_INDEX,
    },
};

pthread_mutex_t _client_mutex = PTHREAD_MUTEX_INITIALIZER;

// Note: the majority of the "clnt" implementation has been plucked from skiq_rpc_client.c

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t sidekiq_rpc_client_init( skiq_xport_id_t *p_xport_id )
{
    int32_t status=0;
    bool client_found=false;
    CLIENT *clnt = NULL;
    const char* p_new_ip = NULL;
    const char* p_curr_ip = NULL;
    uint8_t client_card=0;
    uint8_t server_card=0;
    uint8_t i=0;

    pthread_mutex_lock(&_client_mutex);

    // we only support RPC for the NET transport, so make sure that's the type
    if( (p_xport_id->type != skiq_xport_type_net) )
    {
        skiq_error("RPC currently only supported for the network transport");
        status = -EPROTO;
    }

    if( status == 0 )
    {
        p_new_ip = _net_get_ip_from_uid( p_xport_id->xport_uid );
        // loop through all of the cards to see if there's already a RPC client for this IP
        for( i=0; (i<SKIQ_MAX_NUM_CARDS) && (client_found==false); i++ )
        {
            if( _skiq_client[i].p_client != NULL )
            {
                p_curr_ip = _net_get_ip_from_uid( _skiq_client[i].uid );
                if( strcmp( p_curr_ip, p_new_ip ) == 0 )
                {
                    skiq_debug("RPC client already exists for %s, reusing", p_new_ip);
                    client_found = true;
                    clnt = _skiq_client[i].p_client;
                }
            }
        }
    }

    if( status == 0 )
    {
        if( (client_found == false) && (p_new_ip != NULL) )
        {
            skiq_debug("Creating new RPC client");
            clnt = clnt_create (p_new_ip, SIDEKIQ, SIDEKIQ_API_VER, "udp");
            if (clnt == NULL)
            {
                clnt_pcreateerror (p_new_ip);
                skiq_error("Unable to create RPC client for IP %s", p_new_ip);
                status = -ENODEV;
            }
        }
    }

    // get the card number and save client info
    if( status == 0 )
    {
        // get the card number from the transport info
        status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &client_card );
        if( (status == 0) && (client_card<SKIQ_MAX_NUM_CARDS) )
        {
            server_card = _net_get_card_num_from_uid( p_xport_id->xport_uid );
            skiq_debug("Saving server card %" PRIu8 " (uid=%" PRIx64 ") to client card %" PRIu8,
                       server_card, p_xport_id->xport_uid, client_card);
            _skiq_client[client_card].p_client = clnt;
            _skiq_client[client_card].uid = p_xport_id->xport_uid;
            _skiq_client[client_card].server_card = server_card;
        }
    }

    pthread_mutex_unlock(&_client_mutex);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t sidekiq_rpc_client_exit( skiq_xport_id_t *p_xport_id )
{
    int32_t status=0;
    uint8_t i=0;
    uint8_t client_card=0;
    CLIENT *clnt;
    bool cleanup_card=true;

    skiq_debug("Releasing RPC client");

    pthread_mutex_lock(&_client_mutex);

    status = card_mgr_get_card( p_xport_id->xport_uid, p_xport_id->type, &client_card );
    if( status == 0 )
    {
        // save the client reference
        clnt = _skiq_client[client_card].p_client;

        // clear out the client info
        if( clnt != NULL )
        {
            _skiq_client[client_card].p_client = NULL;
            _skiq_client[client_card].uid = 0;
            _skiq_client[client_card].server_card = INVALID_CARD_INDEX;

            // now determine if anyone else is referencing this client
            for( i=0; (i<SKIQ_MAX_NUM_CARDS) && (cleanup_card==true); i++ )
            {
                if( _skiq_client[i].p_client == clnt )
                {
                    cleanup_card = false;
                }
            }

            // cleanup the client
            if( cleanup_card == true )
            {
                clnt_destroy( clnt );
            }
        }
        else
        {
            status = -ENODEV;
        }
    }

    pthread_mutex_unlock(&_client_mutex);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
bool sidekiq_rpc_is_client_avail( uint8_t card )
{
    bool is_avail = false;

    pthread_mutex_lock(&_client_mutex);
    if( (_skiq_client[card].p_client != NULL) &&
        (_skiq_client[card].server_card < SKIQ_MAX_NUM_CARDS) )
    {
        is_avail = true;
    }
    pthread_mutex_unlock(&_client_mutex);

    return (is_avail);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
bool rpc_client_cal_is_rx_cal_data_present( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            skiq_rf_port_t port )
{
    bool cal_present=false;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        bool_t result_t;
        cal_is_rx_cal_data_present cal_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        cal_arg.card_params.card = server_card;
        cal_arg.card_params.hdl = hdl;
        cal_arg.port = port;

        clnt_status = cal_is_rx_data_present_9(&cal_arg,
                                          &result_t,
                                          p_client);
        pthread_mutex_unlock(&_client_mutex);
        
        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("cal_is_rx_data_present RPC call failed (card=%u) (clnt_status=%d)", card, clnt_status); 
        }
        else
        {
            cal_present = result_t;
        }
    }

    return (cal_present);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_cal_read_cal_offset_in_dB( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              skiq_rf_port_t port,
                                              uint64_t lo_freq,
                                              uint8_t gain_index,
                                              double programmable_rxfir_gain_dB,
                                              double *p_cal_rx_gain_dB )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        cal_offset_in_dB_result result;
        cal_read_offset_in_dB cal_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;
        
        cal_arg.card_params.card = server_card;
        cal_arg.card_params.hdl = hdl;
        cal_arg.port = port;
        cal_arg.lo_freq = lo_freq;
        cal_arg.gain_index = gain_index;
        cal_arg.rxfir_gain_dB = programmable_rxfir_gain_dB;

        clnt_status = cal_read_offset_in_db_9(&cal_arg,
                                              &result,
                                              p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC cal cal_read_offset_in_db failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            {
                *p_cal_rx_gain_dB = result.cal_rx_gain_dB;
            }
        }
    }
    return (status);
}
    
/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
bool rpc_client_cal_is_iq_complex_cal_data_present( uint8_t card,
                                                    skiq_rx_hdl_t hdl )
{
    bool cal_present=false;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        bool_t result;
        rf_params cal_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        cal_arg.card = server_card;
        cal_arg.hdl = hdl;

        clnt_status = cal_is_iq_complex_present_9(&cal_arg,
                                                  &result,
                                                  p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call cal_is_iq_complex_present failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            cal_present = result;
        }

    }
    return (cal_present);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_cal_read_iq_complex_cal_parts( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  uint64_t lo_freq,
                                                  int8_t temper_deg_C,
                                                  double *p_real,
                                                  double *p_imag )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        cal_read_iq_cal_complex_parts_result result;
        cal_read_iq_cal_complex_parts cal_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        cal_arg.card_params.card = server_card;
        cal_arg.card_params.hdl = hdl;
        cal_arg.lo_freq = lo_freq;
        cal_arg.temp_in_C = temper_deg_C;

        clnt_status = cal_read_iq_cal_complex_parts_9(&cal_arg,
                                                      &result,
                                                      p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call cal_read_iq_cal_complex_parts failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            {
                *p_real = result.real;
                *p_imag = result.imag;
            }
        }
    }
    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_tune_write_rx_LO_freq( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint64_t freq,
                                          skiq_freq_tune_mode_t mode,
                                          uint64_t timestamp )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        write_rx_lo  write_rx_lo_freq_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_rx_lo_freq_arg.config_params.card = server_card;
        write_rx_lo_freq_arg.config_params.hdl = hdl;
        write_rx_lo_freq_arg.freq = freq;
        write_rx_lo_freq_arg.tune_mode = mode;
        write_rx_lo_freq_arg.rf_timestamp = timestamp;

        clnt_status = write_rx_lo_freq_9(&write_rx_lo_freq_arg,
                                         &result,
                                         p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call write_rx_lo_freq failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_tune_write_tx_LO_freq( uint8_t card,
                                          skiq_tx_hdl_t hdl,
                                          uint64_t freq,
                                          skiq_freq_tune_mode_t mode,
                                          uint64_t timestamp )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        write_tx_lo  write_tx_lo_freq_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_tx_lo_freq_arg.config_params.card = server_card;
        write_tx_lo_freq_arg.config_params.hdl = hdl;
        write_tx_lo_freq_arg.freq = freq;
        write_tx_lo_freq_arg.tune_mode = mode;
        write_tx_lo_freq_arg.rf_timestamp = timestamp;
        
        clnt_status = write_tx_lo_freq_9(&write_tx_lo_freq_arg,
                                         &result,
                                         p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call write_tx_lo failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_gpio_sysfs_enable_ctrl( uint8_t card,
                                           uint32_t gpio_class,
                                           uint32_t gpio_offset )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        gpio_params  gpio_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        gpio_arg.card = server_card;
        gpio_arg.class = gpio_class;
        gpio_arg.offset = gpio_offset;
        
        clnt_status = gpio_sysfs_enable_ctrl_9(&gpio_arg,
                                               &result,
                                               p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call gpio_sysfs_enable_ctrl failed (card=%u) (clnt_status=%d)", card, clnt_status); 
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_gpio_sysfs_disable_ctrl( uint8_t card,
                                            uint32_t gpio_class,
                                            uint32_t gpio_offset )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        gpio_params  gpio_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        gpio_arg.card = server_card;
        gpio_arg.class = gpio_class;
        gpio_arg.offset = gpio_offset;
        
        clnt_status = gpio_sysfs_disable_ctrl_9(&gpio_arg,
                                                &result,
                                                p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error( "RPC call gpio_sysfs_disable_ctrl failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_gpio_sysfs_configure_direction( uint8_t card,
                                                   uint32_t gpio_class,
                                                   uint32_t gpio_offset,
                                                   gpio_ddr_t dir )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        gpio_direction  gpio_dir_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        gpio_dir_arg.params.card = server_card;
        gpio_dir_arg.params.class = gpio_class;
        gpio_dir_arg.params.offset = gpio_offset;
        gpio_dir_arg.direction = dir;
        
        clnt_status = gpio_sysfs_configure_direction_9(&gpio_dir_arg,
                                                       &result,
                                                       p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call gpio_sysfs_configure_direction failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);    
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_gpio_sysfs_read_value( uint8_t card,
                                          uint32_t gpio_class,
                                          uint32_t gpio_offset,
                                          uint8_t *p_value )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        gpio_read_value result;
        gpio_params gpio_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        gpio_arg.card = server_card;
        gpio_arg.class = gpio_class;
        gpio_arg.offset = gpio_offset;

        clnt_status = gpio_sysfs_read_value_9(&gpio_arg,
                                              &result,
                                              p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call gpio_sysfs_read_value failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            {
                *p_value = result.value;
            }
        }
    }
    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t rpc_client_gpio_sysfs_write_value( uint8_t card,
                                           uint32_t gpio_class,
                                           uint32_t gpio_offset,
                                           uint8_t value )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        gpio_value gpio_val_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        gpio_val_arg.params.card = server_card;
        gpio_val_arg.params.class = gpio_class;
        gpio_val_arg.params.offset = gpio_offset;
        gpio_val_arg.value = value;
        
        clnt_status = gpio_sysfs_write_value_9(&gpio_val_arg,
                                               &result,
                                               p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call gpio_sysfs_write_value failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);    
}

int32_t rpc_client_rfic_reset_and_init( uint8_t card )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = 0;

        clnt_status = rfic_reset_and_init_9(&rfic_arg, &result, p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_reset_and_init failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);    
}

int32_t rpc_client_rfic_write_rx_gain_mode( uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_gain_t mode )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_write_uint32 write_uint32_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_uint32_arg.id.card = server_card;
        write_uint32_arg.id.hdl = (uint32_t)(hdl);
        write_uint32_arg.val = (uint32_t)(mode);
        
        clnt_status = rfic_write_rx_gain_mode_9(&write_uint32_arg,
                                                &result,
                                                p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_write_rx_gain_mode failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

int32_t rpc_client_rfic_read_rx_gain_mode( uint8_t card,
                                           skiq_rx_hdl_t hdl, 
                                           skiq_rx_gain_t *p_gain_mode )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        rfic_read_uint32 result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = hdl;

        clnt_status = rfic_read_rx_gain_mode_9(&rfic_arg,
                                               &result,
                                               p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call read_rx_gain_mode failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            {
                *p_gain_mode = (skiq_rx_gain_t)(result.val);
            }
        }
    }
    return (status);
}

int32_t rpc_client_rfic_write_rx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_rx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_write_sr_and_bw write_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_arg.id.card = server_card;
        write_arg.id.hdl = (uint32_t)(hdl);
        write_arg.sample_rate = sample_rate;
        write_arg.bandwidth = bandwidth;
        
        clnt_status = rfic_write_rx_sample_rate_and_bandwidth_9(&write_arg,
                                                                &result,
                                                                p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_write_rx_sample_rate_and_bandwidth failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

int32_t rpc_client_rfic_read_rx_sample_rate( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        rfic_read_sr result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = hdl;

        clnt_status = rfic_read_rx_sample_rate_9(&rfic_arg,
                                                 &result,
                                                 p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_read_rx_sample_rate failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            { 
                *p_rate = result.req_sample_rate;
                *p_actual_rate = result.act_sample_rate;
            }
        }
    }
    return (status);
}

int32_t rpc_client_rfic_read_rx_chan_bandwidth( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        rfic_read_chan_bw result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = hdl;

        clnt_status = rfic_read_rx_chan_bandwidth_9(&rfic_arg,
                                                    &result,
                                                    p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_read_rx_chan_bandwidth failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            { 
                *p_bandwidth = result.req_bandwidth;
                *p_actual_bandwidth = result.act_bandwidth;
            }
        }
    }
    return (status);
}

int32_t rpc_client_rfic_write_tx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_tx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_write_sr_and_bw write_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_arg.id.card = server_card;
        write_arg.id.hdl = (uint32_t)(hdl);
        write_arg.sample_rate = sample_rate;
        write_arg.bandwidth = bandwidth;
        
        clnt_status = rfic_write_tx_sample_rate_and_bandwidth_9(&write_arg,
                                                                &result,
                                                                p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_write_tx_sample_rate_and_bandwidth failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }
    return (status);
}

int32_t rpc_client_rfic_read_tx_sample_rate( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        rfic_read_sr result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = hdl;

        clnt_status = rfic_read_tx_sample_rate_9(&rfic_arg,
                                                 &result,
                                                 p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_read_tx_sample_rate failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            { 
                *p_rate = result.req_sample_rate;
                *p_actual_rate = result.act_sample_rate;
            }
        }
    }
    return (status);
}

int32_t rpc_client_rfic_read_tx_chan_bandwidth( uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        rfic_read_chan_bw result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = hdl;

        clnt_status = rfic_read_tx_chan_bandwidth_9(&rfic_arg,
                                                    &result,
                                                    p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_read_tx_chan_bandwidth failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            { 
                *p_bandwidth = result.req_bandwidth;
                *p_actual_bandwidth = result.act_bandwidth;
            }
        }
    }
    return (status);
}

int32_t rpc_client_rfic_write_rx_cal_mode(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_cal_mode_t mode )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_write_uint32 write_uint32_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_uint32_arg.id.card = server_card;
        write_uint32_arg.id.hdl = (uint32_t)(hdl);
        write_uint32_arg.val = (uint32_t)(mode);
        
        clnt_status = rfic_write_rx_cal_mode_9( &write_uint32_arg,
                                                &result,
                                                p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_write_rx_cal_mode failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

int32_t rpc_client_rfic_write_tx_quadcal_mode(  uint8_t card, 
                                                skiq_rx_hdl_t hdl, 
                                                skiq_tx_quadcal_mode_t mode )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_write_uint32 write_uint32_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_uint32_arg.id.card = server_card;
        write_uint32_arg.id.hdl = (uint32_t)(hdl);
        write_uint32_arg.val = (uint32_t)(mode);
        
        clnt_status = rfic_write_tx_quadcal_mode_9( &write_uint32_arg,
                                                    &result,
                                                    p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_write_tx_quadcal_mode failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

int32_t rpc_client_rfic_write_rx_cal_mask(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            uint32_t mask )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        int32_t result;
        rfic_write_uint32 write_uint32_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        write_uint32_arg.id.card = server_card;
        write_uint32_arg.id.hdl = (uint32_t)(hdl);
        write_uint32_arg.val = (uint32_t)(mask);
        
        clnt_status = rfic_write_rx_cal_mask_9( &write_uint32_arg,
                                                &result,
                                                p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_write_rx_cal_mask failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result;
        }
    }

    return (status);
}

int32_t rpc_client_rfic_read_rx_cal_mask( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint32_t *p_mask )
{
    int32_t status=-ENODEV;
    uint8_t server_card=0;
    CLIENT *p_client=NULL;
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        enum clnt_stat clnt_status;
        rfic_read_uint32 result;
        rfic_id rfic_arg;

        pthread_mutex_lock(&_client_mutex);
        server_card = _skiq_client[card].server_card;
        p_client = _skiq_client[card].p_client;

        rfic_arg.card = server_card;
        rfic_arg.hdl = hdl;

        clnt_status = rfic_read_rx_cal_mask_9( &rfic_arg,
                                               &result,
                                               p_client);
        pthread_mutex_unlock(&_client_mutex);

        if ( clnt_status != RPC_SUCCESS )
        {
            skiq_error("RPC call rfic_read_rx_cal_mask failed (card=%u) (clnt_status=%d)", card, clnt_status);
        }
        else
        {
            status = result.status;
            if( status == 0 )
            { 
                *p_mask = result.val;
            }
        }
    }
    return (status);
}
#else
// NO RPC support
int32_t sidekiq_rpc_client_init( skiq_xport_id_t *p_xport_id )
{
    return -ENOTSUP;
}

int32_t sidekiq_rpc_client_exit( skiq_xport_id_t *p_xport_id )
{
    return -ENOTSUP;
}

bool sidekiq_rpc_is_client_avail( uint8_t card )
{
    return false;
}

bool rpc_client_cal_is_rx_cal_data_present( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            skiq_rf_port_t port )
{
    return -ENOTSUP;
}

int32_t rpc_client_cal_read_cal_offset_in_dB( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              skiq_rf_port_t port,
                                              uint64_t lo_freq,
                                              uint8_t gain_index,
                                              double programmable_rxfir_gain_dB,
                                              double *p_cal_rx_gain_dB )
{
    return -ENOTSUP;
}

bool rpc_client_cal_is_iq_complex_cal_data_present( uint8_t card,
                                                    skiq_rx_hdl_t hdl )
{
    return -ENOTSUP;
}

int32_t rpc_client_cal_read_iq_complex_cal_parts( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  uint64_t lo_freq,
                                                  int8_t temper_deg_C,
                                                  double *p_real,
                                                  double *p_imag )
{
    return -ENOTSUP;
}

int32_t rpc_client_tune_write_rx_LO_freq( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint64_t freq,
                                          skiq_freq_tune_mode_t mode,
                                          uint64_t timestamp )
{
    return -ENOTSUP;
}

int32_t rpc_client_tune_write_tx_LO_freq( uint8_t card,
                                          skiq_tx_hdl_t hdl,
                                          uint64_t freq,
                                          skiq_freq_tune_mode_t mode,
                                          uint64_t timestamp )
{
    return -ENOTSUP;
}

int32_t rpc_client_gpio_sysfs_enable_ctrl( uint8_t card,
                                           uint32_t gpio_class,
                                           uint32_t gpio_offset )
{
    return -ENOTSUP;
}

int32_t rpc_client_gpio_sysfs_disable_ctrl( uint8_t card,
                                            uint32_t gpio_class,
                                            uint32_t gpio_offset )
{
    return -ENOTSUP;
}

int32_t rpc_client_gpio_sysfs_configure_direction( uint8_t card,
                                                   uint32_t gpio_class,
                                                   uint32_t gpio_offset,
                                                   uint32_t dir )
{
    return -ENOTSUP;
}

int32_t rpc_client_gpio_sysfs_read_value( uint8_t card,
                                          uint32_t gpio_class,
                                          uint32_t gpio_offset,
                                          uint8_t *p_value )
{
    return -ENOTSUP;
}

int32_t rpc_client_gpio_sysfs_write_value( uint8_t card,
                                           uint32_t gpio_class,
                                           uint32_t gpio_offset,
                                           uint8_t value )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_reset_and_init( uint8_t card )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_write_rx_gain_mode( uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_gain_t mode )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_read_rx_gain_mode( uint8_t card,
                                           skiq_rx_hdl_t hdl, 
                                           skiq_rx_gain_t *p_gain_mode )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_write_rx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_rx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_read_rx_sample_rate( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_read_rx_chan_bandwidth( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_write_tx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_tx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_read_tx_sample_rate( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_read_tx_chan_bandwidth( uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_write_tx_quadcal_mode(  uint8_t card, 
                                                skiq_rx_hdl_t hdl, 
                                                skiq_tx_quadcal_mode_t mode )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_write_rx_cal_mode(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_cal_mode_t mode )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_write_rx_cal_mask(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            uint32_t mask )
{
    return -ENOTSUP;
}

int32_t rpc_client_rfic_read_rx_cal_mask(  uint8_t card, 
                                           skiq_rx_hdl_t hdl, 
                                           uint32_t *p_mask )
{
    return -ENOTSUP;
}

#endif // end HAS_RPC
