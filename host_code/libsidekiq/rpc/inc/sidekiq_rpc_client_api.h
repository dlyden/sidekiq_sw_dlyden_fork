#ifndef __SIDEKIQ_RPC_CLIENT_API_H
#define __SIDEKIQ_RPC_CLIENT_API_H

/*****************************************************************************/
/** @file sidekiq_rpc_client_api.h
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface for a client using RPC.
*/

#include <stdbool.h>

#include "sidekiq_types.h"
#include "sidekiq_xport_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************/
/** The sidekiq_rpc_client_init function is responsible for initializing a
    client RPC connection to the IP address specified for the card.

    @param[in] p_xport_id pointer to the transport ID to use with RPC

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t sidekiq_rpc_client_init( skiq_xport_id_t *p_xport_id );

/*****************************************************************************/
/** The sidekiq_rpc_client_exit function is responsible for closing a
    client RPC connection for the Sidekiq card.

    @param[in] p_xport_id pointer to the transport ID that was using RPC

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t sidekiq_rpc_client_exit( skiq_xport_id_t *p_xport_id );

/*****************************************************************************/
/** The sidekiq_is_rpc_client_avail function is responsible for determing if
    a RPC client connection has been established for the card.

    @param[in] card card number of interest

    @return bool true if the RPC client connection exists
*/
bool sidekiq_rpc_is_client_avail( uint8_t card );    

///////////////////////////////////////////////////////////////////////////////    
// calibration API

/*****************************************************************************/
/** The rpc_client_cal_is_rx_cal_data_present function provides a RPC client 
    call for cal_is_rx_cal_data_present().

    refer to calibration function for parameter details
*/
bool rpc_client_cal_is_rx_cal_data_present( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            skiq_rf_port_t port );

/*****************************************************************************/
/** The rpc_client_cal_read_cal_offset_in_dB function provides a RPC client call
    for cal_read_cal_offset_in_dB

    refer to calibration function for parameter details
*/    
int32_t rpc_client_cal_read_cal_offset_in_dB( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              skiq_rf_port_t port,
                                              uint64_t lo_freq,
                                              uint8_t gain_index,
                                              double programmable_rxfir_gain_dB,
                                              double *p_cal_rx_gain_dB );

/*****************************************************************************/
/** The rpc_client_cal_is_iq_complex_cal_data_present function provides a RPC 
    client call for cal_is_iq_complex_data_present

    refer to calibration function for parameter details
*/        
bool rpc_client_cal_is_iq_complex_cal_data_present( uint8_t card,
                                                    skiq_rx_hdl_t hdl );

/*****************************************************************************/
/** The rpc_client_cal_is_iq_complex_cal_data_present function provides a RPC 
    client call for cal_is_iq_complex_data_present

    refer to calibration function for parameter details
*/            
int32_t rpc_client_cal_read_iq_complex_cal_parts( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  uint64_t lo_freq,
                                                  int8_t temper_deg_C,
                                                  double *p_real,
                                                  double *p_imag );
// end calibration API
///////////////////////////////////////////////////////////////////////////////    


///////////////////////////////////////////////////////////////////////////////        
// tune API
    
/*****************************************************************************/
/** The rpc_client_tune_write_rx_LO_freq function provides a RPC client call 
    for tune_write_rx_LO_freq.

    refer to tune function for parameter details
*/        

int32_t rpc_client_tune_write_rx_LO_freq( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint64_t freq,
                                          skiq_freq_tune_mode_t mode,
                                          uint64_t timestamp );

/*****************************************************************************/
/** The rpc_client_tune_write_tx_LO_freq function provides a RPC client call for 
    tune_write_tx_LO_freq.

    refer to tune function for parameter details
*/        

int32_t rpc_client_tune_write_tx_LO_freq( uint8_t card,
                                          skiq_tx_hdl_t hdl,
                                          uint64_t freq,
                                          skiq_freq_tune_mode_t mode,
                                          uint64_t timestamp );
// end tune API
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////    
// GPIO sysfs API
    
/*****************************************************************************/
/** The rpc_client_gpio_sysfs_enable_ctrl function provides a RPC client call 
    for gpio_sysfs_enable_ctrl.

    refer to GPIO sysfs function for parameter details
*/        

int32_t rpc_client_gpio_sysfs_enable_ctrl( uint8_t card,
                                           uint32_t gpio_class,
                                           uint32_t gpio_offset );

/*****************************************************************************/
/** The rpc_client_gpio_sysfs_disable_ctrl function provides a RPC client call 
    for gpio_sysfs_disable_ctrl.

    refer to GPIO sysfs function for parameter details
*/        

int32_t rpc_client_gpio_sysfs_disable_ctrl( uint8_t card,
                                            uint32_t gpio_class,
                                            uint32_t gpio_offset );

/*****************************************************************************/
/** The rpc_client_gpio_sysfs_configure_direction function provides a RPC 
    client call for gpio_sysfs_configure_direction.

    refer to GPIO sysfs function for parameter details
*/        

int32_t rpc_client_gpio_sysfs_configure_direction( uint8_t card,
                                                   uint32_t gpio_class,
                                                   uint32_t gpio_offset,
                                                   uint32_t dir );

/*****************************************************************************/
/** The rpc_client_gpio_sysfs_read_value function provides a RPC client call 
    for gpio_sysfs_read_value

    refer to GPIO sysfs function for parameter details
*/        

int32_t rpc_client_gpio_sysfs_read_value( uint8_t card,
                                          uint32_t gpio_class,
                                          uint32_t gpio_offset,
                                          uint8_t *p_value );

/*****************************************************************************/
/** The rpc_client_gpio_sysfs function provides a RPC client call for 
    for gpio_sysfs_write_value

    refer to GPIO sysfs function for parameter details
*/        

int32_t rpc_client_gpio_sysfs_write_value( uint8_t card,
                                           uint32_t gpio_class,
                                           uint32_t gpio_offset,
                                           uint8_t value );
// end GPIO sysfs API
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// RFIC API

/*****************************************************************************/
/** The rpc_client_rfic_reset_and_init function provides a RPC client call for 
    for rfic_reset_and_init

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_reset_and_init( uint8_t card );

/*****************************************************************************/
/** The rpc_client_rfic_write_rx_gain_mode function provides a RPC client call 
    for rfic_write_rx_gain_mode

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_write_rx_gain_mode( uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_gain_t mode );

/*****************************************************************************/
/** The rpc_client_rfic_read_rx_gain_mode function provides a RPC client call 
    for rfic_read_rx_gain_mode

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_read_rx_gain_mode( uint8_t card,
                                           skiq_rx_hdl_t hdl, 
                                          skiq_rx_gain_t *p_gain_mode );

/*****************************************************************************/
/** The rpc_client_rfic_write_rx_sample_rate_and_bandwidth function provides a
    RPC client call for rfic_write_rx_sample_rate_and_bandwidth

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_write_rx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_rx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth );

/*****************************************************************************/
/** The rpc_client_rfic_read_rx_sample_rate function provides a RPC client call 
    for rfic_read_rx_sample_rate

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_read_rx_sample_rate( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate );

/*****************************************************************************/
/** The rpc_client_rfic_read_rx_chan_bandwidth function provides a RPC client call 
    for rfic_read_rx_chan_bandwidth

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_read_rx_chan_bandwidth( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth );

/*****************************************************************************/
/** The rpc_client_rfic_write_tx_sample_rate_and_bandwidth function provides a
    RPC client call for rfic_write_tx_sample_rate_and_bandwidth

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_write_tx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_tx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth );

/*****************************************************************************/
/** The rpc_client_rfic_read_tx_sample_rate function provides a RPC client call 
    for rfic_read_tx_sample_rate

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_read_tx_sample_rate( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate );

/*****************************************************************************/
/** The rpc_client_rfic_read_tx_chan_bandwidth function provides a RPC client call 
    for rfic_read_tx_chan_bandwidth

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_read_tx_chan_bandwidth( uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth );


/*****************************************************************************/
/** Called on the client side to initiate the RPC call.

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_write_rx_cal_mode(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_cal_mode_t mode );

/*****************************************************************************/
/** Called on the client side to initiate the RPC call.

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_write_tx_quadcal_mode(  uint8_t card, 
                                                skiq_rx_hdl_t hdl, 
                                                skiq_tx_quadcal_mode_t mode );

/*****************************************************************************/
/** Called on the client side to initiate the RPC call.

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_write_rx_cal_mask(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            uint32_t mask );

/*****************************************************************************/
/** Called on the client side to initiate the RPC call.

    refer to sidekiq_rfic function for parameter details
*/
int32_t rpc_client_rfic_read_rx_cal_mask(  uint8_t card, 
                                           skiq_rx_hdl_t hdl, 
                                           uint32_t *p_mask );

// end RFIC API
///////////////////////////////////////////////////////////////////////////////    

#ifdef __cplusplus
}
#endif

#endif // end __SIDEKIQ_RPC_CLIENT_API_H
