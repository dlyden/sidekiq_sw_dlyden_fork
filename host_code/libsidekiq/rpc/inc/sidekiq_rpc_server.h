#ifndef __SIDEKIQ_RPC_SERVER_API_H
#define __SIDEKIQ_RPC_SERVER_API_H

/*****************************************************************************/
/** @file sidekiq_rpc_server.h
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RPC server
*/


#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************/
/** The sidekiq_rpc_server_init function is responsible for initializing the
    server side to support RPC calls.

    @return int32_t  status where 0=success, anything else maps to negative errno
*/
int32_t sidekiq_rpc_server_init( void );
    
#ifdef __cplusplus
}
#endif

#endif // end __SIDEKIQ_RPC_SERVER_API_H
