/*! \file sidekiq_api_hwdev.h
    \brief Private low level hardware interface to libsidekiq

    <pre>
    Copyright 2019-2020 Epiq Solutions, All Rights Reserved
    </pre>
*/

#ifndef __SIDEKIQ_API_HWDEV__
#define __SIDEKIQ_API_HWDEV__

#include "inttypes.h"
#include "sidekiq_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
    @brief  GPSDO related definitions

    @addtogroup factory_gpsdo
    @{
*/

/** @brief  Structure to describe the GPSDO status registers */
typedef struct
{
    /** @brief  The raw status register value */
    uint32_t raw_value;
    /** @brief  Flag to indicate if the control loop has converged according to the algorithm */
    bool gpsdo_locked_state;
    /** @brief  Flag to indicate if the GPS is locked and has a fix */
    bool gps_has_fix;
    /** @brief  Flag to indicate the lock state of the PLL */
    bool pll_locked_state;
    /** @brief  Number of consecutive high frequency error values detected in a row */
    uint8_t counter_unlocked;
    /** @brief  Number of consecutive low frequency error values detected in a row */
    uint8_t counter_locked;
    /** @brief  Current gain parameter */
    uint8_t ki;
    /** @brief  Time at current gain setting */
    uint8_t stage_timer;
} skiq_hwdev_gpsdo_status_t;

/** @brief  Initializer macro for the "skiq_hwdev_gpsdo_status_t" data type */
#define SKIQ_HWDEV_GPSDO_STATUS_T_INITIALIZER   \
    {   \
        .raw_value = 0, \
        .gpsdo_locked_state = false,    \
        .gps_has_fix = false,   \
        .pll_locked_state = false,  \
        .counter_unlocked = 0,  \
        .counter_locked = 0,    \
        .ki = 0,    \
        .stage_timer = 0,   \
    }

/** @} */

/* For I2C access, refer to skiq_fact_i2c_write_before_read(), skiq_fact_i2c_write(), and skiq_fact_i2c_read().
   For FMC I2C access (if supported), specify the bus as external */

/*****************************************************************************/
/** The skiq_hwdev_write_fpga_reg function is responsible for writing data to
    the FPGA register specified by address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] address FPGA register address to write to
    @param[in] data value to be written to the address

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_write_fpga_reg( uint8_t card, uint32_t address, uint32_t data );

/*****************************************************************************/
/** The skiq_hwdev_read_fpga_reg function is responsible for reading the 
    FPGA register specified by the address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] address FPGA register address to read from
    @param[out] p_data pointer to where to store the value read

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_read_fpga_reg( uint8_t card, uint32_t address, uint32_t *p_data );

/*****************************************************************************/
/** The skiq_hwdev_write_rfic_reg function is responsible for writing data to
    the RFIC register specified by address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] cs chip select of the RFIC (0 for cards with single RFIC)
    @param[in] address RFIC register address to write to
    @param[in] data value to be written to the address

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_write_rfic_reg( uint8_t card, uint8_t cs, uint16_t addr, uint8_t data);

/*****************************************************************************/
/** The skiq_hwdev_read_rfic_reg function is responsible for reading the 
    RFIC register specified by the address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] cs chip select of the RFIC (0 for cards with single RFIC)
    @param[in] address RFIC register address to read from
    @param[out] p_data pointer to where to store the value read

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_read_rfic_reg( uint8_t card, uint8_t cs, uint16_t addr, uint8_t* p_data);


extern int32_t skiq_hwdev_io_exp_enable_cache_validation(uint8_t card, bool enable);

    

/**
    @brief  GPSDO related definitions

    @addtogroup factory_gpsdo
    @{
*/

/**
    @brief  Get the GPSDO frequency accuracy information from the specified card

    @param[in]  card            card number of the Sidekiq of interest
    @param[out] p_ppm           the frequency accuracy (in PPM); this should not be NULL
    @param[out] p_freq_counter  the raw frequency counter value from the GPSDO algorithm; this
                                should not be NULL

    @since  4.15.0

    @return 0 on success, else a negative errno
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EINVAL     if @p p_ppm or @p p_freq_counter are NULL
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
    @retval -EAGAIN     if the measurement is not available because the GPSDO algorithm is disabled
*/
extern int32_t skiq_hwdev_gpsdo_get_freq_accuracy(uint8_t card, double *p_ppm,
    int32_t *p_freq_counter);

/**
    @brief  Read the current warp voltage used by the GPSDO

    @param[in]  card            card number of the Sidekiq of interest
    @param[out] p_warp_voltage  the warp voltage value currently in use; this should not be NULL

    @since  4.15.0

    @return 0 on success, else a negative errno
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EINVAL     if @p p_warp_voltage is NULL
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
extern int32_t skiq_hwdev_gpsdo_read_warp_voltage(uint8_t card, uint32_t *p_warp_voltage);

/**
    @brief  Read the GPSDO algorithm status registers

    @param[in]  card        card number of the Sidekiq of interest
    @param[out] p_status    the GPSDO list of status registers; this should not be NULL

    @since  4.15.0

    @return 0 on success, else a negative errno
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EINVAL     if @p p_status is NULL
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
extern int32_t skiq_hwdev_gpsdo_read_status(uint8_t card, skiq_hwdev_gpsdo_status_t *p_status);

/**
    @brief  Set the GPSDO algorithm control parameters

    This function sets the control parameters for the GPSDO to be programmed the next time the
    GPSDO algorithm is enabled; if the algorithm is already running, the register values will
    be written to the FPGA

    @param[in]  card        card number of the Sidekiq of interest
    @param[in]  ki_start    initial Ki setting to use
    @param[in]  ki_end      maximum Ki value (minimum gain value)
    @param[in]  thresh_locked   frequency error to consider within lock
    @param[in]  thresh_unlocked frequency error to consider "out of lock"
    @param[in]  count_locked    the number of consecutive low frequency errors to see before
                                entering lock state
    @param[in]  count_unlocked  the number of consecutive high frequency errors to see before
                                leaving lock state
    @param[in]  time_in_stage   the time (in seconds) to stay at each Ki gain value before
                                reducing the gain
    @param[in]  warp_set_value  the initial VCO warp value to use

    @since  4.15.0

    @return 0 on success, else a negative errno
    @retval -ENOTSUP    if the Sidekiq part does not support an FPGA-based GPSDO
    @retval -ENOSYS     if the FPGA version does not meet minimum requirements for feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
    @retval -EINVAL     if @p warp_set_value contains an invalid value
*/
extern int32_t skiq_hwdev_gpsdo_set_control_parameters(uint8_t card, uint8_t ki_start,
    uint8_t ki_end, uint8_t thresh_locked, uint8_t thresh_unlocked, uint8_t count_locked,
    uint8_t count_unlocked, uint8_t time_in_stage, int32_t warp_set_value);

/** @} */
/*****************************************************************************/


#ifdef __cplusplus
}
#endif

#endif  /* __SIDEKIQ_API_HWDEV__ */
