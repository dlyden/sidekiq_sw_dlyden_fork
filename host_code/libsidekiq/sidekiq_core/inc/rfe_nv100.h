#ifndef __RFE_NV100_H__
#define __RFE_NV100_H__

/**
 * @file   rfe_nv100.h
 * @date   Tue May 19 15:10:09 CDT 2020
 *
 * @brief This file contains the public interface of the RFE (RF front end) implementation for NV100
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include "sidekiq_rfe.h"

/** @brief function pointers for NV100 RFE implementation */
extern rfe_functions_t rfe_nv100_funcs;

#endif  /* __RFE_NV100_H__ */
