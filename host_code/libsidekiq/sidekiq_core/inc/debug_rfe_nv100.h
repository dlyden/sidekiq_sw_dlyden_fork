#ifndef __DEBUG_RFE_NV100_H__
#define __DEBUG_RFE_NV100_H__

/**
 * @file   debug_rfe_nv100.h
 *
 * @brief This file contains debug macros / functions that should only be #included by the
 * rfe_nv100.c source file.
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static void
nv100_print_rx_filters(void)
{
    uint8_t i;

    debug_print("%27s --  low to high -- bs0  bs1  bs2 hi_lo_n\n", "RX1 filter" );
    for (i = 0; i < ARRAY_SIZE(rx1_filter_selections); i++)
    {
        filter_selection_t *f = &(rx1_filter_selections[i]);

        debug_print("%27s -- %4u to %4u --  %u    %u    %u     %u\n",
                    SKIQ_FILT_STRINGS[f->filter], f->lo, f->hi,
                    f->bs0, f->bs1, f->bs2, f->hi_lo_n);
    }

    debug_print("\n");
    debug_print("%27s --  low to high -- bs0  bs1  bs2 hi_lo_n\n", "RX2 filter" );
    for (i = 0; i < ARRAY_SIZE(rx2_filter_selections); i++)
    {
        filter_selection_t *f = &(rx2_filter_selections[i]);

        debug_print("%27s -- %4u to %4u --  %u    %u    %u     %u\n",
                    SKIQ_FILT_STRINGS[f->filter], f->lo, f->hi,
                    f->bs0, f->bs1, f->bs2, f->hi_lo_n);
    }
}
#else
static void
nv100_print_rx_filters(void) {}
#endif  /* DEBUG_PRINT_ENABLED */


#endif  /* __DEBUG_RFE_NV100_H__ */
