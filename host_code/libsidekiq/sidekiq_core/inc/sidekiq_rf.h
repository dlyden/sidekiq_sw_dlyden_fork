#ifndef __SIDEKIQ_RF_H__
#define __SIDEKIQ_RF_H__

/*****************************************************************************/
/** @file sidekiq_rf.h
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the common RF definitions used by various
    Sidekiq RF components.
*/

#include <stdint.h>

#define RATE_KHZ_TO_HZ (1000)
#define RATE_HZ_TO_KHZ (0.001)
#define RATE_MHZ_TO_HZ (1000000)
#define RATE_HZ_TO_MHZ (0.000001)

#define MHZ_TO_HZ(_mhz)                 ((uint64_t)(_mhz) * ((uint64_t)1e6))

/** @brief Parameters needed to identify an RF interface */
typedef struct
{
    uint8_t card;
    uint32_t chip_id;
    uint32_t hdl;
    uint8_t port; // this is actually the port as it relates to the RFIC, not the skiq_rf_port
} rf_id_t;

typedef struct
{
    uint64_t startFreq;
    uint64_t stopFreq;
} freq_range_t;

typedef struct
{
    uint16_t index;
    uint64_t freq;
} freq_hop_t;

/*****************************************************************************/
/** @brief Sidekiq RF capabilities
    @ref skiq_rf_capabilities_t

    @since Type definition added in @b v4.15.0
*/
typedef struct
{
    /* @{ */
    uint64_t minTxFreqHz;    /**< @brief Lowest TX frequency capability   */
    uint64_t maxTxFreqHz;    /**< @brief Maximum TX frequency capability  */
    uint64_t minRxFreqHz;    /**< @brief Lowest RX frequency capability   */
    uint64_t maxRxFreqHz;    /**< @brief Maximum RX frequency capability  */
     /* @} */
} skiq_rf_capabilities_t;

/*****************************************************************************/
/** Sidekiq rf capabilities initializer

    @since MACRO added in @b v4.15.0

    @param[in] var_name desired variable name
*/
#define SKIQ_RF_CAPABILITIES_INITIALIZER(_var)          \
    skiq_rf_capabilities_t _var = {                     \
        .minTxFreqHz = 0,                               \
        .maxTxFreqHz = 0,                               \
        .minRxFreqHz = 0,                               \
        .maxRxFreqHz = 0}
/* @endcond */


#endif  /* __SIDEKIQ_RF_H__ */
