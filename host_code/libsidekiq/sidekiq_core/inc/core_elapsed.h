/**
 * @file   core_elapsed.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Mon Jun  5 16:09:36 2017
 *
 * @brief
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 *
 * </pre>
 *
 */

#ifndef __CORE_ELAPSED_H__
#define __CORE_ELAPSED_H__

/***** INCLUDES *****/

#include <time.h>
#include <stdio.h>
#include <inttypes.h>

/***** DEFINES *****/

#define ELAPSED(_x)         struct elapsed _x = ELAPSED_INITIALIZER
#define ELAPSED_INITIALIZER (struct elapsed){ .total = { 0, 0 } }

/***** TYPEDEFS *****/

struct elapsed
{
    struct timespec start, stop, total;
};

/***** INLINE FUNCTIONS  *****/

static inline void elapsed_start(struct elapsed *e)
{
#if (defined ENABLE_ELAPSED) && (ENABLE_ELAPSED == 1)
    clock_gettime(CLOCK_MONOTONIC, &(e->start));
#endif  /* ENABLE_ELAPSED */
}

/* subtract e->start from e->stop and increment e->total by that difference */
static inline void elapsed_end(struct elapsed *e)
{
#if (defined ENABLE_ELAPSED) && (ENABLE_ELAPSED == 1)
    clock_gettime(CLOCK_MONOTONIC, &(e->stop));

    if ((e->stop.tv_nsec - e->start.tv_nsec) < 0)
    {
        e->total.tv_sec += e->stop.tv_sec - e->start.tv_sec - 1;
        e->total.tv_nsec += e->stop.tv_nsec - e->start.tv_nsec + (long)1e9;
    }
    else
    {
        e->total.tv_sec += e->stop.tv_sec - e->start.tv_sec;
        e->total.tv_nsec += e->stop.tv_nsec - e->start.tv_nsec;
    }
    if (e->total.tv_nsec > (long)1e9)
    {
        e->total.tv_sec += e->total.tv_nsec / (long)1e9;
        e->total.tv_nsec = e->total.tv_nsec % (long)1e9;
    }
#endif  /* ENABLE_ELAPSED */
}

#if (defined ENABLE_ELAPSED) && (ENABLE_ELAPSED == 1)
#define elapsed_print(_x)                                               \
    do {                                                                \
        printf("%30s: %4ld.%09ld\n",                                    \
               #_x,                                                     \
               (long)(_x)->total.tv_sec,                                \
               (_x)->total.tv_nsec);                                    \
    } while (0)
#else
#define elapsed_print(_x) do { } while(0)
#endif  /* ENABLE_ELAPSED */

#if (defined ENABLE_ELAPSED) && (ENABLE_ELAPSED == 1)
#define _elapsed_print_str(_x, _e)       \
    do {                                 \
        printf("%45s: %4ld.%09ld\n",     \
               _x,                       \
               (long)(_e)->total.tv_sec, \
               (_e)->total.tv_nsec);     \
    } while (0)
#else
#define _elapsed_print_str(_x, _e) do { } while(0)
#endif  /* ENABLE_ELAPSED */

#if (defined ENABLE_ELAPSED) && (ENABLE_ELAPSED == 1)
#define elapsed_call(_x)                        \
    do {                                        \
        ELAPSED(_my_elapsed);                   \
        elapsed_start(&_my_elapsed);            \
        _x;                                     \
        elapsed_end(&_my_elapsed);              \
        _elapsed_print_str(#_x, &_my_elapsed);  \
    } while(0)
#else
#define elapsed_call(_x) do { _x; } while(0)
#endif  /* ENABLE_ELAPSED */

static inline
void print_total( struct elapsed *e )
{
#if (defined ENABLE_ELAPSED) && (ENABLE_ELAPSED==1)
    printf("%3" PRId64 ".%09lu seconds\n", (int64_t)e->total.tv_sec, e->total.tv_nsec);
#else
#endif
}

#endif  /* __ELAPSED_H__ */
