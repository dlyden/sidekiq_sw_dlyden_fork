/**
 * @file   rfic_x2_private.h
 * @date   Fri May 17 09:46:44 2019
 * 
 * @brief  Function prototypes for private functions to share with test applications.
 * 
 * 
 */

#ifndef __RFIC_X2_PRIVATE_H__
#define __RFIC_X2_PRIVATE_H__

/***** INCLUDES *****/

#include <stdint.h>

#include "fpga_jesd_ctrl.h"


/***** DEFINES *****/


/***** FUNCTIONS *****/

int32_t reset_jesd_x2( uint8_t card,
                       uint8_t rx_preEmphasis,       // 0..7
                       uint8_t rx_serialAmplitude,   // 18..26
                       uint8_t tx_diffctrl,          // 0..16
                       uint8_t tx_precursor,         // 0..20
                       jesd_sync_result_t *p_sync_result );

int32_t reset_jesd_x2_defaults( uint8_t card,
                                jesd_sync_result_t *p_sync_result );


#endif /* __RFIC_X2_PRIVATE_H__ */
