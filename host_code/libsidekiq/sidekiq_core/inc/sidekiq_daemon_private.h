#ifndef __SIDEKIQ_DAEMON_PRIVATE_H__
#define __SIDEKIQ_DAEMON_PRIVATE_H__

/*! \file sidekiq_daemon_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** INCLUDES *****/
#include "sidekiq_api.h"
#include "sidekiq_types.h"

/***** DEFINES *****/
#define SKIQ_DAEMON_API_MAJOR (1)
#define SKIQ_DAEMON_API_MINOR (0)
#define SKIQ_DAEMON_API_PATCH (0)

#ifndef DEFAULT_PROBE_PORT_NUM
#   define DEFAULT_PROBE_PORT_NUM  3417 // completely arbitrary
#endif

#ifndef DEFAULT_PROBE_IP_ADDR
#   define DEFAULT_PROBE_IP_ADDR  "127.0.0.1"
#endif

#define DAEMON_HDR_SOM (0x5A5A5A5A)

#define RESPONSE_TIMEOUT_SEC (10) // we should always get a response within 10s (note: this is large due to X4 init time...there's probably a better way to deal with this special case

/***** TYPEDEFS *****/

// request types
typedef enum
{
    /////////////
    // probe socket
    SKIQ_DAEMON_GET_API=0, // api_payload_t
    SKIQ_DAEMON_GET_CARDS, // resp_msg_t + variable length list of cards (just uint8_t)
    SKIQ_DAEMON_RESERVE_CARD, // this is where the control socket created is created and reported

    /////////////
    // control socket
    SKIQ_DAEMON_RELEASE_CARD,   // resp_msg_t
    SKIQ_DAEMON_READ_FPGA_REG,  // read_fpga_resp_t
    SKIQ_DAEMON_WRITE_FPGA_REG, // resp_msg_t
    SKIQ_DAEMON_FPGA_REG_VERIFY, // resp_msg_t
    SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY, // resp_msg_t

    //////////////
    // RX streaming messages
    // for start streaming, we need to setup the streaming port
    // for stop streaming, we don't care about a handle so just an empty payload on the control port
    // for pause/resume/flush, no additional parameters needed
    SKIQ_DAEMON_RX_CONFIGURE, // resp_msg_t
    SKIQ_DAEMON_RX_PREP_SOCK, // we should get a port number of the RX stream
    SKIQ_DAEMON_RX_START_STREAMING, // resp_msg_t
    SKIQ_DAEMON_RX_STOP_STREAMING, // resp_msg_t
    SKIQ_DAEMON_RX_PAUSE_STREAMING, // resp_msg_t
    SKIQ_DAEMON_RX_RESUME_STREAMING, // resp_msg_t
    SKIQ_DAEMON_RX_FLUSH, // resp_msg_t

    //////////////
    // TX streaming messages
    // for start streaming, we need to setup the streaming port
    // for stop streaming, we don't care about a handle so just an empty payload on the control port
    SKIQ_DAEMON_TX_CONFIGURE, // resp_msg_t
    SKIQ_DAEMON_TX_START_STREAMING, // we should get a port number of the TX stream
    SKIQ_DAEMON_TX_PRE_STOP_STREAMING, // resp_msg_t
    SKIQ_DAEMON_TX_STOP_STREAMING, // resp_msg_t

    /////////////
    // both sockets
    SKIQ_DAEMON_RELEASE_SOCKET,

} skiq_daemon_req_t;

// request message header
typedef struct
{
    uint32_t som;
    uint32_t request_type;
    uint32_t seq_number;
    uint32_t payload_len;
} __attribute__((packed)) skiq_daemon_req_msg_t;
#define SKIQ_DAEMON_REQ_SIZE (sizeof(skiq_daemon_req_msg_t))

// response message header
typedef struct
{
    uint32_t som;
    int32_t status;
    uint32_t seq_number;
    uint32_t payload_len;
} __attribute__((packed)) skiq_daemon_resp_msg_t;
#define SKIQ_DAEMON_RESP_SIZE (sizeof(skiq_daemon_resp_msg_t))

///////////////////////////////////
// response

// API version
typedef struct
{
    uint8_t maj;
    uint8_t min;
    uint8_t patch;
} __attribute__((packed)) skiq_daemon_api_payload_t;
#define SKIQ_DAEMON_API_PAYLOAD_SIZE (sizeof(skiq_daemon_api_payload_t))

typedef struct
{
    uint16_t port;
} __attribute__((packed)) skiq_daemon_port_resp_payload_t;
#define SKIQ_DAEMON_PORT_RESP_PAYLOAD_SIZE (sizeof(skiq_daemon_port_resp_payload_t))

typedef struct
{
    uint32_t val;
} __attribute__((packed)) skiq_daemon_read_fpga_resp_payload_t;
#define SKIQ_DAEMON_READ_FPGA_RESP_PAYLOAD_SIZE (sizeof(skiq_daemon_read_fpga_resp_payload_t))

typedef struct
{
    skiq_daemon_resp_msg_t header;
    skiq_daemon_read_fpga_resp_payload_t payload;
} __attribute__((packed)) skiq_daemon_read_fpga_resp_t;
#define SKIQ_DAEMON_READ_FPGA_RESP_SIZE (sizeof(skiq_daemon_read_fpga_resp_t))

//////////////////////////////////
// request
typedef struct
{
    uint8_t card_num;
    skiq_xport_init_level_t level;
} __attribute__((packed)) skiq_daemon_reserve_card_payload_t;
#define SKIQ_DAEMON_RESERVE_CARD_PAYLOAD_SIZE (sizeof(skiq_daemon_reserve_card_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_reserve_card_payload_t payload;
} __attribute__((packed)) skiq_daemon_reserve_card_t;
#define SKIQ_DAEMON_RESERVE_CARD (sizeof(skiq_daemon_reserve_card_t))
    
typedef struct
{
    uint32_t val;
    uint32_t addr;
} __attribute__((packed)) skiq_daemon_write_fpga_payload_t;
#define SKIQ_DAEMON_WRITE_FPGA_PAYLOAD_SIZE (sizeof(skiq_daemon_write_fpga_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_write_fpga_payload_t payload;
} __attribute__((packed)) skiq_daemon_write_fpga_t;
#define SKIQ_DAEMON_WRITE_FPGA_SIZE (sizeof(skiq_daemon_write_fpga_t))

typedef struct
{
    uint32_t addr;
} __attribute__((packed)) skiq_daemon_read_fpga_req_payload_t;
#define SKIQ_DAEMON_READ_FPGA_REQ_PAYLOAD_SIZE (sizeof(skiq_daemon_read_fpga_req_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_read_fpga_req_payload_t payload;
} __attribute__((packed)) skiq_daemon_read_fpga_req_t;
#define SKIQ_DAEMON_READ_FPGA_REQ_SIZE (sizeof(skiq_daemon_read_fpga_req_t))

typedef struct
{
    uint32_t val;
    uint32_t addr;
} __attribute__((packed)) skiq_daemon_fpga_reg_verify_payload_t;
#define SKIQ_DAEMON_FPGA_REG_VERIFY_PAYLOAD_SIZE (sizeof(skiq_daemon_fpga_reg_verify_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_fpga_reg_verify_payload_t payload;
} __attribute__((packed)) skiq_daemon_fpga_reg_verify_t;
#define SKIQ_DAEMON_FPGA_REG_VERIFY_SIZE (sizeof(skiq_daemon_fpga_reg_verify_t))

typedef struct
{
    uint32_t val;
    uint32_t addr;
} __attribute__((packed)) skiq_daemon_fpga_reg_write_and_verify_payload_t;
#define SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY_PAYLOAD_SIZE (sizeof(skiq_daemon_fpga_reg_write_and_verify_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_fpga_reg_write_and_verify_payload_t payload;
} __attribute__((packed)) skiq_daemon_fpga_reg_write_and_verify_t;
#define SKIQ_DAEMON_FPGA_REG_WRITE_AND_VERIFY_SIZE (sizeof(skiq_daemon_fpga_reg_write_and_verify_t))

typedef struct
{
    uint32_t usleep_per_skiq_rx;
    uint32_t num_bytes_per_trans;
    uint8_t buffered;
} __attribute__((packed)) skiq_daemon_rx_configure_payload_t;
#define SKIQ_DAEMON_RX_CONFIGURE_PAYLOAD_SIZE (sizeof(skiq_daemon_rx_configure_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_rx_configure_payload_t payload;
} __attribute__((packed)) skiq_daemon_rx_configure_t;
#define SKIQ_DAEMON_RX_CONFIGURE_SIZE (sizeof(skiq_daemon_rx_configure_t))

typedef struct
{
    int32_t transfer_mode;
    uint32_t num_bytes_to_send;
    uint8_t num_send_threads;
    int32_t priority;
} __attribute__((packed)) skiq_daemon_tx_configure_payload_t;
#define SKIQ_DAEMON_TX_CONFIGURE_PAYLOAD_SIZE (sizeof(skiq_daemon_tx_configure_payload_t))
typedef struct
{
    skiq_daemon_req_msg_t header;
    skiq_daemon_tx_configure_payload_t payload;
} __attribute__((packed)) skiq_daemon_tx_configure_t;
#define SKIQ_DAEMON_TX_CONFIGURE_SIZE (sizeof(skiq_daemon_tx_configure_t))

// TX packet header
typedef struct
{
    uint32_t seq_number;
} __attribute__((packed)) skiq_daemon_tx_header_t;
#define SKIQ_DAEMON_TX_HEADER_SIZE (sizeof(skiq_daemon_tx_header_t))

// TX response message
typedef struct
{
    int32_t status;
    uint32_t seq_number;
} __attribute__((packed)) skiq_daemon_tx_resp_msg_t;
#define SKIQ_DAEMON_TX_RESP_SIZE (sizeof(skiq_daemon_tx_resp_msg_t))

#endif /* __SIDEKIQ_DAEMON_PRIVATE_H__ */
