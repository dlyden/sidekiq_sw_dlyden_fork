#ifndef __RFE_Z3U_ABC_H__
#define __RFE_Z3U_ABC_H__

/**
 * @file   rfe_z3u_a.h
 * @date   Thurs Jun 18 09:15:24 2020
 *
 * @brief  This file contains the public interface of the RFE for Sidekiq Z3u rev A/B/C
 * (similar to 2280)
 *
 *
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include "sidekiq_rfe.h"
#include "sidekiq_api_factory.h" /* for skiq_pin_value_t */


/** @brief function pointers for Z3u rev A/B/C/D RFE implementation */
extern rfe_functions_t rfe_z3u_abcd_funcs;

/**************************************************************************************************/
/** The rfe_z3u_override_tx_pa() function allows the caller to override the PA state of the
    specified RF identifier by specifying a pin value that is highly specific to Z3u rev A.

    @attention In order to restore the proper state of the Tx PA, the caller must call
    skiq_write_rf_port_config() twice, first specifying a configuration that is NOT the current /
    desired configuration, then again with the desired configuration.  The RFE caches the RF port
    configuration.

    @ingroup factoryfunctions

    @param[in] p_id Pointer to RF identifier struct
    @param[in] pin_value [::skiq_pin_value_t] desired pin value for TX_AMP_MANUAL_BIAS_ON_N

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP ATE_SUPPORT is not enabled for the library
    @retval -EINVAL Specified @a pin_value is unknown or unsupported
    @retval -EIO Input/output error communicating with the I/O expander
*/
int32_t
rfe_z3u_override_tx_pa( rf_id_t *p_id,
                         skiq_pin_value_t pin_value );

#endif  /* __RFE_Z3U_A_H__ */
