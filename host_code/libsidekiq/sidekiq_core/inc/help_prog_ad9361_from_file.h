#ifndef HELP_PROG_AD9361_FROM_FILE_H
#define HELP_PROG_AD9361_FROM_FILE_H

/*! \file help_prog_ad9361_from_file.h
 * \brief This file contains the public interface of the
 * help_proga_ad9361_from_file module.
 *
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 2/12/2013   JRO   Created file
 *
 *</pre>*/

/***** INCLUDES *****/
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
/***** DEFINES *****/


/***** Extern Functions  *****/
extern int32_t process_ad9361_file(FILE* fd, uint32_t chip, bool configure_gpio, 
                                   uint8_t *p_gpio_pos, bool *p_gpio_state, uint8_t num_gpio);

#endif
