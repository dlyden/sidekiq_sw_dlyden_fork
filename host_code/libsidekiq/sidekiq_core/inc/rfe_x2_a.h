#ifndef __RFE_X2_A_H__
#define __RFE_X2_A_H__

/*****************************************************************************/
/** @file rfe_x2_a.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFE (RF front end)
    implementation for Sidekiq m.2 rev B/C.
*/

#include "sidekiq_rfe.h"

/** @brief function pointers for Sidkeiq X2 A RFE implementation */
extern rfe_functions_t rfe_x2_a_funcs;

#endif /* __RFE_X2_A_H__ */
