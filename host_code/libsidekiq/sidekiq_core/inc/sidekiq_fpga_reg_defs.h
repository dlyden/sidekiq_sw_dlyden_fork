#ifndef __SIDEKIQ_FPGA_REG_DEFS_H__
#define __SIDEKIQ_FPGA_REG_DEFS_H__

/*! @file sidekiq_fpga_reg_defs.h
 *
 * @brief FPGA register definitions, bit field offsets and lengths.
 *
 * This file was generated on 2022-06-18T03:17:46.917767+00:00
 *
 * DO NOT MANUALLY MODIFY CONTENTS!
 *  
 * <pre>
 * Copyright 2022 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 */

#define FPGA_REG_GIT_HASH                                            (0x8000)
#define FPGA_REG_BUILD_DATE                                          (0x8004)
#define FPGA_REG_VERSION                                             (0x8008)
#define FPGA_REG_CAPABILITIES                                        (0x800C)
#define FPGA_REG_TEST_READ                                           (0x8010)
#define FPGA_REG_ADC_NUM_ERRORS_A                                    (0x8014)
#define FPGA_REG_VERSION2                                            (0x8018)
#define FPGA_REG_READ_RF_BAND_SEL                                    (0x801C)
#define FPGA_REG_RF_BAND_SEL                                         (0x8020)
#define FPGA_REG_TIMESTAMP_RST                                       (0x8024)
#define FPGA_REG_TX_NUM_SAMPLES                                      (0x8028)
#define FPGA_REG_RSSI_RD                                             (0x802C)
#define FPGA_REG_RFIC_SPI_CMD                                        (0x8030)
#define FPGA_REG_RFIC_SPI_RD_DATA                                    (0x8034)
#define FPGA_REG_I2C_WR_DATA                                         (0x8038)
#define FPGA_REG_I2C_SLAVE_CMD                                       (0x803C)
#define FPGA_REG_I2C_RD_DATA                                         (0x8040)
#define FPGA_REG_I2C_STATUS                                          (0x8044)
#define FPGA_REG_REGISTER_RESET                                      (0x8048)
#define FPGA_REG_PLL_SPI_WR                                          (0x804C)
#define FPGA_REG_PLL_SPI_RD                                          (0x8050)
#define FPGA_REG_GPS_STATUS                                          (0x8054)
#define FPGA_REG_UART_READ_DATA                                      (0x8058)
#define FPGA_REG_UART_READ_STROBE                                    (0x805C)
#define FPGA_REG_UART_WRITE_DATA                                     (0x8060)
#define FPGA_REG_GPS_CTRL                                            (0x8064)
#define FPGA_REG_LAST_RF_A_1PPS_TIMESTAMP_LOW                        (0x8068)
#define FPGA_REG_LAST_RF_A_1PPS_TIMESTAMP_HIGH                       (0x806C)
#define FPGA_REG_1PPS_RESET_TIMESTAMP_LOW                            (0x8070)
#define FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH                           (0x8074)
#define FPGA_REG_ADC_DAC_PRBS_TEST_CTRL                              (0x8078)
#define FPGA_REG_SYS_TIMESTAMP_LOW                                   (0x807C)
#define FPGA_REG_SYS_TIMESTAMP_HIGH                                  (0x8080)
#define FPGA_REG_I2C_1_WR_DATA                                       (0x8084)
#define FPGA_REG_I2C_1_SLAVE_CMD                                     (0x8088)
#define FPGA_REG_I2C_1_RD_DATA                                       (0x808C)
#define FPGA_REG_I2C_1_STATUS                                        (0x8090)
#define FPGA_REG_TX_RAM_WRITE                                        (0x8094)
#define FPGA_REG_ADC_NUM_ERRORS_B                                    (0x8098)
#define FPGA_REG_ADC_ERROR_B2                                        (0x809C)
#define FPGA_REG_GPIO_FREQ_HOP_CTRL                                  (0x80A0)
#define FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_LOW                       (0x80A4)
#define FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_HIGH                      (0x80A8)
#define FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_B_LOW                       (0x80AC)
#define FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_B_HIGH                      (0x80B0)
#define FPGA_REG_TEST_WRITE                                          (0x80B4)
#define FPGA_REG_BASELINE_VCS_STATUS                                 (0x80B8)
#define FPGA_REG_DMA_CTRL_RX1                                        (0x8100)
#define FPGA_REG_IQ_MULTIPLIER_RX1                                   (0x8104)
#define FPGA_REG_DMA_CTRL_RX5                                        (0x8108)
#define FPGA_REG_DMA_CTRL_RX6                                        (0x810C)
#define FPGA_REG_TIMESTAMP_C_LOW                                     (0x8110)
#define FPGA_REG_TIMESTAMP_C_HIGH                                    (0x8114)
#define FPGA_REG_TIMESTAMP_D_LOW                                     (0x8118)
#define FPGA_REG_TIMESTAMP_D_HIGH                                    (0x811C)
#define FPGA_REG_LAST_RF_C_1PPS_TIMESTAMP_LOW                        (0x8120)
#define FPGA_REG_LAST_RF_C_1PPS_TIMESTAMP_HIGH                       (0x8124)
#define FPGA_REG_LAST_RF_D_1PPS_TIMESTAMP_LOW                        (0x8128)
#define FPGA_REG_LAST_RF_D_1PPS_TIMESTAMP_HIGH                       (0x812C)
#define FPGA_REG_DECIMATOR_PROG_COEFFS_RX1                           (0x8130)
#define FPGA_REG_DECIMATOR_CTRL_RX1                                  (0x8134)
#define FPGA_REG_DECIMATOR_BUILD_INFO_RX1                            (0x8138)
#define FPGA_REG_DECIMATOR_PROG_COEFFS_RX5                           (0x813C)
#define FPGA_REG_DECIMATOR_CTRL_RX5                                  (0x8140)
#define FPGA_REG_DECIMATOR_BUILD_INFO_RX5                            (0x8144)
#define FPGA_REG_DECIMATOR_PROG_COEFFS_RX6                           (0x8148)
#define FPGA_REG_DECIMATOR_CTRL_RX6                                  (0x814C)
#define FPGA_REG_DECIMATOR_BUILD_INFO_RX6                            (0x8150)
#define FPGA_REG_DMA_CTRL_RX2                                        (0x8200)
#define FPGA_REG_IQ_MULTIPLIER_RX2                                   (0x8204)
#define FPGA_REG_DECIMATOR_PROG_COEFFS_RX2                           (0x8230)
#define FPGA_REG_DECIMATOR_CTRL_RX2                                  (0x8234)
#define FPGA_REG_DECIMATOR_BUILD_INFO_RX2                            (0x8238)
#define FPGA_REG_DMA_CTRL_RX3                                        (0x8300)
#define FPGA_REG_IQ_MULTIPLIER_RX3                                   (0x8304)
#define FPGA_REG_DECIMATOR_PROG_COEFFS_RX3                           (0x8330)
#define FPGA_REG_DECIMATOR_CTRL_RX3                                  (0x8334)
#define FPGA_REG_DECIMATOR_BUILD_INFO_RX3                            (0x8338)
#define FPGA_REG_SPI_BUILD_CFG                                       (0x8340)
#define FPGA_REG_FASTLOCK_RAM_ADDR                                   (0x8344)
#define FPGA_REG_FASTLOCK_RAM_WR_DATA_REG                            (0x8348)
#define FPGA_REG_FASTLOCK_RAM_RD_DATA_REG                            (0x834C)
#define FPGA_REG_FASTLOCK_ASSIGNMENT                                 (0x8350)
#define FPGA_REG_SPICE_CTRL                                          (0x8354)
#define FPGA_REG_SPICE_POLL_CFG                                      (0x8358)
#define FPGA_REG_SPICE_LAUNCH                                        (0x835C)
#define FPGA_REG_SPICE_INSTR_RAM_ADDR                                (0x8360)
#define FPGA_REG_SPICE_INSTR_RAM_WR_DATA                             (0x8364)
#define FPGA_REG_SPICE_INSTR_RD_DATA_REG                             (0x8368)
#define FPGA_REG_SPICE_DATA_RAM_ADDR                                 (0x836C)
#define FPGA_REG_SPICE_DATA_RAM_WR_DATA                              (0x8370)
#define FPGA_REG_SPICE_DATA_RD_DATA_REG                              (0x8374)
#define FPGA_REG_SPI_SYS_TS_MSB                                      (0x8378)
#define FPGA_REG_SPI_SYS_TS_LSB                                      (0x837C)
#define FPGA_REG_DMA_CTRL_RX4                                        (0x8400)
#define FPGA_REG_IQ_MULTIPLIER_RX4                                   (0x8404)
#define FPGA_REG_DECIMATOR_PROG_COEFFS_RX4                           (0x8430)
#define FPGA_REG_DECIMATOR_CTRL_RX4                                  (0x8434)
#define FPGA_REG_DECIMATOR_BUILD_INFO_RX4                            (0x8438)
#define FPGA_REG_GPIF_TRISTATE                                       (0x8600)
#define FPGA_REG_GPIF_READ                                           (0x8604)
#define FPGA_REG_GPIF_WRITE                                          (0x8608)
#define FPGA_REG_GPIO_TRISTATE                                       (0x860C)
#define FPGA_REG_GPIO_READ                                           (0x8610)
#define FPGA_REG_GPIO_WRITE                                          (0x8614)
#define FPGA_REG_TX_ERROR_COUNT                                      (0x8618)
#define FPGA_REG_TX_UNDERRUN_TIME                                    (0x861C)
#define FPGA_REG_TIMESTAMP_A_LOW                                     (0x8620)
#define FPGA_REG_TIMESTAMP_A_HIGH                                    (0x8624)
#define FPGA_REG_TIMESTAMP_B_LOW                                     (0x8628)
#define FPGA_REG_TIMESTAMP_B_HIGH                                    (0x862C)
#define FPGA_REG_SET_ALL_TIMESTAMP_LOW                               (0x8630)
#define FPGA_REG_SET_ALL_TIMESTAMP_HIGH                              (0x8634)
#define FPGA_REG_LAST_SYS_1PPS_TIMESTAMP_LOW                         (0x8638)
#define FPGA_REG_LAST_SYS_1PPS_TIMESTAMP_HIGH                        (0x863C)
#define FPGA_REG_LAST_RF_B_1PPS_TIMESTAMP_LOW                        (0x8640)
#define FPGA_REG_LAST_RF_B_1PPS_TIMESTAMP_HIGH                       (0x8644)
#define FPGA_REG_TEMP_READINGS                                       (0x8648)
#define FPGA_REG_FLASH_SPI_CTRL                                      (0x864C)
#define FPGA_REG_FLASH_SPI_WRITE_DATA                                (0x8650)
#define FPGA_REG_FLASH_SPI_READ_ACK                                  (0x8654)
#define FPGA_REG_FLASH_SPI_READ_DATA                                 (0x8658)
#define FPGA_REG_FLASH_SPI_STATUS                                    (0x865C)
#define FPGA_REG_I2C_2_WR_DATA                                       (0x8660)
#define FPGA_REG_I2C_2_SLAVE_CMD                                     (0x8664)
#define FPGA_REG_I2C_2_RD_DATA                                       (0x8668)
#define FPGA_REG_I2C_2_STATUS                                        (0x866C)
#define FPGA_REG_GPSDO_STATUS                                        (0x8670)
#define FPGA_REG_XADC_START                                          (0x8674)
#define FPGA_REG_XADC_READ_BACK                                      (0x8678)
#define FPGA_REG_LAST_TX_TIMESTAMP_LOW                               (0x867C)
#define FPGA_REG_LAST_TX_TIMESTAMP_HIGH                              (0x8680)
#define FPGA_REG_ATTEN_DATA                                          (0x8684)
#define FPGA_REG_ATTEN_STAT                                          (0x8688)
#define FPGA_REG_JESD_DRP_ADDR_CTRL                                  (0x868C)
#define FPGA_REG_JESD_DRP_RDATA                                      (0x8690)
#define FPGA_REG_JESD_DRP_WDATA                                      (0x8694)
#define FPGA_REG_RX_PRBS_ERROR_COUNT                                 (0x8698)
#define FPGA_REG_TX_MEM_DATA                                         (0x869C)
#define FPGA_REG_TX_MEM_LOOP_SIZE                                    (0x86A0)
#define FPGA_REG_FMC_CTRL                                            (0x86A4)
#define FPGA_REG_FPGA_CTRL                                           (0x86A8)
#define FPGA_REG_JESD_STATUS                                         (0x86AC)
#define FPGA_REG_JESD_PRBS_CTRL                                      (0x86B0)
#define FPGA_REG_JESD_UNSYNC_COUNTERS                                (0x86B4)
#define FPGA_REG_JESD_PHY_RX_RATE                                    (0x86B8)
#define FPGA_REG_WBSTAR                                              (0x86BC)
#define FPGA_REG_ICAP_WRITE_DATA                                     (0x86C0)
#define FPGA_REG_ICAP_READ_DATA                                      (0x86C4)
#define FPGA_REG_GPSDO_READ_COUNTER                                  (0x86C8)
#define FPGA_REG_GPSDO_READ_WARP                                     (0x86CC)
#define FPGA_REG_GPSDO_CONFIG1                                       (0x86D0)
#define FPGA_REG_GPSDO_CONFIG2                                       (0x86D4)
#define FPGA_REG_JESD_STATUS2                                        (0x86D8)
#define FPGA_REG_JESD_CTRL                                           (0x86DC)
#define FPGA_REG_DEBUG_IQ_SUBSAMPLE0                                 (0x86E0)
#define FPGA_REG_DEBUG_IQ_SUBSAMPLE1                                 (0x86E4)
#define FPGA_REG_CLK_RATE_EST0                                       (0x86E8)
#define FPGA_REG_CLK_RATE_EST1                                       (0x86EC)

/* RX_HDL_TO_FPGA_REG is a macro for converting between a specific Rx interface handle (RxA1, RxA2,
 * RxB1, RxB2, RxC1, or RxD1) and the FPGA registers used to configure that interface */
#define RX_HDL_TO_FPGA_REG(_hdl,_reg)   ( (_hdl) == skiq_rx_hdl_C1 ? _reg ## _RX5 : \
                                          (_hdl) == skiq_rx_hdl_D1 ? _reg ## _RX6 : \
                                          (_reg ## _RX1+( (_hdl) *0x100 ) ) )

/* LEGACY_RX_HDL_TO_FPGA_REG is a macro for converting between a specific Rx interface handle
 * (limited to A1 through B2) and the FPGA registers used to configure that interface.  Any handle
 * provided above B2 will be provided the FPGA_REG_GIT_HASH address as a way to squash / control
 * behavior */
#define LEGACY_RX_HDL_TO_FPGA_REG(_hdl,_reg)   ( (_hdl) > skiq_rx_hdl_B2 ? FPGA_REG_GIT_HASH : \
                                                 (_reg ## _RX1+( (_hdl) *0x100 ) ) )

 /* RX_HDL_TO_FPGA_REG_OFFSET is legacy and should not be used in new code.  Use
  * FPGA_REG_DMA_CTRL_() and FPGA_REG_IQ_MULT_() instead */
#define RX_HDL_TO_FPGA_REG_OFFSET(_hdl) FPGA_REG_DMA_CTRL_(_hdl)
#define FPGA_REG_DMA_CTRL_(_hdl)        RX_HDL_TO_FPGA_REG(_hdl,FPGA_REG_DMA_CTRL)
#define FPGA_REG_IQ_MULT_(_hdl)         LEGACY_RX_HDL_TO_FPGA_REG(_hdl,FPGA_REG_IQ_MULTIPLIER)
#define FPGA_REG_DEC_PROG_(_hdl)        RX_HDL_TO_FPGA_REG(_hdl,FPGA_REG_DECIMATOR_PROG_COEFFS)
#define FPGA_REG_DEC_CTRL_(_hdl)        RX_HDL_TO_FPGA_REG(_hdl,FPGA_REG_DECIMATOR_CTRL)
#define FPGA_REG_DEC_INFO_(_hdl)        RX_HDL_TO_FPGA_REG(_hdl,FPGA_REG_DECIMATOR_BUILD_INFO)

/* the following is the general structure to be used for
   defining bitfields within an FPGA reg:

    #define <bitfield lsb>_OFFSET <bit pos>
    #define <bitfield>_LEN        <num bits>
 */

/*--- bit field of VERSION (addr 0x8008) ---*/
#define MINOR_OFFSET                                       (0)
#define MINOR_LEN                                          (8)           /* FPGA minor version */

/*--- bit field of VERSION (addr 0x8008) ---*/
#define MAJOR_OFFSET                                       (8)
#define MAJOR_LEN                                          (8)           /* FPGA major version */

/*--- bit field of VERSION (addr 0x8008) ---*/
#define BOARD_ID_0_OFFSET                                  (16)
#define BOARD_ID_0_LEN                                     (1)

/*--- bit field of VERSION (addr 0x8008) ---*/
#define BOARD_ID_1_OFFSET                                  (17)
#define BOARD_ID_1_LEN                                     (1)

/*--- bit field of VERSION (addr 0x8008) ---*/
#define BOARD_ID_2_OFFSET                                  (18)
#define BOARD_ID_2_LEN                                     (1)

/*--- bit field of VERSION (addr 0x8008) ---*/
#define NUM_OBSRX_CHANNELS_OFFSET                          (22)
#define NUM_OBSRX_CHANNELS_LEN                             (2)           /* obsrx chans -- 0: none, 1: 1 chan, 2: 2 chan, 3: invalid */

/*--- bit field of VERSION (addr 0x8008) ---*/
#define TX_FIFO_SIZE_OFFSET                                (24)
#define TX_FIFO_SIZE_LEN                                   (3)

/*--- bit field of VERSION (addr 0x8008) ---*/
#define NUM_TX_CHANNELS_OFFSET                             (27)
#define NUM_TX_CHANNELS_LEN                                (2)           /* tx chans -- 0: none, 1: 1 chan, 2: 2 chan, 3: invalid */

/*--- bit field of VERSION (addr 0x8008) ---*/
#define NUM_RX_CHANNELS_OFFSET                             (29)
#define NUM_RX_CHANNELS_LEN                                (2)           /* rx chans -- 0: none, 1: 1 chan, 2: 2 chan, 3: invalid */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define SYSTEM_CLOCK_RATE_OFFSET                           (0)
#define SYSTEM_CLOCK_RATE_LEN                              (3)           /* system clock rate -- 0: 40 MHz, 1: 122.88 MHz, others: currently invalid */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define SELF_RELOAD_OFFSET                                 (3)
#define SELF_RELOAD_LEN                                    (1)           /* Set to 1 if supports the CAPABILITY SELF RELOAD */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define SELF_RELOAD_WBSTAR_OFFSET                          (4)
#define SELF_RELOAD_WBSTAR_LEN                             (1)           /* Set to 1 if supports the CAPABILITY SELF RELOAD with WBSTAR */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define TX_TS_LATE_SUPPORT_OFFSET                          (5)
#define TX_TS_LATE_SUPPORT_LEN                             (1)           /* Set to 1 if supports the CAPABILITY of Sending Late Packet in Timestamp Mode */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define GPS_MODULE_SUPPORT_OFFSET                          (6)
#define GPS_MODULE_SUPPORT_LEN                             (1)           /* Set to 1 if supports the CAPABILITY of Supporting an External GPS Module */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define IQ_MULT_SUPPORT_OFFSET                             (7)
#define IQ_MULT_SUPPORT_LEN                                (1)           /* Set to 1 if supports the CAPABILITY of Supporting the IQ Multipliers */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define MANUAL_TRIGGER_SUPPORT_OFFSET                      (8)
#define MANUAL_TRIGGER_SUPPORT_LEN                         (1)           /* Set to 1 if supports the CAPABILITY of Supporting the Manual Trigger */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define FLASH_SUPPORT_OFFSET                               (9)
#define FLASH_SUPPORT_LEN                                  (1)           /* Set to 1 if supports the CAPABILITY of Flash interface */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define USER_BAR_LOCATION_OFFSET                           (10)
#define USER_BAR_LOCATION_LEN                              (3)           /* Indicates the BAR that the user registers are located in. */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define RESET_SUPPORT_OFFSET                               (13)
#define RESET_SUPPORT_LEN                                  (1)           /* Set to 1 if supports the CAPABILITY of Register Reset */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define EXT_CLK_SUPPORT_OFFSET                             (14)
#define EXT_CLK_SUPPORT_LEN                                (1)           /* Indicates this FPGA drives a clock that can be used as the RFIC reference clock when ext_clk_sel (bit 30 of :ref:`secref_fmc_ctrl`) is asserted */

/*--- bit field of CAPABILITIES (addr 0x800C) ---*/
#define FPGA_VERSION_PATCH_OFFSET                          (28)
#define FPGA_VERSION_PATCH_LEN                             (4)           /* Reserved for FPGA version pathches */

/*--- bit field of VERSION2 (addr 0x8018) ---*/
#define EXTENDED_BOARD_ID_OFFSET                           (0)
#define EXTENDED_BOARD_ID_LEN                              (8)           /* If reg_board_id[2:0] = 5, then reg_extended_board_id is read to determine board id */

/*--- bit field of READ_RF_BAND_SEL (addr 0x801C) ---*/
#define BAND_1_VALUE_OFFSET                                (0)
#define BAND_1_VALUE_LEN                                   (3)

/*--- bit field of READ_RF_BAND_SEL (addr 0x801C) ---*/
#define BAND_2_VALUE_OFFSET                                (3)
#define BAND_2_VALUE_LEN                                   (3)

/*--- bit field of READ_RF_BAND_SEL (addr 0x801C) ---*/
#define RX_CHAN_1_OVERLOAD_OFFSET                          (6)
#define RX_CHAN_1_OVERLOAD_LEN                             (1)

/*--- bit field of READ_RF_BAND_SEL (addr 0x801C) ---*/
#define RX_CHAN_2_OVERLOAD_OFFSET                          (7)
#define RX_CHAN_2_OVERLOAD_LEN                             (1)

/*--- bit field of READ_RF_BAND_SEL (addr 0x801C) ---*/
#define WAIT_FOR_PPS_OFFSET                                (24)
#define WAIT_FOR_PPS_LEN                                   (1)

/*--- bit field of READ_RF_BAND_SEL (addr 0x801C) ---*/
#define DC_OFFSET_VALID_OFFSET                             (25)
#define DC_OFFSET_VALID_LEN                                (1)           /* M.2 and mPCIe only */

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define BAND_1_CONFIG_OFFSET                               (0)
#define BAND_1_CONFIG_LEN                                  (3)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define BAND_2_CONFIG_OFFSET                               (3)
#define BAND_2_CONFIG_LEN                                  (2)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define BAND_2_TX_CONFIG_OFFSET                            (5)
#define BAND_2_TX_CONFIG_LEN                               (1)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define TX_SELECT_OFFSET                                   (6)
#define TX_SELECT_LEN                                      (3)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define LNA_ENA_OFFSET                                     (9)
#define LNA_ENA_LEN                                        (1)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define TX_ENA_OFFSET                                      (10)
#define TX_ENA_LEN                                         (1)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define LNA_ENA_A2_OFFSET                                  (11)
#define LNA_ENA_A2_LEN                                     (1)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define TX_ENA_A2_OFFSET                                   (12)
#define TX_ENA_A2_LEN                                      (1)

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define FORCE_OVERLOAD_1_OFFSET                            (13)
#define FORCE_OVERLOAD_1_LEN                               (1)           /* SW can force an 'overload' condition by setting this bit.  For a properly configured Sidekiq, this will essentially activate the TX path.  So, for m.2 TDD when we want to switch to Tx, we need to set these bits.  When TDD receive, SW needs to clear them */

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define FORCE_OVERLOAD_2_OFFSET                            (14)
#define FORCE_OVERLOAD_2_LEN                               (1)           /* SW can force an 'overload' condition by setting this.  For a properly configured Sidekiq, this will essentially activate the TX path.  So, for m.2 TDD when we want to switch to Tx, we need to set these bits.  When TDD receive, SW needs to clear them */

/*--- bit field of RF_BAND_SEL (addr 0x8020) ---*/
#define RF_CTRL_W_B31_B15_OFFSET                           (15)
#define RF_CTRL_W_B31_B15_LEN                              (17)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define RX_PACKET_64_WORDS_OFFSET                          (0)
#define RX_PACKET_64_WORDS_LEN                             (1)           /* Available in FPGA v3.9.0 and later, 0: Rx 1024 word packet size, 1: Rx 64 word packet size */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define TX_TRANSMIT_LATE_OFFSET                            (1)
#define TX_TRANSMIT_LATE_LEN                               (1)           /* Check FPGA capabilities (0x800C) for support, 0: Late Tx packet will result in flush of FIFOs in Tx timestamp mode, 1: Late Tx packets will be transmitted in Tx timestamp mode */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define TIMESTAMP_RST_OFFSET                               (2)
#define TIMESTAMP_RST_LEN                                  (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define RESET_TS_ON_PPS_OFFSET                             (3)
#define RESET_TS_ON_PPS_LEN                                (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define SYNC_ON_PPS_OFFSET                                 (4)
#define SYNC_ON_PPS_LEN                                    (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define MANUAL_TRIGGER_OFFSET                              (5)
#define MANUAL_TRIGGER_LEN                                 (1)           /* If set high, starts streaming as if a pps pulse had been received */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define RFIC_RST_OFFSET                                    (6)
#define RFIC_RST_LEN                                       (1)           /* MPCIE rev C only */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define GATE_ON_PPS_START_OFFSET                           (7)
#define GATE_ON_PPS_START_LEN                              (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define GATE_ON_PPS_STOP_OFFSET                            (8)
#define GATE_ON_PPS_STOP_LEN                               (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define PPS_CANCEL_OFFSET                                  (9)
#define PPS_CANCEL_LEN                                     (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define CHIPS_SYNCED_OFFSET                                (10)
#define CHIPS_SYNCED_LEN                                   (1)           /* When sample_clk_a and sample_clk_b are phase aligned, setting this ensure frc_a and frc_b are aligned */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define CHAN_MODE_OFFSET                                   (11)
#define CHAN_MODE_LEN                                      (1)           /* 0: single, 1: dual */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define CONFIG_STREAMING_OFFSET                            (12)
#define CONFIG_STREAMING_LEN                               (1)

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define RECONFIGURE_OFFSET                                 (13)
#define RECONFIGURE_LEN                                    (1)           /* m.2 and X2 only */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define IQ_SWAP_OFFSET                                     (14)
#define IQ_SWAP_LEN                                        (1)           /* When set, swaps the i/q */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define DMA_SINGLE_PACKET_OFFSET                           (15)
#define DMA_SINGLE_PACKET_LEN                              (1)           /* When set to a '1', zynq dma uses a single packet. When set to a '0' (default), zynq dma uses multiple packets */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define FPGA_TDD_MODE_OFFSET                               (16)
#define FPGA_TDD_MODE_LEN                                  (1)           /* When set to a '1', the FPGA controls the Rx and Tx Enable lines */

/*--- bit field of TIMESTAMP_RST (addr 0x8024) ---*/
#define CTRL_W_B31_B17_OFFSET                              (17)
#define CTRL_W_B31_B17_LEN                                 (15)

/*--- bit field of TX_NUM_SAMPLES (addr 0x8028) ---*/
#define TX_NUM_SAMPLES_OFFSET                              (0)
#define TX_NUM_SAMPLES_LEN                                 (16)          /* number of samples per block per channel */

/*--- bit field of RFIC_SPI_CMD (addr 0x8030) ---*/
#define RFIC_SPI_WR_DATA_OFFSET                            (0)
#define RFIC_SPI_WR_DATA_LEN                               (8)

/*--- bit field of RFIC_SPI_CMD (addr 0x8030) ---*/
#define RFIC_SPI_ADDR_OFFSET                               (8)
#define RFIC_SPI_ADDR_LEN                                  (15)

/*--- bit field of RFIC_SPI_CMD (addr 0x8030) ---*/
#define RFIC_SPI_OP_OFFSET                                 (23)
#define RFIC_SPI_OP_LEN                                    (1)           /* 0: read, 1:write */

/*--- bit field of RFIC_SPI_CMD (addr 0x8030) ---*/
#define RFIC_SPI_CS_OFFSET                                 (24)
#define RFIC_SPI_CS_LEN                                    (2)           /* 0: 9371/9379_A, 1: 9528, 2: 9379_B, 3: reserved */

/*--- bit field of RFIC_SPI_CMD (addr 0x8030) ---*/
#define ADI_SPI_CMD_B30_B26_OFFSET                         (26)
#define ADI_SPI_CMD_B30_B26_LEN                            (5)

/*--- bit field of RFIC_SPI_RD_DATA (addr 0x8034) ---*/
#define RFIC_SPI_RD_DATA_OFFSET                            (0)
#define RFIC_SPI_RD_DATA_LEN                               (8)

/*--- bit field of RFIC_SPI_RD_DATA (addr 0x8034) ---*/
#define RFIC_SPI_DONE_OFFSET                               (8)
#define RFIC_SPI_DONE_LEN                                  (1)

/*--- bit field of RFIC_SPI_RD_DATA (addr 0x8034) ---*/
#define ADI_SPI_RD_DATA_B31_B9_OFFSET                      (9)
#define ADI_SPI_RD_DATA_B31_B9_LEN                         (23)

/*--- bit field of I2C_SLAVE_CMD (addr 0x803C) ---*/
#define I2C_SLAVE_ADDR_OFFSET                              (0)
#define I2C_SLAVE_ADDR_LEN                                 (7)

/*--- bit field of I2C_SLAVE_CMD (addr 0x803C) ---*/
#define I2C_CMD_NUM_BYTES_OFFSET                           (7)
#define I2C_CMD_NUM_BYTES_LEN                              (2)

/*--- bit field of I2C_SLAVE_CMD (addr 0x803C) ---*/
#define I2C_SLAVE_RD_WR_OFFSET                             (9)
#define I2C_SLAVE_RD_WR_LEN                                (1)

/*--- bit field of I2C_SLAVE_CMD (addr 0x803C) ---*/
#define I2C_SLAVE_CMD_B30_B10_OFFSET                       (10)
#define I2C_SLAVE_CMD_B30_B10_LEN                          (21)

/*--- bit field of I2C_STATUS (addr 0x8044) ---*/
#define I2C_SLAVE_NACK_OFFSET                              (0)
#define I2C_SLAVE_NACK_LEN                                 (1)

/*--- bit field of I2C_STATUS (addr 0x8044) ---*/
#define I2C_DONE_OFFSET                                    (1)
#define I2C_DONE_LEN                                       (1)

/*--- bit field of I2C_STATUS (addr 0x8044) ---*/
#define I2C_STATUS_OFFSET                                  (2)
#define I2C_STATUS_LEN                                     (30)

/*--- bit field of REGISTER_RESET (addr 0x8048) ---*/
#define REGISTER_RESET_OFFSET                              (0)
#define REGISTER_RESET_LEN                                 (1)           /* When this bit is is transitioned from a '1' to a '0', all of the writeable PDK registers will be reset to their default states */

/*--- bit field of PLL_SPI_WR (addr 0x804C) ---*/
#define PLL_SPI_WR_DATA_OFFSET                             (0)
#define PLL_SPI_WR_DATA_LEN                                (16)          /* RFFC SPI write (X2 only) */

/*--- bit field of PLL_SPI_WR (addr 0x804C) ---*/
#define PLL_SPI_ADDR_OFFSET                                (16)
#define PLL_SPI_ADDR_LEN                                   (6)           /* AD9528 max addr is 0x509, which is 11 bits */

/*--- bit field of PLL_SPI_WR (addr 0x804C) ---*/
#define PLL_SPI_WR_B22_OFFSET                              (22)
#define PLL_SPI_WR_B22_LEN                                 (1)

/*--- bit field of PLL_SPI_WR (addr 0x804C) ---*/
#define PLL_SPI_OP_OFFSET                                  (23)
#define PLL_SPI_OP_LEN                                     (1)           /* 0: read, 1:write */

/*--- bit field of PLL_SPI_WR (addr 0x804C) ---*/
#define PLL_SPI_CS_OFFSET                                  (24)
#define PLL_SPI_CS_LEN                                     (1)           /* 0 selects PLL1, 1 selects PLL2 */

/*--- bit field of PLL_SPI_WR (addr 0x804C) ---*/
#define PLL_SPI_WR_B31_B25_OFFSET                          (25)
#define PLL_SPI_WR_B31_B25_LEN                             (7)

/*--- bit field of PLL_SPI_RD (addr 0x8050) ---*/
#define PLL_SPI_RD_DATA_OFFSET                             (0)
#define PLL_SPI_RD_DATA_LEN                                (16)

/*--- bit field of PLL_SPI_RD (addr 0x8050) ---*/
#define PLL_SPI_DONE_OFFSET                                (16)
#define PLL_SPI_DONE_LEN                                   (1)

/*--- bit field of PLL_SPI_RD (addr 0x8050) ---*/
#define PLL_SPI_RD_B31_B17_OFFSET                          (17)
#define PLL_SPI_RD_B31_B17_LEN                             (15)

/*--- bit field of GPS_STATUS (addr 0x8054) ---*/
#define GPS_RX_FIFO_RD_DATA_COUNT_OFFSET                   (0)
#define GPS_RX_FIFO_RD_DATA_COUNT_LEN                      (12)          /* Number of 64 bit words currently in the GPS Rx FIFO */

/*--- bit field of GPS_STATUS (addr 0x8054) ---*/
#define GPS_TX_FIFO_WR_DATA_COUNT_OFFSET                   (12)
#define GPS_TX_FIFO_WR_DATA_COUNT_LEN                      (12)          /* Number of 64 bit words currently in the GPS Tx FIFO */

/*--- bit field of GPS_STATUS (addr 0x8054) ---*/
#define GPS_FIX_OFFSET                                     (24)
#define GPS_FIX_LEN                                        (1)           /* Status of the gps fix witch can either be from a gps module or from the nmea message parser depending on the platform */

/*--- bit field of GPS_STATUS (addr 0x8054) ---*/
#define GPS_STATUS_B31_B25_OFFSET                          (25)
#define GPS_STATUS_B31_B25_LEN                             (7)

/*--- bit field of UART_WRITE_DATA (addr 0x8060) ---*/
#define UART_WRITE_OFFSET                                  (0)
#define UART_WRITE_LEN                                     (8)

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_POWER_EN_OFFSET                                (0)
#define GPS_POWER_EN_LEN                                   (1)           /* Enables power to GPS module, 0: gps power enabled, 1: gps power disabled */

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_RESET_N_OFFSET                                 (1)
#define GPS_RESET_N_LEN                                    (1)           /* Reset to GPS module, 0: gps out of reset   1: gps in reset */

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_FORCE_ON_OFFSET                                (2)
#define GPS_FORCE_ON_LEN                                   (1)           /* Toggle to wake GPS from deep sleep state */

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_ANT_BIAS_EN_OFFSET                             (3)
#define GPS_ANT_BIAS_EN_LEN                                (1)           /* Enable 3.3V bias to GPS antenna */

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_UART_LOOPBACK_OFFSET                           (4)
#define GPS_UART_LOOPBACK_LEN                              (1)           /* Loopback the uart tx to rx internally in the FPGA */

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_CTRL_B15_B5_OFFSET                             (5)
#define GPS_CTRL_B15_B5_LEN                                (11)

/*--- bit field of GPS_CTRL (addr 0x8064) ---*/
#define GPS_CONTROL_MASK_OFFSET                            (16)
#define GPS_CONTROL_MASK_LEN                               (16)          /* If the upper bit is set, it indicates that the lower 5 bits are used to determine if the corresponding lower 5 control bits of GPS_CTRL are available for this platform, where 1:available and 0:not available */

/*--- bit field of ADC_DAC_PRBS_TEST_CTRL (addr 0x8078) ---*/
#define TX_DATA_SEL_OFFSET                                 (0)
#define TX_DATA_SEL_LEN                                    (4)           /* 4'b1000: tx_tone, 4'b0111: {2{tx_prbs7}}, 4'b0110: {2{tx_prbs15}}, 4'b0101: {2{tx_ramp}}, 4'b0100: {8{tx_ramp[3:0]}}, 4'b0011: tx_pattern */

/*--- bit field of ADC_DAC_PRBS_TEST_CTRL (addr 0x8078) ---*/
#define SW_TX_INIT_OFFSET                                  (4)
#define SW_TX_INIT_LEN                                     (1)           /* Writing a 1 to this bit forces a tx lssi re-initialization */

/*--- bit field of ADC_DAC_PRBS_TEST_CTRL (addr 0x8078) ---*/
#define RX_DATA_SEL_OFFSET                                 (8)
#define RX_DATA_SEL_LEN                                    (3)           /* 3'b111: {2{rx_prbs7_nz}}, 3'b110: {2{rx_prbs15_nz}}, 3'b101: {2{rx_ramp}}, 3'b100: {8{rx_ramp[3:0]}}, 3'b011: rx_pattern */

/*--- bit field of ADC_DAC_PRBS_TEST_CTRL (addr 0x8078) ---*/
#define SW_RX_INIT_OFFSET                                  (12)
#define SW_RX_INIT_LEN                                     (1)           /* Writing a 1 to this bit forces a rx lssi re-initialization */

/*--- bit field of ADC_DAC_PRBS_TEST_CTRL (addr 0x8078) ---*/
#define RX_TX_TEST_PATTERN_OFFSET                          (16)
#define RX_TX_TEST_PATTERN_LEN                             (16)

/*--- bit field of I2C_1_SLAVE_CMD (addr 0x8088) ---*/
#define I2C_1_SLAVE_ADDR_OFFSET                            (0)
#define I2C_1_SLAVE_ADDR_LEN                               (7)

/*--- bit field of I2C_1_SLAVE_CMD (addr 0x8088) ---*/
#define I2C_1_CMD_NUM_BYTES_OFFSET                         (7)
#define I2C_1_CMD_NUM_BYTES_LEN                            (2)

/*--- bit field of I2C_1_SLAVE_CMD (addr 0x8088) ---*/
#define I2C_1_SLAVE_RD_WR_OFFSET                           (9)
#define I2C_1_SLAVE_RD_WR_LEN                              (1)

/*--- bit field of I2C_1_SLAVE_CMD (addr 0x8088) ---*/
#define I2C_1_SLAVE_CMD_B30_B10_OFFSET                     (10)
#define I2C_1_SLAVE_CMD_B30_B10_LEN                        (21)

/*--- bit field of I2C_1_STATUS (addr 0x8090) ---*/
#define I2C_1_SLAVE_NACK_OFFSET                            (0)
#define I2C_1_SLAVE_NACK_LEN                               (1)

/*--- bit field of I2C_1_STATUS (addr 0x8090) ---*/
#define I2C_1_DONE_OFFSET                                  (1)
#define I2C_1_DONE_LEN                                     (1)

/*--- bit field of I2C_1_STATUS (addr 0x8090) ---*/
#define I2C_1_STATUS_B31_B2_OFFSET                         (2)
#define I2C_1_STATUS_B31_B2_LEN                            (30)

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_EN_A_OFFSET                          (0)
#define GPIO_FREQ_HOP_EN_A_LEN                             (1)           /* Allow frequency hoppiong related gpio to be driven by timestamp comparison logic for RFIC_A */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_DATA_A_OFFSET                        (1)
#define GPIO_FREQ_HOP_DATA_A_LEN                           (4)           /* Value to drive up to four gpio pins during timestamp based frequency hopping for RFIC_A */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_GO_A_OFFSET                          (7)
#define GPIO_FREQ_HOP_GO_A_LEN                             (1)           /* A rising edge detected on this bit forces the gpio hopping logic to switch at the next programmed timestamp for RFIC_A */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_EN_B_OFFSET                          (8)
#define GPIO_FREQ_HOP_EN_B_LEN                             (1)           /* Allow frequency hoppiong related gpio to be driven by timestamp comparison logic for RFIC_B */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_DATA_B_OFFSET                        (9)
#define GPIO_FREQ_HOP_DATA_B_LEN                           (4)           /* Value to drive up to four gpio pins during timestamp based frequency hopping for RFIC_B */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_GO_B_OFFSET                          (15)
#define GPIO_FREQ_HOP_GO_B_LEN                             (1)           /* A rising edge detected on this bit forces the gpio hopping logic to switch at the next programmed timestamp for RFIC_B */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_FREQ_INDEX_A_OFFSET                  (16)
#define GPIO_FREQ_HOP_FREQ_INDEX_A_LEN                     (8)           /* Frequency Index for RFIC_A */

/*--- bit field of GPIO_FREQ_HOP_CTRL (addr 0x80A0) ---*/
#define GPIO_FREQ_HOP_FREQ_INDEX_B_OFFSET                  (24)
#define GPIO_FREQ_HOP_FREQ_INDEX_B_LEN                     (8)           /* Frequency Index for RFIC_B */

/*--- bit field of BASELINE_VCS_STATUS (addr 0x80B8) ---*/
#define BASE_SHA_OFFSET                                    (0)
#define BASE_SHA_LEN                                       (28)          /* This field contains the first 7 nibbles of the commit SHA */

/*--- bit field of BASELINE_VCS_STATUS (addr 0x80B8) ---*/
#define BASE_REPO_STATE_OFFSET                             (28)
#define BASE_REPO_STATE_LEN                                (4)           /* This field provides status of the state of the reposotiry at the time of the build. 0x1 if clone was dirty, 0x0 if clone was clean */

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_CTRL_RX1_B0_OFFSET                             (0)
#define DMA_CTRL_RX1_B0_LEN                                (1)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_RX_DATA_SRC_OFFSET                             (1)
#define DMA_RX_DATA_SRC_LEN                                (1)           /* counter or real data */

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_RX_FIFO_CTRL_OFFSET                            (2)
#define DMA_RX_FIFO_CTRL_LEN                               (1)           /* enable FIFO and ADC? */

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_CTRL_RX1_B3_OFFSET                             (3)
#define DMA_CTRL_RX1_B3_LEN                                (1)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_DEST_SELECT_OFFSET                             (4)
#define DMA_DEST_SELECT_LEN                                (1)           /* 0: pcie, 1: SSD */

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_CTRL_RX1_B6_B5_OFFSET                          (5)
#define DMA_CTRL_RX1_B6_B5_LEN                             (2)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_TX_TEST_OFFSET                                 (7)
#define DMA_TX_TEST_LEN                                    (1)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_TX_MODE_OFFSET                                 (8)
#define DMA_TX_MODE_LEN                                    (1)           /* 1: continuous, 0: timestamp */

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_TX_FIFO_CTRL_OFFSET                            (9)
#define DMA_TX_FIFO_CTRL_LEN                               (1)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_PACKED_MODE_OFFSET                             (10)
#define DMA_PACKED_MODE_LEN                                (1)           /* 0: unpacked, 1: packed */

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DC_OFFSET_CORR_ENA_OFFSET                          (11)
#define DC_OFFSET_CORR_ENA_LEN                             (1)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define WAIT_FOR_TRIGGER_OFFSET                            (12)
#define WAIT_FOR_TRIGGER_LEN                               (1)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_CTRL_RX1_B15_B13_OFFSET                        (13)
#define DMA_CTRL_RX1_B15_B13_LEN                           (3)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_NUM_BLOCKS_OFFSET                              (16)
#define DMA_NUM_BLOCKS_LEN                                 (8)

/*--- bit field of DMA_CTRL_RX1 (addr 0x8100) ---*/
#define DMA_CTRL_RX1_B31_B24_OFFSET                        (24)
#define DMA_CTRL_RX1_B31_B24_LEN                           (8)

/*--- bit field of IQ_MULTIPLIER_RX1 (addr 0x8104) ---*/
#define IM_CMPLX_MULT_FACTOR_OFFSET                        (0)
#define IM_CMPLX_MULT_FACTOR_LEN                           (16)          /* imaginary part of complex multiplier factor, fixed point in 2's complement */

/*--- bit field of IQ_MULTIPLIER_RX1 (addr 0x8104) ---*/
#define RE_CMPLX_MULT_FACTOR_OFFSET                        (16)
#define RE_CMPLX_MULT_FACTOR_LEN                           (16)          /* real portion of complex multiplier factor, fixed point in 2's complement */

/*--- bit field of DECIMATOR_PROG_COEFFS_RX1 (addr 0x8130) ---*/
#define DECIMATOR_COEFF_OFFSET                             (0)
#define DECIMATOR_COEFF_LEN                                (16)          /* signed 16-bit coefficient */

/*--- bit field of DECIMATOR_PROG_COEFFS_RX1 (addr 0x8130) ---*/
#define DECIMATOR_STAGE_OFFSET                             (16)
#define DECIMATOR_STAGE_LEN                                (4)           /* stage into which COEFFICIENT is written */

/*--- bit field of DECIMATOR_PROG_COEFFS_RX1 (addr 0x8130) ---*/
#define DECIMATOR_WRITE_OFFSET                             (20)
#define DECIMATOR_WRITE_LEN                                (1)           /* 0->1 transition writes COEFFICIENT into STAGE */

/*--- bit field of DECIMATOR_CTRL_RX1 (addr 0x8134) ---*/
#define DECIMATOR_RESET_OFFSET                             (0)
#define DECIMATOR_RESET_LEN                                (1)           /* completely reset the decimator state, except the coefficients */

/*--- bit field of DECIMATOR_CTRL_RX1 (addr 0x8134) ---*/
#define DECIMATOR_ENABLE_OFFSET                            (1)
#define DECIMATOR_ENABLE_LEN                               (1)           /* enable the decimator */

/*--- bit field of DECIMATOR_CTRL_RX1 (addr 0x8134) ---*/
#define DECIMATOR_RATE_OFFSET                              (2)
#define DECIMATOR_RATE_LEN                                 (4)           /* decimation rate, decimate by 2**DECIMATION_RATE */

/*--- bit field of DECIMATOR_CTRL_RX1 (addr 0x8134) ---*/
#define DECIMATOR_ZERO_PADDED_OUTPUT_OFFSET                (6)
#define DECIMATOR_ZERO_PADDED_OUTPUT_LEN                   (1)           /* decimation rate, decimate by 2**DECIMATION_RATE */

/*--- bit field of DECIMATOR_BUILD_INFO_RX1 (addr 0x8138) ---*/
#define DECIMATOR_STAGES_OFFSET                            (0)
#define DECIMATOR_STAGES_LEN                               (4)           /* number of decimation stages in the FPGA */

/*--- bit field of DECIMATOR_BUILD_INFO_RX1 (addr 0x8138) ---*/
#define DECIMATOR_EVEN_TAPS_HALF_LENGTH_OFFSET             (4)
#define DECIMATOR_EVEN_TAPS_HALF_LENGTH_LEN                (8)           /* half length of the even taps of each stage in the FPGA */

/*--- bit field of DECIMATOR_BUILD_INFO_RX1 (addr 0x8138) ---*/
#define DECIMATOR_A1_CONTROLS_A2_OFFSET                    (12)
#define DECIMATOR_A1_CONTROLS_A2_LEN                       (1)           /* when returns a 1, a1 decimation registers are also used for a2 */

/*--- bit field of DECIMATOR_BUILD_INFO_RX1 (addr 0x8138) ---*/
#define DECIMATOR_HALF_MAX_RATE_OFFSET                     (13)
#define DECIMATOR_HALF_MAX_RATE_LEN                        (1)           /* when returns a 1, decimator can only be run at one half of the max sample rate */

/*--- bit field of DECIMATOR_PROG_COEFFS_RX3 (addr 0x8330) ---*/
#define REG_DECIMATOR_COEFF_B1_OFFSET                      (0)
#define REG_DECIMATOR_COEFF_B1_LEN                         (16)          /* signed 16-bit coefficient */

/*--- bit field of DECIMATOR_PROG_COEFFS_RX3 (addr 0x8330) ---*/
#define REG_DECIMATOR_STAGE_B1_OFFSET                      (16)
#define REG_DECIMATOR_STAGE_B1_LEN                         (4)           /* stage into which COEFFICIENT is written */

/*--- bit field of DECIMATOR_PROG_COEFFS_RX3 (addr 0x8330) ---*/
#define REG_DECIMATOR_WRITE_B1_OFFSET                      (20)
#define REG_DECIMATOR_WRITE_B1_LEN                         (1)           /* 0->1 transition writes COEFFICIENT into STAGE */

/*--- bit field of DECIMATOR_CTRL_RX3 (addr 0x8334) ---*/
#define REG_DECIMATOR_RESET_B1_OFFSET                      (0)
#define REG_DECIMATOR_RESET_B1_LEN                         (1)           /* completely reset the decimator state, except the coefficients */

/*--- bit field of DECIMATOR_CTRL_RX3 (addr 0x8334) ---*/
#define REG_DECIMATOR_ENABLE_B1_OFFSET                     (1)
#define REG_DECIMATOR_ENABLE_B1_LEN                        (1)           /* enable the decimator */

/*--- bit field of DECIMATOR_CTRL_RX3 (addr 0x8334) ---*/
#define REG_DECIMATOR_RATE_B1_OFFSET                       (2)
#define REG_DECIMATOR_RATE_B1_LEN                          (4)           /* decimation rate, decimate by 2**DECIMATION_RATE */

/*--- bit field of DECIMATOR_CTRL_RX3 (addr 0x8334) ---*/
#define REG_DECIMATOR_ZERO_PADDED_OUTPUT_B1_OFFSET         (6)
#define REG_DECIMATOR_ZERO_PADDED_OUTPUT_B1_LEN            (1)           /* decimation rate, decimate by 2**DECIMATION_RATE */

/*--- bit field of DECIMATOR_BUILD_INFO_RX3 (addr 0x8338) ---*/
#define REG_DECIMATOR_STAGES_INSTANTIATED_B1_OFFSET        (0)
#define REG_DECIMATOR_STAGES_INSTANTIATED_B1_LEN           (4)           /* number of decimation stages in the FPGA */

/*--- bit field of DECIMATOR_BUILD_INFO_RX3 (addr 0x8338) ---*/
#define REG_DECIMATOR_EVEN_TAPS_HALF_LENGTH_INSTANTIATED_B1_OFFSET (4)
#define REG_DECIMATOR_EVEN_TAPS_HALF_LENGTH_INSTANTIATED_B1_LEN (8)           /* half length of the even taps of each stage in the FPGA */

/*--- bit field of DECIMATOR_BUILD_INFO_RX3 (addr 0x8338) ---*/
#define REG_DECIMATOR_B1_CONTROLS_B2_OFFSET                (12)
#define REG_DECIMATOR_B1_CONTROLS_B2_LEN                   (1)           /* when returns a 1, a1 decimation registers are also used for a2 */

/*--- bit field of DECIMATOR_BUILD_INFO_RX3 (addr 0x8338) ---*/
#define REG_DECIMATOR_HALF_MAX_RATE_B1_OFFSET              (13)
#define REG_DECIMATOR_HALF_MAX_RATE_B1_LEN                 (1)           /* when returns a 1, decimator can only be run at one half of the max sample rate */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define HAS_SPI_CMD_EXEC_OFFSET                            (0)
#define HAS_SPI_CMD_EXEC_LEN                               (1)           /* Indicates if this implementation includes the AD936x SPI Command executor function.  * 0 = Not implemented * 1 = Implemented */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define HAS_FAST_LOCK_MGR_OFFSET                           (1)
#define HAS_FAST_LOCK_MGR_LEN                              (1)           /* Indicates if this implementation includes the AD936x Fast Lock profile manager function.  * 0 = Not implemented * 1 = Implemented */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define HAS_TX_ATTEN_OFFSET                                (2)
#define HAS_TX_ATTEN_LEN                                   (1)           /* Indicates if this implementation includes the Tx Attenuation writer function. Software does not directly interface with this module. However, SW may wish to know this feature is enabled in order to adjust any SPI interface timeout values, or prevent setting Tx Attenuation values itself for example.  * 0 = Not implemented * 1 = Implemented */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define HAS_USER_APP_DIRECT_SPI_OFFSET                     (3)
#define HAS_USER_APP_DIRECT_SPI_LEN                        (1)           /* Indicates if this implementation includes the direct SPI interface to the user application. Software does not directly interface with this module. However, SW may wish to know this feature is enabled in order to adjust any SPI interface timeout values for example.  * 0 = Not implemented * 1 = Implemented */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define NUM_FAST_LOCK_PROFILES_LOG2_OFFSET                 (4)
#define NUM_FAST_LOCK_PROFILES_LOG2_LEN                    (4)           /* Log base 2 of the number of profiles that can be stored in the extended Fast Lock profile table. For example, if the table can store 128 profiles, this field would read 0x7. */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define SPICE_INSTR_MEM_DEPTH_LOG2_OFFSET                  (8)
#define SPICE_INSTR_MEM_DEPTH_LOG2_LEN                     (4)           /* Log base 2 of the memory depth of the SPI Command Executor instruction memory space. */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define SPICE_DATA_MEM_DEPTH_LOG2_OFFSET                   (12)
#define SPICE_DATA_MEM_DEPTH_LOG2_LEN                      (4)           /* Log base 2 of the memory depth of the SPI Command Executor data memory space. */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define SPI_SW_LOCK_REQ_OFFSET                             (16)
#define SPI_SW_LOCK_REQ_LEN                                (1)           /* SPI Lockout Request. When set to 1, any currently executing SPI command will be completed. Once SPI_SW_LOCKED is asserted the lock has completed. New SPI commands issued by interfaces other than the SW direct interface will not be processesed. */

/*--- bit field of SPI Build Configuration Register (addr 0x8340) ---*/
#define SPI_SW_LOCKED_OFFSET                               (17)
#define SPI_SW_LOCKED_LEN                                  (1)           /* SPI Locked by Software. This bit gets asserted after SPI_SW_LOCK_REQ has been asserted and the SPI interface has been sucessfully locked for software access only. */

/*--- bit field of Fast Lock Profile RAM Address Register (addr 0x8344) ---*/
#define FASTLOCK_RAM_ADDR_SET_OFFSET                       (0)
#define FASTLOCK_RAM_ADDR_SET_LEN                          (16)          /* Set the Fast Lock RAM address pointer. Write this register to set the address pointer of the Fast Lock RAM. Writes to the data register will automatically increment the address pointer. */

/*--- bit field of Fast Lock Profile RAM Address Register (addr 0x8344) ---*/
#define FASTLOCK_RAM_ADDR_RDBK_OFFSET                      (16)
#define FASTLOCK_RAM_ADDR_RDBK_LEN                         (16)          /* Fast Lock RAM Address pointer Readback. This value represents the current state of the Fast Lock RAM address pointer */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_TX_EXTENDED_PROF_ID_OFFSET                (0)
#define FASTLOCK_TX_EXTENDED_PROF_ID_LEN                   (10)          /* The Extended Fast Lock Profile ID. This value specifies which Fast Lock profile stored in the profile table to write to the AD936x Tx profile specified by FASTLOCK_RFIC_TX_PROF_ID. */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_RFIC_TX_PROF_ID_OFFSET                    (10)
#define FASTLOCK_RFIC_TX_PROF_ID_LEN                       (3)           /* The Fast Lock RFIC Tx Profile ID. This is the transmit fast lock profile of the RFIC to configure with the profile data specified by FASTLOCK_EXTENDED_TX_PROF_ID */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_TX_PROG_EN_OFFSET                         (13)
#define FASTLOCK_TX_PROG_EN_LEN                            (1)           /* * 0 = Do not assign the Tx profile. * 1 = Assign the Tx Fast Lock profile. */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_TX_RETUNE_OFFSET                          (14)
#define FASTLOCK_TX_RETUNE_LEN                             (1)           /* Fast Lock Transmit Re-tune. If this bit is set the FPGA sends the command to the RFIC to switch to the Tx Fast Lock profile indicated by FASTLOCK_TX_PROF_ID. If FASTLOCK_TX_PROG_EN is high, the command to switch to the profile is issued after the profile has been configured. */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_RX_EXTENDED_PROF_ID_OFFSET                (16)
#define FASTLOCK_RX_EXTENDED_PROF_ID_LEN                   (10)          /* The Extended Fast Lock Profile ID. This value specifies which Fast Lock profile stored in the profile table to write to the AD936x Rx profile specified by FASTLOCK_RFIC_RX_PROF_ID. */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_RFIC_RX_PROF_ID_OFFSET                    (26)
#define FASTLOCK_RFIC_RX_PROF_ID_LEN                       (3)           /* The Fast Lock RFIC Rx Profile ID. This is the receive fast lock profile of the RFIC to configure with the profile data specified by FASTLOCK_EXTENDED_RX_PROF_ID */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_RX_PROG_EN_OFFSET                         (29)
#define FASTLOCK_RX_PROG_EN_LEN                            (1)           /* * 0 = Do not assign the Rx profile. * 1 = Assign the Rx Fast lock profile. */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_RX_RETUNE_OFFSET                          (30)
#define FASTLOCK_RX_RETUNE_LEN                             (1)           /* Fast Lock Receive Re-tune. If this bit is set the FPGA sends the command to the RFIC to switch to the Tx Fast Lock profile indicated by FASTLOCK_RX_PROF_ID. If FASTLOCK_RX_PROG_EN is high, the command to switch to the profile is issued after the profile has been configured. */

/*--- bit field of Fast Lock Profile Assignment Register (addr 0x8350) ---*/
#define FASTLOCK_BUSY_OFFSET                               (31)
#define FASTLOCK_BUSY_LEN                                  (1)           /* Fast Lock Busy indicator. This bit is asserted while the Fast Lock data is being written to the RFIC. Any writes to this register while this bit is asserted are ignored. Once programming of the RFIC is completed, this bit is cleared. */

/*--- bit field of SPI Command Executor Control Register (addr 0x8354) ---*/
#define SPICE_CURRENT_INSTRUCTION_OFFSET                   (0)
#define SPICE_CURRENT_INSTRUCTION_LEN                      (16)          /* This field provides the current instruction being executed, represented by the address of the instruction memory RAM. */

/*--- bit field of SPI Command Executor Control Register (addr 0x8354) ---*/
#define SPICE_ABORT_OFFSET                                 (28)
#define SPICE_ABORT_LEN                                    (1)           /* Abort the current executing program. Setting and clearing this bit will stop a program currently being executed. Once the program is terminated the SPICE_BUSY flag will be cleared. */

/*--- bit field of SPI Command Executor Control Register (addr 0x8354) ---*/
#define SPICE_TIMEOUT_OFFSET                               (29)
#define SPICE_TIMEOUT_LEN                                  (1)           /* The timeout flag is asserted when the number of polling intervals specified by POLL_TIMEOUT_COUNT is exceeded. If a timeout occurs program execution will stop, this flag will be asserted and the SPICE_BUSY flag will be cleared. This flag is cleared when a new program is launched. */

/*--- bit field of SPI Command Executor Control Register (addr 0x8354) ---*/
#define SPICE_BUSY_OFFSET                                  (31)
#define SPICE_BUSY_LEN                                     (1)           /* SPI Command Executor Busy Flag.  * 0 = SPICE Module is not currently executing a program * 1 = SPICE Module is currently executing a program */

/*--- bit field of SPI Command Executor Polling Configuration Register (addr 0x8358) ---*/
#define SPICE_POLL_INTERVAL_OFFSET                         (0)
#define SPICE_POLL_INTERVAL_LEN                            (8)           /* Sets the polling interval of poll operations in milliseconds. The polling interval is (POLL_INTERVAL + 1) ms */

/*--- bit field of SPI Command Executor Polling Configuration Register (addr 0x8358) ---*/
#define SPICE_POLL_TIMEOUT_COUNT_OFFSET                    (8)
#define SPICE_POLL_TIMEOUT_COUNT_LEN                       (8)           /* Sets the number of poll intervals to attempt before timing out an halting program execution. */

/*--- bit field of SPI Command Executor Launch Register (addr 0x835C) ---*/
#define SPICE_INSTR_START_ADDR_OFFSET                      (0)
#define SPICE_INSTR_START_ADDR_LEN                         (16)          /* This field indicates the programs starting address in the instruction memory space. The instruction at this address location will be the first one executed by the SPICE. Instructions are read sequentially until an EOP instruction is encountered. */

/*--- bit field of SPI Command Executor Launch Register (addr 0x835C) ---*/
#define SPICE_DATA_START_ADDR_OFFSET                       (16)
#define SPICE_DATA_START_ADDR_LEN                          (16)          /* This field indicates starting address in the data memory space. The data at this address is paired with the instruction at INSTR_START_ADDR. Data is read sequentially from this point during program execution. */

/*--- bit field of SPI Command Executor Instruction Memory Space Address Register (addr 0x8360) ---*/
#define SPICE_INSTR_ADDR_SET_OFFSET                        (0)
#define SPICE_INSTR_ADDR_SET_LEN                           (16)          /* Set the SPICE Instruction RAM address pointer. Write this register to set the address pointer of the SPICE instruction RAM. Writes to the data register will automatically increment the address pointer. */

/*--- bit field of SPI Command Executor Instruction Memory Space Address Register (addr 0x8360) ---*/
#define SPICE_INSTR_ADDR_RDBK_OFFSET                       (16)
#define SPICE_INSTR_ADDR_RDBK_LEN                          (16)          /* SPICE Instruction RAM Address pointer Readback. This value represents the current state of the SPICE Instruction RAM address pointer */

/*--- bit field of SPI Command Executor Instruction Memory Space Data Register (addr 0x8364) ---*/
#define SPICE_INSTR_OP_CODE_OFFSET                         (0)
#define SPICE_INSTR_OP_CODE_LEN                            (4)           /* Instruction operation code. Defines the type of SPI operation to perform.   0x0      - Poll. Read the specified SPI register at an interval specified by     POLL_INTERVAL until the masked read value matches the comparison value     programmed in the corresponding data memory space location.   0x1      - Write. Write the data in the data memory space location to the specified     SPI register address.   0x2      - Read-Modify-Write.  Read the specified SPI register, mask the read value and     OR it with the value in the corresponding data memory space location.   0x3-0xE  - N/A   0xF      - End of Program. When this instruction is encountered no SPI transaction     occurs. Instead the SPICE terminates program execution and clears the SPICE_BUSY     flag. */

/*--- bit field of SPI Command Executor Instruction Memory Space Data Register (addr 0x8364) ---*/
#define SPICE_INSTR_MASK_OFFSET                            (8)
#define SPICE_INSTR_MASK_LEN                               (8)           /* Mask applied to the SPI readback value prior to comparison during a POLL operation, or prior to ORing with DATA during a RMW operation */

/*--- bit field of SPI Command Executor Instruction Memory Space Data Register (addr 0x8364) ---*/
#define SPICE_INSTR_SPI_ADDR_OFFSET                        (16)
#define SPICE_INSTR_SPI_ADDR_LEN                           (15)          /* This is the SPI address on which to perform the operation defined by SPICE_INSTR_OP_CODE. */

/*--- bit field of SPI Command Executor Instruction Memory Space Data Readback Register (addr 0x8368) ---*/
#define SPICE_INSTR_OP_CODE_RDBK_OFFSET                    (0)
#define SPICE_INSTR_OP_CODE_RDBK_LEN                       (4)           /* See the description of SPICE_INSTR_OP_CODE in the SPI Command Executor Instruction Memory Space Data Register. */

/*--- bit field of SPI Command Executor Instruction Memory Space Data Readback Register (addr 0x8368) ---*/
#define SPICE_INSTR_MASK_RDBK_OFFSET                       (8)
#define SPICE_INSTR_MASK_RDBK_LEN                          (8)           /* See the description of SPICE_INSTR_MASK in the SPI Command Executor Instruction Memory Space Data Register. */

/*--- bit field of SPI Command Executor Instruction Memory Space Data Readback Register (addr 0x8368) ---*/
#define SPICE_INSTR_SPI_ADDR_RDBK_OFFSET                   (16)
#define SPICE_INSTR_SPI_ADDR_RDBK_LEN                      (15)          /* See the description of SPICE_INSTR_SPI_ADDR in the SPI Command Executor Instruction Memory Space Data Register. */

/*--- bit field of SPI Command Executor Data Memory Space Address Register (addr 0x836C) ---*/
#define SPICE_DATA_ADDR_SET_OFFSET                         (0)
#define SPICE_DATA_ADDR_SET_LEN                            (16)          /* Set the SPICE Data RAM address pointer. Write this register to set the address pointer of the SPICE data RAM. Writes to the data register will automatically increment the address pointer. This address should be evenly divisible by 4, since 4 data bytes are written to the RAM at a time. The lower 2 bits are truncated in hardware. */

/*--- bit field of SPI Command Executor Data Memory Space Address Register (addr 0x836C) ---*/
#define SPICE_DATA_ADDR_RDBK_OFFSET                        (16)
#define SPICE_DATA_ADDR_RDBK_LEN                           (16)          /* SPICE Data RAM Address pointer Readback. This value represents the current state of the SPICE Data RAM address pointer */

/*--- bit field of SPI Command Executor Data Memory Space Data Register (addr 0x8370) ---*/
#define SPICE_WR_DATA_0_OFFSET                             (0)
#define SPICE_WR_DATA_0_LEN                                (8)           /* Write data for instruction SPICE_DATA_ADDR_RDBK + 0 */

/*--- bit field of SPI Command Executor Data Memory Space Data Register (addr 0x8370) ---*/
#define SPICE_WR_DATA_1_OFFSET                             (8)
#define SPICE_WR_DATA_1_LEN                                (8)           /* Write data for instruction SPICE_DATA_ADDR_RDBK + 1 */

/*--- bit field of SPI Command Executor Data Memory Space Data Register (addr 0x8370) ---*/
#define SPICE_WR_DATA_2_OFFSET                             (16)
#define SPICE_WR_DATA_2_LEN                                (8)           /* Write data for instruction SPICE_DATA_ADDR_RDBK + 2 */

/*--- bit field of SPI Command Executor Data Memory Space Data Register (addr 0x8370) ---*/
#define SPICE_WR_DATA_3_OFFSET                             (24)
#define SPICE_WR_DATA_3_LEN                                (8)           /* Write data for instruction SPICE_DATA_ADDR_RDBK + 3 */

/*--- bit field of SPI Command Executor Data Memory Space Data Readback Register (addr 0x8374) ---*/
#define SPICE_WR_DATA_0_RDBK_OFFSET                        (0)
#define SPICE_WR_DATA_0_RDBK_LEN                           (8)           /* Read data for instruction SPICE_DATA_ADDR_RDBK + 0 */

/*--- bit field of SPI Command Executor Data Memory Space Data Readback Register (addr 0x8374) ---*/
#define SPICE_WR_DATA_1_RDBK_OFFSET                        (8)
#define SPICE_WR_DATA_1_RDBK_LEN                           (8)           /* Read data for instruction SPICE_DATA_ADDR_RDBK + 1 */

/*--- bit field of SPI Command Executor Data Memory Space Data Readback Register (addr 0x8374) ---*/
#define SPICE_WR_DATA_2_RDBK_OFFSET                        (16)
#define SPICE_WR_DATA_2_RDBK_LEN                           (8)           /* Read data for instruction SPICE_DATA_ADDR_RDBK + 2 */

/*--- bit field of SPI Command Executor Data Memory Space Data Readback Register (addr 0x8374) ---*/
#define SPICE_WR_DATA_3_RDBK_OFFSET                        (24)
#define SPICE_WR_DATA_3_RDBK_LEN                           (8)           /* Read data for instruction SPICE_DATA_ADDR_RDBK + 3 */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define FLASH_MOSI_OFFSET                                  (0)
#define FLASH_MOSI_LEN                                     (1)           /* M2/M2S.I/O. Flash MOSI; 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define FLASH_MISO_OFFSET                                  (1)
#define FLASH_MISO_LEN                                     (1)           /* M2/M2S.I/O. Flash MISO; 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define FLASH_DQ2_OFFSET                                   (2)
#define FLASH_DQ2_LEN                                      (1)           /* M2/M2S.I/O. Flash DQ2; 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define FLASH_DQ3_OFFSET                                   (3)
#define FLASH_DQ3_LEN                                      (1)           /* M2/M2S.I/O. Flash DQ3; 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define GPIF_TRISTATE_B7_B4_OFFSET                         (4)
#define GPIF_TRISTATE_B7_B4_LEN                            (4)           /* 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define FLASH_CS_B_OFFSET                                  (8)
#define FLASH_CS_B_LEN                                     (1)           /* M2/M2S.I/O. Flash CS_B; 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define FLASH_SPI_CCLK_OFFSET                              (9)
#define FLASH_SPI_CCLK_LEN                                 (1)           /* M2/M2S.I/O. Flash SPI CCLK; 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIF_TRISTATE (addr 0x8600) ---*/
#define GPIF_TRISTATE_B31_B10_OFFSET                       (10)
#define GPIF_TRISTATE_B31_B10_LEN                          (22)          /* 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define REF_SPI_PLL_LE_OFFSET                              (0)
#define REF_SPI_PLL_LE_LEN                                 (1)           /* M2S Only. Output Only. PLL latch enable. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define REF_EXT_10M_OFFSET                                 (1)
#define REF_EXT_10M_LEN                                    (1)           /* M2S Only. Output Only. Selects 10 MHz external reference input configuration. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define REF_EXT_40M_OFFSET                                 (2)
#define REF_EXT_40M_LEN                                    (1)           /* M2S Only. Output Only. Selects 40 MHz external reference input configuration (pass thru). 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define REF_INT_MEMS_OFFSET                                (3)
#define REF_INT_MEMS_LEN                                   (1)           /* M2S Only. Output Only. Selects internal 40 MHz reference configuration. only one of the above 3 should be high at a time. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define PL_CTRL_TDD_TXRX_N_OFFSET                          (4)
#define PL_CTRL_TDD_TXRX_N_LEN                             (1)           /* M2S Only. Output Only. Active high for transmit, active low for receive, used when operating in TDD mode. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EEPROM_WP_N_OFFSET                                 (5)
#define EEPROM_WP_N_LEN                                    (1)           /* M2S Only. Output Only, Write-protect configuration EEPROM when low (default). 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define AUX_40MHZ_OFF_OFFSET                               (6)
#define AUX_40MHZ_OFF_LEN                                  (1)           /* M2S Only. Output Only. write to low (default) to enable oscillator. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO7_OFFSET                                  (7)
#define EDGE_GPIO7_LEN                                     (1)           /* M2S Only. I/O. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_RESET_N_OFFSET                                (8)
#define EDGE_RESET_N_LEN                                   (1)           /* M2S Only. I/O.edge connector RESET signal. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define W_DISABLE_N_OFFSET                                 (9)
#define W_DISABLE_N_LEN                                    (1)           /* M2/M2S Only. I/O. edge connector signal, active low. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define GPIO_TRISTATE_B12_B10_OFFSET                       (10)
#define GPIO_TRISTATE_B12_B10_LEN                          (3)           /* 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO5_PPS_OFFSET                              (13)
#define EDGE_GPIO5_PPS_LEN                                 (1)           /* M2/M2S Only. Input Only. edge connector GPIO5_PPS. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define GPIO_TRISTATE_B15_B14_OFFSET                       (14)
#define GPIO_TRISTATE_B15_B14_LEN                          (2)           /* 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO0_OFFSET                                  (16)
#define EDGE_GPIO0_LEN                                     (1)           /* M2/M2S Only. Inp. edge connector GPIO0. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO1_OFFSET                                  (17)
#define EDGE_GPIO1_LEN                                     (1)           /* M2/M2S Only. Inp. edge connector GPIO1. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO2_OFFSET                                  (18)
#define EDGE_GPIO2_LEN                                     (1)           /* M2/M2S Only. Inp. edge connector GPIO2. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO3_OFFSET                                  (19)
#define EDGE_GPIO3_LEN                                     (1)           /* M2/M2S Only. Inp. edge connector GPIO3. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define EDGE_GPIO4_OFFSET                                  (20)
#define EDGE_GPIO4_LEN                                     (1)           /* M2/M2S Only. Inp. edge connector GPIO4. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define GPIO_RF_PORT_1_OFFSET                              (21)
#define GPIO_RF_PORT_1_LEN                                 (1)           /* Legacy Platforms only. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define GPIO_RF_PORT_2_OFFSET                              (22)
#define GPIO_RF_PORT_2_LEN                                 (1)           /* Legacy Platforms only. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define FPGA_USER_IO_OFFSET                                (23)
#define FPGA_USER_IO_LEN                                   (1)           /* M2/M2S Only. I/O. 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_TRISTATE (addr 0x860C) ---*/
#define GPIO_TRISTATE_B31_B24_OFFSET                       (24)
#define GPIO_TRISTATE_B31_B24_LEN                          (8)           /* 1: FPGA out, 0: FPGA in */

/*--- bit field of GPIO_WRITE (addr 0x8614) ---*/
#define GPIO_W_B23_B0_OFFSET                               (0)
#define GPIO_W_B23_B0_LEN                                  (24)

/*--- bit field of GPIO_WRITE (addr 0x8614) ---*/
#define RFIC_CTRL_IN_OFFSET                                (24)
#define RFIC_CTRL_IN_LEN                                   (4)

/*--- bit field of GPIO_WRITE (addr 0x8614) ---*/
#define GPIO_W_B31_B28_OFFSET                              (28)
#define GPIO_W_B31_B28_LEN                                 (4)

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP1_READING_OFFSET                               (0)
#define TEMP1_READING_LEN                                  (8)

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP2_READING_OFFSET                               (8)
#define TEMP2_READING_LEN                                  (8)

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP3_READING_OFFSET                               (16)
#define TEMP3_READING_LEN                                  (8)

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP1_READING_VALID_OFFSET                         (24)
#define TEMP1_READING_VALID_LEN                            (1)           /* In GPSDO mode after first temp1 reading will be high, otherwise low */

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP2_READING_VALID_OFFSET                         (25)
#define TEMP2_READING_VALID_LEN                            (1)           /* In GPSDO mode after first temp2 reading will be high, otherwise low */

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP3_READING_VALID_OFFSET                         (26)
#define TEMP3_READING_VALID_LEN                            (1)           /* In GPSDO mode after first temp3 reading will be high, otherwise low */

/*--- bit field of TEMP_READINGS (addr 0x8648) ---*/
#define TEMP_READINGS_B31_B27_OFFSET                       (27)
#define TEMP_READINGS_B31_B27_LEN                          (5)

/*--- bit field of FLASH_SPI_CTRL (addr 0x864C) ---*/
#define FLASH_SPI_ENABLE_OFFSET                            (0)
#define FLASH_SPI_ENABLE_LEN                               (1)

/*--- bit field of FLASH_SPI_CTRL (addr 0x864C) ---*/
#define FLASH_SPI_START_OFFSET                             (1)
#define FLASH_SPI_START_LEN                                (1)

/*--- bit field of FLASH_SPI_CTRL (addr 0x864C) ---*/
#define FLASH_SPI_EN_SEC_FLASH_OFFSET                      (2)
#define FLASH_SPI_EN_SEC_FLASH_LEN                         (1)

/*--- bit field of FLASH_SPI_CTRL (addr 0x864C) ---*/
#define FLASH_DELAY_COUNT_OFFSET                           (3)
#define FLASH_DELAY_COUNT_LEN                              (5)

/*--- bit field of FLASH_SPI_CTRL (addr 0x864C) ---*/
#define FLASH_READ_BYTE_COUNT_OFFSET                       (8)
#define FLASH_READ_BYTE_COUNT_LEN                          (10)

/*--- bit field of FLASH_SPI_CTRL (addr 0x864C) ---*/
#define FLASH_WRITE_BYTE_COUNT_OFFSET                      (20)
#define FLASH_WRITE_BYTE_COUNT_LEN                         (10)

/*--- bit field of FLASH_SPI_WRITE_DATA (addr 0x8650) ---*/
#define FLASH_SPI_WRITE_DATA_OFFSET                        (0)
#define FLASH_SPI_WRITE_DATA_LEN                           (8)

/*--- bit field of FLASH_SPI_READ_ACK (addr 0x8654) ---*/
#define FLASH_SPI_READ_FIFO_ACK_OFFSET                     (0)
#define FLASH_SPI_READ_FIFO_ACK_LEN                        (1)           /* Any write to this register triggers a read of the SPI FLASH read fifo */

/*--- bit field of FLASH_SPI_STATUS (addr 0x865C) ---*/
#define FLASH_SPI_DONE_OFFSET                              (0)
#define FLASH_SPI_DONE_LEN                                 (1)

/*--- bit field of FLASH_SPI_STATUS (addr 0x865C) ---*/
#define FLASH_WRITE_FIFO_EMPTY_OFFSET                      (1)
#define FLASH_WRITE_FIFO_EMPTY_LEN                         (1)

/*--- bit field of FLASH_SPI_STATUS (addr 0x865C) ---*/
#define FLASH_READ_FIFO_EMPTY_OFFSET                       (2)
#define FLASH_READ_FIFO_EMPTY_LEN                          (1)

/*--- bit field of I2C_2_SLAVE_CMD (addr 0x8664) ---*/
#define I2C_2_SLAVE_ADDR_OFFSET                            (0)
#define I2C_2_SLAVE_ADDR_LEN                               (7)

/*--- bit field of I2C_2_SLAVE_CMD (addr 0x8664) ---*/
#define I2C_2_CMD_NUM_BYTES_OFFSET                         (7)
#define I2C_2_CMD_NUM_BYTES_LEN                            (2)

/*--- bit field of I2C_2_SLAVE_CMD (addr 0x8664) ---*/
#define I2C_2_SLAVE_RD_WR_OFFSET                           (9)
#define I2C_2_SLAVE_RD_WR_LEN                              (1)

/*--- bit field of I2C_2_SLAVE_CMD (addr 0x8664) ---*/
#define I2C_2_SLAVE_CMD_B30_B10_OFFSET                     (10)
#define I2C_2_SLAVE_CMD_B30_B10_LEN                        (21)

/*--- bit field of I2C_2_STATUS (addr 0x866C) ---*/
#define I2C_2_SLAVE_NACK_OFFSET                            (0)
#define I2C_2_SLAVE_NACK_LEN                               (1)

/*--- bit field of I2C_2_STATUS (addr 0x866C) ---*/
#define I2C_2_DONE_OFFSET                                  (1)
#define I2C_2_DONE_LEN                                     (1)

/*--- bit field of I2C_2_STATUS (addr 0x866C) ---*/
#define I2C_2_STATUS_B31_B2_OFFSET                         (2)
#define I2C_2_STATUS_B31_B2_LEN                            (30)

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_STAGE_TIMER_OFFSET                          (0)
#define REG_RD_STAGE_TIMER_LEN                             (8)           /* Time in seconds at current gain setting */

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_KI_OFFSET                                   (8)
#define REG_RD_KI_LEN                                      (4)           /* Current gain parameter */

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_COUNTER_LOCKED_OFFSET                       (12)
#define REG_RD_COUNTER_LOCKED_LEN                          (4)           /* Number of consecutive low freq error values detected in a row */

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_COUNTER_UNLOCKED_OFFSET                     (16)
#define REG_RD_COUNTER_UNLOCKED_LEN                        (4)           /* Number of consecutive high freq error values detected in a row */

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_PLL_LOCKED_OFFSET                           (20)
#define REG_RD_PLL_LOCKED_LEN                              (1)           /* Lock bit inidcator of the gpsdo pll */

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_FIX_OUT_OFFSET                              (21)
#define REG_RD_FIX_OUT_LEN                                 (1)           /* Fix inidcator of the gpsdo. Note that selecting legacy PPS source forces the FPGA GPSDO GPS FIX (which is monitored with this status bit) to a constant high value */

/*--- bit field of GPSDO_STATUS (addr 0x8670) ---*/
#define REG_RD_GPSDO_LOCKED_OFFSET                         (22)
#define REG_RD_GPSDO_LOCKED_LEN                            (1)           /* Indicates if the control loop has converged according to algorithm */

/*--- bit field of XADC_START (addr 0x8674) ---*/
#define XADC_START_ADDRESS_OFFSET                          (0)
#define XADC_START_ADDRESS_LEN                             (7)

/*--- bit field of XADC_START (addr 0x8674) ---*/
#define XADC_START_STROBE_OFFSET                           (7)
#define XADC_START_STROBE_LEN                              (1)

/*--- bit field of XADC_START (addr 0x8674) ---*/
#define XADC_W_B31_B8_OFFSET                               (8)
#define XADC_W_B31_B8_LEN                                  (24)

/*--- bit field of XADC_READ_BACK (addr 0x8678) ---*/
#define XADC_READ_BACK_DATA_OFFSET                         (0)
#define XADC_READ_BACK_DATA_LEN                            (16)

/*--- bit field of XADC_READ_BACK (addr 0x8678) ---*/
#define XADC_READ_BACK_DONE_OFFSET                         (16)
#define XADC_READ_BACK_DONE_LEN                            (1)

/*--- bit field of XADC_READ_BACK (addr 0x8678) ---*/
#define XADC_R_B31_B17_OFFSET                              (17)
#define XADC_R_B31_B17_LEN                                 (15)

/*--- bit field of ATTEN_DATA (addr 0x8684) ---*/
#define ATTEN_DATA_OFFSET                                  (0)
#define ATTEN_DATA_LEN                                     (8)

/*--- bit field of ATTEN_DATA (addr 0x8684) ---*/
#define ATTEN_SPI_DATA_B31_B8_OFFSET                       (8)
#define ATTEN_SPI_DATA_B31_B8_LEN                          (24)

/*--- bit field of ATTEN_STAT (addr 0x8688) ---*/
#define ATTEN_STAT_OFFSET                                  (0)
#define ATTEN_STAT_LEN                                     (1)           /* 1 is done */

/*--- bit field of ATTEN_STAT (addr 0x8688) ---*/
#define ATTEN_SPI_STAT_B31_B1_OFFSET                       (1)
#define ATTEN_SPI_STAT_B31_B1_LEN                          (31)

/*--- bit field of JESD_DRP_WDATA (addr 0x8694) ---*/
#define GT0_DRP_WDATA_OFFSET                               (0)
#define GT0_DRP_WDATA_LEN                                  (16)          /* Dynamic Reconfiguration Port Write Data (X2/X4 Only) */

/*--- bit field of JESD_DRP_WDATA (addr 0x8694) ---*/
#define JESD_ALLOW_LMFC_RESET_OFFSET                       (31)
#define JESD_ALLOW_LMFC_RESET_LEN                          (1)           /* Indicate if legacy mode of the JESD logic should be used. 0: Use legacy mode  1: Use current mode */

/*--- bit field of TX_MEM_LOOP_SIZE (addr 0x86A0) ---*/
#define TX_MEM_LOOP_SIZE_OFFSET                            (0)
#define TX_MEM_LOOP_SIZE_LEN                               (12)          /* loop size will be this value written + 1, as indexing starts at 0. Valid values are x000 - xFFF. Example, For a loop of 4096 samples, use 0xFFF */

/*--- bit field of TX_MEM_LOOP_SIZE (addr 0x86A0) ---*/
#define TX_MEM_LOOP_SIZE_B27_B12_OFFSET                    (12)
#define TX_MEM_LOOP_SIZE_B27_B12_LEN                       (16)

/*--- bit field of TX_MEM_LOOP_SIZE (addr 0x86A0) ---*/
#define TX_MEM_SELECT_OFFSET                               (28)
#define TX_MEM_SELECT_LEN                                  (4)           /* selects which tx_mem is being written to. 0000=a1, 0001=a2, 0010=b1, 0011=b2, 0100=c1, 0101=c2, 0110=d1, 0111=d2 */

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define X2_FMC_EN_RXA1_X4_FMC_J6_SEL0_OFFSET               (0)
#define X2_FMC_EN_RXA1_X4_FMC_J6_SEL0_LEN                  (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define X2_FMC_EN_RXA2_X4_FMC_J6_SEL1_OFFSET               (1)
#define X2_FMC_EN_RXA2_X4_FMC_J6_SEL1_LEN                  (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL_OFFSET        (2)
#define X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL_LEN           (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_EN_TX_OFFSET                                   (3)
#define FMC_EN_TX_LEN                                      (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_RX_ENABLE_OFFSET                               (4)
#define FMC_RX_ENABLE_LEN                                  (2)           /* 0: RX1, 1: RX2 */

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_TX_ENABLE_OFFSET                               (6)
#define FMC_TX_ENABLE_LEN                                  (2)           /* 0: TX1, 1: TX2 */

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_RX1_ATTEN_LE_OFFSET                            (8)
#define FMC_RX1_ATTEN_LE_LEN                               (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_RX2_ATTEN_LE_OFFSET                            (9)
#define FMC_RX2_ATTEN_LE_LEN                               (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_ORX_ATTEN_LE_OFFSET                            (10)
#define FMC_ORX_ATTEN_LE_LEN                               (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_CTRL_B11_OFFSET                                (11)
#define FMC_CTRL_B11_LEN                                   (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define MYK_PWR_EN_OFFSET                                  (12)
#define MYK_PWR_EN_LEN                                     (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define RESET_N_OFFSET                                     (13)
#define RESET_N_LEN                                        (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define AD9528_SPI_RESET_N_OFFSET                          (14)
#define AD9528_SPI_RESET_N_LEN                             (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define PLL1_RESET_N_OFFSET                                (15)
#define PLL1_RESET_N_LEN                                   (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define PLL2_RESET_N_OFFSET                                (16)
#define PLL2_RESET_N_LEN                                   (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define SYSREF_REQ_OFFSET                                  (17)
#define SYSREF_REQ_LEN                                     (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define TEN_MEG_EN_OFFSET                                  (18)
#define TEN_MEG_EN_LEN                                     (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define VCXO_153M6_EN_OFFSET                               (19)
#define VCXO_153M6_EN_LEN                                  (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define VCXO_100M_EN_OFFSET                                (20)
#define VCXO_100M_EN_LEN                                   (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_REF_SEL_OFFSET                                 (21)
#define FMC_REF_SEL_LEN                                    (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_RED_LED_ENA_OFFSET                             (22)
#define FMC_RED_LED_ENA_LEN                                (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_GREEN_LED_ENA_OFFSET                           (23)
#define FMC_GREEN_LED_ENA_LEN                              (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_BLUE_LED_ENA_OFFSET                            (24)
#define FMC_BLUE_LED_ENA_LEN                               (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_RX_B_ENABLE_OFFSET                             (25)
#define FMC_RX_B_ENABLE_LEN                                (2)           /* 0: RX1_B, 1: RX2_B */

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define FMC_TX_B_ENABLE_OFFSET                             (27)
#define FMC_TX_B_ENABLE_LEN                                (2)           /* 0: TX1_B, 1: TX2_B */

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define RESET_N_B_OFFSET                                   (29)
#define RESET_N_B_LEN                                      (1)

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define EXT_CLK_SEL_OFFSET                                 (30)
#define EXT_CLK_SEL_LEN                                    (1)           /* 1 selects external clock */

/*--- bit field of FMC_CTRL (addr 0x86A4) ---*/
#define EXT_CLK_ENA_OFFSET                                 (31)
#define EXT_CLK_ENA_LEN                                    (1)           /* 1 enables external clock */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define FPGA_SOFT_RESET_OFFSET                             (0)
#define FPGA_SOFT_RESET_LEN                                (1)

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define PPS_SEL_OFFSET                                     (1)
#define PPS_SEL_LEN                                        (1)           /* 0: legacy PPS, 1: GPS PPS. Note that selecting legacy PPS forces the FPGA GPSDO GPS FIX to a constant high value */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define IQ_MULT_ENABLE_OFFSET                              (2)
#define IQ_MULT_ENABLE_LEN                                 (1)           /* 0: Bypass iq channel multipliers, 1: Enable iq channel multipliers */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define TX_A2_MAP_TO_B1_OFFSET                             (3)
#define TX_A2_MAP_TO_B1_LEN                                (1)           /* 0: Normal mode, Tx a2 is mapped to JESD channel a2, 1: Tx a2 is mapped to JESD channel b1 */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define FPGA_CTRL_B6_B4_OFFSET                             (4)
#define FPGA_CTRL_B6_B4_LEN                                (3)

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define GT_TXPRECURSOR_OFFSET                              (7)
#define GT_TXPRECURSOR_LEN                                 (5)           /* jesd tx pre-emphasis setting */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define GT_TXDIFFCTRL_OFFSET                               (12)
#define GT_TXDIFFCTRL_LEN                                  (4)           /* jesd tx amplitude setting */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define FRC_SEL_FOR_TX_OFFSET                              (16)
#define FRC_SEL_FOR_TX_LEN                                 (1)           /* 0: Use frc_a for tx on timestamp, 1: Use frc_sys for tx on timestamp */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define SPI_CTRL_SELECT_OFFSET                             (17)
#define SPI_CTRL_SELECT_LEN                                (1)           /* 0: attenuation spi, 1: RFFC SPI */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define FPGA_CTRL_B18_OFFSET                               (18)
#define FPGA_CTRL_B18_LEN                                  (1)

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define GT_TX_CONTROL_OFFSET                               (19)
#define GT_TX_CONTROL_LEN                                  (1)           /* 0: jesd tx sync normal operation, 1: force incoming jesd tx sync off to aid with jesd debug */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define IQ_COUNT_MODE_DECIMATION_OFFSET                    (20)
#define IQ_COUNT_MODE_DECIMATION_LEN                       (4)           /* setting when using iq count mode in decimation mode, only affect channels a1/a2. decimate by 2**DECIMATION_RATE, values supported = 0-10 */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define TX_LOOPBACK_OFFSET                                 (24)
#define TX_LOOPBACK_LEN                                    (1)           /* set to high to enable PCIe Tx to Rx Loopback mode */

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define FPGA_CTRL_B26_B25_OFFSET                           (25)
#define FPGA_CTRL_B26_B25_LEN                              (2)

/*--- bit field of FPGA_CTRL (addr 0x86A8) ---*/
#define GT_TXPOSTCURSOR_OFFSET                             (27)
#define GT_TXPOSTCURSOR_LEN                                (5)           /* jesd tx post-emphasis setting */

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_SYNC_OFFSET                                     (0)
#define RX_SYNC_LEN                                        (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_TVALID_OFFSET                                   (1)
#define RX_TVALID_LEN                                      (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_1_NOT_FULL_OFFSET                               (2)
#define RX_1_NOT_FULL_LEN                                  (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_2_NOT_FULL_OFFSET                               (3)
#define RX_2_NOT_FULL_LEN                                  (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_SYNC_OFFSET                                  (4)
#define OBSRX_SYNC_LEN                                     (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_TVALID_OFFSET                                (5)
#define OBSRX_TVALID_LEN                                   (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_1_NOT_FULL_OFFSET                            (6)
#define OBSRX_1_NOT_FULL_LEN                               (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_2_NOT_FULL_OFFSET                            (7)
#define OBSRX_2_NOT_FULL_LEN                               (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_SYNC_OFFSET                                     (8)
#define TX_SYNC_LEN                                        (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_TREADY_OFFSET                                   (9)
#define TX_TREADY_LEN                                      (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_RD_EN_1_OFFSET                                  (10)
#define TX_RD_EN_1_LEN                                     (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_RD_EN_2_OFFSET                                  (11)
#define TX_RD_EN_2_LEN                                     (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_RESET_DONE_RX_OFFSET                            (12)
#define RX_RESET_DONE_RX_LEN                               (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_RESET_DONE_OBSRX_OFFSET                         (13)
#define RX_RESET_DONE_OBSRX_LEN                            (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_RESET_DONE_OFFSET                               (14)
#define TX_RESET_DONE_LEN                                  (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define JESD_COMMON0_QPLL0_LOCK_OFFSET                     (15)
#define JESD_COMMON0_QPLL0_LOCK_LEN                        (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_SYNC_B_OFFSET                                   (16)
#define RX_SYNC_B_LEN                                      (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_TVALID_B_OFFSET                                 (17)
#define RX_TVALID_B_LEN                                    (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_1_NOT_FULL_B_OFFSET                             (18)
#define RX_1_NOT_FULL_B_LEN                                (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_2_NOT_FULL_B_OFFSET                             (19)
#define RX_2_NOT_FULL_B_LEN                                (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_SYNC_B_OFFSET                                (20)
#define OBSRX_SYNC_B_LEN                                   (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_TVALID_B_OFFSET                              (21)
#define OBSRX_TVALID_B_LEN                                 (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_1_NOT_FULL_B_OFFSET                          (22)
#define OBSRX_1_NOT_FULL_B_LEN                             (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define OBSRX_2_NOT_FULL_B_OFFSET                          (23)
#define OBSRX_2_NOT_FULL_B_LEN                             (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_SYNC_B_OFFSET                                   (24)
#define TX_SYNC_B_LEN                                      (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_TREADY_B_OFFSET                                 (25)
#define TX_TREADY_B_LEN                                    (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_RD_EN_1_B_OFFSET                                (26)
#define TX_RD_EN_1_B_LEN                                   (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_RD_EN_2_B_OFFSET                                (27)
#define TX_RD_EN_2_B_LEN                                   (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_RESET_DONE_RX_B_OFFSET                          (28)
#define RX_RESET_DONE_RX_B_LEN                             (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define RX_RESET_DONE_OBSRX_B_OFFSET                       (29)
#define RX_RESET_DONE_OBSRX_B_LEN                          (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define TX_RESET_DONE_B_OFFSET                             (30)
#define TX_RESET_DONE_B_LEN                                (1)

/*--- bit field of JESD_STATUS (addr 0x86AC) ---*/
#define JESD_COMMON0_QPLL1_LOCK_OFFSET                     (31)
#define JESD_COMMON0_QPLL1_LOCK_LEN                        (1)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define FORCE_TXERR_OFFSET                                 (0)
#define FORCE_TXERR_LEN                                    (1)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define RXPRBSCNTRESET_OFFSET                              (1)
#define RXPRBSCNTRESET_LEN                                 (1)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define PHY_RESET_OFFSET                                   (2)
#define PHY_RESET_LEN                                      (1)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define FORCE_RX_SYNC_LOW_OFFSET                           (3)
#define FORCE_RX_SYNC_LOW_LEN                              (1)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define TXPRBSSEL_OFFSET                                   (4)
#define TXPRBSSEL_LEN                                      (4)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define RXPRBSSEL_OFFSET                                   (8)
#define RXPRBSSEL_LEN                                      (4)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define RX_4_CHANNEL_PHASE_COHERENT_OFFSET                 (12)
#define RX_4_CHANNEL_PHASE_COHERENT_LEN                    (1)           /* 0: Rx is not four channel phase coherent, 1: Rx is four channel phase coherent */

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define TX_4_CHANNEL_PHASE_COHERENT_OFFSET                 (13)
#define TX_4_CHANNEL_PHASE_COHERENT_LEN                    (1)           /* 0: Tx is not four channel phase coherent, 1: Tx is four channel phase coherent */

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define JESD_PRBS_CTRL_B15_B14_OFFSET                      (14)
#define JESD_PRBS_CTRL_B15_B14_LEN                         (2)

/*--- bit field of JESD_PRBS_CTRL (addr 0x86B0) ---*/
#define SYNC_COUNT_OFFSET                                  (16)
#define SYNC_COUNT_LEN                                     (16)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define RX_UNSYNC_OFFSET                                   (0)
#define RX_UNSYNC_LEN                                      (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define OBSRX_UNSYNC_OFFSET                                (4)
#define OBSRX_UNSYNC_LEN                                   (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define TX_UNSYNC_OFFSET                                   (8)
#define TX_UNSYNC_LEN                                      (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define SNTX_UNSYNC_OFFSET                                 (12)
#define SNTX_UNSYNC_LEN                                    (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define RX_UNSYNC_B_OFFSET                                 (16)
#define RX_UNSYNC_B_LEN                                    (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define OBSRX_UNSYNC_B_OFFSET                              (20)
#define OBSRX_UNSYNC_B_LEN                                 (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define TX_UNSYNC_B_OFFSET                                 (24)
#define TX_UNSYNC_B_LEN                                    (4)

/*--- bit field of JESD_UNSYNC_COUNTERS (addr 0x86B4) ---*/
#define SNTX_UNSYNC_B_OFFSET                               (28)
#define SNTX_UNSYNC_B_LEN                                  (4)

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define GT_RXRATE_PHY0_1_OFFSET                            (0)
#define GT_RXRATE_PHY0_1_LEN                               (3)           /* 1 = 245.76 MHz, 2 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.72 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY0_PHY1_RX_LANE_MAP_OFFSET                       (3)
#define PHY0_PHY1_RX_LANE_MAP_LEN                          (1)           /* For the two PHY pair, 0: JESD lane 0 is mapped to JESD core logic lane 0, 1: JESD lane 1 is mapped to JESD core logic lane 0 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define GT_RXRATE_PHY1_UNUSED_OFFSET                       (4)
#define GT_RXRATE_PHY1_UNUSED_LEN                          (3)           /* Not Currently Used */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY2_PHY3_RX_LANE_MAP_OFFSET                       (7)
#define PHY2_PHY3_RX_LANE_MAP_LEN                          (1)           /* For the two PHY pair, 0: JESD lane 0 is mapped to JESD core logic lane 0, 1: JESD lane 1 is mapped to JESD core logic lane 0 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define GT_RXRATE_PHY2_3_OFFSET                            (8)
#define GT_RXRATE_PHY2_3_LEN                               (3)           /* 1 = 245.76 MHz, 2 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.722 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.72 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY4_PHY5_RX_LANE_MAP_OFFSET                       (11)
#define PHY4_PHY5_RX_LANE_MAP_LEN                          (1)           /* For the two PHY pair, 0: JESD lane 0 is mapped to JESD core logic lane 0, 1: JESD lane 1 is mapped to JESD core logic lane 0 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define GT_RXRATE_PHY3_UNUSED_OFFSET                       (12)
#define GT_RXRATE_PHY3_UNUSED_LEN                          (3)           /* Not Currently Used */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY6_PHY7_RX_LANE_MAP_OFFSET                       (15)
#define PHY6_PHY7_RX_LANE_MAP_LEN                          (1)           /* For the two PHY pair, 0: JESD lane 0 is mapped to JESD core logic lane 0, 1: JESD lane 1 is mapped to JESD core logic lane 0 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define GT_RXRATE_PHY4_5_OFFSET                            (16)
#define GT_RXRATE_PHY4_5_LEN                               (3)           /* 1 = 245.76 MHz, 2 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.722 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.72 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define GT_RXRATE_PHY6_7_OFFSET                            (19)
#define GT_RXRATE_PHY6_7_LEN                               (3)           /* 1 = 245.76 MHz, 2 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.722 = 122.88 MHz, 3 = 61.44 MHz, 4 = 30.72 */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY0_PHY1_RX_DUAL_LANE_OFFSET                      (22)
#define PHY0_PHY1_RX_DUAL_LANE_LEN                         (1)           /* For the two PHY pair, 0: JESD core logic uses 1 JESD Lane, 1: JESD core logic uses 2 JESD Lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY2_PHY3_RX_DUAL_LANE_OFFSET                      (23)
#define PHY2_PHY3_RX_DUAL_LANE_LEN                         (1)           /* For the two PHY pair, 0: JESD core logic uses 1 JESD Lane, 1: JESD core logic uses 2 JESD Lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY4_PHY5_RX_DUAL_LANE_OFFSET                      (24)
#define PHY4_PHY5_RX_DUAL_LANE_LEN                         (1)           /* For the two PHY pair, 0: JESD core logic uses 1 JESD Lane, 1: JESD core logic uses 2 JESD Lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY6_PHY7_RX_DUAL_LANE_OFFSET                      (25)
#define PHY6_PHY7_RX_DUAL_LANE_LEN                         (1)           /* For the two PHY pair, 0: JESD core logic uses 1 JESD Lane, 1: JESD core logic uses 2 JESD Lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY0_PHY1_RX_SINGLE_CHANNEL_ON_DUAL_LANE_OFFSET    (26)
#define PHY0_PHY1_RX_SINGLE_CHANNEL_ON_DUAL_LANE_LEN       (1)           /* For the two PHY pair, when configured for dual Lane, JESD core logic is configured for 0: dual channels use the dual lanes, 1: a single channel uses the dual lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY2_PHY3_RX_SINGLE_CHANNEL_ON_DUAL_LANE_OFFSET    (27)
#define PHY2_PHY3_RX_SINGLE_CHANNEL_ON_DUAL_LANE_LEN       (1)           /* For the two PHY pair, when configured for dual Lane, JESD core logic is configured for 0: dual channels use the dual lanes, 1: a single channel uses the dual lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY4_PHY5_RX_SINGLE_CHANNEL_ON_DUAL_LANE_OFFSET    (28)
#define PHY4_PHY5_RX_SINGLE_CHANNEL_ON_DUAL_LANE_LEN       (1)           /* For the two PHY pair, when configured for dual Lane, JESD core logic is configured for 0: dual channels use the dual lanes, 1: a single channel uses the dual lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY6_PHY7_RX_SINGLE_CHANNEL_ON_DUAL_LANE_OFFSET    (29)
#define PHY6_PHY7_RX_SINGLE_CHANNEL_ON_DUAL_LANE_LEN       (1)           /* For the two PHY pair, when configured for dual Lane, JESD core logic is configured for 0: dual channels use the dual lanes, 1: a single channel uses the dual lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY0_PHY3_TX_SINGLE_CHANNEL_ON_DUAL_LANE_OFFSET    (30)
#define PHY0_PHY3_TX_SINGLE_CHANNEL_ON_DUAL_LANE_LEN       (1)           /* For the four PHY pair,when configured for dual Lane, JESD core logic is configured for 0: dual channels use the dual lanes, 1: a single channel uses the dual lanes */

/*--- bit field of JESD_PHY_RX_RATE (addr 0x86B8) ---*/
#define PHY4_PHY7_TX_SINGLE_CHANNEL_ON_DUAL_LANE_OFFSET    (31)
#define PHY4_PHY7_TX_SINGLE_CHANNEL_ON_DUAL_LANE_LEN       (1)           /* For the four PHY pair,when configured for dual Lane, JESD core logic is configured for 0: dual channels use the dual lanes, 1: a single channel uses the dual lanes */

/*--- bit field of ICAP_READ_DATA (addr 0x86C4) ---*/
#define ICAP_DEBUG_RESERVED_OFFSET                         (0)
#define ICAP_DEBUG_RESERVED_LEN                            (4)           /* Reserved, always returns 0xF */

/*--- bit field of ICAP_READ_DATA (addr 0x86C4) ---*/
#define ICAP_DEBUG_IN_ABORT_B_OFFSET                       (4)
#define ICAP_DEBUG_IN_ABORT_B_LEN                          (1)           /* ABORT in progress (active Low). [7:0] 9F indicates No Sync and No CFGERR */

/*--- bit field of ICAP_READ_DATA (addr 0x86C4) ---*/
#define ICAP_DEBUG_RIP_OFFSET                              (5)
#define ICAP_DEBUG_RIP_LEN                                 (1)           /* Readback in progress (active High). [7:0] DF indicates Sync and No CFGERR */

/*--- bit field of ICAP_READ_DATA (addr 0x86C4) ---*/
#define ICAP_DEBUG_DALIGN_OFFSET                           (6)
#define ICAP_DEBUG_DALIGN_LEN                              (1)           /* Sync word received (active High). [7:0] 5F indicates Sync and CFGERR */

/*--- bit field of ICAP_READ_DATA (addr 0x86C4) ---*/
#define ICAP_DEBUG_CFGERR_B_OFFSET                         (7)
#define ICAP_DEBUG_CFGERR_B_LEN                            (1)           /* Configuration error (active Low). [7:0] 1F indicates No Sync and CFGERR */

/*--- bit field of ICAP_READ_DATA (addr 0x86C4) ---*/
#define ICAP_READ_DATA_B31_B8_OFFSET                       (8)
#define ICAP_READ_DATA_B31_B8_LEN                          (24)

/*--- bit field of GPSDO_READ_COUNTER (addr 0x86C8) ---*/
#define REG_RD_COUNT_OFFSET                                (0)
#define REG_RD_COUNT_LEN                                   (30)          /* Latched value of the signed frequency counter at each PPS edge (i.e., frequency error value) */

/*--- bit field of GPSDO_READ_COUNTER (addr 0x86C8) ---*/
#define REG_RD_SECS_OFFSET                                 (30)
#define REG_RD_SECS_LEN                                    (2)           /* Two-bit counter that counts PPS signals, modulo 4 */

/*--- bit field of GPSDO_READ_WARP (addr 0x86CC) ---*/
#define REG_RD_WARP_OFFSET                                 (0)
#define REG_RD_WARP_LEN                                    (26)          /* 26 bit signed warp voltage value */

/*--- bit field of GPSDO_CONFIG1 (addr 0x86D0) ---*/
#define REG_WARP_SET_VALUE_OFFSET                          (0)
#define REG_WARP_SET_VALUE_LEN                             (26)          /* Set the warp value for the VCO */

/*--- bit field of GPSDO_CONFIG1 (addr 0x86D0) ---*/
#define REG_THRESH_LOCKED_OFFSET                           (26)
#define REG_THRESH_LOCKED_LEN                              (5)           /* Frequency error to considered within lock */

/*--- bit field of GPSDO_CONFIG1 (addr 0x86D0) ---*/
#define REG_ENABLE_OFFSET                                  (31)
#define REG_ENABLE_LEN                                     (1)           /* gpsdo enable */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define REG_KI_START_OFFSET                                (0)
#define REG_KI_START_LEN                                   (4)           /* Initial Ki setting to use */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define REG_KI_END_OFFSET                                  (4)
#define REG_KI_END_LEN                                     (4)           /* Maximum Ki value (minimum gain value) */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define REG_THRESH_UNLOCKED_OFFSET                         (8)
#define REG_THRESH_UNLOCKED_LEN                            (5)           /* Frequency error to consider out of lock */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define REG_TIME_IN_STAGE_OFFSET                           (13)
#define REG_TIME_IN_STAGE_LEN                              (8)           /* Time to stay at each Ki gain value before reducing gain */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define REG_COUNT_LOCKED_OFFSET                            (21)
#define REG_COUNT_LOCKED_LEN                               (4)           /* Number of consecutive low errors to see before entering lock state */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define REG_COUNT_UNLOCKED_OFFSET                          (25)
#define REG_COUNT_UNLOCKED_LEN                             (4)           /* Number of consecutive high frequency counts to see before leaving lock state */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define GPSDO_RESET_OVERRIDE_OFFSET                        (29)
#define GPSDO_RESET_OVERRIDE_LEN                           (1)           /* Override the pps_sel bit from the FPGA_CTRL register with the pps_sel_gps in this register */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define PPS_SEL_GPS_OFFSET                                 (30)
#define PPS_SEL_GPS_LEN                                    (1)           /* selects which pps to use for the gpsdo when the gpsdo_reset_override is set to a 1. Note that the pps_sel from the FPGA_CTRL register gets reset when a register reset is initiated, the pps_sel in this register bit does not, which allows gpsdo functionality to continue during a register reset operation. */

/*--- bit field of GPSDO_CONFIG2 (addr 0x86D4) ---*/
#define MEMS_ENABLE_GPS_OFFSET                             (31)
#define MEMS_ENABLE_GPS_LEN                                (1)           /* enables/disables the MEMs functionality when the gpsdo_reset_override is set to a 1. Note that the ref_int_mems from the GPIO_WRITE register gets reset when a register reset is initiated, the mems_enable_gps in this register bit does not, which allows gpsdo functionality to continue during a register reset operation. */

/*--- bit field of JESD_STATUS2 (addr 0x86D8) ---*/
#define SYSREF_ALIGNMENT_ERROR_CNT_OFFSET                  (0)
#define SYSREF_ALIGNMENT_ERROR_CNT_LEN                     (16)          /* Counts the number of sysref alignment errors once the tx reset done has gone high */

/*--- bit field of JESD_CTRL (addr 0x86DC) ---*/
#define SYSREF_DISABLE_OFFSET                              (0)
#define SYSREF_DISABLE_LEN                                 (1)           /* Disables sysref from affecting the JESD Tx logic */

/*--- bit field of JESD_CTRL (addr 0x86DC) ---*/
#define SYSREF_ONESHOT_OFFSET                              (1)
#define SYSREF_ONESHOT_LEN                                 (1)           /* Only allow one sysref pulse to affect the JESD Tx logic */

/*--- bit field of DEBUG_IQ_SUBSAMPLE0 (addr 0x86E0) ---*/
#define Q_SUBSAMPLE_OFFSET                                 (0)
#define Q_SUBSAMPLE_LEN                                    (16)          /* Q subsample value, signed 16-bit */

/*--- bit field of DEBUG_IQ_SUBSAMPLE0 (addr 0x86E0) ---*/
#define I_SUBSAMPLE_OFFSET                                 (16)
#define I_SUBSAMPLE_LEN                                    (16)          /* I subsample value, signed 16-bit */

/*--- bit field of DEBUG_IQ_SUBSAMPLE1 (addr 0x86E4) ---*/
#define Q_SUBSAMPLE_OFFSET                                 (0)
#define Q_SUBSAMPLE_LEN                                    (16)          /* Q subsample value, signed 16-bit */

/*--- bit field of DEBUG_IQ_SUBSAMPLE1 (addr 0x86E4) ---*/
#define I_SUBSAMPLE_OFFSET                                 (16)
#define I_SUBSAMPLE_LEN                                    (16)          /* I subsample value, signed 16-bit */

/*--- bit field of CLK_RATE_EST0 (addr 0x86E8) ---*/
#define RFFE_REF_CLK_RATE_OFFSET                           (0)
#define RFFE_REF_CLK_RATE_LEN                              (16)          /* Clock rate estimate for the RF Front End Reference clock. */

/*--- bit field of CLK_RATE_EST0 (addr 0x86E8) ---*/
#define SYS_CLK_RATE_OFFSET                                (16)
#define SYS_CLK_RATE_LEN                                   (16)          /* Clock rate estimate for the system clock */

/*--- bit field of CLK_RATE_EST1 (addr 0x86EC) ---*/
#define RX_SAMPLE_CLK_RFIC_A_RATE_OFFSET                   (0)
#define RX_SAMPLE_CLK_RFIC_A_RATE_LEN                      (16)          /* clock rate estimate for the RFIC A Rx sample clock */

/*--- bit field of CLK_RATE_EST1 (addr 0x86EC) ---*/
#define RX_SAMPLE_CLK_RFIC_B_RATE_OFFSET                   (16)
#define RX_SAMPLE_CLK_RFIC_B_RATE_LEN                      (16)          /* clock rate estimate for the RFIC B Rx sample clock */

#endif  /* __SIDEKIQ_FPGA_REG_DEFS_H__ */