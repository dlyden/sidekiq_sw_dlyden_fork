#ifndef __SIDEKIQ_RFIC_H__
#define __SIDEKIQ_RFIC_H__

/*****************************************************************************/
/** @file sidekiq_rfic.h
 
 <pre>
 Copyright 2017,2021 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RF IC.
    The RF IC is responsible for configuring the RF chip.  
    This sidekiq_rfic provides an abstraction layer
    which the specific hardware realization implements.
*/

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <stdio.h>

#include "sidekiq_rf.h"
#include "sidekiq_types.h"
#include "sidekiq_types_private.h"

/***** MACROS *****/

/***** TYPEDEFS *****/

/** @brief The various function pointers to be implemented by a realization of the RFIC */
typedef struct
{
    int32_t (*reset_and_init)( rf_id_t *p_id );
    void (*display_version_info)( void );
    int32_t (*release)( rf_id_t *p_id );

    int32_t (*write_rx_freq)( rf_id_t *p_id, uint64_t freq, double *p_act_freq );
    int32_t (*write_tx_freq)( rf_id_t *p_id, uint64_t freq, double *p_act_freq );

    int32_t (*write_rx_spectrum_invert)( rf_id_t *p_id, bool invert );
    int32_t (*write_tx_spectrum_invert)( rf_id_t *p_id, bool invert );

    int32_t (*write_tx_attenuation)( rf_id_t *p_id, uint16_t atten );
    int32_t (*read_tx_attenuation)( rf_id_t *p_id, uint16_t *p_atten );
    int32_t (*read_tx_attenuation_range)( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min );

    int32_t (*write_rx_gain_mode)( rf_id_t *p_id, skiq_rx_gain_t gain_mode );
    int32_t (*read_rx_gain_mode)( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode );

    int32_t (*read_rx_gain_range)( rf_id_t *p_id, uint8_t *p_gain_max, uint8_t *p_gain_min ); 
    int32_t (*write_rx_gain)( rf_id_t *p_id, uint8_t gain );
    int32_t (*read_rx_gain)( rf_id_t *p_id, uint8_t *p_gain );

    int32_t (*read_adc_resolution)( uint8_t *p_adc_res );
    int32_t (*read_dac_resolution)( uint8_t *p_dac_res );

    int32_t (*read_warp_voltage)( rf_id_t *p_id, uint16_t *p_warp_voltage );
    int32_t (*write_warp_voltage)( rf_id_t *p_id, uint16_t warp_voltage );
    int32_t (*read_warp_voltage_extended_range)( rf_id_t *p_id, uint32_t *p_warp_voltage );
    int32_t (*write_warp_voltage_extended_range)( rf_id_t *p_id, uint32_t warp_voltage );

    int32_t (*enable_rx_chan)( rf_id_t *p_id, bool enable );
    int32_t (*enable_tx_chan)( rf_id_t *p_id, bool enable );

    int32_t (*config_tx_test_tone)( rf_id_t *p_id, bool enable );
    int32_t (*read_tx_test_tone_freq)( rf_id_t *p_id, int32_t *p_tone_offset );
    int32_t (*write_tx_test_tone_freq)( rf_id_t *p_id, int32_t test_freq_offset );
    
    void (*read_min_rx_sample_rate)( rf_id_t *p_id, uint32_t *p_min_rx_sample_rate );
    void (*read_max_rx_sample_rate)( rf_id_t *p_id, uint32_t *p_max_rx_sample_rate );
    void (*read_min_tx_sample_rate)( rf_id_t *p_id, uint32_t *p_min_tx_sample_rate );
    void (*read_max_tx_sample_rate)( rf_id_t *p_id, uint32_t *p_max_tx_sample_rate );

    int32_t (*write_rx_sample_rate_and_bandwidth)( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );
    int32_t (*write_tx_sample_rate_and_bandwidth)( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );

    int32_t (*write_rx_sample_rate_and_bandwidth_multi)( rf_id_t *p_id, skiq_rx_hdl_t handles[],
                                                         uint8_t nr_handles,
                                                         uint32_t rate[],
                                                         uint32_t bandwidth[]);

    int32_t (*read_rx_sample_rate)( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );
    int32_t (*read_tx_sample_rate)( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );

    int32_t (*read_rx_chan_bandwidth)( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
    int32_t (*read_tx_chan_bandwidth)( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );

    int32_t (*init_gpio)( rf_id_t *p_id, uint32_t output_enable );
    int32_t (*write_gpio)( rf_id_t *p_id, uint32_t state );
    int32_t (*read_gpio)( rf_id_t *p_id, uint32_t* p_state );

    int32_t (*read_rx_filter_overflow)( rf_id_t *p_id, bool *p_rx_fir );
    int32_t (*read_tx_filter_overflow)( rf_id_t *p_id, bool *p_int3, bool *p_hb3,
                                        bool *p_hb2, bool *p_qec, bool *p_hb1, bool *p_tx_fir );

    int32_t (*read_fpga_rf_clock)( rf_id_t *p_id, uint64_t *p_freq );

    int32_t (*read_tx_quadcal_mode)( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode );
    int32_t (*write_tx_quadcal_mode)( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode );
    int32_t (*run_tx_quadcal)( rf_id_t *p_id );

    int32_t (*read_rx_stream_hdl_conflict)( rf_id_t *p_id, skiq_rx_hdl_t *p_conflicting_hdls, uint8_t *p_num_hdls );

    bool (*is_chan_enable_xport_dependent)( rf_id_t *p_id );

    int32_t (*read_control_output_rx_gain_config)( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );
    int32_t (*write_control_output_config)( rf_id_t *p_id, uint8_t mode, uint8_t ena );
    int32_t (*read_control_output_config)( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );

    int32_t (*init_from_file)( rf_id_t *p_id, FILE* p_file );

    int32_t (*power_down_tx)( rf_id_t *p_id );
    
    int32_t (*enable_pin_gain_ctrl)(rf_id_t *p_id);

    uint32_t (*read_hop_on_ts_gpio)( rf_id_t *p_id, uint8_t *p_chip_index );

    int32_t (*write_rx_freq_tune_mode)( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
    int32_t (*read_rx_freq_tune_mode)( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

    int32_t (*write_tx_freq_tune_mode)( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
    int32_t (*read_tx_freq_tune_mode)( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

    int32_t (*config_rx_hop_list)( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index );
    int32_t (*read_rx_hop_list)( rf_id_t *p_id, uint16_t *p_num_freq, uint64_t *p_freq_list );

    int32_t (*config_tx_hop_list)( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index );
    int32_t (*read_tx_hop_list)( rf_id_t *p_id, uint16_t *p_num_freq, uint64_t *p_freq_list );

    int32_t (*write_next_rx_hop)( rf_id_t *p_id, uint16_t freq_index );
    int32_t (*write_next_tx_hop)( rf_id_t *p_id, uint16_t freq_index );

    int32_t (*rx_hop)( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );
    int32_t (*tx_hop)( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );

    int32_t (*read_curr_rx_hop)( rf_id_t *p_id, freq_hop_t *p_hop );
    int32_t (*read_next_rx_hop)( rf_id_t *p_id, freq_hop_t *p_hop );

    int32_t (*read_curr_tx_hop)( rf_id_t *p_id, freq_hop_t *p_hop );
    int32_t (*read_next_tx_hop)( rf_id_t *p_id, freq_hop_t *p_hop );

    int32_t (*read_rx_cal_mode)( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode );
    int32_t (*write_rx_cal_mode)( rf_id_t *p_id, skiq_rx_cal_mode_t mode );

    int32_t (*run_rx_cal)( rf_id_t *p_id );

    int32_t (*read_rx_cal_mask)( rf_id_t *p_id, uint32_t *p_cal_mask );
    int32_t (*write_rx_cal_mask)( rf_id_t *p_id, uint32_t cal_mask );

    int32_t (*read_rx_cal_types_avail)( rf_id_t *p_id, uint32_t *p_cal_mask );

    int32_t (*read_tx_enable_mode)( rf_id_t *p_id, skiq_rfic_pin_mode_t *p_mode );
    int32_t (*read_rx_enable_mode)( rf_id_t *p_id, skiq_rfic_pin_mode_t *p_mode );

    int32_t (*write_tx_enable_mode)( rf_id_t *p_id, skiq_rfic_pin_mode_t mode );
    int32_t (*write_rx_enable_mode)( rf_id_t *p_id, skiq_rfic_pin_mode_t mode );

    int32_t (*read_temp)( rf_id_t *p_id, int8_t *p_temp_in_deg_C );
    int32_t (*read_auxadc)( rf_id_t *p_id, uint8_t channel, uint16_t *p_millivolts );

    int32_t (*swap_rx_port_config)( rf_id_t *p_id, uint8_t port_a, uint8_t port_b );
    int32_t (*swap_tx_port_config)( rf_id_t *p_id, uint8_t port_a, uint8_t port_b );

    int32_t (*read_rf_capabilities)( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities );
    int32_t (*write_ext_clock_select)( rf_id_t *p_id, bool ext_clk_sel );

    skiq_rx_hdl_t (*rx_hdl_map_other)( rf_id_t *p_id );
    skiq_tx_hdl_t (*tx_hdl_map_other)( rf_id_t *p_id );

    int32_t (*read_rx_analog_filter_bandwidth)( rf_id_t *p_id, uint32_t *p_bandwidth );
    int32_t (*read_tx_analog_filter_bandwidth)( rf_id_t *p_id, uint32_t *p_bandwidth );

    int32_t (*write_rx_analog_filter_bandwidth)( rf_id_t *p_id, uint32_t bandwidth );
    int32_t (*write_tx_analog_filter_bandwidth)( rf_id_t *p_id, uint32_t bandwidth );

    /****** RFIC Rx FIR function pointers ******/
    int32_t (*read_rx_fir_config)( rf_id_t *p_id, uint8_t *p_num_taps, uint8_t *p_fir_decimation );
    int32_t (*read_rx_fir_coeffs)( rf_id_t *p_id, int16_t *p_coeffs );
    int32_t (*write_rx_fir_coeffs)( rf_id_t *p_id, int16_t *p_coeffs );
    int32_t (*read_rx_fir_gain)( rf_id_t *p_id, skiq_rx_fir_gain_t *p_gain );
    int32_t (*write_rx_fir_gain)( rf_id_t *p_id, skiq_rx_fir_gain_t gain );

    /****** RFIC Tx FIR function pointers ******/
    int32_t (*read_tx_fir_config)( rf_id_t *p_id, uint8_t *p_num_taps, uint8_t *p_fir_interpolation );
    int32_t (*read_tx_fir_coeffs)( rf_id_t *p_id, int16_t *p_coeffs );
    int32_t (*write_tx_fir_coeffs)( rf_id_t *p_id, int16_t *p_coeffs );
    int32_t (*read_tx_fir_gain)( rf_id_t *p_id, skiq_tx_fir_gain_t *p_gain );
    int32_t (*write_tx_fir_gain)( rf_id_t *p_id, skiq_tx_fir_gain_t gain );

} rfic_functions_t;

/** @brief An RFIC instance which contains the RFIC function pointers as well as the RF ID */
typedef struct
{
    rf_id_t *p_id;
    rfic_functions_t *p_funcs;
} rfic_t;

/*****************************************************************************/
/** The rfic_init_functions function initializes the RFIC function pointers based
    on the Sidekiq hardware type.

    @param[in] part part type of the Sidekiq
    @param[in] hw_rev hardware revision of the Sidekiq

    @return rfic_functions_t* pointer to the RFIC function pointers (dynamically allocated)
*/
rfic_functions_t* rfic_init_functions( skiq_part_t part,
                                       skiq_hw_rev_t hw_rev );

/*****************************************************************************/
/** The rfic_free_functions function frees the RFIC function pointers previously
    allocated with the init function

    @param[in] p_funcs pointer to RFIC functions to free
*/
void rfic_free_functions( rfic_functions_t *p_funcs );

/*****************************************************************************/
/** The rfic_add_rpc_functions function updates the RFIC function pointers with
    RPC capable functions if RPC is available

    @param[in] card card of function pointers to update
    @param[in] p_funcs pointer to RFIC functions to free

    @return int32_t
    @retval 0 Success
    @retval -ENOMEM memory allocation to store non-RPC functions failed
*/
int32_t rfic_add_rpc_functions( uint8_t card, rfic_functions_t *p_funcs );

/*****************************************************************************/
/** The rfic_remove_rpc_functions function updates the RFIC function pointers 
    to remove any previously replaced RPC functions with their non-RPC equivalent.

    @param[in] card card of function pointers to update
    @param[in] p_funcs pointer to RFIC functions to free

    @return int32_t
    @retval 0 Success
*/
int32_t rfic_remove_rpc_functions( uint8_t card, rfic_functions_t *p_funcs );


/*****************************************************************************/
/** The rfic_reset_and_init function is responsible for resetting and
    initializing the RF IC.

    @param[in] p_rfic pointer to the RF IC instance

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_reset_and_init( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_display_version_info function is responsible for displaying
    (printing) the version information related the the RF IC.

    @param[in] p_rfic pointer to the RF IC instance

    @return int32_t  status where 0=success, anything else is an error
*/
void rfic_display_version_info( rfic_functions_t *p_rfic_funcs );

/*****************************************************************************/
/** The rfic_release function is responsible for releasing the RF IC interface
    specified.

    @param[in] p_rfic pointer to the RF IC instance

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_release( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_write_rx_freq function is responsible for the tuning RX LO
    frequency requested for the RF port specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] freq requested RX LO frequency
    @param[out] p_act_freq pointer to where to store the actual LO

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_freq( rfic_t *p_rfic, uint64_t freq, double *p_act_freq );

/*****************************************************************************/
/** The rfic_write_tx_freq function is responsible for the tuning the TX LO
    frequency requested for the RF port specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] freq requested TX LO frequency
    @param[out] p_act_freq pointer to where to store the actual LO

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_freq( rfic_t *p_rfic, uint64_t freq, double *p_act_freq );

/*****************************************************************************/
/** The rfic_write_rx_spectrum_invert function is responsible for the
    configuring the RX spectrum invert parameter for the RF port specified in
    the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] spectrum_invert flag indicating whether the spectrum should
    be inverted

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_spectrum_invert( rfic_t *p_rfic, bool spectrum_invert );

/*****************************************************************************/
/** The rfic_write_tx_spectrum_invert function is responsible for the 
    configuring the TX spectrum invert parameter for the RF port specified in 
    the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] spectrum_invert flag indicating whether the spectrum should
    be inverted

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_spectrum_invert( rfic_t *p_rfic, bool spectrum_invert );

/*****************************************************************************/
/** The rfic_write_tx_attenuation_invert function is responsible for the 
    configuring the TX attenuation, in quarter dB steps, for the RF port
    specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] atten attenuation to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_attenuation( rfic_t *p_rfic, uint16_t atten );

/*****************************************************************************/
/** The rfic_read_tx_attenuation_invert function is responsible for reading
    the TX attenuation, in quarter dB steps, for the RF port specified in the
    RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_atten pointer to where to store the attenuation

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_attenuation( rfic_t *p_rfic, uint16_t *p_atten );

/*****************************************************************************/
/** The rfic_read_tx_attenuation_range function is responsible for reading
    the TX attenuation range, in quarter dB steps, for the RF port specified in
    the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_max pointer to where to store the max quarter dB attenuation
    @param[out] p_min pointer to where to store the min quarter dB attenuation

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_attenuation_range( rfic_t *p_rfic, uint16_t *p_max, uint16_t *p_min );

/*****************************************************************************/
/** The rfic_write_rx_gain_mode function is responsible for configuring the
    gain mode (auto or manual) for the RF port specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] gain_mode gain mode to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_gain_mode( rfic_t *p_rfic, skiq_rx_gain_t gain_mode );

/*****************************************************************************/
/** The rfic_read_rx_gain_mode function is responsible for reading the
    gain mode (auto or manual) for the RF port specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_gain_mode pointer to where to store the gain mode

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_gain_mode( rfic_t *p_rfic, skiq_rx_gain_t *p_gain_mode );

/*****************************************************************************/
/** The rfic_read_rx_gain_range is responsible for reading the viable gain
    index range for a given RFIC.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_gain_max pointer to where to store the max gain index
    @param[out] p_gain_min pointer to where to store the min gain index

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_gain_range( rfic_t *p_rfic,
                                 uint8_t *p_gain_max,
                                 uint8_t *p_gain_min );

/*****************************************************************************/
/** The rfic_read_rx_gain function is responsible for configuring the gain 
    setting for the RF port specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] gain value of gain to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_gain( rfic_t *p_rfic, uint8_t gain );

/*****************************************************************************/
/** The rfic_read_rx_gain function is responsible for reading the gain setting 
    for the RF port specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_gain pointer to where to store the gain

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_gain( rfic_t *p_rfic, uint8_t *p_gain );

/*****************************************************************************/
/** The rfic_read_adc_resolution function is responsible for reading the 
    ADC resolution (in # of bits) supported.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_adc_res pointer to where to store the ADC resolution

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_adc_resolution( rfic_t *p_rfic, uint8_t *p_adc_res );

/*****************************************************************************/
/** The rfic_read_dac_resolution function is responsible for reading the 
    DAC resolution (in # of bits) supported.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_dac_res pointer to where to store the DAC resolution

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_dac_resolution( rfic_t *p_rfic, uint8_t *p_dac_res );

/*****************************************************************************/
/** The rfic_read_warp_voltage function is responsible for reading the 
    current warp voltage.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_warp_voltage pointer to where to store the warp voltage

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_warp_voltage( rfic_t *p_rfic, uint16_t *p_warp_voltage );

/*****************************************************************************/
/** The rfic_write_warp_voltage function is responsible for configuring the 
    warp voltage.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] warp_voltage value to set the warp voltage

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_warp_voltage( rfic_t *p_rfic, uint16_t warp_voltage );

/*****************************************************************************/
/** The rfic_read_warp_voltage_extended_range() function is responsible for
    reading the current warp voltage with additional resolution (if available).

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_warp_voltage pointer to where to store the warp voltage

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_warp_voltage_extended_range( rfic_t *p_rfic,
                                               uint32_t *p_warp_voltage );

/*****************************************************************************/
/** The rfic_write_warp_voltage_extended_range() function is responsible for
    configuring the warp voltage with additional resolution (if available).

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] warp_voltage value to set the warp voltage

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_warp_voltage_extended_range( rfic_t *p_rfic,
                                                uint32_t warp_voltage );

/*****************************************************************************/
/** The rfic_enable_rx_chan function is responsible for configuring the 
    enable state of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] enable flag indicating if the channel enable state

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_enable_rx_chan( rfic_t *p_rfic, bool enable );

/*****************************************************************************/
/** The rfic_enable_tx_chan function is responsible for configuring the 
    enable state of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] enable flag indicating if the channel enable state

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_enable_tx_chan( rfic_t *p_rfic, bool enable );

/*****************************************************************************/
/** The rfic_config_tx_test_tone function is responsible for configuring the 
    TX test tone of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] enable flag indicating if the test tone should be enabled

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_config_tx_test_tone( rfic_t *p_rfic,
                                  bool enable );

/*****************************************************************************/
/** The rfic_read_tx_test_tone_freq function is responsible for returning the
    frequency offset of the TX test tone from LO.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_test_freq_offset pointer to the frequency of the TX test tone
    (offset from the LO frequency)

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_test_tone_freq( rfic_t *p_rfic,
                                     int32_t *p_test_freq_offset );

/*****************************************************************************/
/** The rfic_write_tx_test_tone_freq function is responsible for configuring the 
    TX test tone frequency offset of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] test_freq_offset frequency offset of test tone

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_test_tone_freq( rfic_t *p_rfic,
                                      int32_t test_freq_offset );


/*****************************************************************************/
/** The rfic_read_min_rx_sample_rate function is responsible for reading the
    minimum sample rate of the rx channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_min_rx_sample_rate pointer to where to store the minimum sample rate 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_min_rx_sample_rate( rfic_t *p_rfic, uint32_t *p_min_rx_sample_rate );

/*****************************************************************************/
/** The rfic_read_max_rx_sample_rate function is responsible for reading the
    maximum sample rate of the rx channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_max_rx_sample_rate pointer to where to store the maximum sample rate 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_max_rx_sample_rate( rfic_t *p_rfic, uint32_t *p_max_rx_sample_rate );

/*****************************************************************************/
/** The rfic_read_min_tx_sample_rate function is responsible for reading the
    minimum sample rate of the tx channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_min_tx_sample_rate pointer to where to store the minimum sample rate 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_min_tx_sample_rate( rfic_t *p_rfic, uint32_t *p_min_tx_sample_rate );

/*****************************************************************************/
/** The rfic_read_max_tx_sample_rate function is responsible for reading the
    maximum sample rate of the tx channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_max_tx_sample_rate pointer to where to store the maximum sample rate 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_max_tx_sample_rate( rfic_t *p_rfic, uint32_t *p_max_tx_sample_rate );

/*****************************************************************************/
/** The rfic_read_max_freq function is responsible for reading the maximum 
    LO frequency of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_min_sample_rate pointer to where to store the minimum sample rate 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_max_freq( rfic_t *p_rfic, uint32_t *p_max_freq );

/*****************************************************************************/
/** The rfic_read_min_freq function is responsible for reading the minimum 
    LO frequency of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_min_sample_rate pointer to where to store the minimum sample rate 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_min_freq( rfic_t *p_rfic, uint32_t *p_min_freq );

/*****************************************************************************/
/** The rfic_write_rx_sample_rate_and_bandwidth function is responsible for 
    configuring the RX sample rate and channel bandwidth of the channel specified
    in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] rate sample rate to configure
    @param[in] bandwidth channel bandwidth to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_sample_rate_and_bandwidth( rfic_t *p_rfic, uint32_t rate, uint32_t bandwidth );

/*****************************************************************************/
/** The rfic_write_rx_sample_rate_and_bandwidth_multi function is responsible for
    configuring the RX sample rate and channel bandwidth for the array of
    specified rx handles.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] handles [::skiq_rx_hdl_t] array of rx handles to be initialized
    @param[in] nr_handles number of rx handles defined in @p handles
    @param[in] rate array of sample rates corresponding to handles[]
    @param[in] bandwidth array of bandwidth values corresponding to handles[]

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_sample_rate_and_bandwidth_multi( rfic_t *p_rfic, skiq_rx_hdl_t handles[],
                                                       uint8_t nr_handles,
                                                       uint32_t rate[],
                                                       uint32_t bandwidth[]);

/*****************************************************************************/
/** The rfic_write_tx_sample_rate_and_bandwidth function is responsible for 
    configuring the TX sample rate and channel bandwidth of the channel specified
    in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] rate sample rate to configure
    @param[in] bandwidth channel bandwidth to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_sample_rate_and_bandwidth( rfic_t *p_rfic, uint32_t rate, uint32_t bandwidth );

/*****************************************************************************/
/** The rfic_read_rx_sample_rate function is responsible for querying  the 
    RX sample rate of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_rate pointer to where to store the requested sample rate
    @param[out] p_actual_rate pointer to where to store the actual sample rate

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_sample_rate( rfic_t *p_rfic, uint32_t *p_rate, double *p_actual_rate );

/*****************************************************************************/
/** The rfic_read_tx_sample_rate function is responsible for querying  the 
    TX sample rate of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_rate pointer to where to store the requested sample rate
    @param[out] p_actual_rate pointer to where to store the actual sample rate

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_sample_rate( rfic_t *p_rfic, uint32_t *p_rate, double *p_actual_rate );

/*****************************************************************************/
/** The rfic_read_rx_chan_bandwidth function is responsible for querying  the 
    RX channel bandwidth of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_bandwidth pointer to where to store the requested bandwidth
    @param[out] p_actual_bandwidth pointer to where to store the actual bandwidth

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_chan_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );

/*****************************************************************************/
/** The rfic_read_tx_chan_bandwidth function is responsible for querying  the 
    TX channel bandwidth of the channel specified in the RF ID.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_bandwidth pointer to where to store the requested bandwidth
    @param[out] p_actual_bandwidth pointer to where to store the actual bandwidth

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_chan_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );

/*****************************************************************************/
/** The rfic_init_gpio function initializes the GPIO pins on the RFIC as either
    inputs or outputs.
    
    Note: Not all bits in the mask may map to an actual GPIO pin.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] output_enable mask where bit set for output, bit clear for input

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_init_gpio( rfic_t *p_rfic, uint32_t output_enable );

/*****************************************************************************/
/** The rfic_init_gpio function writes the state of the GPIO pins on the RFIC.

    Note: Not all bits in the mask may map to an actual GPIO pin.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] value mask where bit set to drive high, bit clear to set low

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_write_gpio( rfic_t *p_rfic, uint32_t value );

/*****************************************************************************/
/** The rfic_read_gpio function reads the state of the GPIO pins on the RFIC.

    Note: Not all bits in the mask may map to an actual GPIO pin.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_value mask where bit set for high, bit clear for low

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_read_gpio( rfic_t *p_rfic, uint32_t *p_value );


/*****************************************************************************/
/** The rfic_read_rx_filter_overflow function reads the overflow state of the
    RX filters for the channel specified.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_rx_fir pointer to flag indicating if the RX FIR filter overflowed

    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_filter_overflow( rfic_t *p_rfic, bool *p_rx_fir );

/*****************************************************************************/
/** The rfic_read_tx_filter_overflow function reads the overflow state of the
    TX filters for the channel specified.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_int3 pointer to flag indicating if the TX INT3 filter overflowed
    @param[out] p_hb33 pointer to flag indicating if the TX HB3 filter overflowed
    @param[out] p_qec pointer to flag indicating if the TX QEC filter overflowed
    @param[out] p_hb1 pointer to flag indicating if the TX HB1 filter overflowed
    @param[out] p_tx_fir pointer to flag indicating if the TX FIR filter overflowed

    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_filter_overflow( rfic_t *p_rfic, bool *p_int3, bool *p_hb3,
                                      bool *p_hb2, bool *p_qec, bool *p_hb1, bool *p_tx_fir );

/*****************************************************************************/
/** The rfic_read_fpga_rf_clock function reads the clock frequency related to
    the RF being provided to the FPGA.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_freq pointer to frequency of the FPGA RF clock

    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_fpga_rf_clock( rfic_t *p_rfic, uint64_t *p_freq );

/*****************************************************************************/
/** The rfic_read_tx_quadcal_mode function reads the TX quadrature algorithm
    mode.

    @param[in] p_rfic pointer to the RF IC instance
    @param[out] p_mode pointer to the TX quadcal mode

    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_quadcal_mode( rfic_t *p_rfic, skiq_tx_quadcal_mode_t *p_mode );

/*****************************************************************************/
/** The rfic_write_tx_quadcal_mode function writes the TX quadrature algorithm
    mode.

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] mode pointer to the TX quadcal mode

    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_quadcal_mode( rfic_t *p_rfic, skiq_tx_quadcal_mode_t mode );

/*****************************************************************************/
/** The rfic_run_tx_quadcal_mode function runs the TX quadrature calibration
    algorithm.

    @param[in] p_rfic pointer to the RF IC instance

    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_run_tx_quadcal( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_read_rx_stream_hdl_conflict() function populates a list of 
    conflicting handles based on the handle specified.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_conflicting_hdls [::skiq_rx_hdl_t] array of handles that conflict
    @param[out] p_num_hdls pointer of where to store number of handles in conflict list
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_stream_hdl_conflict( rfic_t *p_rfic,
                                          skiq_rx_hdl_t *p_conflicting_hdls,
                                          uint8_t *p_num_hdls );

/*****************************************************************************/
/** The rfic_is_chan_enable_xport_dependent() function returns true if the 
    RFIC channel enable impacts how the channels are transported to the FPGA.

    @param[in] p_rfic pointer to the RFIC instance
    @return bool true if the channel enable impacts the transport 
*/
bool rfic_is_chan_enable_xport_dependent( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_read_control_output_rx_gain_config() function populates the mode
    and enable settings to configure the RFIC control output to in order for 
    the RX gain for the handle specified be presented on the control output.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_mode pointer to where to store the mode 
    @param[out] p_ena pointer to where to store the enable bitmask
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_control_output_rx_gain_config( rfic_t *p_rfic, uint8_t *p_mode, uint8_t *p_ena );

/*****************************************************************************/
/** The rfic_write_control_output_config() function configures the output
    control configuration to the mode and enable bitmask specified.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] mode mode to configure the RFIC to
    @param[in] ena enable bitmask to configure the RFIC control output to
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_control_output_config( rfic_t *p_rfic, uint8_t mode, uint8_t ena );

/*****************************************************************************/
/** The rfic_read_control_output_config() function populates the mode and 
    enable settings of the currently configured RFIC control output.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_mode pointer to where to store the mode 
    @param[out] p_ena pointer to where to store the enable bitmask
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_control_output_config( rfic_t *p_rfic, uint8_t *p_mode, uint8_t *p_ena );

/*****************************************************************************/
/** The rfic_init_from_file() function configures the RFIC from the file
    provided.

    @param[in] p_rfic pointer to the RFIC instance
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_init_from_file( rfic_t *p_rfic, FILE* p_file );

/*****************************************************************************/
/** The rfic_power_down_tx() function powers down the TX

    @param[in] p_rfic pointer to the RFIC instance
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_power_down_tx( rfic_t *p_rfic );


/*****************************************************************************/
/** The rfic_enable_pin_gain_ctrl() function enables manual gain and
    pin control of the RX gain

    @param[in] p_rfic pointer to the RFIC instance
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_enable_pin_gain_ctrl( rfic_t *p_rfic );
/*****************************************************************************/
/** The rfic_read_hop_on_ts_gpio() function reads the GPIO to use to perform
    frequency hopping.  Additionally, the chip_index to which this rfic belongs
    to is specified.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_chip_index pointer to where to store the value of the chip index
    @return uint32_t gpio used for frequency hopping
*/
uint32_t rfic_read_hop_on_ts_gpio( rfic_t *p_rfic, uint8_t *p_chip_index );

/*****************************************************************************/
/** The rfic_write_rx_freq_tune_mode() functions writes the RX frequency
    tune mode.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] mode tune mode to configure
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t mode );
/*****************************************************************************/
/** The rfic_read_rx_freq_tune_mode() functions reads the currently configured
    RX frequency tune mode.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_mode pointer to where to store the tune mode
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t *p_mode );

/*****************************************************************************/
/** The rfic_write_tx_freq_tune_mode() functions writes the TX frequency
    tune mode.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] mode tune mode to configure
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t mode );
/*****************************************************************************/
/** The rfic_read_tx_freq_tune_mode() functions reads the currently configured
    TX frequency tune mode.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_mode pointer to where to store the tune mode
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t *p_mode );

/*****************************************************************************/
/** The rfic_config_rx_hop_list() function configures the RX frequency 
    hopping list.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] num_freq number of hopping frequencies
    @param[in] freq_list list of hopping frequencies
    @param[in] initial_index initial index into hopping list
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_config_rx_hop_list( rfic_t *p_rfic, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index );
/*****************************************************************************/
/** The rfic_read_rx_hop_list() function returns the currently configured
    RX hopping list details.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_num_freq pointer to where to store number of hopping frequencies
    @param[out] freq_list list of hopping frequencies
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_hop_list( rfic_t *p_rfic, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );

/*****************************************************************************/
/** The rfic_config_tx_hop_list() function configures the TX frequency 
    hopping list.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] num_freq number of hopping frequencies
    @param[in] freq_list list of hopping frequencies
    @param[in] initial_index initial index into hopping list
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_config_tx_hop_list( rfic_t *p_rfic, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index );
/*****************************************************************************/
/** The rfic_read_rx_hop_list() function returns the currently configured
    RX hopping list details.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_num_freq pointer to where to store number of hopping frequencies
    @param[out] freq_list list of hopping frequencies
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_hop_list( rfic_t *p_rfic, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );

/*****************************************************************************/
/** The rfic_write_next_rx_hop() function configures the next frequency index
    to hop to.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] freq_index index into frequency hopping list
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_next_rx_hop( rfic_t *p_rfic, uint16_t freq_index );
/*****************************************************************************/
/** The rfic_write_next_tx_hop() function configures the next frequency index
    to hop to.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] freq_index index into frequency hopping list
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_next_tx_hop( rfic_t *p_rfic, uint16_t freq_index );

/*****************************************************************************/
/** The rfic_rx_hop() function performs the RX frequency hop.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] rf_timestamp RF timestamp to perform hop (in hop_on_timestamp)
    @param[out] p_act_freq  pointer to the actual frequency being set when
                            hopping to the next frequency
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_rx_hop( rfic_t *p_rfic, uint64_t rf_timestamp, double *p_act_freq );
/*****************************************************************************/
/** The rfic_tx_hop() function performs the TX frequency hop.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] rf_timestamp RF timestamp to perform hop (in hop_on_timestamp)
    @param[out] p_act_freq  pointer to the actual frequency being set when
                            hopping to the next frequency
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_tx_hop( rfic_t *p_rfic, uint64_t rf_timestamp, double *p_act_freq );

/*****************************************************************************/
/** The rfic_read_curr_rx_hop() reads the current RX frequency hop config.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_hop pointer to where to store the hop config
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_curr_rx_hop( rfic_t *p_rfic, freq_hop_t *p_hop );
/*****************************************************************************/
/** The rfic_read_next_rx_hop() reads the queued RX frequency hop config.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_hop pointer to where to store the hop config
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_next_rx_hop( rfic_t *p_rfic, freq_hop_t *p_hop );

/*****************************************************************************/
/** The rfic_read_curr_tx_hop() reads the current TX frequency hop config.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_hop pointer to where to store the hop config
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_curr_tx_hop( rfic_t *p_rfic, freq_hop_t *p_hop );

/*****************************************************************************/
/** The rfic_read_next_tx_hop() reads the queued TX frequency hop config.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_hop pointer to where to store the hop config
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_next_tx_hop( rfic_t *p_rfic, freq_hop_t *p_hop );    

/*****************************************************************************/
/** The rfic_read_rx_cal() reads the RX calibration mode config.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_mode to where to store the RX calibration mode
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_cal_mode( rfic_t *p_rfic, skiq_rx_cal_mode_t *p_mode );

/*****************************************************************************/
/** The rfic_write_rx_cal() writes the RX calibration mode config.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] mode RX calibration mode to configure
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_cal_mode( rfic_t *p_rfic, skiq_rx_cal_mode_t mode );

/*****************************************************************************/
/** The rfic_run_rx_cal() runs the RX calibration types enabled.

    @param[in] p_rfic pointer to the RFIC instance
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_run_rx_cal( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_read_rx_cal_mask() reads the calibration types mask.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_cal_mask pointer to where to store the calibration types
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_cal_mask( rfic_t *p_rfic, uint32_t *p_cal_mask );

/*****************************************************************************/
/** The rfic_write_rx_cal_mask() writes the calibration types mask.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] cal_mask mask of calibration types to enable
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_cal_mask( rfic_t *p_rfic, uint32_t cal_mask );

/*****************************************************************************/
/** The rfic_read_rx_cal_types_avail() reads the calibration types available.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_cal_mask pointer to where to store the mask of calibration 
    types available
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_cal_types_avail( rfic_t *p_rfic, uint32_t *p_cal_mask );

/*****************************************************************************/
/** The rfic_read_tx_enable_mode() reads the mode which is used to enable
 *  a Tx channel on the RFIC.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] p_mode pointer to where to store the selected mode
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t *p_mode );

/*****************************************************************************/
/** The rfic_read_rx_enable_mode() reads the mode which is used to enable
 *  a Rx channel on the RFIC.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] p_mode pointer to where to store the selected mode
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t *p_mode );

/*****************************************************************************/
/** The rfic_write_tx_enable_mode() sets the mode which is used to enable
    a Tx channel on the RFIC.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] mode desired mode for enabling Tx channels
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t mode );

/*****************************************************************************/
/** The rfic_write_rx_enable_mode() sets the mode which is used to enable
    a Rx channel on the RFIC.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] mode desired mode for enabling Rx channels
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t mode );

/*****************************************************************************/
/**
   @brief The rfic_read_temp() reads the temperature of the RFIC

   @param[in]      p_rfic pointer to the RFIC instance
   @param[out]     p_temp_in_deg_C pointer to where to store the signed 8-bit temperature in degrees Celsius

   @return 0 on success, else a negative errno value
*/
int32_t rfic_read_temp( rfic_t *p_rfic,
                        int8_t *p_temp_in_deg_C );


/**************************************************************************************************/
/**
   @brief The rfic_read_auxadc() reads the voltage from the specified channel of the RFIC's auxADC.

   @note It is the responsibility of the RFIC to convert any ADC readings into millivolts.

   @param[in]      p_rfic pointer to the RFIC instance
   @param[in]      channel ADC channel to query
   @param[out]     p_millivolts pointer to populate the reading in millivolts

   @return 0 on success, else a negative errno value
*/
int32_t rfic_read_auxadc( rfic_t *p_rfic,
                          uint8_t channel,
                          uint16_t *p_millivolts );

/*****************************************************************************/
/** The rfic_swap_rx_port_config function is responsible for swapping the 
    configuration of the "a_port" and "b_port".  So, upon successful
    completion of this function, the from_port is configured identical to the
    to_port prior to calling the function (and vice versa).

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] a_port RFIC port to be swapped with @pb_port
    @param[in] b_port RFIC port to be swapped with @pa_port

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_swap_rx_port_config( rfic_t *p_rfic, uint8_t a_port, uint8_t b_port );

/*****************************************************************************/
/** The rfic_swap_tx_port_config function is responsible for swapping the 
    configuration of the "a_port" and "b_port".  So, upon successful
    completion of this function, the from_port is configured identical to the
    to_port prior to calling the function (and visa versa).

    @param[in] p_rfic pointer to the RF IC instance
    @param[in] a_port RFIC port to be swapped with @pb_port
    @param[in] b_port RFIC port to be swapped with @pa_port

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfic_swap_tx_port_config( rfic_t *p_rfic, uint8_t a_port, uint8_t b_port );

/*****************************************************************************/
/** The rfic_read_rf_capabilities() reads the rf capabilities of the RFIC layer.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_rf_capabilities pointer to store rf_capabilities
    @return int32_t  status
    @retval 0  success
    @retval -ENOTSUP configuration not available for part
*/
int32_t rfic_read_rf_capabilities( rfic_t *p_rfic, skiq_rf_capabilities_t *p_rf_capabilities );

/*****************************************************************************/
/** The rfic_write_ext_clock_select() writes the external clock source from the
 *  RFIC layer.  Set true to use the FMC backplane clock source and false to
 *  use the front panel connector clock source.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] ext_clk_sel set the external clock source

    @return int32_t  status
    @retval 0  success
    @retval -ENOTSUP configuration not available for part
*/
int32_t rfic_write_ext_clock_select( rfic_t *p_rfic, bool ext_clk_sel );

/*****************************************************************************/
/** The rfic_rx_hdl_map_other() function considers an input handle and
 *  returns a (dependent) skiq_rx_hdl_t if there are any shared resources 
 *  between the two.  An example of this would be a shared LO or clock source.


    @param[in] p_rfic pointer to the RFIC instance
    @return skiq_rx_hdl_t of a dependent channel, otherwise skiq_rx_hdl_end if
    there is no dependence on other handles.
*/
skiq_rx_hdl_t rfic_rx_hdl_map_other( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_tx_hdl_map_other() function considers an input handle and
 *  returns a (dependent) skiq_tx_hdl_t if there are any shared resources 
 *  between the two.  An example of this would be a shared LO or clock source.


    @param[in] p_rfic pointer to the RFIC instance
    @return skiq_tx_hdl_t of a dependent channel, otherwise skiq_tx_hdl_end if
    there is no dependence on other handles.
            
*/
skiq_tx_hdl_t rfic_tx_hdl_map_other( rfic_t *p_rfic );

/*****************************************************************************/
/** The rfic_read_rx_analog_filter_bandwidth() reads the configured bandwidth
    of the RX analog filter.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_bandwidth pointer to where to store the actual bandwidth
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_rx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth );

/*****************************************************************************/
/** The rfic_read_tx_analog_filter_bandwidth() reads the configured bandwidth
    of the TX analog filter.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_bandwidth pointer to where to store the actual bandwidth
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_read_tx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth );

/*****************************************************************************/
/** The rfic_write_rx_analog_filter_bandwidth() writes the bandwidth of the RX 
    analog filter.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] bandwidth baseband bandwidth used to configure analog bandwidth
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_rx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t bandwidth );

/*****************************************************************************/
/** The rfic_write_tx_analog_filter_bandwidth() writes the bandwidth of the TX 
    analog filter.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] bandwidth baseband bandwidth used to configure analog bandwidth
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfic_write_tx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t bandwidth );

/*****************************************************************************/
/** The rfic_gpio_test_read() reads the GPIO from the RFIC.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_exp_value pointer to the expected value of the GPIO
    @return int32_t
    @retval 0 success
    @retval -EIO    GPIO value does not match the expected value 
*/
int32_t rfic_gpio_test_read( rfic_t *p_rfic, uint32_t *p_exp_value );

/*****************************************************************************/
/** This function reads the Tx FIR configuration from the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_num_taps Number of FIR filter taps
    @param[out] p_fir_interpolation Interpolation configuration of the Tx FIR

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_read_tx_fir_config( rfic_t *p_rfic,
                                 uint8_t *p_num_taps,
                                 uint8_t *p_fir_interpolation );

/*****************************************************************************/
/** This function reads the Tx FIR coefficients from the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_coeffs Array of signed 16-bit FIR coefficients

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_read_tx_fir_coeffs( rfic_t *p_rfic,
                                 int16_t *p_coeffs );

/*****************************************************************************/
/** This function writes the Tx FIR coefficients to the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] p_coeffs Array of signed 16-bit FIR coefficients

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_write_tx_fir_coeffs( rfic_t *p_rfic,
                                  int16_t *p_coeffs );

/*****************************************************************************/
/** This function reads the Rx FIR configuration from the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_num_taps Number of FIR filter taps
    @param[out] p_fir_decimation Decimation configuration of the Rx FIR

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_read_rx_fir_config( rfic_t *p_rfic,
                                 uint8_t *p_num_taps,
                                 uint8_t *p_fir_decimation );

/*****************************************************************************/
/** This function reads the Rx FIR coefficients from the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_coeffs Array of signed 16-bit FIR coefficients

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_read_rx_fir_coeffs( rfic_t *p_rfic,
                                 int16_t *p_coeffs );

/*****************************************************************************/
/** This function writes the Tx FIR coefficients to the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] p_coeffs Array of signed 16-bit FIR coefficients

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_write_rx_fir_coeffs( rfic_t *p_rfic,
                                  int16_t *p_coeffs );

/*****************************************************************************/
/** This function writes the Rx FIR gain to the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] gain Requested gain setting

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_write_rx_fir_gain( rfic_t *p_rfic,
                                skiq_rx_fir_gain_t gain );

/*****************************************************************************/
/** This function reads the Rx FIR gain from the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_gain reference to the current gain setting

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_read_rx_fir_gain( rfic_t *p_rfic,
                               skiq_rx_fir_gain_t *p_gain );

/*****************************************************************************/
/** This function writes the Tx FIR gain to the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[in] gain Requested gain setting

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_write_tx_fir_gain( rfic_t *p_rfic,
                                skiq_tx_fir_gain_t gain );

/*****************************************************************************/
/** This function reads the Tx FIR gain from the specified RFIC instance.

    @param[in] p_rfic pointer to the RFIC instance
    @param[out] p_gain reference to the current gain setting

    @return 0 on success, else a negative errno value
    @retval -ENOTSUP RFIC does not currently support this functionality
*/
int32_t rfic_read_tx_fir_gain( rfic_t *p_rfic,
                               skiq_tx_fir_gain_t *p_gain );

typedef struct
{
    uint32_t rate;
    uint32_t bandwidth;
    skiq_rx_hdl_t hdl;
} sr_bw_params_t;

#define SR_BW_PARAMS_T_INITIALIZER                  \
    {                                               \
        .rate = 0,                                  \
        .bandwidth = 0,                             \
        .hdl = skiq_rx_hdl_end,                     \
    }

#endif  /* __SIDEKIQ_RFIC_H__ */
