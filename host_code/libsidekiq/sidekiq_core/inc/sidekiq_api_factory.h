/*! \file sidekiq_api_factory.h
    \brief Private factory interface to libsidekiq.

    <pre>
    Copyright 2016 - 2020 Epiq Solutions, All Rights Reserved
    </pre>
*/

#ifndef __SIDEKIQ_API_FACTORY__
#define __SIDEKIQ_API_FACTORY__

/***** Includes *****/
#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup factoryfunctions Factory Functions and Definitions
 *
 * @brief These functions and definitions are related to using the Sidekiq SDR is a factory /
 * production setting and are @b NOT shared with customers or vendors.
 */

/***** Defines *****/

/** @addtogroup factoryfunctions
  * @{
  */

#define SKIQ_FACT_SERIAL_NUM_INVALID_STR ("NONE")
#define SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN (4)
// Legacy Serial numbers are stored as uint16_t, which has a max value of
// 65535. This is represented by 2 bytes and has a numeric length of 5.
#define SKIQ_FACT_SERIAL_NUM_LEGACY_BYTE_LEN (2)
#define SKIQ_FACT_SERIAL_NUM_LEGACY_LEN (5)
#define SKIQ_FACT_SERIAL_NUM_LEGACY_MAX (UINT16_MAX)
#define SKIQ_FACT_SERIAL_NUM_LEGACY_MIN (30000)
// Legacy serial numbers were stored as uint16_t, which has a max value of
// 65535. We need 5 characters plus the null character to fully represent that 
// as a string.
/** \deprecated Use SKIQ_SERIAL_NUM_STRLEN instead. */
#define SKIQ_FACT_SERIAL_NUM_STRING_MAX_LEN (SKIQ_SERIAL_NUM_STRLEN-1)
/** \deprecated Use SKIQ_SERIAL_NUM_STRLEN instead. */
#define SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM (SKIQ_SERIAL_NUM_STRLEN)

/**
    @brief  The maximum length (in bytes) of a Flash chip's manufacturing unique ID.

    @note   This should be at least as large as SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN (see
            sidekiq_flash_types.h) but is not defined to be the same as sidekiq_flash_types.h
            isn't an externally published header.
*/
#define SKIQ_FACT_FLASH_MAX_UNIQUE_ID_LEN   (32)

typedef enum
{
    skiq_rx_filter_config_1=1, /**< for X2 rev A/B/C variant 01 (with filters) */
    skiq_rx_filter_config_2=2, /**< for X2 rev A/B/C variant 02 (without filters) */
    skiq_rx_filter_config_3=3, /**< for X4 rev A/B/C variant 01 (with filters) */
    skiq_rx_filter_config_4=4, /**< for X4 rev A/B/C variant 02 (without filters) */
} skiq_rx_filter_config_t;

typedef enum
{
    skiq_led_color_red,
    skiq_led_color_green,
    skiq_led_color_blue
} skiq_led_color_t;

typedef enum
{
    skiq_pin_value_hiz = -1,
    skiq_pin_value_low = 0,
    skiq_pin_value_high = 1,
} skiq_pin_value_t;

/** @} */

/*****************************************************************************/
/***** Extern Functions *****/
/*****************************************************************************/


/**************************************************************************************************/
/** The skiq_fact_write_serial_string() function is responsible for writing the the serial number of
    a given Sidekiq to its onboard EEPROM.

    All future Sidekiq products should use a 4 character alphanumeric serial number.  Functionally,
    this is accomplished by setting p_serial_num to something like "7B12". This will be placed in
    the EEPROM at the memory location defined by VERSION_INFO_ALPHANUMERIC_ADDR, which is a 4 byte
    contiguous region of memory.
    
    For backwards compatibility, and in the event that an older serial number needs to be restored,
    it is possible to pass in a uint16_t value and have it written to the "legacy" serial number
    position that exists at the independent location of VERSION_INFO_LEGACY_ADDR, which is a 2 byte
    contiguous region of memory. In this case, p_serial_num should be assigned to something like
    "42001". Historically, legacy serial numbers were defined to exist between 30000 and UINT16_MAX
    (i.e. 65535). To prevent future actions going against the established order, legacy serial
    numbers are checked to ensure they fall within these bounds.

    When a serial number is written, to allow the end user to switch between legacy and alphanumeric
    serial number options, the serial number type that is being switched from is invalidated after
    successfully writing the new serial address.  This ensures that the expected serial number is the
    only valid serial number on the card.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] p_serial_num: pointer to string representation of the serial number
    @param[in] is_legacy: set to true if serial number to be written is a legacy serial number.

    @return int32_t: status where 0=success, anything else is an error
    @retval 0 Success
    @retval -3 invalid legacy serial number
    @retval -4 invalid character in alphanumeric serial number
    @retval -5 invalid serial option
    @retval -6 failed to write non-legacy serial number to EEPROM
    @retval -7 failed to write legacy serial number to EEPROM
    @retval -8 failed to invalidate previous serial number when writing a new one
*/
extern int32_t skiq_fact_write_serial_string(uint8_t card, 
                                             char* p_serial_num,
                                             bool is_legacy);

/**************************************************************************************************/
/** The skiq_fact_write_ref_clock() function is responsible for writing the the reference clock
    configuration.  It updates the hardware configuration as well as stores the reference clock
    setting in EEPROM.

    @attention In order for the reference clock update to completely take effect, skiq_exit()
    followed by skiq_init() must be completed.  This setting cannot be updated on the fly.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] ref_clock the value to configure the reference clock to

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_ref_clock( uint8_t card, skiq_ref_clock_select_t ref_clock );


/**************************************************************************************************/
/** The skiq_fact_clear_firmware_stamp() function clears the firmware version that is stamped in the
    unit's EEPROM.  This should only be used by the factory and in cases where the factory is
    downgrading the FX2 firmware to a version previous to 2.0.  This function removes the stored
    firmware version so as not to confused software and the user when the firmware version is
    reported over PCIe.

    @param[in] card card number of the Sidekiq of interest

    @return int32_t
    @retval 0 successfully cleared firmware version stamp
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval other Internal error from hal_write_eeprom()
 */
extern int32_t skiq_fact_clear_firmware_stamp( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_write_ext_ref_clock_freq() stores the frequency of the external reference clock in
    EEPROM. This function should be used in conjunction with skiq_fact_write_ref_clock() to
    configure a Sidekiq for an external reference.
    
    @note Only Sidekiq mPCIe (::skiq_mpcie), Sidekiq M.2 (::skiq_m2), and Sidekiq M.2-2280
    (::skiq_m2_2280) product types are supported.
    
    @attention Only 40 MHz and 30.72 MHz are supported currently.

    @attention In order for the reference clock frequency to be fully completed, skiq_exit()
    followed by skiq_init() must be completed.  This setting cannot be updated on the fly.

    @see skiq_fact_write_ref_clock

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] freq the external reference clock's frequency

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_ext_ref_clock_freq( uint8_t card,
                                                   uint32_t freq );

/**************************************************************************************************/
/** skiq_fact_write_product_and_hw_vers() is responsible for configuring both the product ID and
    hardware revision in EEPROM.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hw_vers hardware version/revision to configure
    @param[in] product product ID to configure

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_product_and_hw_vers( uint8_t card, 
                                                    skiq_hw_vers_t hw_vers,
                                                    skiq_product_t product );

/*************************************************************************************************/
/** The skiq_fact_write_default_tcvcxo_warp_voltage() function is responsible for storing the
    default value of the warp voltage.  This default value is determined during factory calibration
    and is accessible to customers read-only.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] warp_voltage warp voltage value to be written.

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_default_tcvcxo_warp_voltage( uint8_t card,
                                                            uint16_t warp_voltage );

/**************************************************************************************************/
/** The skiq_fact_write_gpio_tristate() function configures the GPIO pins to function as either
    inputs or outputs. If the corresponding bit of the pin is set high in the mask, it will be
    configured as an output. Otherwise, it will be left as an input.
    
    @ingroup factoryfunctions

    @see skiq_fact_read_gpio_tristate
    @see skiq_fact_write_gpio_state
    @see skiq_fact_read_gpio_state

    @param[in] card card number of the Sidekiq of interest
    @param[in] tristate bit mask of pin input and output configuration

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_gpio_tristate( uint8_t card, uint32_t tristate );

/**************************************************************************************************/
/** The skiq_fact_read_gpio_tristate() function reads the current pin configuration of the GPIO
    pins. If the bit is set high, it is currently configured as an output. Otherwise it is an input.
    
    @ingroup factoryfunctions

    @see skiq_fact_write_gpio_tristate
    @see skiq_fact_write_gpio_state
    @see skiq_fact_read_gpio_state

    @param[in] card card number of the Sidekiq of interest
    @param[out] p_tristate pointer to receive tristate status

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_gpio_tristate( uint8_t card, uint32_t* p_tristate );

/**************************************************************************************************/
/** The skiq_fact_write_gpio_state function() writes the current state of the GPIO pins. If the bit
    is set, the pin will be driven high. Otherwise it will be driven low.
    
    @ingroup factoryfunctions

    @see skiq_fact_write_gpio_tristate
    @see skiq_fact_read_gpio_tristate
    @see skiq_fact_read_gpio_state

    @param[in] card card number of the Sidekiq of interest
    @param[in] state desired value of logic state
    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_gpio_state( uint8_t card, uint32_t state );

/**************************************************************************************************/
/** The skiq_fact_read_gpio_state() function reads the current state of the GPIO pins. If the bit is
    set high, it is currently at logic high. Otherwise it is at logic low.
    
    @ingroup factoryfunctions

    @see skiq_fact_read_gpio_tristate
    @see skiq_fact_write_gpio_state
    @see skiq_fact_read_gpio_state

    @param[in] card card number of the Sidekiq of interest
    @param[out] p_state pointer to receive logic status

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_gpio_state( uint8_t card,
                                          uint32_t* p_state );

/**************************************************************************************************/
/** The skiq_fact_test_gpif() function is used to verify to integrity of the GPIF connection.
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] is_fpga_write set to true if FPGA is to output of write value, set to false if the FX2 is to output the data value.
    @param[in] data_write the value to output on the GPIF lines
    @param[out] p_data_read pointer to hold value read back on GPIF lines

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_test_gpif( uint8_t card,
                                    bool is_fpga_write,
                                    uint16_t data_write,
                                    uint16_t* p_data_read );
/**************************************************************************************************/
/** The skiq_fact_test_rfic_ctrl() function is used to verify integrity of the RFIC_CTRL_IN_(0..3) pins 
    between the RFIC and the FPGA. The AD9361 has a mode which uses these pins to adjust the gain values
    directly, instead of requiring a SPI transaction.  This test configures the RFIC for this gain control mode
    and attempts to increment and decrement the gain. By doing so, each pin is validated.

    @attention This is only currently supported on Sidekiq cards with an AD9361. mPCIe and m2.
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    
    @return int32_t  status where 0=success, anything else is an error
*/


extern int32_t skiq_fact_test_rfic_ctrl( uint8_t card );

/**************************************************************************************************/
/** The skiq_fact_test_rfic_gpio() function is used to verify to integrity of the GPIO pins shared
    between the RFIC and the FPGA.
    
    @attention This is only currently supported on Sidekiq X2 (::skiq_x2) and Sidekiq X4 (::skiq_x4)
    
    @attention Only FPGA reads are supported (@a is_fpga_write = @a false).
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] rfic_sel RFIC GPIO to test 0=A, 1=B, etc
    @param[in] is_fpga_write set to true if FPGA is to output of write value, set to false if the RFIC is to output the data value.
    @param[in] data_write the value to output on the GPIF lines
    @param[out] p_data_read pointer to hold value read back on the GPIO

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_test_rfic_gpio( uint8_t card,
                                         uint8_t rfic_sel,
                                         bool is_fpga_write,
                                         uint32_t data_write,
                                         uint32_t* p_data_read );

/**************************************************************************************************/
/** The skiq_fact_reset_rfic() function is used to reset the RFIC on a Sidekiq card.
    
    @attention This is only supported on Sidekiq mPCIe (::skiq_mpcie), Sidekiq M.2 (::skiq_m2),
    Sidekiq Z2 (::skiq_z2), Sidekiq M.2-2280 (::skiq_m2_2280), Sidekiq Z2p (::skiq_z2p), 
    Sidekiq Z3u (::skiq_z3u)

    @param[in] card card number of the Sidekiq of interest

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_reset_rfic( uint8_t card );

/**************************************************************************************************/
/** The skiq_fact_write_io_expander() function is used to write to the I/O expander.
    
    @warning This function will only work with MPCIE rev C Sidekiq cards (::skiq_mpcie) over PCI;
    skiq_init() should be called with ::skiq_xport_type_pcie in order for this function to work as
    intended.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] reg register address of IO expander to target
    @param[in] data value to be written

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_io_expander( uint8_t card,
                                            uint8_t reg,
                                            uint8_t data );

/**************************************************************************************************/
/** The skiq_fact_read_io_expander() function is used to read from the I/O expander.
    
    @warning This function will only work with MPCIE rev C Sidekiq cards (::skiq_mpcie) over PCI;
    skiq_init() should be called with ::skiq_xport_type_pcie in order for this function to work as
    intended.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] reg register address of I/O expander to target
    @param[out] p_data pointer to be updated with data read back

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_io_expander( uint8_t card,
                                           uint8_t reg,
                                           uint8_t* p_data );

/**************************************************************************************************/
/** The skiq_fact_save_golden_fpga_to_flash() function stores a recovery (a.k.a. golden) FPGA
    bitstream into flash memory, allowing it to be automatically loaded upon reboot or loading from
    flash if no user FPGA bitstream is present.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] p_file pointer to the FILE containing the FPGA bitstream

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_save_golden_fpga_to_flash( uint8_t card, FILE* p_file );

/**************************************************************************************************/
/** The skiq_fact_verify_golden_fpga_from_flash() function verifies the contents of flash memory at
    the golden FPGA offset against a given file. This can be used to validate that a given FPGA
    bitstream is accurately stored within flash memory at the golden FPGA offset.

    @since Function added in API @b v4.7.0

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] p_file pointer to the FILE containing the golden FPGA bitstream to verify

    @return int32_t  verification status where 0=match, anything else indicates
            that the file did not match flash contents
*/
extern int32_t skiq_fact_verify_golden_fpga_from_flash( uint8_t card,
                                                        FILE* p_file );

/**************************************************************************************************/
/** The skiq_fact_rf_power_state() is used to control power application to the RF portion of the
    Sidekiq card.

    @attention Currently, this function supports powering down the RF hardware (@a power_enable = @a
    false). Attempting to apply power (@a power_enable = @a true) will do nothing and simply return
    an error code.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] power_enable set to "true" to supply power, "false" to power down

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_rf_power_state( uint8_t card,
                                         bool power_enable );

/**************************************************************************************************/
/** skiq_fact_read_flash() is used to read data from flash memory.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] address location in memory to begin read from
    @param[in] len number of bytes to read from flash
    @param[out] p_data pointer to memory location to store flash data

    @return int32_t status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_flash( uint8_t card,
                                     uint32_t address,
                                     uint32_t len,
                                     uint8_t* p_data );

/**************************************************************************************************/
/** The skiq_fact_read_fpga_temp() function is responsible for reading and providing the current
    temperature of the die of the FPGA (in degrees C).
    
    @attention This feature is only supported for Sidekiq M.2 (::skiq_m2), Sidekiq X2 (::skiq_x2),
    Sidekiq X4 (::skiq_x4), Sidekiq M.2-2280 (::skiq_m2_2280), Sidekiq Z2p (::skiq_z2p), 
    Sidekiq Z3u (::skiq_z3u) parts.
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[out] p_temp_in_deg_C a pointer to where the current temp should be written

    @return 0 on success, else a negative errno value
    @retval -EAGAIN Temperature sensor measurement is temporarily not available, try again later
    @retval -ENODEV Temperature sensor not available in present ::skiq_xport_init_level_t, try ::skiq_xport_init_level_full
    @retval -ENOSYS Loaded FPGA bitstream does not support measuring its temperature
    @retval -EIO I/O communication error occurred during measurement
    @retval -ENOTSUP No sensors for associated Sidekiq product
*/
extern int32_t skiq_fact_read_fpga_temp( uint8_t card,
                                         int8_t* p_temp_in_deg_C );

/**************************************************************************************************/
/** The skiq_fact_read_temp() function is responsible for reading and providing the current
    temperature of the specified sensor (in degrees C).

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] sensor sensor index of interest
    @param[out] p_temp_in_deg_C a pointer to where the current temp should be written

    @return 0 on success, else a negative errno value
    @retval -EAGAIN Temperature sensor measurement is temporarily not available, try again later
    @retval -ENODEV Temperature sensor not available in present ::skiq_xport_init_level_t, try ::skiq_xport_init_level_full
    @retval -EINVAL No supported sensor found at index
    @retval -EIO I/O communication error occurred during measurement
    @retval -ENOTSUP No sensors for associated Sidekiq product
*/
extern int32_t skiq_fact_read_temp_sensor( uint8_t card,
                                           uint8_t sensor,
                                           int8_t* p_temp_in_deg_C );

/**************************************************************************************************/
/** The skiq_fact_card_mgr_destroy() function is responsible for destroying the cached version of
    the initialized cards by card_mgr.  skiq_init() must be called after this function has been
    completed to re-initialize a probe for available cards.
    
    @ingroup factoryfunctions

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_card_mgr_destroy(void);

/**************************************************************************************************/
/** The skiq_fact_wipe_part_info() function is responsible for erasing the part information stored
    in the EEPROM.
    
    @param[in] card card number of the Sidekiq of interest
    @param[in] p_part_number pointer to the part number of actual part (used for EEPROM WP)
    @param[in] p_revision pointer to the revision of the actual part (used for EEPROM WP)

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_wipe_part_info( uint8_t card,
                                         char *p_part_number,
                                         char *p_revision );
    
/**************************************************************************************************/
/** The skiq_fact_write_part_info() function is responsible for writing the part information to
    EEPROM.
    
    @attention This feature is @b NOT supported for Sidekiq mPCIe (::skiq_mpcie) nor Sidekiq M.2
    (::skiq_m2) parts
    
    @param[in] card card number of the Sidekiq of interest
    @param[in] p_part_number pointer to the part number to store
    @param[in] p_revision pointer to the revision to store
    @param[in] p_variant pointer to the variant to store
    @param[in] p_serial pointer to string representation of the serial number

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_part_info( uint8_t card,
                                          char *p_part_number,
                                          char *p_revision,
                                          char *p_variant,
                                          char *p_serial );

/**************************************************************************************************/
/** The skiq_fact_write_rx_preselect_filters() function is responsible for writing the RX filter
    availability to EEPROM.
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] RX handle of the filter configuration
    @param[in] config [::skiq_rx_filter_config_t] filter configuration type

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_rx_preselect_filter_config( uint8_t card,
                                                           skiq_rx_hdl_t hdl,
                                                           skiq_rx_filter_config_t config );

/**************************************************************************************************/
/** The skiq_fact_read_rx_preselect_filters() function is responsible for reading the stored RX
    filter configuration
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] RX handle of the filter configuration
    @param[out] p_config [::skiq_rx_filter_config_t *] pointer to where to store the filter configuration type

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_rx_preselect_filter_config( uint8_t card,
                                                          skiq_rx_hdl_t hdl,
                                                          skiq_rx_filter_config_t *p_config );

/**************************************************************************************************/
/** The skiq_fact_write_usb_enumeration_delay() function sets the FX2 to delay USB enumeration by
    the requested number of milliseconds.
    
    @note A delay value of 65535 (0xFFFF) will be treated as having no delay.  This is to allow
    units that do not have this value set to function as normal due to EEPROM defaulting to 0xFF for
    unprogrammed values.
    
    @attention The delay will only be applied if running firmware @b v2.7 or later.
    
    @note This function will return an error if called on a unit that does not have an FX2.
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] delay_ms total enumeration delay in milliseconds

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_usb_enumeration_delay( uint8_t card,
                                                      uint16_t delay_ms );

/**************************************************************************************************/
/** The skiq_fact_write_led() function sets the LED color and state passed on the parameters passed
    in.
    
    @attention Only available for Sidekiq X2 (::skiq_x2) and Sidekiq X4 (::skiq_x4) parts
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] color [::skiq_led_color_t] LED color to configure
    @param[in] enable flag to enable or disable the LED state

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_led( uint8_t card,
                                    skiq_led_color_t color,
                                    bool enable );

/**************************************************************************************************/
/** The skiq_fact_write_tx_pa() function sets the PA state of the transmit handle specified.
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] TX handle of interest
    @param[in] enable flag to enable or disable the PA

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_tx_pa( uint8_t card,
                                      skiq_tx_hdl_t hdl,
                                      bool enable );


/**************************************************************************************************/
/** The skiq_fact_override_tx_pa() function allows the caller to override the PA state of the
    specified transmit handle by specifying a pin value (high, low, or high-z).  Availability, pre/post
    conditions specific to the actual hardware implementation.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] tx_hdl [::skiq_tx_hdl_t] TX handle of interest
    @param[in] pin_value [::skiq_pin_value_t] desired pin value for TX_AMP_MANUAL_BIAS_ON_N

    @return int32_t
    @retval 0 Success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EDOM transmit handle is out of range
    @retval -ENOTSUP Card index references a Sidekiq part that does not support this feature
    @retval -ENOTSUP ATE_SUPPORT is not enabled for the library
    @retval -EINVAL Specified @a pin_value is unknown or unsupported
    @retval -EIO Input/output error communicating with the I/O expander
*/
extern int32_t skiq_fact_override_tx_pa( uint8_t card,
                                         skiq_tx_hdl_t tx_hdl,
                                         skiq_pin_value_t pin_value );


/**************************************************************************************************/
/** The skiq_fact_write_rx_lna() function sets the LNA of the receive handle specified.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] RX handle of interest
    @param[in] enable flag to enable or disable the LNA

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_rx_lna( uint8_t card,
                                       skiq_rx_hdl_t hdl,
                                       bool enable );

/**************************************************************************************************/
/** skiq_fact_bypass_rx_lna() bypasses the LNA that is associated with the specified receive handle.

    @attention Bypassing the LNA will disable the LNA first before bypassing

    @note Explicitly calling skiq_fact_write_rx_lna() will take the LNA out of bypass mode

    @attention Calling other libsidekiq API functions (for example, changing the RX LO frequency)
    after calling this function may change the state of the LNA without notice.

    @ingroup factoryfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] rx_hdl [::skiq_rx_hdl_t] receive handle associated with the LNA

    @return int32_t
    @retval 0 successful LNA bypass
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EDOM receive handle is out of range
    @retval -ENOTSUP specified RX handle's LNA cannot be bypassed for the associated card / part
 */
extern int32_t skiq_fact_bypass_rx_lna( uint8_t card,
                                        skiq_rx_hdl_t rx_hdl );

/**************************************************************************************************/
/** The skiq_fact_read_current_voltage() function reads the current and voltage measurement of the
    card.

    @attention Only available on Sidekiq X2 (::skiq_x2) and Sidekiq X4 (::skiq_x4)

    X2 has a single sensor:
    - index 0 is the INA219 (U62) with I2C address 0x40

    X4 has two power measurement sensors:
    - index 0 is the INA219 (U32) with I2C address 0x40
    - index 1 is the INA219 (U12) with I2C address 0x41

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] sensor index of the sensor of interest
    @param[out] p_current pointer to where to store the current value (in mA)
    @param[out] p_voltage pointer to where to store the voltage (in V)

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -ENOTSUP Sidekiq product does not have any power measurement sensors
    @retval -EINVAL Sensor index is out of range
    @retval -EIO I/O communication error occurred during measurement
*/
extern int32_t skiq_fact_read_current_voltage( uint8_t card,
                                               uint8_t sensor,
                                               float *p_current,
                                               float *p_voltage );

/**************************************************************************************************/
/** The skiq_fact_write_mix_freq() configures the intermediate frequency used for mixing in order to
    transmit and receive beyond the default tuning range of the RFIC.
    
    @attention available only with Sidekiq X2 (::skiq_x2)
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] freq the desired intermediate frequency to use for mixing

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_write_mix_freq( uint8_t card,
                                         uint64_t freq );

/**************************************************************************************************/
/** The skiq_fact_read_mix_freq() reads back the intermediate frequency used for mixing in order to
    transmit and receive beyond the default tuning range of the RFIC.
    
    @attention available only with Sidekiq X2 (::skiq_x2)
    
    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[out] p_freq the current intermediate frequency to use for mixing

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_mix_freq( uint8_t card,
                                        uint64_t *p_freq );

/**************************************************************************************************/
/** The skiq_fact_read_genuine_golden_fpga_present_in_flash() function is responsible for
    determining if there is a valid and genuine golden image stored in flash.  The @a p_present is
    set based on whether the correct golden FPGA image is detected.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[out] p_present pointer to where to store whether the golden image is present

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_read_genuine_golden_fpga_present_in_flash( uint8_t card,
                                                                    uint8_t *p_present );

/**************************************************************************************************/
/** The skiq_fact_enable_tx_samples() function is for repeatedly streaming the sample buffer on the
    handle specified until told to stop via the skiq_fact_disable_tx_samples() function.  It is
    expected that the LO frequency and attenuation is configured prior to calling this function.
    Channel mode, block size, and transfer mode are configured internal to this function.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] handle to stream the samples on
    @param[in] samples pointer to the sample buffer
    @param[in] sample_len number of I/Q sample pairs included in sample buffer

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_enable_tx_samples( uint8_t card,
                                            skiq_tx_hdl_t hdl,
                                            int16_t* samples,
                                            uint32_t sample_len );

/**************************************************************************************************/
/** The skiq_fact_disable_tx_samples() function is for stopping the TX streaming previously enabled
    by skiq_fact_enable_tx_samples();

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] handle to stop streaming samples

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_disable_tx_samples( uint8_t card,
                                             skiq_tx_hdl_t hdl );


/**************************************************************************************************/
/** The skiq_fact_init_for_config() function is for initializing the appropriate xport layers and
    HAL functions without a valid product, hardware, or serial number detected.  This should be used
    only to bypass the validation in the case of configuring a fresh board.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_init_for_config( uint8_t card );

/**************************************************************************************************/
/** The skiq_fact_exit_for_config() function is for cleaning up the various functions previously
    initialize from skiq_fact_init_for_config().

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest

    @return int32_t  status where 0=success, anything else is an error
*/
extern int32_t skiq_fact_exit_for_config( uint8_t card );

/**************************************************************************************************/
/** The skiq_fact_calibration_clear() function completely clears and frees the
    local calibration cache for a given card index.

    @param[in] card card number of the Sidekiq of interest

    @return int32_t  status where 0=success, anything else is an error
 */
extern int32_t skiq_fact_calibration_clear( uint8_t card );

/**************************************************************************************************/
/** The skiq_fact_calibration_add_rx_by_file() function takes an open file handle and attempts to
    parse its contents into a valid pair of receive calibration tables (ref and gain).  If
    successful, the calibration table is added to the card's local calibration cache.  At that
    point, it may be consulted through gain offset API calls or committed to NVM (using
    skiq_fact_calibration_commit()).

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle associated with file handle
    @param[in] port [::skiq_rf_port_t] RF port associated with file handle
    @param[in] fp file handle referencing calibration data

    @return int32_t  status where 0=success, anything else is an error
 */
extern int32_t skiq_fact_calibration_add_rx_by_file( uint8_t card,
                                                     skiq_rx_hdl_t hdl,
                                                     skiq_rf_port_t port,
                                                     FILE* fp );

/**************************************************************************************************/
/** The skiq_fact_calibration_add_iq_complex_by_file() function takes an open file handle and
    attempts to parse its contents into a valid pair of IQ phase and amplitude calibration tables
    (re_cal and im_cal).  If successful, the calibration table is added to the card's local
    calibration cache.  At that point, it may be consulted through IQ correction API calls or
    committed to NVM (using skiq_fact_calibration_commit()).

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle associated with file handle
    @param[in] fp file handle referencing calibration data

    @return int32_t  status where 0=success, anything else is an error
 */
extern int32_t skiq_fact_calibration_add_iq_complex_by_file( uint8_t card,
                                                             skiq_rx_hdl_t hdl,
                                                             FILE* fp );

/**************************************************************************************************/
/** The skiq_fact_calibration_commit() function takes the locally cached calibration data for a
    given card and stores it in the associated non-volatile memory.

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest

    @return int32_t  status where 0=success, anything else is an error
 */
extern int32_t skiq_fact_calibration_commit( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_prbs_run_test_multi() runs PRBS test on all cards specified on as many handles as
    available.

    @attention The caller is responsible for configure the desired sample rates and bandwidths for
    the specified cards @b PRIOR to calling this function.

    @warning This function will only work for Sidekiq cards based on the AD9361 or AD9364 chipsets

    @note For the @a nr_handles argument, the caller can use @a 0 to indicate an automatic choice of
    1 or 2 receive handles depending on the card's availability.  Calling with @a nr_handles as @a 1
    will force all cards to test with ::skiq_rx_hdl_A1 ONLY.  Calling with @a nr_handles as @a 2
    will force all cards to test with ::skiq_rx_hdl_A1 and ::skiq_rx_hdl_A2 and will return an error
    if one of the cards does not support two receive channels.

    @see skiq_fact_prbs_cancel_test
    @see skiq_fact_prbs_run_test_single

    @ingroup factoryfunctions

    @param[in] cards array of uint8_t values that hold card indices of interest
    @param[in] nr_cards number of valid entries in @a cards, not to exceed SKIQ_MAX_NUM_CARDS
    @param[in] sample_rate desired sample rate to perform PRBS test
    @param[in] nr_handles_requested desired number of receive handles to perform PRBS test
    @param[in] nr_seconds_requested number of seconds to poll for PRBS errors
    @param[out] prbs_errors array of uint32_t values that reflect the number of PRBS errors detected for a given card index (and not its order in @a cards)

    @return int32_t
    @retval 0 successful PRBS test execution
    @retval -EIO failure to configure the associated Sidekiq card
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EINVAL nr_cards is 0 or >= SKIQ_MAX_NUM_CARDS
    @retval -EINVAL nr_handles is 2 and at least one specified card does not support two receive handles
    @retval -EBUSY PRBS test is already running
    @retval -ECANCELED PRBS test was canceled
    @retval -ENOLCK at least one sample rate never achieved PRBS mode lock
    @retval -EFAULT at least one sample rate had PRBS errors (number of errors returned in p_prbs_errors
    @retval -ENOTSUP at least one specified card index references a Sidekiq part that does not support PRBS testing
 */
extern int32_t skiq_fact_prbs_run_test_multi( uint8_t cards[],
                                              uint8_t nr_cards,
                                              uint32_t sample_rate,
                                              uint8_t nr_handles_requested,
                                              uint32_t nr_seconds_requested,
                                              uint32_t prbs_errors[] );


/**************************************************************************************************/
/** skiq_fact_prbs_run_test_single() runs PRBS test on a single specified card on as many handles as
    available.

    @attention The caller is responsible for configure the desired sample rate and bandwidth for the
    specified card @b PRIOR to calling this function.

    @warning This function will only work for Sidekiq cards based on the AD9361 or AD9364 chipsets

    @see skiq_fact_prbs_cancel_test
    @see skiq_fact_prbs_run_test_multi

    @ingroup factoryfunctions

    @param[in] card card index of interest
    @param[in] sample_rate desired sample rate to perform PRBS test
    @param[in] nr_seconds_requested  number of seconds to poll for PRBS errors
    @param[out] p_prbs_errors reference to a uint32_t value that reflect the number of PRBS errors detected

    @return int32_t
    @retval 0 successful PRBS test execution
    @retval -EIO failure to configure the associated Sidekiq card
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EINVAL nr_cards is 0 or >= SKIQ_MAX_NUM_CARDS
    @retval -EBUSY PRBS test is already running
    @retval -ECANCELED PRBS test was canceled
    @retval -ENOLCK at least one sample rate never achieved PRBS mode lock
    @retval -EFAULT at least one sample rate had PRBS errors (number of errors returned in p_prbs_errors
    @retval -ENOTSUP specified card index references a Sidekiq part that does not support PRBS testing
 */
extern int32_t skiq_fact_prbs_run_test_single( uint8_t card,
                                               uint32_t sample_rate,
                                               uint32_t nr_seconds_requested,
                                               uint32_t *p_prbs_errors );


/**************************************************************************************************/
/** The skiq_fact_prbs_cancel_test() function cancels an ongoing PRBS test.  Calling this function
    WITHOUT a running PRBS test will effectively cancel the next PRBS test execution.

    @ingroup factoryfunctions

    @return void
 */
extern void skiq_fact_prbs_cancel_test( void );

/**************************************************************************************************/
/** skiq_fact_i2c_enable_external_bus_access() enables usage of the external I2C bus.

    @attention Supported on Sidekiq Z2 rev B ONLY and is available through the Z2 edge connector

    @ingroup factoryfunctions

    @param[in] card card index of the Sidekiq of interest

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE Specified card index is out of range
    @retval -EACCES Failed to disable external bus access
    @retval -ENOTSUP card /part at specified card index does not support external I2C bus control
 */
extern int32_t skiq_fact_i2c_enable_external_bus_access( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_i2c_disable_external_bus_access() disables usage of the external I2C bus.

    @attention Supported on Sidekiq Z2 rev B ONLY and is available through the Z2 edge connector

    @ingroup factoryfunctions

    @param[in] card card index of the Sidekiq of interest

    @return int32_t
    @retval 0 successful
    @retval -ENODEV Specified card has not been initialized
    @retval -ERANGE Specified card index is out of range
    @retval -EACCES Failed to disable external bus access
    @retval -ENOTSUP card /part at specified card index does not support external I2C bus control
 */
extern int32_t skiq_fact_i2c_disable_external_bus_access( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_i2c_write_before_read() is responsible for performing a read operation that first
    requires a write to set the address that needs to be read.  For example: in order to perform a
    register read in many I2C slave devices, it is often necessary to first write down the register
    address to be read.  So a write operation happens first, followed by a read operation.  This
    function combines them into a single function for easier use.

    @note The @a bus argument specifies the bus to use for I2C transaction.  On Sidekiq Z2
    (::skiq_z2), a bus of @a 1 targets the external (off-board) I2C bus.  On Sidekiq M.2-2280
    (::skiq_m2_2280), Sidekiq Z2p (::skiq_z2p), and Sidekiq Z3u (::skiq_z3u), a bus of @a 1 targets 
    the secondary I2C bus that controls its RFE.

    @ingroup factoryfunctions

    @see skiq_fact_i2c_write
    @see skiq_fact_i2c_read

    @param[in] card card index of the Sidekiq of interest
    @param[in] bus I2C bus index (0=internal, 1=external/secondary)
    @param[in] dev_i2c_addr device's I2C address (7 bits)
    @param[in] reg_addr register address from which to read
    @param[out] p_data reference to uint8_t buffer to be populated, must be at least nr_bytes long
    @param[in] nr_bytes number of bytes to read after writing the reg_addr

    @return int32_t
    @retval 0 successful
    @retval -ENODEV Specified card has not been initialized
    @retval -ERANGE Specified card index is out of range
    @retval -EIO Input/output error trying to use the I2C bus
    @retval -EACCES Failed to enable external bus access
    @retval -EAGAIN Not all of the requested bytes were received
    @retval -EINVAL Specified I2C 'bus' is out of range
 */
extern int32_t skiq_fact_i2c_write_before_read( uint8_t card,
                                                uint8_t bus,
                                                uint8_t dev_i2c_addr,
                                                uint8_t reg_addr,
                                                uint8_t *p_data,
                                                uint16_t nr_bytes );


/**************************************************************************************************/
/** skiq_fact_i2c_write() is responsible for performing an I2C write on the specified bus to the
    specified device address.

    @note The @a bus argument specifies the bus to use for I2C transaction.  On Sidekiq Z2
    (::skiq_z2), a bus of @a 1 targets the external (off-board) I2C bus.  On Sidekiq M.2-2280
    (::skiq_m2_2280), Sidekiq Z2p (::skiq_z2p), and Sidekiq Z3u (::skiq_z3u) a bus of @a 1 targets 
    the secondary I2C bus that controls its RFE.

    @ingroup factoryfunctions

    @see skiq_fact_i2c_write_before_read
    @see skiq_fact_i2c_read

    @param[in] card card index of the Sidekiq of interest
    @param[in] bus I2C bus index (0=internal, 1=external/secondary)
    @param[in] dev_i2c_addr device's I2C address (7 bits)
    @param[out] p_data reference to uint8_t buffer to be written after dev_i2c_addr, must be at least nr_bytes long
    @param[in] nr_bytes number of bytes to write (does not include dev_i2c_addr byte)

    @return int32_t
    @retval 0 successful
    @retval -ENODEV Specified card has not been initialized
    @retval -ERANGE Specified card index is out of range
    @retval -EIO Input/output error trying to use the I2C bus
    @retval -EACCES Failed to enable external bus access
    @retval -EAGAIN Not all of the requested bytes were sent
    @retval -EINVAL Specified I2C 'bus' is out of range
 */
extern int32_t skiq_fact_i2c_write( uint8_t card,
                                    uint8_t bus,
                                    uint8_t dev_i2c_addr,
                                    uint8_t *p_data,
                                    uint16_t nr_bytes );


/**************************************************************************************************/
/** skiq_fact_i2c_read() is responsible for performing an I2C read on the specified bus from the
    specified device address.

    @note The @a bus argument specifies the bus to use for I2C transaction.  On Sidekiq Z2
    (::skiq_z2), a bus of @a 1 targets the external (off-board) I2C bus.  On Sidekiq M.2-2280
    (::skiq_m2_2280), Sidekiq Z2p (::skiq_z2p), Sidekiq Z3u (::skiq_z3u), a bus of @a 1 targets the 
    secondary I2C bus that controls its RFE.

    @ingroup factoryfunctions

    @see skiq_fact_i2c_write_before_read
    @see skiq_fact_i2c_write

    @param[in] card card index of the Sidekiq of interest
    @param[in] bus I2C bus index (0=internal, 1=external/secondary)
    @param[in] dev_i2c_addr device's I2C address (7 bits)
    @param[out] p_data reference to uint8_t buffer to be populated, must be at least nr_bytes long
    @param[in] nr_bytes number of bytes to read

    @return int32_t
    @retval 0 Successful
    @retval -ENODEV Specified card has not been initialized
    @retval -ERANGE Specified card index is out of range
    @retval -EIO Input/output error trying to use the I2C bus
    @retval -EACCES Failed to enable external bus access
    @retval -EAGAIN Not all of the requested bytes were received
    @retval -EINVAL Specified I2C 'bus' is out of range
 */
extern int32_t skiq_fact_i2c_read( uint8_t card,
                                   uint8_t bus,
                                   uint8_t dev_i2c_addr,
                                   uint8_t *p_data,
                                   uint16_t nr_bytes );


/**************************************************************************************************/
/** skiq_fact_verify_fmc_contents_from_file() reads the contents of the referenced file pointer and
    verifies them in their entirety against the contents of the FMC EEPROM device of the associated
    Sidekiq card.  The contents must be less than or equal to the capacity of the FMC EEPROM device.

    @note At this time, only Sidekiq X2 (::skiq_x2) and Sidekiq X4 (::skiq_x4) have FMC EEPROM
    devices and those devices are only accessible when the FMC is in the HTG-K800 FMC carrier card.

    @ingroup factoryfunctions

    @see skiq_fact_write_fmc_contents_from_file
    @see skiq_fact_write_fmc_contents

    @param[in] card card index of the Sidekiq of interest
    @param[in] fp FILE pointer referencing desired contents

    @return int32_t
    @retval 0 successful
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP either card or carrier does not support writing FMC EEPROM contents
    @retval -EINVAL file pointer is not valid
    @retval -EIO I/O error reading file contents
    @retval -ENOMEM unable to allocate enough memory to read file contents
    @retval -ENOSPC file size exceeds capacity of FMC EEPROM device
    @retval -EFAULT EEPROM contents did not match file contents
  */
extern int32_t skiq_fact_verify_fmc_contents_from_file( uint8_t card,
                                                        FILE *fp );


/**************************************************************************************************/
/** skiq_fact_write_fmc_contents_from_file() reads the contents of the referenced file pointer and
    writes them in their entirety to the FMC EEPROM device of the associated Sidekiq card.  The
    contents must be less than or equal to the capacity of the FMC EEPROM device.

    @note At this time, only Sidekiq X2 (::skiq_x2) and Sidekiq X4 (::skiq_x4) have FMC EEPROM
    devices and those devices are only accessible when the FMC is in the HTG-K800 FMC carrier card.

    @ingroup factoryfunctions

    @see skiq_fact_verify_fmc_contents_from_file
    @see skiq_fact_write_fmc_contents

    @param[in] card card index of the Sidekiq of interest
    @param[in] fp FILE pointer referencing desired contents

    @return int32_t
    @retval 0 successful
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP either card or carrier does not support writing FMC EEPROM contents
    @retval -EINVAL file pointer is not valid
    @retval -EIO I/O error reading file contents
    @retval -ENOMEM unable to allocate enough memory to read file contents
    @retval -EFBIG file size exceeds capacity of FMC EEPROM device
 */
extern int32_t skiq_fact_write_fmc_contents_from_file( uint8_t card,
                                                       FILE *fp );


/**************************************************************************************************/
/** skiq_fact_write_fmc_contents() takes specified memory and length and writes the memory contents
    in their entirety to the FMC EEPROM device of the associated Sidekiq card.  The contents must be
    less than or equal to the capacity of the FMC EEPROM device.

    @note At this time, only Sidekiq X2 (::skiq_x2) and Sidekiq X4 (::skiq_x4) have FMC EEPROM
    devices and those devices are only accessible when the FMC is in the HTG-K800 FMC carrier card.

    @ingroup factoryfunctions

    @see skiq_fact_verify_fmc_contents_from_file
    @see skiq_fact_write_fmc_contents_from_file

    @param[in] card card index of the Sidekiq of interest
    @param[in] p_data reference to memory containing the desired contents
    @param[in] length number of valid bytes in the memory reference

    @return int32_t
    @retval 0 successful
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP either card or carrier does not support writing FMC EEPROM contents
    @retval -EINVAL data pointer p_data is NULL or length is 0
    @retval -EFBIG supplied length exceeds capacity of FMC EEPROM device
 */
extern int32_t skiq_fact_write_fmc_contents( uint8_t card,
                                             const uint8_t *p_data,
                                             uint16_t length );


/**************************************************************************************************/
/** skiq_fact_read_default_bias_index() is responsible for reading the factory default LNA bias
    index.  This value is read from the persistent storage of the card.

    @ingroup factoryfunctions

    @see skiq_fact_write_default_bias_index
    @see skiq_fact_read_bias_index
    @see skiq_fact_write_bias_index

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_bias_index reference to uint8_t index to be populated

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE specified card index is out of range
    @retval -ENOENT no default bias index setting stored
    @retval -ENOTSUP Not supported by product (currently valid only for Z2)
 */
extern int32_t skiq_fact_read_default_bias_index( uint8_t card,
                                                  uint8_t *p_bias_index );

/**************************************************************************************************/
/** skiq_fact_write_default_bias_index() is responsible for storing the factory default LNA bias
    index setting.  This setting is then used upon initialization to configure the LNA bias current.
    This value is written to the persistent storage of the card.

    @ingroup factoryfunctions

    @see skiq_fact_read_default_bias_index
    @see skiq_fact_read_bias_index
    @see skiq_fact_write_bias_index

    @param[in] card card index of the Sidekiq of interest
    @param[out] bias_index index for the bias current setting

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE specified card index is out of range
    @retval -EINVAL index setting provided is not valid (1-26)
    @retval -ENOTSUP Not supported by ::skiq_part_t
 */
extern int32_t skiq_fact_write_default_bias_index( uint8_t card,
                                                   uint8_t bias_index );

/**************************************************************************************************/
/** skiq_fact_read_bias_index() is responsible for reading currently configured LNA bias index.

    @ingroup factoryfunctions

    @see skiq_fact_read_default_bias_index
    @see skiq_fact_write_default_bias_index
    @see skiq_fact_write_bias_index

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_bias_index reference to uint8_t index to be populated

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE specified card index is out of range
    @retval -ENOTSUP Not supported by ::skiq_part_t
 */
extern int32_t skiq_fact_read_bias_index( uint8_t card,
                                          uint8_t *p_bias_index );

/**************************************************************************************************/
/** skiq_fact_write_bias_index() is responsible for configuring the LNA bias index

    @ingroup factoryfunctions

    @see skiq_fact_read_default_bias_index
    @see skiq_fact_write_default_bias_index
    @see skiq_fact_read_bias_index

    @param[in] card card index of the Sidekiq of interest
    @param[out] bias_index index for the bias current setting

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE specified card index is out of range
    @retval -EINVAL index setting provided is not valid (1-26)
    @retval -ENOTSUP Not supported by ::skiq_part_t
 */
extern int32_t skiq_fact_write_bias_index( uint8_t card,
                                           uint8_t bias_index );

/**************************************************************************************************/
/** skiq_fact_write_calibration_date() is responsible for storing the specified calibration date and
    interval

    @ingroup factoryfunctions

    @see skiq_fact_reset_calibration_date

    @param[in] card card index of the Sidekiq of interest
    @param[in] cal_year year the calibration is being performed.  Must be 
               in the range of 2000-2063, inclusive.
    @param[in] cal_week week number that the calibration is being performed.
               Must be in the range of 0-51, inclusive.
    @param[in] cal_interval recommendation of how often the calibration should
               be performed (in years).  Must be in the range of 0-15, inclusive.

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE specified card index is out of range
    @retval -ERANGE one of the parameters specified is outside of a valid range
    @retval -ENOTSUP Not supported by ::skiq_part_t
 */
extern int32_t skiq_fact_write_calibration_date( uint8_t card,
                                                 uint16_t cal_year,
                                                 uint8_t cal_week,
                                                 uint8_t cal_interval );

/**************************************************************************************************/
/** skiq_fact_reset_calibration_date() is responsible for resetting the calibration date and
    interval to the unconfigured state.

    @ingroup factoryfunctions

    @see skiq_fact_write_calibration_date

    @param[in] card card index of the Sidekiq of interest

    @return int32_t
    @retval 0 successful
    @retval -ENODEV specified card has not been initialized
    @retval -ERANGE specified card index is out of range
 */
extern int32_t skiq_fact_reset_calibration_date( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_is_golden_locked() indicates whether or not the flash sectors associated with the
    golden (fallback) bitstream are locked.
  
    @ingroup factoryfunctions

    @see skiq_fact_lock_golden_sectors
    @see skiq_fact_unlock_golden_sectors

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_locked reference to boolean to be populated, true means the sectors are locked, false otherwise
  
    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EINVAL provided pointer p_locked is NULL
    @retval -ENOTSUP Sidekiq card does not support locking flash sectors
    @retval -EIO an I/O error occurred while communicating with the flash
 */
extern int32_t skiq_fact_is_golden_locked( uint8_t card,
                                           bool *p_locked );


/**************************************************************************************************/
/** skiq_fact_lock_golden_sectors() locks the flash sectors associated with the golden (fallback)
    FPGA bitstream, but only on Sidekiq parts that support it.
  
    @ingroup factoryfunctions

    @see skiq_fact_is_golden_locked
    @see skiq_fact_unlock_golden_sectors

    @param[in] card card index of the Sidekiq of interest
  
    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support locking flash sectors
    @retval -EIO an I/O error occurred while communicating with the flash
 */
extern int32_t skiq_fact_lock_golden_sectors( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_unlock_golden_sectors() unlocks the flash sectors associated with the golden
    (fallback) FPGA bitstream, but only on Sidekiq parts that support it.
  
    @ingroup factoryfunctions

    @see skiq_fact_is_golden_locked
    @see skiq_fact_lock_golden_sectors

    @param[in] card card index of the Sidekiq of interest
  
    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support unlocking flash sectors
    @retval -EIO an I/O error occurred while communicating with the flash
 */
extern int32_t skiq_fact_unlock_golden_sectors( uint8_t card );


/**************************************************************************************************/
/** skiq_fact_read_tx_jesd_lane_sel() reads the currently configured TX JESD lanes.
  
    @ingroup factoryfunctions

    @see skiq_fact_write_tx_jesd_lane_sel

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_lane_sel pointer to where to store the current TX lane selection
  
    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support JESD configuration
 */
extern int32_t skiq_fact_read_tx_jesd_lane_sel( uint8_t card,
                                                uint8_t *p_lane_sel );

/**************************************************************************************************/
/** skiq_fact_write_tx_jesd_lane_sel() selects JESD lanes to use for TX

    @attention To ensure the JESD link is re-established with the new lane configuration, a new
    sample rate setting should be configured.

    @ingroup factoryfunctions

    @see skiq_fact_read_tx_jesd_lane_sel

    @param[in] card card index of the Sidekiq of interest
    @param[in] lane_sel selects set of JESD lanes to use on the TX (must be 0 or 1)

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support JESD configuration
    @retval -EINVAL lane_sel 0 is default config, 1 is alternate
 */
extern int32_t skiq_fact_write_tx_jesd_lane_sel( uint8_t card,
                                                 uint8_t lane_sel );

/**************************************************************************************************/
/** skiq_fact_read_orx_jesd_lane_sel() reads the currently configured JESD lanes used by the ObsRx
    ports (::skiq_rx_hdl_C1 and ::skiq_rx_hdl_D1) on the Sidekiq X4 (::skiq_x4).

    @ingroup factoryfunctions

    @see skiq_fact_write_orx_jesd_lane_sel

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_lane_sel pointer to where to store the current JESD lane selection for C1/D1

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support JESD lane selection
    @retval -EFAULT  NULL pointer detected for @a p_lane_sel
 */
extern int32_t skiq_fact_read_orx_jesd_lane_sel( uint8_t card,
                                                 uint8_t *p_lane_sel );

/**************************************************************************************************/
/** skiq_fact_write_orx_jesd_lane_sel() selects JESD lanes to use for ObsRx ports (::skiq_rx_hdl_C1
    and ::skiq_rx_hdl_D1) on Sidekiq X4 (::skiq_x4).

    @attention To ensure the JESD link is re-established with the new lane configuration, a new
    sample rate setting is required to be configured for ::skiq_rx_hdl_C1 and ::skiq_rx_hdl_D1,
    depending on which is in use.

    @ingroup factoryfunctions

    @see skiq_fact_read_orx_jesd_lane_sel

    @param[in] card card index of the Sidekiq of interest
    @param[in] lane_sel selects set of JESD lanes to use on the C1/D1 handles (must be 0 or 1)

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support JESD lane selection
    @retval -EINVAL lane_sel 0 is default config, 1 is alternate
 */
extern int32_t skiq_fact_write_orx_jesd_lane_sel( uint8_t card,
                                                  uint8_t lane_sel );

/**************************************************************************************************/
/** skiq_fact_power_down_tx() powers off various aspects of TX in the RF IC.  With certain hardware
    platforms, the TX being powered on may cause undesired spurs and negatively impact the RX
    performance

    @attention Configuring the TX LO frequency after this API will automatically re-enable TX.

    @ingroup factoryfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] transmit handle to power off

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support the feature
 */
extern int32_t skiq_fact_power_down_tx( uint8_t card,
                                        skiq_tx_hdl_t hdl );


/**************************************************************************************************/
/** skiq_fact_read_warp_voltage() queries the warp voltage controlling the oscillator, potentially
    at a greater resolution than is available to the user through the public API.  It does so by
    first calling the RFIC layer's read_warp_voltage_extended_range() then, if it fails, it calls
    the RFIC layer's read_warp_voltage().

    @ingroup factoryfunctions
    @see skiq_fact_write_warp_voltage

    @param[in] card card index of the Sidekiq of interest
    @param[in] p_warp_voltage pointer to a uint32_t to store the raw warp voltage

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support the feature
 */
extern int32_t skiq_fact_read_warp_voltage( uint8_t card,
                                            uint32_t *p_warp_voltage );


/**************************************************************************************************/
/** skiq_fact_write_warp_voltage() configures the warp voltage controlling the oscillator,
    potentially at a greater resolution than is available to the user through the public API.  It
    does so by first calling the RFIC layer's write_warp_voltage_extended_range() then, if it fails,
    it calls the RFIC layer's write_warp_voltage().

    @ingroup factoryfunctions
    @see skiq_fact_read_warp_voltage

    @param[in] card card index of the Sidekiq of interest
    @param[in] warp_voltage desired warp voltage

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Sidekiq card does not support the feature
 */
extern int32_t skiq_fact_write_warp_voltage( uint8_t card,
                                             uint32_t warp_voltage );


/**************************************************************************************************/
/** skiq_fact_write_rx_lna_by_idx() function enables/disables the LNA of the receive handle at the
    specified index.  The new state of the indexed LNA is specified by a boolean parameter where the
    value of @a true enables the LNA while @a false disables the LNA.

    @ingroup factoryfunctions
    @see skiq_fact_bypass_rx_lna_by_idx

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] RX handle of interest
    @param[in] idx LNA index of interest
    @param[in] enable flag to enable or disable the LNA

    @return int32_t
    @retval 0 success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EDOM receive handle is out of range
    @retval -EINVAL specified LNA index is invalid for Sidekiq part
    @retval -ENOTSUP specified RX handle's LNA cannot be bypassed for the associated card / part
 */
extern int32_t skiq_fact_write_rx_lna_by_idx( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              uint8_t idx,
                                              bool enable );


/**************************************************************************************************/
/** skiq_fact_bypass_rx_lna_by_idx() bypasses the LNA that is associated with the specified receive
    handle and LNA index.

    @attention Bypassing the LNA will disable the LNA first before bypassing

    @note Explicitly calling skiq_fact_write_rx_lna() will take the LNA (id = 0) out of bypass mode
    while calling skiq_fact_write_rx_lna_by_idx() will take the LNA (specified by idx) out of bypass
    mode.

    @attention Calling other libsidekiq API functions (for example, changing the RX LO frequency)
    after calling this function may change the state of the LNA without notice.

    @ingroup factoryfunctions
    @see skiq_fact_write_rx_lna_by_idx

    @param[in] card card index of the Sidekiq of interest
    @param[in] rx_hdl [::skiq_rx_hdl_t] receive handle associated with the LNA
    @param[in] idx LNA index of interest

    @return int32_t
    @retval 0 successful LNA bypass
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EDOM receive handle is out of range
    @retval -EINVAL specified LNA index is invalid for Sidekiq part
    @retval -ENOTSUP specified RX handle's LNA cannot be bypassed for the associated card / part
 */
extern int32_t skiq_fact_bypass_rx_lna_by_idx( uint8_t card,
                                               skiq_rx_hdl_t rx_hdl,
                                               uint8_t idx );


/**************************************************************************************************/
/** The skiq_fact_override_rx_lna_by_idx() function allows the caller to override the LNA state of
    the specified receive handle and index by specifying a pin value that is highly specific to
    M.2-2280 rev A and B; Sidekiq Z2p.

    @attention In order to restore the proper state of the Rx LNA, the caller must call
    skiq_write_rf_port_config() twice, first specifying a configuration that is NOT the current
    configuration, then again with the desired configuration.  The RFE caches the RF port
    configuration.

    LNA to LNA index mapping:
    - LNA1 = 0
    - LNA2 = 1

    LNA index to pin mapping:
    - LNA1 = LNA1_MANUAL_BIAS_EN_N
    - LNA2 = LNA2_MANUAL_BIAS_EN_N

    @ingroup factoryfunctions

    @param[in] card card number of the Sidekiq of interest
    @param[in] rx_hdl [::skiq_rx_hdl_t] RX handle of interest
    @param[in] idx LNA to override (0 = LNA1, 1 = LNA2)
    @param[in] pin_value [::skiq_pin_value_t] desired pin value for control line of the specified LNA index

    @return int32_t
    @retval 0 Success
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -EDOM receive handle is out of range
    @retval -ENOTSUP Card index references a Sidekiq part that does not support this feature
    @retval -ENOTSUP ATE_SUPPORT is not enabled for the library
    @retval -EINVAL Specified @a idx is out or range
    @retval -EINVAL Specified @a pin_value is unknown or unsupported
    @retval -EIO Input/output error communicating with the I/O expander
*/
extern int32_t skiq_fact_override_rx_lna_by_idx( uint8_t card,
                                                 skiq_rx_hdl_t rx_hdl,
                                                 uint8_t idx,
                                                 skiq_pin_value_t pin_value );


/**************************************************************************************************/
/** skiq_fact_config_custom_rx_filter() can be used in a factory setting to test the various
    control lines on the RFE of the part.

    The valid range for @a index is 1 to 4, inclusive and maps directly to the filters enumeration
    in the schematic of the part.

    The @a hpf_lpf_setting argument is applied directly to the I/Q expander.  The most significant 4
    bits are the HPF (D,C,B,A) settings while the least significant 4 bits are the LPF (D,C,B,A)
    settings.

    The @a rx_fltr1_sw argument controls which configuration of the FL1 is used.  A value of 1
    asserts the CTL_SW1 pin while a value of 2 asserts the CTL_SW2 pin.  The argument is only
    applicable when the specified @a index is 1.  The caller should specify @a rx_fltr1_sw to be 0
    whenever @a index is not 1.

    The @a rfic_port argument specifies which RX port on the AD9361 to use.  It also configures the
    RF switches to select the correct RF path.  The integer mapping is as follows:
    - 0 = RX1A (2620 MHz to 6000 MHz recommended)
    - 1 = RX1B (  75 MHz to  800 MHz recommended)
    - 2 = RX1C ( 800 MHz to 2620 MHz recommended)

    @ingroup factoryfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] rx_hdl [::skiq_rx_hdl_t] receive handle associated with the filter
    @param[in] index index of desired filter to change (valid range 1-4)
    @param[in] hpf_lpf_setting high-pass and low-pass filter settings
    @param[in] rx_fltr1_sw FL1 configuration selection
    @param[in] rfic_port RX port on the AD9361 to use

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP configuration not available for part
    @retval -EINVAL at least one of the function arguments is invalid
    @retval -EIO I/O error configuring RX filter
    @retval other Internal error
 */
extern int32_t skiq_fact_config_custom_rx_filter( uint8_t card,
                                                  skiq_rx_hdl_t rx_hdl,
                                                  uint8_t index,
                                                  uint8_t hpf_lpf_setting,
                                                  uint8_t rx_fltr1_sw,
                                                  uint8_t rfic_port );


/**************************************************************************************************/
/** skiq_fact_run_eeprom_wp_test() runs the EEPROM write protection test on a single specified card.
    It reports whether or not the write protection successfully protected the library from updating
    the value at a scratch location in EEPROM.

    @warning This function will only work for Sidekiq cards that have write-protection associated
    with their EEPROM parts.

    @ingroup factoryfunctions

    @param[in] card card index of interest

    @return int32_t
    @retval 0 successful EEPROM WP test execution
    @retval -EFAULT WP failed to protect EEPROM contents at scratch location
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP specified card index references a Sidekiq part that does not support EEPROM WP testing
 */
extern int32_t skiq_fact_run_eeprom_wp_test( uint8_t card );



/**************************************************************************************************/
/**
    @brief  Read the manufacturing unique ID for the Flash chip on the specified card

    @param[in]  card        card number of the Sidekiq of interest
    @param[out] p_id        If not NULL, the Flash chip's manufacturing unique ID (if present);
                            this array should be at least ::SKIQ_FACT_FLASH_MAX_UNIQUE_ID_LEN
                            bytes long
    @param[out] p_id_length If not NULL, the actual length of the unique ID in @p p_id (in bytes)

    @return int32_t
    @retval 0           if successful
    @retval -ERANGE     if the specified card index is out of range
    @retval -ENODEV     if the specified card has not been initialized
    @retval -ENOTSUP    if the Flash or transport does not support reading the unique ID
    @retval -EIO        if the Flash unique ID could not be read
*/
extern int32_t skiq_fact_read_flash_unique_id( uint8_t card, uint8_t *p_id, uint8_t *p_id_length );

/**************************************************************************************************/
/**
   @brief  Enables the external dev_clock of a specific Sidekiq device

   @attention This is only supported on Sidekiq X4 revision C (::skiq_x4) and NV100 (::skiq_nv100)

   @param[in]   card        card number of the Sidekiq of interest
   @param[in]   in_port     [::skiq_rf_port_t] RF port in which the dev_clk is provided.
   @param[in]   out_port    [::skiq_rf_port_t] RF port in which the dev_clk is output.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers

   @see skiq_fact_disable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions
*/
extern int32_t skiq_fact_enable_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t in_port, skiq_rf_port_t out_port);

/**************************************************************************************************/
/**
   @brief  Disables the external dev_clock of a specific Sidekiq device

   @attention This is only supported on Sidekiq X4 revision C (::skiq_x4) and NV100 (::skiq_nv100)

   @param[in]  card        card number of the Sidekiq of interest
   @param[in]  in_port     [::skiq_rf_port_t] RF port in which the external dev clock input is to be disabled.
   @param[in]  out_port    [::skiq_rf_port_t] RF port in which the dev clock output is to be disabled.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -ENOTSUP    if the specified card is not supported, ie anything other than X4 Rev C
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers

   @see skiq_fact_enable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions

*/
extern int32_t skiq_fact_disable_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t in_port, skiq_rf_port_t out_port);

/**************************************************************************************************/
/**
   @brief  Configures the card to count SYSREF_IN pulses on the specified port

   @attention This is only supported on Sidekiq X4 revision C (::skiq_x4)

   @param[in]   card        card number of the Sidekiq of interest
   @param[in]   port        [::skiq_rf_port_t] RF port in which the SYSREF pulses are provided.
   @param[in]   ms          duration in milliseconds to count SYSREF pulses
   @param[out]  p_pulse_count  pointer to uint32_t of counted pulses

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -ENOTSUP    if the specified card is not supported, ie anything other than X4 Rev C
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers
   @retval -EFAULT     NULL pointer detected for @a p_pulseCount


   @see skiq_fact_disable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions

*/
extern int32_t skiq_fact_count_sysref( uint8_t card, skiq_rf_port_t port, uint32_t ms, uint32_t * p_pulse_count);

/**************************************************************************************************/
/**
   @brief  Reads the board revision.  Replaces skiq_fact_read_board_id.  For Sidekiq X4 , the board id
           is obtained by reading bits 31:30 of FPGA_REG_GPIO_READ.  For all other parts, the id is
           bits 3:0 of FPGA_REG_VERSION.

   @param[in]   card         card number of the Sidekiq of interest
   @param[out]  p_board_rev  pointer to uint8_t of the board revision.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -EBADMSG    if an error occurred transacting with FPGA registers
   @retval -EFAULT     NULL pointer detected for @a p_board_rev

   @see skiq_fact_read_board_id
   @ingroup factoryfunctions

*/
extern int32_t skiq_fact_read_board_rev(uint8_t card, uint8_t *p_board_rev);

/**
    @brief  Perform a "soft reset" of the FPGA.

    If the loaded FPGA supports it, this function tells the FPGA to perform a soft reset,
    which effectively resets all of the registers.

    @param[in]  card        card number of the Sidekiq of interest

    @since Function added in API @b v4.16.0

    @return 0 on success, else a negative errno value
    @return -ENOSYS     if the loaded FPGA bitstream doesn't support FPGA register reads or writes
    @return -ENOTSUP    if the loaded FPGA bitstream doesn't support this feature
    @retval -EBADMSG    if an error occurred transacting with FPGA registers
*/
extern int32_t skiq_fact_soft_reset_fpga(uint8_t card);

/**************************************************************************************************/
/**
   @brief  Writes the hardware interface version of the specified card to EEPROM.  For EEPROM location
   information please refer to: https://confluence.epiq.rocks/display/EN/Sidekiq+EEPROM+Memory.

   @param[in]   card                 card number of Sidekiq of interest
   @param[in]   major_hw_iface_vers  major portion of hardware interface version being written to EEPROM
   @param[in]   minor_hw_iface_vers  minor portion of hardware interface version being written to EEPROM
   @param[in]   p_part_number        pointer to part number used to disable WP on eeprom
   @param[in]   p_revision           pointer to revision used to disable WP on eeprom

   @see skiq_fact_read_hw_iface_vers
   @ingroup factoryfunctions

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
*/
extern int32_t skiq_fact_write_hw_iface_vers(uint8_t card, uint8_t major_hw_iface_vers, uint8_t minor_hw_iface_vers, char *p_part_number, char *p_revision);

/**************************************************************************************************/
/**
   @brief  Reads the hardware interface version of the specified card.

   @param[in]   card                   card number of Sidekiq of interest
   @param[out]  p_major_hw_iface_vers  if not NULL, pointer to major portion of the hardware interface version.
   @param[out]  p_minor_hw_iface_vers  if not NULL, pointer to minor portion of the hardware interface version.

   @see skiq_fact_write_hw_iface_vers
   @ingroup factoryfunctions

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -EFAULT     NULL pointer detected for @p p_major_hw_iface_vers or @p p_minor_hw_iface_vers
*/
extern int32_t skiq_fact_read_hw_iface_vers(uint8_t card, uint8_t *p_major_hw_iface_vers, uint8_t *p_minor_hw_iface_vers);

#ifdef __cplusplus
}
#endif

#endif  /* __SIDEKIQ_API_FACTORY__ */
