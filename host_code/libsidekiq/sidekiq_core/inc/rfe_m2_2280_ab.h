#ifndef __RFE_M2_2280_AB_H__
#define __RFE_M2_2280_AB_H__

/**
 * @file   rfe_m2_2280_ab.h
 * @date   Wed Apr 10 16:30:29 2019
 *
 * @brief This file contains the public interface of the RFE (RF front end) implementation for M.2
 * 2280 rev A
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include "sidekiq_rfe.h"
#include "sidekiq_api_factory.h" /* for skiq_pin_value_t */


/** @brief function pointers for M.2 2280 rev A and rev B RFE implementation */
extern rfe_functions_t rfe_m2_2280_ab_funcs;

/**************************************************************************************************/
/** The rfe_m2_2280_override_rx_lna_by_idx() function allows the caller to override the LNA state of
    the specified receive handle and index by specifying a pin value that is highly specific to
    M.2-2280 rev A and B.

    @attention In order to restore the proper state of the Rx LNA, the caller must call
    skiq_write_rf_port_config() twice, first specifying a configuration that is NOT the current
    configuration, then again with the desired configuration.  The RFE caches the RF port
    configuration.

    LNA to LNA index mapping:
    - LNA1 = 0
    - LNA2 = 1

    LNA index to pin mapping:
    - LNA1 = LNA1_MANUAL_BIAS_EN_N
    - LNA2 = LNA2_MANUAL_BIAS_EN_N

    @ingroup factoryfunctions

    @param[in] p_id Pointer to RF identifier struct
    @param[in] idx LNA to override (0 = LNA1, 1 = LNA2)
    @param[in] pin_value [::skiq_pin_value_t] desired pin value for control line of the specified LNA index

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP ATE_SUPPORT is not enabled for the library
    @retval -EINVAL Specified @a idx is out or range
    @retval -EINVAL Specified @a pin_value is unknown or unsupported
    @retval -EIO Input/output error communicating with the I/O expander
*/
int32_t
rfe_m2_2280_override_rx_lna_by_idx( rf_id_t *p_id,
                                    uint8_t idx,
                                    skiq_pin_value_t pin_value );


#endif  /* __RFE_M2_2280_AB_H__ */
