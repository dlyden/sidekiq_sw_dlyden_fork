#ifndef SIDEKIQ_X2_PRIVATE_H
#define SIDEKIQ_X2_PRIVATE_H

/*! \file sidekiq_types_private.h
 * \brief This file contains the private type definitions and macros used
 *  internally for Sidekiq X2.
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#ifdef __cplusplus
extern "C" {
#endif

/***** INCLUDES *****/

#include <stdint.h>

/***** DEFINES *****/

// TODO: Should these be left as defines? Made into enums?
#define PLL_SPI_CS_PLL1 (0) // PLL1
#define PLL_SPI_CS_PLL2 (1) // PLL2
#define PLL_SPI_CS_INVALID (0xFF)

/***** MACROS *****/

/** @brief Obtains the RFFC5071a PLL chip select line for a given Rx handle. */
#define X2_MAP_RX_HDL_TO_PLL_CS(rx_hdl)                 \
    ((skiq_rx_hdl_A1 == (rx_hdl)) ? PLL_SPI_CS_PLL1 :   \
     (skiq_rx_hdl_A2 == (rx_hdl)) ? PLL_SPI_CS_PLL1 :   \
     (skiq_rx_hdl_B1 == (rx_hdl)) ? PLL_SPI_CS_PLL2 :   \
     PLL_SPI_CS_INVALID)

/** @brief Obtains the RFFC5071a PLL chip select line for a given Tx handle. */
#define X2_MAP_TX_HDL_TO_PLL_CS(tx_hdl)             \
    ((skiq_tx_hdl_A1 == tx_hdl) ? PLL_SPI_CS_PLL2 : \
     PLL_SPI_CS_INVALID)

/** @brief Obtains the RFFC5071a PLL id for a given card and Rx handle. */
#define X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, rx_hdl)      \
    (((uint64_t) card) |                                    \
    (((uint64_t) X2_MAP_RX_HDL_TO_PLL_CS((rx_hdl))) << 16))

/** @brief Obtains the RFFC5071a PLL id for a given card and Tx handle. */
#define X2_MAP_CARD_AND_TX_HDL_TO_PLL_ID(card, tx_hdl)      \
    (((uint64_t) card) |                                    \
    (((uint64_t) X2_MAP_TX_HDL_TO_PLL_CS((tx_hdl))) << 16))

/** @brief Obtains the RFFC5071a PLL chip select for a given PLL ID. */
#define X2_MAP_PLL_ID_TO_SPI_CS(id) \
    ((id >> 16) & 0xFF)

/** @brief Obtains the Sidekiq card for a given PLL ID. */
#define X2_MAP_PLL_ID_TO_SKIQ_CARD(id) \
    (id & 0xFF)

#ifdef __cplusplus
}
#endif

#endif
