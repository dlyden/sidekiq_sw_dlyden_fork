#ifndef _CALC_AD9528_H_
#define _CALC_AD9528_H_

/*! \file calc_ad9528.h
 * \brief This file contains the public interface to calculate
 * the AD9528 settings based on the requested sample rate
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 12/26/2017   MEZ   Created file
 *
 * </pre>
 */

#include <stdbool.h>
#include <stdint.h>

#include <t_ad9528.h>

#define AD9528_MAX_CLOCK_OUTS (14)

#define AD9528_GBT_CLK_OUT(_n)          (1 << (_n))
#define AD9528_GBT_CLK_OUT_ALL          (AD9528_GBT_CLK_OUT(AD9528_MAX_CLOCK_OUTS) - 1)

// clock divider can either be 1/2/4 or 1*2 / 2*2 / 2*4 when FPGA output divider is 2x
typedef enum
{
    CLK_DIV_1=1,
    CLK_DIV_2=2,
    CLK_DIV_4=4,
    CLK_DIV_8=8,
    CLK_DIV_INVALID,
} clk_div_t;

typedef struct
{
    uint32_t fpga_clk_sel_mask; /* FPGA GBT clock select output mask, if bit N is 1, then clock
                                   output is selected as FPGA GBT */
    uint8_t debug_clk_sel;     // Debug clock select output
    uint8_t rffc1_clk_sel;     // RFFC 1 clock select output
    uint8_t rffc2_clk_sel;     // RFFC 2 clock select output
} clk_sel_t;


static inline bool AD9528_GBT_CLK_ISSET( uint32_t clk_mask,
                                         uint8_t bit )
{
    uint32_t bit_mask = AD9528_GBT_CLK_OUT( bit );
    return ( ( clk_mask & bit_mask ) == bit_mask );
}

int32_t calc_ad9528_for_sample_rate( uint32_t req_sample_rate,
                                     uint32_t *p_actual_sample_rate,
                                     ad9528pll1Settings_t *p_pll1,
                                     ad9528pll2Settings_t *p_pll2,
                                     ad9528outputSettings_t *p_output,
                                     ad9528sysrefSettings_t *p_sysref,
                                     clk_div_t *p_fpga_div,
                                     uint32_t *p_dev_clk,
                                     clk_div_t *p_dev_clk_div,
                                     uint32_t *p_rffc_freq,
                                     uint64_t fpga_min_dev_clk,
                                     uint64_t fpga_max_dev_clk,
                                     uint64_t *p_fpga_gbt_clock,
                                     const clk_sel_t clk_sel,
                                     skiq_part_t part  );

void ad9528_init_struct( ad9528pll1Settings_t *p_pll1,
                         ad9528pll2Settings_t *p_pll2 );

#endif  /* _CALC_AD9528_H_ */
