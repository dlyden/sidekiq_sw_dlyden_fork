/**
   @file    sidekiq_util.h
   @author  <info@epiq-solutions.com>
   @date    Mon May 18 16:21:21 CDT 2020

   @brief   Implementations of useful functions that have no place to live.

   <pre>
   Copyright 2020 Epiq Solutions, All Rights Reserved
   </pre>

*/

#ifndef __SIDEKIQ_UTIL_H__
#define __SIDEKIQ_UTIL_H__

/***** INCLUDES *****/

#include <stdbool.h>
#include <stdint.h>


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

/**************************************************************************************************/
/**
   @brief Looks for @p item in the @p list array (which has @p nr_entries valid entries).  Return @c
   true if @p item is NOT in @p list and @c false otherwise.
 */
bool u8_is_not_in_list( uint8_t item,
                        uint8_t list[],
                        uint8_t nr_entries );

/**************************************************************************************************/
/**
   @brief Looks for @p item in the @p list array (which has @p nr_entries valid entries).  Return @c
   true if @p item is NOT in @p list and @c false otherwise.
 */
bool u64_is_not_in_list( uint64_t item,
                         uint64_t list[],
                         uint8_t nr_entries );


#endif  /* __SIDEKIQ_UTIL_H__ */
