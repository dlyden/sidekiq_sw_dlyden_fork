/**
 * @file   backtrace.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Mon Jun  5 16:09:36 2017
 * 
 * @brief  
 * 
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 * 
 */

#ifndef __BACKTRACE_H__
#define __BACKTRACE_H__

/***** INCLUDES *****/

#include <stdio.h>
#include <execinfo.h>

/***** DEFINES *****/

#define BT_BUF_SIZE 100

/***** TYPEDEFS *****/

/***** MACROS  *****/

#if (defined ENABLE_BACKTRACE) && (ENABLE_BACKTRACE == 1)
#define BACKTRACE                                                       \
    do {                                                                \
        int _j, _nptrs;                                                 \
        void *_buffer[BT_BUF_SIZE];                                     \
        char **_strings;                                                \
        _nptrs = backtrace(_buffer, BT_BUF_SIZE);                       \
        fprintf(stderr, "backtrace() returned %d addresses\n", _nptrs); \
        _strings = backtrace_symbols(_buffer, _nptrs);                  \
        if ( _strings != NULL ) {                                       \
            for (_j = 0; _j < _nptrs; _j++)                             \
                fprintf(stderr, "\t%s\n", _strings[_j]);                \
            free( _strings );                                           \
        } else {                                                        \
            perror("backtrace_symbols");                                \
        }                                                               \
    } while (0)
#else
#define BACKTRACE
#endif  /* ENABLE_BACKTRACE */


#endif  /* __BACKTRACE_H__ */
