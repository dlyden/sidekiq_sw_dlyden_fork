/**
 * @file   hardware.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  9 09:57:32 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 *
 *
 * </pre>
 */

#ifndef __HARDWARE_H__
#define __HARDWARE_H__


/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_types.h"
#include "sidekiq_private.h"

/***** DEFINES *****/


/***** TYPEDEFS *****/
// Warning! These defines are stored in EE.
// If you make a change, you could break backwards compatability.
typedef enum
{
    skiq_eeprom_hw_vers_mpcie_a = 0x001,
    skiq_eeprom_hw_vers_mpcie_b = 0x002,
    skiq_eeprom_hw_vers_mpcie_c = 0x003,
    // mPCIe rev E cards masquerade as legacy rev C cards for backwards compatibility, so this
    // revision shouldn't be stored in the EEPROM... thus no entry for it. mPCIe rev D cards can
    // either masquerade or identify themselves as a rev D in the EEPROM.
    skiq_eeprom_hw_vers_mpcie_d = 0x004,
    //skiq_eeprom_hw_vers_m2_a = 0x010, NOTE: no longer supported (6/17/16), no one should have rev A
    skiq_eeprom_hw_vers_m2_b = 0x020,
    skiq_eeprom_hw_vers_m2_c = 0x030,

    skiq_eeprom_hw_vers_ext = 0xEEE, // indicates to go to other address for part info, skiq_hw_vers_end

    skiq_eeprom_hw_vers_invalid = 0xFFF,
} skiq_eeprom_hw_vers_t;

typedef enum
{
    skiq_eeprom_product_mpcie_001 = 0x0,
    skiq_eeprom_product_mpcie_002 = 0x1,
    skiq_eeprom_product_m2_001 = 0x4,
    skiq_eeprom_product_m2_002 = 0x8,
    skiq_eeprom_product_ext = 0xE, // indicates to go to other address for part info, skiq_product_end
    skiq_eeprom_product_invalid = 0xF,
} skiq_eeprom_product_vers_t;

/***** EXTERN FUNCTIONS  *****/

skiq_hw_vers_t card_map_hw_vers( uint16_t hw_vers );
skiq_product_t card_map_product_vers( uint16_t hardware );
skiq_part_t card_map_part( const char *p_part_number );
skiq_product_t card_map_part_to_product( const char *p_part_number );

skiq_hw_rev_t card_map_hw_rev( const char *p_revision );

void card_map_rev_and_part( skiq_hw_vers_t hw_vers,
                            char *p_part_number,
                            char *p_revision,
                            skiq_part_t *p_part,
                            skiq_hw_rev_t *p_hw_rev );

int32_t card_map_version( skiq_hw_vers_t hw_vers,
                          skiq_product_t product,
                          uint16_t *p_version );

int32_t card_map_legacy_part_info( skiq_hw_vers_t hw_vers,
                                   skiq_product_t product,
                                   char *p_part_number,
                                   char *p_revision,
                                   char *p_variant );

#endif  /* __HARDWARE_H__ */
