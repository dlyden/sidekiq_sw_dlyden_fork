#ifndef __SIDEKIQ_RFE_H__
#define __SIDEKIQ_RFE_H__

/*****************************************************************************/
/** @file sidekiq_rfe.h
 
    <pre>
    Copyright 2017-2020 Epiq Solutions, All Rights Reserved
    </pre>

    @brief This file contains the public interface of the RFE (RF front end).
    The RFE is responsible for configuring the various RF front end settings,
    where the RF front end is the hardware components which sit in front of the
    RFIC in the RF lineup.  This sidekiq_rfe provides an abstraction layer
    which the specific hardware realization implements.
*/

#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_types.h"
#include "sidekiq_types_private.h"
#include "sidekiq_rf.h"
#include "sidekiq_api_factory.h"
#include "hw_iface.h"

/** @brief Used to specify a desired LNA state when calling update_rx_lna() */
typedef enum
{
    lna_disabled,               /**! Disable the LNA */
    lna_enabled,                /**! Enable the LNA  */
    lna_bypassed,               /**! Bypass the LNA */

} rfe_lna_state_t;

/** @brief The various function pointers to be implemented by a realization of the RFE */
typedef struct
{
    int32_t (*init)( rf_id_t *p_id );
    int32_t (*post_init)( rf_id_t *p_id );
    int32_t (*release)( rf_id_t *p_id );
    int32_t (*pre_release)( rf_id_t *p_id );
    int32_t (*update_tx_pa)( rf_id_t *p_id,
                             bool enable );
    int32_t (*override_tx_pa)( rf_id_t *p_id,
                               skiq_pin_value_t pin_value );
    int32_t (*update_rx_lna)( rf_id_t *p_id,
                              rfe_lna_state_t new_state );
    int32_t (*update_rx_lna_by_idx)( rf_id_t *p_id,
                                     uint8_t idx,
                                     rfe_lna_state_t new_state );
    int32_t (*update_before_rx_lo_tune)( rf_id_t *p_rfe_id,
                                         uint64_t rx_lo_freq );
    int32_t (*update_after_rx_lo_tune)( rf_id_t *p_rfe_id,
                                        uint64_t rx_lo_freq );
    int32_t (*update_before_tx_lo_tune)( rf_id_t *p_id,
                                         uint64_t tx_lo_freq );
    int32_t (*update_after_tx_lo_tune)( rf_id_t *p_id,
                                        uint64_t tx_lo_freq );
    
    int32_t (*write_rx_filter_path)( rf_id_t *p_id,
                                     skiq_filt_t path );
    int32_t (*write_tx_filter_path)( rf_id_t *p_id,
                                     skiq_filt_t path );
    int32_t (*read_rx_filter_path)( rf_id_t *p_id,
                                    skiq_filt_t *p_path );
    int32_t (*read_tx_filter_path)( rf_id_t *p_id,
                                    skiq_filt_t *p_path );
    
    int32_t (*read_rx_filter_capabilities)( rf_id_t *p_id,
                                            skiq_filt_t *p_filters,
                                            uint8_t *p_num_filters );
    int32_t (*read_tx_filter_capabilities)( rf_id_t *p_id,
                                            skiq_filt_t *p_filters,
                                            uint8_t *p_num_filters );
    int32_t (*write_filters)( rf_id_t *p_id,
                              uint8_t num_filters,
                              const skiq_filt_t *p_filters );

    int32_t (*write_rx_attenuation)( rf_id_t *p_id,
                                     uint16_t atten_quarter_db );
    int32_t (*read_rx_attenuation)( rf_id_t *p_id,
                                    uint16_t *p_atten_quarter_db );

    int32_t (*read_rx_attenuation_range)( rf_id_t *p_id,
                                          uint16_t *p_atten_quarter_db_max,
                                          uint16_t *p_atten_quarter_db_min );

    int32_t (*write_rx_attenuation_mode)( rf_id_t *p_id,
                                          skiq_rx_attenuation_mode_t mode );
    int32_t (*read_rx_attenuation_mode)( rf_id_t *p_id,
                                         skiq_rx_attenuation_mode_t *p_mode );
    int32_t (*read_rx_rf_ports_avail)( rf_id_t *p_id,
                                       uint8_t *p_num_fixed_rf_ports,
                                       skiq_rf_port_t *p_fixed_rf_port_list,
                                       uint8_t *p_num_trx_rf_ports,
                                       skiq_rf_port_t *p_trx_rf_port_list);
    int32_t (*read_rx_rf_port)( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
    int32_t (*write_rx_rf_port)( rf_id_t *p_id, skiq_rf_port_t rf_port );
    int32_t (*read_tx_rf_ports_avail)( rf_id_t *p_id,
                                       uint8_t *p_num_fixed_rf_ports,
                                       skiq_rf_port_t *p_fixed_rf_port_list,
                                       uint8_t *p_num_trx_rf_ports,
                                       skiq_rf_port_t *p_trx_rf_port_list );
    int32_t (*read_tx_rf_port)( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
    int32_t (*write_tx_rf_port)( rf_id_t *p_id, skiq_rf_port_t rf_port );

    int32_t (*rf_port_config_avail)( rf_id_t *p_id, bool *p_fixed, bool *p_trx );
    int32_t (*write_rf_port_config)( rf_id_t *p_id, skiq_rf_port_config_t config );
    int32_t (*read_rf_port_operation)( rf_id_t *p_id, bool *p_transmit );
    int32_t (*write_rf_port_operation)( rf_id_t *p_id, bool transmit );

    int32_t (*read_bias_index)( rf_id_t *p_id, uint8_t *p_bias_index );
    int32_t (*write_bias_index)( rf_id_t *p_id, uint8_t bias_index );

    int32_t (*read_rx_stream_hdl_conflict)( rf_id_t *p_id, skiq_rx_hdl_t *p_conflicting_hdls, uint8_t *p_num_hdls );

    int32_t (*enable_rx_chan)( rf_id_t *p_id, bool enable );
    int32_t (*enable_tx_chan)( rf_id_t *p_id, bool enable );

    int32_t (*write_rx_freq_hop_list)( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );
    int32_t (*write_tx_freq_hop_list)( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );

    int32_t (*config_custom_rx_filter)( rf_id_t *p_id,
                                        uint8_t filt_index,
                                        uint8_t hpf_lpf_setting,
                                        uint8_t rx_fltr1_sw,
                                        uint8_t rfic_port );

    int32_t (*read_rfic_port_for_rx_hdl)( rf_id_t *p_id, uint8_t *p_port );
    int32_t (*read_rfic_port_for_tx_hdl)( rf_id_t *p_id, uint8_t *p_port );

    int32_t (*swap_rx_port_config)( rf_id_t *p_id, uint8_t port_a, uint8_t port_b );
    int32_t (*swap_tx_port_config)( rf_id_t *p_id, uint8_t port_a, uint8_t port_b );

    int32_t (*read_rf_capabilities)( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities );

} rfe_functions_t;

/** @brief An RFE instance which contains the RFE function pointers as well as the RF ID */
typedef struct
{
    rf_id_t *p_id;
    rfe_functions_t *p_funcs;
} rfe_t;

/*****************************************************************************/
/** The rfe_init_functions function intializes the RFE function pointers based
    on the Sidekiq hardware type.

    @param[in] part part type of the Sidekiq
    @param[in] hw_rev hardware revision of the Sidekiq
    @param[in] hw_iface_vers hardware interface version of the Sidekiq

    @return rfe_functions_t* pointer to RFE function pointers
*/
rfe_functions_t* rfe_init_functions( skiq_part_t part,
                                     skiq_hw_rev_t hw_rev,
                                     hw_iface_vers_t hw_iface_vers );

/*****************************************************************************/
/** The rfe_init function is responsible for performing the various required
    initialization of the RFE.

    @param[in] p_rfe pointer to the RFE instance

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_init( rfe_t *p_rfe );

/*****************************************************************************/
/** The rfe_post_init function is responsible for initialization of the RFE
    that will occur after the RFIC is initialized

    @param[in] p_rfe pointer to the RFE instance

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_post_init( rfe_t *p_rfe );

/*****************************************************************************/
/** The rfe_release() function is responsible for performing the various
    required actions associated with the shutdown of the RFE.

    @param[in] p_rfe pointer to the RFE instance

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_release( rfe_t *p_rfe );

/*****************************************************************************/
/** The rfe_pre_release() function is responsible for shutdown tasks that will
    occur before the RFIC is released.

    @param[in] p_rfe pointer to the RFE instance

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_pre_release( rfe_t *p_rfe );

/*****************************************************************************/
/** The rfe_update_tx_pa function is responsible for configuring the state of 
    the TX PA.  The specific RF port of interest is contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] enable flag indicating whether the PA should be enabled

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_tx_pa( rfe_t *p_rfe,
                          bool enable );

/*****************************************************************************/
/** The rfe_update_rx_lna function is responsible for configuring the state of 
    the RX LNA.  The specific RF port of interest is contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] new_state [::rfe_lna_state_t] desired new state of the LNA

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_rx_lna( rfe_t *p_rfe,
                           rfe_lna_state_t new_state );

/**************************************************************************************************/
/** The rfe_update_rx_lna_by_idx() function is responsible for configuring the state of the RX LNA
    at the specified LNA index.  The specific RF port of interest is contained within the RF ID and
    the LNA index is specified by the function argument @a idx.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] idx LNA index of interest
    @param[in] new_state [::rfe_lna_state_t] desired new state of the LNA

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_rx_lna_by_idx( rfe_t *p_rfe,
                                  uint8_t idx,
                                  rfe_lna_state_t new_state );

/**************************************************************************************************/
/** The rfe_update_before_rx_lo_tune() function is responsible for configuring the RFE based on an
    upcoming specified LO frequency tune.  Depending on the hardware variant, this may disconnect
    and/or isolate the receive path in case the RFIC needs to perform calibration or to prevent a
    surge of RF energy.  The specific RF port of interest is contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] rx_lo_freq RX LO frequency being configured

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_before_rx_lo_tune( rfe_t *p_rfe,
                                      uint64_t rx_lo_freq );

/**************************************************************************************************/
/** The rfe_update_after_rx_lo_tune() function is responsible for configuring the RFE based on a
    completed LO frequency tune.  Depending on the hardware variant, this may re-connect receive
    path.  The specific RF port of interest is contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] rx_lo_freq RX LO frequency being configured

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_after_rx_lo_tune( rfe_t *p_rfe,
                                     uint64_t rx_lo_freq );

/**************************************************************************************************/
/** The rfe_update_before_tx_lo_tune function is responsible for configuring the RFE based on an
    upcoming specified LO frequency.  Depending on the hardware variant, this may update the TX
    filter path as well as the PA state.  The specific RF port of interest is contained within the
    RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] tx_lo_freq TX LO frequency being configured

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_before_tx_lo_tune( rfe_t *p_rfe,
                                      uint64_t tx_lo_freq );

/**************************************************************************************************/
/** The rfe_update_after_tx_lo_tune function is responsible for configuring the RFE based on a
    completed LO frequency tune.  Depending on the hardware variant, this may update the TX filter
    path as well as the PA state.  The specific RF port of interest is contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] tx_lo_freq TX LO frequency being configured

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_update_after_tx_lo_tune( rfe_t *p_rfe,
                                     uint64_t tx_lo_freq );

/*****************************************************************************/
/** The rfe_write_rx_filter_path function is responsible for configuring filter
    path requested.  The specific RF port of interest is contained within
    the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] path RX filter path to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_rx_filter_path( rfe_t *p_rfe,
                                  skiq_filt_t path );

/*****************************************************************************/
/** The rfe_write_tx_filter_path function is responsible for configuring filter
    path requested.  The specific RF port of interest is contained within
    the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] path TX filter path to configure

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_tx_filter_path( rfe_t *p_rfe,
                                  skiq_filt_t path );

/*****************************************************************************/
/** The rfe_read_rx_filter_path function is responsible for returning the 
    currently configured RX filter path.  The specific RF port of interest is 
    contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_path pointer to where to store the filter path

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_filter_path( rfe_t *p_rfe,
                                 skiq_filt_t *p_path );

/*****************************************************************************/
/** The rfe_read_tx_filter_path function is responsible for returning the 
    currently configured TX filter path.  The specific RF port of interest is 
    contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_path pointer to where to store the filter path

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_tx_filter_path( rfe_t *p_rfe,
                                 skiq_filt_t *p_path );

/*****************************************************************************/
/** The rfe_read_rx_filter_capabilities function is responsible for returning 
    a list of the available RF filters.  The specific RF port of interest is
    contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_filters pointer to where to store the filter list
    @param[out] p_num_filters pointer to where to store # filters in list
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_filter_capabilities( rfe_t *p_rfe,
                                         skiq_filt_t *p_filters,
                                         uint8_t *p_num_filters );

/*****************************************************************************/
/** The rfe_read_rx_filter_capabilities function is responsible for returning 
    a list of the available RF filters.  The specific RF port of interest is
    contained within the RF ID.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_filters pointer to where to store the filter list
    @param[out] p_num_filters pointer to where to store # filters in list
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_tx_filter_capabilities( rfe_t *p_rfe,
                                         skiq_filt_t *p_filters,
                                         uint8_t *p_num_filters );

/*****************************************************************************/
/** The rfe_write_filters function is responsible for storing the filter 
    list available for the specific RFE.  Not all RFE implementations 
    support this configuration and should return an error if unsupported.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] num_filters # of filters contained in the filter list
    @param[in] p_filters pointer to the list of filters to store
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_filters( rfe_t *p_rfe,
                           uint8_t num_filters,
                           const skiq_filt_t *p_filters );

/*****************************************************************************/
/** The rfe_write_rx_attenuation function is responsible for configuring the
    RX attenuation specified.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] attenuation attenuation to apply in dB.
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_rx_attenuation( rfe_t *p_rfe,
                                  uint16_t atten_quarter_db );

/*****************************************************************************/
/** The rfe_read_rx_attenuation function is responsible for reading back the
    current Rx attenuation.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_attenuation pointer to take current attenuation in dB.
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_attenuation( rfe_t *p_rfe,
                                 uint16_t *p_atten_quarter_db );

/*****************************************************************************/
/** The rfe_read_rx_attenuation_range reads the acceptable range of Rx
    attenuation in dB.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_atten_quarter_db_max pointer to be updated with the maximum
                Rx attenuation in quarted dB steps.
    @param[out] p_atten_quarter_db_min pointer to be updated with the minimum
                Rx attenuation in quarted dB steps.
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_attenuation_range( rfe_t *p_rfe,
                                       uint16_t *p_atten_quarter_db_max,
                                       uint16_t *p_atten_quarter_db_min );

/*****************************************************************************/
/** The rfe_write_rx_attenuation_mode configures the RFE to apply the Rx
    attenuation according to the passed in mode.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] mode the mode to configure Rx attenuation for
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_rx_attenuation_mode( rfe_t *p_rfe,
                                       skiq_rx_attenuation_mode_t mode );

/*****************************************************************************/
/** The rfe_read_rx_attenuation_mode reads back the currently configured Rx
    attenuation mode.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_mode pointer to be updated with current Rx attenuation mode
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_attenuation_mode( rfe_t *p_rfe,
                                      skiq_rx_attenuation_mode_t *p_mode );

/*****************************************************************************/
/** The rfe_read_rx_rf_ports_avail creates a list of RF ports available 

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_num_fixed_rf_ports pointer to number of fixed RF ports contained in list
    @param[out] p_fixed_rf_port_list pointer to beginning of list of fixed RF ports
    @param[out] p_num_trx_rf_ports pointer to number of TRX RF ports contained in list
    @param[out] p_trx_rf_port_list pointer to beginning of list of TRX RF ports
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_rf_ports_avail( rfe_t *p_rfe,
                                    uint8_t *p_num_fixed_rf_ports,
                                    skiq_rf_port_t *p_fixed_rf_port_list,
                                    uint8_t *p_num_trx_rf_ports,
                                    skiq_rf_port_t *p_trx_rf_port_list );

/*****************************************************************************/
/** The rfe_read_rx_rf_port reads the current RF port configured for RX hdl

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_rf_port pointer to be updated with current RF port
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_rf_port( rfe_t *p_rfe, skiq_rf_port_t *p_rf_port );

/*****************************************************************************/
/** The rfe_read_rx_rf_port writes the RF port for the RX hdl

    @note This may result in the RFIC to handle port mapping to update,
    refer to @ref rfe_read_rfic_port_for_rx_hdl and 
    @ref rfe_read_rfic_port_for_tx_hdl

    @param[in] p_rfe pointer to the RFE instance
    @param[in] rf_port RF port to configure hdl to use 
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_rx_rf_port( rfe_t *p_rfe, skiq_rf_port_t rf_port );

/*****************************************************************************/
/** The rfe_read_tx_rf_ports_avail creates a list of RF ports available 

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_num_fixed_rf_ports pointer to number of fixed RF ports contained in list
    @param[out] p_fixed_rf_port_list pointer to beginning of list of fixed RF ports
    @param[out] p_num_trx_rf_ports pointer to number of TRX RF ports contained in list
    @param[out] p_trx_rf_port_list pointer to beginning of list of TRX RF ports
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_tx_rf_ports_avail( rfe_t *p_rfe,
                                    uint8_t *p_num_fixed_rf_ports,
                                    skiq_rf_port_t *p_fixed_rf_port_list,
                                    uint8_t *p_num_trx_rf_ports,
                                    skiq_rf_port_t *p_trx_rf_port_list );

/*****************************************************************************/
/** The rfe_read_tx_rf_port reads the current RF port configured for TX hdl

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_rf_port pointer to be updated with current RF port
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_tx_rf_port( rfe_t *p_rfe, skiq_rf_port_t *p_rf_port );

/*****************************************************************************/
/** The rfe_write_tx_rf_port writes the RF port for the TX hdl

    @note This may result in the RFIC to handle port mapping to update,
    refer to @ref rfe_read_rfic_port_for_rx_hdl and 
    @ref rfe_read_rfic_port_for_tx_hdl

    @param[in] p_rfe pointer to the RFE instance
    @param[in] rf_port RF port to configure hdl to use 
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_tx_rf_port( rfe_t *p_rfe, skiq_rf_port_t rf_port );

/*****************************************************************************/
/** The rfe_read_rf_port_config_avail reads the RF port configuration options
    supported by the RFE.  The RF port configuration controls the Rx/Tx 
    capabilities for a given RF port.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_fixed pointer to flag indicating if fixed RF port config supported
    @param[out] p_trx pointer to flag indicating if TRX RF port config supported
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rf_port_config_avail( rfe_t *p_rfe, bool *p_fixed, bool *p_trx );

/*****************************************************************************/
/** The rfe_write_rf_port_config() function allows the RF port configuration to 
    be configured.  

    @note This may result in the RFIC to handle port mapping to update,
    refer to @ref rfe_read_rfic_port_for_rx_hdl and 
    @ref rfe_read_rfic_port_for_tx_hdl

    @param[in] p_rfe pointer to the RFE instance
    @param[in] config [::skiq_rf_port_config_t] RF port configuration to apply
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfe_write_rf_port_config( rfe_t *p_rfe,
                                  skiq_rf_port_config_t config );

/*****************************************************************************/
/** The rfe_read_rf_port_operation() function reads the operation mode of the
    RF port(s).  If the transmit flag is set, then the port(s) are configured to
    transmit, otherwise it is configured for receive.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_transmit pointer to flag indicating whether to transmit or receive
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfe_read_rf_port_operation( rfe_t *p_rfe,
                                    bool *p_transmit );

/*****************************************************************************/
/** The rfe_write_rf_port_operation() function sets the operation mode of the
    RF port(s) to either transmit or receive.  If the transmit flag is set, then
    the port(s) are configured to transmit, otherwise it is configured for
    receive.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] transmit flag indicating whether to transmit or receive
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfe_write_rf_port_operation( rfe_t *p_rfe,
                                     bool transmit );

/*****************************************************************************/
/** The rfe_read_bias_index() function reads the currently applied bias
    current index.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_bias_index pointer to where to store the current index
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfe_read_bias_index( rfe_t *p_rfe, uint8_t *p_bias_index );

/*****************************************************************************/
/** The rfe_write_bias_index() function configures the currently applied
    bias index to the value specified.

    @param[in] p_rfe pointer to the RFE instance
    @param[in] bias_index bias index to apply
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfe_write_bias_index( rfe_t *p_rfe, uint8_t bias_index );

/*****************************************************************************/
/** The rfe_read_rx_stream_hdl_conflict() function populates a list of 
    conflicting handles based on the handle specified.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_conflicting_hdls [::skiq_rx_hdl_t] array of handles that conflict
    @param[out] p_num_hdls pointer of where to store number of handles in conflict list
    @return int32_t status where 0=success, anything else is an error
*/
int32_t rfe_read_rx_stream_hdl_conflict( rfe_t *p_rfe,
                                         skiq_rx_hdl_t *p_conflicting_hdls,
                                         uint8_t *p_num_hdls );


/*****************************************************************************/
/** The rfe_enable_rx_chan function is responsible for configuring the 
    enable state of the channel specified in the RF ID.

    @param[in] p_rfe pointer to the RF IC instance
    @param[in] enable flag indicating the channel enable state

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_enable_rx_chan( rfe_t *p_rfe, bool enable );

/*****************************************************************************/
/** The rfe_enable_tx_chan function is responsible for configuring the 
    enable state of the channel specified in the RF ID.

    @param[in] p_rfe pointer to the RF IC instance
    @param[in] enable flag indicating the channel enable state

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_enable_tx_chan( rfe_t *p_rfe, bool enable );

/*****************************************************************************/
/** The rfe_write_rx_freq_hop_list function is responsible for configuring the 
    RFE to support the frequencies in the hopping list.

    @param[in] p_rfe pointer to the RF IC instance
    @param[in] num_freq number of frequencies in list
    @param[in] freq_list frequency hopping list

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_rx_freq_hop_list( rfe_t *p_rfe, uint16_t num_freq, uint64_t freq_list[] );

/*****************************************************************************/
/** The rfe_write_tx_freq_hop_list function is responsible for configuring the 
    RFE to support the frequencies in the hopping list.

    @param[in] p_rfe pointer to the RF IC instance
    @param[in] num_freq number of frequencies in list
    @param[in] freq_list frequency hopping list

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_write_tx_freq_hop_list( rfe_t *p_rfe, uint16_t num_freq, uint64_t freq_list[] );    

/**************************************************************************************************/
/** rfe_config_custom_rx_filter() can be used in a factory setting to test the various control lines
    on the RFE of the part.

    The valid range for @a filt_index is 1 to 4, inclusive and maps directly to the filters
    enumeration in the schematic of the part.

    The @p hpf_lpf_setting argument is applied directly to the I/O expander.  The most significant 4
    bits are the HPF (D,C,B,A) settings while the least significant 4 bits are the LPF (D,C,B,A)
    settings.

    The @p rx_fltr1_sw argument controls which configuration of the FL1 is used.  A value of 1
    asserts the CTL_SW1 pin while a value of 2 asserts the CTL_SW2 pin.  The argument is only
    applicable when the specified @a filt_index is 1.  The caller should specify @a rx_fltr1_sw to
    be 0 whenever @a filt_index is not 1.

    The @p rfic_port argument specifies which RX port on the AD9361 to use.  It also configures the
    RF switches to select the correct RF path.  The integer mapping is as follows:
    - 0 = RX1A (2620 MHz to 6000 MHz recommended)
    - 1 = RX1B (  75 MHz to  800 MHz recommended)
    - 2 = RX1C ( 800 MHz to 2620 MHz recommended)

    @param[in] filt_index index of desired filter to change (valid range 1-4)
    @param[in] hpf_lpf_setting high-pass and low-pass filter settings
    @param[in] rx_fltr1_sw FL1 configuration selection
    @param[in] rfic_port RX port on the AD9361 to use

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP configuration not available for part
    @retval -EINVAL at least one of the function arguments is invalid
    @retval -EIO I/O error configuring RX filter
    @retval other Internal error
 */
int32_t rfe_config_custom_rx_filter( rfe_t *p_rfe,
                                     uint8_t filt_index,
                                     uint8_t hpf_lpf_setting,
                                     uint8_t rx_fltr1_sw,
                                     uint8_t rfic_port );

/*****************************************************************************/
/** The rfe_read_rfic_port_for_rx_hdl function is responsible for determining
    the mapping of RFIC port based on the RX handle provided.

    @param[in] p_rfe pointer to the RF IC instance
    @param[out] p_port pointer to the RFIC port

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rfic_port_for_rx_hdl( rfe_t *p_rfe,
                                       uint8_t *p_port );

/*****************************************************************************/
/** The rfe_read_rfic_port_for_tx_hdl function is responsible for determining
    the mapping of RFIC port based on the TX handle provided.

    @param[in] p_rfe pointer to the RF IC instance
    @param[out] p_port pointer to the RFIC port

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_read_rfic_port_for_tx_hdl( rfe_t *p_rfe,
                                       uint8_t *p_port );

/*****************************************************************************/
/** The rfe_swap_rx_port_config function is responsible for swapping the 
    configuration of the "a_port" and "b_port".  So, upon successful
    completion of this function, the a_port is configured identical to the
    b_port prior to calling the function (and visa versa).

    @param[in] p_rfe pointer to the RFE instance
    @param[in] a_port RFIC port to be swapped with @pb_port
    @param[in] b_port RFIC port to be swapped with @pa_port

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_swap_rx_port_config( rfe_t *p_rfe, uint8_t a_port, uint8_t b_port );

/*****************************************************************************/
/** The rfe_swap_tx_port_config function is responsible for swapping the 
    configuration of the "a_port" and "b_port".  So, upon successful
    completion of this function, the from_port is configured identical to the
    to_port prior to calling the function (and visa versa).

    @param[in] p_rfe pointer to the RFE instance
    @param[in] a_port RFIC port to be swapped with @pb_port
    @param[in] b_port RFIC port to be swapped with @pa_port

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t rfe_swap_tx_port_config( rfe_t *p_rfe, uint8_t a_port, uint8_t b_port );

/*****************************************************************************/
/** The rfe_read_rf_capabilities() reads the rf capabilities of the RFE layer.

    @param[in] p_rfe pointer to the RFE instance
    @param[out] p_rf_capabilities pointer to store rf_capabilities
    @return int32_t
    @retval 0 success
    @retval -ENOTSUP configuration not available for part

*/
int32_t rfe_read_rf_capabilities( rfe_t *p_rfe, skiq_rf_capabilities_t *p_rf_capabilities );

/**************************************************************************************************/
/** The rfe_override_tx_pa() function allows the caller to override the PA state of the
    specified RF identifier by specifying a pin value (high, low, or high-z).  Availability, pre/post
    conditions specific to the actual hardware implementation.

    @ingroup factoryfunctions

    @param[in] p_id Pointer to RF identifier struct
    @param[in] pin_value [::skiq_pin_value_t] desired pin value for TX_AMP_MANUAL_BIAS_ON_N

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP ATE_SUPPORT is not enabled for the library
    @retval -EINVAL Specified @a pin_value is unknown or unsupported
    @retval -EIO Input/output error communicating with the I/O expander
*/
int32_t rfe_override_tx_pa( rfe_t *p_rfe,
                            skiq_pin_value_t pin_value );

#endif  /* __SIDEKIQ_RFE_H__ */
