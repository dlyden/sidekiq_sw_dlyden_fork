/**
 * @file   rfic_adrv9002.h
 * @author <info@epiqsolutions.com>
 * @date   Wed Jun 17 14:59:03 2020
 *
 * @brief  This file contains the public interface of the RFIC implementation for the ADRV9002.
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#ifndef __RFIC_ADRV9002_H__
#define __RFIC_ADRV9002_H__

#include "sidekiq_rfic.h"

/* only write to the first 16 bits of the GPIO register on ADRV9002 */
#define RFIC_ADRV9002_GPIO_MASK     (0xFFFF0000)

extern int32_t navassa_enable_prbs_mode(uint8_t card);
extern int32_t navassa_disable_prbs_mode(rf_id_t *p_id);
extern int32_t navassa_check_prbs_status(uint8_t card, uint8_t nr_handles, uint32_t *prbs_errors);

/** @brief function pointers for ADRV9002 RFIC implementation */
extern rfic_functions_t rfic_adrv9002_funcs;

#endif  /* __RFIC_ADRV9002_H__ */
