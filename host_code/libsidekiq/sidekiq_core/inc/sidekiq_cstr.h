#ifndef __SIDEKIQ_CSTR_H__
#define __SIDEKIQ_CSTR_H__

/**
 * @file   sidekiq_cstr.h
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 14:39:29 2018
 * 
 * @brief  
 * 
 * <pre>
 * Copyright 2018-2019 Epiq Solutions, All Rights Reserved
 * 
 * 
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <stdbool.h>

#include "sidekiq_types.h"
#include "sidekiq_types_private.h"
#include "sidekiq_xport_types.h"

#include "sidekiq_rfe.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** EXTERN FUNCTIONS  *****/

/* Standard Types */
const char *bool_cstr( bool flag );

/* Standard Sidekiq Types */
const char *part_cstr( skiq_part_t part );
const char *rx_hdl_cstr( skiq_rx_hdl_t hdl );
const char *tx_hdl_cstr( skiq_tx_hdl_t hdl );
const char *filt_cstr( skiq_filt_t filt );
const char *rf_port_cstr( skiq_rf_port_t rf_port );
const char *rf_port_config_cstr( skiq_rf_port_config_t config );
const char *pps_source_cstr( skiq_1pps_source_t source );
const char *fpga_tx_fifo_size_cstr( skiq_fpga_tx_fifo_size_t fifo_size );
char hw_rev_cstr( skiq_hw_rev_t rev );
const char *hw_vers_cstr( skiq_hw_vers_t vers );
const char *product_vers_cstr( skiq_product_t product_vers );
const char *rfe_lna_state_cstr( rfe_lna_state_t state );
const char *fpga_device_cstr( skiq_fpga_device_t fpga_device );
const char *fmc_carrier_cstr( skiq_fmc_carrier_t fmc_carrier );
const char *xport_type_cstr( skiq_xport_type_t type );
const char *xport_level_cstr( skiq_xport_init_level_t level );
const char *ref_clock_select_cstr( skiq_ref_clock_select_t ref_clock );


#endif  /* __SIDEKIQ_CSTR_H__ */
