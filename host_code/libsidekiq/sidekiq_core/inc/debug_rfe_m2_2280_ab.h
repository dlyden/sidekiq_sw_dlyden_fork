#ifndef __DEBUG_RFE_M2_2280_AB_H__
#define __DEBUG_RFE_M2_2280_AB_H__

/**
 * @file   debug_rfe_m2_2280_ab.h
 * @date   Thu Aug 15 09:25:36 CDT 2019
 *
 * @brief This file contains debug macros / functions that should only be #included by the
 * rfe_m2_2280_ab.c source file.
 *
 * <pre>
 * Copyright 2019-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static const char *
stretch_lna_state_cstr( stretch_rfe_lna_state_t state )
{
    const char * p_lna_state_str =
        (state == stretch_lna_bypassed) ? "bypassed" :
        (state == stretch_lna_disabled) ? "disabled" :
        (state == stretch_lna_enabled_fixed) ? "enabled (fixed)" :
        (state == stretch_lna_enabled_trx) ? "enabled (trx)" :
        "unknown";

    return p_lna_state_str;
}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static const char *
stretch_pa_state_cstr( stretch_rfe_pa_state_t state )
{
    const char * p_pa_state_str =
        (state == stretch_pa_disabled) ? "disabled" :
        (state == stretch_pa_enabled_fixed) ? "enabled (fixed)" :
        (state == stretch_pa_enabled_trx) ? "enabled (trx)" :
        "unknown";

    return p_pa_state_str;
}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static const char *
rx_sel_cstr( uint32_t rx_sel )
{
    rx_sel &= RX_SEL_IO_EXP_MASK;

    const char *p_rx_sel_str =
        ( rx_sel == RX1A_SEL ) ? "RX1A" :
        ( rx_sel == RX1B_SEL ) ? "RX1B" :
        ( rx_sel == RX1C_SEL ) ? "RX1C" :
        "none";

    return p_rx_sel_str;
}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static void
stretch_print_rx_filters(void)
{
    uint8_t i;

    debug_print("%27s --  low to high -- PORT -- 20p0 20p1 20p2 21p0 21p1 22p0 22p1\n", "filter" );
    for (i = 0; i < ARRAY_SIZE(rx_filter_selections); i++)
    {
        filter_selection_t *f = &(rx_filter_selections[i]);

        debug_print("%27s -- %4u to %4u -- %s -- %02X   %02X   %02X   %02X   %02X   %02X   %02X\n",
                    SKIQ_FILT_STRINGS[f->filter], f->lo, f->hi,
                    (f->ad9361_port == ad9361_rx_port_a) ? "RX1A" :
                    (f->ad9361_port == ad9361_rx_port_b) ? "RX1B" :
                    (f->ad9361_port == ad9361_rx_port_c) ? "RX1C" : "unk",
                    f->x20_port0_valu, f->x20_port1_valu, f->x20_port2_valu,
                    f->x21_port0_valu, f->x21_port1_valu,
                    f->x22_port0_valu, f->x22_port1_valu);
    }
}
#else
static void
stretch_print_rx_filters(void) {}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static void
stretch_print_tx_filters(void)
{
    uint8_t i;

    debug_print("%27s --  low to high -- 20p0 mask\n", "filter" );
    for (i = 0; i < ARRAY_SIZE(tx_filter_selections); i++)
    {
        filter_selection_t *f = &(tx_filter_selections[i]);

        debug_print("%27s -- %4u to %4u --  %02X   %02X\n",
                    SKIQ_FILT_STRINGS[f->filter], f->lo, f->hi,
                    f->x20_port0_valu, f->x20_port0_mask);
    }
}
#else
static void
stretch_print_tx_filters(void) {}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static void
stretch_print_pl_trxrx_state( uint8_t card )
{
    uint32_t value;
    int32_t status = 0;

    debug_print_plain(YEL("\t##########   PL T/R: "));

    status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
    if ( status == 0 )
    {
        debug_print_plain("%s\n",
                          (RBF_GET(value, PL_CTRL_TDD_TXRX_N) > 0) ? "transmit" : "receive");
    }
    else
    {
        debug_print_plain("unknown\n");
    }
}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static void
stretch_print_io_pin_states( uint8_t card )
{
    uint32_t output = 0, config = 0;
    int i;
    const char *pins[16] = {
        "TX_FLT1_SEL",
        "TX_FLT2_SEL",
        "TX_AMP_MANUAL_BIAS_ON_N",
        "TX1A_SEL",
        "TX1B_SEL",
        "RXTX_ANT1_TOG_EN",
        "RXTX_ANT1_TOG_EN_N",
        "RXTX_AMPS_TOG_EN_N",
        "RX_ANT2_SEL",
        "LNA1_BYPASS_TOG",
        "LNA2_BYPASS_TOG",
        "LNA1_MANUAL_BIAS_EN_N",
        "LNA2_MANUAL_BIAS_EN_N",
        "RX1A_SEL",
        "RX1B_SEL",
        "RX1C_SEL",
    };

    pcal6524_io_exp_read_output( card, M2_2280_IOE_I2C_BUS, x20_ADDR, &output );
    pcal6524_io_exp_read_direction( card, M2_2280_IOE_I2C_BUS, x20_ADDR, &config );

    for (i = 0; i < 16; i++)
    {
        debug_print_plain(YEL("\t########## %25s is") " %s\n", pins[i],
                          ((config & 1) > 0) ? "Hi-Z" :
                          ((output & 1) > 0) ? "1" : "0");
        config >>=1;
        output >>=1;
    }
}
#endif  /* DEBUG_PRINT_ENABLED */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
#define stretch_print_states(_card)                                     \
    do {                                                                \
        debug_print("\n");                                              \
        stretch_print_pl_trxrx_state(_card);                            \
        debug_print_plain(YEL("\t##########  RF PORT: ") "%s,%s,op=%s\n", \
                    current_rf_port_state[_card].valid ? "valid" : "invalid", \
                    !current_rf_port_state[_card].valid ? "-invalid-" : \
                    (current_rf_port_state[_card].config == skiq_rf_port_config_fixed) ? "fixed" : "T/R", \
                    !current_rf_port_state[_card].valid ? "-invalid-" : \
                    bool_cstr( current_rf_port_state[_card].operation)); \
        debug_print_plain(YEL("\t########## LNA1 RFE:") "%-10s " YEL("STRETCH:") " %s\n", \
                          rfe_lna_state_cstr( rfe_lna_state( _card, LNA1 ) ), \
                          stretch_lna_state_cstr( current_lna_state[_card][LNA1].state ) ); \
        debug_print_plain(YEL("\t########## LNA2 RFE:") "%-10s " YEL("STRETCH:") " %s\n", \
                          rfe_lna_state_cstr( rfe_lna_state( _card, LNA2 ) ), \
                          stretch_lna_state_cstr( current_lna_state[_card][LNA2].state ) ); \
        debug_print_plain(YEL("\t##########   PA RFE:") "%-10s " YEL("STRETCH:") " %s\n", \
                          bool_cstr( rfe_pa_state( _card ) ),           \
                          stretch_pa_state_cstr( current_pa_state[_card].state ) ); \
        stretch_print_io_pin_states(_card);                             \
    } while (0)
#else
#define stretch_print_states(_card) do { } while (0)
#endif  /* DEBUG_PRINT_ENABLED */


#endif  /* __DEBUG_RFE_M2_2280_AB_H__ */
