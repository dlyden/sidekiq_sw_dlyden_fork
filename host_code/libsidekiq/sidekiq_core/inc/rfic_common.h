/**
 * @file   rfic_common.h
 * @author  <info@epiqsolutions.com>
 * @date   Fri May 10 11:07:56 EDT 2019
 *
 * @brief  A collection of common functions for use across RFIC implementations.
 *
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 *
 */

#ifndef __RFIC_COMMON_H__
#define __RFIC_COMMON_H__

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>
#include "sidekiq_types.h"
#include "sidekiq_rf.h"         /* for rf_id_t */

/***** STRUCTS *****/

struct result {
    uint8_t dec_rate;
    int32_t delta_rate, delta_bw;
};

typedef struct
{
    uint32_t dev_clock;
    uint32_t sample_rate;
    uint32_t bandwidth;

} rate_config_t;


/***** MACROs *****/

#define RESULTS_ARRAY(_name,_nr)                \
    struct result _name[_nr] = {                \
        [ 0 ... ((_nr)-1) ] = {                 \
            .dec_rate = 0,                      \
            .delta_rate = INT32_MAX,            \
            .delta_bw = INT32_MAX,              \
        },                                      \
    }


/***** PROTOTYPES *****/


void
rfic_consider_profile( uint8_t card,
                       skiq_rx_hdl_t hdl,
                       uint32_t request_rate, uint32_t request_bw,
                       uint32_t profile_rate, uint32_t profile_bw,
                       uint8_t nr_stages_available,
                       uint32_t half_max_sample_clk_hz,
                       struct result *p_result );

int32_t
rfic_find_best_result( struct result results[],
                       uint8_t nr_results,
                       uint8_t *p_best_index );

int32_t
rfic_store_rx_rate_config( rf_id_t *p_id,
                           uint32_t sample_rate,
                           uint32_t bandwidth );

int32_t
rfic_store_tx_rate_config( rf_id_t *p_id,
                           uint32_t sample_rate,
                           uint32_t bandwidth );

void
rfic_clear_rate_config( rf_id_t *p_id );

bool
rfic_hdl_has_rx_rate_config( rf_id_t *p_id );


/**************************************************************************************************/
/** rfic_get_rx_rate_config() populates @a *p_sample_rate and @a *p_bandwidth with the sample rate
    and bandwidth configuration associated with the RF ID.  The caller may pass NULL as either of
    the arguments to indicate that they are not interested in the value.

    @param[in] p_id pointer to the RF ID of interest
    @param[out] p_sample_rate reference to uint32_t to populate with the configured sample rate
    @param[out] p_bandwidth reference to uint32_t to populate with the configured bandwidth

    @return int32_t
    @retval 0 Success
    @retval -EINVAL p_id->hdl is invalid / out-of-bounds
 */
int32_t
rfic_get_rx_rate_config( rf_id_t *p_id,
                         uint32_t *p_sample_rate,
                         uint32_t *p_bandwidth );


/**************************************************************************************************/
/** rfic_get_tx_rate_config() populates @a *p_sample_rate and @a *p_bandwidth with the sample rate
    and bandwidth configuration associated with the RF ID.  The caller may pass NULL as either of
    the arguments to indicate that they are not interested in the value.

    @param[in] p_id pointer to the RF ID of interest
    @param[out] p_sample_rate reference to uint32_t to populate with the configured sample rate
    @param[out] p_bandwidth reference to uint32_t to populate with the configured bandwidth

    @return int32_t
    @retval 0 Success
    @retval -EINVAL p_id->hdl is invalid / out-of-bounds
 */
int32_t
rfic_get_tx_rate_config( rf_id_t *p_id,
                         uint32_t *p_sample_rate,
                         uint32_t *p_bandwidth );


#endif  /* __RFIC_COMMON_H__ */
