#ifndef __RFIC_RPC_H__
#define __RFIC_RPC_H__

#include "sidekiq_rfic.h"

/*****************************************************************************/
/** @file rfic_rpc.h
 
 <pre>
 Copyright 2021 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFIC implementation 
    using RPC.
*/

/*****************************************************************************/
/** The handle_rpc_rfic_reset_and_init function executes rfic_reset_and_init
    on the server.  The RFIC instance is obtained from the card argument.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_reset_and_init( uint8_t card );

/*****************************************************************************/
/** The handle_client_rfic_write_rx_gain_mode function executes rfic_write_rx_gain_mode
    on the server.  The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_write_rx_gain_mode( uint8_t card, skiq_rx_hdl_t hdl, skiq_rx_gain_t gain_mode );

/*****************************************************************************/
/** The handle_rpc_rfic_read_rx_gain_mode function executes rfic_read_rx_gain_mode
    on the server.  The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_read_rx_gain_mode( uint8_t card,
                                           skiq_rx_hdl_t hdl, 
                                           skiq_rx_gain_t *p_gain_mode );

/*****************************************************************************/
/** The handle_rpc_rfic_write_rx_sample_rate_and_bandwidth function executes 
    rfic_write_rx_sample_rate_and_bandwidth on the server.  The RFIC instance is 
    obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_write_rx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_rx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth );

/*****************************************************************************/
/** The handle_rpc_rfic_read_rx_sample_rate function executes rfic_read_rx_sample_rate 
    on the server.  The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_read_rx_sample_rate( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate );

/*****************************************************************************/
/** The handle_client_rfic_read_rx_chan_bandwidth function executes 
    rfic_read_rx_chan_bandwidth on the server.  The RFIC instance is obtained 
    from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_read_rx_chan_bandwidth( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth );

/*****************************************************************************/
/** The handle_rpc_rfic_write_tx_sample_rate_and_bandwidth function executes 
    rfic_write_tx_sample_rate_and_bandwidth on the server.  The RFIC instance is 
    obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_write_tx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_tx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth );

/*****************************************************************************/
/** The handle_rpc_rfic_read_rx_sample_rate function executes rfic_read_rx_sample_rate 
    on the server.  The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_read_tx_sample_rate( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate );

/*****************************************************************************/
/** The handle_client_rfic_read_tx_chan_bandwidth function executes 
    rfic_read_tx_chan_bandwidth on the server.  The RFIC instance is obtained 
    from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_read_tx_chan_bandwidth( uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth );


/*****************************************************************************/
/** The handle_rpc_rfic_write_tx_quadcal_mode function executes rfic_write_tx_quadcal_mode on the server.  
    The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_write_tx_quadcal_mode(  uint8_t card, 
                                                skiq_tx_hdl_t hdl, 
                                                skiq_tx_quadcal_mode_t mode );

/*****************************************************************************/
/** The handle_rpc_rfic_write_rx_cal_mode function executes rfic_write_rx_cal_mode on the server.  
    The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_write_rx_cal_mode(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            skiq_rx_cal_mode_t mode );

/*****************************************************************************/
/** The handle_rpc_rfic_write_rx_cal_mask function executes rfic_write_rx_cal_mask on the server.  
    The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_write_rx_cal_mask(  uint8_t card, 
                                            skiq_rx_hdl_t hdl, 
                                            uint32_t mask );

/*****************************************************************************/
/** The handle_rpc_rfic_read_rx_cal_mask function executes rfic_read_rx_cal_mask on the server.  
    The RFIC instance is obtained from the card and hdl arguments.

    refer to sidekiq_rfic function for parameter details
*/
int32_t handle_rpc_rfic_read_rx_cal_mask( uint8_t card,                                                                                                      
                                          skiq_rx_hdl_t hdl,
                                          uint32_t *p_mask );


#endif  /* __RFIC_RPC_H__ */
