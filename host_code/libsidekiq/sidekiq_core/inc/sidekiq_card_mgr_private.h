#ifndef SIDEKIQ_CARD_MGR_PRIVATE_H
#define SIDEKIQ_CARD_MGR_PRIVATE_H

/*****************************************************************************/
/** @file sidekiq_card_mgr_private.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the private typedefs used internally by the 
    Sidekiq card manager.
*/

/***** INCLUDES *****/
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/types.h>

#include "sidekiq_card_mgr.h"
#include "sidekiq_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/***** DEFINES *****/

/** @brief name of shared memory that stores mutex info */
#define CARD_MGR_MUTEXES_STR "/skiq_card_mgr_mutexes"
/** @brief name of shared memory that stores card/management info */
#define CARD_MGR_STR "/skiq_card_mgr"

/** @brief name of shared memory that stores extesion v3 info */
#define CARD_MGR_EXT_V3_STR "/skiq_card_mgr_ext_v3"    

/** @brief name of shared memory that stores extension v4 info */
#define CARD_MGR_EXT_V4_STR "/skiq_card_mgr_ext_v4"

/** @brief name of shared memory that stores extension v5 info */
#define CARD_MGR_EXT_V5_STR "/skiq_card_mgr_ext_v5"

/** @brief name of shared memory that stores extension v6 info */
#define CARD_MGR_EXT_V6_STR "/skiq_card_mgr_ext_v6"

/** @brief name of tmp file created during initialization */
#define CARD_MGR_INIT_STR "/tmp/skiq_card_mgr_init"

/** @brief version info of shared memory data structure */
#define CARD_MGR_API_VERSION (6)

/* before CARD_MGR_API_VERSION 4 there was a maximum of 4 cards */
#define LEGACY_SKIQ_MAX_NUM_CARDS         (4)

/***** TYPEDEFS *****/


/*****************************************************************************/
/** The card_mgr_create_card_mgr function is responsible for creating and 
    initializing the shared memory neccessary for the card manager.  This 
    should only ever need to be called once prior to any Sidekiq use.

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_create_card_mgr( void );

/*****************************************************************************/
/** The card_mgr_destroy_card_mgr function is responsible for destroying the 
    the shared memory of card manager.  This should only ever be called when
    trying to force a new probe to occur.  This is primarily available for
    developer's only.

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_destroy_card_mgr( void );

/*****************************************************************************/
/** The card_mgr_xport_probe_card function is responsible for creating a 
    probe card # based on the skiq_xport_id_t info passed in.  Some of the 
    probing / init functions require a mapping from "card" to "XPORT UID"
    in order to obtain the info necessary to determine the information about
    the specific card (like serial #).  This function is not intended to be 
    robust or error proof as it should only ever be used by a "trusted" source
    during a probing.

    @param p_xport_id ID to use for probing
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_xport_probe_card( skiq_xport_id_t *p_xport_id );

/*****************************************************************************/
/** The card_mgr_probe function is responsible for probing and registering
    cards detected of any XPORT type.

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_probe( void );

#ifdef __cplusplus
}
#endif

#endif
