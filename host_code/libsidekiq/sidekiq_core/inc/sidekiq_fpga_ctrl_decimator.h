/**
 * @file   sidekiq_fpga_ctrl_decimator.h
 * @author  <info@epiqsolutions.com>
 * @date   Mon Mar  4 16:24:22 2019
 * 
 * @brief  Provides access to the decimator instances in the FPGA (if present)
 * 
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 * 
 */

#ifndef __SIDEKIQ_FPGA_CTRL_DECIMATOR_H__
#define __SIDEKIQ_FPGA_CTRL_DECIMATOR_H__

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>
#include "sidekiq_types.h"


/***** PROTOTYPES *****/


/**************************************************************************************************/
/** The decimator_init_config() function operates on the decimator associated with the specified
    Sidekiq card.  If the decimator is present, it will first reset the decimator, then write the
    coefficients of a Sinc decimation filter for all available handles.

    @param[in] card Sidekiq card number of interest

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
decimator_init_config( uint8_t card );


/**************************************************************************************************/
/** The decimator_reset_config() function operates on the decimator associated with the specified
    Sidekiq card.  If the decimator is present, it will reset and disable the decimator.

    @param[in] card Sidekiq card number of interest

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
decimator_reset_config( uint8_t card );


/**************************************************************************************************/
/** The decimator_nr_stages() function returns the number of decimation stages present and available
    for a specified card and receive handle.

    @return uint32_t
    @retval number of decimation stages available, inclusive.  0 stages indicates no decimation available
 */
uint32_t
decimator_nr_stages( uint8_t card,
                     skiq_rx_hdl_t hdl );


/**************************************************************************************************/
/** decimator_enable_if_present() enables the decimator path for the specified handle and card.

    @warning This function @b ignores the cases where a decimator is not available for the specified
    handle and card and subsequently return success

    @param[in] card Sidekiq card number of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest

    @return int32_t

    @retval 0 Success
    @retval -EPROTO Internal failure occurred
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
decimator_enable_if_present( uint8_t card,
                             skiq_rx_hdl_t hdl );


/**************************************************************************************************/
/** decimator_disable_if_present() disables the decimator path for the specified handle and card.

    @warning This function @b ignores the cases where a decimator is not available for the specified
    handle and card and subsequently return success

    @param[in] card Sidekiq card number of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest

    @return int32_t

    @retval 0 Success
    @retval -EPROTO Internal failure occurred
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
decimator_disable_if_present( uint8_t card,
                              skiq_rx_hdl_t hdl );


/**************************************************************************************************/
/** The decimator_set_rate() function operates on the decimator associated with the specified
    Sidekiq card and receive handle.  The provided argument @a rate specifies the number of
    decimation stages to use during decimation and thus defines the equivalent sample rate.

    @param[in] card Sidekiq card number of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] rate RATE where decimated sample rate SR is 1 / ( pow(2,RATE) )

    @return int32_t
    @retval 0 Success
    @retval -EINVAL Specified rate is not less than nor equal to value returned by decimator_nr_stages(card,hdl)
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
decimator_set_rate( uint8_t card,
                    skiq_rx_hdl_t hdl,
                    uint8_t rate );


/**************************************************************************************************/
/** The decimator_get_rate() function stores the current decimation rate to the provided pointer
    reference @a p_rate.

    @param[in] card Sidekiq card number of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_rate pointer to RATE where decimated sample rate SR is 1 / ( pow(2,RATE) )

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
decimator_get_rate( uint8_t card,
                    skiq_rx_hdl_t hdl,
                    uint8_t *p_rate );


/**************************************************************************************************/
/** The decimator_only_supports_half_max_rate() function returns whether or not it supports
    decimating samples up to half of the maximum RFIC sample rate or not.  The FPGA implementation
    may consider saving on logical resources and/or ease timing constraints by supporting only up to
    half of the maximum sampling rate.

    @note The RFIC's maximum sample rate obviously depends on the RFIC, so consult the RFIC User
    Guide if needed.

    @param[in] card Sidekiq card number of interest
    @param[in] hdl Sidekiq receive handle of interest

    @return bool
    @retval true Decimator only supports decimating sample rates up to half of the maximum RFIC sample rate
    @retval false Decimator supports decimating sample rates up to the maximum RFIC sample rate
 */
bool
decimator_only_supports_half_max_rate( uint8_t card,
                                       skiq_rx_hdl_t hdl );


#endif  /* __SIDEKIQ_FPGA_CTRL_DECIMATOR_H__ */
