#ifndef __RFIC_X2_H__
#define __RFIC_X2_H__

/***** INCLUDES *****/

#include "sidekiq_rfic.h"

/***** DEFINES *****/

#define RFIC_X2_GPIO_MASK (0x7FFFF)

/***** EXTERN DATA *****/

/*****************************************************************************/
/** @file rfic_x2.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFIC implementation 
    for the Sidekiq X2, which utilizes the AD9371 (mykonos) and AD9528.
*/
extern rfic_functions_t rfic_x2_funcs;

#endif /* __RFIC_X2_H__ */
