#ifndef __SIDEKIQ_PRIVATE_H__
#define __SIDEKIQ_PRIVATE_H__

/*! \file sidekiq_private.h
 * \brief 
 *  
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * 
 * 
 *</pre>*/

/***** INCLUDES *****/

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>

#include "sidekiq_types.h"
#include "sidekiq_types_private.h"
#include "sidekiq_api.h"
#include "sidekiq_rfe.h"
#include "sidekiq_rfic.h"
#include "sidekiq_fpga_ctrl.h"
#include "sidekiq_cstr.h"
#include "hw_iface.h"


/***** DEFINES *****/

#ifndef static_assert
    #ifndef __cplusplus
        #define GCC_VERSION (__GNUC__ * 10000 \
                            + __GNUC_MINOR__ * 100 \
                            + __GNUC_PATCHLEVEL__)
        #if (GCC_VERSION >= 40600)
            #define static_assert _Static_assert
        #endif
    #endif
#endif

#ifndef static_assert
    #define static_assert(expr, fmt) // no-op
#endif

/* MIN and MAX in C (https://stackoverflow.com/a/3437484) */
#ifndef MAX
#define MAX(a,b)                                \
    ({ __typeof__ (a) _a = (a);                 \
        __typeof__ (b) _b = (b);                \
        _a > _b ? _a : _b; })
#endif

#ifndef MIN
#define MIN(a,b)                                \
    ({ __typeof__ (a) _a = (a);                 \
        __typeof__ (b) _b = (b);                \
        _a < _b ? _a : _b; })
#endif

#define IN_RANGE_INCL(_arg,_low,_high)                  \
    ({ __typeof__ (_arg) __arg = (_arg);                \
        ( ( __arg >= _low ) &&                          \
          ( __arg <= _high ) ) ? true: false; })

/* The absolute difference between a and b without underflowing intermediate result */
#define ABS_DIFF(a,b)                           \
    ({ __typeof__ (a) _a = (a);                 \
        __typeof__ (b) _b = (b);                \
        _a > _b ? (_a - _b) : (_b - _a); })

/* returns whichever value is non-zero, giving preference to `a` */
#define NONZERO(a,b)                            \
    ({ __typeof__ (a) _a = (a);                 \
        __typeof__ (b) _b = (b);                \
        _a != 0 ? _a : _b; })

/// @brief Integer divide rounding up.
#define DIV_ROUND_UP(numerator, denominator)                            \
    ({ __typeof__ (numerator) _numerator = (numerator);                 \
        __typeof__ (denominator) _denominator = (denominator);          \
        ( (_numerator + (_denominator - 1)) / _denominator ); })

#define ROUND_UP(_numerator, _denominator)     (_ROUND_UP((_numerator),(_denominator)))
#define _ROUND_UP(_numerator, _denominator)    (_numerator + (_denominator - 1)) / _denominator

/** @brief A simple macro for comparing fimrware version (major, minor) */
#define FW_VERSION(a,b)       (((a) << 8) + (b))
/** @brief An inverse macro to FW_VERSION() for obtaining the major version */
#define FW_VERSION_MAJOR(v)   (((v) >> 8) & 0xFF)
/** @brief An inverse macro to FW_VERSION() for obtaining the minor version */
#define FW_VERSION_MINOR(v)   (((v) >> 0) & 0xFF)
/** @brief A simple macro for comparing FPGA version (major, minor) */
#define FPGA_VERSION(a,b,c)   (((a) << 16) + ((b) << 8) + (c))
/** @brief An inverse macro to FPGA_VERSION() for obtaining the major version */
#define FPGA_VERSION_MAJOR(v) (((v) >> 16) & 0xFF)
/** @brief An inverse macro to FPGA_VERSION() for obtaining the minor version */
#define FPGA_VERSION_MINOR(v) (((v) >>  8) & 0xFF)
/** @brief An inverse macro to FPGA_VERSION() for obtaining the patch version */
#define FPGA_VERSION_PATCH(v) (((v) >>  0) & 0xFF)

/** @brief A simple macro for creating a cstring from FPGA version (major, minor, patch) */
#define FPGA_VERSION_STR(a,b,c)         "v" #a "." #b "." #c

// FPGA version requirement for start streaming updates
#define FPGA_VERS_MINIMUM               FPGA_VERSION(3,0,0)
#define FPGA_VERS_MINIMUM_CSTR          FPGA_VERSION_STR(3,0,0)
#define FPGA_VERS_MAXIMUM               FPGA_VERSION(4,0,0)
#define FPGA_VERS_MAXIMUM_CSTR          FPGA_VERSION_STR(4,0,0)

// FPGA requirement for reading the board ID pins
#define FPGA_VERS_BOARD_ID_VALID        FPGA_VERSION(3,2,0)

// FPGA requirement for reading num TX chans
#define FPGA_VERS_TX_CHANS_VALID        FPGA_VERSION(3,3,0)

// FPGA requirement for reading num RX chans
#define FPGA_VERS_RX_CHANS_VALID        FPGA_VERSION(3,4,0)

// FPGA requirement for reading internal temperature
#define FPGA_VERS_INT_TEMP_VALID        FPGA_VERSION(3,4,0)

/* FPGA requirement for mapping TxA2 to TxB1 */
#define FPGA_VERS_A2_MAP_TO_B1          FPGA_VERSION(3,13,0)
#define FPGA_VERS_A2_MAP_TO_B1_CSTR     FPGA_VERSION_STR(3,13,0)

#define FPGA_VERS_A2_B1_META          FPGA_VERSION(3,17,1)
#define FPGA_VERS_A2_B1_META_CSTR     FPGA_VERSION_STR(3,17,1)

// FX2 requirement for setting enumeration delay
#define FX2_MAJ_ENUM_DELAY_REQ (2)
#define FX2_MIN_ENUM_DELAY_REQ (7)

// Golden image is using reserved version of 1.255
#define FPGA_VERS_GOLDEN                FPGA_VERSION(1,255,0)

// Invalid FPGA version number
#define FPGA_VERS_INVALID               FPGA_VERSION(0,0,0)

// The base address of the "Extended Product Info" section in the EEPROM for
// most platforms (with the exception of mPCIe and m.2 cards)
#define EEPROM_EXT_PRODUCT_INFO_BASE_ADDR  (0)

// The base address of the "Extended Product Info" section in the EEPROM for
// the mPCIe and m.2 cards (rev D & above)
#define EEPROM_EXT_PRODUCT_INFO_MPCIE_M2_BASE_ADDR  (0x3F70)

// The offset into the "Extended Product Info" EEPROM section that holds the part number
#define EEPROM_PART_NUM_OFFSET (0)
// The offset into the "Extended Product Info" EEPROM section that holds the revision information
#define EEPROM_REVISION_OFFSET (EEPROM_PART_NUM_OFFSET + SKIQ_PART_NUM_STRLEN)
// The offset into the "Extended Product Info" EEPROM section that holds the variant information
#define EEPROM_VARIANT_OFFSET (EEPROM_REVISION_OFFSET + SKIQ_REVISION_STRLEN)

// certain products require storage of default bias current setting
#define EEPROM_Z2_BIAS_CURRENT_ADDR (0x3FE4)
#define EEPROM_Z2_BIAS_CURRENT_LENGTH (1)
#define EEPROM_Z2_BIAS_CURRENT_UNCONFIGURED (0xFF)

/* This two byte width location is used in factory testing for platforms that support write
 * protecting their EEPROM.  For more information about EEPROM usage, navigate to
 * https://confluence.epiq.rocks/display/EN/Sidekiq+EEPROM+Memory
 *
 * On ::skiq_mpcie and ::skiq_m2, this location is used for a different function (USB enumeration
 * delay)
 */
#define EEPROM_WP_SCRATCH_ADDR          0x3FF7
#define EEPROM_WP_SCRATCH_LENGTH        2

/*
  use math MACROs to calculate number of bytes in a block when I/Q samples are packed:

  _x is in number of bytes

  1. Convert from number of bytes to number of words in a block
  2. Subtract number of header words to get number of payload words
  3. Determine number of payload words in a packed payload
  3a. Convert number of payload words into number of packed samples
  3b. Convert number of packed samples into number of valid payload words
  4. Add number of header words to get number of valid block words
  5. Convert from number of valid block words to number of valid block bytes

 */
#define RX_BLOCK_SIZE_STREAM_PACKED(_x)                                 \
    (                                                                   \
        (                                                               \
            SKIQ_NUM_WORDS_IN_PACKED_BLOCK(                             \
                SKIQ_NUM_PACKED_SAMPLES_IN_BLOCK(                       \
                    ( (_x) / sizeof( uint32_t ) ) - SKIQ_RX_HEADER_SIZE_IN_WORDS \
                )                                                       \
            ) + SKIQ_RX_HEADER_SIZE_IN_WORDS                            \
        ) * sizeof( uint32_t )                                          \
    )

/* These definitions are used by start_rx_streaming() to apply the two skiq_rx_stream_mode_t
 * enumerations.  These definitions specify a number of BYTES in the block. */
#define RX_BLOCK_SIZE_STREAM_HIGH_TPUT     (4096)
#define RX_BLOCK_SIZE_STREAM_HIGH_TPUT_PACKED                   \
    RX_BLOCK_SIZE_STREAM_PACKED(RX_BLOCK_SIZE_STREAM_HIGH_TPUT)
#define RX_BLOCK_SIZE_STREAM_BALANCED      (4096)
#define RX_BLOCK_SIZE_STREAM_BALANCED_PACKED                    \
    RX_BLOCK_SIZE_STREAM_PACKED(RX_BLOCK_SIZE_STREAM_BALANCED)
#define RX_BLOCK_SIZE_STREAM_LOW_LATENCY   (256)

/* Placeholder value for operating temperature until calibration data has a need to query actual
 * temperature for a look-up, in degrees Celsius */
#define OPER_TEMPER_PLACEHOLDER            (int8_t)(45)

/** @brief Frequency in hertz of default mPCIe Sidekiq reference clock. */
#define SKIQ_MPCIE_REF_CLOCK_DEFAULT (40e6)
/** @brief Frequency in hertz of default M2 Sidekiq reference clock. */
#define SKIQ_M2_REF_CLOCK_DEFAULT (40e6)
/** @brief Frequency in hertz of default M.2-2280 Sidekiq reference clock. */
#define SKIQ_M2_2280_REF_CLOCK_DEFAULT (40e6)
/** @brief Frequency in hertz of default Sidekiq NV100 reference clock. */
#define SKIQ_NV100_REF_CLOCK_DEFAULT (40e6)
/** @brief Frequency in hertz of default Z3u Sidekiq reference clock. */
#define SKIQ_Z3U_REF_CLOCK_DEFAULT (40e6)
/** @brief Frequency in hertz of default Z2 Sidekiq reference clock. */
#define SKIQ_Z2_REF_CLOCK_DEFAULT (40e6)
/** @brief Frequency in hertz of default X2 Sidekiq reference clock. */
#define SKIQ_X2_REF_CLOCK_DEFAULT (10e6)
/** @brief Frequency in hertz of default X4 Sidekiq reference clock. */
#define SKIQ_X4_REF_CLOCK_DEFAULT (10e6)

/* Signal Definitions */
#define SIGNAL_10MHZ                    10000000
#define SIGNAL_30_72MHZ                 30720000
#define SIGNAL_40MHZ                    40000000
#define SIGNAL_100MHZ                   100000000


#define INVALID_RF_PORT (0) // 0 isn't a valid RF port

// [0 ... size] = value is a GCC specific method of initializing an array
// https://gcc.gnu.org/onlinedocs/gcc/Designated-Inits.html
#define INIT_ARRAY(size, value)         { [0 ... ((size)-1)] = value }

#define SKIQ_CARD_PARAM_INITIALIZER                     \
    {                                                   \
        .init_level = skiq_xport_init_level_unknown,    \
        .part_type = skiq_part_invalid,                 \
        .part_fmc_carrier = skiq_fmc_carrier_unknown,   \
        .part_info = {                                  \
            .number_string = {'\0'},                    \
            .revision_string = {'\0'},                  \
            .variant_string = {'\0'},                   \
        },                                              \
        .xport = skiq_xport_type_unknown,               \
        .is_accelerometer_present = false,              \
        .card = SKIQ_MAX_NUM_CARDS,                     \
        .serial_string = {"\0"},                        \
    }

#define SKIQ_FPGA_PARAM_INITIALIZER                     \
    {                                                   \
        .fpga_device = skiq_fpga_device_unknown,        \
        .tx_fifo_size = skiq_fpga_tx_fifo_size_unknown, \
        .build_date = 0xFFFFFFFF,                       \
        .git_hash = 0xFFFFFFFF,                         \
        .sys_timestamp_freq = 0,                        \
        .version_major = 0,                             \
        .version_minor = 0,                             \
        .version_patch = 0,                             \
        .fpga_state = FPGA_STATE_VERSION_INACCESSIBLE,          \
    }

#define SKIQ_FW_PARAM_INITIALIZER                       \
    {                                                   \
        .is_present = false,                            \
        .enumeration_delay_ms = 0,                      \
        .version_major = 0,                             \
        .version_minor = 0,                             \
    }

#define SKIQ_RF_PARAM_INITIALIZER                                       \
    {                                                                   \
        .ref_clock_config = skiq_ref_clock_invalid,                     \
        .is_rf_port_fixed = false,                                      \
        .is_rf_port_tdd_supported = false,                              \
        .is_rf_port_trx_supported = false,                              \
        .num_rx_channels = 0,                                           \
        .rx_handles = INIT_ARRAY(skiq_rx_hdl_end, skiq_rx_hdl_end),     \
        .num_tx_channels = 0,                                           \
        .tx_handles = INIT_ARRAY(skiq_tx_hdl_end, skiq_tx_hdl_end),     \
        .ref_clock_freq = 0,                                            \
    }

#define SKIQ_RX_PARAM_INITIALIZER                                       \
    {                                                                   \
        .filters = {[0 ... (skiq_filt_max-1)] = skiq_filt_invalid},     \
        .atten_quarter_db_max = 0,                                      \
        .atten_quarter_db_min = 0,                                      \
        .gain_index_max = 0,                                            \
        .gain_index_min = 0,                                            \
        .iq_resolution = 0,                                             \
        .lo_freq_max = 0,                                               \
        .lo_freq_min = 0,                                               \
        .num_filters = 0,                                               \
        .sample_rate_max = 0,                                           \
        .sample_rate_min = 0,                                           \
        .num_fixed_rf_ports = 0,                                        \
        .fixed_rf_ports = {[0 ... (skiq_rf_port_max-1)] = skiq_rf_port_unknown}, \
        .num_trx_rf_ports = 0,                                        \
        .trx_rf_ports = {[0 ... (skiq_rf_port_max-1)] = skiq_rf_port_unknown}, \
    }

#define SKIQ_TX_PARAM_INITIALIZER                                       \
    {                                                                   \
        .filters = {[0 ... (skiq_filt_max-1)] = skiq_filt_invalid},     \
        .atten_quarter_db_max = 0,                                      \
        .atten_quarter_db_min = 0,                                      \
        .iq_resolution = 0,                                             \
        .lo_freq_max = 0,                                               \
        .lo_freq_min = 0,                                               \
        .num_filters = 0,                                               \
        .sample_rate_max = 0,                                           \
        .sample_rate_min = 0,                                           \
        .num_fixed_rf_ports = 0,                                        \
        .fixed_rf_ports = {[0 ... (skiq_rf_port_max-1)] = skiq_rf_port_unknown}, \
        .num_trx_rf_ports = 0,                                          \
        .trx_rf_ports = {[0 ... (skiq_rf_port_max-1)] = skiq_rf_port_unknown}, \
    }

#define SKIQ_PARAM_INITIALIZER                          \
    {                                                   \
        .card_param = SKIQ_CARD_PARAM_INITIALIZER,      \
        .fpga_param = SKIQ_FPGA_PARAM_INITIALIZER,      \
        .fw_param = SKIQ_FW_PARAM_INITIALIZER,          \
        .rf_param = SKIQ_RF_PARAM_INITIALIZER,          \
        .rx_param = { [0 ... (skiq_rx_hdl_end-1)] =     \
            SKIQ_RX_PARAM_INITIALIZER },                \
        .tx_param = { [0 ... (skiq_tx_hdl_end-1)] =     \
            SKIQ_TX_PARAM_INITIALIZER },                \
    }

/*!< free the specified pointer if it is not invalid */
#define FREE_IF_NOT_NULL( _ptr )                                        \
    do {                                                                \
        if ( NULL != (_ptr) ) { free( _ptr ); (_ptr) = NULL; };         \
    } while (0)

/*!< value to assign to unopened file descriptors */
#if (defined __MINGW32__)
#define UNOPENED_FD                     ((HANDLE)INVALID_HANDLE_VALUE)
#else
#define UNOPENED_FD                     ((int)-1)
#endif  /* __MINGW32__ */

/*!< closes the file descriptor if it is not UNOPENED_FD */
#define _CLOSE_FD_IF_OPEN(_fd,_fn)                                      \
    do {                                                                \
        if ( UNOPENED_FD != (_fd) ) { _fn( _fd ); (_fd) = UNOPENED_FD; } \
    } while (0)
#if (defined __MINGW32__)
#define CLOSE_FD_IF_OPEN(_fd)           _CLOSE_FD_IF_OPEN(_fd,CloseHandle)
#else
#define CLOSE_FD_IF_OPEN(_fd)           _CLOSE_FD_IF_OPEN(_fd,close)
#endif


#define ARRAY_WITH_DEFAULTS(_type,_name,_nr,_default_val)       \
    _type _name[_nr] = { [ 0 ... ((_nr)-1) ] = _default_val, }

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))


/**************************************************************************************************/
/** locks a pthread mutex */
#define LOCK(_mutex)                                            \
    do {                                                        \
        pthread_mutex_lock(&(_mutex));                          \
    } while (0)

/** unlocks a pthread mutex */
#define UNLOCK(_mutex)                                          \
    do {                                                        \
        pthread_mutex_unlock(&(_mutex));                        \
    } while (0)

/** unlocks a pthread mutex AND returns from the calling function with a return value of _val */
#define UNLOCK_AND_RETURN(_mutex,_val)                          \
    do {                                                        \
        UNLOCK(_mutex);                                         \
        return (_val);                                          \
    } while (0)

/** declares an array with default values set of pthread_mutex_t with size `_nr` */
#define MUTEX_ARRAY(_name,_nr)                                          \
    ARRAY_WITH_DEFAULTS(pthread_mutex_t, _name, _nr, PTHREAD_MUTEX_INITIALIZER)

/** declares an array with default values set of pthread_cond_t with size `_nr` */
#define COND_ARRAY(_name,_nr)                                          \
    ARRAY_WITH_DEFAULTS(pthread_cond_t, _name, _nr, PTHREAD_COND_INITIALIZER)
/**************************************************************************************************/


/**************************************************************************************************/
/** locks a pthread mutex for a given card index in an array of mutexes */
#define CARD_LOCK(_mutex_array,_card)                       LOCK(_mutex_array[_card])

/** unlocks a pthread mutex for a given card index in an array of mutexes */
#define CARD_UNLOCK(_mutex_array,_card)                     UNLOCK(_mutex_array[_card])

/** unlocks a pthread mutex for a given card index in an array of mutexes AND returns from the
    calling function with a return value of _val */
#define CARD_UNLOCK_AND_RETURN(_mutex_array,_card,_val)     UNLOCK_AND_RETURN(_mutex_array[_card], _val)
/**************************************************************************************************/

/**************************************************************************************************/
/** locks a pthread mutex for a given card index in a struct array where the struct has a pthread_mutex_t field */
#define FIELD_LOCK(_array,_field,_card)                     LOCK(_array[_card]._field)

/** unlocks a pthread mutex for a given card index in a struct array where the struct has a pthread_mutex_t field */
#define FIELD_UNLOCK(_array,_field,_card)                   UNLOCK(_array[_card]._field)

/** unlocks a pthread mutex for a given card index in a struct array where the struct has a
    pthread_mutex_t field AND returns from the calling function with a return value of _val */
#define FIELD_UNLOCK_AND_RETURN(_array,_field,_card,_val)     UNLOCK_AND_RETURN(_array[_card]._field, _val)
/**************************************************************************************************/


#define CHECK_CARD_RANGE(_card)                                         \
    do {                                                                \
        __typeof__ (_card) __card = (_card);                            \
        if ( __card >= SKIQ_MAX_NUM_CARDS )                             \
        {                                                               \
            _skiq_log(SKIQ_LOG_ERROR, "invalid card (%u) specified\n", __card); \
            return -ERANGE;                                             \
        }                                                               \
    } while (0)

#define CHECK_CARD_ACTIVE(_card)                                        \
    do {                                                                \
        __typeof__ (_card) __card = (_card);                            \
        if( skiq.card[__card].card_active != 1 )                        \
        {                                                               \
            skiq_error("requested card (%u) has not been activated\n", __card); \
            return -ENODEV;                                             \
        }                                                               \
    } while (0)

#define CHECK_VALID_TX_HDL(_card,_hdl)                                  \
    do {                                                                \
        __typeof__ (_card) __card = (_card);                            \
        __typeof__ (_hdl) __hdl = (_hdl);                               \
        int32_t _status = _skiq_validate_tx_hdl( __card, __hdl );       \
        if ( _status == -EINVAL )                                       \
        {                                                               \
            skiq_error("Invalid skiq_tx_hdl_t value (%u) passed to %s() on card %u\n", __hdl, \
                       __FUNCTION__, __card);                           \
            return _status;                                             \
        }                                                               \
        else if ( _status == -EDOM )                                    \
        {                                                               \
            skiq_error("Unsupported Tx handle (%s) passed to %s() on card %u\n", \
                       tx_hdl_cstr(__hdl), __FUNCTION__, __card);       \
            return _status;                                             \
        }                                                               \
    } while (0)

#define CHECK_VALID_RX_HDL(_card,_hdl)                                  \
    do {                                                                \
        __typeof__ (_card) __card = (_card);                            \
        __typeof__ (_hdl) __hdl = (_hdl);                               \
        int32_t _status = _skiq_validate_rx_hdl( __card, __hdl );       \
        if ( _status == -EINVAL )                                       \
        {                                                               \
            skiq_error("Invalid skiq_rx_hdl_t value (%u) passed to %s() on card %u\n", __hdl, \
                       __FUNCTION__, __card);                           \
            return _status;                                             \
        }                                                               \
        else if ( _status == -EDOM )                                    \
        {                                                               \
            skiq_error("Unsupported Rx handle (%s) passed to %s() on card %u\n", \
                       rx_hdl_cstr(__hdl), __FUNCTION__, __card);       \
            return _status;                                             \
        }                                                               \
    } while (0)

#define CHECK_NULL_PARAM(_param)                                        \
    do {                                                                \
        if ( (_param) == NULL )                                         \
        {                                                               \
            skiq_error("NULL pointer (" #_param ") passed to %s\n", __FUNCTION__); \
            return -EFAULT;                                             \
        }                                                               \
    } while (0)

/* shorthand for _skiq_log() calls */
#define skiq_debug(...)                 do { _skiq_log(SKIQ_LOG_DEBUG, __VA_ARGS__); } while (0)
#define skiq_error(...)                 do { _skiq_log(SKIQ_LOG_ERROR, __VA_ARGS__); } while (0)
#define skiq_info(...)                  do { _skiq_log(SKIQ_LOG_INFO, __VA_ARGS__); } while (0)
#define skiq_warning(...)               do { _skiq_log(SKIQ_LOG_WARNING, __VA_ARGS__); } while (0)

/** @brief Use this function wherever future hardware may confuse libsidekiq */
#define skiq_unknown(_msg,...)          do { skiq_error( _msg "\n\tAn unexpected error has occurred.  This error MAY be related to a lack of\n\thardware support in this library release. Please check the Support Forum\n\tfor the latest software release to ensure hardware compatibility.\n\n", __VA_ARGS__ ); } while (0)


/***** TYPEDEFS *****/


typedef struct
{
    skiq_part_t part;
    uint8_t bus;
    uint8_t addr;

} i2c_device_t;

#define I2C_DEVICE(_part,_bus,_addr)    { .part = (_part), .bus = (_bus), .addr = (_addr), }


/***** EXTERN DATA  *****/


/***** EXTERN FUNCTIONS  *****/

#ifdef __MINGW32__
#include "os_logger.h"
#endif /* __MINGW32__ */

extern void _skiq_syslog( int32_t priority, const char* message );
void _skiq_vlog( int32_t priority,
                 const char *format,
                 va_list args );
void _skiq_log( int32_t priority, const char *format, ... )
#ifdef __GNUC__
/* check calls against printf-style arguments.  'format' is in position 2,
 * and any arguments begin in position 3.
 */
__attribute__ ((format (printf, 2, 3)));
#else /* __GNUC__ */
;
#endif /* __GNUC__ */
void _skiq_log_str( int32_t priority, const char *str );
void _skiq_vlog( int32_t priority, const char *format, va_list args );

void _skiq_save_ref_clock( uint8_t card, skiq_ref_clock_select_t ref_clock );


/** A helper MACRO that takes a value created by FPGA_VERSION() instead of three separate values */
#define _skiq_meets_FPGA_VERSION(_card,_vers) \
    _skiq_meets_fpga_version(_card,FPGA_VERSION_MAJOR(_vers),FPGA_VERSION_MINOR(_vers), \
                             FPGA_VERSION_PATCH(_vers))


/*************************************************************************************************/
/** Helper function that queries a cache of the FPGA semantic version and checks whether or not it
    meets a minimum required version.

    @param[in] card card number of the Sidekiq of interest
    @param[in] req_major minimum required major version
    @param[in] req_minor minimum required minor version
    @param[in] req_patch minimum required patch version

    @return bool
    @retval true FPGA bitstream version associated with card meets the minimum required version
    @retval false FPGA bitstream version FAILS to meet the minimum required version
 */
bool _skiq_meets_fpga_version( uint8_t card,
                               uint8_t req_major,
                               uint8_t req_minor,
                               uint8_t req_patch );

/*************************************************************************************************/
/** Helper function that queries the FPGA semantic version directly and checks whether or not it
    meets a minimum required version.

    @param[in] card card number of the Sidekiq of interest
    @param[in] req_major minimum required major version
    @param[in] req_minor minimum required minor version
    @param[in] req_patch minimum required patch version

    @return bool
    @retval true FPGA bitstream version associated with card meets the minimum required version
    @retval false FPGA bitstream version FAILS to meet the minimum required version
 */
bool _skiq_meets_fpga_version_nocache( uint8_t card,
                                       uint8_t req_major,
                                       uint8_t req_minor,
                                       uint8_t req_patch );


/*************************************************************************************************/
/** Helper function that returns whether or not a give Sidekiq card has a DC offset correlator in
    the FPGA

    @param[in] card card index of the Sidekiq of interest

    @return bool
    @retval true Sidekiq card has a DC offset correlator
    @retval false Sidekiq card DOES NOT have a DC offset correlator
 */
bool _skiq_has_dc_offset_corr( uint8_t card );


/*****************************************************************************/
/** The _skiq_get_part function returns the part enumerated type for a given
    Sidekiq card.

    @param card card number of the Sidekiq of interest

    @return skiq_part_t part enumerated type for the specified Sidekiq card
*/
skiq_part_t _skiq_get_part(uint8_t card);

/*****************************************************************************/
/** The _skiq_get_hw_iface_vers function obtains the hardware interface version
    for a given Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return hw_iface_vers_t hardware interface version for the specified
    Sidekiq card
*/
hw_iface_vers_t _skiq_get_hw_iface_vers(uint8_t card);


/**************************************************************************************************/
/** The _skiq_validate_rx_hdl function indicates whether or not the @p hdl is valid and supported
    for a given Sidekiq card

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl  [::skiq_rx_hdl_t] receive handle to validate

    @return 0 on success, else a negative errno value
    @retval -EINVAL  Requested handle is out of range
    @retval -EDOM    Requested handle is not available for the Sidekiq card
*/
int32_t _skiq_validate_rx_hdl(uint8_t card, skiq_rx_hdl_t hdl);


/**************************************************************************************************/
/** The _skiq_get_nr_rx_hdl function returns the number of valid receive handles for a given
    Sidekiq card.

    @param[in] card card number of the Sidekiq of interest

    @return uint8_t number of valid receive handles for the specified Sidekiq card
*/
uint8_t _skiq_get_nr_rx_hdl( uint8_t card );


/**************************************************************************************************/
/** The _skiq_get_rx_hdl function returns the 'idx-th' receive handle associated with the specified
    Sidekiq card.

    For example, let's consider two Sidekiq products.

    With the Sidekiq M.2, there are two available receive handles: A1 and A2, so:
      rf_params.rx_handles[0] = skiq_rx_hdl_A1      (aka the 0th handle)
      rf_params.rx_handles[1] = skiq_rx_hdl_A2      (aka the 1th handle)

    With the Sidekiq NV100, there are two available receive handles: A1 and B1, so:
      rf_params.rx_handles[0] = skiq_rx_hdl_A1      (aka the 0th handle)
      rf_params.rx_handles[1] = skiq_rx_hdl_B1      (aka the 1th handle)

    Traditionally libsidekiq performs the handle lookup by using a typecast, which was okay until
    NV100 showed up:

    If rf_params.num_rx_channels = 2:
    then the 0th handle was (skiq_rx_hdl_t)(0) -> skiq_rx_hdl_A1 and
    then the 1th handle was (skiq_rx_hdl_t)(1) -> skiq_rx_hdl_A2

    @param[in] card card number of the Sidekiq of interest

    @return skiq_rx_hdl_t the 'idx-th' receive handle
    @retval skiq_rx_hdl_end if the @p idx is out of bounds
*/
skiq_rx_hdl_t _skiq_get_rx_hdl( uint8_t card,
                                uint8_t idx );


/**************************************************************************************************/
/** The _skiq_validate_tx_hdl function indicates whether or not the @p hdl is valid and supported
    for a given Sidekiq card

    @param[in] card card number of the Sidekiq of interest
    @param[in] hdl  [::skiq_tx_hdl_t] transmit handle to validate

    @return 0 on success, else a negative errno value
    @retval -EINVAL  Requested handle is out of range
    @retval -EDOM    Requested handle is not available for the Sidekiq card
*/
int32_t _skiq_validate_tx_hdl(uint8_t card, skiq_tx_hdl_t hdl);


/**************************************************************************************************/
/** The _skiq_get_nr_tx_hdl function returns the number of valid transmit handles for a given
    Sidekiq card.

    @param[in] card card number of the Sidekiq of interest

    @return uint8_t number of valid transmit handles for the specified Sidekiq card
*/
uint8_t _skiq_get_nr_tx_hdl( uint8_t card );


/*************************************************************************************************/
/** The _skiq_get_fmc_carrier function returns the cached board enumerated type for a given Sidekiq
    card.

    @param[in] card card number of the Sidekiq of interest

    @return skiq_fmc_carrier_t board enumerated type for the specified Sidekiq card
*/
skiq_fmc_carrier_t _skiq_get_fmc_carrier( uint8_t card );

/*************************************************************************************************/
/** The _skiq_get_fmc_carrier function returns the board enumerated type read directly for a given
    Sidekiq card without relying on cached values.

    @param[in] card card number of the Sidekiq of interest

    @return skiq_fmc_carrier_t board enumerated type for the specified Sidekiq card
*/
skiq_fmc_carrier_t _skiq_get_fmc_carrier_nocache( uint8_t card );

/*************************************************************************************************/
/** The _skiq_get_fpga_device function returns the FPGA device enumerated type for a given Sidekiq
    card.

    @param card card number of the Sidekiq of interest

    @return skiq_fpga_device_t FPGA device enumerated type for the specified Sidekiq card
*/
skiq_fpga_device_t _skiq_get_fpga_device(uint8_t card);

/*************************************************************************************************/
/** The _skiq_get_fpga_device_nocache function returns the FPGA device enumerated type read directly
    for a given Sidekiq card without relying on cached values.

    @warning The ::skiq_part_t may be cached, however all other information is retrieved from the
    hardware.

    @param card card number of the Sidekiq of interest

    @return skiq_fpga_device_t FPGA device enumerated type for the specified Sidekiq card
*/
skiq_fpga_device_t _skiq_get_fpga_device_nocache(uint8_t card);

/*****************************************************************************/
/** The _skiq_get_fpga_caps function obtains the FPGA's capabilities for a given
    Sidekiq card.

    @param card card number of the Sidekiq of interest

    @return struct fpga_capabilities FPGA capabilities for the specified Sidekiq card
*/
struct fpga_capabilities _skiq_get_fpga_caps(uint8_t card);

/**************************************************************************************************/
/** The resolve_transport_type() function takes a transport type and resolves it to a preferred
    transport type for use with HAL and the Flash layer of libsidekiq.

    Here's the preference resolution, based on configured transport type:
    - skiq_xport_type_auto
    -- If both PCIe and USB are available, prefer USB, otherwise prefer the available transport
    - skiq_xport_type_pcie
    -- If both PCIe and USB are available, prefer USB, otherwise PCIe
    - skiq_xport_type_usb and skiq_xport_type_custom
    -- Prefer configuration transport type

    @param[in] card card number of the Sidekiq of interest

    @return ::skiq_xport_type_t
    @retval ::skiq_xport_type_unknown Unable to resolve to a preferred transport type
    @retval others Preferred transport type to use for HAL and Flash layer
 */
skiq_xport_type_t _skiq_resolve_transport_type( uint8_t card );

/*****************************************************************************/
/** The _skiq_write_rx_preselect_filters function is responsible for configuring
    available RX preselect filters of the card and handle specified.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the RX interface to configure
    @param num_filters number of filters in list
    @param p_filters pointer to the filter list 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_write_rx_preselect_filters( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint8_t num_filters,
                                          const skiq_filt_t *p_filters );

/*****************************************************************************/
/** The _skiq_write_tx_preselect_filters function is responsible for configuring
    available TX preselect filters of the card and handle specified.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the TX interface to configure
    @param p_filters pointer to the filter list 

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_write_tx_preselect_filters( uint8_t card,
                                          skiq_tx_hdl_t hdl,
                                          uint8_t num_filters,
                                          const skiq_filt_t *p_filters );

/*****************************************************************************/
/** The _skiq_get_rx_rfe_instance obtains the RF front end instance for a
    given Rx handle.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the desired Rx interface

    @return rfe_t   the RF front end instance
*/
rfe_t _skiq_get_rx_rfe_instance( uint8_t card, skiq_rx_hdl_t hdl );

/*****************************************************************************/
/** The _skiq_get_rx_rfe_instance obtains the RF front end instance for a
    given Tx handle.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the desired Tx interface

    @return rfe_t   the RF front end instance
*/
rfe_t _skiq_get_tx_rfe_instance( uint8_t card, skiq_tx_hdl_t hdl );

/**************************************************************************************************/
/** The _skiq_get_generic_rfe_instance obtains the RFFE instance not related to a particular
    handle.

    @param card card number of the Sidekiq of interest

    @return rfe_t  the RFFE instance
*/
rfe_t _skiq_get_generic_rfe_instance( uint8_t card );

/*****************************************************************************/
/** The _skiq_get_rx_rfic_instance obtains the RF IC instance for a given Rx
    handle.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the desired Rx interface

    @return rfic_t  the RF IC instance
*/
rfic_t _skiq_get_rx_rfic_instance( uint8_t card, skiq_rx_hdl_t hdl );

/*****************************************************************************/
/** The _skiq_get_tx_rfic_instance obtains the RF IC instance for a given Tx
    handle.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the desired Tx interface

    @return rfic_t  the RF IC instance
*/
rfic_t _skiq_get_tx_rfic_instance( uint8_t card, skiq_tx_hdl_t hdl );

/**************************************************************************************************/
/** The _skiq_get_generic_rfic_instance obtains the RF IC instance not related to a particular
    handle.

    @param card card number of the Sidekiq of interest

    @return rfic_t  the RF IC instance
*/
rfic_t _skiq_get_generic_rfic_instance( uint8_t card );

/*****************************************************************************/
/** The _skiq_read_rx_filter_overflow obtains filter overflow condition for
    a given Rx handle.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the desired Rx interface
    @param p_rx_fir pointer to flag indicating if the RX FIR filter overflowed
    
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_read_rx_filter_overflow( uint8_t card,
                                       skiq_rx_hdl_t hdl,
                                       bool *p_rx_fir );

/*****************************************************************************/
/** The _skiq_read_tx_filter_overflow obtains filter overflow condition for
    a given Tx handle.

    @param card card number of the Sidekiq of interest
    @param hdl handle of the desired Tx interface
    @param p_int3 pointer to flag indicating if the TX INT3 filter overflowed
    @param p_hb3 pointer to flag indicating if the TX HB3 filter overflowed
    @param p_hb2 pointer to flag indicating if the TX HB2 filter overflowed
    @param p_qec pointer to flag indicating if the TX QEC filter overflowed
    @param p_hb1 pointer to flag indicating if the TX HB1 filter overflowed
    @param p_tx_fir pointer to flag indicating if the TX FIR filter overflowed
    
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_read_tx_filter_overflow( uint8_t card,
                                       skiq_tx_hdl_t hdl,
                                       bool *p_int3,
                                       bool *p_hb3,
                                       bool *p_hb2,
                                       bool *p_qec,
                                       bool *p_hb1,
                                       bool *p_tx_fir );

/*****************************************************************************/
/** The _skiq_init_xport initalizes the xport layer and appropriate function
    pointers based on the xport type and level requested for the specified card.

    @param card card number of the Sidekiq of interest
    
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_init_xport( uint8_t card,
                                   skiq_xport_type_t type,
                                   skiq_xport_init_level_t level );

/*****************************************************************************/
/** The _skiq_get_hw_vers function obtains the hardware vers enumerated type for a given
    Sidekiq card.

    @param card card number of the Sidekiq of interest

    @return skiq_hw_vers_t hw_vers enumerated type for the specified Sidekiq card
*/
skiq_hw_vers_t _skiq_get_hw_vers( uint8_t card );

/*****************************************************************************/
/** The _skiq_get_hw_rev function obtains the hardware rev enumerated type for a given
    Sidekiq card.

    @param card card number of the Sidekiq of interest

    @return skiq_hw_rev_t hw_rev enumerated type for the specified Sidekiq card
*/
skiq_hw_rev_t _skiq_get_hw_rev( uint8_t card );

/*************************************************************************************************/
/** The _skiq_get_hw_rev_minor function obtains the minor number of the hardware revision for a
    given Sidekiq card.

    @param[in] card card number of the Sidekiq of interest

    @return uint8_t revision minor number for the specified Sidekiq card
*/
uint8_t _skiq_get_hw_rev_minor( uint8_t card );

/*****************************************************************************/
/** The _skiq_get_fpga_board_id function obtains the FPGA's board ID for a given
    Sidekiq card.

    @param card card number of the Sidekiq of interest

    @return FPGA boardID for the specified Sidekiq card
*/
uint8_t _skiq_get_fpga_board_id( uint8_t card );

/**************************************************************************************************/
/**
   The _skiq_get_fpga_board_id_ext() function obtains the FPGA's extended board ID for a given
   Sidekiq card.

   @param[in] card       card number of the Sidekiq of interest

   @return FPGA extended board ID for the specified Sidekiq card
*/
uint8_t _skiq_get_fpga_board_id_ext( uint8_t card );

/*****************************************************************************/
/** The _skiq_get_max_rfic_cs obtains the maximum valid chip select value for 
    the Sidekiq card specified.

    @param card card number of the Sidekiq of interest

    @return maximum value of RFIC chip select
*/
uint8_t _skiq_get_max_rfic_cs( uint8_t card );

/*****************************************************************************/
/** The _skiq_update_rfic_port_mapping function determines if the RFIC to handle
    port mapping has changed for any of the handles.  If the mapping has changed,
    the RFE and RFIC configuration is updated to reflect this change.

    @param card card number of the Sidekiq of interest

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_update_rfic_port_mapping( uint8_t card );

#endif  /* __SIDEKIQ_PRIVATE_H__ */
