#ifndef SIDEKIQ_CARD_MGR_H
#define SIDEKIQ_CARD_MGR_H
/**************************************************************************************************/
/**
   @file sidekiq_card_mgr.h

   @brief This file contains the public interface of the Sidekiq card manager.  The card manager is
   responsible for obtaining ownership of the cards and mapping of transport IDs to card IDs
   (https://confluence.epiq.rocks/display/EN/Sidekiq+Card+Manager)
 
 <pre>
   Copyright 2017-2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

/***** INCLUDES *****/
#include "sidekiq_xport_types.h"
#include "sidekiq_private.h"

#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

/***** DEFINES *****/
    
/** @brief Number of bytes available to store any private XPORT data across processes */
#define CARD_MGR_XPORT_PRIVATE_SIZE (100) 

/***** Extern Functions  *****/

/**************************************************************************************************/
/**
   The card_mgr_init() function is responsible for initializing the local card_mgr member variables
   by reading the shared memory.  Note that it is expected that the shared memory had already been
   created and initialized, so it just reads and caches the most up-to-date info.

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_init( void );

/**************************************************************************************************/
/**
   The card_mgr_reprobe() function takes care of reprobing available (e.g. unlocked) cards for any
   changed transport information, discovering new cards, or removing transports from cards that are
   no longer present.

   @return 0 on success, else a negative errno value
   @retval -EBUSY    The mutex could not be acquired because it was already locked.
 */
int32_t card_mgr_reprobe( void );

/**************************************************************************************************/
/**
   The card_mgr_exit() function is responsible for unmapping the previously mapped shared memory (by
   card_mgr_init()).

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_exit( void );    

/**************************************************************************************************/
/**
   The card_mgr_probe_trylock() function attempts to obtain a lock of the probe mutex.  If the lock
   is unavailable, the PID of the process which currently has the mutex is provided.

    @param[out] p_owner pointer to where to store the PID of the owner of the probe lock

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_probe_trylock( pid_t *p_owner );

/**************************************************************************************************/
/**
   The card_mgr_probe_unlock() function attempts to unlock the probe mutex.  If the unlock is
   requested by a process that does not currently own the lock, an error is returned.

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_probe_unlock( void );

/**************************************************************************************************/
/**
   The card_mgr_card_trylock() function attempts to obtain a lock of the card mutex.  If the lock is
   unavailable, the PID of the process which currently has the mutex is provided.

    @param[in] card card # of the mutex requested
    @param[out] p_owner pointer to where to store the PID of the owner of the card lock

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_card_trylock( uint8_t card, pid_t *p_owner );

/**************************************************************************************************/
/**
   The card_mgr_card_unlock() function attempts to unlock the card mutex.  If the unlock is
   requested by a process that does not currently own the lock, an error is returned.

    @param[in] card card # of mutex

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_card_unlock( uint8_t card );    

/**************************************************************************************************/
/**
   The card_mgr_register_xport() function registers the XPORT layer for the card.

    @param[in] xport_uid unique identifier (across processes) of the XPORT layer
   @param[in] type XPORT type that detected the card
    @param[in] serial_num serial # of the card to register
    @param[in] hardware_vers hardware version info of the card to register
    @param[in] p_part_info pointer to the part information of the card
    @param[in] num_bytes number of bytes of private data to save
    @param[in] p_private_data pointer to private data to store with xport

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_register_xport( uint64_t xport_uid,
                                 skiq_xport_type_t type,
                                 uint32_t serial_num,
                                 uint16_t hardware_vers,
                                 skiq_part_info_t *p_part_info,
                                 uint8_t num_bytes,
                                 uint8_t *p_private_data);
    
/*************************************************************************************************/
/**
   The card_mgr_deregister_xport() function removes all private data of a specified transport type
   from the specified card.

   @param[in] card  card index
   @param[in] type  XPORT type to deregister

   @return 0 on success, else a negative errno value
   @retval -EINVAL if the @p type is not valid
   @retval -EPROTO if the card information associated with @p card is not accessible
 */
int32_t card_mgr_deregister_xport( uint8_t card,
                                   skiq_xport_type_t type );

/*****************************************************************************/
/** The card_mgr_read_xport_private function reads the private data provided
    when the card was previously registered.

    @param[in] xport_uid unique identifier (across processes) of the XPORT layer
    @param[in] type XPORT type that detected the card
    @param[in] p_xport_private byte array to be saved

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_read_xport_private( uint64_t xport_uid,
                                     skiq_xport_type_t type,
                                     uint8_t *p_num_bytes,
                                     uint8_t *p_xport_private );

/*****************************************************************************/
/** The card_mgr_update_xport_private function retrieves and copies the private
    XPORT data requested.

    @param[in] xport_uid unique identifier (across processes) of the XPORT layer
    @param[in] type XPORT type that detected the card
    @param[out] p_xport_private byte array to be retrieved
    @param[in] num_private_bytes # of bytes requested

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_update_xport_private( uint64_t xport_uid,
                                       skiq_xport_type_t type,
                                       uint8_t num_private_bytes,
                                       uint8_t *p_xport_private );

/**************************************************************************************************/
/**
   The card_mgr_is_xport_avail() function determines if the XPORT has been registered for the card #
   requested.

    @param[in] card card # requested
    @param[in] type XPORT type that detected the card

   @return 0 on success, else a negative errno value
*/
bool card_mgr_is_xport_avail( uint8_t card,
                              skiq_xport_type_t type );

/**************************************************************************************************/
/**
   The card_mgr_get_xport_id() function returns the XPORT UID based on the card # provided.

    @param[in] card card # requested
    @param[in] type XPORT type that detected the card
    @param[out] p_xport_id pointer to where to store the XPORT UID

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_get_xport_id( uint8_t card,
                               skiq_xport_type_t type,
                               uint64_t *p_xport_id );
    
/**************************************************************************************************/
/**
   The card_mgr_get_card() function returns the card # based on the XPORT ID provided.

    @param[in] xport_id XPORT UID to locate
    @param[in] type XPORT type that detected the card
    @param[out] p_card pointer to where to store the card #

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_get_card( uint64_t xport_uid,
                           skiq_xport_type_t type,
                           uint8_t *p_card_id );

/**************************************************************************************************/
/**
   The card_mgr_get_card_info() function returns the information of the card based on the card #
   provided.

    @param[in] card card # requested
    @param[out] p_serial_num pointer to where to store the serial #
    @param[out] p_hardware_vers pointer to where to store the hw vers info
    @param[out] p_part_info pointer to the part information of the card

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_mgr_get_card_info( uint8_t card,
                                uint32_t *p_serial_num,
                                uint16_t *p_hardware_vers,
                                skiq_part_info_t *p_part_info );
    
/**************************************************************************************************/
/**
   The card_mgr_is_card_present() function indicates whether or not the card is present.  The
   function bases its decision on whether or not the registered card has any transports available

   @param[in] card           card index
   @param[out] p_is_present  pointer to bool to place result

   @return 0 on success, else a negative errno value
   @retval -ENODEV    No card information available for specified @p card argument
*/
int32_t card_mgr_is_card_present( uint8_t card,
                                  bool *p_is_present );
    
/**************************************************************************************************/
/**
   The card_mgr_display_info() function prints the information on all cards / XPORTs available.
*/
void card_mgr_display_info( void );


/**************************************************************************************************/
/**
   The card_mgr_get_cards() function populates a list of card #s that have been detected by card
   manager.

    @param[out] p_cards pointer to list of card #s with XPORT detected
    @param[out] p_num_cards pointer to number of cards detected

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_get_cards( uint8_t *p_cards,
                            uint8_t *p_num_cards );


/**************************************************************************************************/
/**
   The card_mgr_fpga_programming_lock() function attempts to obtain a lock of the FPGA programming
    mutex.

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_fpga_programming_lock( void );


/**************************************************************************************************/
/**
   The card_mgr_fpga_programming_unlock() function attempts to unlock the FPGA programming mutex.
   If the unlock is requested by a process that does not currently own the lock, an error is
   returned.

   @return 0 on success, else a negative errno value
*/
int32_t card_mgr_fpga_programming_unlock( void );


#ifdef __cplusplus
}
#endif

#endif  /* __SIDEKIQ_CARD_MGR_H__ */
