#ifndef PROG_FPGA_H
#define PROG_FPGA_H

/*! \file prog_fpga.h
 * \brief This file contains the public interface of the
 * prog_fpga module.
 *
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 11/22/2013   MEZ   Created file
 *
 *</pre>*/

/***** INCLUDES *****/
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
/***** DEFINES *****/


/***** Extern Functions  *****/
extern int32_t program_fpga_from_file( uint8_t index,
                                       FILE* p_file,
                                       uint8_t legacy_method );

#endif

