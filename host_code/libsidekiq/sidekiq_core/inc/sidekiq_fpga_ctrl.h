#ifndef __SIDEKIQ_FPGA_CTRL_H__
#define __SIDEKIQ_FPGA_CTRL_H__

/*! \file sidekiq_fpga_ctrl.h
 *  
 * @brief This file contains the public interface Sidekiq FPGA Control.  It is intended to be called
 * only from sidekiq_core and provides wrapping of lower level FPGA register manipulation.  It is
 * expected that any caller of these functions have already validated a card (range and activated)
 * and that an appropriate transport layer implementing FPGA register reads/writes is initialized.
 * 
 * <pre>
 * Copyright 2017-2019 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <stdint.h>
#include <stdbool.h>
#include <complex.h>

#include "sidekiq_types.h"
#include "sidekiq_fpga_ctrl_decimator.h"

#define BOARD_ID_0(_x)                  ((_x) & (1 << 0))

typedef struct
{
    skiq_rx_hdl_t hdl;
    uint32_t ctrl_reg;
    bool packed;
    bool complex_mult;
    skiq_data_src_t data_src;
    bool dc_offset;
} fpga_ctrl_rx_params_t;

typedef struct
{
    uint16_t fpga_block_size;
    skiq_tx_flow_mode_t flow_mode;
    bool packed;
    bool wait_for_pps;
    uint64_t pps_sys_timestamp;
    skiq_tx_ena_callback_t tx_enabled_cb;
} fpga_ctrl_tx_params_t;

/* These bitfields are positioned based on ordering of FPGA register 0x800C */
struct fpga_capabilities
{
    uint32_t clock_rate:3;
    uint32_t self_reload:1;
    uint32_t self_reload_wbstar:1;
    uint32_t tx_ts_late_support:1;
    uint32_t gps_module_support:1;
    uint32_t iq_complex_mult:1;
    uint32_t manual_trigger:1;
    uint32_t flash_support:1;
    uint32_t user_bar_location:3;
    uint32_t fpga_soft_reset:1;

    uint32_t reserved:18;       /* the 'leftover' portion of the capabilities field, needs to fill
                                 * the remainder up to 32 bits total */
};

#define FPGA_CAPABILITIES_INITIALIZER {                                 \
        .clock_rate = 0,                                                \
        .self_reload = 0,                                               \
        .self_reload_wbstar = 0,                                        \
        .tx_ts_late_support = 0,                                        \
        .gps_module_support = 0,                                        \
        .iq_complex_mult = 0,                                           \
        .manual_trigger = 0,                                            \
        .flash_support = 0,                                             \
        .user_bar_location = 0,                                         \
        .fpga_soft_reset = 0,                                           \
        .reserved = 0,                                                  \
    }

/* simple MACROs to abstract the A2/B1 second transmit channel selection */
#define fpga_ctrl_select_TxA2_as_second_chan(_card)    sidekiq_fpga_reg_RMWV(_card, 0, TX_A2_MAP_TO_B1, FPGA_REG_FPGA_CTRL)
#define fpga_ctrl_select_TxB1_as_second_chan(_card)    sidekiq_fpga_reg_RMWV(_card, 1, TX_A2_MAP_TO_B1, FPGA_REG_FPGA_CTRL)

/*****************************************************************************/
/** The fpga_ctrl_read_version reads all of the version related information
    from the FPGA.

    @param[in] card Sidekiq card of interest
    @param[out] p_git_hash  a pointer to where the 32-bit git hash will be written
    @param[out] p_build_date  a pointer to where the 32-bit build date will be written
    @param[out] p_major a pointer to where the major rev # should be written
    @param[out] p_minor a pointer to where the minor rev # should be written
    @param[out] p_baseline_hash a pointer to where the 32-bit baseline git hash will be written
    @param[out] p_tx_fifo_size a pointer to where the FPGA's TX FIFO size enumeration should be written

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_version(uint8_t card,
                               uint32_t* p_git_hash,
                               uint32_t* p_build_date,
                               uint8_t *p_major,
                               uint8_t *p_minor,
                               uint8_t *p_patch,
                               uint32_t* p_baseline_hash,
                               skiq_fpga_tx_fifo_size_t *p_tx_fifo_size);

/*****************************************************************************/
/** The fpga_ctrl_read_board_id reads all of the BOARD_ID bits from the FPGA.

    @param[in] card Sidekiq card of interest
    @param[out] p_board_id  reference to where the 3-bit board ID will be returned

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_board_id( uint8_t card,
                                 uint8_t *p_board_id );

/**************************************************************************************************/
/**
   The fpga_ctrl_read_board_id_ext reads the EXTENDED_BOARD_ID field of the FPGA_REG_VERSION2
   register from the FPGA.

   @param[in] card Sidekiq card of interest
   @param[out] p_board_id_ext  reference to where the 8-bit extended board ID will be returned

   @return int32_t
   @retval 0 Success
   @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_board_id_ext( uint8_t card,
                                     uint8_t *p_board_id_ext );

/*****************************************************************************/
/** The fpga_ctrl_read_capabilities reads the FPGA_CAPABILITIES register from
    the FPGA and returns a mapped structure.

    @param[in] card Sidekiq card of interest
    @param[out] p_caps  reference to where the FPGA capabilities will be returned

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_capabilities( uint8_t card,
                                     struct fpga_capabilities *p_caps );


int32_t fpga_ctrl_resume_rx_stream_multi( uint8_t card,
                                          fpga_ctrl_rx_params_t rx_params[],
                                          uint8_t nr_rx_params );

/**************************************************************************************************/
/** fpga_ctrl_start_rx_stream_multi() starts streaming on multiple receive streams.  It does so
    synchronously if the FPGA bitstream supports it and if the caller requests it
    (::skiq_trigger_src_synced or ::skiq_trigger_src_1pps).

    @param[in] card Sidekiq card of interest
    @param[in] rx_params array of RX stream parameters
    @param[in] nr_rx_params number of entries in rx_params[]
    @param[in] trigger_src type of trigger
    @param[in] pps_sys_timestamp that must occur prior to the stream being enabled

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP requested synchronization trigger not supported by FPGA bitstream
    @retval -ETIMEDOUT 1PPS trigger was requested and never occurred
 */
int32_t fpga_ctrl_start_rx_stream_multi( uint8_t card,
                                         fpga_ctrl_rx_params_t rx_params[],
                                         uint8_t nr_rx_params,
                                         skiq_trigger_src_t trigger_src,
                                         uint64_t pps_sys_timestamp );

/**************************************************************************************************/
/** fpga_ctrl_stop_rx_stream_multi() stop streaming on multiple receive streams.  It does so
    synchronously if the FPGA bitstream supports it and if the caller requests it
    (::skiq_trigger_src_synced or ::skiq_trigger_src_1pps).

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP Requested synchronization trigger not supported by FPGA bitstream
    @retval -ETIMEDOUT 1PPS trigger was requested and never occurred
 */
int32_t fpga_ctrl_stop_rx_stream_multi( uint8_t card,
                                        fpga_ctrl_rx_params_t rx_params[],
                                        uint8_t nr_rx_params,
                                        skiq_trigger_src_t trigger_src,
                                        uint64_t pps_sys_timestamp );

/*****************************************************************************/
/** The fpga_ctrl_start_tx_streaming function enables the TX stream in the FPGA.  
    
    @param[in] card Sidekiq card of interest
    @param[in] p_tx_params pointer to the TX stream parameters
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_start_tx_streaming( uint8_t card,
                                      fpga_ctrl_tx_params_t *p_tx_params );

/*****************************************************************************/
/** The fpga_ctrl_stop_tx_stream function disables the TX stream in the FPGA.  
    
    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_stop_tx_stream( uint8_t card );

/*****************************************************************************/
/** The fpga_ctrl_stop_tx_stream function disables the TX stream in the FPGA
    on a 1PPS edge.  This function blocks until the 1PPS edge occurs.
    
    @param[in] card Sidekiq card of interest
    @param[in] pps_sys_timestamp this is the system timestamp that must occur
    prior to the stream being disabled

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_stop_tx_stream_on_1pps( uint8_t card, uint64_t pps_sys_timestamp );

/*****************************************************************************/
/** The fpga_ctrl_enable_stream function notifies the FPGA that at least one
    channel's streaming is enabled (RX or TX).
    
    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_enable_stream( uint8_t card );

/*****************************************************************************/
/** The fpga_ctrl_finalize_stream_disable function notifies the FPGA that 
    all channel streaming is disabled (RX and TX).
    
    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_finalize_stream_disable( uint8_t card );

/*****************************************************************************/
/** The fpga_ctrl_cnacel_1pps function cancels any FPGA transaction that 
    may have been pending waiting for a 1PPS to occur.
    
    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
*/
void fpga_ctrl_cancel_1pps( uint8_t card );


/*************************************************************************************************/
/** The fpga_ctrl_read_sys_timestamp_freq function queries the FPGA and/or RFIC for the frequency at
    which the system timestamp increments.

    @param[in] card Sidekiq card of interest
    @param[out] p_sys_timestamp_freq pointer to where to store the system timestamp frequency

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ERANGE The system timestamp frequency indicated by the FPGA is out of range
    @retval -ENOTSUP Sidekiq RFIC does not support querying system timestamp frequency
*/
int32_t fpga_ctrl_read_sys_timestamp_freq( uint8_t card, uint64_t *p_sys_timestamp_freq );


/*****************************************************************************/
/** The fpga_ctrl_enable_rx_packet_64_words function configures the FPGA to use
    receive packets that are 64 words in size.

    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_enable_rx_packet_64_words( uint8_t card );


/*****************************************************************************/
/** The fpga_ctrl_disable_rx_packet_64_words function configures the FPGA NOT to
    use receive packets that are 64 words in size.

    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_ctrl_disable_rx_packet_64_words( uint8_t card );


/*************************************************************************************************/
/** The fpga_ctrl_enable_iq_complex_multiplier function tells the FPGA to apply the IQ complex
    multiplication for a given card.

    @note A user can only enable / disable the complex multipliers over @b all available handles.
    If a user wishes to enable / disable multipliers for a specific handle, they may effectively do
    so by writing an absolute factor of (1.0, 0.0).

    @attention Using this function while receive data is streaming will result in unexpected
    behavior.

    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_enable_iq_complex_multiplier( uint8_t card );


/*************************************************************************************************/
/** The fpga_ctrl_disable_iq_complex_multiplier function tells the FPGA to bypass the IQ complex
    multiplication for a given card.

    @note A user can only enable / disable the complex multipliers over @b all available handles.
    If a user wishes to enable / disable multipliers for a specific handle, they may effectively do
    so by writing an absolute factor of (1.0, 0.0).

    @attention Using this function while receive data is streaming will result in unexpected
    behavior.

    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_disable_iq_complex_multiplier( uint8_t card );


/*************************************************************************************************/
/** The fpga_ctrl_read_iq_complex_multiplier function queries the FPGA for the complex
    multiplication factor for a given card and handle.

    @param[in] card Sidekiq card of interest
    @param[in] hdl  receive handle of interest
    @param[out] p_factor reference to the complex multiplication factor

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_read_iq_complex_multiplier( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              float complex *p_factor );

/*************************************************************************************************/
/** The fpga_ctrl_write_iq_complex_multiplier function converts the provided complex multiplication
    factor into fixed point representation, then writes it to the FPGA for a given card and handle.

    @attention The accepted range for the real and imaginary parts of the complex multiplication
    factor is -1.0 to 1.0, inclusive

    @param[in] card Sidekiq card of interest
    @param[in] hdl  receive handle of interest
    @param[in] factor complex multiplication factor

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ERANGE either i_factor or q_factor are out of the supported range
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_write_iq_complex_multiplier( uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               float complex factor );

/*************************************************************************************************/
/** The fpga_ctrl_wait_for_bit function polls the FPGA register at @a reg_addr every @a poll_ns
    nanoseconds up to a total of @a timeout_ns nanoseconds checking if the value at @a field_offset
    of length @a field_length matches @a desired_value.

    @param[in] card Sidekiq card of interest
    @param[in] desired_value desired value for the field at offset of field_offset and length of field_offset
    @param[in] field_offset offset of the field to check
    @param[in] field_length length of the field to check
    @param[in] reg_addr FPGA register address to poll
    @param[in] poll_ns number of nanoseconds to delay in between checks
    @param[in] timeout_ns maximum number of nanoseconds to wait for value to match

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EPROTO A fault occurred getting a clock value for timekeeping
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ETIMEDOUT Polling for the value to match exceeded the specified timeout
*/
int32_t fpga_ctrl_wait_for_bit( uint8_t card,
                                uint32_t desired_value,
                                uint8_t field_offset,
                                uint8_t field_length,
                                uint32_t reg_addr,
                                uint32_t poll_ns,
                                uint32_t timeout_ns );

int32_t fpga_ctrl_config_hop_on_timestamp( uint8_t card,
                                           bool enable,
                                           uint32_t rfic_gpio,
                                           uint8_t chip_index /* 0=A, 1=B...TODO: a diff ID?*/ );

int32_t fpga_ctrl_hop( uint8_t card,
                       uint8_t chip_index, // 0=A, 1=B...TODO: a diff ID?
                       uint8_t rfic_ctrl_in,
                       uint8_t hop_index,
                       uint64_t timestamp );

/*************************************************************************************************/
/** The fpga_ctrl_init_config() function initializes the FPGA feature configuration for the
    specified Sidekiq card.

    @param[in] card Sidekiq card of interest

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
fpga_ctrl_init_config( uint8_t card );

/*************************************************************************************************/
/** The fpga_ctrl_reset_config() function is responsible for setting all FPGA features back to their
    default reset state.

    @param[in] card Sidekiq card of interest

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t
fpga_ctrl_reset_config( uint8_t card );

/*************************************************************************************************/
/** The fpga_ctrl_read_iq_order_mode function queries the FPGA for the I/Q ordering mode
    for a given card.

    @param[in] card Sidekiq card of interest
    @param[out] p_iq_order_mode reference to the IQ ordering mode

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements


*/
int32_t fpga_ctrl_read_iq_order_mode( uint8_t card,
                                      skiq_iq_order_t *p_iq_order_mode );

/*************************************************************************************************/
/** The fpga_ctrl_write_iq_order_mode function writes the FPGA for the I/Q ordering mode
    for a given card.

    @param[in] card Sidekiq card of interest
    @param[in] iq_order_mode specifying the IQ ordering

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_write_iq_order_mode( uint8_t card,
                                       skiq_iq_order_t iq_order_mode );

/**************************************************************************************************/
/**
   This function reads the RF and System Timestamps from the last 1PPS pulse and validates that they
   are from the *same* last 1PPS period.  This eliminates the case where the 1PPS pulse occurs
   between register reads.

   @param[in]    card            Sidekiq card ID of interest
   @param[out]   p_rf_timestamp  pointer to RF Timestamp to populate
   @param[out]   p_sys_timestamp pointer to System Timestamp to populate

   @return 0 on success, else a negative errno value
   @retval -EBADMSG Error occurred transacting with FPGA registers
   @retval -ERANGE  Timestamps could not be validated to be from the same 1PPS period
 */
int32_t fpga_ctrl_read_last_1pps_timestamps( uint8_t card,
                                             uint64_t* p_rf_timestamp,
                                             uint64_t* p_sys_timestamp );

/**************************************************************************************************/
/**
    @brief  Issue a soft reset of the Sidekiq PDK registers

    @note   FPGA versions 3.15.1 and greater should have a capability bit to determine if this
            feature is supported.

    @param[in]    card            Sidekiq card ID of interest

    @return 0 on success, else a negative errno
    @retval -ENOSYS     the FPGA doesn't have register read / write functionality
    @retval -ENOTSUP    the FPGA doesn't support the soft reset feature
    @retval -EBADMSG    if an error occurred transacting with the FPGA registers
*/
int32_t fpga_ctrl_issue_soft_reset( uint8_t card );

/**************************************************************************************************/
/**
   This function reads checks the capabilities bit in the FPGA to determine whether or not a Carrier
   Edge reference clock source is a valid option for the Sidekiq card in question.

   @param[in]    card               Sidekiq card ID of interest
   @param[out]   p_carrier_capable  pointer to Carrier Edge Capability to populate

   @return 0 on success, else a negative errno value
   @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t fpga_ctrl_is_carrier_edge_ref_clock_capable( uint8_t card,
                                                     bool* p_carrier_capable );


#endif  /* __SIDEKIQ_FPGA_CTRL_H__ */
