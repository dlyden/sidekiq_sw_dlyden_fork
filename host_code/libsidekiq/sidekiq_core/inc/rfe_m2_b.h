#ifndef __RFE_M2_B_H__
#define __RFE_M2_B_H__

/*****************************************************************************/
/** @file rfe_m2_b.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFE (RF front end)
    implementation for Sidekiq m.2 rev B/C.
*/

#include "sidekiq_rfe.h"

/** @brief function pointers for Sidkeiq m.2 B/C RFE implementation */
extern rfe_functions_t rfe_m2_b_funcs;

#endif  /* __RFE_2_B_H__ */
