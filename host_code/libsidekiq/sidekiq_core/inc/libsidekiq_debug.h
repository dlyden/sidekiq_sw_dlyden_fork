#ifndef LIBSIDEKIQ_DEBUG_H
#define LIBSIDEKIQ_DEBUG_H

/*! \file libsidekiq_debug.h
 * \brief This file contains various macros related to debugging.
 *
 * <pre>
 * Copyright 2013-2021 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 2/12/2013   JRO   Created file
 *
 *</pre>*/

/***** INCLUDES *****/
#include <stdio.h>
#include <stdbool.h>

/***** DEFINES *****/

#define ANSI_RED                        "\033[31m"
#define ANSI_GREEN                      "\033[32m"
#define ANSI_YELLOW                     "\033[33m"
#define ANSI_CYAN                       "\033[36m"
#define ANSI_RESET                      "\033[0m"

/* use string concatenation, so no parentheses allowed */
#define RED(_x)                         ANSI_RED _x ANSI_RESET
#define GRN(_x)                         ANSI_GREEN _x ANSI_RESET
#define YEL(_x)                         ANSI_YELLOW _x ANSI_RESET
#define CYN(_x)                         ANSI_CYAN _x ANSI_RESET

#define PAUSE                           \
    do {                                                                \
        fprintf(stderr, "Waiting for user input in %s at line %u: ", __FUNCTION__, __LINE__); \
        getchar();                                                      \
        fprintf(stderr, "\n");                                          \
    } while (0)


/* DEBUG_ACTIVE=0 -> disable debugging
   DEBUG_ACTIVE=1 -> enable debugging
*/
#define LIBSIDEKIQ_DEBUG_ACTIVE 0

/* TRACE_ACTIVE=0 -> disable func tracing
   TRACE_ACTIVE=1 -> enable func tracing
*/
#define LIBSIDEKIQ_TRACE_ACTIVE 0

#define LOG(fmt, ...)							\
      do { if (LIBSIDEKIQ_DEBUG_ACTIVE) fprintf(stderr, fmt, ##__VA_ARGS__); } while (0)

#define TRACE(fmt, ...)						\
      do { if (LIBSIDEKIQ_TRACE_ACTIVE) fprintf(stderr, "%s:%d:%s()\n" fmt, __FILE__, \
                              __LINE__, __func__, ##__VA_ARGS__); } while (0)

/* if a source file wants to use these MACROs, it *MUST* define DEBUG_PRINT_TRACE_ENABLED to 1 or
 * true *BEFORE* including libsidekiq_debug.h.  It will also define DEBUG_PRINT_ENABLED. */
#if (defined DEBUG_PRINT_TRACE_ENABLED)
# if (!defined DEBUG_PRINT_ENABLED)
# define DEBUG_PRINT_ENABLED
# endif
#define TRACE_ENTER                     do { debug_print("entered function\n"); } while (0)
#define TRACE_EXIT                      do { debug_print("exited function\n"); } while (0)
#define TRACE_EXIT_STATUS(_x)           do { debug_print("exited function with status %d\n", _x); } while (0)
#define TRACE_ARGS(...)                 do { debug_print(__VA_ARGS__); } while (0)
#else
#define TRACE_ENTER                     do { ; } while (0)
#define TRACE_EXIT                      do { ; } while (0)
#define TRACE_EXIT_STATUS(_x)           do { ; } while (0)
#define TRACE_ARGS(...)                 do { ; } while (0)
#endif

/* if a source file wants to use these MACROs, it *MUST* define
 * DEBUG_PRINT_ENABLED to 1 or true *BEFORE* including libsidekiq_debug.h */
#if (defined DEBUG_PRINT_ENABLED)
#define debug_vprint(fmt, ap)                                           \
    do {                                                                \
        fprintf(stderr, RED("== DEBUG == ") GRN("%s") ":%d:" CYN("%s()") ": ", __FILE__, \
                __LINE__, __func__);                                    \
        vfprintf(stderr, fmt, ap);                                      \
    } while (0)
#define debug_print(fmt, ...)                                           \
    do {                                                                \
        fprintf(stderr, RED("== DEBUG == ") GRN("%s") ":%d:" CYN("%s()") ": " fmt, __FILE__, \
                __LINE__, __func__, ##__VA_ARGS__);                     \
    } while (0)
#define debug_print_plain(fmt, ...)                             \
    do {                                                        \
        fprintf(stderr, fmt, ##__VA_ARGS__);                    \
    } while (0)
#else
#define debug_vprint(fmt, ap)           do { ; } while (0)
#define debug_print(fmt, ...)           do { ; } while (0)
#define debug_print_plain(fmt, ...)     do { ; } while (0)
#endif  /* DEBUG_PRINT_ENABLED */

#endif
