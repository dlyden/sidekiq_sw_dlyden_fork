#ifndef __DEBUG_RFIC_ADRV9002_H__
#define __DEBUG_RFIC_ADRV9002_H__

/**
 * @file   debug_rfic_adrv9002.h
 * @date   Tue Sep 22 09:25:01 CDT 2020
 *
 * @brief This file contains debug macros / functions that should only be #included by the
 * rfic_adrv9002.c source file.
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static void debug_print_channel_states( const char * function,
                                        unsigned int line,
                                        uint8_t card )
{
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    adi_adrv9001_RadioState_t current_state;
    int32_t status;

    status = adi_adrv9001_Radio_State_Get( device, &current_state );
    if ( status == 0 )
    {
        debug_print("%s:%u -> channel states: ADI_CHANNEL_1 --> RX:%s TX:%s"
                    " ADI_CHANNEL_2 --> RX:%s TX:%s\n", function, line,
                    channel_state_cstr( current_state.channelStates[ADI_RX][0] ),
                    channel_state_cstr( current_state.channelStates[ADI_TX][0] ),
                    channel_state_cstr( current_state.channelStates[ADI_RX][1] ),
                    channel_state_cstr( current_state.channelStates[ADI_TX][1] ));
    }

    return;
}
#define DEBUG_PRINT_CHANNEL_STATES(_card)                           \
    do {                                                            \
        debug_print_channel_states( __FUNCTION__, __LINE__, _card); \
    } while (0)
#else
#define DEBUG_PRINT_CHANNEL_STATES(_card)    do { } while (0)
#endif  /* DEBUG_PRINT_ENABLED */


#endif  /* __DEBUG_RFIC_ADRV9002_H__ */
