#ifndef __CARD_SERVICES_PRIVATE_H__
#define __CARD_SERVICES_PRIVATE_H__

/**
 * @file   card_services_private.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Feb 27 10:46:38 CST 2020
 * 
 * @brief  
 * 
 * <pre>
 * Copyright 2020 Epiq Solutions, All Rights Reserved
 * </pre>
 */


/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

/***** DEFINES *****/

/***** TYPEDEFS *****/

/***** EXTERN FUNCTIONS  *****/

int32_t card_read_temp_i2c( uint8_t card,
                            uint8_t sensor,
                            int8_t* p_temp_in_deg_C );

int32_t card_read_temp_reg( uint8_t card,
                            uint8_t sensor,
                            int8_t* p_temp_in_deg_C );

#endif  /* __CARD_SERVICES_PRIVATE_H__ */
