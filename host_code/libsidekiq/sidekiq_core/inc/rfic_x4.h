#ifndef __RFIC_X4_H__
#define __RFIC_X4_H__

/***** INCLUDES *****/

#include "sidekiq_rfic.h"
#include "t_ad9528.h"

#if (defined HAS_X4_SUPPORT)
#include "ad9528_register_sets.h"
extern int32_t write_ad9528_regs(
                        uint8_t card,
                        const ad9528_register_t *reg_buf,
                        uint16_t num_regs);
#endif  /* HAS_X4_SUPPORT */



/***** DEFINES *****/
#define RFIC_X4_GPIO_MASK (0x7FFFF)
// FPGA version requirement
#define FPGA_VERS_X4_REV_C              FPGA_VERSION(3,11,1)
#define FPGA_VERS_X4_REV_C_CSTR         FPGA_VERSION_STR(3,11,1)

/***** EXTERN DATA *****/

/*****************************************************************************/
/** @file rfic_x4.h
 
 <pre>
 Copyright 2018 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFIC implementation 
    for the Sidekiq X4, which utilizes the AD9379 (talise) and AD9528.
*/
extern rfic_functions_t rfic_x4_funcs;




#endif /* __RFIC_X4_H__ */
