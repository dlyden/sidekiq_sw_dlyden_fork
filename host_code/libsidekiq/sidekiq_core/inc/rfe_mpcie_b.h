#ifndef __RFE_MPCIE_B_H__
#define __RFE_MPCIE_B_H__

/*****************************************************************************/
/** @file rfe_mpcie_b.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFE (RF front end)
    implementation for Sidekiq mPCIe rev B/C.
*/

#include "sidekiq_rfe.h"

/** @brief function pointers for Sidkeiq mPCIe B/C RFE implementation */
extern rfe_functions_t rfe_mpcie_b_funcs;

#endif  /* __RFE_MPCIE_B_H__ */
