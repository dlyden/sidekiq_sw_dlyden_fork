#ifndef __RFIC_AD9361_H__
#define __RFIC_AD9361_H__

#include "sidekiq_rfic.h"

/*****************************************************************************/
/** @file rfic_ad9361.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFIC implementation 
    for the AD9361.
*/

/** @brief function pointers for AD9361 RFIC implementation */
extern rfic_functions_t rfic_ad9361_funcs;

#endif  /* __RFIC_AD9361_H__ */
