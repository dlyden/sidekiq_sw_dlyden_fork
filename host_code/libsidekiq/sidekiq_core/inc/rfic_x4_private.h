/*****************************************************************************/
/** @file rfic_x4_private.h
 
 <pre>
 Copyright 2018 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the private interface of the Sidekiq X4 RFIC interface.
*/

#ifndef __RFIC_X4_PRIVATE_H__
#define __RFIC_X4_PRIVATE_H__

/***** INCLUDES *****/

#include <stdint.h>

#include "sidekiq_rfic.h"
#include "fpga_jesd_ctrl.h"

/***** FUNCTIONS *****/

extern int32_t _x4_init_3v3_gpio( rf_id_t *p_id, uint32_t output_enable );
extern int32_t _x4_read_3v3_gpio( rf_id_t *p_id, uint32_t *p_value );
extern int32_t _x4_write_3v3_gpio( rf_id_t *p_id, uint32_t value );

extern int32_t reset_jesd_x4( uint8_t card,
                              uint8_t rx_preEmphasis,       // 0..4
                              uint8_t rx_serialAmplitude,   // 0..15
                              uint8_t tx_diffctrl,          // 0..16
                              uint8_t tx_precursor,         // 0..20
                              jesd_sync_result_t *p_sync_result );

extern int32_t reset_jesd_x4_defaults( uint8_t card,
                                       jesd_sync_result_t *p_sync_result );

#endif /* __RFIC_X4_PRIVATE_H__ */
