#ifndef __CARD_SERVICES_H__
#define __CARD_SERVICES_H__

/**
 * @file   card_services.h
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Jun  8 15:34:57 2016
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 *
 *
 * </pre>
 */


/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "sidekiq_types.h"
#include "sidekiq_types_private.h"
#include "hw_iface.h"


/***** DEFINES *****/

/***** TYPEDEFS *****/

/***** EXTERN FUNCTIONS  *****/

int32_t card_read_temp(uint8_t card, uint8_t sensor, int8_t* p_temp_in_deg_C);

int32_t card_read_rfic_temp(uint8_t card, int8_t* p_temp_in_deg_C);

int32_t card_read_fpga_temp(uint8_t card, int8_t* p_temp_in_deg_C);

int32_t card_read_serial_num(uint8_t card, uint32_t *p_serial);

int32_t card_read_hw_version(uint8_t card, uint16_t *p_hw_version);

int32_t card_read_hw_iface_vers(uint8_t card, hw_iface_vers_t *p_hw_iface_vers);

int32_t card_read_part_info(uint8_t card,
                            uint16_t hw_versions,
                            char *p_part_number,
                            char *p_revision,
                            char *p_variant);

int32_t card_read_warp_voltage_calibration( uint8_t card,
                                            uint8_t index,
                                            uint16_t *p_warp_voltage );

int32_t card_write_warp_voltage_calibration( uint8_t card,
                                             uint8_t index,
                                             uint16_t warp_voltage );

/**
    @brief  Determine if a legacy mPCIe / m.2 card is masquerading its revision

    mPCIe rev D & rev E cards (plus possibly some newer m.2 revisions) masquerade their hardware
    revision as an older revision to maintain backwards compatibility with older versions of
    libsidekiq.

    @param[in]  card                The libsidekiq card number
    @param[in]  hw_version          The read hardware version from the card's EEPROM
    @param[in]  p_revision_string   The card's revision string
    @param[out] p_actual_hw_vers    If not NULL, the actual determined hardware version
    @param[out] p_actual_hw_rev     If not NULL, the actual determined hardware revision
*/
void card_unmasquerade(uint8_t card, skiq_hw_vers_t hw_vers, const char *p_revision_string,
    skiq_hw_vers_t *p_actual_hw_vers, skiq_hw_rev_t *p_actual_hw_rev);

/* @todo remove from global scope */
int32_t card_write_accel_reg(uint8_t card, uint8_t reg, uint8_t *p_data, uint8_t length);

/* @todo remove from global scope */
int32_t card_read_accel_reg(uint8_t card, uint8_t reg, uint8_t *p_data, uint8_t length);

int32_t card_read_accel( uint8_t card,
                         int16_t *p_x_data,
                         int16_t *p_y_data,
                         int16_t *p_z_data );

int32_t card_write_accel_state( uint8_t card,
                                uint8_t enabled );

int32_t card_read_ref_clock( uint8_t card, skiq_ref_clock_select_t *p_ref_clock );

int32_t card_write_ext_ref_clock_freq( uint8_t card, uint32_t freq );

int32_t card_read_ext_ref_clock_freq( uint8_t card, uint32_t *p_freq );

int32_t card_write_usb_enumeration_delay(uint8_t card,
                                         uint16_t delay_ms);

int32_t card_read_usb_enumeration_delay(uint8_t card,
                                        uint16_t* p_delay_ms);


int32_t card_read_current_voltage( uint8_t card,
                                   uint8_t sensor,
                                   float *p_current,
                                   float *p_voltage );

int32_t card_update_iq_complex_multiplier( uint8_t card,
                                           skiq_rx_hdl_t hdl,
                                           uint64_t lo_freq );

int32_t card_read_bias_index_calibration( uint8_t card,
                                          uint8_t *p_bias_index );

int32_t card_write_bias_index_calibration( uint8_t card,
                                           uint8_t bias_index );

int32_t card_read_cal_date( uint8_t card,
                            uint16_t *p_cal_year,
                            uint8_t *p_cal_week,
                            uint8_t *p_cal_interval );

int32_t card_write_cal_date( uint8_t card,
                             uint16_t cal_year,
                             uint8_t cal_week,
                             uint8_t cal_interval );

int32_t card_reset_cal_date( uint8_t card );

// locks mutex, disables write protect
int32_t card_eeprom_lock_and_disable_wp( uint8_t card );
// enables write protect, releases mutex
int32_t card_eeprom_unlock_and_enable_wp( uint8_t card );
// convenience function that acquires the lock, performs the write, and releases the lock
int32_t card_write_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

// tells us if hardware write protect available for the part
bool card_eeprom_wp_avail( skiq_part_t part );
// configures the write protect state
int32_t card_set_eeprom_wp( uint8_t card, bool write_protect );

// locks mutex, disables write protect
int32_t card_fmc_eeprom_lock_and_disable_wp( uint8_t card );
// enables write protect, releases mutex
int32_t card_fmc_eeprom_unlock_and_enable_wp( uint8_t card );
// convenience function that acquires the lock, performs the write, and releases the lock
int32_t card_write_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes );

int32_t card_fmc_eeprom_size( uint8_t card );
bool card_fmc_eeprom_has_dba( uint8_t card );

#endif  /* __CARD_SERVICES_H__ */
