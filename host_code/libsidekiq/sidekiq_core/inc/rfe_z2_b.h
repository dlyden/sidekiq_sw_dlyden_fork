#ifndef __RFE_Z2_B_H__
#define __RFE_Z2_B_H__

/*****************************************************************************/
/** @file rfe_z2_b.h
 
 <pre>
 Copyright 2017 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFE (RF front end)
    implementation for Z2 rev B.
*/

#include "sidekiq_rfe.h"

/** @brief function pointers for Z2 rev B RFE implementation */
extern rfe_functions_t rfe_z2_b_funcs;

#define Z2_BIAS_INDEX_MIN               (1)
#define Z2_BIAS_INDEX_MAX               (26)
#define Z2_BIAS_INDEX_DEFAULT           (23)

#endif  /* __RFE_Z2_B_H__ */
