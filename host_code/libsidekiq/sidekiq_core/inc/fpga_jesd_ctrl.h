/**
 * @file   fpga_jesd_ctrl.h
 * @author  <info@epiq-solutions.com>
 * @date   Wed Oct 25 13:32:18 2017
 *
 * @brief
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 *
 * </pre>
 */

#ifndef __FPGA_JESD_CTRL_H__
#define __FPGA_JESD_CTRL_H__

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>

#include "sidekiq_types.h"

/***** EXTERN FUNCTIONS  *****/

// TODO: this probably isn't the right place to define this
typedef struct
{
    uint8_t rx_unsync;
    uint8_t orx_unsync;
    uint8_t tx_unsync;
} jesd_unsync_t;
    
typedef struct
{
    uint32_t jesd_status_imm;
    uint32_t jesd_unsync_imm;

    uint32_t framer_a_rx;
    uint32_t framer_a_orx;
    uint32_t deframer_a;

    uint32_t framer_b_rx;
    uint32_t framer_b_orx;
    uint32_t deframer_b;

    uint32_t jesd_status_dwell;
    uint32_t jesd_unsync_dwell;

    jesd_unsync_t unsync_a;
    jesd_unsync_t unsync_b;

    uint8_t num_retries;
} jesd_sync_result_t;

/*****************************************************************************/
/** The fpga_ctrl_reset function performs a reset of the JESD core of the
    FPGA.

    @param[in] card Sidekiq card of interest
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_jesd_ctrl_core_reset(uint8_t card);

/*****************************************************************************/
/** The fpga_jesd_ctrl_phy_reset function performs a reset of the PHY layer of the
    JESD interface.  This should be done after the clock to the JESD is present.

    @param[in] card Sidekiq card of interest
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_jesd_ctrl_phy_reset(uint8_t card);

/*****************************************************************************/
/** The fpga_jesd_ctrl_tx_config function configures the PHY layer parameters
    for the JESD TX interface.

    Note: X2/X4 only

    @param[in] card Sidekiq card of interest
    @param[in] diffctrl diffcontrol of TX PHY (0:15 is 170mVpp-1080mVpp)
    @param[in] precursor premphasis of TX PHY (0:20 is 0 to 6.02 dB)

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t fpga_jesd_ctrl_tx_config(uint8_t card, uint8_t diffctrl, uint8_t precursor);


/**************************************************************************************************/
/** fpga_jesd_phy_modify_tx_rate() writes the corresponding 'reg' value to all affected TX PHYs on
 * the specified card.  This function writes values to the GTHE4_CHANNEL TXOUT_DIV register in the
 * FPGA.
 *
 * @param[in] card Sidekiq card of interest
 * @param[in] tx_hdl TX handle to modify the rate to
 * @param[in] divider desired divider value
 * @param[in] nr_lanes number of lanes to affect
 *
 * @return int32_t  status where 0=success, anything else is an error
 */
int32_t fpga_jesd_phy_modify_tx_rate( uint8_t card,
                                      skiq_tx_hdl_t tx_hdl,
                                      uint8_t divider,
                                      uint8_t nr_lanes );


int32_t fpga_jesd_write_phy_divider( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     uint8_t divider );

/**************************************************************************************************/
/** fpga_jesd_sync_status() reads the FPGA's JESD status register and reports whether or not the
 * JESD is synchronized for the provided arguments.  The function checks, by default, the RX and ORX
 * sync status of RFIC A.  If @a check_tx is @a true, then the TX sync status is also checked.  If
 * @a check_rfic_b is true, then the JESD sync status on RFIC B is also checked (@a check_tx applies
 * accordingly to this condition as well).
 * 
 * @param[in] card         Sidekiq card index of interest
 * @param[in] check_tx     Indicates whether or not to check TX sync status fields
 * @param[in] check_rfic_b Indicates whether or not to check RFIC B sync status fields (RX/ORX if @a check_tx is false, RX/ORX/TX if @a check_tx is true)
 * 
 * @return int32_t
 * @retval 0 Success
 * @retval -EIO FPGA_REG_JESD_STATUS indicated inadequate JESD sync
 */
int32_t fpga_jesd_sync_status( uint8_t card,
                               bool check_tx,
                               bool check_rfic_b );

int32_t fpga_jesd_qpll_freq_range( uint8_t card, uint64_t *p_min_freq, uint64_t *p_max_freq );

int32_t fpga_jesd_config_tx_lanes( uint8_t card,
                                   skiq_tx_hdl_t tx_hdl,
                                   uint8_t nr_lanes );

int32_t fpga_jesd_config_rx_lanes( uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint8_t num_lanes,
                                   bool dual_chan,
                                   bool swap_chans );

int32_t fpga_jesd_read_tx_lane_sel( uint8_t card, uint8_t *p_lane_sel );

int32_t fpga_jesd_write_tx_lane_sel( uint8_t card, uint8_t lane_sel );

int32_t fpga_jesd_read_orx_lane_sel( uint8_t card,
                                     uint8_t *p_lane_sel );

int32_t fpga_jesd_write_orx_lane_sel( uint8_t card,
                                      uint8_t lane_sel );

int32_t fpga_jesd_write_rx_phase_coherent( uint8_t card,
                                           bool enable );

int32_t fpga_jesd_write_tx_phase_coherent( uint8_t card,
                                           bool enable );

int32_t fpga_jesd_read_unsync_counts( uint8_t card,
                                      jesd_unsync_t *p_unsync_a,
                                      jesd_unsync_t *p_unsync_b,
                                      uint32_t *p_unsync_val );


/**************************************************************************************************/
/** fpga_jesd_calc_lane_div() takes @a fpga_div and shifts it according to the ratio between @a
    sample_rate_khz and @a devclk_khz.  If @a allow_dual_lane indicates a two lane configuration is
    permissible, then @a lane_div and @a nr_lanes can be adjusted accordingly, otherwise @a lane_div
    is adjusted with the single lane restriction.

    @param[in] card Sidekiq card index of interest
    @param[in] sample_rate_khz Requested sample rate in kHz
    @param[in] devclk_khz Device clock frequency in kHz
    @param[in] fpga_div Baseline FPGA PHY divider
    @param[in] allow_dual_lane Whether or not multiple lanes are permitted as a configuration
    @param[out] p_lane_div reference to the required PHY divider
    @param[out] p_nr_lanes reference to the required number of lanes (if applicable)

    @return int32_t
    @retval 0 Success
    @retval -EINVAL The requested sample rate is not a power-of-2 ratio of the device clock frequency
    @retval -ERANGE The sample rate and device clock combination needs a different lane configuration than is permissible
 */
int32_t fpga_jesd_calc_lane_div( uint8_t card,
                                 uint32_t sample_rate_khz,
                                 uint32_t devclk_khz,
                                 uint8_t fpga_div,
                                 bool allow_dual_lane,
                                 uint8_t *p_lane_div,
                                 uint8_t *p_nr_lanes );


/**************************************************************************************************/
/** fpga_jesd_config_sysref_continuous() function configures the SYSREF settings of the FPGA to be
    either disabled or enabled. If enable, the continuous mode and SYSREF is enabled to the FPGA 
    JESD core.  If disable, SYSREF is disabled to the core and the continuous mode bit is clear.

    @param[in] card Sidekiq card index of interest
    @param[in] enable enable or disable continuous sysref to the FGPA

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP FPGA control of bitstream not supported
    @retval -EIO FPGA register transaction not successful
 */
int32_t fpga_jesd_config_sysref_continuous( uint8_t card,
                                            bool enable );

#endif  /* __FPGA_JESD_CTRL_H__ */
