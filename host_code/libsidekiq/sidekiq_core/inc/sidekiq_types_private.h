#ifndef SIDEKIQ_TYPES_PRIVATE_H
#define SIDEKIQ_TYPES_PRIVATE_H

/*! \file sidekiq_types_private.h
 * \brief This file contains the private type definitions used internally for
 * libsidekiq.
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// This enum is used by the RFE section
typedef enum
{
    hw_rev_a=0,
    hw_rev_b,
    hw_rev_c,
    hw_rev_d,
    hw_rev_e,
    // add all revisions above this line
    hw_rev_invalid
} skiq_hw_rev_t;

#define BOARD_ID_AMS_WB3XZD                     ((uint8_t)1)
#define BOARD_ID_IVEIA                          ((uint8_t)2) /* not yet public */
#define BOARD_ID_HTG_K800_WITH_XCKU115          ((uint8_t)3)
#define BOARD_ID_EXTENDED                       ((uint8_t)5) /* look at FPGA_REG_VERSION2 register
                                                              * and EXTENDED_BOARD_ID field */
#define BOARD_ID_HTG_K800_WITH_XCKU060          ((uint8_t)7)

/* EXTENDED_BOARD_ID values */
#define BOARD_ID_EXTENDED_NOT_APPLICABLE        ((uint8_t)0)
#define BOARD_ID_EXTENDED_AMS_WB3XBM            ((uint8_t)1)
#define BOARD_ID_EXTENDED_HTG_K810_WITH_XCKU060 ((uint8_t)2)
#define BOARD_ID_EXTENDED_HTG_K810_WITH_XCKU115 ((uint8_t)3)

#ifdef __cplusplus
}
#endif

#endif
