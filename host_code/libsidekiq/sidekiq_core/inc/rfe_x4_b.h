#ifndef __RFE_X4_B_H__
#define __RFE_X4_B_H__

/*****************************************************************************/
/** @file rfe_x4_b.h
 
 <pre>
 Copyright 2018-2019 Epiq Solutions, All Rights Reserved
 </pre>

    @brief This file contains the public interface of the RFE (RF front end)
    implementation for Sidekiq X4
*/

#include "sidekiq_rfe.h"

/** @brief function pointers for Sidkeiq X4 B RFE implementation */
extern rfe_functions_t rfe_x4_b_funcs;

#endif /* __RFE_X4_B_H__ */
