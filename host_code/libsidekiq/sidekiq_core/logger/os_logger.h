#ifndef _LOGGER_H
#define _LOGGER_H
/*! \file logger.h
 * \brief This file contains a an API for logging
 *
 * <pre>
 * Copyright 2015 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 05/21/2015   MS    Created file
 *
 *</pre>
 */

#include <stdbool.h>
#include <stddef.h>

#ifdef ERROR
#undef ERROR
#endif

#include "sidekiq_types.h"      /* for LOG_level_t */

#ifdef __cplusplus
extern "C" {
#endif

/** @brief a log module object identifier */
typedef void *LOG_moduleId_t;
/** @brief a log module object identifier value.
 * GENERIC_MODULE_ID can be used as the argument in the logging functions,
 * excluding LOG_addModule(), to be included in a generic category of log messages.
 * Programs can use this module without needing to ever call LOG_addModule(). */
#define GENERIC_MODULE_ID ((LOG_moduleId_t)NULL)

/** @brief The maximum unique log modules the library is coded to handle. */
#define LOG_MAX_MODULES (50)

/** @brief add a logging module.
 * Creates an object that can be used with the logging functions.
 *
 * @param[in] name the name of the requested module
 * @return a valid log module object ID or NULL
 */
extern LOG_moduleId_t LOG_addModule(const char * const name);

/** @brief remove a logging module.
 * Destroys a log module object that had been created with LOG_addModule()
 *
 * @param[in] moduleId the log module object ID
 * @return true if successful, or false
 */
extern bool LOG_removeModule(LOG_moduleId_t moduleId);

#ifndef _REMOVE_ALL_DEBUGGING
/** @brief sets the logging level.
 * Sets the level for the specified log module.
 *
 * @param[in] moduleId the log module object ID
 * @param[in] newLevel the new logging level.  All log messages at the
 * specified level or higher will be logged.
 * @return true if successful, or false
 */
extern bool LOG_setLevel(
        const LOG_moduleId_t moduleId,
        const LOG_level_t newLevel);

/** @brief Logs a message.
 * Log a message for the module at the level specified for the module.  If the
 * module's level is at or below the logLevel provided, the message will be
 * provided in the log; otherwise the message will not be included in the log.
 *
 * @param[in] moduleId the log module object ID
 * @param[in] logLevel the requested level of this message.
 * @param[in] format a printf-like variable argument list describing the
 * message to log.  This is expected to be non-null.
 * @return the length of the message logged, zero (0) would indicate the module's
 * level is not set sufficiently for the message to be present in the log.
 */
extern int LOG_logMsg(
        const LOG_moduleId_t moduleId,
        const LOG_level_t logLevel,
        const char * const format,
        ... )
#ifdef __GNUC__
/* check calls against printf-style arguments.  'format' is in position 3,
 * and any arguments begin in position 4.
 */
__attribute__ ((format (printf, 3, 4)));
#else /* __GNUC__ */
;
#endif /* __GNUC__ */

#else
#define LOG_setLevel(a, b)  (0)
#define LOG_logMsg(fmt, ...)    (0)
#endif

#ifdef __cplusplus
}
#endif

#endif /* _LOGGER_H */
