#ifndef __TPRINTF_H
#define __TPRINTF_H
/*! \file tprintf.h
 * \brief This file contains a function to print with the time prepended
 *
 * <pre>
 * Copyright 2015 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 11/03/2009   ASM   Added header
 * 05/21/2015   MS    Added use of vtprintf()
 *
 *</pre>*/

/***** INCLUDES *****/

#include <sys/time.h>
#include <stdarg.h>
#include <stdio.h>
#include "os_logger.h"

#ifdef __cplusplus
extern "C" {
#endif

/***** TYPE DEFINITIONS *****/

/***** DEFINITIONS *****/

/***** FUNCTION PROTOTYPES *****/

/*! \brief Calculates elapsed time between two struct timeval's */
float
elapsed_time (
    const struct timeval * const starting_time,
    const struct timeval * const ending_time );

/*! \brief Calculates elapsed time between starting time and now */
float
elapsed_time_to_now (
    const struct timeval * const starting_time );

/*! \brief Prints with the elapsed time prepended */
int
tprintf (
    const char * const format,
    ... )
#ifdef __GNUC__
/* check calls against printf-style arguments.  'format' is in position 1,
 * and any arguments begin in position 2.
 */
__attribute__ ((format (printf, 1, 2)));
#else /* __GNUC__ */
;
#endif /* __GNUC__ */

#define vtprintf(id,level,format,ap)    vtfprintf(stdout,id,level,format,ap)

int vtfprintf (
    FILE *stream,
    const int id,
    const LOG_level_t level,
    const char * const format,
    va_list argp );

void
TPRINTF_set_elapsed( void );

void
TPRINTF_set_absolute( void );

char *
TPRINTF_error (
    const char * const format,
    ... )
#ifdef __GNUC__
/* check calls against printf-style arguments.  'format' is in position 1,
 * and any arguments begin in position 2.
 */
__attribute__ ((format (printf, 1, 2)));
#else /* __GNUC__ */
;
#endif /* __GNUC__ */



/*! \brief Allocates and prints into a string */
int
mprintf (
    char ** string,
    const char * const format,
    ... )
#ifdef __GNUC__
/* check calls against printf-style arguments.  'format' is in position 2,
 * and any arguments begin in position 3.
 */
__attribute__ ((format (printf, 2, 3)));
#else /* __GNUC__ */
;
#endif /* __GNUC__ */


#ifdef __cplusplus
}
#endif

#endif /* #ifndef __TPRINTF_H */

