/*! \file tprintf.h
 * \brief This file contains a function to print with the time prepended
 *
 * <pre>
 * Copyright 2015 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 11/03/2009   ASM   Added header
 * 05/21/2015   MS    Added use of vtprintf()
 *
 *</pre>*/

/***** INCLUDES *****/

#include <pthread.h> /* thread stuff */
#include <stdarg.h> /* to replicate variable arguments of printf */
#include <sys/time.h> /* gettimeofday */
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "tprintf.h"

//#define _INCLUDE_THREAD_ID_IN_LOGGER

#ifdef _INCLUDE_THREAD_ID_IN_LOGGER
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#endif /* _INCLUDE_THREAD_ID_IN_LOGGER */


/***** TYPE DEFINITIONS *****/

/***** DEFINITIONS *****/

/***** FILE GLOBALS *****/

/* variables to allow the starting time to be set the first time the function is called */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static struct timeval g_starting_time;
static bool first_pass = false;
static bool print_elapsed = true;

/***** FUNCTIONS *****/

/*! \brief Calculates elapsed time between two struct timeval's */
float
elapsed_time(
    const struct timeval * const starting_time,
    const struct timeval * const ending_time )
{
    float elapsed_time = 0.;
    if ( ( NULL != starting_time ) &&
         ( NULL != ending_time ) )
    {
        elapsed_time = ending_time->tv_sec - starting_time->tv_sec;
        elapsed_time += ( ending_time->tv_usec - starting_time->tv_usec ) / 1.e6;
    }
    return ( elapsed_time );
} /* end elapsed_time */


/*! \brief Calculates elapsed time between starting time and now */
float
elapsed_time_to_now (
    const struct timeval * const starting_time )
{
    struct timeval ending_time;
    float elapsed_time = 0.;
    if ( NULL != starting_time )
    {
        gettimeofday( &ending_time, NULL );
        elapsed_time = ending_time.tv_sec - starting_time->tv_sec;
        elapsed_time += ( ending_time.tv_usec - starting_time->tv_usec ) / 1.e6;
    }
    return ( elapsed_time );
} /* end elapsed_time_to_now */


/*! \brief Allocates and prints into a string */
int
mprintf (
    char ** string,
    const char * const format,
    ... )
{
    va_list argp;
    int length = 0; /* return length printed like standard printf */
    int existing_length;
    int new_length;
    char * tmp;

    if ( ( NULL != string ) &&
         ( NULL != format ) )
    {
        existing_length = ( NULL == *string ) ? 0 : strlen( *string );
        va_start( argp, format );
        new_length = 1 + vsnprintf( NULL, 0, format, argp );
        va_end ( argp );
        tmp = (char *)realloc( *string, existing_length + new_length );
        if ( NULL != tmp )
        {
            *string = tmp;
            va_start( argp, format );
            length = vsnprintf( *string + existing_length, new_length, format, argp );
            va_end ( argp );
        }
    }
    if ( ( 0 == length ) &&
         ( NULL != string ) )
    {
        *string = NULL;
    }
    return ( length );
} /* end mprintf */


void
TPRINTF_set_elapsed( void )
{
    print_elapsed = true;
}


void
TPRINTF_set_absolute( void )
{
    print_elapsed = false;
}

/*! \brief Prints with the elapsed time prepended */
int vtfprintf (
    FILE *stream,
    const int id,
    const LOG_level_t level,
    const char * const format,
    va_list argp )
{

    struct timeval current_time;
    int length = 0; /* return length printed like standard printf */

    if ( NULL != format )
    {
        gettimeofday( &current_time, NULL );
        /* only allow one thread to print at a time */
        pthread_mutex_lock( &mutex );
        if ( false == first_pass )
        {
            memcpy( &g_starting_time, &current_time, sizeof(g_starting_time) );
            first_pass = true;
        }

        if ( true == print_elapsed )
        {
            /* subtract the starting time from the current time to calculate elapsed time */
            current_time.tv_sec -= g_starting_time.tv_sec;
            /* if current usec is greater, just subtract, otherwise carry from seconds */
            if ( current_time.tv_usec >= g_starting_time.tv_usec )
            {
                current_time.tv_usec -= g_starting_time.tv_usec ;
            }
            else
            {
                current_time.tv_sec--;
                current_time.tv_usec -= g_starting_time.tv_usec - 1000000UL;
            }
        }
        /* print time */
#ifdef _INCLUDE_THREAD_ID_IN_LOGGER
        length = fprintf( stream, "%lu.%06lu [%d:%d] (%ld): ", current_time.tv_sec, current_time.tv_usec, moduleId, level, syscall(SYS_gettid));
#else
        length = fprintf( stream, "%lu.%06lu [%d:%d]: ", current_time.tv_sec, current_time.tv_usec, id, level );
#endif /* _INCLUDE_THREAD_ID_IN_LOGGER */
        length += vfprintf( stream, format, argp );
        pthread_mutex_unlock( &mutex );
    }
    return ( length );
} /* end tprintf */

int tprintf(const char * const format, ...)
{
    int result;

    va_list argp;
    va_start(argp, format);
    result = vtprintf(0, 0, format, argp);
    va_end(argp);

    return (result);
}

/*! \brief Prints with the elapsed time prepended to an allocated string which is returned */
char *
TPRINTF_error (
    const char * const format,
    ... )
{
    char * string = NULL; /* pointer to space allocated to hold printed string for return to caller */
    struct timeval current_time;
    va_list argp;
    int length; /*!< length of pieces as they are printed */
    int string_length; /*!< length of string allocated to hold all printing */

    if ( NULL != format )
    {
        gettimeofday( &current_time, NULL );
        pthread_mutex_lock( &mutex );
        if ( false == first_pass )
        {
            memcpy( &g_starting_time, &current_time, sizeof(g_starting_time) );
            first_pass = true;
        }
        pthread_mutex_unlock( &mutex );

        /* subtract the starting time from the current time to calculate elapsed time */
        current_time.tv_sec -= g_starting_time.tv_sec;
        /* if current usec is greater, just subtract, otherwise carry from seconds */
        if ( current_time.tv_usec >= g_starting_time.tv_usec )
        {
            current_time.tv_usec -= g_starting_time.tv_usec ;
        }
        else
        {
            current_time.tv_sec--;
            current_time.tv_usec -= g_starting_time.tv_usec - 1000000UL;
        }
        /* print time */
        length = snprintf( string, 0, "%lu.%06lu : ", current_time.tv_sec, current_time.tv_usec );
        va_start( argp, format );
        length += vsnprintf( string, 0,  format, argp );
        va_end ( argp );
        string_length = length + 1; /* add one for end-of-string */
        string = (char *)malloc( string_length );
        if ( NULL != string )
        {
            length = snprintf( string, string_length, "%lu.%06lu : ", current_time.tv_sec, current_time.tv_usec );
            va_start( argp, format );
            vsnprintf( string + length, string_length - length, format, argp );
            va_end ( argp );
            perror( string );
        }
    }
    return ( string );
} /* end TPRINTF_error */


