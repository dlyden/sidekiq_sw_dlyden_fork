/*! \file logger.c
 * \brief This file contains a an API for logging
 *
 * <pre>
 * Copyright 2015 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 05/21/2015   MS    Created file
 *
 *</pre>
 */


#include "os_logger.h"
#include "tprintf.h"
#include <stdarg.h> /* to replicate variable arguments of printf */
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

#ifndef _REMOVE_ALL_DEBUGGING
/* don't use entry with ID=0 since we reserve it for the "generic" module */
static int g_nextId = 1;

/* null-terminated string of at most 32 characters */
#define MAX_MODULE_NAME_LENGTH  (33)

typedef struct _logModuleObject {
    int id;
    char name[MAX_MODULE_NAME_LENGTH];
    LOG_level_t activeLevel;
} logModuleObject_t;


static logModuleObject_t g_genericLogModuleObject = {0, "generic", LOG_ALL};

LOG_moduleId_t LOG_addModule(const char * const name)
{
    logModuleObject_t *moduleObj;
    LOG_moduleId_t moduleId;

    /* any slots left? */
    if (g_nextId >= LOG_MAX_MODULES) {
        return (NULL);
    }


    moduleObj = malloc(sizeof(logModuleObject_t));
    moduleId = (LOG_moduleId_t)moduleObj;

    if (moduleObj) {
        /* use the next ID and then increment it */
        moduleObj->id = g_nextId++;
        strncpy(moduleObj->name, name, sizeof(moduleObj->name));
        /* if we were given a name over MAX_MODULE_NAME_LENGTH - 1 characters,
         * the following will ensure it's null-terminated */
        moduleObj->name[MAX_MODULE_NAME_LENGTH - 1] = '\0';
        moduleObj->activeLevel = LOG_ALL;
        /* return the handle */
        moduleId = (LOG_moduleId_t *)moduleObj;
    }
    else {
        return (NULL);
    }
    return (moduleId);
}

bool LOG_removeModule(LOG_moduleId_t moduleId)
{
    bool status = false;

    logModuleObject_t *moduleObj = (logModuleObject_t *)moduleId;
    /* do some basic sanity test that the memory points to a valid log object */
    if ((moduleObj != NULL) &&
            ((moduleObj->id >= 0) && (moduleObj->id < LOG_MAX_MODULES)) &&
            ((moduleObj->activeLevel >= LOG_ALL) && (moduleObj->activeLevel <= LOG_NONE))) {
        free(moduleObj);
        status = true;
    }
    return (status);
}


bool LOG_setLevel(
        const LOG_moduleId_t moduleId,
        const LOG_level_t newLevel)
{
    bool status = false;

    logModuleObject_t *moduleObj = (logModuleObject_t *)moduleId;
    if (moduleObj == NULL) {
        moduleObj = &g_genericLogModuleObject;
    }

    /* keep track of the logging level on a per-module basis */
    if (((moduleObj->id >= 0) && (moduleObj->id < LOG_MAX_MODULES)) &&
            ((newLevel >= LOG_ALL) && (newLevel <= LOG_NONE))) {
        moduleObj->activeLevel = newLevel;
        status = true;
    }

    return (status);
}


int LOG_logMsg(
        const LOG_moduleId_t moduleId,
        const LOG_level_t level,
        const char * const format,
        ... )
{
    int length = 0;
    va_list argp;

    logModuleObject_t *moduleObj = (logModuleObject_t *)moduleId;
    if (moduleObj == NULL) {
        moduleObj = &g_genericLogModuleObject;
    }


    if (level >= moduleObj->activeLevel) {
        va_start( argp, format );
        length += vtfprintf( stderr, moduleObj->id, level, format, argp );
        va_end ( argp );
        fflush(stderr);
    }

    return (length);
}


#endif
