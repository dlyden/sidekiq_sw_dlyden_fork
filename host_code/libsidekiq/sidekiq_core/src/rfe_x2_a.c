/*****************************************************************************/
/** @file rfe_x2_a.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

#include "rfe_x2_a.h"
#include "rffc5071a_api.h"
#include "sidekiq_private.h"
#include "sidekiq_rffc5071a_private.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_hal.h"
#include "bit_ops.h"
#include "card_services.h"

/* enable debug_print and debug_print_plain when DEBUG_RFE_X2 is defined */
#if (defined DEBUG_RFE_X2)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/***** Defines *****/

#define NUM_RX_HDLS (3)
#define NUM_TX_HDLS (2)

/* define minimum, maximum, and invalid RX Attenuation settings.  Setting an invalid value outside
 * of the usable range should guarantee that any request to write the attenuation will result in
 * actually writing the setting down to the IC */
#define RX_ATTENUATION_MIN              0
#define RX_ATTENUATION_MAX              127
#define RX_ATTENUATION_INVALID          (RX_ATTENUATION_MAX + 1)

#define PLL_SPI_CS_PLL1 (0) // PLL1
#define PLL_SPI_CS_PLL2 (1) // PLL2

/* Possible values for the SPI_CTRL_SELECT field of FPGA CTRL register (addr 0x86A8) */
#define SPI_CTRL_SELECT_ATTEN           0
#define SPI_CTRL_SELECT_RFFC            1

#define RFFC_REF_CLOCK (30720000)

// PLL1
#define RXA1_FILTER_SEL_OFFSET (0)
#define RXA2_FILTER_SEL_OFFSET (3)
// PLL2
#define RXB1_FILTER_SEL_OFFSET (0)
#define TXA1_FILTER_SEL_OFFSET (3)

#define FILTER_SEL_LEN     (3)

#define EEPROM_FILTER_CONFIG_OFFSET (100)
#define EEPROM_FILTER_CONFIG_ELEMENT_SIZE (sizeof(uint8_t))

/*  Max number of filter elements + 1
 *  same as_x2_rev_a_num_rx_filters_with_preselect + 1 */
#define EEPROM_FILTER_CONFIG_NUM_ELEMENTS (9)

#define RX_PARAM_INIT                                                   \
{                                                                           \
    .filters = {NULL, 0, 0},                                                \
    .attenuation_mode = skiq_rx_attenuation_mode_noise_figure,              \
    .attenuation = RX_ATTENUATION_INVALID,                                  \
    .freq = 0,                                                              \
    .filter_config = skiq_filt_invalid                                      \
}

#define TX_PARAM_INIT                                                       \
{                                                                           \
    .freq = 0,                                                              \
    .filter_config = skiq_filt_invalid                                      \
}

#define RFE_PARAM_INIT                                                      \
{                                                                           \
    .rx = {                                                                 \
        [0 ... (NUM_RX_HDLS-1)] = RX_PARAM_INIT,                            \
    },                                                                      \
    .tx = {                                                                 \
        [0 ... (NUM_TX_HDLS-1)] = TX_PARAM_INIT,                            \
    },                                                                      \
    .rx_filter_indexes = {                                                  \
        [0 ... (EEPROM_FILTER_CONFIG_NUM_ELEMENTS-1)] = 0,                  \
    },                                                                      \
}

/***** Structs *****/

typedef struct
{
    skiq_filt_t filter;
    uint64_t start_freq;
    uint64_t stop_freq;
    uint8_t sw_setting;
} _filter_config_t;

typedef struct
{
    uint8_t *p_filter_indexes;
    uint8_t num_filters;
    uint8_t select_offset;
} _filter_path_t;

typedef struct
{
    _filter_path_t filters;
    skiq_rx_attenuation_mode_t attenuation_mode;
    uint16_t attenuation;
    uint64_t freq;
    skiq_filt_t filter_config;
} _rx_param_t;

typedef struct
{
    uint64_t freq;
    skiq_filt_t filter_config;
} _tx_param_t;

typedef struct
{
    _rx_param_t rx[NUM_RX_HDLS];
    _tx_param_t tx[NUM_TX_HDLS];
    uint8_t rx_filter_indexes[EEPROM_FILTER_CONFIG_NUM_ELEMENTS];
} _rfe_param_t;

/***** Local Variables *****/

const skiq_filt_t _x2_rev_a_rx_filters_with_preselect[] =
{
    skiq_filt_0_to_440MHz,
    skiq_filt_440_to_580MHz,
    skiq_filt_580_to_810MHz,
    skiq_filt_810_to_1170MHz,
    skiq_filt_1170_to_1695MHz,
    skiq_filt_1695_to_2540MHz,
    skiq_filt_2540_to_3840MHz,
    skiq_filt_3840_to_6000MHz
};
const uint8_t _x2_rev_a_num_rx_filters_with_preselect =
    sizeof(_x2_rev_a_rx_filters_with_preselect)/sizeof(skiq_filt_t);

const skiq_filt_t _x2_rev_a_rx_filters_no_preselect[] =
{
    skiq_filt_0_to_440MHz,
    skiq_filt_440_to_6000MHz
};
const uint8_t _x2_rev_a_num_rx_filters_no_preselect =
    sizeof(_x2_rev_a_rx_filters_no_preselect)/sizeof(skiq_filt_t);

// Note: generic utilities for filter config may be worth supporting at some point
// However, since we really only have 2 filter options (all or nothing), we'll
// make this a bit more specific for now.  For generic parameterization, the flash
// functions may provide a good example of how to support it...we may also want
// some generic utility functions to auto select the appropriate filter outside
// of the hardware specific implementation at some point.

// all RX filters
static const _filter_config_t _rx_filter_config[] =
{
    {    skiq_filt_0_to_440MHz,           0,    440000000,   0},
    { skiq_filt_440_to_6000MHz,   440000000,   6000000001,   3},   // (SW1, SW0)    
    {  skiq_filt_440_to_580MHz,   440000000,    580000000,   4},   // (SW2)
    {  skiq_filt_580_to_810MHz,   580000000,    810000000,   7},   // (SW2, SW1, SW0)
    { skiq_filt_810_to_1170MHz,   810000000,   1170000000,   3},   // (SW1, SW0)
    {skiq_filt_1170_to_1695MHz,  1170000000,   1695000000,   2},   // (SW1)
    {skiq_filt_1695_to_2540MHz,  1695000000,   2540000000,   5},   // (SW2, SW0)
    {skiq_filt_2540_to_3840MHz,  2540000000,   3840000000,   1},   // (SW0)
    {skiq_filt_3840_to_6000MHz,  3840000000,   6000000001,   6},   // (SW2, SW1)
};
static_assert((sizeof(_rx_filter_config)/sizeof(_filter_config_t)) ==
              EEPROM_FILTER_CONFIG_NUM_ELEMENTS,
              "array size incorrect");

// Note: TX2 doesn't have anything!
static const _filter_config_t _tx1_filter_config[] =
{
    {   skiq_filt_0_to_300MHz,            0,    300000000,   2},   // (SW1) 
    {skiq_filt_300_to_6000MHz,    300000000,   6000000000,   5},   // (SW2, SW0)
};

#define X2_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define X2_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define X2_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define X2_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)


static _rfe_param_t _rfe[SKIQ_MAX_NUM_CARDS] =
{
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = RFE_PARAM_INIT,
};

/***** Local Functions *****/

// TODO: this probably becomes obsolete?
static int32_t _init_rfe_defaults( uint8_t card );

static int32_t _update_rffc_gpo( uint64_t id, uint8_t gpo_val,
                                 uint8_t gpo_offset, uint8_t gpo_len );

static int32_t _find_filter( uint64_t freq,
                             const _filter_config_t *p_filter_config,
                             uint8_t *p_filter_indexes,
                             uint8_t num_filters,
                             skiq_filt_t *p_filter );    // out

static int32_t _rffc_reg_read( uint64_t id, uint8_t addr, uint16_t *p_val );
static int32_t _rffc_reg_write( uint64_t id, uint8_t addr, uint16_t val );
static int32_t _pll_reg_read( uint8_t card, uint8_t pll_sel, uint8_t addr, uint16_t *p_val );
static int32_t _pll_reg_write( uint8_t card, uint8_t pll_sel, uint8_t addr, uint16_t val );

static int32_t _atten_spi_write( uint8_t card, skiq_rx_hdl_t hdl, uint8_t data );

static int32_t _init( rf_id_t *p_id );

static int32_t _update_tx_pa( rf_id_t *p_id,
                              bool enable );
static int32_t _update_rx_lna( rf_id_t *p_id,
                               rfe_lna_state_t new_state );
static int32_t _update_tx_lo( rf_id_t *p_id,
                              uint64_t tx_lo_freq );
static int32_t _write_rx_filter_path( rf_id_t *p_id,
                                       skiq_filt_t path );
static int32_t _write_tx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t path );
static int32_t _read_rx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t *p_path );
static int32_t _read_tx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t *p_path );
static int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters );
static int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                              skiq_filt_t *p_filters,
                                              uint8_t *p_num_filters );

static int32_t _write_filters( rf_id_t *p_id,
                               uint8_t num_filters,
                               const skiq_filt_t *p_filters );

static int32_t _write_rx_attenuation( rf_id_t *p_id, uint16_t attenuation );

static int32_t _read_rx_attenuation_range( rf_id_t *p_id,
                                           uint16_t *atten_quarter_db_max,
                                           uint16_t *atten_quarter_db_min );

static int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );
static int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
static int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list);
static int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );


static int32_t _write_rx_attenuation_normalized( rf_id_t *p_id )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    uint64_t freq = _rfe[card].rx[hdl].freq;
    uint8_t atten = 0;
    float tmp = 0.0;
    int32_t status = 0;

    // The following equation should help to achieve a consistent gain across
    // the entire frequency range of X2.
    // 0 attenuation at 6Ghz, 12db (48 in quarter dB steps) attenuation at 0Hz
    tmp = 48.0 - (8.0e-9 * freq);

    if( tmp < RX_ATTENUATION_MIN ) tmp = RX_ATTENUATION_MIN;
    if( tmp > RX_ATTENUATION_MAX ) tmp = RX_ATTENUATION_MAX;

    atten = (uint8_t) tmp;

    status = _atten_spi_write(card, hdl, atten);

    return status;
}

static int32_t _write_rx_attenuation_noise_figure( rf_id_t *p_id )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    uint8_t atten = 0;
    int32_t status = 0;

    // 0 attenuation across all frequencies for best noise figure
    atten = 0;

    status = _atten_spi_write(card, hdl, atten);

    return status;
}

int32_t _rffc_reg_read( uint64_t id, uint8_t addr, uint16_t *p_val )
{
    int32_t status=0;
    uint8_t card=0;
    uint8_t pll_sel=0;

    card = X2_MAP_PLL_ID_TO_SKIQ_CARD(id);
    pll_sel = X2_MAP_PLL_ID_TO_SPI_CS(id);
    status=_pll_reg_read( card, pll_sel, addr, p_val );

    return (status);
}

int32_t _rffc_reg_write( uint64_t id, uint8_t addr, uint16_t val )
{
    int32_t status=0;
    uint8_t card=0;
    uint8_t pll_sel=0;
    
    card = X2_MAP_PLL_ID_TO_SKIQ_CARD(id);
    pll_sel = X2_MAP_PLL_ID_TO_SPI_CS(id);
    status=_pll_reg_write( card, pll_sel, addr, val );

    return (status);
}

int32_t _pll_reg_read( uint8_t card, uint8_t pll_sel, uint8_t addr, uint16_t *p_val )
{
    int32_t status=0;
    uint32_t reg=0;
    uint32_t spi_ready_attempts=0;

    /* Selecting the RFFC SPI in FPGA_CTRL start the free running SPI clock */
    sidekiq_fpga_reg_RMWV( card, SPI_CTRL_SELECT_RFFC, SPI_CTRL_SELECT, FPGA_REG_FPGA_CTRL );

    reg = 0;
    RBF_SET(reg, pll_sel, PLL_SPI_CS);
    RBF_SET(reg, 1, PLL_SPI_OP); /* 0 = write, 1 = read */
    RBF_SET(reg, addr, PLL_SPI_ADDR);
    sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_PLL_SPI_WR, reg);

    reg=0;
    while(spi_ready_attempts < 100)
    {
        sidekiq_fpga_reg_read(card, FPGA_REG_PLL_SPI_RD, &reg);
        if ( RBF_GET(reg, PLL_SPI_DONE) > 0 )
        {
            break;
        }
        spi_ready_attempts++;
        hal_microsleep(1);
    }

    if (spi_ready_attempts >= 100)
    {
        skiq_error("Failed to read PLL reg addr 0x%04X on card %u, aborting\n", addr, card);
        hal_critical_exit(-1);
    }

    *p_val = RBF_GET(reg, PLL_SPI_RD_DATA);

    /* Selecting the Attenuation SPI in FPGA_CTRL stops the free running SPI clock, which can screw
     * up the Attenuation IC's control interface */
    sidekiq_fpga_reg_RMWV( card, SPI_CTRL_SELECT_ATTEN, SPI_CTRL_SELECT, FPGA_REG_FPGA_CTRL );

    return (status);
}

int32_t _pll_reg_write( uint8_t card, uint8_t pll_sel, uint8_t addr, uint16_t val )
{
    int32_t status=0;
    uint32_t reg=0;
    uint32_t spi_ready_attempts=0;

    /* Selecting the RFFC SPI in FPGA_CTRL start the free running SPI clock */
    sidekiq_fpga_reg_RMWV( card, SPI_CTRL_SELECT_RFFC, SPI_CTRL_SELECT, FPGA_REG_FPGA_CTRL );

    reg=0;
    RBF_SET(reg, pll_sel, PLL_SPI_CS);
    RBF_SET(reg, 0, PLL_SPI_OP); /* 0 = write, 1 = read */
    RBF_SET(reg, addr, PLL_SPI_ADDR);
    RBF_SET(reg, val, PLL_SPI_WR_DATA);
    sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_PLL_SPI_WR, reg);

    reg=0;
    while(spi_ready_attempts < 100)
    {
        sidekiq_fpga_reg_read(card, FPGA_REG_PLL_SPI_RD, &reg);
        if ( RBF_GET(reg, PLL_SPI_DONE) > 0 )
        {
            break;
        }
        spi_ready_attempts++;
        hal_microsleep(1);
    }

    if (spi_ready_attempts >= 100)
    {
        skiq_error("Failed to write PLL reg addr 0x%04X on card %u, aborting\n", addr, card);
        hal_critical_exit(-1);
    }

    /* Selecting the Attenuation SPI in FPGA_CTRL stops the free running SPI clock, which can screw
     * up the Attenuation IC's control interface */
    sidekiq_fpga_reg_RMWV( card, SPI_CTRL_SELECT_ATTEN, SPI_CTRL_SELECT, FPGA_REG_FPGA_CTRL );

    return (status);
}

int32_t _atten_spi_write( uint8_t card, skiq_rx_hdl_t hdl, uint8_t data )
{
    uint32_t reg=0;
    uint32_t spi_ready_attempts=0;
    int32_t status=0;
    uint32_t offset=0;
    uint32_t len=0;

    // no need to update unless there's a change
    if ( data == _rfe[card].rx[hdl].attenuation )
    {
        debug_print("Skip updating RX attenuation since request (%u) does not change setting on "
                    "card %u and hdl %s\n", data, card, rx_hdl_cstr( hdl ));
        return (0);
    }
    debug_print("Updating RX attenuation to %u on card %u and hdl %s\n", data, card,
                rx_hdl_cstr( hdl ));

    switch( hdl )
    {
        case skiq_rx_hdl_A1:
            offset = FMC_RX1_ATTEN_LE_OFFSET;
            len = FMC_RX1_ATTEN_LE_LEN;
            break;

        case skiq_rx_hdl_A2:
            offset = FMC_RX2_ATTEN_LE_OFFSET;
            len = FMC_RX2_ATTEN_LE_LEN;
            break;

        case skiq_rx_hdl_B1:
            offset = FMC_ORX_ATTEN_LE_OFFSET;
            len = FMC_ORX_ATTEN_LE_LEN;
            break;

        default:
            status = -1;
            break;
    }

    if ( status == 0 )
    {
        /* make sure that the SPI clock isn't constantly running.  Selecting the Attenuation SPI in
         * FPGA_CTRL stops the free running SPI clock. */
        sidekiq_fpga_reg_RMWV( card, 0, SPI_CTRL_SELECT, FPGA_REG_FPGA_CTRL);

        reg = 0;
        RBF_SET(reg, data, ATTEN_DATA);
        sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_ATTEN_DATA, reg);

        reg = 0;
        while ( spi_ready_attempts < 100 )
        {
            sidekiq_fpga_reg_read( card, FPGA_REG_ATTEN_STAT, &reg );
            if ( RBF_GET(reg, ATTEN_STAT) > 0 )
            {
                break;
            }
            spi_ready_attempts++;
            hal_microsleep(1);
        }

        if (spi_ready_attempts >= 100)
        {
            skiq_error("Failed to write RX attenuation value on card %u and hdl %s, aborting\n",
                       card, rx_hdl_cstr( hdl ));
            hal_critical_exit(-1);
        }

        // now we need to hit the latch
        sidekiq_fpga_reg_read(card, FPGA_REG_FMC_CTRL, &reg);
        BF_SET(reg, 1, offset, len );
        sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_FMC_CTRL, reg );
        hal_nanosleep(MICROSEC);
        BF_SET(reg, 0, offset, len );
        sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_FMC_CTRL, reg );

        _rfe[card].rx[hdl].attenuation = data;
    }

    return (status);
}

int32_t _update_rffc_gpo( uint64_t id, uint8_t gpo_val,
                          uint8_t gpo_offset, uint8_t gpo_len )
{
    int32_t status=0;
    uint8_t gpo=0;

    if( (status=rffc5071a_get_gpo( id, &gpo )) == 0 )
    {
        BF_SET( gpo, gpo_val, gpo_offset, gpo_len );
        status=rffc5071a_set_gpo( id, gpo );
    }

    return (status);
}


/* _read_rffc_gpo is not currently used */
#if 0
int32_t _read_rffc_gpo( uint64_t id, uint8_t *p_gpo_val,
                        uint8_t gpo_offset, uint8_t gpo_len )
{
    int32_t status=0;
    uint8_t gpo=0;

    if( (status=rffc5071a_get_gpo( id, &gpo )) == 0 )
    {
        *p_gpo_val = BF_GET( gpo, gpo_offset, gpo_len );
    }

    return (status);
}
#endif

int32_t _init_rfe_defaults( uint8_t card )
{
    int32_t status=0;
    uint64_t id=0;
    uint8_t i=0;
    rf_id_t rf_id;

    rffc5071a_register_functions( _rffc_reg_read, _rffc_reg_write );
    // thanks MR for telling me what to do: http://confluence/display/EN/RFFC5017a+software+driver+library
    rf_id.card = card;

    for( i=0; (i<NUM_RX_HDLS) && (0==status); i++ )
    {
        id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(card, i);

        if( rffc5071a_init(id) == 0 )
        {
            _skiq_log(SKIQ_LOG_INFO, "successfully initialized RFFC\n");
            if( (rffc5071a_set_ref_clock_value(id, RFFC_REF_CLOCK) != 0) ||
                (rffc5071a_set_soft_control(id, true) != 0) ||
                (rffc5071a_set_four_wire_spi(id, false) != 0) ||
                (rffc5071a_set_full_duplex(id, true) != 0) )
            {
                _skiq_log(SKIQ_LOG_ERROR, "unable to configure RFFC\n");
                status=-2;
            }
        }
        else
        {
            _skiq_log(SKIQ_LOG_ERROR, "unable to initialize RFFC\n");
            status=-1;
        }

        rf_id.hdl = i;

        // turn on the LNAs by default
        if( 0 == status )
        {
            status = _update_rx_lna( &rf_id, lna_enabled );
        }

        // configure rx attenuation to optimize for noise figure
        if( 0 == status )
        {
            _rfe[card].rx[i].attenuation_mode =
                skiq_rx_attenuation_mode_noise_figure;
            status = _write_rx_attenuation_noise_figure(&rf_id);
        }
    }

    return (status);
}

int32_t _init( rf_id_t *p_id )
{
    uint8_t card = p_id->card;
    int32_t status=0;
    uint32_t val=0;
    uint8_t num_filters=0;
    uint8_t i=0;

    
    /////////////////////////////////
    // reset chips
    sidekiq_fpga_reg_read( card,
                           FPGA_REG_FMC_CTRL,
                           &val );
    // make sure the various reset pins are cleared
    BF_SET( val, 0, PLL1_RESET_N_OFFSET, PLL1_RESET_N_LEN );
    BF_SET( val, 0, PLL2_RESET_N_OFFSET, PLL2_RESET_N_LEN );
    sidekiq_fpga_reg_write( card,
                            FPGA_REG_FMC_CTRL,
                            val );
    sidekiq_fpga_reg_verify( card, FPGA_REG_FMC_CTRL, val );
    /////////////////////////////////


    /////////////////////////////////
    // turn on the receivers, mykonos, 153.6M clock. 10M clock
    sidekiq_fpga_reg_read( card,
                           FPGA_REG_FMC_CTRL,
                           &val );
    BF_SET( val, 1, MYK_PWR_EN_OFFSET, MYK_PWR_EN_LEN );
    BF_SET( val, 1, PLL1_RESET_N_OFFSET, PLL1_RESET_N_LEN );
    BF_SET( val, 1, PLL2_RESET_N_OFFSET, PLL2_RESET_N_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_FMC_CTRL, val );
    sidekiq_fpga_reg_verify( card, FPGA_REG_FMC_CTRL, val );
    /////////////////////////////////

    ///////////////////////////////
    // TODO: this should be removed...there seems to be an issue where the LNA for A2
    // takes some time to ramp up
    _skiq_log(SKIQ_LOG_DEBUG, "Enabling all LNAs\n");
    RBF_SET( val, 1, X2_FMC_EN_RXA1_X4_FMC_J6_SEL0 );
    RBF_SET( val, 1, X2_FMC_EN_RXA2_X4_FMC_J6_SEL1 );
    RBF_SET( val, 1, X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL );
    sidekiq_fpga_reg_write( card, FPGA_REG_FMC_CTRL, val );
    sidekiq_fpga_reg_verify( card, FPGA_REG_FMC_CTRL, val );

    /////////////////////////////////
    // read the filter config from EEPROM
    status = hal_read_eeprom( card,
                     EEPROM_FILTER_CONFIG_OFFSET,
                     &(num_filters),
                     sizeof(uint8_t) );
    if (status == 0)
    {
        if( num_filters >= EEPROM_FILTER_CONFIG_NUM_ELEMENTS )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                    "Invalid # (%u) of filter elements configured, defaulting to none\n",
                    num_filters);
        }
        else
        {
            status = hal_read_eeprom( card,
                            (EEPROM_FILTER_CONFIG_OFFSET+1),
                            &(_rfe[card].rx_filter_indexes[0]),
                            num_filters );
            // note: we only have a single filter config for all RX handles...
            if (status == 0)
            {
                for( i=0; i<NUM_RX_HDLS; i++ )
                {
                    _rfe[card].rx[i].filters.p_filter_indexes = _rfe[card].rx_filter_indexes;
                    _rfe[card].rx[i].filters.num_filters = num_filters;
                    _rfe[card].rx[i].filters.select_offset =
                        (i == skiq_rx_hdl_A1) ? RXA1_FILTER_SEL_OFFSET :
                        (i == skiq_rx_hdl_A2) ? RXA2_FILTER_SEL_OFFSET :
                        (i == skiq_rx_hdl_B1) ? RXB1_FILTER_SEL_OFFSET :
                        0;
                }
            }
        }
    }
    else
    {
        skiq_error("Unable to read filter config from EEPROM on card %" PRIu8 " (status %" PRIi32 ")\n",card, status);
    }
    /////////////////////////////////

    // TODO: this probably gets removed...
    if( 0 == status )
    {
        status = _init_rfe_defaults( card );
    }

    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to initialize RFE\n");
    }

    return (status);
}

int32_t _find_filter( uint64_t freq,
                      const _filter_config_t *p_filter_config,
                      uint8_t *p_filter_indexes,
                      uint8_t num_filters,
                      skiq_filt_t *p_filter )
{
    int32_t status=-1;
    uint8_t i=0;
    bool bFound = false;

    for( i=0; (i<num_filters) && (bFound==false); i++ )
    {
        // check the frequency range
        if( (freq >= p_filter_config[p_filter_indexes[i]].start_freq) &&
            (freq < p_filter_config[p_filter_indexes[i]].stop_freq) )
        {
            status=0;
            bFound = true;
            *p_filter = p_filter_config[p_filter_indexes[i]].filter;
        }
    }

    return (status);
}

int32_t _update_tx_pa( rf_id_t *p_id,
                       bool enable )
{
    int32_t status=-1;
    uint32_t val=0;
    rfic_t rfic_instance;
    uint16_t max_atten=0;
    uint16_t min_atten=0;
    uint16_t curr_atten=0;

    // we need to adjust to max attenuation settings before changing the PA state
    rfic_instance =_skiq_get_tx_rfic_instance( p_id->card, p_id->hdl );
    if( (rfic_read_tx_attenuation_range(&rfic_instance, &max_atten, &min_atten) == 0) &&
        (rfic_read_tx_attenuation(&rfic_instance, &curr_atten) == 0) )
    {
        if( rfic_write_tx_attenuation( &rfic_instance, max_atten ) == 0 )
        {
            if( enable == true )
            {
                val = 1;
            }
            else
            {
                val = 0;
            }
            status = sidekiq_fpga_reg_RMWV( p_id->card, val, FMC_EN_TX, FPGA_REG_FMC_CTRL );
        }

        // now restore the previous attenuation setting
        rfic_write_tx_attenuation( &rfic_instance, curr_atten );
    }
    else
    {
        status = -ENODEV;
        _skiq_log(SKIQ_LOG_ERROR, "Unable to determine attenuation configuration\n");
    }

    return (status);
}

static int32_t _update_rx_lna_by_hdl( uint8_t card,
                                      skiq_rx_hdl_t hdl,
                                      uint8_t value )
{
    int32_t status = -EINVAL;

    if ( hdl == skiq_rx_hdl_A1 )
    {
        status = sidekiq_fpga_reg_RMWV( card, value, X2_FMC_EN_RXA1_X4_FMC_J6_SEL0,
                    FPGA_REG_FMC_CTRL );
    }
    else if ( hdl == skiq_rx_hdl_A2 )
    {
        status = sidekiq_fpga_reg_RMWV( card, value, X2_FMC_EN_RXA2_X4_FMC_J6_SEL1,
                    FPGA_REG_FMC_CTRL );
    }
    else if ( hdl == skiq_rx_hdl_B1 )
    {
        status = sidekiq_fpga_reg_RMWV( card, value, X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL,
                    FPGA_REG_FMC_CTRL );
    }

    return (status);
}

int32_t _update_rx_lna( rf_id_t *p_id,
                        rfe_lna_state_t new_state )
{
    int32_t status;

    switch (new_state)
    {
        case lna_enabled:
            status = _update_rx_lna_by_hdl( p_id->card, p_id->hdl, 1 );
            break;

        case lna_disabled:
            status = _update_rx_lna_by_hdl( p_id->card, p_id->hdl, 0 );
            break;

        case lna_bypassed:
            status = -ENOTSUP;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return (status);
}

int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                   uint64_t rx_lo_freq )
{
    return _update_rx_lna( p_id, lna_disabled );
}

int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                  uint64_t rx_lo_freq )
{
    skiq_filt_t filter = skiq_filt_invalid;
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    int32_t status=-1;

    // Turn on the RX LNA always
    status = _update_rx_lna( p_id, lna_enabled );

    if ( status == 0 )
    {
        status = _find_filter( rx_lo_freq,
                               _rx_filter_config,
                               _rfe[card].rx[hdl].filters.p_filter_indexes,
                               _rfe[card].rx[hdl].filters.num_filters,
                               &filter );
    }

    if( status==0 )
    {
        status=_write_rx_filter_path( p_id, filter );
    }

    if( status==0 )
    {
        _rfe[card].rx[hdl].freq = rx_lo_freq;
        switch( _rfe[card].rx[hdl].attenuation_mode )
        {
        case( skiq_rx_attenuation_mode_noise_figure ):
            status = _write_rx_attenuation_noise_figure(p_id);
            break;
        case( skiq_rx_attenuation_mode_normalized ):
            status = _write_rx_attenuation_normalized(p_id);
            break;
        case( skiq_rx_attenuation_mode_manual ):
        default:
            // no need to update rx attenuation
            (void) status;
        }
    }

    return (status);
}

int32_t _update_tx_lo( rf_id_t *p_id,
                       uint64_t tx_lo_freq )
{
    int32_t status=-1;
    bool bFound = false;
    uint8_t i=0;
    uint8_t num_filters=0;
    
    // Turn on the TX PA always
    status=_update_tx_pa( p_id, true );
    // update filter only for TX1
    if( (status==0) && (p_id->hdl == skiq_tx_hdl_A1) )
    {
        num_filters = sizeof(_tx1_filter_config) / sizeof(_filter_config_t);
        for( i=0; ((i<num_filters) && (bFound == false)); i++ )
        {
            if( (tx_lo_freq >= _tx1_filter_config[i].start_freq) &&
                (tx_lo_freq <= _tx1_filter_config[i].stop_freq) )
            {
                bFound = true;
                status = _write_tx_filter_path( p_id, _tx1_filter_config[i].filter );
                if( status == 0 )
                {
                    // update the cached freq
                    _rfe[p_id->card].tx[p_id->hdl].freq = tx_lo_freq;
                }
            }
        }
    }

    return (status);
}

int32_t _write_rx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    int32_t status=-1;
    uint8_t i=0;
    bool bFound=false;
    _filter_path_t *p_hdl_filter_path = &(_rfe[card].rx[hdl].filters);
    uint8_t filter_index=0;
    uint64_t id=0;

    // see if new value already matches the cached
    if( path == _rfe[card].rx[hdl].filter_config )
    {
        // setting already matches
        return (0);
    }
    
    // we need to look for the filter index for this card/handle
    for( i=0; (i<p_hdl_filter_path->num_filters) && (bFound==false); i++ )
    {
        filter_index = p_hdl_filter_path->p_filter_indexes[i];
        if( path == _rx_filter_config[filter_index].filter )
        {
            bFound = true;
        }
    }
    if( bFound == true )
    {
        id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID(p_id->card, p_id->hdl);
        status = _update_rffc_gpo( id,
                                   _rx_filter_config[filter_index].sw_setting,
                                   p_hdl_filter_path->select_offset,
                                   FILTER_SEL_LEN );
        if( status == 0 )
        {
            _rfe[card].rx[hdl].filter_config = path;
        }
    }
    
    return (status);
}

int32_t _write_tx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status=-1;
    bool bFound = false;
    uint64_t id=0;
    uint8_t i=0;
    uint8_t num_filters=0;

    // see if new value already matches the cached
    if( path == _rfe[p_id->card].tx[p_id->hdl].filter_config )
    {
        // setting already matches
        return (0);
    }

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        num_filters = sizeof(_tx1_filter_config) / sizeof(_filter_config_t);
        for( i=0; (i<num_filters) && (bFound==false); i++ )
        {
            if( path == _tx1_filter_config[i].filter )
            {
                bFound = true;
                id = X2_MAP_CARD_AND_TX_HDL_TO_PLL_ID(p_id->card, p_id->hdl);
                status = _update_rffc_gpo( id,
                                           _tx1_filter_config[i].sw_setting,
                                           TXA1_FILTER_SEL_OFFSET,
                                           FILTER_SEL_LEN );
                if( status == 0 )
                {
                    _rfe[p_id->card].tx[p_id->hdl].filter_config = path;
                }
            }
        }
    }
    
    return (status);
}

int32_t _read_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    *p_path = _rfe[p_id->card].rx[p_id->hdl].filter_config;

    return (0);
}

int32_t _read_tx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    *p_path = _rfe[p_id->card].rx[p_id->hdl].filter_config;

    return (0);
}

int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    int32_t status=0;
    uint8_t i=0;
    uint8_t *p_indexes;

    *p_num_filters = _rfe[card].rx[hdl].filters.num_filters;
    p_indexes = _rfe[card].rx[hdl].filters.p_filter_indexes;
    for( i=0; i<*(p_num_filters); i++ )
    {
        p_filters[i] = _rx_filter_config[p_indexes[i]].filter;
    }

    return (status);
}

int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status=0;
    uint8_t i=0;

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_num_filters = sizeof(_tx1_filter_config)/sizeof(_filter_config_t);
        for( i=0; i<*p_num_filters; i++ )
        {
            p_filters[i] = _tx1_filter_config[i].filter;
        }
    }
    else
    {
        // no filters
        *p_num_filters = 0;
    }

    return (status);
}

int32_t _write_filters( rf_id_t *p_id,
                        uint8_t num_filters,
                        const skiq_filt_t *p_filters )
{
    int32_t status=-1;
    uint8_t i=0;
    uint8_t j=0;
    bool bFound = false;
    uint8_t filter_indexes[EEPROM_FILTER_CONFIG_NUM_ELEMENTS];
    uint8_t filters_found=0;

    // Note: for this hardware version, we only support a global RX configuration
    
    if( num_filters > EEPROM_FILTER_CONFIG_NUM_ELEMENTS )
    {
        _skiq_log(SKIQ_LOG_ERROR, "too many filter elements specified to configure\n");
    }
    else
    {
        // we need to loop through all of the filters specified and find the matching index
        for( i=0; i<num_filters; i++ )
        {
            bFound = false;
            for( j=0; (j<EEPROM_FILTER_CONFIG_NUM_ELEMENTS) && (bFound==false); j++ )
            {
                // found our filter index
                if( _rx_filter_config[j].filter == p_filters[i] )
                {
                    bFound = true;
                    filter_indexes[i] = j;
                    filters_found++;
                    _skiq_log(SKIQ_LOG_INFO, "found filter enum %u at index %u\n",
                              p_filters[i], j);
                }   
            }
            if( bFound == false )
            {
                _skiq_log(SKIQ_LOG_ERROR, "unable to find index for filter %u\n",
                          p_filters[i]);
            }
        }
        if( filters_found == num_filters )
        {
            status = 0;
            
            _skiq_log(SKIQ_LOG_INFO, "about to write %u filters\n", num_filters);
            
            // write the number of filters first
            status = card_write_eeprom( p_id->card,
                                        EEPROM_FILTER_CONFIG_OFFSET,
                                        &num_filters,
                                        sizeof(num_filters) );
            if( status == 0 )
            {
                // write the active filter indexes
                status = card_write_eeprom( p_id->card,
                                            (EEPROM_FILTER_CONFIG_OFFSET+1),
                                            filter_indexes,
                                            num_filters );
            }
        }
    }

    return (status);
}

int32_t _write_rx_attenuation( rf_id_t *p_id,
                               uint16_t attenuation )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    int32_t status = -EINVAL;

    if( skiq_rx_attenuation_mode_manual == _rfe[card].rx[hdl].attenuation_mode )
    {
        status = _atten_spi_write(card, hdl, attenuation);
        if( 0 == status )
        {
            _rfe[card].rx[hdl].attenuation = attenuation;
        }
    }

    return (status);
}

int32_t _read_rx_attenuation( rf_id_t *p_id,
                              uint16_t *p_attenuation )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;

    *p_attenuation = _rfe[card].rx[hdl].attenuation;

    return 0;
}

int32_t _write_rx_attenuation_mode( rf_id_t* p_id,
                                    skiq_rx_attenuation_mode_t mode )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    int32_t status = -ENOTSUP;

    switch( mode )
    {
    case( skiq_rx_attenuation_mode_noise_figure ):
        _rfe[card].rx[hdl].attenuation_mode = mode;
        status = _write_rx_attenuation_noise_figure(p_id);
        break;

    case( skiq_rx_attenuation_mode_normalized ):
        _rfe[card].rx[hdl].attenuation_mode = mode;
        status = _write_rx_attenuation_normalized(p_id);
        break;

    case( skiq_rx_attenuation_mode_manual ):
        _rfe[card].rx[hdl].attenuation_mode = mode;
        status = 0;
        // user is responsible for eventually calling into _write_rx_attenuation
        break;
    }

    return status;
}

int32_t _read_rx_attenuation_mode( rf_id_t *p_id,
                                   skiq_rx_attenuation_mode_t *p_mode )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;

    *p_mode = _rfe[card].rx[hdl].attenuation_mode;

    return 0;
}

int32_t _read_rx_attenuation_range( rf_id_t *p_id,
                                    uint16_t *atten_quarter_db_max,
                                    uint16_t *atten_quarter_db_min )
{
    (void) p_id;
    *atten_quarter_db_max = RX_ATTENUATION_MAX;
    *atten_quarter_db_min = RX_ATTENUATION_MIN;

    return 0;
}

int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;

    (void)(p_trx_rf_port_list);

    *p_num_fixed_rf_ports = 1;
    *p_num_trx_rf_ports = 0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J2;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J1;
    }
    else if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J3;
    }    
    else
    {
        *p_num_fixed_rf_ports = 0;
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_J2;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_rf_port = skiq_rf_port_J1;
    }
    else if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        *p_rf_port = skiq_rf_port_J3;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;

    *p_num_fixed_rf_ports = 1;
    *p_num_trx_rf_ports = 0;

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J4;
    }
    else if( p_id->hdl == skiq_tx_hdl_A2 )
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J5;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_J4;
    }
    else if( p_id->hdl == skiq_tx_hdl_A2 )
    {
        *p_rf_port = skiq_rf_port_J5;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = X2_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = X2_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = X2_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = X2_MAX_RX_FREQUENCY_IN_HZ;


    return (status);
}
/***** Global Variables *****/

rfe_functions_t rfe_x2_a_funcs =
{
    .init                           = _init,
    .release                        = NULL,
    .update_tx_pa                   = _update_tx_pa,
    .override_tx_pa                 = NULL,    
    .update_rx_lna                  = _update_rx_lna,
    .update_rx_lna_by_idx           = NULL,
    .update_before_rx_lo_tune       = _update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = _update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = _update_tx_lo,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = _write_rx_filter_path,
    .write_tx_filter_path           = _write_tx_filter_path,
    .read_rx_filter_path            = _read_rx_filter_path,
    .read_tx_filter_path            = _read_tx_filter_path,
    .read_rx_filter_capabilities    = _read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = _read_tx_filter_capabilities,
    .write_filters                  = _write_filters,
    .write_rx_attenuation           = _write_rx_attenuation,
    .read_rx_attenuation            = _read_rx_attenuation,
    .read_rx_attenuation_range      = _read_rx_attenuation_range,
    .write_rx_attenuation_mode      = _write_rx_attenuation_mode,
    .read_rx_attenuation_mode       = _read_rx_attenuation_mode,
    .read_rx_rf_ports_avail         = _read_rx_rf_ports_avail,
    .read_rx_rf_port                = _read_rx_rf_port,
    .write_rx_rf_port               = NULL,
    .read_tx_rf_ports_avail         = _read_tx_rf_ports_avail,
    .read_tx_rf_port                = _read_tx_rf_port,
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = NULL,
    .write_rf_port_config           = NULL,
    .read_rf_port_operation         = NULL,
    .write_rf_port_operation        = NULL,
    .read_bias_index                = NULL,
    .write_bias_index               = NULL,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = NULL,
    .enable_tx_chan                 = NULL,
    .config_custom_rx_filter        = NULL,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = NULL,
    .swap_rx_port_config            = NULL,
    .read_rf_capabilities           = _read_rf_capabilities,
};
