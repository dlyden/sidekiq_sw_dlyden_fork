/**
 * @file   rfe_nv100.c
 * @date   Tue May 19 15:16:42 CDT 2020
 *
 * @brief  Implements the RF Front End for NV100
 * 
 * @note   This API only supports NV100 rev C
 * 
 * @link   https://confluence.epiq.rocks/display/EN/Sidekiq+Stretch2+FPGA+Development
 * @link   /mnt/storage/projects/sidekiq_stretch2/docs/nv100_icd_v0.1.0_build5.pdf
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */


/***** INCLUDES *****/

#include <inttypes.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "rfe_nv100.h"

#include "rfic_adrv9002.h"
#include "sidekiq_fpga.h"
#include "sidekiq_private.h"
#include "sidekiq_types.h"
#include "sidekiq_rf.h"
#include "sidekiq_fpga_reg_defs.h"
#include "io_expander_pcal6524.h"

/* enable TRACE macros, debug_print, and debug_print_plain when DEBUG_RFE_NV100_TRACE is
 * defined */
#if (defined DEBUG_RFE_NV100_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

/* enable debug_print and debug_print_plain when DEBUG_RFE_NV100 is defined */
#if (defined DEBUG_RFE_NV100)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/* register definitions are defined in the FPGA development link associated with NV100 */
#define RX1_BS0_OFFSET                  1
#define RX1_BS0_LEN                     1
#define RX1_BS1_OFFSET                  2
#define RX1_BS1_LEN                     1
#define RX1_BS2_OFFSET                  3
#define RX1_BS2_LEN                     1
#define RX1_HI_LO_N_OFFSET              24
#define RX1_HI_LO_N_LEN                 1
#define RX2_BS0_OFFSET                  11
#define RX2_BS0_LEN                     1
#define RX2_BS1_OFFSET                  10
#define RX2_BS1_LEN                     1
#define RX2_BS2_OFFSET                  7
#define RX2_BS2_LEN                     1
#define RX2_LO_HI_N_OFFSET              5
#define RX2_LO_HI_N_LEN                 1
#define RX2_PATH_SEL_OFFSET             6
#define RX2_PATH_SEL_LEN                1
#define TX1_EN_OFFSET                   12
#define TX1_EN_LEN                      1
#define TX2_EN_OFFSET                   13
#define TX2_EN_LEN                      1
#define RX1_EN_OFFSET                   14
#define RX1_EN_LEN                      1
#define RX2_EN_OFFSET                   15
#define RX2_EN_LEN                      1

/* IO expander information can be found in the ICD documentation link from Jenkins */
#define NV100_IOE_I2C_BUS             0
#define x22_ADDR                        0x22 /* I2C address of U501 (PCAL6524) */
#define DISABLE_FE1                     PORT(1, 0)
#define DISABLE_FE2                     PORT(1, 1)

#define NV100_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define NV100_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define NV100_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define NV100_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)


/***** MACROS ******/

#define NOT_YET_IMPLEMENTED                                             \
    do {                                                                \
        skiq_error("!!! The function '%s' for NV100 RFE is not yet implemented\n", \
                   __FUNCTION__);                                       \
        status = -ENOTSUP;                                              \
    } while (0)

#define RF_PORT_STATE_INITIALIZER \
    { .valid = false, .config = skiq_rf_port_config_fixed, \
      .j1_operation = false, .j2_operation = false, .rx2_path = false }

#define RX_FILT_STATE_INITIALIZER \
    {                             \
        .j1_filter = NULL,        \
        .j2_filter = NULL,        \
        .rx_enabled = false,      \
    }

#define LOCK_RFE(_card)                 CARD_LOCK(rfe_mutex,_card)
#define UNLOCK_RFE(_card)               CARD_UNLOCK(rfe_mutex,_card)

#define RX_FILT_SEL_(_range,_lo,_hi,_bs0,_bs1,_bs2,_hi_lo_n) \
    RX_FILT_SEL__(skiq_filt_##_range,_lo,_hi,_bs0,_bs1,_bs2,_hi_lo_n)
#define RX_FILT_SEL__(_filt,_lo,_hi,_bs0,_bs1,_bs2,_hi_lo_n) \
    { .filter = _filt, \
      .name = #_filt, \
            .lo = (_lo), \
            .hi = (_hi), \
            .bs0 = (_bs0), \
            .bs1 = (_bs1), \
            .bs2 = (_bs2), \
            .hi_lo_n = (_hi_lo_n), \
            }
#define RX_FILT_(_filt,_lo,_hi,_bs0,_bs1,_bs2,_hi_lo_n)         RX_FILT_SEL_(_filt,_lo,_hi,_bs0,_bs1,_bs2,_hi_lo_n)

/***** ENUMS *****/


/***** TYPE DEFINITIONS *****/

typedef struct
{
    bool valid;
    skiq_rf_port_config_t config;
    bool j1_operation;
    bool j2_operation;
    bool rx2_path;
} nv100_rf_port_state_t;

typedef struct
{
    skiq_filt_t filter;
    char *name;
    uint16_t lo, hi;
    uint8_t bs0;
    uint8_t bs1;
    uint8_t bs2;
    uint8_t hi_lo_n;
} filter_selection_t;

typedef struct
{
    filter_selection_t *j1_filter;
    filter_selection_t *j2_filter;
    bool rx_enabled;
} nv100_rx_filt_state_t;


/***** LOCAL DATA *****/

static ARRAY_WITH_DEFAULTS(nv100_rx_filt_state_t, current_rx_filter, SKIQ_MAX_NUM_CARDS, RX_FILT_STATE_INITIALIZER);

/**
   @brief Mutex for protecting all things RFE
 */
static ARRAY_WITH_DEFAULTS(pthread_mutex_t,
                           rfe_mutex,
                           SKIQ_MAX_NUM_CARDS,
                           PTHREAD_MUTEX_INITIALIZER);

/**
 * @note The only times when RF port configuration is invalid is before rfe_init() and after
 * rfe_release()
 */
static ARRAY_WITH_DEFAULTS(nv100_rf_port_state_t,
                           current_rf_port_state,
                           SKIQ_MAX_NUM_CARDS,
                           RF_PORT_STATE_INITIALIZER);

/**************************************************************************************************/
static skiq_filt_t available_filters[] =
{
    skiq_filt_30_to_450MHz,
    skiq_filt_450_to_600MHz,
    skiq_filt_600_to_800MHz,
    skiq_filt_800_to_1200MHz,
    skiq_filt_1200_to_1700MHz,
    skiq_filt_1700_to_2700MHz,
    skiq_filt_2700_to_3600MHz,
    skiq_filt_3600_to_6000MHz,
};


static filter_selection_t rx_filter_standby =
{
    .filter = 0, .name = "standby", .lo = 0, .hi = 0,
    .bs0 = 0, .bs1 = 0, .bs2 = 0, .hi_lo_n = 0,
};

static filter_selection_t rx1_filter_selections[] =
{
    RX_FILT_(30_to_450MHz,    0, 450, 0, 0, 0, 0),
    RX_FILT_(450_to_600MHz, 450, 600, 0, 0, 1, 0),
    RX_FILT_(600_to_800MHz, 600, 800, 1, 1, 1, 0),
    RX_FILT_(800_to_1200MHz, 800, 1200, 1, 1, 0, 0),
    RX_FILT_(1200_to_1700MHz, 1200, 1700, 0, 1, 0, 0),
    RX_FILT_(1700_to_2700MHz, 1700, 2700, 1, 0, 1, 1),
    RX_FILT_(2700_to_3600MHz, 2700, 3600, 1, 0, 0, 1),
    RX_FILT_(3600_to_6000MHz, 3600, 6000, 0, 1, 1, 1),
};

static filter_selection_t rx2_filter_selections[] =
{
    RX_FILT_(30_to_450MHz,    0, 450, 0, 0, 0, 1),
    RX_FILT_(450_to_600MHz, 450, 600, 0, 0, 1, 1),
    RX_FILT_(600_to_800MHz, 600, 800, 1, 1, 1, 1),
    RX_FILT_(800_to_1200MHz, 800, 1200, 1, 1, 0, 1),
    RX_FILT_(1200_to_1700MHz, 1200, 1700, 0, 1, 0, 1),
    RX_FILT_(1700_to_2700MHz, 1700, 2700, 1, 0, 1, 0),
    RX_FILT_(2700_to_3600MHz, 2700, 3600, 1, 0, 0, 0),
    RX_FILT_(3600_to_6000MHz, 3600, 6000, 0, 1, 1, 0),
};

/***** DEBUG FUNCTIONS *****/

#include "debug_rfe_nv100.h"


/***** LOCAL FUNCTIONS *****/

/*************************************************************************************************/
static int32_t
write_rf_port_operation(uint8_t card, uint8_t hdl, bool transmit)
{
    int32_t status = 0;

    /**
       Set GPO TR_N(1|2) to 0 (@a transmit is false) or 1 (@a transmit is true)
    */
    if(hdl == skiq_rx_hdl_A1)
    {
        if(status == 0)
        {
            debug_print("Writing %d to buffer TX1_EN in FPGA_REG_GPIO_WRITE\n", transmit);
            status = sidekiq_fpga_reg_RMWV(card, transmit, TX1_EN, FPGA_REG_GPIO_WRITE);
        }
        if(status == 0)
        {
            current_rf_port_state[card].j1_operation = transmit;
        }
    }
    else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
    {
        if(status == 0)
        {
            debug_print("Writing %d to buffer TX2_EN in FPGA_REG_GPIO_WRITE\n", transmit);
            status = sidekiq_fpga_reg_RMWV(card, transmit, TX2_EN, FPGA_REG_GPIO_WRITE);
        }
        if(status == 0)
        {
            current_rf_port_state[card].j2_operation = transmit;
        }
    }
    else
    {
        skiq_error("Invalid handle selected for card %" PRIu8 "\n", card);
        status = -EINVAL;
    }
    

    return status;
}

static int32_t
write_rf_rx_port(uint8_t card, uint32_t hdl, skiq_rf_port_t rf_port)
{
    int32_t status = 0;

    if(rf_port == skiq_rf_port_J1)
    {
        if(current_rf_port_state[card].config == skiq_rf_port_config_trx)
        {
            status = -ENOTSUP;
            skiq_error("Rx2 receive on J1 while in skiq_rf_port_config_trx mode not supported on card %u", card);
        }
        else
        {
            debug_print("Writing 1 to buffer RX2_PATH_SEL in FPGA_REG_GPIO_WRITE\n");
            status = sidekiq_fpga_reg_RMWV(card, 1, RX2_PATH_SEL, FPGA_REG_GPIO_WRITE);
            if(status == 0)
            {
                current_rf_port_state[card].rx2_path = 1;
            }
        }
    }
    else if(rf_port == skiq_rf_port_J2)
    {
        debug_print("Writing 0 to buffer RX2_PATH_SEL in FPGA_REG_GPIO_WRITE\n");
        status = sidekiq_fpga_reg_RMWV(card, 0, RX2_PATH_SEL, FPGA_REG_GPIO_WRITE);
        if(status == 0)
        {
            current_rf_port_state[card].rx2_path = 0;
        }
    }
    else
    {
        skiq_error("Invalid RF port selected for card %" PRIu8 "\n", card);
        status = -EINVAL;
    }


    return status;
}

static filter_selection_t *
find_filter_by_enum(filter_selection_t *fs_table,
                    uint8_t nr_entries,
                    skiq_filt_t filter)
{
    uint8_t i;
    filter_selection_t *fs_entry = NULL;

    for (i = 0; ( i < nr_entries ) && ( fs_entry == NULL ); i++)
    {
        if ( fs_table[i].filter == filter )
        {
            fs_entry = &(fs_table[i]);
        }
    }

    return fs_entry;
}

static filter_selection_t *
find_rx_filter_by_enum(uint8_t hdl, skiq_filt_t filter)
{
    if(hdl == skiq_rx_hdl_A1)
    {
        return find_filter_by_enum(rx1_filter_selections, ARRAY_SIZE(rx1_filter_selections), filter);
    }
    else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
    {
        return find_filter_by_enum(rx2_filter_selections, ARRAY_SIZE(rx2_filter_selections), filter);
    }
    else
    {
        skiq_error("Invalid handle used to select filter\n");
        return NULL;
    }
}

static filter_selection_t *
find_filter_by_freq(filter_selection_t *fs_table,
                    uint8_t nr_entries,
                    uint64_t freq)
{
    uint8_t i;
    filter_selection_t *fs_entry = NULL;

    TRACE_ENTER;
    TRACE_ARGS("fs_table: %p, nr_entries: %u, freq: %" PRIu64 "\n", fs_table, nr_entries, freq);

    for (i = 0; ( i < nr_entries ) && ( fs_entry == NULL ); i++)
    {
        if ( ( MHZ_TO_HZ(fs_table[i].lo) < freq ) &&
             ( freq <= MHZ_TO_HZ(fs_table[i].hi) ) )
        {
            fs_entry = &(fs_table[i]);
        }
    }

    return fs_entry;
}

static filter_selection_t *
find_rx_filter_by_freq(uint8_t hdl, uint64_t freq)
{
    if(hdl == skiq_rx_hdl_A1)
    {
        return find_filter_by_freq(rx1_filter_selections, ARRAY_SIZE(rx1_filter_selections), freq);
    }
    else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
    {
        return find_filter_by_freq(rx2_filter_selections, ARRAY_SIZE(rx2_filter_selections), freq);
    }
    else
    {
        skiq_error("Invalid handle used to select filter\n");
        return NULL;
    }
}

static int32_t
set_rx_filter(uint8_t card, uint8_t hdl, filter_selection_t *rxfs)
{
    int32_t status = 0;
    uint32_t val = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIO_WRITE, &val);
    if(status == 0)
    {
        if(hdl == skiq_rx_hdl_A1)
        {
            BF_SET(val, rxfs->bs0, RX1_BS0_OFFSET, RX1_BS0_LEN);
            BF_SET(val, rxfs->bs1, RX1_BS1_OFFSET, RX1_BS1_LEN);
            BF_SET(val, rxfs->bs2, RX1_BS2_OFFSET, RX1_BS2_LEN);
            BF_SET(val, rxfs->hi_lo_n, RX1_HI_LO_N_OFFSET, RX1_HI_LO_N_LEN);
        }
        else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
        {
            BF_SET(val, rxfs->bs0, RX2_BS0_OFFSET, RX2_BS0_LEN);
            BF_SET(val, rxfs->bs1, RX2_BS1_OFFSET, RX2_BS1_LEN);
            BF_SET(val, rxfs->bs2, RX2_BS2_OFFSET, RX2_BS2_LEN);
            BF_SET(val, rxfs->hi_lo_n, RX2_LO_HI_N_OFFSET, RX2_LO_HI_N_LEN);
        }
        else
        {
            skiq_error("Invalid handle selected for card %" PRIu8 "\n", card);
            status = -EINVAL;
        }
        if(status == 0)
        {
            debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", val);
            status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_GPIO_WRITE, val);
        }
    }

    return status;
}

static int32_t
select_rx_filter(uint8_t card,
                 uint8_t chip_id,
                 uint8_t hdl,
                 filter_selection_t *rxfs)
{
    int32_t status = 0;

    if(hdl == skiq_rx_hdl_A1)
    {
        current_rx_filter[card].j1_filter = rxfs;
    }
    else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
    {
        current_rx_filter[card].j2_filter = rxfs;
    }
    else
    {
        skiq_error("Invalid handle selected for card %" PRIu8 "\n", card);
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        debug_print("Stored filter '%s' for %s on card %" PRIu8 "\n", rxfs->name,
                    rx_hdl_cstr(hdl), card);
    }

    if((status == 0) && (current_rx_filter[card].rx_enabled))
    {
        debug_print("Applying filter '%s' for %s on card %" PRIu8 "\n", rxfs->name,
                    rx_hdl_cstr(hdl), card);
        status = set_rx_filter(card, hdl, rxfs);
    }

    return status;
}

static int32_t
select_standby_filter_for_all_ports(uint8_t card, uint8_t chip_id)
{
    int32_t status = 0;
    filter_selection_t *rxfs = &rx_filter_standby;

    status = set_rx_filter(card, skiq_rx_hdl_A1, rxfs);
    if(status == 0)
    {
        current_rx_filter[card].j1_filter = rxfs;
    }

    status = set_rx_filter(card, skiq_rx_hdl_B1, rxfs);
    if(status == 0)
    {
        current_rx_filter[card].j2_filter = rxfs;
    }

    return status;
}

static int32_t
enable_rx_chan(uint8_t card, skiq_rx_hdl_t hdl, bool enable)
{
    int32_t status = 0;
    filter_selection_t *rxfs = NULL;

    if (enable)
    {
        if (hdl == skiq_rx_hdl_A1)
        {
            rxfs = current_rx_filter[card].j1_filter;
        }
        else if ((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
        {
            rxfs = current_rx_filter[card].j2_filter;
        }
        else
        {
            status = -EINVAL;
        }
    }
    else
    {
        rxfs = &rx_filter_standby;
    }

    if ( status == 0 )
    {
        debug_print("Applying filter '%s' for %s on card %" PRIu8 "\n", rxfs->name,
                    rx_hdl_cstr(hdl), card);
        current_rx_filter[card].rx_enabled = enable;
        status = set_rx_filter(card, hdl, rxfs);
    }

    return status;
}

static int32_t
isolate_rx_lineup(uint8_t card,
                  uint8_t hdl)
{
    int32_t status = 0;

    /* disable the LNA for tuning */
    if(hdl == skiq_rx_hdl_A1)
    {
        status = pcal6524_io_exp_set_as_output(card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE1, DISABLE_FE1);
    }
    else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
    {
        status = pcal6524_io_exp_set_as_output(card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE2, DISABLE_FE2);
        /* if the LNA was successfully disabled set Rx2 to receive on J2 */
        if(status==0)
        {
            debug_print("Writing 0 to buffer RX2_PATH_SEL in FPGA_REG_GPIO_WRITE\n");
            status = sidekiq_fpga_reg_RMWV(card, 0, RX2_PATH_SEL, FPGA_REG_GPIO_WRITE);
        }
    }

    return status;
}

static int32_t
connect_rx_lineup(uint8_t card,
                  uint8_t hdl)
{
    int32_t status = 0;

    /* enable the LNA that was disabled */
    if(hdl == skiq_rx_hdl_A1)
    {
        status = pcal6524_io_exp_set_as_output(card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE1, 0);
    }
    else if((hdl == skiq_rx_hdl_A2) || (hdl == skiq_rx_hdl_B1))
    {
        status = pcal6524_io_exp_set_as_output(card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE2, 0);

        /* On the NV100 design, in order to receive on Rx2 through J1, both FE1 -AND- FE2 need to be
           enabled */
        if((status == 0) && (current_rf_port_state[card].rx2_path == 1))
        {
            status = pcal6524_io_exp_set_as_output(card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE1, 0);
        }

        /* if the frontend configuration was successfully enabled, set the Rx2 path selection to the
           current_rf_port_state */
        if(status==0)
        {
            debug_print("Writing %d to buffer RX2_PATH_SEL in FPGA_REG_GPIO_WRITE\n",
                        current_rf_port_state[card].rx2_path);
            status = sidekiq_fpga_reg_RMWV(card, current_rf_port_state[card].rx2_path, RX2_PATH_SEL,
                                           FPGA_REG_GPIO_WRITE);
        }
    }

    return status;
}

static int32_t
update_before_rx_lo_tune(rf_id_t *p_id,
                         uint64_t rx_lo_freq)
{
    int32_t status = 0;

    if (status == 0)
    {
        status = isolate_rx_lineup(p_id->card, p_id->hdl);
    }

    if (status == 0)
    {
        filter_selection_t *rxfs;

        rxfs = find_rx_filter_by_freq(p_id->hdl, rx_lo_freq);
        if (rxfs == NULL)
        {
            skiq_error("Unable to find RX filter on card %u for LO frequency %" PRIu64 "\n",
                        p_id->card, rx_lo_freq);
            status = -EINVAL;
        }
        else
        {
            debug_print("Selecting RX filter '%s' on card %u for LO frequency %" PRIu64 "\n",
                        SKIQ_FILT_STRINGS[ rxfs->filter ], p_id->card, rx_lo_freq);
            status = select_rx_filter(p_id->card, p_id->chip_id, p_id->hdl, rxfs);
        }
    }

    return status;
}

static int32_t
update_after_rx_lo_tune(rf_id_t *p_id,
                        uint64_t rx_lo_freq)
{
    int32_t status = 0;

    status = connect_rx_lineup(p_id->card, p_id->hdl);

    return status;
}

static int32_t
write_rf_port_config(rf_id_t *p_id,
                     skiq_rf_port_config_t config,
                     bool update_rfe_states )
{
    int32_t status = 0;

    switch (config)
    {
        case skiq_rf_port_config_fixed:
            // No need to make any changes from trx to fixed
            // trx ports are configured in a valid fixed setting
            break;

        case skiq_rf_port_config_trx:
            // Setup RX on J2, the fixed configuration of 2xRxJ1
            // is not supported in trx configuration 
            status = write_rf_rx_port(p_id->card, p_id->hdl, skiq_rf_port_J2);
            break;

        default:
            status = -EINVAL;
            break;
    }

    if (status == 0)
    {
        /* store RF port configuration and mark it valid for the time being */
        current_rf_port_state[p_id->card].config = config;
        current_rf_port_state[p_id->card].valid = true;

        /* Default RF port operation to 'receive', even in fixed mode, the FPGA (PL) output needs to
         * be in a well-defined state */
        status = write_rf_port_operation(p_id->card, p_id->hdl, false /* receive */);
    }

    /* if it turns out that writing the various elements of the RF port configuration did not
     * succeed, mark the RF port state as not valid */
    if (status != 0)
    {
        current_rf_port_state[p_id->card].valid = false;
    }

    return status;
}

static int32_t
update_disable_fe(rf_id_t *p_id, bool disable)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("disable: %d\n", disable);

    /* update the rx lna based on the new state passed in */
    if(p_id->hdl == skiq_rx_hdl_A1)
    {
        if(disable)
        {
            status = pcal6524_io_exp_set_as_output(p_id->card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE1, DISABLE_FE1);
        }
        else
        {
            status = pcal6524_io_exp_set_as_output(p_id->card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE1, 0);
        }
    }
    else if((p_id->hdl == skiq_rx_hdl_A2) || (p_id->hdl == skiq_rx_hdl_B1))
    {
        if(disable)
        {
            status = pcal6524_io_exp_set_as_output(p_id->card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE2, DISABLE_FE2);
        }
        else
        {
            status = pcal6524_io_exp_set_as_output(p_id->card, NV100_IOE_I2C_BUS, x22_ADDR, DISABLE_FE2, 0);
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
/***** RFE FUNCTIONS *****/
/*************************************************************************************************/

/*************************************************************************************************/
/** @brief Initializes the RFE for the NV100.

    @param[in] p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_init(rf_id_t *p_id)
{
    int32_t status = 0;
    uint32_t mask = 0, val = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 0. print all available filters if debug */
    nv100_print_rx_filters();

    /* 1. Unconditionally unset RF port configuration valid flag,
     * nothing can be trusted during initialization */
    current_rf_port_state[p_id->card].valid = false;

    /* 2. Disable both Front-End 1 and Front-End 2 by default */
    if(status == 0)
    {
        mask = DISABLE_FE1 | DISABLE_FE2;
        val = DISABLE_FE1 | DISABLE_FE2;

        status = pcal6524_io_exp_set_as_output(p_id->card, NV100_IOE_I2C_BUS, x22_ADDR, mask, val);
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief After initialization of the RFE for the NV100 is complete setup the Front-End.

    @param[in] p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_post_init(rf_id_t *p_id)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 0. Set default RF port configurations to 'fixed' */
    status = write_rf_port_config(p_id, skiq_rf_port_config_fixed, false /* update_rfe_states */);

    /* 1. Set receive filters to standby */
    if(status == 0)
    {
        debug_print("Selecting RX filter 'standby' for ports J1 and J2 on card %u\n", p_id->card);
        status = select_standby_filter_for_all_ports(p_id->card, p_id->chip_id);
    }

    /* 2. Set Rx2 to receive on port J2 upon init */
    if(status == 0)
    {
        status = write_rf_rx_port(p_id->card, p_id->hdl, skiq_rf_port_J2);
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
/** @brief Releases the RFE for the NV100.

    @param[in] p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_release( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 0. Unconditionally unset RF port configuration valid flag,
      * nothing can be trusted during initialization */
    current_rf_port_state[p_id->card].valid = false;
 
    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
/** @brief Before the RFE is released for the NV100 configure a good state to the RFE.

    @param[in] p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_pre_release( rf_id_t *p_id )
{
    int32_t status = 0, op_status = 0;
    uint32_t mask = 0, val = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 0. Set default RF port configurations to 'fixed' */
    op_status = write_rf_port_config(p_id, skiq_rf_port_config_fixed, false /* update_rfe_states */);
    status = NONZERO(op_status, status);
    if(op_status != 0)
    {
        debug_print("Unable to set RF port configuration to 'fixed' with status: %" PRIi32 "\n",
                    op_status);
    }

    /* 1. Disable LNA and Tx PA for Front-End 1 */
    mask = DISABLE_FE1 | DISABLE_FE2;
    val = DISABLE_FE1 | DISABLE_FE2;

    op_status = pcal6524_io_exp_set_as_output(p_id->card, NV100_IOE_I2C_BUS, x22_ADDR, mask, val);
    status = NONZERO(op_status, status);
    if(op_status != 0)
    {
        debug_print("Unable to disable RF Front-End 1 and RF Front-End 2 with status: %" PRIi32
                    "\n", op_status);
    }

    /* 2. Set receive filters to standby */
    debug_print("Selecting RX filter 'standby' for ports J1 and J2 on card %u\n", p_id->card);
    op_status = select_standby_filter_for_all_ports(p_id->card, p_id->chip_id);
    status = NONZERO(op_status, status);
    if(op_status != 0)
    {
        debug_print("Unable to set receive filters to standby with status: %" PRIi32 "\n",
                    op_status);
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
/** @brief Updates the RX LNA for the NV100

    @note The RX LNAs and the TX PAs do not have individual control on NV100, updating one will
          update the other.  Refer to @ref nv100_update_tx_pa

    @param[in] p_id         Pointer to RF identifier struct.
    @param[in] new_state    Enum that enables or disables the RX LNA, bypassing is not supported.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_update_rx_lna(rf_id_t *p_id, rfe_lna_state_t new_state)
{
    int32_t status = 0;
    bool value = false;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("new_state: %d\n", new_state);

    LOCK_RFE(p_id->card);

    if(new_state >= lna_bypassed)
    {
        status = -ENOTSUP;
        skiq_error("Unsupported state for lna provided on card: %" PRIu8 "\n", p_id->card);
    }
    else
    {
        if(new_state == lna_disabled)
        {
            value = true;
        }
        else if(new_state == lna_enabled)
        {
            value = false;
        }

        status = update_disable_fe(p_id, value);
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
/** @brief Updates the TX PA for the NV100

    @note The TX PAs and the RX LNAs do not have individual control on NV100, updating one will
          update the other.  Refer to @ref nv100_update_rx_lna

    @param[in] p_id     Pointer to RF identifier struct.
    @param[in] enable   Bool that enables or disables the TX PA.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_update_tx_pa(rf_id_t *p_id, bool enable)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("enable: %d\n", enable);

    LOCK_RFE(p_id->card);

    /* have to invert enable to update the disable line correctly */
    status = update_disable_fe(p_id, !enable);

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end PRIOR to re-tuning to a given Rx LO frequency.

    @param[in] p_id Pointer to RF identifier struct.
    @param[in] rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_update_before_rx_lo_tune(rf_id_t *p_id,
                                  uint64_t rx_lo_freq)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rx_lo_freq: %" PRIu64 "\n", rx_lo_freq);

    LOCK_RFE(p_id->card);

    status = update_before_rx_lo_tune(p_id, rx_lo_freq);

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end AFTER re-tuning to a given Rx LO frequency.

    @param[in] p_id Pointer to RF identifier struct.
    @param[in] rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_update_after_rx_lo_tune(rf_id_t *p_id,
                                 uint64_t rx_lo_freq)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rx_lo_freq: %" PRIu64 "\n", rx_lo_freq);

    LOCK_RFE(p_id->card);

    status = update_after_rx_lo_tune(p_id, rx_lo_freq);

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Configures the Rx filter configuration.

    @param[in] p_id Pointer to RF identifier struct.
    @param[in] path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_write_rx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status = 0;
    filter_selection_t *rxfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("path: %s\n", filt_cstr(path));

    LOCK_RFE(p_id->card);

    rxfs = find_rx_filter_by_enum(p_id->hdl, path);
    if ( rxfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting RX filter %s on card %u as requested\n",
                    SKIQ_FILT_STRINGS[ rxfs->filter ], p_id->card);
        status = select_rx_filter(p_id->card, p_id->chip_id, p_id->hdl, rxfs);
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the current Rx filter configuration.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_read_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    int32_t status = 0;
    filter_selection_t *rxfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    rxfs = (p_id->hdl == skiq_rx_hdl_A1) ? current_rx_filter[p_id->card].j1_filter :
            ((p_id->hdl == skiq_rx_hdl_A2) || (p_id->hdl == skiq_rx_hdl_B1)) ? current_rx_filter[p_id->card].j2_filter : NULL;
    if ( rxfs == NULL )
    {
        status = -ERANGE;
        p_path = NULL;
    }
    else
    {
        *p_path = rxfs->filter;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the available Rx filters.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_filters Pointer to be updated with available filters.
    @param[out] p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
nv100_read_rx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    *p_num_filters = ARRAY_SIZE(available_filters);
    memcpy( p_filters, available_filters, sizeof( available_filters ) );

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_read_rx_rf_ports_avail(rf_id_t *p_id,
                                uint8_t *p_num_fixed_rf_ports,
                                skiq_rf_port_t *p_fixed_rf_port_list,
                                uint8_t *p_num_trx_rf_ports,
                                skiq_rf_port_t *p_trx_rf_port_list)
{
    int32_t status = 0;
    skiq_rx_hdl_t hdl;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    hdl = p_id->hdl;

    *p_num_fixed_rf_ports = 0;
    if ( hdl == skiq_rx_hdl_A1 )
    {
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J1;
    }
    else if ( ( hdl == skiq_rx_hdl_A2 ) || ( hdl == skiq_rx_hdl_B1 ) )
    {
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J1;
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J2;
    }

    *p_num_trx_rf_ports = 0;
    if ( hdl == skiq_rx_hdl_A1 )
    {
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J1;
    }
    else if ( ( hdl == skiq_rx_hdl_A2 ) || ( hdl == skiq_rx_hdl_B1 ) )
    {
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J2;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_read_rx_rf_port(rf_id_t *p_id,
                         skiq_rf_port_t *p_rf_port)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if(current_rf_port_state[p_id->card].valid)
    {
        if(p_id->hdl == skiq_rx_hdl_A1)
        {
            *p_rf_port = skiq_rf_port_J1;
        }
        else if((p_id->hdl == skiq_rx_hdl_A2) || (p_id->hdl == skiq_rx_hdl_B1))
        {
            if(current_rf_port_state[p_id->card].config == skiq_rf_port_config_fixed)
            {
                *p_rf_port = (current_rf_port_state[p_id->card].rx2_path == 0) ?
                    skiq_rf_port_J2 : skiq_rf_port_J1;
            }
            else
            {
                *p_rf_port = skiq_rf_port_J2;
            }
        }
        else
        {
            skiq_error("Invalid handle selected for card %" PRIu8 "\n", p_id->card);
            status = -EINVAL;
        }
        
    }
    else
    {
        status = -EINVAL;
    }
    

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_write_rx_rf_port(rf_id_t *p_id, skiq_rf_port_t rf_port)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rf_port: %s\n", rf_port_cstr(rf_port));

    LOCK_RFE(p_id->card);

    status = write_rf_rx_port(p_id->card, p_id->hdl, rf_port);

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_read_tx_rf_ports_avail(rf_id_t *p_id,
                                uint8_t *p_num_fixed_rf_ports,
                                skiq_rf_port_t *p_fixed_rf_port_list,
                                uint8_t *p_num_trx_rf_ports,
                                skiq_rf_port_t *p_trx_rf_port_list)
{
    int32_t status = 0;
    skiq_tx_hdl_t hdl;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    hdl = p_id->hdl;

    *p_num_fixed_rf_ports = 0;
    if ( hdl == skiq_tx_hdl_A1 )
    {
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J1;
    }
    else if ( ( hdl == skiq_tx_hdl_A2 ) || ( hdl == skiq_tx_hdl_B1 ) )
    {
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J2;
    }

    *p_num_trx_rf_ports = 0;
    if ( hdl == skiq_tx_hdl_A1 )
    {
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J1;
    }
    else if ( ( hdl == skiq_tx_hdl_A2 ) || ( hdl == skiq_tx_hdl_B1 ) )
    {
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J2;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_read_tx_rf_port(rf_id_t *p_id,
                         skiq_rf_port_t *p_rf_port)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if(current_rf_port_state[p_id->card].valid)
    {
        if(p_id->hdl == skiq_rx_hdl_A1)
        {
            *p_rf_port = skiq_rf_port_J1;
        }
        else if((p_id->hdl == skiq_rx_hdl_A2) || (p_id->hdl == skiq_rx_hdl_B1))
        {
            *p_rf_port = skiq_rf_port_J2;
        }
        else
        {
            skiq_error("Invalid handle selected for card %" PRIu8 "\n", p_id->card);
            status = -EINVAL;
        }
    }
    else
    {
        status = -EINVAL;
    }
    

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_rf_port_config_avail(rf_id_t *p_id,
                              bool *p_fixed,
                              bool *p_trx)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if(p_id->hdl == skiq_rx_hdl_A1 ||
       p_id->hdl == skiq_rx_hdl_A2 ||
       p_id->hdl == skiq_rx_hdl_B1)
    {
        *p_fixed = true;
        *p_trx = true;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_write_rf_port_config(rf_id_t *p_id,
                              skiq_rf_port_config_t config)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("config: %s\n", rf_port_config_cstr(config));

    LOCK_RFE(p_id->card);

    switch (config)
    {
        case skiq_rf_port_config_fixed:
        case skiq_rf_port_config_trx:
            debug_print("Updating RF port config to %s on card %u\n", rf_port_config_cstr(config),
                        p_id->card);
            status = write_rf_port_config(p_id, config, true /* update_rfe_states */);
            break;

        default:
            skiq_error("Unhandled RF port configuration (%u) requested on card %u\n", config,
                       p_id->card);
            status = -EINVAL;
            break;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_read_rf_port_operation(rf_id_t *p_id, bool *p_transmit)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if(p_id->hdl == skiq_rx_hdl_A1)
    {
        *p_transmit = current_rf_port_state[p_id->card].j1_operation;
    }
    else if((p_id->hdl == skiq_rx_hdl_A2) || (p_id->hdl == skiq_rx_hdl_B1))
    {
        *p_transmit = current_rf_port_state[p_id->card].j2_operation;
    }
    else
    {
        skiq_error("Invalid handle %" PRIu32 " selected for card %" PRIu8 "\n", p_id->hdl, p_id->card);
        status = -EINVAL;
    }
    

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_write_rf_port_operation(rf_id_t *p_id, bool transmit)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("transmit: %s\n", bool_cstr(transmit));

    LOCK_RFE(p_id->card);
    
    status = write_rf_port_operation(p_id->card, p_id->hdl, transmit);

    UNLOCK_RFE(p_id->card);
    
    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_enable_rx_chan(rf_id_t *p_id, bool enable)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("enable: %s\n", bool_cstr(enable));

    LOCK_RFE(p_id->card);
    
    status = enable_rx_chan(p_id->card, p_id->hdl, enable);

    UNLOCK_RFE(p_id->card);
    
    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_write_rx_freq_hop_list(rf_id_t *p_id,
                                uint16_t num_freq,
                                uint64_t freq_list[])
{
    int32_t status = 0;
    uint16_t i = 0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;
    uint64_t freq = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("num_freq: %u, freq_list: %p\n", num_freq, freq_list);

    if (num_freq == 0)
    {
        return -EINVAL;
    }

    LOCK_RFE(p_id->card);

    for (i = 0; (i < num_freq) && (status == 0); i++)
    {
        filter_selection_t *pfs;

        pfs = find_rx_filter_by_freq(p_id->hdl, freq_list[i]);
        if (pfs == NULL)
        {
            status = -EINVAL;
        }
        else if (pfs->filter == skiq_filt_invalid)
        {
            status = -EPROTO;
        }
        else if ((skiq_filt != skiq_filt_invalid) &&
                 (skiq_filt != pfs->filter))
        {
            status = -EINVAL;
            skiq_error("All frequencies in the Rx hop list must be within a single frequency "
                       "filter band for Sidekiq %s on card %u\n",
                       part_cstr(_skiq_get_part(p_id->card)), p_id->card);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq,
                       SKIQ_FILT_STRINGS[skiq_filt]);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq_list[i],
                       SKIQ_FILT_STRINGS[pfs->filter]);
        }
        else
        {
            skiq_filt = pfs->filter;
            freq = freq_list[i];
        }
    }

    /* if the frequencies are within a band, then actually apply it.  Since we know all of the
     * frequencies use the same filter setting, we can just set RFE for the first frequency in the
     * hop list */
    if (status == 0)
    {
        status = update_before_rx_lo_tune(p_id, freq_list[0]);
    }

    if (status == 0)
    {
        status = update_after_rx_lo_tune(p_id, freq_list[0]);
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
nv100_write_tx_freq_hop_list(rf_id_t *p_id,
                                uint16_t num_freq,
                                uint64_t freq_list[])
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("num_freq: %u, freq_list: %p\n", num_freq, freq_list);

    NOT_YET_IMPLEMENTED;

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
int32_t nv100_read_rf_capabilities( rf_id_t *p_id,
                                       skiq_rf_capabilities_t *p_rf_capabilities )
{
    int32_t status = 0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = NV100_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = NV100_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = NV100_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = NV100_MAX_RX_FREQUENCY_IN_HZ;

    return (status);
}


/***** GLOBAL DATA *****/

rfe_functions_t rfe_nv100_funcs =
{
    .init                           = nv100_init,
    .post_init                      = nv100_post_init,
    .release                        = nv100_release,
    .pre_release                    = nv100_pre_release,
    .update_tx_pa                   = nv100_update_tx_pa,
    .override_tx_pa                 = NULL,
    .update_rx_lna                  = nv100_update_rx_lna,
    .update_rx_lna_by_idx           = NULL,
    .update_before_rx_lo_tune       = nv100_update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = nv100_update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = NULL,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = nv100_write_rx_filter_path,
    .write_tx_filter_path           = NULL,
    .read_rx_filter_path            = nv100_read_rx_filter_path,
    .read_tx_filter_path            = NULL,
    .read_rx_filter_capabilities    = nv100_read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = NULL,
    .write_filters                  = NULL,
    .write_rx_attenuation           = NULL,
    .read_rx_attenuation            = NULL,
    .read_rx_attenuation_range      = NULL,
    .write_rx_attenuation_mode      = NULL,
    .read_rx_attenuation_mode       = NULL,
    .read_rx_rf_ports_avail         = nv100_read_rx_rf_ports_avail,
    .read_rx_rf_port                = nv100_read_rx_rf_port,
    .write_rx_rf_port               = nv100_write_rx_rf_port,
    .read_tx_rf_ports_avail         = nv100_read_tx_rf_ports_avail,
    .read_tx_rf_port                = nv100_read_tx_rf_port,
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = nv100_rf_port_config_avail,
    .write_rf_port_config           = nv100_write_rf_port_config,
    .read_rf_port_operation         = nv100_read_rf_port_operation,
    .write_rf_port_operation        = nv100_write_rf_port_operation,
    .read_bias_index                = NULL,
    .write_bias_index               = NULL,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = nv100_enable_rx_chan,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = nv100_write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = nv100_write_tx_freq_hop_list,
    .config_custom_rx_filter        = NULL,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = NULL,
    .swap_rx_port_config            = NULL,
    .swap_tx_port_config            = NULL,
    .read_rf_capabilities           = nv100_read_rf_capabilities,
};
