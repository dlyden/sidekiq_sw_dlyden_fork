/*! \file help_prog_ad9361_from_file.c
 * \brief This file contains a the functions needed to
 * support ad9361 programming from a file within a
 * libmaveriq app.  This functionality used to live in
 * the prog_ad9361_from_file test app, but was moved
 * here to support in-app reprogramming of ad9361 chips.
 *
 *
 * Just like in the original prog_ad9361_from_file app:
 *
 * It is assumed that the user has copied the first three
 * columns from the normal spreadsheet output file from the
 * ad9361 software into a text file.  Whitespace matters
 * here, as that seems to be the only delimiter.
 *
 * Note: only the SPIWrite, SPIRead, WAIT, and WAIT_CALDONE
 * commands are supported currently.
 *
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 *</pre>*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <bit_ops.h>

#include <sidekiq_hal.h>
#include <ad9361_driver.h>

extern int32_t hal_ad9361_read_reg(uint32_t chip, uint16_t addr, uint8_t* p_data);
extern int32_t hal_ad9361_write_reg(uint32_t chip, uint16_t addr, uint8_t data);


/* PRINT_DEBUG is used for simple printing of all operations without
   actually performing the operation to any device */
#define PRINT_DEBUG 0

#define DEBUG(fmt, ...)							\
      do { if (PRINT_DEBUG) fprintf(stderr, fmt, ##__VA_ARGS__); } while (0)

/* the following #defines indicate different fixed character count offsets
   into specific command "lines" of the ad9361 eval software's register
   file output...these offsets are used to extract fields in a given line */
#define AD9361_SPIWRITE_REG_ADDR_OFFSET 9
#define AD9361_REG_ADDR_LEN 3
#define AD9361_SPIWRITE_REG_DATA_OFFSET 13
#define AD9361_REG_DATA_LEN 2
#define AD9361_SPIREAD_REG_ADDR_OFFSET 8
#define AD9361_WAIT_CALDONE_BIT_OFFSET 13
#define AD9361_WAIT_TIME_OFFSET 5

#define AD9361_GPIO_REG1 (0x026)
#define AD9361_GPIO_REG2 (0x027)

/* the wait_caldone_entry structure is used to define a
   specific calibration operation that can be requested
   by the ad9361 eval software.  It consists of the
   bitname (as defined by the eval sw), the max time
   to poll the bit before giving up, the reg address to
   poll, the bit position to poll, and the expected state
   of the bit to indicate that the calibration has
   successfully completed */
struct wait_caldone_entry
{
    char* bitname;
    uint32_t timeout_in_mS;
    uint16_t addr;
    uint8_t bit;
    uint8_t cal_complete_state;
};

/* The wait_caldone_list contains a series of entries defining
   different calibrations that can potentially be requested
   by the ad9361 eval software. */
struct wait_caldone_entry wait_caldone_list[] =
{
    {"BBPLL",2000,0x05e,7,1},
    {"RXCP",100,0x244,7,1},
    {"TXCP",100,0x284,7,1},
    {"RXFILTER",2000,0x016,7,0},
    {"TXFILTER",2000,0x016,6,0},
    {"BBDC",2000,0x016,0,0},
    {"RFDC",2000,0x016,1,0},
    {"TXQUAD",2000,0x016,4,0},
    {"END",0,0,0,0}
};

static bool _configure_gpio[SKIQ_MAX_NUM_CARDS] = { [0 ... SKIQ_MAX_NUM_CARDS-1] = false };
static uint8_t *_p_gpio_pos[SKIQ_MAX_NUM_CARDS] = { [0 ... SKIQ_MAX_NUM_CARDS-1] = NULL };
static bool *_p_gpio_state[SKIQ_MAX_NUM_CARDS] = { [0 ... SKIQ_MAX_NUM_CARDS-1] = NULL };
static uint8_t _num_gpio[SKIQ_MAX_NUM_CARDS] = { [0 ... SKIQ_MAX_NUM_CARDS-1] = 0 };

static void process_line(char* p_line, uint32_t line_num, uint32_t chip);
static int process_spiwrite(char* p_line, uint32_t line_num, uint32_t chip);
static int process_spiread(char* p_line, uint32_t line_num, uint32_t chip);
static int process_wait(char* p_line, uint32_t line_num);
static int process_wait_caldone(char* p_line, uint32_t line_num, uint32_t chip);
static int process_reset_dut(char* p_line, uint32_t line_num, uint32_t chip);

/*****************************************************************************/
/** This function parses and executes the commands specified in the
    ad9361 reg file passed in on the cmd line.

    @param fd the previously opened file descriptor for the config file
    @param chip the chip_id to use
    @return int32_t indicating status (0=success, anything else is an error)
*/
int32_t process_ad9361_file(FILE* fd, uint32_t chip, bool configure_gpio,
                            uint8_t *p_gpio_pos, bool *p_gpio_state, uint8_t num_gpio)
{
    char p_line[400];
    uint32_t line_num=0;
    int32_t status=0;

    // make sure chip < SKIQ_MAX_NUM_CARDS
    if( chip >= SKIQ_MAX_NUM_CARDS )
    {
        return -EINVAL;
    }
    
    _configure_gpio[chip] = configure_gpio;
    _p_gpio_pos[chip] = p_gpio_pos;
    _p_gpio_state[chip] = p_gpio_state;
    _num_gpio[chip] = num_gpio;

    // ensure we're at the beginning of the file
    rewind(fd);

    while (feof(fd) == 0 )
    {
        if( fgets( p_line, 400, fd ) != 0 )
        {
            process_line(p_line,line_num++,chip);
        }
        /* hal_nanosleep(100*MILLISEC); */
    }

    return(status);
}

/*****************************************************************************/
/** This function parses a single line from an ad9361 reg file and performs
    the requested operation.

    @param p_line a pointer to char containing the line to process
    @param line_num the line number of the line being processed
    @param chip the chip_id to use
    @return void
*/
static void process_line(char* p_line, uint32_t line_num, uint32_t chip)
{
    if (strncmp(p_line,"SPIWrite",strlen("SPIWrite")) == 0)
    {
	process_spiwrite(p_line,line_num,chip);
    }
    else if (strncmp(p_line,"SPIRead",strlen("SPIRead")) == 0)
    {
	process_spiread(p_line,line_num,chip);
    }

    /* WAIT_CALDONE _must_ be ahead of WAIT, since strncmp will trigger
       if WAIT is found when looking for WAIT_CALDONE if the order
       is reversed */
    else if (strncmp(p_line,"WAIT_CALDONE",strlen("WAIT_CALDONE")) == 0)
    {
	process_wait_caldone(p_line,line_num,chip);
    }
    else if (strncmp(p_line,"WAIT",strlen("WAIT")) == 0)
    {
	process_wait(p_line,line_num);
    }
    else if (strncmp(p_line,"RESET_DUT",strlen("RESET_DUT")) == 0)
    {
	process_reset_dut(p_line,line_num,chip);
    }
}

/*****************************************************************************/
/** This function processes a SPIWrite command

    @param p_line a pointer to char containing the line to process
    @param line_num the line number of the line being processed
    @param chip the chip_id to use

    @return void
*/
static int process_spiwrite(char* p_line, uint32_t line_num, uint32_t chip)
{
    char reg_addr_text[4];
    char reg_data_text[3];
    uint16_t addr;
    uint8_t data;

    memset(reg_addr_text,0x00,4);
    memset(reg_data_text,0x00,3);

    /* extract the reg addr/data strings, null terminated above */
    memcpy(reg_addr_text,p_line+AD9361_SPIWRITE_REG_ADDR_OFFSET,AD9361_REG_ADDR_LEN);
    memcpy(reg_data_text,p_line+AD9361_SPIWRITE_REG_DATA_OFFSET,AD9361_REG_DATA_LEN);

    /* convert from text to numbers */
    addr=(uint16_t)(strtoul(reg_addr_text,NULL,16));
    data=(uint8_t)(strtoul(reg_data_text,NULL,16));

#if PRINT_DEBUG
    DEBUG("Info: line #=%d SPIWrite addr=0x%04x data=0x%02x\n",line_num,addr,data);
#else
    // TODO: there is likely a cleaner way to do this, but it works for now
    // if we're using ad9361 gpio, we need to make sure our GPIO is always on for clocks
    if( _configure_gpio[chip] == true )
    {
        // for reg 0x026, we need to make sure that D7 is always set (enables GPIO manual control)
        if( (addr == AD9361_GPIO_REG1) )
        {
            BF_SET(data,1,BIT4,1);
        }
        // for reg 0x027, we need to make sure that the bits are set properly for our clocks
        else if( (addr == AD9361_GPIO_REG2) )
        {
            uint8_t i=0;
            // set the GPIO properly
            for( i=0; i<_num_gpio[chip]; i++ )
            {
                if( _p_gpio_state[chip][i] == true )
                {
                    BF_SET(data,1,(_p_gpio_pos[chip][i])+4,1);
                }
                else
                {
                    BF_SET(data,0,(_p_gpio_pos[chip][i])+4,1);
                }
            }
        }
    }
    hal_ad9361_write_reg(chip,addr,data);
#endif

    return(0);	
}

/*****************************************************************************/
/** This function processes a SPIRead command

    @param p_line a pointer to char containing the line to process
    @param line_num the line number of the line being processed
    @param chip the chip_id to use

    @return void
*/
static int process_spiread(char* p_line, uint32_t line_num, uint32_t chip)
{
    char reg_addr_text[4];
    uint16_t addr;
    uint8_t data;

    memset(reg_addr_text,0x00,4);

    /* extract the reg addr/data strings, null terminated above */
    memcpy(reg_addr_text,p_line+AD9361_SPIREAD_REG_ADDR_OFFSET,AD9361_REG_ADDR_LEN);

    /* convert from text to numbers */
    addr=(uint16_t)(strtoul(reg_addr_text,NULL,16));

#if PRINT_DEBUG
    DEBUG("Info: line #=%d SPIRead addr=0x%04x\n",line_num,addr);
    data=0xff;
#else
    hal_ad9361_read_reg(chip,addr,&data);
#endif

    return(0);	
}

/*****************************************************************************/
/** This function processes a WAIT command

    @param p_line a pointer to char containing the line to process
    @param line_num the line number of the line being processed
    @return void
*/
static int process_wait(char* p_line, uint32_t line_num)
{
    uint32_t time_in_mS;
    char* token;

    /* extract the wait time text, grabbing the number up to
       the tab char */
    token=strtok(p_line+AD9361_WAIT_TIME_OFFSET,"\t");

    /* convert from text to numbers */
    time_in_mS=(uint32_t)(strtoul(token,NULL,10));

    DEBUG("Info: line#=%d Wait %d mS\n",line_num,time_in_mS);
    hal_nanosleep(time_in_mS*MILLISEC);

    return(0);
}

/*****************************************************************************/
/** This function processes a WAIT_CALDONE command

    @param p_line a pointer to char containing the line to process
    @param line_num the line number of the line being processed
    @param chip the chip_id to use

    @return void
*/
static int process_wait_caldone(char* p_line, uint32_t line_num, uint32_t chip)
{
    uint16_t addr = 0;
    uint8_t data = 0;
    uint8_t bit_pos = 0;
    char* token;
    char* bitname;
    char* timeout_in_mS_str;
    uint32_t timeout_in_mS;
    uint8_t cal_complete_state = 0;
    bool done = false;
    uint32_t max_num_tries=0;
    uint32_t i=0;
    uint32_t cal_trys=0;
    bool cal_complete = false;

    DEBUG("Info: line #=%d has a WAIT_CALDONE\n",line_num);
    /* extract the bit name + timeout string for the calibration */
//    token=strtok_r(p_line+AD9361_WAIT_CALDONE_BIT_OFFSET,"\t", &save_ptr1);
    token=strtok(p_line+AD9361_WAIT_CALDONE_BIT_OFFSET,"\t");
    DEBUG("Info: Extracted token is %s\n",token);

    /* separate bit field and time */
    bitname=strtok(token,",");
    DEBUG("Info: Extracted bit name is %s\n",bitname);

    timeout_in_mS_str=strtok(NULL,",");
    timeout_in_mS = (uint32_t)(strtoul(timeout_in_mS_str,NULL,10));
    max_num_tries = timeout_in_mS;
    DEBUG("Info: Extracted timeout in mS is %d\n",timeout_in_mS);

    done = false;
    while (!done)
    {
        if (strncmp(bitname,wait_caldone_list[i].bitname,8) == 0)
        {
            /* found a match, extract everything else */
            addr = wait_caldone_list[i].addr;
            bit_pos = wait_caldone_list[i].bit;
            cal_complete_state = wait_caldone_list[i].cal_complete_state;
            DEBUG("Info: Found a wait_caldone entry match for %s\n",bitname);
            DEBUG("   addr=0x%04x, bitpos=%d, cal_complete_state=%d\n", \
              addr,bit_pos,cal_complete_state);
            done = true;
        }
        if (strncmp ("END",wait_caldone_list[i].bitname,3) == 0)
        {
            printf("Error: didn't find bit field %s in the wait_caldone_list\n",bitname);
            hal_critical_exit(-1);
        }
        i++;
    }

    cal_trys=0;
    cal_complete=false;

    /* perform check of bit */
    while((cal_complete == false) && (cal_trys < max_num_tries))
    {
        hal_nanosleep(1*MILLISEC);
#if PRINT_DEBUG
        /* DEBUG("Info: read ad9361 reg addr 0x%04x\n",addr); */
        data=0xff;
#else
        hal_ad9361_read_reg(chip,addr,&data);
#endif
        if (cal_complete_state == 1)
        {
            if (data & (1<<bit_pos))
            {
                DEBUG("Info: Calibration for %s was successful after %d mS\n",bitname,cal_trys);
                cal_complete = true;
            }
            else
            {
                cal_trys++;
            }
        }
        else
        {
            if (data & (1<<bit_pos))
            {
                cal_trys++;
            }
            else
            {
                DEBUG("Calibration for %s was successful after %d mS\n",bitname,cal_trys);
                cal_complete = true;
            }
        }
    }
    if (cal_complete == false)
    {
        printf("Error: failed to calibrate bitfield %s after %d mS\n",bitname,cal_trys);
        hal_critical_exit(-1);
    }

    return(0);
}

/*****************************************************************************/
/** This function processes the RESET_DUT command.

    @param p_line a pointer to char containing the line to process
    @param line_num the line number of the line being processed
    @param chip the chip_id to use

    @return void
*/
static int process_reset_dut(char* p_line, uint32_t line_num, uint32_t chip)
{
    uint8_t i=0;
    
    /* to reset the ad9361, a manual toggling of bit 7 in reg 0x000 will
       do the trick */
    hal_ad9361_write_reg(chip,0x000,0x80);
    hal_ad9361_write_reg(chip,0x000,0x00);

    // if we need the GPIO enabled, be sure to configure appropriately after reset
    if( _configure_gpio[chip] == true )
    {
        ad9361_write_manual_gpio_state( chip, true );
        // setup the rest of the GPIO
        for( i=0; i<_num_gpio[chip]; i++ )
        {
            // set the RF power on and internal ref
            ad9361_write_gpio_ctrl( chip,
                                    _p_gpio_pos[chip][i],
                                    _p_gpio_state[chip][i] );
        }

    }

    return(0);
}

