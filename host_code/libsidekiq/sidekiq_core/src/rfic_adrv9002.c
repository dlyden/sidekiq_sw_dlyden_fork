/**
   @file   rfic_adrv9002.c
   @author  <info@epiqsolutions.com>
   @date   Mon May 18 11:13:40 2020

   @brief  Implements the libsidekiq RFIC interface for the RFIC ADRV9002


   <pre>
   Copyright 2020-2022 Epiq Solutions, All Rights Reserved

   </pre>

*/

/***** INCLUDES *****/

#include <stdint.h>
#include <stdarg.h>             /* for va_list */
#include <string.h>             /* for memcpy() */
#include <inttypes.h>
#include <math.h>

#include "sidekiq_types.h"
#include "sidekiq_fpga.h"       /* for sidekiq_fpga_reg_*() and friends */
#include "sidekiq_rfic.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"        /* for hal_rfic_read_reg() and hal_rfic_write_reg() */

#include "hal_nv100.h"          /* for nv100_select_ref_clock() */
#include "dctcxo_sit5356.h"     /* sit5356_dctcxo_*_warp_voltage() */

#include "rfic_adrv9002.h"

#include "adi_adrv9001.h"
#include "adi_adrv9001_auxadc.h"  /* for adi_adrv9001_AuxAdc_*() functions */
#include "adi_adrv9001_bbdc.h"  /* for adi_adrv9001_bbdc_*() functions */
#include "adi_adrv9001_cals.h"
#include "adi_adrv9001_gpio.h"  /* for adi_adrv9001_gpio_*() functions */
#include "adi_adrv9001_pfirBuffer_types.h"
#include "adi_adrv9001_radio.h"
#include "adi_adrv9001_rx.h"    /* for adi_adrv9001_Rx_*() functions */
#include "adi_adrv9001_rx_gaincontrol.h"    /* for adi_adrv9001_Rx_*() functions */
#include "adi_adrv9001_ssi.h"   /* for support of PRBS testing */
#include "adi_adrv9001_tx.h"    /* for adi_adrv9001_Tx_*() functions */
#include "adi_adrv9001_utilities.h"
#include "adi_adrv9001_version.h"
#include "adi_common_error.h"   /* for adi_common_ErrorClear() */
#include "epiq_adrv9002.h"
#include "epiq_adrv9002_pal.h"
#include "epiq_adrv9002_profiles.h"
#include "epiq_adrv9002_rx_pfir.h"

/* enable TRACE_* macros when DEBUG_RFIC_ADRV9002_TRACE is defined */
#if (defined DEBUG_RFIC_ADRV9002_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

/* enable debug_print and debug_print_plain when DEBUG_RFIC_ADRV9002 is defined */
#if (defined DEBUG_RFIC_ADRV9002)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define ENABLE                          true
#define DISABLE                         false

#define LOOK_FOR_TX                     true
#define LOOK_FOR_RX                     false

#define BW_HZ_INVALID                   UINT32_MAX


/***** PRODUCT DEFINITIONS *****/

/* GPIO bits */
#define TX1_EN_OFFSET                   12
#define TX1_EN_LEN                      1
#define TX2_EN_OFFSET                   13
#define TX2_EN_LEN                      1
#define RX1_EN_OFFSET                   14
#define RX1_EN_LEN                      1
#define RX2_EN_OFFSET                   15
#define RX2_EN_LEN                      1

/* PRBS bits - for use with FPGA_REG_ADC_NUM_ERRORS_A and FPGA_REG_ADC_NUM_ERRORS_B */
#define BIT_ERROR_OFFSET                0
#define BIT_ERROR_LEN                   1
#define OUT_OF_SYNC_ERROR_OFFSET        1
#define OUT_OF_SYNC_ERROR_LEN           1

/* PRBS error masks - used in navassa_check_prbs_status() to indicate error types detected */
#define BIT_ERROR_RX1_MASK              (1 << 0)
#define OOS_ERROR_RX1_MASK              (1 << 1)
#define BIT_ERROR_RX2_MASK              (1 << 2)
#define OOS_ERROR_RX2_MASK              (1 << 3)

/***** PART DEFINITIONS *****/

#define DEFAULT_WARP_VOLTAGE_NV100      (1 << 15)
#define ADI_ADRV9001_AUXADC_THERM       ADI_ADRV9001_AUXADC0
#define RX_RF_PORT_B_MAX_FREQ_HZ        (uint64_t)(1700000000)


/***** FPGA DEFINITIONS *****/

#define TX_TONE_EN                      0x8
#define TX_TONE_DIS                     0x0


/***** RFIC DEFINITIONS *****/

#define NAV_ADC_RESOLUTION              16
#define NAV_DAC_RESOLUTION              16
#define NAV_MIN_SAMPLE_RATE             24000
#define NAV_MAX_SAMPLE_RATE             61440000
#define NAV_MIN_TX_FREQUENCY_IN_HZ      (uint64_t)(30ULL*RATE_MHZ_TO_HZ)
#define NAV_MAX_TX_FREQUENCY_IN_HZ      (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define NAV_MIN_RX_FREQUENCY_IN_HZ      (uint64_t)(30ULL*RATE_MHZ_TO_HZ)
#define NAV_MAX_RX_FREQUENCY_IN_HZ      (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define NAV_MIN_TX_ATTEN_QDB            0
#define NAV_MAX_TX_ATTEN_QDB            167
#define NAV_NR_DIGITAL_GPIO             16

#define NAV_INIT_CAL_TIMEOUT_MS         24000

#define NAV_DEFAULT_SAMPLE_RATE         15360000
#define NAV_DEFAULT_BANDWIDTH           9000000

/**
   @brief ADI recommends performing calibration if tuning more than 100MHz from the previous
    calibration frequency
*/
#define NAV_MAX_FREQ_CAL_DELTA          ((uint64_t)100000000)

#define NAV_RX_CAL_TYPES                (skiq_rx_cal_type_dc_offset | skiq_rx_cal_type_quadrature)


#if (defined DEBUG_PRINT_ENABLED)
#define RFIC_LOG_LEVEL                  ADI_LOGLEVEL_DEBUG
#else
#define RFIC_LOG_LEVEL                  ADI_LOGLEVEL_INFO
#endif

/*
  The configuration delays in EPIQ_HAL_RX_SSI_CFG_DEFAULT were determined by empirical testing over
  several Sidekiq NV100 units.

  For more information, please visit
  https://confluence.epiq.rocks/display/EN/Sidekiq+NV100+PRBS+testing+and+LSSI+configuration
 */
#define EPIQ_HAL_RX_SSI_CFG_DEFAULT             \
    {                                           \
        .clk_delay = 3,                         \
        .strobe_delay = 0,                      \
        .idata_delay = 0,                       \
        .qdata_delay = 0,                       \
    }

/*
  The configuration delays in EPIQ_HAL_TX_SSI_CFG_DEFAULT were determined by analysis of the PCB
  routing and FPGA design and verified on several Sidekiq NV100 units.
 */
#define EPIQ_HAL_TX_SSI_CFG_DEFAULT             \
    {                                           \
        .ref_clk_delay = 0,                     \
        .clk_delay = 6,                         \
        .strobe_delay = 0,                      \
        .idata_delay = 0,                       \
        .qdata_delay = 0,                       \
    }

#define EPIQ_HAL_DEFAULT                                                \
    {                                                                   \
        .card = 0,                                                      \
        .cs = 0,                                                        \
        .logLevel = RFIC_LOG_LEVEL,                                     \
        .rx_ssi_cfg = INIT_ARRAY(ADI_ADRV9001_NUM_CHANNELS, EPIQ_HAL_RX_SSI_CFG_DEFAULT), \
        .tx_ssi_cfg = INIT_ARRAY(ADI_ADRV9001_NUM_CHANNELS, EPIQ_HAL_TX_SSI_CFG_DEFAULT), \
        .spi_read_byte = hal_rfic_read_reg,                             \
        .spi_write_byte = hal_rfic_write_reg,                           \
        .log_msg = log_adihal_msg,                                      \
        .hw_reset = rfic_hw_reset,                                      \
    }                                                                   \


static const adi_adrv9001_Device_t navassa_device_default =
{
    .common =
    {
        .devHalInfo = NULL,
        .error = {
            .logEnable = true,
        },
    },

    .spiSettings =
    {
        .msbFirst = 1,
        .enSpiStreaming = 0,
        .autoIncAddrUp = 1,
        .fourWireMode = 1,
        .cmosPadDrvStrength = ADI_ADRV9001_CMOSPAD_DRV_WEAK,
    },
};

/***** MACROS *****/

#define TRACE_ARGS_RF_ID(_rf_id)                                        \
    do {                                                                \
        TRACE_ARGS(#_rf_id ": %p, " #_rf_id "->card: %" PRIu8 ", " #_rf_id "->hdl: %" PRIu8 "\n", \
                   (_rf_id), (_rf_id)->card, (_rf_id)->hdl);            \
    } while (0)

/**
   @brief This macro performs two actions (one of them conditionally)

   1. It handles the translation of return codes from Navassa API functions into the libsidekiq
      error code domain (negative errno).  It needs to follow *EACH* Navassa API function call.  In
      practice, calling Navassa API functions without having a cleared error report would result in
      more errors, even if the error had been dealt with.

      ADI_COMMON_ACT_NO_ACTION becomes 0.

      Anything else becomes -EPROTO

   2. If the return code is not ADI_COMMON_ACT_NO_ACTION, it also calls adi_common_ErrorClear() to
      clear out the error variables so that subsequent Navassa API functions may be called
      successfully
 */
#define ADI_HANDLE_ERROR(device_f,status_f)                       \
    do {                                                          \
        if ( ( status_f ) == ADI_COMMON_ACT_NO_ACTION )           \
        {                                                         \
            status_f = 0;                                         \
        }                                                         \
        else                                                      \
        {                                                         \
            status_f = -EPROTO;                                   \
            adi_common_ErrorClear( &((device_f)->common) );       \
        }                                                         \
    } while (0)

#define RADIO_LOCK(_card)               LOCK(radio_lock[_card])
#define RADIO_UNLOCK(_card)             UNLOCK(radio_lock[_card])


/***** INITIALIZER MACROS *****/

#define RADIO_CARRIER_INITIALIZER                                       \
    {                                                                   \
        .loGenOptimization = ADI_ADRV9001_LO_GEN_OPTIMIZATION_PHASE_NOISE, \
        .carrierFrequency_Hz = 0,                                       \
        .intermediateFrequency_Hz = 0,                                  \
        .manualRxport = ADI_ADRV9001_RX_A,                              \
    }

#define RADIO_TX_HDL_CONFIG_INITIALIZER                         \
    {                                                           \
        .enabled = false,                                       \
        .carrier = RADIO_CARRIER_INITIALIZER,                   \
        .atten_mdB = NAV_TX_ATTEN_DEFAULT,                      \
        .cal_mode = skiq_tx_quadcal_mode_manual,                \
        .last_cal_freq = UINT64_MAX,                            \
    }

#define RADIO_RX_HDL_CONFIG_INITIALIZER                         \
    {                                                           \
        .enabled = false,                                       \
        .carrier = RADIO_CARRIER_INITIALIZER,                   \
        .gain_mode = NAV_RX_GAIN_CONTROL_MODE_DEFAULT,          \
        .gain_index = NAV_RX_GAIN_INDEX_DEFAULT,                \
        .samp_rate_hz = UINT32_MAX,                             \
        .bandwidth_hz = BW_HZ_INVALID,                          \
        .actual_bandwidth_hz = BW_HZ_INVALID,                   \
        .cal_mode = skiq_rx_cal_mode_manual,                    \
        .last_cal_freq = UINT64_MAX,                            \
        .cal_mask = NAV_RX_CAL_TYPES,                           \
    }


/***** TYPEDEFS *****/


/***** STRUCTS *****/

struct radio_LO_selection
{
    adi_adrv9001_LoSel_e rx1LoSelect;
    adi_adrv9001_LoSel_e tx1LoSelect;
    adi_adrv9001_LoSel_e rx2LoSelect;
    adi_adrv9001_LoSel_e tx2LoSelect;
    bool lo_selection_pending;
};

struct radio_tx_hdl_config
{
    bool enabled;
    adi_adrv9001_Carrier_t carrier;
    uint16_t atten_mdB;

    skiq_tx_quadcal_mode_t cal_mode;
    uint64_t last_cal_freq;
};

struct radio_rx_hdl_config
{
    bool enabled;
    adi_adrv9001_Carrier_t carrier;
    adi_adrv9001_RxGainControlMode_e gain_mode;
    uint8_t gain_index;

    uint32_t samp_rate_hz;
    uint32_t bandwidth_hz, actual_bandwidth_hz;

    skiq_rx_cal_mode_t cal_mode;
    uint64_t last_cal_freq;
    uint32_t cal_mask;
};

struct radio_config
{
    bool active;

    bool test_tone_enabled;

    skiq_rx_hdl_t rfic_control_output_dest;

    adi_adrv9001_Init_t *reference_profile;

    struct radio_rx_hdl_config rx[skiq_rx_hdl_end];
    struct radio_tx_hdl_config tx[skiq_tx_hdl_end];

    struct radio_LO_selection lo;
};


/***** LOCAL FUNCTION PROTOTYPES *****/

static int32_t log_adihal_msg( uint8_t logLevel,
                               const char *format,
                               va_list args );

static int32_t write_warp_voltage( uint8_t card,
                                   uint16_t warp_voltage );

static int32_t read_gpio(uint8_t card,
                         uint32_t *p_value );

static int32_t inspect_tx_test_tone( uint8_t card,
                                     bool *p_enable );

static int32_t config_tx_test_tone( uint8_t card,
                                     bool enable );

static int32_t rfic_hw_reset( uint8_t card,
                              uint8_t pinLevel );

static int32_t run_rx_cal_if_needed( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     skiq_rx_cal_mode_t cal_mode,
                                     uint64_t lo_freq,
                                     uint64_t *p_last_cal_freq );

static int32_t run_tx_quadcal_if_needed( uint8_t card,
                                         skiq_tx_hdl_t hdl,
                                         skiq_tx_quadcal_mode_t cal_mode,
                                         uint64_t lo_freq,
                                         uint64_t *p_last_cal_freq );

static int32_t enable_rx_chan( uint8_t card,
                               skiq_rx_hdl_t hdl,
                               bool enable );

static int32_t enable_rx_chan_all( rf_id_t *p_id,
                                   bool enable );

static int32_t enable_tx_chan( uint8_t card,
                               skiq_tx_hdl_t hdl,
                               bool enable );

static int32_t enable_tx_chan_all( rf_id_t *p_id,
                                   bool enable );

static int32_t write_control_output_config( uint8_t card,
                                            uint8_t mode );

static adi_adrv9001_Device_t *get_adrv9001_device( uint8_t card );

static int32_t write_rx_cal_mask( uint8_t card, skiq_rx_hdl_t hdl, uint32_t cal_mask);

#if (defined DEBUG_PRINT_ENABLED)
static const char * channel_state_cstr( adi_adrv9001_ChannelState_e state );
#endif

/***** LOCAL VARIABLES *****/


static const adi_common_ChannelNumber_e skiq_rx_hdl_to_channel[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = ADI_CHANNEL_1,
    [skiq_rx_hdl_A2] = ADI_CHANNEL_2,
    [skiq_rx_hdl_B1] = ADI_CHANNEL_2,
};

static const adi_common_ChannelNumber_e skiq_tx_hdl_to_channel[skiq_tx_hdl_end] =
{
    [skiq_tx_hdl_A1] = ADI_CHANNEL_1,
    [skiq_tx_hdl_A2] = ADI_CHANNEL_2,
    [skiq_tx_hdl_B1] = ADI_CHANNEL_2,
};

static const adi_common_ChannelNumber_e skiq_rx_hdl_to_mask_channel[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = ADI_ADRV9001_RX1,
    [skiq_rx_hdl_A2] = ADI_ADRV9001_RX2,
    [skiq_rx_hdl_B1] = ADI_ADRV9001_RX2,
};

static const adi_common_ChannelNumber_e skiq_tx_hdl_to_mask_channel[skiq_tx_hdl_end] =
{
    [skiq_tx_hdl_A1] = ADI_ADRV9001_TX1,
    [skiq_tx_hdl_A2] = ADI_ADRV9001_TX2,
    [skiq_tx_hdl_B1] = ADI_ADRV9001_TX2,
};

static const uint8_t skiq_rx_hdl_to_array_index[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = 0,
    [skiq_rx_hdl_A2] = 1,
    [skiq_rx_hdl_B1] = 1,
};

static const uint8_t skiq_tx_hdl_to_array_index[skiq_tx_hdl_end] =
{
    [skiq_tx_hdl_A1] = 0,
    [skiq_tx_hdl_A2] = 1,
    [skiq_tx_hdl_B1] = 1,
};

/**
   @brief The radio_LO_selection of radio_config was chosen to match legacy 
   behavior of nv100 on libsidekiq versions <= v4.17.5, before the introduction
   of LO steering
*/
static const struct radio_config radio_config_default =
{
    .active = false,
    .test_tone_enabled = false,
    .rfic_control_output_dest = skiq_rx_hdl_end,
    .reference_profile = NULL,
    .rx = {
        [ 0 ... (skiq_rx_hdl_end-1) ] = RADIO_RX_HDL_CONFIG_INITIALIZER,
    },
    .tx = {
        [ 0 ... (skiq_tx_hdl_end-1) ] = RADIO_TX_HDL_CONFIG_INITIALIZER,
    },
    .lo.rx1LoSelect = ADI_ADRV9001_LOSEL_LO1,
    .lo.tx1LoSelect = ADI_ADRV9001_LOSEL_LO2,
    .lo.rx2LoSelect = ADI_ADRV9001_LOSEL_LO2,
    .lo.tx2LoSelect = ADI_ADRV9001_LOSEL_LO2,
    .lo.lo_selection_pending = false,
};


/**
   @brief The @a radio_lock mutexes protect access to @a radio_conf and serialize API calls for a
   given radio specified by a card index
*/
static MUTEX_ARRAY(radio_lock, SKIQ_MAX_NUM_CARDS);
static struct radio_config radio_conf[SKIQ_MAX_NUM_CARDS];


/**
   @brief After indexing by card, the adi_adrv9001_Device_t entry is passed into all NAVASSA API
   functions
*/
static adi_adrv9001_Device_t rfic[SKIQ_MAX_NUM_CARDS];

static ARRAY_WITH_DEFAULTS(epiq_adrv9002_dev_t, rfic_hal, SKIQ_MAX_NUM_CARDS, EPIQ_HAL_DEFAULT);

static adi_adrv9001_Init_t current_profile[SKIQ_MAX_NUM_CARDS];


/***** DEBUG FUNCTIONS *****/

#include "debug_rfic_adrv9002.h"


/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
/**
   @brief Use this function prior to using @p hdl in a lookup in radio_conf[card].rx[hdl]
 */
static int32_t check_rx_hdl_valid( skiq_rx_hdl_t hdl )
{
    int32_t status = 0;

    switch (hdl)
    {
        case skiq_rx_hdl_A1:
        case skiq_rx_hdl_A2:
        case skiq_rx_hdl_B1:
            status = 0;
            break;

        default:
            status = -EINVAL;
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief Use this function prior to using @p hdl in a lookup in radio_conf[card].tx[hdl]
 */
static int32_t check_tx_hdl_valid( skiq_tx_hdl_t hdl )
{
    int32_t status = 0;

    switch (hdl)
    {
        case skiq_tx_hdl_A1:
        case skiq_tx_hdl_A2:
        case skiq_tx_hdl_B1:
            status = 0;
            break;

        default:
            status = -EINVAL;
    }

    return status;
}


/**************************************************************************************************/
#if (defined DEBUG_PRINT_ENABLED)
static const char * channel_state_cstr( adi_adrv9001_ChannelState_e state )
{
    const char* p_chan_state_str =
        ( state == ADI_ADRV9001_CHANNEL_STANDBY ) ? "STANDBY" :
        ( state == ADI_ADRV9001_CHANNEL_CALIBRATED ) ? "CALIBRATED" :
        ( state == ADI_ADRV9001_CHANNEL_PRIMED ) ? "PRIMED" :
        ( state == ADI_ADRV9001_CHANNEL_RF_ENABLED ) ? "RF ENABLED" :
        "unknown";

    return p_chan_state_str;
}
#endif


/**************************************************************************************************/
static int32_t log_adihal_msg( uint8_t logLevel,
                               const char *format,
                               va_list args )
{
    int32_t priority = SKIQ_LOG_ERROR;
    bool strictly_debug = false;

    /* translate ADI_COMMON_LOG levels to SKIQ_LOG levels or mark them as `strictly_debug` */
    switch (logLevel)
    {
        case ADI_LOGLEVEL_INFO:
            priority = SKIQ_LOG_INFO;
            break;

        case ADI_LOGLEVEL_WARN:
            priority = SKIQ_LOG_WARNING;
            break;

        case ADI_LOGLEVEL_ERROR:
            priority = SKIQ_LOG_ERROR;
            break;

        case ADI_LOGLEVEL_NONE:
        case ADI_LOGLEVEL_TRACE:
        case ADI_LOGLEVEL_DEBUG:
            strictly_debug = true;
            break;

        default:
            priority = SKIQ_LOG_ERROR;
            break;
    }

    /* if the log level is deemed strictly debug, use libsidekiq's debug_vprint() facility,
     * otherwise go through _skiq_vlog() so that users can see the message */
    if ( strictly_debug )
    {
        debug_vprint(format, args);
        debug_print_plain("\n");
    }
    else
    {
        _skiq_vlog(priority, format, args);
    }

    return 0;
}


/**************************************************************************************************/
static int32_t rfic_hw_reset( uint8_t card,
                              uint8_t pinLevel )
{
    int32_t status = 0;

    debug_print("Set RFIC's RESETB to %" PRIu8 " on card %" PRIu8 "\n", pinLevel, card);
    status = nv100_set_rfic_reset( card, ( pinLevel == 0 ) );

    return status;
}


/**************************************************************************************************/
static adi_adrv9001_Device_t *get_adrv9001_device( uint8_t card )
{
    return &(rfic[card]);
}


/**************************************************************************************************/
static epiq_adrv9002_dev_t *get_adrv9001_device_hal( uint8_t card )
{
    return &(rfic_hal[card]);
}


/**************************************************************************************************/
static bool is_rx_chan_enabled( uint8_t card,
                                skiq_rx_hdl_t rx_hdl )
{
    return radio_conf[card].rx[rx_hdl].enabled;
}


/**************************************************************************************************/
static void mark_rx_chan_state( uint8_t card,
                                skiq_rx_hdl_t rx_hdl,
                                bool enable )
{
    radio_conf[card].rx[rx_hdl].enabled = enable;
}


/**************************************************************************************************/
static bool is_tx_chan_enabled( uint8_t card,
                                skiq_tx_hdl_t tx_hdl )
{
    return radio_conf[card].tx[tx_hdl].enabled;
}


/**************************************************************************************************/
static void mark_tx_chan_state( uint8_t card,
                                skiq_tx_hdl_t tx_hdl,
                                bool enable )
{
    radio_conf[card].tx[tx_hdl].enabled = enable;
}


/**************************************************************************************************/
static adi_adrv9001_Carrier_t *get_rx_carrier( uint8_t card,
                                               skiq_rx_hdl_t rx_hdl )
{
    return &(radio_conf[card].rx[rx_hdl].carrier);
}


/**************************************************************************************************/
static adi_adrv9001_Carrier_t *get_tx_carrier( uint8_t card,
                                               skiq_tx_hdl_t tx_hdl )
{
    return &(radio_conf[card].tx[tx_hdl].carrier);
}


/**************************************************************************************************/
static bool is_rfic_streaming( uint8_t card )
{
    struct radio_config *p_conf = &(radio_conf[card]);
    bool is_streaming;

    is_streaming =
        p_conf->rx[skiq_rx_hdl_A1].enabled ||
        p_conf->rx[skiq_rx_hdl_A2].enabled ||
        p_conf->rx[skiq_rx_hdl_B1].enabled ||
        p_conf->tx[skiq_tx_hdl_A1].enabled ||
        p_conf->tx[skiq_tx_hdl_A2].enabled ||
        p_conf->tx[skiq_tx_hdl_B1].enabled;

    return is_streaming;
}

static bool is_requested_rx_lo_source_active( uint8_t card,
                                              skiq_rx_hdl_t rx_hdl )
{
    bool result = true;
    /* To use A2, LO1 must be selected, aka rx2LoSelect == ADI_ADRV9001_LOSEL_LO1  */
    if ((rx_hdl == skiq_rx_hdl_A2) && (radio_conf[card].lo.rx2LoSelect != ADI_ADRV9001_LOSEL_LO1))
    {
        result = false;
    }
    /* To use B1, LO2 must be selected, aka rx2LoSelect == ADI_ADRV9001_LOSEL_LO2*/
    else if ((rx_hdl == skiq_rx_hdl_B1) && (radio_conf[card].lo.rx2LoSelect != ADI_ADRV9001_LOSEL_LO2))
    {
        result = false;
    }

    return (result);
}


/**************************************************************************************************/
static int32_t wait_chan_state( adi_adrv9001_Device_t *device,
                                adi_common_Port_e port,
                                adi_common_ChannelNumber_e channel,
                                adi_adrv9001_ChannelState_e to_state )
{
    int32_t status = 0;
    const uint8_t NUM_TRIES = 50;
    uint8_t i;
    adi_adrv9001_ChannelState_e chan_state_verify;

    for ( i = 0; ( i < NUM_TRIES ) && ( status == 0 ); i++)
    {
        status = adi_adrv9001_Radio_Channel_State_Get( device, port, channel, &chan_state_verify );
        ADI_HANDLE_ERROR( device, status );
        if ( status == 0 )
        {
            if ( chan_state_verify == to_state )
            {
                break;
            }

            status = hal_microsleep( 100 );
        }
    }

    if ( status == 0 )
    {
        if ( chan_state_verify != to_state )
        {
            status = -ETIMEDOUT;
        }
    }

    return status;
}


/**************************************************************************************************/
static int32_t wait_rx_chan_state( adi_adrv9001_Device_t *device,
                                   adi_common_ChannelNumber_e channel,
                                   adi_adrv9001_ChannelState_e to_state )
{
    return wait_chan_state( device, ADI_RX, channel, to_state );
}


/**************************************************************************************************/
static int32_t wait_tx_chan_state( adi_adrv9001_Device_t *device,
                                   adi_common_ChannelNumber_e channel,
                                   adi_adrv9001_ChannelState_e to_state )
{
    return wait_chan_state( device, ADI_TX, channel, to_state );
}


/**************************************************************************************************/
/**
   @brief Transitions the specified transmit channel from one of { CALIBRATED, PRIMED, RF_ENABLED }
   to { CALIBRATED }.
 */
static int32_t move_chan_to_calibrated( uint8_t card,
                                        adi_common_Port_e port,
                                        adi_common_ChannelNumber_e channel )
{
    int32_t status = 0;
    uint32_t value = 0, prev_value = 0;
    adi_adrv9001_ChannelState_e chan_state;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );

    status = adi_adrv9001_Radio_Channel_State_Get( device, port, channel, &chan_state );
    ADI_HANDLE_ERROR( device, status );
    if ( ( status == 0 ) && ( chan_state == ADI_ADRV9001_CHANNEL_CALIBRATED ) )
    {
        return 0;
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
    }

    if ( status == 0 )
    {
        prev_value = value;
        if ( channel == ADI_CHANNEL_1 )
        {
            if ( port == ADI_RX )
            {
                RBF_SET( value, 0, RX1_EN );
            }
            else
            {
                /* both RX1 and TX1 need to be in PRIMED for a retune (in CALIBRATED) to work */
                RBF_SET( value, 0, RX1_EN );
                RBF_SET( value, 0, TX1_EN );
            }
        }
        else if ( channel == ADI_CHANNEL_2 )
        {
            if ( port == ADI_RX )
            {
                RBF_SET( value, 0, RX2_EN );
            }
            else
            {
                /* both RX2 and TX2 need to be in PRIMED for a retune (in CALIBRATED) to work */
                RBF_SET( value, 0, RX2_EN );
                RBF_SET( value, 0, TX2_EN );
            }
        }
        else
        {
            status = -EINVAL;
        }
    }

    if ( ( status == 0 ) && ( prev_value != value ) )
    {
        debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", value);
        status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
    }

    if ( status == 0 )
    {
        /**
           @attention Sometimes the RFIC's channel state is not correct.  It will indicate the
           channel is in PRIMED when it may be in RF_ENABLED. So even if wait_chan_state() returns a
           success, the channel may still be in RF_ENABLED.  However, the code that follows this
           call will repeatedly request the CALIBRATED state and seems to take no more than two
           requests.
        */
        status = wait_chan_state( device, port, channel, ADI_ADRV9001_CHANNEL_PRIMED );
    }

    /* Repeatedly request to transition to CALIBRATED and then immediately check the current state.
     * If the state is not CALIBRATED, repeat the request */
    {
        adi_adrv9001_ChannelState_e to_state = ADI_ADRV9001_CHANNEL_CALIBRATED;

        const uint8_t NUM_TRIES = 50;
        uint8_t i;

        for ( i = 0; ( i < NUM_TRIES ) && ( status == 0 ); i++)
        {
            if ( status == 0 )
            {
                status = adi_adrv9001_Radio_Channel_ToState( device, port, channel, to_state );
                ADI_HANDLE_ERROR( device, status );
            }

            if ( status == 0 )
            {
                status = adi_adrv9001_Radio_Channel_State_Get( device, port, channel, &chan_state );
                ADI_HANDLE_ERROR( device, status );
            }

            if ( chan_state == to_state )
            {
                break;
            }
        }

        if ( chan_state != to_state )
        {
            debug_print("Timed out transitioning to CALIBRATED\n");
            status = -ETIMEDOUT;
        }
        else
        {
            debug_print("Took %u time(s) to transition to CALIBRATED\n", i);
        }
    }

    return status;
}


/**************************************************************************************************/
static int32_t move_rx_chan_to_calibrated( uint8_t card,
                                           adi_common_ChannelNumber_e channel )
{
    return move_chan_to_calibrated( card, ADI_RX, channel );
}


/**************************************************************************************************/
static int32_t move_tx_chan_to_calibrated( uint8_t card,
                                           adi_common_ChannelNumber_e channel )
{
    return move_chan_to_calibrated( card, ADI_TX, channel );
}


/**************************************************************************************************/
/*
  Takes the specified rx_hdl from CALIBRATED to PRIMED and conditionally to RF_ENABLED.  Blocks
  waiting for the transition to RF_ENABLED when the complementary transmit handle is NOT enabled and
  the receive handle IS enabled.
 */
static int32_t restore_rx_chan_state_from_calibrated( uint8_t card,
                                                      skiq_rx_hdl_t rx_hdl )
{
    int32_t status = 0;
    uint32_t value = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    adi_common_ChannelNumber_e channel = skiq_rx_hdl_to_channel[rx_hdl];

    /* always transition to PRIMED */
    if ( status == 0 )
    {
        status = epiq_adrv9002_restore_channel_state( device, ADI_RX, channel,
                                                      ADI_ADRV9001_CHANNEL_PRIMED );
        ADI_HANDLE_ERROR( device, status );
    }

    /* if the transmit channel is enabled, assert the corresponding TXn_EN pin */
    if ( ( status == 0 ) && is_rx_chan_enabled( card, rx_hdl ) )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
        if ( status == 0 )
        {
            if ( channel == ADI_CHANNEL_1 )
            {
                RBF_SET( value, 1, RX1_EN );
            }
            else if ( channel == ADI_CHANNEL_2 )
            {
                RBF_SET( value, 1, RX2_EN );
            }

            debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", value);
            status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
        }
    }

    if ( ( status == 0 ) && is_rx_chan_enabled( card, rx_hdl ) )
    {
        skiq_tx_hdl_t tx_hdl =
            (rx_hdl == skiq_rx_hdl_A1) ? skiq_tx_hdl_A1 :
            (rx_hdl == skiq_rx_hdl_A2) ? skiq_tx_hdl_A2 : skiq_tx_hdl_B1;

        if ( !is_tx_chan_enabled( card, tx_hdl ) )
        {
            /* if the complementary transmit handle is NOT enabled and the receive handle IS
             * enabled, then we know that the receive channel will transition to RF_ENABLED after
             * RXn_EN is asserted, so wait here until that happens */
            status = wait_rx_chan_state( device, channel, ADI_ADRV9001_CHANNEL_RF_ENABLED );
        }
    }

    return status;
}


/**************************************************************************************************/
/*
  Takes the specified tx_hdl from CALIBRATED to PRIMED and conditionally to RF_ENABLED.  Does not
  block waiting for the transition to RF_ENABLED since the FPGA's proxy mode may be holding back the
  TXn_EN from reaching the RFIC if there is no data to transmit.
 */
static int32_t restore_tx_chan_state_from_calibrated( uint8_t card,
                                                      skiq_tx_hdl_t tx_hdl )
{
    int32_t status = 0;
    uint32_t value = 0, prev_value = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    adi_common_ChannelNumber_e channel = skiq_tx_hdl_to_channel[tx_hdl];

    /* always transition to PRIMED */
    if ( status == 0 )
    {
        status = epiq_adrv9002_restore_channel_state( device, ADI_TX, channel,
                                                      ADI_ADRV9001_CHANNEL_PRIMED );
        ADI_HANDLE_ERROR( device, status );
    }

    /* if the transmit channel is enabled, assert the corresponding TXn_EN pin */
    if ( ( status == 0 ) && is_tx_chan_enabled( card, tx_hdl ) )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
        if ( status == 0 )
        {
            prev_value = value;
            if ( channel == ADI_CHANNEL_1 )
            {
                RBF_SET( value, 1, TX1_EN );
            }
            else if ( channel == ADI_CHANNEL_2 )
            {
                RBF_SET( value, 1, TX2_EN );
            }

            if ( prev_value != value )
            {
                debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", value);
                status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
            }
        }
    }

    /* if the complementary receive channel is enabled, assert the corresponding RXn_EN pin */
    if ( ( status == 0 ) && is_rx_chan_enabled( card,
                                                (tx_hdl == skiq_tx_hdl_A1) ? skiq_rx_hdl_A1 :
                                                (tx_hdl == skiq_tx_hdl_A2) ? skiq_rx_hdl_A2 : skiq_rx_hdl_B1 ) )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
        if ( status == 0 )
        {
            prev_value = value;
            if ( channel == ADI_CHANNEL_1 )
            {
                RBF_SET( value, 1, RX1_EN );
            }
            else if ( channel == ADI_CHANNEL_2 )
            {
                RBF_SET( value, 1, RX2_EN );
            }

            if ( prev_value != value )
            {
                debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", value);
                status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
            }
        }
    }

    return status;
}


/**************************************************************************************************/
static void debug_print_device_state( uint8_t card )
{
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    uint16_t devState = device->devStateInfo.devState;

    debug_print("state:");
    if ( devState == ADI_ADRV9001_STATE_POWERON_RESET )
    {
        debug_print_plain(" POR");
    }
    if ( ( devState & ADI_ADRV9001_STATE_ANA_INITIALIZED ) > 0 )
    {
        debug_print_plain(" ANA");
    }
    if ( ( devState & ADI_ADRV9001_STATE_DIG_INITIALIZED ) > 0 )
    {
        debug_print_plain(" DIG");
    }
    if ( ( devState & ADI_ADRV9001_STATE_STREAM_LOADED ) > 0 )
    {
        debug_print_plain(" STRLOAD");
    }
    if ( ( devState & ADI_ADRV9001_STATE_ARM_DEBUG_LOADED ) > 0 )
    {
        debug_print_plain(" ARMDBG");
    }
    if ( ( devState & ADI_ADRV9001_STATE_ARM_LOADED ) > 0 )
    {
        debug_print_plain(" ARMLOAD");
    }
    if ( ( devState & ADI_ADRV9001_STATE_INITCALS_RUN ) > 0 )
    {
        debug_print_plain(" INITCALS");
    }
    if ( ( devState & ADI_ADRV9001_STATE_PRIMED ) > 0 )
    {
        debug_print_plain(" PRIMED");
    }
    if ( ( devState & ADI_ADRV9001_STATE_IDLE ) > 0 )
    {
        debug_print_plain(" IDLE");
    }
    if ( ( devState & ADI_ADRV9001_STATE_STANDBY ) > 0 )
    {
        debug_print_plain(" STDBY");
    }
    debug_print_plain("\n");
}


/**************************************************************************************************/
static void setup_adrv9001_device( uint8_t card,
                                   uint8_t cs )
{
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    epiq_adrv9002_dev_t *device_hal = get_adrv9001_device_hal(card);

    /* set up the device HAL structure */
    device_hal->card = card;
    device_hal->cs = cs;

    /* copy over the device defaults, then assign the devHalInfo */
    memcpy(device, &navassa_device_default, sizeof(*device));
    device->common.devHalInfo = (void *)device_hal;
}


/**************************************************************************************************/
static int32_t save_config( uint8_t card )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    struct radio_config *p_conf = &(radio_conf[card]);

    skiq_rx_hdl_t other_hdl = is_requested_rx_lo_source_active( card, skiq_rx_hdl_A2 ) ? skiq_rx_hdl_A2 : skiq_rx_hdl_B1;
    if (radio_conf[card].lo.lo_selection_pending)
    {
        /* We want to save the carrier config for active handles, and the pending flag indicates
           a change is incoming.  ie we're going from A2 -> B1 or B1->A2

           As a result, is_requested_rx_lo_source_active() wont reflect
           the truth, as the profile hasn't been applied. So the "other handle"
           we want to save should be swapped from what is_requested_rx_lo_source_active() says.
        */ 
        other_hdl = (other_hdl == skiq_rx_hdl_A2) ? skiq_rx_hdl_B1 : skiq_rx_hdl_A2;
    }
    
    DEBUG_PRINT_CHANNEL_STATES( card );

    if ( status == 0 )
    {
        debug_print("Saving RX carrier info for ADI_CHANNEL_1 on card %" PRIu8 "\n", card );
        status = adi_adrv9001_Radio_Carrier_Inspect( device, ADI_RX, ADI_CHANNEL_1,
                                                     get_rx_carrier( card, skiq_rx_hdl_A1 ) );
        ADI_HANDLE_ERROR(device, status);
    }

    if ( status == 0 )
    {
        debug_print("Saving RX carrier info for ADI_CHANNEL_2 on card %" PRIu8 "\n", card );
        status = adi_adrv9001_Radio_Carrier_Inspect( device, ADI_RX, ADI_CHANNEL_2,
                                                     get_rx_carrier( card, other_hdl ) );
        ADI_HANDLE_ERROR(device, status);
    }

    if ( status == 0 )
    {
        debug_print("Saving TX carrier info for ADI_CHANNEL_1 on card %" PRIu8 "\n", card );
        status = adi_adrv9001_Radio_Carrier_Inspect( device, ADI_TX, ADI_CHANNEL_1,
                                                     get_tx_carrier( card, skiq_tx_hdl_A1 ) );
        ADI_HANDLE_ERROR(device, status);
    }

    if ( status == 0 )
    {
        debug_print("Saving TX carrier info for ADI_CHANNEL_2 on card %" PRIu8 "\n", card );
        status = adi_adrv9001_Radio_Carrier_Inspect( device, ADI_TX, ADI_CHANNEL_2,
                                                     get_tx_carrier( card, skiq_tx_hdl_B1 ) );
        ADI_HANDLE_ERROR(device, status);
    }

    if ( status == 0 )
    {
        debug_print("Saving transmit test tone configuration on card %" PRIu8 "\n", card);
        status = inspect_tx_test_tone( card, &p_conf->test_tone_enabled );
    }

    return status;
}


/**************************************************************************************************/
static int32_t restore_carrier( uint8_t card,
                                adi_common_Port_e port,
                                adi_common_ChannelNumber_e channel,
                                adi_adrv9001_Carrier_t *p_carrier )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);

    debug_print("Restoring %s carrier info for ADI_CHANNEL_%s on card %" PRIu8 "\n",
                (port == ADI_RX) ? "RX" : "TX", (channel == ADI_CHANNEL_1) ? "1" : "2",
                card );
    if ( p_carrier->carrierFrequency_Hz == 0 )
    {
        p_carrier->carrierFrequency_Hz = NAV_CARRIER_FREQ_HZ_DEFAULT;
    }
    status = adi_adrv9001_Radio_Carrier_Configure( device, port, channel, p_carrier );
    ADI_HANDLE_ERROR( device, status );

    return status;
}


/**************************************************************************************************/
static int32_t apply_rx_fir_filters( uint8_t card )
{
    int32_t status = 0;
    skiq_rx_hdl_t hdl;
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);

    for ( hdl = skiq_rx_hdl_A1; ( hdl <= skiq_rx_hdl_B1 ) && ( status == 0 ); hdl++ )
    {
        uint32_t bw_hz = radio_conf[card].rx[hdl].bandwidth_hz;
        uint32_t sr_hz = radio_conf[card].rx[hdl].samp_rate_hz;

        if ( bw_hz != BW_HZ_INVALID )
        {
            float actual_percentage_bw;

            status = epiq_adrv9002_set_Rx_Passband( device, skiq_rx_hdl_to_mask_channel[hdl],
                                                    (100.0f * bw_hz) / sr_hz,
                                                    &actual_percentage_bw );
            ADI_HANDLE_ERROR( device, status );
            if ( status == 0 )
            {
                float actual_bandwidth_hz_f = (sr_hz * actual_percentage_bw) / 100.0f;

                actual_bandwidth_hz_f = roundf(actual_bandwidth_hz_f);
                radio_conf[card].rx[hdl].actual_bandwidth_hz = (uint32_t)(actual_bandwidth_hz_f);
            }
        }
    }

    return status;
}


/**************************************************************************************************/
static int32_t restore_config( uint8_t card )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    struct radio_config *p_conf = &(radio_conf[card]);
    skiq_part_t part = _skiq_get_part( card );
    skiq_rx_hdl_t other_hdl = is_requested_rx_lo_source_active( card, skiq_rx_hdl_A2 ) ?  skiq_rx_hdl_A2 : skiq_rx_hdl_B1;
    skiq_rx_hdl_t hdl_array[ADI_ADRV9001_NUM_CHANNELS]  = INIT_ARRAY(ADI_ADRV9001_NUM_CHANNELS, skiq_rx_hdl_end);
    hdl_array[0] = skiq_rx_hdl_A1; hdl_array[1] = other_hdl;

    DEBUG_PRINT_CHANNEL_STATES( card );

    if ( status == 0 )
    {
        status = restore_carrier( card, ADI_RX, ADI_CHANNEL_1, get_rx_carrier( card, skiq_rx_hdl_A1 ) );
        if ( status == 0 )
        {
            p_conf->rx[skiq_rx_hdl_A1].last_cal_freq = UINT64_MAX;
        }
    }

    if ( status == 0 )
    {
        status = restore_carrier( card, ADI_RX, ADI_CHANNEL_2, get_rx_carrier( card, other_hdl ) );
        if ( status == 0 )
        {
            p_conf->rx[other_hdl].last_cal_freq = UINT64_MAX;
        }
    }

    if ( status == 0 )
    {
        status = restore_carrier( card, ADI_TX, ADI_CHANNEL_1, &(p_conf->tx[0].carrier) );
        if ( status == 0 )
        {
            p_conf->tx[skiq_tx_hdl_A1].last_cal_freq = UINT64_MAX;
        }
    }

    if ( status == 0 )
    {
        status = restore_carrier( card, ADI_TX, ADI_CHANNEL_2, &(p_conf->tx[1].carrier) );
        if ( status == 0 )
        {
            p_conf->tx[skiq_tx_hdl_B1].last_cal_freq = UINT64_MAX;
        }
    }

    if ( status == 0 )
    {
        status = config_tx_test_tone( card, p_conf->test_tone_enabled );
    }

    if ( status == 0 )
    {
        status = write_control_output_config( card, p_conf->rfic_control_output_dest );

        /* if the RFIC control output destination has not been set, no harm, no foul */
        if ( status == -ENOENT )
        {
            status = 0;
        }
    }

    if ( status == 0 )
    {
        uint8_t hdl_idx=0;
        for ( hdl_idx = 0; ( hdl_idx < ADI_ADRV9001_NUM_CHANNELS ) && ( status == 0 ); hdl_idx++ )
        {
            skiq_rx_hdl_t hdl = hdl_array[hdl_idx];
            if ( p_conf->rx[hdl].gain_mode == ADI_ADRV9001_RX_GAIN_CONTROL_MODE_SPI )
            {
                uint8_t gain_index = p_conf->rx[hdl].gain_index;

                if ( gain_index > 0 )
                {
                    debug_print("Restoring RX gain index on %s to %u\n", rx_hdl_cstr(hdl), gain_index);
                    status = adi_adrv9001_Rx_Gain_Set( device, skiq_rx_hdl_to_channel[hdl], gain_index );
                    ADI_HANDLE_ERROR( device, status );
                }
            }
            else if ( p_conf->rx[hdl].gain_mode == ADI_ADRV9001_RX_GAIN_CONTROL_MODE_AUTO )
            {
                debug_print("Restoring RX gain control mode on %s to 'auto' on card %" PRIu8 "\n",
                            rx_hdl_cstr(hdl), card);
                status = adi_adrv9001_Rx_GainControl_Mode_Set(device, skiq_rx_hdl_to_channel[hdl],
                                                              p_conf->rx[hdl].gain_mode);
                ADI_HANDLE_ERROR( device, status );
            }
            else
            {
                status = -EINVAL;
                skiq_error("Unhandled value when restoring Rx gain control mode on card %" PRIu8
                           "\n", card);
            }
            if (status == 0)
            {
                status = write_rx_cal_mask(card,hdl,p_conf->rx[hdl].cal_mask);
            }
        }
    }

    if ( status == 0 )
    {
        skiq_tx_hdl_t hdl;

        for ( hdl = skiq_tx_hdl_A1; ( hdl <= skiq_tx_hdl_B1 ) && ( status == 0 ); hdl++ )
        {
            uint16_t atten_mdB = radio_conf[card].tx[hdl].atten_mdB;

            if ( atten_mdB != UINT16_MAX )
            {
                debug_print("Restoring TX attenuation on %s to %u mdB\n", tx_hdl_cstr(hdl), atten_mdB);
                status = adi_adrv9001_Tx_Attenuation_Set( device, skiq_tx_hdl_to_channel[hdl], atten_mdB );
                ADI_HANDLE_ERROR( device, status );
            }
        }
    }

    if ( status == 0 )
    {
        status = apply_rx_fir_filters( card );
    }

    /* configure the AUX ADC to allow for temperature sensing */
    if ( ( status == 0 ) && ( part == skiq_nv100 ) )
    {
        status = adi_adrv9001_AuxAdc_Configure( device, ADI_ADRV9001_AUXADC_THERM, ENABLE );
        ADI_HANDLE_ERROR( device, status );
    }

    return status;
}


/**************************************************************************************************/
/* This function drives SW_RX_INIT high then low to reset and re-sync both Rx LSSI interfaces in the
 * FPGA */
static int32_t reinit_rx_lssi( uint8_t card )
{
    int32_t status = 0;
    const uint32_t reg = FPGA_REG_ADC_DAC_PRBS_TEST_CTRL;
    uint32_t value;

    status = sidekiq_fpga_reg_read( card, reg, &value );
    if ( status == 0 )
    {
        RBF_SET( value, 1, SW_RX_INIT );
        status = sidekiq_fpga_reg_write_and_verify( card, reg, value );
    }

    if ( status == 0 )
    {
        /* NOTE: This bit is implemented in the FPGA as a "rising edge detect", so no delay is
         * necessary between setting this bit high then low */
        RBF_SET( value, 0, SW_RX_INIT );
        status = sidekiq_fpga_reg_write_and_verify( card, reg, value );
    }

    return status;
}


/**************************************************************************************************/
static void get_profile_rate_and_bandwidth( uint16_t profile_index,
                                            bool look_for_tx,
                                            uint32_t *p_profile_rate,
                                            uint32_t *p_profile_bw )
{
    if ( look_for_tx )
    {
        *p_profile_rate = epiq_adrv9002_profiles[profile_index]->tx.txProfile[0].txInputRate_Hz;
        *p_profile_bw = epiq_adrv9002_profiles[profile_index]->tx.txProfile[0].primarySigBandwidth_Hz;
    }
    else
    {
        *p_profile_rate = epiq_adrv9002_profiles[profile_index]->rx.rxChannelCfg[0].profile.rxOutputRate_Hz;
        *p_profile_bw = epiq_adrv9002_profiles[profile_index]->rx.rxChannelCfg[0].profile.primarySigBandwidth_Hz;
    }
}


/**************************************************************************************************/
static int32_t find_profile( uint8_t card,
                             uint32_t rate,
                             uint32_t bandwidth,
                             bool look_for_tx,
                             adi_adrv9001_Init_t **pp_found_profile )
{
    int32_t status = 0;
    adi_adrv9001_Init_t *p_profile = NULL;
    uint16_t i;

    /* find a profile that matches (fs,bw) exactly */
    for (i = 0; (i < ADRV9002_NR_PROFILES) && (p_profile == NULL); i++)
    {
        uint32_t profile_rate, profile_bw;

        get_profile_rate_and_bandwidth( i, look_for_tx, &profile_rate, &profile_bw );
        if ( ( rate == profile_rate ) && ( bandwidth == profile_bw ) )
        {
            p_profile = epiq_adrv9002_profiles[i];
            debug_print("Profile #%u (fs=%u,bw=%u) MATCHED requested on card %" PRIu8 "\n",
                        i, profile_rate, profile_bw, card);
        }
        else
        {
            debug_print("Profile #%u (fs=%u,bw=%u) did not match requested on card %" PRIu8 "\n",
                        i, profile_rate, profile_bw, card);
        }
    }

    /* find a profile that matches (fs) exactly, but bw is the minimum over requested */
    if ( p_profile == NULL )
    {
        uint32_t minimum_over = UINT32_MAX;

        debug_print("No exact match, looking for a profile with the closest bandwidth above requested\n");
        for (i = 0; i < ADRV9002_NR_PROFILES; i++)
        {
            uint32_t profile_rate, profile_bw;

            get_profile_rate_and_bandwidth( i, look_for_tx, &profile_rate, &profile_bw );
            if ( ( rate == profile_rate ) && ( bandwidth < profile_bw ) )
            {
                if ( profile_bw < minimum_over )
                {
                    minimum_over = profile_bw;
                    p_profile = epiq_adrv9002_profiles[i];
                    debug_print("Profile #%u (fs=%u,bw=%u) MATCHED fs requested, but bw > %u on card %" PRIu8 "\n",
                                i, profile_rate, profile_bw, bandwidth, card);
                }
            }
            else
            {
                debug_print("Profile #%u (fs=%u,bw=%u) did not match requested on card %" PRIu8 "\n",
                            i, profile_rate, profile_bw, card);
            }
        }
    }

    /* find a profile that matches (fs) exactly, but bw is the maximum under requested */
    if ( p_profile == NULL )
    {
        uint32_t maximum_over = 0;

        debug_print("No exact match, looking for a profile with the closest bandwidth below requested\n");
        for (i = 0; i < ADRV9002_NR_PROFILES; i++)
        {
            uint32_t profile_rate, profile_bw;

            get_profile_rate_and_bandwidth( i, look_for_tx, &profile_rate, &profile_bw );
            if ( ( rate == profile_rate ) && ( bandwidth > profile_bw ) )
            {
                if ( profile_bw > maximum_over )
                {
                    maximum_over = profile_bw;
                    p_profile = epiq_adrv9002_profiles[i];
                    debug_print("Profile #%u (fs=%u,bw=%u) MATCHED fs requested, but bw < %u on card %" PRIu8 "\n",
                                i, profile_rate, profile_bw, bandwidth, card);
                }
            }
            else
            {
                debug_print("Profile #%u (fs=%u,bw=%u) did not match requested on card %" PRIu8 "\n",
                            i, profile_rate, profile_bw, card);
            }
        }
    }

    if ( p_profile == NULL )
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        *pp_found_profile = p_profile;
    }

    return status;
}


/**************************************************************************************************/
static int32_t find_rx_profile( uint8_t card,
                                uint32_t rate,
                                uint32_t bandwidth,
                                adi_adrv9001_Init_t **pp_found_profile )
{
    return find_profile( card, rate, bandwidth, LOOK_FOR_RX, pp_found_profile );
}


/**************************************************************************************************/
static int32_t find_tx_profile( uint8_t card,
                                uint32_t rate,
                                uint32_t bandwidth,
                                adi_adrv9001_Init_t **pp_found_profile )
{
    return find_profile( card, rate, bandwidth, LOOK_FOR_TX, pp_found_profile );
}


/**************************************************************************************************/
static int32_t apply_profile( uint8_t card,
                              adi_adrv9001_Init_t *profile )
{
    int32_t status = 0;
    uint32_t channelMask = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    adi_adrv9001_Init_t *card_profile = &(current_profile[card]);

    /* Check to see if the requested profile matches what the RFIC is presently configured and
       that there's no LO selection pending  If so, we can skip re-applying the profile
       for no effective reason */

    if ( ( radio_conf[card].reference_profile == profile ) && 
         ( radio_conf[card].lo.lo_selection_pending == false) )
    {
        debug_print("Current profile (%p) is the same as requested (%p) on card %" PRIu8 "\n",
                    radio_conf[card].reference_profile, profile, card);

        /* The reference profile may be the same, but the FIR filter configuration might be
         * different, so apply the filters */
        status = apply_rx_fir_filters( card );

        return status;
    }

    /* Copy the source profile to the card's current profile to make location modifications to a few
     * fields */
    memcpy( card_profile, profile, sizeof(adi_adrv9001_Init_t) );

    /* @todo make this part of profile selection.  It controls whether the second RX handle is
     * considered A2 or B1.  A2 would be LO1 and B1 would be LO2.
     *
     * @attention once this becomes part of profile selection, revisit the .reference_profile check
     * above since a different LO source may make applying a profile necessary
     */

    debug_print("Configuring RFIC LO selection: rx1:%" PRIu8" rx2:%" PRIu8" tx1:%" PRIu8" tx2:%" PRIu8" on card %" PRIu8 "\n",
                radio_conf[card].lo.rx1LoSelect, radio_conf[card].lo.rx2LoSelect,
                radio_conf[card].lo.tx1LoSelect, radio_conf[card].lo.tx1LoSelect,
                card );

    card_profile->clocks.rx1LoSelect = radio_conf[card].lo.rx1LoSelect;
    card_profile->clocks.tx1LoSelect = radio_conf[card].lo.tx1LoSelect;
    card_profile->clocks.rx2LoSelect = radio_conf[card].lo.rx2LoSelect;
    card_profile->clocks.tx2LoSelect = radio_conf[card].lo.tx2LoSelect;

    /* Capture the device's configuration prior to shutting down so that it can be restored after
     * bring-up */
    if ( radio_conf[card].active )
    {
        status = save_config( card );
    }

    if ( status == 0 )
    {
        status = epiq_adrv9002_shutdown( device );
        ADI_HANDLE_ERROR( device, status );
    }

    if ( status == 0 )
    {
        uint32_t value;

        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
        RBF_SET(value, 0, RX1_EN);
        RBF_SET(value, 0, RX2_EN);
        RBF_SET(value, 0, TX1_EN);
        RBF_SET(value, 0, TX2_EN);
        status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
    }

    if ( status == 0 )
    {
        /* clear the device state info */
        memset( &(device->devStateInfo), 0, sizeof(device->devStateInfo) );

        status = epiq_adrv9002_initialize( device, card_profile );
        ADI_HANDLE_ERROR( device, status );
    }

    if ( status == 0 )
    {
        debug_print("Re-initializing Rx LSSI on card %" PRIu8 "\n", card);
        status = reinit_rx_lssi( card );
    }

    if ( status == 0 )
    {
        debug_print("Re-initializing Tx LSSI on card %" PRIu8 "\n", card);
        status = sidekiq_fpga_reg_RMW( card, 1, SW_TX_INIT, FPGA_REG_ADC_DAC_PRBS_TEST_CTRL );
        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_RMW( card, 0, SW_TX_INIT, FPGA_REG_ADC_DAC_PRBS_TEST_CTRL );
        }
    }

    /*
      @note The rxInitChannelMask in most profiles include some ancillary channels (Observation),
      but the epiq_adrv9002_*() functions know to mask those out from this requested mask.
     */
    channelMask = card_profile->rx.rxInitChannelMask | card_profile->tx.txInitChannelMask;

    if ( status == 0 )
    {
        status = epiq_adrv9002_calibrate( device, channelMask );
        ADI_HANDLE_ERROR( device, status );
    }

    if ( status == 0 )
    {
        status = epiq_adrv9002_configure( device, channelMask );
        ADI_HANDLE_ERROR( device, status );
    }

    DEBUG_PRINT_CHANNEL_STATES( card );

    /* Restore the device's configuration */
    if ( ( status == 0 ) && ( radio_conf[card].active ) )
    {
        status = restore_config( card );
    }

    if ( status == 0 )
    {
        status = epiq_adrv9002_prime( device, channelMask );
        ADI_HANDLE_ERROR( device, status );
    }

    if ( status == 0 )
    {
        status = epiq_adrv9002_set_ChannelEnableMode( device, channelMask, ADI_ADRV9001_PIN_MODE );
        ADI_HANDLE_ERROR( device, status );
    }

    /* Restore the Rx channel's enable/disable state */
    if ( status == 0 )
    {
        skiq_rx_hdl_t rx_hdl = skiq_rx_hdl_A1;

        for ( rx_hdl = skiq_rx_hdl_A1; ( rx_hdl <= skiq_rx_hdl_B1 ) && ( status == 0 ); rx_hdl++ )
        {
            if ( is_rx_chan_enabled( card, rx_hdl ) &&
                 ( ( channelMask & skiq_rx_hdl_to_mask_channel[rx_hdl] ) != 0 ) )
            {
                status = enable_rx_chan( card, rx_hdl, ENABLE );
            }
            else
            {
                mark_rx_chan_state( card, rx_hdl, DISABLE );
            }
        }
    }

    /* Restore the Tx channel's enable/disable state */
    if ( status == 0 )
    {
        skiq_tx_hdl_t tx_hdl;

        for ( tx_hdl = skiq_tx_hdl_A1; ( tx_hdl <= skiq_tx_hdl_B1 ) && ( status == 0 ); tx_hdl++ )
        {
            if ( is_tx_chan_enabled( card, tx_hdl ) &&
                 ( ( channelMask & skiq_tx_hdl_to_mask_channel[tx_hdl] ) != 0 ) )
            {
                status = enable_tx_chan( card, tx_hdl, ENABLE );
            }
            else
            {
                mark_tx_chan_state( card, tx_hdl, DISABLE );
            }
        }
    }

    if ( status == 0 )
    {
        radio_conf[card].reference_profile = profile;
    }

    return status;
}


/**************************************************************************************************/
static int32_t navassa_bringup( uint8_t card )
{
    adi_adrv9001_Init_t *found_profile = NULL;
    adi_adrv9001_Device_t *device = NULL;
    uint32_t rate, bandwidth;
    skiq_rx_hdl_t hdl = skiq_rx_hdl_A1;
    int32_t status;

    setup_adrv9001_device( card, 0 /* CS */ );
    device = get_adrv9001_device( card );

    /* binds all of the HAL functionality to `device` */
    status = epiq_adrv9002_create( device );

    if ( status == 0 )
    {
        rate = NAV_DEFAULT_SAMPLE_RATE;
        bandwidth = NAV_DEFAULT_BANDWIDTH;
        status = find_rx_profile( card, rate, bandwidth, &found_profile );
    }

    if ( status == 0 )
    {
        uint32_t sr_hz, bw_hz;

        sr_hz = radio_conf[card].rx[hdl].samp_rate_hz;
        bw_hz = radio_conf[card].rx[hdl].bandwidth_hz;
        radio_conf[card].rx[hdl].samp_rate_hz = rate;
        radio_conf[card].rx[hdl].bandwidth_hz = MIN(rate, bandwidth);

        status = apply_profile( card, found_profile );
        if ( status != 0 )
        {
            radio_conf[card].rx[hdl].samp_rate_hz = sr_hz;
            radio_conf[card].rx[hdl].bandwidth_hz = bw_hz;
        }
    }

    if ( status == 0 )
    {
        debug_print_device_state( card );
    }

    if ( status == 0 )
    {
        DEBUG_PRINT_CHANNEL_STATES( card );
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief
*/
static void navassa_display_version_info( void )
{
    TRACE_ENTER;

    skiq_info("RFIC API v%s\n", ADI_ADRV9001_CURRENT_VERSION);

    TRACE_EXIT;

    return;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t write_rx_freq( uint8_t card,
                              skiq_rx_hdl_t hdl,
                              uint64_t freq )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    adi_common_ChannelNumber_e adi_channel = skiq_rx_hdl_to_channel[hdl];

    /**
       1. The channel needs to be transitioned to the CALIBRATED state since it is always either in
       PRIMED or RF_ENABLED
    */
    if ( status == 0 )
    {
        status = move_rx_chan_to_calibrated( card, adi_channel );
    }

    /**
       2. Once device is in CALIBRATED state, Set the LO frequency by calling
       adi_adrv9001_Radio_Carrier_Configure().
    */
    if ( status == 0 )
    {
        adi_adrv9001_Carrier_t *p_carrier = get_rx_carrier( card, hdl );

        p_carrier->carrierFrequency_Hz = freq;

        /* To match the behavior in the NV100 RFE, the low frequency in the range is not included
           whereas the high frequency in included. */
        if (freq <= RX_RF_PORT_B_MAX_FREQ_HZ)
        {
            p_carrier->manualRxport = ADI_ADRV9001_RX_B;
        }
        else
        {
            p_carrier->manualRxport = ADI_ADRV9001_RX_A;
        }

        status = adi_adrv9001_Radio_Carrier_Configure( device, ADI_RX, adi_channel, p_carrier );
        ADI_HANDLE_ERROR( device, status );
    }

    /**
       3. Run Rx calibration if needed, based on Rx cal_mode, last calibrated frequency, and current
       frequency
    */
    if ( status == 0 )
    {
        status = run_rx_cal_if_needed( card, hdl, radio_conf[card].rx[hdl].cal_mode, freq,
                                       &(radio_conf[card].rx[hdl].last_cal_freq) );
    }

    /**
       4. Return channel back to its previous state
    */
    if ( status == 0 )
    {
        status = restore_rx_chan_state_from_calibrated( card, hdl );
    }

    DEBUG_PRINT_CHANNEL_STATES( card );

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_freq( rf_id_t *p_id,
                                      uint64_t freq,
                                      double *p_act_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        /**
           1. Check that the RFIC has been initialized
        */
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        /**
           2. Verify the specified handle is valid and supported
        */
        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        /**
           3. Verify the handle's LO source is active
        */
        if ( status == 0 )
        {
            if (is_requested_rx_lo_source_active( card, hdl ) == false)
            {
                if(is_rfic_streaming(card))
                {
                    skiq_error("Unable to write rx_freq for %s on card %" PRIu8 "." \
                                "LO source cannot be changed while streaming\n", rx_hdl_cstr( hdl ), card);
                    status = -EPERM;
                }

                if ( status == 0 )
                /* Issue a warning about a penalty incurred for writing rx_freq before setting rate.*/
                {
                    skiq_warning("Requested handle uses unconfigured LO mapping. Applying LO profile and performing recalibration.\n" );
                    radio_conf[card].lo.lo_selection_pending = true;
                    radio_conf[card].lo.rx2LoSelect = (hdl == skiq_rx_hdl_A2) ? ADI_ADRV9001_LOSEL_LO1: ADI_ADRV9001_LOSEL_LO2 ;
                    status = apply_profile(card, &(current_profile[card]));
                }
            }
        }

        /**
           4. Check to see that the requested frequency is different from the configured frequency
        */
        if ( status == 0 )
        {
            adi_adrv9001_Carrier_t *p_carrier = get_rx_carrier( card, hdl );

            if ( p_carrier->carrierFrequency_Hz != freq )
            {
                status = write_rx_freq( card, hdl, freq );
            }
        }

        /* The actual frequency is the specified frequency as far as ADI admits */
        if ( status == 0 )
        {
            *p_act_freq = freq;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t write_tx_freq( uint8_t card,
                              skiq_tx_hdl_t hdl,
                              uint64_t freq )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    adi_common_ChannelNumber_e adi_channel = skiq_tx_hdl_to_channel[hdl];

    /**
       1. The channel needs to be transitioned to the CALIBRATED state since it is always either
       in PRIMED or RF_ENABLED
    */
    if ( status == 0 )
    {
        status = move_tx_chan_to_calibrated( card, adi_channel );
    }

    if ( status == 0 )
    {
        DEBUG_PRINT_CHANNEL_STATES( card );
    }

    /**
       2. Once device is in CALIBRATED state, Set the LO frequency by calling
       adi_adrv9001_Radio_Carrier_Configure()
    */
    if ( status == 0 )
    {
        adi_adrv9001_Carrier_t *p_carrier = get_tx_carrier(card, hdl);

        p_carrier->carrierFrequency_Hz = freq;
        status = adi_adrv9001_Radio_Carrier_Configure( device, ADI_TX, adi_channel, p_carrier );
        ADI_HANDLE_ERROR( device, status );
    }

    /**
       3. Run Tx calibration if needed, based on Tx cal_mode, last calibrated frequency, and current
       frequency
    */
    if ( status == 0 )
    {
        status = run_tx_quadcal_if_needed( card, hdl, radio_conf[card].tx[hdl].cal_mode, freq,
                                           &(radio_conf[card].tx[hdl].last_cal_freq) );
    }

    /**
       4. Return channel back to its previous state
    */
    if ( status == 0 )
    {
        status = restore_tx_chan_state_from_calibrated( card, hdl );
    }

    DEBUG_PRINT_CHANNEL_STATES( card );

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_tx_freq( rf_id_t *p_id,
                                      uint64_t freq,
                                      double *p_act_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        /**
           1. Check that the RFIC has been initialized
        */
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        /**
           2. Check to see that the requested frequency is different from the configured frequency
        */
        if ( status == 0 )
        {
            adi_adrv9001_Carrier_t *p_carrier = get_tx_carrier( card, hdl );

            if ( p_carrier->carrierFrequency_Hz != freq )
            {
                status = write_tx_freq( card, hdl, freq );
            }
        }

        /* The actual frequency is the specified frequency as far as ADI admits */
        if ( status == 0 )
        {
            *p_act_freq = freq;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}



/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_spectrum_invert( rf_id_t *p_id,
                                                 bool invert )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    /* ADRV9001 has no support for spectral inversion */
    if ( invert )
    {
        status = -ENOTSUP;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_tx_spectrum_invert( rf_id_t *p_id,
                                                 bool invert )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    /* ADRV9001 has no support for spectral inversion */
    if ( invert )
    {
        status = -ENOTSUP;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_tx_attenuation( rf_id_t *p_id,
                                             uint16_t atten_qdB )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );
        uint16_t atten_mdB;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            if ( ( atten_qdB < NAV_MIN_TX_ATTEN_QDB ) || ( atten_qdB > NAV_MAX_TX_ATTEN_QDB ) )
            {
                status = -EINVAL;
            }
        }

        if ( status == 0 )
        {
            /* Convert quarter dB to milli dB */
            atten_mdB = (uint16_t)((((double) atten_qdB) / 4.0) * 1000);

            /* The resolution of adi_adrv9001_Tx_Attenuation_Set() is only 0.05dB, so truncate if
             * necessary */
            if ( ( atten_mdB % 50 ) != 0 )
            {
                atten_mdB -= ( atten_mdB % 50 );
                debug_print("Truncating %u qdB to %u mdB\n", atten_qdB, atten_mdB);
            }
            else
            {
                debug_print("Converting %u qdB to %u mdB\n", atten_qdB, atten_mdB);
            }

            status = adi_adrv9001_Tx_Attenuation_Set( device, skiq_tx_hdl_to_channel[hdl], atten_mdB );
            ADI_HANDLE_ERROR( device, status );
        }

        if ( status == 0 )
        {
            radio_conf[card].tx[hdl].atten_mdB = atten_mdB;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_attenuation( rf_id_t *p_id,
                                            uint16_t *p_atten_qdB )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        uint16_t attenuation_mdB;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            attenuation_mdB = radio_conf[card].tx[hdl].atten_mdB;
            *p_atten_qdB = (uint16_t)((((double)attenuation_mdB) / 1000) * 4.0);
            debug_print("Converting %u mdB to %u qdB\n", attenuation_mdB, *p_atten_qdB);
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_attenuation_range( rf_id_t *p_id,
                                                  uint16_t *p_max,
                                                  uint16_t *p_min )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    *p_max = NAV_MAX_TX_ATTEN_QDB;
    *p_min = NAV_MIN_TX_ATTEN_QDB;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_gain_mode( rf_id_t *p_id,
                                           skiq_rx_gain_t gain_mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );
        adi_adrv9001_RxGainControlMode_e rfic_gain_mode;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        /* translate from skiq_rx_gain_t to adi_adrv9001_RxGainControlMode_e */
        if ( status == 0 )
        {
            switch ( gain_mode )
            {
                case skiq_rx_gain_manual:
                    rfic_gain_mode = ADI_ADRV9001_RX_GAIN_CONTROL_MODE_SPI;
                    break;

                case skiq_rx_gain_auto:
                    rfic_gain_mode = ADI_ADRV9001_RX_GAIN_CONTROL_MODE_AUTO;
                    break;

                default:
                    status = -ENOTSUP;
                    break;
            }
        }

        /* Configure RFIC for specified rfic_gain_mode */
        if ( status == 0 )
        {
            status = adi_adrv9001_Rx_GainControl_Mode_Set(device, skiq_rx_hdl_to_channel[hdl],
                                                          rfic_gain_mode);
            ADI_HANDLE_ERROR( device, status );
        }

        /* If specified rfic_gain_mode is manual, apply the gain_index stored in radio_conf[] */
        if ( ( status == 0 ) && ( rfic_gain_mode == ADI_ADRV9001_RX_GAIN_CONTROL_MODE_SPI ) )
        {
            uint8_t gain_index = radio_conf[card].rx[hdl].gain_index;

            if ( gain_index > 0 )
            {
                status = adi_adrv9001_Rx_Gain_Set( device, skiq_rx_hdl_to_channel[hdl], gain_index );
                ADI_HANDLE_ERROR( device, status );
            }
        }

        /* Cache the RX gain mode setting */
        if ( status == 0 )
        {
            radio_conf[card].rx[hdl].gain_mode = rfic_gain_mode;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_gain_mode( rf_id_t *p_id,
                                          skiq_rx_gain_t *p_gain_mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            switch ( radio_conf[card].rx[hdl].gain_mode )
            {
                case ADI_ADRV9001_RX_GAIN_CONTROL_MODE_PIN:
                case ADI_ADRV9001_RX_GAIN_CONTROL_MODE_SPI:
                    *p_gain_mode = skiq_rx_gain_manual;
                    break;

                case ADI_ADRV9001_RX_GAIN_CONTROL_MODE_AUTO:
                    *p_gain_mode = skiq_rx_gain_auto;
                    break;

                default:
                    status = -EPROTO;
                    break;
            }
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_gain_range( rf_id_t *p_id,
                                           uint8_t *p_gain_max,
                                           uint8_t *p_gain_min )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    /* The RX gain range is pulled from the device pointer, so the radio must be active prior to
     * querying the gain range */
    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            *p_gain_min = NAV_MIN_RX_GAIN_INDEX;
            *p_gain_max = NAV_MAX_RX_GAIN_INDEX;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_gain( rf_id_t *p_id,
                                      uint8_t gain )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    skiq_info("Requesting RX gain on %s to be index %u\n", rx_hdl_cstr(hdl), gain);

    RADIO_LOCK(card);
    {
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            if ( ( gain < NAV_MIN_RX_GAIN_INDEX ) || ( gain > NAV_MAX_RX_GAIN_INDEX ) )
            {
                status = -EINVAL;
            }
        }

        /* Writing a manual gain index is only allowed when in SPI gain control mode */
        if ( ( status == 0 ) &&
             ( radio_conf[card].rx[hdl].gain_mode != ADI_ADRV9001_RX_GAIN_CONTROL_MODE_SPI ) )
        {
            status = -EPERM;
        }

        if ( status == 0 )
        {
            skiq_info("Setting RX gain on %s to index %u\n", rx_hdl_cstr(hdl), gain);
            status = adi_adrv9001_Rx_Gain_Set( device, skiq_rx_hdl_to_channel[hdl], gain );
            ADI_HANDLE_ERROR( device, status );
        }

        if ( status == 0 )
        {
            radio_conf[card].rx[hdl].gain_index = gain;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_gain( rf_id_t *p_id,
                                     uint8_t *p_gain )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        /* Reading a manual gain index only makes sense when in SPI gain control mode */
        if ( ( status == 0 ) &&
             ( radio_conf[card].rx[hdl].gain_mode != ADI_ADRV9001_RX_GAIN_CONTROL_MODE_SPI ) )
        {
            status = -EPERM;
        }

        if ( status == 0 )
        {
            *p_gain = radio_conf[card].rx[hdl].gain_index;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}



/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_adc_resolution( uint8_t *p_adc_res )
{
    int32_t status = 0;

    TRACE_ENTER;

    *p_adc_res = NAV_ADC_RESOLUTION;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_dac_resolution( uint8_t *p_dac_res )
{
    int32_t status = 0;

    TRACE_ENTER;

    *p_dac_res = NAV_DAC_RESOLUTION;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t read_warp_voltage_extended_range( uint8_t card,
                                                 uint32_t *p_warp_voltage )
{
    int32_t status = 0;
    bool has_gpsdo = false, is_gpsdo_running = false;
    int32_t gpsdo_status=0;

    /*
      Check to see if the card has a GPSDO module in the FPGA.  If it does, check to see if the
      algorithm is running.
     */
    status = gpsdo_fpga_has_module( card, &has_gpsdo, &gpsdo_status );
    if ( status == 0 )
    {
        /* entry assumption: has_gpsdo is valid */

        if ( has_gpsdo )
        {
            status = gpsdo_fpga_is_enabled( card, &is_gpsdo_running );
        }
    }

    /*
      Here's where the "read extended warp voltage" functionality is dispatched to either the GPSDO
      interface, the SiT3536, or indicated as not implemented (-ENOSYS).
     */
    if ( status == 0 )
    {
        /* entry assumption: is_gpsdo_running is valid because:
             1. status is 0 and has_gpsdo is true
             2. is_gpsdo_running retained its default value (false)
        */

        skiq_part_t part = _skiq_get_part( card );

        if ( is_gpsdo_running )
        {
            status = gpsdo_fpga_read_warp_voltage( card, p_warp_voltage );
        }
        else if ( part == skiq_nv100 )
        {
            status = sit5356_dctcxo_read_warp_voltage( card, p_warp_voltage );
        }
        else
        {
            /* no function implemented for reading extended warp voltage */
            status = -ENOSYS;
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_warp_voltage_extended_range( rf_id_t *p_id,
                                                         uint32_t *p_warp_voltage )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = read_warp_voltage_extended_range( card, p_warp_voltage );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t write_warp_voltage_extended_range( uint8_t card,
                                                  uint32_t warp_voltage )
{
    int32_t status = -ENOSYS;
    bool has_gpsdo = false, is_gpsdo_running = false;
    int32_t gpsdo_status=0;

    /*
      Check to see if the card has a GPSDO module in the FPGA.  If it does, check to see if the
      algorithm is running.
     */
    status = gpsdo_fpga_has_module( card, &has_gpsdo, &gpsdo_status );
    if ( status == 0 )
    {
        /* entry assumption: has_gpsdo is valid */

        if ( has_gpsdo )
        {
            status = gpsdo_fpga_is_enabled( card, &is_gpsdo_running );
        }
    }

    /*
      Here's where the "write extended warp voltage" functionality is dispatched to either indicate
      resource is busy (-EBUSY), the SiT3536, or indicate as not implemented (-ENOSYS).
     */
    if ( status == 0 )
    {
        skiq_part_t part = _skiq_get_part( card );

        /* entry assumption: is_gpsdo_running is valid because:
             1. status is 0 and has_gpsdo is true
             2. is_gpsdo_running retained its default value (false)
        */

        if ( is_gpsdo_running )
        {
            status = -EBUSY;
        }
        else if ( part == skiq_nv100 )
        {
            status = sit5356_dctcxo_write_warp_voltage( card, warp_voltage );
        }
        else
        {
            /* no function implemented for writing extended warp voltage */
            status = -ENOSYS;
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_warp_voltage_extended_range( rf_id_t *p_id,
                                                          uint32_t warp_voltage )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = write_warp_voltage_extended_range( card, warp_voltage );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t read_warp_voltage( uint8_t card,
                                  uint16_t *p_warp_voltage )
{
    int32_t status = -ENOSYS;
    skiq_part_t part = _skiq_get_part( card );

    if ( part == skiq_nv100 )
    {
        skiq_ref_clock_select_t ref_clock;

        /* only write the warp voltage if the reference clock is set as internal because on the
         * M.2-2280, the DAC is powered off when not using the internal reference */
        status = skiq_read_ref_clock_select( card, &ref_clock );
        if ( ( status == 0 ) && ( ref_clock == skiq_ref_clock_internal ) )
        {
            uint32_t warp_voltage_extended;

            /* @attention The user and factory warp voltage values that are stored in EEPROM
             * represent the upper 16 bits of the 26-bit resolution of the SiT5356. If the
             * caller wishes to have access to the full 26-bit resolution, use
             * navassa_read_warp_voltage_extended_range() instead. */
            status = read_warp_voltage_extended_range( card, &warp_voltage_extended );
            if ( status == 0 )
            {
                warp_voltage_extended = (warp_voltage_extended >> 10) & 0xFFFF;
                *p_warp_voltage = (uint16_t)warp_voltage_extended;
            }
        }
        else
        {
            status = -ENODEV;
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_warp_voltage( rf_id_t *p_id,
                                          uint16_t *p_warp_voltage )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = read_warp_voltage( card, p_warp_voltage );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t write_warp_voltage( uint8_t card,
                                   uint16_t warp_voltage )
{
    int32_t status = -ENOSYS;
    skiq_part_t part = _skiq_get_part( card );

    if ( part == skiq_nv100 )
    {
        skiq_ref_clock_select_t ref_clock;

        /* only write the warp voltage if the reference clock is set as internal because on the
         * M.2-2280, the DAC is powered off when not using the internal reference */
        status = skiq_read_ref_clock_select( card, &ref_clock );
        if ( ( status == 0 ) && ( ref_clock == skiq_ref_clock_internal ) )
        {
            uint32_t warp_voltage_extended;

            /* @attention The user and factory warp voltage values that are stored in EEPROM
             * represent the upper 16 bits of the 26-bit resolution of the SiT5356. If the caller
             * wishes to have access to the full 26-bit resolution, use
             * navassa_write_warp_voltage_extended_range() instead. */
            warp_voltage_extended = (uint32_t)warp_voltage << 10;

            status = write_warp_voltage_extended_range( card, warp_voltage_extended );
        }
        else
        {
            status = -ENODEV;
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_warp_voltage( rf_id_t *p_id,
                                           uint16_t warp_voltage )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = write_warp_voltage( card, warp_voltage );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t enable_rx_chan( uint8_t card,
                               skiq_rx_hdl_t hdl,
                               bool enable )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    adi_common_ChannelNumber_e channel = 0;

    if ( status == 0 )
    {
        debug_print("Changing %s to %s\n", rx_hdl_cstr(hdl), enable ? "enabled" : "disabled");

        /* @todo revisit when RFIC pin control mode feature is accepted */

        /* translate from the skiq RX handle to the RFIC channel number */
        switch (hdl)
        {
            case skiq_rx_hdl_A1:
            case skiq_rx_hdl_A2:
            case skiq_rx_hdl_B1:
                channel = skiq_rx_hdl_to_channel[hdl];
                break;

            default:
                status = -ENOTSUP;
                break;
        }
    }

    /* Only check the lo source if we're enabling*/
    if ((status == 0) && (enable == true))
    {
        if (is_requested_rx_lo_source_active(card, hdl) == false)
        {
            status = -EPERM;
        }
    }

    DEBUG_PRINT_CHANNEL_STATES( card );

    if ( status == 0 )
    {
        uint32_t value = 0;
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
        if ( status == 0 )
        {
            if ( channel == ADI_CHANNEL_1 )
            {
                RBF_SET( value, enable ? 1 : 0, RX1_EN );
            }
            else if ( channel == ADI_CHANNEL_2 )
            {
                RBF_SET( value, enable ? 1 : 0, RX2_EN );
            }
            else
            {
                status = -EINVAL;
            }

            if ( status == 0 )
            {
                debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", value);
                status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
            }
        }
    }

    if ( status == 0 )
    {
        if ( enable )
        {
            skiq_tx_hdl_t tx_hdl =                                      \
                ( hdl == skiq_rx_hdl_A1 ) ? skiq_tx_hdl_A1 :            \
                ( hdl == skiq_rx_hdl_A2 ) ? skiq_tx_hdl_A2 : skiq_tx_hdl_B1;

            /**
               @attention For PROXY mode only: if the corresponding transmit handle is NOT enabled,
               then wait for the receive channel to transition to RF_ENABLED or PRIMED.  If the
               transmit handle is enabled, there's no clear way to determine if or when the RX
               channel will transition, so don't bother to block waiting for a transition that may
               not happen.
            */
            if ( !is_tx_chan_enabled(card, tx_hdl) )
            {
                /* poll state until channel has completed transition (or timed out) */
                status = wait_rx_chan_state( device, channel,
                                             enable ? ADI_ADRV9001_CHANNEL_RF_ENABLED : \
                                             ADI_ADRV9001_CHANNEL_PRIMED );
            }
        }
        else
        {
            /**
               @attention For PROXY mode only: if RXn_EN is being disabled (set to 0), then wait for
               the transition back to PRIMED.  It may very well already be in PRIMED.
            */

            /* poll state until channel has completed transition (or timed out) */
            status = wait_rx_chan_state( device, channel, ADI_ADRV9001_CHANNEL_PRIMED );
        }
    }

    if ( status == 0 )
    {
        mark_rx_chan_state( card, hdl, enable );
    }

    DEBUG_PRINT_CHANNEL_STATES( card );

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t enable_rx_chan_all( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl;

    for( hdl = skiq_rx_hdl_A1; (hdl < skiq_rx_hdl_B2) && (status == 0); hdl++)
    {
        status = enable_rx_chan( card, hdl, enable );
        if (status != 0)
        {
            skiq_error("Unable to update all rx channels on Sidekiq %s, failed to update handle %s on card %u with status %" PRIi32,
                        part_cstr(_skiq_get_part(p_id->card)), rx_hdl_cstr(hdl), p_id->card, status);
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_enable_rx_chan( rf_id_t *p_id,
                                       bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            status = enable_rx_chan( card, hdl, enable );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t enable_tx_chan( uint8_t card,
                               skiq_tx_hdl_t hdl,
                               bool enable )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    adi_common_ChannelNumber_e channel = 0;

    if ( status == 0 )
    {
        debug_print("Changing %s to %s\n", rx_hdl_cstr(hdl), enable ? "enabled" : "disabled");

        /* @todo revisit when RFIC pin control mode feature is accepted */

        /* translate from the skiq RX handle to the RFIC channel number */
        switch (hdl)
        {
            case skiq_tx_hdl_A1:
            case skiq_tx_hdl_A2:
            case skiq_tx_hdl_B1:
                channel = skiq_tx_hdl_to_channel[hdl];
                break;

            default:
                status = -ENOTSUP;
                break;
        }
    }

    if ( status == 0 )
    {
        uint32_t value = 0;
        status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_WRITE, &value );
        if ( status == 0 )
        {
            if ( channel == ADI_CHANNEL_1 )
            {
                RBF_SET( value, enable ? 1 : 0, TX1_EN );
            }
            else if ( channel == ADI_CHANNEL_2 )
            {
                RBF_SET( value, enable ? 1 : 0, TX2_EN );
            }
            else
            {
                status = -EINVAL;
            }

            if ( status == 0 )
            {
                debug_print("Writing %08x to FPGA_REG_GPIO_WRITE\n", value);
                status = sidekiq_fpga_reg_write( card, FPGA_REG_GPIO_WRITE, value );
            }
        }
    }

    if ( status == 0 )
    {
        /**
           @attention For PROXY mode only: when TXn_EN = 1, then TXn_EN_TO_RFIC is dependent on
           whether or not a transmit block is present and being transmitted, so there's no clear way
           to determine if or when the TX channel will transition RFIC states (PRIMED or
           RF_ENABLED), so don't bother to block waiting for a transition that may not happen.

           However, if TXn_EN is being disabled (set to 0), then wait for the transition back to
           PRIMED.  It may very well already be in PRIMED.
        */

        if ( !enable )
        {
            status = wait_tx_chan_state( device, channel, ADI_ADRV9001_CHANNEL_PRIMED );
        }
    }


    if ( status == 0 )
    {
        mark_tx_chan_state( card, hdl, enable );
    }

    DEBUG_PRINT_CHANNEL_STATES( card );

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t enable_tx_chan_all( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl;

    for( hdl = skiq_tx_hdl_A1; (hdl < skiq_tx_hdl_B2) && (status == 0); hdl++)
    {
        status = enable_tx_chan( card, hdl, enable );
        if (status != 0)
        {
            skiq_error("Unable to update all tx channels on Sidekiq %s, failed to update handle %s on card %u with status %u",
                        part_cstr(_skiq_get_part(p_id->card)), tx_hdl_cstr(hdl), p_id->card, status);
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_enable_tx_chan( rf_id_t *p_id,
                                       bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = enable_tx_chan( card, hdl, enable );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


static int32_t inspect_tx_test_tone( uint8_t card,
                                     bool *p_enable )
{
    int32_t status = 0;
    uint32_t value = 0;

    status = sidekiq_fpga_reg_read( card, FPGA_REG_ADC_DAC_PRBS_TEST_CTRL, &value );
    if ( status == 0 )
    {
        *p_enable = ( RBF_GET( value, TX_DATA_SEL ) == TX_TONE_EN );
    }

    return status;
}


static int32_t config_tx_test_tone( uint8_t card,
                                    bool enable )
{
    int32_t status = 0;

    status = sidekiq_fpga_reg_RMW( card, enable ? TX_TONE_EN : TX_TONE_DIS, TX_DATA_SEL,
                                   FPGA_REG_ADC_DAC_PRBS_TEST_CTRL );

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_config_tx_test_tone( rf_id_t *p_id,
                                            bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        DEBUG_PRINT_CHANNEL_STATES( card );

        if ( status == 0 )
        {
            status = config_tx_test_tone( card, enable );
        }

        DEBUG_PRINT_CHANNEL_STATES( card );
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_test_tone_freq( rf_id_t *p_id,
                                               int32_t *p_tone_offset )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        uint8_t chan_index = UINT8_MAX;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_tx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            chan_index = skiq_tx_hdl_to_array_index[hdl];
        }

        if ( status == 0 )
        {
            /* The transmit test tone pattern in the FPGA is 8 samples long, so that means the test
             * tone appears at a frequency that is 1/8 of of the sample rate */
            *p_tone_offset = (uint32_t)(current_profile[card].tx.txProfile[chan_index].txInputRate_Hz) / 8;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct
*/
static void navassa_read_min_sample_rate( rf_id_t *p_id,
                                          uint32_t *p_min_sample_rate )
{
    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    *p_min_sample_rate = NAV_MIN_SAMPLE_RATE;

    TRACE_EXIT;

    return;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct
*/
static void navassa_read_max_sample_rate( rf_id_t *p_id,
                                          uint32_t *p_max_sample_rate )
{
    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    *p_max_sample_rate = NAV_MAX_SAMPLE_RATE;

    TRACE_EXIT;

    return;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t
navassa_write_rx_sample_rate_and_bandwidth( rf_id_t *p_id,
                                            uint32_t rate,
                                            uint32_t bandwidth )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    skiq_info("Requesting RX sample rate %u and bandwidth %u for %s on card %" PRIu8 "\n", rate,
              bandwidth, rx_hdl_cstr( hdl ), card);

    RADIO_LOCK(card);
    {
        adi_adrv9001_Init_t *found_profile = NULL;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            status = find_rx_profile( card, rate, bandwidth, &found_profile );
        }

        if ( status == 0 )
        {
            uint32_t sr_hz, bw_hz;

            sr_hz = radio_conf[card].rx[hdl].samp_rate_hz;
            bw_hz = radio_conf[card].rx[hdl].bandwidth_hz;
            radio_conf[card].rx[hdl].samp_rate_hz = rate;
            radio_conf[card].rx[hdl].bandwidth_hz = MIN(rate, bandwidth);

            if (is_requested_rx_lo_source_active(card, hdl) == false)
            { /* If the requested LO source isn't active, update it unless we're streaming*/

                if(is_rfic_streaming(card))
                {
                    status = -EPERM;
                }

                if (status == 0)
                {
                    /* Set a flag to ensure the profile is applied */
                    radio_conf[card].lo.lo_selection_pending = true;

                    if(hdl == skiq_rx_hdl_A1)
                    {
                        radio_conf[card].lo.rx1LoSelect = ADI_ADRV9001_LOSEL_LO1;
                    }
                    else if(hdl == skiq_rx_hdl_A2)
                    {
                        radio_conf[card].lo.rx2LoSelect = ADI_ADRV9001_LOSEL_LO1;
                    }
                    else if(hdl == skiq_rx_hdl_B1)
                    {
                        radio_conf[card].lo.rx2LoSelect = ADI_ADRV9001_LOSEL_LO2;
                    }
                }
            }

            if ( status == 0 )
            {
                status = apply_profile( card, found_profile );
                radio_conf[card].lo.lo_selection_pending = false;  //ok to clear this on a failure
            }

            if ( status != 0 )
            {
                radio_conf[card].rx[hdl].samp_rate_hz = sr_hz;
                radio_conf[card].rx[hdl].bandwidth_hz = bw_hz;
            }
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_tx_sample_rate_and_bandwidth( rf_id_t *p_id,
                                                           uint32_t rate,
                                                           uint32_t bandwidth )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    skiq_info("Requesting TX sample rate %u and bandwidth %u for %s on card %" PRIu8 "\n", rate,
              bandwidth, tx_hdl_cstr( hdl ), card);

    RADIO_LOCK(card);
    {
        adi_adrv9001_Init_t *found_profile = NULL;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = find_tx_profile( card, rate, bandwidth, &found_profile );
        }

        if ( status == 0 )
        {
            status = apply_profile( card, found_profile );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_sample_rate( rf_id_t *p_id,
                                            uint32_t *p_rate,
                                            double *p_actual_rate )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        uint8_t chan_index = UINT8_MAX;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            chan_index = skiq_rx_hdl_to_array_index[hdl];
        }

        if ( status == 0 )
        {
            *p_rate = (uint32_t)(current_profile[card].rx.rxChannelCfg[chan_index].profile.rxOutputRate_Hz);
            *p_actual_rate = (double)(*p_rate);
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_sample_rate( rf_id_t *p_id,
                                            uint32_t *p_rate,
                                            double *p_actual_rate )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        uint8_t chan_index = UINT8_MAX;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_tx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            chan_index = skiq_tx_hdl_to_array_index[hdl];
        }

        if ( status == 0 )
        {
            *p_rate = (uint32_t)(current_profile[card].tx.txProfile[chan_index].txInputRate_Hz);
            *p_actual_rate = (double)(*p_rate);
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_chan_bandwidth( rf_id_t *p_id,
                                               uint32_t *p_bandwidth,
                                               uint32_t *p_actual_bandwidth )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            *p_bandwidth = radio_conf[card].rx[hdl].bandwidth_hz;
            *p_actual_bandwidth = radio_conf[card].rx[hdl].actual_bandwidth_hz;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_chan_bandwidth( rf_id_t *p_id,
                                               uint32_t *p_bandwidth,
                                               uint32_t *p_actual_bandwidth )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        uint8_t chan_index = UINT8_MAX;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_tx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            chan_index = skiq_tx_hdl_to_array_index[hdl];
        }

        if ( status == 0 )
        {
            /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              @attention On the Day 14 release of ADI's SDK for Navassa, the actual primary signal
              bandwidth on the transmit path is the same as the sample rate because it's not being
              filtered.  Until adrv9002_driver is updated to use a later release, just report the
              txInputRate_Hz here instead
              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
            *p_bandwidth = (uint32_t)(current_profile[card].tx.txProfile[chan_index].txInputRate_Hz);
            *p_actual_bandwidth = *p_bandwidth;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t read_gpio(uint8_t card, uint32_t *p_value)
{
    int32_t status;
    int32_t pin = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device(card);
    adi_adrv9001_GpioPinLevel_e adi_level;
    *p_value = 0;

    for(pin = 0; pin < NAV_NR_DIGITAL_GPIO; pin++)
    {
        status = adi_adrv9001_gpio_InputPinLevel_Get(device,
                                                      ADI_ADRV9001_GPIO_DIGITAL_00 + pin,
                                                      &adi_level);
        ADI_HANDLE_ERROR(device, status);
        if (status == ADI_COMMON_ACT_NO_ACTION)
        {
            if(adi_level > 0)
            {
                BIT_SET(*p_value, BIT(pin));
            }
        }
        else
        {
            break;
        }
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_gpio( rf_id_t *p_id, uint32_t *p_value)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = read_gpio(card, p_value);
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_quadcal_mode( rf_id_t *p_id,
                                             skiq_tx_quadcal_mode_t *p_mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_tx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            *p_mode = radio_conf[card].tx[hdl].cal_mode;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_tx_quadcal_mode( rf_id_t *p_id,
                                              skiq_tx_quadcal_mode_t mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_tx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            if ( ( mode == skiq_tx_quadcal_mode_auto ) ||
                 ( mode == skiq_tx_quadcal_mode_manual ) )
            {
                radio_conf[card].tx[hdl].cal_mode = mode;
            }
            else
            {
                status = -EINVAL;
            }
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
static int32_t run_tx_quadcal( uint8_t card,
                               skiq_tx_hdl_t hdl )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );

    debug_print("Running TX calibration for %s on card %" PRIu8 "\n", tx_hdl_cstr( hdl ), card );
    status = epiq_adrv9002_calibrate_channel( device, ADI_TX, 
                                              skiq_tx_hdl_to_channel[hdl] );
    ADI_HANDLE_ERROR( device, status );

    return status;
}


/**************************************************************************************************/
static int32_t run_tx_quadcal_if_needed( uint8_t card,
                                         skiq_tx_hdl_t hdl,
                                         skiq_tx_quadcal_mode_t cal_mode,
                                         uint64_t lo_freq,
                                         uint64_t *p_last_cal_freq )
{
    int32_t status = 0;

    if ( cal_mode == skiq_tx_quadcal_mode_auto )
    {
        if ( ABS_DIFF( *p_last_cal_freq, lo_freq ) > NAV_MAX_FREQ_CAL_DELTA )
        {
            status = run_tx_quadcal( card, hdl );
            if ( status == 0 )
            {
                if ( *p_last_cal_freq == UINT64_MAX )
                {
                    debug_print("Updating last calibrated LO frequency from N/A to %" PRIu64 "\n",
                                lo_freq);
                }
                else
                {
                    debug_print("Updating last calibrated LO frequency from %" PRIu64 " to %"
                                PRIu64 "\n", *p_last_cal_freq, lo_freq);
                }

                *p_last_cal_freq = lo_freq;
            }
        }
        else
        {
            debug_print("No Tx calibration needed, %" PRIu64 " is within %" PRIu64 " threshold\n",
                        lo_freq, NAV_MAX_FREQ_CAL_DELTA);
        }
    }
    else
    {
        debug_print("No Tx calibration needed, cal_mode is not skiq_tx_quadcal_mode_auto\n");
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_run_tx_quadcal( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_tx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_tx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            status = run_tx_quadcal( card, hdl );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_stream_hdl_conflict( rf_id_t *p_id,
                                                    skiq_rx_hdl_t conflicting_hdl_array[],
                                                    uint8_t *p_num_hdl_array )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    /* default to no conflicts */
    *p_num_hdl_array = 0;

    if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        conflicting_hdl_array[*p_num_hdl_array] = skiq_rx_hdl_B1;
        *p_num_hdl_array = 1;
    }
    else if ( p_id->hdl == skiq_rx_hdl_B1 )
    {
        conflicting_hdl_array[*p_num_hdl_array] = skiq_rx_hdl_A2;
        *p_num_hdl_array = 1;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static bool navassa_is_chan_enable_xport_dependent( rf_id_t *p_id )
{
    return false;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_control_output_rx_gain_config( rf_id_t *p_id,
                                                           uint8_t *p_mode,
                                                           uint8_t *p_ena )
{
    int32_t status = 0;
    skiq_rx_hdl_t hdl;
    uint8_t mode;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    hdl = p_id->hdl;

    /* This RFIC does not support the mode/ena interface, so pass the handle through *p_mode and set
     * *p_ena to 0 */
    switch (hdl)
    {
        case skiq_rx_hdl_A1:
        case skiq_rx_hdl_A2:
        case skiq_rx_hdl_B1:
            mode = (uint8_t)hdl;
            break;

        default:
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        *p_ena = 0;
        *p_mode = mode;
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
static int32_t write_control_output_config( uint8_t card,
                                            uint8_t mode )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );
    skiq_rx_hdl_t hdl = (skiq_rx_hdl_t)mode;
    uint8_t channel;

    switch (hdl)
    {
        case skiq_rx_hdl_A1:
            channel = ADI_CHANNEL_1;
            break;

        case skiq_rx_hdl_A2:
        case skiq_rx_hdl_B1:
            channel = ADI_CHANNEL_2;
            break;

        case skiq_rx_hdl_end:
            status = -ENOENT;
            break;

        default:
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        status = move_rx_chan_to_calibrated( card, channel );
    }

    if ( status == 0 )
    {
        adi_adrv9001_GainIndexPinCfg_t gipc;
        gipc.gainIndex_01_00 = ADI_ADRV9001_GPIO_PIN_CRUMB_01_00;
        gipc.gainIndex_03_02 = ADI_ADRV9001_GPIO_PIN_CRUMB_03_02;
        gipc.gainIndex_05_04 = ADI_ADRV9001_GPIO_PIN_CRUMB_05_04;
        gipc.gainIndex_07_06 = ADI_ADRV9001_GPIO_PIN_CRUMB_07_06;

        status = adi_adrv9001_Rx_GainIndex_Gpio_Configure( device, channel, &gipc );
        ADI_HANDLE_ERROR( device, status );
    }

    if ( status == 0 )
    {
        status = restore_rx_chan_state_from_calibrated( card, hdl );
    }

    if ( status == 0 )
    {
        if ( ( radio_conf[card].rfic_control_output_dest != skiq_rx_hdl_end ) &&
             ( radio_conf[card].rfic_control_output_dest != hdl ) )
        {
            skiq_warning("Updating RFIC control output destination to %s on card %" PRIu8 "\n",
                         rx_hdl_cstr( hdl ), card);
        }
        radio_conf[card].rfic_control_output_dest = hdl;
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_control_output_config( rf_id_t *p_id,
                                                    uint8_t mode,
                                                    uint8_t ena )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = write_control_output_config( card, mode );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_control_output_config( rf_id_t *p_id,
                                                   uint8_t *p_mode,
                                                   uint8_t *p_ena )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            *p_ena = 0;
            *p_mode = (uint8_t)radio_conf[card].rfic_control_output_dest;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_init_from_file( rf_id_t *p_id,
                                       FILE* p_file )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_enable_pin_gain_ctrl( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    /* @todo revisit This seems to be only applicable to a factory test for AD9361 RFICs */

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static uint32_t navassa_read_hop_on_ts_gpio( rf_id_t *p_id,
                                             uint8_t *p_chip_index )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_freq_tune_mode( rf_id_t *p_id,
                                                skiq_freq_tune_mode_t mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_freq_tune_mode( rf_id_t *p_id,
                                               skiq_freq_tune_mode_t *p_mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_tx_freq_tune_mode( rf_id_t *p_id,
                                                skiq_freq_tune_mode_t mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_freq_tune_mode( rf_id_t *p_id,
                                               skiq_freq_tune_mode_t *p_mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_config_rx_hop_list( rf_id_t *p_id,
                                           uint16_t num_freq,
                                           uint64_t freq_list[],
                                           uint16_t initial_index )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_hop_list( rf_id_t *p_id,
                                         uint16_t *p_num_freq,
                                         uint64_t *p_freq_list )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_config_tx_hop_list( rf_id_t *p_id,
                                           uint16_t num_freq,
                                           uint64_t freq_list[],
                                           uint16_t initial_index )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_tx_hop_list( rf_id_t *p_id,
                                         uint16_t *p_num_freq,
                                         uint64_t *p_freq_list )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_next_rx_hop( rf_id_t *p_id,
                                          uint16_t freq_index )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_next_tx_hop( rf_id_t *p_id,
                                          uint16_t freq_index )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_rx_hop( rf_id_t *p_id,
                               uint64_t rf_timestamp,
                               double *p_act_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_tx_hop( rf_id_t *p_id,
                               uint64_t rf_timestamp,
                               double *p_act_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_curr_rx_hop( rf_id_t *p_id,
                                         freq_hop_t *p_hop )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_next_rx_hop( rf_id_t *p_id,
                                         freq_hop_t *p_hop )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_curr_tx_hop( rf_id_t *p_id,
                                         freq_hop_t *p_hop )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_next_tx_hop( rf_id_t *p_id,
                                         freq_hop_t *p_hop )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    status = -ENOTSUP;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_cal_mode( rf_id_t *p_id,
                                         skiq_rx_cal_mode_t *p_mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            *p_mode = radio_conf[card].rx[hdl].cal_mode;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_cal_mode( rf_id_t *p_id,
                                          skiq_rx_cal_mode_t mode )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            if ( ( mode == skiq_rx_cal_mode_auto ) ||
                 ( mode == skiq_rx_cal_mode_manual ) )
            {
                radio_conf[card].rx[hdl].cal_mode = mode;
            }
            else
            {
                status = -EINVAL;
            }
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
static int32_t run_rx_cal( uint8_t card,
                           skiq_rx_hdl_t hdl )
{
    int32_t status = 0;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );

    debug_print("Running RX calibration for %s on card %" PRIu8 "\n", rx_hdl_cstr( hdl ), card );
    status = epiq_adrv9002_calibrate_channel( device, ADI_RX, skiq_rx_hdl_to_channel[hdl] );
    ADI_HANDLE_ERROR( device, status );

    return status;
}


/**************************************************************************************************/
static int32_t run_rx_cal_if_needed( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     skiq_rx_cal_mode_t cal_mode,
                                     uint64_t lo_freq,
                                     uint64_t *p_last_cal_freq )
{
    int32_t status = 0;

    if ( cal_mode == skiq_rx_cal_mode_auto )
    {
        if ( ABS_DIFF( *p_last_cal_freq, lo_freq ) > NAV_MAX_FREQ_CAL_DELTA )
        {
            status = run_rx_cal( card, hdl );
            if ( status == 0 )
            {
                if ( *p_last_cal_freq == UINT64_MAX )
                {
                    debug_print("Updating last calibrated LO frequency from N/A to %" PRIu64 "\n",
                                lo_freq);
                }
                else
                {
                    debug_print("Updating last calibrated LO frequency from %" PRIu64 " to %"
                                PRIu64 "\n", *p_last_cal_freq, lo_freq);
                }

                *p_last_cal_freq = lo_freq;
            }
        }
        else
        {
            debug_print("No Rx calibration needed, %" PRIu64 " is within %" PRIu64 " threshold\n",
                        lo_freq, NAV_MAX_FREQ_CAL_DELTA);
        }
    }
    else
    {
        debug_print("No Rx calibration needed, cal_mode is not skiq_rx_cal_mode_auto\n");
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_run_rx_cal( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            status = run_rx_cal( card, hdl );
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_cal_mask( rf_id_t *p_id,
                                         uint32_t *p_cal_mask )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            *p_cal_mask = radio_conf[card].rx[hdl].cal_mask;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_write_rx_cal_mask( rf_id_t *p_id,
                                          uint32_t cal_mask )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    skiq_rx_hdl_t hdl = p_id->hdl;

    RADIO_LOCK(card);
    {
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            status = check_rx_hdl_valid( hdl );
        }

        if ( status == 0 )
        {
            if ( ( cal_mask & NAV_RX_CAL_TYPES ) != cal_mask )
            {
                status = -EINVAL;
            }
        }

        if ( status == 0 )
        {
            if ( is_rfic_streaming( card ))
            {
                status = -EPERM;
            }
        }

        if ( status == 0 )
        {
            if ( cal_mask != radio_conf[card].rx[hdl].cal_mask )
            {
                DEBUG_PRINT_CHANNEL_STATES( card );

                status = write_rx_cal_mask(card, hdl, cal_mask);

                DEBUG_PRINT_CHANNEL_STATES( card );
            }
        }

        if ( status == 0 )
        {
            radio_conf[card].rx[hdl].cal_mask = cal_mask;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
 * @brief Writes the rx_cal settings/mask.  This would typically be used to restore the settings
 *        after having been cleared by another operation, such as
 *        write_rx_sample_rate_and_bandwidth().
 *
 * @warning This function performs no validation of the cached values or locking.
 *
 * @param[in] card requested Sidekiq card ID
 * @param[in] hdl  Sidekiq Rx handle of interest
 * @param[in] cal_mask mask of calibration types to enable, skiq_rx_cal_type_t, limited
 *            to what is supported in NAV_RX_CAL_TYPES.
 *
 * @return 0 on success, else -EPROTO
 * @retval
**/

static int32_t write_rx_cal_mask(uint8_t card, skiq_rx_hdl_t hdl, uint32_t cal_mask)
{
    int32_t status = 0;
    bool enable_dc_offset = ((cal_mask & skiq_rx_cal_type_dc_offset ) == skiq_rx_cal_type_dc_offset);
    bool enable_qec = ((cal_mask & skiq_rx_cal_type_quadrature) == skiq_rx_cal_type_quadrature);
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );

    debug_print("Writing Rx cal settings for hdl %s: enable_dc_offset=%s enable_qec=%s\n",
                rx_hdl_cstr(hdl),bool_cstr(enable_dc_offset),bool_cstr(enable_qec));
    status = epiq_adrv9002_update_rx_cal_tracking(device, skiq_rx_hdl_to_channel[hdl],
                                                  enable_dc_offset, enable_qec);
    if(status != 0 )
    {
        debug_print("Unable to write Rx cal settings for hdl %s with status=%" PRIi32 "\n",
                rx_hdl_cstr(hdl), status);
    }
    ADI_HANDLE_ERROR( device, status );

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rx_cal_types_avail( rf_id_t *p_id,
                                                uint32_t *p_cal_mask )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    *p_cal_mask = NAV_RX_CAL_TYPES;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_temp( rf_id_t *p_id,
                                  int8_t *p_temp_in_deg_C )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( status == 0 )
        {
            int16_t temperature_C;

            status = adi_adrv9001_Temperature_Get( device, &temperature_C );
            ADI_HANDLE_ERROR( device, status );
            if ( status == ADI_COMMON_ACT_NO_ACTION )
            {
                debug_print("Got %d from RFIC\n", temperature_C);
                temperature_C = MAX(INT8_MIN, temperature_C);
                temperature_C = MIN(INT8_MAX, temperature_C);
                *p_temp_in_deg_C = (int8_t)temperature_C;
            }
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_auxadc( rf_id_t *p_id,
                                    uint8_t channel,
                                    uint16_t *p_millivolts )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    /* 0. Check to see that RFIC is ready */
    RADIO_LOCK(card);
    {
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );
        adi_adrv9001_AuxAdc_e adc_channel = ADI_ADRV9001_AUXADC0 + channel;

        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        if ( ( adc_channel > ADI_ADRV9001_AUXADC3 ) ||
             ( adc_channel < ADI_ADRV9001_AUXADC0 ) )
        {
            status = -EINVAL;
        }

        /* @todo check if requested ADC channel is actually active / configured */

        if ( status == 0 )
        {
            uint16_t reading_mV;

            status = adi_adrv9001_AuxAdc_Voltage_Get( device, adc_channel, &reading_mV );
            ADI_HANDLE_ERROR( device, status );
            if ( status == ADI_COMMON_ACT_NO_ACTION )
            {
                debug_print("AuxADC voltage is %" PRIu16 " mV on card %" PRIu8 "\n",
                            reading_mV, card);
                *p_millivolts = reading_mV;
            }
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_read_rf_capabilities( rf_id_t *p_id,
                                             skiq_rf_capabilities_t *p_rf_capabilities )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = NAV_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = NAV_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = NAV_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = NAV_MAX_RX_FREQUENCY_IN_HZ;

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
static skiq_rx_hdl_t navassa_rx_hdl_map_other( rf_id_t *p_id )
{  
    skiq_rx_hdl_t curr_hdl = p_id->hdl;
    skiq_rx_hdl_t other_hdl = skiq_rx_hdl_end;

    RADIO_LOCK(p_id->card);

    /* other_hdl applies when the LO is shared between A1 and A2*/
    
    /* handle the case for A1: A2 should be hdl_other*/
    if( (curr_hdl == skiq_rx_hdl_A1)  &&
        (radio_conf[p_id->card].lo.rx2LoSelect == ADI_ADRV9001_LOSEL_LO1))
    {
        other_hdl = skiq_rx_hdl_A2;
    } /* handle the case for A2: A1 should be hdl_other */
    else if( (curr_hdl == skiq_rx_hdl_A2)  &&
             (radio_conf[p_id->card].lo.rx2LoSelect == ADI_ADRV9001_LOSEL_LO1))
    {
        other_hdl = skiq_rx_hdl_A1;
    }

    RADIO_UNLOCK(p_id->card);

       
    return other_hdl;
}


/**************************************************************************************************/
static skiq_tx_hdl_t navassa_tx_hdl_map_other( rf_id_t *p_id )
{
    return skiq_tx_hdl_end;
}


/**************************************************************************************************/
static int32_t select_ref_clock( uint8_t card )
{
    int32_t status;
    skiq_part_t part = _skiq_get_part( card );

    switch (part)
    {
        case skiq_nv100:
            status = nv100_select_ref_clock( card );
            break;

        default:
            status = -ENOTSUP;
            break;
    }

    return status;
}


/**************************************************************************************************/
/**
   @brief

   Reference(s):
   - ADRV9001 System Development User Guide UG-1828 (Rev PrA)
   -- API INITIALIZATION SEQUENCE starting on PAGE 37

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_reset_and_init( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        skiq_part_t part = _skiq_get_part( card );
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );

        /* 0. Check if already initialized */
        if ( radio_conf[card].active )
        {
            status = -EEXIST;
        }

        /* set all of the radio_conf[card] values to defaults */
        if ( status == 0 )
        {
            memcpy( &(radio_conf[card]),
                    &radio_config_default, sizeof( struct radio_config ) );
        }

        /***** Reference clock selection *****/
        if ( status == 0 )
        {
            status = select_ref_clock( card );
        }

        /* Disable TX test tone */
        if ( status == 0 )
        {
            status = config_tx_test_tone( card, false );
        }

        /***** RFIC Initialization *****/
        if ( status == 0 )
        {
            status = navassa_bringup( card );
        }

        /* configure the AUX ADC to allow for temperature sensing on Sidekiq NV100 */
        if ( ( status == 0 ) && ( part == skiq_nv100 ) )
        {
            status = adi_adrv9001_AuxAdc_Configure( device, ADI_ADRV9001_AUXADC_THERM, ENABLE );
            ADI_HANDLE_ERROR( device, status );
        }

        /* configure default warp voltage for a Sidekiq NV100 */
        if ( ( status == 0 ) && ( part == skiq_nv100 ) )
        {
            status = write_warp_voltage( card, DEFAULT_WARP_VOLTAGE_NV100 );
            if ( status == -ENODEV )
            {
                /* ignore a status of -ENODEV during reset-and-init since it means the reference
                 * clock selection is not internal */
                status = 0;
            }
            else if ( status == -EBUSY )
            {
                /* ignore a status of -EBUSY during reset-and-init since it means the reference
                 * clock warp is under the control of the GPSDO */
                status = 0;
            }
        }

        if ( status == 0 )
        {
            skiq_info("Setting Transmit to use System Timestamp on card %" PRIu8 "\n", card);
            status = sidekiq_fpga_reg_RMWV( card, 1, FRC_SEL_FOR_TX, FPGA_REG_FPGA_CTRL );
        }

        if ( status == 0 )
        {
            radio_conf[card].active = true;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
/**
   @brief

   @param[in] p_id Reference to RF identifier struct

   @return 0 on success, else a negative errno value
   @retval
*/
static int32_t navassa_release( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;

    RADIO_LOCK(card);
    {
        skiq_part_t part = _skiq_get_part( card );
        adi_adrv9001_Device_t *device = get_adrv9001_device( card );

        /* check first to see if the radio is active / initialized */
        if ( !radio_conf[card].active )
        {
            status = -ENODEV;
        }

        /* disable the AUX ADC0 prior to shutdown */
        if ( ( status == 0 ) && ( part == skiq_nv100 ) )
        {
            status = adi_adrv9001_AuxAdc_Configure( device, ADI_ADRV9001_AUXADC_THERM, DISABLE );
            ADI_HANDLE_ERROR( device, status );
        }

        /* disable RX and TX channels prior to shutdown */
        if ( status == 0 )
        {
            status = enable_rx_chan_all(p_id, false);
        }

        if ( status == 0 )
        {
            status = enable_tx_chan_all(p_id, false);
        }

        /* N. Shutdown the ADRV9002 */
        if ( status == 0 )
        {
            debug_print("Shutting down the RFIC\n");
            status = epiq_adrv9002_shutdown( device );
            ADI_HANDLE_ERROR( device, status );
        }

        /* Finally, mark the radio as inactive */
        if ( status == 0 )
        {
            radio_conf[card].active = false;
        }
    }
    RADIO_UNLOCK(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
static int32_t configure_prbs_mode( adi_adrv9001_Device_t *device,
                                    adi_adrv9001_SsiTestModeData_e test_mode_data )
{
    int32_t status = 0;
    adi_adrv9001_RxSsiTestModeCfg_t rx_config;

    rx_config.testData = test_mode_data;
    rx_config.fixedDataPatternToTransmit = 0;

    status = adi_adrv9001_Ssi_Rx_TestMode_Configure( device, ADI_CHANNEL_1,
                                                     ADI_ADRV9001_SSI_TYPE_LVDS,
                                                     ADI_ADRV9001_SSI_FORMAT_16_BIT_I_Q_DATA,
                                                     &rx_config );
    ADI_HANDLE_ERROR(device, status);
    if ( status == 0 )
    {
        status = adi_adrv9001_Ssi_Rx_TestMode_Configure( device, ADI_CHANNEL_2,
                                                         ADI_ADRV9001_SSI_TYPE_LVDS,
                                                         ADI_ADRV9001_SSI_FORMAT_16_BIT_I_Q_DATA,
                                                         &rx_config );
        ADI_HANDLE_ERROR(device, status);
    }

    return status;
}


/**************************************************************************************************/
int32_t navassa_enable_prbs_mode(uint8_t card)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("card: %d\n", card);

    adi_adrv9001_Device_t *device = get_adrv9001_device( card );

    status = configure_prbs_mode( device, ADI_ADRV9001_SSI_TESTMODE_DATA_PRBS15 );

    if ( status == 0 )
    {
        /* set FPGA RX select to 15 bit PRBS data */
        status = sidekiq_fpga_reg_RMWV(card, 0x6, RX_DATA_SEL, FPGA_REG_ADC_DAC_PRBS_TEST_CTRL);
    }

    if ( status == 0 )
    {
        /* reset and re-sync the RX LSSI interface in the FPGA *AFTER* both the RFIC and FPGA test
           modes are configured for the PRBS15 pattern */
        status = reinit_rx_lssi( card );
    }

    TRACE_EXIT_STATUS(status);

    return status;
}


/**************************************************************************************************/
int32_t navassa_disable_prbs_mode(rf_id_t *p_id)
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS_RF_ID(p_id);

    uint8_t card = p_id->card;
    adi_adrv9001_Device_t *device = get_adrv9001_device( card );

    status = configure_prbs_mode( device, ADI_ADRV9001_SSI_TESTMODE_DATA_NORMAL );

    if ( status == 0 )
    {
        /* set FPGA RX select to I/Q data */
        status = sidekiq_fpga_reg_RMWV(card, 0x0, RX_DATA_SEL, FPGA_REG_ADC_DAC_PRBS_TEST_CTRL);
    }

    if ( status == 0 )
    {
        /* reset and re-sync the RX LSSI interface in the FPGA *AFTER* both the RFIC and FPGA test
           modes are configured for "normal data" */
        status = reinit_rx_lssi( card );
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
/* This function checks FPGA PRBS registers for bit error and out-of-sync indications.  The FPGA
   design (borrowed from ADI), does not count the number of errors, but provides a mask of what
   error types are detected

   Bit 0 - Bit errors detected on RX1
   Bit 1 - Out of sync error detected on RX1
   Bit 2 - Bit errors detected on RX2
   Bit 3 - Out of sync error detected on RX2
 */
int32_t navassa_check_prbs_status(uint8_t card, uint8_t nr_handles, uint32_t *p_prbs_errors)
{
    int32_t status = 0;
    uint32_t val = 0;

    TRACE_ENTER;
    TRACE_ARGS("card: %d, nr_handles: %d, p_prbs_errors: %p\n", card, nr_handles, p_prbs_errors);

    *p_prbs_errors = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_ADC_NUM_ERRORS_A, &val);
    if ( status == 0 )
    {
        if ( RBF_GET( val, BIT_ERROR ) > 0 )
        {
            skiq_error("PRBS bit error detected on handle A1\n");
            *p_prbs_errors |= BIT_ERROR_RX1_MASK;
            status = -EFAULT;
        }
    }

    if ( status == 0 )
    {
        if ( RBF_GET( val, OUT_OF_SYNC_ERROR ) > 0 )
        {
            skiq_error("PRBS Out-of-Sync error detected on handle A1\n");
            *p_prbs_errors |= OOS_ERROR_RX1_MASK;
            status = -ENOLCK;
        }
    }

    if ( ( status == 0 ) && ( nr_handles == 2 ) )
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_ADC_NUM_ERRORS_B, &val);
        if ( status == 0 )
        {
            if ( RBF_GET( val, BIT_ERROR ) > 0 )
            {
                skiq_error("PRBS bit error detected on handle B1\n");
                *p_prbs_errors |= BIT_ERROR_RX2_MASK;
                status = -EFAULT;
            }
        }

        if ( status == 0 )
        {
            if ( RBF_GET( val, OUT_OF_SYNC_ERROR ) > 0 )
            {
                skiq_error("PRBS Out-of-Sync error detected on handle B1\n");
                *p_prbs_errors |= OOS_ERROR_RX2_MASK;
                status = -ENOLCK;
            }
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}

int cmp_hdl_t(const void *p, const void *q)
{
    uint8_t l = ((sr_bw_params_t *)p)->hdl;
    uint8_t r = ((sr_bw_params_t *)q)->hdl; 

    /* Compare the enum values of skiq_rx_hdl_t and sort descending */
    /* So for the case of p=A1=0, q=A2=1, we want the result > 0*/
    /* Return value meaning:
        <0 The element pointed by p goes before the element pointed by q
        0  The element pointed by p is equivalent to the element pointed by q
        >0 The element pointed by p goes after the element pointed by q
    */

    return (r-l);
}


int32_t navassa_write_rx_sample_rate_and_bandwidth_multi( rf_id_t *p_id, skiq_rx_hdl_t handles[],
                                                          uint8_t nr_handles,
                                                          uint32_t rate[],
                                                          uint32_t bandwidth[])
{
    int32_t status = 0;
    rf_id_t rf_id =  *p_id;
    ARRAY_WITH_DEFAULTS(sr_bw_params_t, req_hdls, skiq_rx_hdl_end, SR_BW_PARAMS_T_INITIALIZER);

    /* Store inputs into a struct since we want to reorder
       and maintain the relationship */
    for(uint8_t i=0; i<nr_handles; i++)
    {
        req_hdls[i].bandwidth = bandwidth[i];
        req_hdls[i].rate = rate[i];
        req_hdls[i].hdl = handles[i];
    }

    if (status == 0)
    {
        qsort(req_hdls, nr_handles, sizeof(sr_bw_params_t),cmp_hdl_t);
    }

    for(uint8_t i=0; ((i<nr_handles) && (status == 0)); i++)
    {
        rf_id.hdl = req_hdls[i].hdl;
        status  = navassa_write_rx_sample_rate_and_bandwidth(&rf_id, req_hdls[i].rate, req_hdls[i].bandwidth);
    }

    return status;
}

/***** GLOBAL DATA *****/

rfic_functions_t rfic_adrv9002_funcs =
{
    .reset_and_init                           = navassa_reset_and_init,
    .display_version_info                     = navassa_display_version_info,
    .release                                  = navassa_release,
    .write_rx_freq                            = navassa_write_rx_freq,
    .write_tx_freq                            = navassa_write_tx_freq,
    .write_rx_spectrum_invert                 = navassa_write_rx_spectrum_invert,
    .write_tx_spectrum_invert                 = navassa_write_tx_spectrum_invert,
    .write_tx_attenuation                     = navassa_write_tx_attenuation,
    .read_tx_attenuation                      = navassa_read_tx_attenuation,
    .read_tx_attenuation_range                = navassa_read_tx_attenuation_range,
    .write_rx_gain_mode                       = navassa_write_rx_gain_mode,
    .read_rx_gain_mode                        = navassa_read_rx_gain_mode,
    .read_rx_gain_range                       = navassa_read_rx_gain_range,
    .write_rx_gain                            = navassa_write_rx_gain,
    .read_rx_gain                             = navassa_read_rx_gain,
    .read_adc_resolution                      = navassa_read_adc_resolution,
    .read_dac_resolution                      = navassa_read_dac_resolution,
    .read_warp_voltage                        = navassa_read_warp_voltage,
    .write_warp_voltage                       = navassa_write_warp_voltage,
    .read_warp_voltage_extended_range         = navassa_read_warp_voltage_extended_range,
    .write_warp_voltage_extended_range        = navassa_write_warp_voltage_extended_range,
    .enable_rx_chan                           = navassa_enable_rx_chan,
    .enable_tx_chan                           = navassa_enable_tx_chan,
    .config_tx_test_tone                      = navassa_config_tx_test_tone,
    .read_tx_test_tone_freq                   = navassa_read_tx_test_tone_freq,
    .write_tx_test_tone_freq                  = NULL,
    .read_min_rx_sample_rate                  = navassa_read_min_sample_rate,
    .read_max_rx_sample_rate                  = navassa_read_max_sample_rate,
    .read_min_tx_sample_rate                  = navassa_read_min_sample_rate,
    .read_max_tx_sample_rate                  = navassa_read_max_sample_rate,
    .write_rx_sample_rate_and_bandwidth       = navassa_write_rx_sample_rate_and_bandwidth,
    .write_rx_sample_rate_and_bandwidth_multi = navassa_write_rx_sample_rate_and_bandwidth_multi,
    .write_tx_sample_rate_and_bandwidth       = navassa_write_tx_sample_rate_and_bandwidth,
    .read_rx_sample_rate                      = navassa_read_rx_sample_rate,
    .read_tx_sample_rate                      = navassa_read_tx_sample_rate,
    .read_rx_chan_bandwidth                   = navassa_read_rx_chan_bandwidth,
    .read_tx_chan_bandwidth                   = navassa_read_tx_chan_bandwidth,
    .init_gpio                                = NULL,
    .write_gpio                               = NULL,
    .read_gpio                                = navassa_read_gpio,
    .read_rx_filter_overflow                  = NULL,
    .read_tx_filter_overflow                  = NULL,
    .read_fpga_rf_clock                       = NULL,
    .read_tx_quadcal_mode                     = navassa_read_tx_quadcal_mode,
    .write_tx_quadcal_mode                    = navassa_write_tx_quadcal_mode,
    .run_tx_quadcal                           = navassa_run_tx_quadcal,
    .read_rx_stream_hdl_conflict              = navassa_read_rx_stream_hdl_conflict,
    .is_chan_enable_xport_dependent           = navassa_is_chan_enable_xport_dependent,
    .read_control_output_rx_gain_config       = navassa_read_control_output_rx_gain_config,
    .write_control_output_config              = navassa_write_control_output_config,
    .read_control_output_config               = navassa_read_control_output_config,
    .init_from_file                           = navassa_init_from_file,
    .power_down_tx                            = NULL,
    .enable_pin_gain_ctrl                     = navassa_enable_pin_gain_ctrl,
    .read_hop_on_ts_gpio                      = navassa_read_hop_on_ts_gpio,
    .write_rx_freq_tune_mode                  = navassa_write_rx_freq_tune_mode,
    .write_tx_freq_tune_mode                  = navassa_write_tx_freq_tune_mode,
    .read_rx_freq_tune_mode                   = navassa_read_rx_freq_tune_mode,
    .read_tx_freq_tune_mode                   = navassa_read_tx_freq_tune_mode,
    .config_rx_hop_list                       = navassa_config_rx_hop_list,
    .config_tx_hop_list                       = navassa_config_tx_hop_list,
    .read_rx_hop_list                         = navassa_read_rx_hop_list,
    .read_tx_hop_list                         = navassa_read_tx_hop_list,
    .write_next_rx_hop                        = navassa_write_next_rx_hop,
    .write_next_tx_hop                        = navassa_write_next_tx_hop,
    .rx_hop                                   = navassa_rx_hop,
    .tx_hop                                   = navassa_tx_hop,
    .read_curr_rx_hop                         = navassa_read_curr_rx_hop,
    .read_next_rx_hop                         = navassa_read_next_rx_hop,
    .read_curr_tx_hop                         = navassa_read_curr_tx_hop,
    .read_next_tx_hop                         = navassa_read_next_tx_hop,
    .read_rx_cal_mode                         = navassa_read_rx_cal_mode,
    .write_rx_cal_mode                        = navassa_write_rx_cal_mode,
    .run_rx_cal                               = navassa_run_rx_cal,
    .read_rx_cal_mask                         = navassa_read_rx_cal_mask,
    .write_rx_cal_mask                        = navassa_write_rx_cal_mask,
    .read_rx_cal_types_avail                  = navassa_read_rx_cal_types_avail,
    .read_tx_enable_mode                      = NULL,
    .read_rx_enable_mode                      = NULL,
    .write_tx_enable_mode                     = NULL,
    .write_rx_enable_mode                     = NULL,
    .read_temp                                = navassa_read_temp,
    .read_auxadc                              = navassa_read_auxadc,
    .swap_rx_port_config                      = NULL,
    .swap_tx_port_config                      = NULL,
    .read_rf_capabilities                     = navassa_read_rf_capabilities,
    .write_ext_clock_select                   = NULL,
    .rx_hdl_map_other                         = navassa_rx_hdl_map_other,
    .tx_hdl_map_other                         = navassa_tx_hdl_map_other,
    .read_rx_analog_filter_bandwidth          = NULL,
    .read_tx_analog_filter_bandwidth          = NULL,
    .write_rx_analog_filter_bandwidth         = NULL,
    .write_tx_analog_filter_bandwidth         = NULL,

    /****** RFIC Rx FIR function pointers ******/
    /**
       @warn The Navassa RFIC uses 24-bit filter coefficients and CANNOT implement these functions
       since they are currently typed to use 16-bit coefficients
     */
    .read_rx_fir_config                 = NULL,
    .read_rx_fir_coeffs                 = NULL,
    .write_rx_fir_coeffs                = NULL,
    .read_rx_fir_gain                   = NULL,
    .write_rx_fir_gain                  = NULL,

    /****** RFIC Tx FIR function pointers ******/
    /**
       @warn The Navassa RFIC uses 24-bit filter coefficients and CANNOT implement these functions
       since they are currently typed to use 16-bit coefficients
     */
    .read_tx_fir_config                 = NULL,
    .read_tx_fir_coeffs                 = NULL,
    .write_tx_fir_coeffs                = NULL,
    .read_tx_fir_gain                   = NULL,
    .write_tx_fir_gain                  = NULL,
};
