/**
 * @file   sidekiq_card_mgr.c
 * @date   2017-03-01 15:23:29
 *
 * @brief  Sidekiq Card Manager
 *
 * <pre>
 * Copyright 2017-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** INCLUDES *****/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#ifndef __MINGW32__
#include <sys/mman.h>
#endif /* __MINGW32__ */
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <inttypes.h>
#include <errno.h>
#include <ctype.h>              /* for isalnum() */
#ifndef __MINGW32__
#include <sys/inotify.h>
#include <syslog.h>
#include <limits.h>
#include <poll.h>
#else
#include "os_logger.h"
#endif /* __MINGW32__ */

#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_card_mgr_private.h"
#include "sidekiq_card.h"
#include "sidekiq_api_factory.h" /* for SKIQ_FACT_SERIAL_NUM_LEGACY_MAX */


/***** DEFINES *****/

/* enable debug_print and debug_print_plain when DEBUG_CARD_MGR_TRACE is defined */
#if (defined DEBUG_CARD_MGR_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

/* enable debug_print and debug_print_plain when DEBUG_CARD_MGR is defined */
#if (defined DEBUG_CARD_MGR)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/* No card manager under Windows nor robust pthread mutexes */
#if (defined __MINGW32__)
#define _HAVE_CARD_MGR                  (0)
#define _HAVE_ROBUST_MUTEX              (0)
#else
#define _HAVE_CARD_MGR                  (1)
#define _HAVE_ROBUST_MUTEX              (1)
#endif /* __MINGW32__ */

#if _HAVE_ROBUST_MUTEX
#   define _USE_ROBUST_NP               (1)

#   if defined(__GLIBC__) && defined(__GLIBC_PREREQ)
/*
    glibc 2.12 introduced pthread_mutexattr_setrobust,
    pthread_mutexattr_getrobust, and pthread_mutex_consistent which replace the _np
    (not portable) variants... if possible, use the newer functions (the _np variants
    were marked as deprecated in glibc 2.34)
*/
#       if __GLIBC_PREREQ(2, 12)
#           undef _USE_ROBUST_NP        /* used to avoid the "redefined" warning */
#           define _USE_ROBUST_NP       (0)
#       endif
#   endif /* __GLIBC__ && __GLIBC_PREREQ */

/* Define pre-processor aliases to use the correct pthread functions */
#if _USE_ROBUST_NP
#   define local_pthread_mutexattr_setrobust    pthread_mutexattr_setrobust_np
#   define local_pthread_mutexaddr_getrobust    pthread_mutexattr_getrobust_np
#   define local_pthread_mutex_consistent       pthread_mutex_consistent_np
#else
#   define local_pthread_mutexattr_setrobust    pthread_mutexattr_setrobust
#   define local_pthread_mutexaddr_getrobust    pthread_mutexattr_getrobust
#   define local_pthread_mutex_consistent       pthread_mutex_consistent
#endif /* _USE_ROBUST_NP */

#endif /* _HAVE_ROBUST_MUTEX */

#define UNUSED_PID (0) // 0 is "reserved" PID, so treat this as the avail/unused PID
#define PID_PROC_LEN (50)

#define CARD_MGR_EXTENSION_START (3) // API v3 is the first one to use the extension

#define SHM_OPEN_FLAG_EXCL              (O_RDWR | O_CREAT | O_EXCL)
#define SHM_OPEN_FLAG_RW                (O_RDWR)
#define CREATE_PERM                     (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)

#define LEGACY_NR_XPORTS                3
#define EXTENDED_NR_XPORTS              16
#define MAX_NR_XPORTS                   (LEGACY_NR_XPORTS + EXTENDED_NR_XPORTS)

/***** MACROs *****/

/* MACROs to simplify creating, opening, and closing shared resources */
#define CREATE_RESOURCE(_x,_fn)         do { (_x) = create_resource(_fn,sizeof(*_x)); } while (0)
#define OPEN_RESOURCE(_x,_fn)           do { (_x) = open_resource(_fn,sizeof(*_x)); } while (0)
#define CLOSE_RESOURCE(_x)              do { (_x) = close_resource(_x,sizeof(*_x)); } while (0)


/***** TYPEDEFS *****/

/** @brief Keeps track of mutex and owner */
typedef struct
{
    pthread_mutex_t mutex;   /// actual mutex
    pid_t owner;             /// owner of mutex
} card_mgr_mutex_t;

/** @brief Keeps track of probe and card mutex info */
typedef struct
{
    card_mgr_mutex_t probe_mutex;   /// probe mutex
    card_mgr_mutex_t card_mutex[LEGACY_SKIQ_MAX_NUM_CARDS]; // array of card mutexes
} card_mgr_mutexes_t;

/** @brief XPORT instance of a single card */
typedef struct
{
    uint64_t xport_uid;   /// unique XPORT ID
    uint8_t present;      /// flag set when XPORT is available
    uint8_t num_bytes;    /// # private bytes stored
    uint8_t xport_private[CARD_MGR_XPORT_PRIVATE_SIZE]; /// private XPORT storage
} xport_private_t;

/** @brief instance of a single card that has been registered by at least 1 XPORT */
typedef struct
{
    uint32_t serial;         /// serial #
    uint16_t hardware_vers;  /// hardware version info
    uint8_t registered;      /// flag set when card is registered
    xport_private_t xport_private[LEGACY_NR_XPORTS]; /// XPORT specific data (indexed by type)
} registered_card_t;

/** @brief array of all registered cards and API version of memory */
typedef struct
{
    uint8_t api_version; /// API version of data structure
    registered_card_t card_info[LEGACY_SKIQ_MAX_NUM_CARDS]; /// array of cards
} card_manager_t;

typedef struct
{
    skiq_part_info_t part_info[LEGACY_SKIQ_MAX_NUM_CARDS]; /// part info for each card
} card_mgr_ext_v3_t;

typedef struct
{
    card_mgr_mutex_t card_mutex[SKIQ_MAX_NUM_CARDS]; // array of card mutexes
    registered_card_t card_info[SKIQ_MAX_NUM_CARDS]; /// array of cards
    skiq_part_info_t part_info[SKIQ_MAX_NUM_CARDS]; /// part info for each card
} card_mgr_ext_v4_t;

typedef struct
{
    card_mgr_mutex_t fpga_programming_mutex;
} card_mgr_ext_v5_t;

typedef struct
{
    /// XPORT specific data (indexed by card and by type above skiq_xport_type_custom)
    xport_private_t extended_xport_private[SKIQ_MAX_NUM_CARDS][EXTENDED_NR_XPORTS];
} card_mgr_ext_v6_t;


/***** LOCAL VARIABLES *****/

static card_manager_t *p_card_mgr = NULL;
static card_mgr_mutexes_t *p_mutexes = NULL;
static card_mgr_ext_v3_t *p_card_mgr_ext_v3 = NULL;
static card_mgr_ext_v4_t *p_card_mgr_ext_v4 = NULL;
static card_mgr_ext_v5_t *p_card_mgr_ext_v5 = NULL;
static card_mgr_ext_v6_t *p_card_mgr_ext_v6 = NULL;

/** @brief variables to be used during the probing process
 * ::_probe_id:: used to hold transport information during probing
 * ::_probing:: boolean to signify whether probing is occuring,
 * used to follow different paths when necessary
 * ::probe_card:: local var that holds the card that will be used
 * for probing which is determined by the first card that is available */
skiq_xport_id_t _probe_id = SKIQ_XPORT_ID_INITIALIZER;
volatile bool _probing=false;
static uint8_t probe_card=0;


/***** LOCAL FUNCTIONS *****/

#if _HAVE_CARD_MGR

/******************************************************************************/
/* performs an (arbitrary) write to init_fd, then closes and unlinks the file */
static void release_init_fd_resources( int init_fd )
{
    /* release the init_fd resources */
    debug_print("releasing %s resources\n", CARD_MGR_INIT_STR);
    if( write( init_fd, CARD_MGR_INIT_STR, 1 ) < 0 )
    {
        skiq_error("failed to write initialize file, recommend reboot\n");
    }
    close( init_fd );
    if ( unlink( CARD_MGR_INIT_STR ) < 0 )
    {
        skiq_error("unlink() of %s failed: %s\n", CARD_MGR_INIT_STR, strerror(errno));
    }

} /* release_init_fd_resources */


/******************************************************************************/
/* opens and creates shared memory resource */
static int card_mgr_shm_create( const char *fn )
{
    mode_t prev_umask;
    int r;

    // Note: with O_EXCL from man page on shm_open:
    /*
      If  O_CREAT  was  also specified, and a shared memory object
      with the given name already exists, return  an  error.   The
      check  for  the existence of the object, and its creation if
      it does not exist, are performed atomically.
    */
    debug_print("calling shm_open(\"%s\") exclusively\n", fn);

    prev_umask = umask( S_IXUSR | S_IXGRP | S_IXOTH );
    {
        errno = 0;
        r = shm_open( fn, SHM_OPEN_FLAG_EXCL, CREATE_PERM );
        if (0 > r)
        {
            /* If the shm already exists, it must be leftover from a card_mgr_uninit that didn't
             * know about this extension. Since the caller is requesting shm creation, it must also
             * have permission to unlink it first if it exists.  After successful unlinking, try
             * once more to create the shm exclusively */
            if ( errno == EEXIST )
            {
                debug_print("\"%s\" already exists, unlinking because it needs recreating\n", fn);

                errno = 0;
                r = shm_unlink( fn );
                if ( 0 == r )
                {
                    errno = 0;
                    r = shm_open( fn, SHM_OPEN_FLAG_EXCL, CREATE_PERM );
                    if ( 0 > r )
                    {
                        skiq_error("Failed to create shared memory segment '%s' after unlinking "
                                   "(%d: '%s')\n", fn, errno, strerror(errno));
                    }
                }
                else
                {
                    skiq_error("Failed to unlink existing shared memory segment '%s' (%d: '%s')\n",
                               fn, errno, strerror(errno));
                }
            }
            else
            {
                skiq_error("Failed to create shared memory segment '%s' (%d: '%s')\n", fn, errno,
                           strerror(errno));
            }
        }
    }
    umask( prev_umask );

    /* note that errno is potentially lost since umask may modify it */
    return ( r );

} /* card_mgr_shm_create */


/******************************************************************************/
/* opens an existing shared memory resource */
static int card_mgr_shm_open( const char *fn )
{
    int status = 0;

    debug_print("calling shm_open(\"%s\")\n", fn);

    errno = 0;
    status = shm_open( fn, SHM_OPEN_FLAG_RW, CREATE_PERM );
    if (0 > status)
    {
        if (ENOENT != errno)
        {
            skiq_error("Failed to open shared memory segment '%s' (%d: '%s')\n", fn, errno,
                strerror(errno));
        }
    }

    return (status);

} /* card_mgr_shm_open */


/******************************************************************************/
/* destroy / unlink shared memory resource */
static int card_mgr_shm_destroy( const char *fn )
{
    int status = 0;

    debug_print("attempting shm_unlink(\"%s\")\n", fn);

    errno = 0;
    status = shm_unlink( fn );
    if ( status < 0 )
    {
        skiq_error("Destroying shared memory (%s) failed (%d: '%s')\n", fn, errno, strerror(errno));
    }

    return ( status );

} /* card_mgr_shm_destroy */


/******************************************************************************/
/* memory map an open file descriptor */
static void * card_mgr_mmap( int size,
                             int fd )
{
    void *ptr = NULL;

    errno = 0;
    ptr = mmap( NULL,
                size,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                fd,
                0 );
    if ( ptr == MAP_FAILED )
    {
        skiq_error("Mapping shared resource of size %d into memory failed (%d: %s)", size,
            errno, strerror(errno));
        ptr = NULL;
    }

    return (ptr);
}


/******************************************************************************/
static void *create_resource( const char *fn,
                              int resource_size )
{
    int fd;
    void *ptr = NULL;

    fd = card_mgr_shm_create( fn );
    if ( fd >= 0 )
    {
        int status = 0;

        errno = 0;
        status = ftruncate( fd, resource_size );
        if (status != 0)
        {
            skiq_error("Failed to truncate shared resource '%s' to size %d (%d: '%s')\n",
                fn, resource_size, errno, strerror(errno));
        }
        else
        {
            ptr = card_mgr_mmap( resource_size, fd );
        }
        close( fd );
    }

    return ptr;
}


/******************************************************************************/
static void *open_resource( const char *fn,
                            int resource_size )
{
    int fd;
    void *ptr = NULL;

    fd = card_mgr_shm_open( fn );
    if ( fd >= 0 )
    {
        ptr = card_mgr_mmap( resource_size, fd );
        close( fd );
    }

    return ptr;
}


/******************************************************************************/
static void *close_resource( void *p_resource,
                             int resource_size )
{
    if ( p_resource != NULL )
    {
        int status = 0;

        errno = 0;
        status = msync( p_resource, resource_size, MS_SYNC );
        if (status != 0)
        {
            skiq_error("Failed to sync resource of size %d (%d: '%s'); continuing...\n",
                resource_size, errno, strerror(errno));
        }

        errno = 0;
        status = munmap( p_resource, resource_size );
        if (status != 0)
        {
            skiq_error("Failed to unmap resource of size %d (%d: '%s'); continuing...\n",
                resource_size, errno, strerror(errno));
        }
    }

    return NULL;
}


/******************************************************************************/
static void create_all_resources( void )
{
    CREATE_RESOURCE( p_card_mgr, CARD_MGR_STR );
    CREATE_RESOURCE( p_mutexes, CARD_MGR_MUTEXES_STR );
    CREATE_RESOURCE( p_card_mgr_ext_v3, CARD_MGR_EXT_V3_STR );
    CREATE_RESOURCE( p_card_mgr_ext_v4, CARD_MGR_EXT_V4_STR );
    CREATE_RESOURCE( p_card_mgr_ext_v5, CARD_MGR_EXT_V5_STR );
    CREATE_RESOURCE( p_card_mgr_ext_v6, CARD_MGR_EXT_V6_STR );
}


/******************************************************************************/
static void open_all_resources( void )
{
    OPEN_RESOURCE(p_mutexes, CARD_MGR_MUTEXES_STR);
    OPEN_RESOURCE(p_card_mgr, CARD_MGR_STR);
    OPEN_RESOURCE(p_card_mgr_ext_v3, CARD_MGR_EXT_V3_STR);
    OPEN_RESOURCE(p_card_mgr_ext_v4, CARD_MGR_EXT_V4_STR);
    OPEN_RESOURCE(p_card_mgr_ext_v5, CARD_MGR_EXT_V5_STR);
    OPEN_RESOURCE(p_card_mgr_ext_v6, CARD_MGR_EXT_V6_STR);
}


/******************************************************************************/
static void close_all_resources( void )
{
    CLOSE_RESOURCE( p_mutexes );
    CLOSE_RESOURCE( p_card_mgr );
    CLOSE_RESOURCE( p_card_mgr_ext_v3 );
    CLOSE_RESOURCE( p_card_mgr_ext_v4 );
    CLOSE_RESOURCE( p_card_mgr_ext_v5 );
    CLOSE_RESOURCE( p_card_mgr_ext_v6 );
}

#endif  /* _HAVE_CARD_MGR */


/******************************************************************************/
static void card_mutex_initialize( card_mgr_mutex_t *p_mutex )
{
    if ( p_mutex != NULL )
    {
        int status = 0;
        pthread_mutexattr_t attr;

        status = pthread_mutexattr_init( &attr );
        if (0 != status)
        {
            skiq_error("Failed to initialize mutex attributes for mutex %p (status %d); attempting"
                " to continue...\n", p_mutex, status);
        }
        else
        {
            status = pthread_mutexattr_setpshared( &attr, PTHREAD_PROCESS_SHARED );
            if ((0 != status) && (ENOTSUP != status) && (ENOSYS != status))
            {
                skiq_error("Failed to set process-shared attributes for mutex %p (status %d);"
                    " attempting to continue...\n", p_mutex, status);
            }
        }

        status = pthread_mutex_init( &p_mutex->mutex, &attr);
        if (0 != status)
        {
            skiq_error("Failed to initialize mutex %p (status %d)\n", p_mutex, status);
        }

        status = pthread_mutexattr_destroy( &attr );
        if (0 != status)
        {
            skiq_error("Failed to destroy mutex attributes for mutex %p (status %d); attempting to"
                " continue...\n", p_mutex, status);
        }

        p_mutex->owner = UNUSED_PID;
    }
}


/******************************************************************************/
#if _HAVE_ROBUST_MUTEX
static void card_mutex_initialize_robust( card_mgr_mutex_t *p_mutex )
{
    if ( p_mutex != NULL )
    {
        int status = 0;
        pthread_mutexattr_t attr;

        status = pthread_mutexattr_init( &attr );
        if (0 != status)
        {
            skiq_error("Failed to initialize mutex attributes for mutex %p (status %d); attempting"
                " to continue...\n", p_mutex, status);
        }
        else
        {
            status = pthread_mutexattr_setpshared( &attr, PTHREAD_PROCESS_SHARED );
            if ((0 != status) && (ENOTSUP != status) && (ENOSYS != status))
            {
                skiq_error("Failed to set process-shared attributes for mutex %p (status %d);"
                    " attempting to continue...\n", p_mutex, status);
            }

            status = local_pthread_mutexattr_setrobust( &attr, PTHREAD_MUTEX_ROBUST_NP );
            if (0 != status)
            {
                skiq_error("Failed to make mutex %p robust (status %d); attempting to"
                    " continue...\n", p_mutex, status);
            }
        }

        status = pthread_mutex_init( &p_mutex->mutex, &attr);
        if (0 != status)
        {
            skiq_error("Failed to initialize mutex %p (status %d)\n", p_mutex, status);
        }

        status = pthread_mutexattr_destroy( &attr );
        if (0 != status)
        {
            skiq_error("Failed to destroy mutex attributes for mutex %p (status %d); attempting to"
                " continue...\n", p_mutex, status);
        }

        p_mutex->owner = UNUSED_PID;
    }
}
#else
#warning No robust pthread support available for target
static inline void card_mutex_initialize_robust( card_mgr_mutex_t *p_mutex )
{
    card_mutex_initialize( p_mutex );
}
#endif


/******************************************************************************/
static void clear_xport_private( xport_private_t *p_xport_priv )
{
    p_xport_priv->xport_uid = SKIQ_XPORT_UID_INVALID;
    p_xport_priv->present = 0;
    p_xport_priv->num_bytes = 0;
    memset(p_xport_priv->xport_private, 0, CARD_MGR_XPORT_PRIVATE_SIZE * sizeof(uint8_t));
}


/******************************************************************************/
static void card_info_initialize( registered_card_t *p_card_info )
{
    if ( p_card_info != NULL )
    {
        skiq_xport_type_t type;

        p_card_info->serial = 0;
        p_card_info->hardware_vers = 0;
        p_card_info->registered = 0;

        /* only clear out xport private data for legacy xport types */
        for ( type = 0; type < LEGACY_NR_XPORTS; type++ )
        {
            clear_xport_private( &(p_card_info->xport_private[type]) );
        }
    }
}


/******************************************************************************/
static void part_info_initialize( skiq_part_info_t *p_part_info )
{
    if ( p_part_info != NULL )
    {
        memset( p_part_info->number_string, 0, SKIQ_PART_NUM_STRLEN );
        memset( p_part_info->revision_string, 0, SKIQ_REVISION_STRLEN );
        memset( p_part_info->variant_string, 0, SKIQ_VARIANT_STRLEN );
    }
}


/******************************************************************************/
static void part_info_copy( skiq_part_info_t *p_part_info_dest,
                            skiq_part_info_t *p_part_info_src )
{
    if ( ( p_part_info_dest != NULL ) && ( p_part_info_src != NULL ) )
    {
        memcpy( p_part_info_dest->number_string,
                p_part_info_src->number_string,
                SKIQ_PART_NUM_STRLEN );
        memcpy( p_part_info_dest->revision_string,
                p_part_info_src->revision_string,
                SKIQ_REVISION_STRLEN );
        memcpy( p_part_info_dest->variant_string,
                p_part_info_src->variant_string,
                SKIQ_VARIANT_STRLEN );
    }
}


#if _HAVE_CARD_MGR
/******************************************************************************/
/* depending on the card index, return a card_mgr_mutex_t reference from the
 * legacy shm or the V4 shm */
static card_mgr_mutex_t *get_card_mutex( uint8_t card )
{
    card_mgr_mutex_t *p_mutex = NULL;

    if ( card < LEGACY_SKIQ_MAX_NUM_CARDS )
    {
        p_mutex = &(p_mutexes->card_mutex[card]);
    }
    else if ( card < SKIQ_MAX_NUM_CARDS )
    {
        p_mutex = &(p_card_mgr_ext_v4->card_mutex[card]);
    }

    return p_mutex;
}
#endif  /* _HAVE_CARD_MGR */


/******************************************************************************/
/* depending on the card index, return a registered_card_t reference from the
 * legacy shm or the V4 shm */
static registered_card_t *get_card_info( uint8_t card )
{
    registered_card_t *p_card = NULL;

    if ( card < LEGACY_SKIQ_MAX_NUM_CARDS )
    {
        p_card = &(p_card_mgr->card_info[card]);
    }
    else if ( card < SKIQ_MAX_NUM_CARDS )
    {
        if ( p_card_mgr_ext_v4 != NULL )
        {
            p_card = &(p_card_mgr_ext_v4->card_info[card]);
        }
    }

    return p_card;
}

/******************************************************************************/
/* depending on the card index, return a skiq_part_info_t reference from the
 * legacy shm or the V4 shm */
static skiq_part_info_t *get_part_info( uint8_t card )
{
    skiq_part_info_t *p_part_info = NULL;

    if ( card < LEGACY_SKIQ_MAX_NUM_CARDS )
    {
        if ( p_card_mgr_ext_v3 != NULL )
        {
            p_part_info = &(p_card_mgr_ext_v3->part_info[card]);
        }
    }
    else if ( card < SKIQ_MAX_NUM_CARDS )
    {
        if ( p_card_mgr_ext_v4 != NULL )
        {
            p_part_info = &(p_card_mgr_ext_v4->part_info[card]);
        }
    }

    return p_part_info;
}

/******************************************************************************/
/* depending on the xport type, return a xport_private_t reference from the
 * legacy shm or the V6 shm */
static xport_private_t *get_xport_private( uint8_t card,
                                           skiq_xport_type_t type )
{
    xport_private_t *p_xport_private = NULL;

    if ( ( card < SKIQ_MAX_NUM_CARDS ) &&
         ( type < MAX_NR_XPORTS ) )
    {
        if ( type < LEGACY_NR_XPORTS )
        {
            registered_card_t *card_info;

            card_info = get_card_info( card );
            if ( card_info != NULL )
            {
                p_xport_private = &(card_info->xport_private[type]);
            }
        }
        else if ( ( type - LEGACY_NR_XPORTS ) < EXTENDED_NR_XPORTS )
        {
            if ( p_card_mgr_ext_v6 != NULL )
            {
                p_xport_private = &(p_card_mgr_ext_v6->extended_xport_private[card][(type - LEGACY_NR_XPORTS)]);
            }
        }
    }

    return p_xport_private;
}

/**
    @brief  Copy xport private data from one card to another

    @param[in]  old_card    The old card index
    @param[in]  new_card    The new card index
    @param[in]  type        The transport type
*/
static void copy_xport_private( uint8_t old_card, uint8_t new_card, skiq_xport_type_t type )
{
    xport_private_t *p_xport_private_old = NULL;
    xport_private_t *p_xport_private_new = NULL;

    p_xport_private_old = get_xport_private(old_card, type);
    p_xport_private_new = get_xport_private(new_card, type);

    if ( ( p_xport_private_old != NULL ) &&
         ( p_xport_private_new != NULL ) )
    {
        p_xport_private_new->xport_uid = p_xport_private_old->xport_uid;
        p_xport_private_new->present = p_xport_private_old->present;
        p_xport_private_new->num_bytes = p_xport_private_old->num_bytes;
        memcpy( &(p_xport_private_new->xport_private[0]), &(p_xport_private_old->xport_private[0]),
            p_xport_private_old->num_bytes );
    }
}

/******************************************************************************/
static void v4_initialize( card_mgr_ext_v4_t *p_ext )
{
    uint8_t card;

    if ( p_ext != NULL )
    {
        /* even though V4 doesn't track cards between 0 and
         * LEGACY_SKIQ_MAX_NUM_CARDS-1, initialize the whole structure */
        for (card = 0; card < SKIQ_MAX_NUM_CARDS; card++ )
        {
            card_mutex_initialize( &(p_ext->card_mutex[card]) );
            card_info_initialize( &(p_ext->card_info[card]) );
            part_info_initialize( &(p_ext->part_info[card]) );
        }
    }
}

/******************************************************************************/
static void v5_initialize( card_mgr_ext_v5_t *p_ext )
{
    if ( p_ext != NULL )
    {
        card_mutex_initialize_robust( &(p_ext->fpga_programming_mutex) );
    }
}

/******************************************************************************/
static void v6_initialize( card_mgr_ext_v6_t *p_ext )
{
    if ( p_ext != NULL )
    {
        uint8_t card, type_extended;

        for (card = 0; card < SKIQ_MAX_NUM_CARDS; card++ )
        {
            for ( type_extended = 0; type_extended < EXTENDED_NR_XPORTS; type_extended++ )
            {
                clear_xport_private( &(p_ext->extended_xport_private[card][type_extended]) );
            }
        }
    }
}


/******************************************************************************/
#if _HAVE_CARD_MGR
static int32_t _card_mgr_create_ext( uint8_t ext_vers )
{
    int32_t status=-1;
    uint8_t i=0;

    TRACE_ENTER;
    debug_print("ext_vers=%" PRIu8 "\n", ext_vers);

    if( ext_vers < CARD_MGR_EXTENSION_START )
    {
        skiq_error("Invalid card manager extension specified %" PRIu8 "\n", ext_vers);
    }
    else
    {
        if ( ext_vers == 3 )
        {
            CREATE_RESOURCE( p_card_mgr_ext_v3, CARD_MGR_EXT_V3_STR );

            // if we got the memory OK, initialize everything
            if ( p_card_mgr_ext_v3 != NULL )
            {
                debug_print("Creating v3 shared memory extension\n");
                for ( i = 0; i < LEGACY_SKIQ_MAX_NUM_CARDS; i++ )
                {
                    part_info_initialize( &(p_card_mgr_ext_v3->part_info[i]) );
                }

                card_mgr_probe_unlock();
                status = card_mgr_probe( );
            }
            else
            {
                status = -1;
            }

        }
        else if ( ext_vers == 4 )
        {
            CREATE_RESOURCE( p_card_mgr_ext_v4, CARD_MGR_EXT_V4_STR );

            // if we got the memory OK, initialize everything
            if ( p_card_mgr_ext_v4 != NULL )
            {
                v4_initialize( p_card_mgr_ext_v4 );

                card_mgr_probe_unlock();
                status = card_mgr_probe( );
            }
            else
            {
                status = -1;
            }
        }
        else if ( ext_vers == 5 )
        {
            CREATE_RESOURCE( p_card_mgr_ext_v5, CARD_MGR_EXT_V5_STR );

            // if we got the memory OK, initialize everything
            if ( p_card_mgr_ext_v5 != NULL )
            {
                v5_initialize( p_card_mgr_ext_v5 );

                card_mgr_probe_unlock();
                status = card_mgr_probe( );
            }
            else
            {
                status = -1;
            }
        }
        else if ( ext_vers == 6 )
        {
            CREATE_RESOURCE( p_card_mgr_ext_v6, CARD_MGR_EXT_V6_STR );

            // if we got the memory OK, initialize everything
            if ( p_card_mgr_ext_v6 != NULL )
            {
                v6_initialize( p_card_mgr_ext_v6 );

                card_mgr_probe_unlock();
                status = card_mgr_probe( );
            }
            else
            {
                status = -1;
            }
        }

        /* display status of extension initialization */
        if ( status == 0 )
        {
            skiq_info("Initialization of extension %" PRIu8 " complete!\n", ext_vers);
        }
        else
        {
            skiq_info("Initialization of extension %" PRIu8 " failed with status %" PRId32 "\n",
                      ext_vers, status);
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);

} /* _card_mgr_create_ext */
#endif /* _HAVE_CARD_MGR */

/******************************************************************************/
#if _HAVE_CARD_MGR
static int32_t _card_mgr_init( void )
{
    int32_t status = -1;

    TRACE_ENTER;

    if ( (NULL != p_mutexes) ||
         (NULL != p_card_mgr) ||
         (NULL != p_card_mgr_ext_v3) ||
         (NULL != p_card_mgr_ext_v4) ||
         (NULL != p_card_mgr_ext_v5) ||
         (NULL != p_card_mgr_ext_v6) )
    {
        /* if any reference is non-NULL, init has already been called, don't
         * perform initialization again! */
        status = 0;
    }
    else
    {
        // initialize our local variables
        _probe_id.xport_uid = SKIQ_XPORT_UID_INVALID;
        _probe_id.type = skiq_xport_type_unknown;

        /* open all known resources */
        open_all_resources();

        if ( ( p_mutexes != NULL ) && ( p_card_mgr != NULL ) )
        {
            /* handle the extensions separately, they may not be available, so
             * consider valid p_mutexes and p_card_mgr references to be a success */
            status = 0;
        }

        if ( status != 0 )
        {
            // clean up resources if partially allocated
            close_all_resources();
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
static int32_t _card_mgr_init( void ) { return 0; }
#endif  /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR

/******************************************************************************/
#if _HAVE_ROBUST_MUTEX
static int32_t robust_mutex_lock( card_mgr_mutex_t *p_mutex )
{
    int32_t status = -1;

    TRACE_ENTER;
    TRACE_ARGS("p_mutex=%p\n", p_mutex);

    if ( p_mutex != NULL )
    {
        status = pthread_mutex_lock( &p_mutex->mutex );

        // if we got the lock, update the PID
        if ( status == 0 )
        {
            p_mutex->owner = getpid();
        }
        else if ( status == EOWNERDEAD )
        {
            skiq_warning("Process %" PRIdMAX " had owned lock but no longer exists, making "
                         "consistent\n", (intmax_t) p_mutex->owner);
            p_mutex->owner = getpid();
            status = local_pthread_mutex_consistent( &p_mutex->mutex );
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}
#endif

static int32_t _trylock( card_mgr_mutex_t *p_card_mutex )
{
    int32_t status = 0;
    FILE *p_pid = NULL;
    char buf[PID_PROC_LEN];

    status = pthread_mutex_trylock( &p_card_mutex->mutex);

    // if we got the lock, update the PID
    if( status == 0 )
    {
        p_card_mutex->owner = getpid();
    }
    else if ( status == EBUSY )
    {
        snprintf(buf, PID_PROC_LEN, "/proc/%" PRIdMAX, (intmax_t) p_card_mutex->owner);
        if ( ( p_pid = fopen(buf, "r") ) == NULL )
        {
            skiq_warning( "Process %" PRIdMAX " had owned lock but no longer exists, forcing "
                          "unlock\n", (intmax_t) p_card_mutex->owner);

            // force unlock and try again
            pthread_mutex_unlock( &(p_card_mutex->mutex) );
            p_card_mutex->owner = UNUSED_PID;
        }
        else
        {
            fclose(p_pid);
        }
    }
    else
    {
        status = -EPROTO;
    }

    return (status);
}


/******************************************************************************/
static int32_t mutex_trylock( card_mgr_mutex_t *p_mutex, pid_t *p_owner )
{
    int32_t status = -1;
    bool retry_lock;

    TRACE_ENTER;
    TRACE_ARGS("p_mutex=%p,p_owner=%p\n", p_mutex, p_owner);

    do
    {
        retry_lock = false;
        if ( p_mutex != NULL )
        {
            status = _trylock( p_mutex );

            /* if the lock's owner was dead (e.g. status is non-zero and owner
             * is UNUSED_PID, try again */
            if ( ( status != 0 ) && ( p_mutex->owner == UNUSED_PID ) )
            {
                retry_lock = true;
            }
            else
            {
                /* set the PID for whoever owns the mutex right now */
                *p_owner = p_mutex->owner;
            }
        }

    } while (retry_lock);

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
static int32_t mutex_unlock( card_mgr_mutex_t *p_mutex )
{
    int32_t status=-1;
    pid_t caller_pid = getpid();

    /* unlock only if the owner matches the calling PID */
    if ( caller_pid == p_mutex->owner )
    {
        /* disassociate owner, then unlock the mutex */
        p_mutex->owner = UNUSED_PID;
        status = pthread_mutex_unlock( &(p_mutex->mutex) );
    }
    else
    {
        skiq_error("Card manager mutex unlocking attempted by non-owner (caller = %" PRIdMAX
                   ", owner=%" PRIdMAX ")\n", (intmax_t) caller_pid, (intmax_t) p_mutex->owner);
    }

    return (status);
}

#endif  /* _HAVE_CARD_MGR */


/******************************************************************************/
static void info_initialize( uint8_t card )
{
    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 "\n", card);

    card_info_initialize( get_card_info( card ) );
    part_info_initialize( get_part_info( card ) );

    TRACE_EXIT;
}


/******************************************************************************/
/****************************** GLOBAL FUNCTIONS ******************************/
/******************************************************************************/


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_create_card_mgr( void )
{
    int32_t status = -1;
    int init_fd = -1;
    uint8_t i = 0;

    TRACE_ENTER;

    init_fd = open( CARD_MGR_INIT_STR,
                    O_RDWR | O_CREAT | O_EXCL,
                    CREATE_PERM );

    if( init_fd >= 0 )
    {
        skiq_info("Performing detection of cards\n");

        create_all_resources();

        debug_print("=== Resource check: p_card_mgr = %p p_mutexes = %p p_card_mgr_ext_v3 = %p"
                    " p_card_mgr_ext_v4 = %p p_card_mgr_ext_v5 = %p p_card_mgr_ext_v6 = %p\n",
                    p_card_mgr, p_mutexes, p_card_mgr_ext_v3, p_card_mgr_ext_v4, p_card_mgr_ext_v5,
                    p_card_mgr_ext_v6);

        if ( ( p_card_mgr != NULL ) &&
             ( p_mutexes != NULL ) &&
             ( p_card_mgr_ext_v3 != NULL ) &&
             ( p_card_mgr_ext_v4 != NULL ) &&
             ( p_card_mgr_ext_v5 != NULL ) &&
             ( p_card_mgr_ext_v6 != NULL ) )
        {
            // initialize to 0 cards and the appropriate API version
            p_card_mgr->api_version = CARD_MGR_API_VERSION;

            /* initialize the 'probe' mutex */
            card_mutex_initialize( &p_mutexes->probe_mutex );

            // go through all the cards and initialize the structures
            for ( i = 0; i < LEGACY_SKIQ_MAX_NUM_CARDS; i++ )
            {
                card_mutex_initialize( &(p_mutexes->card_mutex[i]) );
                card_info_initialize( &(p_card_mgr->card_info[i]) );
                part_info_initialize( &(p_card_mgr_ext_v3->part_info[i]) );
            }

            /* initialize V4 structure */
            v4_initialize( p_card_mgr_ext_v4 );

            /* initialize V5 structure */
            v5_initialize( p_card_mgr_ext_v5 );

            /* initialize V6 structure */
            v6_initialize( p_card_mgr_ext_v6 );

            /* finally probe for hardware */
            status = card_mgr_probe();

            /* close all shared resources now that they have been created /
             * initialized / populated */
            close_all_resources();

            skiq_info("Sidekiq card detection completed successfully!\n");
        }
        else
        {
            status = -1;
            skiq_error("Allocation of shared resources failed!\n");
        }

        release_init_fd_resources( init_fd );
    }
    else
    {
        // init file exists
        skiq_info("Unable to perform initialization, exiting creation of shared resources\n");
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_create_card_mgr( void )
{
    int32_t status=-1;

    p_card_mgr = (card_manager_t *)malloc(sizeof(card_manager_t));
    p_card_mgr_ext_v3 = (card_mgr_ext_v3_t *)malloc(sizeof(card_mgr_ext_v3_t));
    p_card_mgr_ext_v4 = (card_mgr_ext_v4_t *)malloc(sizeof(card_mgr_ext_v4_t));
    p_card_mgr_ext_v5 = (card_mgr_ext_v5_t *)malloc(sizeof(card_mgr_ext_v5_t));
    p_card_mgr_ext_v6 = (card_mgr_ext_v6_t *)malloc(sizeof(card_mgr_ext_v6_t));

    if ( ( p_card_mgr != NULL ) &&
         ( p_card_mgr_ext_v3 != NULL ) &&
         ( p_card_mgr_ext_v4 != NULL ) &&
         ( p_card_mgr_ext_v5 != NULL ) &&
         ( p_card_mgr_ext_v6 != NULL ) )
    {
        uint8_t i;

        // initialize to 0 cards and the appropriate API version
        p_card_mgr->api_version = CARD_MGR_API_VERSION;

        // go through all the cards and initialize the structures
        for ( i = 0; i < LEGACY_SKIQ_MAX_NUM_CARDS; i++ )
        {
            card_info_initialize( &(p_card_mgr->card_info[i]) );
            part_info_initialize( &(p_card_mgr_ext_v3->part_info[i]) );
        }

        /* initialize V4 structure */
        v4_initialize( p_card_mgr_ext_v4 );

        /* initialize V5 structure */
        v5_initialize( p_card_mgr_ext_v5 );

        /* initialize V6 structure */
        v6_initialize( p_card_mgr_ext_v6 );

        status = card_mgr_probe();
    }
    else
    {
        status = -1;
    }

    if ( status != 0 )
    {
        FREE_IF_NOT_NULL(p_card_mgr);
        FREE_IF_NOT_NULL(p_card_mgr_ext_v3);
        FREE_IF_NOT_NULL(p_card_mgr_ext_v4);
        FREE_IF_NOT_NULL(p_card_mgr_ext_v5);
        FREE_IF_NOT_NULL(p_card_mgr_ext_v6);
    }

    return (status);
}
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
int32_t card_mgr_probe( void )
{
    int32_t status = 0;
    skiq_xport_type_t type;
    pid_t probe_owner;

    TRACE_ENTER;

    // try to get the probe lock
    status = card_mgr_probe_trylock( &probe_owner );
    if ( status == 0 )
    {
        _probing = true;

        // if we got here, we must already have the probe lock...assume it's managed elsewhere
        for ( type = skiq_xport_type_pcie; ( type < skiq_xport_type_max ) && ( status == 0 ); type++ )
        {
            status = skiq_xport_card_probe( type );
            if ( status != 0 )
            {
                _skiq_log(SKIQ_LOG_WARNING, "%s transport probe error occurred with status %d\n", xport_type_cstr(type), status);

                /* on platforms where libusb_init() fails because there is no
                 * USB support, the probe will return an error, but we have to
                 * keep probing.  It would be nice to know the difference
                 * between no USB support and a failed USB probe.
                 *
                 * so set status to 0 intentionally to let the show go on.
                 *
                 * even if status is not set to 0, the call to card_mgr_probe_unlock() overwrites it
                 * anyway.
                 */
                status = 0;
            }
        }

        status = card_mgr_probe_unlock();
        if ( status != 0 )
        {
            skiq_error("Card manager probe unlock error occurred with status %" PRId32 "\n",
                       status);
        }

        _probing = false;
    }
    else
    {
        skiq_error("Card manager unable to obtain probe lock, locked by another process (PID=%"
                   PRIdMAX ")\n", (intmax_t) probe_owner);
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_destroy_card_mgr(void)
{
    int32_t status=0;

    TRACE_ENTER;

    // @todo check for locked mutexes before unlinking??

    card_mgr_shm_destroy( CARD_MGR_MUTEXES_STR );
    card_mgr_shm_destroy( CARD_MGR_STR );
    card_mgr_shm_destroy( CARD_MGR_EXT_V3_STR );
    card_mgr_shm_destroy( CARD_MGR_EXT_V4_STR );
    card_mgr_shm_destroy( CARD_MGR_EXT_V5_STR );
    card_mgr_shm_destroy( CARD_MGR_EXT_V6_STR );

    p_card_mgr = NULL;
    p_mutexes = NULL;
    p_card_mgr_ext_v3 = NULL;
    p_card_mgr_ext_v4 = NULL;
    p_card_mgr_ext_v5 = NULL;
    p_card_mgr_ext_v6 = NULL;

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_destroy_card_mgr(void) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
int32_t card_mgr_xport_probe_card( skiq_xport_id_t *p_xport_id )
{
    int32_t status=-1;

    TRACE_ENTER;
    TRACE_ARGS("p_xport_id=%p\n", p_xport_id);

    if( _probing == true )
    {
        status=0;
        // save the xport ID info for the probe
        _probe_id.xport_uid = p_xport_id->xport_uid;
        _probe_id.type = p_xport_id->type;
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
#if _HAVE_CARD_MGR
/**
    @brief  Check if card initialization is currently happening by another process
            and, if so, wait for it to complete

    @param[in]  timeout_ms      The amount of time to wait for a "initialization complete"
                                notification; if `-1` then wait indefinitely

    @return 0 on success, else a negative errno
    @retval -EFAULT         if there was a major error while attempting to check for
                            card initialization
    @retval -ETIMEDOUT      if waiting for the "initialization complete" notification
                            timed out
*/
static int32_t
wait_for_initialization(int timeout_ms)
{
/**
    @brief  The size of the inotify read buffer (this value represents a sufficient
            size for at least one event, as specified by the `inotify()` man page)
*/
#define NOTIFY_BUFFER_SIZE      (sizeof(struct inotify_event) + NAME_MAX + 1)

    int32_t status = 0;
    FILE *p_init_file = NULL;

    errno = 0;
    p_init_file = fopen( CARD_MGR_INIT_STR, "r" );
    if( p_init_file != NULL )
    {
        /* The temporary file can be opened, hence another process must be trying to initialize */

        int inotify_fd = -1;

        /*
            This file handle was only opened to determine if the file exists and isn't needed any
            more
        */
        fclose(p_init_file);
        p_init_file = NULL;

        skiq_info("Card Manager: waiting for card detection to complete...\n");

        errno = 0;
        inotify_fd = inotify_init();
        if ( inotify_fd != -1 )
        {
            int wd = -1;

            errno = 0;
            wd = inotify_add_watch( inotify_fd,
                                    CARD_MGR_INIT_STR,
                                    IN_DELETE | IN_MODIFY );
            if( wd == -1 )
            {
                /* Failed to add watch on this file */

                if (errno == ENOENT)
                {
                    /*
                        The initialization file (CARD_MGR_INIT_STR) doesn't seem to exist, so
                        it was deleted between the initial step to check if the file existed
                        and here. Let's count it as a success.
                    */

                    debug_print("Card Manager: failed to open watch file; assuming it was"
                        " already deleted\n");
                    skiq_info("Card Manager: card detection complete, continuing"
                        " initialization\n");
                }
                else
                {
                    skiq_error("Card Manager: failed before waiting for another libsidekiq"
                        " process to finish initialization (%d: '%s'); critical error, cannot"
                        " continue\n", errno, strerror(errno));
                    status = -EFAULT;
                    hal_critical_exit( status );
                }
            }
            else
            {
                /* Wait for an inotify event to determine if it's safe to continue. */

                int result = 0;
                struct pollfd poll_fd;

                poll_fd.fd = inotify_fd;
                poll_fd.events = POLLIN;
                poll_fd.revents = 0;

                errno = 0;
                result = poll(&poll_fd, 1, timeout_ms);
                if (result > 0)
                {
                    /* An event was discovered */

/**
                    @todo   This code attempts to read the event but then does nothing with it...
                            so is it needed? Even if the event is read and we see one of the events
                            we're waiting for, does that give us any additional information? Keeping
                            this code for now to flush inotify events but treating a failure to
                            read as non-fatal...
*/
                    char buf[NOTIFY_BUFFER_SIZE] = { [0 ... (NOTIFY_BUFFER_SIZE-1)] = '\0' };

                    debug_print("Card Manager: inotify event discovered, attempting to read"
                        " event.\n");

                    errno = 0;
                    if( read(inotify_fd, buf, NOTIFY_BUFFER_SIZE) > 0 )
                    {
                        skiq_info("Card Manager: card detection complete, continuing"
                            " initialization\n");
                    }
                    else
                    {
                        /* Is this really an error? This is more an issue with inotify... */
                        debug_print("Card Manager: failed to read inotify event (%d: '%s')\n",
                            errno, strerror(errno));
                    }
                }
                else if (result == 0)
                {
                    /* Timed out waiting for an event */

                    skiq_error("Card Manager: timed out while waiting for another libsidekiq"
                        " process to finish initialization; critical error, cannot continue\n");
                    status = -ETIMEDOUT;
                    hal_critical_exit( status );
                }
                else
                {
                    /* An error happened while waiting for an event */

                    skiq_error("Card Manager: failed while waiting for another libsidekiq process"
                        " to finish initialization (%d: '%s'); critical error, cannot continue\n",
                        errno, strerror(errno));
                    /* Mask off the error code so we can control the possible return values */
                    status = -EBADF;
                    hal_critical_exit( status );
                }

                /*
                    Attempt to clean up the "watch" descriptor (created with `inotify_add_watch()`),
                    though we don't really care about the result as we can't do anything about
                    it
                */
                (void) inotify_rm_watch(inotify_fd, wd);
                wd = -1;
            }

            close(inotify_fd);
            inotify_fd = -1;
        }
        else
        {
            skiq_error("Card Manager: failed to set up wait for another libsidekiq process"
                " to finish initialization (%d: '%s'); critical error, cannot continue\n",
                errno, strerror(errno));
            status = -EFAULT;
            hal_critical_exit( status );
        }
    }
    else
    {
        /* The temporary file can't be opened... */

        if (errno == ENOENT)
        {
            /* The file doesn't exist - it's OK to continue */

            debug_print("Card Manager: initialization temporary file cannot be opened; assuming"
                " that initialization is free to continue\n");
        }
        else
        {
            /*
                Another error occurred... at this point we're not sure if another card is
                attempting to initialize, but assume that one isn't
            */
            skiq_warning("Card Manager: cannot determine if another libsidekiq process is"
                " initializing; attempting to continue...\n");
        }
    }

    return (status);
#undef NOTIFY_BUFFER_SIZE
}

/**
    @brief  Check the API version of the Card Manager and upgrade it if it doesn't meet
            this API version

    @return 0 on success, else a negative errno
*/
static int32_t
upgrade_api(void)
{
    int32_t status = 0;
    pid_t pid;

    if( ((p_card_mgr->api_version) < CARD_MGR_API_VERSION) )
    {
        skiq_info("Card Manager: Previous API version (%" PRIu8 " < %" PRIu8 ") detected,"
            " attempting API update\n", p_card_mgr->api_version, CARD_MGR_API_VERSION);

        // make sure there are no locks and then re-create the card manager
        status = card_mgr_probe_trylock( &pid );
        if ( status == 0 )
        {
            uint8_t i=0;
            uint8_t j=0;

            for( i=0; ((i<LEGACY_SKIQ_MAX_NUM_CARDS) && (status == 0)); i++ )
            {
                status = card_mgr_card_trylock( i, &pid );
                if ( status != 0 )
                {
                    skiq_error("Card Manager: unable to obtain lock for card %" PRIu8 " (%"
                        PRIi32 ")\n", i, status);
                    status = -ENOLCK;

                    // lock failed, need to unlock everything else
                    card_mgr_probe_unlock();
                    for( j=0; j<i; j++ )
                    {
                        card_mgr_card_unlock(j);
                    }
                }
            }

            // we successfully obtained locks for all the cards + probe which means that nothing
            // else is using any of the cards so we need to create the extension and then update
            // the API.  If status is not 0, then none of the cards are locked
            if ( status == 0 )
            {
                /* start at either CARD_MGR_EXTENSION_START or the current API version + 1.  API
                 * version 1 and 2 are essentially the same, 2 was introduced in a non-backward
                 * compatible mode for X2 only and thus needs to skipped */
                for ( i = MAX(CARD_MGR_EXTENSION_START, p_card_mgr->api_version + 1); (i<=CARD_MGR_API_VERSION) && (status==0); i++ )
                {
                    status = _card_mgr_create_ext( i );
                }

                // update the API if we're able to successfully initialize it
                if( status == 0 )
                {
                    p_card_mgr->api_version = i;
                    status = _card_mgr_init();
                }

                // unlock all of the card and probe mutexes
                for ( i = 0; i < LEGACY_SKIQ_MAX_NUM_CARDS; i++ )
                {
                    card_mgr_card_unlock(i);
                }
            }
        }
    }

    return (status);
}

int32_t card_mgr_init( void )
{
/**
    @brief  The amount of time to wait for initialization to succeed in milliseconds; if
            negative, this waits forever.
*/
#define POLL_TIMEOUT_MS         (-1)

    int32_t status = 0;

    TRACE_ENTER;

    // see if we need to wait for someone else to complete initialization first
    status = wait_for_initialization(POLL_TIMEOUT_MS);

    // if we failed to initialize, see if we can create and then init again...
    if( ( status != -EFAULT ) && ( _card_mgr_init() != 0 ) )
    {
        skiq_info("Need to perform full initialization\n");
        status = card_mgr_create_card_mgr();
        if ( status == 0 )
        {
            skiq_info("Preliminary initialization complete, continue full initialization\n");
            status=_card_mgr_init();
        }
        else
        {
            skiq_error("Failed to initialize card manager (%" PRIi32 ")\n", status);
        }
    }

    if( status == 0 )
    {
        // see if the API version indicates that it's old...if it is, then we
        // need to re-initialize the shared memory

        status = upgrade_api();
    }

    TRACE_EXIT_STATUS(status);

    return (status);

#undef POLL_TIMEOUT_MS
}
#else
int32_t card_mgr_init( void )
{
    int32_t status=0;

    // if we failed to initialize, see if we can create and then init again...
    skiq_info("Need to perform full initialization\n");

    status = card_mgr_create_card_mgr();
    if ( status == 0 )
    {
        skiq_info("Preliminary initialization complete, continue full initialization\n");
        status=_card_mgr_init();
    }

    return (status);
}
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_exit( void )
{
    int32_t status=0;

    TRACE_ENTER;

    close_all_resources();

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_exit( void )
{
    FREE_IF_NOT_NULL(p_card_mgr);
    FREE_IF_NOT_NULL(p_mutexes);
    FREE_IF_NOT_NULL(p_card_mgr_ext_v3);
    FREE_IF_NOT_NULL(p_card_mgr_ext_v4);
    FREE_IF_NOT_NULL(p_card_mgr_ext_v5);
    FREE_IF_NOT_NULL(p_card_mgr_ext_v6);

    return (0);
}
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_probe_trylock( pid_t *p_owner )
{
    int32_t status=0;

    TRACE_ENTER;
    TRACE_ARGS("p_owner=%p\n", p_owner);

    status = mutex_trylock( &(p_mutexes->probe_mutex), p_owner );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_probe_trylock( pid_t *p_owner ) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_probe_unlock( void )
{
    int32_t status=-1;

    TRACE_ENTER;

    status = mutex_unlock( &(p_mutexes->probe_mutex) );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_probe_unlock( void ) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_card_trylock( uint8_t card, pid_t *p_owner )
{
    int32_t status=0;

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 ",p_owner=%p\n", card, p_owner);

    status = mutex_trylock( get_card_mutex( card ), p_owner );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_card_trylock( uint8_t card, pid_t *p_owner ) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_card_unlock( uint8_t card )
{
    int32_t status=-1;

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 "\n", card);

    status = mutex_unlock( get_card_mutex( card ) );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_card_unlock( uint8_t card ) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR && _HAVE_ROBUST_MUTEX
int32_t card_mgr_fpga_programming_lock( void )
{
    int32_t status = 0;

    TRACE_ENTER;

    status = robust_mutex_lock( &(p_card_mgr_ext_v5->fpga_programming_mutex) );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#elif _HAVE_CARD_MGR
int32_t card_mgr_fpga_programming_lock( void )
{
    pid_t owner;
    int32_t status = 0;

    TRACE_ENTER;

    status = mutex_trylock( &(p_card_mgr_ext_v5->fpga_programming_mutex), &owner );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_fpga_programming_lock( void ) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
#if _HAVE_CARD_MGR
int32_t card_mgr_fpga_programming_unlock( void )
{
    int32_t status = -1;

    TRACE_ENTER;

    status = mutex_unlock( &(p_card_mgr_ext_v5->fpga_programming_mutex) );

    TRACE_EXIT_STATUS(status);

    return (status);
}
#else
int32_t card_mgr_fpga_programming_unlock( void ) { return (0); }
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
int32_t card_mgr_register_xport( uint64_t xport_uid,
                                 skiq_xport_type_t type,
                                 uint32_t serial_num,
                                 uint16_t hardware_vers,
                                 skiq_part_info_t *p_part_info,
                                 uint8_t num_bytes,
                                 uint8_t *p_private_data )
{
    int32_t status=0;
    uint8_t card=SKIQ_MAX_NUM_CARDS;

    TRACE_ENTER;
    TRACE_ARGS("xport_uid=0x%016" PRIx64 ",serial_num=%" PRIu32 ",hardware_vers=0x%04x,p_part_info="
               "%p,type=%s\n", xport_uid, serial_num, hardware_vers, p_part_info, xport_type_cstr(type));

    // check probe in progress
    if( _probing == true )
    {
        uint8_t legacy_card_index = SKIQ_MAX_NUM_CARDS;
        uint8_t card_index = SKIQ_MAX_NUM_CARDS;
        uint8_t matching_card_index = SKIQ_MAX_NUM_CARDS;
        uint8_t i = 0;
        bool must_move = false;
        registered_card_t *p_card_info = NULL;
        xport_private_t *p_xport_priv = NULL;

        /* in order to maintain backward compatibility, any PCIe xport_uid
         * that's greater than the LEGACY_SKIQ_MAX_NUM_CARDS must be placed into
         * a card manager entry at index LEGACY_SKIQ_MAX_NUM_CARDS or higher.
         * The special case where a matching card already occupies an entry at a
         * lower index must be handled by moving (setting must_move = true) the
         * card to a higher index. */
        if ( ( xport_uid >= LEGACY_SKIQ_MAX_NUM_CARDS ) &&
             ( type == skiq_xport_type_pcie ) )
        {
            must_move = true;
        }

        /* search through 0 ... LEGACY_SKIQ_MAX_NUM_CARDS-1 to find an available
         * legacy_card_index and possibly a matching_card_index */
        for ( i = 0; i < LEGACY_SKIQ_MAX_NUM_CARDS; i++ )
        {
            p_card_info = get_card_info( i );

            // see if there's a card actually present
            if ( p_card_info->registered == 0 )
            {
                if ( legacy_card_index == SKIQ_MAX_NUM_CARDS )
                {
                    legacy_card_index = i;
                }
            }
            else
            {
                // see if the serial # matches..if so, then this card already exists, so
                // we should use that index and stop searching through the cards
                if ( p_card_info->serial == serial_num )
                {
                    // found our card
                    matching_card_index = i;
                }
            }
        }

        /* search through LEGACY_SKIQ_MAX_NUM_CARDS ... SKIQ_MAX_NUM_CARDS-1 to
         * find an available card_index and possibly a matching_card_index */
        for ( i = LEGACY_SKIQ_MAX_NUM_CARDS; i < SKIQ_MAX_NUM_CARDS; i++ )
        {
            p_card_info = get_card_info( i );

            /* get_card_info() can return NULL if the extension level 4 hasn't been initialized /
             * created yet */
            if ( p_card_info != NULL )
            {
                // see if there's a card actually present
                if ( p_card_info->registered == 0 )
                {
                    if ( card_index == SKIQ_MAX_NUM_CARDS )
                    {
                        card_index = i;
                    }
                }
                else
                {
                    // see if the serial # matches..if so, then this card already exists, so
                    // we should use that index and stop searching through the cards
                    if ( p_card_info->serial == serial_num )
                    {
                        // found our card
                        matching_card_index = i;
                    }
                }
            }
        }

        debug_print("must_move=%" PRIu8 ", legacy_card_index = %" PRIu8 ", matching_card_index = %"
                    PRIu8 ", card_index = %" PRIu8 "\n", must_move, legacy_card_index,
                    matching_card_index, card_index);

        if ( must_move )
        {
            /* In this case, legacy_card_index cannot be used. Check if there's
             * a matching card and if it's in the legacy space.  if it is, move
             * it to card_index */

            if ( matching_card_index == SKIQ_MAX_NUM_CARDS )
            {
                /* no existing match, use card_index.  card_index could be
                 * SKIQ_MAX_NUM_CARDS, but that's okay because it means all the
                 * entries are registered and there's no room anyway. */
                card = card_index;
                debug_print("must move: no existing match, use card_index = %" PRIu8 "\n",
                            card_index);
            }
            else if ( matching_card_index < LEGACY_SKIQ_MAX_NUM_CARDS )
            {
                /* must move this entry */
                debug_print("must move: existing match at a legacy index, move everything to card_index = %" PRIu8 "\n", matching_card_index);

                /* 1. copy entry from matching_card_index to card_index */
                memcpy( get_card_info( card_index ), get_card_info( matching_card_index ), sizeof( registered_card_t ) );
                part_info_copy( get_part_info( card_index ), get_part_info( matching_card_index ) );

                /* 2. copy xport_private data from matching_card_index to card_index */
                copy_xport_private( matching_card_index, card_index, type );

                /* 3. nullify entry at matching_card_index by initializing (clearing) the info */
                info_initialize( matching_card_index );

                /* 4. assign new slot at card_index */
                card = card_index;
            }
            else
            {
                /* use the match's index */
                debug_print("must move: existing match, use matching_card_index = %" PRIu8 "\n", matching_card_index);
                card = matching_card_index;
            }
        }
        else
        {
            /* In this case use legacy_card_index or matching_card_index if
             * there's a match, no movement necessary */

            if ( matching_card_index == SKIQ_MAX_NUM_CARDS )
            {
                /* no existing match, use legacy_card_index.  legacy_card_index
                 * could be SKIQ_MAX_NUM_CARDS, but that's okay because it means
                 * all the legacy entries are registered and there's no room. */
                debug_print("can stay: no existing match, use legacy_card_index = %" PRIu8 "\n", legacy_card_index);
                card = legacy_card_index;
            }
            else
            {
                /* use the match's index */
                debug_print("can stay: existing match, use matching_card_index = %" PRIu8 "\n", matching_card_index);
                card = matching_card_index;
            }
        }

        // see if we have a valid card index to save our transport info to
        if ( card < SKIQ_MAX_NUM_CARDS )
        {
            /* get reference to the registered_card_t from the card index */
            p_card_info = get_card_info( card );

            /* get_card_info() can return NULL if the extension level 4 hasn't been initialized or
             * created yet */
            if ( p_card_info != NULL )
            {
                // save generic card info
                p_card_info->serial = serial_num;
                p_card_info->hardware_vers = hardware_vers;
                p_card_info->registered = 1;
                part_info_copy( get_part_info( card ), p_part_info );

                // save xport specific card info
                p_xport_priv = get_xport_private( card, type );
                if ( p_xport_priv != NULL )
                {
                    p_xport_priv->xport_uid = xport_uid;
                    p_xport_priv->present = 1;
                    p_xport_priv->num_bytes = 0;
                }

                // copy the private data
                if(  (num_bytes <= CARD_MGR_XPORT_PRIVATE_SIZE) )
                {
                    if( (num_bytes > 0) && (p_private_data != NULL) && (p_xport_priv != NULL) )
                    {
                        memcpy( p_xport_priv->xport_private,
                                p_private_data,
                                num_bytes );
                        p_xport_priv->num_bytes = num_bytes;
                    }
                }
                else
                {
                    status = -ERANGE;
                }
            }
        }
        else
        {
            debug_print("No space available for card with xport_uid=0x%016" PRIx64 ",serial_num=%"
                        PRIu32 ",hardware_vers=0x%04x,p_part_info=%p,type=%s\n", xport_uid,
                        serial_num, hardware_vers, p_part_info, xport_type_cstr(type));
            status = -ENOSPC;
        }
    }
    else
    {
        status=-2;
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
int32_t card_mgr_read_xport_private( uint64_t xport_uid,
                                     skiq_xport_type_t type,
                                     uint8_t *p_num_bytes,
                                     uint8_t *p_xport_private_data )
{
    int32_t status=0;
    uint8_t card=0;

    TRACE_ENTER;

    // find the card
    if( card_mgr_get_card(xport_uid, type, &card) == 0 )
    {
        xport_private_t *p_xport_priv;

        p_xport_priv = get_xport_private( card, type );
        if( p_xport_priv != NULL )
        {
            // copy the private data
            *p_num_bytes = p_xport_priv->num_bytes;
            memcpy( p_xport_private_data,
                    p_xport_priv->xport_private,
                    *p_num_bytes );
        }
    }
    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
int32_t card_mgr_update_xport_private( uint64_t xport_uid,
                                       skiq_xport_type_t type,
                                       uint8_t num_private_bytes,
                                       uint8_t *p_xport_private )
{
    int32_t status=-1;

    TRACE_ENTER;
    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
bool card_mgr_is_xport_avail( uint8_t card,
                              skiq_xport_type_t type )
{
    bool present = false;
    xport_private_t *p_xport_priv = NULL;

    // no need to check card # bounds here since this is a trusted interface (only called from
    // sidekiq_api) and that trusted interface already performs the bounds check

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 ",type=%s\n", card, xport_type_cstr(type));

    // see if the xport is present for this card
    p_xport_priv = get_xport_private( card, type );
    if ( ( p_xport_priv != NULL ) && ( p_xport_priv->present == 1 ) )
    {
        present = true;
    }

    TRACE_EXIT_STATUS(present);

    return (present);
}


/******************************************************************************/
int32_t card_mgr_get_xport_id( uint8_t card,
                               skiq_xport_type_t type,
                               uint64_t *p_xport_id )
{
    int32_t status = -ENXIO;

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 ",type=%s,p_xport_id=%p\n", card, xport_type_cstr(type), p_xport_id);

    // check probe card if probe in progress
    if( (card == probe_card) &&
        (_probe_id.type == type) &&
        (_probing==true) )
    {
        *p_xport_id = _probe_id.xport_uid;
        status = 0;
    }
    else
    {
        xport_private_t *p_xport_priv = get_xport_private( card, type );

        // no need to check card # bounds here since this is a trusted interface (only called from
        // sidekiq_api) and that trusted interface already performs the bounds check

        // see if the xport is present for this card
        if ( p_xport_priv != NULL )
        {
            if( p_xport_priv->present == 1 )
            {
                *p_xport_id = p_xport_priv->xport_uid;
                status = 0;
            }
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
int32_t card_mgr_get_card( uint64_t xport_uid,
                           skiq_xport_type_t type,
                           uint8_t *p_card_id )
{
    int32_t status=-1;
    uint8_t i=0;
    bool found_card=false;

    TRACE_ENTER;
    TRACE_ARGS("xport_uid=0x%" PRIx64 ",type=%s,p_card_id=%p\n", xport_uid, xport_type_cstr(type), p_card_id);

    // check probe card if probe in progress
    if( (_probe_id.type == type) &&
        (_probe_id.xport_uid == xport_uid) &&
        (_probing == true) )
    {
        *p_card_id = probe_card;
        status = 0;
    }
    else
    {
        registered_card_t *p_card_info = NULL;
        xport_private_t *p_xport_priv = NULL;

        for ( i = 0; (i < SKIQ_MAX_NUM_CARDS) && (found_card == false); i++ )
        {
            p_card_info = get_card_info( i );
            if ( p_card_info != NULL )
            {
                p_xport_priv = get_xport_private( i, type );
                if ( p_xport_priv != NULL )
                {
                    // check if there's a card registered at this index and if the xport of this
                    // type is present
                    if ( ( p_card_info->registered == 1 ) && ( p_xport_priv->present == 1 ) )
                    {
                        // see if the UID matches
                        if ( p_xport_priv->xport_uid == xport_uid )
                        {
                            // got it!
                            *p_card_id = i;
                            found_card = true;
                            status = 0;
                        }
                    }
                }
            }
        }
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
int32_t card_mgr_get_cards( uint8_t *p_cards,
                            uint8_t *p_num_cards )
{
    int32_t status=0;
    uint8_t i=0;
    uint8_t num_cards=0;

    TRACE_ENTER;
    TRACE_ARGS("p_cards=%p,p_num_cards=%p\n", p_cards, p_num_cards);

    /* look for cards that are registered */
    for ( i = 0; i < SKIQ_MAX_NUM_CARDS; i++ )
    {
        registered_card_t *p_card_info = get_card_info( i );
        if ( ( NULL != p_card_info ) && ( p_card_info->registered == 1 ) )
        {
            p_cards[num_cards] = i;
            num_cards++;
        }
    }

    *p_num_cards = num_cards;

    debug_print("Card Manager returning %" PRIu8 " cards:\n", num_cards);
    for (i = 0; i < num_cards; i++ )
    {
        debug_print("\tp_cards[%" PRIu8 "] = %" PRIu8 "\n", i, p_cards[i]);
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/******************************************************************************/
int32_t card_mgr_get_card_info( uint8_t card,
                                uint32_t *p_serial_num,
                                uint16_t *p_hardware_vers,
                                skiq_part_info_t *p_part_info )
{
    int32_t status = -ENODEV;
    registered_card_t *p_card_info = NULL;

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 ",p_serial_num=%p,p_hardware_vers=%p,p_part_info=%p\n",
               card, p_serial_num, p_hardware_vers, p_part_info);

    p_card_info = get_card_info( card );

    /* if the card is registered, fill in the serial number, hardware version,
     * and part information from the legacy card manager shared memory
     * resource */
    if ( ( p_card_info != NULL ) && ( p_card_info->registered == 1 ) )
    {
        status = 0;
        *p_serial_num = p_card_info->serial;
        *p_hardware_vers = p_card_info->hardware_vers;
        part_info_copy( p_part_info, get_part_info( card ) );
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
/**
   The card_mgr_is_card_present function indicates whether or not the card is present.  The function
   bases its decision on whether or not the registered card has any transports available
*/
int32_t card_mgr_is_card_present( uint8_t card,
                                  bool *p_is_present )
{
    int32_t status=0;

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 ",p_is_present=%p\n", card, p_is_present);

    skiq_xport_type_t type_idx;
    uint8_t nr_xport_present = 0;
        
    for ( type_idx = 0; ( type_idx < skiq_xport_type_max ) && ( nr_xport_present == 0 ); type_idx++ )
    {
        xport_private_t *p_xport_priv = get_xport_private( card, type_idx );

        if ( p_xport_priv != NULL )
        {
            if ( p_xport_priv->present > 0 )
            {
                nr_xport_present++;
            }
        }
    }

    *p_is_present = ( nr_xport_present > 0 );

    debug_print("Card manager indicates card %" PRIu8 " is %spresent\n", card,
                *p_is_present ? "" : "NOT ");

    TRACE_EXIT_STATUS(status);

    return (status);
}


/**************************************************************************************************/
/**
   The card_mgr_deregister_xport() function removes all private data of a specified transport type
   from the specified card.
 */
int32_t card_mgr_deregister_xport( uint8_t card,
                                   skiq_xport_type_t xport_type )
{
    int32_t status = -EINVAL;
    xport_private_t *p_xport_priv = NULL;

    TRACE_ENTER;
    TRACE_ARGS("card=%" PRIu8 ",xport=%s\n", card, xport_type_cstr( xport_type ));

    /* check bounds on `xport_type` */
    if ( xport_type < skiq_xport_type_max )
    {
        status=0;
    }

    /* get card information */
    if ( status == 0 )
    {
        p_xport_priv = get_xport_private( card, xport_type );
        if ( p_xport_priv == NULL )
        {
            status = -ENODEV;
        }
    }

    /* deregister all information relevant to the transport type `xport_type` */
    if ( status == 0 )
    {
        clear_xport_private( p_xport_priv );
    }

    TRACE_EXIT_STATUS(status);

    return (status);
}


#if _HAVE_CARD_MGR
/**************************************************************************************************/
static void
card_mgr_display_mutex( uint8_t card )
{
    card_mgr_mutex_t *p_card_mutex = get_card_mutex( card );

    if ( p_card_mutex )
    {
        int32_t status;
        pid_t owner;

        status = card_mgr_card_trylock( card, &owner );
        if ( status == 0 )
        {
            printf("\tmutex:   unlocked\n");
            card_mgr_card_unlock( card );
        }
        else if ( ( status == EBUSY ) || ( status == -EBUSY ) )
        {
            printf("\tmutex:   locked by process %jd\n", (intmax_t)p_card_mutex->owner);
        }
        else
        {
            printf("\tmutex:   unknown (status = %" PRIi32 ")\n", status);
        }
    }
    else
    {
        printf("\tmutex:   error\n");
    }
}
#endif /* _HAVE_CARD_MGR */


/******************************************************************************/
void card_mgr_display_info( void )
{
    uint8_t i=0;
    uint8_t j=0;

    TRACE_ENTER;

    printf("*******************************\n");

    printf("    API version: %" PRIu8 "\n", p_card_mgr->api_version);
    for ( i = CARD_MGR_EXTENSION_START; i <= p_card_mgr->api_version; i++)
    {
        void *p_ext_addr = NULL;
        switch(i)
        {
        case 3:
            p_ext_addr = (void *) p_card_mgr_ext_v3;
            break;
        case 4:
            p_ext_addr = (void *) p_card_mgr_ext_v4;
            break;
        case 5:
            p_ext_addr = (void *) p_card_mgr_ext_v5;
            break;
        case 6:
            p_ext_addr = (void *) p_card_mgr_ext_v6;
            break;
        default:
            break;
        }
        printf("    Extension %" PRIu8 " present? %s\n", i,
            (p_ext_addr != NULL) ? "Yes" : "No");
    }

    printf("*******************************\n");
    for ( i = 0; i < SKIQ_MAX_NUM_CARDS; i++ )
    {
        registered_card_t *p_card_info = get_card_info( i );
        skiq_part_info_t *p_part_info = get_part_info( i );

        // print the card info if there's a card registered
        if ( p_card_info && ( p_card_info->registered == 1 ) )
        {
            printf("Card: %2" PRIu8 "\n", i);
            printf("\tserial:  0x%08x ", p_card_info->serial);
            if ( p_card_info->serial < SKIQ_FACT_SERIAL_NUM_LEGACY_MAX )
            {
                printf("(%" PRIu32 ")\n", p_card_info->serial);
            }
            else
            {
                int sizeof_serial = sizeof( p_card_info->serial );

                printf("(");
                for ( j = 0; j < sizeof_serial; j++ )
                {
                    int byte_index = sizeof_serial - 1 - j;
                    int bit_shift = 8 * byte_index;
                    char c = (char)( p_card_info->serial >> bit_shift );

                    printf("%c", ( 0 != isalnum( c ) ) ? c : '.' );
                }
                printf(")\n");
            }
            printf("\thw vers: 0x%x\n", p_card_info->hardware_vers);
            if ( p_part_info != NULL )
            {
                printf("\tpart:    ES%s-%s-%s\n", p_part_info->number_string,
                       p_part_info->revision_string, p_part_info->variant_string);
            }
#if _HAVE_CARD_MGR
            card_mgr_display_mutex( i );
#endif /* _HAVE_CARD_MGR */

            for( j=0; j<skiq_xport_type_max; j++ )
            {
                xport_private_t *p_xport_priv = get_xport_private( i, j );

                // if the xport is present for this card, display the xport info
                if( ( p_xport_priv != NULL ) &&
                    ( p_xport_priv->present == 1 ) )
                {
                    printf("\txport:   %s\n", xport_type_cstr( j ));
                    printf("\t         UID: %" PRIu64 " (0x%016" PRIx64 ")\n",
                           p_xport_priv->xport_uid, p_xport_priv->xport_uid);
                }
            }
        }
        else
        {
            printf("Card: %2" PRIu8 " (unregistered)\n", i);
        }
    }
    printf("*******************************\n");

    TRACE_EXIT;
}


#if _HAVE_CARD_MGR
/**************************************************************************************************/
/**
   The card_mgr_reprobe() function takes care of reprobing available (e.g. unlocked) cards for any
   changed transport information, discovering new cards, or removing cards that are no longer
   present.
 */
int32_t card_mgr_reprobe( void )
{
    ARRAY_WITH_DEFAULTS(uint8_t, locked_cards, SKIQ_MAX_NUM_CARDS, SKIQ_MAX_NUM_CARDS);
    ARRAY_WITH_DEFAULTS(uint8_t, avail_cards, SKIQ_MAX_NUM_CARDS, SKIQ_MAX_NUM_CARDS);
    uint8_t nr_locked = 0, nr_avail = 0;
    int32_t status = 0;
    uint8_t i;
    pid_t probe_owner;
    bool have_probe_lock = false;

    TRACE_ENTER;

    /* 1. lock others out from hotplug */
    status = card_mgr_probe_trylock( &probe_owner );
    if ( status == EBUSY )
    {
        /* return immediately if another process owns the probe lock */
        debug_print("Skipping re-probe, currently locked by %" PRIdMAX "\n", (intmax_t)probe_owner);
        return 0;
    }
    else if ( status == 0 )
    {
        /* successfully acquired probe_lock, make note of it so it's unlocked down below */
        have_probe_lock = true;

        /* 2. grab locks for all card entries (even unregistered slots in case a new card shows up
         * during the transport's rescan) */
        for ( i = 0; i < SKIQ_MAX_NUM_CARDS; i++ )
        {
            pid_t owner;
            int32_t status_trylock;

            /* for this loop, there's no failing to trylock, just consider any card that fails to
             * lock (either because it is legitimately locked by another process or the trylock
             * failed for some unknown reason) unobtainable.  as such, use a separate
             * `status_trylock` variable to track the return code from card_mgr_card_trylock(). */
            status_trylock = card_mgr_card_trylock(i, &owner);
            if ( status_trylock == 0 )
            {
                avail_cards[nr_avail++] = i;
            }
            else if ( ( status_trylock == EBUSY ) || ( status_trylock == -EBUSY ) )
            {
                debug_print("Skipping card %" PRIu8 ", currently locked by %" PRIdMAX "\n", i,
                            (intmax_t) owner);
                locked_cards[nr_locked++] = i;
            }
            else
            {
                skiq_error("Unhandled error %" PRIi32 " from card_mgr_card_trylock on card %" PRIu8
                           ", considering it off-limits for re-probe\n", status_trylock, i);
                locked_cards[nr_locked++] = i;
            }
        }

        if(nr_avail > 0)
        {
            probe_card = avail_cards[0];
        }
        else
        {
            skiq_info("There are no available cards to use for probing");
            return status;
        }
        debug_print("Probing using available card %" PRIu8 "\n", probe_card);
    }

    /* 3. ask transport(s) to re-probe */
    if ( status == 0 )
    {
        skiq_xport_type_t type;

        _probing = true;

        for ( type = skiq_xport_type_pcie; ( type < skiq_xport_type_max ) && ( status == 0 ); type++ )
        {
            status = skiq_xport_card_hotplug( type, locked_cards, nr_locked );
            if ( status != 0 )
            {
                skiq_warning("%s transport hotplug error occurred with status %" PRIi32 ", continuing\n",
                             xport_type_cstr(type), status);

                /* on platforms where libusb_init() fails because there is no
                 * USB support, the probe will return an error, but we have to
                 * keep probing.  It would be nice to know the difference
                 * between no USB support and a failed USB probe.
                 *
                 * so set status to 0 intentionally to let the show go on.
                 */
                status = 0;
            }
        }

        _probing = false;
    }

    /* unconditionally attempt to unlock all cards that were acquired above in step 2 */
    {
        for (i = 0; i < nr_avail; i++)
        {
            int32_t status_unlock;

            status_unlock = card_mgr_card_unlock(avail_cards[i]);
            if ( status_unlock != 0 )
            {
                skiq_error("Unhandled error %" PRIi32 " from card_mgr_card_unlock on card %" PRIu8
                           "\n", status_unlock, i);
            }
        }
    }

    /* 4. unlock others out from hotplug */
    if ( have_probe_lock )
    {
        int32_t status_unlock;

        status_unlock = card_mgr_probe_unlock();
        if ( status_unlock != 0 )
        {
            skiq_error("Unhandled error %" PRIi32 " from card_mgr_probe_unlock on card %" PRIu8
                       "\n", status_unlock, i);
        }
    }

    TRACE_EXIT_STATUS(status);

    return status;
}
#else
int32_t card_mgr_reprobe( void )  { return 0; }
#endif	/* _HAVE_CARD_MGR */
