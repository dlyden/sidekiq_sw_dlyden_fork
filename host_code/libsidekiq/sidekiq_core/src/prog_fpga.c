/*! \file prog_fpga.h
 * \brief This file contains the implementation to program the FPGA.
 *
 * <pre>
 * Copyright 2013 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 11/22/2013   MEZ   Created file
 *
 *</pre>*/

#include <sidekiq_api.h>
#include <sidekiq_hal.h>
#include <prog_fpga.h>
#include <usb_interface.h>

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


#define FAST_NUM_BYTES_TO_SEND (4096)
#define LEGACY_NUM_BYTES_TO_SEND (64)

int32_t program_fpga_from_file(uint8_t index,
                               FILE *pFile,
                               uint8_t legacy_method)
{
    int32_t status = 0;
    ssize_t num_bytes;
    uint8_t buf[FAST_NUM_BYTES_TO_SEND];
    uint32_t i=0;
    uint8_t fpga_status = 0;
    uint32_t num_bytes_to_send=0;
    int fd = fileno(pFile);
    struct stat stat_pFile;
    off_t remaining;

    if ( 0 != fstat(fd, &stat_pFile) )
    {
        perror("prog_fpga/fstat");
        return -1;
    }
    remaining = stat_pFile.st_size;

    if( legacy_method == 1 )
    {
        printf("Info: using legacy FPGA programming, programming may take >20 seconds\n");
        num_bytes_to_send = LEGACY_NUM_BYTES_TO_SEND;
    }
    else
    {
        printf("Info: using fast FPGA programming\n");
        num_bytes_to_send = FAST_NUM_BYTES_TO_SEND;
    }

    if ((status=usb_init_fpga_for_programming(index)) != 0) {
        printf("Error: init FPGA for programming failed with status %d\n",
               status);
        return -1;
    }

    usb_read_fpga_config(index, &fpga_status);
    if (0xFF == fpga_status) {
        printf("Error: init_b is low at start!\n");
        return -1;
    }

    while (feof(pFile) == 0) {
        /* if in FAST mode and there are fewer than 450,000 bytes (about 80% of
         * an m.2 image), switch to the LEGACY block size (but not legacy
         * mode) */
        if ( ( FAST_NUM_BYTES_TO_SEND == num_bytes_to_send ) && ( remaining < 450000 ) )
        {
            num_bytes_to_send = LEGACY_NUM_BYTES_TO_SEND;
        }

        num_bytes = fread(buf, 1, num_bytes_to_send, pFile);
        remaining -= num_bytes;

        // TODO: check file type to determine if swapping necessary
        for (i = 0; i < num_bytes; i++) {
            /* swap bit endianess (http://stackoverflow.com/a/36928286) */
            buf[i] = ((buf[i]>>4) & 0x0F) | ((buf[i]<<4) & 0xF0);
            buf[i] = ((buf[i]>>2) & 0x33) | ((buf[i]<<2) & 0xCC);
            buf[i] = ((buf[i]>>1) & 0x55) | ((buf[i]<<1) & 0xAA);
        }
        // send the fpga config down
        usb_send_fpga_config(index, buf, num_bytes, legacy_method);
    }

    // finish it
    usb_finish_fpga_programming(index);

    // read the config state, check to see if init_b is asserted
    usb_read_fpga_config(index, &fpga_status);
    if ((0 == fpga_status) || (0xFF == fpga_status))
    {
        printf("Error: FPGA not configured successfully\n");
        return -1;
    }

    return (status);
}
