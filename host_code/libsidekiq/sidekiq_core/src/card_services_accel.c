/**
 * @file   card_services_accel.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Oct 18 13:21:51 CDT 2018
 *
 * @brief
 *
 *
 * <pre>
 * Copyright 2018-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <inttypes.h>
#include <glib.h>

/* gmacros.h (by way of glib.h) provides a simplistic MIN and MAX that are better defined by our own
 * sidekiq_private.h */
#undef MIN
#undef MAX

/* @todo this needs to change! */
#include <sidekiq_usb_cmds.h>

#include "card_services.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_fpga.h"
#include "hardware.h"

/* enable debug_print and debug_print_plain when DEBUG_ACCEL is defined */
#if (defined DEBUG_ACCEL)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/*
 * ADXL346 accelerometer registers
 *
 * Found on Sidekiq miniPCIe and M.2
 */
#define ADXL346_ACCEL_ADDR              ACCEL_I2C_ADDRESS
#define ACCEL_POWER_MODE_REG            (0x2D)
#define ACCEL_INT_SOURCE                (0x30) /* note: interrupts don't have to be enabled for data
                                                * ready */
#define ACCEL_X_COORD_REG               (0x32)
#define ACCEL_Y_COORD_REG               (0x34)
#define ACCEL_Z_COORD_REG               (0x36)
#define ACCEL_ENABLE_MEASURE            (0x08)
#define ACCEL_DISABLE_MEASURE           (0x00)
#define ACCEL_BW_RATE_REG               (0x2C)
#define ACCEL_BW_RATE_100HZ             (0x0A)
#define DATA_READY_MASK                 (0x80) /* bit 7 of reg 0x30 */

/*
 * ICM-20602 accelerometer registers
 *
 * Found on Sidekiq Z2 rev C / M.2-2280 / Z2p / Z3u / NV100
 *
 * Note: B1 of Z2p (and possibly others due to part shortage), have
 * swapped ICM-20602 to ICM-20601.  These parts are "nearly" identical.
 * As a result, we're still going to refer to this as ICM20602 even though
 * the actual part in use may be ICM20601.  The one exception to these 
 * parts being identical as far as libsidekiq is concerned is the 
 * accelerometer coordinate conversion (which is done in 
 * icm20602_read_accel_coord() ).  We can determine which part we are
 * actually using by reading the WHO_AM_I register and address any differences.
 */
#define ICM20602_ACCEL_ADDR             (0x68)

#define ICM20602_SMPLRT_DIV             (0x19)
#define  ICM20602_SMPLRT_100Hz          (9)    /* SAMPLE_RATE = INTERNAL_SAMPLE_RATE / (1 +
                                                  SMPLRT_DIV) where INTERNAL_SAMPLE_RATE = 1 kHz */
#define ICM20602_ACCEL_CFG_1            (0x1C)
#define ICM20602_ACCEL_CFG_2            (0x1D)
#define ICM20602_INT_STATUS             (0x3A)
#define  ICM20602_DATA_READY            (1 << 0)
#define ICM20602_X_COORD_REG            (0x3B) /* 2 bytes, high, then low */
#define ICM20602_Y_COORD_REG            (0x3D) /* 2 bytes, high, then low */
#define ICM20602_Z_COORD_REG            (0x3F) /* 2 bytes, high, then low */
#define ICM20602_PWR_MGMT_1             (0x6B)
#define ICM20602_WHO_AM_I               (0x75)
#define  ICM20602_CLK_PLL               (1 << 0)
#define  ICM20602_SLEEP                 (1 << 6)
#define  ICM20602_DEVICE_RESET          (1 << 7)
#define  ICM20602_DEVICE_RESET_TIMEOUT  (300000) /* 300mS, chosen arbitrarily, datasheet for
                                                  * ICM-20602 does not specify how long it takes to
                                                  * reset internal registers */
#define ICM20602_PWR_MGMT_2             (0x6C)
#define  ICM20602_STBY_ACCEL_MASK       ~(uint8_t)(0x7 << 3)
#define  ICM20602_STBY_ACCEL_EN         (0)
#define  ICM20602_STBY_ACCEL_DIS        ICM20602_STBY_ACCEL_MASK

#define ICM20601_DEVICE_ID              (0xAC)
#define ICM20602_DEVICE_ID              (0x12)

/* scaling factor determined by number of bits (15) and resolution (2 or 4), div by 1000 to convert to mg 
   ex. scaling_factor = (2^15 / resolution) / 1000 
*/
#define ICM20602_DEFAULT_SCALING_FACTOR (16.384) /* 2G is default */
#define ICM20601_DEFAULT_SCALING_FACTOR (8.192)  /* 4G is default */       


/***** TYPEDEFS *****/

int32_t card_write_accel_reg( uint8_t card,
                              uint8_t reg,
                              uint8_t *p_data,
                              uint8_t length );

int32_t card_read_accel_reg( uint8_t card,
                             uint8_t reg,
                             uint8_t *p_data,
                             uint8_t length );


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/**************************************************************************************************/
static uint8_t
accel_addr( uint8_t card )
{
    uint8_t addr;

    switch ( _skiq_get_part( card ) )
    {
        case skiq_z2:
        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
        case skiq_nv100:
            addr = ICM20602_ACCEL_ADDR;
            break;

        default:
            addr = ADXL346_ACCEL_ADDR;
            break;
    }

    return addr;

} /* accel_addr */


/**************************************************************************************************/
static int32_t
adxl346_read_accel_coord( uint8_t card,
                          uint8_t reg,
                          int16_t *p_coord )
{
    int32_t status = 0;
    int16_t coord = 0;

    /* NOTE: the ADXL346 uses 13 bits of precision regardless of g-range setting
     *
     * As such, the datasheet states 4mg / LSB (page 26, FULL_RES bit).
     *
     * Convert the raw reading into units of milli-g
     *
     * Additionally, the accelerometer coordinates are presented low,high (page 26)
     */

    status = hal_read_le16_i2c_reg( card, accel_addr( card ), reg, &coord );
    if ( 0 == status )
    {
        *p_coord = (int16_t)( coord * 4 );
    }

    return status;
}


/**************************************************************************************************/
static int32_t
adxl346_read_accel( uint8_t card,
                    int16_t *p_x_data,
                    int16_t *p_y_data,
                    int16_t *p_z_data )
{
    int32_t status = 0;
    uint8_t int_source = 0;


    /* The scaling factor for ICM20601 is 2x
     * the factor used in ICM20602.  Specifically:
     *     ICM-20602 (original pn) = 2g, 4g, 8g, 16g
     *     ICM-20601 (substitution PN) = 4g, 8g, 16g, 32g
     */

    /* first make sure that the data is ready */
    status = card_read_accel_reg(card, ACCEL_INT_SOURCE, &int_source, 1);
    if ( status == 0 )
    {
        if ( (int_source & DATA_READY_MASK) != 0 )
        {
            status = adxl346_read_accel_coord(card, ACCEL_X_COORD_REG, p_x_data);

            if ( 0 == status )
            {
                status = adxl346_read_accel_coord(card, ACCEL_Y_COORD_REG, p_y_data);
            }

            if ( 0 == status )
            {
                status = adxl346_read_accel_coord(card, ACCEL_Z_COORD_REG, p_z_data);
            }

            /* a non-zero status indicates a failure to perform I/O communications */
            if ( status != 0 )
            {
                status = -EIO;
            }
        }
        else
        {
            debug_print("accelerometer data isn't ready");
            status = -EAGAIN;
        }
    }
    return status;
}


/**************************************************************************************************/
static int32_t
adxl346_set_state( uint8_t card,
                   bool enable )
{
    int32_t status = 0;
    uint8_t data = 0;

    /* set data to ACCEL_ENABLE_MEASURE or ACCEL_DISABLE_MEASURE depending on argument */
    data = enable ? ACCEL_ENABLE_MEASURE : ACCEL_DISABLE_MEASURE;
    status = card_write_accel_reg(card, ACCEL_POWER_MODE_REG, &data, 1);

    /* verify state change */
    if ( status == 0 )
    {
        uint8_t verify = 0;

        status = card_read_accel_reg(card, ACCEL_POWER_MODE_REG, &verify, 1);
        if ( ( status == 0 ) && ( verify != data ) )
        {
            status = -1;
            skiq_warning("On-board accelerometer failed to accept state change (%s)\n",
                         enable ? "enable" : "disable");
        }
    }

    /* if accelerometer is being enable, also set BW_RATE to 100Hz */
    if ( (status == 0) && enable )
    {
        data = ACCEL_BW_RATE_100HZ;
        status = card_write_accel_reg(card, ACCEL_BW_RATE_REG, &data, 1);
    }

    return status;
}


static int32_t
icm20602_read_accel_coord( uint8_t card,
                           uint8_t reg,
                           int16_t *p_coord )
{
    int32_t status = 0;
    int16_t coord = 0;
    uint8_t device_id = 0;
    float scaling_factor = 0;

    /* NOTE: this function assumes that the ICM20602/ICM20601 is using the 
     * default g-range setting.  
     * 
     * For ICM20602, this is +- 2g.  As such, the Sensitivty Scale Factor (p10) is 
     * 16384 LSB/g which is 16.384 LSB / mg (ICM20602_DEFAULT_SCALING_FACTOR)  

     * For ICM20601 this is +- 4g.  As such, the Sensitivty Scale Factor (p10) is 
     * 132768 LSB/g which is 32.768 LSB / mg (ICM20601_DEFAULT_SCALING_FACTOR).
     *
     */


    /* determine what scaling factor we should use based on the device ID */
    status = card_read_accel_reg( card, ICM20602_WHO_AM_I, &device_id, 1 );
    if( status == 0 )
    {
        switch( device_id )
        {
            case ICM20601_DEVICE_ID:
                scaling_factor = ICM20601_DEFAULT_SCALING_FACTOR;
                break;

            case ICM20602_DEVICE_ID:
                scaling_factor = ICM20602_DEFAULT_SCALING_FACTOR;
                break;

            default:
                skiq_error( "Unknown acceleromter device ID 0x%x read for card %u", device_id, card );
                status = -ENODEV;
        }
        debug_print("ICM2060x device ID 0x%x, scale factor %f\n", device_id, scaling_factor);
    }

    if( status == 0 )
    {
        /* read the raw coordinates */
        status = hal_read_be16_i2c_reg(card, accel_addr( card ), reg, &coord);
        if ( 0 == status )
        {
            /* apply the scaling factor */
            *p_coord = (int16_t)( (float)coord / scaling_factor );
        }
    }

    return status;
}


/**************************************************************************************************/
static int32_t
icm20602_read_accel( uint8_t card,
                     int16_t *p_x_data,
                     int16_t *p_y_data,
                     int16_t *p_z_data )
{
    int32_t status = 0;
    uint8_t int_source = 0;

#if (defined DEBUG_ACCEL)
    {
        int32_t debug_status;
        uint8_t config;
        debug_status = card_read_accel_reg( card, ICM20602_ACCEL_CFG_1, &config, 1 );
        if ( debug_status == 0 )
        {
            debug_print("Accelerometer Configuration 1: 0x%02X\n", config);
        }

        debug_status = card_read_accel_reg( card, ICM20602_ACCEL_CFG_2, &config, 1 );
        if ( debug_status == 0 )
        {
            debug_print("Accelerometer Configuration 2: 0x%02X\n", config);
        }
    }
#endif  /* DEBUG_ACCEL */

    /* first make sure that the data is ready */
    status = card_read_accel_reg(card, ICM20602_INT_STATUS, &int_source, 1);
    if ( status == 0 )
    {
        if ( (int_source & ICM20602_DATA_READY) != 0 )
        {
            status = icm20602_read_accel_coord( card, ICM20602_X_COORD_REG, p_x_data );

            if ( 0 == status )
            {
                status = icm20602_read_accel_coord( card, ICM20602_Y_COORD_REG, p_y_data );
            }

            if ( 0 == status )
            {
                status = icm20602_read_accel_coord( card, ICM20602_Z_COORD_REG, p_z_data );
            }

            /* a non-zero status indicates a failure to perform I/O communications */
            if ( status != 0 )
            {
                status = -EIO;
            }
        }
        else
        {
            debug_print("accelerometer data isn't ready");
            status = -EAGAIN;
        }
    }
    return status;
}


/**************************************************************************************************/
static int32_t
icm20602_set_state( uint8_t card,
                    bool enable )
{
    int32_t status = 0;
    uint8_t power;

    /* if not awake && enable:
     *
     * 1. perform DEVICE_RESET in REG_PWR_MGMT_1
     * 2. set CLK_PLL in REG_PWR_MGMT_1
     * 3. sleep for REG_UP_TIME_USEC
     * 4. enable all accelerometer axes in REG_PWR_MGMT_2
     * 4. configure SMPLRT_DIV
     *
     * if awake && enable:
     *
     * 1. do nothing
     */

    /* if not awake && !enable:
     *
     * 1. do nothing
     *
     * if awake && !enable:
     *
     * 1. set CLK_PLL and SLEEP in REG_PWR_MGMT_1
     *
     */

    /* check to see if awake */
    status = card_read_accel_reg(card, ICM20602_PWR_MGMT_1, &power, 1 );
    if ( status == 0 )
    {
        if ( enable )
        {
            if ( ( power & ICM20602_SLEEP ) > 0 )
            {
                uint64_t reset_deadline = 0;

                /* chip is set to sleep mode */

                /* 1. perform DEVICE_RESET in REG_PWR_MGMT_1 and bring out of sleep, poll until DEVICE_RESET and SLEEP clears */
                power |= ICM20602_DEVICE_RESET;
                power &= ~ICM20602_SLEEP;
                status = card_write_accel_reg( card, ICM20602_PWR_MGMT_1, &power, 1 );
                reset_deadline = g_get_monotonic_time();
                while ( status == 0 )
                {
                    status = card_read_accel_reg( card, ICM20602_PWR_MGMT_1, &power, 1 );
                    if ( ( status == 0 ) && (( power & ICM20602_DEVICE_RESET ) == 0)  )
                    {
                        break;
                    }

                    /* DEVICE_RESET timed out, assume I/O communications failed */
                    if ( g_get_monotonic_time() > reset_deadline )
                    {
                        status = -EIO;
                        break;
                    }
                }

                /* 2. set BIT_CLK_PLL in REG_PWR_MGMT_1 */
                if ( status == 0 )
                {
                    power &= ~ICM20602_DEVICE_RESET;
                    power |= ICM20602_CLK_PLL;
                    power &= ~ICM20602_SLEEP;
                    status = card_write_accel_reg( card, ICM20602_PWR_MGMT_1, &power, 1 );
                }

                /* 3. sleep for REG_UP_TIME_USEC */
                if ( status == 0 )
                {
                    hal_nanosleep(100 * MICROSEC);
                }

                /* 4. enable all accelerometer axes in REG_PWR_MGMT_2 */
                if ( status == 0 )
                {
                    uint8_t data;

                    status = card_read_accel_reg( card, ICM20602_PWR_MGMT_2, &data, 1 );
                    if ( status == 0 )
                    {
                        data &= ICM20602_STBY_ACCEL_MASK;
                        data |= ICM20602_STBY_ACCEL_EN;
                        status = card_write_accel_reg( card, ICM20602_PWR_MGMT_2, &data, 1 );
                    }
                }

                /* 5. configure SMPLRT_DIV */
                if ( status == 0 )
                {
                    uint8_t data = ICM20602_SMPLRT_100Hz;
                    status = card_write_accel_reg( card, ICM20602_SMPLRT_DIV, &data, 1 );
                }
            }
            else
            {
                status = 0;
            }
        }
        else
        {
            if ( ( power & ICM20602_SLEEP ) > 0 )
            {
                /* chip is set to sleep mode */
                status = 0;
            }
            else
            {
                /* 1. set CLK_PLL and SLEEP in REG_PWR_MGMT_1 */
                power |= ICM20602_SLEEP | ICM20602_CLK_PLL;
                status = card_write_accel_reg( card, ICM20602_PWR_MGMT_1, &power, 1 );
            }
        }
    }

    return status;
}


/*****************************************************************************/


/***** GLOBAL FUNCTIONS *****/


/* @todo remove from global scope */
int32_t card_write_accel_reg(uint8_t card, uint8_t reg, uint8_t *p_data, uint8_t length)
{
    int32_t status=0;
    uint8_t addr, data[length+1];

    data[0] = reg;
    memcpy( &data[1], p_data, length );

    addr = accel_addr( card );

#if (defined DEBUG_ACCEL)
    {
        uint8_t _i;

        debug_print("I2C slave %02X, reg %02X, write [", addr, reg);
        for (_i = 0; _i < length; _i++)
        {
            debug_print_plain(" %02X", data[_i+1]);
        }
        debug_print_plain(" ]\n");
    }
#endif  /* DEBUG_ACCEL */

    status = hal_write_i2c( card, addr, data, length+1 );

    /* a non-zero status indicates a failure to perform I/O communications */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/* @todo remove from global scope */
int32_t card_read_accel_reg(uint8_t card, uint8_t reg, uint8_t *p_data, uint8_t length)
{
    int32_t status=0;
    uint8_t addr;

    addr = accel_addr( card );
    status = hal_write_then_read_i2c( card, addr, reg, p_data, length );

    /************************** DEBUG_ACCEL **************************/
    if ( status == 0 )
    {
        uint8_t _i;

        debug_print("I2C slave %02X, reg %02X, read [", addr, reg);
        for (_i = 0; _i < length; _i++)
        {
            debug_print_plain(" %02X", p_data[_i]);
        }
        debug_print_plain(" ]\n");
    }
    else
    {
        debug_print("hal_write_then_read_i2c failed with status %d\n", status);
    }
    /************************** DEBUG_ACCEL **************************/

    /* a non-zero status indicates a failure to perform I/O communications */
    if ( status != 0 )
    {
        status = -EIO;
    }

    return status;
}


/*****************************************************************************/
/** The card_read_accel function is responsible for reading and providing the
    accelerometer data.  The data format is twos compliment and 16 bits.

    @param[in] card card number of the Sidekiq of interest
    @param[out] p_x_data pointer to where the X accelerometer reading is written
    @param[out] p_y_data pointer to where the Y accelerometer reading is written
    @param[out] p_z_data pointer to where the Z accelerometer reading is written

    @return int32_t  status where 0=success, anything else is an error
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -EAGAIN accelerometer measurement is not available
    @retval -EIO error communicating with the accelerometer
*/
int32_t card_read_accel( uint8_t card,
                         int16_t *p_x_data,
                         int16_t *p_y_data,
                         int16_t *p_z_data )
{
    int32_t status;

    switch ( _skiq_get_part( card ) )
    {
        case skiq_z2:
        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
        case skiq_nv100:
            status = icm20602_read_accel( card, p_x_data, p_y_data, p_z_data );
            break;

        case skiq_mpcie:
        case skiq_m2:
            status = adxl346_read_accel( card, p_x_data, p_y_data, p_z_data );
            break;

        default:
            status = -ENOTSUP;
            break;
    }

    return status;
}


/*****************************************************************************/
/** The card_write_accel_state function is responsible for enabling or disabling
    the accelerometer to take measurements.

    @param card card number of the Sidekiq of interest
    @param enabled accelerometer state (1=enabled, 0=disabled)

    @return int32_t  status where 0=success, anything else is an error
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -EIO error communicating with the accelerometer
*/
int32_t card_write_accel_state( uint8_t card,
                                uint8_t enabled )
{
    int32_t status;

    switch ( _skiq_get_part( card ) )
    {
        case skiq_z2:
        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
        case skiq_nv100:
            status = icm20602_set_state( card, enabled );
            break;

        case skiq_mpcie:
        case skiq_m2:
            status = adxl346_set_state( card, enabled );
            break;

        default:
            status = -ENOTSUP;
            break;
    }

    return status;
}
