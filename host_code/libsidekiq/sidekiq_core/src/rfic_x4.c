/*****************************************************************************/
/** @file rfic_x4.c

 <pre>
 Copyright 2017-2021 Epiq Solutions, All Rights Reserved
 </pre>
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <stdarg.h>
#include <inttypes.h>
#include <math.h>
#include <ctype.h>              /* for isprint() */

#include "rfic_x4.h"
#include "rfic_x4_private.h"
#include "rfic_common.h"
#include "sidekiq_private.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_hal.h"
#include "fpga_jesd_ctrl.h"
#include "calc_ad9528.h"
#include "sidekiq_fpga_ctrl.h"

#if (defined DEBUG_RFIC_X4)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

#include "t_ad9528.h"
#include "ad9528.h"


#include "adi_hal.h"

#include "talise.h"
#include "talise_version.h"
#include "talise_error_types.h"
#include "talise_types.h"
#include "talise_gpio.h"
#include "talise_gpio_types.h"
#include "talise_radioctrl.h"
#include "talise_arm.h"
#include "talise_arm_types.h"
#include "talise_tdd_arm_firmware.h"
#include "talise_stream_bin.h"
#include "talise_stream_bin_adc_stitching.h"
#include "talise_cals.h"
#include "talise_cals_types.h"
#include "talise_jesd204.h"
#include "talise_jesd204_types.h"
#include "talise_tx.h"
#include "talise_rx.h"
#include "talise_error.h"
#include "ad9379_profiles.h"
#include "ad9379_parse_profile.h"
#include "core_elapsed.h"

// TODO: remove this once the sync is in the API
extern adiHalErr_t ADIHAL_spiWriteField(void *devHalInfo,
                                        uint16_t addr, uint8_t fieldVal, uint8_t mask, uint8_t startBit);
extern adiHalErr_t ADIHAL_spiWriteByte(void *devHalInfo, uint16_t addr, uint8_t data);

/* default sample rate and bandwidth during first initialization */
#define DEFAULT_SAMPLE_RATE             ((uint32_t)122880000)
#define DEFAULT_BANDWIDTH               ((uint32_t)100000000)

// preliminary analysis for jesd_sweep for X4
#define DEFAULT_JESD_TX_DIFFCTRL (3)
#define DEFAULT_JESD_TX_PRECURSOR (13)

//relocated from talise_config.h
#define DEFAULT_JESD_RX_PREEMPHASIS               (1)
#define DEFAULT_JESD_RX_SERIALIZER_AMPLITUDE      (15)

#define AD9379_ADC_RES (16)
#define AD9379_DAC_RES (14)

// refer to Table 130 of UG-1295 (p.214)
#define AD9379_MONITOR_INDEX_AGC_MON_8   (0x09)
#define AD9379_MONITOR_INDEX_AGC_MON_9   (0x0A)

#define RFIC_SPI_CS_AD9379_A   (0)
#define RFIC_SPI_CS_AD9528     (1)
#define RFIC_SPI_CS_AD9379_B   (2)

#define AD9528_SCRATCH_REG (0x07)

#define GPIO_RFE_CTRL_OE (0x1FF) // bits 0:8 need to be outputs for RFE

#define ADI_MAX_LOG_MSG_SIZE (400) // arbitrarily chosen constant...should be tons of room

#define MAX_NUM_MCS_TRIES (10) // randomly selected...we've seen this take 2 polls...so 10 sufficient

#define FRAMER_SYNC_IN_MASK (0x04) // see talise_jesd.h: TALISE_readFramerStatus()
#define DEFRAMER_SYNC_IN_MASK (0x82) // see talise_jesd.h: TALISE_readFramerStatus()

#define NUM_RFIC (2)

#define DEFAULT_PLL_FREQ (1800000000)

// TX calibration fails <= 61.44M currently
#define TX_SAMPLE_RATE_CAL_MIN (61440000)

// see mcsStatus table is Talise User Guide for bit defs (p. 67)
#define MCS_STATUS_DEV_CLK_DIV  (0x08)
#define MCS_STATUS_CLKPLL_SDM   (0x04)
#define MCS_STATUS_DIG_CLK      (0x02)
#define MCS_STATUS_JSED_SYSREF  (0x01)

// we use GPIO8 for frequency hopping
#define RFIC_HOP_GPIO (8)

#define MAX_CAL_RETRY (5) // arbitrarily chosen...testing indicates the max retry times we've seen as 2

/////////////////////////////////////////
// all possible cal flags...handy for reference...defined in talise_cal_types.h and p.162 of Talise HW ref manual
#if 0
#define ALL_INIT_CAL \
    (TAL_TX_BB_FILTER |     /*!< Tx BB filter calibration */                   \
     TAL_ADC_TUNER |        /*!< ADC tuner calibration */                      \
     TAL_TIA_3DB_CORNER |   /*!< TIA 3dB corner calibration */                 \
     TAL_DC_OFFSET |   /*!< DC offset calibration */                           \
     TAL_TX_ATTENUATION_DELAY |   /*!< Tx attenuation delay calibration */     \
     TAL_RX_GAIN_DELAY |    /*!< Rx gain delay calibration */                   \
     TAL_FLASH_CAL |   /*!< Flash converter comparator calibration */          \
     TAL_PATH_DELAY |   /*!< TX Path delay equalization calibration */            \
     TAL_TX_LO_LEAKAGE_INTERNAL |   /*!< Internal Tx LO leakage calibration */ \
     TAL_TX_LO_LEAKAGE_EXTERNAL |   /*!< External Tx LO leakage calibration */ \
     TAL_TX_QEC_INIT |   /*!< Tx quadrature error correction calibration */    \
     TAL_LOOPBACK_RX_LO_DELAY |   /*!< Loopback Rx LO delay path calibration */ \
     TAL_LOOPBACK_RX_RX_QEC_INIT |   /*!< Loopback Rx quadrature error correction calibration */ \
     TAL_RX_LO_DELAY |   /*!< Rx LO delay path calibration - ADRV9009 DON'T USE */                  \
     TAL_RX_QEC_INIT |   /*!< Rx quadrature error correction calibration */    \
     TAL_RX_PHASE_CORRECTION |   /*!< Rx Phase correction calibration */       \
     TAL_ORX_LO_DELAY |   /*!< ORx LO delay path calibration - ADRV9009 DON'T USE */                \
     TAL_ORX_QEC_INIT |   /*!< ORx quadrature error correction calibration */  \
     TAL_TX_DAC)   /*!< Tx DAC passband calibration */
#endif
/////////////////////////////////////////


#define BASE_INIT_CAL \
    (TAL_ADC_TUNER |        /*!< ADC tuner calibration */                      \
     TAL_TIA_3DB_CORNER |   /*!< TIA 3dB corner calibration */                 \
     TAL_RX_GAIN_DELAY |   /*!< Rx gain delay calibration */                   \
     TAL_FLASH_CAL)   /*!< Flash converter comparator calibration */    \

#define TX_INIT_CAL \
    (TAL_TX_BB_FILTER |     /*!< Tx BB filter calibration */            \
     TAL_TX_ATTENUATION_DELAY |   /*!< Tx attenuation delay calibration */     \
     TAL_PATH_DELAY | /*!< TX Path delay equalization calibration */    \
     TAL_TX_LO_LEAKAGE_INTERNAL |   /*!< Internal Tx LO leakage calibration */ \
     TAL_TX_QEC_INIT |   /*!< Tx quadrature error correction calibration */    \
     TAL_TX_DAC)   /*!< Tx DAC passband calibration */

#define SKIQ_RX_CAL_TYPES \
    (skiq_rx_cal_type_dc_offset | skiq_rx_cal_type_quadrature)

// just turn all tracking on all the time for now
#define TRACKING_CAL (TAL_TRACK_ALL)

/* Use a calibration of 25s since ADI's public source uses 20s for all calibration except TX LO
 * Leakage which it runs separately.  Add 5 seconds to cover the LO leakage calibration. */
#define INIT_CAL_TIMEOUT_MS (25000)

/** @brief Max quarter dB attentuation for Tx.
    \note (41950 mdB / 1000) * 4 = 167.8 quarter dB = 167 (uint16_t) */
#define MAX_TX_QUARTER_DB_ATTEN (167)
/** @brief Min quarter dB attentuation for Tx. */
#define MIN_TX_QUARTER_DB_ATTEN (0)

/** @brief Maximum mdB attentuation for Tx */
#define MAX_TX_MILLI_DB_ATTEN           41950

/** @brief Table 36 from Talise User Guide. Any time we cross the boundary, we need to re-run cal */
#define MAX_FREQ_TABLE_ENTRIES          ARRAY_SIZE(_freqTable)

/** @brief ADI recommends performing calibration if tuning more than 100MHz from the previous
    calibration frequency */
#define MAX_FREQ_CAL_OFFSET             100000000

////////////////////////////////
// TODO: we'll need to update this when we support higher sample rates
// from p. 169 of UG-1295...the cal PLL (aux PLL) is used for RX QEC
// and has an upper bound of 6.061GHz, so we need to prevent our cal
// from running at this upper bound (a 200MHz profile would require
// cal PLL to go up to 6.1GHz.  To handle this, we'll plan to never
// perform a cal <100MHz or >5900MHz...since the cal is required to
// run every 100MHz boundary, we can just tune to the maximum bound,
// run the cal, and then tune to the actually requested PLL
// TODO: it seems as though there is some relationship with these bounds
// and the sample rate...we need to update our bounds relative to sample rate
// Currently, our max is 245.75...we empirically determined that it failes <110M
// with this configuration...so we'll bound it at 115M to be "safe" but
// we need to detemine this bound more elegantly in the future
#define MIN_CAL_FREQ (150000000) // 125 MHz
#define MAX_CAL_FREQ (5900000000) // 5900 MHz
////////////////////////////////

// Table 35 of Talise User Guide indicates we need to adjust loop filter BW
#define LOOP_FILTER_LOW_MAX_FREQ (3000000000) // 3000 MHz
#define LOOP_FILTER_BW_KHZ_LOW (50)   // 50 kHz
#define LOOP_FILTER_BW_KHZ_HIGH (300) // 300 kHz

// in Talise User Guide, refer to RF PLL LOCK STATUS (p. 132)
#define TAL_CLK_PLL_LOCK (0x01)
#define TAL_RF_PLL_LOCK (0x02)
#define TAL_AUX_PLL_LOCK (0x04)
#define TAL_RX_ONLY_PLL_LOCK (TAL_CLK_PLL_LOCK | TAL_RF_PLL_LOCK)
#define TAL_TRX_PLL_PLOCK (TAL_RX_ONLY_PLL_LOCK | TAL_AUX_PLL_LOCK) // AUX PLL used only for TX

#define HOST_REF_CLOCK_FREQ (40000000) // 40MHz

/* warp voltage defines...Preferred operating range is 0.5V to 2.5V, 
   but permit the full range of the DAC to be configured by the user.
   According to Jeff P "I would suggest making the limits 0-65535 in order 
   to support a wider range of oscillators.  Normally it won't be programmed 
   outside of the 0.4-2.4V range, but no harm will come to the part in the 
   0-2.5V range.  (Certain specs might not apply at the extremes.)" 
*/
#define MIN_WARP_VOLTAGE (0)
#define DEFAULT_WARP_VOLTAGE (32768)
#define MAX_WARP_VOLTAGE (65535)

// Note: we're always just going to generate a test tone at a fixed offset of 500kHz
#define DEFAULT_TX_TEST_TONE_FREQ_OFFSET (500000)

// increasing retries for FPGA 3.17.1 which resolves X4 latency issues (max failures in regression is ~17)
#define MAX_NUM_JESD_RETRIES (50) 

#define MAX_NUM_JESD_STATUS_CHECKS (15)
#define JESD_STATUS_CHECK_DELAY_MS (10) // 10ms between each status check

#define LO_FREQ_MIN_RESOLUTION_HZ (1000) // 1kHz


/* AD9379 Frequency Range RX: 75 to 6000 MHz, TX: 75 to 6000 MHz*/
#define AD9379_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(75ULL*RATE_MHZ_TO_HZ)
#define AD9379_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define AD9379_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(75ULL*RATE_MHZ_TO_HZ)
#define AD9379_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

/********************* half of maximum sample clock (not rate!) *********************/
/* In order to meet timing, the FPGA cannot make the decimator available at the full maximum sample
 * clock and will indicate so with the DECIMATOR_HALF_MAX_RATE bit in each DECIMATOR_BUILD_INFO
 * register.  So if there's a future FPGA design that works at the full sample clock, the code is
 * designed to handle both cases. */
#define X4_HALF_MAX_SAMPLE_CLK_HZ                 125000000


/********************* ORX to dual lane *********************/
/* This could eventually be 307200 if the FPGA design was constrained accordingly, but for now, it's
 * constrained to 250MHz */
#define ORX_DUAL_LANE_RATE_THRESHOLD_KHZ          250000
#define ORX_SINGLE_LANE_0_SERIALIZER_SETTING      BIT(2)   /* Lane 2 */
#define ORX_SINGLE_LANE_1_SERIALIZER_SETTING      BIT(3)   /* Lane 3 */
#define ORX_DUAL_LANE_SERIALIZER_SETTING          (ORX_SINGLE_LANE_0_SERIALIZER_SETTING | \
                                                   ORX_SINGLE_LANE_1_SERIALIZER_SETTING)


/********************* TX to dual lane *********************/
/* This could eventually be 307200 if the FPGA design was constrained accordingly, but for now, it's
 * constrained to 250MHz */
#define TX_DUAL_LANE_RATE_THRESHOLD_KHZ           250000
#define TX_DUAL_LANE_RATE_THRESHOLD_HZ            (TX_DUAL_LANE_RATE_THRESHOLD_KHZ * RATE_KHZ_TO_HZ)
#define TX_LANE_SEL_0_DESERIALIZER_LANES          (BIT(0) | BIT(1)) /* lanes 0 and 1 enabled */
#define TX_LANE_SEL_1_DESERIALIZER_LANES          (BIT(2) | BIT(3)) /* lanes 2 and 3 enabled */
#define TX_DUAL_LANE_DESERIALIZER_SETTING         (TX_LANE_SEL_0_DESERIALIZER_LANES | \
                                                   TX_LANE_SEL_1_DESERIALIZER_LANES)

//taliseRadioCtlCfg1_t has no enum for SW control but 0 means no enables (Tx/Rx) are controlled by pin
#define TAL_TXRX_ENABLE_MODE_SW                      0


// used to ID which chip 9379 A or 9739 B
typedef enum
{
    RF_CHIP_A = 0,
    RF_CHIP_B = 1,
    RF_CHIP_MAX
} rf_chip_id_t;

// data structure that contains the freq and freq index when calibration was ran
typedef struct
{
    uint8_t freqTableIndex; // index into freqTable of current freq
    uint64_t lastCalFreq;   // last freq when cal was performed
} freqCalState_t;

typedef struct
{
    rate_config_t rate;
    taliseInit_t *p_init;
} talise_profile_t;

// used to track TX test tone config
typedef struct
{
    bool enable;
    int32_t tx1_freq_offset;
    int32_t tx2_freq_offset;
} tx_test_tone_t;

typedef struct
{
    uint8_t tal_gain;
    bool gain_valid;
} tal_rx_config_t;

typedef struct
{
    uint16_t tal_atten_mdB;
} tal_tx_config_t;

typedef struct
{
    taliseFhmMode_t mode;
    taliseFhmConfig_t config;
} tal_fhm_config_t;

typedef struct
{
    char id;

    uint32_t gpio_out_ena; // TALISE_getGpioOe
    uint32_t gpio_src_ctrl; // TALISE_getGpioSourceCtrl
    uint32_t gpio_pin_level; // TALISE_getGpioPinLevel

    uint16_t gpio_3v3_out_ena; // TALISE_getGpio3v3Oe
    uint16_t gpio_3v3_src_ctrl; // TALISE_getGpio3v3SourceCtrl
    uint16_t gpio_3v3_pin_level; // TALISE_getGpio3v3PinLevel

    uint8_t mon_index; // TALISE_getGpioMonitorOut
    uint8_t mon_mask; // TALISE_getGpioMonitorOut

    taliseRxORxChannels_t rx_chans; // TALISE_getRxTxEnable
    taliseTxChannels_t tx_chans; // TALISE_getRxTxEnable

    uint64_t rf_pll_freq; // TALISE_getRfPllFrequency
    uint16_t rf_pll_loopbw; // TALISE_getRfPllLoopFilter
    uint8_t rf_pll_stability; // TALISE_getRfPllLoopFilter

    taliseGainMode_t tal_gain_mode;

    tal_rx_config_t rx1;
    tal_rx_config_t rx2;
    tal_rx_config_t orx1;

    tal_tx_config_t tx1;
    tal_tx_config_t tx2;

    tal_fhm_config_t fhm;

    uint8_t tal_pin_control_mask;      //TALISE_getRadioCtlPinMode
    taliseRadioCtlCfg2_t tal_pin_control_mask_orx;  //TALISE_getRadioCtlPinMode

} tal_chip_config_t;

struct found_profile_t
{
    uint8_t dec_rate;
    ad9379_profile_t *p_profile;
};

static ad9379_profile_t *_p_min_rx_profile = NULL, *_p_min_orx_profile = NULL, *_p_min_tx_profile = NULL;
static ad9379_profile_t *_p_max_rx_profile = NULL, *_p_max_orx_profile = NULL, *_p_max_tx_profile = NULL;
static ad9379_profile_t *_p_max_dual_lane_orx_profile = NULL;
static uint32_t _max_rx_profile_index = 0, _max_orx_profile_index = 0, _max_tx_profile_index = 0;

typedef struct
{
    skiq_freq_tune_mode_t tune_mode;
    freq_range_t hop_range;
    uint16_t num_freqs;
    uint64_t hop_list[SKIQ_MAX_NUM_FREQ_HOPS];
    // refer to http://confluence/display/EN/Sidekiq+X4+-+Fast+Frequency+Hopping
    freq_hop_t mailbox;  // this is the only thing users can write to
    freq_hop_t next_hop; // this is populated from mailbox when writing next hop
    freq_hop_t curr_hop; // this is populated only after a hop was performed from next hop
} x4_freq_hop_t;


static const ad9528SpiSettings_t _x4_clockSpiSettings =
{
    .chipSelectIndex = RFIC_SPI_CS_AD9528,
    .writeBitPolarity = 0,
    .longInstructionWord = 1,
    .MSBFirst = 1,
    .CPHA = 0,
    .CPOL = 0,
    .enSpiStreaming = 0,
    .autoIncAddrUp = 1,
    .fourWireMode = 1,
    .spiClkFreq_Hz = 0,         /* not used */
    .reg_read = hal_rfic_read_reg,
    .reg_write = hal_rfic_write_reg,
};

static const clk_sel_t _x4_clk_sel =
{
    .fpga_clk_sel_mask = AD9528_GBT_CLK_OUT(1) | AD9528_GBT_CLK_OUT(2),
    .debug_clk_sel = 0,
    // X4 doesn't have an RFFC, so just set these to max
    .rffc1_clk_sel = AD9528_MAX_CLOCK_OUTS,
    .rffc2_clk_sel = AD9528_MAX_CLOCK_OUTS,
};

///////////////////////////////////
static const ad9528sysrefSettings_t _x4_clockSysrefSettings =
{
    .sysrefRequestMethod = 0,  // SPI = 0, PIN = 1
    .sysrefSource = INTERNAL,  // EXTERNAL = 0, EXT_RESAMPLED = 1, INTERNAL = 2
    .sysrefPinEdgeMode = LEVEL_ACTIVE_HIGH,    // LEVEL_ACTIVE_HIGH = 0, LEVEL_ACTIVE_LOW = 1, RISING_EDGE = 2, FALLING_EDGE = 3
    .sysrefPinBufferMode = DISABLED,
    .sysrefPatternMode = NSHOT,    // NSHOT = 0, CONTINUOUS = 1, PRBS = 2, STOP = 3
    .sysrefNshotMode = EIGHT_PULSES, // ONE_PULSE = 0, TWO_PULSES = 2, FOUR_PULSES = 3, SIX_PULSES = 4, EIGHT_PULSES = 5
    .sysrefDivide = 512,
    .sysrefClock = SYSREF_PLL2_CLK, /* default value, may be changed by _update_ad9528_struct() */
};


/**** These defs from the X4 schemaitc
 * CLK0 (test connector)
 * CLK1 (GBTCLK0)
 * CLK2 (GBTCLK1)
 * CLK3 (FPGA_SYSREF)
 * CLK4 (CLK3_BIDIR)
 * CLK5 (CLK1_M2C)
 *
 * CLK7 (SYSREF TALISE B)
 * CLK8 (REFCLK TALISE B)
 *
 * CLK12 (SYSREF TALISE A)
 * CLK13 (REFCLK TALISE A)
 **/

static const ad9528outputSettings_t _x4_clockOutputSettings =
{
    // bit per output, if 1 power down that output, 14 outputs in total
    .outPowerDown = 0x0,

    // CHANNEL_DIV, PLL1_OUTPUT, SYSREF, INV_PLL1_OUTPUT  : order is 0....14
    .outSource = {
        [0]  = CHANNEL_DIV,
        [1]  = CHANNEL_DIV,
        [2]  = CHANNEL_DIV,
        [3]  = SYSREF_PLL2,     /* default value, may be changed by _update_ad9528_struct() */
        [4]  = CHANNEL_DIV,
        [5]  = CHANNEL_DIV,
        [6]  = CHANNEL_DIV,
        [7]  = SYSREF_PLL2,     /* default value, may be changed by _update_ad9528_struct() */
        [8]  = CHANNEL_DIV,
        [9]  = CHANNEL_DIV,
        [10] = CHANNEL_DIV,
        [11] = CHANNEL_DIV,
        [12] = SYSREF_PLL2,     /* default value, may be changed by _update_ad9528_struct() */
        [13] = CHANNEL_DIV,
    },

    // LVDS, LVDS_BOOST, HSTL
    .outBufferCtrl = {
        [0]  = LVDS,
        [1]  = LVDS,
        [2]  = LVDS,
        [3]  = HSTL,
        [4]  = HSTL,
        [5]  = HSTL,
        [6]  = LVDS,
        [7]  = LVDS,
        [8]  = LVDS,
        [9]  = LVDS,
        [10] = LVDS,
        [11] = LVDS,
        [12] = LVDS,
        [13] = LVDS,
    },

    /* 5 bits, [4] = enable, [3:0] = delay */
    .outAnalogDelay = {
        [ 0 ... (AD9528_MAX_CLOCK_OUTS-1) ] = 0,
    },

    /* 6 bits, 1/2 clock resolution @ channel div input frequency */
    .outDigitalDelay = {
        [ 0 ... (AD9528_MAX_CLOCK_OUTS-1) ] = 0,
    },

    /* 8 bit channel divider, default to 1 */
    .outChannelDiv = {
        [ 0 ... (AD9528_MAX_CLOCK_OUTS-1) ] = 1,
    },

    /* output clock frequency per clock output (note: this is informational only) */
    .outFrequency_Hz = {
        [ 0 ... (AD9528_MAX_CLOCK_OUTS-1) ] = 0,
    },

    /* 1 if vcxo to distribution is enabled */
    .vcxo_to_dist = 0,
};
///////////////////////////////////

// Table 36 from Talise User Guide...any time we cross the boundary, we need to re-run cal
static const freq_range_t _freqTable[] =
{
    { 0, 187500000 },            // Epiq added...this isn't defined in the user guide, but we
                                 // received word that this will be updated
    { 187500000, 375000000 },    // divide by 32 (187.5-375M)
    { 375000000, 750000000 },    // divide by 16 (375-750M)
    { 750000000, 1500000000 },   // divide by 8 (750-1500M)
    { 1500000000, 3000000000 },  // divide by 4 (1500-3000M)
    { 3000000000, 6000000000 },  // divide by 2 (3000-6000M)
};

// currently selected profile (contains all the radio config)
static taliseInit_t * _taliseDevice[SKIQ_MAX_NUM_CARDS][RF_CHIP_MAX] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... RF_CHIP_MAX-1] = NULL,
    },
};
ARRAY_WITH_DEFAULTS(static taliseInit_t *, _p_user_taliseDevice, SKIQ_MAX_NUM_CARDS, NULL);   //should we be tracking rfic a,b here?
static ad9379_profile_t * _p_last_profile[SKIQ_MAX_NUM_CARDS][RF_CHIP_MAX] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... RF_CHIP_MAX-1] = NULL,
    },
};
static uint8_t _p_dec_rates[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... skiq_rx_hdl_end-1] = 0,
    },
};
ARRAY_WITH_DEFAULTS(static uint32_t, _dev_clock, SKIQ_MAX_NUM_CARDS, 0);

// contains the settings when the calibration was last ran on the card/hdl
#define INVALID_CAL_INDEX               ARRAY_SIZE(_freqTable)
#define INVALID_CAL_FREQ                0
#define FREQ_CAL_STATE_INITIALIZER                                      \
    {                                                                   \
        .freqTableIndex = INVALID_CAL_INDEX,                            \
        .lastCalFreq = INVALID_CAL_FREQ,                                \
    }

static freqCalState_t _rx_cal_state[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... skiq_rx_hdl_end-1] = FREQ_CAL_STATE_INITIALIZER,
    },
};

static freqCalState_t _tx_cal_state[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... skiq_tx_hdl_end-1] = FREQ_CAL_STATE_INITIALIZER,
    },
};


static ARRAY_WITH_DEFAULTS(uint64_t, _fpga_gbt_clock, SKIQ_MAX_NUM_CARDS, 0);

static ARRAY_WITH_DEFAULTS(uint16_t, _warp_voltage, SKIQ_MAX_NUM_CARDS, DEFAULT_WARP_VOLTAGE);

static bool _rfic_pin_control_enable[SKIQ_MAX_NUM_CARDS][RF_CHIP_MAX] = { {false} };


// TX quadcal mode for each card/hdl
static skiq_tx_quadcal_mode_t _tx_quadcal[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... skiq_tx_hdl_end-1] = skiq_tx_quadcal_mode_manual,
    },
};

// RX cal mode for each card/hdl
static skiq_rx_cal_mode_t _rx_cal_mode[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... skiq_rx_hdl_end-1] = skiq_rx_cal_mode_auto,
    },
};

// RX cal types for each card/hdl
static uint32_t _rx_cal_mask[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... skiq_rx_hdl_end-1] = SKIQ_RX_CAL_TYPES,
    },
};

static void log_adihal_msg( adiLogLevel_t logLevel, const char *format, ... );

static const adiEpiqFuncs_t _adiFuncs = {
    .spi_read_byte =  hal_rfic_read_reg,
    .spi_write_byte = hal_rfic_write_reg,
    .log_msg =        log_adihal_msg,
};

static ad9528Device_t _clockDevice[SKIQ_MAX_NUM_CARDS];
static taliseDevice_t _rficDeviceA[SKIQ_MAX_NUM_CARDS];
static taliseDevice_t _rficDeviceB[SKIQ_MAX_NUM_CARDS];

#define HAL_DEV_INITIALIZER(_rfic)                      \
    {                                                   \
        .card = 0,                                      \
            .cs = RFIC_SPI_CS_AD9379_ ## _rfic,         \
            .adiLogLevel = ADIHAL_LOG_ALL,              \
            .p_funcs = (adiEpiqFuncs_t *)(&_adiFuncs),  \
            }

static ARRAY_WITH_DEFAULTS(adiEpiqDev_t, _halDevA, SKIQ_MAX_NUM_CARDS, HAL_DEV_INITIALIZER(A));
static ARRAY_WITH_DEFAULTS(adiEpiqDev_t, _halDevB, SKIQ_MAX_NUM_CARDS, HAL_DEV_INITIALIZER(B));

/* track configured transmit handle attenuation since Talise will only report the configured
 * attenuation iff the channel is enabled */
static tal_tx_config_t _tx_config[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {
        [ skiq_tx_hdl_A1 ... (skiq_tx_hdl_end-1) ] = {
            .tal_atten_mdB = MAX_TX_MILLI_DB_ATTEN,
        },
    },
};

/* track configured receive handle gain and its validity since Talise will only report the
 * configured gain iff the chip has entered receiver mode */
static tal_rx_config_t _rx_config[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {
        [ skiq_rx_hdl_A1 ... (skiq_rx_hdl_end-1) ] = {
            .tal_gain = 0,
            .gain_valid = false,
        },
    },
};

// we need to cache the pin level for restoring...
// reading from the chip doesn't return the output config, but only what's read as an input
static uint32_t _gpio_pin_level[SKIQ_MAX_NUM_CARDS][RF_CHIP_MAX] = { {0} };
static uint16_t _gpio_3v3_pin_level[SKIQ_MAX_NUM_CARDS][RF_CHIP_MAX] = { {0} };

// Note: for this release, we'll run with RX1 and RX2 always enabled together...
// when ADI supports ORX and RX2 enabled, we'll need to update this logic
static const taliseRxORxChannels_t rx_hdl_to_chan_en[skiq_rx_hdl_end] =
{ [skiq_rx_hdl_A1] = TAL_RX1RX2_EN /*TAL_RX2_EN */,
  [skiq_rx_hdl_A2] = TAL_RX1RX2_EN /*TAL_RX1_EN */,
  [skiq_rx_hdl_B1] = TAL_RX1RX2_EN /*TAL_RX2_EN */,
  [skiq_rx_hdl_B2] = TAL_RX1RX2_EN /*TAL_RX1_EN */,
  [skiq_rx_hdl_C1] = TAL_ORX1_EN /*C1*/,
  [skiq_rx_hdl_D1] = TAL_ORX1_EN /*D1*/ };


// maps RX handle to chip ID (0=A, 1=B)
static const uint8_t rx_hdl_to_chip_id[skiq_rx_hdl_end] =
{ [skiq_rx_hdl_A1] = RF_CHIP_A,
  [skiq_rx_hdl_A2] = RF_CHIP_A,
  [skiq_rx_hdl_B1] = RF_CHIP_B,
  [skiq_rx_hdl_B2] = RF_CHIP_B,
  [skiq_rx_hdl_C1] = RF_CHIP_A,
  [skiq_rx_hdl_D1] = RF_CHIP_B };

  // maps RX handle to chip ID (0=A, 1=B)
static const uint8_t rx_hdl_to_other_chip_id[skiq_rx_hdl_end] =
{ [skiq_rx_hdl_A1] = RF_CHIP_B,
  [skiq_rx_hdl_A2] = RF_CHIP_B,
  [skiq_rx_hdl_B1] = RF_CHIP_A,
  [skiq_rx_hdl_B2] = RF_CHIP_A,
  [skiq_rx_hdl_C1] = RF_CHIP_B,
  [skiq_rx_hdl_D1] = RF_CHIP_A };

// maps TX handle to talise TX channel enable
static const taliseTxChannels_t tx_hdl_to_chan_en[skiq_tx_hdl_end] =
{ [skiq_tx_hdl_A1] = TAL_TX1,
  [skiq_tx_hdl_A2] = TAL_TX2,
  [skiq_tx_hdl_B1] = TAL_TX1,
  [skiq_tx_hdl_B2] = TAL_TX2 };

// maps TX handle to chip ID (0=A, 1=B)
static const uint8_t tx_hdl_to_chip_id[skiq_tx_hdl_end] =
{ [skiq_tx_hdl_A1] = RF_CHIP_A,
  [skiq_tx_hdl_A2] = RF_CHIP_A,
  [skiq_tx_hdl_B1] = RF_CHIP_B,
  [skiq_tx_hdl_B2] = RF_CHIP_B };

#define TX_TEST_TONE_INITIALIZER                               \
    {                                                          \
        .enable = false,                                       \
        .tx1_freq_offset = DEFAULT_TX_TEST_TONE_FREQ_OFFSET,   \
        .tx2_freq_offset = DEFAULT_TX_TEST_TONE_FREQ_OFFSET,   \
    }

#define FREQ_HOP_INITIALIZER                                   \
    {                                                          \
        .tune_mode = skiq_freq_tune_mode_standard,             \
        .num_freqs = 0,                                        \
    }

static ARRAY_WITH_DEFAULTS(tx_test_tone_t, _tx_test_toneA, SKIQ_MAX_NUM_CARDS, TX_TEST_TONE_INITIALIZER);
static ARRAY_WITH_DEFAULTS(tx_test_tone_t, _tx_test_toneB, SKIQ_MAX_NUM_CARDS, TX_TEST_TONE_INITIALIZER);

static x4_freq_hop_t _freq_hop_config[SKIQ_MAX_NUM_CARDS][RF_CHIP_MAX] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [0 ... (RF_CHIP_MAX-1)] = FREQ_HOP_INITIALIZER,
    },
};

static ARRAY_WITH_DEFAULTS(bool, rfic_active, SKIQ_MAX_NUM_CARDS, false);

static taliseDevice_t * rf_id_to_rficDevice( rf_id_t *p_id );
static int32_t restore_tx_test_tone( rf_id_t *p_id );

static int32_t _perform_mcs( ad9528Device_t *p_ad9528, taliseDevice_t *p_rfic_a, taliseDevice_t *p_rfic_b );

static int32_t _reset_jesd( ad9528Device_t *p_ad9528,
                            taliseDevice_t *p_rfic_a,
                            taliseDevice_t *p_rfic_b,
                            bool check_deframers,
                            jesd_sync_result_t *p_sync_result );


// check req_freq vs cal state
// determine chans to have enabled
static bool _is_cal_required( rf_id_t *p_id,
                              bool is_transmit,
                              uint64_t req_freq, uint64_t *p_cal_freq,
                              taliseRxORxChannels_t *p_calRxChan,
                              taliseTxChannels_t *p_calTxChan );
// update chans (setRxTxEnable)
// run cal
// update cal state
static int32_t _run_cal( rf_id_t *p_id,
                         taliseDevice_t *p_rfic,
                         uint64_t cal_freq,
                         taliseRxORxChannels_t calRxChans,
                         taliseTxChannels_t calTxChans );

static int32_t _reset_rf_iface( uint8_t card,
                                ad9528Device_t *p_ad9528,
                                taliseDevice_t *p_rfic_a, taliseDevice_t *p_rfic_b );

// caches all relevant settings to be restored upon reinit
static int32_t _cache_config( uint8_t card,
                              tal_chip_config_t *p_chipA_config,
                              tal_chip_config_t *p_chipB_config );
// restores all previously cached settings
static int32_t _restore_config( uint8_t card,
                                tal_chip_config_t *p_chipA_config,
                                tal_chip_config_t *p_chipB_config );

static int32_t _talise_set_params_from_profile( rf_id_t *p_id,
                                      ad9379_profile_t *p_profile,
                                      taliseInit_t *p_taliseDevice );

static int32_t _talise_set_default_params( rf_id_t *p_id,
                                      taliseInit_t *p_taliseDevice );

// Note: this can return an invalid index (MAX_FREQ_TABLE_ENTRIES) if the frequency specified is out of bounds
static bool _find_freq_table_index( uint64_t lo_freq, uint8_t *p_index );

static int32_t _tx_test_tone_actual_freq( uint32_t tx_sample_rate_kHz, int32_t test_tone_kHz );

static int32_t _reset_and_init( rf_id_t *p_id );
static int32_t _reset_and_init_rfic( rf_id_t *p_id );
static void _display_version_info( void );
static int32_t _release( rf_id_t *p_id );

static void _init_min_max_sample_rate( uint8_t card );

// 9379 is a TDD chip, so RX and TX tuning are shared
// TODO: we may be able to use the AUX PLL for ORx and then tune independently
static int32_t _write_freq( rf_id_t *p_id, taliseDevice_t *p_rfic, uint64_t freq, double *p_act_freq );
static int32_t _write_rx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq );
static int32_t _write_tx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq );

static int32_t _write_rx_spectrum_invert( rf_id_t *p_id, bool invert );
static int32_t _write_tx_spectrum_invert( rf_id_t *p_id, bool invert );

static int32_t _write_tx_attenuation( rf_id_t *p_id, uint16_t atten );
static int32_t _read_tx_attenuation( rf_id_t *p_id, uint16_t *p_atten );
static int32_t _read_tx_attenuation_range( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min );

static int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode );
static int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode );
static int32_t _read_rx_gain_range( rf_id_t *p_id, uint8_t *p_gain_max, uint8_t *p_gain_min );
static int32_t _write_rx_gain( rf_id_t *p_id, uint8_t gain );
static int32_t _read_rx_gain( rf_id_t *p_id, uint8_t *p_gain );

static int32_t _read_adc_resolution( uint8_t *p_adc_res );
static int32_t _read_dac_resolution( uint8_t *p_dac_res );

static int32_t _read_warp_voltage( rf_id_t *p_id, uint16_t *p_warp_voltage );
static int32_t _write_warp_voltage( rf_id_t *p_id, uint16_t warp_voltage );

static int32_t _enable_rx_chan( rf_id_t *p_id, bool enable );
static int32_t _enable_rx_chan_all( rf_id_t *p_id, bool enable );
static int32_t _enable_tx_chan( rf_id_t *p_id, bool enable );
static int32_t _enable_tx_chan_all( rf_id_t *p_id, bool enable );

static int32_t _config_tx_test_tone( rf_id_t *p_id, bool enable );
static int32_t _read_tx_test_tone_freq( rf_id_t *p_id,
                                        int32_t *p_test_freq_offset );
static int32_t _write_tx_test_tone_freq( rf_id_t *p_id,
                                         int32_t test_freq_offset );

static void _read_min_rx_sample_rate( rf_id_t *p_id, uint32_t *p_min_rx_sample_rate );
static void _read_max_rx_sample_rate( rf_id_t *p_id, uint32_t *p_max_rx_sample_rate );
static void _read_min_tx_sample_rate( rf_id_t *p_id, uint32_t *p_min_tx_sample_rate );
static void _read_max_tx_sample_rate( rf_id_t *p_id, uint32_t *p_max_tx_sample_rate );

static int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );
static int32_t _write_rx_sample_rate_and_bandwidth_multi( rf_id_t *p_id, skiq_rx_hdl_t handles[],
                                                          uint8_t nr_handles,
                                                          uint32_t rate[],
                                                          uint32_t bandwidth[]);

static int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );

static int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );
static int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );

static int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
static int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
static int32_t _init_gpio( rf_id_t *p_id, uint32_t output_enable );
static int32_t _write_gpio( rf_id_t *p_id, uint32_t value );
static int32_t _read_gpio( rf_id_t *p_id, uint32_t *p_value );

static int32_t _read_fpga_rf_clock( rf_id_t *p_id, uint64_t *p_freq );

static int32_t _read_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode );
static int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode );
static int32_t _run_tx_quadcal( rf_id_t *p_id );

static int32_t _read_rx_stream_hdl_conflict( rf_id_t *p_id,
                                             skiq_rx_hdl_t *p_conflicting_hdls,
                                             uint8_t *p_num_hdls );

static int32_t _read_control_output_rx_gain_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );
static int32_t _write_control_output_config( rf_id_t *p_id, uint8_t mode, uint8_t ena );
static int32_t _read_control_output_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );

static uint32_t _read_hop_on_ts_gpio( rf_id_t *p_id, uint8_t *p_chip_index );

static int32_t _write_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
static int32_t _read_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

// NOTE: hopping for RX and TX are the same for X4...so use the same functions...
static int32_t _config_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index );
static int32_t _read_hop_list( rf_id_t *p_id, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );

static int32_t _write_next_hop( rf_id_t *p_id, uint16_t freq_index );

static int32_t _hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );

static int32_t _read_curr_hop( rf_id_t *p_id, freq_hop_t *p_hop );
static int32_t _read_next_hop( rf_id_t *p_id, freq_hop_t *p_hop );

static int32_t _read_temp( rf_id_t *p_id, int8_t *p_temp_in_deg_C );

static int32_t _init_from_file( rf_id_t *p_id, FILE* p_file );

static int32_t _read_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode );
static int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode );

static int32_t _run_rx_cal( rf_id_t *p_id );

static int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask );
static int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask );

static int32_t _read_rx_cal_types_avail( rf_id_t *p_id, uint32_t *p_cal_mask );

static int32_t _talise_set_params_from_file( rf_id_t *p_id,
                                             taliseInit_t *p_dest_taliseDevice,
                                             taliseInit_t *p_src_taliseDevice );

static int32_t _write_tx_rx_enable_mode(rf_id_t *p_id, skiq_rfic_pin_mode_t p_mode);

static int32_t _read_tx_rx_enable_mode(rf_id_t *p_id, skiq_rfic_pin_mode_t *p_mode);

static int32_t _setRxTxEnable(const rf_id_t *p_id, taliseDevice_t *device, taliseRxORxChannels_t rxOrxChannel, taliseTxChannels_t txChannel);

static int32_t _getRxTxEnable(const rf_id_t *p_id, taliseDevice_t *device, taliseRxORxChannels_t *rxOrxChannel, taliseTxChannels_t *txChannel);

static int32_t _talise_profile_alloc( taliseInit_t **p_taliseDevice );
static void _talise_profile_free( taliseInit_t **pp_taliseDevice );

static int32_t _convert_skiq_rx_cal_to_tal_cal( skiq_rx_hdl_t hdl, uint32_t skiq_cal_mask, uint32_t *p_tal_cal_mask );

static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

static int32_t _reset_cdr( uint8_t card );  // CDR=clock data recovery...this is done by ADI and is recommended for JESD syncing (see https://github.com/analogdevicesinc/no-OS/blob/master/projects/adrv9009/src/app/app_talise.c...this is done after enabling SYSREF)

static skiq_rx_hdl_t _rx_hdl_map_other(rf_id_t *p_id);
static skiq_tx_hdl_t _tx_hdl_map_other(rf_id_t *p_id);

rfic_functions_t rfic_x4_funcs =
{
    .reset_and_init           = _reset_and_init_rfic,
    .display_version_info     = _display_version_info,
    .release                  = _release,
    .write_rx_freq            = _write_rx_freq,
    .write_tx_freq            = _write_tx_freq,
    .write_rx_spectrum_invert = _write_rx_spectrum_invert,
    .write_tx_spectrum_invert = _write_tx_spectrum_invert,
    .write_tx_attenuation     = _write_tx_attenuation,
    .read_tx_attenuation      = _read_tx_attenuation,
    .read_tx_attenuation_range = _read_tx_attenuation_range,
    .write_rx_gain_mode       = _write_rx_gain_mode,
    .read_rx_gain_mode        = _read_rx_gain_mode,
    .read_rx_gain_range       = _read_rx_gain_range,
    .write_rx_gain            = _write_rx_gain,
    .read_rx_gain             = _read_rx_gain,
    .read_adc_resolution      = _read_adc_resolution,
    .read_dac_resolution      = _read_dac_resolution,
    .read_warp_voltage        = _read_warp_voltage,
    .write_warp_voltage       = _write_warp_voltage,
    .read_warp_voltage_extended_range = NULL,
    .write_warp_voltage_extended_range = NULL,
    .enable_rx_chan           = _enable_rx_chan,
    .enable_tx_chan           = _enable_tx_chan,
    .config_tx_test_tone      = _config_tx_test_tone,
    .read_tx_test_tone_freq   = _read_tx_test_tone_freq,
    .write_tx_test_tone_freq  = _write_tx_test_tone_freq,
    .read_min_rx_sample_rate     = _read_min_rx_sample_rate,
    .read_max_rx_sample_rate     = _read_max_rx_sample_rate,
    .read_min_tx_sample_rate     = _read_min_tx_sample_rate,
    .read_max_tx_sample_rate     = _read_max_tx_sample_rate,
    .write_rx_sample_rate_and_bandwidth = _write_rx_sample_rate_and_bandwidth,
    .write_rx_sample_rate_and_bandwidth_multi = _write_rx_sample_rate_and_bandwidth_multi,
    .write_tx_sample_rate_and_bandwidth = _write_tx_sample_rate_and_bandwidth,
    .read_rx_sample_rate      = _read_rx_sample_rate,
    .read_tx_sample_rate      = _read_tx_sample_rate,
    .read_rx_chan_bandwidth   = _read_rx_chan_bandwidth,
    .read_tx_chan_bandwidth   = _read_tx_chan_bandwidth,
    .init_gpio                = _init_gpio,
    .write_gpio               = _write_gpio,
    .read_gpio                = _read_gpio,
    .read_rx_filter_overflow  = NULL,
    .read_tx_filter_overflow  = NULL,
    .read_fpga_rf_clock       = _read_fpga_rf_clock,
    .read_tx_quadcal_mode     = _read_tx_quadcal_mode,
    .write_tx_quadcal_mode    = _write_tx_quadcal_mode,
    .run_tx_quadcal           = _run_tx_quadcal,
    .read_rx_stream_hdl_conflict = _read_rx_stream_hdl_conflict,
    .is_chan_enable_xport_dependent = NULL,
    .read_control_output_rx_gain_config = _read_control_output_rx_gain_config,
    .write_control_output_config = _write_control_output_config,
    .read_control_output_config = _read_control_output_config,
    .init_from_file           = _init_from_file,
    .power_down_tx            = NULL,
    .enable_pin_gain_ctrl     = NULL,
    .read_hop_on_ts_gpio      = _read_hop_on_ts_gpio,
    .write_rx_freq_tune_mode  = _write_freq_tune_mode,
    .write_tx_freq_tune_mode  = _write_freq_tune_mode,
    .read_rx_freq_tune_mode   = _read_freq_tune_mode,
    .read_tx_freq_tune_mode   = _read_freq_tune_mode,
    .config_rx_hop_list       = _config_hop_list,
    .config_tx_hop_list       = _config_hop_list,
    .read_rx_hop_list         = _read_hop_list,
    .read_tx_hop_list         = _read_hop_list,
    .write_next_rx_hop        = _write_next_hop,
    .write_next_tx_hop        = _write_next_hop,
    .rx_hop                   = _hop,
    .tx_hop                   = _hop,
    .read_curr_rx_hop         = _read_curr_hop,
    .read_next_rx_hop         = _read_next_hop,
    .read_curr_tx_hop         = _read_curr_hop,
    .read_next_tx_hop         = _read_next_hop,
    .read_rx_cal_mode         = _read_rx_cal_mode,
    .write_rx_cal_mode        = _write_rx_cal_mode,
    .run_rx_cal               = _run_rx_cal,
    .read_rx_cal_mask         = _read_rx_cal_mask,
    .write_rx_cal_mask        = _write_rx_cal_mask,
    .read_rx_cal_types_avail  = _read_rx_cal_types_avail,
    .read_rx_enable_mode      = _read_tx_rx_enable_mode,
    .read_tx_enable_mode      = _read_tx_rx_enable_mode,
    .write_rx_enable_mode     = _write_tx_rx_enable_mode,
    .write_tx_enable_mode     = _write_tx_rx_enable_mode,
    .read_temp                = _read_temp,
    .read_auxadc              = NULL,
    .swap_rx_port_config      = NULL,
    .swap_tx_port_config      = NULL,
    .read_rf_capabilities     = _read_rf_capabilities,
    .write_ext_clock_select   = NULL,
    .rx_hdl_map_other      = _rx_hdl_map_other,
    .tx_hdl_map_other      = _tx_hdl_map_other,
    .read_rx_analog_filter_bandwidth    = NULL,
    .read_tx_analog_filter_bandwidth    = NULL,
    .write_rx_analog_filter_bandwidth   = NULL,
    .write_tx_analog_filter_bandwidth   = NULL,

    /****** RFIC Rx FIR function pointers ******/
    .read_rx_fir_config       = NULL,
    .read_rx_fir_coeffs       = NULL,
    .write_rx_fir_coeffs      = NULL,
    .read_rx_fir_gain         = NULL,
    .write_rx_fir_gain        = NULL,

    /****** RFIC Tx FIR function pointers ******/
    .read_tx_fir_config       = NULL,
    .read_tx_fir_coeffs       = NULL,
    .write_tx_fir_coeffs      = NULL,
    .read_tx_fir_gain         = NULL,
    .write_tx_fir_gain        = NULL,
};

/**************************************************************************************************/
/* RX_IQRATE simplifies queries to p_profile->p_rxProfile->rxOutputRate_kHz
 */
static inline uint32_t
RX_IQRATE( const ad9379_profile_t *p_profile )
{ return p_profile->p_rxProfile->rxOutputRate_kHz; }

static inline uint32_t
RX_IQRATE_HZ( const ad9379_profile_t *p_profile )
{ return RX_IQRATE(p_profile) * RATE_KHZ_TO_HZ; }

static inline uint32_t
DEV_CLK_KHZ( const ad9379_profile_t *p_profile )
{ return p_profile->p_clocks->deviceClock_kHz; }

/**************************************************************************************************/
/* RX_BW_HZ simplifies queries to p_profile->p_rxProfile->rfBandwidth_Hz
 */
static inline uint32_t
RX_BW_HZ( const ad9379_profile_t *p_profile )
{ return p_profile->p_rxProfile->rfBandwidth_Hz; }

/**************************************************************************************************/
/* ORX_IQRATE simplifies queries to p_profile->p_orxProfile->orxOutputRate_kHz
 */
static inline uint32_t
ORX_IQRATE( const ad9379_profile_t *p_profile )
{ return p_profile->p_orxProfile->orxOutputRate_kHz; }

static inline uint32_t
ORX_IQRATE_HZ( const ad9379_profile_t *p_profile )
{ return ORX_IQRATE(p_profile) * RATE_KHZ_TO_HZ; }


/**************************************************************************************************/
/* ORX_BW_HZ simplifies queries to p_profile->p_orxProfile->rfBandwidth_Hz
 */
static inline uint32_t
ORX_BW_HZ( const ad9379_profile_t *p_profile )
{ return p_profile->p_orxProfile->rfBandwidth_Hz; }


static inline bool IS_ORX_HDL(uint32_t _hdl)
{
    return(( _hdl == skiq_rx_hdl_C1 ) || ( _hdl == skiq_rx_hdl_D1 ));
}

static inline uint32_t
IQRATE_HZ( rf_id_t *p_id, const ad9379_profile_t *p_profile )
{ return (IS_ORX_HDL(p_id->hdl)) ? ORX_IQRATE_HZ(p_profile) : RX_IQRATE_HZ(p_profile); }

static inline uint32_t
BW_HZ( rf_id_t *p_id, const ad9379_profile_t *p_profile )
{ return (IS_ORX_HDL(p_id->hdl)) ? ORX_BW_HZ(p_profile) : RX_BW_HZ(p_profile); }


/**************************************************************************************************/
/* TX_IQRATE simplifies queries to p_profile->p_txProfile->iqRate_kHz
 */
static inline uint32_t
TX_IQRATE( const ad9379_profile_t *p_profile )
{ return p_profile->p_txProfile->txInputRate_kHz; }

static inline uint32_t
TX_IQRATE_HZ( const ad9379_profile_t *p_profile )
{ return TX_IQRATE(p_profile) * RATE_KHZ_TO_HZ; }

/**************************************************************************************************/
/* TX_BW_HZ simplifies queries to p_profile->p_txProfile->rfBandwidth_Hz
 */
static inline uint32_t
TX_BW_HZ( const ad9379_profile_t *p_profile )
{ return p_profile->p_txProfile->rfBandwidth_Hz; }

void _init_min_max_sample_rate( uint8_t card )
{
    uint8_t i=0;
    uint64_t fpga_min_freq=0;
    uint64_t fpga_max_freq=0;
    uint32_t actual_sample_rate=0;
    ad9528pll1Settings_t pll1;
    ad9528pll2Settings_t pll2;
    ad9528outputSettings_t output;
    ad9528sysrefSettings_t sysref;
    clk_div_t fpga_div = CLK_DIV_INVALID;
    uint32_t dev_clock = 0;
    clk_div_t dev_clock_div = CLK_DIV_INVALID;
    uint32_t rffc_freq = 0;
    uint64_t fpga_gbt_clock = 0;

    /* Initialize pll1, pll2, output, and sysref before using them */
    memset( &pll1, 0, sizeof( pll1 ) );
    memset( &pll2, 0, sizeof( pll2 ) );
    memset( &output, 0, sizeof( output ) );
    memset( &sysref, 0, sizeof( sysref ) );

    fpga_jesd_qpll_freq_range( card, &fpga_min_freq, &fpga_max_freq );

    if ( _p_min_rx_profile == NULL && _p_min_orx_profile == NULL && _p_min_tx_profile == NULL &&
         _p_max_rx_profile == NULL && _p_max_orx_profile == NULL && _p_max_tx_profile == NULL &&
         _p_max_dual_lane_orx_profile == NULL )
    {
        // let's find the best profile match
        for ( i = 0; i < AD9379_NR_PROFILES; i++ )
        {
            debug_print("Profile #%u:\n", i);
            debug_print("  RX1/RX2: sample rate %u ksps, RF bandwidth %u Hz\n",
                        RX_IQRATE(&ad9379_profiles[i]), RX_BW_HZ(&ad9379_profiles[i]));
            debug_print("      ORX: sample rate %u ksps, RF bandwidth %u Hz\n",
                        ORX_IQRATE(&ad9379_profiles[i]), ORX_BW_HZ(&ad9379_profiles[i]));

            // see what the clock settings are
            if( calc_ad9528_for_sample_rate( ad9379_profiles[i].p_rxProfile->rxOutputRate_kHz * RATE_KHZ_TO_HZ,
                                             &actual_sample_rate,
                                             &pll1,
                                             &pll2,
                                             &output,
                                             &sysref,
                                             &fpga_div,
                                             &dev_clock,
                                             &dev_clock_div,
                                             &rffc_freq,
                                             fpga_min_freq,
                                             fpga_max_freq,
                                             &fpga_gbt_clock,
                                             _x4_clk_sel,
                                             skiq_x4) == 0 )
            {
                /**********************************************************************************/
                /* Find the minimum RX1/RX2 profile */
                if( _p_min_rx_profile == NULL )
                {
                    _p_min_rx_profile = &ad9379_profiles[i];
                }
                else if ( RX_IQRATE(&ad9379_profiles[i]) < RX_IQRATE(_p_min_rx_profile) )
                {
                    _p_min_rx_profile = &ad9379_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if ( ( RX_IQRATE(&ad9379_profiles[i]) == RX_IQRATE(_p_min_rx_profile) ) &&
                          ( RX_BW_HZ(&ad9379_profiles[i]) < RX_BW_HZ(_p_min_rx_profile) ) )
                {
                    _p_min_rx_profile = &ad9379_profiles[i];
                }

                /**********************************************************************************/
                /* Find the maximum RX1/RX2 profile */
                if( _p_max_rx_profile == NULL )
                {
                    _p_max_rx_profile = &ad9379_profiles[i];
                    _max_rx_profile_index = i;
                }
                else if( RX_IQRATE(&ad9379_profiles[i]) > RX_IQRATE(_p_max_rx_profile) )
                {
                    _p_max_rx_profile = &ad9379_profiles[i];
                    _max_rx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( RX_IQRATE(&ad9379_profiles[i]) == RX_IQRATE(_p_max_rx_profile) ) &&
                         ( RX_BW_HZ(&ad9379_profiles[i]) > RX_BW_HZ(_p_max_rx_profile) ) )
                {
                    _p_max_rx_profile = &ad9379_profiles[i];
                    _max_rx_profile_index = i;
                }

                /**********************************************************************************/
                /* Find the minimum TX1/TX2 profile */
                if( _p_min_tx_profile == NULL )
                {
                    _p_min_tx_profile = &ad9379_profiles[i];
                }
                else if ( TX_IQRATE(&ad9379_profiles[i]) == 0 )
                {
                    /*  when sample rate is 0 this profile does not support TX so skip it */
                }
                else if ( TX_IQRATE(&ad9379_profiles[i]) < TX_IQRATE(_p_min_tx_profile) )
                {
                    _p_min_tx_profile = &ad9379_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if ( ( TX_IQRATE(&ad9379_profiles[i]) == TX_IQRATE(_p_min_tx_profile) ) &&
                          ( TX_BW_HZ(&ad9379_profiles[i]) < TX_BW_HZ(_p_min_tx_profile) ) )
                {
                    _p_min_tx_profile = &ad9379_profiles[i];
                }

                /**********************************************************************************/
                /* Find the maximum TX1/TX2 profile */
                if( _p_max_tx_profile == NULL )
                {
                    _p_max_tx_profile = &ad9379_profiles[i];
                    _max_tx_profile_index = i;
                }
                else if( TX_IQRATE(&ad9379_profiles[i]) > TX_IQRATE(_p_max_tx_profile) )
                {
                    _p_max_tx_profile = &ad9379_profiles[i];
                    _max_tx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( TX_IQRATE(&ad9379_profiles[i]) == TX_IQRATE(_p_max_tx_profile) ) &&
                         ( TX_BW_HZ(&ad9379_profiles[i]) > TX_BW_HZ(_p_max_tx_profile) ) )
                {
                    _p_max_tx_profile = &ad9379_profiles[i];
                    _max_tx_profile_index = i;
                }

                /**********************************************************************************/
                /* Find the minimum ORX profile */
                if( _p_min_orx_profile == NULL )
                {
                    _p_min_orx_profile = &ad9379_profiles[i];
                }
                else if ( ORX_IQRATE(&ad9379_profiles[i]) < ORX_IQRATE(_p_min_orx_profile) )
                {
                    _p_min_orx_profile = &ad9379_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if ( ( ORX_IQRATE(&ad9379_profiles[i]) == ORX_IQRATE(_p_min_orx_profile) ) &&
                          ( ORX_BW_HZ(&ad9379_profiles[i]) < ORX_BW_HZ(_p_min_orx_profile) ) )
                {
                    _p_min_orx_profile = &ad9379_profiles[i];
                }

                /**********************************************************************************/
                /* Find the maximum ORX profile */
                if( _p_max_orx_profile == NULL )
                {
                    _p_max_orx_profile = &ad9379_profiles[i];
                    _max_orx_profile_index = i;
                }
                else if ( ( ORX_IQRATE(&ad9379_profiles[i]) <= ORX_DUAL_LANE_RATE_THRESHOLD_KHZ ) &&
                          ( ORX_IQRATE(&ad9379_profiles[i]) > ORX_IQRATE(_p_max_orx_profile) ) )
                {
                    _p_max_orx_profile = &ad9379_profiles[i];
                    _max_orx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( ORX_IQRATE(&ad9379_profiles[i]) <= ORX_DUAL_LANE_RATE_THRESHOLD_KHZ ) &&
                         ( ORX_IQRATE(&ad9379_profiles[i]) == ORX_IQRATE(_p_max_orx_profile) ) &&
                         ( ORX_BW_HZ(&ad9379_profiles[i]) > ORX_BW_HZ(_p_max_orx_profile) ) )
                {
                    _p_max_orx_profile = &ad9379_profiles[i];
                    _max_orx_profile_index = i;
                }

                /**********************************************************************************/
                /* Find the maximum dual-lane ORX profile */
                if( _p_max_dual_lane_orx_profile == NULL )
                {
                    _p_max_dual_lane_orx_profile = &ad9379_profiles[i];
                }
                else if ( ORX_IQRATE(&ad9379_profiles[i]) > ORX_IQRATE(_p_max_dual_lane_orx_profile) )
                {
                    _p_max_dual_lane_orx_profile = &ad9379_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( ORX_IQRATE(&ad9379_profiles[i]) == ORX_IQRATE(_p_max_dual_lane_orx_profile) ) &&
                         ( ORX_BW_HZ(&ad9379_profiles[i]) > ORX_BW_HZ(_p_max_dual_lane_orx_profile) ) )
                {
                    _p_max_dual_lane_orx_profile = &ad9379_profiles[i];
                }
            }
        }
        debug_print("Profile #%" PRIu32 " is maximum RX profile\n", _max_rx_profile_index);
        debug_print("Profile #%" PRIu32 " is maximum TX profile\n", _max_tx_profile_index);
        debug_print("Profile #%" PRIu32 " is maximum ORX profile\n", _max_orx_profile_index);
    }
}


bool _find_freq_table_index( uint64_t lo_freq, uint8_t *p_index )
{
    uint8_t i=0;
    bool bFound = false;

    for( i=0; (i<MAX_FREQ_TABLE_ENTRIES) && (bFound==false); i++ )
    {
        if( (lo_freq >= _freqTable[i].startFreq) &&
            (lo_freq <= _freqTable[i].stopFreq) )
        {
            bFound = true;
            *p_index = i;
        }
    }

    if( bFound == false )
    {
        skiq_error("Unable to locate frequency table offset for %" PRIu64 "\n", lo_freq);
        *p_index = MAX_FREQ_TABLE_ENTRIES;
    }

    return (bFound);
}

void log_adihal_msg( adiLogLevel_t logLevel, const char *format, ... )
{
    char str[ADI_MAX_LOG_MSG_SIZE];
    int num_bytes=0;
    va_list args;
    int32_t priority=0;

    if( (logLevel & ADIHAL_LOG_ERR) == ADIHAL_LOG_ERR )
    {
        priority = SKIQ_LOG_ERROR;
        num_bytes = snprintf( str, ADI_MAX_LOG_MSG_SIZE, "[ADI-ERROR]: " );
    }
    else if( (logLevel & ADIHAL_LOG_WARN) == ADIHAL_LOG_WARN )
    {
        priority = SKIQ_LOG_WARNING;
        num_bytes = snprintf( str, ADI_MAX_LOG_MSG_SIZE, "[ADI-WARN]: " );
    }
    else if( (logLevel & ADIHAL_LOG_MSG) == ADIHAL_LOG_MSG )
    {
#if (defined DEBUG_RFIC_X4)
        priority = SKIQ_LOG_INFO;
        num_bytes = snprintf( str, ADI_MAX_LOG_MSG_SIZE, "[ADI-MSG]: " );
#endif
    }
    else
    {
#if (defined DEBUG_RFIC_X4)
        priority = SKIQ_LOG_DEBUG;
        num_bytes = snprintf( str, ADI_MAX_LOG_MSG_SIZE, "[ADI-VERBOSE]: " );
#endif
    }

    if( (logLevel & ADIHAL_LOG_SPI) == ADIHAL_LOG_SPI )
    {
#if (defined DEBUG_RFIC_X4)
        priority = SKIQ_LOG_DEBUG;
        num_bytes = snprintf( &(str[num_bytes]), ADI_MAX_LOG_MSG_SIZE-num_bytes, "{ADISPI}: ");
#endif
    }

    if( num_bytes > 0 )
    {
        va_start( args, format );
        vsnprintf( &(str[num_bytes]), ADI_MAX_LOG_MSG_SIZE-num_bytes, format, args );
        va_end( args );
        _skiq_log( priority, "%s", str );
    }
}


/**************************************************************************************************/
taliseDevice_t * rf_id_to_rficDevice( rf_id_t *p_id )
{
    taliseDevice_t *p_rfic = NULL;

    if ( rx_hdl_to_chip_id[p_id->hdl] == RF_CHIP_A )
    {
        p_rfic = &(_rficDeviceA[p_id->card]);
    }
    else if ( rx_hdl_to_chip_id[p_id->hdl] == RF_CHIP_B )
    {
        p_rfic = &(_rficDeviceB[p_id->card]);
    }
    else
    {
        skiq_error("Handle %u not currently supported (card=%u)\n", p_id->hdl, p_id->card);
    }

    return p_rfic;
}


#if (defined DEBUG_PRINT_ENABLED)
/**************************************************************************************************/
static const char *
rxorx_chan_cstr( taliseRxORxChannels_t rx_chans )
{
    const char *p_chan_str =
        ( rx_chans == TAL_RXOFF_EN ) ? "RXOFF" :
        ( rx_chans == TAL_RX1_EN ) ? "RX1" :
        ( rx_chans == TAL_RX2_EN ) ? "RX2" :
        ( rx_chans == TAL_RX1RX2_EN ) ? "RX1 RX2" :
        ( rx_chans == TAL_ORX1_EN ) ? "ORX1" :
        ( rx_chans == TAL_ORX2_EN ) ? "ORX2" :
        ( rx_chans == TAL_ORX1ORX2_EN ) ? "ORX1 ORX2" :
        "unknown";

    return p_chan_str;
}


/**************************************************************************************************/
static const char *
tx_chan_cstr( taliseTxChannels_t tx_chans )
{
    const char *p_chan_str =
        ( tx_chans == TAL_TX1TX2 ) ? "TX1 TX2" :
        ( tx_chans == TAL_TX1 ) ? "TX1" :
        ( tx_chans == TAL_TX2 ) ? "TX2" :
        "TXOFF";

    return p_chan_str;
}


/**************************************************************************************************/
static const char *
gain_mode_cstr( taliseGainMode_t mode )
{
    const char *p_mode_str =
        ( mode == TAL_MGC ) ? "MANUAL" :
        ( mode == TAL_AGCFAST ) ? "AGC-FAST" :
        ( mode == TAL_AGCSLOW ) ? "AGC-SLOW" :
        ( mode == TAL_HYBRID ) ? "HYBRID" :
        "unknown";

    return p_mode_str;
}


/**************************************************************************************************/
// print contents of chip config
void _display_config( tal_chip_config_t *p_chip_config )
{
    char id = ( 0 != isprint(p_chip_config->id) ) ? p_chip_config->id : '?';

    debug_print("\t[chip %c] GPIO: OE=0x%08" PRIx32 " SrcCtrl=0x%08" PRIx32 " PinLevel=0x%08" PRIx32 "\n", id,
                 p_chip_config->gpio_out_ena,
                 p_chip_config->gpio_src_ctrl,
                 p_chip_config->gpio_pin_level);
    debug_print("\t[chip %c] 3v3 GPIO: OE=0x%04" PRIx16 " SrcCtrl=0x%04" PRIx16 " PinLevel=0x%04" PRIx16 "\n", id,
                p_chip_config->gpio_3v3_out_ena,
                p_chip_config->gpio_3v3_src_ctrl,
                p_chip_config->gpio_3v3_pin_level);
    debug_print("\t[chip %c] Monitor index=%" PRIu8 " mask=0x%02" PRIx8 "\n", id,
                p_chip_config->mon_index,
                p_chip_config->mon_mask);
    debug_print("\t[chip %c] RX Channels: %s\n", id, rxorx_chan_cstr( p_chip_config->rx_chans ));
    debug_print("\t[chip %c] TX Channels: %s\n", id, tx_chan_cstr( p_chip_config->tx_chans ));
    debug_print("\t[chip %c] PLL freq=%" PRIu64 " loopbw=%" PRIu16 " stability=%" PRIu8 "\n", id,
                p_chip_config->rf_pll_freq,
                p_chip_config->rf_pll_loopbw,
                p_chip_config->rf_pll_stability);
    debug_print("\t[chip %c] Gain: mode=%s rx1=(%" PRIu8 ",%s) rx2=(%" PRIu8 ",%s) orx1=(%" PRIu8 ",%s)\n", id,
                gain_mode_cstr(p_chip_config->tal_gain_mode),
                p_chip_config->rx1.tal_gain, p_chip_config->rx1.gain_valid ? "valid" : "invalid",
                p_chip_config->rx2.tal_gain, p_chip_config->rx2.gain_valid ? "valid" : "invalid",
                p_chip_config->orx1.tal_gain, p_chip_config->orx1.gain_valid ? "valid" : "invalid");
    debug_print("\t[chip %c] Attenuation: tx1=%" PRIu16 " tx2=%" PRIu16 "\n", id,
                p_chip_config->tx1.tal_atten_mdB,
                p_chip_config->tx2.tal_atten_mdB);
    debug_print("\t[chip %c] Pin Ctrl Mode (Tx + Rx):  %s\n", id,
                (p_chip_config->tal_pin_control_mask == 0) ? "Sofware controlled" : "Pin Controlled" );
}
#else
#define _display_config(...)            do { } while (0)
#endif  /* DEBUG_PRINT_ENABLED */


// caches all relevant settings to be restored upon reinit
int32_t _cache_config( uint8_t card,
                       tal_chip_config_t *p_chipA_config,
                       tal_chip_config_t *p_chipB_config )
{
    int32_t status=0;
    uint8_t i=0;
    rf_id_t tmp_rf_id = {0};
    tal_chip_config_t *p_chip_config;
    taliseDevice_t *p_rfic = NULL;

    tmp_rf_id.card = card;

    /* use the _tx_config[card] array instead of calling TALISE_getTxAttenuation(), which can be
     * inaccurate depending on which channels are enabled:

     From the API:

     """The Tx data path must be powered up for the current attenuation value to be valid.  If the
     Tx data path is powered down or the radio is off, the last Tx attenuation setting when the Tx
     output was previously active will be read back"""

    */
    p_chipA_config->tx1 = _tx_config[card][skiq_tx_hdl_A1];
    p_chipA_config->tx2 = _tx_config[card][skiq_tx_hdl_A2];
    p_chipB_config->tx1 = _tx_config[card][skiq_tx_hdl_B1];
    p_chipB_config->tx2 = _tx_config[card][skiq_tx_hdl_B2];

    /* use the _rx_config[card] array instead of calling TALISE_getRxGain(), which can be inaccurate
     * depending on which channels are enabled:

     From the API:

     """This function may be called any time after device initialization and the ARM processor has
        moved to Radio ON state at least once.  However, gain indices are tracked only after the
        device goes into a Receiver mode."""

     Also, remember that RX1 and RX2 are swapped to A2 and A1 (or B2 and B1) respectively

    */
    p_chipA_config->rx2  = _rx_config[card][skiq_rx_hdl_A1];
    p_chipA_config->rx1  = _rx_config[card][skiq_rx_hdl_A2];
    p_chipA_config->orx1 = _rx_config[card][skiq_rx_hdl_C1];
    p_chipB_config->rx2  = _rx_config[card][skiq_rx_hdl_B1];
    p_chipB_config->rx1  = _rx_config[card][skiq_rx_hdl_B2];
    p_chipB_config->orx1 = _rx_config[card][skiq_rx_hdl_D1];

    // loop through both chips
    for( i=0; (i<RF_CHIP_MAX) && (status==0); i++ )
    {
        // initialize based on chip ID
        if( i==0 )
        {
            tmp_rf_id.hdl = skiq_rx_hdl_A1;
            p_chip_config = p_chipA_config;
            p_chip_config->id = 'A';
        }
        else if( i==1 )
        {
            tmp_rf_id.hdl = skiq_rx_hdl_B1;
            p_chip_config = p_chipB_config;
            p_chip_config->id = 'B';
        }
        // get our talise instance
        p_rfic = rf_id_to_rficDevice( &tmp_rf_id );

        ////////////////////////
        // GPIO
        if( TALISE_getGpioOe( p_rfic, &(p_chip_config->gpio_out_ena) ) != 0 )
        {
            skiq_error("Unable to read GPIO OE (card=%u, chip=%u)\n", card, i);
            status = -EIO;
        }
        if( status == 0 )
        {
            if( TALISE_getGpioSourceCtrl( p_rfic, &(p_chip_config->gpio_src_ctrl) ) != 0 )
            {
                skiq_error("Unable to read GPIO source ctrl (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            p_chip_config->gpio_pin_level= _gpio_pin_level[card][i];
        }
        ////////////////////////

        ////////////////////////
        // 3.3V GPIO
        if( status == 0 )
        {
            if( TALISE_getGpio3v3Oe( p_rfic, &(p_chip_config->gpio_3v3_out_ena) ) != 0 )
            {
                skiq_error("Unable to read 3.3V GPIO OE (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            if( TALISE_getGpio3v3SourceCtrl( p_rfic, &(p_chip_config->gpio_3v3_src_ctrl) ) != 0 )
            {
                skiq_error("Unable to read 3.3V GPIO source ctrl (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            p_chip_config->gpio_3v3_pin_level= _gpio_3v3_pin_level[card][i];
        }
        ////////////////////////

        ////////////////////////
        // monitor out
        if( status == 0 )
        {
            if( TALISE_getGpioMonitorOut( p_rfic,
                                          &(p_chip_config->mon_index),
                                          &(p_chip_config->mon_mask) ) != 0 )
            {
                skiq_error("Unable to read monitor output settings (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // Rx/Tx Channels
        if( status == 0 )
        {
            if( _getRxTxEnable( &tmp_rf_id, p_rfic,
                                      &(p_chip_config->rx_chans),
                                      &(p_chip_config->tx_chans) ) != 0 )
            {
                skiq_error("Unable to read channel enable configuration (card=%u, chip=%u)\n",
                           card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // Rx/Tx Channel enable mode
        if ( status == 0)
        {
            status = TALISE_getRadioCtlPinMode( p_rfic, &(p_chip_config->tal_pin_control_mask), &(p_chip_config->tal_pin_control_mask_orx) );
            if ( status != TALACT_NO_ACTION )
            {
                status = -EBADMSG;
                skiq_error("Failed to get radio control pin mode with status %" PRIi32 " on card %"
                           PRIu8 " and chip %" PRIu8 "\n", status, card, i);
            }
        }
        ////////////////////////

        ////////////////////////
        // PLL settings
        if( status == 0 )
        {
            if( TALISE_getRfPllFrequency( p_rfic, TAL_RF_PLL, &(p_chip_config->rf_pll_freq) ) != 0 )
            {
                skiq_error("Unable to read RF PLL frequency (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            if( TALISE_getRfPllLoopFilter( p_rfic,
                                           &(p_chip_config->rf_pll_loopbw),
                                           &(p_chip_config->rf_pll_stability) ) != 0 )
            {
                skiq_error("Unable to read 3.3V RF PLL loop filter (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // RX configuration
        // gain mode
        if( status == 0 )
        {
            p_chip_config->tal_gain_mode = p_rfic->devStateInfo.gainMode;
        }
        ////////////////////////

        ///////////////////////
        // FHM
        if( status == 0 )
        {
            if( TALISE_getFhmMode( p_rfic, &(p_chip_config->fhm.mode) ) != 0 )
            {
                skiq_error("Unable to read frequency hopping mode (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
            else
            {
                // save additional hopping info if enabled
                if( p_chip_config->fhm.mode.fhmEnable == 1 )
                {
                    if( TALISE_getFhmConfig( p_rfic, &(p_chip_config->fhm.config) ) != 0 )
                    {
                        skiq_error("Unable to read frequency hopping configuration (card=%u, chip=%u)\n",
                                   card, i);
                        status = -EIO;
                    }
                }
            }
        }
        ///////////////////////

        if( status == 0 )
        {
            _display_config( p_chip_config );
        }
    }

    return (status);
}


/**************************************************************************************************/
int32_t restore_tx_test_tone( rf_id_t *p_id )
{
    int32_t status = 0;
    rf_id_t rf_id_tx1;
    rf_id_t rf_id_tx2;
    tx_test_tone_t *p_tone;

    // TODO: we should support re-initializing the RFIC independently eventually...but for now,
    // it's fixed
    memcpy(&rf_id_tx1, p_id, sizeof(rf_id_t));
    memcpy(&rf_id_tx2, p_id, sizeof(rf_id_t));

    if ( ( p_id->hdl == skiq_tx_hdl_A1 ) || ( p_id->hdl == skiq_tx_hdl_A2 ) )
    {
        rf_id_tx1.hdl = skiq_tx_hdl_A1;
        rf_id_tx2.hdl = skiq_tx_hdl_A2;
        p_tone = &(_tx_test_toneA[p_id->card]);
    }
    else if ( ( p_id->hdl == skiq_tx_hdl_B1 ) || ( p_id->hdl == skiq_tx_hdl_B2 ) )
    {
        rf_id_tx1.hdl = skiq_tx_hdl_B1;
        rf_id_tx2.hdl = skiq_tx_hdl_B2;
        p_tone = &(_tx_test_toneB[p_id->card]);
    }
    else
    {
        status = -EINVAL;
    }

    // update the TX test tone freq config
    if (( status == 0 ) && (p_tone->enable))
    {
        status = _write_tx_test_tone_freq( &rf_id_tx1, p_tone->tx1_freq_offset );
        if ( status == 0 )
        {
            status = _write_tx_test_tone_freq( &rf_id_tx2, p_tone->tx2_freq_offset );
        }
    }

    return status;
}


// restores all previously cached settings
int32_t _restore_config( uint8_t card,
                         tal_chip_config_t *p_chipA_config,
                         tal_chip_config_t *p_chipB_config )
{
    int32_t status=0;
    uint8_t i=0;
    rf_id_t tmp_rf_id;
    tal_chip_config_t *p_chip_config;
    taliseDevice_t *p_rfic = NULL;
    uint8_t lockStatus=0;
    uint64_t cal_freq;
    taliseTxChannels_t calTxChan;
    taliseRxORxChannels_t calRxChan;
    uint8_t j=0;

    tmp_rf_id.card = card;

    // clear all previous cal since the chip has been reset
    for( j=0; j<skiq_rx_hdl_end; j++ )
    {
        _rx_cal_state[card][j].freqTableIndex = INVALID_CAL_INDEX;
        _rx_cal_state[card][j].lastCalFreq = INVALID_CAL_FREQ;
    }
    for( j=0; j<skiq_tx_hdl_end; j++ )
    {
        _tx_cal_state[card][j].freqTableIndex = INVALID_CAL_INDEX;
        _tx_cal_state[card][j].lastCalFreq = INVALID_CAL_FREQ;
    }

    // loop through both chips
    for( i=0; (i<RF_CHIP_MAX) && (status==0); i++ )
    {
        // initialize based on chip ID
        if( i==0 )
        {
            tmp_rf_id.hdl = skiq_rx_hdl_A1;
            p_chip_config = p_chipA_config;
        }
        else if( i==1 )
        {
            tmp_rf_id.hdl = skiq_rx_hdl_B1;
            p_chip_config = p_chipB_config;
        }
        // get our talise instance
        p_rfic = rf_id_to_rficDevice( &tmp_rf_id );

        _display_config( p_chip_config );

        if( TALISE_radioOff( p_rfic ) != 0 )
        {
            skiq_error("Unable to turn off the radio (card=%u, chip=%u)\n", card, i);
            status = -EIO;
        }

        ////////////////////////
        // GPIO
        if( status == 0 )
        {
            if( TALISE_setGpioOe( p_rfic, p_chip_config->gpio_out_ena, p_chip_config->gpio_out_ena ) != 0 )
            {
                skiq_error("Unable to configure GPIO OE (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            if( TALISE_setGpioSourceCtrl( p_rfic, p_chip_config->gpio_src_ctrl ) != 0 )
            {
                skiq_error("Unable to configure GPIO source ctrl (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            if( TALISE_setGpioPinLevel( p_rfic, p_chip_config->gpio_pin_level ) != 0 )
            {
                skiq_error("Unable to GPIO pin level (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // 3.3V GPIO
        if( status == 0 )
        {
            if( TALISE_setGpio3v3Oe( p_rfic,
                                     p_chip_config->gpio_3v3_out_ena,
                                     p_chip_config->gpio_3v3_out_ena ) != 0 )
            {
                skiq_error("Unable to set 3.3V GPIO (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            if( TALISE_setGpio3v3SourceCtrl( p_rfic, p_chip_config->gpio_3v3_src_ctrl ) != 0 )
            {
                skiq_error("Unable to set 3.3V GPIO source ctrl (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        if( status == 0 )
        {
            if( TALISE_setGpio3v3PinLevel( p_rfic, p_chip_config->gpio_3v3_pin_level ) != 0 )
            {
                skiq_error("Unable to set 3.3V GPIO pin level (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // monitor out
        if( status == 0 )
        {
            if( TALISE_setGpioMonitorOut( p_rfic,
                                          p_chip_config->mon_index,
                                          p_chip_config->mon_mask ) != 0 )
            {
                skiq_error("Unable to set monitor output (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // PLL settings
        if( status == 0 )
        {
            if( TALISE_setRfPllFrequency( p_rfic, TAL_RF_PLL, p_chip_config->rf_pll_freq ) != 0 )
            {
                skiq_warning("Unable to set RF PLL to %" PRIu64 " Hz using default of %" PRIu64
                             " Hz (card=%u, chip=%u)\n", p_chip_config->rf_pll_freq,
                             (uint64_t)(DEFAULT_PLL_FREQ), card, i);
                // update the PLL freq to default
                p_chip_config->rf_pll_freq = DEFAULT_PLL_FREQ;
            }
            else
            {
                if( TALISE_setRfPllLoopFilter( p_rfic,
                                               p_chip_config->rf_pll_loopbw,
                                               p_chip_config->rf_pll_stability ) != 0 )
                {
                    skiq_error("Unable to set RF PLL loop filter (card=%u, chip=%u)\n", card, i);
                    status = -EIO;
                }
                else
                {
                    if( TALISE_getPllsLockStatus( p_rfic,
                                                  &lockStatus ) != 0 )
                    {
                        skiq_error("Unable to read PLL lock status (card=%u, chip=%u)\n", card, i);
                        status = -EIO;
                    }
                    else
                    {
                        if( ((lockStatus & TAL_RX_ONLY_PLL_LOCK) != TAL_RX_ONLY_PLL_LOCK) )
                        {
                            skiq_error("PLL lock status failed (0x%x) (card=%u, chip=%u)\n",
                                       lockStatus, card, i );
                            status = -EIO;
                        }
                    }
                }
            }
        }
        ////////////////////////

        ////////////////////////
        // Gain configuration
        if( status == 0 )
        {
            if( TALISE_setRxGainControlMode( p_rfic, p_chip_config->tal_gain_mode ) != 0 )
            {
                skiq_error("Unable to set gain control (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
            else
            {
                if( p_chip_config->tal_gain_mode == TAL_MGC )
                {
                    // Rx1
                    if( status == 0 )
                    {
                        if( p_chip_config->rx1.gain_valid == true )
                        {
                            debug_print("Restoring RX manual gain to RX1 as %" PRIu8 " on card %"
                                        PRIu8 "\n", p_chip_config->rx1.tal_gain, card);
                            if( TALISE_setRxManualGain( p_rfic, TAL_RX1, p_chip_config->rx1.tal_gain ) != 0 )
                            {
                                skiq_error("Unable to set Rx1 gain (card=%u, chip=%u)\n", card, i);
                                status = -EIO;
                            }
                        }
                    }
                    // Rx2
                    if( status == 0 )
                    {
                        if( p_chip_config->rx2.gain_valid == true )
                        {
                            debug_print("Restoring RX manual gain to RX2 as %" PRIu8 " on card %"
                                        PRIu8 "\n", p_chip_config->rx2.tal_gain, card);
                            if( TALISE_setRxManualGain( p_rfic, TAL_RX2, p_chip_config->rx2.tal_gain ) != 0 )
                            {
                                skiq_error("Unable to set Rx2 gain (card=%u, chip=%u)\n", card, i);
                                status = -EIO;
                            }
                        }
                    }
                    // ORx
                    if( status == 0 )
                    {
                        if( p_chip_config->orx1.gain_valid == true )
                        {
                            if( TALISE_setObsRxManualGain( p_rfic, TAL_ORX1, p_chip_config->orx1.tal_gain ) != 0 )
                            {
                                skiq_error("Unable to set ORx gain (card=%u, chip=%u)\n", card, i);
                                status = -EIO;
                            }
                        }
                    }
                }
            }
        }
        ////////////////////////


        ////////////////////////
        // TX settings
        // Tx1
        if( status == 0 )
        {
            if( TALISE_setTxAttenuation( p_rfic, TAL_TX1, p_chip_config->tx1.tal_atten_mdB ) != 0 )
            {
                skiq_error("Unable to set Tx1 attenuation (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        // Tx2
        if( status == 0 )
        {
            if( TALISE_setTxAttenuation( p_rfic, TAL_TX2, p_chip_config->tx2.tal_atten_mdB ) != 0 )
            {
                skiq_error("Unable to set Tx2 attenuation (card=%u, chip=%u)\n", card, i);
                status = -EIO;
            }
        }
        ////////////////////////

        ////////////////////////
        // Rx/Tx Channel enable mode
        if (status == 0)
        {
            status = TALISE_setRadioCtlPinMode( p_rfic, p_chip_config->tal_pin_control_mask, p_chip_config->tal_pin_control_mask_orx);
            if ( status != 0 )
            {
                skiq_error("Failed to set radio control pin mode with status %" PRIi32 " on card %"
                            PRIu8 "\n", status, card);
            }
        }
        ////////////////////////

        if( status == 0 )
        {
            // turn on the channels
            if( _setRxTxEnable(&tmp_rf_id,
                                p_rfic,
                                p_chip_config->rx_chans,
                                p_chip_config->tx_chans ) != 0 )
            {
                skiq_warning("Unable to enable channels (card=%u, chip=%u)\n", card, i);
            }
        }

        // Run calibration after clearing all previous calibration
        if( status == 0 )
        {
            bool transmit=false;
            // run TX cal if TX is enable, otherwise check and run receive
            if((p_chip_config->tx_chans != TAL_TXOFF) )
            {
                transmit = true;
            }

            // determine if we need to run calibration
            if ( ( ( p_chip_config->rx_chans != TAL_RXOFF_EN ) ||
                   ( p_chip_config->tx_chans != TAL_TXOFF) ) &&
                 _is_cal_required( &tmp_rf_id,
                                  transmit,
                                  p_chip_config->rf_pll_freq,
                                  &cal_freq,
                                  &calRxChan,
                                  &calTxChan ) == true )
            {
                status = _run_cal( &tmp_rf_id,
                                   p_rfic,
                                   cal_freq,
                                   calRxChan,
                                   calTxChan );
            }
            else
            {
                // make sure the radio is on
                TALISE_radioOn(p_rfic);
            }

            /* restore cached test tone */
            if ( status == 0 )
            {
                status = restore_tx_test_tone( &tmp_rf_id );
            }
        }

        ///////////////////////
        // FHM
        if( status == 0 )
        {
            if( p_chip_config->fhm.mode.fhmEnable == 1 )
            {
                // the radio needs to be off to update the hopping config
                if( TALISE_radioOff(p_rfic) == 0 )
                {
                    if( TALISE_setFhmConfig( p_rfic, &(p_chip_config->fhm.config) ) != 0 )
                    {
                        skiq_error("Unable to write frequency hopping configuration (card=%u, chip=%u)\n",
                                   card, i);
                        status = -EIO;
                    }
                    else
                    {
                        if( TALISE_radioOn(p_rfic) == 0 )
                        {
                            if( TALISE_setFhmMode( p_rfic, &(p_chip_config->fhm.mode) ) != 0 )
                            {
                                skiq_error("Unable to write frequency hopping mode (card=%u, chip=%u)\n", card, i);
                                status = -EIO;
                            }
                        }
                        else
                        {
                            skiq_error("Unable to turn radio on (card=%u, chip=%u)\n",
                                       card, i);
                            status = -EIO;
                        }
                    } // end FHM Config
                } // end radio off
                else
                {
                    skiq_error("Unable to turn radio off prior to applying hopping configuration (card=%u, chip=%u)\n",
                               card, i);
                }
            } // end hopping enable
        } // status ok
        ///////////////////////

    } // end of looping through chips

    return (status);
}


int32_t _perform_mcs( ad9528Device_t *p_ad9528, taliseDevice_t *p_rfic_a, taliseDevice_t *p_rfic_b )
{
    int32_t status=0;
    uint8_t i=0;
    uint8_t mcsStatusA = 0;
    uint8_t mcsStatusB = 0;

    // ensure SYSREF is in nshot / 8 pulse config and it's off
    if( (AD9528_requestSysref(p_ad9528, 0) != 0 ) ||
        (AD9528_updateSYSREFMode(p_ad9528, NSHOT, EIGHT_PULSES) != 0 ) )
    {
        skiq_error("Unable to update SYSREF\n");
        status = -ENODEV;
    }
    else
    {
        if( (TALISE_enableMultichipSync(p_rfic_a, 1, &mcsStatusA) != 0) ||
            (TALISE_enableMultichipSync(p_rfic_b, 1, &mcsStatusB) != 0) )
        {
            skiq_error("MCS failure\n" );
            status = -1;
        }
        else
        {
            // toggle SYSREF - doc says 2xs...we're setup for 8 pulse
            AD9528_requestSysref( p_ad9528, 1 );
            do
            {
                // pause a bit for SYSREF to occur
                hal_nanosleep( 1*MILLISEC );
                i++;
                // now perform MCS again...make sure the status is ok
                TALISE_enableMultichipSync(p_rfic_a, 0, &mcsStatusA);
                TALISE_enableMultichipSync(p_rfic_b, 0, &mcsStatusB);
                skiq_debug( "polling MCS for dev clock sync 0x%x 0x%x (i=%u)\n",
                           mcsStatusA, mcsStatusB, i );
            } while( (!(mcsStatusA & MCS_STATUS_DEV_CLK_DIV) || !(mcsStatusB & MCS_STATUS_DEV_CLK_DIV)) &&
                     (i<MAX_NUM_MCS_TRIES) ); // wait for Dev clock sync
            if( i >= MAX_NUM_MCS_TRIES )
            {
                status = -2;
                skiq_error("Unable to sync dev clocks\n" );
            }
            else
            {
                // SYSREF
                i=0;
                AD9528_requestSysref( p_ad9528, 1 );
                
                do
                {
                    // TODO: is this needed?
                    hal_nanosleep( 1*MILLISEC );
                    i++;
                    // now perform MCS again...make sure the status is ok
                    TALISE_enableMultichipSync(p_rfic_a, 0, &mcsStatusA);
                    TALISE_enableMultichipSync(p_rfic_b, 0, &mcsStatusB);
                    skiq_debug( "polling MCS for dig clock sync 0x%x 0x%x (i=%u)\n",
                               mcsStatusA, mcsStatusB, i );
                } while( (!(mcsStatusA & MCS_STATUS_DIG_CLK) || (!(mcsStatusB & MCS_STATUS_DIG_CLK))) &&
                         (i<MAX_NUM_MCS_TRIES) ); // wait for dig clock sync
                if( i >= MAX_NUM_MCS_TRIES )
                {
                    status = -3;
                    _skiq_log( SKIQ_LOG_ERROR, "Unable to sync dig clocks\n");
                }
            }
        }
    }

    return (status);
}

int32_t reset_jesd_x4( uint8_t card,
                       uint8_t rx_preEmphasis,
                       uint8_t rx_serialAmplitude,
                       uint8_t tx_diffctrl,
                       uint8_t tx_precursor,
                       jesd_sync_result_t *p_sync_result )
{
    int32_t status=0;
    ad9528Device_t *p_ad9528 = &(_clockDevice[card]);
    taliseDevice_t *p_rfic_a = &(_rficDeviceA[card]);
    taliseDevice_t *p_rfic_b = &(_rficDeviceB[card]);

    if( fpga_jesd_ctrl_tx_config(card, tx_diffctrl, tx_precursor) == 0 )
    {
        skiq_debug("Setup TX JESD parameters successfully (diff=%u, precurs=%u)\n",
                  tx_diffctrl, tx_precursor);
    }
    else
    {
        skiq_error("Unable to configure TX JESD parameters\n");
    }

    // we need to re-initialize serializers with the new preemphasis / amplit
    _taliseDevice[card][RF_CHIP_A]->jesd204Settings.serPreEmphasis = rx_preEmphasis;
    _taliseDevice[card][RF_CHIP_A]->jesd204Settings.serAmplitude = rx_serialAmplitude;
    if( (status=TALISE_setupSerializers(p_rfic_a, _taliseDevice[card][RF_CHIP_A])) != 0 )
    {
        skiq_error("Unable to configure serializers for RFIC A (status=%d)\n", status);
        status = -1;
    }
    else
    {
        skiq_debug("Setup RFIC A serializers with premp=%u, amp=%u\n",
                   _taliseDevice[card][RF_CHIP_A]->jesd204Settings.serPreEmphasis,
                   _taliseDevice[card][RF_CHIP_A]->jesd204Settings.serAmplitude);
    }
    _taliseDevice[card][RF_CHIP_B]->jesd204Settings.serPreEmphasis = rx_preEmphasis;
    _taliseDevice[card][RF_CHIP_B]->jesd204Settings.serAmplitude = rx_serialAmplitude;
    if( (status=TALISE_setupSerializers(p_rfic_b, _taliseDevice[card][RF_CHIP_B])) != 0 )
    {
        skiq_error("Unable to configure serializers for RFIC B (status=%d)\n", status);
        status = -1;
    }
    else
    {
        skiq_debug("Setup RFIC B serializers with premp=%u, amp=%u\n",
                   _taliseDevice[card][RF_CHIP_B]->jesd204Settings.serPreEmphasis,
                   _taliseDevice[card][RF_CHIP_B]->jesd204Settings.serAmplitude);
    }

    if( status == 0 )
    {
        bool check_deframers;

        check_deframers = (( _taliseDevice[card][RF_CHIP_A]->tx.txChannels != TAL_TXOFF ) ||
                           (_taliseDevice[card][RF_CHIP_B]->tx.txChannels != TAL_TXOFF));

        status = _reset_jesd( p_ad9528, p_rfic_a, p_rfic_b, check_deframers, p_sync_result );
    }

    return (status);
}

int32_t reset_jesd_x4_defaults( uint8_t card,
                                jesd_sync_result_t *p_sync_result )
{
    return reset_jesd_x4( card,
                          DEFAULT_JESD_RX_PREEMPHASIS,
                          DEFAULT_JESD_RX_SERIALIZER_AMPLITUDE,
                          DEFAULT_JESD_TX_DIFFCTRL,
                          DEFAULT_JESD_TX_PRECURSOR, p_sync_result );
}

int32_t _reset_jesd( ad9528Device_t *p_ad9528,
                     taliseDevice_t *p_rfic_a,
                     taliseDevice_t *p_rfic_b,
                     bool check_deframers,
                     jesd_sync_result_t *p_sync_result )
{
    int32_t status=-1;
    adiEpiqDev_t *p_halDev_a = (adiEpiqDev_t*)(p_rfic_a->devHalInfo);
    uint8_t framerStatusA_rx = 0;
    uint8_t framerStatusA_orx = 0;
    uint16_t deframerStatusA = 0;
    uint8_t framerStatusB_rx = 0;
    uint8_t framerStatusB_orx = 0;
    uint16_t deframerStatusB = 0;
    uint32_t mismatch_a=0;
    uint32_t mismatch_b=0;
    uint32_t jesd_status=0;
    uint32_t jesd_cnt=0;
    uint8_t num_retries=0;
    uint8_t num_status_checks=0;
    bool synced = false;
    bool use_continuous = false;

    if( TALISE_radioOff( p_rfic_a ) == 0 &&
        TALISE_radioOff( p_rfic_b ) == 0 &&
        TALISE_serializerReset( p_rfic_a ) == 0 &&
        TALISE_serializerReset( p_rfic_b ) == 0 )
    {
        _skiq_log( SKIQ_LOG_DEBUG, "Radio off\n" );

        if( (status=fpga_jesd_config_sysref_continuous( p_halDev_a->card, false )) == 0 )
        {
            use_continuous = true;
            AD9528_requestSysref( p_ad9528, 0 );
        }
        else
        {
            // not having continuous mode isn't an error
            if( status != -ENOTSUP )
            {
                skiq_error("Unable to configure continuous SYSREF mode (card=%u)\n", p_halDev_a->card);
                return (status);
            }
        }
        // reset status
        status=-EAGAIN;

        for( num_retries=0; (num_retries<MAX_NUM_JESD_RETRIES) && (status==-EAGAIN); num_retries++ )
        {            
            // disable the sysref to the framers / deframers
            if( (TALISE_enableFramerLink(p_rfic_a, TAL_FRAMER_A_AND_B, 0) == 0) &&
                (TALISE_enableDeframerLink(p_rfic_a, TAL_DEFRAMER_A, 0) == 0) &&
                (TALISE_enableSysrefToFramer(p_rfic_a, TAL_FRAMER_A_AND_B, 0) == 0) &&
                (TALISE_enableSysrefToDeframer(p_rfic_a, TAL_DEFRAMER_A, 0) == 0) &&

                (TALISE_enableFramerLink(p_rfic_b, TAL_FRAMER_A_AND_B, 0) == 0) &&
                (TALISE_enableDeframerLink(p_rfic_b, TAL_DEFRAMER_A, 0) == 0) &&
                (TALISE_enableSysrefToFramer(p_rfic_b, TAL_FRAMER_A_AND_B, 0) == 0) &&
                (TALISE_enableSysrefToDeframer(p_rfic_b, TAL_DEFRAMER_A, 0) == 0) )
            {
                _skiq_log( SKIQ_LOG_DEBUG, "SYSREF to (de)framer disabled\n" );

                if( use_continuous == true )
                {
                    // ensure SYSREF is off
                    AD9528_requestSysref( p_ad9528, 0 );
                }

                // reset the FPGA JESD stuff
                fpga_jesd_ctrl_phy_reset( p_halDev_a->card );

                // now enable SYSREF to the framers / deframers
                if( (TALISE_enableFramerLink(p_rfic_a, TAL_FRAMER_A_AND_B, 1) == 0) &&
                    (TALISE_enableDeframerLink(p_rfic_a, TAL_DEFRAMER_A, 1) == 0) &&
                    (TALISE_enableSysrefToFramer(p_rfic_a, TAL_FRAMER_A_AND_B, 1) == 0) &&
                    (TALISE_enableSysrefToDeframer( p_rfic_a, TAL_DEFRAMER_A, 1) == 0) &&

                    (TALISE_enableFramerLink(p_rfic_b, TAL_FRAMER_A_AND_B, 1) == 0) &&
                    (TALISE_enableDeframerLink(p_rfic_b, TAL_DEFRAMER_A, 1) == 0) &&
                    (TALISE_enableSysrefToFramer(p_rfic_b, TAL_FRAMER_A_AND_B, 1) == 0) &&
                    (TALISE_enableSysrefToDeframer( p_rfic_b, TAL_DEFRAMER_A, 1) == 0) )
                {
                    _skiq_log( SKIQ_LOG_DEBUG, "SYSREF to (de)framer enabled\n" );

                    if( use_continuous == true )
                    {
                        AD9528_updateSYSREFMode( p_ad9528,
                                                 CONTINUOUS,
                                                 EIGHT_PULSES );
                    }
                    else
                    {
                        AD9528_updateSYSREFMode( p_ad9528,
                                                 NSHOT,
                                                 EIGHT_PULSES );
                        hal_millisleep(50);
                    }

                    if( _reset_cdr( p_halDev_a->card ) != 0 )
                    {
                        skiq_error( "Unable to reset clock recovery (card=%u)\n", p_halDev_a->card );
                    }

                    // pulse/enable SYSREF...we want to do it after the phy has reset but before the core is
                    // reset so that the FPGA JESD core see's the transition of TVALID for each chip
                    AD9528_requestSysref( p_ad9528, 1 );

                    /* Delaying 25 ms here drastically reduces the overall number of JESD sync
                     * retries for Sidekiq X4. This is likely needed to allow some time for the 
                     FPGA to actually observe SYSREF pulsing */
                    hal_millisleep(25);

                    fpga_jesd_ctrl_core_reset( p_halDev_a->card );

                    /* we should poll the status as it may take some time after the PHY is reset before
                       the framer status indicates success */
                    
                    // reset synced field
                    synced = false;

                    for( num_status_checks=0;
                         (num_status_checks<MAX_NUM_JESD_STATUS_CHECKS) && (synced==false) && (status==-EAGAIN);
                         num_status_checks++ )
                    {

                        // now check the framer status
                        if( (TALISE_readFramerStatus(p_rfic_a, TAL_FRAMER_A, &framerStatusA_rx) == 0) &&
                            (TALISE_readFramerStatus(p_rfic_a, TAL_FRAMER_B, &framerStatusA_orx) == 0) &&
                            (TALISE_readDeframerStatus(p_rfic_a, TAL_DEFRAMER_A, &deframerStatusA) == 0) &&
                            (TALISE_readFramerStatus(p_rfic_b, TAL_FRAMER_A, &framerStatusB_rx) == 0) &&
                            (TALISE_readFramerStatus(p_rfic_b, TAL_FRAMER_B, &framerStatusB_orx) == 0) &&
                            (TALISE_readDeframerStatus(p_rfic_b, TAL_DEFRAMER_A, &deframerStatusB) == 0) )
                        {
                            if ( check_deframers )
                            {
                                skiq_info("Framer status A 0x%x 0x%x B 0x%x 0x%x, deframer status A 0x%x B 0x%x (num checks %u)\n",
                                          framerStatusA_rx, framerStatusA_orx,
                                          framerStatusB_rx, framerStatusB_orx,
                                          deframerStatusA, deframerStatusB, num_status_checks);
                            }
                            else
                            {
                                skiq_info("Framer status A 0x%x 0x%x B 0x%x 0x%x, deframer status *ignored for profile* (num checks %u)\n",
                                          framerStatusA_rx, framerStatusA_orx,
                                          framerStatusB_rx, framerStatusB_orx, num_status_checks);
                            }

                            // make sure that the framer indicates its synced
                            if( ((framerStatusA_rx & FRAMER_SYNC_IN_MASK) != FRAMER_SYNC_IN_MASK) ||
                                ((framerStatusB_rx & FRAMER_SYNC_IN_MASK) != FRAMER_SYNC_IN_MASK) ||
                                ((framerStatusA_orx & FRAMER_SYNC_IN_MASK) != FRAMER_SYNC_IN_MASK) ||
                                ((framerStatusB_orx & FRAMER_SYNC_IN_MASK) != FRAMER_SYNC_IN_MASK) ||
                                ( check_deframers && ((deframerStatusA & DEFRAMER_SYNC_IN_MASK) != DEFRAMER_SYNC_IN_MASK) ) ||
                                ( check_deframers && ((deframerStatusB & DEFRAMER_SYNC_IN_MASK) != DEFRAMER_SYNC_IN_MASK) ) )
                            {
                                // if we're not synced yet, wait a bit and check again
                                if( num_status_checks<MAX_NUM_JESD_STATUS_CHECKS )
                                {
                                    debug_print("Polling status (num_status_checks=%u)\n", num_status_checks);
                                    hal_millisleep(JESD_STATUS_CHECK_DELAY_MS);
                                }
                                else
                                {
                                    status = -EAGAIN;
                                }
                            }
                            else
                            {
                                debug_print( "Got sync successfully!\n");
                                // we got sync!
                                synced = true;
                            }
                        } // framer status check ok
                        else
                        {
                            skiq_error("Unable to read framer status\n");
                            status = -ENODEV;
                        }
                    } // end of loop checking framer/deframer status

                    // see if we synced successfully
                    if( synced == false )
                    {
                        skiq_error("RX/TX (de)framer failed to sync\n");

                        TALISE_getDfrmIlasMismatch( p_rfic_a, TAL_DEFRAMER_A, &mismatch_a,
                                                    NULL, NULL );
                        TALISE_getDfrmIlasMismatch( p_rfic_b, TAL_DEFRAMER_A, &mismatch_b,
                                                        NULL, NULL );
                        skiq_error("ILAS mismatch A=0x%x, B=0x%x (num_retries=%u)\n",
                                   mismatch_a, mismatch_b, num_retries);

                        if( num_retries < (MAX_NUM_JESD_RETRIES - 1) )
                        {
                            skiq_debug("Retrying JESD sync (retries=%u) (card=%u)\n",
                                       num_retries, p_halDev_a->card);
                            status = -EAGAIN;
                        }
                        else
                        {
                            skiq_error("Max JESD sync retries=%u reached (card=%u)\n",
                                       num_retries, p_halDev_a->card);
                            hal_critical_exit(-1);
                            status = -1;
                            return (status);
                        }
                    }
                    else
                    {
                        // framer/deframer synced
                        jesd_unsync_t unsync_a;
                        jesd_unsync_t unsync_b;
                        uint32_t unsync;

                        skiq_debug("JESD sync successful (retries=%u) (card=%u)\n",
                                   num_retries, p_halDev_a->card);
                        
                        // wait 1 ms and make sure the JESD errors
                        hal_millisleep(1);
                        fpga_jesd_read_unsync_counts(p_halDev_a->card,
                                                     &unsync_a,
                                                     &unsync_b,
                                                     &unsync);
                        debug_print("JESD unsync count (RFIC A): rx %u, orx, %u, tx %u\n",
                                    unsync_a.rx_unsync, unsync_a.orx_unsync, unsync_a.tx_unsync);
                        debug_print("JESD unsync count (RFIC B): rx %u, orx, %u, tx %u\n",
                                    unsync_b.rx_unsync, unsync_b.orx_unsync, unsync_b.tx_unsync);
                        
                        // TX must be <2, everything else should be 0
                        if( (unsync_a.rx_unsync == 0) &&
                            (unsync_a.orx_unsync == 0) &&
                            ( !check_deframers || ( unsync_a.tx_unsync < 2) ) &&
                            (unsync_b.rx_unsync == 0) &&
                            (unsync_b.orx_unsync == 0) &&
                            ( !check_deframers || ( unsync_b.tx_unsync < 2) ) &&
                            (fpga_jesd_sync_status(p_halDev_a->card,
                                                   check_deframers /* TX */,
                                                   true /* RFIC B */) == 0) )
                        {
                            status=0;
                        }
                        else
                        {
                            if( num_retries < MAX_NUM_JESD_RETRIES - 1)
                            {
                                skiq_debug("Detected errors in JESD link, retrying (retries=%u) (card=%u)\n",
                                           num_retries, p_halDev_a->card);
                                status = -EAGAIN;
                            }
                            else
                            {
                                skiq_error("Max JESD sync retries=%u reached (card=%u)\n",
                                       num_retries, p_halDev_a->card);
                                hal_critical_exit(-1);
                                status = -1;
                                return (status);
                            }
                        }
                    } // end framer/deframer synced

                    // read FPGA registers for some additional details
                    sidekiq_fpga_reg_read(p_halDev_a->card, FPGA_REG_JESD_STATUS, &jesd_status);
                    sidekiq_fpga_reg_read(p_halDev_a->card, FPGA_REG_JESD_UNSYNC_COUNTERS, &jesd_cnt);
                    if( p_sync_result != NULL )
                    {
                        // populate our JESD result structure with more info
                        
                        // FPGA registers
                        p_sync_result->jesd_status_imm = jesd_status;
                        p_sync_result->jesd_unsync_imm = jesd_cnt;
                        
                        // A framer/deframer
                        p_sync_result->framer_a_rx = framerStatusA_rx;
                        p_sync_result->framer_a_orx = framerStatusA_orx;
                        p_sync_result->deframer_a = deframerStatusA;
                        
                        // B framer/deframer
                        p_sync_result->framer_b_rx = framerStatusB_rx;
                        p_sync_result->framer_b_orx = framerStatusB_orx;
                        p_sync_result->deframer_b = deframerStatusB;
                        
                        p_sync_result->num_retries = num_retries;
                    } // end p_sync_result != NULL
                } // end enable framer link ok
                else
                {
                    skiq_error("Unable to disable framer\n");
                    status = -3;
                }
            } // end enable sysref
            else
            {
                skiq_error( "Unable to enable sysref\n");
                status = -2;
            }
        } // end loop retries
    } // turn off radio
    else
    {
        skiq_error("Unable to disable radio\n");
        status = -4;
    }

    if( status == 0 )
    {
        skiq_debug("Disable SYSREF to (de)framer");
        // now disable SYSREF to the framers / deframers (no need to leave it on after synced
        if( (TALISE_enableSysrefToFramer(p_rfic_a, TAL_FRAMER_A_AND_B, 0) != 0) ||
            (TALISE_enableSysrefToDeframer( p_rfic_a, TAL_DEFRAMER_A, 0) != 0) ||
            
            (TALISE_enableSysrefToFramer(p_rfic_b, TAL_FRAMER_A_AND_B, 0) != 0) ||
            (TALISE_enableSysrefToDeframer( p_rfic_b, TAL_DEFRAMER_A, 0) != 0) )
        {
            skiq_error( "Unable to disable sysref\n");
            status = -2;
        }

        if( use_continuous == true )
        {
            status = fpga_jesd_config_sysref_continuous( p_halDev_a->card, false );
            // not having continuous mode isn't an error
            if( status == -ENOTSUP )
            {
                status=0;
            }
        }
    }
    
    return (status);
}

int32_t _reset_rf_iface( uint8_t card, ad9528Device_t *p_ad9528, taliseDevice_t *p_rfic_a, taliseDevice_t *p_rfic_b )
{
    int32_t status=0;
    uint32_t val=0;
    uint8_t scratch_val=0x5A;
    uint8_t verify_val=0;

    _skiq_log(SKIQ_LOG_DEBUG, "Resetting RF interface (card=%u)\n", card);

    // assert resets
    sidekiq_fpga_reg_read( card, FPGA_REG_FMC_CTRL, &val );
    RBF_SET(val, 0, AD9528_SPI_RESET_N );
    // AD9379 A
    RBF_SET(val, 0, RESET_N);
    // AD9379 B
    RBF_SET(val, 0, RESET_N_B);
    status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_FMC_CTRL, val );

    if( status == 0 )
    {
        // deassert resets
        RBF_SET(val, 1, AD9528_SPI_RESET_N );
        // AD9379 A
        RBF_SET(val, 1, RESET_N);
        // AD9379 B
        RBF_SET(val, 1, RESET_N_B);
        status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_FMC_CTRL, val );

        if( status == 0 )
        {
            // configure the SPI interface...note: we need to configure the 9379 first
            // otherwise we seem to get 9528 SPI errors.  Note that setting the SPI config
            // for the Talise automatically has a verification step at the end
            if( (TALISE_setSpiSettings(p_rfic_a, &(_taliseDevice[card][RF_CHIP_A]->spiSettings)) == TALACT_NO_ACTION) &&
                (TALISE_setSpiSettings(p_rfic_b, &(_taliseDevice[card][RF_CHIP_B]->spiSettings)) == TALACT_NO_ACTION) )
            {
                if( AD9528_setSpiSettings( p_ad9528 ) == 0 )
                {
                    // write/read to scratch register and verify
                    hal_rfic_write_reg( card, RFIC_SPI_CS_AD9528, AD9528_SCRATCH_REG, scratch_val );
                    hal_rfic_read_reg( card, RFIC_SPI_CS_AD9528, AD9528_SCRATCH_REG, &verify_val );
                    if( verify_val != scratch_val )
                    {
                        skiq_error("AD9528 SPI verification failed, read 0x%x, expected 0x%x (card=%u)\n",
                                  verify_val, scratch_val, card);
                        status = -ENODEV;
                    }
                }
                else
                {
                    skiq_error("AD9528 SPI configuration failed (card=%u)\n", card);
                    status = -ENODEV;
                }
            }
            else
            {
                skiq_error("AD9379 SPI configuration/verification failed (card=%u)\n", card);
                status = -ENODEV;
            }
        }
    }

    return (status);
}


static uint32_t read_profile_rx_sample_rate_khz( uint8_t card,
                                                 skiq_rx_hdl_t rx_hdl )
{
    uint32_t actual_rate_khz = 0;

    switch (rx_hdl)
    {
        case skiq_rx_hdl_A1:
        case skiq_rx_hdl_A2:
        case skiq_rx_hdl_B1:
        case skiq_rx_hdl_B2:
            /* @warning This assumes that the profile settings represent (or will represent) the
            actual device's present state */
            actual_rate_khz = _taliseDevice[card][rx_hdl_to_chip_id[rx_hdl]]->rx.rxProfile.rxOutputRate_kHz;
            break;

        case skiq_rx_hdl_C1:
        case skiq_rx_hdl_D1:
            /* @warning This assumes that both rficA and rficB are configured with the same profile
             * and that the profile settings represent (or will represent) the actual device's
             * present state */
            actual_rate_khz = _taliseDevice[card][rx_hdl_to_chip_id[rx_hdl]]->obsRx.orxProfile.orxOutputRate_kHz;
            break;

        default:
            skiq_error( "Handle %s (%u) not currently supported on Sidekiq %s at card %u\n",
                        rx_hdl_cstr( rx_hdl ), rx_hdl, part_cstr( _skiq_get_part( card ) ), card );
            break;
    }

    return actual_rate_khz;
}

static double read_profile_rx_sample_rate_hz( rf_id_t *p_id )
{
    return (double)(read_profile_rx_sample_rate_khz(p_id->card, p_id->hdl)) * RATE_KHZ_TO_HZ;
}


/**************************************************************************************************/
static uint32_t read_profile_tx_sample_rate_khz( uint8_t card,
                                                 skiq_tx_hdl_t tx_hdl )
{
    uint32_t actual_rate_khz = 0;

    switch (tx_hdl)
    {
        case skiq_tx_hdl_A1:
        case skiq_tx_hdl_A2:
        case skiq_tx_hdl_B1:
        case skiq_tx_hdl_B2:
            /* @warning This assumes that both rficA and rficB are configured with the same profile
             * and that the profile settings represent the actual device's present state.  however,
             * the devstate is not always up-to-date when this function is called */
            actual_rate_khz = _taliseDevice[card][tx_hdl_to_chip_id[tx_hdl]]->tx.txProfile.txInputRate_kHz;
            break;

        default:
            skiq_error( "Handle %s (%u) not currently supported on Sidekiq %s at card %u\n",
                        tx_hdl_cstr( tx_hdl ), tx_hdl, part_cstr( _skiq_get_part( card ) ), card );
            break;
    }

    return actual_rate_khz;
}


/**************************************************************************************************/
static double read_profile_tx_sample_rate_hz( rf_id_t *p_id )
{
    return (double)(read_profile_tx_sample_rate_khz(p_id->card, p_id->hdl)) * RATE_KHZ_TO_HZ;
}


/**************************************************************************************************/
static int32_t
config_orx_jesd_lanes( uint8_t card,
                       skiq_rx_hdl_t rx_hdl,
                       uint32_t devclk_khz,
                       uint8_t fpga_div )
{
    int32_t status = 0;
    uint32_t sample_rate_khz;
    uint8_t nr_lanes, orx_div, lane_setting;

    if ( ( rx_hdl == skiq_rx_hdl_C1 ) || ( rx_hdl == skiq_rx_hdl_D1 ) )
    {
        uint8_t orx_lane_sel;
        bool lane_swap = false;

        /* orx_div needs to be shifted depending on the ratio between ORX sample rate and dev_clk
         * frequency, use `fpga_jesd_calc_lane_div` to figure it out */
        sample_rate_khz = read_profile_rx_sample_rate_khz( card, rx_hdl );
        status = fpga_jesd_calc_lane_div( card, sample_rate_khz, devclk_khz, fpga_div,
                                          true, /* allow_dual_lane */
                                          &orx_div, &nr_lanes );

        /* In order to retain test coverage on all available JESD lanes on Sidekiq X4, the factory
         * API allows configuration of the RX JESD lane to use for the ObsRX ports, however it takes
         * effect only when the sample rate is changed.  In any case, read the lane selection, then
         * in the case of a single lane configuration, choose the corresponding JESD lane
         * accordingly. */
        if ( status == 0 )
        {
            status = fpga_jesd_read_orx_lane_sel( card, &orx_lane_sel );
        }

        if ( status == 0 )
        {
            if ( nr_lanes == 2 )
            {
                lane_setting = ORX_DUAL_LANE_SERIALIZER_SETTING;
                if ( orx_lane_sel != 0 )
                {
                    skiq_warning("Ignoring ORX JESD lane selection since RFIC profile is using both "
                                 "lanes on card %u\n", card);
                }
            }
            else if ( nr_lanes == 1 )
            {
                if ( orx_lane_sel == 0 )
                {
                    lane_setting = ORX_SINGLE_LANE_0_SERIALIZER_SETTING;
                    lane_swap = false;
                }
                else if ( orx_lane_sel == 1 )
                {
                    lane_setting = ORX_SINGLE_LANE_1_SERIALIZER_SETTING;
                    lane_swap = true;
                }
                else
                {
                    lane_setting = ORX_SINGLE_LANE_0_SERIALIZER_SETTING;
                    lane_swap = false;
                }
            }
            else
            {
                status = -EPROTO;
            }
        }

        if ( status == 0 )
        {
            /* Set the serializerLanesEnabled mask in the profile based on the number of lanes.
             * @warning This assignment will not take effect until TALISE_initialize() is called */
            _taliseDevice[card][RF_CHIP_A]->jesd204Settings.framerB.serializerLanesEnabled = lane_setting;
            _taliseDevice[card][RF_CHIP_B]->jesd204Settings.framerB.serializerLanesEnabled = lane_setting;
            /* configure the JESD lanes and write the PHY divider in the FPGA */
            debug_print("Configuring JESD for %s to use %u lane(s) with sample rate %u kHz on card "
                        "%u\n", rx_hdl_cstr( rx_hdl ), nr_lanes, sample_rate_khz, card );
            status = fpga_jesd_config_rx_lanes( card, rx_hdl, nr_lanes,
                                                false /* single chan */,
                                                lane_swap );
        }

        if ( status == 0 )
        {
            debug_print("Configuring JESD for %s to use a PHY divider of %u with sample rate %u "
                        "kHz on card %u\n", rx_hdl_cstr( rx_hdl ), orx_div, sample_rate_khz, card );
            status = fpga_jesd_write_phy_divider( card, rx_hdl, orx_div );
        }
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}


/**************************************************************************************************/
static int32_t
calc_rx1rx2_fpga_div( uint8_t card,
                      skiq_rx_hdl_t rx_hdl,
                      uint32_t devclk_khz,
                      uint8_t fpga_div,
                      uint8_t *p_rx_fpga_div )
{
    int32_t status = 0;

    if ( ( rx_hdl == skiq_rx_hdl_A1 ) || ( rx_hdl == skiq_rx_hdl_A2 ) ||
         ( rx_hdl == skiq_rx_hdl_B1 ) || ( rx_hdl == skiq_rx_hdl_B2 ) )
    {
        uint32_t sample_rate_khz;

        sample_rate_khz = read_profile_rx_sample_rate_khz( card, rx_hdl );

        status = fpga_jesd_calc_lane_div( card, sample_rate_khz, devclk_khz, fpga_div,
                                          false, /* allow_dual_lane */
                                          p_rx_fpga_div, NULL );
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}


/**************************************************************************************************/
#if (defined UNUSED)
static int32_t
calc_tx_fpga_div( uint8_t card,
                  skiq_tx_hdl_t tx_hdl,
                  uint32_t devclk_khz,
                  uint8_t fpga_div,
                  uint8_t *p_tx_fpga_div )
{
    int32_t status = 0;

    if ( ( tx_hdl == skiq_tx_hdl_A1 ) || ( tx_hdl == skiq_tx_hdl_A2 ) ||
         ( tx_hdl == skiq_tx_hdl_B1 ) || ( tx_hdl == skiq_tx_hdl_B2 ) )
    {
        uint32_t sample_rate_khz;

        sample_rate_khz = read_profile_tx_sample_rate_khz( card, tx_hdl );

        status = fpga_jesd_calc_lane_div( card, sample_rate_khz, devclk_khz, fpga_div,
                                          true, /* allow_dual_lane */
                                          p_tx_fpga_div, NULL );
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}
#endif  /* UNUSED */

/**************************************************************************************************/
static uint8_t *_stream_binary_by_rate( taliseDevice_t *p_device )
{
    uint8_t *p_stream_proc = NULL;

    if (p_device->devStateInfo.orxAdcStitchingEnabled > 0)
    {
        debug_print("Using the ADC stitching stream processor\n");
        p_stream_proc = TaliseStream_bin_adc_stitching;
    }
    else
    {
        debug_print("Using the default stream processor\n");
        p_stream_proc = TaliseStream_bin;
    }

    return p_stream_proc;
}


static int32_t
config_tx_jesd_lanes( uint8_t card,
                      skiq_tx_hdl_t tx_hdl,
                      uint32_t devclk_khz,
                      uint8_t fpga_div )
{
    int32_t status = 0;
    uint32_t sample_rate_khz;
    uint8_t nr_lanes, tx_div;
    skiq_tx_hdl_t tx_hdl_other;

    debug_print("tx_hdl: %s, devclk_khz: %ukHz, fpga_div: %u\n", tx_hdl_cstr(tx_hdl), devclk_khz, fpga_div);

    if ( tx_hdl == skiq_tx_hdl_A1 )
    {
        tx_hdl_other = skiq_tx_hdl_A2;
    }
    else if ( tx_hdl == skiq_tx_hdl_B1 )
    {
        tx_hdl_other = skiq_tx_hdl_B2;
    }
    else if ( ( tx_hdl == skiq_tx_hdl_A2 ) || ( tx_hdl == skiq_tx_hdl_B2 ) )
    {
        /* don't allow lane configuration for the secondary handles because the handles are
         * interdependent */
        skiq_error("Configuring Tx JESD lanes is only permitted for handles A1 and B1\n");
        status = -ERANGE;
    }
    else
    {
        status = -EINVAL;
    }


    if ( status == 0 )
    {
        uint8_t lane_setting, tx_lane_sel;

        /* tx_div may need to be shifted depending on the ratio between the TX sample rate and
         * dev_clk frequency, use `fpga_jesd_calc_lane_div` to figure it out */
        sample_rate_khz = read_profile_tx_sample_rate_khz( card, tx_hdl );
        status = fpga_jesd_calc_lane_div( card, sample_rate_khz, devclk_khz, fpga_div,
                                          true, /* allow_dual_lane */
                                          &tx_div, &nr_lanes );

        /* In order to retain test coverage on all available JESD lanes on Sidekiq X4, the factory
         * API allows configuration of the TX JESD lane to use for the TX ports, however it takes
         * effect only when the sample rate is changed.  In any case, read the lane selection, then
         * in the case of a single lane configuration, choose the corresponding JESD lane
         * accordingly. */
        if ( status == 0 )
        {
            status = fpga_jesd_read_tx_lane_sel( card, &tx_lane_sel );
        }

        if ( status == 0 )
        {
            if ( nr_lanes == 2 )
            {
                lane_setting = TX_DUAL_LANE_DESERIALIZER_SETTING;
                if ( tx_lane_sel != 0 )
                {
                    skiq_warning("Ignoring TX JESD lane selection since RFIC profile is using all "
                                 "lanes on card %u\n", card);
                }
            }
            else if ( nr_lanes == 1 )
            {
                if ( tx_lane_sel == 0 )
                {
                    lane_setting = TX_LANE_SEL_0_DESERIALIZER_LANES;
                }
                else if ( tx_lane_sel == 1 )
                {
                    lane_setting = TX_LANE_SEL_1_DESERIALIZER_LANES;
                }
                else
                {
                    status = -EPROTO;
                }
            }
            else
            {
                status = -EPROTO;
            }
        }

        if ( status == 0 )
        {
            /* Set the serializerLanesEnabled mask in the profile based on the number of lanes.
             * @warning This assignment will not take effect until TALISE_initialize() is called */
            _taliseDevice[card][RF_CHIP_A]->jesd204Settings.deframerA.deserializerLanesEnabled = lane_setting;
            _taliseDevice[card][RF_CHIP_B]->jesd204Settings.deframerA.deserializerLanesEnabled = lane_setting;

            /* configure the JESD lanes (single or dual) */
            debug_print("Configuring Tx JESD for %s and %s to use %u lane(s) with sample rate %u kHz on "
                        "card %u\n", tx_hdl_cstr( tx_hdl ), tx_hdl_cstr( tx_hdl_other ), nr_lanes,
                        sample_rate_khz, card );
            status = fpga_jesd_config_tx_lanes( card, tx_hdl, nr_lanes );
        }
    }

    if ( status == 0 )
    {
        debug_print("Configuring Tx JESD for %s to use a PHY divider of %u with sample rate %u "
                    "kHz on card %u\n", tx_hdl_cstr( tx_hdl ), tx_div, sample_rate_khz, card );
        status = fpga_jesd_phy_modify_tx_rate( card, tx_hdl, tx_div, nr_lanes );
    }

    if ( status == 0 )
    {
        debug_print("Configuring Tx JESD for %s to use a PHY divider of %u with sample rate %u "
                    "kHz on card %u\n", tx_hdl_cstr( tx_hdl_other ), tx_div, sample_rate_khz, card );
        status = fpga_jesd_phy_modify_tx_rate( card, tx_hdl_other, tx_div, nr_lanes );
    }

    return status;
}

static int32_t calc_clocks_and_dividers(rf_id_t *p_id)
{

    int32_t status = 0;
    uint32_t actual_sample_rate=0;
    clk_div_t fpga_div = CLK_DIV_INVALID;
    uint32_t dev_clk = _taliseDevice[p_id->card][RF_CHIP_A]->clocks.deviceClock_kHz * RATE_KHZ_TO_HZ;
    clk_div_t dev_clk_div = CLK_DIV_INVALID;
    uint32_t rffc_freq;
    uint64_t fpga_gbt_clk;
    // FPGA clocks from fpga_jesd_ctrl.c
    uint64_t fpga_min_dev_clk;
    uint64_t fpga_max_dev_clk;
    fpga_jesd_qpll_freq_range( p_id->card, &fpga_min_dev_clk, &fpga_max_dev_clk );
    status = calc_ad9528_for_sample_rate( _taliseDevice[p_id->card][RF_CHIP_A]->rx.rxProfile.rxOutputRate_kHz * RATE_KHZ_TO_HZ,
                                          &actual_sample_rate,
                                          _clockDevice[p_id->card].pll1Settings,
                                          _clockDevice[p_id->card].pll2Settings,
                                          _clockDevice[p_id->card].outputSettings,
                                          _clockDevice[p_id->card].sysrefSettings,
                                          &fpga_div,
                                          &dev_clk,
                                          &dev_clk_div,
                                          &rffc_freq,
                                          fpga_min_dev_clk,
                                          fpga_max_dev_clk,
                                          &fpga_gbt_clk,
                                          _x4_clk_sel,
                                          skiq_x4);

    debug_print("Results of calc_ad9528_for_sample_rate:\n"
                "\t actual_sample_rate: %" PRIu32 "\n"
                "\t fpga_div:           %" PRIu8 "\n"
                "\t dev_clk:            %" PRIu32 "\n"
                "\t dev_clk_div:        %" PRIu8 "\n"
                "\t rffc_freq:          %" PRIu32 "\n"
                "\t fpga_gbt_clk:       %" PRIu64 "\n",
                actual_sample_rate, fpga_div, dev_clk, dev_clk_div, rffc_freq, fpga_gbt_clk);

    /* Configure the JESD lanes for RX handles based on dev_clk and fpga_div */
    if ( status == 0 )
    {
        uint8_t rx_fpga_div;
        _fpga_gbt_clock[p_id->card] = fpga_gbt_clk;

        status = calc_rx1rx2_fpga_div( p_id->card, skiq_rx_hdl_A1,
                                       _taliseDevice[p_id->card][RF_CHIP_A]->clocks.deviceClock_kHz,
                                       CLK_DIV_1, &rx_fpga_div );
        debug_print("rx_fpga_div: %" PRIu8 "\n", rx_fpga_div );
        if ( status == 0 )
        {
            // setup the JESD lanes
            if( (fpga_jesd_config_rx_lanes( p_id->card, skiq_rx_hdl_A1, 2, true, true ) != 0) ||
                (fpga_jesd_config_rx_lanes( p_id->card, skiq_rx_hdl_B1, 2, true, true ) != 0) )
            {
                skiq_error("Unable to initialize JESD lanes correctly for card %u\n", p_id->card);
                hal_critical_exit(-EBADMSG);
                return (-EBADMSG);
            }
        }

        if ( status == 0 )
        {
            // set hdl 0/1/2/3 to the same config
            skiq_debug("Configuring FPGA PHY dividers to %u for Rx A1/A2/B1/B2 (card=%u)\n",
                      rx_fpga_div, p_id->card);

            if( (fpga_jesd_write_phy_divider( p_id->card, skiq_rx_hdl_A1, rx_fpga_div ) != 0) ||
                (fpga_jesd_write_phy_divider( p_id->card, skiq_rx_hdl_A2, rx_fpga_div ) != 0) ||
                (fpga_jesd_write_phy_divider( p_id->card, skiq_rx_hdl_B1, rx_fpga_div ) != 0) ||
                (fpga_jesd_write_phy_divider( p_id->card, skiq_rx_hdl_B2, rx_fpga_div ) != 0) )
            {
                skiq_error("Unable to configure the FPGA JESD dividers for card %u\n", p_id->card);
                hal_critical_exit(-EBADMSG);
                return (-EBADMSG);
            }
        }
    }

    /* Configure the JESD lanes for ORX handles based on dev_clk and fpga_div */
    if ( status == 0 )
    {
        status = config_orx_jesd_lanes( p_id->card, skiq_rx_hdl_C1,
                                        _taliseDevice[p_id->card][RF_CHIP_A]->clocks.deviceClock_kHz,
                                        CLK_DIV_1 );
        if ( status == 0 )
        {
            status = config_orx_jesd_lanes( p_id->card, skiq_rx_hdl_D1,
                                            _taliseDevice[p_id->card][RF_CHIP_B]->clocks.deviceClock_kHz,
                                            CLK_DIV_1 );
        }

        if ( status != 0 )
        {
            skiq_error("Unable to configure JESD lanes for ORX handles for card %u (status code "
                       "= %d)\n", p_id->card, status);
            hal_critical_exit(status);
            return status;
        }
    }

    /* Configure the JESD lanes for TX handles based on dev_clk and fpga_div */
    if ( ( status == 0 ) && ( _taliseDevice[p_id->card][RF_CHIP_A]->tx.txChannels != TAL_TXOFF ) )
    {
        /* configure the TX JESD lanes by designating the primary transmit handles and `fpga_div` */
        if ( status == 0 )
        {
            status = config_tx_jesd_lanes( p_id->card, skiq_tx_hdl_A1,
                                           _taliseDevice[p_id->card][RF_CHIP_A]->clocks.deviceClock_kHz,
                                           CLK_DIV_1 );
        }

        if ( status == 0 )
        {
            status = config_tx_jesd_lanes( p_id->card, skiq_tx_hdl_B1,
                                           _taliseDevice[p_id->card][RF_CHIP_B]->clocks.deviceClock_kHz,
                                           CLK_DIV_1 );
        }

        if ( status != 0 )
        {
            skiq_error("Unable to configure JESD lanes for TX handles for card %u (status code "
                       "= %d)\n", p_id->card, status);
            hal_critical_exit(status);
            return status;
        }
    }
    if (status == 0)
    {
        fpga_jesd_ctrl_tx_config(p_id->card, DEFAULT_JESD_TX_DIFFCTRL, DEFAULT_JESD_TX_PRECURSOR);
    }

    return status;

}

#if (defined DEBUG_PRINT_ENABLED)
void _dump_ad9528_reg_range(uint8_t card, uint16_t start_addr, uint16_t end_addr )
{
    uint16_t addr=0;
    uint8_t data=0;
    
    for( addr=start_addr; addr<=end_addr; addr++ )
    {
        hal_rfic_read_reg( card, RFIC_SPI_CS_AD9528, addr, &data );
        debug_print("AD9528 register 0x%x=0x%x\n", addr, data);
    }
}
#endif


int32_t _reset_and_init( rf_id_t *p_id )
{
    int32_t status=0;
    uint32_t val=0;
    taliseDevice_t *p_rfic_a = NULL;
    taliseDevice_t *p_rfic_b = NULL;
    uint8_t lockStatusA=0;
    uint8_t lockStatusB=0;
    uint32_t init_cal_mask = BASE_INIT_CAL;
    uint8_t err_flag=0;
    rf_id_t rf_id_a;
    rf_id_t rf_id_b;
    skiq_ref_clock_select_t ref_clock = skiq_ref_clock_invalid;
    uint32_t ref_clock_freq;

    // TODO: we should support re-initializing the RFIC independently eventually...but for now,
    // it's fixed
    memcpy(&rf_id_a, p_id, sizeof(rf_id_t));
    rf_id_a.hdl = skiq_rx_hdl_A1;
    // This is a bit hackey, but we're assuming that we need to init B in the same
    // call, so we're going to copy the rf_id_t data structure and make sure the hdl is B
    memcpy(&rf_id_b, p_id, sizeof(rf_id_t));
    rf_id_b.hdl = skiq_rx_hdl_B1;

    p_rfic_a = rf_id_to_rficDevice( &rf_id_a );
    p_rfic_b = rf_id_to_rficDevice( &rf_id_b );

    _skiq_log( SKIQ_LOG_DEBUG, "RFIC initialization (card=%u)\n", p_id->card );

    // free memory if previously alloced
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].spiSettings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll1Settings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll2Settings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].outputSettings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].sysrefSettings );

    /////////////////////
    // alloc mem and copy default params
    _skiq_log( SKIQ_LOG_DEBUG, "Initializing 9528 data structures (card=%u)\n", p_id->card);
    _clockDevice[p_id->card].spiSettings =
        (ad9528SpiSettings_t*)(malloc(sizeof(ad9528SpiSettings_t)));
    if( _clockDevice[p_id->card].spiSettings == NULL )
    {
        status = -ENOMEM;
        skiq_error( "Unable to allocate memory for SPI settings (card=%u)\n", p_id->card);
        return (status);
    }
    memcpy( (void*)(_clockDevice[p_id->card].spiSettings),
            (void*)(&_x4_clockSpiSettings),
            sizeof(ad9528SpiSettings_t) );

    _clockDevice[p_id->card].pll1Settings =
        (ad9528pll1Settings_t*)(malloc(sizeof(ad9528pll1Settings_t)));
    if( _clockDevice[p_id->card].pll1Settings == NULL )
    {
        status = -ENOMEM;
        skiq_error( "Unable to allocate memory for PLL1 settings (card=%u)\n", p_id->card);
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].spiSettings );
        return (status);
    }

    _clockDevice[p_id->card].pll2Settings =
        (ad9528pll2Settings_t*)(malloc(sizeof(ad9528pll2Settings_t)));
    if( _clockDevice[p_id->card].pll2Settings == NULL )
    {
        status = -ENOMEM;
        skiq_error( "Unable to allocate memory for PLL2 settings (card=%u)\n", p_id->card);
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].spiSettings );
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll1Settings );
        return (status);
    }

    _clockDevice[p_id->card].outputSettings =
        (ad9528outputSettings_t*)(malloc(sizeof(ad9528outputSettings_t)));
    if( _clockDevice[p_id->card].outputSettings == NULL )
    {
        status = -ENOMEM;
        skiq_error( "Unable to allocate memory for clock output settings (card=%u)\n", p_id->card);
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].spiSettings );
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll1Settings );
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll2Settings );
        return (status);
    }
    memcpy( (void*)(_clockDevice[p_id->card].outputSettings),
            (void*)(&_x4_clockOutputSettings),
            sizeof(ad9528outputSettings_t) );

    _clockDevice[p_id->card].sysrefSettings =
        (ad9528sysrefSettings_t*)(malloc(sizeof(ad9528sysrefSettings_t)));
    if( _clockDevice[p_id->card].sysrefSettings == NULL )
    {
        status = -ENOMEM;
        skiq_error( "Unable to allocate memory for clock SYSREF settings (card=%u)\n", p_id->card);
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].spiSettings );
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll1Settings );
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll2Settings );
        FREE_IF_NOT_NULL( _clockDevice[p_id->card].outputSettings );
        return (status);
    }
    memcpy( (void*)(_clockDevice[p_id->card].sysrefSettings),
            (void*)(&_x4_clockSysrefSettings),
            sizeof(ad9528sysrefSettings_t) );

    // init func ptrs, other misc params
    _clockDevice[p_id->card].spiSettings->id = p_id->card;
    _clockDevice[p_id->card].id = p_id->card;
    _clockDevice[p_id->card].CMB_hardReset = NULL; // we don't need a reset here, we're already toggling reset line ourselves
    _clockDevice[p_id->card].CMB_regRead = NULL;
    _clockDevice[p_id->card].CMB_regWrite = NULL;

    ///////////////////////////////////////////////
    // Configure 9528
    // figure out our clock config
    // see what the clock settings are
    ad9528_init_struct( _clockDevice[p_id->card].pll1Settings,
                        _clockDevice[p_id->card].pll2Settings );

    // setup the clock based on internal or external reference
    sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
    skiq_read_ref_clock_select( p_id->card, &ref_clock );
    if( (ref_clock == skiq_ref_clock_external) ||
        (ref_clock == skiq_ref_clock_carrier_edge) ||
        (ref_clock == skiq_ref_clock_host) )
    {
        // turn on ext ref clock
        RBF_SET(val, 1, EXT_CLK_ENA);

        // clear clock select for external (p. 6 of schematic)
        RBF_SET(val, 0, EXT_CLK_SEL);

        // turn off the internal clock
        RBF_SET(val, 0, TEN_MEG_EN);

        // disable the internal 10M source
        // now set the ref clock to A on the 9528 (turn off B)
        _clockDevice[p_id->card].pll1Settings->refA_bufferCtrl = DISABLED;
        _clockDevice[p_id->card].pll1Settings->refB_bufferCtrl = DIFFERENTIAL;
        _clockDevice[p_id->card].ref_sel_mode = ref_sel_mode_refB;
        if((ref_clock == skiq_ref_clock_external) ||
            (ref_clock == skiq_ref_clock_carrier_edge))
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Configuring reference clock for external (card=%u)\n", p_id->card);

            status = skiq_read_ext_ref_clock_freq(p_id->card, &ref_clock_freq);
            if(status != 0)
            {
                skiq_error("Unable to read external reference clock frequency (card=%u)\n", p_id->card);
                goto init_complete;
            }

            _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = ref_clock_freq;
            _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = ref_clock_freq;
        }
        else
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Configuring reference clock for host (card=%u)\n", p_id->card);
            _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = HOST_REF_CLOCK_FREQ;
            _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = HOST_REF_CLOCK_FREQ;
        }
        _skiq_log(SKIQ_LOG_DEBUG, "Configuring ref frequency to %" PRIu32 " (card=%u)\n",
                  _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz, p_id->card);
    }
    else
    {
        if( ref_clock != skiq_ref_clock_internal )
        {
            skiq_error( "invalid reference clock detected, assuming internal (card=%u)\n", p_id->card);
        }
        _skiq_log(SKIQ_LOG_DEBUG, "Configuring reference clock for internal (card=%u)\n", p_id->card);

        // turn off ext ref clock
        RBF_SET(val, 0, EXT_CLK_ENA);

        // clear clock select for external (p. 6 of schematic)
        RBF_SET(val, 1, EXT_CLK_SEL);

        // turn on the internal clock
        RBF_SET(val, 1, TEN_MEG_EN);

        // now set the ref clock to A on the 9528 (turn off B)
        _clockDevice[p_id->card].pll1Settings->refA_bufferCtrl = DIFFERENTIAL;
        _clockDevice[p_id->card].pll1Settings->refB_bufferCtrl = DISABLED;
        _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = HOST_REF_CLOCK_FREQ;
        _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = HOST_REF_CLOCK_FREQ;
        _clockDevice[p_id->card].ref_sel_mode = ref_sel_mode_refA;
    }
    // write and verify
    status = sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_FMC_CTRL, val );
    if( status != 0 )
    {
        skiq_error( "Unable to configure FPGA FMC control register (card=%u)\n", p_id->card);
        goto init_complete;
    }

    if(ref_clock == skiq_ref_clock_carrier_edge)
    {
        bool carrier_edge_capable = false;

        // choose external clock select source [SMA|FPGA] based on the Carrier Edge capabilities bit (p. 6 of schematic)
        status = fpga_ctrl_is_carrier_edge_ref_clock_capable(p_id->card, &carrier_edge_capable);

        if((status == 0) && (carrier_edge_capable == 1))
        {
            // setup the clock based on internal or external reference
            status = sidekiq_fpga_reg_read(p_id->card, FPGA_REG_FMC_CTRL, &val);
            if(status == 0)
            {
                RBF_SET(val, 1, EXT_CLK_SEL);
                // write and verify
                status = sidekiq_fpga_reg_write_and_verify(p_id->card, FPGA_REG_FMC_CTRL, val);
                if(status != 0)
                {
                    skiq_error("Unable to configure FPGA FMC control register (card=%u)"
                            "with status: %" PRIi32 "\n", p_id->card, status);
                }
            }
        }
        else
        {
            skiq_error("Carrier Edge external reference clock is not supported on card %u", p_id->card);
            status = -ENOTSUP;
        }
    }
    else
    {
        // setup the clock based on internal or external reference
        status = sidekiq_fpga_reg_read(p_id->card, FPGA_REG_FMC_CTRL, &val);
        if(status == 0)
        {
            RBF_SET(val, 0, EXT_CLK_SEL);
            // write and verify
            status = sidekiq_fpga_reg_write_and_verify(p_id->card, FPGA_REG_FMC_CTRL, val);
            if(status != 0)
            {
                skiq_error("Failed to clear FPGA FMC control register (card=%u)"
                        "with status: %" PRIi32 "\n", p_id->card, status);
            }
        }
    }

    if(status == 0)
    {
        status = calc_clocks_and_dividers( p_id);
    }

    if ( status != 0 )
    {
        skiq_error( "Unable to calculate clocking configuration for dev_clock (card=%u) "
                    "(status=%d)\n", p_id->card, status );
        goto init_complete;
    }

    sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );

    // select the appropriate oscialltor
    if( _clockDevice[p_id->card].pll1Settings->vcxo_Frequency_Hz == 153600000 )
    {
        _skiq_log( SKIQ_LOG_DEBUG, "Selecting 153.6M clock (card=%u)\n", p_id->card );
        RBF_SET( val, 1, VCXO_153M6_EN );
    }
    else if( _clockDevice[p_id->card].pll1Settings->vcxo_Frequency_Hz == 100000000 )
    {
        _skiq_log( SKIQ_LOG_DEBUG, "Selecting 100M VCXO clock (card=%u)\n", p_id->card );
        RBF_SET( val, 0, VCXO_153M6_EN );
    }
    else
    {
        status = -EINVAL;
        skiq_error("Invalid VCXO frequency calculated (card=%u)\n", p_id->card);
        hal_critical_exit( status );
    }
    // write and verify
    status = sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_FMC_CTRL, val );
    if( status != 0 )
    {
        skiq_error( "Unable to configure FPGA FMC control register (card=%u)\n", p_id->card);
        goto init_complete;
    }

    // TODO: investigate this...we want to wait a bit for the reset signal to be applied before
    // trying to do anything.  We should investigate this further
    hal_nanosleep(50*MILLISEC);

    // RF reset and verification
    _skiq_log( SKIQ_LOG_INFO, "Resetting the RF interface (card=%u)\n", p_id->card);
    if( _reset_rf_iface(p_id->card, &(_clockDevice[p_id->card]), p_rfic_a, p_rfic_b ) != 0 )
    {
        _skiq_log(SKIQ_LOG_DEBUG, "Unable to reset the RF interface\n");
        hal_critical_exit(-1);
        return (-1);
    }

    _skiq_log( SKIQ_LOG_DEBUG, "Attempting AD9528 initialization using VCXO %u (card=%u)\n",
               _clockDevice[p_id->card].pll1Settings->vcxo_Frequency_Hz, p_id->card );

    //possible optimization here, don't reset/configure 9528 unless the dev_clock is different

    if( AD9528_initialize( &_clockDevice[p_id->card] ) == 0 )
    {
        _skiq_log(SKIQ_LOG_INFO, "AD9528 initialization success (card=%u)!\n", p_id->card);
    }
    else
    {
        skiq_error("AD9528 initialization failed (card=%u)\n", p_id->card);

        hal_critical_exit(-1);
        return (-1);
    }

    /*  make sure to reset the phy now that the clock is configured*/
    if( fpga_jesd_ctrl_phy_reset( p_id->card ) != 0 )
    {
        skiq_error( "Unable to reset the JESD PHY (card=%u)\n", p_id->card);
        hal_critical_exit(-1);
        return (-1);
    }

    fpga_jesd_ctrl_tx_config(p_id->card, DEFAULT_JESD_TX_DIFFCTRL, DEFAULT_JESD_TX_PRECURSOR);

    _skiq_log( SKIQ_LOG_DEBUG, "Attempting open HW (card=%u)\n", p_id->card);
    if( (TALISE_openHw(p_rfic_a) == 0) &&
        (TALISE_openHw(p_rfic_b) == 0) )
    {
        _skiq_log( SKIQ_LOG_DEBUG, "Successfully opened HW, attempting init (card=%u)\n", p_id->card);
        if( (TALISE_initialize(p_rfic_a, _taliseDevice[p_id->card][RF_CHIP_A]) == 0) &&
            (TALISE_initialize(p_rfic_b, _taliseDevice[p_id->card][RF_CHIP_B]) == 0) )
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Successfully initialize RFIC (card=%u)\n", p_id->card);

            // check PLL lock status
            if( (TALISE_getPllsLockStatus(p_rfic_a, &lockStatusA) != TALACT_NO_ACTION) ||
                (TALISE_getPllsLockStatus(p_rfic_b, &lockStatusB) != TALACT_NO_ACTION) )
            {
                skiq_error( "PLLs not locked (card=%u)!\n", p_id->card);
                TALISE_shutdown( p_rfic_a );
                TALISE_shutdown( p_rfic_b );
                return (-1);
            }
            else
            {
                // see talise_radioctrl.h: TALISE_getPllLockStatus
                // bit 0 => CLK PLL lock
                // bit 1 => RF PLL lock
                // bit 2 => AUX PLL lock
                _skiq_log(SKIQ_LOG_DEBUG, "PLLs locked successfully lockA=0x%x lockA=0x%x (card=%u)\n",
                          lockStatusA, lockStatusB, p_id->card);
                // now perform MCS
                status =  _perform_mcs( &_clockDevice[p_id->card], p_rfic_a, p_rfic_b );
                if( status != 0 )
                {
                    skiq_error( "Unable to perform MCS procedure (card=%u)\n", p_id->card);
                    hal_critical_exit(status);
                }

                TALISE_getPllsLockStatus(p_rfic_a, &lockStatusA);
                TALISE_getPllsLockStatus(p_rfic_b, &lockStatusB);
                _skiq_log(SKIQ_LOG_DEBUG, "PLLs lock status 0x%x 0x%x (card=%u)\n",
                          lockStatusA, lockStatusB, p_id->card);

                if( (TALISE_initArm( p_rfic_a, _taliseDevice[p_id->card][RF_CHIP_A] ) == 0) &&
                    (TALISE_initArm( p_rfic_b, _taliseDevice[p_id->card][RF_CHIP_B] ) == 0) )
                {
                    _skiq_log(SKIQ_LOG_DEBUG, "ARM initialized successfully (card=%u)\n", p_id->card);


                    // try programming the ARM and Stream processor
                    if( (TALISE_loadStreamFromBinary( p_rfic_a,
                                                      _stream_binary_by_rate( p_rfic_a ) ) == 0 ) &&
                        (TALISE_loadStreamFromBinary( p_rfic_b,
                                                      _stream_binary_by_rate( p_rfic_b ) ) == 0 ) &&
                        (TALISE_loadArmFromBinary( p_rfic_a,
                                                   TaliseTDDArmFirmware_bin,
                                                   TaliseTDDArmFirmware_bin_len ) == 0) &&
                        (TALISE_loadArmFromBinary( p_rfic_b,
                                                   TaliseTDDArmFirmware_bin,
                                                   TaliseTDDArmFirmware_bin_len ) == 0) )
                    {
                        _skiq_log(SKIQ_LOG_INFO, "ARM and Stream processor loaded successfully (0x%x) (card=%u)\n",
                                  p_rfic_a->devStateInfo.devState, p_id->card);
                        // configure default PLL
                        if( (TALISE_setRfPllFrequency(p_rfic_a,
                                                      TAL_RF_PLL,
                                                      DEFAULT_PLL_FREQ) == 0) &&
                            (TALISE_setRfPllFrequency(p_rfic_b,
                                                      TAL_RF_PLL,
                                                      DEFAULT_PLL_FREQ) == 0) )
                        {
                            TALISE_getPllsLockStatus( p_rfic_a,
                                                      &lockStatusA );
                            TALISE_getPllsLockStatus( p_rfic_b,
                                                      &lockStatusB );

                            if( ((lockStatusA & TAL_RX_ONLY_PLL_LOCK) != TAL_RX_ONLY_PLL_LOCK) ||
                                ((lockStatusB & TAL_RX_ONLY_PLL_LOCK) != TAL_RX_ONLY_PLL_LOCK) )
                            {
                                _skiq_log(SKIQ_LOG_DEBUG, "PLL lock status failed (0x%x) (0x%x) (card=%u)\n",
                                          lockStatusA, lockStatusB, p_id->card );
                                status = -2;
                            }
                            else
                            {
                                _skiq_log(SKIQ_LOG_DEBUG, "PLLs locked (0x%x, 0x%x) (card=%u)!\n",
                                          lockStatusA, lockStatusB, p_id->card);

                                /////////////////////////////////////////////////
                                // The following is necessary from ADI to  sync things with API v6.0.
                                // This it outlined in "Talise_Phase_Sync_using_API_SW_v6.0.pdf",
                                // at /mnt/storage/projects/sidekiq_x4/sw/ADI_API_v6.0_RC9
                                // Link.SpiWrite(hex2dec('180'), hex2dec('98'));
                                // Link.SpiWriteField(hex2dec('182'), 0, hex2dec('10'), 4);
                                // Link.SpiWriteField(hex2dec('182'), 1, hex2dec('10'), 4);
                                // Link.SpiWriteField(hex2dec('1382'), 1, hex2dec('01'), 0);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.SpiWriteField(hex2dec('1382'), 0, hex2dec('01'), 0);
                                // Link.SpiWrite(hex2dec('180'), hex2dec('93'));
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                //////////////////////////////////////////
                                // Link.Talise.enableMultichipRfLOPhaseSync(hex2dec('1'));
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Talise.enableMultichipRfLOPhaseSync(hex2dec('0'));
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                /////////////////////////////////////////////////
                                
                                // ensure SYSREF is disabled
                                AD9528_requestSysref( &_clockDevice[p_id->card], 0 );
                                // setup SYSREF for NSHOT
                                AD9528_updateSYSREFMode( &_clockDevice[p_id->card], NSHOT, EIGHT_PULSES );

                                _skiq_log(SKIQ_LOG_DEBUG, "Performing additional sync procedure (card=%u)\n", p_id->card);

                                // Link.SpiWrite(hex2dec('180'), hex2dec('98'));
                                ADIHAL_spiWriteByte( p_rfic_a->devHalInfo, 0x180, 0x98 );
                                ADIHAL_spiWriteByte( p_rfic_b->devHalInfo, 0x180, 0x98 );
                                // Link.SpiWriteField(hex2dec('182'), 0, hex2dec('10'), 4);
                                ADIHAL_spiWriteField( p_rfic_a->devHalInfo, 0x182, 0, 0x10, 4 );
                                ADIHAL_spiWriteField( p_rfic_b->devHalInfo, 0x182, 0, 0x10, 4 );
                                // Link.SpiWriteField(hex2dec('182'), 1, hex2dec('10'), 4);
                                ADIHAL_spiWriteField( p_rfic_a->devHalInfo, 0x182, 1, 0x10, 4 );
                                ADIHAL_spiWriteField( p_rfic_b->devHalInfo, 0x182, 1, 0x10, 4 );
                                // Link.SpiWriteField(hex2dec('1382'), 1, hex2dec('01'), 0);
                                ADIHAL_spiWriteField( p_rfic_a->devHalInfo, 0x1382, 1, 0x01, 0 );
                                ADIHAL_spiWriteField( p_rfic_b->devHalInfo, 0x1382, 1, 0x01, 0 );

                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // note: we're setup for 8 pulse mode, so only need 1 call
                                AD9528_requestSysref( &_clockDevice[p_id->card], 1 );

                                // Link.SpiWriteField(hex2dec('1382'), 0, hex2dec('01'), 0);
                                ADIHAL_spiWriteField( p_rfic_a->devHalInfo, 0x1382, 0, 0x01, 0 );
                                ADIHAL_spiWriteField( p_rfic_b->devHalInfo, 0x1382, 0, 0x01, 0 );
                                // Link.SpiWrite(hex2dec('180'), hex2dec('93'));
                                ADIHAL_spiWriteByte( p_rfic_a->devHalInfo, 0x180, 0x93 );
                                ADIHAL_spiWriteByte( p_rfic_b->devHalInfo, 0x180, 0x93 );

                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // note: we're setup for 8 pulse mode, so only need 1 call                                
                                AD9528_requestSysref( &_clockDevice[p_id->card], 1 );

                                // Link.Talise.enableMultichipRfLOPhaseSync(hex2dec('1'));
                                TALISE_enableMultichipRfLOPhaseSync( p_rfic_a, 1 );
                                TALISE_enableMultichipRfLOPhaseSync( p_rfic_b, 1 );

                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // note: we're setup for 8 pulse mode, so only need 1 call                                
                                AD9528_requestSysref( &_clockDevice[p_id->card], 1 );

                                // Link.Talise.enableMultichipRfLOPhaseSync(hex2dec('0'));
                                TALISE_enableMultichipRfLOPhaseSync( p_rfic_a, 0 );
                                TALISE_enableMultichipRfLOPhaseSync( p_rfic_b, 0 );

                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // Link.Ad9528.RequestSysref(1);
                                // note: we're setup for 8 pulse mode, so only need 1 call                                
                                AD9528_requestSysref( &_clockDevice[p_id->card], 1 );

                                // let's default the phase coherency operation to enabled for now
                                status = fpga_jesd_write_rx_phase_coherent( p_id->card, true );
                                if ( status != 0 )
                                {
                                    return status;
                                }
                                status = fpga_jesd_write_tx_phase_coherent( p_id->card, true );
                                if ( status != 0 )
                                {
                                    return status;
                                }

                                // run init cal
                                if( ((status=TALISE_runInitCals(p_rfic_a, init_cal_mask)) == 0) &&
                                    ((status=TALISE_runInitCals(p_rfic_b, init_cal_mask)) == 0) )
                                {
                                    _skiq_log(SKIQ_LOG_DEBUG, "Init calibration complete (card=%u)\n", p_id->card);
                                    if( ((status=TALISE_waitInitCals(p_rfic_a, INIT_CAL_TIMEOUT_MS, &err_flag)) == 0) &&
                                        ((status=TALISE_waitInitCals(p_rfic_b, INIT_CAL_TIMEOUT_MS, &err_flag)) == 0) )
                                    {
                                        bool check_deframers;

                                        check_deframers = (( _taliseDevice[p_id->card][RF_CHIP_A]->tx.txChannels != TAL_TXOFF ) ||
                                                            ( _taliseDevice[p_id->card][RF_CHIP_B]->tx.txChannels != TAL_TXOFF ));

                                        _skiq_log(SKIQ_LOG_DEBUG, "Wait init cal success (card=%u)!\n", p_id->card);
                                        if( _reset_jesd( &_clockDevice[p_id->card], p_rfic_a, p_rfic_b, check_deframers, NULL ) == 0 )
                                        {
                                            // enable the tracking calibration
                                            if( (TALISE_enableTrackingCals(p_rfic_a, TRACKING_CAL) == 0) &&
                                                (TALISE_enableTrackingCals(p_rfic_b, TRACKING_CAL) == 0) )
                                            {
                                                if( (TALISE_radioOn( p_rfic_a ) == 0) &&
                                                    (TALISE_radioOn( p_rfic_b ) == 0) )
                                                {
                                                    _skiq_log(SKIQ_LOG_DEBUG, "Radio on (card=%u)!\n", p_id->card);
                                                    // default t RX on and TX off for now
                                                    if( (_setRxTxEnable(       p_id,
                                                                               p_rfic_a,
                                                                               TAL_RXOFF_EN,
                                                                               TAL_TXOFF ) == 0) &&
                                                        (_setRxTxEnable(       p_id,
                                                                               p_rfic_b,
                                                                               TAL_RXOFF_EN,
                                                                               TAL_TXOFF ) == 0) )
                                                    {
                                                        _skiq_log(SKIQ_LOG_DEBUG, "Channels enabled, initialization complete!(card=%u)\n", p_id->card);
                                                        // disable continuous sysref
                                                        AD9528_requestSysref( &_clockDevice[p_id->card], 0 );
                                                        AD9528_updateSYSREFMode( &_clockDevice[p_id->card], NSHOT, EIGHT_PULSES );
                                                    }
                                                    else
                                                    {
                                                        skiq_error("Unable to configure channel enables (card=%u)\n", p_id->card);
                                                        status = -ENODEV;
                                                    }
                                                }
                                                else
                                                {
                                                    skiq_error( "Unable to enable radio (card=%u)\n", p_id->card);
                                                    status = -ENODEV;
                                                }
                                            }
                                            else
                                            {
                                                skiq_error( "Unable to enable tracking cal (card=%u)\n", p_id->card);
                                                status = -ENODEV;
                                            }
                                        }
                                        else
                                        {
                                            skiq_error( "Unable to bring up JESD interface (card=%u)\n", p_id->card);
                                            status = -ENODEV;
                                        }
                                    }
                                    else
                                    {
                                        uint32_t calsSincePowerUp;
                                        uint32_t calsLastRun;
                                        uint32_t calsMin;
                                        uint8_t initErrCal;
                                        uint8_t initErrCode;

                                        skiq_error( "Wait init cal failed (0x%x) (0x%x) (card=%u)\n",
                                                  status, err_flag, p_id->card);
                                        TALISE_getInitCalStatus( p_rfic_a,
                                                                 &calsSincePowerUp,
                                                                 &calsLastRun,
                                                                 &calsMin,
                                                                 &initErrCal,
                                                                 &initErrCode );
                                        skiq_error( "Cal A state 0x%x 0x%x 0x%x 0x%x 0x%x\n",
                                                  calsSincePowerUp, calsLastRun, calsMin, initErrCal, initErrCode);
                                        TALISE_getInitCalStatus( p_rfic_b,
                                                                 &calsSincePowerUp,
                                                                 &calsLastRun,
                                                                 &calsMin,
                                                                 &initErrCal,
                                                                 &initErrCode );
                                        skiq_error( "Cal B state 0x%x 0x%x 0x%x 0x%x 0x%x\n",
                                                  calsSincePowerUp, calsLastRun, calsMin, initErrCal, initErrCode);

                                        hal_critical_exit(-1);
                                    }
                                }
                                else
                                {
                                    skiq_error( "Run init cal failed (card=%u)\n", p_id->card);
                                    hal_critical_exit(-1);
                                }
                            }
                        }
                        else
                        {
                            skiq_error( "Unable to configure default PLL (card=%u)\n", p_id->card);
                            status = -3;
                        }
                    }
                    else
                    {
                        skiq_error( "Unable to load ARM (card=%u)\n", p_id->card);
                    }
                }
                else
                {
                    skiq_error( "ARM init failed (card=%u)\n", p_id->card);
                }
            }
            // set the GPIOs for the RFE to output enabled
            if( (_x4_init_3v3_gpio( &rf_id_a, GPIO_RFE_CTRL_OE ) != 0) ||
                (_x4_init_3v3_gpio( &rf_id_b, GPIO_RFE_CTRL_OE ) != 0) )
            {
                skiq_error("Unable to intialize front end filter GPIOs (card=%u)\n", p_id->card);
                status = -EIO;
            }
        }
        else
        {
            skiq_error( "Unable initialize RFIC (card=%u)\n", p_id->card);
        }
    }
    else
    {
        status = -ENODEV;
    }

    if( status == 0 )
    {
        rfic_active[p_id->card] = true;
    }

init_complete:
    return (status);
}


/**************************************************************************************************/
/** The is_profile_valid_for_card() function determines if the specified @a p_profile is valid for
    the referenced @a rf_id_t and the requested sample rate.

    @param[in] p_id Pointer to the RF identification
    @param[in] p_profile Pointer to a Talise profile to consider
    @param[in] request_rate Requested sample rate in samples per second

    @return bool
    @retval true Profile is valid for given card configuration
    @retval false Profile is NOT valid for given card configuration
 */
static bool
is_profile_valid_for_card( rf_id_t *p_id,
                           ad9379_profile_t *p_profile,
                           uint32_t request_rate )
{
    bool is_valid = true;

    /* FPGA bitstreams prior to 3.12.1 cannot handle TX profiles that need dual JESD lanes (those
     * profiles that have TX_IQRATE that *strictly* exceeds the dual lane rate threshold) */
    if ( ( TX_IQRATE_HZ(p_profile) > TX_DUAL_LANE_RATE_THRESHOLD_HZ ) &&
         !_skiq_meets_fpga_version( p_id->card, 3, 12, 1 ) )
    {
        is_valid = false;
        skiq_warning("FPGA version must be at least v3.12.1 to support transmit sample rates above"
                     " %.1f Msps, skipping profile\n",
                     (float)TX_DUAL_LANE_RATE_THRESHOLD_KHZ * RATE_HZ_TO_KHZ);
    }

    return is_valid;
}


/**************************************************************************************************/
/** The find_profile_with_decimation() function considers all of the available profiles to determine
    which one, considering available decimator stages, best satisfies the requested sample rate
    (first priority) and bandwidth (second priority).

    @param[in] p_id Pointer to the RF identification
    @param[in] request_rate Requested sample rate in samples per second
    @param[in] request_bw Requested RF bandwidth in Hertz
    @param[in] nr_stages_available Number of decimator stages available for p_id->hdl, may be 0
    @param[in] look_for_tx_profiles flag to determine which rate/bw of the profile to consider
    @param[out] p_found_profile Details on the profile that meets requested rate / bw
    @param[in] complementary flag restricting profile search to compatible profiles since a related
               handle has already been configured. 
    
    @return int32_t
    @retval 0 Success
    @retval -EINVAL p_found_profile pointer is NULL
    @retval -ERANGE No profile meets both requested rate and bandwidth
 */
static int32_t
find_profile_with_decimation( rf_id_t *p_id,
                              uint32_t request_rate,
                              uint32_t request_bw,
                              uint8_t nr_stages_available,
                              bool look_for_tx_profiles,
                              struct found_profile_t *p_found_profile,
                              bool complementary )
{
    int32_t status = 0;
    ad9379_profile_t *p_profile = NULL;

    uint8_t i=0;
    RESULTS_ARRAY(results, AD9379_NR_PROFILES);

    if ( p_found_profile == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Looking for %s handle %s profile to meet rate=%u, bw=%u for card %u\n",
                    look_for_tx_profiles ? "TX" : "RX", rx_hdl_cstr(p_id->hdl), request_rate,
                    request_bw, p_id->card);
    }

    /* first pass: calculate all of the differences between the profile rates / bandwidths and the
     * requested rate / bandwidth while considering the available decimation stages */
    for (i = 0; ( i < AD9379_NR_PROFILES ) && ( status == 0 ); i++)
    {
        debug_print("Considering profile #%u\n", i);
        p_profile = (ad9379_profile_t *) &(ad9379_profiles[i]);

        /* make sure it's a valid profile before performing additional checking */
        if ( is_profile_valid_for_card( p_id, p_profile, request_rate ) )
        {
            /* find the maximum decimation rate (even if that decimation rate is 0) that provides a
             * sample rate and bandwidth that satisfies the request */
            uint32_t profile_rate, profile_bw;
            bool consider_profile = true;

            /* if a complementary profile is active, then restrict the search to profiles that have the
               same dev_clock and iqrate */
            if (complementary)
            {
                if (DEV_CLK_KHZ(p_profile) != _taliseDevice[p_id->card][rx_hdl_to_other_chip_id[p_id->hdl]]->clocks.deviceClock_kHz)
                {
                    consider_profile = false;
                }
                if (!consider_profile)
                {
                    debug_print("Profile #%u deemed not suitable wrt dev clock mismatch  "
                                "%u,%u\n", i, DEV_CLK_KHZ(p_profile), _taliseDevice[p_id->card][rx_hdl_to_other_chip_id[p_id->hdl]]->clocks.deviceClock_kHz);
                }
                else
                {
                    /* Only consider a profile if the IQRATE is the same as the already configured profile.
                        This restriction may need to be relaxed in the future. */
                    consider_profile = (RX_IQRATE_HZ(p_profile)==_taliseDevice[p_id->card][rx_hdl_to_other_chip_id[p_id->hdl]]->rx.rxProfile.rxOutputRate_kHz*1000);
                    if (!consider_profile)
                    {
                        debug_print("Profile #%u deemed not suitable wrt sample_rate mismatch  "
                                "%u,%u\n", i, RX_IQRATE_HZ(p_profile), _taliseDevice[p_id->card][rx_hdl_to_other_chip_id[p_id->hdl]]->rx.rxProfile.rxOutputRate_kHz*1000);
                    }
                }
            }
            /* sample rate and bandwidth is currently the same in the talise_profile_t type
             * regardless of receive or transmit */
            if ( look_for_tx_profiles )
            {
                profile_rate = TX_IQRATE_HZ(p_profile);
                profile_bw = TX_BW_HZ( p_profile );
            }
            else
            {
                profile_rate = IQRATE_HZ( p_id, p_profile );
                profile_bw = BW_HZ( p_id, p_profile );
            }

            /* if this is a profile where the RX and ORX sample rates differ, check to see if the
             * complementary ORX handle's already been configured to determine if the profile is
             * okay to consider.  This addresses the issue where the Rx LO tuning range is limited
             * on Rx1/2 whenever ORx is configured to a higher sample rate.  So if the ORx hasn't
             * yet been configured, don't allow those profiles to be considered. */
            if ( ORX_IQRATE_HZ( p_profile ) != RX_IQRATE_HZ( p_profile ) )
            {
                rf_id_t comp_rf_id = *p_id;
                uint32_t configured_rate;

                switch ( p_id->hdl )
                {
                    case skiq_rx_hdl_A1:
                    case skiq_rx_hdl_A2:
                        comp_rf_id.hdl = skiq_rx_hdl_C1;
                        break;

                    case skiq_rx_hdl_B1:
                    case skiq_rx_hdl_B2:
                        comp_rf_id.hdl = skiq_rx_hdl_D1;
                        break;

                    case skiq_rx_hdl_C1:
                    case skiq_rx_hdl_D1:
                        /* mark these ORx cases as special because we always want to consider all
                         * available profiles.  set the .hdl to ->hdl so it can be identified down
                         * below. */
                        comp_rf_id.hdl = p_id->hdl;
                        break;

                    default:
                        status = -EINVAL;
                        skiq_error("Internal Error: unhandled skiq_rx_hdl_t (%u) finding decimated "
                                   "profiles on card %u\n", p_id->hdl, p_id->card);
                        break;
                }

                /* only check for complementary handle configuration if the handle is different from
                 * the one provided */
                if ( p_id->hdl != comp_rf_id.hdl )
                {
                    if ( status == 0 )
                    {
                        status = rfic_get_rx_rate_config( &comp_rf_id, &configured_rate, NULL );
                    }

                    if ( status == 0 )
                    {
                        /* If the complementary handle hasn't been configured and RX profiles are
                         * being considered, stick to profiles that have matching sample rates
                         * across all handles to give user the most flexibility without having to
                         * outright configure unused handles.  When TX profiles are being considered,
                         * complementary handle configuration does not impact consideration of
                         * possible TX profiles.
                         *
                         * A concrete example:
                         *
                         * - Profile 1 has 245.76Msps for RX1/2 and 491.52Msps for ORX
                         * - Profile 2 has 245.76Msps for RX1/2 and 245.76Msps for ORX
                         *
                         * If the user has not configured ORX, then Profile 2 should be preferred
                         * since it won't end up restricting the RF PLL tuning range (which looks at
                         * both RX1/2 and ORX bandwidths).
                         */
                        if ( ( configured_rate == 0 ) && !look_for_tx_profiles )
                        {
                            consider_profile = false;
                        }
                    }
                }
            }

            if ( consider_profile )
            {
                debug_print("Considering profile #%u -- %u,%u\n", i, profile_rate, profile_bw);
                rfic_consider_profile( p_id->card, p_id->hdl,
                                       request_rate, request_bw,
                                       profile_rate, profile_bw,
                                       nr_stages_available,
                                       X4_HALF_MAX_SAMPLE_CLK_HZ,
                                       &(results[i]) );
                debug_print("------- Best decimation choice for #%u -- decimation rate %u, "
                            "delta_rate: %d, delta_bw: %d\n", i, results[i].dec_rate,
                            results[i].delta_rate, results[i].delta_bw );
            }
            else
            {
                debug_print("Profile #%u deemed not suitable wrt complementary handle(s) -- "
                            "%u,%u\n", i, profile_rate, profile_bw);
            }
        }
        else
        {
            debug_print("Profile #%u deemed not valid for requested rate %u\n", i, request_rate);
        }
    }

    /* second pass: minimize delta_rate (first) and delta_bw (second) */
    if ( status == 0 )
    {
        uint8_t best_index;

        status = rfic_find_best_result( results, AD9379_NR_PROFILES, &best_index );
        if ( status == 0 )
        {
            p_found_profile->dec_rate = results[best_index].dec_rate;
            p_found_profile->p_profile = (ad9379_profile_t *) &(ad9379_profiles[best_index]);
        }
    }

    return status;
}


static int32_t
find_rx_profile( rf_id_t *p_id,
                 uint32_t request_rate,
                 uint32_t request_bw,
                 struct found_profile_t *p_found_profile,
                 bool complementary )
{
    uint8_t nr_stages = decimator_nr_stages( p_id->card, p_id->hdl );
    skiq_rx_hdl_t hdl;
    rf_id_t id = *p_id;

    /* restrict nr_stages to the minimum across all "configured" handles according to stored
     * sample_rate and bandwidth in rfic_common */
    for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
    {
        id.hdl = hdl;
        if ( rfic_hdl_has_rx_rate_config( &id ) )
        {
            if(!complementary)
            {
                nr_stages = MIN(nr_stages, decimator_nr_stages( id.card, id.hdl ));
            }
        }
    }

    return find_profile_with_decimation( p_id, request_rate, request_bw,
                                         nr_stages,
                                         false, /* don't look for TX profiles */
                                         p_found_profile,
                                         complementary  );
}


static int32_t
find_tx_profile( rf_id_t *p_id,
                 uint32_t request_rate,
                 uint32_t request_bw,
                 struct found_profile_t *p_found_profile,
                 bool complementary )
{
    return find_profile_with_decimation( p_id, request_rate, request_bw,
                                         0,     /* no decimation */
                                         true,  /* look for TX profiles */
                                         p_found_profile,
                                         complementary );
}


/* This function should only be called when the RFIC is first initialized by libsidekiq when
 * enabling a card. */
int32_t _reset_and_init_rfic( rf_id_t *p_id )
{
    int32_t status = 0;
    rf_id_t id = *p_id;

    // make sure to reset our dev clock variable
    _dev_clock[p_id->card] = 0;

    /* zero out the rate_config arrays for the specified RF ID */
    rfic_clear_rate_config( p_id );

    /* set up static _halDevA and _halDevB instances and have _rficDeviceA and_rficDeviceB reference
     * them */
    _halDevA[p_id->card].card = p_id->card;
    _halDevB[p_id->card].card = p_id->card;
    _rficDeviceA[p_id->card].devHalInfo = &(_halDevA[p_id->card]);
    _rficDeviceB[p_id->card].devHalInfo = &(_halDevB[p_id->card]);

    /* set each element of the _tx_config array to a known quantity to use in _cache_config() */
    {
        skiq_tx_hdl_t hdl;

        debug_print("Initializing _tx_config to max attenuation for card %" PRIu8 "\n", p_id->card);
        for ( hdl = skiq_tx_hdl_A1; hdl < skiq_tx_hdl_end; hdl++ )
        {
            _tx_config[p_id->card][hdl].tal_atten_mdB = MAX_TX_MILLI_DB_ATTEN;
        }
    }

    /* set each element of the _rx_config, _rx_cal_mode, and _rx_cal_mask arrays to invalid to use in _cache_config() */
    {
        skiq_rx_hdl_t hdl;

        debug_print("Initializing _rx_config as invalid for card %" PRIu8 "\n", p_id->card);
        for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
        {
            _rx_config[p_id->card][hdl].gain_valid = false;
            _rx_cal_mode[p_id->card][hdl] = skiq_rx_cal_mode_auto;
            _rx_cal_mask[p_id->card][hdl] = SKIQ_RX_CAL_TYPES;
        }
    }

    struct found_profile_t fp;
    for(rf_chip_id_t i=RF_CHIP_A; ((i < RF_CHIP_MAX) && (status == 0)) ; i++)
    {
        //These have to be manually populated since they're hardcoded at a higher layer
        id.chip_id = i;
        id.hdl = (id.chip_id == RF_CHIP_A) ? skiq_rx_hdl_A1 : skiq_rx_hdl_B1;
        //perform allocation and set default parameters
        if (_taliseDevice[p_id->card][i] == NULL)
        {
            // allocate for FIR settings
            status = _talise_profile_alloc(&_taliseDevice[p_id->card][i]);

            if (status != 0)
            {
                return -ENOMEM;
            }

            // copy default profile over (maybe split this into a function?)
            status = _talise_set_default_params(&id, _taliseDevice[p_id->card][i] );

            /* find a profile for the default sample rate and bandwidth */
            if (( status == 0 ) && (i==RF_CHIP_A))    //Skip the profile search for RF_CHIP_B
            {
                status = find_rx_profile( &id, DEFAULT_SAMPLE_RATE, DEFAULT_BANDWIDTH, &fp, false );
            }

            /* store that profile for the Talise device */
            if ( status == 0 )
            {

                status = _talise_set_params_from_profile( &id, fp.p_profile, _taliseDevice[p_id->card][i] );
            }

        }
    }

    // configure default warp
    if ( status == 0 )
    {
        status = _write_warp_voltage( p_id, DEFAULT_WARP_VOLTAGE );
    }

    if ( status == 0 )
    {
        status = _reset_and_init( p_id );
    }

    if ( status == 0 )
    {
        _init_min_max_sample_rate(p_id->card);
    }

    return status;
}


void _display_version_info( void )
{
    skiq_info( "RF IC driver version %u.%u.%u.%u-%u\n",
               TAL_CURRENT_SI_VERSION,
               TAL_CURRENT_MAJOR_VERSION,
               TAL_CURRENT_MINOR_VERSION,
               TAL_CURRENT_BUILD_VERSION,
               TAL_CURRENT_SUBMODULE_VERSION );
}

int32_t _release( rf_id_t *p_id )
{
    int32_t status=0;
    taliseDevice_t *p_rfic_a = NULL;
    taliseDevice_t *p_rfic_b = NULL;
    rf_id_t rf_id_b;

    /* zero out the rate_config arrays for the specified card index */
    rfic_clear_rate_config( p_id );

    // This is a bit hackey, but we're assuming that we need to init B in the same
    // call, so we're going to copy the rf_id_t data structure and make sure the hdl is B
    memcpy(&rf_id_b, p_id, sizeof(rf_id_t));
    rf_id_b.hdl = skiq_rx_hdl_B1;

    p_rfic_a = rf_id_to_rficDevice( p_id );
    p_rfic_b = rf_id_to_rficDevice( &rf_id_b );

    _skiq_log(SKIQ_LOG_DEBUG, "Turning off the radio (card=%u)\n", p_id->card);
    TALISE_radioOff(p_rfic_a);
    TALISE_radioOff(p_rfic_b);

    _skiq_log(SKIQ_LOG_DEBUG, "Disabling RX and TX channels prior to shutting off the RF IC (card=%u)\n", p_id->card);
    status = _enable_rx_chan_all(p_id, false);
    if(status != 0)
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to disable RX channels on card %u\n", p_id->card);
        return status;
    }   
    status = _enable_tx_chan_all(p_id, false);
    if(status != 0)
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to disable TX channels on card %u\n", p_id->card);
        return status;
    }
    _skiq_log(SKIQ_LOG_DEBUG, "RX and TX channel disable complete (card=%u)\n", p_id->card);

    _skiq_log(SKIQ_LOG_DEBUG, "Shutting off the RF IC (card=%u)\n", p_id->card);
    TALISE_shutdown( p_rfic_a );
    TALISE_shutdown( p_rfic_b );
    _skiq_log(SKIQ_LOG_DEBUG, "RF IC shutdown complete (card=%u)\n", p_id->card);

    _talise_profile_free( &(_p_user_taliseDevice[p_id->card]) );
    _talise_profile_free( &(_taliseDevice[p_id->card][RF_CHIP_A]) );
    _talise_profile_free( &(_taliseDevice[p_id->card][RF_CHIP_B]) );


    // free previously alloced mem
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].spiSettings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll1Settings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].pll2Settings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].outputSettings );
    FREE_IF_NOT_NULL( _clockDevice[p_id->card].sysrefSettings );

    rfic_active[p_id->card] = false;

    return (status);
}

int32_t _write_rx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;
    uint64_t cal_freq;
    taliseRxORxChannels_t calRxChan;
    taliseTxChannels_t calTxChan;

    p_rfic = rf_id_to_rficDevice( p_id );

    status = _write_freq( p_id, p_rfic, freq, p_act_freq );

    if( status == 0 )
    {
        // determine if we need to run cal
        if( _is_cal_required( p_id,
                              false,
                              (uint64_t)(*p_act_freq),
                              &cal_freq,
                              &calRxChan,
                              &calTxChan ) == true )
        {
            status = _run_cal( p_id, p_rfic, cal_freq, calRxChan, calTxChan );
        }
    }

    return (status);
}

int32_t _write_tx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;
    uint64_t cal_freq;
    taliseRxORxChannels_t calRxChan;
    taliseTxChannels_t calTxChan;

    p_rfic = rf_id_to_rficDevice( p_id );

    status = _write_freq( p_id, p_rfic, freq, p_act_freq );

    if( status == 0 )
    {
        // determine if we need to run cal
        if( _is_cal_required( p_id,
                              true,
                              (uint64_t)(*p_act_freq),
                              &cal_freq,
                              &calRxChan,
                              &calTxChan ) == true )
        {
            status = _run_cal( p_id, p_rfic, cal_freq, calRxChan, calTxChan );
        }
    }

    return (status);
}

int32_t _write_freq( rf_id_t *p_id, taliseDevice_t *p_rfic, uint64_t freq, double *p_act_freq )
{
    int32_t status=0;
    uint16_t bandwidth_kHz=0;
    uint8_t stability=0;
    bool update_bw = false;
    uint8_t pll_lock = 0;
    uint32_t tal_status=0;
    uint64_t freq_remainder=0;
    uint64_t freq_adjusted=freq;
    static bool display_freq_warning = true;

    /////////////////////////////////////
    // TODO: identify a "real" resolution
    // HACKY WORKAROUND:
    // It seems as though we can't tune the PLL to
    // certain frequencies.  When the error occurs, the
    // only advertised way to work around the issue is to reset
    // the ARM, which is an involved process.  To avoid
    // entering this error condition, we'll make sure that we
    // tune on 1kHz boundaries since that seems to work.
    // Ultimately, we need to follow up with ADI to determine
    // what the preferred resolution is.
    if( (freq_remainder=(freq % LO_FREQ_MIN_RESOLUTION_HZ)) != 0 )
    {
        // we need to adjust the freq to be on a 1kHz boundary
        if( (LO_FREQ_MIN_RESOLUTION_HZ / 2) <= freq_remainder )
        {
            freq_adjusted = (freq + LO_FREQ_MIN_RESOLUTION_HZ) - freq_remainder;
        }
        else
        {
            freq_adjusted = freq  - freq_remainder;
        }
        if( display_freq_warning == true )
        {
            skiq_info("Minimum LO resolution is %" PRIu32 " configuring actual frequency to %"
                      PRIu64 " (card=%u)\n", LO_FREQ_MIN_RESOLUTION_HZ, freq_adjusted, p_id->card);
            display_freq_warning = false;
        }
    }
    /////////////////////////////////////

    // turn the radio off
    if( TALISE_radioOff(p_rfic) == 0 )
    {
        // figure out if we need to change our loop BW setting
        TALISE_getPllLoopFilter( p_rfic,
                                 TAL_RF_PLL,
                                 &bandwidth_kHz,
                                 &stability );
        if( (freq_adjusted <= LOOP_FILTER_LOW_MAX_FREQ) &&
            (bandwidth_kHz != LOOP_FILTER_BW_KHZ_LOW) )
        {
            update_bw = true;
            bandwidth_kHz = LOOP_FILTER_BW_KHZ_LOW;
        }
        else if( (freq_adjusted > LOOP_FILTER_LOW_MAX_FREQ) &&
                 (bandwidth_kHz != LOOP_FILTER_BW_KHZ_HIGH) )
        {
            update_bw = true;
            bandwidth_kHz = LOOP_FILTER_BW_KHZ_HIGH;
        }
        if( update_bw == true )
        {
            if( TALISE_setRfPllLoopFilter(p_rfic,
                                          bandwidth_kHz,
                                          stability) != 0 )
            {
                skiq_error( "Unable to update loop filter bandwidth (card=%u)\n", p_id->card);
                status = -EINVAL;
                goto freq_retune_complete;
            }
        }

        // set the new LO freq
        if( (tal_status=TALISE_setRfPllFrequency( p_rfic, TAL_RF_PLL, freq_adjusted )) == 0 )
        {
            // make sure it got a lock
            TALISE_getPllsLockStatus( p_rfic, &pll_lock );
            if( (pll_lock & TAL_RX_ONLY_PLL_LOCK) != TAL_RX_ONLY_PLL_LOCK )
            {
                status = -EINVAL;
                skiq_error( "Unable to get a PLL lock (card=%u)\n", p_id->card);
                goto freq_retune_complete;
            }
        }
        else
        {
            status = -EINVAL;
            skiq_error( "Unable to configure the PLL freq, %u (card=%u)\n",
                      tal_status, p_id->card );
            goto freq_retune_complete;
        }
        // turn the radio back on
        if( TALISE_radioOn( p_rfic ) != 0 )
        {
            status = -ENODEV;
            skiq_error("Unable to turn radio back on (card=%u)\n", p_id->card);
            goto freq_retune_complete;
        }
    }
    else
    {
        status = -ENODEV;
        goto freq_retune_complete;
    }

freq_retune_complete:
    if( status == 0 )
    {
        *p_act_freq = (double)(freq_adjusted);
    }

    return (status);
}

int32_t _write_rx_spectrum_invert( rf_id_t *p_id, bool invert )
{
    int32_t status=-1;

    // no way to invert it...return success if not inverted
    if( invert == false )
    {
        status = 0;
    }

    return (status);
}

int32_t _write_tx_spectrum_invert( rf_id_t *p_id, bool invert )
{
    int32_t status=-1;

    // no way to invert it...return success if not inverted
    if( invert == false )
    {
        status = 0;
    }

    return (status);
}

int32_t _write_tx_attenuation( rf_id_t *p_id, uint16_t atten )
{
    int32_t status=0;
    taliseTxChannels_t txChan;
    uint16_t atten_mdB=0;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (p_id->hdl == skiq_tx_hdl_A1) ||
        (p_id->hdl == skiq_tx_hdl_B1) )
    {
        txChan = TAL_TX1;
    }
    else if( (p_id->hdl == skiq_tx_hdl_A2) ||
             (p_id->hdl == skiq_tx_hdl_B2) )
    {
        txChan = TAL_TX2;
    }
    else
    {
        status = -ENODEV;
    }

    if( status == 0 )
    {
        // Convert quarter dB to milli dB.
        atten_mdB = (((float) atten) / 4.0) * 1000;

        if( TALISE_setTxAttenuation( p_rfic, txChan, atten_mdB ) != 0 )
        {
            skiq_error( "Unable to configure TX attenuation (card=%u)\n", p_id->card);
            status = -EINVAL;
        }
        else
        {
            _tx_config[p_id->card][p_id->hdl].tal_atten_mdB = atten_mdB;
        }
    }

    return (status);
}

int32_t _read_tx_attenuation( rf_id_t *p_id, uint16_t *p_atten )
{
    int32_t status=0;
    taliseTxChannels_t txChan;
    uint16_t atten_mdB=0;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (p_id->hdl == skiq_tx_hdl_A1) ||
        (p_id->hdl == skiq_tx_hdl_B1) )
    {
        txChan = TAL_TX1;
    }
    else if( (p_id->hdl == skiq_tx_hdl_A2) ||
             (p_id->hdl == skiq_tx_hdl_B2) )
    {
        txChan = TAL_TX2;
    }
    else
    {
        status = -ENODEV;
    }

    if( status == 0 )
    {
        if( TALISE_getTxAttenuation( p_rfic, txChan, &atten_mdB ) != 0 )
        {
            skiq_error( "Unable to read TX attenuation (card=%u)\n", p_id->card);
            status = -EINVAL;
        }
        else
        {
            // Convert milli dB to quarter dB
            *p_atten = atten_mdB / 250;
        }
    }

    return (status);
}

int32_t _read_tx_attenuation_range( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min )
{
    int32_t status=0;

    *p_max = MAX_TX_QUARTER_DB_ATTEN;
    *p_min = MIN_TX_QUARTER_DB_ATTEN;

    return (status);
}

int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode )
{
    int32_t status=0;
    taliseGainMode_t gainMode = TAL_MGC;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( gain_mode == skiq_rx_gain_auto )
    {
        // TODO: Fast not supported by talise_rx.c currently, so set to slow
        gainMode = TAL_AGCSLOW;//TAL_AGCFAST; // this matches the mode implemented in the 9361
        // TODO: support other modes?
    }
    if( TALISE_setRxGainControlMode( p_rfic, gainMode ) != 0 )
    {
        status = -EINVAL;
    }

    return (status);
}

int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode )
{
    int32_t status=0;
    taliseGainMode_t gainMode;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    gainMode = p_rfic->devStateInfo.gainMode;
    if( gainMode == TAL_MGC )
    {
        *p_gain_mode = skiq_rx_gain_manual;
    }
    else
    {
        *p_gain_mode = skiq_rx_gain_auto;
    }

    return (status);
}

int32_t _read_rx_gain_range( rf_id_t *p_id, uint8_t *p_gain_max, uint8_t *p_gain_min )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (p_id->hdl == skiq_rx_hdl_A2) ||
        (p_id->hdl == skiq_rx_hdl_B2) )
    {
        *p_gain_min = p_rfic->devStateInfo.gainIndexes.rx1MinGainIndex;
        *p_gain_max = p_rfic->devStateInfo.gainIndexes.rx1MaxGainIndex;
    }
    else if( (p_id->hdl == skiq_rx_hdl_A1) ||
             (p_id->hdl == skiq_rx_hdl_B1) )
    {
        *p_gain_min = p_rfic->devStateInfo.gainIndexes.rx2MinGainIndex;
        *p_gain_max = p_rfic->devStateInfo.gainIndexes.rx2MaxGainIndex;
    }
    else if( p_id->hdl == skiq_rx_hdl_C1 ||
             p_id->hdl == skiq_rx_hdl_D1 )
    {
        *p_gain_min = p_rfic->devStateInfo.gainIndexes.orx1MinGainIndex;
        *p_gain_max = p_rfic->devStateInfo.gainIndexes.orx1MaxGainIndex;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _write_rx_gain( rf_id_t *p_id, uint8_t gain )
{
    int32_t status=0;
    taliseRxChannels_t rxChan;
    taliseDevice_t *p_rfic = NULL;
    bool b_obs_rx = false;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (p_id->hdl == skiq_rx_hdl_A2) ||
        (p_id->hdl == skiq_rx_hdl_B2) )
    {
        rxChan = TAL_RX1;
    }
    else if( (p_id->hdl == skiq_rx_hdl_A1) ||
             (p_id->hdl == skiq_rx_hdl_B1) )
    {
        rxChan = TAL_RX2;
    }
    else if( (p_id->hdl == skiq_rx_hdl_C1) ||
             (p_id->hdl == skiq_rx_hdl_D1) )
    {
        b_obs_rx = true;
        if( TALISE_setObsRxManualGain(p_rfic, TAL_ORX1, gain) != 0 )
        {
            status = -EINVAL;
        }
    }
    else
    {
        status = -ENODEV;
    }

    if( (status == 0) && (b_obs_rx == false) )
    {
        if( TALISE_setRxManualGain(p_rfic, rxChan, gain) != 0 )
        {
            status = -EINVAL;
        }
    }

    if ( status == 0 )
    {
        _rx_config[p_id->card][p_id->hdl] = (tal_rx_config_t) {
            .tal_gain = gain,
            .gain_valid = true,
        };
    }

    return (status);
}

int32_t _read_rx_gain( rf_id_t *p_id, uint8_t *p_gain )
{
    int32_t status=0;
    taliseRxChannels_t rxChan;
    taliseDevice_t *p_rfic = NULL;
    bool b_obs_rx = false;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (p_id->hdl == skiq_rx_hdl_A2) ||
        (p_id->hdl == skiq_rx_hdl_B2) )
    {
        rxChan = TAL_RX1;
    }
    else if( (p_id->hdl == skiq_rx_hdl_A1) ||
             (p_id->hdl == skiq_rx_hdl_B1) )
    {
        rxChan = TAL_RX2;
    }
    else if( (p_id->hdl == skiq_rx_hdl_C1) ||
             (p_id->hdl == skiq_rx_hdl_D1) )
    {
        b_obs_rx = true;
        if( TALISE_getObsRxGain(p_rfic, TAL_ORX1, p_gain) != 0 )
        {
            status = -EINVAL;
        }
    }
    else
    {
        status = -ENODEV;
    }

    if( (status == 0) && (b_obs_rx == false) )
    {
        if( TALISE_getRxGain(p_rfic, rxChan, p_gain) != 0 )
        {
            status = -EINVAL;
        }
    }

    return (status);
}

int32_t _read_adc_resolution( uint8_t *p_adc_res )
{
    int32_t status=0;

    *p_adc_res = AD9379_ADC_RES;

    return (status);
}

int32_t _read_dac_resolution( uint8_t *p_dac_res )
{
    int32_t status=0;

    *p_dac_res = AD9379_DAC_RES;

    return (status);
}

int32_t _read_warp_voltage( rf_id_t *p_id, uint16_t *p_warp_voltage )
{
    int32_t status=0;

    if ( !rfic_active[p_id->card] )
    {
        status = -ENODEV;
    }
    
    if ( status == 0 )
    {
        // the DAC doesn't support reading, so we'll just return the cached value
        *p_warp_voltage = _warp_voltage[p_id->card];
    }

    return (status);
}

int32_t _write_warp_voltage( rf_id_t *p_id, uint16_t warp_voltage )
{
    int32_t status=0;

    if( (warp_voltage >= MIN_WARP_VOLTAGE) &&
        (warp_voltage <= MAX_WARP_VOLTAGE) )
    {
        status = hal_spi_dac_write( p_id->card, warp_voltage );
        // cache the value
        if( status == 0 )
        {
            _warp_voltage[p_id->card] = warp_voltage;
        }
    }
    else
    {
        skiq_error( "Warp voltage must be in the range of %u-%u (card=%u), specified %u\n",
                    MIN_WARP_VOLTAGE, MAX_WARP_VOLTAGE, p_id->card, warp_voltage );
        status = -EINVAL;
    }

    return (status);
}

int32_t _enable_rx_chan( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;
    taliseTxChannels_t txChan;
    taliseRxORxChannels_t rxChan;
    taliseRxORxChannels_t updateRxChan;
    taliseDevice_t *p_rfic = NULL;
    uint64_t lo_freq;
    uint64_t cal_freq=0;

    if( (p_id->hdl) > skiq_rx_hdl_D1 )
    {
        skiq_error( "Unsupported RX handle %u (card=%u)\n", p_id->hdl, p_id->card);
        return -ENODEV;
    }

    p_rfic = rf_id_to_rficDevice( p_id );

    // For now, we can run with either Rx1/2 enabled or ORx1
    // other configurations may be supported in future ADI releases

    if( _getRxTxEnable(p_id, p_rfic, &rxChan, &txChan) == 0 )
    {
        // let's always have receive channels enabled...a new enable request will result in the channels being overwritten
        if( enable == true )
        {
            updateRxChan = rx_hdl_to_chan_en[p_id->hdl];
        }

        if( (status == 0) && (enable == true) )
        {
            if( (updateRxChan & rxChan) != updateRxChan )
            {
                if( txChan != TAL_TXOFF )
                {
                    skiq_info("Requested RX channel, TX now disabled (card=%u)\n", p_id->card);
                    txChan = TAL_TXOFF;
                }

                if(_setRxTxEnable( p_id, p_rfic, updateRxChan, txChan) !=0 )
                {
                    skiq_error( "Unable to enable channels (card=%u)\n", p_id->card);
                    status = -EINVAL;
                }

                // determine if need to recal...don't cal if we're hopping
                if( (status == 0) &&
                    (_freq_hop_config[p_id->card][rx_hdl_to_chip_id[p_id->hdl]].tune_mode == skiq_freq_tune_mode_standard) )
                {
                    if( TALISE_getRfPllFrequency(p_rfic, TAL_RF_PLL, &lo_freq) == 0 )
                    {
                        if( _is_cal_required( p_id,
                                              false,
                                              lo_freq,
                                              &cal_freq,
                                              &rxChan,
                                              &txChan ) == true )
                        {
                            status = _run_cal( p_id, p_rfic, cal_freq, updateRxChan, txChan );
                        }
                    }
                }
            }
        }
    }
    else
    {
        skiq_error( "Unable to read currently enabled channels (card=%u)\n", p_id->card);
        status = -ENODEV;
    }
    return (status);
}

int32_t _enable_rx_chan_all( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;
    skiq_rx_hdl_t hdl;
    rf_id_t id = *p_id;

    for( hdl = skiq_rx_hdl_A1; (hdl < skiq_rx_hdl_end) && (status == 0); hdl++ )
    {
        id.hdl = hdl;
        status = _enable_rx_chan(&id, enable);
        if (status != 0)
        {
            skiq_error("Unable to update all RX channels on Sidekiq %s, failed to update handle %s"
                " on card %u with status %" PRIi32 "\n",
                part_cstr(_skiq_get_part(id.card)), rx_hdl_cstr(hdl), id.card, status);
        }
    }

    return status;
}

int32_t _enable_tx_chan( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;
    taliseTxChannels_t txChan;
    taliseRxORxChannels_t rxChan;
    taliseDevice_t *p_rfic = NULL;
    uint64_t lo_freq;
    uint64_t cal_freq;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( _getRxTxEnable( p_id, p_rfic, &rxChan, &txChan) == 0 )
    {
        if( enable == true )
        {
            switch( p_id->hdl )
            {
                case skiq_tx_hdl_A1:
                    // intentional fall-thru
                case skiq_tx_hdl_B1:
                    txChan |= TAL_TX1;
                    break;

                case skiq_tx_hdl_A2:
                    // intentional fall-thru
                case skiq_tx_hdl_B2:
                    txChan |= TAL_TX2;
                    break;

                    // TODO: other channels
                default:
                    skiq_error("Unsupported TX handle %u (card=%u)\n", p_id->hdl, p_id->card);
                    status = -ENODEV;
                    break;
            }
            // we need to turn off RX if it's on
            if( rxChan != TAL_RXOFF_EN )
            {
                skiq_warning("Disabling RX as TX has been requested to be enabled (card=%u)\n",
                             p_id->card);
                rxChan = TAL_RXOFF_EN;
            }
        }
        else
        {
            switch( p_id->hdl )
            {
                case skiq_tx_hdl_A1:
                    // intentional fall-thru
                case skiq_tx_hdl_B1:
                    txChan &= ~TAL_TX1;
                    break;

                case skiq_tx_hdl_A2:
                    // intentional fall-thru
                case skiq_tx_hdl_B2:
                    txChan &= ~TAL_TX2;
                    break;

                    // TODO: other channels
                default:
                    skiq_error("Unsupported TX handle %u (card=%u)\n", p_id->hdl, p_id->card);
                    status = -ENODEV;
                    break;
            }
        }
        if( status == 0 )
        {
            if( _setRxTxEnable( p_id, p_rfic, rxChan, txChan) !=0 )
            {
                skiq_error( "Unable to update channels (card=%u)\n", p_id->card);
                status = -EINVAL;
            }
            else
            {
                // determine if need to recal
                if( enable == true )
                {
                    if( TALISE_getRfPllFrequency(p_rfic, TAL_RF_PLL, &lo_freq) == 0 )
                    {
                        if( _is_cal_required( p_id,
                                              true,
                                              lo_freq,
                                              &cal_freq,
                                              &rxChan,
                                              &txChan ) == true )
                        {
                            status = _run_cal( p_id, p_rfic, lo_freq, rxChan, txChan );
                        }
                    }
                }
            }
        }
    }
    else
    {
        skiq_error( "Unable to read currently enabled channels (card=%u)\n", p_id->card);
        status = -ENODEV;
    }

    return (status);
}

int32_t _enable_tx_chan_all( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;
    skiq_tx_hdl_t hdl;
    rf_id_t id = *p_id;

    for( hdl = skiq_tx_hdl_A1; (hdl < skiq_tx_hdl_end) && (status == 0); hdl++ )
    {
        id.hdl = hdl;
        status = _enable_tx_chan(&id, enable);
        if (status != 0)
        {
            skiq_error("Unable to update all TX channels on Sidekiq %s, failed to update handle %s"
                " on card %u with status %" PRIi32 "\n",
                part_cstr(_skiq_get_part(id.card)), tx_hdl_cstr(hdl), id.card, status);
        }
    }

    return status;
}

int32_t _tx_test_tone_actual_freq( uint32_t tx_sample_rate_kHz, int32_t test_tone_kHz )
{
    int32_t nco_tune_word = 0;
    int32_t actual_freq_kHz = 0;

    // NOTE: ADI does not provide us with the ability to read the "actual" TX frequency,
    // so we need to perform the inverse of the calculation that they perform to enable the
    // test tone.  This code is the inverse of TALISE_enableTxNco() in talise_tx.c.  If
    // we receive a new release from ADI for the AD9379, we need to be sure to ensure that
    // this code is updated if necessary
    nco_tune_word = (int32_t)(((((int64_t)test_tone_kHz << 20) /
                                (tx_sample_rate_kHz >> 1)) + 1) >> 1) * -1;
    actual_freq_kHz = (-1 * (int32_t)(((((int64_t)nco_tune_word << 1) - 1) *
                                       ((int64_t)tx_sample_rate_kHz >> 1)) >> 20));

    return (actual_freq_kHz);
}

int32_t _config_tx_test_tone( rf_id_t *p_id, bool enable )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;
    taliseTxNcoTestToneCfg_t test_tone_config;
    tx_test_tone_t *p_test_tone;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (p_id->hdl == skiq_tx_hdl_A1) ||
        (p_id->hdl == skiq_tx_hdl_A2) )
    {
        p_test_tone = &_tx_test_toneA[p_id->card];
    }
    else if( (p_id->hdl == skiq_tx_hdl_B1) ||
             (p_id->hdl == skiq_tx_hdl_B2) )
    {
        p_test_tone = &_tx_test_toneB[p_id->card];
    }
    else
    {
        skiq_error( "Invalid handle (%u) specified in configuring the TX test tone (card=%u)\n", p_id->hdl, p_id->card);
        status = -ENODEV;
    }

    if( status == 0 )
    {
        // copy over the test tone parameters
        if( enable )
        {
            test_tone_config.enable = 1;
        }
        else
        {
            test_tone_config.enable = 0;
        }
        test_tone_config.tx1ToneFreq_kHz = p_test_tone->tx1_freq_offset*RATE_HZ_TO_KHZ;
        test_tone_config.tx2ToneFreq_kHz = p_test_tone->tx2_freq_offset*RATE_HZ_TO_KHZ;

        if( TALISE_enableTxNco( p_rfic,
                                &test_tone_config ) != 0 )
        {
            status = -EINVAL;
        }
        else
        {
            // success, save off the update
            p_test_tone->enable = enable;
        }
    }

    return (status);
}

int32_t _read_tx_test_tone_freq( rf_id_t *p_id,
                                 int32_t *p_test_freq_offset )
{
    int32_t status=0;
    int32_t config_freq_offset_Hz = 0;
    int32_t config_freq_offset_kHz = 0;

    switch( p_id->hdl )
    {
        case skiq_tx_hdl_A1:
            config_freq_offset_Hz = _tx_test_toneA[p_id->card].tx1_freq_offset;
            break;

        case skiq_tx_hdl_A2:
            config_freq_offset_Hz = _tx_test_toneA[p_id->card].tx2_freq_offset;
            break;

        case skiq_tx_hdl_B1:
            config_freq_offset_Hz = _tx_test_toneB[p_id->card].tx1_freq_offset;
            break;

        case skiq_tx_hdl_B2:
            config_freq_offset_Hz = _tx_test_toneB[p_id->card].tx2_freq_offset;
            break;

        default:
            skiq_error("Unsupported hdl %u specified (card=%u)\n", p_id->hdl, p_id->card);
            status = -ENODEV;
            break;
    }

    if( status == 0 )
    {
        config_freq_offset_kHz = config_freq_offset_Hz*RATE_HZ_TO_KHZ;
        *p_test_freq_offset =
            (_tx_test_tone_actual_freq( _taliseDevice[p_id->card][tx_hdl_to_chip_id[p_id->hdl]]->tx.txProfile.txInputRate_kHz,
                                        config_freq_offset_kHz ))*RATE_KHZ_TO_HZ;
    }

    return (status);
}

int32_t _write_tx_test_tone_freq( rf_id_t *p_id,
                                  int32_t test_freq_offset )
{
    int32_t status=0;
    uint64_t lo_freq=0;
    taliseDevice_t *p_rfic = NULL;
    taliseTxNcoTestToneCfg_t test_tone_config;
    tx_test_tone_t *p_test_tone;
    int32_t *p_test_freq = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( TALISE_getRfPllFrequency(p_rfic, TAL_RF_PLL, &lo_freq) == 0 )
    {
        switch( p_id->hdl )
        {
            case skiq_tx_hdl_A1:
                p_test_tone = &(_tx_test_toneA[p_id->card]);
                test_tone_config.tx1ToneFreq_kHz = test_freq_offset*RATE_HZ_TO_KHZ;
                test_tone_config.tx2ToneFreq_kHz =
                    _tx_test_toneA[p_id->card].tx2_freq_offset*RATE_HZ_TO_KHZ;
                p_test_freq = &(_tx_test_toneA[p_id->card].tx1_freq_offset);
                break;

            case skiq_tx_hdl_A2:
                p_test_tone = &(_tx_test_toneA[p_id->card]);
                test_tone_config.tx1ToneFreq_kHz =
                    _tx_test_toneA[p_id->card].tx1_freq_offset*RATE_HZ_TO_KHZ;
                test_tone_config.tx2ToneFreq_kHz = test_freq_offset*RATE_HZ_TO_KHZ;
                p_test_freq = &(_tx_test_toneA[p_id->card].tx2_freq_offset);
                break;

            case skiq_tx_hdl_B1:
                p_test_tone = &(_tx_test_toneB[p_id->card]);
                test_tone_config.tx1ToneFreq_kHz = test_freq_offset*RATE_HZ_TO_KHZ;
                test_tone_config.tx2ToneFreq_kHz =
                    _tx_test_toneB[p_id->card].tx2_freq_offset*RATE_HZ_TO_KHZ;
                p_test_freq = &(_tx_test_toneB[p_id->card].tx1_freq_offset);
                break;

            case skiq_tx_hdl_B2:
                p_test_tone = &(_tx_test_toneB[p_id->card]);
                test_tone_config.tx1ToneFreq_kHz =
                    _tx_test_toneB[p_id->card].tx1_freq_offset*RATE_HZ_TO_KHZ;
                test_tone_config.tx2ToneFreq_kHz = test_freq_offset*RATE_HZ_TO_KHZ;
                p_test_freq = &(_tx_test_toneB[p_id->card].tx2_freq_offset);
                break;

            default:
                skiq_error("Invalid handle %s specified in configuring the TX test tone (card=%u)\n",
                           tx_hdl_cstr(p_id->hdl), p_id->card);
                status = -ENODEV;
                break;
        }

        if( status == 0 )
        {
            // copy over the test tone parameters
            if( p_test_tone->enable == true )
            {
                test_tone_config.enable = 1;
            }
            else
            {
                test_tone_config.enable = 0;
            }
            if( TALISE_enableTxNco(p_rfic, &test_tone_config) == 0 )
            {
                // save the updated config
                *p_test_freq = test_freq_offset;
            }
            else
            {
                skiq_error("Unable to update TX test tone to %d Hz, handle %s (card=%u)\n",
                           *p_test_freq, tx_hdl_cstr(p_id->hdl), p_id->card);
                status = -EINVAL;
            }
        }
    }
    else
    {
        skiq_error("Unable to obtain RF PLL frequency when updating offset (card=%u)\n", p_id->card);
        status = -ENODEV;
    }

    return (status);
}

void _read_min_rx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_min_rx_sample_rate )
{
    uint32_t nr_stages = decimator_nr_stages( p_id->card, p_id->hdl );

    if (IS_ORX_HDL(p_id->hdl))
    {
        *p_min_rx_sample_rate = ORX_IQRATE(_p_min_orx_profile) >> nr_stages;
    }
    else
    {
        *p_min_rx_sample_rate = RX_IQRATE(_p_min_rx_profile) >> nr_stages;
    }
    *p_min_rx_sample_rate *= RATE_KHZ_TO_HZ;
}

void _read_max_rx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_max_rx_sample_rate )
{
    // check for orx handle
    if (IS_ORX_HDL(p_id->hdl))
    {
        /* all Sidekiq X4 FPGAs support dual lane ORX */
        *p_max_rx_sample_rate = ORX_IQRATE(_p_max_dual_lane_orx_profile);
    }
    else
    {
        *p_max_rx_sample_rate = RX_IQRATE(_p_max_rx_profile);
    }
    *p_max_rx_sample_rate *= RATE_KHZ_TO_HZ;
}

void _read_min_tx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_min_tx_sample_rate )
{
    *p_min_tx_sample_rate = TX_IQRATE(_p_min_tx_profile) * RATE_KHZ_TO_HZ;
}

void _read_max_tx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_max_tx_sample_rate )
{
    *p_max_tx_sample_rate = TX_IQRATE(_p_max_tx_profile) * RATE_KHZ_TO_HZ;
}

int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    return (_write_rx_sample_rate_and_bandwidth_multi(p_id,&(p_id->hdl), 1, &rate, &bandwidth));
}

int cmp_sr_bw_params_t(const void *p, const void *q)
{
    uint32_t l = ((sr_bw_params_t *)p)->rate;
    uint32_t r = ((sr_bw_params_t *)q)->rate;
    return ((int)((int)r-(int)l));
}

int32_t _write_rx_sample_rate_and_bandwidth_multi( rf_id_t *p_id, skiq_rx_hdl_t handles[],
                                                          uint8_t nr_handles,
                                                          uint32_t rate[],
                                                          uint32_t bandwidth[])
{
    int32_t status = 0;
    struct found_profile_t fp;
    ad9379_profile_t *p_profile;
    tal_chip_config_t chipA, chipB;
    bool complementary = false;
    rf_id_t id = *p_id;
    ARRAY_WITH_DEFAULTS(uint32_t, nr_stages, skiq_rx_hdl_end, 0);
    ARRAY_WITH_DEFAULTS(sr_bw_params_t, req_hdls, skiq_rx_hdl_end, SR_BW_PARAMS_T_INITIALIZER);

    /* higher layers ensure rx_handles are valid 
      and clamp bandwidth to be at most the sample rate. */

    /* Store inputs into a struct since we want to reorder
       and maintain the relationship */
    for(uint8_t i=0; i<nr_handles; i++)
    {
        req_hdls[i].bandwidth = bandwidth[i];
        req_hdls[i].rate = rate[i];
        req_hdls[i].hdl = handles[i];
    }

    //Reorder handles by highest sample rate
    if (status == 0)
    {
        qsort(req_hdls, nr_handles, sizeof(sr_bw_params_t),cmp_sr_bw_params_t);
    }

    for(uint8_t i=0; ((i<nr_handles) && (status == 0)); i++)
    {
        complementary = false;
        rf_id_t tmp_id_rx1 = *p_id;
        rf_id_t tmp_id_rx2 = *p_id;
        skiq_rx_hdl_t curr_hdl = req_hdls[i].hdl;
        nr_stages[curr_hdl]= decimator_nr_stages( p_id->card, curr_hdl );

        /* Both RFICs share the same dev_clock, so check the "other" (see rx_hdl_to_other_chip_id) handles to see if they've 
           been configured. If so, restrict the profile search to only include complementary profiles (the same DEV_CLOCK and RX_IQRATE) and
           use the decimator to satisfy the requested rate */  

        switch(curr_hdl)
        {
            case skiq_rx_hdl_A1:
            case skiq_rx_hdl_A2:
                tmp_id_rx1.hdl = skiq_rx_hdl_B1;
                tmp_id_rx2.hdl = skiq_rx_hdl_B2;
                if ((rfic_hdl_has_rx_rate_config(&tmp_id_rx1) || rfic_hdl_has_rx_rate_config(&tmp_id_rx2)) && (nr_stages[curr_hdl]>0)) 
                {
                    complementary = true;
                    skiq_debug("Rate configured on RFIC B, considering complementary profile. ");
                }
                break;
            case skiq_rx_hdl_B1:
            case skiq_rx_hdl_B2:
                tmp_id_rx1.hdl = skiq_rx_hdl_A1;
                tmp_id_rx2.hdl = skiq_rx_hdl_A2;
                if ((rfic_hdl_has_rx_rate_config(&tmp_id_rx1) || rfic_hdl_has_rx_rate_config(&tmp_id_rx2)) && (nr_stages[curr_hdl]>0)) 
                {
                    complementary = true;
                }
                break;
            case skiq_rx_hdl_C1:
                tmp_id_rx1.hdl = skiq_rx_hdl_D1;
                if (rfic_hdl_has_rx_rate_config(&tmp_id_rx1) )
                {
                    complementary = true;
                }
                break;
            case skiq_rx_hdl_D1:
                tmp_id_rx1.hdl = skiq_rx_hdl_C1;
                if (rfic_hdl_has_rx_rate_config(&tmp_id_rx1) )
                {
                    complementary = true;
                }
                break;
            default:
                status = -EINVAL;
                skiq_error("Invalid handle %u specified on card %u\n", p_id->hdl, p_id->card);
                break;
        }
        if (status == 0)
        {
            //find a profile that meets the criteria
            id.hdl = curr_hdl;
            id.chip_id = rx_hdl_to_chip_id[curr_hdl];
            status = find_rx_profile( &id, req_hdls[i].rate, req_hdls[i].bandwidth, &fp , complementary );
        }
        if ( status == 0 )
        {
            p_profile = fp.p_profile;
            skiq_debug("Found RX1/RX2 profile: sample rate=%u bw=%u\n", RX_IQRATE_HZ(p_profile),
                    RX_BW_HZ(p_profile));
            skiq_debug("Found ORX profile: sample rate=%u bw=%u\n", ORX_IQRATE_HZ(p_profile),
                    ORX_BW_HZ(p_profile));

            if (_p_last_profile[p_id->card][id.chip_id] != p_profile )
            {
                skiq_debug("Re-initializing with sample rate %u Hz (card=%u)\n",
                (_taliseDevice[p_id->card][id.chip_id]->rx.rxProfile.rxOutputRate_kHz * RATE_KHZ_TO_HZ), p_id->card);

                if (complementary)
                {
                    status = _talise_set_params_from_profile(&id, p_profile,_taliseDevice[p_id->card][id.chip_id] );
                }
                else
                {
                    status = _talise_set_params_from_profile(&id, p_profile,_taliseDevice[p_id->card][RF_CHIP_A] );
                    if (status == 0)
                    {
                        status = _talise_set_params_from_profile(&id, p_profile,_taliseDevice[p_id->card][RF_CHIP_B] );
                    }
                }

                /* before applying a profile, always enable receive / transmit channel phase coherency
                * bits.  The phase coherency will only take effect if it is set prior to applying a
                * profile.  Transitioning from enabled to disable can happen at any time, but enabling
                * can only occur during profile application. */
                if (status == 0)
                {
                    status = fpga_jesd_write_rx_phase_coherent( p_id->card, true );
                }
                if ( status == 0 )
                {
                    status = fpga_jesd_write_tx_phase_coherent( p_id->card, true );
                }

                /* cache the radio configuration, then perform a full reset and initialization, then
                * restore the radio configuration */
                if ( status == 0 )
                {
                    status = _cache_config( p_id->card, &chipA, &chipB );
                }

                if( status == 0 )
                {
                    status = _reset_and_init( p_id );
                }

                if( status == 0 )
                {
                    status = _restore_config( p_id->card, &chipA, &chipB );
                }
            }
            if ( status == 0 )
            {
                if((curr_hdl == skiq_rx_hdl_C1) || (curr_hdl == skiq_rx_hdl_D1))
                {
                    status = rfic_store_rx_rate_config( &id, ORX_IQRATE_HZ(p_profile), ORX_BW_HZ(p_profile) );
                }
                else
                {
                    status = rfic_store_rx_rate_config( &id, RX_IQRATE_HZ(p_profile)>>fp.dec_rate, RX_BW_HZ(p_profile) );
                }

                /* Store the decimation rate */
                _p_dec_rates[p_id->card][id.hdl] = fp.dec_rate;
            }
        }
    }

    /* apply the decimation rate across "configured" handles regardless of a change of profile */
    if ( status == 0 )
    {
        id = *p_id;
        skiq_rx_hdl_t hdl;
        bool decimationEnabled = false;

        for ( hdl = skiq_rx_hdl_A1; ( hdl < skiq_rx_hdl_end ) && ( status == 0 ); hdl++ )
        {
            id.hdl = hdl;
            if ( rfic_hdl_has_rx_rate_config( &id ) )
            {
                status = decimator_set_rate( p_id->card, hdl, _p_dec_rates[p_id->card][hdl] );
                if ((_p_dec_rates[p_id->card][hdl] > 0) && (status == 0))
                {
                    decimationEnabled = true;
                }
            }
        }

        /* disable receive channel phase coherency bits if decimation is enabled because X4 only has
        * decimation on one side (either A or B, but not both).  Decimation does not work if phase
        * coherency is enabled */
        if (decimationEnabled)
        {
            status = fpga_jesd_write_rx_phase_coherent( p_id->card, false );
            skiq_warning("Disabling phase coherency since decimation is active (card=%u)\n",
                         p_id->card);
        }
    }

    return status;

}

int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status = -EINVAL;
    struct found_profile_t fp;
    ad9379_profile_t *p_profile;

    status = find_tx_profile( p_id, rate, bandwidth, &fp, false );
    
    if ( status == 0 )
    {
        p_profile = fp.p_profile;
        skiq_debug("Found TX profile: sample rate=%u Hz bw=%u Hz\n", TX_IQRATE_HZ(p_profile), TX_BW_HZ(p_profile));

        if (_p_last_profile[p_id->card][tx_hdl_to_chip_id[p_id->hdl]] != p_profile )
        {
            tal_chip_config_t chipA, chipB;

            _talise_set_params_from_profile(p_id, p_profile,_taliseDevice[p_id->card][RF_CHIP_A] );
            _talise_set_params_from_profile(p_id, p_profile,_taliseDevice[p_id->card][RF_CHIP_B] );

            skiq_debug("Re-initializing with sample rate RFIC A: %u Hz RFIC B: %u Hz (card=%u)\n",
                       (_taliseDevice[p_id->card][RF_CHIP_A]->tx.txProfile.txInputRate_kHz * RATE_KHZ_TO_HZ), 
                       (_taliseDevice[p_id->card][RF_CHIP_B]->tx.txProfile.txInputRate_kHz * RATE_KHZ_TO_HZ), 
                       p_id->card);

            /* before applying a profile, enable receive / transmit channel phase coherency bits */
            status = fpga_jesd_write_rx_phase_coherent( p_id->card, true );
            if ( status == 0 )
            {
                status = fpga_jesd_write_tx_phase_coherent( p_id->card, true );
            }

            /* cache the radio configuration, then perform a full reset and initialization, then
             * restore the radio configuration */
            if ( status == 0 )
            {
                status = _cache_config( p_id->card, &chipA, &chipB );
            }

            if( status == 0 )
            {
                status = _reset_and_init( p_id );
            }

            if( status == 0 )
            {
                status = _restore_config( p_id->card, &chipA, &chipB );
            }
        }
    }

    if ( status == 0 )
    {
        status = rfic_store_tx_rate_config( p_id, rate, bandwidth );
    }

    return status;
}

int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;
    uint8_t nr_stages_in_use;

    status = rfic_get_rx_rate_config( p_id, p_rate, NULL /* don't need bandwidth */);
    if ( status == 0 )
    {
        *p_actual_rate = read_profile_rx_sample_rate_hz( p_id );

        if ( *p_actual_rate > 0.0 )
        {
            status = decimator_get_rate( p_id->card, p_id->hdl, &nr_stages_in_use );
            if ( status == 0 )
            {
                *p_actual_rate /= (double)(1 << nr_stages_in_use);
            }
        }
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;

    status = rfic_get_tx_rate_config( p_id, p_rate, NULL /* don't need bandwidth */);
    if ( status == 0 )
    {
        *p_actual_rate = read_profile_tx_sample_rate_hz( p_id );
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;
    uint8_t nr_stages_in_use;

    status = rfic_get_rx_rate_config( p_id, NULL /* don't need sample_rate */, p_bandwidth );
    if ( status == 0 )
    {
    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) )
    {
        *p_actual_bandwidth = _rficDeviceA[p_id->card].devStateInfo.rxBandwidth_Hz;
    }
    else if( (p_id->hdl == skiq_rx_hdl_B1) ||
             (p_id->hdl == skiq_rx_hdl_B2) )
    {
        *p_actual_bandwidth = _rficDeviceB[p_id->card].devStateInfo.rxBandwidth_Hz;
    }
    else if( (p_id->hdl == skiq_rx_hdl_C1) )
    {
        *p_actual_bandwidth = _rficDeviceA[p_id->card].devStateInfo.orxBandwidth_Hz;
    }
    else if( (p_id->hdl == skiq_rx_hdl_D1) )
    {
        *p_actual_bandwidth = _rficDeviceB[p_id->card].devStateInfo.orxBandwidth_Hz;
    }
    else
    {
        status = -ENODEV;
            skiq_error( "Receive handle %s not currently supported on Sidekiq %s at card %u\n",
                        rx_hdl_cstr( p_id->hdl ), part_cstr( _skiq_get_part( p_id->card ) ),
                        p_id->card );
        }
    }

    if ( status == 0 )
    {
        status = decimator_get_rate( p_id->card, p_id->hdl, &nr_stages_in_use );
    }

    if ( ( status == 0 ) && ( nr_stages_in_use > 0 ) )
    {
        double actual_rate;

        actual_rate = read_profile_rx_sample_rate_hz( p_id );
        if ( actual_rate > 0.0 )
        {
            *p_actual_bandwidth = MIN( *p_actual_bandwidth,
                                       actual_rate / (double)(1 << nr_stages_in_use) );
        }
        else
        {
            status = -ENODEV;
        }
    }

    return (status);
}

int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;

    status = rfic_get_tx_rate_config( p_id, NULL /* don't need sample_rate */, p_bandwidth );
    if ( status == 0 )
    {
    if( (p_id->hdl == skiq_tx_hdl_A1) ||
        (p_id->hdl == skiq_tx_hdl_A2) )
    {
        *p_actual_bandwidth = _rficDeviceA[p_id->card].devStateInfo.txBandwidth_Hz;
    }
    else if( (p_id->hdl == skiq_tx_hdl_B1) ||
             (p_id->hdl == skiq_tx_hdl_B2) )
    {
        *p_actual_bandwidth = _rficDeviceB[p_id->card].devStateInfo.txBandwidth_Hz;
    }
    else
    {
        status = -ENODEV;
        skiq_error( "Transmit handle %s not currently supported on Sidekiq %s at card %u\n",
                        tx_hdl_cstr( p_id->hdl ), part_cstr( _skiq_get_part( p_id->card ) ),
                        p_id->card );
        }
    }

    return (status);
}

int32_t _init_gpio( rf_id_t *p_id, uint32_t output_enable )
{
    int32_t status = 0;
    taliseDevice_t *p_rfic = NULL;
    // we need to set the GPIO source to BITBANG (each nibble)
    uint32_t gpio_src = (TAL_GPIO_BITBANG_MODE) |
        (TAL_GPIO_BITBANG_MODE << 4) |
        (TAL_GPIO_BITBANG_MODE << 8) |
        (TAL_GPIO_BITBANG_MODE << 12) |
        (TAL_GPIO_BITBANG_MODE << 16);

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (status=TALISE_setGpioOe( p_rfic,
                                  output_enable,
                                  output_enable)) != 0 )
    {
        skiq_error("Unable to configure GPIO OE %d (card=%" PRIu8 ")\n",
                   status, p_id->card);
    }
    else
    {
        // this only affects GPIOs with OE set to output, so we need to set direction before source
        status=TALISE_setGpioSourceCtrl(p_rfic, gpio_src);
    }

    return (status);
}

int32_t _x4_init_3v3_gpio( rf_id_t *p_id, uint32_t output_enable )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;
    // we need to set the GPIO source to BITBANG (each nibble)
    uint32_t gpio_src = (TAL_GPIO_BITBANG_MODE) |
        (TAL_GPIO_BITBANG_MODE << 4) |
        (TAL_GPIO_BITBANG_MODE << 8);

    p_rfic = rf_id_to_rficDevice( p_id );

    // Try to set the output enable
    if( (status=TALISE_setGpio3v3Oe( p_rfic,
                                     (uint16_t)(output_enable),
                                     (uint16_t)(output_enable) ) != 0) )
    {
        skiq_error( "Unable to configure GPIO OE %d (card=%u)\n", status, p_id->card );
    }
    else
    {
        // this only affects GPIOs with OE set to output, so we need to set direction before source
        status=TALISE_setGpio3v3SourceCtrl(p_rfic, gpio_src);
    }

    return (status);
}

int32_t _write_gpio( rf_id_t *p_id, uint32_t value )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;

    if( value != _gpio_pin_level[p_id->card][rx_hdl_to_chip_id[p_id->hdl]] )
    {
        p_rfic = rf_id_to_rficDevice( p_id );

        if( (status=TALISE_setGpioPinLevel(p_rfic, value)) == 0 )
        {
            _gpio_pin_level[p_id->card][rx_hdl_to_chip_id[p_id->hdl]] = value;
        }
        else
        {
            skiq_error( "Unable to configure GPIO output (%d) (card=%u)\n", status, p_id->card );
        }
    }

    return (status);
}

int32_t _x4_write_3v3_gpio( rf_id_t *p_id, uint32_t value )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;

    // only update if levels don't match
    if( value != _gpio_3v3_pin_level[p_id->card][rx_hdl_to_chip_id[p_id->hdl]] )
    {
        p_rfic = rf_id_to_rficDevice( p_id );

        if( (status=TALISE_setGpio3v3PinLevel(p_rfic, value)) == 0 )
        {
            _gpio_3v3_pin_level[p_id->card][rx_hdl_to_chip_id[p_id->hdl]] = value;
        }
        else
        {
            skiq_error( "Unable to configure GPIO output (%d) (card=%u)\n", status, p_id->card );
            status = -EIO;
        }
    }

    return (status);
}

int32_t _read_gpio( rf_id_t *p_id, uint32_t *p_value )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (status=TALISE_getGpioPinLevel(p_rfic, p_value)) != 0 )
    {
        skiq_error( "Unable to configure GPIO output (%d) (card=%u)\n", status, p_id->card );
        status = -EIO;
    }

    return (status);
}

int32_t _x4_read_3v3_gpio( rf_id_t *p_id, uint32_t *p_value )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    if( (status=TALISE_getGpio3v3PinLevel(p_rfic, (uint16_t*)(p_value))) != 0 )
    {
        skiq_error( "Unable to configure GPIO output (%d) (card=%u)\n", status, p_id->card );
    }

    return (status);
}

int32_t _read_fpga_rf_clock( rf_id_t *p_id, uint64_t *p_freq )
{
    int32_t status=0;

    *p_freq = _fpga_gbt_clock[p_id->card];

    return (status);
}

int32_t _read_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _tx_quadcal[p_id->card][p_id->hdl];

    return (status);
}

int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode )
{
    int32_t status=0;

    _tx_quadcal[p_id->card][p_id->hdl] = mode;

    return (status);
}

int32_t _run_tx_quadcal( rf_id_t *p_id )
{
    int32_t status=-ENODEV;
    uint64_t cal_freq;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    // Get the current freq and that's what we'll cal to
    if( TALISE_getRfPllFrequency( p_rfic,
                                  TAL_RF_PLL,
                                  &cal_freq ) == 0 )
    {
        status=_run_cal( p_id,
                         p_rfic,
                         cal_freq,
                         TAL_RXOFF_EN,
                         tx_hdl_to_chan_en[p_id->hdl] );
    }

    return (status);
}

int32_t _read_rx_stream_hdl_conflict( rf_id_t *p_id,
                                      skiq_rx_hdl_t *p_conflicting_hdls,
                                      uint8_t *p_num_hdls )
{
    int32_t status=0;

    // default to no conflicts
    *p_num_hdls = 0;

    // A1/A2 can't share with C1
    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_C1;
        *p_num_hdls = 1;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_C1;
        *p_num_hdls = 1;
    }
    // C1 can't share with A1/A2...note that this may change with new ADI release
    else if( p_id->hdl == skiq_rx_hdl_C1 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_A1;
        *p_num_hdls = *p_num_hdls + 1;
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_A2;
        *p_num_hdls = *p_num_hdls + 1;
    }
    // B1/B2 can't share with D1
    if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_D1;
        *p_num_hdls = 1;
    }
    else if( p_id->hdl == skiq_rx_hdl_B2 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_D1;
        *p_num_hdls = 1;
    }
    // D1 can't share with B1/B2...note that this may change with new ADI release
    else if( p_id->hdl == skiq_rx_hdl_D1 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_B1;
        *p_num_hdls = *p_num_hdls + 1;
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_B2;
        *p_num_hdls = *p_num_hdls + 1;
    }

    return (status);
}

// check req_freq vs cal state
// determine chans to have enabled
bool _is_cal_required( rf_id_t *p_id, bool is_transmit,
                       uint64_t req_freq, uint64_t *p_cal_freq,
                       taliseRxORxChannels_t *p_calRxChan,
                       taliseTxChannels_t *p_calTxChan )
{
    bool recal=false;
    uint8_t cal_freq_index=0;

    // Note: there is currently a problem running calibration when our
    // sample rate is set to 61.44M for transmit, so we need to
    // ensure that we always skip it if that's our sample rate
    if( is_transmit == true )
    {
        uint32_t rate;
        double actual;

        if( _tx_quadcal[p_id->card][p_id->hdl] == skiq_tx_quadcal_mode_manual )
        {
            recal = false;
            return (recal);
        }

        if( _read_tx_sample_rate( p_id, &rate, &actual ) == 0 )
        {
            if( (uint32_t)(actual) <= TX_SAMPLE_RATE_CAL_MIN )
            {
                skiq_info("TX calibration not supported with sample rate <= %f, skipping (card=%u)\n",
                          actual, p_id->card);
                recal = false;
                return (recal);
            }
        }
        else
        {
            skiq_error("Unable to read current sample rate (card=%u)\n", p_id->card);
        }
    }
    else
    {
        if( _rx_cal_mode[p_id->card][p_id->hdl] == skiq_rx_cal_mode_manual )
        {
            recal = false;
            return (recal);
        }
    }

    // due to cal PLL, we can only run cal within a smaller freq range...so
    // we need to make sure that we we only run cal within the freq bounds
    if( (req_freq < MIN_CAL_FREQ) )
    {
        *p_cal_freq = MIN_CAL_FREQ;
    }
    else if( (req_freq > MAX_CAL_FREQ) )
    {
        *p_cal_freq = MAX_CAL_FREQ;
    }
    else
    {
        *p_cal_freq = req_freq;
    }

    // now see if we actually have to recal
    if( _find_freq_table_index( *p_cal_freq, &cal_freq_index ) == true )
    {
        if( is_transmit == true )
        {
            if( (cal_freq_index != _tx_cal_state[p_id->card][p_id->hdl].freqTableIndex) ||
                ((ABS_DIFF(*p_cal_freq, _tx_cal_state[p_id->card][p_id->hdl].lastCalFreq)) > MAX_FREQ_CAL_OFFSET) )
            {
                // we need to recal
                recal = true;
            }
            // RX off
            *p_calRxChan = TAL_RXOFF_EN;
            // TX on
            *p_calTxChan = tx_hdl_to_chan_en[p_id->hdl];
        }
        else
        {
            if( (cal_freq_index != _rx_cal_state[p_id->card][p_id->hdl].freqTableIndex) ||
                ((ABS_DIFF(*p_cal_freq, _rx_cal_state[p_id->card][p_id->hdl].lastCalFreq)) > MAX_FREQ_CAL_OFFSET) )
            {
                // we need to recal
                recal = true;
            }
            // RX on
            *p_calRxChan = rx_hdl_to_chan_en[p_id->hdl];
            // TX off
            *p_calTxChan = TAL_TXOFF;
        }
    }
    else
    {
        skiq_error("Unable to locate frequency index offset for freq %" PRIu64 " (card=%u)\n",
                   *p_cal_freq, p_id->card);
        recal = false;
    }

    return (recal);
}

// update chans (setRxTxEnable)
// run cal
// update cal state
int32_t _run_cal( rf_id_t *p_id,
                  taliseDevice_t *p_rfic,
                  uint64_t cal_freq,
                  taliseRxORxChannels_t calRxChan,
                  taliseTxChannels_t calTxChan )
{
    int32_t status=0;
    taliseRxORxChannels_t rxChan;
    taliseTxChannels_t txChan;
    uint32_t init_cal_mask=0;
    taliseRxORxChannels_t updateRx;
    taliseTxChannels_t updateTx;
    uint8_t freq_index=0;
    uint64_t freq=0;
    uint8_t i=0;
    uint8_t err_flag=0;
    uint8_t retry_cal_count=0;
    bool cal_complete=false;
    uint32_t rx_cal_mask=0;

    ELAPSED(cal_time);

    // cache the enables
    if( _getRxTxEnable( p_id, p_rfic, &rxChan, &txChan) != 0 )
    {
        status = -ENODEV;
        return (status);
    }

    if( TALISE_radioOff(p_rfic) != 0 )
    {
        status = -ENODEV;
        return (status);
    }

    //////////////////////////////////////////////
    // Update RX channels
    // if RX is currently off and our channel doesn't match what we need to calibrate, then update it
    if( (rxChan != calRxChan) &&
        (rxChan == TAL_RXOFF_EN) )
    {
        updateRx = calRxChan;
    }
    else
    {
        if( (calRxChan != TAL_RXOFF_EN) &&
            ((rxChan & calRxChan) == 0) )
        {
            skiq_warning("RX Calibration not being ran for hdl %s but requested (card=%u)\n",
                         rx_hdl_cstr(p_id->hdl), p_id->card);
        }
        updateRx = rxChan;
    }
    /////////////////////////////////

    ////////////////////////////////////
    // Update TX channels
    // if TX is currently off and our channel doesn't match what we need to calibrate, then update it
    if( (txChan != calTxChan) &&
        (txChan == TAL_TXOFF) )
    {
        updateTx = calTxChan;
    }
    else
    {
        if( (calTxChan != TAL_TXOFF) &&
            ((txChan & calTxChan) == 0) )
        {
            skiq_warning("TX Calibration not being ran for hdl %s but requested (card=%u)\n",
                         tx_hdl_cstr(p_id->hdl), p_id->card);
        }
        updateTx = txChan;
    }
    ///////////////////////////////

    // debug display current channel config
    if (_setRxTxEnable( p_id, p_rfic, updateRx, updateTx ) != 0)
    {
        skiq_warning("Unable to enable channels (card=%u, hdl=%u)\n", p_id->card, p_id->hdl);
    }

    if (_getRxTxEnable( p_id, p_rfic, &rxChan, &txChan) != 0)
    {
        skiq_warning("Unable to determine enabled channels (card=%u, hdl=%u)\n", p_id->card, p_id->hdl);
    }

    ///////////////////////////////////////
    // determine which cals to run
    // setup the RX cals to run
    if( updateRx != TAL_RXOFF_EN )
    {
        status = _convert_skiq_rx_cal_to_tal_cal( p_id->hdl,
                                                  _rx_cal_mask[p_id->card][p_id->hdl],
                                                  &rx_cal_mask );
        if( status == 0 )
        {
            init_cal_mask |= rx_cal_mask;
        }
    }
    // setup the TX cals to run
    if( updateTx != TAL_TXOFF )
    {
        init_cal_mask |= TX_INIT_CAL;
    }
    ///////////////////////////////////////

    ////////////////////////////////
    // Update freq prior to cal if needed
    // if cal freq doesn't match current, update PLL
    if( (TALISE_getRfPllFrequency( p_rfic,
                                   TAL_RF_PLL,
                                   &freq) == 0) )
    {
        if( freq != cal_freq )
        {
            if( (TALISE_setRfPllFrequency(p_rfic,
                                          TAL_RF_PLL,
                                          cal_freq) != 0) )
            {
                skiq_error("Unable to set frequency before calibration (card=%u)\n", p_id->card);
                status = -EINVAL;
                goto run_cal_complete;
            }
        }
    }
    else
    {
        skiq_error("Unable to determine current frequency (card=%u)\n", p_id->card);
        status = -ENODEV;
        goto run_cal_complete;
    }
    ////////////////////////////////

    ////////////////////////////////
    // Perform calibration
    {
        elapsed_start( &cal_time );
        for( retry_cal_count=0; (retry_cal_count<MAX_CAL_RETRY) && (cal_complete==false); retry_cal_count++ )
        {
            skiq_debug( "Running calibration with mask 0x%x at PLL %" PRIu64 " (rxchan=0x%x) (txchan=0x%x) (card=%u)\n",
                      init_cal_mask, cal_freq, rxChan, txChan, p_id->card);

            if( TALISE_runInitCals(p_rfic, init_cal_mask) == 0 )
            {
                _skiq_log(SKIQ_LOG_DEBUG, "Init calibration in progress, waiting for completion (card=%u)\n", p_id->card);
                // wait for cal to complete
                if( (status=TALISE_waitInitCals(p_rfic, INIT_CAL_TIMEOUT_MS, &err_flag)) == 0 )
                {
                    _skiq_log(SKIQ_LOG_DEBUG, "Initial calibration completed successfully (card=%u)\n", p_id->card);
                    cal_complete = true;

                    // make sure we find an index in bounds
                    if( _find_freq_table_index( cal_freq, &freq_index ) == false )
                    {
                        status = -EINVAL;
                        skiq_error( "Invalid frequency requested (index=%u, max=%u) (card=%u)\n",
                                    freq_index, (uint8_t)MAX_FREQ_TABLE_ENTRIES, p_id->card);
                        goto run_cal_complete;
                    }

                    // we need to update our RX cal states
                    if( updateRx != TAL_RXOFF_EN )
                    {
                        for( i=0; i<skiq_rx_hdl_end; i++ )
                        {
                            // make sure the chips match
                            if( rx_hdl_to_chip_id[i] == rx_hdl_to_chip_id[p_id->hdl] )
                            {
                                if( (updateRx & rx_hdl_to_chan_en[i]) == 0 )
                                {
                                    {
                                        skiq_debug( "Resetting last calibration for RX hdl %s (card=%u)\n",
                                                  rx_hdl_cstr(i), p_id->card);
                                        _rx_cal_state[p_id->card][i].freqTableIndex = INVALID_CAL_INDEX;
                                        _rx_cal_state[p_id->card][i].lastCalFreq = INVALID_CAL_FREQ;
                                    }
                                }
                                else
                                {
                                    // save the cal info
                                    skiq_debug( "Saving last calibration for RX hdl %s (card=%u)\n",
                                              rx_hdl_cstr(i), p_id->card);
                                    _rx_cal_state[p_id->card][i].freqTableIndex = freq_index;
                                    _rx_cal_state[p_id->card][i].lastCalFreq = cal_freq;
                                }
                            }
                        }
                    }

                    // we need to update our TX cal states
                    if( updateTx != TAL_TXOFF )
                    {
                        for( i=0; i<skiq_tx_hdl_end; i++ )
                        {
                            // make sure the chips match
                            if( tx_hdl_to_chip_id[i] == tx_hdl_to_chip_id[p_id->hdl] )
                            {
                                if( (updateTx & tx_hdl_to_chan_en[i]) == 0 )
                                {
                                    {
                                        skiq_debug("Resetting last calibration for TX hdl %s (card=%u)\n",
                                                   tx_hdl_cstr(i), p_id->card);
                                        _tx_cal_state[p_id->card][i].freqTableIndex = INVALID_CAL_INDEX;
                                        _tx_cal_state[p_id->card][i].lastCalFreq = INVALID_CAL_FREQ;
                                    }
                                }
                                else
                                {
                                    // save the cal info
                                    skiq_debug("Saving last calibration for TX hdl %s (card=%u)\n",
                                               tx_hdl_cstr(i), p_id->card);
                                    _tx_cal_state[p_id->card][i].freqTableIndex = freq_index;
                                    _tx_cal_state[p_id->card][i].lastCalFreq = cal_freq;
                                }
                            }
                        }
                    }
                } // end wait for cal to complete
                else
                {
                    skiq_warning( "Calibration did not complete successfully, %d, hdl=%u (card=%u)\n",
                                  status, p_id->hdl, p_id->card);

                    uint32_t calsSincePowerUp;
                    uint32_t calsLastRun;
                    uint32_t calsMin;
                    uint8_t initErrCal;
                    uint8_t initErrCode;
                    uint32_t retVal=0;

                    skiq_error( "Wait init cal failed (0x%x) (0x%x) (card=%u)\n",
                                status, err_flag, p_id->card);
                    retVal = TALISE_getInitCalStatus( p_rfic,
                                                      &calsSincePowerUp,
                                                      &calsLastRun,
                                                      &calsMin,
                                                      &initErrCal,
                                                      &initErrCode );
                    skiq_debug( "Cal state 0x%x 0x%x 0x%x 0x%x 0x%x %u, cal retry count %u\n",
                                calsSincePowerUp, calsLastRun, calsMin, initErrCal, initErrCode, retVal, retry_cal_count);
                }
            } // end of run cal failed
            else
            {
                skiq_error( "Running calibrations failed (card=%u)\n", p_id->card);
            }
        } // end of cal retry loop
        if( cal_complete == false )
        {
            skiq_error("Calibration failed (card=%u)\n", p_id->card);
            hal_critical_exit(-1);
            return (-EIO);
        }
        elapsed_end( &cal_time );
        debug_print( "Calibration time: ");
        print_total(&cal_time);
    } // end running cal

run_cal_complete:

    // reset the freq back to what it was
    if( freq != cal_freq )
    {
        TALISE_setRfPllFrequency( p_rfic, TAL_RF_PLL, freq );
    }
    TALISE_radioOn(p_rfic);

    /* restore cached test tone state after calibration since calibration disrupts the state */
    if ( ( status == 0 ) && ( txChan != TAL_TXOFF ) )
    {
        status = restore_tx_test_tone( p_id );
    }

    return (status);
}

int32_t _read_control_output_rx_gain_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;

    *p_ena = 0xFF;
    if( p_id->hdl == skiq_rx_hdl_A2 ||
        p_id->hdl == skiq_rx_hdl_B2 )
    {
        *p_mode = AD9379_MONITOR_INDEX_AGC_MON_8;
    }
    else if( p_id->hdl == skiq_rx_hdl_A1 ||
             p_id->hdl == skiq_rx_hdl_B1 )
    {
        *p_mode = AD9379_MONITOR_INDEX_AGC_MON_9;
    }
    else if( p_id->hdl == skiq_rx_hdl_C1 ||
             p_id->hdl == skiq_rx_hdl_D1 )
    {
        status = -ENOTSUP;
        skiq_warning("Gain Index monitoring not available for handle %s (card=%u)\n",
                     rx_hdl_cstr(p_id->hdl), p_id->card);
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

int32_t _write_control_output_config( rf_id_t *p_id, uint8_t mode, uint8_t ena )
{
    int32_t status=0;
    taliseDevice_t *p_rfic_a = NULL;
    rf_id_t rf_id_b;
    taliseDevice_t *p_rfic_b = NULL;
    uint32_t src_ctrl =
        ((TAL_GPIO_MONITOR_MODE) |
         (TAL_GPIO_MONITOR_MODE << 4));
    uint32_t read_src_ctrl=0;

    // This is a bit hackey, but we're assuming that we need to init B in the same
    // call, so we're going to copy the rf_id_t data structure and make sure the hdl is B
    memcpy(&rf_id_b, p_id, sizeof(rf_id_t));
    rf_id_b.hdl = skiq_rx_hdl_B1;

    p_rfic_a = rf_id_to_rficDevice( p_id );
    p_rfic_b = rf_id_to_rficDevice( &rf_id_b );

    // NOTE: right now, we don't actually populate the handle for this API, so
    // just set both chips the same
    status = TALISE_setGpioMonitorOut( p_rfic_a, mode, ena );
    if( status == 0 )
    {
        status = TALISE_setGpioMonitorOut( p_rfic_b, mode, ena );
    }

    // set OE to enable mask
    if( status == 0 )
    {
        status = TALISE_setGpioOe( p_rfic_a, ena, ena );
        if( status == 0 )
        {
            status = TALISE_setGpioOe( p_rfic_b, ena, ena );
        }
    }

    // set the GPIO source to monitor mode
    if( status == 0 )
    {
        TALISE_getGpioSourceCtrl( p_rfic_a, &read_src_ctrl );
        src_ctrl = read_src_ctrl | src_ctrl;
        status = TALISE_setGpioSourceCtrl( p_rfic_a, src_ctrl );
        if( status == 0 )
        {
            TALISE_getGpioSourceCtrl( p_rfic_b, &read_src_ctrl );
            src_ctrl = read_src_ctrl | src_ctrl;
            status = TALISE_setGpioSourceCtrl( p_rfic_b, src_ctrl );
        }
    }

    return (status);
}

int32_t _read_control_output_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    status = TALISE_getGpioMonitorOut( p_rfic, p_mode, p_ena );

    return (status);
}

uint32_t _read_hop_on_ts_gpio( rf_id_t *p_id, uint8_t *p_chip_index )
{
    uint32_t gpio = 0;

    // GPIO8
    BF_SET( gpio, 1, RFIC_HOP_GPIO, 1 );
    // get the appropriate chip_id
    *p_chip_index = rx_hdl_to_chip_id[p_id->hdl];

    return (gpio);
}

int32_t _write_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode )
{
    int32_t status=0;
    rf_chip_id_t chip_id;
    taliseDevice_t *p_rfic = NULL;
    taliseFhmMode_t fhmMode;

    p_rfic = rf_id_to_rficDevice( p_id );

    // both RX and TX handles map to the same chip so just use rx_hdl
    chip_id = rx_hdl_to_chip_id[p_id->hdl];

    // TODO: do we allow hopping for ORx?

    if( mode == skiq_freq_tune_mode_standard )
    {
        // reset num freqs
        _freq_hop_config[p_id->card][chip_id].num_freqs = 0;
        fhmMode.fhmEnable = 0;
        fhmMode.fhmExitMode = TAL_FHM_FULL_EXIT;

        if( TALISE_setFhmMode( p_rfic, &fhmMode ) == 0 )
        {
            debug_print("Successfully enabled FHM for hdl %s (card=%u)\n",
                        rx_hdl_cstr(p_id->hdl), p_id->card);
        }
        else
        {
            uint32_t errSrc=0;
            uint32_t errCode=0;
            TALISE_getErrCode(p_rfic, &errSrc, &errCode);
            skiq_error("Unable to configure frequency hopping for card=%u (%u/%u: %s)\n",
                       p_id->card, errSrc, errCode, TALISE_getErrorMessage(errSrc, errCode));
            status = -ENODEV;
        }
    }

    // Note: the only way to enable this right now is to configure the hopping list
    if( status == 0 )
    {
        _freq_hop_config[p_id->card][chip_id].tune_mode = mode;
    }

    return (status);
}

int32_t _read_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;
    rf_chip_id_t chip_id;

    // both RX and TX handles map to the same chip so just use rx_hdl
    chip_id = rx_hdl_to_chip_id[p_id->hdl];

    *p_mode = _freq_hop_config[p_id->card][chip_id].tune_mode;

    return (status);
}

int32_t _config_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index )
{
    int32_t status=0;
    uint16_t i=0;
    x4_freq_hop_t *p_hop_config;
    taliseDevice_t *p_rfic = NULL;
    rf_chip_id_t chip_id;
    taliseFhmConfig_t fhmConfig;
    taliseFhmMode_t fhmMode;
    uint32_t errSrc=0;
    uint32_t errCode=0;

    // Note: validation of num freqs (>0, <MAX_NUM_FREQ) should be done at a higher layer

    chip_id = rx_hdl_to_chip_id[p_id->hdl];
    p_hop_config = &(_freq_hop_config[p_id->card][chip_id]);

    //clear out the old values in case this function is called multiple times
    p_hop_config->num_freqs = 0;

    // default min/max to first entry in list
    p_hop_config->hop_range.startFreq = freq_list[0];
    p_hop_config->hop_range.stopFreq = freq_list[0];

    // we need to determine min/max freqs and also copy over the list
    for( i=0; i<num_freq; i++ )
    {
        // check if we have new min or max
        if( freq_list[i] < p_hop_config->hop_range.startFreq )
        {
            p_hop_config->hop_range.startFreq = freq_list[i];
        }
        if( freq_list[i] > p_hop_config->hop_range.stopFreq )
        {
            p_hop_config->hop_range.stopFreq = freq_list[i];
        }
        // save the new freq
        p_hop_config->hop_list[p_hop_config->num_freqs++] = freq_list[i];
    }

    p_rfic = rf_id_to_rficDevice( p_id );

    ///////////////////////////////
    // config
    if( _freq_hop_config[p_id->card][chip_id].tune_mode == skiq_freq_tune_mode_hop_on_timestamp )
    {
        fhmConfig.fhmGpioPin = TAL_GPIO_08; // we'll toggle GPIO8 to perform a hop
    }
    else
    {
        fhmConfig.fhmGpioPin = TAL_GPIO_INVALID; // we're not using GPIO so set to invalid
    }
    fhmConfig.fhmMinFreq_MHz = (p_hop_config->hop_range.startFreq / 1000000);  //Integer division rounds down
    fhmConfig.fhmMaxFreq_MHz = ROUND_UP(p_hop_config->hop_range.stopFreq, 1000000);

    // radio must be off to setup FHM config
    TALISE_radioOff(p_rfic);
    if( TALISE_setFhmConfig(p_rfic, &fhmConfig) == 0 )
    {
        debug_print("FHM configured successfully! (card=%u)\n", p_id->card);
    }
    else
    {
        skiq_error("Unable to configure FHM (card=%u", p_id->card);
        status = -ENODEV;
    }
    // make sure the radio gets turned back on
    TALISE_radioOn( p_rfic );
    ///////////////////////////////

    ///////////////////////////////
    // mode
    if( status == 0 )
    {
        fhmMode.fhmEnable = 1;
        fhmMode.enableMcsSync = 1;
        if( p_hop_config->tune_mode == skiq_freq_tune_mode_hop_on_timestamp )
        {
            fhmMode.fhmTriggerMode = TAL_FHM_GPIO_MODE;
        }
        else
        {
            fhmMode.fhmTriggerMode = TAL_FHM_NON_GPIO_MODE;
        }
        // default the freq to the first entry in the list
        fhmMode.fhmInitFrequency_Hz = p_hop_config->hop_list[initial_index];

        if( (status=TALISE_setFhmMode( p_rfic, &fhmMode )) == 0 )
        {
            debug_print("FHM mode setup successfully! (card=%u)\n", p_id->card);
        }
        else
        {
            skiq_error("Unable to configure FHM mode (card=%u), status=%d",
                       p_id->card, status);
            status = -ENODEV;
        }
    }
    ///////////////////////////////

    ////////////////////////////////////
    // make sure the GPIO is configured so that the FPGA can drive it
    // NOTE: if this happens before setting the mode, we get an ARM error
    // configure as input for RFIC
    _init_gpio( p_id, 0 );

    // display detailed talise error message
    if( status == -ENODEV )
    {
        TALISE_getErrCode(p_rfic, &errSrc, &errCode);
        skiq_error("Detailed FHM error for card=%u (%u/%u: %s)\n",
                   p_id->card, errSrc, errCode, TALISE_getErrorMessage(errSrc, errCode));
    }

    if( status == 0 )
    {
        // put the "initial" into our next hop
        p_hop_config->next_hop.index = initial_index;
        p_hop_config->next_hop.freq = freq_list[initial_index];
        // set current / mailbox to none
        p_hop_config->curr_hop.index = SKIQ_MAX_NUM_FREQ_HOPS;
        p_hop_config->curr_hop.freq = 0;
        p_hop_config->mailbox.index = SKIQ_MAX_NUM_FREQ_HOPS;
        p_hop_config->mailbox.freq = 0;
    }

    return (status);
}

int32_t _read_hop_list( rf_id_t *p_id, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=0;
    rf_chip_id_t chip_id;
    x4_freq_hop_t *p_hop_config;

    // Note: validation of num freqs (>0, <MAX_NUM_FREQ) should be done at a higher layer

    chip_id = rx_hdl_to_chip_id[p_id->hdl];
    p_hop_config = &(_freq_hop_config[p_id->card][chip_id]);

    *p_num_freq = p_hop_config->num_freqs;
    memcpy( &(freq_list[0]),
            p_hop_config->hop_list,
            sizeof(uint64_t)*(*p_num_freq) );

    return (status);
}

int32_t _write_next_hop( rf_id_t *p_id, uint16_t freq_index )
{
    int32_t status=0;
    rf_chip_id_t chip_id;
    taliseDevice_t *p_rfic = NULL;
    uint64_t next_freq=0;

    chip_id = rx_hdl_to_chip_id[p_id->hdl];

    if( status == 0 )
    {
        p_rfic = rf_id_to_rficDevice( p_id );

        next_freq = _freq_hop_config[p_id->card][chip_id].hop_list[freq_index];

        debug_print("Setting next hop to %" PRIu64 "Hz (card=%u)\n", next_freq, p_id->card);

        if( _freq_hop_config[p_id->card][chip_id].tune_mode == skiq_freq_tune_mode_hop_on_timestamp )
        {
            if( TALISE_setFhmHop( p_rfic, next_freq ) == 0 )
            {
                debug_print("Successfully configured next freq hop (card=%u)\n", p_id->card);
                // put this freq into our mailbox
                _freq_hop_config[p_id->card][chip_id].mailbox.index = freq_index;
                _freq_hop_config[p_id->card][chip_id].mailbox.freq = next_freq;
            }
            else
            {
                uint32_t errSrc=0;
                uint32_t errCode=0;

                TALISE_getErrCode(p_rfic, &errSrc, &errCode);
                skiq_error("Frequency hop error for card=%u (%u/%u: %s)\n",
                           p_id->card, errSrc, errCode, TALISE_getErrorMessage(errSrc, errCode));
                status = -ENODEV;
            }
        }
        else
        {
            // put this freq into our mailbox
            _freq_hop_config[p_id->card][chip_id].mailbox.index = freq_index;
            _freq_hop_config[p_id->card][chip_id].mailbox.freq = next_freq;
        }
    }

    return (status);
}

int32_t _hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status=0;
    rf_chip_id_t chip_id;
    taliseDevice_t *p_rfic = NULL;
    x4_freq_hop_t *p_hop_config = NULL;

    chip_id = rx_hdl_to_chip_id[p_id->hdl];
    p_hop_config = &(_freq_hop_config[p_id->card][chip_id]);

    debug_print("Performing hop to %" PRIu64 "Hz (card=%u)\n",
                p_hop_config->next_hop.freq, p_id->card); 

    if( p_hop_config->tune_mode == skiq_freq_tune_mode_hop_on_timestamp )
    {
        status = fpga_ctrl_hop( p_id->card,
                                chip_id,
                                1 /* rfic_ctrl_in */,
                                p_hop_config->mailbox.index,
                                rf_timestamp );

    }
    else
    {
        p_rfic = rf_id_to_rficDevice( p_id );

        // push down the config
        if( TALISE_setFhmHop( p_rfic, p_hop_config->mailbox.freq ) == 0 )
        {
            debug_print("Successfully configured next freq hop (card=%u)\n", p_id->card);
        }
        else
        {
            uint32_t errSrc=0;
            uint32_t errCode=0;

            TALISE_getErrCode(p_rfic, &errSrc, &errCode);
            skiq_error("Frequency hop error for card=%u (%u/%u: %s)\n",
                       p_id->card, errSrc, errCode, TALISE_getErrorMessage(errSrc, errCode));
            status = -ENODEV;
        }
    }

    if( status == 0 )
    {
        *p_act_freq = (double)(p_hop_config->mailbox.freq);
        
        // update our hop config

        // next moves to current
        memcpy( &(p_hop_config->curr_hop),
                &(p_hop_config->next_hop),
                sizeof(freq_hop_t) );
        // mailbox moves to next
        memcpy( &(p_hop_config->next_hop),
                &(p_hop_config->mailbox),
                sizeof(freq_hop_t) );
        // reset our mailbox
        p_hop_config->mailbox.index = SKIQ_MAX_NUM_FREQ_HOPS;
        p_hop_config->mailbox.freq = 0;
    }

#if (defined DEBUG_RFIC_X4)
        uint64_t lo_freq = 0;
        TALISE_getFhmRfPllFrequency(p_rfic, &lo_freq );
        debug_print("TALISE_getFhmRfPllFrequency: %lu for handle: %s\n", lo_freq, rx_hdl_cstr(p_id->hdl)  );
#endif
    return (status);
}

int32_t _read_curr_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=-ENODEV;
    rf_chip_id_t chip_id;
    x4_freq_hop_t *p_hop_config = NULL;

    chip_id = rx_hdl_to_chip_id[p_id->hdl];
    p_hop_config = &(_freq_hop_config[p_id->card][chip_id]);

    if( p_hop_config->curr_hop.index != SKIQ_MAX_NUM_FREQ_HOPS )
    {
        status = 0;
        memcpy( p_hop,
                &p_hop_config->curr_hop,
                sizeof(freq_hop_t) );
    }

    return (status);
}

int32_t _read_next_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=-ENODEV;
    rf_chip_id_t chip_id;
    x4_freq_hop_t *p_hop_config = NULL;

    chip_id = rx_hdl_to_chip_id[p_id->hdl];
    p_hop_config = &(_freq_hop_config[p_id->card][chip_id]);

    if( p_hop_config->next_hop.index != SKIQ_MAX_NUM_FREQ_HOPS )
    {
        status = 0;
        memcpy( p_hop,
                &p_hop_config->next_hop,
                sizeof(freq_hop_t) );
    }
    return (status);
}

int32_t _init_from_file( rf_id_t *p_id, FILE* p_file )
{
    int32_t status=0;

    //Unfortunately p_id->hdl is always set to A1, so users can't specify two profiles.

    if( _p_user_taliseDevice[p_id->card] == NULL )
    {
        skiq_debug("Allocating memory for user profile (card=%u)\n", p_id->card);

        status=_talise_profile_alloc( &_p_user_taliseDevice[p_id->card] );
    }

    if( status == 0 )
    {
        skiq_info("Applying user provided profile for card %u\n", p_id->card);
        status = ad9379_parse_profile_from_file( _p_user_taliseDevice[p_id->card], p_file );

        if( status == 0 )
        {
            //if parsing was successful, copy from _p_user_taliseDevice into _taliseDevice
            status = _talise_set_params_from_file(p_id,_taliseDevice[p_id->card][RF_CHIP_A], _p_user_taliseDevice[p_id->card]);
            if (status == 0)
            {
                status = _talise_set_params_from_file(p_id,_taliseDevice[p_id->card][RF_CHIP_B], _p_user_taliseDevice[p_id->card]);
            }
            //apply_profile_change
            if (status == 0)
            {
                status = _reset_and_init(p_id);
            }
        }
        else
        {
            skiq_error("Parsing profile failed for card %u\n", p_id->card);
            _talise_profile_free( &(_p_user_taliseDevice[p_id->card]) );
        }

    }
    else
    {
        skiq_error("Unable to allocate memory for user profile (card=%u)\n", p_id->card);
        FREE_IF_NOT_NULL( _p_user_taliseDevice[p_id->card] );
    }

    return (status);
}
int32_t _talise_profile_alloc( taliseInit_t **p_taliseInit )
{
    int32_t status = 0;

    if (*p_taliseInit != NULL)
    {
        _talise_profile_free(p_taliseInit);
    }

    //expect this to be called with a null ptr

    *p_taliseInit =(taliseInit_t*) (calloc(1,sizeof(taliseInit_t)));
    (**p_taliseInit).rx.rxProfile.rxFir.coefs = (int16_t*)calloc(AD9379_MAX_COEFFS,sizeof(int16_t));
    if(((**p_taliseInit).rx.rxProfile.rxFir.coefs) == NULL)
    {
        status = -ENOMEM;
        FREE_IF_NOT_NULL(*p_taliseInit);
    }
    (**p_taliseInit).obsRx.orxProfile.rxFir.coefs = (int16_t*)calloc(AD9379_MAX_COEFFS,sizeof(int16_t));
    if(((**p_taliseInit).obsRx.orxProfile.rxFir.coefs) == NULL)
    {
        status = -ENOMEM;
        FREE_IF_NOT_NULL(*p_taliseInit);
        FREE_IF_NOT_NULL((**p_taliseInit).rx.rxProfile.rxFir.coefs);
    }
    (**p_taliseInit).tx.txProfile.txFir.coefs = (int16_t*)calloc(AD9379_MAX_COEFFS,sizeof(int16_t));
    if ((**p_taliseInit).tx.txProfile.txFir.coefs == NULL)
    {
        status = -ENOMEM;
        FREE_IF_NOT_NULL(*p_taliseInit);
        FREE_IF_NOT_NULL((**p_taliseInit).rx.rxProfile.rxFir.coefs);
        FREE_IF_NOT_NULL((**p_taliseInit).obsRx.orxProfile.rxFir.coefs);
    }
    return (status);
}

void _talise_profile_free( taliseInit_t **pp_taliseDevice )
{
    // free up our previously allocated memory
    if ( *pp_taliseDevice != NULL )
    {
        FREE_IF_NOT_NULL( (*pp_taliseDevice)->rx.rxProfile.rxFir.coefs );
        FREE_IF_NOT_NULL( (*pp_taliseDevice)->tx.txProfile.txFir.coefs );
        FREE_IF_NOT_NULL( (*pp_taliseDevice)->obsRx.orxProfile.rxFir.coefs );
        FREE_IF_NOT_NULL( *pp_taliseDevice );
    }
}

static int32_t _talise_set_default_params( rf_id_t *p_id,
                                         taliseInit_t *p_taliseDevice )
{
    CHECK_NULL_PARAM(p_taliseDevice);

    p_taliseDevice->spiSettings.MSBFirst = ad9379_default_profile.spiSettings.MSBFirst;
    p_taliseDevice->spiSettings.enSpiStreaming = ad9379_default_profile.spiSettings.enSpiStreaming;
    p_taliseDevice->spiSettings.fourWireMode = ad9379_default_profile.spiSettings.fourWireMode;
    p_taliseDevice->spiSettings.cmosPadDrvStrength = ad9379_default_profile.spiSettings.cmosPadDrvStrength;
    p_taliseDevice->spiSettings.autoIncAddrUp = ad9379_default_profile.spiSettings.autoIncAddrUp;

    p_taliseDevice->rx.framerSel = ad9379_default_profile.rx.framerSel;
    p_taliseDevice->rx.rxGainCtrl.gainMode = ad9379_default_profile.rx.rxGainCtrl.gainMode;
    p_taliseDevice->rx.rxGainCtrl.rx1GainIndex = ad9379_default_profile.rx.rxGainCtrl.rx1GainIndex;
    p_taliseDevice->rx.rxGainCtrl.rx2GainIndex = ad9379_default_profile.rx.rxGainCtrl.rx2GainIndex;
    p_taliseDevice->rx.rxGainCtrl.rx1MaxGainIndex = ad9379_default_profile.rx.rxGainCtrl.rx1MaxGainIndex;
    p_taliseDevice->rx.rxGainCtrl.rx1MinGainIndex = ad9379_default_profile.rx.rxGainCtrl.rx1MinGainIndex;
    p_taliseDevice->rx.rxGainCtrl.rx2MaxGainIndex = ad9379_default_profile.rx.rxGainCtrl.rx2MaxGainIndex;
    p_taliseDevice->rx.rxGainCtrl.rx2MinGainIndex = ad9379_default_profile.rx.rxGainCtrl.rx2MinGainIndex;
    p_taliseDevice->rx.rxChannels = ad9379_default_profile.rx.rxChannels;

    p_taliseDevice->tx.deframerSel = ad9379_default_profile.tx.deframerSel;
    p_taliseDevice->tx.txChannels = ad9379_default_profile.tx.txChannels;
    p_taliseDevice->tx.txAttenStepSize = ad9379_default_profile.tx.txAttenStepSize;
    p_taliseDevice->tx.tx1Atten_mdB = ad9379_default_profile.tx.tx1Atten_mdB;
    p_taliseDevice->tx.tx2Atten_mdB = ad9379_default_profile.tx.tx2Atten_mdB;
    p_taliseDevice->tx.disTxDataIfPllUnlock = ad9379_default_profile.tx.disTxDataIfPllUnlock;

    p_taliseDevice->obsRx.orxGainCtrl.gainMode = ad9379_default_profile.obsRx.orxGainCtrl.gainMode;
    p_taliseDevice->obsRx.orxGainCtrl.orx1GainIndex = ad9379_default_profile.obsRx.orxGainCtrl.orx1GainIndex;
    p_taliseDevice->obsRx.orxGainCtrl.orx2GainIndex = ad9379_default_profile.obsRx.orxGainCtrl.orx2GainIndex;
    p_taliseDevice->obsRx.orxGainCtrl.orx1MaxGainIndex = ad9379_default_profile.obsRx.orxGainCtrl.orx1MaxGainIndex;
    p_taliseDevice->obsRx.orxGainCtrl.orx1MinGainIndex = ad9379_default_profile.obsRx.orxGainCtrl.orx1MinGainIndex;
    p_taliseDevice->obsRx.orxGainCtrl.orx2MaxGainIndex = ad9379_default_profile.obsRx.orxGainCtrl.orx2MaxGainIndex;
    p_taliseDevice->obsRx.orxGainCtrl.orx2MinGainIndex = ad9379_default_profile.obsRx.orxGainCtrl.orx2MinGainIndex;

    p_taliseDevice->obsRx.framerSel = ad9379_default_profile.obsRx.framerSel;
    p_taliseDevice->obsRx.obsRxChannelsEnable = ad9379_default_profile.obsRx.obsRxChannelsEnable;
    p_taliseDevice->obsRx.obsRxLoSource = ad9379_default_profile.obsRx.framerSel;

    //jesd memcpy
    memcpy(&p_taliseDevice->jesd204Settings,&ad9379_default_profile.jesd204Settings, sizeof(taliseJesdSettings_t));

    return 0;
}

static int32_t _talise_set_params_from_profile( rf_id_t *p_id,
                                      ad9379_profile_t *p_profile,
                                      taliseInit_t *p_taliseDevice )

{
    int32_t status = 0;
    uint8_t i, num_fir_coeffs=0;

    CHECK_NULL_PARAM(p_profile);
    CHECK_NULL_PARAM(p_taliseDevice);

    //rx:
    num_fir_coeffs = p_profile->p_rxProfile->rxFir.numFirCoefs;
    p_taliseDevice->rx.rxProfile.rxFir.numFirCoefs = num_fir_coeffs;
    for(i=0;i<num_fir_coeffs;i++)
    {
        p_taliseDevice->rx.rxProfile.rxFir.coefs[i] = p_profile->p_rxProfile->rxFir.coefs[i];
    }

    p_taliseDevice->rx.rxProfile.rxFir.gain_dB = p_profile->p_rxProfile->rxFir.gain_dB;
    p_taliseDevice->rx.rxProfile.rxFirDecimation = p_profile->p_rxProfile->rxFirDecimation;
    p_taliseDevice->rx.rxProfile.rxDec5Decimation = p_profile->p_rxProfile->rxDec5Decimation;
    p_taliseDevice->rx.rxProfile.rhb1Decimation = p_profile->p_rxProfile->rhb1Decimation;
    p_taliseDevice->rx.rxProfile.rxOutputRate_kHz = p_profile->p_rxProfile->rxOutputRate_kHz;
    p_taliseDevice->rx.rxProfile.rfBandwidth_Hz = p_profile->p_rxProfile->rfBandwidth_Hz;
    p_taliseDevice->rx.rxProfile.rxBbf3dBCorner_kHz = p_profile->p_rxProfile->rxBbf3dBCorner_kHz;

    for(i=0;i<AD9379_ADC_NUM_COEFFS;i++)
    {
        p_taliseDevice->rx.rxProfile.rxAdcProfile[i] = p_profile->p_rxProfile->rxAdcProfile[i];
    }
    p_taliseDevice->rx.rxProfile.rxDdcMode = p_profile->p_rxProfile->rxDdcMode;
    p_taliseDevice->rx.rxProfile.rxNcoShifterCfg = p_profile->p_rxProfile->rxNcoShifterCfg;

    //tx
    p_taliseDevice->tx.txProfile.dacDiv = p_profile->p_txProfile->dacDiv;
    p_taliseDevice->tx.txProfile.txFir.gain_dB = p_profile->p_txProfile->txFir.gain_dB;
    num_fir_coeffs =p_profile->p_txProfile->txFir.numFirCoefs;
    p_taliseDevice->tx.txProfile.txFir.numFirCoefs = num_fir_coeffs;

    for(i=0;i<num_fir_coeffs;i++)
    {
        p_taliseDevice->tx.txProfile.txFir.coefs[i] = p_profile->p_txProfile->txFir.coefs[i];
    }

    p_taliseDevice->tx.txProfile.txFirInterpolation = p_profile->p_txProfile->txFirInterpolation;
    p_taliseDevice->tx.txProfile.thb1Interpolation = p_profile->p_txProfile->thb1Interpolation;
    p_taliseDevice->tx.txProfile.thb2Interpolation = p_profile->p_txProfile->thb2Interpolation;
    p_taliseDevice->tx.txProfile.thb3Interpolation = p_profile->p_txProfile->thb3Interpolation;
    p_taliseDevice->tx.txProfile.txInt5Interpolation = p_profile->p_txProfile->txInt5Interpolation;
    p_taliseDevice->tx.txProfile.txInputRate_kHz = p_profile->p_txProfile->txInputRate_kHz;
    p_taliseDevice->tx.txProfile.primarySigBandwidth_Hz = p_profile->p_txProfile->primarySigBandwidth_Hz;
    p_taliseDevice->tx.txProfile.rfBandwidth_Hz = p_profile->p_txProfile->rfBandwidth_Hz;
    p_taliseDevice->tx.txProfile.txDac3dBCorner_kHz = p_profile->p_txProfile->txDac3dBCorner_kHz;
    p_taliseDevice->tx.txProfile.txBbf3dBCorner_kHz = p_profile->p_txProfile->txBbf3dBCorner_kHz;

    /* imported profiles do not retain their .txChannels setting, so this code enables TAL_TX1TX2 if
     * any of the loopBackAdcProfile coefficients are non-zero.  If all coefficients are zero, this
     * indicates that the TX should be configured as off. */
    p_taliseDevice->tx.txChannels = TAL_TXOFF;
    for(i=0;i<AD9379_ADC_NUM_COEFFS;i++)
    {
        if ( ( p_taliseDevice->tx.txChannels == TAL_TXOFF ) &&
             ( p_profile->p_txProfile->loopBackAdcProfile[i] > 0 ) )
        {
            debug_print("Enabling TX1TX2 for profile on card %u\n", p_id->card);
            p_taliseDevice->tx.txChannels = TAL_TX1TX2;
        }
        p_taliseDevice->tx.txProfile.loopBackAdcProfile[i] = p_profile->p_txProfile->loopBackAdcProfile[i];
    }

    //orx
    num_fir_coeffs = p_profile->p_orxProfile->rxFir.numFirCoefs;

    p_taliseDevice->obsRx.orxProfile.rxFir.numFirCoefs = num_fir_coeffs;
    p_taliseDevice->obsRx.orxProfile.rxFir.gain_dB = p_profile->p_orxProfile->rxFir.gain_dB;

    for(i=0;i<num_fir_coeffs;i++)
    {
        p_taliseDevice->obsRx.orxProfile.rxFir.coefs[i] = p_profile->p_orxProfile->rxFir.coefs[i];
    }

    p_taliseDevice->obsRx.orxProfile.rxFirDecimation = p_profile->p_orxProfile->rxFirDecimation;
    p_taliseDevice->obsRx.orxProfile.rxDec5Decimation = p_profile->p_orxProfile->rxDec5Decimation;
    p_taliseDevice->obsRx.orxProfile.rhb1Decimation = p_profile->p_orxProfile->rhb1Decimation;
    p_taliseDevice->obsRx.orxProfile.orxOutputRate_kHz = p_profile->p_orxProfile->orxOutputRate_kHz;
    p_taliseDevice->obsRx.orxProfile.rfBandwidth_Hz = p_profile->p_orxProfile->rfBandwidth_Hz;
    p_taliseDevice->obsRx.orxProfile.rxBbf3dBCorner_kHz = p_profile->p_orxProfile->rxBbf3dBCorner_kHz;

    for(i=0;i<AD9379_ADC_NUM_COEFFS;i++)
    {
        p_taliseDevice->obsRx.orxProfile.orxLowPassAdcProfile[i] = p_profile->p_orxProfile->orxLowPassAdcProfile[i];
    }

    for(i=0;i<AD9379_ADC_NUM_COEFFS;i++)
    {
        p_taliseDevice->obsRx.orxProfile.orxBandPassAdcProfile[i] = p_profile->p_orxProfile->orxBandPassAdcProfile[i];
    }

    p_taliseDevice->obsRx.orxProfile.orxDdcMode = p_profile->p_orxProfile->orxDdcMode;

    for(i=0;i<AD9379_ORX_MERGE_FILTER_COEFFS;i++)
    {
        p_taliseDevice->obsRx.orxProfile.orxMergeFilter[i] = p_profile->p_orxProfile->orxMergeFilter[i];
    }

    //clocks:
    p_taliseDevice->clocks.deviceClock_kHz = p_profile->p_clocks->deviceClock_kHz;
    p_taliseDevice->clocks.clkPllVcoFreq_kHz = p_profile->p_clocks->clkPllVcoFreq_kHz;
    p_taliseDevice->clocks.clkPllHsDiv = p_profile->p_clocks->clkPllHsDiv;
    p_taliseDevice->clocks.rfPllUseExternalLo = p_profile->p_clocks->rfPllUseExternalLo;
    p_taliseDevice->clocks.rfPllPhaseSyncMode = p_profile->p_clocks->rfPllPhaseSyncMode;

    //store the reference to this profile for later comparison
    _p_last_profile[p_id->card][rx_hdl_to_chip_id[p_id->hdl]] = p_profile;

    return status;
}

static int32_t _talise_set_params_from_file( rf_id_t *p_id,
                                             taliseInit_t *p_dest_taliseDevice,
                                             taliseInit_t *p_src_taliseDevice )
{
    uint8_t i, num_fir_coeffs=0;

    CHECK_NULL_PARAM(p_src_taliseDevice);
    CHECK_NULL_PARAM(p_dest_taliseDevice);

    memcpy(&p_dest_taliseDevice->clocks, &p_src_taliseDevice->clocks, sizeof(taliseDigClocks_t));

    //orx
    num_fir_coeffs = p_src_taliseDevice->obsRx.orxProfile.rxFir.numFirCoefs;
    p_dest_taliseDevice->obsRx.orxProfile.rxFir.numFirCoefs = num_fir_coeffs;
    p_dest_taliseDevice->obsRx.orxProfile.rxFir.gain_dB = p_src_taliseDevice->obsRx.orxProfile.rxFir.gain_dB;
    for(i=0;i<num_fir_coeffs;i++)
    {
        p_dest_taliseDevice->obsRx.orxProfile.rxFir.coefs[i] = p_src_taliseDevice->obsRx.orxProfile.rxFir.coefs[i];
    }
    p_dest_taliseDevice->obsRx.orxProfile.rxFirDecimation = p_src_taliseDevice->obsRx.orxProfile.rxFirDecimation;
    p_dest_taliseDevice->obsRx.orxProfile.rxDec5Decimation = p_src_taliseDevice->obsRx.orxProfile.rxDec5Decimation;
    p_dest_taliseDevice->obsRx.orxProfile.rhb1Decimation = p_src_taliseDevice->obsRx.orxProfile.rhb1Decimation;
    p_dest_taliseDevice->obsRx.orxProfile.orxOutputRate_kHz = p_src_taliseDevice->obsRx.orxProfile.orxOutputRate_kHz;
    p_dest_taliseDevice->obsRx.orxProfile.rfBandwidth_Hz = p_src_taliseDevice->obsRx.orxProfile.rfBandwidth_Hz;
    p_dest_taliseDevice->obsRx.orxProfile.rxBbf3dBCorner_kHz = p_src_taliseDevice->obsRx.orxProfile.rxBbf3dBCorner_kHz;

    for(i=0; i<AD9379_ADC_NUM_COEFFS; i++)
    {
        p_dest_taliseDevice->obsRx.orxProfile.orxLowPassAdcProfile[i] = p_src_taliseDevice->obsRx.orxProfile.orxLowPassAdcProfile[i];
        p_dest_taliseDevice->obsRx.orxProfile.orxBandPassAdcProfile[i] = p_src_taliseDevice->obsRx.orxProfile.orxBandPassAdcProfile[i];
    }
    p_dest_taliseDevice->obsRx.orxProfile.orxDdcMode = p_src_taliseDevice->obsRx.orxProfile.orxDdcMode;

    for(i=0; i<AD9379_ORX_MERGE_FILTER_COEFFS; i++)
    {
        p_dest_taliseDevice->obsRx.orxProfile.orxMergeFilter[i] = p_src_taliseDevice->obsRx.orxProfile.orxMergeFilter[i];
    }

    p_dest_taliseDevice->obsRx.obsRxChannelsEnable = p_src_taliseDevice->obsRx.obsRxChannelsEnable;
    p_dest_taliseDevice->obsRx.obsRxLoSource = p_src_taliseDevice->obsRx.obsRxLoSource;

    //rx:

    num_fir_coeffs = p_src_taliseDevice->rx.rxProfile.rxFir.numFirCoefs;
    p_dest_taliseDevice->rx.rxProfile.rxFir.numFirCoefs = num_fir_coeffs;
    p_dest_taliseDevice->rx.rxProfile.rxFir.gain_dB = p_src_taliseDevice->rx.rxProfile.rxFir.gain_dB;

    for(i=0;i<num_fir_coeffs;i++)
    {
        p_dest_taliseDevice->rx.rxProfile.rxFir.coefs[i] = p_src_taliseDevice->rx.rxProfile.rxFir.coefs[i];
    }

    p_dest_taliseDevice->rx.rxProfile.rxFirDecimation = p_src_taliseDevice->rx.rxProfile.rxFirDecimation;
    p_dest_taliseDevice->rx.rxProfile.rxDec5Decimation = p_src_taliseDevice->rx.rxProfile.rxDec5Decimation;
    p_dest_taliseDevice->rx.rxProfile.rhb1Decimation = p_src_taliseDevice->rx.rxProfile.rhb1Decimation;
    p_dest_taliseDevice->rx.rxProfile.rxOutputRate_kHz = p_src_taliseDevice->rx.rxProfile.rxOutputRate_kHz;
    p_dest_taliseDevice->rx.rxProfile.rfBandwidth_Hz = p_src_taliseDevice->rx.rxProfile.rfBandwidth_Hz;
    p_dest_taliseDevice->rx.rxProfile.rxBbf3dBCorner_kHz = p_src_taliseDevice->rx.rxProfile.rxBbf3dBCorner_kHz;

    for(i=0; i<AD9379_ADC_NUM_COEFFS; i++)
    {
        p_dest_taliseDevice->rx.rxProfile.rxAdcProfile[i] = p_src_taliseDevice->rx.rxProfile.rxAdcProfile[i];

    }
    p_dest_taliseDevice->rx.rxProfile.rxDdcMode = p_src_taliseDevice->rx.rxProfile.rxDdcMode;
    p_dest_taliseDevice->rx.rxProfile.rxNcoShifterCfg = p_src_taliseDevice->rx.rxProfile.rxNcoShifterCfg;

    p_dest_taliseDevice->rx.framerSel = p_src_taliseDevice->rx.framerSel;
    p_dest_taliseDevice->rx.rxChannels = p_src_taliseDevice->rx.rxChannels;

    //tx
    p_dest_taliseDevice->tx.txProfile.dacDiv = p_src_taliseDevice->tx.txProfile.dacDiv;
    p_dest_taliseDevice->tx.txProfile.txFir.gain_dB = p_src_taliseDevice->tx.txProfile.txFir.gain_dB;

    num_fir_coeffs = p_src_taliseDevice->tx.txProfile.txFir.numFirCoefs;
    p_dest_taliseDevice->tx.txProfile.txFir.numFirCoefs = num_fir_coeffs;
    p_dest_taliseDevice->tx.txProfile.txFir.gain_dB = p_src_taliseDevice->tx.txProfile.txFir.gain_dB;

    for(i=0;i<num_fir_coeffs;i++)
    {
        p_dest_taliseDevice->tx.txProfile.txFir.coefs[i] = p_src_taliseDevice->tx.txProfile.txFir.coefs[i];
    }

    p_dest_taliseDevice->tx.txProfile.txFirInterpolation = p_src_taliseDevice->tx.txProfile.txFirInterpolation;
    p_dest_taliseDevice->tx.txProfile.thb1Interpolation = p_src_taliseDevice->tx.txProfile.thb1Interpolation;
    p_dest_taliseDevice->tx.txProfile.thb2Interpolation = p_src_taliseDevice->tx.txProfile.thb2Interpolation;
    p_dest_taliseDevice->tx.txProfile.thb3Interpolation = p_src_taliseDevice->tx.txProfile.thb3Interpolation;
    p_dest_taliseDevice->tx.txProfile.txInt5Interpolation = p_src_taliseDevice->tx.txProfile.txInt5Interpolation;
    p_dest_taliseDevice->tx.txProfile.txInputRate_kHz = p_src_taliseDevice->tx.txProfile.txInputRate_kHz;
    p_dest_taliseDevice->tx.txProfile.primarySigBandwidth_Hz = p_src_taliseDevice->tx.txProfile.primarySigBandwidth_Hz;
    p_dest_taliseDevice->tx.txProfile.rfBandwidth_Hz = p_src_taliseDevice->tx.txProfile.rfBandwidth_Hz;
    p_dest_taliseDevice->tx.txProfile.txDac3dBCorner_kHz = p_src_taliseDevice->tx.txProfile.txDac3dBCorner_kHz;
    p_dest_taliseDevice->tx.txProfile.txBbf3dBCorner_kHz = p_src_taliseDevice->tx.txProfile.txBbf3dBCorner_kHz;

    /* imported profiles do not retain their .txChannels setting, so this code enables TAL_TX1TX2 if
     * any of the loopBackAdcProfile coefficients are non-zero.  If all coefficients are zero, this
     * indicates that the TX should be configured as off. */
    p_dest_taliseDevice->tx.txChannels = TAL_TXOFF;
    for(i=0; i<AD9379_ADC_NUM_COEFFS; i++)
    {
        if ( ( p_dest_taliseDevice->tx.txChannels == TAL_TXOFF ) &&
             ( p_src_taliseDevice->tx.txProfile.loopBackAdcProfile[i] > 0 ) )
        {
            debug_print("Enabling TX1TX2 for profile on card %u\n", p_id->card);
            p_dest_taliseDevice->tx.txChannels = TAL_TX1TX2;
        }
        p_dest_taliseDevice->tx.txProfile.loopBackAdcProfile[i] = p_src_taliseDevice->tx.txProfile.loopBackAdcProfile[i];
    }

    //set the last profile to NULL since this one is user generated
    _p_last_profile[p_id->card][tx_hdl_to_chip_id[p_id->hdl]] = NULL;

    return 0;
}


int32_t write_ad9528_regs( uint8_t card,
                        const ad9528_register_t *reg_buf,
                        uint16_t num_regs)
{

    uint32_t status;

    status = AD9528_registerOverride(&(_clockDevice[card]), reg_buf, num_regs);
    if (status != 0)
    {   //squash this error code
        status = -EBADMSG;
    }

    return status;

}
int32_t _read_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _rx_cal_mode[p_id->card][p_id->hdl];

    return (status);
}

int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode )
{
    int32_t status=0;

    _rx_cal_mode[p_id->card][p_id->hdl] = mode;

    return (status);
}

int32_t _run_rx_cal( rf_id_t *p_id )
{
    int32_t status=-ENODEV;
    uint64_t cal_freq;
    taliseDevice_t *p_rfic = NULL;

    p_rfic = rf_id_to_rficDevice( p_id );

    // Get the current freq and that's what we'll cal to
    if( TALISE_getRfPllFrequency( p_rfic,
                                  TAL_RF_PLL,
                                  &cal_freq ) == 0 )
    {
        status=_run_cal( p_id,
                         p_rfic,
                         cal_freq,
                         rx_hdl_to_chan_en[p_id->hdl],
                         TAL_TXOFF );
    }

    return (status);
}

int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    *p_cal_mask = _rx_cal_mask[p_id->card][p_id->hdl];

    return (status);
}

int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask )
{
    int32_t status=0;

    // validate the calibration mask
    if( (cal_mask & ~SKIQ_RX_CAL_TYPES) != 0 )
    {
        status = -EINVAL;
        skiq_error("Invalid calibration requested, only the following 0x%x is supported for card=%u / hdl=%s",
                  SKIQ_RX_CAL_TYPES, p_id->card, rx_hdl_cstr(p_id->hdl));
    }
    else
    {
        _rx_cal_mask[p_id->card][p_id->hdl] = cal_mask;
    }

    return (status);
}

int32_t _read_rx_cal_types_avail( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    // all handles support the same calibration type
    *p_cal_mask = SKIQ_RX_CAL_TYPES;

    return (status);
}

int32_t _convert_skiq_rx_cal_to_tal_cal( skiq_rx_hdl_t hdl, uint32_t skiq_rx_cal_mask, uint32_t *p_tal_cal_mask )
{
    int32_t status=0;
    int32_t tal_cal_mask = 0;

    switch( hdl )
    {
        case skiq_rx_hdl_A1:
        case skiq_rx_hdl_A2:
        case skiq_rx_hdl_B1:
        case skiq_rx_hdl_B2:
            // This maps to RX1/2 calibrations
            tal_cal_mask = TAL_RX_PHASE_CORRECTION; // we always want to do the phase correction for now
            if( (skiq_rx_cal_mask & skiq_rx_cal_type_dc_offset) != 0 )
            {
                tal_cal_mask |= TAL_DC_OFFSET;
            }
            if( (skiq_rx_cal_mask & skiq_rx_cal_type_quadrature) != 0 )
            {
                tal_cal_mask |= TAL_RX_QEC_INIT;
            }
            break;

        case skiq_rx_hdl_C1:
        case skiq_rx_hdl_D1:
            // ORx calibrations
            tal_cal_mask = TAL_ADC_STITCHING; // always do the stitching cal
            if( (skiq_rx_cal_mask & skiq_rx_cal_type_dc_offset) != 0 )
            {
                tal_cal_mask |= TAL_DC_OFFSET;
            }
            if( (skiq_rx_cal_mask & skiq_rx_cal_type_quadrature) != 0 )
            {
                tal_cal_mask |= TAL_ORX_QEC_INIT;
            }
            break;

        default:
            status = -ENODEV;
            break;
    }

    if( status == 0 )
    {
        *p_tal_cal_mask = tal_cal_mask;
    }

    return (status);
}


int32_t _write_tx_rx_enable_mode(rf_id_t *p_id, skiq_rfic_pin_mode_t mode)
{
    int32_t status=0;
    uint8_t pinCtrlMode_txrx = TAL_TXRX_ENABLE_MODE_SW;

    if((p_id->hdl == skiq_rx_hdl_C1 ) || (p_id->hdl == skiq_rx_hdl_D1 ))
    {
        status = -ENOTSUP;
    }

    if (mode == skiq_rfic_pin_control_fpga_gpio )
    {
        pinCtrlMode_txrx = TAL_TXRX_PIN_MODE;
    }
    else if (mode == skiq_rfic_pin_control_sw)
    {
        pinCtrlMode_txrx = TAL_TXRX_ENABLE_MODE_SW;
    }
    else
    {
        status = -EINVAL;
        skiq_error("Invalid skiq_rfic_pin_mode_t specified. Failed to set Radio Control Pin Mode with status %" PRIi32 " on card %"
                        PRIu8 "\n", status, p_id->card);
    }

    if (status == 0)
    {
        taliseDevice_t *p_rfic = NULL;
        rf_id_t rf_id_a = *p_id;
        rf_id_a.chip_id = rx_hdl_to_chip_id[p_id->hdl];



        p_rfic = rf_id_to_rficDevice( &rf_id_a );

        skiq_info("Setting Radio Control Pin Mode to use %s for RFIC %s on card %" PRIu8 "\n",
                 ((mode == skiq_rfic_pin_control_sw) ? "SW control" : "FPGA I/O" ),
                 ((rf_id_a.chip_id == RF_CHIP_A) ? "A" : "B") , p_id->card);

        status = TALISE_setRadioCtlPinMode( p_rfic, pinCtrlMode_txrx, TAL_ORX1ORX2_PAIR_NONE_SEL );
        if ( status != TALACT_NO_ACTION )
        {
            hal_critical_exit(-EBADMSG);
            status = -EBADMSG;
            skiq_error("Failed to set Radio Control Pin Mode with status %" PRIi32 " on card %"
                        PRIu8 "\n", status, p_id->card);
        }
        else
        {
            _rfic_pin_control_enable[p_id->card][rf_id_a.chip_id] = (pinCtrlMode_txrx == TAL_TXRX_PIN_MODE);
        }
    }
    return status;
}

static int32_t _read_tx_rx_enable_mode(rf_id_t *p_id, skiq_rfic_pin_mode_t *p_mode)
{
    int32_t status=0;
    taliseDevice_t *p_rfic = NULL;
    rf_id_t rf_id_a = *p_id;
    rf_id_a.chip_id = rx_hdl_to_chip_id[p_id->hdl];

    CHECK_NULL_PARAM(p_mode);

    if((p_id->hdl == skiq_rx_hdl_C1 ) || (p_id->hdl == skiq_rx_hdl_D1 ))
    {
        status = -ENOTSUP;
    }

    if (status == 0)
    {
        p_rfic = rf_id_to_rficDevice( &rf_id_a );
        uint8_t pinCtrlMode_txrx;
        taliseRadioCtlCfg2_t pinCtrlMode_orx;
        status = TALISE_getRadioCtlPinMode( p_rfic, &pinCtrlMode_txrx, &pinCtrlMode_orx );
        if ( status != TALACT_NO_ACTION )
        {
            status = -EBADMSG;
            skiq_error("Failed to get Radio Control Pin Mode with status %" PRIi32 " on card %"
                        PRIu8 "\n", status, p_id->card);
        }
        else
        {
            *p_mode = ((pinCtrlMode_txrx & TAL_TXRX_PIN_MODE) > 0) ? skiq_rfic_pin_control_fpga_gpio :skiq_rfic_pin_control_sw ;
        }
    }

    return status;
}

static int32_t _setRxTxEnable(const rf_id_t *p_id, taliseDevice_t *device, taliseRxORxChannels_t rxOrxChannel, taliseTxChannels_t txChannel)
{

    int32_t status = 0;

    /* Check to see if any enables are controlled by pins.  If so, the FPGA must enable them, since TALISE_setRxTxEnable
       wont do anything on that handle */
    if (_rfic_pin_control_enable[p_id->card][rx_hdl_to_chip_id[p_id->hdl]])
    {
        uint32_t val=0;
        uint8_t tx_addr_offset = (tx_hdl_to_chip_id[p_id->hdl] == RF_CHIP_A) ? FMC_TX_ENABLE_OFFSET : FMC_TX_B_ENABLE_OFFSET;
        uint8_t rx_addr_offset = (rx_hdl_to_chip_id[p_id->hdl] == RF_CHIP_A) ? FMC_RX_ENABLE_OFFSET : FMC_RX_B_ENABLE_OFFSET;

        status = sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
        if (status == 0)
        {
            switch(rxOrxChannel)
            {
                case TAL_RXOFF_EN:
                case TAL_RX1RX2_EN:  //intentional fallthrough
                    BF_SET(val, TAL_RX1RX2_EN, rx_addr_offset,FMC_RX_ENABLE_LEN);
                    break;
                case TAL_RX1_EN: //Mind the Rx1/Rx2 swap
                    BF_SET(val, 2, rx_addr_offset,FMC_RX_ENABLE_LEN);
                    break;
                case TAL_RX2_EN: //Mind the Rx1/Rx2 swap
                    BF_SET(val, 1, rx_addr_offset,FMC_RX_ENABLE_LEN);
                    break;
                default:
                    status = -EINVAL;
                    skiq_error("Invalid rx channel specified. (card=%u, chip=%u, rxOrxChannel=%u)\n",
                               p_id->card, p_id->chip_id, rxOrxChannel );
                    break;
            }

            switch(txChannel)
            {
                case TAL_TXOFF:
                    break;
                case TAL_TX1:  //intentional fallthrough
                case TAL_TX2:
                case TAL_TX1TX2:
                    BF_SET(val, txChannel, tx_addr_offset,FMC_TX_ENABLE_LEN);
                    break;
                default:
                    status = -EINVAL;
                    skiq_error("Invalid tx channel specified. (card=%u, chip=%u, txChannel=%u)\n",
                               p_id->card, p_id->chip_id, txChannel);
                    break;
            }

            if (status == 0)
            {
                status = sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_FMC_CTRL, val );
            }

        }
    }
    else if (TALISE_setRxTxEnable( device, rxOrxChannel, txChannel) != TALACT_NO_ACTION )
    {
        skiq_warning("Unable to enable channels (card=%u, chip=%u)\n", p_id->card, p_id->chip_id);
        status = -EBADMSG;
    }

    return status;
}

static int32_t _getRxTxEnable(const rf_id_t *p_id, taliseDevice_t *device, taliseRxORxChannels_t *rxOrxChannel, taliseTxChannels_t *txChannel)
{
    int32_t status = 0;
    uint32_t val = 0;

    /* Use the rx_hdl_to_chip_id[] look-up since p_id->hdl could be greater than skiq_tx_hdl_B2 if
       it is referring to something like skiq_rx_hdl_C1 or D1.  Further, this only works because the
       first four entries of rx_hdl_to_chip_id[] and tx_hdl_to_chip_id[] match.  Ideally rf_id_t
       would track skiq_rx_hdl_t and skiq_tx_hdl_t (and chip_id) separately so this code could know
       if p_id represents a receive channel or transmit channel */
    uint8_t chip_id = rx_hdl_to_chip_id[p_id->hdl];

    uint8_t tx_addr_offset = (chip_id == RF_CHIP_A) ? FMC_TX_ENABLE_OFFSET : FMC_TX_B_ENABLE_OFFSET;
    uint8_t rx_addr_offset = (chip_id == RF_CHIP_A) ? FMC_RX_ENABLE_OFFSET : FMC_RX_B_ENABLE_OFFSET;

    if (_rfic_pin_control_enable[p_id->card][chip_id])
    {
        status = sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
        if (status == 0)
        {
            if ( BF_GET(val,rx_addr_offset,FMC_RX_ENABLE_LEN) == TAL_RX1RX2_EN)
            {
                *rxOrxChannel = TAL_RX1RX2_EN;
            }
            else if (BF_GET(val,rx_addr_offset,FMC_RX_ENABLE_LEN) == 1)  //watch out for a1/a2 swap
            {
                *rxOrxChannel = TAL_RX2_EN;
            }
            else if ( BF_GET(val,rx_addr_offset,FMC_RX_ENABLE_LEN) == 2) //watch out for a1/a2 swap
            {
                *rxOrxChannel = TAL_RX1_EN;
            }
            else
            {
                *rxOrxChannel = TAL_RXOFF_EN;
            }


            *txChannel = BF_GET(val,tx_addr_offset,FMC_TX_ENABLE_LEN);
        }
    }
    else
    {
        status = TALISE_getRxTxEnable( device,
                                       rxOrxChannel,
                                       txChannel );
        if (status != TALACT_NO_ACTION)
        {
            status = -EBADMSG;
        }
    }

    return status;
}


/**************************************************************************************************/
int32_t _read_temp( rf_id_t *p_id, int8_t *p_temp_in_deg_C )
{
    int32_t status = 0;
    int16_t temperature;

    if ( !rfic_active[p_id->card] )
    {
        /* if the RFIC is not active, assume that the ARM is also not available (which services the
         * temperature request) */
        status = -ENOSYS;
    }

    if ( status == 0 )
    {
        uint32_t talise_status;

        talise_status = TALISE_getTemperature( rf_id_to_rficDevice( p_id ), &temperature );
        if ( talise_status != TALACT_NO_ACTION )
        {
            status = -EIO;
        }
    }

    if ( status == 0 )
    {
        debug_print("RFIC temperature is %" PRIi16 "\n", temperature);

        temperature = MIN(temperature, INT8_MAX);
        temperature = MAX(temperature, INT8_MIN);

        *p_temp_in_deg_C = (int8_t)temperature;
        debug_print("Reporting RFIC temperature as %" PRIi8 "\n", *p_temp_in_deg_C);
    }

    return status;
}


/**************************************************************************************************/
int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = AD9379_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = AD9379_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = AD9379_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = AD9379_MAX_RX_FREQUENCY_IN_HZ;


    return (status);
}

// CDR=clock data recovery...this is done by ADI and is recommended for JESD syncing (see https://github.com/analogdevicesinc/no-OS/blob/master/projects/adrv9009/src/app/app_talise.c...this is done after enabling SYSREF)
int32_t _reset_cdr( uint8_t card )
{
    uint8_t phy_ctrl_a;
    uint8_t phy_ctrl_b;
    int32_t status=0;
    
#define TALISE_ADDR_DES_PHY_GENERAL_CTL_1 (0x187E) // defined "talise_reg_addr_macros.h"
                
    status=hal_rfic_read_reg( card, RFIC_SPI_CS_AD9379_A, TALISE_ADDR_DES_PHY_GENERAL_CTL_1, &phy_ctrl_a );

    if( status == 0 )
    {
        status = hal_rfic_read_reg( card, RFIC_SPI_CS_AD9379_B, TALISE_ADDR_DES_PHY_GENERAL_CTL_1, &phy_ctrl_b );
    }

    // clear bit 7
    if( status == 0 )
    {
        status = hal_rfic_write_reg( card, RFIC_SPI_CS_AD9379_A, TALISE_ADDR_DES_PHY_GENERAL_CTL_1, phy_ctrl_a & (~0x80));
    }
    if( status == 0 )
    {
        status = hal_rfic_write_reg( card, RFIC_SPI_CS_AD9379_B, TALISE_ADDR_DES_PHY_GENERAL_CTL_1, phy_ctrl_b & (~0x80));
    }
                
    if( status == 0 )
    {
        status = hal_rfic_write_reg( card, RFIC_SPI_CS_AD9379_A, TALISE_ADDR_DES_PHY_GENERAL_CTL_1, phy_ctrl_a );
    }
    if( status == 0 )
    {
        status = hal_rfic_write_reg( card, RFIC_SPI_CS_AD9379_B, TALISE_ADDR_DES_PHY_GENERAL_CTL_1, phy_ctrl_b );
    }

    return (status);
}


static skiq_rx_hdl_t _rx_hdl_map_other(rf_id_t *p_id)
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    return (                                            
            ((hdl) == skiq_rx_hdl_A1) ? skiq_rx_hdl_A2 :
            ((hdl) == skiq_rx_hdl_A2) ? skiq_rx_hdl_A1 :
            ((hdl) == skiq_rx_hdl_B1) ? skiq_rx_hdl_B2 :
            ((hdl) == skiq_rx_hdl_B2) ? skiq_rx_hdl_B1 : 
            skiq_rx_hdl_end);
}

static skiq_tx_hdl_t _tx_hdl_map_other(rf_id_t *p_id)
{
    skiq_tx_hdl_t hdl = p_id->hdl;

    return (((hdl) == skiq_tx_hdl_A1) ? skiq_tx_hdl_A2 :
            ((hdl) == skiq_tx_hdl_A2) ? skiq_tx_hdl_A1 :
            ((hdl) == skiq_tx_hdl_B1) ? skiq_tx_hdl_B2 :
            ((hdl) == skiq_tx_hdl_B2) ? skiq_tx_hdl_B1 :
            skiq_tx_hdl_end);
}
