/*****************************************************************************/
/** @file rfe_z2_b.c

 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/


/***** INCLUDES *****/

#include <stdlib.h>
#include <string.h>

#include "rfe_z2_b.h"
#include "sidekiq_hal.h"
#include "ad9361_driver.h"
#include "io_z2.h"
#include "io_expander_tca6408.h"
#include "card_services.h"
#include "gpio_sysfs.h"

/* enable debug_print and debug_print_plain when DEBUG_RFE_Z2 is defined */
#if (defined DEBUG_RFE_Z2)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/***** DEFINES *****/

// IO Expander U20 
#define U20_RX_ANT_1_MASK               0x80 // P7
#define U20_RX_ANT_2_MASK               0x40 // P6
#define U20_LNA_BIAS1_MASK              0x20 // P5
#define U20_LNA_BIAS2_MASK              0x10 // P4
#define U20_LNA_BIAS3_MASK              0x08 // P3
#define U20_BAND_0_2_3_MASK             0x04 // P2
#define U20_BAND_0_MASK                 0x02 // P1
#define U20_BAND_1_MASK                 0x01 // P0

// IO Expander U21
#define U21_BAND_2_MASK                 0x80 // P7
#define U21_BAND_3_MASK                 0x40 // P6
#define U21_BAND_4_MASK                 0x20 // P5
#define U21_LNA_SW_MASK                 0x10 // P4
#define U21_TX_BAND_1_MASK              0x08 // P3
#define U21_TX_BAND_2_MASK              0x04 // P2
#define U21_RFIC_RESET_N_MASK           0x02 // P1
#define U21_EN_EXT_I2C_3V3_N_MASK       0x01 // P0

// generic GPIO access
#define GPIO_CLASS (906)

#define VARIANT_RF_PORT_INDEX (1)
#define BOTTOM_PORT_VARIANT ('0')
#define TOP_PORT_VARIANT ('1')

// Zynq Technical Reference Manual v1.11, Ch. 14 (p. 381)
#define EMIO_OFFSET (54) // 0-53 are MIO GPIOs...EMIO starts at 54
// Related to our Zynq config...our FPGA design allocates the EMIO to pin config
#define TDD_GPIO_EMIO_OFFSET (0) // GPIO[0] of Bank 2 is our TDD control
#define TDD_GPIO_ABS_OFFSET (EMIO_OFFSET+TDD_GPIO_EMIO_OFFSET)
#define TDD_GPIO_TRANSMIT (0)
#define TDD_GPIO_RECEIVE (1)

/***** MACROS ******/
#define IS_RANGE(arg, low, high)    \
    (((arg) > (low)) && ((arg) <= (high)))

/***** ENUMS *****/

typedef enum
{
    rx_antenna_isolation = 0,
    rx_antenna_1,   // J3
    rx_antenna_2,   // J2
    rx_antenna_tdd, // J1/J300
} rx_antenna_t;

typedef struct
{
    skiq_rf_port_t skiq_rf_port;
    rx_antenna_t z2_ant_sel;
} z2_rf_config_t;

typedef enum
{
    pin_0,
    pin_hiz,
    pin_1,
} pin_state_t;

typedef struct
{
    pin_state_t bias1;
    pin_state_t bias2;
    pin_state_t bias3;
} lna_bias_pin_state_t;

/***** LOCAL DATA *****/

static skiq_filt_t _rx_filters[] = {
    skiq_filt_0_to_3000_MHz,
    skiq_filt_50_to_435MHz,
    skiq_filt_435_to_910MHz,
    skiq_filt_910_to_1950MHz,
    skiq_filt_1950_to_6000MHz,
};

static skiq_filt_t _tx_filters[] = {
    skiq_filt_0_to_3000_MHz,
    skiq_filt_3000_to_6000_MHz,
};

#define Z2_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define Z2_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define Z2_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define Z2_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)


static const z2_rf_config_t _rx_rf_ports[] = {
    {skiq_rf_port_J3, rx_antenna_1},
    {skiq_rf_port_J2, rx_antenna_2},
};

// These pin states are from the bias current settings described in neptune_i2c_settings_REV_C.ods
// located in /mnt/storage/projects/neptune/docs/
static const lna_bias_pin_state_t _lna_bias_config[Z2_BIAS_INDEX_MAX+1] = {
    {pin_0, pin_0, pin_0},          // index 0 -- not valid...turns it off
    {pin_0, pin_0, pin_1},          // index 1
    {pin_0, pin_0, pin_hiz},        // index 2
    {pin_0, pin_1, pin_0},          // index 3
    {pin_0, pin_1, pin_1},          // index 4
    {pin_0, pin_1, pin_hiz},        // index 5
    {pin_0, pin_hiz, pin_0},        // index 6
    {pin_0, pin_hiz, pin_1},        // index 7
    {pin_0, pin_hiz, pin_hiz},      // index 8
    {pin_1, pin_0, pin_0},          // index 9
    {pin_1, pin_0, pin_1},          // index 10
    {pin_1, pin_0, pin_hiz},        // index 11
    {pin_1, pin_1, pin_0},          // index 12
    {pin_1, pin_1, pin_1},          // index 13
    {pin_1, pin_1, pin_hiz},        // index 14
    {pin_1, pin_hiz, pin_0},        // index 15
    {pin_1, pin_hiz, pin_1},        // index 16
    {pin_1, pin_hiz, pin_hiz},      // index 17
    {pin_hiz, pin_0, pin_0},        // index 18
    {pin_hiz, pin_0, pin_1},        // index 19
    {pin_hiz, pin_0, pin_hiz},      // index 20
    {pin_hiz, pin_1, pin_0},        // index 21
    {pin_hiz, pin_1, pin_1},        // index 22
    {pin_hiz, pin_1, pin_hiz},      // index 23
    {pin_hiz, pin_hiz, pin_0},      // index 24
    {pin_hiz, pin_hiz, pin_1},      // index 25
    {pin_hiz, pin_hiz, pin_hiz}     // index 26    
};

static const z2_rf_config_t _rx_isolation = {skiq_rf_port_unknown, rx_antenna_isolation};

static uint8_t _num_rx_rf_ports[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = sizeof(_rx_rf_ports)/sizeof(z2_rf_config_t),
};
// Current RF config
static z2_rf_config_t _curr_rx_rf_port[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = {skiq_rf_port_J3, rx_antenna_1}
};

static skiq_filt_t curr_rx_filt[SKIQ_MAX_NUM_CARDS] = {
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = skiq_filt_invalid,
};

static bool _use_top_port[SKIQ_MAX_NUM_CARDS] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = false
};

static uint8_t _bias_index[SKIQ_MAX_NUM_CARDS] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = Z2_BIAS_INDEX_DEFAULT
};

static rfe_lna_state_t _lna_state[SKIQ_MAX_NUM_CARDS] = {
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = lna_disabled
};

/***** LOCAL FUNCTIONS *****/
static bool _is_rx_lna_enabled( rf_id_t *p_id );

static int32_t _disable_tx_filter_path( rf_id_t *p_id );

static int32_t _write_rx_antenna_port( rf_id_t *p_id,
                                       z2_rf_config_t *p_rf_config );

static void _update_pin_state( pin_state_t pin_state,
                               uint8_t pin_pos,
                               uint8_t *p_out_mask,
                               uint8_t *p_out_val,
                               uint8_t *p_hiz_mask,
                               uint8_t *p_hiz_val );

//////////////////////////////
// RFE function pointers
static int32_t _init( rf_id_t *p_id );

static int32_t _update_rx_lna( rf_id_t *p_id,
                               rfe_lna_state_t new_state );

static int32_t _update_tx_lo( rf_id_t *p_id,
                              uint64_t tx_lo_freq );

static int32_t _write_rx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t path );

static int32_t _write_tx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t path );

static int32_t _read_rx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t *p_path );

static int32_t _read_tx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t *p_path );

static int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters );

static int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters );

static int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list);

static int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );

static int32_t _write_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t rf_port );

static int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );

static int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );

static int32_t _rf_port_config_avail( rf_id_t *p_id, bool *p_fixed, bool *p_trx );
static int32_t _write_rf_port_config( rf_id_t *p_id, skiq_rf_port_config_t config );
static int32_t _read_rf_port_operation( rf_id_t *p_id, bool *p_transmit );
static int32_t _write_rf_port_operation( rf_id_t *p_id, bool transmit );

static int32_t _read_bias_index( rf_id_t *p_id, uint8_t *p_bias_index );
static int32_t _write_bias_index( rf_id_t *p_id, uint8_t bias_index );

static int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );
static int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );
static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

// end RFE function pointers
//////////////////////////////

static skiq_filt_t _find_rx_filt( uint64_t rx_lo_freq );
static skiq_filt_t _find_tx_filt( uint64_t tx_lo_freq );

/*****************************************************************************/
/** @brief Initializes the RFE for the Z2 card.

    @param p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _init( rf_id_t *p_id )
{
    uint8_t slave[2] = { U20_IO_EXPANDER_ADDR, U21_IO_EXPANDER_ADDR };
    int32_t status = 0;
    uint8_t n = 0;
    skiq_param_t params;
    
    TRACE();

    // invalidate the filter setting
    curr_rx_filt[p_id->card] = skiq_filt_invalid;

    // The below loop forces the IO expanders to their default state following
    // a power cycle. Z2 should be designed to handle this case quite well.
    for( n = 0; (n < sizeof(slave)) && (0 == status); n++ )
    {
        /* set all pins as inputs, also sets output values to zero */
        status = tca6408_io_exp_set_as_input( p_id->card, slave[n], IO_EXPANDER_ALL_PINS );

        if( 0 == status )
        {
            /* set all pins to no inversion (0) */
            status = tca6408_io_exp_set_pin_polarity( p_id->card, slave[n], IO_EXPANDER_ALL_PINS, 0 );
        }
    }

    // get the default bias index
    if( 0 == status )
    {
        // if we don't read the configured value successfully, just use the default
        if( card_read_bias_index_calibration( p_id->card, &(_bias_index[p_id->card]) ) != 0 )
        {
            _bias_index[p_id->card] = Z2_BIAS_INDEX_DEFAULT;
        }
        else
        {
            if( (_bias_index[p_id->card] < Z2_BIAS_INDEX_MIN) ||
                 (_bias_index[p_id->card] > Z2_BIAS_INDEX_MAX) )
            {
                _skiq_log(SKIQ_LOG_ERROR, "Invalid bias current index of %u read for card %u\n",
                          _bias_index[p_id->card], p_id->card);
                return (-ERANGE);
            }
        }
    }

    if( 0 == status )
    {
        // disable the LNA initially
        status = _update_rx_lna(p_id, lna_disabled);
    }

    if( 0 == status )
    {
        // default Rx antenna to isolation state
        status = _write_rx_antenna_port(p_id, (z2_rf_config_t*)(&_rx_isolation));        
    }

    if( 0 == status )
    {
        // enable the TDD GPIO and configure as an output with TX enabled by default
        if( (status = gpio_sysfs_enable_ctrl(p_id->card, GPIO_CLASS, TDD_GPIO_ABS_OFFSET)) == 0 )
        {
            if( (status = gpio_sysfs_configure_direction(p_id->card, GPIO_CLASS, TDD_GPIO_ABS_OFFSET, gpio_ddr_out)) == 0 )
            {
                status = gpio_sysfs_write_value( p_id->card, GPIO_CLASS, TDD_GPIO_ABS_OFFSET, TDD_GPIO_TRANSMIT );
            }
        }
    }

    // set the top/bottom port flag based on the variant
    if( status == 0 )
    {
        skiq_read_parameters( p_id->card, &params );
        if( params.card_param.part_info.variant_string[VARIANT_RF_PORT_INDEX] == TOP_PORT_VARIANT )
        {
            _use_top_port[p_id->card] = true;
        }
        else if( params.card_param.part_info.variant_string[VARIANT_RF_PORT_INDEX] == BOTTOM_PORT_VARIANT )
        {
            _use_top_port[p_id->card] = false;
        }
        else
        {
            _skiq_log(SKIQ_LOG_ERROR, "Invalid variant detected (%s)",
                      params.card_param.part_info.variant_string);
            status = -EINVAL;
        }
    }

    return status;
}

/*****************************************************************************/
/** @brief Determines if the Rx LNA is enabled or not.

    @param p_id Pointer to RF identifier struct.

    @return bool True if enabled, false otherwise.
*/
bool _is_rx_lna_enabled( rf_id_t *p_id )
{
    bool enabled=true;

    TRACE();

    if( _lna_state[p_id->card] == lna_disabled )
    {
        enabled = false;
    }

    return (enabled);
}

/*****************************************************************************/
/** @brief Determines if the Rx LNA is bypassed or not.

    @param p_id Pointer to RF identifier struct.

    @return bool True if bypassed, false otherwise.
*/
static bool _is_rx_lna_bypassed( rf_id_t *p_id )
{
    bool bypassed=false;

    TRACE();

    if( _lna_state[p_id->card] == lna_bypassed )
    {
        bypassed = true;
    }
    
    /* if LNA_SW is set, consider the LNA to be bypassed */
    return (bypassed);
}


/**************************************************************************************************/
/* Enables the RX LNA associated with the referenced rf_id_t */
static int32_t _enable_rx_lna( rf_id_t *p_id )
{
    int32_t status = 0;
    uint8_t hiz_mask = 0;
    uint8_t hiz_val = 0;
    uint8_t out_mask = 0;
    uint8_t out_val = 0;
    // grab the pin state to configure based on the bias current index
    lna_bias_pin_state_t pin_state = _lna_bias_config[_bias_index[p_id->card]];

    TRACE();

    _update_pin_state( pin_state.bias1,
                       U20_LNA_BIAS1_MASK,
                       &out_mask,
                       &out_val,
                       &hiz_mask,
                       &hiz_val );
    _update_pin_state( pin_state.bias2,
                       U20_LNA_BIAS2_MASK,
                       &out_mask,
                       &out_val,
                       &hiz_mask,
                       &hiz_val );
    _update_pin_state( pin_state.bias3,
                       U20_LNA_BIAS3_MASK,
                       &out_mask,
                       &out_val,
                       &hiz_mask,
                       &hiz_val );

    if( (tca6408_io_exp_set_as_hiz( p_id->card, U20_IO_EXPANDER_ADDR, hiz_mask, hiz_val ) != 0) ||
        (tca6408_io_exp_set_as_output( p_id->card, U20_IO_EXPANDER_ADDR, out_mask, out_val ) != 0) )
    {
        status = -EIO;
    }
    else
    {
        _lna_state[p_id->card] = lna_enabled;
    }

    return (status);
}

void _update_pin_state( pin_state_t pin_state,
                        uint8_t pin_pos,
                        uint8_t *p_out_mask,
                        uint8_t *p_out_val,
                        uint8_t *p_hiz_mask,
                        uint8_t *p_hiz_val )
{
    if( pin_state == pin_hiz )
    {
        *(p_hiz_mask) |= pin_pos;
        *(p_hiz_val) |= pin_pos;
    }
    else if( pin_state == pin_1 )
    {
        *(p_out_mask) |= pin_pos;
        *(p_out_val) |= pin_pos;
    }
    else
    {
        *(p_out_mask) |= pin_pos;
        *(p_out_val) &= ~pin_pos;
    }
}


/**************************************************************************************************/
/* Disables the RX LNA associated with the referenced rf_id_t */
static int32_t _disable_rx_lna( rf_id_t *p_id )
{
    int32_t status = 0;
    uint8_t pins = U20_LNA_BIAS1_MASK | U20_LNA_BIAS2_MASK | U20_LNA_BIAS3_MASK;

    TRACE();

    /* Set all 3 pins low to disable the LNA */
    status = tca6408_io_exp_set_as_output( p_id->card, U20_IO_EXPANDER_ADDR, pins, 0 );
    if( status == 0 )
    {
        _lna_state[p_id->card] = lna_disabled;
    }

    return (status);
}


/**************************************************************************************************/
/* Bypasses (bypass=true) or switches in (bypass=false) the RX LNA associated
 * with the referenced rf_id_t */
static int32_t _bypass_rx_lna( rf_id_t *p_id,
                               bool bypass )
{
    int32_t status = 0;
    uint8_t pins = U21_LNA_SW_MASK;

    TRACE();

    /* Set LNA_SW high to bypass the LNA */
    status = tca6408_io_exp_set_as_output( p_id->card, U21_IO_EXPANDER_ADDR, pins, bypass ? pins : 0 );
    if( status == 0 )
    {
        _lna_state[p_id->card] = lna_bypassed;
    }

    return (status);
}

/*****************************************************************************/
/** @brief Enables / disables / bypasses the Rx low noise amplifier.

    @param p_id Pointer to RF identifier struct.
    @param enable True to enable, false to disable.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _update_rx_lna( rf_id_t *p_id,
                        rfe_lna_state_t new_state )
{
    int32_t status;

    TRACE();

    switch (new_state)
    {
        case lna_enabled:
            /* switch in the LNA AFTER enabling the LNA if it's bypassed */
            status = _enable_rx_lna( p_id );
            if ( ( 0 == status ) && _is_rx_lna_bypassed( p_id ) )
            {
                status = _bypass_rx_lna( p_id, false );
            }
            break;

        case lna_disabled:
            /* switch in the LNA AFTER disabling the LNA if it's bypassed */
            status = _disable_rx_lna( p_id );
            if ( ( 0 == status ) && _is_rx_lna_bypassed( p_id ) )
            {
                status = _bypass_rx_lna( p_id, false );
            }
            break;

        case lna_bypassed:
            /* disable the LNA AFTER bypassing to conserve power if enabled */
            status = _bypass_rx_lna( p_id, true );
            if ( ( 0 == status ) && _is_rx_lna_enabled( p_id ) )
            {
                status = _disable_rx_lna( p_id );
            }
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}


/*****************************************************************************/
/** @brief Updates the radio front end before tuning to the given Rx LO frequency.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
_update_before_rx_lo_tune( rf_id_t *p_id,
                           uint64_t rx_lo_freq )
{
    return _update_rx_lna(p_id, lna_disabled);
}


/*****************************************************************************/
/** @brief Updates the radio front end after tuning to a given Rx LO frequency.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
_update_after_rx_lo_tune( rf_id_t *p_id,
                          uint64_t rx_lo_freq )
{
    skiq_filt_t filt = skiq_filt_invalid;
    int32_t status = 0;

    TRACE();

    filt = _find_rx_filt( rx_lo_freq );
    if( filt == skiq_filt_invalid )
    {
        status = -ERANGE;
    }

    if( 0 == status )
    {
        debug_print("filter selected: %s\n", SKIQ_FILT_STRINGS[filt]);
        if ( curr_rx_filt[p_id->card] != filt )
        {
            debug_print("filter differs from current, pushing down change from %s to %s\n",
                        SKIQ_FILT_STRINGS[curr_rx_filt[p_id->card]], SKIQ_FILT_STRINGS[filt]);
            status = _write_rx_filter_path(p_id, filt);
            if( status == 0 )
            {
                curr_rx_filt[p_id->card] = filt;
            }
        }
    }

    if( 0 == status )
    {
        /*
            A register transaction here is inevitable: either we're checking the LNA state or
            we're enabling the LNA state, so might as well just enable it.
        */
        status = _update_rx_lna(p_id, lna_enabled);
    }

    return status;
}

/*****************************************************************************/
/** @brief Updates the radio front end according to a given Tx LO frequency.

    @param p_id Pointer to RF identifier struct.
    @param tx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _update_tx_lo( rf_id_t *p_id,
                       uint64_t tx_lo_freq )
{
    skiq_filt_t filt = skiq_filt_invalid;
    int32_t status = 0;

    TRACE();

    filt = _find_tx_filt( tx_lo_freq );
    if( filt == skiq_filt_invalid )
    {
        status = -ERANGE;
    }
    
    if( 0 == status )
    {
        status = _write_tx_filter_path(p_id, filt);
    }

    return status;
}

/*****************************************************************************/
/** @brief Configures the Z2's Rx filter configuration.

    @param p_id Pointer to RF identifier struct.
    @param path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _write_rx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    uint8_t ad9361_id = p_id->chip_id;
    uint8_t ad9361_port = 0;
    uint8_t u20_pins = U20_BAND_0_2_3_MASK | U20_BAND_0_MASK | U20_BAND_1_MASK;
    uint8_t u21_pins = U21_BAND_2_MASK | U21_BAND_3_MASK | U21_BAND_4_MASK;
    uint8_t u20_val = 0;
    uint8_t u21_val = 0;
    int32_t status = 0;
    bool reenable_lna = false;

    TRACE();

    switch(path)
    {
    case( skiq_filt_0_to_3000_MHz ):
        u20_val = U20_BAND_0_2_3_MASK | U20_BAND_0_MASK;
        ad9361_port = ad9361_rx_port_c;
        break;
    case( skiq_filt_50_to_435MHz ):
        u20_val = U20_BAND_1_MASK;
        ad9361_port = ad9361_rx_port_b;
        break;
    case( skiq_filt_435_to_910MHz ):
        u20_val = U20_BAND_0_2_3_MASK;
        u21_val = U21_BAND_2_MASK;
        ad9361_port = ad9361_rx_port_c;
        break;
    case( skiq_filt_910_to_1950MHz ):
        u20_val = U20_BAND_0_2_3_MASK;
        u21_val = U21_BAND_3_MASK;
        ad9361_port = ad9361_rx_port_c;
        break;
    case( skiq_filt_1950_to_6000MHz ):
        u21_val = U21_BAND_4_MASK;
        ad9361_port = ad9361_rx_port_a;
        break;
    default:
        status = -1;
    }

    if( _is_rx_lna_enabled(p_id) )
    {
        // disable LNA
        reenable_lna = true;
        status = _update_rx_lna(p_id, lna_disabled);
    }

    if( 0 == status )
    {
        // engage isolation state
        status = _write_rx_antenna_port(p_id, (z2_rf_config_t*)(&_rx_isolation));
    }

    if( 0 == status )
    {
        // write the input port of the AD9361
        status = ad9361_write_rx_input_port(ad9361_id, ad9361_port);
    }

    /* set U20 pins as outputs with values according to u20_val */
    if( 0 == status )
    {
        status = tca6408_io_exp_set_as_output( p_id->card, U20_IO_EXPANDER_ADDR, u20_pins, u20_val );
    }

    /* set U21 pins as outputs with values according to u21_val */
    if( 0 == status )
    {
        status = tca6408_io_exp_set_as_output( p_id->card, U21_IO_EXPANDER_ADDR, u21_pins, u21_val );
    }

    if( 0 == status )
    {
        // enable the cached antenna
        status = _write_rx_antenna_port(p_id, &(_curr_rx_rf_port[p_id->card]));
    }

    if( (0 == status) && (reenable_lna) )
    {
        status = _update_rx_lna(p_id, lna_enabled);
    }

    return status;
}

/*****************************************************************************/
/** @brief Configures the Z2's Tx filter configuration.

    @param p_id Pointer to RF identifier struct.
    @param path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _write_tx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    uint8_t ad9361_id = p_id->chip_id;
    uint8_t ad9361_port = 0;
    uint8_t pins = U21_TX_BAND_1_MASK | U21_TX_BAND_2_MASK;
    uint8_t val = 0;
    int32_t status = 0;

    switch(path)
    {
    case( skiq_filt_0_to_3000_MHz ):
        val = U21_TX_BAND_1_MASK;
        ad9361_port = ad9361_tx_port_b;
        break;
    case( skiq_filt_3000_to_6000_MHz ):
        val = U21_TX_BAND_2_MASK;
        ad9361_port = ad9361_tx_port_a;
        break;
    default:
        status = -1;
    }

    /* set 'pins' as output with values from 'val' */
    if( 0 == status )
    {
        status = tca6408_io_exp_set_as_output( p_id->card, U21_IO_EXPANDER_ADDR, pins, val );
    }

    if( 0 == status )
    {
        // write the input port of the AD9361
        status = ad9361_write_tx_output_port(ad9361_id, ad9361_port);
    }

    return status;
}

/*****************************************************************************/
/** @brief Obtains the Z2's current Rx filter configuration.

    @param p_id Pointer to RF identifier struct.
    @param p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _read_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    uint8_t u20_mask = U20_BAND_0_2_3_MASK | U20_BAND_0_MASK | U20_BAND_1_MASK;
    uint8_t u21_mask = U21_BAND_2_MASK | U21_BAND_3_MASK | U21_BAND_4_MASK;
    uint8_t u20_val = 0;
    uint8_t u21_val = 0;
    int32_t status = 0;

    TRACE();

    status = tca6408_io_exp_read_output( p_id->card, U20_IO_EXPANDER_ADDR, &u20_val );
    if( 0 == status )
    {
        status = tca6408_io_exp_read_output( p_id->card, U21_IO_EXPANDER_ADDR, &u21_val );
    }

    if( 0 == status )
    {
        u20_val &= u20_mask;
        u21_val &= u21_mask;

        if( u20_val & U20_BAND_0_MASK )
        {
            *p_path = skiq_filt_0_to_3000_MHz;
        }
        else if( u20_val & U20_BAND_1_MASK )
        {
            *p_path = skiq_filt_50_to_435MHz;
        }
        else if( u21_val & U21_BAND_2_MASK )
        {
            *p_path = skiq_filt_435_to_910MHz;
        }
        else if( u21_val & U21_BAND_3_MASK )
        {
            *p_path = skiq_filt_910_to_1950MHz;
        }
        else if( u21_val & U21_BAND_4_MASK )
        {
            *p_path = skiq_filt_1950_to_6000MHz;
        }
        else
        {
            *p_path = skiq_filt_invalid;
            status = -EINVAL;
        }
    }

    return status;
}

/*****************************************************************************/
/** @brief Obtain's the Z2's current Tx filter configuration.

    @param p_id Pointer to RF identifier struct.
    @param p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _read_tx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    uint8_t mask = U21_TX_BAND_1_MASK | U21_TX_BAND_2_MASK;
    uint8_t val = 0;
    int32_t status = 0;

    TRACE();

    status = tca6408_io_exp_read_output( p_id->card, U21_IO_EXPANDER_ADDR, &val );
    if( 0 == status )
    {
        val &= mask;

        if( val & U21_TX_BAND_1_MASK )
        {
            *p_path = skiq_filt_0_to_3000_MHz;
        }
        else if( val & U21_TX_BAND_2_MASK )
        {
            *p_path = skiq_filt_3000_to_6000_MHz;
        }
        else
        {
            status = -EINVAL;
        }
    }

    return status;
}

/*****************************************************************************/
/** @brief Disables the Tx filter path on Z2. This should be done whenever
    Tx is not being performed.

    @param p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _disable_tx_filter_path( rf_id_t *p_id )
{
    uint8_t pins = U21_TX_BAND_1_MASK | U21_TX_BAND_2_MASK;
    int32_t status = 0;

    TRACE();

    /* set both TX band pins as low outputs */
    status = tca6408_io_exp_set_as_output( p_id->card, U21_IO_EXPANDER_ADDR, pins, 0 );

    return status;
}

/*****************************************************************************/
/** @brief Obtain's the Z2's available Rx filters.

    @param p_id Pointer to RF identifier struct.
    @param p_filters Pointer to be updated with available filters.
    @param p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status = 0;

    *p_num_filters = sizeof(_rx_filters) / sizeof(_rx_filters[0]);
    memcpy(p_filters, _rx_filters, sizeof(_rx_filters));

    return status;
}

/*****************************************************************************/
/** @brief Obtain's the Z2's available Tx filters.

    @param p_id Pointer to RF identifier struct.
    @param p_filters Pointer to be updated with available filters.
    @param p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status = 0;

    *p_num_filters = sizeof(_tx_filters) / sizeof(_tx_filters[0]);
    memcpy(p_filters, _tx_filters, sizeof(_tx_filters));

    return status;
}

int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;
    uint8_t i=0;

    *p_num_fixed_rf_ports = _num_rx_rf_ports[p_id->card];
    for( i=0; i<*p_num_fixed_rf_ports; i++ )
    {
        p_fixed_rf_port_list[i] = _rx_rf_ports[i].skiq_rf_port;
    }

    *p_num_trx_rf_ports = 1;
    if( _use_top_port[p_id->card] == true )
    {
        p_trx_rf_port_list[0] = skiq_rf_port_J1;
    }
    else
    {
        p_trx_rf_port_list[0] = skiq_rf_port_J300;
    }

    return (status);
}

int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    *p_rf_port = _curr_rx_rf_port[p_id->card].skiq_rf_port;

    return (status);
}

int32_t _write_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t rf_port )
{
    int32_t status=0;
    uint8_t i=0;
    bool found_port=false;

    if( rf_port != _curr_rx_rf_port[p_id->card].skiq_rf_port )
    {
        for( i=0; (i<_num_rx_rf_ports[p_id->card]) && (found_port==false); i++ )
        {
            if( (_rx_rf_ports[i]).skiq_rf_port == rf_port )
            {
                found_port = true;
                // try to configure it...note that writing it saves off the selection
                status=_write_rx_antenna_port(p_id, (z2_rf_config_t*)(&_rx_rf_ports[i]));
            }
        }
        if( found_port == false )
        {
            _skiq_log(SKIQ_LOG_ERROR, "unable to locate RF port %u", rf_port);
            status = -ENODEV;
        }
    }
    
    return (status);    
}

int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;

    *p_num_fixed_rf_ports = 1;
    *p_num_trx_rf_ports = 1;
    if( _use_top_port[p_id->card] == true )
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J1;
        p_trx_rf_port_list[0] = skiq_rf_port_J1;
    }
    else
    {
        p_fixed_rf_port_list[0] = skiq_rf_port_J300;
        p_trx_rf_port_list[0] = skiq_rf_port_J300;
    }

    return (status);
}

int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( _use_top_port[p_id->card] == true )
    {
        *p_rf_port = skiq_rf_port_J1;
    }
    else
    {
        *p_rf_port = skiq_rf_port_J300;
    }

    return (status);    
}

int32_t _rf_port_config_avail( rf_id_t *p_id, bool *p_fixed, bool *p_trx )
{
    int32_t status=0;

    *p_fixed = true;
    *p_trx = true;
    
    return (status);
}

int32_t _write_rf_port_config( rf_id_t *p_id, skiq_rf_port_config_t config )
{
    int32_t status=0;
    z2_rf_config_t rx_config;

    if( (config == skiq_rf_port_config_trx) ||
        (config == skiq_rf_port_config_tdd) )
    {
        rx_config.z2_ant_sel = rx_antenna_tdd;
        if( _use_top_port[p_id->card] == true )
        {
            rx_config.skiq_rf_port = skiq_rf_port_J1;
        }
        else
        {
            rx_config.skiq_rf_port = skiq_rf_port_J300;
        }
    }
    else
    {
        rx_config = _rx_rf_ports[0];
    }

    // switch our antenna port selection
    if( (status=_write_rx_antenna_port( p_id, &rx_config )) == 0 )
    {
        // if we're in TRX, default to receive otherwise default to transmit
        if( config == skiq_rf_port_config_trx )
        {
            // default to receive
            status = _write_rf_port_operation( p_id, false );
        }
        else
        {
            // if we're back to fixed, we need to make sure the TX GPIO is enabled since
            // that port should always be set for TX when in fixed mode
            status = _write_rf_port_operation( p_id, true );
        }
    }

    return (status);
}

int32_t _read_rf_port_operation( rf_id_t *p_id, bool *p_transmit )
{
    int32_t status=0;
    uint8_t value = 0;

    gpio_sysfs_read_value( p_id->card, GPIO_CLASS, TDD_GPIO_ABS_OFFSET, &value );
    if( value == TDD_GPIO_RECEIVE )
    {
        *p_transmit = false;
    }
    else
    {
        *p_transmit = true;
    }

    return (status);
}

int32_t _write_rf_port_operation( rf_id_t *p_id, bool transmit )
{
    int32_t status=0;
    uint8_t value = TDD_GPIO_RECEIVE;

    if( transmit == true )
    {
        value = TDD_GPIO_TRANSMIT;
    }

    status = gpio_sysfs_write_value( p_id->card, GPIO_CLASS, TDD_GPIO_ABS_OFFSET, value );

    return (status);
}

/*****************************************************************************/
/** @brief Configure the Z2's antenna port for Rx.

    @param p_id Pointer to RF identifier struct.
    @param antenna The antenna to select.

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _write_rx_antenna_port( rf_id_t *p_id,
                                z2_rf_config_t *p_rf_config )
{
    uint8_t u20_pins = (U20_RX_ANT_1_MASK | U20_RX_ANT_2_MASK);
    uint8_t u21_pins = (U21_LNA_SW_MASK);
    uint8_t u20_val = 0;
    uint8_t u21_val = 0;
    int32_t status = 0;

    TRACE();

    switch( p_rf_config->z2_ant_sel )
    {
    case( rx_antenna_isolation ):
        u20_val = 0; // clear antenna selection and enable bypass path
        u21_val = U21_LNA_SW_MASK;
        break;
    case( rx_antenna_1 ):
        u20_val = U20_RX_ANT_1_MASK;
        u21_val = 0; // TODO: allow this to be disabled?
        break;
    case( rx_antenna_2 ):
        u20_val = U20_RX_ANT_2_MASK; 
        u21_val = 0; // TODO: allow this to be disabled?
        break;
    case( rx_antenna_tdd ):
        u20_val = 0; // ANT1/2 needs to be cleared
        u21_val = 0; 
        gpio_sysfs_write_value( p_id->card, GPIO_CLASS, TDD_GPIO_ABS_OFFSET, TDD_GPIO_RECEIVE );
        break;
    default:
        status = -EINVAL;
    }

    if( (0 == status) && (rx_antenna_tdd != p_rf_config->z2_ant_sel) )
    {
        status = _disable_tx_filter_path(p_id);
    }

    /* write U20 pins as outputs */
    if( 0 == status )
    {
        status = tca6408_io_exp_set_as_output( p_id->card, U20_IO_EXPANDER_ADDR, u20_pins, u20_val );
    }

    /* write U21 pins as outputs */
    if( 0 == status )
    {
        status = tca6408_io_exp_set_as_output( p_id->card, U21_IO_EXPANDER_ADDR, u21_pins, u21_val );
    }

    // save off our new config if it's not isolation
    if( (status == 0) && (p_rf_config->z2_ant_sel != rx_antenna_isolation) )
    {
        memcpy( &(_curr_rx_rf_port[p_id->card]),
                p_rf_config,
                sizeof(z2_rf_config_t) );
    }

    return status;
}


int32_t _read_bias_index( rf_id_t *p_id, uint8_t *p_bias_index )
{
    int32_t status=0;

    *p_bias_index = _bias_index[p_id->card];

    return (status);
}

int32_t _write_bias_index( rf_id_t *p_id, uint8_t bias_index )
{
    int32_t status=0;

    // check the range first
    if( (bias_index >= Z2_BIAS_INDEX_MIN) &&
        (bias_index <= Z2_BIAS_INDEX_MAX) )
    {
        // save the index to use
        _bias_index[p_id->card] = bias_index;
        // if the LNA was already enabled, just enable it again and the new setting is applied
        if( _is_rx_lna_enabled(p_id) == true )
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Updating LNA bias current\n");
            status = _enable_rx_lna(p_id);
        }
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "Invalid bias index of %u specified (%u-%u)\n",
                  bias_index, Z2_BIAS_INDEX_MIN, Z2_BIAS_INDEX_MAX);
        status = -EINVAL;
    }

    return (status);
}

int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    skiq_filt_t filt = skiq_filt_invalid;
    uint16_t i=0;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds
    
    filt = _find_rx_filt( freq_list[0] );
    for( i=1; (i<num_freq) && (status==0) && (filt != skiq_filt_invalid); i++ )
    {
        if( _find_rx_filt(freq_list[i]) != filt )
        {
            status = -EINVAL;
            skiq_error("RX Hopping range must reside within a single frequency filter band for Z2");
        }
    }

    // if the frequencies are within a band, then actually apply it.  Since we know
    // all of the frequencies use the same filter setting, we can just set RFE for the
    // first frequency in the hop list
    if( status == 0 )
    {
        status = _update_before_rx_lo_tune( p_id, freq_list[0] );
    }
    if( status == 0 )
    {
        status = _update_after_rx_lo_tune( p_id, freq_list[0] );
    }

    return (status);
}

int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    skiq_filt_t filt = skiq_filt_invalid;
    uint16_t i=0;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds
    
    filt = _find_tx_filt( freq_list[0] );
    for( i=1; (i<num_freq) && (status==0); i++ )
    {
        if( _find_tx_filt(freq_list[i]) != filt )
        {
            status = -EINVAL;
            skiq_error("TX Hopping range must reside within a single frequency filter band for Z2");
        }
    }

    if( status == 0 )
    {
        // if the frequencies are within a band, then actually apply it.  Since we know
        // all of the frequencies use the same filter setting, we can just set RFE for the
        // first frequency in the hop list
        status = _update_tx_lo( p_id, freq_list[0] );
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = Z2_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = Z2_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = Z2_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = Z2_MAX_RX_FREQUENCY_IN_HZ;


    return (status);
}

skiq_filt_t _find_rx_filt( uint64_t rx_lo_freq )
{
    skiq_filt_t filt = skiq_filt_invalid;

    // NOTE: The IS_RANGE macro is exclusive.

    if( IS_RANGE(rx_lo_freq, 0, 435e6) )
    {
        filt = skiq_filt_50_to_435MHz;
    }
    else if( IS_RANGE(rx_lo_freq, 435e6, 910e6) )
    {
        filt = skiq_filt_435_to_910MHz;
    }
    else if( IS_RANGE(rx_lo_freq, 910e6, 1950e6) )
    {
        filt = skiq_filt_910_to_1950MHz;
    }
    else if( IS_RANGE(rx_lo_freq, 1950e6, 6000e6) )
    {
        filt = skiq_filt_1950_to_6000MHz;
    }
    else if( IS_RANGE(rx_lo_freq, 0, 3000e6) || (rx_lo_freq == 3000e6) )
    {
        filt = skiq_filt_0_to_3000_MHz;
    }

    return (filt);
}

skiq_filt_t _find_tx_filt( uint64_t tx_lo_freq )
{
    skiq_filt_t filt = skiq_filt_invalid;
    
    // NOTE: The IS_RANGE macro is exclusive.

    if( IS_RANGE(tx_lo_freq, 0, 3000e6) || (tx_lo_freq == 3000e6) )
    {
        filt = skiq_filt_0_to_3000_MHz;
    }
    else if( IS_RANGE(tx_lo_freq, 3000e6, 6000e6) )
    {
        filt = skiq_filt_3000_to_6000_MHz;
    }

    return (filt);
}


/***** GLOBAL DATA *****/

rfe_functions_t rfe_z2_b_funcs =
{
    .init                           = _init,
    .release                        = NULL,
    .update_tx_pa                   = NULL,
    .override_tx_pa                 = NULL,    
    .update_rx_lna                  = _update_rx_lna,
    .update_rx_lna_by_idx           = NULL,
    .update_before_rx_lo_tune       = _update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = _update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = _update_tx_lo,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = _write_rx_filter_path,
    .write_tx_filter_path           = _write_tx_filter_path,
    .read_rx_filter_path            = _read_rx_filter_path,
    .read_tx_filter_path            = _read_tx_filter_path,
    .read_rx_filter_capabilities    = _read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = _read_tx_filter_capabilities,
    .write_filters                  = NULL,
    .write_rx_attenuation           = NULL,
    .read_rx_rf_ports_avail         = _read_rx_rf_ports_avail,
    .read_rx_rf_port                = _read_rx_rf_port,
    .write_rx_rf_port               = _write_rx_rf_port,
    .read_tx_rf_ports_avail         = _read_tx_rf_ports_avail,
    .read_tx_rf_port                = _read_tx_rf_port,
    .write_tx_rf_port               = NULL, // can't change TX port
    .rf_port_config_avail           = _rf_port_config_avail,
    .write_rf_port_config           = _write_rf_port_config,
    .read_rf_port_operation         = _read_rf_port_operation,
    .write_rf_port_operation        = _write_rf_port_operation,
    .read_bias_index                = _read_bias_index,
    .write_bias_index               = _write_bias_index,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = NULL,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = _write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = _write_tx_freq_hop_list,
    .config_custom_rx_filter        = NULL,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = NULL,
    .swap_rx_port_config            = NULL,
    .read_rf_capabilities           = _read_rf_capabilities,
};
