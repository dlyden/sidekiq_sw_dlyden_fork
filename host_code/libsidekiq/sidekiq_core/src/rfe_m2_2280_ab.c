/**
 * @file   rfe_m2_2280_ab.c
 * @date   Wed Apr 10 16:01:24 2019
 *
 * @brief  Implements the RF Front End for Sidekiq M.2 2280 rev A and rev B
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */


/***** INCLUDES *****/

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "sidekiq_types.h"
#include "sidekiq_rf.h"
#include "rfe_m2_2280_ab.h"
#include "sidekiq_fpga.h"
#include "io_expander_pcal6524.h"

#include "sidekiq_hal.h"
#include "ad9361_driver.h"
#include "card_services.h"

/* enable TRACE_* macros when DEBUG_RFE_M2_2280_TRACE is defined */
#if (defined DEBUG_RFE_M2_2280_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

/* enable debug_print and debug_print_plain when DEBUG_RFE_M2_2280 is defined */
#if (defined DEBUG_RFE_M2_2280)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define STRETCH_NR_LNA                  2
#define LNA1                            0 /* used to index in current_lna_state[card] */
#define LNA2                            1 /* used to index in current_lna_state[card] */

#define M2_2280_IOE_I2C_BUS             1
#define x20_ADDR                        0x20 /* I2C address of U39 (PCAL6524) on Rev A/B of M.2-2280 */
#define x20_PORT2_MASK                  0xFF
#define x21_ADDR                        0x21 /* I2C address of U59 (PCAL6524) on Rev A/B of M.2-2280 */
#define x21_PORT0_MASK                  0xFF
#define x21_PORT1_MASK                  0xFF
#define x22_ADDR                        0x22 /* I2C address of U300 (PCAL6524) on Rev A/B of M.2-2280 */
#define x22_PORT0_MASK                  0xFF
#define x22_PORT1_MASK                  0xFF


#define ANT_IO_EXP_ADDR                 x20_ADDR
#define TX_FLT1_SEL                     PORT(0,0)     /* P00 */
#define TX_FLT2_SEL                     PORT(0,1)     /* P01 */
#define TX_AMP_MANUAL_BIAS_ON_N         PORT(0,2)     /* P02 */
#define TX1A_SEL                        PORT(0,3)     /* P03 */
#define TX1B_SEL                        PORT(0,4)     /* P04 */
#define RXTX_ANT1_TOG_EN                PORT(0,5)     /* P05 */
#define RXTX_ANT1_TOG_EN_N              PORT(0,6)     /* P06 */
#define RXTX_AMPS_TOG_EN_N              PORT(0,7)     /* P07 */
#define RX_ANT2_SEL                     PORT(1,0)     /* P10 */

/*
  HIGH= LNA1 selected, bypassed disabled.
  LOW = LNA1 is bypassed
  HiZ = LNA1 is bypassed (External Rpd)

  Yes, the logic is opposite LNA2.
*/
#define LNA1_BYPASS_TOG                 PORT(1,1)     /* P11 */

/*
  HIGH= LNA2 is bypassed
  LOW = LNA2 selected, bypassed disabled.
  HiZ =  LNA2 selected, bypassed disabled. (External Rpd)

  Yes, the logic is opposite LNA1.
*/
#define LNA2_BYPASS_TOG                 PORT(1,2)     /* P12 */

/*
  HIGH = LNA1 forced OFF (Bypass still needs to be selected if desired)
  LOW = LNA1  forced ON  (LNA still needs to be switched in to be used)
  HIZ = TDD Mode: Setting it to HiZ will put the LNA1 bias under the control of PL_CTRL_TDD_TXRX_N
*/
#define LNA1_MANUAL_BIAS_EN_N           PORT(1,3)     /* P13 */
#define LNA2_MANUAL_BIAS_EN_N           PORT(1,4)     /* P14 */
#define RX1A_SEL                        PORT(1,5)     /* P15 */
#define RX1B_SEL                        PORT(1,6)     /* P16 */
#define RX1C_SEL                        PORT(1,7)     /* P17 */


#define LNA_IO_EXP_ADDR                 x20_ADDR
#define RX_SEL_IO_EXP_ADDR              x20_ADDR
#define RX_SEL_IO_EXP_MASK              (uint32_t)(RX1A_SEL | RX1B_SEL | RX1C_SEL)

#define RF_PORT_PIN_MASK                (RXTX_ANT1_TOG_EN | RXTX_ANT1_TOG_EN_N | \
                                         RXTX_AMPS_TOG_EN_N | RX_ANT2_SEL)
#define RF_PORT_J1_VALUE                (RXTX_ANT1_TOG_EN)
#define RF_PORT_J2_VALUE                (RXTX_ANT1_TOG_EN_N | RXTX_AMPS_TOG_EN_N | RX_ANT2_SEL)
#define RF_PORT_NONE_VALUE              (RXTX_ANT1_TOG_EN_N | RXTX_AMPS_TOG_EN_N)

/*

  TX filter path  (stand-by, <3GHz, >=3GHz)
  - TX_FLT1_SEL (<3GHz)
  - TX_FLT2_SEL (>=3GHz)
  - TX1A_SEL    (>=3GHz)
  - TX1B_SEL    (<3GHz)

  RX filter path  (stand-by, 17 bands)
  - RX1A_SEL
  - RX1B_SEL
  - RX1C_SEL
  - I/O expanders at 0x20 (P2), 0x21 (P0 and P1), and 0x22 (P0 and P1)

  RX port selection  (none, J1, J2)
  - RXTX_ANT1_TOG_EN
  - RXTX_ANT1_TOG_EN_N
  - RXTX_AMPS_TOG_EN_N
  - RX_ANT2_SEL

  LNA1 control  (bypass, enable, disable)
  - LNA1_MANUAL_BIAS_EN_N
  - LNA1_BYPASS_TOG

  LNA2 control  (bypass, enable, disable)
  - LNA2_MANUAL_BIAS_EN_N
  - LNA2_BYPASS_TOG

  PA control  (enable, disable, T/R controlled)
  - TX_AMP_MANUAL_BIAS_ON_N
  - PL_CTRL_TDD_TXRX_N

 */


/***** ENUMS *****/


typedef enum
{
    stretch_lna_bypassed,
    stretch_lna_disabled,
    stretch_lna_enabled_fixed,
    stretch_lna_enabled_trx,

} stretch_rfe_lna_state_t;


typedef enum
{
    stretch_pa_disabled,
    stretch_pa_enabled_fixed,
    stretch_pa_enabled_trx,

} stretch_rfe_pa_state_t;


/***** TYPE DEFINITIONS *****/

typedef struct
{
    skiq_filt_t filter;
    uint8_t ad9361_port;
    uint16_t lo, hi;
    uint8_t x20_port0_valu, x20_port0_mask;
    uint8_t x20_port0_hiz;      /* used in skiq_rf_port_config_trx mode only */
    uint8_t x20_port1_valu, x20_port1_mask;
    uint8_t x20_port2_valu;
    uint8_t x21_port0_valu, x21_port1_valu;
    uint8_t x22_port0_valu, x22_port1_valu;
} filter_selection_t;


typedef struct
{
    uint8_t id;
    uint8_t i2c_addr;
    uint32_t pin_mask;
    uint32_t pin_bypass;
    uint32_t pin_disable;
    uint32_t pin_enable;
    uint32_t hiz_enable;        /* used in skiq_rf_port_config_trx mode only */
} lna_setting_t;


typedef struct
{
    bool valid;
    skiq_rf_port_config_t config;
    bool operation;
} stretch_rf_port_state_t;


typedef struct
{
    bool valid;
    stretch_rfe_lna_state_t state;
} stretch_lna_state_t;


typedef struct
{
    bool valid;
    stretch_rfe_pa_state_t state;
} stretch_pa_state_t;


/***** MACROS ******/

#define NOT_YET_IMPLEMENTED                                             \
    do {                                                                \
        skiq_error("!!! The function '%s' for Sidekiq M.2 2280 is not yet implemented\n", __FUNCTION__); \
        status = -ENOTSUP;                                              \
    } while (0)

#define PORT_to_PIN(_port)                                              \
    (((_port) & (0xFF << 16)) == (_port)) ? (((_port) >> 16) & 0xFF) :  \
    (((_port) & (0xFF <<  8)) == (_port)) ? (((_port) >>  8) & 0xFF) :  \
    (((_port) & (0xFF <<  0)) == (_port)) ? (((_port) >>  0) & 0xFF) : 0

#define RX_PORT_to_PIN(_rfic_port)                                      \
    ((_rfic_port) == ad9361_rx_port_a) ? PORT_to_PIN(RX1A_SEL) :        \
    ((_rfic_port) == ad9361_rx_port_b) ? PORT_to_PIN(RX1B_SEL) :        \
    ((_rfic_port) == ad9361_rx_port_c) ? PORT_to_PIN(RX1C_SEL) : 0

#define RX_SEL_PIN_MASK          (uint8_t)PORT_to_PIN(RX_SEL_IO_EXP_MASK)
#define RX_FILT_SEL_(_range,_lo,_hi,_port,_b,_c,_d,_e,_f)               \
    RX_FILT_SEL__(skiq_filt_##_range,_lo,_hi,ad9361_rx_##_port,_b,_c,_d,_e,_f)
#define RX_FILT_SEL__(_filt,_lo,_hi,_rfic_port,_b,_c,_d,_e,_f)          \
    { .filter = _filt,                                                  \
            .ad9361_port = _rfic_port,                                  \
            .lo = (_lo),                                                \
            .hi = (_hi),                                                \
            .x20_port0_valu =    0, .x20_port1_mask = 0x00,             \
            .x20_port1_valu = RX_PORT_to_PIN(_rfic_port),               \
            .x20_port1_mask = RX_SEL_PIN_MASK,                          \
            .x20_port2_valu = (_b),                                     \
            .x21_port0_valu = (_c),                                     \
            .x21_port1_valu = (_d),                                     \
            .x22_port0_valu = (_e),                                     \
            .x22_port1_valu = (_f),                                     \
            }

#define _OFF                             0xF0
#define RX_FILT0_(_filt,_lo,_hi)         RX_FILT_SEL_(_filt,_lo,_hi,port_b,0xA2,_OFF,_OFF,_OFF,_OFF)
#define RX_FILT1_(_filt,_lo,_hi,_sel)    RX_FILT_SEL_(_filt,_lo,_hi,port_b,0x62,_sel,_OFF,_OFF,_OFF)
#define RX_FILT2A_(_filt,_lo,_hi,_sel)   RX_FILT_SEL_(_filt,_lo,_hi,port_b,0x25,_OFF,_sel,_OFF,_OFF)
#define RX_FILT2B_(_filt,_lo,_hi,_sel)   RX_FILT_SEL_(_filt,_lo,_hi,port_c,0x25,_OFF,_sel,_OFF,_OFF)
#define RX_FILT3_(_filt,_lo,_hi,_sel)    RX_FILT_SEL_(_filt,_lo,_hi,port_c,0x29,_OFF,_OFF,_sel,_OFF)
#define RX_FILT4_(_filt,_lo,_hi,_sel)    RX_FILT_SEL_(_filt,_lo,_hi,port_a,0x30,_OFF,_OFF,_OFF,_sel)

#define TX_FILT_(_filt,_lo,_hi,_port,_sel,_hiz)                         \
    { .filter = skiq_filt_##_filt,                                      \
            .ad9361_port = ad9361_tx_##_port,                           \
            .lo = (_lo),                                                \
            .hi = (_hi),                                                \
            .x20_port0_valu = (_sel),                                   \
            .x20_port0_mask = 0x1B,             /* bits 0,1,3,4 */      \
            .x20_port0_hiz  = (_hiz),                                   \
            .x20_port1_valu = 0, .x20_port1_mask = 0,                   \
            .x20_port2_valu = 0,                                        \
            .x21_port0_valu = 0,                                        \
            .x21_port1_valu = 0,                                        \
            .x22_port0_valu = 0,                                        \
            .x22_port1_valu = 0,                                        \
            }

#define RF_PORT_STATE_INITIALIZER \
    { .valid = false, .config = skiq_rf_port_config_fixed, .operation = false }

#define LNA_STATE_INITIALIZER \
    { .valid = false, .state = stretch_lna_disabled }

#define PA_STATE_INITIALIZER \
    { .valid = false, .state = stretch_pa_disabled }

#define IS_LNA_DISABLED(_card,_idx)     (rfe_lna_state(_card,_idx) == lna_disabled)
#define IS_LNA_ENABLED(_card,_idx)      (rfe_lna_state(_card,_idx) == lna_enabled)

#define LOCK_RFE(_card)                 CARD_LOCK(rfe_mutex,_card)
#define UNLOCK_RFE(_card)               CARD_UNLOCK(rfe_mutex,_card)


/***** LOCAL DATA *****/

static ARRAY_WITH_DEFAULTS(filter_selection_t *, current_rx_filter, SKIQ_MAX_NUM_CARDS, NULL);
static ARRAY_WITH_DEFAULTS(filter_selection_t *, current_tx_filter, SKIQ_MAX_NUM_CARDS, NULL);


/**
   @brief Mutex for protecting all things RFE
 */
static ARRAY_WITH_DEFAULTS(pthread_mutex_t,
                           rfe_mutex,
                           SKIQ_MAX_NUM_CARDS,
                           PTHREAD_MUTEX_INITIALIZER);


/**
 * @note The only times when RF port configuration is invalid is before rfe_init() and after
 * rfe_release()
 */
static ARRAY_WITH_DEFAULTS(stretch_rf_port_state_t,
                           current_rf_port_state,
                           SKIQ_MAX_NUM_CARDS,
                           RF_PORT_STATE_INITIALIZER);


/**
   @brief track LNA1 and LNA2 states
*/
static stretch_lna_state_t current_lna_state[SKIQ_MAX_NUM_CARDS][STRETCH_NR_LNA] =
{
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [LNA1] = LNA_STATE_INITIALIZER,
        [LNA2] = LNA_STATE_INITIALIZER,
    },
};


/**
   @brief track PA states
 */
static ARRAY_WITH_DEFAULTS(stretch_pa_state_t,
                           current_pa_state,
                           SKIQ_MAX_NUM_CARDS,
                           PA_STATE_INITIALIZER);


/**************************************************************************************************/
static skiq_filt_t available_rx_filters[] =
{
    skiq_filt_47_to_135MHz,
    skiq_filt_135_to_145MHz,
    skiq_filt_145_to_150MHz,
    skiq_filt_150_to_162MHz,
    skiq_filt_162_to_175MHz,
    skiq_filt_175_to_190MHz,
    skiq_filt_190_to_212MHz,
    skiq_filt_212_to_230MHz,
    skiq_filt_230_to_280MHz,
    skiq_filt_280_to_366MHz,
    skiq_filt_366_to_475MHz,
    skiq_filt_475_to_625MHz,
    skiq_filt_625_to_800MHz,
    skiq_filt_800_to_1175MHz,
    skiq_filt_1175_to_1500MHz,
    skiq_filt_1500_to_2100MHz,
    skiq_filt_2100_to_2775MHz,
    skiq_filt_2775_to_3360MHz,
    skiq_filt_3360_to_4600MHz,
    skiq_filt_4600_to_6000MHz,
};

#define M2_2280_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(47ULL*RATE_MHZ_TO_HZ)
#define M2_2280_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

static filter_selection_t rx_filter_standby =
{
    .filter = 0, .lo = 0, .hi = 0,
    .ad9361_port = ad9361_rx_port_a,
    .x20_port0_valu = 0, .x20_port0_mask = 0,
    .x20_port1_valu = 0x18, .x20_port1_mask = 0xE0,
    .x20_port2_valu = 0,
    .x21_port0_valu = 0,
    .x21_port1_valu = 0,
    .x22_port0_valu = 0,
    .x22_port1_valu = 0,
};


static filter_selection_t rx_filter_selections[] =
{
    RX_FILT0_(47_to_135MHz,      47, 135),
    RX_FILT1_(135_to_145MHz,    135, 145, 0x22),
    RX_FILT1_(145_to_150MHz,    145, 150, 0x53),
    RX_FILT1_(150_to_162MHz,    150, 162, 0x77),
    RX_FILT1_(162_to_175MHz,    162, 175, 0x98),
    RX_FILT1_(175_to_190MHz,    175, 190, 0xB9),
    RX_FILT1_(190_to_212MHz,    190, 212, 0xCB),
    RX_FILT1_(212_to_230MHz,    212, 230, 0xDC),
    RX_FILT1_(230_to_280MHz,    230, 280, 0xED),
    RX_FILT1_(280_to_366MHz,    280, 366, 0xFF),
    RX_FILT2A_(366_to_475MHz,   366, 475, 0x04),
    RX_FILT2A_(475_to_625MHz,   475, 625, 0x89),
    RX_FILT2B_(625_to_800MHz,   625, 800, 0xDC),
    RX_FILT2B_(800_to_1175MHz,  800, 1175, 0xFF),
    RX_FILT3_(1175_to_1500MHz, 1175, 1500, 0x26),
    RX_FILT3_(1500_to_2100MHz, 1500, 2100, 0xCB),
    RX_FILT3_(2100_to_2775MHz, 2100, 2775, 0xFE),
    RX_FILT4_(2775_to_3360MHz, 2775, 3360, 0x52),
    RX_FILT4_(3360_to_4600MHz, 3360, 4600, 0xCB),
    RX_FILT4_(4600_to_6000MHz, 4600, 6001, 0xFF),
};


/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
#if (defined ATE_SUPPORT)
#define FACT_RX_FILT_SEL_(_rfic_port,_fl_sel,_fl1,_fl2,_fl3,_fl4) \
    RX_FILT_SEL__(skiq_filt_max,0,6001,_rfic_port,_fl_sel,_fl1,_fl2,_fl3,_fl4)

/*                                      (FLTR_EN | FLTR23_SEL | FLTRn_SEL | FLTR1_SW) */
#define FLTR1_SEL(_sw)                  (0x20 | 0x00 | 0x02 | (((_sw) & 0x03) << 6))
#define FLTR2_SEL                       (0x20 | 0x01 | 0x04)
#define FLTR3_SEL                       (0x20 | 0x01 | 0x08)
#define FLTR4_SEL                       (0x20 | 0x00 | 0x10)

static filter_selection_t rx_filter_factory = \
    FACT_RX_FILT_SEL_(ad9361_rx_port_a,0,_OFF,_OFF,_OFF,_OFF);

#endif  /* ATE_SUPPORT */
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */


static skiq_filt_t available_tx_filters[] =
{
    skiq_filt_0_to_3000_MHz,
    skiq_filt_3000_to_6000_MHz,
};

#define M2_2280_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define M2_2280_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

static filter_selection_t tx_filter_standby =
{
    .filter = 0, .lo = 0, .hi = 0,
    .ad9361_port = ad9361_tx_port_a,
    .x20_port0_valu = 0x00, .x20_port0_mask = 0x1B,
    .x20_port0_hiz  = 0,
    .x20_port1_valu = 0, .x20_port1_mask = 0,
    .x20_port2_valu = 0,
    .x21_port0_valu = 0,
    .x21_port1_valu = 0,
    .x22_port0_valu = 0,
    .x22_port1_valu = 0,
};


static filter_selection_t tx_filter_selections[] =
{
    /**
       @attention, TX_FLT1_SEL, TX_FLT2_SEL, TX1A_SEL, and TX1B_SEL are PORTS, but also happen to be
       in the lowest bank, so their definitions are safe to use here

       @note the filter selections are either asserted (in fixed) or Hi-Z (in trx), so when in
       fixed RF port configuration:
       -  x20_port0_valu & x20_port0_mask => high outputs
       - ~x20_port0_valu & x20_port0_mask => low outputs
       and when in trx RF port configuration:
       -  (x20_port0_valu ^ x20_port0_hiz) & x20_port0_mask => high outputs
       -                   (x20_port0_hiz) & x20_port0_mask => Hi-Z

       Example:
       - x20_port0_valu = 0x11
       - x20_port0_mask = 0x1B
       - x20_port0_hiz  = 0x01
       ====== fixed ======
       - high outputs =>  0x11 & 0x1B => 0x11
       -  low outputs => ~0x11 & 0x1B => 0xEE & 0x1B => 0x0A
       ======= trx =======
       - high outputs =>  (0x11 ^ 0x01) & 0x1B => 0x10 & 0x1B => 0x10
       -   Hi-Z state =>         (0x01) & 0x1B => 0x01
       -  low outputs =>  everything not high / Hi-Z => 0x0A

    */
    TX_FILT_(0_to_3000_MHz,       0, 3000, port_b, TX_FLT1_SEL | TX1B_SEL, TX_FLT1_SEL ),
    TX_FILT_(3000_to_6000_MHz, 3000, 6001, port_a, TX_FLT2_SEL | TX1A_SEL, TX_FLT2_SEL ),
};


/* all necessary information needed to bypass, disable, or enable the LNA of interest, indexed by
 * lna_idx */
static const lna_setting_t lna_settings[] =
{
    [0] = {
        .id = 1,
        .i2c_addr = LNA_IO_EXP_ADDR,
        .pin_mask = LNA1_MANUAL_BIAS_EN_N | LNA1_BYPASS_TOG,
        .pin_bypass = LNA1_MANUAL_BIAS_EN_N,
        .pin_disable = LNA1_MANUAL_BIAS_EN_N | LNA1_BYPASS_TOG,
        .pin_enable = LNA1_BYPASS_TOG,
        .hiz_enable = LNA1_MANUAL_BIAS_EN_N, /* used in skiq_rf_port_config_fixed mode only */
    },
    [1] = {
        .id = 2,
        .i2c_addr = LNA_IO_EXP_ADDR,
        .pin_mask = LNA2_MANUAL_BIAS_EN_N | LNA2_BYPASS_TOG,
        .pin_bypass = LNA2_MANUAL_BIAS_EN_N | LNA2_BYPASS_TOG,
        .pin_disable = LNA2_MANUAL_BIAS_EN_N,
        .pin_enable = 0,
        .hiz_enable = LNA2_MANUAL_BIAS_EN_N, /* used in skiq_rf_port_config_fixed mode only */
    },
};


/***** DEBUG FUNCTIONS *****/

#include "debug_rfe_m2_2280_ab.h"


/***** LOCAL FUNCTIONS *****/


static skiq_rf_port_t
current_rx_rf_port( uint8_t card )
{
    skiq_rf_port_t curr_port = skiq_rf_port_unknown;

    if ( current_rf_port_state[card].valid )
    {
        if ( current_rf_port_state[card].config == skiq_rf_port_config_fixed )
        {
            curr_port = skiq_rf_port_J2;
        }
        else
        {
            curr_port = skiq_rf_port_J1;
        }
    }

    return curr_port;
}


/**
   @brief Provides current `stretch_rfe_lna_state_t` of the specified card and LNA index
 */
static stretch_rfe_lna_state_t
stretch_rfe_lna_state( uint8_t card,
                       uint8_t lna_index )
{
    stretch_rfe_lna_state_t stretch_state = stretch_lna_disabled;

    if ( lna_index < STRETCH_NR_LNA )
    {
        if ( current_lna_state[card][lna_index].valid )
        {
            stretch_state = current_lna_state[card][lna_index].state;
        }
        else
        {
            skiq_error("Internal Error: current LNA state not valid, but LNA state "
                       "requested for card %u\n", card);
        }
    }
    else
    {
        skiq_error("Internal Error: out-of-range LNA index (%u !< %u) requested for card %u\n",
                   lna_index, STRETCH_NR_LNA, card);
    }

    return stretch_state;
}


/**
   @brief Translates from current `stretch_rfe_lna_state_t` to `rfe_lna_state_t` based on specified
   card and LNA index
 */
static rfe_lna_state_t
rfe_lna_state( uint8_t card,
               uint8_t lna_index )
{
    rfe_lna_state_t state = lna_disabled;

    if ( lna_index < STRETCH_NR_LNA )
    {
        if ( current_lna_state[card][lna_index].valid )
        {
            switch (current_lna_state[card][lna_index].state)
            {
                case stretch_lna_bypassed:
                    state = lna_bypassed;
                    break;

                case stretch_lna_disabled:
                    state = lna_disabled;
                    break;

                case stretch_lna_enabled_fixed:
                case stretch_lna_enabled_trx:
                    state = lna_enabled;
                    break;

                default:
                    state = lna_disabled;
                    break;
            }
        }
        else
        {
            skiq_error("Internal Error: LNA state not valid, but LNA state translation "
                       "requested for card %u\n", card);
        }
    }
    else
    {
        skiq_error("Internal Error: out-of-range LNA index (%u !< %u) requested for card %u\n",
                   lna_index, STRETCH_NR_LNA, card);
    }

    return state;
}


/**
   @brief Translates from `rfe_lna_state_t` to `stretch_rfe_lna_state_t` taking a card's RF port
   configuration into account.
 */
static stretch_rfe_lna_state_t
rfe_lna_state_to_stretch( uint8_t card,
                          rfe_lna_state_t state )
{
    stretch_rfe_lna_state_t stretch_state;

    if ( current_rf_port_state[card].valid )
    {
        switch (state)
        {
            case lna_bypassed:
                stretch_state = stretch_lna_bypassed;
                break;

            case lna_disabled:
                stretch_state = stretch_lna_disabled;
                break;

            case lna_enabled:
                if ( current_rf_port_state[card].config == skiq_rf_port_config_fixed )
                {
                    stretch_state = stretch_lna_enabled_fixed;
                }
                else
                {
                    stretch_state = stretch_lna_enabled_trx;
                }
                break;

            default:
                skiq_warning("Unknown / unhandled LNA state (%u) for card %u\n", state, card);
                stretch_state = stretch_lna_disabled;
                break;
        }
    }
    else
    {
        skiq_error("Internal Error: RF port configuration not valid, but LNA state "
                   "translation requested for card %u\n", card);
        stretch_state = stretch_lna_disabled;
    }

    return stretch_state;
}


/**
   @brief Provides current `stretch_rfe_pa_state_t` from a specified card's PA state
 */
static stretch_rfe_pa_state_t
stretch_rfe_pa_state( uint8_t card )
{
    stretch_rfe_pa_state_t stretch_state = stretch_pa_disabled;

    if ( current_pa_state[card].valid )
    {
        stretch_state = current_pa_state[card].state;
    }
    else
    {
        skiq_error("Internal Error: PA state not valid, but PA state requested for "
                   "card %u\n", card);
    }

    return stretch_state;
}


/**
   @brief Translates current `stretch_rfe_pa_state_t` to `bool` from a specified card's PA state
 */
#if (defined DEBUG_PRINT_ENABLED)
static bool
rfe_pa_state( uint8_t card )
{
    bool enable;

    if ( current_pa_state[card].valid )
    {
        if ( ( current_pa_state[card].state == stretch_pa_enabled_fixed ) ||
             ( current_pa_state[card].state == stretch_pa_enabled_trx ) )
        {
            enable = true;
        }
        else
        {
            enable = false;
        }
    }
    else
    {
        skiq_error("Internal Error: PA state not valid, but PA state translation requested for "
                   "card %u\n", card);
        enable = false;
    }

    return enable;
}
#endif  /* DEBUG_PRINT_ENABLED */


/**
   @brief Translates from `bool` to `stretch_rfe_pa_state_t` taking a card's RF port configuration
   into account.
 */
static stretch_rfe_pa_state_t
rfe_pa_state_to_stretch( uint8_t card,
                         bool enable )
{
    stretch_rfe_pa_state_t state;

    if ( current_rf_port_state[card].valid )
    {
        if ( enable )
        {
            if ( current_rf_port_state[card].config == skiq_rf_port_config_fixed )
            {
                state = stretch_pa_enabled_fixed;
            }
            else
            {
                state = stretch_pa_enabled_trx;
            }
        }
        else
        {
            state = stretch_pa_disabled;
        }
    }
    else
    {
        skiq_error("Internal Error: RF port configuration not valid, but PA state "
                   "translation requested for card %u\n", card);
        state = stretch_pa_disabled;
    }

    return state;
}


static int32_t
select_rx_filter( uint8_t card,
                  uint8_t chip_id,
                  filter_selection_t *rxfs )
{
    int32_t status = 0;

    if ( status == 0 )
    {
        uint32_t x20_mask, x20_values;

        x20_mask =   (rxfs->x20_port0_mask << 0) | (rxfs->x20_port1_mask << 8) | (      x20_PORT2_MASK << 16);
        x20_values = (rxfs->x20_port0_valu << 0) | (rxfs->x20_port1_valu << 8) | (rxfs->x20_port2_valu << 16);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x20_ADDR, x20_mask, x20_values );
    }

    if ( status == 0 )
    {
        uint32_t x21_mask, x21_values;

        x21_mask =   (      x21_PORT0_MASK << 0) | (      x21_PORT1_MASK << 8);
        x21_values = (rxfs->x21_port0_valu << 0) | (rxfs->x21_port1_valu << 8);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x21_ADDR, x21_mask, x21_values );
    }

    if ( status == 0 )
    {
        uint32_t x22_mask, x22_values;

        x22_mask =   (      x22_PORT0_MASK << 0) | (      x22_PORT1_MASK << 8);
        x22_values = (rxfs->x22_port0_valu << 0) | (rxfs->x22_port1_valu << 8);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x22_ADDR, x22_mask, x22_values );
    }

    if ( status == 0 )
    {
        status = ad9361_write_rx_input_port( chip_id, rxfs->ad9361_port );
    }

    if ( status == 0 )
    {
        current_rx_filter[card] = rxfs;
    }

    return status;
}


static int32_t
select_tx_filter( uint8_t card,
                  uint8_t chip_id,
                  filter_selection_t *txfs )
{
    int32_t status = 0;

    /* The method used to select the transmit filter depends on the RF port configuration.
     *
     * If it is fixed (FDD), then 1 enables the filter path and 0 disables the filter path.
     *
     * If it is TRX (TDD), the Hi-Z enables the filter path (when the RF port operation is TX), and
     * 0 disables the filter path.
     */
    if ( current_rf_port_state[card].valid )
    {
        if ( current_rf_port_state[card].config == skiq_rf_port_config_fixed )
        {
            status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x20_ADDR,
                                                    txfs->x20_port0_mask, txfs->x20_port0_valu );
        }
        else
        {
            /* pins in x20_port0_mask should either be Hi-Z or low output */
            status = pcal6524_io_exp_set_as_hiz( card, M2_2280_IOE_I2C_BUS, x20_ADDR,
                                                 txfs->x20_port0_mask,
                                                 txfs->x20_port0_hiz,
                                                 txfs->x20_port0_hiz ^ txfs->x20_port0_valu /* high_mask */ );
        }
    }
    else
    {
        skiq_error("Internal error, TX filter selection without RF port configured on card %u\n", card);
        status = -EPROTO;
    }

    if ( status == 0 )
    {
        status = ad9361_write_tx_output_port( chip_id, txfs->ad9361_port );
    }

    if ( status == 0 )
    {
        current_tx_filter[card] = txfs;
    }

    return status;
}


static filter_selection_t *
find_filter_by_enum( filter_selection_t *fs_table,
                     uint8_t nr_entries,
                     skiq_filt_t filter )
{
    uint8_t i;
    filter_selection_t *fs_entry = NULL;

    for (i = 0; ( i < nr_entries ) && ( fs_entry == NULL ); i++)
    {
        if ( fs_table[i].filter == filter )
        {
            fs_entry = &(fs_table[i]);
        }
    }

    return fs_entry;
}


static filter_selection_t *
find_rx_filter_by_enum( skiq_filt_t filter )
{
    return find_filter_by_enum( rx_filter_selections, ARRAY_SIZE(rx_filter_selections), filter );
}


static filter_selection_t *
find_tx_filter_by_enum( skiq_filt_t filter )
{
    return find_filter_by_enum( tx_filter_selections, ARRAY_SIZE(tx_filter_selections), filter );
}


static filter_selection_t *
find_filter_by_freq( filter_selection_t *fs_table,
                     uint8_t nr_entries,
                     uint64_t freq )
{
    uint8_t i;
    filter_selection_t *fs_entry = NULL;

    TRACE_ENTER;
    TRACE_ARGS("fs_table: %p, nr_entries: %u, freq: %" PRIu64 "\n", fs_table, nr_entries, freq);

    for (i = 0; ( i < nr_entries ) && ( fs_entry == NULL ); i++)
    {
        if ( ( MHZ_TO_HZ(fs_table[i].lo) <= freq ) &&
             ( freq < MHZ_TO_HZ(fs_table[i].hi) ) )
        {
            fs_entry = &(fs_table[i]);
        }
    }

    return fs_entry;
}

static filter_selection_t *
find_rx_filter_by_freq( uint64_t freq )
{
    return find_filter_by_freq( rx_filter_selections, ARRAY_SIZE(rx_filter_selections), freq );
}

static filter_selection_t *
find_tx_filter_by_freq( uint64_t freq )
{
    return find_filter_by_freq( tx_filter_selections, ARRAY_SIZE(tx_filter_selections), freq );
}


static uint32_t
lna_cfg_output_values( const lna_setting_t *lna,
                       stretch_rfe_lna_state_t state )
{
    return ( state == stretch_lna_bypassed) ? lna->pin_bypass :
        (state == stretch_lna_disabled) ? lna->pin_disable :
        (state == stretch_lna_enabled_fixed) ? lna->pin_enable :
        (state == stretch_lna_enabled_trx) ? lna->pin_enable :
        lna->pin_disable;
}


/**
    LNA1
                Fixed                    TRX
    Bypassed    BT=0/Z, MB=1             BT=0/Z, MB=1
    Disabled    BT=1, MB=1               BT=1, MB=1
    Enabled     BT=1, MB=0               BT=1, MB=Z


    LNA2
                Fixed                    TRX
    Bypassed    BT=1, MB=1               BT=1, MB=1
    Disabled    BT=0/Z, MB=1             BT=0/Z, MB=1
    Enabled     BT=0/Z, MB=0             BT=0/Z, MB=Z
 */
static int32_t
update_rx_lna( uint8_t card,
               uint8_t lna_index,
               stretch_rfe_lna_state_t new_state )
{
    int32_t status = 0;

    if ( !current_lna_state[card][lna_index].valid ||
         ( new_state != current_lna_state[card][lna_index].state ) )
    {
        const lna_setting_t *lna = &(lna_settings[lna_index]);

        debug_print("Updating LNA%u to %s on card %u\n", lna->id,
                    stretch_lna_state_cstr( new_state ), card);
        switch (new_state)
        {
            case stretch_lna_bypassed:
            case stretch_lna_disabled:
            case stretch_lna_enabled_fixed:
                status = pcal6524_io_exp_set_as_output( card,
                                                        M2_2280_IOE_I2C_BUS,
                                                        lna->i2c_addr,
                                                        lna->pin_mask,
                                                        lna_cfg_output_values( lna, new_state ) );
                break;

            case stretch_lna_enabled_trx:
                status = pcal6524_io_exp_set_as_hiz( card,
                                                     M2_2280_IOE_I2C_BUS,
                                                     lna->i2c_addr,
                                                     lna->pin_mask,
                                                     lna->hiz_enable,
                                                     lna_cfg_output_values( lna, new_state ) );
                break;

            default:
                skiq_error("Unhandled LNA state (%u) requested on card %u\n", new_state, card);
                status = -EINVAL;
                break;
        }

        if ( status == 0 )
        {
            current_lna_state[card][lna_index].state = new_state;
            current_lna_state[card][lna_index].valid = true;
        }
    }

    return status;
}


static int32_t
disable_both_lna( uint8_t card )
{
    int32_t status = 0;

    status = update_rx_lna( card, LNA1, stretch_lna_disabled );
    if ( status == 0 )
    {
        status = update_rx_lna( card, LNA2, stretch_lna_disabled );
    }

    return status;
}


static int32_t
disable_each_lna_unless_bypassed( uint8_t card )
{
    int32_t status = 0;

    /* disable LNAs if they are enabled, leave them alone if they are bypassed */
    if ( IS_LNA_ENABLED( card, LNA1 ) && IS_LNA_ENABLED( card, LNA2 ) )
    {
        debug_print("Disabling LNA bias on LNA1 and LNA2 on card %u\n", card);
        status = disable_both_lna( card );
    }
    else if ( IS_LNA_ENABLED( card, LNA1 ) )
    {
        debug_print("Disabling LNA bias on LNA1 on card %u\n", card);
        status = update_rx_lna( card, LNA1, stretch_lna_disabled );
    }
    else if ( IS_LNA_ENABLED( card, LNA2 ) )
    {
        debug_print("Disabling LNA bias on LNA2 on card %u\n", card);
        status = update_rx_lna( card, LNA2, stretch_lna_disabled );
    }

    return status;
}


static int32_t
enable_both_lna( uint8_t card )
{
    int32_t status = 0;

    status = update_rx_lna( card, LNA1, rfe_lna_state_to_stretch( card, lna_enabled ) );
    if ( status == 0 )
    {
        status = update_rx_lna( card, LNA2, rfe_lna_state_to_stretch( card, lna_enabled ) );
    }

    return status;
}


static int32_t
enable_each_lna_unless_bypassed( uint8_t card )
{
    int32_t status = 0;

    if ( IS_LNA_DISABLED( card, LNA1 ) && IS_LNA_DISABLED( card, LNA2 ) )
    {
        debug_print("Enabling LNA bias on LNA1 and LNA2 on card %u\n", card);
        status = enable_both_lna( card );
    }
    else if ( IS_LNA_DISABLED( card, LNA1 ) )
    {
        debug_print("Enabling LNA bias on LNA1 on card %u\n", card);
        status = update_rx_lna( card, LNA1, rfe_lna_state_to_stretch( card, lna_enabled ) );
    }
    else if ( IS_LNA_DISABLED( card, LNA2 ) )
    {
        debug_print("Enabling LNA bias on LNA2 on card %u\n", card);
        status = update_rx_lna( card, LNA2, rfe_lna_state_to_stretch( card, lna_enabled ) );
    }

    return status;
}

/**
    PA
                Fixed      TRX
    Disabled    MB=1       MB=1
    Enabled     MB=0       MB=Z
 */
static int32_t
update_tx_pa( uint8_t card,
              stretch_rfe_pa_state_t new_state )
{
    int32_t status = 0;

    if ( !current_pa_state[card].valid || ( new_state != current_pa_state[card].state ) )
    {
        debug_print("Updating PA to %s on card %u\n", stretch_pa_state_cstr( new_state ), card );
        switch (new_state)
        {
            case stretch_pa_disabled:
                status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x20_ADDR, TX_AMP_MANUAL_BIAS_ON_N,
                                                        TX_AMP_MANUAL_BIAS_ON_N );
                break;

            case stretch_pa_enabled_fixed:
                status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x20_ADDR, TX_AMP_MANUAL_BIAS_ON_N, 0 );
                break;

            case stretch_pa_enabled_trx:
                status = pcal6524_io_exp_set_as_hiz( card, M2_2280_IOE_I2C_BUS, x20_ADDR, TX_AMP_MANUAL_BIAS_ON_N,
                                                     TX_AMP_MANUAL_BIAS_ON_N, 0 );
                break;

            default:
                skiq_error("Internal Error: Unhandled PA state (%u) requested on card %u\n",
                           new_state, card);
                status = -EINVAL;
                break;
        }

        if ( status == 0 )
        {
            current_pa_state[card].state = new_state;
            current_pa_state[card].valid = true;
        }
    }

    return status;
}


static int32_t
connect_rx_lineup( uint8_t card,
                   uint8_t chip_id,
                   skiq_rf_port_t port )
{
    int32_t status = 0;

    if ( skiq_rf_port_J1 == port )
    {
        /**
           Rx on J1: PL_CTRL_TDD_TXRX_N = LOW (Connect antenna J1 to RX Lineup)

           @note PL_CTRL_TDD_TXRX_N in FPGA design is output-only
         */
        debug_print("Connect antenna J1 to Rx lineup on card %u\n", card);
        status = sidekiq_fpga_reg_RMWV( card, 0, PL_CTRL_TDD_TXRX_N, FPGA_REG_GPIO_WRITE );
    }
    else if ( skiq_rf_port_J2 == port )
    {
        /* Rx on J2: I2C_CTRL_RX_ANT2_SEL = HIGH (Connect antenna J2 to Rx Lineup) */
        debug_print("Connect antenna J2 to Rx lineup on card %u\n", card);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, ANT_IO_EXP_ADDR,
                                                RX_ANT2_SEL, RX_ANT2_SEL );
    }
    else
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        /* enable LNAs if they are disabled, leave them alone if they are bypassed */
        status = enable_each_lna_unless_bypassed( card );
    }

    if ( status == 0 )
    {
        uint32_t rx_sel = current_rx_filter[card]->x20_port1_valu << 8;

        /*
          Set I2C_CTRL_RX1A_SEL or I2C_CTRL_RX1B_SEL or I2C_CTRL_RX1C_SEL = HIGH as appropriate

          Use the 'current RX filter' pointer to determine which RX port should be selected.
         */
        debug_print("Selecting RX switch as %s on card %u\n", rx_sel_cstr( rx_sel ), card);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, RX_SEL_IO_EXP_ADDR,
                                                RX_SEL_IO_EXP_MASK, rx_sel );
    }

    if ( status == 0 )
    {
        uint8_t ad9361_port = current_rx_filter[card]->ad9361_port;

        debug_print("Selecting RFIC RX input as %u on chip ID %u\n", ad9361_port, chip_id);
        status = ad9361_write_rx_input_port( chip_id, ad9361_port );
    }

    return status;
}


static int32_t
isolate_rx_lineup( uint8_t card,
                   uint8_t chip_id,
                   skiq_rf_port_t port )
{
    int32_t status;

    if ( skiq_rf_port_J1 == port )
    {
        /**
           Set GPO PL_CTRL_TDD_TXRX_N = HIGH (Isolate antenna J1 from Rx Lineup)

           @note PL_CTRL_TDD_TXRX_N in FPGA design is output-only
        */
        debug_print("Disconnect RF port J1 from Rx lineup on card %u\n", card);
        status = sidekiq_fpga_reg_RMWV( card, 1, PL_CTRL_TDD_TXRX_N, FPGA_REG_GPIO_WRITE );
    }
    else if ( skiq_rf_port_J2 == port )
    {
        /* Rx on J2: I2C_CTRL_RX_ANT2_SEL = LOW (Isolate antenna J2 from Rx Lineup) */
        debug_print("Disconnect RF port J2 from Rx lineup on card %u\n", card);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, ANT_IO_EXP_ADDR,
                                                RX_ANT2_SEL, 0 );
    }
    else
    {
        debug_print("Unknown / unsupported RF port provided (%u)\n", port);
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        /* disable LNAs if they are enabled, leave them alone if they are bypassed */
        status = disable_each_lna_unless_bypassed( card );
    }

    if ( status == 0 )
    {
        /*
          I2C_CTRL_RX1A_SEL = LOW  (Disconnect AD936x RX1A from RX Lineup)
          I2C_CTRL_RX1B_SEL = LOW  (Disconnect AD936x RX1B from RX Lineup)
          I2C_CTRL_RX1C_SEL = LOW  (Disconnect AD936x RX1C from RX Lineup)
        */
        debug_print("De-selecting all RX switches on card %u\n", card);
        status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, RX_SEL_IO_EXP_ADDR,
                                                RX_SEL_IO_EXP_MASK, 0 );
    }

    if ( status == 0 )
    {
        debug_print("De-selecting all RFIC RX inputs on chip ID %u\n", chip_id);
        status = ad9361_write_rx_input_port( chip_id, ad9361_rx_port_a );
    }

    return status;
}


/*************************************************************************************************/
static int32_t
write_rf_port_operation( uint8_t card,
                         bool transmit )
{
    int32_t status = 0;

    /**
       Set GPO PL_CTRL_TDD_TXRX_N to 0 (@a transmit is false) or 1 (@a transmit is true)

       @note PL_CTRL_TDD_TXRX_N in FPGA design is output-only
    */
    status = sidekiq_fpga_reg_RMWV( card, (transmit) ? 1 : 0, PL_CTRL_TDD_TXRX_N,
                                    FPGA_REG_GPIO_WRITE );
    if ( status == 0 )
    {
        current_rf_port_state[card].operation = transmit;
    }

    return status;
}


/*************************************************************************************************/
static int32_t
switch_rf_port( uint8_t card,
                uint8_t chip_id,
                skiq_rf_port_t from_port,
                skiq_rf_port_t to_port )
{
    int32_t status = 0;

    debug_print("Switching RF port from %s to %s on card %u and chip ID %u\n",
                rf_port_cstr( from_port ), rf_port_cstr( to_port ), card, chip_id );

    /* Configure RFFE for Receive on 'to_port' */
    if ( status == 0 )
    {
        uint32_t pin_mask;
        uint32_t output_mask;

        switch (to_port)
        {
            case skiq_rf_port_J1:
                pin_mask = RF_PORT_PIN_MASK;
                output_mask = RF_PORT_J1_VALUE;
                break;

            case skiq_rf_port_J2:
                pin_mask = RF_PORT_PIN_MASK;
                output_mask = RF_PORT_J2_VALUE;
                break;

            case skiq_rf_port_unknown:
                pin_mask = RF_PORT_PIN_MASK;
                output_mask = RF_PORT_NONE_VALUE;
                break;

            default:
                status = -EINVAL;
                break;
        }

        if ( status == 0 )
        {
            status = pcal6524_io_exp_set_as_output( card, M2_2280_IOE_I2C_BUS, x20_ADDR, pin_mask, output_mask );
        }
    }

    return status;
}


/*************************************************************************************************/
static int32_t
write_rf_port_config( rf_id_t *p_id,
                      skiq_rf_port_config_t config,
                      bool update_rfe_states )
{
    int32_t status = 0;

    switch (config)
    {
        case skiq_rf_port_config_fixed:
            /* Setup Rx on J2 */
            status = switch_rf_port( p_id->card, p_id->chip_id, current_rx_rf_port(p_id->card),
                                     skiq_rf_port_J2 );
            break;

        case skiq_rf_port_config_trx:
            /* Setup RX on J1 */
            status = switch_rf_port( p_id->card, p_id->chip_id, current_rx_rf_port(p_id->card),
                                     skiq_rf_port_J1 );
            break;

        default:
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        /* store RF port configuration and mark it valid for the time being */
        current_rf_port_state[p_id->card].config = config;
        current_rf_port_state[p_id->card].valid = true;

        /* Default RF port operation to 'receive', even in fixed mode, the FPGA (PL) output needs to
         * be in a well-defined state */
        status = write_rf_port_operation( p_id->card, false /* receive */ );
    }

    /* Update Tx PA state to its current state, possibly taking a RF port configuration into
     * consideration */
    if ( update_rfe_states && ( status == 0 ) )
    {
        status = update_tx_pa( p_id->card, stretch_rfe_pa_state( p_id->card ) );
    }

    /* Update RX LNA1 state to its current state, possibly taking an RF port configuration into
     * consideration */
    if ( update_rfe_states && ( status == 0 ) )
    {
        status = update_rx_lna( p_id->card, LNA1, stretch_rfe_lna_state( p_id->card, LNA1 ) );
    }

    /* Update RX LNA2 state to its current state, possibly taking an RF port configuration into
     * consideration */
    if ( update_rfe_states && ( status == 0 ) )
    {
        status = update_rx_lna( p_id->card, LNA2, stretch_rfe_lna_state( p_id->card, LNA2 ) );
    }

    /* if it turns out that writing the various elements of the RF port configuration did not
     * succeed, mark the RF port state as not valid */
    if ( status != 0 )
    {
        current_rf_port_state[p_id->card].valid = false;
    }

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_before_rx_lo_tune( rf_id_t *p_id,
                          uint64_t rx_lo_freq )
{
    int32_t status = 0;

    if ( status == 0 )
    {
        status = isolate_rx_lineup( p_id->card, p_id->chip_id, current_rx_rf_port(p_id->card) );
    }

    if ( status == 0 )
    {
        filter_selection_t *rxfs;

        rxfs = find_rx_filter_by_freq( rx_lo_freq );
        if ( rxfs == NULL )
        {
            status = -EINVAL;
        }
        else
        {
            debug_print("Selecting RX filter %s on card %u for LO frequency %" PRIu64 "\n",
                        SKIQ_FILT_STRINGS[ rxfs->filter ], p_id->card, rx_lo_freq);
            status = select_rx_filter( p_id->card, p_id->chip_id, rxfs );
        }
    }

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency AFTER the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_after_rx_lo_tune( rf_id_t *p_id,
                         uint64_t rx_lo_freq )
{
    int32_t status = 0;

    status = connect_rx_lineup( p_id->card, p_id->chip_id, current_rx_rf_port(p_id->card) );

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Tx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param tx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_before_tx_lo_tune( rf_id_t *p_id,
                          uint64_t tx_lo_freq )
{
    int32_t status = 0;
    filter_selection_t *txfs;

    txfs = find_tx_filter_by_freq( tx_lo_freq );
    if ( txfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting TX filter %s on card %u for LO frequency %" PRIu64 "\n",
                    SKIQ_FILT_STRINGS[ txfs->filter ], p_id->card, tx_lo_freq);
        status = select_tx_filter( p_id->card, p_id->chip_id, txfs );
    }

    return status;
}


/*************************************************************************************************/
/***** RFE FUNCTIONS *****/
/*************************************************************************************************/


/*************************************************************************************************/
/** @brief Enables or disables the Tx power amplifier.

    @param p_id Pointer to RF identifier struct.
    @param new_state New state for the PA

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_update_tx_pa( rf_id_t *p_id,
                      bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("new_state: %s\n", bool_cstr( enable ));
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    status = update_tx_pa( p_id->card, rfe_pa_state_to_stretch( p_id->card, enable ) );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/**************************************************************************************************/
/** The stretch_override_tx_pa() function allows the caller to override the PA state of the
    specified RF identifier by specifying a pin value that is highly specific to M.2-2280 rev A and
    B.

    @attention In order to restore the proper state of the Tx PA, the caller must call
    skiq_write_rf_port_config() twice, first specifying a configuration that is NOT the current /
    desired configuration, then again with the desired configuration.  The RFE caches the RF port
    configuration.

    @param[in] p_id Pointer to RF identifier struct
    @param[in] pin_value [::skiq_pin_value_t] desired pin value for TX_AMP_MANUAL_BIAS_ON_N

    @return int32_t
    @retval 0 Success
    @retval -ENOTSUP ATE_SUPPORT is not enabled for the library
    @retval -EINVAL Specified @a pin_value is unknown or unsupported
    @retval -EIO Input/output error communicating with the I/O expander
*/
int32_t
stretch_override_tx_pa( rf_id_t *p_id,
                        skiq_pin_value_t pin_value )
#if (defined ATE_SUPPORT)
{
    int32_t status = 0;
    uint32_t pin_mask = TX_AMP_MANUAL_BIAS_ON_N;
    uint32_t hiz_mask = 0, high_mask = 0;

    switch (pin_value)
    {
        case skiq_pin_value_hiz:
            hiz_mask = pin_mask;
            break;

        case skiq_pin_value_low:
            high_mask = 0;
            break;

        case skiq_pin_value_high:
            high_mask = pin_mask;
            break;

        default:
            skiq_error("Unknown / unsupported pin value %u for card %u\n", pin_value, p_id->card);
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        status = pcal6524_io_exp_set_as_hiz( p_id->card, M2_2280_IOE_I2C_BUS, x20_ADDR, pin_mask, hiz_mask, high_mask );
    }

    return status;
}
#else
{ return -ENOTSUP; }
#endif  /* ATE_SUPPORT */

/*************************************************************************************************/
/** @brief Enables / disables / bypasses all Rx low noise amplifiers in the RFE.

    @param p_id Pointer to RF identifier struct.
    @param new_state [::rfe_lna_state_t] New state for all LNAs

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_update_rx_lna( rf_id_t *p_id,
                       rfe_lna_state_t new_state )
{
    int32_t status = 0;
    uint8_t card = p_id->card;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("new_state: %s\n", rfe_lna_state_cstr( new_state ));
    stretch_print_states(p_id->card);

    LOCK_RFE(card);

    status = update_rx_lna( card, LNA1, rfe_lna_state_to_stretch( card, new_state) );
    if ( status == 0 )
    {
        status = update_rx_lna( card, LNA2, rfe_lna_state_to_stretch( card, new_state) );
    }

    UNLOCK_RFE(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Enables / disables / bypasses the specified Rx low noise amplifier.

    @param p_id Pointer to RF identifier struct.
    @param[in] idx LNA index of interest
    @param new_state [::rfe_lna_state_t] New state for the LNA

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_update_rx_lna_by_idx( rf_id_t *p_id,
                              uint8_t idx,
                              rfe_lna_state_t new_state )
{
    int32_t status = 0;
    uint8_t card = p_id->card;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("new_state: %s\n", rfe_lna_state_cstr( new_state ));
    stretch_print_states(p_id->card);

    LOCK_RFE(card);

    if ( idx == 0 )
    {
        status = update_rx_lna( card, LNA1, rfe_lna_state_to_stretch( card, new_state) );
    }
    else if ( idx == 1 )
    {
        status = update_rx_lna( card, LNA2, rfe_lna_state_to_stretch( card, new_state) );
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_update_before_rx_lo_tune( rf_id_t *p_id,
                                  uint64_t rx_lo_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rx_lo_freq: %" PRIu64 "\n", rx_lo_freq);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    status = update_before_rx_lo_tune( p_id, rx_lo_freq );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency AFTER the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_update_after_rx_lo_tune( rf_id_t *p_id,
                                 uint64_t rx_lo_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rx_lo_freq: %" PRIu64 "\n", rx_lo_freq);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    status = update_after_rx_lo_tune( p_id, rx_lo_freq );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Tx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param tx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_update_before_tx_lo_tune( rf_id_t *p_id,
                                  uint64_t tx_lo_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("tx_lo_freq: %" PRIu64 "\n", tx_lo_freq);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    status = update_before_tx_lo_tune( p_id, tx_lo_freq );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Configures the Rx filter configuration.

    @param p_id Pointer to RF identifier struct.
    @param path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_write_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t path )
{
    int32_t status = 0;
    filter_selection_t *rxfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("path: %s\n", filt_cstr(path));
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    rxfs = find_rx_filter_by_enum( path );
    if ( rxfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting RX filter %s on card %u as requested\n",
                    SKIQ_FILT_STRINGS[ rxfs->filter ], p_id->card);
        status = select_rx_filter( p_id->card, p_id->chip_id, rxfs );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Configures the Tx filter configuration for this RFE

    @param[in] p_id Pointer to RF identifier struct.
    @param[in] path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_write_tx_filter_path( rf_id_t *p_id,
                              skiq_filt_t path )
{
    int32_t status = 0;
    filter_selection_t *txfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("path: %s\n", filt_cstr(path));
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    txfs = find_tx_filter_by_enum( path );
    if ( txfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting TX filter %s on card %u as requested\n",
                    SKIQ_FILT_STRINGS[ txfs->filter ], p_id->card);
        status = select_tx_filter( p_id->card, p_id->chip_id, txfs );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the current Rx filter configuration.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_read_rx_filter_path( rf_id_t *p_id,
                             skiq_filt_t *p_path )
{
    int32_t status = 0;
    filter_selection_t *rxfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    rxfs = current_rx_filter[p_id->card];
    if ( rxfs == NULL )
    {
        status = -ERANGE;
    }
    else
    {
        *p_path = rxfs->filter;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the current Tx filter configuration.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_read_tx_filter_path( rf_id_t *p_id,
                             skiq_filt_t *p_path )
{
    int32_t status = 0;
    filter_selection_t *txfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    txfs = current_tx_filter[p_id->card];
    if ( txfs == NULL )
    {
        status = -ERANGE;
    }
    else
    {
        *p_path = txfs->filter;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the available Rx filters.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_filters Pointer to be updated with available filters.
    @param[out] p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_read_rx_filter_capabilities( rf_id_t *p_id,
                                     skiq_filt_t *p_filters,
                                     uint8_t *p_num_filters )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    *p_num_filters = ARRAY_SIZE(available_rx_filters);
    memcpy( p_filters, available_rx_filters, sizeof( available_rx_filters ) );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the available Tx filters.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_filters Pointer to be updated with available filters.
    @param[out] p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_read_tx_filter_capabilities( rf_id_t *p_id,
                                     skiq_filt_t *p_filters,
                                     uint8_t *p_num_filters )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    *p_num_filters = ARRAY_SIZE(available_tx_filters);
    memcpy( p_filters, available_tx_filters, sizeof( available_tx_filters ) );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_read_rx_rf_ports_avail( rf_id_t *p_id,
                                uint8_t *p_num_fixed_rf_ports,
                                skiq_rf_port_t *p_fixed_rf_port_list,
                                uint8_t *p_num_trx_rf_ports,
                                skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_num_fixed_rf_ports = 0;
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J2;

        *p_num_trx_rf_ports = 0;
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J1;
    }
    else
    {
        NOT_YET_IMPLEMENTED;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_read_rx_rf_port( rf_id_t *p_id,
                         skiq_rf_port_t *p_rf_port )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_rf_port = current_rx_rf_port(p_id->card);
    }
    else
    {
        NOT_YET_IMPLEMENTED;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_read_tx_rf_ports_avail( rf_id_t *p_id,
                                uint8_t *p_num_fixed_rf_ports,
                                skiq_rf_port_t *p_fixed_rf_port_list,
                                uint8_t *p_num_trx_rf_ports,
                                skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_num_fixed_rf_ports = 0;
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J1;

        *p_num_trx_rf_ports = 0;
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J1;
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_read_tx_rf_port( rf_id_t *p_id,
                         skiq_rf_port_t *p_rf_port )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_J1;
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_rf_port_config_avail( rf_id_t *p_id,
                              bool *p_fixed,
                              bool *p_trx )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_fixed = true;
        *p_trx = true;
    }
    else
    {
        NOT_YET_IMPLEMENTED;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_read_rf_port_operation( rf_id_t *p_id,
                                bool *p_transmit )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    *p_transmit = current_rf_port_state[p_id->card].operation;

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_write_rf_port_operation( rf_id_t *p_id,
                                 bool transmit )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("transmit: %s\n", bool_cstr(transmit));
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    status = write_rf_port_operation( p_id->card, transmit );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_write_rf_port_config( rf_id_t *p_id,
                              skiq_rf_port_config_t config )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("config: %s\n", rf_port_config_cstr(config));
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    switch (config)
    {
        case skiq_rf_port_config_fixed:
        case skiq_rf_port_config_trx:
            debug_print("Updating RF port config to %s on card %u\n", rf_port_config_cstr( config ),
                        p_id->card );
            status = write_rf_port_config( p_id, config, true /* update_rfe_states */ );
            break;

        default:
            skiq_error("Unhandled RF port configuration (%u) requested on card %u\n", config,
                       p_id->card);
            status = -EINVAL;
            break;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_enable_rx_chan( rf_id_t *p_id,
                        bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("enable: %s\n", bool_cstr(enable));
    stretch_print_states(p_id->card);

    LOCK_RFE(p_id->card);

    if ( enable )
    {
        status = enable_each_lna_unless_bypassed( p_id->card );
    }
    else
    {
        status = disable_each_lna_unless_bypassed( p_id->card );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_write_rx_freq_hop_list( rf_id_t *p_id,
                                uint16_t nr_freq,
                                uint64_t freq_list[] )
{
    int32_t status = 0;
    uint16_t i = 0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;
    uint64_t freq = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("nr_freq: %u\n", nr_freq);
    stretch_print_states(p_id->card);

    if ( nr_freq == 0 )
    {
        return -EINVAL;
    }

    LOCK_RFE(p_id->card);

    for ( i = 0; ( i < nr_freq ) && ( status == 0 ); i++ )
    {
        filter_selection_t *pfs;

        pfs = find_rx_filter_by_freq( freq_list[i] );
        if ( pfs == NULL )
        {
            status = -EINVAL;
        }
        else if ( pfs->filter == skiq_filt_invalid )
        {
            status = -EPROTO;
        }
        else if ( ( skiq_filt != skiq_filt_invalid ) &&
                  ( skiq_filt != pfs->filter ) )
        {
            status = -EINVAL;
            skiq_error("All frequencies in the Rx hop list must be within a single frequency "
                       "filter band for Sidekiq %s on card %u\n",
                       part_cstr( _skiq_get_part( p_id->card ) ), p_id->card);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq,
                       SKIQ_FILT_STRINGS[skiq_filt]);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq_list[i],
                       SKIQ_FILT_STRINGS[pfs->filter]);
        }
        else
        {
            skiq_filt = pfs->filter;
            freq = freq_list[i];
        }
    }

    /* if the frequencies are within a band, then actually apply it.  Since we know all of the
     * frequencies use the same filter setting, we can just set RFE for the first frequency in the
     * hop list */
    if ( status == 0 )
    {
        status = update_before_rx_lo_tune( p_id, freq_list[0] );
    }

    if ( status == 0 )
    {
        status = update_after_rx_lo_tune( p_id, freq_list[0] );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
stretch_write_tx_freq_hop_list( rf_id_t *p_id,
                                uint16_t nr_freq,
                                uint64_t freq_list[] )
{
    int32_t status = 0;
    uint16_t i = 0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;
    uint64_t freq = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("nr_freq: %u\n", nr_freq);
    stretch_print_states(p_id->card);

    if ( nr_freq == 0 )
    {
        return -EINVAL;
    }

    LOCK_RFE(p_id->card);

    for ( i = 0; ( i < nr_freq ) && ( status == 0 ); i++ )
    {
        filter_selection_t *pfs;

        pfs = find_tx_filter_by_freq( freq_list[i] );
        if ( pfs == NULL )
        {
            status = -EINVAL;
        }
        else if ( pfs->filter == skiq_filt_invalid )
        {
            status = -EPROTO;
        }
        else if ( ( skiq_filt != skiq_filt_invalid ) &&
                  ( skiq_filt != pfs->filter ) )
        {
            status = -EINVAL;
            skiq_error("All frequencies in the Tx hop list must be within a single frequency "
                       "filter band for Sidekiq %s on card %u\n",
                       part_cstr( _skiq_get_part( p_id->card ) ), p_id->card);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq,
                       SKIQ_FILT_STRINGS[skiq_filt]);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq_list[i],
                       SKIQ_FILT_STRINGS[pfs->filter]);
        }
        else
        {
            skiq_filt = pfs->filter;
            freq = freq_list[i];
        }
    }

    /* if the frequencies are within a band, then actually apply it.  Since we know all of the
     * frequencies use the same filter setting, we can just set RFE for the first frequency in the
     * hop list */
    if ( status == 0 )
    {
        status = update_before_tx_lo_tune( p_id, freq_list[0] );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Initializes the RFE for the M.2 2280 card.

    @param p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_init( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 0. print all available filters if debug */
    stretch_print_rx_filters();
    stretch_print_tx_filters();

    /* 0. Unconditionally unset both LNA valid flags, Tx PA state valid flag and RF port
     * configuration valid flag, nothing can be trusted during initialization */
    current_lna_state[p_id->card][LNA1].valid = false;
    current_lna_state[p_id->card][LNA2].valid = false;
    current_pa_state[p_id->card].valid = false;
    current_rf_port_state[p_id->card].valid = false;

    /* 1. Set default RF port configuration for fixed */
    if ( status == 0 )
    {
        status = write_rf_port_config( p_id, skiq_rf_port_config_fixed,
                                       false /* update_rfe_states */ );
    }

    /* 1a. Set receive filter to standby */
    if ( status == 0 )
    {
        debug_print("Selecting RX filter 'standby' on card %u\n", p_id->card);
        status = select_rx_filter( p_id->card, p_id->chip_id, &rx_filter_standby );
    }

    /* 1b. Set transmit filter to standby */
    if ( status == 0 )
    {
        debug_print("Selecting TX filter 'standby' on card %u\n", p_id->card);
        status = select_tx_filter( p_id->card, p_id->chip_id, &tx_filter_standby );
    }

    /* 2. Disable both LNAs */
    if ( status == 0 )
    {
        status = disable_both_lna( p_id->card );
    }

    /* 3. Disable Tx PA */
    if ( status == 0 )
    {
        status = update_tx_pa( p_id->card, stretch_pa_disabled );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Shuts down the RFE for the M.2 2280 card.

    @param p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
stretch_release( rf_id_t *p_id )
{
    int32_t status = 0, op_status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 1. Set RF port configuration to 'fixed' */
    op_status = write_rf_port_config( p_id, skiq_rf_port_config_fixed,
                                      false /* update_rfe_states */ );
    status = NONZERO(op_status,status);
    if(op_status != 0)
    {
        debug_print("Unable to set RF port configuration to 'fixed' with status: %" PRIi32 "\n",
                    op_status);
    }

    /* 2. Disable Tx PA */
    op_status = update_tx_pa( p_id->card, stretch_pa_disabled );
    status = NONZERO(op_status,status);
    if(op_status != 0)
    {
        debug_print("Unable to disable Tx PA with status: %" PRIi32 "\n", op_status);
    }

    /* 3. Disable both LNAs */
    op_status = disable_both_lna( p_id->card );
    status = NONZERO(op_status,status);
    if(op_status != 0)
    {
        debug_print("Unable to disable both LNAs with status: %" PRIi32 "\n", op_status);
    }

    /* 4a. Set receive filter to standby */
    debug_print("Selecting RX filter 'standby' on card %u\n", p_id->card);
    op_status = select_rx_filter( p_id->card, p_id->chip_id, &rx_filter_standby );
    status = NONZERO(op_status,status);
    if(op_status != 0)
    {
        debug_print("Unable to set receive filter to standby with status: %" PRIi32 "\n",
                    op_status);
    }

    /* 4b. Set transmit filter to standby */
    debug_print("Selecting TX filter 'standby' on card %u\n", p_id->card);
    op_status = select_tx_filter( p_id->card, p_id->chip_id, &tx_filter_standby );
    status = NONZERO(op_status,status);
    if(op_status != 0)
    {
        debug_print("Unable to set transmit filter to standby with status: %" PRIi32 "\n",
                    op_status);
    }

    /* 5. Unconditionally unset both LNA state valid flags, Tx PA state valid flag and RF port
     * configuration valid flag */
    current_lna_state[p_id->card][LNA1].valid = false;
    current_lna_state[p_id->card][LNA2].valid = false;
    current_pa_state[p_id->card].valid = false;
    current_rf_port_state[p_id->card].valid = false;

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

static int32_t
stretch_config_custom_rx_filter( rf_id_t *p_id,
                                 uint8_t filt_index,
                                 uint8_t hpf_lpf_setting,
                                 uint8_t rx_fltr1_sw,
                                 uint8_t rfic_port )
#if (defined ATE_SUPPORT)
{
    int32_t status = 0;

    if ( rfic_port > ad9361_rx_port_c )
    {
        status = -EINVAL;
    }

    if ( rx_fltr1_sw > 2 )
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        switch (filt_index)
        {
            case 1:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR1_SEL(rx_fltr1_sw),
                                      hpf_lpf_setting,_OFF,_OFF,_OFF);
                break;

            case 2:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR2_SEL,
                                      _OFF,hpf_lpf_setting,_OFF,_OFF);
                break;

            case 3:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR3_SEL,
                                      _OFF,_OFF,hpf_lpf_setting,_OFF);
                break;

            case 4:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR4_SEL,
                                      _OFF,_OFF,_OFF,hpf_lpf_setting);
                break;

            default:
                status = -EINVAL;
                break;
        }
    }

    if ( status == 0 )
    {
        debug_print("Selecting factory filter on card %u\n", p_id->card);
        status = select_rx_filter( p_id->card, p_id->chip_id, &rx_filter_factory );
    }

    return status;
}
#else
{ return -ENOTSUP; }
#endif  /* ATE_SUPPORT */

int32_t 
stretch_read_rf_capabilities(   rf_id_t *p_id, 
                                skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void) p_id;

    p_rf_capabilities->minTxFreqHz = M2_2280_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = M2_2280_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = M2_2280_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = M2_2280_MAX_RX_FREQUENCY_IN_HZ;


    return (status);
}

/***** GLOBAL DATA *****/

rfe_functions_t rfe_m2_2280_ab_funcs =
{
    .init                           = stretch_init,
    .release                        = stretch_release,
    .update_tx_pa                   = stretch_update_tx_pa,
    .override_tx_pa                 = stretch_override_tx_pa,
    .update_rx_lna                  = stretch_update_rx_lna,
    .update_rx_lna_by_idx           = stretch_update_rx_lna_by_idx,
    .update_before_rx_lo_tune       = stretch_update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = stretch_update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = stretch_update_before_tx_lo_tune,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = stretch_write_rx_filter_path,
    .write_tx_filter_path           = stretch_write_tx_filter_path,
    .read_rx_filter_path            = stretch_read_rx_filter_path,
    .read_tx_filter_path            = stretch_read_tx_filter_path,
    .read_rx_filter_capabilities    = stretch_read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = stretch_read_tx_filter_capabilities,
    .write_filters                  = NULL,
    .write_rx_attenuation           = NULL,
    .read_rx_rf_ports_avail         = stretch_read_rx_rf_ports_avail,
    .read_rx_rf_port                = stretch_read_rx_rf_port,
    .write_rx_rf_port               = NULL,
    .read_tx_rf_ports_avail         = stretch_read_tx_rf_ports_avail,
    .read_tx_rf_port                = stretch_read_tx_rf_port,
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = stretch_rf_port_config_avail,
    .write_rf_port_config           = stretch_write_rf_port_config,
    .read_rf_port_operation         = stretch_read_rf_port_operation,
    .write_rf_port_operation        = stretch_write_rf_port_operation,
    .read_bias_index                = NULL,
    .write_bias_index               = NULL,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = stretch_enable_rx_chan,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = stretch_write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = stretch_write_tx_freq_hop_list,
    .config_custom_rx_filter        = stretch_config_custom_rx_filter,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = NULL,
    .swap_rx_port_config            = NULL,
    .read_rf_capabilities           = stretch_read_rf_capabilities,
};


/***** GLOBAL FUNCTIONS *****/

int32_t
rfe_m2_2280_override_rx_lna_by_idx( rf_id_t *p_id,
                                    uint8_t idx,
                                    skiq_pin_value_t pin_value )
#if (defined ATE_SUPPORT)
{
    int32_t status = 0;
    uint32_t pin_mask = 0, hiz_mask = 0, high_mask = 0;

    if ( idx == LNA1 )
    {
        pin_mask = LNA1_MANUAL_BIAS_EN_N;
    }
    else if ( idx == LNA2 )
    {
        pin_mask = LNA2_MANUAL_BIAS_EN_N;
    }
    else
    {
        skiq_error("LNA index %u out of range for card %u\n", idx, p_id->card);
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        switch (pin_value)
        {
            case skiq_pin_value_hiz:
                hiz_mask = pin_mask;
                break;

            case skiq_pin_value_low:
                high_mask = 0;
                break;

            case skiq_pin_value_high:
                high_mask = pin_mask;
                break;

            default:
                skiq_error("Unknown / unsupported pin value %u for card %u\n", pin_value,
                           p_id->card);
                status = -EINVAL;
                break;
        }
    }

    if ( status == 0 )
    {
        status = pcal6524_io_exp_set_as_hiz( p_id->card, M2_2280_IOE_I2C_BUS, x20_ADDR, pin_mask, hiz_mask, high_mask );
    }

    return status;
}
#else
{ return -ENOTSUP; }
#endif  /* ATE_SUPPORT */




