/*****************************************************************************/
/** @file rfic_rpc.c

This file provides the implementatation for the RFIC functions specific to the
use of RPC calls used by the network transport.
 
 <pre>
 Copyright 2021 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <math.h>

#include "rfic_rpc.h"
#include "sidekiq_rpc_client_api.h"
#include "sidekiq_private.h"
#include "sidekiq_rfic.h"

// Converts uint64 to host or network form
// https://stackoverflow.com/questions/3022552/is-there-any-standard-htonl-like-function-for-64-bits-integers-in-c
#define htonll(x) ((1==htonl(1)) ? (x) : ((uint64_t)htonl((x) & 0xFFFFFFFF) << 32) | htonl((x) >> 32))
#define ntohll(x) ((1==ntohl(1)) ? (x) : ((uint64_t)ntohl((x) & 0xFFFFFFFF) << 32) | ntohl((x) >> 32))


///////////////////////////////////////////////////////////////////////////////////////////////////
// RFIC functions
static int32_t _reset_and_init( rf_id_t *p_id );
static int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode );
static int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode );
static int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );
static int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );
static int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
static int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );
static int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );
static int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
static int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode );
static int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode );
static int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask );
static int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *cal_mask );

///////////////////////////////////////////////////////////////////////////////////////////////////

// non-RPC function pointers...we need to save these off for when RPC functions are removed
rfic_functions_t *p_non_rpc_funcs[SKIQ_MAX_NUM_CARDS] = { [0 ... SKIQ_MAX_NUM_CARDS-1] = NULL };

///////////////////////////////////////////////////////////////////////////////////////////////////
// RPC function pointer manipulation functions
int32_t rfic_add_rpc_functions( uint8_t card, rfic_functions_t *p_funcs )
{
    int32_t status=0;
    
    if( sidekiq_rpc_is_client_avail( card ) == true )
    {
        // we need to save the non RPC functions to be restored in the future..if the functions aren't null, assume they've already been saved
        if( p_non_rpc_funcs[card] == NULL )
        {
            p_non_rpc_funcs[card] = (rfic_functions_t*)(malloc( sizeof(rfic_functions_t) ));
            if( p_non_rpc_funcs[card] != NULL )
            {
                // save the non RPC calls before overwriting
                memcpy( p_non_rpc_funcs[card],
                        p_funcs,
                        sizeof(rfic_functions_t) );
            }
            else
            {
                skiq_error("Unable to allocate memory to save copy of RFIC functions");
                status = -ENOMEM;
            }
        }
            
        if( status==0 )
        {
            // TODO: is there a better way to do this?
            p_funcs->reset_and_init = _reset_and_init;
            p_funcs->write_rx_gain_mode = _write_rx_gain_mode;
            p_funcs->read_rx_gain_mode = _read_rx_gain_mode;
            p_funcs->write_rx_sample_rate_and_bandwidth = _write_rx_sample_rate_and_bandwidth;
            p_funcs->read_rx_sample_rate = _read_rx_sample_rate;
            p_funcs->read_rx_chan_bandwidth = _read_rx_chan_bandwidth;
            p_funcs->write_tx_sample_rate_and_bandwidth = _write_tx_sample_rate_and_bandwidth;
            p_funcs->read_tx_sample_rate = _read_tx_sample_rate;
            p_funcs->read_tx_chan_bandwidth = _read_tx_chan_bandwidth;
            p_funcs->write_rx_cal_mode = _write_rx_cal_mode;
            p_funcs->write_tx_quadcal_mode = _write_tx_quadcal_mode;
            p_funcs->write_rx_cal_mask = _write_rx_cal_mask;
            p_funcs->read_rx_cal_mask = _read_rx_cal_mask;
        }
    }

    return (status);
}

int32_t rfic_remove_rpc_functions( uint8_t card, rfic_functions_t *p_funcs )
{
    int32_t status=0;
    
    if( p_non_rpc_funcs[card] != NULL )
    {
        // TODO: is there a better way to do this?
        p_funcs->reset_and_init = p_non_rpc_funcs[card]->reset_and_init;
        p_funcs->write_rx_gain_mode = p_non_rpc_funcs[card]->write_rx_gain_mode;
        p_funcs->read_rx_gain_mode = p_non_rpc_funcs[card]->read_rx_gain_mode;
        p_funcs->write_rx_sample_rate_and_bandwidth = p_non_rpc_funcs[card]->write_rx_sample_rate_and_bandwidth;
        p_funcs->read_rx_sample_rate = p_non_rpc_funcs[card]->read_rx_sample_rate;
        p_funcs->read_rx_chan_bandwidth = p_non_rpc_funcs[card]->read_rx_chan_bandwidth;
        p_funcs->write_tx_sample_rate_and_bandwidth = p_non_rpc_funcs[card]->write_tx_sample_rate_and_bandwidth;
        p_funcs->read_tx_sample_rate = p_non_rpc_funcs[card]->read_tx_sample_rate;
        p_funcs->read_tx_chan_bandwidth = p_non_rpc_funcs[card]->read_tx_chan_bandwidth;
        p_funcs->write_rx_cal_mode = p_non_rpc_funcs[card]->write_rx_cal_mode;
        p_funcs->write_tx_quadcal_mode = p_non_rpc_funcs[card]->write_tx_quadcal_mode;
        p_funcs->write_rx_cal_mask = p_non_rpc_funcs[card]->write_rx_cal_mask;
        p_funcs->read_rx_cal_mask = p_non_rpc_funcs[card]->read_rx_cal_mask;

        FREE_IF_NOT_NULL( p_non_rpc_funcs[card] );
        p_non_rpc_funcs[card] = NULL;
    }

    return (status);
}
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
// RFIC functions
// Functions called on the client side to initiate the RPC call.
int32_t _reset_and_init( rf_id_t *p_id )
{
    int32_t status=0;
    
    status = rpc_client_rfic_reset_and_init( p_id->card );

    return (status);
}

int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode )
{
    int32_t status=0;

    status = rpc_client_rfic_write_rx_gain_mode( p_id->card, p_id->hdl, gain_mode );

    return (status);
}

int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode )
{
    int32_t status=0;

    status = rpc_client_rfic_read_rx_gain_mode( p_id->card, p_id->hdl, p_gain_mode );

    return (status);
}

int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status=0;

    status = rpc_client_rfic_write_rx_sample_rate_and_bandwidth( p_id->card, p_id->hdl, rate, bandwidth );

    return (status);
}

int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status = 0;

    status = rpc_client_rfic_read_rx_sample_rate( p_id->card, p_id->hdl, p_rate, p_actual_rate );

    return (status);
}

int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;

    status = rpc_client_rfic_read_rx_chan_bandwidth( p_id->card, p_id->hdl, p_bandwidth, p_actual_bandwidth );

    return (status);
}

int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status=0;

    status = rpc_client_rfic_write_tx_sample_rate_and_bandwidth( p_id->card, p_id->hdl, rate, bandwidth );

    return (status);
}

int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;

    status = rpc_client_rfic_read_tx_sample_rate( p_id->card, p_id->hdl, p_rate, p_actual_rate );

    return (status);
}

int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;

    status = rpc_client_rfic_read_tx_chan_bandwidth( p_id->card, p_id->hdl, p_bandwidth, p_actual_bandwidth );

    return (status);
}

int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode )
{
    int32_t status=0;

    status = rpc_client_rfic_write_rx_cal_mode( p_id->card, p_id->hdl, mode );

    return (status);
}

int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode )
{
    int32_t status=0;

    status = rpc_client_rfic_write_tx_quadcal_mode( p_id->card, p_id->hdl, mode );

    return (status);
}

int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask )
{
    int32_t status=0;

    status = rpc_client_rfic_write_rx_cal_mask( p_id->card, p_id->hdl, cal_mask );

    return (status);
}

int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    status = rpc_client_rfic_read_rx_cal_mask( p_id->card, p_id->hdl, p_cal_mask );

    return (status);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
// handle RFIC functions on server
// The handle function is called on the server side when the RPC call is received from the client
int32_t handle_rpc_rfic_reset_and_init( uint8_t card )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);

    // get RFIC instance
    rfic_instance = _skiq_get_generic_rfic_instance( card );

    status = rfic_reset_and_init( &rfic_instance );

    return (status);   
}

int32_t handle_rpc_rfic_write_rx_gain_mode( uint8_t card, skiq_rx_hdl_t hdl, skiq_rx_gain_t gain_mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_write_rx_gain_mode( &rfic_instance, gain_mode );

    return (status);
}

int32_t handle_rpc_rfic_read_rx_gain_mode( uint8_t card,
                                           skiq_rx_hdl_t hdl, 
                                           skiq_rx_gain_t *p_gain_mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_read_rx_gain_mode( &rfic_instance, p_gain_mode );

    return (status);
}

int32_t handle_rpc_rfic_write_rx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_rx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_write_rx_sample_rate_and_bandwidth( &rfic_instance, 
                                                      sample_rate,
                                                      bandwidth );

    return (status);
}

int32_t handle_rpc_rfic_read_rx_sample_rate( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_read_rx_sample_rate( &rfic_instance, p_rate, p_actual_rate );

    return (status);
}

int32_t handle_rpc_rfic_read_rx_chan_bandwidth( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_read_rx_chan_bandwidth( &rfic_instance, p_bandwidth, p_actual_bandwidth );

    return (status);
}

int32_t handle_rpc_rfic_write_tx_sample_rate_and_bandwidth( uint8_t card,
                                                            skiq_tx_hdl_t hdl, 
                                                            uint32_t sample_rate,
                                                            uint32_t bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_TX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    status = rfic_write_tx_sample_rate_and_bandwidth( &rfic_instance, sample_rate, bandwidth );

    return (status);
}

int32_t handle_rpc_rfic_read_tx_sample_rate( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint32_t *p_rate, 
                                             double *p_actual_rate )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_TX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    status = rfic_read_tx_sample_rate( &rfic_instance, p_rate, p_actual_rate );

    return (status);
}

int32_t handle_rpc_rfic_read_tx_chan_bandwidth( uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                uint32_t *p_bandwidth,
                                                uint32_t *p_actual_bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_TX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    status = rfic_read_tx_chan_bandwidth( &rfic_instance, p_bandwidth, p_actual_bandwidth );

    return (status);
}

int32_t handle_rpc_rfic_write_rx_cal_mode( uint8_t card, skiq_rx_hdl_t hdl, skiq_rx_cal_mode_t mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_write_rx_cal_mode( &rfic_instance, mode );

     return (status);
}

int32_t handle_rpc_rfic_write_tx_quadcal_mode( uint8_t card, skiq_tx_hdl_t hdl, skiq_tx_quadcal_mode_t mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_TX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    status = rfic_write_tx_quadcal_mode( &rfic_instance, mode );

     return (status);
}

int32_t handle_rpc_rfic_write_rx_cal_mask( uint8_t card, skiq_rx_hdl_t hdl, uint32_t cal_mask )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_write_rx_cal_mask( &rfic_instance, cal_mask );

     return (status);
}

int32_t handle_rpc_rfic_read_rx_cal_mask( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint32_t *p_mask )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_VALID_RX_HDL( card, hdl );

    // get RFIC instance
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    status = rfic_read_rx_cal_mask( &rfic_instance, p_mask );

    return (status);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
