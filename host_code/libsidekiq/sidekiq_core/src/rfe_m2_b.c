/*****************************************************************************/
/** @file rfe_m2_b.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <stdlib.h>
#include <string.h>
#include "rfe_mpcie_b.h"

#include "bit_ops.h"

#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "ad9361_driver.h"
#include "sidekiq_private.h"

#define HIGH_BAND_THRESHOLD_FREQ (3000000000llu)

static const skiq_filt_t _filter_list[] =
{
    skiq_filt_0_to_3000_MHz,
    skiq_filt_3000_to_6000_MHz
};

#define M2_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define M2_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define M2_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define M2_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

static bool _rf_port_trx_mode[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = false };

static skiq_hw_vers_t _hw_vers[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = skiq_hw_vers_invalid};

#define _NUM_FILTERS (sizeof(_filter_list)/sizeof(skiq_filt_t))

#define AD9361_GPIO_TX_BAND_POS      (1)  // 1=highband (>3G), 0=lowband (<3G)
#define AD9361_GPIO_RX_BAND_POS      (0)  // 1=highband (>3G), 0=lowband (<3G)

static int32_t _init( rf_id_t *p_id );

static int32_t _update_tx_pa( rf_id_t *p_id,
                              bool enable );
static int32_t _update_rx_lna( rf_id_t *p_id,
                              rfe_lna_state_t new_state );
static int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                          uint64_t rx_lo_freq );
static int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                         uint64_t rx_lo_freq );
static int32_t _update_tx_lo( rf_id_t *p_id,
                              uint64_t tx_lo_freq );
static int32_t _write_rx_filter_path( rf_id_t *p_id,
                                       skiq_filt_t path );
static int32_t _write_tx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t path );
static int32_t _read_rx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t *p_path );
static int32_t _read_tx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t *p_path );
static int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters );
static int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                              skiq_filt_t *p_filters,
                                              uint8_t *p_num_filters );
static int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );
static int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
static int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );
static int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
static int32_t _rf_port_config_avail( rf_id_t *p_id, bool *p_fixed, bool *p_trx );
static int32_t _write_rf_port_config( rf_id_t *p_id, skiq_rf_port_config_t config );
static int32_t _read_rf_port_operation( rf_id_t *p_id, bool *p_transmit );
static int32_t _write_rf_port_operation( rf_id_t *p_id, bool transmit );

static int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );
static int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );

static int32_t _calc_filt_for_hopping( uint16_t num_freq, uint64_t freq_list[], skiq_filt_t *p_filt );
static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

rfe_functions_t rfe_m2_b_funcs =
{
    .init                           = _init,
    .release                        = NULL,
    .update_tx_pa                   = _update_tx_pa,
    .override_tx_pa                 = NULL,
    .update_rx_lna                  = _update_rx_lna,
    .update_rx_lna_by_idx           = NULL,
    .update_before_rx_lo_tune       = _update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = _update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = _update_tx_lo,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = _write_rx_filter_path,
    .write_tx_filter_path           = _write_tx_filter_path,
    .read_rx_filter_path            = _read_rx_filter_path,
    .read_tx_filter_path            = _read_tx_filter_path,
    .read_rx_filter_capabilities    = _read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = _read_tx_filter_capabilities,
    .write_filters                  = NULL,
    .write_rx_attenuation           = NULL,
    .read_rx_attenuation            = NULL,
    .read_rx_attenuation_range      = NULL,
    .write_rx_attenuation_mode      = NULL,
    .read_rx_attenuation_mode       = NULL,
    .read_rx_rf_ports_avail         = _read_rx_rf_ports_avail,
    .read_rx_rf_port                = _read_rx_rf_port,
    .write_rx_rf_port               = NULL,
    .read_tx_rf_ports_avail         = _read_tx_rf_ports_avail,
    .read_tx_rf_port                = _read_tx_rf_port,
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = _rf_port_config_avail,
    .write_rf_port_config           = _write_rf_port_config,
    .read_rf_port_operation         = _read_rf_port_operation,
    .write_rf_port_operation        = _write_rf_port_operation,
    .read_bias_index                = NULL,
    .write_bias_index               = NULL,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = NULL,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = _write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = _write_tx_freq_hop_list,
    .config_custom_rx_filter        = NULL,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = NULL,
    .swap_rx_port_config            = NULL,    
    .read_rf_capabilities           = _read_rf_capabilities,
};

int32_t _init( rf_id_t *p_id )
{
    _hw_vers[p_id->card] = _skiq_get_hw_vers(p_id->card);

    return (0);
}

int32_t _update_tx_pa( rf_id_t *p_id,
                       bool enable )
{
    int32_t status=-1;
    uint32_t data=0;

    sidekiq_fpga_reg_read(p_id->card, FPGA_REG_RF_BAND_SEL, &data);
    if( enable == true )
    {
        if( p_id->hdl == skiq_tx_hdl_A1 )
        {
            BF_SET(data, 1, TX_ENA_OFFSET,TX_ENA_LEN);
        }
        else
        {
            BF_SET(data, 1, TX_ENA_A2_OFFSET, TX_ENA_A2_LEN);
        }
    }
    else
    {
        if( p_id->hdl == skiq_tx_hdl_A1 )
        {
            BF_SET(data, 0, TX_ENA_OFFSET,TX_ENA_LEN);
        }
        else
        {
            BF_SET(data, 0, TX_ENA_A2_OFFSET, TX_ENA_A2_LEN);
        }
    }
    sidekiq_fpga_reg_write(p_id->card, FPGA_REG_RF_BAND_SEL, data);
    status=sidekiq_fpga_reg_verify(p_id->card, FPGA_REG_RF_BAND_SEL, data);
    
    return (status);
}

static int32_t _update_rx_lna_by_hdl( uint8_t card,
                                      skiq_rx_hdl_t hdl,
                                      uint8_t value )
{
    int32_t status = -EINVAL;

    if ( hdl == skiq_rx_hdl_A1 )
    {
        status = sidekiq_fpga_reg_RMWV( card, value, LNA_ENA, FPGA_REG_RF_BAND_SEL);
    }
    else if ( hdl == skiq_rx_hdl_A2 )
    {
        status = sidekiq_fpga_reg_RMWV( card, value, LNA_ENA_A2, FPGA_REG_RF_BAND_SEL);
    }

    return (status);
}

int32_t _update_rx_lna( rf_id_t *p_id,
                        rfe_lna_state_t new_state )
{
    int32_t status;

    switch (new_state)
    {
        case lna_enabled:
            status = _update_rx_lna_by_hdl( p_id->card, p_id->hdl, 1 );
            break;

        case lna_disabled:
            status = _update_rx_lna_by_hdl( p_id->card, p_id->hdl, 0 );
            break;

        case lna_bypassed:
            status = -ENOTSUP;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return (status);
}

int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                   uint64_t rx_lo_freq )
{
    return _update_rx_lna( p_id, lna_disabled );
}

int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                  uint64_t rx_lo_freq )
{
    int32_t status=-1;
    skiq_filt_t filt;

    // update the preselect filter and LNA selection

    // figure out if we're low or high band
    if( rx_lo_freq > HIGH_BAND_THRESHOLD_FREQ )
    {
        filt = skiq_filt_3000_to_6000_MHz;
        // turn on the LNA
        status = _update_rx_lna( p_id, lna_enabled );
    }
    else
    {
        filt = skiq_filt_0_to_3000_MHz;
        // turn off the LNA
        status = _update_rx_lna( p_id, lna_disabled );
    }

    if ( status == 0 )
    {
        status = _write_rx_filter_path( p_id, filt );
    }

    return (status);
}

int32_t _update_tx_lo( rf_id_t *p_id,
                       uint64_t tx_lo_freq )
{
    int32_t status=-1;
    skiq_filt_t filt;
    
    // update the preselect filter path
    if( tx_lo_freq > HIGH_BAND_THRESHOLD_FREQ )
    {
        filt = skiq_filt_3000_to_6000_MHz;
    }
    else
    {
        filt = skiq_filt_0_to_3000_MHz;
    }
    status=_write_tx_filter_path( p_id, filt );
    return (status);
}

int32_t _write_rx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status=-1;

    switch( path )
    {
        case skiq_filt_0_to_3000_MHz:
            if( (status=ad9361_write_rx_input_port(p_id->chip_id,
                                                   ad9361_rx_port_b)) == 0 )
            {
                status=ad9361_write_gpio_ctrl( p_id->card,
                                               AD9361_GPIO_RX_BAND_POS,
                                               false );
            }
            break;

        case skiq_filt_3000_to_6000_MHz:
            if( (status=ad9361_write_rx_input_port( p_id->chip_id,
                                                    ad9361_rx_port_a )) == 0 )
            {
                status=ad9361_write_gpio_ctrl( p_id->card,
                                               AD9361_GPIO_RX_BAND_POS,
                                               true );
            }
            break;

        default:
            status=-1;
            break;
    }
    
    return (status);
}

int32_t _write_tx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status=-1;

    switch( path )
    {
        case skiq_filt_0_to_3000_MHz:
            if( (status=ad9361_write_tx_output_port(p_id->chip_id,
                                                    ad9361_tx_port_b)) == 0 )
            {
                status=ad9361_write_gpio_ctrl( p_id->card,
                                               AD9361_GPIO_TX_BAND_POS,
                                               false );
            }
            break;

        case skiq_filt_3000_to_6000_MHz:
            if( (status=ad9361_write_tx_output_port(p_id->chip_id,
                                                    ad9361_tx_port_a)) == 0 )
            {
                status=ad9361_write_gpio_ctrl( p_id->card,
                                               AD9361_GPIO_TX_BAND_POS,
                                               true );
            }
            break;

        default:
            break;
    }
    
    return (status);
}

int32_t _read_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    int32_t status=-1;
    bool gpio_state=false;

    status = ad9361_read_gpio_ctrl( p_id->card, AD9361_GPIO_RX_BAND_POS, &gpio_state );
    if( gpio_state == false )
    {
        *p_path = skiq_filt_0_to_3000_MHz;
    }
    else
    {
        *p_path = skiq_filt_3000_to_6000_MHz;
    }

    return (status);
}

int32_t _read_tx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    int32_t status=-1;
    bool gpio_state=false;

    status = ad9361_read_gpio_ctrl( p_id->card,
                                    AD9361_GPIO_TX_BAND_POS,
                                    &gpio_state );
    if( gpio_state == false )
    {
        *p_path = skiq_filt_0_to_3000_MHz;
    }
    else
    {
        *p_path = skiq_filt_3000_to_6000_MHz;
    }

    return (status);
}

int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status=0;

    *p_num_filters = _NUM_FILTERS;
    memcpy( p_filters, _filter_list, *p_num_filters*sizeof(skiq_filt_t) );

    return (status);
}

int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status=0;

    *p_num_filters = _NUM_FILTERS;
    memcpy( p_filters, _filter_list, *p_num_filters*sizeof(skiq_filt_t) );

    return (status);
}

int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;
    bool trx = false;
    bool fixed = false;

    if( (status=_rf_port_config_avail( p_id, &fixed, &trx )) == 0 )
    {
        *p_num_trx_rf_ports = 0;
        *p_num_fixed_rf_ports = 0;
        if( p_id->hdl == skiq_rx_hdl_A1 )
        {
            if( trx == true )
            {
                *p_num_trx_rf_ports = 1;
                p_trx_rf_port_list[0] = skiq_rf_port_J2;
            }
            if( fixed == true )
            {
                *p_num_fixed_rf_ports = 1;
                p_fixed_rf_port_list[0] = skiq_rf_port_J2;
            }
        }
        else if( p_id->hdl == skiq_rx_hdl_A2 )
        {
            if( trx == true )
            {
                *p_num_trx_rf_ports = 1;
                p_trx_rf_port_list[0] = skiq_rf_port_J3;
            }
            if( fixed == true )
            {
                *p_num_fixed_rf_ports = 1;
                p_fixed_rf_port_list[0] = skiq_rf_port_J3;
            }
        }
        else
        {
            status = -ENODEV;
        }
    }
    else
    {
        *p_num_fixed_rf_ports = 0;
        *p_num_trx_rf_ports = 0;
    }

    return (status);
}

int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_J2;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_rf_port = skiq_rf_port_J3;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;
    bool trx = false;
    bool fixed = false;
    
    if( (status=_rf_port_config_avail( p_id, &fixed, &trx )) == 0 )
    {
        *p_num_trx_rf_ports = 0;
        *p_num_fixed_rf_ports = 0;
        if( p_id->hdl == skiq_tx_hdl_A1 )
        {
            if( trx == true )
            {
                *p_num_trx_rf_ports = 1;
                p_trx_rf_port_list[0] = skiq_rf_port_J2;
            }
            if( fixed == true )
            {
                *p_num_fixed_rf_ports = 1;
                p_fixed_rf_port_list[0] = skiq_rf_port_J1;
            }
        }
        else if( p_id->hdl == skiq_tx_hdl_A2 )
        {
            if( trx == true )
            {
                *p_num_trx_rf_ports = 1;
                p_trx_rf_port_list[0] = skiq_rf_port_J3;
            }
            if( fixed == true )
            {
                *p_num_fixed_rf_ports = 1;
                p_fixed_rf_port_list[0] = skiq_rf_port_J4;
            }
        }
        else
        {
            status = -ENODEV;
        }
    }

    return (status);
}

int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        if( _rf_port_trx_mode[p_id->card] == false )
        {
            *p_rf_port = skiq_rf_port_J1;
        }
        else
        {
            *p_rf_port = skiq_rf_port_J2;
        }
    }
    else if( p_id->hdl == skiq_tx_hdl_A2 )
    {
        if( _rf_port_trx_mode[p_id->card] == false )
        {
            *p_rf_port = skiq_rf_port_J4;
        }
        else
        {
            *p_rf_port = skiq_rf_port_J3;
        }
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _rf_port_config_avail( rf_id_t *p_id, bool *p_fixed, bool *p_trx )
{
    int32_t status=0;
    
    // for TRX, we need m.2 rev B hardware, FPGA >=3.2, and board ID=0x2
    if(  _hw_vers[p_id->card] == skiq_hw_vers_m2_b )
    {
        if ( _skiq_meets_FPGA_VERSION(p_id->card, FPGA_VERS_BOARD_ID_VALID) )
        {
            // check the board ID...0 means TDD
            if ( BOARD_ID_0(_skiq_get_fpga_board_id(p_id->card)) == 0 )
            {
                *p_trx = true;
                *p_fixed = false;
            }
            else
            {
                *p_trx = false;
                *p_fixed = true;
            }
        }
    }
    // m.2 rev C & D support both
    else if( (_hw_vers[p_id->card] == skiq_hw_vers_m2_c) ||
             (_hw_vers[p_id->card] == skiq_hw_vers_m2_d) )
    {
        *p_fixed = true;
        *p_trx = true;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _write_rf_port_config( rf_id_t *p_id, skiq_rf_port_config_t config )
{
    int32_t status=-ENOTSUP;
    uint32_t val=0;
    bool fixed = false;
    bool trx = false;

    // only supported with m.2 rev C & D
    if( (_hw_vers[p_id->card] == skiq_hw_vers_m2_c) ||
        (_hw_vers[p_id->card] == skiq_hw_vers_m2_d) )
    {
        // first make sure the RF port GPIO is set for an output
        sidekiq_fpga_reg_read(p_id->card, FPGA_REG_GPIO_TRISTATE, &val);
        BF_SET(val, 1, GPIO_RF_PORT_1_OFFSET, GPIO_RF_PORT_1_LEN);
        BF_SET(val, 1, GPIO_RF_PORT_2_OFFSET, GPIO_RF_PORT_2_LEN);
        status = sidekiq_fpga_reg_write(p_id->card, FPGA_REG_GPIO_TRISTATE, val);

        // now set the RF port value
        if( status == 0 )
        {
            sidekiq_fpga_reg_read(p_id->card, FPGA_REG_GPIO_WRITE, &val);
            if( config == skiq_rf_port_config_fixed )
            {
                // Note: these signals seem to be active low so 0 to activate
                BF_SET(val, 1, GPIO_RF_PORT_1_OFFSET, GPIO_RF_PORT_1_LEN);
                BF_SET(val, 1, GPIO_RF_PORT_2_OFFSET, GPIO_RF_PORT_2_LEN);
                _rf_port_trx_mode[p_id->card] = false;
            }
            else if( (config == skiq_rf_port_config_tdd) ||
                     (config == skiq_rf_port_config_trx) )
            {
                // Note: these signals seem to be active low so 0 to activate
                BF_SET(val, 0, GPIO_RF_PORT_1_OFFSET, GPIO_RF_PORT_1_LEN);
                BF_SET(val, 0, GPIO_RF_PORT_2_OFFSET, GPIO_RF_PORT_2_LEN);
                _rf_port_trx_mode[p_id->card] = true;
            }
            status = sidekiq_fpga_reg_write(p_id->card, FPGA_REG_GPIO_WRITE, val);
        }
    }
    else if( _hw_vers[p_id->card] == skiq_hw_vers_m2_b )
    {
        // m.2 rev B can't be changed...but if we're trying to write to what we
        // are already configured for, indicate a success
        _rf_port_config_avail( p_id, &fixed, &trx );
        if( ((fixed == true) && (config == skiq_rf_port_config_fixed)) ||
            ((trx == true) &&
             ((config == skiq_rf_port_config_trx) || (config == skiq_rf_port_config_tdd))) )
        {
            status = 0;
        }
            
    }
    
    return (status);
}

int32_t _read_rf_port_operation( rf_id_t *p_id, bool *p_transmit )
{
    int32_t status=0;
    uint32_t val=0;
    uint32_t force_overload_1 = 0;
    uint32_t force_overload_2 = 0;

    // check force overload bits (TX means overload on)
    sidekiq_fpga_reg_read(p_id->card, FPGA_REG_RF_BAND_SEL, &val);
    force_overload_1 = BF_GET(val, FORCE_OVERLOAD_1_OFFSET, FORCE_OVERLOAD_1_LEN);
    force_overload_2 = BF_GET(val, FORCE_OVERLOAD_2_OFFSET, FORCE_OVERLOAD_2_LEN);
    if( ( force_overload_1 == 1) &&
        ( force_overload_2 == 1) )
    {
        *p_transmit = true;
    }
    else if( (force_overload_1 == 0) &&
             (force_overload_2 == 0) )
    {
        *p_transmit = false;
    }
    else
    {
        // we always set both channels so we should never be in a state of a mismatch
        _skiq_log(SKIQ_LOG_ERROR, "invalid RF port operation configuration\n");
        status = -EINVAL;
    }

    return (status);
}

int32_t _write_rf_port_operation( rf_id_t *p_id, bool transmit )
{
    int32_t status=0;
    uint32_t val=0;    

    if( (status=sidekiq_fpga_reg_read(p_id->card, FPGA_REG_RF_BAND_SEL, &val)) == 0 )
    {
        // set (or clear) the force overload bits (TX means overload on)
        if( transmit == true )
        {
            BF_SET(val, 1, FORCE_OVERLOAD_1_OFFSET, FORCE_OVERLOAD_1_LEN);
            BF_SET(val, 1, FORCE_OVERLOAD_2_OFFSET, FORCE_OVERLOAD_2_LEN);
        }
        else
        {
            BF_SET(val, 0, FORCE_OVERLOAD_1_OFFSET, FORCE_OVERLOAD_1_LEN);
            BF_SET(val, 0, FORCE_OVERLOAD_2_OFFSET, FORCE_OVERLOAD_2_LEN);
        }
        status=sidekiq_fpga_reg_write(p_id->card, FPGA_REG_RF_BAND_SEL, val);
    }
    
    return (status);
}

int32_t _calc_filt_for_hopping( uint16_t num_freq, uint64_t freq_list[], skiq_filt_t *p_filt )
{
    int32_t status=0;
    uint16_t i=0;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    *p_filt = skiq_filt_invalid;

    for( i=0; (i<num_freq) && (status==0); i++ )
    {
        // Low band
        if( freq_list[i] < HIGH_BAND_THRESHOLD_FREQ )
        {
            if( *p_filt == skiq_filt_3000_to_6000_MHz )
            {
                skiq_error("Hopping range must all <3GHz or all >=3GHz for Sidekiq m.2");
                status = -EINVAL;
            }
            else
            {
                *p_filt = skiq_filt_0_to_3000_MHz;
            }
        }
        // High band
        else
        {
            if( *p_filt == skiq_filt_0_to_3000_MHz )
            {
                skiq_error("Hopping range must all <3GHz or all >=3GHz for Sidekiq m.2");
                status = -EINVAL;
            }
            else
            {
                *p_filt = skiq_filt_3000_to_6000_MHz;
            }
        }
    }

    return (status);
}

int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    status = _calc_filt_for_hopping( num_freq, freq_list, &skiq_filt );

    // if the frequencies are within a band, then actually apply it.  Since we know
    // all of the frequencies use the same filter setting, we can just set RFE for the
    // first frequency in the hop list
    if( status == 0 )
    {
        status = _update_before_rx_lo_tune( p_id, freq_list[0] );
    }
    if( status == 0 )
    {
        status = _update_after_rx_lo_tune( p_id, freq_list[0] );
    }

    return (status);
}

int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    status = _calc_filt_for_hopping( num_freq, freq_list, &skiq_filt );

    // if the frequencies are within a band, then actually apply it.  Since we know
    // all of the frequencies use the same filter setting, we can just set RFE for the
    // first frequency in the hop list
    if( status == 0 )
    {
        status = _update_tx_lo( p_id, freq_list[0] );
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void) p_id;

    p_rf_capabilities->minTxFreqHz = M2_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = M2_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = M2_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = M2_MAX_RX_FREQUENCY_IN_HZ;


    return (status);
}
