/*****************************************************************************/
/** @file sidekiq_rfe.c
 
 <pre>
 Copyright 2017-2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "sidekiq_rfe.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "rfe_mpcie_b.h"
#include "rfe_m2_b.h"
#include "rfe_x2_a.h"
#include "rfe_x4_b.h"
#include "rfe_z2_b.h"
#include "rfe_m2_2280_ab.h"
#include "rfe_z3u_abcd.h"
#include "rfe_nv100.h"

/***** DEFINES *****/

#if (defined CONFIG_STUB_RFE)
#define STUB_RFE(_p_rfe)                do { return 0; } while (0)
#define STUB_RFE_PARAM(_var,_val)       do { (_var) = (_val); } while (0)
#else
#define STUB_RFE(_p_rfe)                do { } while (0)
#define STUB_RFE_PARAM(_var,_val)       do { } while (0)
#endif


#define CHECK_RFE(_p_rfe)                       \
    do {                                        \
        STUB_RFE(_p_rfe);                       \
        if ( (_p_rfe) == NULL )                 \
        { return -EFAULT; }                     \
        if ( (_p_rfe)->p_funcs == NULL )        \
        { return -EFAULT; }                     \
    } while (0)

#define DEFAULT_HW_IFACE_VERSION        HW_IFACE_VERSION(255, 255)

/***** LOCAL FUNCTIONS *****/
static int32_t _rfe_meets_eeprom_hw_iface_version(uint16_t eeprom_hw_iface_version, uint16_t rfe_hw_iface_version);


/*************************************************************************************************/
/** Helper function that queries whether or not an RFE's hardware interface version meets the
    requirements to support the cards hardware interface version as read from EEPROM.

    @note When selecting an RFE during full initialization of a card the RFE hardware interface
    version is used to determine whether or not there is full support, partial support, or no
    support for the RFE.  During selection, an EEPROM major version that is equal to the RFE
    major version and an EEPROM minor version that is less than or equal to the RFE minor version
    relays full support for that RFE.  A major version that is equal to the supported major
    version, but has a minor version greater than the supported minor version relays partial
    support for that RFE; meaning that the RFE can be selected, but some available functionality
    may not be present for that card. When the major version is not equal to the supported major
    version, the RFE is not supported.

    Test cases for this functionality can be found in confluence here:
    https://confluence.epiq.rocks/display/EN/Libsidekiq+Test+Cases+for+hw_iface

    @param[in] eeprom_hw_iface_version hardware interface version of the card as read from EEPROM
    @param[in] rfe_hw_iface_version hardware interface version tied to the implementation of the RFE
    interface

    @return int32_t status
    @retval 0 RFE hardware interface version meets the requirements for EEPROM hardware interface version
    @retval -EINVAL RFE hardware interface version FAILS to meet the requirements for EEPROM hardware interface version
 */
int32_t _rfe_meets_eeprom_hw_iface_version(uint16_t eeprom_hw_iface_version, uint16_t rfe_hw_iface_version)
{
    int32_t status = -EINVAL;
    
    /* If either of the hardware interface versions are equal to the default version, treat them as version 0.0. */
    if( eeprom_hw_iface_version == DEFAULT_HW_IFACE_VERSION )
    {
        eeprom_hw_iface_version = HW_IFACE_VERSION(0, 0);
    }
    if( rfe_hw_iface_version == DEFAULT_HW_IFACE_VERSION )
    {
        rfe_hw_iface_version = HW_IFACE_VERSION(0, 0);
    }

    /* Truncate high 8 bits and low 8 bits to do minor and major evaluations for RFE selection. */
    uint8_t major_eeprom_hw_iface_version = (uint8_t)((eeprom_hw_iface_version >> 8) & 0xFF);
    uint8_t minor_eeprom_hw_iface_version = (uint8_t)(eeprom_hw_iface_version & 0xFF);
    uint8_t major_rfe_hw_iface_version = (uint8_t)((rfe_hw_iface_version >> 8) & 0xFF);
    uint8_t minor_rfe_hw_iface_version = (uint8_t)(rfe_hw_iface_version & 0xFF);

    /* Major version is supported, and minor version is less than or equal to the supported minor version. */
    if( ( major_eeprom_hw_iface_version == major_rfe_hw_iface_version ) &&
        ( minor_eeprom_hw_iface_version <= minor_rfe_hw_iface_version ) )
    {
        status = 0;
    }
    else if( major_eeprom_hw_iface_version == major_rfe_hw_iface_version )
    {
        /*  Major version is supported, but minor version is greater than the supported minor version. This is
            supported, but send a warning to notify the user about possible limited functionality. */
        skiq_warning("EEPROM minor version not fully supported; RFFE selected, but some functionality may not be present.\n");
        status = 0;
    }
    /*  The major version is not supported, this should result in no selection. No need to update status,
        just send an error status of -EINVAL.  Since this function may be called multiple times before 
        finding a suitable RFFE version, let the next layer report any errors as it does currently.*/
    return status;
}

/***** GLOBAL FUNCTIONS *****/

rfe_functions_t* rfe_init_functions( skiq_part_t part,
                                     skiq_hw_rev_t hw_rev,
                                     hw_iface_vers_t hw_iface_vers )
{
    rfe_functions_t *p_rfe_functions=NULL;
    int32_t status = 0;

    switch (part)
    {
        case skiq_mpcie:
            // mPCIe revs B-E share the same RFE
            if ( (hw_rev==hw_rev_b) || (hw_rev==hw_rev_c) || (hw_rev==hw_rev_d) ||
                 (hw_rev==hw_rev_e) )
            {
                p_rfe_functions = &(rfe_mpcie_b_funcs);
            }
            break;

        case skiq_m2:
            // M.2 revs B-D share the same RFE
            if ( (hw_rev==hw_rev_b) || (hw_rev==hw_rev_c) || (hw_rev==hw_rev_d) )
            {
                p_rfe_functions = &(rfe_m2_b_funcs);
            }
            break;

        case skiq_x2:
            // x2 rev A/B/C share the same RFE
#if (defined HAS_X2_SUPPORT)
            if ( _rfe_meets_eeprom_hw_iface_version(hw_iface_vers.version, DEFAULT_HW_IFACE_VERSION) == 0 )
            {
                p_rfe_functions = &(rfe_x2_a_funcs);
            }
#else
            skiq_warning("Support for X2 hardware not available in this library configuration\n");
#endif  /* HAS_X2_SUPPORT */
            break;

        case skiq_z2:
            // Note: revA/B have slight differences...so warn for now (and remove A support
            // eventually)
            if ( _rfe_meets_eeprom_hw_iface_version(hw_iface_vers.version, DEFAULT_HW_IFACE_VERSION) == 0 )
            {
                if ( hw_rev == hw_rev_a )
                {
                    skiq_warning("Using rev B RFE for rev A hardware\n");
                }
                p_rfe_functions = &(rfe_z2_b_funcs);
            }
            break;

        case skiq_x4:
            // x4 rev A/B share the same RFE
#if (defined HAS_X4_SUPPORT)
            if ( _rfe_meets_eeprom_hw_iface_version(hw_iface_vers.version, DEFAULT_HW_IFACE_VERSION) == 0 )
            {
                p_rfe_functions = &(rfe_x4_b_funcs);
            }
#else
            skiq_warning("Support for X4 hardware not available in this library configuration\n");
#endif  /* HAS_X4_SUPPORT */
            break;

        case skiq_m2_2280:
        case skiq_z2p:
            if ( _rfe_meets_eeprom_hw_iface_version(hw_iface_vers.version, DEFAULT_HW_IFACE_VERSION) == 0 )
            {
                /* M.2-2280/Z2p rev A and B share the same RFE */
                p_rfe_functions = &rfe_m2_2280_ab_funcs;
            }
            break;

        case skiq_z3u:
            if ( _rfe_meets_eeprom_hw_iface_version(hw_iface_vers.version, DEFAULT_HW_IFACE_VERSION) == 0 )
            {
                /* Z3u A/B/C/D share the same RFE */
                p_rfe_functions = &rfe_z3u_abcd_funcs;
            }
            break;

        case skiq_nv100:
            /* NV100 revs A and B are not supported revisions of NV100 at this time */
            if ( (hw_rev == hw_rev_a) || (hw_rev == hw_rev_b) )
            {
                status = -ENOTSUP;
                skiq_error("Unsupported hardware revision %c on Sidekiq part %s (%u)\n",
                        hw_rev_cstr(hw_rev), part_cstr(part), part);
                hal_critical_exit(status);
            }
            else if ( _rfe_meets_eeprom_hw_iface_version(hw_iface_vers.version, DEFAULT_HW_IFACE_VERSION) == 0 )
            {
                p_rfe_functions = &rfe_nv100_funcs;
            }
            break;

        default:
            skiq_error("Unhandled Sidekiq part %s (%u)\n", part_cstr(part), part);
            p_rfe_functions = NULL;
            break;
    }

    return (p_rfe_functions);
}

int32_t rfe_init( rfe_t *p_rfe )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->init != NULL )
    {
        status = p_rfe->p_funcs->init( p_rfe->p_id );
    }

    return (status);
}

int32_t rfe_post_init( rfe_t *p_rfe )
{
    int32_t status=0;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->post_init != NULL )
    {
        status = p_rfe->p_funcs->post_init( p_rfe->p_id );
    }

    return (status);
}

int32_t rfe_release( rfe_t *p_rfe )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->release != NULL )
    {
        status = p_rfe->p_funcs->release( p_rfe->p_id );
    }

    return (status);
}

int32_t rfe_pre_release( rfe_t *p_rfe )
{
    int32_t status=0;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->pre_release != NULL )
    {
        status = p_rfe->p_funcs->pre_release( p_rfe->p_id );
    }

    return (status);
}

int32_t rfe_update_tx_pa( rfe_t *p_rfe,
                          bool enable )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->update_tx_pa != NULL )
    {
        status=p_rfe->p_funcs->update_tx_pa( p_rfe->p_id,
                                             enable );
    }

    return (status);
}

int32_t rfe_override_tx_pa( rfe_t *p_rfe,
                            skiq_pin_value_t pin_value )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->override_tx_pa != NULL )
    {
        status=p_rfe->p_funcs->override_tx_pa( p_rfe->p_id,
                                               pin_value );
    }

    return (status);
}

int32_t rfe_update_rx_lna( rfe_t *p_rfe,
                           rfe_lna_state_t new_state )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if ( p_rfe->p_funcs->update_rx_lna != NULL )
    {
        status = p_rfe->p_funcs->update_rx_lna( p_rfe->p_id, new_state );
    }

    return (status);
}

int32_t rfe_update_rx_lna_by_idx( rfe_t *p_rfe,
                                  uint8_t idx,
                                  rfe_lna_state_t new_state )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if ( p_rfe->p_funcs->update_rx_lna_by_idx != NULL )
    {
        status = p_rfe->p_funcs->update_rx_lna_by_idx( p_rfe->p_id, idx, new_state );
    }

    return (status);
}

int32_t rfe_update_before_rx_lo_tune( rfe_t *p_rfe,
                                      uint64_t rx_lo_freq )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->update_before_rx_lo_tune != NULL )
    {
        status=p_rfe->p_funcs->update_before_rx_lo_tune( p_rfe->p_id, rx_lo_freq );
    }

    return (status);
}

int32_t rfe_update_after_rx_lo_tune( rfe_t *p_rfe,
                                     uint64_t rx_lo_freq )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->update_after_rx_lo_tune != NULL )
    {
        status=p_rfe->p_funcs->update_after_rx_lo_tune( p_rfe->p_id, rx_lo_freq );
    }

    return (status);
}

int32_t rfe_update_before_tx_lo_tune( rfe_t *p_rfe,
                                      uint64_t tx_lo_freq )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->update_before_tx_lo_tune != NULL )
    {
        status=p_rfe->p_funcs->update_before_tx_lo_tune( p_rfe->p_id,
                                                         tx_lo_freq );
    }

    return (status);
}

int32_t rfe_update_after_tx_lo_tune( rfe_t *p_rfe,
                                     uint64_t tx_lo_freq )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->update_after_tx_lo_tune != NULL )
    {
        status=p_rfe->p_funcs->update_after_tx_lo_tune( p_rfe->p_id,
                                                        tx_lo_freq );
    }

    return (status);
}

int32_t rfe_write_rx_filter_path( rfe_t *p_rfe,
                                  skiq_filt_t path )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rx_filter_path != NULL )
    {
        status=p_rfe->p_funcs->write_rx_filter_path( p_rfe->p_id,
                                                     path );
    }

    return (status);
}

int32_t rfe_write_tx_filter_path( rfe_t *p_rfe,
                                  skiq_filt_t path )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_tx_filter_path != NULL )
    {
        status=p_rfe->p_funcs->write_tx_filter_path( p_rfe->p_id,
                                                     path );
    }

    return (status);
}

int32_t rfe_read_rx_filter_path( rfe_t *p_rfe,
                                 skiq_filt_t *p_path )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_path, skiq_filt_0_to_6000MHz);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_filter_path != NULL )
    {
        status=p_rfe->p_funcs->read_rx_filter_path( p_rfe->p_id,
                                                    p_path );
    }

    return (status);
}

int32_t rfe_read_tx_filter_path( rfe_t *p_rfe,
                                 skiq_filt_t *p_path )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_path, skiq_filt_0_to_6000MHz);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_filter_path != NULL )
    {
        status=p_rfe->p_funcs->read_rx_filter_path( p_rfe->p_id,
                                                    p_path );
    }

    return (status);
}


int32_t rfe_read_rx_filter_capabilities( rfe_t *p_rfe,
                                         skiq_filt_t *p_filters,
                                         uint8_t *p_num_filters )
{
    int32_t status = 0;

    STUB_RFE_PARAM(*p_num_filters, 0);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_filter_capabilities != NULL )
    {
        status=p_rfe->p_funcs->read_rx_filter_capabilities( p_rfe->p_id,
                                                            p_filters,
                                                            p_num_filters );
    }
    else
    {
        /* .read_rx_filter_capabilities is optional */
        *p_num_filters = 0;
    }

    return (status);
}

int32_t rfe_read_tx_filter_capabilities( rfe_t *p_rfe,
                                         skiq_filt_t *p_filters,
                                         uint8_t *p_num_filters )
{
    int32_t status = 0;

    STUB_RFE_PARAM(*p_num_filters, 0);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_tx_filter_capabilities != NULL )
    {
        status=p_rfe->p_funcs->read_tx_filter_capabilities( p_rfe->p_id,
                                                            p_filters,
                                                            p_num_filters );
    }
    else
    {
        /* .read_tx_filter_capabilities is optional */
        *p_num_filters = 0;
    }

    return (status);
}

int32_t rfe_write_filters( rfe_t *p_rfe,
                           uint8_t num_filters,
                           const skiq_filt_t *p_filters )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_filters != NULL )
    {
        status = p_rfe->p_funcs->write_filters( p_rfe->p_id,
                                                num_filters,
                                                p_filters );
    }

    return (status);
}

int32_t rfe_write_rx_attenuation( rfe_t *p_rfe,
                                  uint16_t atten_quarter_db )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rx_attenuation != NULL )
    {
        status = p_rfe->p_funcs->write_rx_attenuation( p_rfe->p_id,
                                                       atten_quarter_db );
    }

    return (status);
}

int32_t rfe_read_rx_attenuation( rfe_t *p_rfe,
                                 uint16_t *p_atten_quarter_db )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_atten_quarter_db, 0);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_attenuation != NULL )
    {
        status = p_rfe->p_funcs->read_rx_attenuation( p_rfe->p_id,
                                                      p_atten_quarter_db );
    }

    return (status);
}

int32_t rfe_read_rx_attenuation_range( rfe_t *p_rfe,
                                       uint16_t *p_atten_quarter_db_max,
                                       uint16_t *p_atten_quarter_db_min )
{
    int32_t status = 0;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_attenuation_range != NULL )
    {
        status = p_rfe->p_funcs->read_rx_attenuation_range( p_rfe->p_id,
                                                            p_atten_quarter_db_max,
                                                            p_atten_quarter_db_min );
    }
    else
    {
        // If not supported, default attenuation range to zero.
        *p_atten_quarter_db_max = 0;
        *p_atten_quarter_db_min = 0;
    }

    return status;
}

int32_t rfe_write_rx_attenuation_mode( rfe_t *p_rfe,
                                       skiq_rx_attenuation_mode_t mode )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rx_attenuation_mode!= NULL )
    {
        status = p_rfe->p_funcs->write_rx_attenuation_mode( p_rfe->p_id,
                                                            mode );
    }

    return (status);
}

int32_t rfe_read_rx_attenuation_mode( rfe_t *p_rfe,
                                      skiq_rx_attenuation_mode_t *p_mode )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_mode, skiq_rx_attenuation_mode_manual);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rx_attenuation_mode!= NULL )
    {
        status = p_rfe->p_funcs->read_rx_attenuation_mode( p_rfe->p_id,
                                                           p_mode );
    }

    return (status);
}

int32_t rfe_read_rx_rf_ports_avail( rfe_t *p_rfe,
                                    uint8_t *p_num_fixed_rf_ports,
                                    skiq_rf_port_t *p_fixed_rf_port_list,
                                    uint8_t *p_num_trx_rf_ports,
                                    skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_num_fixed_rf_ports, 1);
    STUB_RFE_PARAM(*p_fixed_rf_port_list, skiq_rf_port_J1);
    STUB_RFE_PARAM(*p_num_trx_rf_ports, 1);
    STUB_RFE_PARAM(*p_trx_rf_port_list, skiq_rf_port_J1);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_rf_ports_avail != NULL )
    {
        status = p_rfe->p_funcs->read_rx_rf_ports_avail( p_rfe->p_id,
                                                         p_num_fixed_rf_ports,
                                                         p_fixed_rf_port_list,
                                                         p_num_trx_rf_ports,
                                                         p_trx_rf_port_list );
    }
    
    return (status);
}

int32_t rfe_read_rx_rf_port( rfe_t *p_rfe, skiq_rf_port_t *p_rf_port )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_rf_port, skiq_rf_port_J1);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_rf_port != NULL )
    {
        status = p_rfe->p_funcs->read_rx_rf_port( p_rfe->p_id,
                                                  p_rf_port );
    }
    
    return (status);
}

int32_t rfe_write_rx_rf_port( rfe_t *p_rfe, skiq_rf_port_t rf_port )
{
    int32_t status=-ENOTSUP;
    skiq_rf_port_t curr_port;

    CHECK_RFE(p_rfe);

    // determine if we're writing to something different
    status = rfe_read_rx_rf_port( p_rfe, &curr_port );
    if( status == 0 )
    {
        if( rf_port != curr_port )
        {
            if( p_rfe->p_funcs->write_rx_rf_port != NULL )
            {
                status = p_rfe->p_funcs->write_rx_rf_port( p_rfe->p_id,
                                                           rf_port );
            }
        }
    }
    
    return (status);
}


int32_t rfe_read_tx_rf_ports_avail( rfe_t *p_rfe,
                                    uint8_t *p_num_fixed_rf_ports,
                                    skiq_rf_port_t *p_fixed_rf_port_list,
                                    uint8_t *p_num_trx_rf_ports,
                                    skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=-ENOTSUP;

    /* The STUB_RFE configuration is a way for the hardware folks to still use libsidekiq if the RFE
       implementation is still under development or if they want to implement their own.  In this
       case, these macros complete the rfe_read_tx_rf_ports_avail() functionality to satisfy the
       rest of libsidekiq's needs.  It pretends that there is one fixed RF port and one TRX RF port,
       both through J1 */
    STUB_RFE_PARAM(*p_num_fixed_rf_ports, 1);
    STUB_RFE_PARAM(*p_fixed_rf_port_list, skiq_rf_port_J1);
    STUB_RFE_PARAM(*p_num_trx_rf_ports, 1);
    STUB_RFE_PARAM(*p_trx_rf_port_list, skiq_rf_port_J1);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_tx_rf_ports_avail != NULL )
    {
        status = p_rfe->p_funcs->read_tx_rf_ports_avail( p_rfe->p_id,
                                                         p_num_fixed_rf_ports,
                                                         p_fixed_rf_port_list,
                                                         p_num_trx_rf_ports,
                                                         p_trx_rf_port_list );
    }
    
    return (status);
}


int32_t rfe_read_tx_rf_port( rfe_t *p_rfe, skiq_rf_port_t *p_rf_port )
{
    int32_t status=-ENOTSUP;

    STUB_RFE_PARAM(*p_rf_port, skiq_rf_port_J1);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_tx_rf_port != NULL )
    {
        status = p_rfe->p_funcs->read_tx_rf_port( p_rfe->p_id,
                                                  p_rf_port );
    }
    
    return (status);
}


int32_t rfe_write_tx_rf_port( rfe_t *p_rfe, skiq_rf_port_t rf_port )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rx_rf_port != NULL )
    {
        status = p_rfe->p_funcs->write_rx_rf_port( p_rfe->p_id,
                                                   rf_port );
    }
    
    return (status);
}

int32_t rfe_read_rf_port_config_avail( rfe_t *p_rfe, bool *p_fixed, bool *p_trx )
{
    int32_t status=0;

    CHECK_RFE(p_rfe);

    // all the radios assume to support fixed, so if there isn't an RFE implementation,
    // use that as a default
    if( p_rfe->p_funcs->rf_port_config_avail != NULL )
    {
        status = p_rfe->p_funcs->rf_port_config_avail( p_rfe->p_id,
                                                       p_fixed,
                                                       p_trx );
    }
    else
    {
        *p_fixed = true;
        *p_trx = false;
    }
    
    return (status);
}

int32_t rfe_write_rf_port_config( rfe_t *p_rfe,
                                  skiq_rf_port_config_t config )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rf_port_config != NULL )
    {
        status = p_rfe->p_funcs->write_rf_port_config( p_rfe->p_id,
                                                       config );
    }
    else if( config == skiq_rf_port_config_fixed )
    {
        // all RFEs should support fixed mode, so indicate success
        status = 0;
    }

    return (status);
}

int32_t rfe_read_rf_port_operation( rfe_t *p_rfe,
                                    bool *p_transmit )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rf_port_operation != NULL )
    {
        status = p_rfe->p_funcs->read_rf_port_operation( p_rfe->p_id,
                                                         p_transmit );
    }

    return (status);
}

int32_t rfe_write_rf_port_operation( rfe_t *p_rfe,
                                     bool transmit )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rf_port_operation != NULL )
    {
        status = p_rfe->p_funcs->write_rf_port_operation( p_rfe->p_id,
                                                          transmit );
    }

    return (status);
}

int32_t rfe_read_bias_index( rfe_t *p_rfe, uint8_t *p_bias_index )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_bias_index != NULL )
    {
        status = p_rfe->p_funcs->read_bias_index( p_rfe->p_id,
                                                  p_bias_index );
    }

    return (status);
}

int32_t rfe_write_bias_index( rfe_t *p_rfe, uint8_t bias_index )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_bias_index != NULL )
    {
        status = p_rfe->p_funcs->write_bias_index( p_rfe->p_id,
                                                   bias_index );
    }
    
    return (status);
}

int32_t rfe_read_rx_stream_hdl_conflict( rfe_t *p_rfe,
                                         skiq_rx_hdl_t *p_conflicting_hdls,
                                         uint8_t *p_num_hdls )
{
    int32_t status=0;

    STUB_RFE_PARAM(*p_num_hdls,0);
    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rx_stream_hdl_conflict != NULL )
    {
        status = p_rfe->p_funcs->read_rx_stream_hdl_conflict( p_rfe->p_id,
                                                              p_conflicting_hdls,
                                                              p_num_hdls );
    }
    else
    {
        // if there's no specific implementation, assume no conflicts
        *p_num_hdls = 0;
    }

    return (status);
}

int32_t rfe_enable_rx_chan( rfe_t *p_rfe, bool enable )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->enable_rx_chan != NULL )
    {
        status = p_rfe->p_funcs->enable_rx_chan( p_rfe->p_id, enable );
    }

    return (status);
}

int32_t rfe_enable_tx_chan( rfe_t *p_rfe, bool enable )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->enable_tx_chan != NULL )
    {
        status = p_rfe->p_funcs->enable_tx_chan( p_rfe->p_id, enable );
    }

    return (status);
}

int32_t rfe_write_rx_freq_hop_list( rfe_t *p_rfe, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_rx_freq_hop_list != NULL )
    {
        status = p_rfe->p_funcs->write_rx_freq_hop_list(p_rfe->p_id, num_freq, freq_list);
    }

    return (status);
}

int32_t rfe_write_tx_freq_hop_list( rfe_t *p_rfe, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->write_tx_freq_hop_list != NULL )
    {
        status = p_rfe->p_funcs->write_tx_freq_hop_list(p_rfe->p_id, num_freq, freq_list);
    }

    return (status);
}

int32_t rfe_config_custom_rx_filter( rfe_t *p_rfe,
                                     uint8_t filt_index,
                                     uint8_t hpf_lpf_setting,
                                     uint8_t rx_fltr1_sw,
                                     uint8_t rfic_port )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->config_custom_rx_filter != NULL )
    {
        status = p_rfe->p_funcs->config_custom_rx_filter( p_rfe->p_id,
                                                          filt_index,
                                                          hpf_lpf_setting,
                                                          rx_fltr1_sw,
                                                          rfic_port );
    }

    return (status);
}

int32_t rfe_read_rfic_port_for_rx_hdl( rfe_t *p_rfe,
                                       uint8_t *p_port )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rfic_port_for_rx_hdl != NULL )
    {
        status = p_rfe->p_funcs->read_rfic_port_for_rx_hdl( p_rfe->p_id,
                                                            p_port );
    }
    else
    {
        // RFE must not care, just map the port to the hdl+1 since handle starts at 0 but
        // port starts at 1.  So handle=0 maps to port=1 by default
        *p_port = (uint8_t)(p_rfe->p_id->hdl)+1;
        status = 0;
    }

    return (status);
}

int32_t rfe_read_rfic_port_for_tx_hdl( rfe_t *p_rfe,
                                       uint8_t *p_port )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);
    
    if( p_rfe->p_funcs->read_rfic_port_for_tx_hdl != NULL )
    {
        status = p_rfe->p_funcs->read_rfic_port_for_tx_hdl( p_rfe->p_id,
                                                            p_port );
    }
    else
    {
        // RFE must not care, just map the port to the hdl+1, since handle starts at 0 but
        // port starts at 1.  So handle=0 maps to port=1 by default
        *p_port = (uint8_t)(p_rfe->p_id->hdl)+1;
        status = 0;
    }

    return (status);
}

int32_t rfe_swap_rx_port_config( rfe_t *p_rfe, uint8_t a_port, uint8_t b_port )
{
    int32_t status=0;

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->swap_rx_port_config != NULL )
    {
        status = p_rfe->p_funcs->swap_rx_port_config( p_rfe->p_id,
                                                      a_port,
                                                      b_port );
    }

    return (status);
}

int32_t rfe_swap_tx_port_config( rfe_t *p_rfe, uint8_t a_port, uint8_t b_port )
{
    int32_t status=-ENOTSUP;

    CHECK_RFE(p_rfe);

    skiq_error("Swapping TX port not currently supported\n");

    return (status);
}

int32_t rfe_read_rf_capabilities( rfe_t *p_rfe, skiq_rf_capabilities_t *p_rf_capabilities )
{
    int32_t status=-ENOTSUP;

#if (defined CONFIG_STUB_RFE)
    p_rf_capabilities->minTxFreqHz = 0;
    p_rf_capabilities->maxTxFreqHz = UINT64_MAX;
    p_rf_capabilities->minRxFreqHz = 0;
    p_rf_capabilities->maxRxFreqHz = UINT64_MAX;
#endif

    CHECK_RFE(p_rfe);

    if( p_rfe->p_funcs->read_rf_capabilities != NULL )
    {
        status = p_rfe->p_funcs->read_rf_capabilities( p_rfe->p_id,
                                                       p_rf_capabilities );
    }

    return (status);
}
