/*****************************************************************************/
/** @file sidekiq_fpga_ctrl.c

 <pre>
 Copyright 2017-2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <time.h>

#include "sidekiq_fpga_ctrl.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_fpga.h"

#include "sidekiq_private.h"
#include "sidekiq_hal.h"

#include "bit_ops.h"

/* enable debug_print and debug_print_plain when DEBUG_FPGA_CTRL is defined */
#if (defined DEBUG_FPGA_CTRL)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"



#define FPGA_VERS_MANUAL_TRIGGER        FPGA_VERSION(3,11,0)
#define FPGA_VERS_HOP_ON_TS             FPGA_VERSION(3,12,0)
#define FPGA_VERS_HOP_ON_TS_CSTR        FPGA_VERSION_STR(3,12,0)

/**
   @brief It's critical that this polling interval be minimized.  Once the 1PPS edge occurs, the
   FPGA begins streaming samples, and any calling function needs to return control back to the user
   so they may handle received samples in a timely fashion.  As an example, the Sidekiq X4 is
   capable of sending samples up to the host at 250Msps x 4 = 1Gsps = 4GB/s.  In 1 millisecond,
   that's 4MB, which is half of the dmadriver's default ring buffer depth.
 */
#define PPS_POLL_TIME_MS                1    /* Poll the FPGA to see if the 1PPS rising edge
                                              * arrived */
#define PPS_MAX_TIME_MS                 2500 /* wait a max of 2.5s */

/* As of FPGA v3.10.0, the fixed point multiplication factors for I/Q are 16-bit 2's complement,
 * -32768 to 32767 */
#define RE_CMPLX_MULT_FACTOR_RESOLUTION           (RE_CMPLX_MULT_FACTOR_LEN-1)
#define IM_CMPLX_MULT_FACTOR_RESOLUTION           (IM_CMPLX_MULT_FACTOR_LEN-1)
#define RE_CMPLX_MULT_FACTOR_MIN                  (-(1 << (RE_CMPLX_MULT_FACTOR_RESOLUTION)))
#define IM_CMPLX_MULT_FACTOR_MIN                  (-(1 << (IM_CMPLX_MULT_FACTOR_RESOLUTION)))
#define RE_CMPLX_MULT_FACTOR_MAX                  ((1 << (RE_CMPLX_MULT_FACTOR_RESOLUTION)) - 1)
#define IM_CMPLX_MULT_FACTOR_MAX                  ((1 << (IM_CMPLX_MULT_FACTOR_RESOLUTION)) - 1)

static_assert(RE_CMPLX_MULT_FACTOR_RESOLUTION <= 15, "I/Q real factor resolution too large for int16_t");
static_assert(IM_CMPLX_MULT_FACTOR_RESOLUTION <= 15, "I/Q imaginary factor resolution too large for int16_t");

static int32_t _wait_1pps_after_ts( uint8_t card,
                                    uint64_t pps_sys_timestamp );
static void _clear_1pps(uint8_t card);
static int32_t prepare_next_1pps(uint8_t card);

/**************************************************************************************************/
/** The _wait_dc_offset_valid function polls an FPGA register until the DC_OFFSET_VALID bit is set.
    The DC Offset block in the FPGA has a 1920 cycle delay (reference: ref_design/hdl/dc_offset.v in
    maveriq_fpga.git) before samples are ready and available.  1920 cycles at the minimum sample
    rate of 233kHz is 8.24 ms.  This bit's time to set is dependent on the receive sample rate, so
    it may take up to ~9ms before seting.  This function checks against a monotonically increasing
    clock source as a way to check if the ~9ms deadline has passed.
 */
#define _wait_dc_offset_valid(_card)  fpga_ctrl_wait_for_bit(_card, 1, DC_OFFSET_VALID_OFFSET, \
                                                             DC_OFFSET_VALID_LEN, \
                                                             FPGA_REG_READ_RF_BAND_SEL, \
                                                             100 * MICROSEC, 9 * MILLISEC)


/**************************************************************************************************/
/* Helper function that compares two `struct timespec` pointers and returns true if a >= b */
static bool timespec_gte( struct timespec *a,
                          struct timespec *b )
{
    if ( a->tv_sec == b->tv_sec )
    {
        return ( a->tv_nsec >= b->tv_nsec );
    }
    else
    {
        return ( a->tv_sec > b->tv_sec );
    }
}


/**************************************************************************************************/
/* A MACRO to set the field at 'bitfield name' to the specified 'value' across all rx_params */
#define write_ctrl_reg_field_multi(_card,_rx_params,_nr_rx_params,_bfname,_value) \
    ({                                                                  \
        int32_t __status = 0;                                           \
        uint16_t _i;                                                    \
        __typeof__ (_nr_rx_params) __nr_rx_params = (_nr_rx_params);    \
        for ( _i = 0; ( _i < __nr_rx_params )  && ( __status == 0 ); _i++) \
        {                                                               \
            __status = sidekiq_fpga_reg_RMWV(_card, _value, _bfname, _rx_params[_i].ctrl_reg); \
        }                                                               \
        __status;                                                       \
    })


/**************************************************************************************************/
static inline int32_t
set_wait_for_trigger_multi( uint8_t card,
                            fpga_ctrl_rx_params_t rx_params[],
                            uint8_t nr_rx_params )
{
    return write_ctrl_reg_field_multi( card, rx_params, nr_rx_params, WAIT_FOR_TRIGGER, 1 );
}


/**************************************************************************************************/
static inline int32_t
clear_wait_for_trigger_multi( uint8_t card,
                              fpga_ctrl_rx_params_t rx_params[],
                              uint8_t nr_rx_params )
{
    return write_ctrl_reg_field_multi( card, rx_params, nr_rx_params, WAIT_FOR_TRIGGER, 0 );
}


/**************************************************************************************************/
static inline int32_t
set_dma_rx_fifo_ctrl_multi( uint8_t card,
                            fpga_ctrl_rx_params_t rx_params[],
                            uint8_t nr_rx_params )
{
    return write_ctrl_reg_field_multi( card, rx_params, nr_rx_params, DMA_RX_FIFO_CTRL, 1 );
}


/**************************************************************************************************/
static inline int32_t
clear_dma_rx_fifo_ctrl_multi( uint8_t card,
                        fpga_ctrl_rx_params_t rx_params[],
                        uint8_t nr_rx_params )
{
    return write_ctrl_reg_field_multi( card, rx_params, nr_rx_params, DMA_RX_FIFO_CTRL, 0 );
}


/**************************************************************************************************/
static bool has_iq_complex_mult( uint8_t card )
{
    bool has_iq_complex_mult = false;

    /* FPGA bitstreams v3.11.0 and later have a proper capability bit for the I/Q complex multiplier
     * feature. */
    if ( _skiq_meets_fpga_version( card, 3, 11, 0 ) )
    {
        struct fpga_capabilities fpga_caps;

        fpga_caps = _skiq_get_fpga_caps( card );
        has_iq_complex_mult = (fpga_caps.iq_complex_mult > 0);
    }

    return has_iq_complex_mult;
}


/**************************************************************************************************/
static int32_t
switch_to_iq_mode_decimator( uint8_t card,
                             skiq_rx_hdl_t hdl )
{
    int32_t status = 0;

    /* enable the decimator and remove I/Q count mode decimation */
    status = decimator_enable_if_present( card, hdl );
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV( card, 0, IQ_COUNT_MODE_DECIMATION, FPGA_REG_FPGA_CTRL );
    }

    return status;
}


/**************************************************************************************************/
static int32_t
switch_to_counter_mode_decimator( uint8_t card,
                                  skiq_rx_hdl_t hdl )
{
    int32_t status = 0;
    uint8_t nr_stages;

    /* disable the decimator and set the I/Q count mode decimation based on the number of decimator
     * states currently configured for the handle */
    status = decimator_disable_if_present( card, hdl );
    if ( status == 0 )
    {
        status = decimator_get_rate( card, hdl, &nr_stages);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV( card, nr_stages, IQ_COUNT_MODE_DECIMATION,
                                        FPGA_REG_FPGA_CTRL );
    }

    return status;
}


/**************************************************************************************************/
static bool has_iq_order_mode( uint8_t card )
{
    bool has_iq_order_mode = false;

    /* FPGA bitstreams v3.12.0 and later can configure IQ ordering */
    if ( _skiq_meets_fpga_version( card, 3, 12, 0 ) )
    {
        has_iq_order_mode = true; 
    }

    return has_iq_order_mode;
}

/**************************************************************************************************/
/** configure_iq_complex_multiplier() accepts an array of rx_params and either enables / disables
    the card-wide I/Q complex multipliers based on the card's configuration or handle's data source
    configuration.

    @attention At this time, this function does NOT check if other handles are already streaming.
    This means that ongoing receive transactions may be affected if the I/Q complex multipliers
    become enabled or become disabled as a result of calling this function.

    @return int32_t
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements
 */
static int32_t
configure_iq_complex_multiplier( uint8_t card,
                                 fpga_ctrl_rx_params_t rx_params[],
                                 uint8_t nr_rx_params )
{
    uint8_t nr_src_iq = 0, nr_src_counter = 0;
    int32_t status = 0;
    uint8_t i;

    /* count up the number of handles configured for I/Q vs counter */
    for ( i = 0; i < nr_rx_params; i++)
    {
        fpga_ctrl_rx_params_t *pr = &(rx_params[i]);

        if ( pr->data_src == skiq_data_src_counter )
        {
            nr_src_counter++;
        }
        else if ( pr->data_src == skiq_data_src_iq )
        {
            nr_src_iq++;
        }
    }

    /* If there is at least one handle configured for counter data source or the card (rx_params[0])
     * indicates .complex_mult is false (i.e. not configured to use I/Q complex multipliers), then
     * disable their usage, otherwise enable the multipliers. */
    if ( ( nr_src_counter > 0 ) || ( !rx_params[0].complex_mult ) )
    {
        /* If there are both data source configurations present, warn the user with a call to
         * skiq_warning().  The I/Q complex multiplication is done at a per-card basis, not at a
         * per-handle basis.  The data source (I/Q vs counter) can, however, be configured at a
         * per-handle basis, so if the code is to disable I/Q complex multiplication, the user needs
         * to be informed somehow that their I/Q data is no longer being adjusted with complex
         * multiplication. */
        if ( ( nr_src_iq > 0 ) && ( nr_src_counter > 0 ) )
        {
            if ( has_iq_complex_mult( card ) )
            {
                skiq_warning("I/Q complex multipliers being disabled since at least one receive "
                             "handle is configured for a counter data source on card %u\n", card);
            }
        }

        status = fpga_ctrl_disable_iq_complex_multiplier( card );
    }
    else
    {
        status = fpga_ctrl_enable_iq_complex_multiplier( card );
    }

    if ( status == -ENOSYS )
    {
        /* Enabling or disabling the I/Q complex multipliers in configure_iq_complex_multiplier()
         * wasn't the user's request, it's an internal decision to disable the multipliers when
         * counter mode is selected.  So even though -ENOSYS is returned, ignore it because it isn't
         * an error for a platform not to support it */
        status = 0;
    }

    return status;
}


/*****************************************************************************/
/** The fpga_ctrl_read_version reads all of the version related information
    from the FPGA.

    @param[in] card Sidekiq card of interest
    @param[out] p_git_hash  a pointer to where the 32-bit git hash will be written
    @param[out] p_build_date  a pointer to where the 32-bit build date will be written
    @param[out] p_major a pointer to where the major rev # should be written
    @param[out] p_minor a pointer to where the minor rev # should be written
    @param[out] p_tx_fifo_size a pointer to where the FPGA's TX FIFO size enumeration should be written

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_version(uint8_t card,
                               uint32_t* p_git_hash,
                               uint32_t* p_build_date,
                               uint8_t *p_major,
                               uint8_t *p_minor,
                               uint8_t *p_patch,
                               uint32_t* p_baseline_hash,
                               skiq_fpga_tx_fifo_size_t *p_tx_fifo_size)
{
    int32_t status = -EBADMSG;
    uint32_t val = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_GIT_HASH, p_git_hash);
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_BUILD_DATE, p_build_date);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_VERSION, &val);
    }

    if ( status == 0 )
    {
        *p_major = RBF_GET(val, MAJOR);
        *p_minor = RBF_GET(val, MINOR);
        *p_tx_fifo_size = RBF_GET(val, TX_FIFO_SIZE);

        if ( FPGA_VERSION(*p_major,*p_minor,0) < FPGA_VERSION(3,8,0) )
        {
            *p_patch = 0;
        }
        else
        {
            status = sidekiq_fpga_reg_read( card, FPGA_REG_CAPABILITIES, &val );
            if ( status == 0 )
            {
                *p_patch = RBF_GET( val, FPGA_VERSION_PATCH );
            }
            else
            {
                *p_patch = 0;
            }
        }
    }

    if ( status == 0 )
    {
        if ( FPGA_VERSION(*p_major,*p_minor,*p_patch) >= FPGA_VERSION(3,14,2) )
        {
            status = sidekiq_fpga_reg_read(card, FPGA_REG_BASELINE_VCS_STATUS, p_baseline_hash);
        }
        else
        {
            *p_baseline_hash = 0;
        }
    }

    return (status);
}


/*****************************************************************************/
/** The fpga_ctrl_read_board_id reads all of the BOARD_ID bits from the FPGA.

    @param[in] card Sidekiq card of interest
    @param[out] p_board_id  reference to where the 3-bit board ID will be returned

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_board_id( uint8_t card,
                                 uint8_t *p_board_id )
{
    int32_t status = -EBADMSG;
    uint32_t val = 0;

    if ( sidekiq_fpga_reg_read( card, FPGA_REG_VERSION, &val ) == 0 )
    {
        *p_board_id =
            (RBF_GET(val, BOARD_ID_0) << 0) |
            (RBF_GET(val, BOARD_ID_1) << 1) |
            (RBF_GET(val, BOARD_ID_2) << 2);

        status = 0;
    }

    return (status);
}


/**************************************************************************************************/
/**
   The fpga_ctrl_read_board_id_ext reads the EXTENDED_BOARD_ID field of the FPGA_REG_VERSION2
   register from the FPGA.
*/
int32_t fpga_ctrl_read_board_id_ext( uint8_t card,
                                     uint8_t *p_board_id_ext )
{
    int32_t status = -EBADMSG;
    uint32_t val = 0;

    status = sidekiq_fpga_reg_read( card, FPGA_REG_VERSION2, &val );
    if ( status == 0 )
    {
        *p_board_id_ext = RBF_GET(val, EXTENDED_BOARD_ID);
    }

    return status;
}


/*****************************************************************************/
/** The fpga_ctrl_read_capabilities reads the FPGA_CAPABILITIES register from
    the FPGA and returns a mapped structure.

    @param[in] card Sidekiq card of interest
    @param[out] p_caps  reference to where the FPGA capabilities will be returned

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
int32_t fpga_ctrl_read_capabilities( uint8_t card,
                                     struct fpga_capabilities *p_caps )
{
    int32_t status = -EBADMSG;

    if ( sidekiq_fpga_reg_read( card, FPGA_REG_CAPABILITIES, (uint32_t *)p_caps ) == 0 )
    {
        status = 0;
    }

    return (status);
}


/**************************************************************************************************/
/** fpga_ctrl_resume_rx_stream_multi() resumes streaming on multiple receive streams.  It does so
    synchronously if the FPGA bitstream supports it.

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
int32_t fpga_ctrl_resume_rx_stream_multi( uint8_t card,
                                          fpga_ctrl_rx_params_t rx_params[],
                                          uint8_t nr_rx_params )
{
    bool use_manual_trigger = false;
    int32_t status = 0;
    uint8_t i;

    /* zero rx_params, just return success */
    if ( nr_rx_params == 0 )
    {
        return 0;
    }

    /* check if more than one rx_params is requested, then check for FPGA version and capabilities
     * before using manual trigger to resume streaming */
    if ( ( nr_rx_params > 1 ) &&
         _skiq_meets_FPGA_VERSION( card, FPGA_VERS_MANUAL_TRIGGER ) )
    {
        struct fpga_capabilities caps;

        caps = _skiq_get_fpga_caps(card);
        if ( caps.manual_trigger > 0 )
        {
            use_manual_trigger = true;
        }
    }

    /* iterate over all of the control registers, refreshing the decimator settings, whether it be
     * the counter mode decimation or I/Q data decimation */
    for ( i = 0; ( i < nr_rx_params ) && ( status == 0 ); i++)
    {
        fpga_ctrl_rx_params_t *pr = &(rx_params[i]);

        /* see if we're running in counter mode and refresh the bits appropriately */
        if ( pr->data_src == skiq_data_src_iq )
        {
            /* enable the decimator and remove I/Q count mode decimation */
            status = switch_to_iq_mode_decimator( card, pr->hdl );
        }
        else
        {
            /* disable the decimator and switch to the counter mode decimator */
            status = switch_to_counter_mode_decimator( card, pr->hdl );
        }
    }

    if ( use_manual_trigger )
    {
        /* let's set the "wait for trigger bit" */
        status = set_wait_for_trigger_multi( card, rx_params, nr_rx_params );

        /* enable RX FIFO control bit across rx_params */
        if ( status == 0 )
        {
            status = set_dma_rx_fifo_ctrl_multi( card, rx_params, nr_rx_params );
        }

        /* poll until the DC Offset block in the FPGA indicates ADC hold-off is complete.  This
         * isn't exactly intuitively named, however there's an ADC hold-off as an input into the DC
         * Offset block that causes a 1920 cycle delay (reference: ref_design/hdl/dc_offset.v in
         * maveriq_fpga.git) before samples are ready and available.  The cycle time is based on the
         * currently configured sample rate.  If MANUAL_TRIGGER is set before that hold-off is
         * complete, the samples are not aligned / synchronized.  */
        if ( status == 0 )
        {
            if ( _skiq_has_dc_offset_corr( card ) )
            {
                status = _wait_dc_offset_valid( card );
            }
        }

        /* set MANUAL_TRIGGER */
        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_RMWV(card, 1, MANUAL_TRIGGER, FPGA_REG_TIMESTAMP_RST);
        }

        /* let's clear the "wait for trigger bit" across all handles */
        if ( status == 0 )
        {
            status = clear_wait_for_trigger_multi( card, rx_params, nr_rx_params );
        }

        /* now that everything's started, cancel the manual trigger bit in the timestamp block */
        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_RMWV(card, 0, MANUAL_TRIGGER, FPGA_REG_TIMESTAMP_RST);
        }
    }
    else
    {
        /* without a manual trigger capability, just enable them sequentially */
        status = set_dma_rx_fifo_ctrl_multi( card, rx_params, nr_rx_params );
    }

    /* squash return code to -EBADMSG for non-zero status */
    if ( status != 0 )
    {
        status = -EBADMSG;
    }

    return (status);
}


/**************************************************************************************************/
/** fpga_ctrl_start_rx_stream_multi() starts streaming on multiple receive streams.  It does so
    synchronously if the FPGA bitstream supports it and if the caller requests it
    (::skiq_trigger_src_synced or ::skiq_trigger_src_1pps).

    @param[in] card Sidekiq card of interest
    @param[in] rx_params array of RX stream parameters
    @param[in] nr_rx_params number of entries in rx_params[]
    @param[in] trigger_src type of trigger
    @param[in] pps_sys_timestamp that must occur prior to the stream being enabled

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP requested synchronization trigger not supported by FPGA bitstream
    @retval -EPROTO System Timestamp is not changing or moving backwards
    @retval -ETIMEDOUT 1PPS trigger was requested and never occurred
 */
int32_t fpga_ctrl_start_rx_stream_multi( uint8_t card,
                                         fpga_ctrl_rx_params_t rx_params[],
                                         uint8_t nr_rx_params,
                                         skiq_trigger_src_t trigger_src,
                                         uint64_t pps_sys_timestamp )
{
    bool wait_for_pps = (trigger_src == skiq_trigger_src_1pps);
    bool synced = (trigger_src == skiq_trigger_src_synced);
    int32_t status=0;
    uint8_t i;

    /* zero rx_params, just return success */
    if ( nr_rx_params == 0 )
    {
        return 0;
    }

    /* one rx_params and synced requested, just fallback to immediate */
    if ( nr_rx_params == 1 )
    {
        synced = false;
    }

    /* check for FPGA version and capabilities before allowing 'skiq_trigger_src_synced' as a
     * trigger source */
    if ( synced )
    {
        if ( _skiq_meets_FPGA_VERSION( card, FPGA_VERS_MANUAL_TRIGGER ) )
        {
            struct fpga_capabilities caps;

            caps = _skiq_get_fpga_caps(card);
            if ( caps.manual_trigger == 0 )
            {
                status = -ENOTSUP;
            }
        }
        else
        {
            status = -ENOTSUP;
        }
    }

    if ( status == 0 )
    {
        status = configure_iq_complex_multiplier( card, rx_params, nr_rx_params );
    }

    /* configure all of the control registers */
    for ( i = 0; ( i < nr_rx_params ) && ( status == 0 ); i++)
    {
        fpga_ctrl_rx_params_t *pr = &(rx_params[i]);
        uint32_t data = 0;

        status = sidekiq_fpga_reg_read(card, pr->ctrl_reg, &data);
        if ( status == 0 )
        {
            /* set the "wait for trigger" bit if we're waiting for PPS or want a synchronized
             * start */
            RBF_SET(data, (wait_for_pps || synced) ? 1 : 0, WAIT_FOR_TRIGGER);

            /* set the packed mode bit appropriately */
            RBF_SET(data, pr->packed ? 1 : 0, DMA_PACKED_MODE);

            /* see if we're running in counter mode and update the bits appropriately */
            if ( pr->data_src == skiq_data_src_iq )
            {
                /* set for IQ mode and configure DC offset */
                RBF_SET(data, 0, DMA_RX_DATA_SRC);
                RBF_SET(data, pr->dc_offset ? 1 : 0, DC_OFFSET_CORR_ENA);

                /* enable the decimator and remove I/Q count mode decimation */
                status = switch_to_iq_mode_decimator( card, pr->hdl );
            }
            else
            {
                /* set for counter mode and disable DC offset */
                RBF_SET(data, 1, DMA_RX_DATA_SRC);
                RBF_SET(data, 0, DC_OFFSET_CORR_ENA);

                /* disable the decimator and switch to the counter mode decimator */
                status = switch_to_counter_mode_decimator( card, pr->hdl );
            }

            /* enable the FIFO control */
            RBF_SET( data, 1, DMA_RX_FIFO_CTRL );

            if ( status == 0 )
            {
            /* pr->ctrl_reg are on a different clock domain, so must verify to confirm write */
            status = sidekiq_fpga_reg_write_and_verify( card, pr->ctrl_reg, data );
            }
        }
    }

    if ( status == 0 )
    {
        if ( wait_for_pps )
        {
            /* set the pps timestamp after */
            status = sidekiq_fpga_reg_write_64(card,
                                               FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH,
                                               FPGA_REG_1PPS_RESET_TIMESTAMP_LOW,
                                               pps_sys_timestamp);

            /*********** toggle the GATE_ON_PPS_START ***********/
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 1, GATE_ON_PPS_START, FPGA_REG_TIMESTAMP_RST);
            }
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 0, GATE_ON_PPS_START, FPGA_REG_TIMESTAMP_RST);
            }
            /****************************************************/

            /* wait for the PPS edge */
            if ( status == 0 )
            {
                status = _wait_1pps_after_ts(card, pps_sys_timestamp);
            }

            /* let's clear the "wait for trigger bit" across all handles */
            if ( status == 0 )
            {
                clear_wait_for_trigger_multi( card, rx_params, nr_rx_params );
            }

            /* Prepare the FPGA for the next 1PPS pulse edge alignment */
            if (status == 0)
            {
                status = prepare_next_1pps(card);
            }
        }
        else if ( synced )
        {
            if ( _skiq_has_dc_offset_corr( card ) )
            {
                /* NOTE: if 'synced', there's an assumption that both WAIT_FOR_TRIGGER and
                 * DMA_RX_FIFO_CTRL have been set for all handles in rx_params.  This means all
                 * handles are effectively 'armed' */

                /* poll until the DC Offset block in the FPGA indicates ADC hold-off is complete.
                 * This isn't exactly intuitively named, however there's an ADC hold-off as an input
                 * into the DC Offset block that causes a 1920 cycle delay before samples are ready
                 * and available.  If MANUAL_TRIGGER is set before that hold-off is complete, the
                 * samples are not aligned / synchronized.  */
                status = _wait_dc_offset_valid(card);
            }

            /* set MANUAL_TRIGGER */
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 1, MANUAL_TRIGGER, FPGA_REG_TIMESTAMP_RST);
            }

            /* Clear the "wait for trigger bit" across all rx_params, manual trigger is immediate
             * and samples are already flowing */
            if ( status == 0 )
            {
                status = clear_wait_for_trigger_multi( card, rx_params, nr_rx_params );
            }

            /* now that all handles have started streaming, 'reset' the MANUAL_TRIGGER bit in the
             * TIMESTAMP_RST block */
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 0, MANUAL_TRIGGER, FPGA_REG_TIMESTAMP_RST);
            }
        }
    }

    /* squash return code to -EBADMSG for any error encountered reading / writing FPGA registers.
     * Don't change -ENOTSUP however, that's from FPGA version / capability checks, nor -ETIMEDOUT,
     * nor -EPROTO */
    if ( ( status != 0 ) && ( status != -ENOTSUP ) && ( status != -ETIMEDOUT ) &&
         ( status != -EPROTO ) )
    {
        status = -EBADMSG;
    }

    return (status);
}


/**************************************************************************************************/
/** fpga_ctrl_stop_rx_stream_multi() stop streaming on multiple receive streams.  It does so
    synchronously if the FPGA bitstream supports it and if the caller requests it
    (::skiq_trigger_src_synced or ::skiq_trigger_src_1pps).

    @return int32_t
    @retval 0 success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP Requested synchronization trigger not supported by FPGA bitstream
    @retval -EPROTO System Timestamp is not changing or moving backwards
    @retval -ETIMEDOUT 1PPS trigger was requested and never occurred
 */
int32_t fpga_ctrl_stop_rx_stream_multi( uint8_t card,
                                        fpga_ctrl_rx_params_t rx_params[],
                                        uint8_t nr_rx_params,
                                        skiq_trigger_src_t trigger_src,
                                        uint64_t pps_sys_timestamp )
{
    bool wait_for_pps = (trigger_src == skiq_trigger_src_1pps);
    bool synced = (trigger_src == skiq_trigger_src_synced);
    int32_t status = 0;

    /* zero rx_params, just return success */
    if ( nr_rx_params == 0 )
    {
        return 0;
    }

    /* one rx_params and synced requested, just fallback to immediate */
    if ( nr_rx_params == 1 )
    {
        synced = false;
    }

    /* check for FPGA version and capabilities before allowing 'skiq_trigger_src_synced' as a
     * trigger source */
    if ( synced )
    {
        if ( _skiq_meets_FPGA_VERSION( card, FPGA_VERS_MANUAL_TRIGGER ) )
        {
            struct fpga_capabilities caps;

            caps = _skiq_get_fpga_caps(card);
            if ( caps.manual_trigger == 0 )
            {
                status = -ENOTSUP;
            }
        }
        else
        {
            status = -ENOTSUP;
        }
    }

    if ( status == 0 )
    {
        if ( wait_for_pps )
        {
            /* let's set the "wait for trigger bit" */
            status = set_wait_for_trigger_multi( card, rx_params, nr_rx_params );

            /* set the pps timestamp after */
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_write_64(card,
                                                   FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH,
                                                   FPGA_REG_1PPS_RESET_TIMESTAMP_LOW,
                                                   pps_sys_timestamp);
            }

            /****************************************************/
            /* toggle the GATE_ON_PPS_STOP */
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 1, GATE_ON_PPS_STOP, FPGA_REG_TIMESTAMP_RST);
            }
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 0, GATE_ON_PPS_STOP, FPGA_REG_TIMESTAMP_RST);
            }
            /****************************************************/

            /* wait for the PPS edge */
            if ( status == 0 )
            {
                status = _wait_1pps_after_ts(card, pps_sys_timestamp);
            }

            /* let's clear the "wait for trigger bit" */
            if ( status == 0 )
            {
                status = clear_wait_for_trigger_multi( card, rx_params, nr_rx_params );
            }

            /* Prepare the FPGA for the next 1PPS pulse edge alignment */
            if (status == 0)
            {
                status = prepare_next_1pps(card);
            }
        }
        else if ( synced )
        {
            /* set the MANUAL_TRIGGER */
            status = sidekiq_fpga_reg_RMWV(card, 1, MANUAL_TRIGGER, FPGA_REG_TIMESTAMP_RST);

            /* let's set the "wait for trigger bit" */
            if ( status == 0 )
            {
                status = set_wait_for_trigger_multi( card, rx_params, nr_rx_params );
            }

            /* clear the MANUAL_TRIGGER */
            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_RMWV(card, 0, MANUAL_TRIGGER, FPGA_REG_TIMESTAMP_RST);
            }

            /* let's clear the "wait for trigger bit" */
            if ( status == 0 )
            {
                status = clear_wait_for_trigger_multi( card, rx_params, nr_rx_params );
            }
        }
    }

    if ( status == 0 )
    {
        /* disable FIFO control across all control registers */
        status = clear_dma_rx_fifo_ctrl_multi( card, rx_params, nr_rx_params );
    }

    if ( status == 0 )
    {
        uint8_t i;

        /* iterate over all of the control registers, setting all back to the I/Q decimator */
        for ( i = 0; ( i < nr_rx_params ) && ( status == 0 ); i++)
        {
            fpga_ctrl_rx_params_t *pr = &(rx_params[i]);

            /* enable the decimator and remove I/Q count mode decimation */
            status = switch_to_iq_mode_decimator( card, pr->hdl );
        }
    }

    /* squash return code to -EBADMSG for any error encountered reading / writing FPGA registers.
     * Don't change -ENOTSUP however, that's from FPGA version / capability checks, nor -ETIMEDOUT,
     * nor -EPROTO */
    if ( ( status != 0 ) && ( status != -ENOTSUP ) && ( status != -ETIMEDOUT ) )
    {
        status = -EBADMSG;
    }

    return (status);
}

int32_t fpga_ctrl_enable_stream( uint8_t card )
{
    int32_t status = 0;

    /* set FPGA to be in non-streaming mode pin configuration */
    status = sidekiq_fpga_reg_RMWV(card, 1, CONFIG_STREAMING, FPGA_REG_TIMESTAMP_RST);

    return (status);
}

int32_t fpga_ctrl_finalize_stream_disable( uint8_t card )
{
    int32_t status=0;

    status = sidekiq_fpga_reg_RMWV(card, 0, CONFIG_STREAMING, FPGA_REG_TIMESTAMP_RST);

    return (status);
}

int32_t fpga_ctrl_start_tx_streaming( uint8_t card,
                                      fpga_ctrl_tx_params_t *p_tx_params )
{
    int32_t status=0;
    uint32_t data=0;

    /* set the block size...this should only be pushed to the FPGA on a reg update */
    sidekiq_fpga_reg_read(card, FPGA_REG_TX_NUM_SAMPLES, &data);
    RBF_SET(data, p_tx_params->fpga_block_size, TX_NUM_SAMPLES);
    sidekiq_fpga_reg_write(card, FPGA_REG_TX_NUM_SAMPLES, data);

    /* flush the FIFO  */
    sidekiq_fpga_reg_read(card,FPGA_REG_DMA_CTRL_RX1,&data);
    // we need to verify here to make sure the update is actually committed
    // before we do a read later on
    RBF_SET(data,1,DMA_TX_FIFO_CTRL);
    status = sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_DMA_CTRL_RX1,data);

    if( status == 0 )
    {
        BF_SET(data,0,DMA_TX_FIFO_CTRL_OFFSET,DMA_TX_FIFO_CTRL_LEN);
        status = sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_DMA_CTRL_RX1,data);

        if( status == 0 )
        {
            sidekiq_fpga_reg_read(card,FPGA_REG_DMA_CTRL_RX1,&data);

            /* set/clear the "wait for trigger" bit */
            RBF_SET(data, p_tx_params->wait_for_pps ? 1 : 0, WAIT_FOR_TRIGGER);

            /* set the mode */
            RBF_SET(data,
                    ( p_tx_params->flow_mode == skiq_tx_immediate_data_flow_mode ) ? 1 : 0,
                    DMA_TX_MODE);

            /* set the packed mode bit appropriately */
            RBF_SET(data, p_tx_params->packed ? 1 : 0, DMA_PACKED_MODE);

            status = sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_DMA_CTRL_RX1,data);

            // call the TX enabled callback if it's registered
            if( p_tx_params->tx_enabled_cb != NULL )
            {
                p_tx_params->tx_enabled_cb( card, status );
            }
        }
    }

    if( (p_tx_params->wait_for_pps == true) && (status==0) )
    {
        /* set the pps timestamp after */
        sidekiq_fpga_reg_write_64(card,
                                  FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH,
                                  FPGA_REG_1PPS_RESET_TIMESTAMP_LOW,
                                  p_tx_params->pps_sys_timestamp);

        /* set, then clear the GATE_ON_PPS */
        sidekiq_fpga_reg_RMWV(card, 1, GATE_ON_PPS_START, FPGA_REG_TIMESTAMP_RST);
        sidekiq_fpga_reg_RMWV(card, 0, GATE_ON_PPS_START, FPGA_REG_TIMESTAMP_RST);

        status = _wait_1pps_after_ts(card, p_tx_params->pps_sys_timestamp);

        /* let's clear the "wait for PPS bit */
        sidekiq_fpga_reg_RMWV(card, 0, WAIT_FOR_TRIGGER, FPGA_REG_DMA_CTRL_RX1);

        /* Prepare the FPGA for the next 1PPS pulse edge alignment */
        if (status == 0)
        {
            status = prepare_next_1pps(card);
        }
    }
    return (status);
}

int32_t fpga_ctrl_stop_tx_stream( uint8_t card )
{
    int32_t status=0;
    uint32_t data=0;

    /* let's clear the "wait for PPS bit */
    sidekiq_fpga_reg_RMWV(card, 0, WAIT_FOR_TRIGGER, FPGA_REG_DMA_CTRL_RX1);

    /* disable the output buffer in the FPGA */
    sidekiq_fpga_reg_read(card,FPGA_REG_RF_BAND_SEL,&data);
    RBF_SET(data, 0, TX_ENA);
    RBF_SET(data, 0, TX_ENA_A2);
    sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_RF_BAND_SEL,data);

    /* flush the FIFOs */
    sidekiq_fpga_reg_RMWV(card, 1, DMA_TX_FIFO_CTRL, FPGA_REG_DMA_CTRL_RX1);

    return (status);
}

int32_t fpga_ctrl_stop_tx_stream_on_1pps( uint8_t card, uint64_t pps_sys_timestamp )
{
    int32_t status=0;

    /////////////////////////////////////////////
    /* set the pps timestamp after */
    sidekiq_fpga_reg_write_64(card, FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH,
                              FPGA_REG_1PPS_RESET_TIMESTAMP_LOW, pps_sys_timestamp);

    /* set, then clear the GATE_ON_PPS_STOP */
    sidekiq_fpga_reg_RMWV(card, 1, GATE_ON_PPS_STOP, FPGA_REG_TIMESTAMP_RST);
    sidekiq_fpga_reg_RMWV(card, 0, GATE_ON_PPS_STOP, FPGA_REG_TIMESTAMP_RST);

    /* wait for the PPS edge */
    status = _wait_1pps_after_ts(card, pps_sys_timestamp);

    /* Prepare the FPGA for the next 1PPS pulse edge alignment */
    if (status == 0)
    {
        status = prepare_next_1pps(card);
    }
    ///////////////////////////////

    return (status);
}

/*****************************************************************************/
/** The _wait_1pps function is responsible for polling the FPGA to see if the
    PPS has occurred.

    @param card Sidekiq card of interest

    @return int32_t
    @retval -ETIMEDOUT Never received 1PPS edge in the allowed time
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
static int32_t _wait_1pps(uint8_t card, uint32_t timeout_ms)
{
    int32_t status=0;
    uint32_t data=0;
    uint32_t wait_time_ms=0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_READ_RF_BAND_SEL, &data);
    while( (status==0) &&
           (RBF_GET(data, WAIT_FOR_PPS) != 0) &&
           (wait_time_ms<=timeout_ms) )
    {
        hal_millisleep(PPS_POLL_TIME_MS);
        wait_time_ms += PPS_POLL_TIME_MS;
        
        status = sidekiq_fpga_reg_read(card, FPGA_REG_READ_RF_BAND_SEL, &data);
    }

    /* squash return code to -EBADMSG for non-zero status when reading from FPGA registers */
    if ( status != 0 )
    {
        status = -EBADMSG;
    }

    // set error if timed out
    if ( ( status == 0 ) && ( wait_time_ms > timeout_ms ) )
    {
        skiq_error( "Never received 1PPS edge while waiting (card=%u)", card);
        status = -ETIMEDOUT;
    }
    
    // make to clear the 1PPS trigger if any error occurred
    if( status != 0 )
    {
        fpga_ctrl_cancel_1pps(card);
    }

    return (status);
}


/**************************************************************************************************/
/** The _wait_1pps_after_ts() function is responsible for polling the FPGA to see if the PPS has
    occurred after the specified timestamp has passed.

    @attention It is highly recommended, if not required, that ::FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH
    and ::FPGA_REG_1PPS_RESET_TIMESTAMP_LOW have been set to @p pps_sys_timestamp PRIOR to calling
    this function.  The ::FPGA_REG_1PPS_RESET register's value indicates the minimum system
    timestamp the card's FRC must reach before considering 1PPS pulses.

    @param[in] card Sidekiq card of interest
    @param[in] pps_sys_timestamp Minimum system timestamp before polling for 1PPS

    @return int32_t
    @retval -EPROTO System Timestamp is not changing or moving backwards
    @retval -ETIMEDOUT Never received 1PPS edge in the allowed time
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
static int32_t _wait_1pps_after_ts( uint8_t card,
                                    uint64_t pps_sys_timestamp )
{
    uint64_t sys_ts = 0, last_sys_ts = 0;
    int32_t status = 0;

    while ( ( status == 0 ) && ( sys_ts < pps_sys_timestamp ) )
    {
        if ( sys_ts > 0 )
        {
            hal_millisleep(PPS_POLL_TIME_MS);
        }

        status = sidekiq_fpga_reg_read_64( card,
                                           FPGA_REG_SYS_TIMESTAMP_HIGH,
                                           FPGA_REG_SYS_TIMESTAMP_LOW,
                                           &sys_ts );
        if ( ( status == 0 ) && ( sys_ts <= last_sys_ts ) )
        {
            /* the System Timestamp is not behaving, it either didn't change or it went backward, so
             * bail with an Protocol error to indicate something's not right */
            status = -EPROTO;
        }
        else if ( status == 0 )
        {
            last_sys_ts = sys_ts;
        }
        else
        {
            status = -EBADMSG;
        }
    }

    if ( status == 0 )
    {
        status = _wait_1pps( card, PPS_MAX_TIME_MS );
    }

    return status;
}


/*****************************************************************************/
/** The _clear_1pps function is responsible for clearing any PPS related bits
    that may be set.

    @param card Sidekiq card of interest
    @return void
*/
static void _clear_1pps(uint8_t card)
{
    uint32_t data=0;

    /* we need to clear all the PPS pending states and then toggle the cancel bit */
    sidekiq_fpga_reg_read(card, FPGA_REG_TIMESTAMP_RST, &data);
    /* clear anything that might be waiting for a trigger */
    RBF_SET(data, 0, RESET_TS_ON_PPS);
    RBF_SET(data, 0, SYNC_ON_PPS);
    RBF_SET(data, 0, GATE_ON_PPS_START);
    RBF_SET(data, 0, GATE_ON_PPS_STOP);

    sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_TIMESTAMP_RST, data);
}


/**************************************************************************************************/
/** The _prepare_next_1pps function is responsible for toggling PPS_CANCEL in FPGA_REG_TIMESTAMP_RST.
    It's required to do so after every successful (or unsuccessful) TRIGGER to prepare the FPGA to
    get ready for the next 1PPS pulse edge alignment

    @param card Sidekiq card of interest
    @return void
*/
static int32_t prepare_next_1pps(uint8_t card)
{
    int32_t status;

    status = sidekiq_fpga_reg_RMWV(card, 1, PPS_CANCEL, FPGA_REG_TIMESTAMP_RST);
    if (status == 0)
    {
        status = sidekiq_fpga_reg_RMWV(card, 0, PPS_CANCEL, FPGA_REG_TIMESTAMP_RST);
    }

    return status;
}


/****************************************************************************/
/** The cancel_1pps function is responsible for canceling/disabling any \
    possible pending action waiting for 1PPS edge.

    @param card Sidekiq card of interest
    @return void
*/
void fpga_ctrl_cancel_1pps(uint8_t card)
{
    /* Clear all the PPS pending states */
    _clear_1pps(card);

    /* Prepare the FPGA for the next 1PPS pulse edge alignment */
    (void)prepare_next_1pps(card);
}


/*************************************************************************************************/
/** The fpga_ctrl_read_sys_timestamp_freq function queries the FPGA and/or RFIC for the frequency at
    which the system timestamp increments.

    @param[in] card Sidekiq card of interest
    @param[out] p_sys_timestamp_freq pointer to where to store the system timestamp frequency

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ERANGE The system timestamp frequency indicated by the FPGA is out of range
    @retval -ENOTSUP Sidekiq RFIC does not support querying system timestamp frequency
*/
int32_t fpga_ctrl_read_sys_timestamp_freq( uint8_t card,
                                           uint64_t *p_sys_timestamp_freq )
{
    int32_t status=0;
    uint32_t val=0;
    uint32_t sys_freq=0;
    rfic_t rfic_instance;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_CAPABILITIES, &val);
    if ( status == 0 )
    {
        sys_freq = BF_GET(val, SYSTEM_CLOCK_RATE_OFFSET, SYSTEM_CLOCK_RATE_LEN);
        switch( sys_freq )
        {
            // 40 MHz
            case 0:
                *p_sys_timestamp_freq = 40000000;
                break;

            // based on FPGA RF clock
            case 1:
                rfic_instance = _skiq_get_generic_rfic_instance( card );
                status = rfic_read_fpga_rf_clock( &rfic_instance, p_sys_timestamp_freq );
                break;

            default:
                status = -ERANGE;
                break;
        }
    }
    else
    {
        /* Error occurred transacting with FPGA registers */
        status = -EBADMSG;
    }

    return (status);
}


/* set the field value of RX_PACKET_64_WORDS in TIMESTAMP_RST */
static int32_t set_rx_packet_64_words( uint8_t card, uint32_t field_value )
{
    int32_t status;
    uint32_t value;

    status = sidekiq_fpga_reg_read( card, FPGA_REG_TIMESTAMP_RST, &value );
    if ( status == 0 )
    {
        BF_SET(value, field_value, RX_PACKET_64_WORDS_OFFSET, RX_PACKET_64_WORDS_LEN);
        status = sidekiq_fpga_reg_write( card, FPGA_REG_TIMESTAMP_RST, value );
    }

    return (status);
}


/* sets RX packet 64 words control bit */
int32_t fpga_ctrl_enable_rx_packet_64_words( uint8_t card )
{
    return (set_rx_packet_64_words( card, 1 ));
}


/* clears RX packet 64 words control bit */
int32_t fpga_ctrl_disable_rx_packet_64_words( uint8_t card )
{
    return (set_rx_packet_64_words( card, 0 ));
}


/*************************************************************************************************/
/** The fpga_ctrl_enable_iq_complex_multiplier function tells the FPGA to apply the IQ complex
    multiplication for a given card.

    @note A user can only enable / disable the complex multipliers over @b all available handles.
    If a user wishes to enable / disable multipliers for a specific handle, they may effectively do
    so by writing an absolute factor of (1.0, 0.0).

    @attention Using this function while receive data is streaming will result in unexpected
    behavior.

    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_enable_iq_complex_multiplier( uint8_t card )
{
    int32_t status = 0;

    if ( has_iq_complex_mult( card ) )
    {
        status = sidekiq_fpga_reg_RMWV( card, 1, IQ_MULT_ENABLE, FPGA_REG_FPGA_CTRL );
        if ( status != 0 )
        {
            /* a fault occurred communicating with the FPGA */
            status = -EIO;
        }
    }
    else
    {
        /* FPGA does not meet minimum interface version requirements */
        status = -ENOSYS;
    }

    return (status);

} /* fpga_ctrl_enable_iq_complex_multiplier */


/*************************************************************************************************/
/** The fpga_ctrl_disable_iq_complex_multiplier function tells the FPGA to bypass the IQ complex
    multiplication for a given card.

    @note A user can only enable / disable the complex multipliers over @b all available handles.
    If a user wishes to enable / disable multipliers for a specific handle, they may effectively do
    so by writing an absolute factor of (1.0, 0.0).

    @attention Using this function while receive data is streaming will result in unexpected
    behavior.

    @param[in] card Sidekiq card of interest

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_disable_iq_complex_multiplier( uint8_t card )
{
    int32_t status = 0;

    if ( has_iq_complex_mult( card ) )
    {
        status = sidekiq_fpga_reg_RMWV( card, 0, IQ_MULT_ENABLE, FPGA_REG_FPGA_CTRL );
        if ( status != 0 )
        {
            /* a fault occurred communicating with the FPGA */
            status = -EIO;
        }
    }
    else
    {
        /* FPGA does not meet minimum interface version requirements */
        status = -ENOSYS;
    }

    return (status);

} /* fpga_ctrl_disable_iq_complex_multiplier */


/*************************************************************************************************/
/** The fpga_ctrl_read_iq_complex_multiplier function queries the FPGA for the complex
    multiplication factor for a given card and handle.

    @param[in] card Sidekiq card of interest
    @param[in] hdl  receive handle of interest
    @param[out] p_factor reference to the complex multiplication factor

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -EINVAL An invalid / unsupported receive handle was specified
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_read_iq_complex_multiplier( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              float complex *p_factor )
{
    int32_t status = 0;

    if ( has_iq_complex_mult( card ) )
    {
        if ( hdl < skiq_rx_hdl_C1 )
        {
            uint32_t data = 0;

            status = sidekiq_fpga_reg_read( card, FPGA_REG_IQ_MULT_(hdl), &data );
            if ( status == 0 )
            {
                int16_t real_fxp, imag_fxp;

                real_fxp = RBF_GET( data, RE_CMPLX_MULT_FACTOR );
                imag_fxp = RBF_GET( data, IM_CMPLX_MULT_FACTOR );

                *p_factor = ((float)real_fxp) / (float)(1 << RE_CMPLX_MULT_FACTOR_RESOLUTION);
                *p_factor += I * ((float)imag_fxp) / (float)(1 << IM_CMPLX_MULT_FACTOR_RESOLUTION);
            }
            else
            {
                /* a fault occurred communicating with the FPGA */
                status = -EIO;
            }
        }
        else
        {
            /* I/Q complex multipliers only support handles A1/A2/B1/B2 */
            status = -EINVAL;
        }
    }
    else
    {
        /* FPGA does not meet minimum interface version requirements */
        status = -ENOSYS;
    }

    return (status);

} /* fpga_ctrl_read_iq_complex_multiplier */


/*************************************************************************************************/
/** The fpga_ctrl_write_iq_complex_multiplier function converts the provided complex multiplication
    factor into fixed point representation, then writes it to the FPGA for a given card and handle.

    @attention The accepted range for the real and imaginary parts of the complex multiplication
    factor is -1.0 to 1.0, inclusive

    @param[in] card Sidekiq card of interest
    @param[in] hdl  receive handle of interest
    @param[in] factor complex multiplication factor

    @return int32_t  status where 0=success, anything else is an error
    @retval 0 Success
    @retval -EIO A fault occurred communicating with the FPGA
    @retval -ERANGE either i_factor or q_factor are out of the supported range
    @retval -ENOSYS FPGA does not meet minimum interface version requirements

*/
int32_t fpga_ctrl_write_iq_complex_multiplier( uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               float complex factor )
{
    int32_t status = 0;

    if ( ( crealf(factor) < -1.0 ) || ( crealf(factor) > 1.0 ) ||
         ( cimagf(factor) < -1.0 ) || ( cimagf(factor) > 1.0 ) )
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        if ( has_iq_complex_mult( card ) )
        {
            uint32_t data = 0;
            int16_t real_fxp, imag_fxp;
            int32_t real_fxp32, imag_fxp32;

            /* convert from floating point to fixed point */
            real_fxp32 = (int32_t)(crealf(factor) * (float)(1 << RE_CMPLX_MULT_FACTOR_RESOLUTION));
            imag_fxp32 = (int32_t)(cimagf(factor) * (float)(1 << IM_CMPLX_MULT_FACTOR_RESOLUTION));

            /* saturate values greater than maximum */
            real_fxp32 = MIN(real_fxp32, (int32_t)RE_CMPLX_MULT_FACTOR_MAX);
            real_fxp32 = MAX(real_fxp32, (int32_t)RE_CMPLX_MULT_FACTOR_MIN);
            imag_fxp32 = MIN(imag_fxp32, (int32_t)IM_CMPLX_MULT_FACTOR_MAX);
            imag_fxp32 = MAX(imag_fxp32, (int32_t)IM_CMPLX_MULT_FACTOR_MIN);

            /* type conversion to int16_t */
            real_fxp = (int16_t)real_fxp32;
            imag_fxp = (int16_t)imag_fxp32;

            /* construct the register contents */
            RBF_SET(data, real_fxp, RE_CMPLX_MULT_FACTOR);
            RBF_SET(data, imag_fxp, IM_CMPLX_MULT_FACTOR);

            status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_IQ_MULT_(hdl), data );
            if ( status != 0 )
            {
                /* a fault occurred communicating with the FPGA */
                status = -EIO;
            }
        }
        else
        {
            /* FPGA does not meet minimum interface version requirements */
            status = -ENOSYS;
        }
    }

    return (status);

} /* fpga_ctrl_write_iq_complex_multiplier */


/**************************************************************************************************/
int32_t
fpga_ctrl_init_config( uint8_t card )
{
    int32_t status;

    status = decimator_init_config( card );

    return status;
}


/**************************************************************************************************/
int32_t
fpga_ctrl_reset_config( uint8_t card )
{
    int32_t status;

    status = decimator_reset_config( card );
    if ( status == 0 )
    {
        status = fpga_ctrl_disable_rx_packet_64_words( card );
    }

    // If present, attempt to reset the PDK registers using the soft reset functionality;
    // only return a failure on a failure and not if the transport and/or FPGA don't support
    // it
    if ( status == 0 )
    {
        status = fpga_ctrl_issue_soft_reset(card);
        if ((-ENOSYS == status) || (-ENOTSUP == status))
        {
            status = 0;
        }
    }

    return status;
}


/**************************************************************************************************/
int32_t fpga_ctrl_write_iq_order_mode( uint8_t card,
                                       skiq_iq_order_t iq_order_mode )
{
    int32_t status = 0;

    if (has_iq_order_mode( card ))
    {
 
        status = sidekiq_fpga_reg_RMWV(card, ((iq_order_mode == skiq_iq_order_qi) ? 0 : 1), 
                                       IQ_SWAP, FPGA_REG_TIMESTAMP_RST );
        if ( status != 0 )
        {
            /* a fault occurred communicating with the FPGA */
            status = -EIO;
        }
    }
    else if (iq_order_mode == skiq_iq_order_qi)
    {
        /* requesting of skiq_iq_order_qi even on older FPGAs should be allowed */
        status = 0;
    }
    else
    {
        /* FPGA does not meet minimum interface version requirements */
        status = -ENOSYS;
    }

    return (status);

} /* fpga_ctrl_write_iq_order_mode */


/**************************************************************************************************/
int32_t fpga_ctrl_read_iq_order_mode( uint8_t card,
                                      skiq_iq_order_t *iq_order_mode )
{
    int32_t status = 0;
    uint32_t val = 0;

    if (has_iq_order_mode( card ))
    {

        if ( sidekiq_fpga_reg_read( card, FPGA_REG_TIMESTAMP_RST, &val ) == 0 )
        {
            *iq_order_mode = (RBF_GET(val, IQ_SWAP) ? skiq_iq_order_iq : skiq_iq_order_qi);
        }
        else
        {
            /* a fault occurred communicating with the FPGA */
            status = -EIO;
         }
    }
    else
    {
        /* FPGA does not meet minimum interface version requirements */
        *iq_order_mode = skiq_iq_order_qi;
    }
    return (status);

} /* fpga_ctrl_read_iq_order_mode */


/**************************************************************************************************/
int32_t fpga_ctrl_wait_for_bit( uint8_t card,
                                uint32_t desired_value,
                                uint8_t field_offset,
                                uint8_t field_length,
                                uint32_t reg_addr,
                                uint32_t poll_ns,
                                uint32_t timeout_ns )
{
    int32_t status = 0;
    uint32_t data = 0;
    bool is_valid = false, first_time = true;
    struct timespec ts_now, ts_deadline;

    if ( clock_gettime( CLOCK_MONOTONIC, &ts_now ) == 0 )
    {
        ts_deadline = ts_now;
        ts_deadline.tv_nsec += timeout_ns;
        while ( ts_deadline.tv_nsec >= (long)1e9 )
        {
            ts_deadline.tv_nsec -= (long)1e9;
            ts_deadline.tv_sec++;
        }

        /* poll the register at `reg_addr`, checking the register field (offset,length) against
         * `desired_value` */
        do
        {
            /* if it's not the first time through the loop, sleep for 100us and update ts_now in a
             * call to clock_gettime() */
            if ( !first_time )
            {
                hal_nanosleep( poll_ns );
                if ( clock_gettime( CLOCK_MONOTONIC, &ts_now ) != 0 )
                {
                    /* squash errors from clock_gettime() into a simple I/O error */
                    status = -EPROTO;
                }
            }
            else
            {
                first_time = false;
            }

            if ( status == 0 )
            {
                status = sidekiq_fpga_reg_read( card, reg_addr, &data );
                if ( status != 0 )
                {
                    /* squash return code to -EBADMSG for any error encountered reading / writing
                     * FPGA registers. */
                    status = -EIO;
                }
            }

            if ( status == 0 )
            {
                is_valid = ( BF_GET(data, field_offset, field_length) == desired_value );
            }

        } while ( ( status == 0 ) && !is_valid && timespec_gte( &ts_deadline, &ts_now ) );

        if ( ( status == 0 ) && !is_valid )
        {
            skiq_error("Timed out waiting for field to match on card %" PRIu8 "\n", card);
            status = -ETIMEDOUT;
        }
    }
    else
    {
        status = -EPROTO;
    }

    return status;
}


int32_t fpga_ctrl_config_hop_on_timestamp( uint8_t card,
                                           bool enable,
                                           uint32_t rfic_gpio,
                                           uint8_t chip_index )
{
    int32_t status=0;
    uint32_t val=0;
    uint32_t gpio_ddr_reg = FPGA_REG_GPIO_TRISTATE;
    uint32_t gpio_val_reg = FPGA_REG_GPIO_WRITE;
    uint32_t hop_en_offset = GPIO_FREQ_HOP_EN_A_OFFSET;
    uint32_t hop_en_len = GPIO_FREQ_HOP_EN_A_LEN;
    uint32_t hop_go_offset = GPIO_FREQ_HOP_GO_A_OFFSET;
    uint32_t hop_go_len = GPIO_FREQ_HOP_GO_A_LEN;

    // update to use the B defs
    if( chip_index == 1 )
    {
        gpio_ddr_reg = FPGA_REG_GPIF_TRISTATE;
        gpio_val_reg = FPGA_REG_GPIF_WRITE;
        hop_en_offset = GPIO_FREQ_HOP_EN_B_OFFSET;
        hop_en_len = GPIO_FREQ_HOP_EN_B_LEN;
        hop_go_offset = GPIO_FREQ_HOP_GO_B_OFFSET;
        hop_go_len = GPIO_FREQ_HOP_GO_B_LEN;
    }

    if ( _skiq_meets_FPGA_VERSION( card, FPGA_VERS_HOP_ON_TS ) )
    {
        // configure the RFIC CTRL as output from the FPGA perspective
        status = sidekiq_fpga_reg_read( card, gpio_ddr_reg, &val );
        if( status == 0 )
        {
            if( enable == true )
            {
                val = val | rfic_gpio;
            }
            else
            {
                val = val & ~rfic_gpio;
            }
            status = sidekiq_fpga_reg_write_and_verify( card, gpio_ddr_reg, val );
        }

        if( status == 0 )
        {
            // make sure the RFIC CTRL is set low to start
            if( enable == true )
            {
                status = sidekiq_fpga_reg_read( card, gpio_val_reg, &val );
                if( status == 0 )
                {
                    val = val & ~rfic_gpio;
                    status = sidekiq_fpga_reg_write_and_verify( card, gpio_val_reg, val );
                }
            }
        }

        if( status == 0 )
        {
            // make sure the FPGA is ready to drive the GPIO when hopping
            status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_FREQ_HOP_CTRL, &val );
            if( status == 0 )
            {
                if( enable == true )
                {
                    // set the enable GPIO to be driven by FPGA
                    BF_SET( val, 1, hop_en_offset, hop_en_len );
                    // idle with the GO low
                    BF_SET( val, 0, hop_go_offset, hop_go_len );
                }
                else
                {
                    // clear the enable GPIO to be driven by FPGA
                    BF_SET( val, 0, hop_en_offset, hop_en_len );
                }
                status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPIO_FREQ_HOP_CTRL, val );
            }
        }
    }
    else
    {
        // we only want to be noisy about the requirements if we're trying to enable hop on timestamp
        if( enable == true )
        {
            status = -ENOTSUP;
            skiq_error("FPGA does not meet minimum requirement (%s) to execute frequency hopping "
                       "on timestamp (card=%u)", FPGA_VERS_HOP_ON_TS_CSTR, card );
        }
    }
    
    return (status);
}

int32_t fpga_ctrl_hop( uint8_t card,
                       uint8_t chip_index, // 0=A, 1=B...TODO: a diff ID?
                       uint8_t rfic_ctrl_in,
                       uint8_t hop_index,
                       uint64_t timestamp )
{
    int32_t status=0;
    uint32_t val=0;
    uint32_t hop_data_cs_offset;
    uint32_t hop_index_cs_offset;
    uint32_t hop_go_cs_offset;
    uint32_t hop_ena_cs_offset;
    uint32_t hop_data_cs_len;
    uint32_t hop_index_cs_len;
    uint32_t hop_go_cs_len;
    uint32_t hop_ena_cs_len;
    uint32_t ts_high;
    uint32_t ts_low;

    // TODO: there is a cleaner way to do this...
    // A
    if( chip_index == 0 )
    {
        hop_data_cs_offset = GPIO_FREQ_HOP_DATA_A_OFFSET;
        hop_index_cs_offset = GPIO_FREQ_HOP_FREQ_INDEX_A_OFFSET;
        hop_go_cs_offset = GPIO_FREQ_HOP_GO_A_OFFSET;
        hop_ena_cs_offset = GPIO_FREQ_HOP_GO_A_OFFSET;
        hop_data_cs_len = GPIO_FREQ_HOP_DATA_A_LEN;
        hop_index_cs_len = GPIO_FREQ_HOP_FREQ_INDEX_A_LEN;
        hop_go_cs_len = GPIO_FREQ_HOP_GO_A_LEN;
        hop_ena_cs_len = GPIO_FREQ_HOP_GO_A_LEN;

        ts_high = FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_HIGH;
        ts_low = FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_A_LOW;
    }
    // B
    else if( chip_index == 1 )
    {
        hop_data_cs_offset = GPIO_FREQ_HOP_DATA_B_OFFSET;
        hop_index_cs_offset = GPIO_FREQ_HOP_FREQ_INDEX_B_OFFSET;
        hop_go_cs_offset = GPIO_FREQ_HOP_GO_B_OFFSET;
        hop_ena_cs_offset = GPIO_FREQ_HOP_GO_B_OFFSET;
        hop_data_cs_len = GPIO_FREQ_HOP_DATA_B_LEN;
        hop_index_cs_len = GPIO_FREQ_HOP_FREQ_INDEX_B_LEN;
        hop_go_cs_len = GPIO_FREQ_HOP_GO_B_LEN;
        hop_ena_cs_len = GPIO_FREQ_HOP_GO_B_LEN;

        ts_high = FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_B_HIGH;
        ts_low = FPGA_REG_GPIO_FREQ_HOP_TIMESTAMP_B_LOW;
    }
    else
    {
        status = -ERANGE;
    }

    if( status == 0 )
    {
        // program the hop timestamp
        status = sidekiq_fpga_reg_write_64( card,
                                            ts_high,
                                            ts_low,
                                            timestamp );
        if( status == 0 )
        {
            // now program the control
            status = sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_FREQ_HOP_CTRL, &val );
            if( status == 0 )
            {
                // set the CTRL_IN value
                BF_SET( val, rfic_ctrl_in, hop_data_cs_offset, hop_data_cs_len );
                // set the hop index
                BF_SET( val, hop_index, hop_index_cs_offset, hop_index_cs_len );
                // enable GPIO to be driven by FPGA
                BF_SET( val, 1, hop_ena_cs_offset, hop_ena_cs_len );
                // set the GO bit
                BF_SET( val, 1, hop_go_cs_offset, hop_go_cs_len );
                status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPIO_FREQ_HOP_CTRL, val );
                if( status == 0 )
                {
                    // bring the GO bit back low
                    BF_SET( val, 0, hop_go_cs_offset, hop_go_cs_len );
                    status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_GPIO_FREQ_HOP_CTRL, val );
                }
            }
        }
    }

    return (status);
}


/*
  @brief This function reads the 64-bit last 1PPS RF timestamp register.

  @note This function also returns 0 if @p p_rf_timestamp is NULL (caller can use NULL to indicate
  that they do not care about the value)
 */
static int32_t _read_last_1pps_rf_timestamp( uint8_t card,
                                             uint64_t *p_rf_timestamp )
{
    int32_t status = 0;

    if ( p_rf_timestamp != NULL )
    {
        uint64_t rf_timestamp = 0;

        status = sidekiq_fpga_reg_read_64( card, FPGA_REG_LAST_RF_A_1PPS_TIMESTAMP_HIGH,
                                           FPGA_REG_LAST_RF_A_1PPS_TIMESTAMP_LOW, &rf_timestamp );
        if ( status == 0 )
        {
            *p_rf_timestamp = rf_timestamp;
        }
    }

    return status;
}


/*
  @brief This function reads the 64-bit last 1PPS System timestamp register.

  @note This function also returns 0 if @p p_sys_timestamp is NULL (caller can use NULL to indicate
  that they do not care about the value)
 */
static int32_t _read_last_1pps_system_timestamp( uint8_t card,
                                                 uint64_t *p_sys_timestamp )
{
    int32_t status = 0;

    if ( p_sys_timestamp != NULL )
    {
        uint64_t sys_timestamp = 0;

        status = sidekiq_fpga_reg_read_64( card, FPGA_REG_LAST_SYS_1PPS_TIMESTAMP_HIGH,
                                           FPGA_REG_LAST_SYS_1PPS_TIMESTAMP_LOW, &sys_timestamp );
        if ( status == 0 )
        {
            *p_sys_timestamp = sys_timestamp;
        }
    }

    return status;
}


/*
  Sample the last 1PPS System Timestamp, then the last 1PPS RF Timestamp.

  If the two RF Timestamps match (rf_timestamp_to_verify and *p_rf_timestamp), then the
  *p_sys_timestamp sample is valid, indicate otherwise with a return of -ERANGE.
 */
static int32_t _qualify_system_timestamp( uint8_t card,
                                          uint64_t rf_timestamp_to_verify,
                                          uint64_t* p_rf_timestamp,
                                          uint64_t* p_sys_timestamp )
{
    int32_t status;

    status = _read_last_1pps_system_timestamp( card, p_sys_timestamp );
    if ( status == 0 )
    {
        status = _read_last_1pps_rf_timestamp( card, p_rf_timestamp );
    }

    if ( status == 0 )
    {
        if ( *p_rf_timestamp != rf_timestamp_to_verify )
        {
            status = -ERANGE;
        }
    }

    return status;
}


/*
   The user wants both RF Timestamp and System Timestamp, so take extra steps to make certain they
   are both from the same 1PPS period.
 */
static int32_t _read_last_1pps_timestamps_qualified( uint8_t card,
                                                     uint64_t* p_rf_timestamp,
                                                     uint64_t* p_sys_timestamp )
{
    int32_t status = 0;
    uint64_t rf_timestamp_base = 0, rf_timestamp_check = 0;
    uint64_t system_timestamp = 0;

    CHECK_NULL_PARAM( p_rf_timestamp );
    CHECK_NULL_PARAM( p_sys_timestamp );

    status = _read_last_1pps_rf_timestamp( card, &rf_timestamp_base );
    if ( status == 0 )
    {
        status = _qualify_system_timestamp( card, rf_timestamp_base,
                                            &rf_timestamp_check, &system_timestamp );

        /* -ERANGE means the register reads where successful, but the RF timestamps didn't agree,
            meaning the system_timestamp was either from the last last 1PPS period or the last 1PPS
            period.  Since we can't know for sure, perform the qualification again */
        if ( status == -ERANGE )
        {
            rf_timestamp_base = rf_timestamp_check;

            /* If this call to qualify ends up failing with -ERANGE, just go with it.  If the first
             * qualification fails, it should be squarely in the 1PPS period for the next timestamp
             * sampling, so any qualification failure here means the transport is unreliable
             * timewise (e.g. slow or intermittent) to get a good sampling.  Any further sampling is
             * then suspectible to the transport's reliability to provide reasonable register
             * access. */
            status = _qualify_system_timestamp( card, rf_timestamp_base,
                                                &rf_timestamp_check, &system_timestamp );
        }

        /* either the first or second call to _qualify_system_timestamp() passed qualification, so
         * copy the two timestamps to the user-provided references */
        if ( status == 0 )
        {
            *p_rf_timestamp = rf_timestamp_check;
            *p_sys_timestamp = system_timestamp;
        }
    }

    if ( ( status != 0 ) && ( status != -ERANGE ) )
    {
        /* squash failed register transactions to -EBADMSG */
        status = -EBADMSG;
    }

    return status;
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t fpga_ctrl_read_last_1pps_timestamps( uint8_t card,
                                             uint64_t* p_rf_timestamp,
                                             uint64_t* p_sys_timestamp )
{
    int32_t status = 0;

    /* If one of the pointers is NULL, then just read the 64-bit register normally, otherwise call
     * _read_last_1pps_timestamps_qualified() to guarantee the timestamps are from the same
     * period */
    if ( p_rf_timestamp == NULL )
    {
        /* this function returns 0 if successful or if p_sys_timestamp is NULL */
        status = _read_last_1pps_system_timestamp( card, p_sys_timestamp );
    }
    else if ( p_sys_timestamp == NULL )
    {
        /* this function returns 0 if successful or if p_rf_timestamp is NULL */
        status = _read_last_1pps_rf_timestamp( card, p_rf_timestamp );
    }
    else
    {
        status = _read_last_1pps_timestamps_qualified( card, p_rf_timestamp, p_sys_timestamp );
    }

    return status;
}

/**
    @brief  Issue a soft reset of the Sidekiq PDK registers
*/
int32_t
fpga_ctrl_issue_soft_reset(uint8_t card)
{
#define FPGA_SOFT_RESET_VER_MAJOR   (3)
#define FPGA_SOFT_RESET_VER_MINOR   (15)
#define FPGA_SOFT_RESET_VER_PATCH   (1)

    int32_t status = -ENOTSUP;
    bool hasFeature = false;

    if (_skiq_meets_fpga_version(card, FPGA_SOFT_RESET_VER_MAJOR, FPGA_SOFT_RESET_VER_MINOR,
            FPGA_SOFT_RESET_VER_PATCH))
    {
        struct fpga_capabilities fpga_caps = FPGA_CAPABILITIES_INITIALIZER;

        /*
            call fpga_ctrl_read_capabilities() here instead of _skiq_get_fpga_caps() because there
            there are cases when the capabilities are not yet cached
        */
        status = fpga_ctrl_read_capabilities(card, &fpga_caps);
        if (0 != status)
        {
            skiq_error("Failed to read FPGA capabilities on card %" PRIu8 " (status = %" PRIi32
                ")\n", card, status);
            status = -EBADMSG;
        }
        else
        {
            hasFeature = (fpga_caps.fpga_soft_reset > 0);
        }

        debug_print("card %" PRIu8 ": FPGA %s the soft reset feature\n", card,
            hasFeature ? "has" : "does not have");
    }

    if (hasFeature)
    {
        status = sidekiq_fpga_reg_RMWV(card, 1, REGISTER_RESET, FPGA_REG_REGISTER_RESET);
        if (0 != status)
        {
            skiq_error("Failed to put registers into reset on card %" PRIu8 " (status %"
                PRIi32 ")\n", card, status);
        }
        else
        {
            status = sidekiq_fpga_reg_RMWV(card, 0, REGISTER_RESET, FPGA_REG_REGISTER_RESET);
            if (0 != status)
            {
                skiq_error("Failed to get registers out of reset on card %" PRIu8 " (status %"
                    PRIi32 ")\n", card, status);
            }
        }
    }

    if ((0 != status) && (-ENOTSUP != status) && (-ENOSYS != status) && (-EBADMSG != status))
    {
        /* squash failed register transactions to -EBADMSG */
        status = -EBADMSG;
    }

    debug_print("card %" PRIu8 ": FPGA soft reset returned with status %" PRIi32 "\n",
        card, status);

    return status;
#undef FPGA_SOFT_RESET_VER_PATCH
#undef FPGA_SOFT_RESET_VER_MINOR
#undef FPGA_SOFT_RESET_VER_MAJOR
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
int32_t fpga_ctrl_is_carrier_edge_ref_clock_capable(uint8_t card, bool* p_carrier_capable)
{
    int32_t status = 0;
    uint32_t val = 0;
    
    // choose external clock select source [SMA|FPGA] based on the Carrier Edge capabilities bit (p. 6 of schematic)
    status = sidekiq_fpga_reg_read(card, FPGA_REG_CAPABILITIES, &val);
    if(status == 0)
    {
        *p_carrier_capable = RBF_GET(val, EXT_CLK_SUPPORT);
    }

    return status;
}
