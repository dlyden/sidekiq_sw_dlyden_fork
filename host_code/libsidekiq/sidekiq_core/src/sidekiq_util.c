/**
   @file    sidekiq_util.c
   @author  <author@epiq-solutions.com>
   @date    Mon May 18 16:27:08 CDT 2020

   @brief   Implementations of useful functions that have no place to live.

   <pre>
   Copyright 2020 Epiq Solutions, All Rights Reserved
   </pre>

*/

/***** INCLUDES *****/

#include "sidekiq_util.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
bool u8_is_not_in_list( uint8_t item,
                        uint8_t list[],
                        uint8_t nr_entries )
{
    bool is_not_in_list = true;
    uint8_t i;

    for ( i = 0; ( i < nr_entries ) && is_not_in_list; i++ )
    {
        if ( item == list[i] )
        {
            is_not_in_list = false;
        }
    }

    return is_not_in_list;
}


/**************************************************************************************************/
bool u64_is_not_in_list( uint64_t item,
                         uint64_t list[],
                         uint8_t nr_entries )
{
    bool is_not_in_list = true;
    uint8_t i;

    for ( i = 0; ( i < nr_entries ) && is_not_in_list; i++ )
    {
        if ( item == list[i] )
        {
            is_not_in_list = false;
        }
    }

    return is_not_in_list;
}
