/**
 * @file   sidekiq_api_factory.c
 * @author  <meaghan@epiq-solutions.com>
 * @date   2016-08-09 11:42:16
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

#include <sidekiq_api_factory.h>
#include <sidekiq_hal.h>
#include <sidekiq_usb_cmds.h>
#include <sidekiq_fpga.h>
#include <sidekiq_flash.h>
#include <sidekiq_private.h>
#include <sidekiq_rfic.h>
#include <sidekiq_card.h>
#include <rfic_x2.h>
#include <rfic_x4.h>
#include <rfic_adrv9002.h>
#include <card_services.h>
#include <hardware.h> // sidekiq_core/inc/hardware.h
#include <hw_iface.h>
#include <flash_defines.h>
#include <fpga_reg_hal.h>
#include <usb_interface.h>
#include <usb_private.h>
#include <sidekiq_card_mgr_private.h>
#include <bit_ops.h>
#include <tune.h>
#include <fpga_jesd_ctrl.h>
#include <libsidekiq_debug.h>

#include "calibration.h"

#include <ad9361_driver.h>

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <inttypes.h>
#include <limits.h>

#include "io_expander_cache.h"      /* for io_exp_initialize_cache() */
#include "io_z2.h"                  /* for skiq_fact_i2c_* functions */
#include "gpsdo_fpga.h"             /* for gpsdo_fpga_get_freq_accuracy() */
#include "rfe_m2_2280_ab.h"         /* for rfe_m2_2280_override_rx_lna_by_idx() */
#include "io_expander_pcal6524.h"   /* for nv100 devclk select testing */

/***** DEFINES *****/

#define TEST_SAMPLE_RATE (10000000) // 10Msps

#define GPIF_M2_BITMASK (0x2800)

/* AD9361/9364 addresses for use in PRBS test mode */
#define REG_LVDS_INVERT_CTRL1           (0x03D)
#define REG_LVDS_INVERT_CTRL2           (0x03E)
#define REG_BIST_CONFIG                 (0x3F4)

/* AD9361 addresses for use in RFIC_CTRL_IN test */

#define REG_RX1_MANUAL_GAIN            (0x109)
#define REG_RX2_MANUAL_GAIN            (0x10C)
#define RFIC_CTRL_IN_0_RX1_INC         (1)
#define RFIC_CTRL_IN_1_RX1_DEC         (1 << 1)
#define RFIC_CTRL_IN_2_RX2_INC         (1 << 2)
#define RFIC_CTRL_IN_3_RX2_DEC         (1 << 3)
#define RFIC_RX1_RX2_MGC_DEFAULT       (20)
#define RFIC_CTRL_PINS_ENABLE_OUTPUT   (0xF)

/* Length and offset for the external dev clock control on NV100 */
#define DEVCLK_SEL_N    PORT(0,4)


/***** STRUCTS *****/

typedef struct
{
    pthread_t thread;
    pthread_cond_t wait;
    pthread_mutex_t mutex;
    skiq_tx_block_t **p_tx_blocks;
    skiq_tx_hdl_t hdl;
    uint8_t card;
    uint32_t num_blocks;
    volatile uint32_t sent_blocks;
    bool is_thread_active;
    bool is_stop_requested;

} tx_test_samples_data_t;


typedef struct
{
    pthread_mutex_t mutex;
    bool is_running;
    bool is_stop_requested;

} prbs_state_t;


/***** LOCAL DATA *****/

static tx_test_samples_data_t _tx_test[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] =
    {
        .mutex = PTHREAD_MUTEX_INITIALIZER,
        .wait = PTHREAD_COND_INITIALIZER,
        .p_tx_blocks = NULL,
        .hdl = skiq_tx_hdl_end,
        .card = SKIQ_MAX_NUM_CARDS,
        .num_blocks = 0,
        .sent_blocks = 0,
        .is_thread_active = false,
        .is_stop_requested = false,
    },
};

static prbs_state_t prbs_state =
{
    .mutex = PTHREAD_MUTEX_INITIALIZER,
    .is_running = false,
    .is_stop_requested = false,
};

/***** LOCAL DATA *****/

/***** EXTERN DATA *****/

extern skiq_sys skiq;

extern const skiq_filt_t _x2_rev_a_rx_filters_with_preselect[];
extern const uint8_t _x2_rev_a_num_rx_filters_with_preselect;
extern const skiq_filt_t _x2_rev_a_rx_filters_no_preselect[];
extern const uint8_t _x2_rev_a_num_rx_filters_no_preselect;

extern const skiq_filt_t _x4_rev_c_rx_filters_with_preselect[];
extern const uint8_t _x4_rev_c_num_rx_filters_with_preselect;
extern const skiq_filt_t _x4_rev_c_rx_filters_no_preselect[];
extern const uint8_t _x4_rev_c_num_rx_filters_no_preselect;

static int32_t _skiq_fact_write_part_info( uint8_t card,
                                           char *p_current_part_number,
                                           char *p_part_number,
                                           char *p_revision,
                                           char *p_variant,
                                           char *p_serial );
static int32_t _skiq_fact_write_serial_string(uint8_t card,
                                              char* p_serial_num,
                                              bool is_legacy);
static int32_t _disable_eeprom_wp_for_part( uint8_t card,
                                            char *p_part_number,
                                            char *p_revision,
                                            bool *p_has_wp );

/*****************************************************************************/
/** This function prints differences between two data blocks of size 'length'

    @param[in] p_data1 a reference to the first block of raw bytes
    @param[in] p_data2 a reference to the second block of raw bytes
    @param[in] length: the number of bytes referenced by p_data1 and p_data2
*/
static void diff_hex_dump(uint8_t* p_data1, uint8_t *p_data2, uint32_t length)
{
    uint32_t i, j;
    const int buflen = 256;
    char buffer[buflen];
    int n = 0;

    for (i = 0; i < length; i += 16)
    {
        /* reset n */
        n = 0;

        /* print offset */
        n += snprintf(buffer + n, buflen - n, "%06X:", i);

        /* print HEX */
        for (j = 0; j < 16; j++)
        {
            if ( ( j % 2 ) == 0 )
            {
                n += snprintf(buffer + n, buflen - n, " ");
            }
            if ( i + j < length )
            {
                if ( p_data1[i + j] == p_data2[i + j] )
                {
                    n += snprintf(buffer + n, buflen - n, " %02X    ", p_data1[i + j]);
                }
                else
                {
                    n += snprintf(buffer + n, buflen - n, " %02X<>%02X", p_data1[i + j],
                                  p_data2[i + j]);
                }
            }
            else
            {
                n += snprintf(buffer + n, buflen - n, "      ");
            }
        }
        skiq_info("%s\n", buffer);
    }
}

/**
    @brief  Validate that an address + offset doesn't exceed the EEPROM address space

    @param[in]  base_addr   The base EEPROM address
    @param[in]  offset      The offset from @p baseAddr

    @note   This function currently assumes that the EEPROM address space is 16-bit (as that's
            what is currently implicitly used by the HAL EEPROM functions); this function needs
            to change if this is no longer true.

    @return true if the new address (@p baseAddr + @p offset) doesn't exceed the EEPROM address
            space, else false
*/
static bool verify_eeprom_address_offset(uint16_t base_addr, uint16_t offset)
{
    uint32_t address = (uint32_t) base_addr + (uint32_t) offset;

    /* Return true if the address is valid (e.g. doesn't exceed a 16-bit address space) */
    return (address < UINT16_MAX);
}


/***** GLOBAL FUNCTIONS *****/

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
static int32_t _skiq_fact_write_serial_string(uint8_t card,
                                           char* p_serial_num,
                                           bool is_legacy)
{
    int32_t status = 0;

    // Buffer used for I2C writes. We send 4 bytes at a time.
    uint8_t p_buf[] = {0, 0, 0, 0};
    // Initialize to 0xFF, this is the "unprogrammed" state in EEPROM.
    uint8_t p_alphanumeric[] = {0xFF, 0xFF, 0xFF, 0xFF};
    // Create an invalidate array to enable switching from one version to another.
    uint8_t p_invalidate[] = {0xFF, 0xFF, 0xFF, 0xFF};
    // Used to determine length of bytes used to invalidate.
    uint8_t invalidate_len = 0;
    uint16_t legacy = INVALID_SERIAL_NUM;
    uint16_t eeprom_addr = 0;
    uint16_t eeprom_invalid_addr = 0;
    uint32_t n = 0;

    n = strnlen(p_serial_num, SKIQ_FACT_SERIAL_NUM_STRING_MAX_MEM);

    if( (true == is_legacy) && (SKIQ_FACT_SERIAL_NUM_LEGACY_LEN == n) )
    {
        // Legacy serial number is an unsigned 16 bit integer value.
        legacy = strtol(p_serial_num, NULL, 0);
        if( (legacy > SKIQ_FACT_SERIAL_NUM_LEGACY_MAX ) ||
            (legacy < SKIQ_FACT_SERIAL_NUM_LEGACY_MIN) )
        {
            skiq_error("Invalid legacy serial '%u' on card %" PRIu8 "\n", legacy, card);
            return -3;
        }
    }
    else if( (false == is_legacy) && (SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN == n) )
    {
        // Make sure all characters are alphanumeric.
        for( n = 0; n < SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN; n++ )
        {
            if( 0 == isalnum(p_serial_num[n]) )
            {
                // allow 0xFF (which forces EEPROM to be wiped)
                if( (uint8_t) p_serial_num[n] != 0xFF )
                {
                    // Garbage serial number....
                    skiq_error("Encountered invalid serial character 0x%02" PRIx8 "\n",
                        (uint8_t) p_serial_num[n]);
                    status = -4;
                    goto skiq_fact_write_serial_string_exit;
                }
            }
            p_alphanumeric[n] = p_serial_num[n];
        }
    }
    else
    {
        skiq_error("Invalid serial option!\n");
        // Invalid option/serial number.
        status = -5;
        goto skiq_fact_write_serial_string_exit;
    }

    if( is_legacy == false )
    {
        // Alphanumeric serial number is stored little endian.
        eeprom_addr = VERSION_INFO_ALPHANUMERIC_ADDR;
        eeprom_invalid_addr = VERSION_INFO_LEGACY_ADDR;
        invalidate_len = sizeof(legacy);

        p_buf[0] = p_alphanumeric[3];
        p_buf[1] = p_alphanumeric[2];
        p_buf[2] = p_alphanumeric[1];
        p_buf[3] = p_alphanumeric[0];
        skiq_info("Write non-legacy serial number to addr 0x%04x, 0x%02x 0x%02x 0x%02x 0x%02x\n",
                  eeprom_addr, p_buf[0], p_buf[1], p_buf[2], p_buf[3]);
        if( (status = hal_write_eeprom(card, eeprom_addr, p_buf, SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN)) != 0 )
        {
            skiq_error("Failed to write EEPROM on card %" PRIu8 " with error code %" PRIu32 "\n", card, status);
            status = -6;
            goto skiq_fact_write_serial_string_exit;
        }
    }
    else
    {
        eeprom_addr = VERSION_INFO_LEGACY_ADDR;
        eeprom_invalid_addr = VERSION_INFO_ALPHANUMERIC_ADDR;
        invalidate_len = ARRAY_SIZE(p_invalidate);

        p_buf[0] = (legacy >> 0) & 0xFF;
        p_buf[1] = (legacy >> 8) & 0xFF;
        skiq_info("Write legacy serial number to addr 0x%04x -> 0x%02x 0x%02x\n", eeprom_addr,
                  p_buf[0], p_buf[1]);
        if( (status = hal_write_eeprom(card, eeprom_addr, p_buf, SKIQ_FACT_SERIAL_NUM_LEGACY_BYTE_LEN)) != 0 )
        {
            skiq_error("Failed to write EEPROM on card %" PRIu8 " with error code %" PRIu32 "\n", card, status);
            status = -7;
            goto skiq_fact_write_serial_string_exit;
        }
    }

    if( (status = hal_write_eeprom(card, eeprom_invalid_addr, p_invalidate, invalidate_len)) != 0 )
    {
        skiq_error("Failed to invalidate previous serial number on card %" PRIu8 " with error code %" PRIu32 "\n", card, status);
        status = -8;
        goto skiq_fact_write_serial_string_exit;
    }
skiq_fact_write_serial_string_exit:
    return status;
}

int32_t skiq_fact_write_serial_string(uint8_t card,
                                           char* p_serial_num,
                                           bool is_legacy)
{
    return (_skiq_fact_write_serial_string(card,
                                          p_serial_num,
                                          is_legacy));
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_clear_firmware_stamp( uint8_t card )
{
    int32_t status = 0;
    uint16_t blank_version = 0xFFFF;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    status = hal_write_eeprom( card, FIRMWARE_VERSION_INFO_ADDR, (uint8_t *)&blank_version,
                               sizeof( blank_version ) );

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_ref_clock( uint8_t card, skiq_ref_clock_select_t ref_clock )
{
    int32_t status=0;
    skiq_hw_vers_t hw_vers;
    uint8_t buf=0;
    uint8_t index=0;
    skiq_part_t part;

    part = _skiq_get_part(card);

    if( (ref_clock != skiq_ref_clock_internal) &&
        (ref_clock != skiq_ref_clock_external) &&
        (ref_clock != skiq_ref_clock_host) &&
        (ref_clock != skiq_ref_clock_carrier_edge))
    {
        skiq_error("Invalid reference clock configuration specified for card %" PRIu8 "\n", card);
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        status = skiq_read_hw_version(card, &hw_vers);
        if ( status != 0 )
        {
            skiq_error("Unable to read hardware version for card %" PRIu8 " (status=%" PRId32
                       ")\n", card, status);
        }
    }

    if( status == 0 )
    {
        if( (hw_vers == skiq_hw_vers_mpcie_c) || (hw_vers == skiq_hw_vers_mpcie_d) ||
            (hw_vers == skiq_hw_vers_mpcie_e) )
        {
            // we need to translate from card to UID to index
            status = usb_get_index(card, &index);
            if ( status == 0 )
            {
                status = usb_write_ref_clock(index, ref_clock);
            }
        }
        // For M.2 and M.2-2280, we can have either internal or external clock, for X2, X4, Z2, and
        // NV100, we can also have a Host ref clock
        else if( (part == skiq_m2) ||
                 (part == skiq_x2) ||
                 (part == skiq_z2) ||
                 (part == skiq_x4) ||
                 (part == skiq_m2_2280) ||
                 (part == skiq_z3u) ||
                 (part == skiq_nv100) )
        {
            if( ref_clock == skiq_ref_clock_internal )
            {
                buf = REFERENCE_CLOCK_INTERNAL;
            }
            // additional ref clock mode supported for X2/X4/Z2/NV100 only
            else if( (ref_clock == skiq_ref_clock_host) &&
                     ((part == skiq_z2) ||
                      (part == skiq_x2) ||
                      (part == skiq_x4) ||
                      (part == skiq_nv100) ) )
            {
                buf = REFERENCE_CLOCK_HOST;
            }
            else if((ref_clock == skiq_ref_clock_carrier_edge) &&
                    (part == skiq_x4))
            {
                bool carrier_edge_capable = false;
                status = fpga_ctrl_is_carrier_edge_ref_clock_capable(card, &carrier_edge_capable);
                if(carrier_edge_capable != 0)
                {
                    buf = REFERENCE_CLOCK_CARRIER_EDGE;
                }
                else
                {
                    skiq_error("Invalid reference clock selection, card %u does not support carrier edge reference clock\n", card);
                    status = -EINVAL;
                }
            }
            else
            {
                buf = REFERENCE_CLOCK_EXTERNAL;
            }
            if(status == 0)
            {
                // write the value out to EEPROM
                status = card_write_eeprom( card,
                                            REFERENCE_CLOCK_INFO_ADDR,
                                            &buf,
                                            REFERENCE_CLOCK_INFO_LENGTH );
            }
        }
        else if( (part == skiq_z2p) && (ref_clock != skiq_ref_clock_internal) )
        {
            status = -ENOTSUP;
            skiq_error("Only internal reference clock is supported for product");
        }
        else
        {
            /* not supported */
            skiq_error("Reference clock configuration not supported for this hardware revision!\n");
            status = -ENOTSUP;
        }
    }

    if ( status == 0 )
    {
        _skiq_save_ref_clock( card, ref_clock );
    }

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_ext_ref_clock_freq( uint8_t card, uint32_t freq )
{
    int32_t status = -EINVAL;
    skiq_part_t part = _skiq_get_part(card);

    if( (skiq_m2 != part) &&
        (skiq_mpcie != part) &&
        (skiq_m2_2280 != part) &&
        (skiq_z3u != part) &&
        (skiq_x4 != part) &&
        (skiq_nv100 != part) )
    {
        skiq_error("External clock frequency not configurable for Sidekiq %s\n", part_cstr(part));
        status = -ENOTSUP;
    }
    else
    {
        switch (freq)
        {
            case SIGNAL_40MHZ:
                /* all Sidekiqs support 40M */
                status = 0;
                break;

            case SIGNAL_30_72MHZ:
                /* all AD9361-based Sidekiqs support 30.72M */
                if ( ( part == skiq_m2 ) ||
                     ( part == skiq_mpcie ) ||
                     ( part == skiq_m2_2280 ) ||
                     ( part == skiq_z3u ) )
                {
                    status = 0;
                }
                break;

            case SIGNAL_10MHZ:
                /* The following can accept an external 10M reference */
                if ( ( part == skiq_x4 ) ||
                     ( part == skiq_nv100 ) || 
                     ( part == skiq_z3u ) ||
                     ( part == skiq_m2_2280 ) )
                {
                    status = 0;
                }
                break;

            case SIGNAL_100MHZ:
                /* Sidekiq X4 can accept 100M */
                if ( part == skiq_x4 )
                {
                    status = 0;
                }
                break;

            default:
                status = -ENOTSUP;
                skiq_error("Specified external clock frequency of %" PRIu32 " Hz is not supported "
                           "by any known product\n", freq);
                break;
        }

        if ( status == -EINVAL )
        {
            skiq_error("Specified external clock frequency of %" PRIu32 " Hz is not valid on "
                       "Sidekiq %s\n", freq, part_cstr( part ));
        }
    }

    // if everything is good, actually write it
    if ( status == 0 )
    {
        status = card_write_ext_ref_clock_freq(card, freq);
    }

    return status;
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_product_and_hw_vers( uint8_t card,
                                             skiq_hw_vers_t hw_vers,
                                             skiq_product_t product )
{
    int32_t status=0;
    uint8_t index=0;
    uint16_t vers;

    if( (status=card_map_version(hw_vers,
                                 product,
                                 &vers)) == 0 )
    {
        if( (status=usb_get_index(card, &index)) == 0 )
        {
            if( (status=usb_write_board_num(index, vers)) != 0 )
            {
                _skiq_log(SKIQ_LOG_ERROR, "failed to write hardware version, status %d\n",status);
            }
        }
    }

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_default_tcvcxo_warp_voltage( uint8_t card,
                                                     uint16_t warp_voltage )
{
    return card_write_warp_voltage_calibration(card,
                                               DEFAULT_WARP_VOLTAGE_OFFSET,
                                               warp_voltage);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_gpio_tristate( uint8_t card, uint32_t tristate )
{
    return sidekiq_fpga_reg_write(card, FPGA_REG_GPIO_TRISTATE, tristate);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_gpio_tristate( uint8_t card, uint32_t* p_tristate )
{
    return sidekiq_fpga_reg_read(card, FPGA_REG_GPIO_TRISTATE, p_tristate);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_gpio_state( uint8_t card, uint32_t state )
{
    return sidekiq_fpga_reg_write(card, FPGA_REG_GPIO_WRITE, state);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_gpio_state( uint8_t card, uint32_t* p_state )
{
    return sidekiq_fpga_reg_read(card, FPGA_REG_GPIO_READ, p_state);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_test_gpif(uint8_t card,
                              bool is_fpga_write,
                              uint16_t data_write,
                              uint16_t* p_data_read)
{
    skiq_hw_vers_t hw_vers = skiq_hw_vers_invalid;
    int32_t status = 0;
    uint32_t tmp = 0;
    uint8_t index=0;

    status = skiq_read_hw_version(card, &hw_vers);
    if( 0 != status )
    {
        return (-1);
    }

    if( hw_vers == skiq_hw_vers_reserved )
    {
        _skiq_log(SKIQ_LOG_ERROR, "GPIF test not supported for this hardware revision");
        return (-2);
    }

    if( true == is_fpga_write )
    {
        // we need to translate from card to UID to index
        if( (status=usb_get_index(card, &index)) != 0 )
        {
            goto skiq_fact_test_gpif_exit;
        }
        // FPGA is driving the interface...configure USB for read
        status = usb_init_gpif_test(index, false);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // set FPGA to drive it
        status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_TRISTATE, 0xFFFF);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // for m2, we can't modify LED or INIT_B (GPIF13 and GPIF11)
        if( (hw_vers == skiq_hw_vers_m2_b) || (hw_vers == skiq_hw_vers_m2_c) ||
            (hw_vers == skiq_hw_vers_m2_d) )
        {
            data_write |= GPIF_M2_BITMASK;
        }

        tmp = data_write;
        status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_WRITE, tmp);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // read the value from the USB chip
        status = usb_gpif_test_read(index, p_data_read);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // for m2, just manually set LED and INIT_B on readback since FPGA
        // doesn't have ctrl
        if( (hw_vers == skiq_hw_vers_m2_b) || (hw_vers == skiq_hw_vers_m2_c) ||
            (hw_vers == skiq_hw_vers_m2_d) )
        {
            *p_data_read |= GPIF_M2_BITMASK;
        }
    }
    else
    {
        // USB is driving the interface...configure FPGA as input
        status = sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_TRISTATE, 0);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // set USB to drive it
        status = usb_init_gpif_test(index, true);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // for m2, we can't modify LED or INIT_B (GPIF13 and GPIF11)
        if( (hw_vers == skiq_hw_vers_m2_b) || (hw_vers == skiq_hw_vers_m2_c) ||
            (hw_vers == skiq_hw_vers_m2_d) )
        {
            data_write |= GPIF_M2_BITMASK;
        }

        // output it from USB
        status = usb_gpif_test_write(index, (uint16_t)(data_write));
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }

        // read the value from the FPGA
        status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIF_READ, &tmp);
        if( 0 != status )
        {
            goto skiq_fact_test_gpif_exit;
        }
        *p_data_read = (uint16_t) tmp;
    }

skiq_fact_test_gpif_exit:
    // If we errored out, attempt to put the Sidekiq back into a known state.
    sidekiq_fpga_reg_write(card, FPGA_REG_GPIF_TRISTATE, 0);
    usb_exit_gpif_test(index);

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_reset_rfic( uint8_t card )
{
    int32_t status = 0;
    int32_t error = 0;
    uint32_t tmp = 0;

    status = sidekiq_fpga_reg_read(card, FPGA_REG_TIMESTAMP_RST, &tmp);
    if( 0 != status )
    {
        return -1;
    }

    tmp |= (1 << RFIC_RST_OFFSET);
    status = sidekiq_fpga_reg_write(card, FPGA_REG_TIMESTAMP_RST, tmp);
    if( 0 != status )
    {
        // latch the error and attempt to recover through next write
        error = status;
    }

    // arbitrary short sleep just to be safe
    hal_millisleep(100);

    tmp &= ~(1 << RFIC_RST_OFFSET);
    status = sidekiq_fpga_reg_write(card, FPGA_REG_TIMESTAMP_RST, tmp);
    if( 0 != status )
    {
        error = status;
    }

    return error;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_io_expander( uint8_t card, uint8_t reg, uint8_t data )
{
    skiq_hw_vers_t hw_vers = skiq_hw_vers_invalid;
    uint8_t buf[2] = {0, 0};
    int32_t status = 0;

    // We can only set the IO expander on MPCIE Rev C and D boards.
    status = skiq_read_hw_version(card, &hw_vers);
    if( (0 != status) ||
        ( (skiq_hw_vers_mpcie_c != hw_vers) &&
          (skiq_hw_vers_mpcie_d != hw_vers) &&
          (skiq_hw_vers_mpcie_e != hw_vers) ))
    {
        return -1;
    }

    buf[0] = reg;
    buf[1] = data;
    return fpga_reg_hal_write_i2c(card, IO_EXPANDER_I2C_ADDRESS, buf, 2);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_io_expander( uint8_t card, uint8_t reg, uint8_t* p_data )
{
    skiq_hw_vers_t hw_vers = skiq_hw_vers_invalid;
    int32_t status = 0;

    // We can only reset the RFIC on MPCIE rev C and D Sidekiq cards.
    status = skiq_read_hw_version(card, &hw_vers);
    if( (0 != status) ||
        ( (skiq_hw_vers_mpcie_c != hw_vers) &&
          (skiq_hw_vers_mpcie_d != hw_vers) &&
          (skiq_hw_vers_mpcie_e != hw_vers) ))
    {
        return -1;
    }

    status = fpga_reg_hal_write_i2c(card, IO_EXPANDER_I2C_ADDRESS, &reg, 1);
    if( 0 != status )
    {
        return -1;
    }

    return fpga_reg_hal_read_i2c(card, IO_EXPANDER_I2C_ADDRESS, p_data, 1);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_save_golden_fpga_to_flash( uint8_t card, FILE* p_file )
{
    uint32_t address = 0;
    int32_t status = 0;

    status = flash_get_golden_fpga_addr( card, &address );
    if (0 != status)
    {
        skiq_error("Failed to get golden FPGA address for card %" PRIu8 " (status = %" PRIi32 ")\n",
            card, status);
    }
    else
    {
        status = flash_write_file_fpga(card, address, p_file);
    }

    return status;
}


/**************************************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_verify_golden_fpga_from_flash( uint8_t card,
                                                 FILE* p_file )
{
    uint32_t address = 0;
    int32_t status = 0;

    status = flash_get_golden_fpga_addr( card, &address);
    if (0 != status)
    {
        skiq_error("Failed to get golden FPGA address for card %" PRIu8 " (status = %" PRIi32 ")\n",
            card, status);
    }
    else
    {
        status = flash_verify_file(card, address, p_file);
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_rf_power_state( uint8_t card, bool power_enable )
{
    int32_t status = 0;

    if( false == power_enable )
    {
        // Power down the RF hardware of Sidekiq.
        status = ad9361_enter_sleep_mode(card);
    }
    else
    {
        // Power up the RF hardware of Sidekiq.
        // NOT SUPPORTED!
        status = -1;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_flash( uint8_t card,
                              uint32_t address,
                              uint32_t len,
                              uint8_t* p_data )
{
    return flash_read(card, address, p_data, len);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_fpga_temp(uint8_t card, int8_t* p_temp_in_deg_C)
{
    int32_t status;

    status = card_read_fpga_temp(card, p_temp_in_deg_C);

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_temp_sensor( uint8_t card,
                                    uint8_t sensor,
                                    int8_t* p_temp_in_deg_C )
{
    return card_read_temp( card, sensor, p_temp_in_deg_C );
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_card_mgr_destroy(void)
{
    int32_t status=0;

    // make sure exit is called
    skiq_exit();

    // now destroy the card_mgr
    card_mgr_destroy_card_mgr();

    return (status);
}

int32_t _disable_eeprom_wp_for_part( uint8_t card,
                                     char *p_part_number,
                                     char *p_revision,
                                     bool *p_has_wp )
{
    int32_t status=0;
    skiq_part_t skiq_part;
    skiq_hw_rev_t skiq_hw_rev;

    *p_has_wp = false;

    // Determine skiq part mapping
    card_map_rev_and_part( skiq_hw_vers_reserved,
                           p_part_number,
                           p_revision,
                           &skiq_part, &skiq_hw_rev );

    /* some of the card/eeprom_wp functions call _skiq_get_part(), so we need to fill out part_type
     * here, based on what card_map_rev_and_part() thinks, in order to have them resolve the part
     * correctly and enable/disable write protection */
    skiq.card[card].card_params.part_type = skiq_part;

    *p_has_wp = card_eeprom_wp_avail( skiq_part );

    // this will give us a mutex but not the hardware write protect (if avail)
    card_eeprom_lock_and_disable_wp(card);

    // we know that that part we're trying to program has wp but sidekiq doesn't know it yet...
    // so we need to manually disable the write protect
    if( *p_has_wp == true )
    {
        status = card_set_eeprom_wp( card, false );
        if ( status == 0 )
        {
            skiq_info("Successfully disabled EEPROM WP\n");
        }
    }

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
int32_t skiq_fact_wipe_part_info( uint8_t card,
                                  char *p_part_number,
                                  char *p_revision )
{
    int32_t status=0;
    bool is_wp=false;
    char blank_eeprom_pn[SKIQ_PART_NUM_STRLEN] = { [0 ... (SKIQ_PART_NUM_STRLEN-1)] = 0xFF };
    char blank_eeprom_rev[SKIQ_REVISION_STRLEN] = { [0 ... (SKIQ_REVISION_STRLEN-1)] = 0xFF };
    char blank_eeprom_var[SKIQ_VARIANT_STRLEN] = { [0 ... (SKIQ_VARIANT_STRLEN-1)] = 0xFF };
    char blank_eeprom_sn[SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN+1] =
        { [0 ... (SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN)] = 0xFF };

    blank_eeprom_pn[SKIQ_PART_NUM_STRLEN-1] = '\0';
    blank_eeprom_rev[SKIQ_REVISION_STRLEN-1] = '\0';
    blank_eeprom_var[SKIQ_VARIANT_STRLEN-1] = '\0';
    blank_eeprom_sn[SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN] = '\0';

    status = _disable_eeprom_wp_for_part( card, p_part_number, p_revision, &is_wp );
    if( status == 0 )
    {
        status = _skiq_fact_write_part_info( card,
                                             p_part_number,
                                             blank_eeprom_pn,
                                             blank_eeprom_rev,
                                             blank_eeprom_var,
                                             blank_eeprom_sn );
    }

    if( is_wp == true )
    {
        if( card_set_eeprom_wp( card, true ) == 0 )
        {
            skiq_info("Successfully re-enabled EEPROM WP\n");
        }
    }

    // release the mutex
    card_eeprom_unlock_and_enable_wp(card);

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_part_info( uint8_t card,
                                   char *p_part_number,
                                   char *p_revision,
                                   char *p_variant,
                                   char *p_serial )
{
    int32_t status=0;
    int32_t tmp_status = 0;
    bool is_wp=false;

    skiq_info("Storing part %s revision %s variant %s on card %" PRIu8 "\n", p_part_number,
        p_revision, p_variant, card);

    status = _disable_eeprom_wp_for_part( card, p_part_number, p_revision, &is_wp );
    if( status == 0 )
    {
        status = _skiq_fact_write_part_info( card,
                                             p_part_number,
                                             p_part_number,
                                             p_revision,
                                             p_variant,
                                             p_serial );
    }

    if( is_wp == true )
    {
        tmp_status = card_set_eeprom_wp( card, true );
        if( tmp_status == 0 )
        {
            skiq_info("Successfully re-enabled EEPROM WP on card %" PRIu8 "\n", card);
        }
        else
        {
            skiq_warning("Failed to re-enable EEPROM WP (status = %" PRIi32 ") on card %"
                PRIu8 "\n", tmp_status, card);
        }
    }
    // release the mutex
    card_eeprom_unlock_and_enable_wp(card);

    return (status);
}

/**************************************************************************************************/
/** The _skiq_fact_write_part_info() function is responsible for writing the part information to
    EEPROM.  It is expected that the EEPROM WP is controlled outside of this function.

    @param[in] card                     card number of the Sidekiq of interest
    @param[in] p_current_part_number    pointer to the current part number string (used when the
                                        card can't be identified, likely due to missing info in
                                        the EEPROM)
    @param[in] p_part_number            pointer to the part number to store
    @param[in] p_revision               pointer to the revision to store
    @param[in] p_variant                pointer to the variant to store
    @param[in] p_serial                 pointer to string representation of the serial number

    @return int32_t  status where 0=success, anything else is an error
*/
static int32_t _skiq_fact_write_part_info( uint8_t card,
                                           char *p_current_part_number,
                                           char *p_part_number,
                                           char *p_revision,
                                           char *p_variant,
                                           char *p_serial )
{
    int32_t status = 0;
    uint16_t pcb_product = 0;
    uint16_t base_address = EEPROM_EXT_PRODUCT_INFO_BASE_ADDR;
    skiq_part_t part = skiq_part_invalid;
    skiq_hw_rev_t hw_rev = hw_rev_invalid;
    skiq_product_t product = skiq_product_invalid;
    uint16_t part_offset = 0;
    uint16_t revision_offset = 0;
    uint16_t variant_offset = 0;

    if ( SKIQ_PART_NUM_STRLEN == strnlen(p_part_number, SKIQ_PART_NUM_STRLEN) )
    {
        skiq_error("Part number string provided '%s' is too big for slot on card %u\n",
                   p_part_number, card);
        return -EINVAL;
    }

    if ( SKIQ_REVISION_STRLEN == strnlen(p_revision, SKIQ_REVISION_STRLEN) )
    {
        skiq_error("Revision string provided '%s' is too big for slot on card %u\n", p_revision,
                   card);
        return -EINVAL;
    }

    if ( SKIQ_VARIANT_STRLEN == strnlen(p_variant, SKIQ_VARIANT_STRLEN) )
    {
        skiq_error("Variant string provided '%s' is too big for slot on card %u\n", p_variant,
                   card);
        return -EINVAL;
    }

    /* Convert the arguments into their corresponding constants */
    part = card_map_part(p_current_part_number);
    product = card_map_part_to_product(p_current_part_number);
    if( (uint8_t) p_revision[0] != 0xFF )
    {
        /* Don't worry about mapping the revision if this function is wiping the card info */
        hw_rev = card_map_hw_rev(p_revision);
    }

    /* Adjust the "extended part information" EEPROM base address if this is an mPCIe or m.2 card */
    if( (part == skiq_mpcie) || (part == skiq_m2) )
    {
        base_address = EEPROM_EXT_PRODUCT_INFO_MPCIE_M2_BASE_ADDR;
    }

    /* Validate the EEPROM addresses to write before using them */
    if( status == 0 )
    {
        if( !verify_eeprom_address_offset(base_address, EEPROM_PART_NUM_OFFSET) )
        {
            skiq_error("Invalid part number EEPROM address (0x%" PRIx32 ") on card %" PRIu8 "\n",
                ((uint32_t) base_address + (uint32_t) EEPROM_PART_NUM_OFFSET), card);
            status = -EFAULT;
        }
        else
        {
            part_offset = (uint16_t) (base_address + (uint16_t) EEPROM_PART_NUM_OFFSET);
        }
    }
    if( status == 0 )
    {
        if( !verify_eeprom_address_offset(base_address, EEPROM_REVISION_OFFSET) )
        {
            skiq_error("Invalid revision EEPROM address (0x%" PRIx32 ") on card %" PRIu8 "\n",
                ((uint32_t) base_address + (uint32_t) EEPROM_REVISION_OFFSET), card);
            status = -EFAULT;
        }
        else
        {
            revision_offset = (uint16_t) (base_address + (uint16_t) EEPROM_REVISION_OFFSET);
        }
    }
    if( status == 0 )
    {
        if( !verify_eeprom_address_offset(base_address, EEPROM_VARIANT_OFFSET) )
        {
            skiq_error("Invalid variant EEPROM address (0x%" PRIx32 ") on card %" PRIu8 "\n",
                ((uint32_t) base_address + (uint32_t) EEPROM_VARIANT_OFFSET), card);
            status = -EFAULT;
        }
        else
        {
            variant_offset = (uint16_t) (base_address + (uint16_t) EEPROM_VARIANT_OFFSET);
        }
    }

    // it is expected that the EEPROM WP is controlled outside of this function

    if( status == 0 )
    {
        debug_print("Writing part number '%s' to EEPROM address 0x%" PRIx16 "\n", p_part_number,
            part_offset);

        status = hal_write_eeprom(card, part_offset, (uint8_t*)(p_part_number),
                    SKIQ_PART_NUM_STRLEN);
        if( status != 0 )
        {
            skiq_error("Failed to write part number on card %" PRIu8 " (status = %" PRIi32 ")\n",
                card, status);
        }
    }

    // revision
    if( status == 0 )
    {
        debug_print("Writing revision '%s' to EEPROM address 0x%" PRIx16 "\n", p_revision,
            revision_offset);

        status = hal_write_eeprom(card, revision_offset, (uint8_t*)(p_revision),
                    SKIQ_REVISION_STRLEN);
        if( status != 0 )
        {
            skiq_error("Failed to write revision on card %" PRIu8 " (status = %" PRIi32 ")\n",
                card, status);
        }
    }

    // variant
    if( status == 0 )
    {
        debug_print("Writing variant '%s' to EEPROM address 0x%" PRIx16 "\n", p_variant,
            variant_offset);

        status = hal_write_eeprom( card, variant_offset, (uint8_t*)(p_variant),
                    SKIQ_VARIANT_STRLEN );
        if( status != 0 )
        {
            skiq_error("Failed to write variant on card %" PRIu8 " (status = %" PRIi32 ")\n",
                card, status);
        }
    }

    // make sure the PCB_INFO is updated (note that this address does not exist in the
    // "extended product info" EEPROM section so the address does not need to be offset - at time
    // of writing)
    if( status == 0 )
    {
        // Default to "extended" by setting both fields to "reserved"
        skiq_hw_vers_t tmp_hw_version = skiq_hw_vers_reserved;
        skiq_product_t tmp_product = skiq_product_reserved;

        /*
            Don't write "reserved" in the legacy fields if this is an mPCIe rev D or E - instead,
            identify as the masquerading hardware version
        */
        if( (part == skiq_mpcie) && ((hw_rev == hw_rev_d) || (hw_rev == hw_rev_e)) )
        {
            tmp_hw_version = skiq_hw_vers_mpcie_masquerade;
            tmp_product = product;
        }
        else if ( (part == skiq_m2) && (hw_rev == hw_rev_d) )
        {
            tmp_hw_version = skiq_hw_vers_m2_masquerade;
            tmp_product = product;
        }

        status = card_map_version( tmp_hw_version, tmp_product, &pcb_product );
        if( status == 0 )
        {
            debug_print("Writing PCB_INFO 0x%" PRIx16 " to EEPROM address 0x%" PRIx16 "\n",
                pcb_product, (uint16_t) PCB_INFO_ADDR);

            status = hal_write_eeprom( card, PCB_INFO_ADDR, (uint8_t*)(&pcb_product), 2 );
            if( status != 0 )
            {
                skiq_error("Failed to write PCB information on card %" PRIu8 " (status = %"
                    PRIi32 ")\n", card, status);
            }
        }
        else
        {
            skiq_error("Failed to map hardware & product version number on card %" PRIu8
                " (status %" PRIi32 "); not writing serial number to EEPROM.\n", card, status);
        }
    }

    // now write the serial #
    if( status == 0 )
    {
        status = _skiq_fact_write_serial_string( card, p_serial, false );
        if( status != 0 )
        {
            skiq_error("Failed to write serial string on card %" PRIu8 " (status = %" PRIi32 ")\n",
                card, status);
        }
    }

    if( status == 0 )
    {
        skiq_info("Successfully stored part info on card %" PRIu8 "!\n", card);
    }

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_rx_preselect_filter_config( uint8_t card,
                                                    skiq_rx_hdl_t hdl,
                                                    skiq_rx_filter_config_t config )
{
    int32_t status = 0;
    const skiq_filt_t *p_filters = NULL;
    uint8_t num_filters=0;
    skiq_part_t part = _skiq_get_part(card);

    switch ( part )
    {
        #if (defined HAS_X2_SUPPORT)
            case skiq_x2:
            if( config == skiq_rx_filter_config_1 )
            {
                p_filters = _x2_rev_a_rx_filters_with_preselect;
                num_filters = _x2_rev_a_num_rx_filters_with_preselect;
            }
            else if( config == skiq_rx_filter_config_2 )
            {
                p_filters = _x2_rev_a_rx_filters_no_preselect;
                num_filters = _x2_rev_a_num_rx_filters_no_preselect;
            }
            else
            {
                _skiq_log(SKIQ_LOG_ERROR, "Invalid filter configuration provided on card %" PRIu8 ""
                          " (status %" PRIi32 ")\n", card, status);
                status = -EINVAL;
            }
            break;
        #endif  /* HAS_X4_SUPPORT */


        #if (defined HAS_X4_SUPPORT)
            case skiq_x4:
            if( config == skiq_rx_filter_config_3 )
            {
                p_filters = _x4_rev_c_rx_filters_with_preselect;
                num_filters = _x4_rev_c_num_rx_filters_with_preselect;
            }
            else if( config == skiq_rx_filter_config_4 )
            {
                p_filters = _x4_rev_c_rx_filters_no_preselect;
                num_filters = _x4_rev_c_num_rx_filters_no_preselect;
            }
            else
            {
                _skiq_log(SKIQ_LOG_ERROR, "Invalid filter configuration provided on card %" PRIu8 ""
                          " (status %" PRIi32 ")\n", card, status);
                status = -EINVAL;
            }
            break;
        #endif  /* HAS_X4_SUPPORT */


        default:
            status = -ENOTSUP;
            break;
    }
    if( status==0 )
    {
        status=_skiq_write_rx_preselect_filters( card, hdl, num_filters, p_filters );
    }

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_rx_preselect_filter_config( uint8_t card,
                                                   skiq_rx_hdl_t hdl,
                                                   skiq_rx_filter_config_t *p_config )
{
    int32_t status = 0;
    skiq_filt_t filters[skiq_filt_max];
    uint8_t num_filters=0;
    skiq_part_t part = _skiq_get_part(card);

    status = skiq_read_rx_filters_avail( card, hdl, filters, &num_filters );

    if (status == 0)
    {
        switch ( part )
        {
            #if (defined HAS_X2_SUPPORT)
            case skiq_x2:
                if( memcmp(filters, _x2_rev_a_rx_filters_with_preselect, num_filters*sizeof(skiq_filt_t)) == 0 )
                {
                    *p_config = skiq_rx_filter_config_1;
                }
                else if( memcmp(filters, _x2_rev_a_rx_filters_no_preselect, num_filters*sizeof(skiq_filt_t)) == 0 )
                {
                    *p_config = skiq_rx_filter_config_2;
                }
                else
                {
                    status = -EINVAL;
                    _skiq_log(SKIQ_LOG_ERROR, "Invalid filter configuration detected on card %" PRIu8 " (status %" PRIi32 ")\n",
                    card, status);
                }
                break;
            #endif
            #if (defined HAS_X4_SUPPORT)
            case skiq_x4:
                if( memcmp(filters, _x4_rev_c_rx_filters_with_preselect, num_filters*sizeof(skiq_filt_t)) == 0 )
                {
                    *p_config = skiq_rx_filter_config_3;
                }
                else if( memcmp(filters, _x4_rev_c_rx_filters_no_preselect, num_filters*sizeof(skiq_filt_t)) == 0 )
                {
                    *p_config = skiq_rx_filter_config_4;
                }
                else
                {
                    status = -EINVAL;
                    _skiq_log(SKIQ_LOG_ERROR, "Invalid filter configuration detected on card %" PRIu8 ""
                            "(status %" PRIi32 ")\n", card, status);
                }
                break;
            #endif
            default:
                status = -ENOTSUP;
                break;
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_usb_enumeration_delay( uint8_t card,
                                               uint16_t delay_ms )
{
    return card_write_usb_enumeration_delay(card, delay_ms);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_led( uint8_t card, skiq_led_color_t color, bool enable )
{
    int32_t status=-1;
    uint32_t offset=0;
    uint32_t len=0;
    uint32_t val=0;
    bool active_low=false;
    skiq_part_t part = _skiq_get_part(card);

    // only supported for X2/X4
    if( (skiq_x2 == part) ||
        (skiq_x4 == part) )
    {
        if( (status=sidekiq_fpga_reg_read(card, FPGA_REG_FMC_CTRL, &val)) == 0 )
        {
            status=0;
            switch( color )
            {
                case skiq_led_color_red:
                    offset = FMC_RED_LED_ENA_OFFSET;
                    len = FMC_RED_LED_ENA_LEN;
                    break;

                case skiq_led_color_green:
                    offset = FMC_GREEN_LED_ENA_OFFSET;
                    len = FMC_GREEN_LED_ENA_LEN;
                    break;

                case skiq_led_color_blue:
                    offset = FMC_BLUE_LED_ENA_OFFSET;
                    len = FMC_BLUE_LED_ENA_LEN;
                    active_low = true;
                    break;

                default:
                    status=-2;
                    break;
            }
            if( status == 0 )
            {
                if( ((enable==true) && (active_low==false)) ||
                    ((enable==false) && (active_low==true)) )
                {
                    BF_SET(val, 1, offset, len);
                }
                else
                {
                    BF_SET(val, 0, offset, len);
                }
                status = sidekiq_fpga_reg_write( card, FPGA_REG_FMC_CTRL, val);
            }
        }
    }

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_tx_pa( uint8_t card, skiq_tx_hdl_t hdl, bool enable )
{
    int32_t status=0;
    rfe_t rfe_instance;

    rfe_instance = _skiq_get_tx_rfe_instance( card, hdl );
    status = rfe_update_tx_pa( &rfe_instance, enable );

    return (status);
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_override_tx_pa( uint8_t card,
                                  skiq_tx_hdl_t tx_hdl,
                                  skiq_pin_value_t pin_value )
{
    int32_t status = -ENOTSUP;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_VALID_TX_HDL(card, tx_hdl);

    rfe_instance = _skiq_get_tx_rfe_instance( card, tx_hdl );
    status = rfe_override_tx_pa( &rfe_instance, pin_value );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_rx_lna( uint8_t card, skiq_rx_hdl_t hdl, bool enable )
{
    int32_t status=0;
    rfe_t rfe_instance;

    rfe_instance = _skiq_get_rx_rfe_instance( card, hdl );
    status = rfe_update_rx_lna( &rfe_instance, enable ? lna_enabled : lna_disabled );

    return (status);
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_bypass_rx_lna( uint8_t card,
                                 skiq_rx_hdl_t rx_hdl )
{
    int32_t status=0;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_VALID_RX_HDL(card, rx_hdl);

    rfe_instance = _skiq_get_rx_rfe_instance( card, rx_hdl );
    status = rfe_update_rx_lna( &rfe_instance, lna_bypassed );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_current_voltage( const uint8_t card,
                                        const uint8_t sensor,
                                        float *p_current,
                                        float *p_voltage )
{
    return (card_read_current_voltage(card, sensor, p_current, p_voltage));
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_mix_freq( uint8_t card,
                                  uint64_t freq )
{
    return tune_write_mix_freq(card, freq);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_mix_freq( uint8_t card,
                                 uint64_t *p_freq )
{
    tune_read_mix_freq(card, p_freq);

    return 0;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_genuine_golden_fpga_present_in_flash( uint8_t card,
                                                             uint8_t *p_present )
{
    bool present;
    skiq_part_t part;
    int32_t status = 0;
    uint32_t address = 0;

#define GENUINE_GOLDEN_FPGA_CHECK_OFFSET          (0x9D)
#define GENUINE_GOLDEN_FPGA_CHECK_VALUE           (0x12)

    status = flash_verify_golden_present(card, &present);
    if ( ( status == 0 ) && (present) )
    {
        part = _skiq_get_part(card);
        if ( part == skiq_mpcie )
        {
            uint8_t data_bytes[GENUINE_GOLDEN_FPGA_CHECK_OFFSET+1];

            status = flash_get_golden_fpga_addr( card, &address);
            if (0 != status)
            {
                skiq_error("Failed to get golden FPGA address for card %" PRIu8 " (status = %"
                    PRIi32 ")\n", card, status);
                return (status);
            }

            /* check at offset 0x9D for value 0x12 if skiq_mpcie */
            status = flash_read( card, address, data_bytes, GENUINE_GOLDEN_FPGA_CHECK_OFFSET+1 );
            if ( status == 0 )
            {
                if ( data_bytes[GENUINE_GOLDEN_FPGA_CHECK_OFFSET] == GENUINE_GOLDEN_FPGA_CHECK_VALUE )
                {
                    present = true;
                }
                else
                {
                    present = false;
                }
            }
        }
    }

    // verbose mapping of bool to uint8, just to be safe
    if ( status == 0 ) *p_present = (present) ? 1 : 0;

    return status;
}


static int32_t _increment_rx1_rx2_gain_by_pins( uint8_t card )
{
    int32_t status = 0;

    status = sidekiq_fpga_reg_RMWV( card, RFIC_CTRL_IN_0_RX1_INC | RFIC_CTRL_IN_2_RX2_INC,
                                    RFIC_CTRL_IN, FPGA_REG_GPIO_WRITE );
    if( 0 != status )
    {
        skiq_error("Error FPGA write: FPGA_REG_GPIO_WRITE with status: %d and card: %u\n",
                   status, card);
        status = -EIO;
    }

    if (status == 0)
    {
        /*  Must be > 2 ClkRF cycles, where ClkRF = Rx FIR input clock.
            From p39 AD9361 reference manual: UG-570:

            The pulse is asynchronous so setup and hold are not relevant but the time
            high and low must be at least two ClkRF cycles for the AD9361 to detect the
            event. ClkRF is the clock used at the input of the receive FIR filters.
            The RX FIR uses ADC_CLK/2:

            (1 / 20000000)  * 2 cycles = 100ns */

        hal_nanosleep(100);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV(card, 0, RFIC_CTRL_IN, FPGA_REG_GPIO_WRITE);
        if( 0 != status )
        {
            skiq_error("Error FPGA write: FPGA_REG_GPIO_WRITE with status: %d and card: %u\n",
                       status, card);
            status = -EIO;
        }
    }

    return status;
}

static int32_t _decrement_rx1_rx2_gain_by_pins( uint8_t card )
{
    int32_t status = 0;

    status = sidekiq_fpga_reg_RMWV(card, RFIC_CTRL_IN_1_RX1_DEC | RFIC_CTRL_IN_3_RX2_DEC,
                                   RFIC_CTRL_IN, FPGA_REG_GPIO_WRITE);
    if( 0 != status )
    {
        skiq_error("Error FPGA read: FPGA_REG_GPIO_WRITE with status: %d and card: %u\n",
                   status, card);
        status = -EIO;
    }

    if ( status == 0 )
    {
        /*  Must be > 2 ClkRF cycles, where ClkRF = Rx FIR input clock. */
        hal_nanosleep(100);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV(card, 0, RFIC_CTRL_IN, FPGA_REG_GPIO_WRITE);
        if( 0 != status )
        {
            skiq_error("Error FPGA write: FPGA_REG_GPIO_WRITE with status: %d and card: %u\n",
                       status, card);
            status = -EIO;
        }
    }

    return status;
}

static int32_t _read_gain_rx1_rx2(uint8_t card, uint8_t *rx1_gain, uint8_t *rx2_gain)
{
    int32_t status = 0;

    /* read gain for both channels  */
    status = ad9361_read_rx_gain( card, 1, rx1_gain );
    if( 0 != status )
    {
        skiq_error("Error RFIC read: Gain register rx1 with status: %d and card: %u\n",
                   status, card);
        status = -EIO;
    }

    if ( status == 0 )
    {
        status = ad9361_read_rx_gain( card, 2, rx2_gain );
        if( 0 != status )
        {
            skiq_error("Error RFIC read: Gain register rx2 with status: %d and card: %u\n",
                       status, card);
            status = -EIO;
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_test_rfic_ctrl( uint8_t card )
{
    skiq_part_t part = skiq_part_invalid;
    int32_t status = 0;
    bool increment_failed = false, decrement_failed = false;

    part = _skiq_get_part(card);
    if (( part == skiq_mpcie ) ||
        ( part == skiq_m2 ) ||
        ( part == skiq_z2 ) ||
        ( part == skiq_m2_2280 ) ||
        ( part == skiq_z2p ) ||
        ( part == skiq_z3u ))
    {
        uint8_t rx1_gain, rx2_gain, rx1_gain_old, rx2_gain_old = 0;
        rfic_t rfic;

        rfic = _skiq_get_generic_rfic_instance(card);

        /* enable both rx channels via the AD9361 registers directly regardless of part */
        status = ad9361_write_rx_enable_state(card, true, true);
        if( 0 != status )
        {
            skiq_error("Error: ad9361_write_rx_enable_state with status: %d and card: %u\n", status, card);
            return status;
        }

        /* write an initial value into the gain register rx1*/
        status = hal_rfic_write_reg(card, 0, REG_RX1_MANUAL_GAIN, RFIC_RX1_RX2_MGC_DEFAULT);
        if( 0 != status )
        {
            skiq_error("Error: REG_RX1_MANUAL_GAIN with status: %d and card: %u\n", status, card);
            return status;
        }

        /* write an initial value into the gain register rx2*/
        status = hal_rfic_write_reg(card, 0, REG_RX2_MANUAL_GAIN, RFIC_RX1_RX2_MGC_DEFAULT);
        if( 0 != status )
        {
            skiq_error("Error: REG_RX2_MANUAL_GAIN with status: %d and card: %u\n", status, card);
            return status;
        }

        status = sidekiq_fpga_reg_RMWV(card, 0, RFIC_CTRL_IN, FPGA_REG_GPIO_WRITE);
        if( 0 != status )
        {
            skiq_error("Error: FPGA_REG_GPIO_WRITE with status: %d and card: %u\n", status, card);
            return status;
        }

        /* This should be the same as I'm doing above */
        {
            uint32_t rfic_hop_gpio = 0;
            uint8_t chip_index = 0;

            rfic_hop_gpio = rfic_read_hop_on_ts_gpio( &rfic, &chip_index );
            fpga_ctrl_config_hop_on_timestamp( card, false, rfic_hop_gpio, chip_index);
        }

        /* Configure FPGA pins as outputs */
        status = sidekiq_fpga_reg_RMWV(card, RFIC_CTRL_PINS_ENABLE_OUTPUT,
                                       RFIC_CTRL_IN, FPGA_REG_GPIO_TRISTATE);
        if( 0 != status )
        {
            skiq_error("Error: FPGA_REG_GPIO_TRISTATE with status: %d and card: %u\n", status, card);
            return status;
        }

        /* enable the pin gain control */
        rfic = _skiq_get_generic_rfic_instance(card);
        status = rfic_enable_pin_gain_ctrl(&rfic);
        if( 0 != status )
        {
            skiq_error("Unable to configure RFIC for MGC with status: %d and card: %u\n", status, card);
            return status;
        }

        /* Start of test.  Read the initial gain values and see if they change */
        status = _read_gain_rx1_rx2(card, &rx1_gain_old, &rx2_gain_old);
        if( 0 != status )
        {
            skiq_error("Unable to read rx1 + rx2 gain with status: %d and card: %u\n", status, card);
            return status;
        }

        status = _increment_rx1_rx2_gain_by_pins(card);
        if( 0 != status )
        {
            skiq_error("Unable to increment rx1 + rx2 gain with status: %d and card: %u\n", status, card);
            return status;
        }

        status = _read_gain_rx1_rx2(card, &rx1_gain, &rx2_gain);
        if( 0 != status )
        {
            skiq_error("Unable to read rx1 + rx2 gain with status: %d and card: %u\n", status, card);
            return status;
        }

        skiq_info("RX1 Gain %X -> %X   RX2 Gain %X -> %X\n", rx1_gain_old, rx1_gain,
                  rx2_gain_old, rx2_gain );

        if ( ((rx1_gain_old + 1) != rx1_gain) || ((rx2_gain_old + 1 ) != rx2_gain) )
        {
            /* Test has failed, lines are not connected properly */
            skiq_error("Gain increment failed.  RX1 Gain %X -> %X   RX2 Gain %X -> %X\n",
                       rx1_gain_old, rx1_gain,
                       rx2_gain_old, rx2_gain);
            increment_failed = true;
        }

        rx1_gain_old = rx1_gain;
        rx2_gain_old = rx2_gain;

        status = _decrement_rx1_rx2_gain_by_pins(card);
        if( 0 != status )
        {
            skiq_error("Unable to decrement rx1 + rx2 gain with status: %d and card: %u\n", status, card);
            return status;
        }

        status = _read_gain_rx1_rx2(card, &rx1_gain, &rx2_gain);
        if( 0 != status )
        {
            skiq_error("Unable to read rx1 + rx2 gain with status: %d and card: %u\n", status, card);
            return status;
        }

        skiq_info("RX1 Gain %X -> %X   RX2 Gain %X -> %X\n", rx1_gain_old, rx1_gain,
                  rx2_gain_old, rx2_gain );

        if ( ((rx1_gain_old - 1) != rx1_gain) || ((rx2_gain_old - 1 ) != rx2_gain) )
        {
            /* Test has failed, lines are not connected properly */
            skiq_error("Gain decrement failed.  RX1 Gain %X -> %X   RX2 Gain %X -> %X\n",
                       rx1_gain_old, rx1_gain,
                       rx2_gain_old, rx2_gain );
            decrement_failed = true;
        }

    }
    else
    {
        skiq_error("AD9361 not present but required for skiq_fact_test_rfic_ctrl");
        status = -ENOTSUP;
    }

    if ( ( status == 0 ) && ( increment_failed || decrement_failed ) )
    {
        status = -EFAULT;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_test_rfic_gpio( uint8_t card,
                                  uint8_t rfic_sel,
                                  bool is_fpga_write,
                                  uint32_t data_write,
                                  uint32_t* p_data_read )
{
    rfic_t rfic;
    skiq_part_t part = skiq_part_invalid;
    int32_t status = 0;
    uint32_t reg_addr;
    uint32_t gpio_oe;
    uint32_t value = 0;

    part = _skiq_get_part(card);
    if(is_fpga_write)
    {
        if(part == skiq_nv100)
        {
            rfic = _skiq_get_generic_rfic_instance(card);
            data_write = ((data_write << 16) & RFIC_ADRV9002_GPIO_MASK);
            /* setup the FPGA to drive the GPIF which is where the GPIO is */
            status = sidekiq_fpga_reg_read(card, FPGA_REG_GPIF_TRISTATE, &value);
            if(status == 0)
            {
                status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_GPIF_TRISTATE, (value | RFIC_ADRV9002_GPIO_MASK));
            }
            
            /* write the data to the FPGA */
            if(status == 0)
            {
                status = sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_GPIF_WRITE, data_write);
            }

            /* test to make sure the RFIC sees the updated values on the GPIO */
            if(status == 0)
            {
                /* Sidekiq NV100 uses only 12 GPIO, however the ADRV9002 implements 16 GPIO */
                status = rfic_read_gpio(&rfic, p_data_read);
            }
        }
    }
    else
    {
        if ( part == skiq_x2 )
        {
            gpio_oe = RFIC_X2_GPIO_MASK;
            if( (rfic_sel != 0) )
            {
                skiq_error("Invalid RF IC %" PRIu8 " selection requested (card=%" PRIu8 ")",
                           rfic_sel, card);
                status = -ENOTSUP;
            }
        }
        else if( part == skiq_x4 )
        {
            gpio_oe = RFIC_X4_GPIO_MASK;
            if( (rfic_sel != 0) && (rfic_sel != 1) )
            {
                skiq_error("Invalid RF IC %" PRIu8 " selection requested (card=%" PRIu8 ")",
                           rfic_sel, card);
            }
        }
        else
        {
            // TODO: Do we want to support the GPIO on the AD9361 as well?
           /* only necessary for any AD9361 based radio: mPCIe, m.2, Z2, Stretch, Z2, Z2+ - not X2/4*/

            skiq_error("Invalid part (%u) for RFIC GPIO test (card=%u)\n", part, card);
        }

        if( status == 0 )
        {
            switch( rfic_sel )
            {
                case 0:
                    rfic = _skiq_get_rx_rfic_instance(card, skiq_rx_hdl_A1);
                    reg_addr = FPGA_REG_GPIO_READ;
                    break;
                case 1:
                    rfic = _skiq_get_rx_rfic_instance(card, skiq_rx_hdl_B1);
                    reg_addr = FPGA_REG_GPIF_READ;
                    break;
                default:
                    status = -EINVAL;
                    skiq_error("Invalid RF IC selection %u specified (card=%u)", rfic_sel, card);
                    break;
            }

            if( status==0 )
            {
                status = rfic_init_gpio(&rfic, gpio_oe);
                if( 0 == status )
                {
                    status = rfic_write_gpio(&rfic, data_write);
                    if( 0 == status )
                    {
                        status = sidekiq_fpga_reg_read(card,
                                                       reg_addr,
                                                       p_data_read);
                    }
                }
            }
        }
    }

    return status;
}

/* callback function for packet complete */
static void _callback_test_tx_samples( int32_t status,
                                       skiq_tx_block_t *p_block,
                                       void *p_user )
{
    tx_test_samples_data_t* p_test = (tx_test_samples_data_t*) p_user;

    // just signal that we're ready to send another packet
    LOCK(p_test->mutex);
    {
        p_test->sent_blocks++;
        pthread_cond_signal(&(p_test->wait));
    }
    UNLOCK(p_test->mutex);
}

/* function to actually send the sample data */
static void *_do_test_tx_samples( void* const arg )
{
    tx_test_samples_data_t* p_test = (tx_test_samples_data_t*) arg;
    skiq_tx_block_t** p_tx_blocks = p_test->p_tx_blocks;
    skiq_tx_hdl_t hdl = p_test->hdl;
    uint32_t num_blocks = p_test->num_blocks;
    uint8_t card = p_test->card;
    uint32_t curr_block = 0;
    int32_t status = 0;

    p_test->is_thread_active = true;
    status = skiq_start_tx_streaming(card, hdl);

    // continue running until we're told to stop
    while( (false == p_test->is_stop_requested) && (0 == status) )
    {
        // transmit a block at a time
        while( (false == p_test->is_stop_requested) &&
               (curr_block < num_blocks) &&
               (0 == status) )
        {
            // transmit the data
            status = skiq_transmit(card, hdl, p_tx_blocks[curr_block], p_test);
            if( status == SKIQ_TX_ASYNC_SEND_QUEUE_FULL )
            {
                LOCK(p_test->mutex);
                {
                    pthread_cond_wait(&(p_test->wait), &(p_test->mutex));
                }
                UNLOCK(p_test->mutex);
                status = 0;
            }
            else if( status == 0 )
            {
                curr_block++;
            }
        }

        curr_block = 0;
    }

    p_test->is_thread_active = false;

    return NULL;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_enable_tx_samples( uint8_t card,
                                     skiq_tx_hdl_t hdl,
                                     int16_t* samples,
                                     uint32_t sample_len )
{
    /* reference to an array of transmit block references */
    skiq_chan_mode_t chan_mode = skiq_chan_mode_single;
    skiq_tx_flow_mode_t flow_mode = skiq_tx_immediate_data_flow_mode;
    skiq_tx_transfer_mode_t xfer_mode = skiq_tx_transfer_mode_async;
    uint16_t block_words = 0;
    uint16_t alloc_block = 0;
    uint16_t block_bytes = 0;
    uint32_t num_blocks = 0;
    uint8_t num_threads = 3;
    uint32_t m = 0, n = 0;
    uint8_t* p_dst = NULL;
    uint8_t* p_src = NULL;
    int32_t status = 0;

    if ( true == _tx_test[card].is_thread_active )
    {
        return -EEXIST;
    }

    if( skiq_tx_hdl_A1 == hdl )
    {
        block_words = 16380;
        alloc_block = block_words;
        chan_mode = skiq_chan_mode_single;
    }
    else if( skiq_tx_hdl_A2 == hdl )
    {
        block_words = 16382;
        alloc_block = block_words*2;
        chan_mode = skiq_chan_mode_dual;
    }
    else
    {
        return -EINVAL;
    }

    block_bytes = block_words * 4;
    num_blocks = sample_len / block_words;
    if( 0 == num_blocks )
    {
        return -EINVAL;
    }

    status = skiq_write_chan_mode(card, chan_mode);
    if( 0 != status )
    {
        return status;
    }

    status = skiq_write_tx_block_size(card, hdl, block_words);
    if( 0 != status )
    {
        return status;
    }

    status = skiq_write_tx_data_flow_mode(card, hdl, flow_mode);
    if( 0 != status )
    {
        return status;
    }

    status = skiq_write_iq_pack_mode(card, false);
    if( 0 != status )
    {
        return status;
    }

    status = skiq_write_tx_transfer_mode(card, hdl, xfer_mode);
    if( 0 != status )
    {
        return status;
    }

    status = skiq_write_num_tx_threads(card, num_threads);
    if( 0 != status )
    {
        return status;
    }

    status = skiq_register_tx_complete_callback(card,
                                                &_callback_test_tx_samples);
    if( 0 != status )
    {
        return status;
    }

    _tx_test[card].card = card;
    _tx_test[card].hdl = hdl;
    _tx_test[card].num_blocks = num_blocks;
    _tx_test[card].p_tx_blocks = calloc(num_blocks, sizeof(skiq_tx_block_t*));
    if( _tx_test[card].p_tx_blocks == NULL )
    {
        _skiq_log(SKIQ_LOG_ERROR,
                  "unable to allocate array memory (%d bytes)\n",
                 (uint32_t)(num_blocks * sizeof(skiq_tx_block_t*)));
        return -ENOMEM;
    }
    p_src = (uint8_t*) samples;
    for( m = 0; (m < (num_blocks)) && (0 == status); m++ )
    {
        // allocate a transmit block by number of words
        _tx_test[card].p_tx_blocks[m] = skiq_tx_block_allocate(alloc_block);

        if ( NULL == _tx_test[card].p_tx_blocks[m] )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "unable to allocate transmit block memory (%d bytes)\n",
                      (uint32_t) sizeof(skiq_tx_block_t));
            return (-ENOMEM);
        }
        _tx_test[card].p_tx_blocks[m]->timestamp = m;
        p_dst = (uint8_t*) _tx_test[card].p_tx_blocks[m]->data;
        memcpy(p_dst, p_src, block_bytes);
        if( skiq_chan_mode_dual == chan_mode )
        {
            // duplicate the block of samples into the second half of the
            // transmit block's data array
            memcpy(p_dst + block_bytes, p_src, block_bytes);
        }
        p_src += block_bytes;
    }

    if( 0 == status )
    {
        status = pthread_create(&(_tx_test[card].thread),
                                NULL,
                                (void*) &_do_test_tx_samples,
                                (void*) &(_tx_test[card]));
    }

    if( 0 != status )
    {
        // try and clean up memory
        for( n = 0; n < m; n++ )
        {
            skiq_tx_block_free(_tx_test[card].p_tx_blocks[n]);
        }
        free(_tx_test[card].p_tx_blocks);
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_disable_tx_samples(uint8_t card,
                                     skiq_tx_hdl_t hdl)
{
    int32_t status = 0;

    if( true == _tx_test[card].is_thread_active )
    {
        _tx_test[card].is_stop_requested = true;
        status = pthread_join(_tx_test[card].thread, NULL);
        _tx_test[card].is_stop_requested = false;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_init_for_config( uint8_t card )
{
    int32_t status=0;

#ifndef __MINGW32__
    /* immediately open syslog */
    openlog( "SKIQ", (LOG_PID | LOG_NDELAY | LOG_CONS | LOG_PERROR), LOG_USER );
#endif /* __MINGW32__ */

    if( (status=card_mgr_init()) == 0 )
    {
        status=_skiq_init_xport( card,
                                 skiq_xport_type_auto,
                                 skiq_xport_init_level_basic );

    }

    if ( status == 0 )
    {
        io_exp_initialize_cache(card);
    }

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_exit_for_config( uint8_t card )
{
    int32_t status=0;

    status = skiq_xport_card_exit( card );
    hal_reset_functions( card );
    card_mgr_destroy_card_mgr();

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_calibration_clear( uint8_t card )
{
    return (card_calibration_clear(card));
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_calibration_add_rx_by_file( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              skiq_rf_port_t port,
                                              FILE* fp )
{
    return (card_calibration_add_rx_by_file(card, hdl, port, fp));
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_calibration_add_iq_complex_by_file( uint8_t card,
                                                      skiq_rx_hdl_t hdl,
                                                      FILE* fp )
{
    return (card_calibration_add_iq_complex_by_file(card, hdl, fp));
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_calibration_commit( uint8_t card )
{
    return (card_calibration_commit(card,_skiq_get_part(card)));
}


/** @brief enables PRBS test mode for the specified card

    @attention These functions work ONLY with AD9361 and AD9364 RFIC chipsets
*/
static void _enable_prbs_mode( uint8_t card )
{
    /* enable PRBS test mode and modify LVDS inversion */
    hal_rfic_write_reg(card, 0, REG_BIST_CONFIG, 0x09);
    hal_rfic_write_reg(card, 0, REG_LVDS_INVERT_CTRL1, 0xFF);
    hal_rfic_write_reg(card, 0, REG_LVDS_INVERT_CTRL2, 0x0F);
}


/** @brief disables PRBS test mode for the specified card

    @attention These functions work ONLY with AD9361 and AD9364 RFIC chipsets
*/
static void _disable_prbs_mode( uint8_t card )
{
    // disable PRBS mode (3F4 00)
    hal_rfic_write_reg(card, 0, REG_BIST_CONFIG, 0x00);

    // reset inversion to default
    hal_rfic_write_reg(card, 0, REG_LVDS_INVERT_CTRL1, 0x00);
    hal_rfic_write_reg(card, 0, REG_LVDS_INVERT_CTRL2, 0x00);
}

static int32_t _check_prbs_status( uint8_t card, uint8_t nr_handles, uint32_t *prbs_errors )
{
    int32_t status = 0;
    uint32_t data_A1, data_A2;
    
    /* the register interactions will be updated in upcoming libsidekiq v5.0,
       the FPGA team is working towards an interface that is more specific to the
       card that is in use */
    
    /* check locking status for A1 and optionally A2 */
    sidekiq_fpga_reg_read(card, FPGA_REG_ADC_NUM_ERRORS_A, &data_A1);
    if ( (data_A1 & 0x80000000) == 0 )
    {
        _skiq_log(SKIQ_LOG_WARNING, "PRBS not locked for card %u for handle A1!\n", card);
        status = -ENOLCK;
    }
    else if ( nr_handles == 2 )
    {
        sidekiq_fpga_reg_read(card, FPGA_REG_ADC_NUM_ERRORS_B, &data_A2);
        if ( (data_A2 & 0x80000000) == 0 )
        {
            _skiq_log(SKIQ_LOG_WARNING, "PRBS not locked for card %u for handle A2!\n", card);
            status = -ENOLCK;
        }
    }

    /* handle(s) have lock, so check number of errors for A1 and optionally A2 */
    if ( status == 0 )
    {
        *prbs_errors = 0;

        _skiq_log(SKIQ_LOG_INFO, "At the current sample rate, number of errors A1=%u for card %u\n", (data_A1 & 0x7FFFFFF), card);
        if ( nr_handles == 2 )
        {
            _skiq_log(SKIQ_LOG_INFO, "At the current sample rate, number of errors A2=%u for card %u\n", (data_A2 & 0x7FFFFFF), card);
        }

        if ( (data_A1 & 0x7FFFFFFF) > 0 )
        {
            status = -EFAULT;
            *prbs_errors = (data_A1 & 0x7FFFFFFF);
        }

        if ( ( nr_handles == 2 ) && ( ( data_A2 & 0x7FFFFFFF ) > 0 ) )
        {
            status = -EFAULT;
            *prbs_errors += (data_A2 & 0x7FFFFFFF);
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_prbs_run_test_multi( uint8_t cards[],
                                       uint8_t nr_cards,
                                       uint32_t sample_rate,
                                       uint8_t nr_handles_requested,
                                       uint32_t nr_seconds_requested,
                                       uint32_t prbs_errors[] )
{
    int32_t status = 0;
    uint32_t seconds = 0;
    skiq_part_t part;
    skiq_rx_hdl_t handles[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end];
    uint8_t nr_handles[SKIQ_MAX_NUM_CARDS];
    skiq_chan_mode_t chan_mode = skiq_chan_mode_single;
    uint8_t i;

    /* check passed parameters */
    if ( ( nr_cards == 0 ) || ( nr_cards > SKIQ_MAX_NUM_CARDS ) )
    {
        return -EINVAL;
    }

    /* check that all card indices are in range, active, and represent a part that is
     * 9361/9364-based */
    for (i = 0; i < nr_cards; i++)
    {
        uint8_t card_idx = cards[i];
        skiq_rx_hdl_t rx_hdl;

        CHECK_CARD_RANGE(card_idx);
        CHECK_CARD_ACTIVE(card_idx);

        part = _skiq_get_part( card_idx );
        if ( ( part != skiq_mpcie ) &&
             ( part != skiq_m2 ) &&
             ( part != skiq_z2 ) &&
             ( part != skiq_m2_2280 ) &&
             ( part != skiq_nv100 ) &&
             ( part != skiq_z2p ) &&
             ( part != skiq_z3u ) )
        {
            /* not a 9361, 9364, or navassa based product, so no PRBS support (yet) */
            return -ENOTSUP;
        }

        /* two handles requested, check if card supports two receive handles */
        if ( ( nr_handles_requested == 2 ) && ( _skiq_get_nr_rx_hdl( card_idx ) < 2 ) )
        {
            return -EINVAL;
        }

        /* build up the handles[card_idx][] array based on product's capabilities and number of
         * handles requested */
        nr_handles[card_idx] = 0;
        if ( _skiq_get_nr_rx_hdl( card_idx ) > 0 )
        {
            rx_hdl = _skiq_get_rx_hdl(card_idx, 0);
            if ( rx_hdl < skiq_rx_hdl_end )
            {
                handles[card_idx][nr_handles[card_idx]++] = rx_hdl;
            }
            else
            {
                skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                           " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                           card_idx, _skiq_get_nr_rx_hdl( card_idx ), 0);
                return -EPROTO;
            }
        }

        if( (nr_handles_requested == 2) && ( _skiq_get_nr_rx_hdl( card_idx ) >= 2) )
        {
            if (part == skiq_nv100)
            {
                /* Sidekiq NV100's "second" handle */ 
                rx_hdl = skiq_rx_hdl_B1;
            }
            else
            {
                /* Every other Sidekiq part's "second" handle */
                rx_hdl = _skiq_get_rx_hdl(card_idx, 1);
            }
            
            if ( rx_hdl < skiq_rx_hdl_end )
            {
                handles[card_idx][nr_handles[card_idx]++] = rx_hdl;
            }
            else
            {
                skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                           " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                           card_idx, _skiq_get_nr_rx_hdl( card_idx ), 1);
                return -EPROTO;
            }
        }

        /* 'auto' nr_handles_requested, so throw in the second (e.g index 1) if it's available */
        if ( ( nr_handles_requested == 0 ) && ( _skiq_get_nr_rx_hdl( card_idx ) > 1 ) )
        {
            rx_hdl = _skiq_get_rx_hdl(card_idx, 1);
            if ( rx_hdl < skiq_rx_hdl_end )
            {
                handles[card_idx][nr_handles[card_idx]++] = rx_hdl;
            }
            else
            {
                skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                           " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                           card_idx, _skiq_get_nr_rx_hdl( card_idx ), 1);
                return -EPROTO;
            }
        }
    }

    LOCK(prbs_state.mutex);
    {
        if ( prbs_state.is_running )
        {
            /* test is already running, don't start again */
            UNLOCK_AND_RETURN(prbs_state.mutex, -EBUSY);
        }

        if ( prbs_state.is_stop_requested )
        {
            /* stop was requested, acknowledge by setting request to false */
            prbs_state.is_stop_requested = false;
            UNLOCK_AND_RETURN(prbs_state.mutex, -ECANCELED);
        }
        else
        {
            /* all good, start the test */
            prbs_state.is_running = true;
        }
    }
    UNLOCK(prbs_state.mutex);

    for (i = 0; i < nr_cards; i++)
    {
        uint8_t card_idx = cards[i];

        /* use dual channel mode if there are two receive handles for this card */
        if ( nr_handles[card_idx] == 2 )
        {
            chan_mode = skiq_chan_mode_dual;
        }
        else
        {
            chan_mode = skiq_chan_mode_single;
        }

        status = skiq_write_chan_mode(card_idx, chan_mode);
        if ( status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR, "Unable to set chan mode for card %u with internal status %d, halting test\n", card_idx, status);
            status = -EIO;
            goto finished;
        }
    }

    /* for each card, enable PRBS test mode and configure receive sample rate */
    for (i = 0; i < nr_cards; i++)
    {
        uint8_t card_idx = cards[i];

        /* configure sample rate */
        skiq_info( "Updating sample rate to %" PRIu32 " for card %" PRIu8 "\n", sample_rate, card_idx);

        status = skiq_write_rx_sample_rate_and_bandwidth( card_idx, skiq_rx_hdl_A1, sample_rate, sample_rate );
        if ( status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR, "Unable to configure receive sample rate for card %u with internal status %d, halting test\n", card_idx, status);
            status = -EIO;
            goto finished;
        }

        /**
           @warn On the Sidekiq NV100 (Navassa based), PRBS mode must be enabled *AFTER* configuring
           the sample rate and bandwidth
        */
        part = _skiq_get_part( card_idx );
        if ( part == skiq_nv100 )
        {
#if (defined HAS_NV100_SUPPORT)
            status = navassa_enable_prbs_mode(card_idx);
            if ( status != 0 )
            {
                skiq_error("Unable to enable PRBS mode rate for card %" PRIu8 " with internal "
                           "status %d, halting test\n", card_idx, status);
                status = -EIO;
                goto finished;
            }
#endif
        }
        else
        {
            _enable_prbs_mode( card_idx );
        }

    }

    /* for each card, start receive streaming on all associated handles */
    for (i = 0; i < nr_cards; i++)
    {
        uint8_t card_idx = cards[i];

        status = skiq_start_rx_streaming_multi_immediate( card_idx, handles[card_idx], nr_handles[card_idx] );
        if ( status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR, "Error starting RX streaming for card %u with internal status %d, halting test\n", card_idx, status);
            status = -EIO;

            /* unroll / stop streaming for all cards up to the point of failed starting, in reverse order */
            if ( i > 0 )
            {
                do
                {
                    i--;
                    card_idx = cards[i];

                    /* disable PRBS test mode */
                    part = _skiq_get_part( card_idx );
                    if(part == skiq_nv100)
                    {
                        rfic_t rfic = _skiq_get_generic_rfic_instance(card_idx);
#if (defined HAS_NV100_SUPPORT)
                        navassa_disable_prbs_mode(rfic.p_id);
#endif
                    }
                    else
                    {
                        _disable_prbs_mode( card_idx );
                    }

                    status = skiq_stop_rx_streaming_multi_immediate( card_idx, handles[card_idx], nr_handles[card_idx] );
                    if ( status != 0 )
                    {
                        _skiq_log(SKIQ_LOG_ERROR, "Error stopping RX streaming for card %u with status %d\n", card_idx, status);

                        /* even though there's an error, try to stop streaming for the remaining cards */
                    }

                } while ( i > 0 );
            }
            goto finished;
        }
    }

    /* perform the PRBS test for the number of seconds specified */
    seconds = 0;
    while ( ( seconds < nr_seconds_requested ) && ( status == 0 ) && !prbs_state.is_stop_requested )
    {
        hal_nanosleep(SEC);
        seconds++;

        /* check status for each card in the system */
        _skiq_log(SKIQ_LOG_INFO, "Checking current sample rate at second %u\n", seconds);

        for ( i = 0; i < nr_cards; i++ )
        {
            uint8_t card_idx = cards[i];

            part = _skiq_get_part( card_idx );
            if(part == skiq_nv100)
            {
#if (defined HAS_NV100_SUPPORT)
                status = navassa_check_prbs_status(card_idx, nr_handles[card_idx], &prbs_errors[card_idx]);
#endif
            }
            else
            {
                status = _check_prbs_status(card_idx, nr_handles[card_idx], &prbs_errors[card_idx]);
            }
        }
    }

    for ( i = 0; i < nr_cards; i++ )
    {
        uint8_t card_idx = cards[i];
        int32_t stop_status;

        /* disable PRBS test mode */
        part = _skiq_get_part( card_idx );
        if ( part == skiq_nv100 )
        {
            rfic_t rfic = _skiq_get_generic_rfic_instance(card_idx);
#if (defined HAS_NV100_SUPPORT)
            navassa_disable_prbs_mode(rfic.p_id);
#endif
        }
        else
        {
            _disable_prbs_mode( card_idx );
        }

        stop_status = skiq_stop_rx_streaming_multi_immediate( card_idx, handles[card_idx], nr_handles[card_idx] );
        if ( stop_status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR, "Error stopping RX streaming for card %u with internal status %d\n", card_idx, stop_status);

            /* even though there's an error, try to stop streaming for the remaining cards */
        }
    }

finished:
    LOCK(prbs_state.mutex);
    {
        if ( prbs_state.is_stop_requested )
        {
            /* stop was requested, acknowledge by setting request to false */
            prbs_state.is_stop_requested = false;
            status = -ECANCELED;
        }

        /* regardless of being canceled or not, test is over */
        prbs_state.is_running = false;
    }
    UNLOCK(prbs_state.mutex);

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_prbs_run_test_single( uint8_t card,
                                        uint32_t sample_rate,
                                        uint32_t nr_seconds_requested,
                                        uint32_t *p_prbs_errors )
{
    return skiq_fact_prbs_run_test_multi( &card, 1, sample_rate, 0 /* nr_handles_requested */,
                                          nr_seconds_requested, p_prbs_errors );
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
void skiq_fact_prbs_cancel_test( void )
{
    LOCK(prbs_state.mutex);
    {
        /* request that the test be stopped whenever convenient */
        prbs_state.is_stop_requested = true;

        _skiq_log(SKIQ_LOG_WARNING, "PRBS test canceling\n");
    }
    UNLOCK(prbs_state.mutex);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_i2c_enable_external_bus_access( uint8_t card )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( _skiq_get_part( card ) == skiq_z2 )
    {
        status = io_z2_enable_external_bus_access(card);
        if ( status != 0 )
        {
            status = -EACCES;
        }
    }
    else if ( _skiq_get_fmc_carrier( card ) == skiq_fmc_carrier_htg_k800 )
    {
        /* The I2C read/write functions will enable bus access */
        status = 0;
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}


/**************************************************************************************************/
/** _i2c_enable_bus is a short helper function that enables access to whatever bus is specified.

    @param[in] card card index of the Sidekiq of interest
    @param[in] bus bus identifier (currently 0 = internal and 1 = external/secondary, 2 = extended )

    @return int32_t
    @retval 0 Success
    @retval -EINVAL Specified bus identifier is out of range
    @retval -EINVAL Specified card index is out of range
    @retval -EACCES Failed to enable access for specified bus
    @retval -ENOTSUP card /part at specified card index does not support external I2C bus control
    @retval -ENODEV Specified card has not been initialized
 */
static int32_t _i2c_enable_bus( uint8_t card,
                                uint8_t bus )
{
    int32_t status = 0;

    if ( ( bus == 1 ) && ( _skiq_get_part( card ) == skiq_z2 ) )
    {
        status = skiq_fact_i2c_enable_external_bus_access( card );
        if ( status != 0 )
        {
            status = -EACCES;
        }
    }
    else if ( ( bus == 0 ) || ( bus == 1 ) || ( bus == 2 ) )
    {
        status = 0;
    }
    else
    {
        /* 'bus' is out of range */
        status = -EINVAL;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_i2c_disable_external_bus_access( uint8_t card )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( _skiq_get_part( card ) == skiq_z2 )
    {
        status = io_z2_disable_external_bus_access(card);
        if ( status != 0 )
        {
            status = -EACCES;
        }
    }
    else if ( _skiq_get_fmc_carrier( card ) == skiq_fmc_carrier_htg_k800 )
    {
        /* Currently no support for disabling external bus access on HTG-K800, but a successful
         * return value is required for things to work smoothly */
        status = 0;
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_i2c_write_before_read( uint8_t card,
                                         uint8_t bus,
                                         uint8_t dev_i2c_addr,
                                         uint8_t reg_addr,
                                         uint8_t *p_data,
                                         uint16_t nr_bytes )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    /* enable the requested bus */
    status = _i2c_enable_bus( card, bus );

    /* write I2C register address and then read 'nr_bytes' bytes from I2C register */
    if ( status == 0 )
    {
        status = hal_write_then_read_i2c_by_bus( card, bus, dev_i2c_addr, reg_addr, p_data, nr_bytes );
        if ( ( status > 0 ) && ( status < nr_bytes ) )
        {
            status = -EAGAIN;
        }
        else if ( status < 0 )
        {
            status = -EIO;
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_i2c_write( uint8_t card,
                             uint8_t bus,
                             uint8_t dev_i2c_addr,
                             uint8_t *p_data,
                             uint16_t nr_bytes )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    /* enable the requested bus */
    status = _i2c_enable_bus( card, bus );

    if ( status == 0 )
    {
        status = hal_write_i2c_by_bus( card, bus, dev_i2c_addr, p_data, nr_bytes );
        if ( ( status > 0 ) && ( status < nr_bytes ) )
        {
            status = -EAGAIN;
        }
        else if ( status < 0 )
        {
            status = -EIO;
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_i2c_read( uint8_t card,
                            uint8_t bus,
                            uint8_t dev_i2c_addr,
                            uint8_t *p_data,
                            uint16_t nr_bytes )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    /** USB transport does not support reading from I2C.  If the part is an
        M.2 or an mPCIe card and the PCIe transport is not available, then
        that card is using the USB transport and this test is not supported.
    */

    /* enable the requested bus */
    status = _i2c_enable_bus( card, bus );

    if ( status == 0 )
    {
        status = hal_read_i2c_by_bus( card, bus, dev_i2c_addr, p_data, nr_bytes );
        if ( ( status > 0 ) && ( status < nr_bytes ) )
        {
            status = -EAGAIN;
        }
        else if ( ( status < 0 ) && ( status != -ENOTSUP ) )
        {
            status = -EIO;
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_verify_fmc_contents_from_file( uint8_t card,
                                                 FILE *fp )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( fp != NULL )
    {
        if ( fseek( fp, 0, SEEK_END) == 0 )
        {
            uint8_t *p_file_data = NULL;
            long size;

            size = ftell(fp);
            rewind(fp);

            if ( size > 0 )
            {
                p_file_data = calloc(1, (size_t)size);
                if ( p_file_data != NULL )
                {
                    size_t n;

                    n = fread( p_file_data, 1, size, fp );
                    if ( n == size )
                    {
                        uint8_t *p_fmc_data = NULL;

                        p_fmc_data = calloc(1, (size_t)size);
                        if ( p_fmc_data != NULL )
                        {
                            status = hal_read_fmc_eeprom( card, 0, p_fmc_data, size );
                            if ( status == 0 )
                            {
                                if ( 0 != memcmp( p_file_data, p_fmc_data, size ) )
                                {
                                    /* contents did not match */
                                    status = -EFAULT;
                                    skiq_info("-------------------------------------------------------------------\n");
                                    skiq_info("----- Differences shown as 'FILE<>FMC' otherwise byte matched -----\n");
                                    skiq_info("-------------------------------------------------------------------\n");
                                    diff_hex_dump( p_file_data, p_fmc_data, size );
                                }
                            }

                            /* free allocated memory */
                            FREE_IF_NOT_NULL( p_fmc_data );
                        }
                        else
                        {
                            /* allocation for p_fmc_data failed */
                            status = -ENOMEM;
                        }
                    }
                    else if ( feof( fp ) )
                    {
                        /* error reading file contents */
                        skiq_error("'Short read' encountered reading file contents\n");
                        status = -EIO;
                    }
                    else if ( ferror( fp ) )
                    {
                        /* error reading file contents */
                        skiq_error("Error encountered reading file contents (%d)\n", errno);
                        status = -EIO;
                    }

                    /* free allocated memory */
                    FREE_IF_NOT_NULL( p_file_data );
                }
                else
                {
                    /* allocation for p_file_data failed */
                    status = -ENOMEM;
                }
            }
            else
            {
                /* ftell() must have returned EBADF */
                status = -EINVAL;
            }
        }
        else
        {
            /* fseek() must have returned EBADF */
            status = -EINVAL;
        }
    }
    else
    {
        status = -EINVAL;
    }

    return (status);

} /* skiq_fact_verify_fmc_contents_from_file */


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_fmc_contents_from_file( uint8_t card,
                                                FILE *fp )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( fp != NULL )
    {
        if ( fseek( fp, 0, SEEK_END) == 0 )
        {
            uint8_t *p_data = NULL;
            long size;

            size = ftell(fp);
            rewind(fp);

            if ( size > 0 )
            {
                p_data = calloc(1, (size_t)size);
                if ( p_data != NULL )
                {
                    size_t n;

                    n = fread( p_data, 1, size, fp );
                    if ( n == size )
                    {
                        status = card_write_fmc_eeprom( card, 0, p_data, size );
                    }
                    else if ( feof( fp ) )
                    {
                        /* error reading file contents */
                        skiq_error("'Short read' encountered reading file contents\n");
                        status = -EIO;
                    }
                    else if ( ferror( fp ) )
                    {
                        /* error reading file contents */
                        skiq_error("Error encountered reading file contents (%d)\n", errno);
                        status = -EIO;
                    }

                    /* free allocated memory */
                    FREE_IF_NOT_NULL( p_data );
                }
                else
                {
                    /* allocation failed */
                    status = -ENOMEM;
                }
            }
            else
            {
                /* ftell() must have returned EBADF */
                status = -EINVAL;
            }
        }
        else
        {
            /* fseek() must have returned EBADF */
            status = -EINVAL;
        }
    }
    else
    {
        status = -EINVAL;
    }

    return (status);

} /* skiq_fact_write_fmc_contents_from_file */


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_fmc_contents( uint8_t card,
                                      const uint8_t *p_data,
                                      uint16_t length )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( ( p_data != NULL ) && ( length > 0 ) )
    {
        status = card_write_fmc_eeprom( card, 0, (uint8_t *)p_data, length );
    }
    else
    {
        status = -EINVAL;
    }

    return (status);

} /* skiq_fact_write_fmc_contents */


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_default_bias_index( uint8_t card,
                                           uint8_t *p_bias_index )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    status = card_read_bias_index_calibration( card, p_bias_index );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_default_bias_index( uint8_t card,
                                            uint8_t bias_index )
{
    int32_t status=-ENOTSUP;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    status = card_write_bias_index_calibration( card, bias_index );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_bias_index( uint8_t card,
                                   uint8_t *p_bias_index )
{
    int32_t status=-ENOTSUP;
    rfe_t rfe;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    rfe = _skiq_get_generic_rfe_instance(card);
    status = rfe_read_bias_index( &rfe, p_bias_index );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_bias_index( uint8_t card,
                                    uint8_t bias_index )
{
    int32_t status=-ENOTSUP;
    rfe_t rfe;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    rfe = _skiq_get_generic_rfe_instance(card);
    status = rfe_write_bias_index( &rfe, bias_index );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_calibration_date( uint8_t card,
                                          uint16_t cal_year,
                                          uint8_t cal_week,
                                          uint8_t cal_interval )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    status = card_write_cal_date( card,
                                  cal_year,
                                  cal_week,
                                  cal_interval );

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_reset_calibration_date( uint8_t card )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    status = card_reset_cal_date(card);

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_is_golden_locked( uint8_t card, bool *p_locked )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( p_locked == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        status = flash_is_golden_locked( card, p_locked );
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_lock_golden_sectors( uint8_t card )
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    return flash_lock_golden_sectors( card );
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_unlock_golden_sectors( uint8_t card )
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    return flash_unlock_golden_sectors( card );
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_tx_jesd_lane_sel( uint8_t card, uint8_t *p_lane_sel )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    status = fpga_jesd_read_tx_lane_sel( card, p_lane_sel );

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_tx_jesd_lane_sel( uint8_t card, uint8_t lane_sel )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if( (lane_sel != 0) && (lane_sel != 1) )
    {
        status = -EINVAL;
    }
    else
    {
        status = fpga_jesd_write_tx_lane_sel( card, lane_sel );
    }

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_orx_jesd_lane_sel( uint8_t card,
                                          uint8_t *p_lane_sel )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_lane_sel);

    status = fpga_jesd_read_orx_lane_sel( card, p_lane_sel );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_orx_jesd_lane_sel( uint8_t card,
                                           uint8_t lane_sel )
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if ( ( lane_sel != 0 ) && ( lane_sel != 1 ) )
    {
        status = -EINVAL;
    }
    else
    {
        status = fpga_jesd_write_orx_lane_sel( card, lane_sel );
    }

    return (status);
}
/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_power_down_tx( uint8_t card, skiq_tx_hdl_t hdl )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );
    status = rfic_power_down_tx( &rfic_instance );

    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_warp_voltage( uint8_t card,
                                     uint32_t *p_warp_voltage )
{
    int32_t status;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    rfic_instance = _skiq_get_generic_rfic_instance(card);
    status = rfic_read_warp_voltage_extended_range( &rfic_instance, p_warp_voltage );
    if ( status == -ENOTSUP )
    {
        uint16_t warp_voltage;
        status = rfic_read_warp_voltage( &rfic_instance, &warp_voltage );
        if ( status == 0 )
        {
            *p_warp_voltage = warp_voltage;
        }
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_warp_voltage( uint8_t card,
                                      uint32_t warp_voltage )
{
    int32_t status;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    rfic_instance = _skiq_get_generic_rfic_instance(card);
    status = rfic_write_warp_voltage_extended_range( &rfic_instance, warp_voltage );
    if ( status == -ENOTSUP )
    {
        status = rfic_write_warp_voltage( &rfic_instance, (uint16_t)(warp_voltage & 0xFFFF) );
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_rx_lna_by_idx( uint8_t card,
                                       skiq_rx_hdl_t rx_hdl,
                                       uint8_t idx,
                                       bool enable )
{
    int32_t status = 0;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_VALID_RX_HDL(card, rx_hdl);

    rfe_instance = _skiq_get_rx_rfe_instance( card, rx_hdl );
    status = rfe_update_rx_lna_by_idx( &rfe_instance, idx, enable ? lna_enabled : lna_disabled );

    return (status);
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_bypass_rx_lna_by_idx( uint8_t card,
                                        skiq_rx_hdl_t rx_hdl,
                                        uint8_t idx )
{
    int32_t status=0;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_VALID_RX_HDL(card, rx_hdl);

    rfe_instance = _skiq_get_rx_rfe_instance( card, rx_hdl );
    status = rfe_update_rx_lna_by_idx( &rfe_instance, idx, lna_bypassed );

    return (status);
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_override_rx_lna_by_idx( uint8_t card,
                                          skiq_rx_hdl_t rx_hdl,
                                          uint8_t idx,
                                          skiq_pin_value_t pin_value )
{
    int32_t status = -ENOTSUP;
    rfe_t rfe_instance;
    skiq_part_t part;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_VALID_RX_HDL(card, rx_hdl);

    part = _skiq_get_part( card );

    if ( (part == skiq_m2_2280) ||
         (part == skiq_z2p) )
    {
        rfe_instance = _skiq_get_rx_rfe_instance( card, rx_hdl );
        status = rfe_m2_2280_override_rx_lna_by_idx( rfe_instance.p_id, idx, pin_value );
    }

    return (status);
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_board_id( uint8_t card,
                                 uint8_t *p_board_id )
{
    return fpga_ctrl_read_board_id( card, p_board_id );
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_config_custom_rx_filter( uint8_t card,
                                           skiq_rx_hdl_t rx_hdl,
                                           uint8_t index,
                                           uint8_t hpf_lpf_setting,
                                           uint8_t rx_fltr1_sw,
                                           uint8_t rfic_port )
{
    int32_t status = 0;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_VALID_RX_HDL(card, rx_hdl);

    rfe_instance = _skiq_get_rx_rfe_instance( card, rx_hdl );
    status = rfe_config_custom_rx_filter( &rfe_instance, index, hpf_lpf_setting, rx_fltr1_sw,
                                          rfic_port );

    return status;
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_run_eeprom_wp_test( uint8_t card )
{
    int32_t status = 0;
    skiq_part_t part;
    uint8_t value[EEPROM_WP_SCRATCH_LENGTH], new_value[EEPROM_WP_SCRATCH_LENGTH], i;

    static_assert(EEPROM_WP_SCRATCH_LENGTH < UINT8_MAX, "EEPROM WP scratch length overflows loop variable");

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    part = _skiq_get_part( card );
    switch ( part )
    {
        case skiq_x4:
        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
        case skiq_nv100:
            status = 0;
            break;

        default:
            status = -ENOTSUP;
            break;
    }

    if ( status == 0 )
    {
        status = hal_read_eeprom( card, EEPROM_WP_SCRATCH_ADDR, value,
                                  EEPROM_WP_SCRATCH_LENGTH );
        if ( status == 0 )
        {
            for ( i = 0; i < EEPROM_WP_SCRATCH_LENGTH; i++)
            {
                new_value[i] = ~value[i];
            }
            status = hal_write_eeprom( card, EEPROM_WP_SCRATCH_ADDR, new_value,
                                       EEPROM_WP_SCRATCH_LENGTH );
        }

        if ( status == 0 )
        {
            status = hal_read_eeprom( card, EEPROM_WP_SCRATCH_ADDR, value,
                                      EEPROM_WP_SCRATCH_LENGTH );
        }

        if ( status == 0 )
        {
            /* consider it a failure if writing to the EEPROM succeeded.  The hal_write_eeprom()
             * function does not disable write protection. */
            if ( 0 == memcmp(value, new_value, EEPROM_WP_SCRATCH_LENGTH) )
            {
                status = -EFAULT;
            }
        }
    }

    return status;
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_flash_unique_id( uint8_t card, uint8_t *p_id, uint8_t *p_id_length )
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    static_assert(SKIQ_FACT_FLASH_MAX_UNIQUE_ID_LEN >= SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN,
        "SKIQ_FACT_FLASH_MAX_UNIQUE_ID_LEN must be at least SIDEKIQ_FLASH_MAX_UNIQUE_ID_LEN"
        " bytes long");

    return flash_get_unique_id(card, p_id, p_id_length);
}


/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_gpsdo_freq_accuracy( uint8_t card,
                                            double *p_ppm )
{
    int32_t freq_counter = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_ppm);

    return gpsdo_fpga_get_freq_accuracy( card, p_ppm, &freq_counter );
}

/**
    @brief  Enable the GPSDO control algorithm on the specified card
*/
int32_t skiq_fact_gpsdo_enable( uint8_t card )
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    return gpsdo_fpga_enable(card);
}

/**
    @brief  Disable the GPSDO control algorithm on the specified card
*/
int32_t skiq_fact_gpsdo_disable( uint8_t card )
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    return gpsdo_fpga_disable(card);
}

/**
    @brief  Check the enable status of the GPSDO control algorithm on the specified card
*/
int32_t skiq_fact_gpsdo_is_enabled( uint8_t card, bool *p_is_enabled )
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_is_enabled);

    return gpsdo_fpga_is_enabled(card, p_is_enabled);
}

#if (defined HAS_X4_SUPPORT)
/**************************************************************************************************/
/**
   @brief  Configures the card to accept a dev_clk input on the specified port and output
            the clock on the specified output port.

   @attention This is only supported on Sidekiq X4 revision C (::skiq_x4) and NV100 (::skiq_nv100)

   @param[in]   card        card number of the Sidekiq of interest
   @param[in]   in_port     [::skiq_rf_port_t] RF port in which the dev_clk is provided.
   @param[in]   out_port    [::skiq_rf_port_t] RF port in which the dev_clk is output.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers

   @see skiq_fact_enable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions
*/
static int32_t _enable_x4_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t port, skiq_rf_port_t out_port)
{
    int32_t status = 0;
    uint32_t tmp = 0;

    if (hw_rev_c != _skiq_get_hw_rev(card))
    {
        _skiq_log(SKIQ_LOG_ERROR, "Sidekiq X4 Rev C required. (card=%u)\n", card);
        status =  -ENOTSUP;
    }

    /* Configure GPIO for external dev_clk.  @todo, should this be done before/after 9528?

       PIN                  BITFIELD       OFFSET          VAL
    FMC_J6_SEL0         (FMC_EN_RXA1)   FMC_CTRL[0]         1
    FMC_J6_SEL1         (FMC_EN_RXA2)   FMC_CTRL[1]         0
    FMC_EXT_DEV_CLK_SEL (FMC_EN_ORX)    FMC_CTRL[2]         1
    FMC_VCXO_SEL        (VCXO_153M6_EN) FMC_CTRL[19]        1
    */

    if (status == 0)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_FMC_CTRL, &tmp);
    }

    if (status == 0)
    {
        RBF_SET(tmp, 1, X2_FMC_EN_RXA1_X4_FMC_J6_SEL0 );
        RBF_SET(tmp, 0, X2_FMC_EN_RXA2_X4_FMC_J6_SEL1 );
        RBF_SET(tmp, 1, X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL );
        RBF_SET(tmp, 1, VCXO_153M6_EN);
        status = sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_FMC_CTRL, tmp);
    }

    if (status == 0)
    {
        status = write_ad9528_regs(card, AD9528_factoryTestRegisters[IDX_DEVCLK_IN_ON_J5_REGS].p_registers,
                                     AD9528_factoryTestRegisters[IDX_DEVCLK_IN_ON_J5_REGS].numRegisters);
    }

    return status;
}

int32_t skiq_fact_count_sysref( uint8_t card, skiq_rf_port_t port, uint32_t ms, uint32_t * p_pulse_count)
{
    int32_t status = 0;
    uint32_t tmp = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_pulse_count);

    if ((hw_rev_c != _skiq_get_hw_rev(card)) || (skiq_x4 != _skiq_get_part(card)))
    {
        _skiq_log(SKIQ_LOG_ERROR, "Sidekiq X4 Rev C required. (card=%u)\n", card);
        status = -ENOTSUP;
    }

    if( status == 0 )
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_FMC_CTRL, &tmp);
    }

    /* Configure GPIO for external dev_clk.  @todo, should this be done before/after 9528?

       PIN                  BITFIELD       OFFSET          VAL
    FMC_J6_SEL0         (FMC_EN_RXA1)   FMC_CTRL[0]         0
    FMC_J6_SEL1         (FMC_EN_RXA2)   FMC_CTRL[1]         1
    FMC_EXT_DEV_CLK_SEL (FMC_EN_ORX)    FMC_CTRL[2]         0
    FMC_VCXO_SEL        (VCXO_153M6_EN) FMC_CTRL[19]        1
    */

    if (status == 0)
    {
        RBF_SET(tmp, 0, X2_FMC_EN_RXA1_X4_FMC_J6_SEL0 );
        RBF_SET(tmp, 1, X2_FMC_EN_RXA2_X4_FMC_J6_SEL1 );
        RBF_SET(tmp, 0, X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL );
        RBF_SET(tmp, 1, VCXO_153M6_EN);

        status = sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_FMC_CTRL, tmp);
    }

    if (status == 0)
    {
        status = write_ad9528_regs(card, AD9528_factoryTestRegisters[IDX_SYSREF_IN_ON_J6_REGS].p_registers,
                                         AD9528_factoryTestRegisters[IDX_SYSREF_IN_ON_J6_REGS].numRegisters);
    }

    if (status == 0)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_FMC_CTRL, &tmp);
    }

    // read the initial pulse count
    if (status == 0)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_TEST_READ, &tmp);
    }

    if (status == 0)
    {
        hal_millisleep(ms);
        status = sidekiq_fpga_reg_read(card, FPGA_REG_TEST_READ, p_pulse_count);
    }

    if (status == 0)
    {
        if (*p_pulse_count >= tmp)
        {
            *p_pulse_count = *p_pulse_count - tmp;
        }
        else
        {
            skiq_error("Underflow detected, attempting again on card %" PRIu8 ""
                          " (status %" PRIi32 ")\n", card, status);
            status = sidekiq_fpga_reg_read(card, FPGA_REG_TEST_READ, &tmp);
            if (status == 0)
            {
                hal_millisleep(ms);
                status = sidekiq_fpga_reg_read(card, FPGA_REG_TEST_READ, p_pulse_count);
            }
            if ((*p_pulse_count >= tmp) && (status == 0))
            {
                *p_pulse_count = *p_pulse_count - tmp;
            }
            else
            {
                status = -EBADMSG;
            }
        }
    }

    if (status != 0)
    {
        skiq_error("An error occured counting sysref pulses on card %" PRIu8 ""
                          " (status %" PRIi32 ")\n", card, status);
    }

    status = skiq_fact_disable_ext_dev_clk_and_clk_output(card,skiq_rf_port_J5,skiq_rf_port_J6 );
    if ( status != 0)
    {
        skiq_error("Failed to restore AD9528 and/or GPIO to the default configuration on card %" PRIu8 ""
                          " (status %" PRIi32 ")\n", card, status);
    }
    return status;

}

/**************************************************************************************************/
/**
   @brief  Disables the external dev_clock of a specific Sidekiq device

   @attention This is only supported on Sidekiq X4 revision C (::skiq_x4) and NV100 (::skiq_nv100)

   @param[in]  card        card number of the Sidekiq of interest
   @param[in]  in_port     [::skiq_rf_port_t] RF port in which the external dev clock input is to be disabled.
   @param[in]  out_port    [::skiq_rf_port_t] RF port in which the dev clock output is to be disabled.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -ENOTSUP    if the specified card is not supported, ie anything other than X4 Rev C
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers

   @see skiq_fact_disable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions

*/
static int32_t _disable_x4_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t in_port, skiq_rf_port_t out_port)
{
    int32_t status = 0;
    uint32_t tmp = 0;

    if (hw_rev_c != _skiq_get_hw_rev(card))
    {
        _skiq_log(SKIQ_LOG_ERROR, "Sidekiq X4 Rev C required. (card=%u)\n", card);
        status = -ENOTSUP;
    }

    if (status == 0)
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_FMC_CTRL, &tmp);
        if (status == 0)
        {
            RBF_SET(tmp, 0, X2_FMC_EN_RXA1_X4_FMC_J6_SEL0 );
            RBF_SET(tmp, 0, X2_FMC_EN_RXA2_X4_FMC_J6_SEL1 );
            RBF_SET(tmp, 0, X2_FMC_EN_ORX_X4_FMC_EXT_DEV_CLK_SEL );
            RBF_SET(tmp, 0, VCXO_153M6_EN);
        }
    }

    if (status == 0)
    {
        status = sidekiq_fpga_reg_write_and_verify(card,FPGA_REG_FMC_CTRL, tmp);
    }

    if (status == 0)
    {
        rfic_t rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_reset_and_init( &(rfic_instance) );
    }

    return (status);
}

/**************************************************************************************************/
/**
   @brief  Configures the card to accept a dev_clk input on the specified port and output
            the clock on the specified output port.

   @attention This is only supported on Sidekiq NV100 (::skiq_nv100)

   @param[in]   card        card number of the Sidekiq of interest
   @param[in]   in_port     [::skiq_rf_port_t] RF port in which the dev_clk is provided.
   @param[in]   out_port    [::skiq_rf_port_t] RF port in which the dev_clk is output.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers

   @see skiq_fact_enable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions
*/
static int32_t _enable_NV100_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t port, skiq_rf_port_t out_port)
{
    int32_t status = 0;

    status = pcal6524_io_exp_set_as_output(card, 0, 0x22, DEVCLK_SEL_N, DEVCLK_SEL_N);

    return status;
}

/**************************************************************************************************/
/**
   @brief  Disables the external dev_clock input by resetting digital GPIO controlling the external
            dev clock to its default state.

   @attention This is only supported on Sidekiq NV100 (::skiq_nv100)

   @param[in]  card        card number of the Sidekiq of interest
   @param[in]  in_port     [::skiq_rf_port_t] RF port in which the external dev clock input is to be disabled.
   @param[in]  out_port    [::skiq_rf_port_t] RF port in which the dev clock output is to be disabled.

   @return 0 on success, else a negative errno value
   @retval -ERANGE     if the specified card index is out of range
   @retval -ENODEV     if the specified card has not been initialized
   @retval -EBADMSG    if an error occurred transacting with FPGA/AD9528 registers

   @see skiq_fact_disable_ext_dev_clk_and_clk_output
   @ingroup factoryfunctions

*/
static int32_t _disable_NV100_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t in_port, skiq_rf_port_t out_port)
{
    int32_t status = 0;

    status = pcal6524_io_exp_set_as_output(card, 0, 0x22, DEVCLK_SEL_N, 0);

    return (status);
}

int32_t skiq_fact_enable_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t port, skiq_rf_port_t out_port)
{
    int32_t status = 0;
    skiq_part_t part;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    part = _skiq_get_part(card);

    if(part == skiq_x4)
    {
        status = _enable_x4_ext_dev_clk_and_clk_output(card, port, out_port);
    }
    else if(part == skiq_nv100)
    {
        status = _enable_NV100_ext_dev_clk_and_clk_output(card, port, out_port);
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}

int32_t skiq_fact_disable_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t in_port, skiq_rf_port_t out_port)
{
    int32_t status = 0;
    skiq_part_t part;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    part = _skiq_get_part(card);

    if(part == skiq_x4)
    {
        status = _disable_x4_ext_dev_clk_and_clk_output(card, in_port, out_port);
    }
    else if(part == skiq_nv100)
    {
        status = _disable_NV100_ext_dev_clk_and_clk_output(card, in_port, out_port);
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}
#else
int32_t skiq_fact_enable_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t port, skiq_rf_port_t out_port)
{
    return -ENOTSUP;
}
int32_t skiq_fact_disable_ext_dev_clk_and_clk_output( uint8_t card, skiq_rf_port_t in_port, skiq_rf_port_t out_port)
{
    return -ENOTSUP;
}

int32_t skiq_fact_count_sysref( uint8_t card, skiq_rf_port_t port, uint32_t ms, uint32_t * p_pulse_count)
{
    return -ENOTSUP;
}
#endif  /* HAS_X4_SUPPORT */


int32_t skiq_fact_read_board_rev(uint8_t card, uint8_t *p_board_rev)
{
    int32_t status = -EBADMSG;
    uint32_t val = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_board_rev);

    skiq_part_t part = _skiq_get_part(card);

    if (part == skiq_x4)
    {
        if ( sidekiq_fpga_reg_read( card, FPGA_REG_GPIO_READ, &val ) == 0 )
        {
            *p_board_rev = (BF_GET(val,31,1) << 1) | (BF_GET(val, 30,1));
            status = 0;
        }
    }
    else
    {
        status = fpga_ctrl_read_board_id( card, p_board_rev );
    }

    return status;
}


/**
    @brief  Perform a "soft reset" of the FPGA.
*/
int32_t skiq_fact_soft_reset_fpga(uint8_t card)
{
    int32_t status = 0;

    status = fpga_ctrl_issue_soft_reset(card);
    if (-ENOSYS == status)
    {
        skiq_warning("FPGA on card %" PRIu8 " doesn't support register read / write\n", card);
    }
    else if (-ENOTSUP == status)
    {
        skiq_warning("FPGA on card %" PRIu8 " doesn't support FPGA soft reset\n", card);
    }
    else if (0 != status)
    {
        skiq_warning("Failed to perform FPGA soft reset on card %" PRIu8 " (status = %" PRIi32
            ")\n", card, status);
    }

    return (status);
}

/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_write_hw_iface_vers( uint8_t card, uint8_t major_hw_iface_vers, uint8_t minor_hw_iface_vers, char *p_part_number, char *p_revision )
{
    int32_t status = 0;
    bool is_wp=false;
    uint8_t p_data[HW_IFACE_VERS_EEPROM_LEN] = {minor_hw_iface_vers, major_hw_iface_vers};

    skiq_part_t part = _skiq_get_part(card);
    if((part == skiq_mpcie) || (part == skiq_m2))
    {
        status = -ENOTSUP;
        skiq_error("Writing hardware interface version is not supported on card %" PRIu8 " (status = %" PRIi32 ")\n", card, status);
        return status;
    }
    
    status = _disable_eeprom_wp_for_part( card, p_part_number, p_revision, &is_wp );
    if( status == 0 )
    {
        status = hal_write_eeprom(card, HW_IFACE_VERS_EEPROM_ADDR, p_data, HW_IFACE_VERS_EEPROM_LEN);
    }

    if( is_wp == true )
    {
        int32_t tmp_status = card_set_eeprom_wp( card, true );
        if( tmp_status == 0 )
        {
            skiq_info("Successfully re-enabled EEPROM WP on card %" PRIu8 "\n", card);
        }
        else
        {
            skiq_warning("Failed to re-enable EEPROM WP (status = %" PRIi32 ") on card %"
                PRIu8 "\n", tmp_status, card);
        }
    }
    card_eeprom_unlock_and_enable_wp(card);

    return status;
}

/******************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t skiq_fact_read_hw_iface_vers( uint8_t card, uint8_t *p_major_hw_iface_vers, uint8_t *p_minor_hw_iface_vers)
{
    int32_t status = 0;
    uint8_t p_data[HW_IFACE_VERS_EEPROM_LEN];

    CHECK_NULL_PARAM(p_major_hw_iface_vers);
    CHECK_NULL_PARAM(p_minor_hw_iface_vers);

    skiq_part_t part = _skiq_get_part(card);
    if((part == skiq_mpcie) || (part == skiq_m2))
    {
        status = -ENOTSUP;
        skiq_error("Reading hardware interface version is not supported on card %" PRIu8 "(status = %" PRIi32 ")\n", card, status);
        return status;
    }

    status = hal_read_eeprom(card, HW_IFACE_VERS_EEPROM_ADDR, p_data, HW_IFACE_VERS_EEPROM_LEN);
    if ( status == 0 )
    {
        *p_minor_hw_iface_vers = p_data[0];
        *p_major_hw_iface_vers = p_data[1];
    }

    return status;
}