/**
 * @file   card_services_temperature_i2c.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Feb 27 09:35:10 CST 2020
 *
 * @brief  Provides temperature sensors access over I2C
 *
 *
 * <pre>
 * Copyright 2016-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <inttypes.h>

#include "card_services.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_fpga.h"
#include "ad9361_driver.h"

/* enable debug_print and debug_print_plain when DEBUG_TEMP_SENSOR_I2C is defined */
#if (defined DEBUG_TEMP_SENSOR_I2C)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** MACROS *****/

#define _SENSOR_LIST(_part,_nr_sensors,_addr_list,_bus) \
    {                                                   \
        .part = (_part),                                \
        .nr_sensors = (_nr_sensors),                    \
        .bus = (_bus),                                  \
        .addr_list = (_addr_list),                      \
    }
#define SENSOR_LIST(_part,_addr_list,_bus)                      \
    _SENSOR_LIST(_part,ARRAY_SIZE(_addr_list),_addr_list,_bus)
#define NO_SENSORS(_part)               _SENSOR_LIST(_part,0,NULL,0)


/***** DEFINES *****/

#define SKIQ_TEMP_I2C_ADDR              (0x70)
#define SKIQ_X2_TEMP_I2C_ADDR           (0x48)
#define SKIQ_X4_TEMP_I2C_ADDR           (0x49)
#define SKIQ_Z2P_FPGA_TEMP_I2C_ADDR     (0x70)
#define SKIQ_Z2P_TEMP_MEMS_I2C_ADDR     (0x71)
#define SKIQ_Z2P_TEMP_I2C_ADDR          (0x72)
#define SKIQ_Z3U_FPGA_TEMP_I2C_ADDR     (0x70)
#define SKIQ_Z3U_TEMP_MEMS_I2C_ADDR     (0x71)
#define SKIQ_Z3U_TEMP_I2C_ADDR          (0x72)

/** The TMP103 temperature sensor is used in all cases where the implementation
    provided here is used.  This define is used to know the register address
    from which to start reading the temperature sensor data.  Register address
    details can be found on Table 4: Pointer Address on page 17 of the TMP103
    datasheet: https://www.ti.com/lit/ds/symlink/tmp103.pdf?ts=1652094898589.
 */
#define TMP103_READ_REG_ADDR            (0x00)

/***** TYPEDEFS *****/


/***** STRUCTS *****/

struct sensor_list
{
    skiq_part_t part;
    uint8_t nr_sensors;
    uint8_t bus;
    const uint8_t *addr_list;
};


/***** LOCAL VARIABLES *****/

static const uint8_t default_sensors[] = { SKIQ_TEMP_I2C_ADDR };
static const uint8_t x2_sensors[] = { SKIQ_X2_TEMP_I2C_ADDR };
static const uint8_t x4_sensors[] = { SKIQ_X4_TEMP_I2C_ADDR };
static const uint8_t m2_2280_sensors[] = { 0x70, 0x71, 0x72 };
static const uint8_t z2p_sensors[] = { SKIQ_Z2P_FPGA_TEMP_I2C_ADDR,
                                       SKIQ_Z2P_TEMP_MEMS_I2C_ADDR,
                                       SKIQ_Z2P_TEMP_I2C_ADDR };
static const uint8_t z3u_sensors[] = { SKIQ_Z3U_FPGA_TEMP_I2C_ADDR,
                                       SKIQ_Z3U_TEMP_MEMS_I2C_ADDR,
                                       SKIQ_Z3U_TEMP_I2C_ADDR };

static const struct sensor_list sensor_lists[] = {
    SENSOR_LIST(skiq_mpcie,   default_sensors, 0),
    SENSOR_LIST(skiq_m2,      default_sensors, 0),
    SENSOR_LIST(skiq_z2,      default_sensors, 0),
    SENSOR_LIST(skiq_x2,      x2_sensors,      0),
    SENSOR_LIST(skiq_x4,      x4_sensors,      0),
    SENSOR_LIST(skiq_m2_2280, m2_2280_sensors, 2),
    SENSOR_LIST(skiq_z2p,     z2p_sensors,     2),
    SENSOR_LIST(skiq_z3u,     z3u_sensors,     2),
    NO_SENSORS(skiq_nv100),
};
static const uint8_t nr_sensor_lists = ARRAY_SIZE(sensor_lists);


/***** LOCAL FUNCTIONS *****/


/*************************************************************************************************/
/**
   @retval 0 Successful
   @retval -EINVAL sensor index out of range
   @retval -ENOTSUP no sensors for associated Sidekiq product
 */
static int32_t
i2c_sensor_addr( uint8_t card,
                 uint8_t sensor,
                 uint8_t *p_addr )
{
    skiq_part_t part = _skiq_get_part( card );
    int32_t status = -ENOTSUP;
    uint8_t i;

    for ( i = 0; i < nr_sensor_lists; i++ )
    {
        if ( ( part == sensor_lists[i].part ) && ( sensor_lists[i].addr_list != NULL ) )
        {
            if ( sensor < sensor_lists[i].nr_sensors )
            {
                status = 0;
                *p_addr = sensor_lists[i].addr_list[sensor];
                debug_print("Found sensor at index %" PRIu8 " (addr = 0x%02" PRIx8 ") for %s on "
                            "card %" PRIu8 "\n", i, *p_addr, part_cstr( part ), card);
            }
            else
            {
                status = -EINVAL;
                debug_print("Sensor index %" PRIu8 " out-of-range for %s on card %" PRIu8 "\n", i,
                            part_cstr( part ), card);
            }
            break;
        }
    }

    if ( status == -ENOTSUP )
    {
        debug_print("No sensors found for part %s on card %" PRIu8 "\n", part_cstr( part ), card);
    }

    return status;
}


/*************************************************************************************************/
/**
   @retval 0 Successful
   @retval -EINVAL sensor index out of range
   @retval -ENOTSUP no sensors for associated Sidekiq product
 */
static int32_t
i2c_sensor_bus( uint8_t card,
                uint8_t sensor,
                uint8_t *p_bus )
{
    skiq_part_t part = _skiq_get_part( card );
    int32_t status = -ENOTSUP;
    uint8_t i;

    for ( i = 0; i < nr_sensor_lists; i++ )
    {
        if ( part == sensor_lists[i].part )
        {
            status = 0;
            *p_bus = sensor_lists[i].bus;
            debug_print("Found sensor at index %" PRIu8 " (bus = 0x%02" PRIx8 ") for %s on "
                        "card %" PRIu8 "\n", i, *p_bus, part_cstr( part ), card);
            break;
        }
    }

    if ( status == -ENOTSUP )
    {
        debug_print("No sensors found for part %s on card %" PRIu8 "\n", part_cstr( part ), card);
    }

    return status;
}
/*************************************************************************************************/


/***** GLOBAL FUNCTIONS *****/


/*************************************************************************************************/
/**
     @retval 0 Success
     @retval -EINVAL Sensor index is out of range
     @retval -EIO I/O communication error occurred during measurement
     @retval -ENOTSUP no sensors for associated Sidekiq product
 */
int32_t
card_read_temp_i2c( uint8_t card,
                    uint8_t sensor,
                    int8_t *p_temp_in_deg_C )
{
    int32_t status;
    uint8_t i2c_addr, i2c_bus;

    status = i2c_sensor_addr( card, sensor, &i2c_addr );

    if ( status == 0 )
    {
        status = i2c_sensor_bus( card, sensor, &i2c_bus );
    }

    if ( status == 0 )
    {
        uint8_t tmp;

        status = hal_write_then_read_i2c_by_bus( card, i2c_bus, i2c_addr, TMP103_READ_REG_ADDR, &tmp, 1 );
        if ( status == 0 )
        {
            *p_temp_in_deg_C = (int8_t)(tmp);
        }
        else
        {
            /* squash non-zero return code from hal_write_then_read_i2c_by_bus to indicate generic I/O
             * communication error */
            status = -EIO;
        }
    }

    return (status);
}
