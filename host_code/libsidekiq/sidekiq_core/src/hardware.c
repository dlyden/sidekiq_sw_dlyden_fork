/**
 * @file   hardware.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Jun  9 09:56:49 2016
 * 
 * @brief  
 * 
 * <pre>
 * Copyright 2016-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/ 

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "hardware.h"
#include "sidekiq_types.h"
#include "sidekiq_private.h"
#include "bit_ops.h"            /* for BF_GET */

#include "inttypes.h"

/***** DEFINES *****/

/* bit offsets / length for hardware vers */
#define EEPROM_HW_VERS_OFFSET (0)
#define EEPROM_HW_VERS_LEN    (12)

#define EEPROM_PRODUCT_ID_OFFSET (12)
#define EEPROM_PRODUCT_ID_LEN    (4)

#define BOARD_REV_A ("A0")
#define BOARD_REV_B ("B0")
#define BOARD_REV_C ("C0")
#define BOARD_REV_D ("D0")
#define BOARD_REV_E ("E0")

#define VARIANT_LEGACY ("00")

// https://confluence.epiq.rocks/display/PROD/ES004+-+Sidekiq
#define PART_NUM_MPCIE_001 ("004202")
#define PART_NUM_MPCIE_002 ("004206")

// https://confluence.epiq.rocks/display/PROD/ES014+-+Sidekiq+M2
#define PART_NUM_M2_001 ("01420*") // 014-202, 014-203, 014-206, 014-207
#define PART_NUM_M2_002 ("01420*") // 014-205, 014-206

/** @brief  Valid part numbers for m.2-001; these should be used only with m.2 rev D and above */
#define PART_NUM_M2_001_1   ("014203")
#define PART_NUM_M2_001_2   ("014206")
#define PART_NUM_M2_001_3   ("014207")
/** @brief  Valid part numbers for m.2-002; these should be used only with m.2 rev D and above */
#define PART_NUM_M2_002_1   ("014200")
#define PART_NUM_M2_002_2   ("014205")

#define PART_NUM_X2 ("020201")

#define PART_NUM_Z2 ("023201")

#define PART_NUM_X4 ("024201")

#define PART_NUM_Z2P ("028201")

#define PART_NUM_Z3U ("032201")

/* https://confluence.epiq.rocks/display/PROD/ES025+-+Sidekiq+Stretch+-+M.2+2280+Card */
#define PART_NUM_M2_2280                "025201"

/* https://confluence.epiq.rocks/pages/viewpage.action?pageId=18809982 */
#define PART_NUM_NV100                  "035201"

const char* SKIQ_PART_NUM_STRING_MPCIE_001 = PART_NUM_MPCIE_001;
const char* SKIQ_PART_NUM_STRING_MPCIE_002 = PART_NUM_MPCIE_002;
const char* SKIQ_PART_NUM_STRING_M2 = PART_NUM_M2_001;
const char* SKIQ_PART_NUM_STRING_X2 = PART_NUM_X2;
const char* SKIQ_PART_NUM_STRING_Z2 = PART_NUM_Z2;
const char* SKIQ_PART_NUM_STRING_X4 = PART_NUM_X4;
const char* SKIQ_PART_NUM_STRING_M2_2280 = PART_NUM_M2_2280;
const char* SKIQ_PART_NUM_STRING_Z2P = PART_NUM_Z2P;
const char* SKIQ_PART_NUM_STRING_Z3U = PART_NUM_Z3U;
const char* SKIQ_PART_NUM_STRING_NV100 = PART_NUM_NV100;

/***** TYPEDEFS *****/

/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/
static void _card_map_rev_and_part( char *p_part_number, char *p_revision, skiq_part_t *p_part,
    skiq_hw_rev_t *p_hw_rev )
{
    *p_hw_rev = card_map_hw_rev(p_revision);
    *p_part = card_map_part(p_part_number);
}

/*****************************************************************************/

static bool _does_part_match( const char *p_part_number, const char *p_skiq_part_number )
{
    return( 0 == strncmp(p_part_number, p_skiq_part_number, SKIQ_PART_NUM_STRLEN) );
}


/***** GLOBAL FUNCTIONS *****/


skiq_hw_vers_t card_map_hw_vers( uint16_t hw_vers )
{
    skiq_hw_vers_t ret_hw_vers = skiq_hw_vers_mpcie_b;
    uint16_t masked_hw_vers = BF_GET(hw_vers, EEPROM_HW_VERS_OFFSET, EEPROM_HW_VERS_LEN);

    switch( masked_hw_vers )
    {
        case skiq_eeprom_hw_vers_mpcie_a:
            ret_hw_vers = skiq_hw_vers_mpcie_a;
            break;

        case skiq_eeprom_hw_vers_mpcie_b:
            ret_hw_vers = skiq_hw_vers_mpcie_b;
            break;

        case skiq_eeprom_hw_vers_mpcie_c:
            ret_hw_vers = skiq_hw_vers_mpcie_c;
            break;

        case skiq_eeprom_hw_vers_mpcie_d:
            ret_hw_vers = skiq_hw_vers_mpcie_d;
            break;

        /*
            mPCIe rev E should always masquerade as another HW version, so there is no entry here
            for it
        */

        case skiq_eeprom_hw_vers_m2_b:
            ret_hw_vers = skiq_hw_vers_m2_b;
            break;

        case skiq_eeprom_hw_vers_m2_c:
            ret_hw_vers = skiq_hw_vers_m2_c;
            break;

        /*
            m.2 rev D should always masquerade as another HW version, so there is no entry here
            for it
        */

        case skiq_eeprom_hw_vers_ext:
            ret_hw_vers = skiq_hw_vers_reserved;
            break;

        default:
            ret_hw_vers = skiq_hw_vers_invalid;
            break;
    }

    return ret_hw_vers;
}

skiq_product_t card_map_product_vers( uint16_t hardware )
{
    skiq_product_t ret_product = skiq_product_invalid;
    uint16_t product = BF_GET(hardware, EEPROM_PRODUCT_ID_OFFSET, EEPROM_PRODUCT_ID_LEN);

    switch( product )
    {
        case skiq_eeprom_product_mpcie_001:
            ret_product = skiq_product_mpcie_001;
            break;

        case skiq_eeprom_product_mpcie_002:
            ret_product = skiq_product_mpcie_002;
            break;

        case skiq_eeprom_product_m2_001:
            ret_product = skiq_product_m2_001;
            break;

        case skiq_eeprom_product_m2_002:
            ret_product = skiq_product_m2_002;
            break;

        case skiq_eeprom_product_ext:
            ret_product = skiq_product_reserved;
            break;

        default:
            ret_product = skiq_product_invalid;
            break;
    }
    return ret_product;
}

int32_t card_map_version( skiq_hw_vers_t hw_vers,
                          skiq_product_t product,
                          uint16_t *p_version )
{
    int32_t status=0;
    skiq_eeprom_hw_vers_t eeprom_hw=skiq_eeprom_hw_vers_invalid;
    skiq_eeprom_product_vers_t eeprom_product=skiq_eeprom_product_invalid;
    // make sure version is 0
    *p_version = 0;

    // Verify the hardware version is appropriate for the product
    if( (product == skiq_product_m2_001) ||
        (product == skiq_product_m2_002) )
    {
        if( (hw_vers != skiq_hw_vers_m2_b) &&
            (hw_vers != skiq_hw_vers_m2_c) &&
            (hw_vers != skiq_hw_vers_m2_d) )
        {
            skiq_unknown("Invalid / unsupported hardware revision (%u) specified for M.2", hw_vers);
            status = -1;
        }
    }
    else if( (product == skiq_product_mpcie_001) ||
             (product == skiq_product_mpcie_002) )
    {
        if( (hw_vers != skiq_hw_vers_mpcie_a) &&
            (hw_vers != skiq_hw_vers_mpcie_b) &&
            (hw_vers != skiq_hw_vers_mpcie_c) &&
            (hw_vers != skiq_hw_vers_mpcie_d) &&
            (hw_vers != skiq_hw_vers_mpcie_e) )
        {
            skiq_unknown("Invalid / unsupported hardware revision (%u) specified for mPCIe",
                hw_vers);
            status = -2;
        }
    }
    else if( (product == skiq_product_reserved) )
    {
        if( (hw_vers != skiq_hw_vers_reserved) )
        {
            skiq_unknown("Invalid / unsupported hardware revision (%u) specified for product",
                hw_vers);
            status = -3;
        }
    }

    switch( hw_vers )
    {
        case skiq_hw_vers_mpcie_a:
            eeprom_hw = skiq_eeprom_hw_vers_mpcie_a;
            break;

        case skiq_hw_vers_mpcie_b:
            eeprom_hw = skiq_eeprom_hw_vers_mpcie_b;
            break;

        case skiq_hw_vers_mpcie_c:
            eeprom_hw = skiq_eeprom_hw_vers_mpcie_c;
            break;

        case skiq_hw_vers_mpcie_d:
            eeprom_hw = skiq_eeprom_hw_vers_mpcie_d;
            break;

        /*
            mPCIe rev E should always masquerade as another HW version, so there is no entry here
            for it
        */

        case skiq_hw_vers_m2_b:
            eeprom_hw = skiq_eeprom_hw_vers_m2_b;
            break;

        case skiq_hw_vers_m2_c:
            eeprom_hw = skiq_eeprom_hw_vers_m2_c;
            break;

        /*
            m.2 rev D should always masquerade as another HW version, so there is no entry here
            for it
        */

        case skiq_hw_vers_reserved:
            eeprom_hw = skiq_eeprom_hw_vers_ext;
            break;

        default:
            skiq_unknown("Invalid / unsupported hardware revision (%u) specified", hw_vers);
            status = -4;
            break;
    }

    switch( product )
    {
        case skiq_product_mpcie_001:
            eeprom_product = skiq_eeprom_product_mpcie_001;
            break;

        case skiq_product_mpcie_002:
            eeprom_product = skiq_eeprom_product_mpcie_002;
            break;

        case skiq_product_m2_001:
            eeprom_product = skiq_eeprom_product_m2_001;
            break;

        case skiq_product_m2_002:
            eeprom_product = skiq_eeprom_product_m2_002;
            break;

        case skiq_product_reserved:
            eeprom_product = skiq_eeprom_product_ext;
            break;

        default:
            skiq_unknown("Invalid / unsupported product (%u) specified", product);
            status = -5;
            break;
    }

    // make sure we have valid mappings before setting value
    if( status==0 )
    {
        BF_SET( *(p_version), eeprom_hw, EEPROM_HW_VERS_OFFSET, EEPROM_HW_VERS_LEN );
        BF_SET( *(p_version), eeprom_product, EEPROM_PRODUCT_ID_OFFSET, EEPROM_PRODUCT_ID_LEN );
    }

    return (status);
}

int32_t card_map_legacy_part_info( skiq_hw_vers_t hw_vers,
                                   skiq_product_t product,
                                   char *p_part_number,
                                   char *p_revision,
                                   char *p_variant )
{
    int32_t status=0;

    // legacy variant is fixed
    strncpy( p_variant, (const char*)(&(VARIANT_LEGACY)), SKIQ_VARIANT_STRLEN );

    // map hardware revision
    if( (hw_vers == skiq_hw_vers_mpcie_a) )
    {
        strncpy( p_revision, (const char*)(&(BOARD_REV_A)), SKIQ_REVISION_STRLEN );
    }
    else if( (hw_vers == skiq_hw_vers_mpcie_b) ||
             (hw_vers == skiq_hw_vers_m2_b) )
    {
        strncpy( p_revision, (const char*)(&(BOARD_REV_B)), SKIQ_REVISION_STRLEN );
    }
    else if( (hw_vers == skiq_hw_vers_mpcie_c) ||
             (hw_vers == skiq_hw_vers_m2_c) )
    {
        strncpy( p_revision, (const char*)(&(BOARD_REV_C)), SKIQ_REVISION_STRLEN );
    }
    else if( (hw_vers == skiq_hw_vers_mpcie_d) ||
             (hw_vers == skiq_hw_vers_m2_d) )
    {
        strncpy( p_revision, (const char*)(&(BOARD_REV_D)), SKIQ_REVISION_STRLEN );
    }
    else if( (hw_vers == skiq_hw_vers_mpcie_e) )
    {
        strncpy( p_revision, (const char*)(&(BOARD_REV_E)), SKIQ_REVISION_STRLEN );
    }
    else
    {
        skiq_unknown("Invalid / unsupported hardware revision (%u) specified", hw_vers);
        status = -1;
    }

    // now map part number
    if( (product == skiq_product_mpcie_001) )
    {
        strncpy( p_part_number, (const char*)(&(PART_NUM_MPCIE_001)), SKIQ_PART_NUM_STRLEN );
    }
    else if( (product == skiq_product_mpcie_002) )
    {
        strncpy( p_part_number, (const char*)(&(PART_NUM_MPCIE_002)), SKIQ_PART_NUM_STRLEN );
    }
    else if( (product == skiq_product_m2_001) )
    {
        strncpy( p_part_number, (const char*)(&(PART_NUM_M2_001)), SKIQ_PART_NUM_STRLEN );
    }
    else if( (product == skiq_product_m2_002) )
    {
        strncpy( p_part_number, (const char*)(&(PART_NUM_M2_002)), SKIQ_PART_NUM_STRLEN );
    }
    else
    {
        skiq_unknown("Invalid / unsupported product (%u) specified", product);
        status = -2;
    }

    return (status);
}

void card_map_rev_and_part( skiq_hw_vers_t hw_vers,
                            char *p_part_number,
                            char *p_revision,
                            skiq_part_t *p_part,
                            skiq_hw_rev_t *p_hw_rev )
{
    *p_part = skiq_part_invalid;
    *p_hw_rev = hw_rev_invalid;

    switch( hw_vers )
    {
        case skiq_hw_vers_mpcie_a:
            *p_part = skiq_mpcie;
            *p_hw_rev = hw_rev_a;
            break;

        case skiq_hw_vers_mpcie_b:
            *p_part = skiq_mpcie;
            *p_hw_rev = hw_rev_b;
            break;

        case skiq_hw_vers_mpcie_c:
            *p_part = skiq_mpcie;
            *p_hw_rev = hw_rev_c;
            break;

        case skiq_hw_vers_mpcie_d:
            *p_part = skiq_mpcie;
            *p_hw_rev = hw_rev_d;
            break;

        case skiq_hw_vers_mpcie_e:
            *p_part = skiq_mpcie;
            *p_hw_rev = hw_rev_e;
            break;

        case skiq_hw_vers_m2_b:
            *p_part = skiq_m2;
            *p_hw_rev = hw_rev_b;
            break;

        case skiq_hw_vers_m2_c:
            *p_part = skiq_m2;
            *p_hw_rev = hw_rev_c;
            break;

        case skiq_hw_vers_m2_d:
            *p_part = skiq_m2;
            *p_hw_rev = hw_rev_d;
            break;

        case skiq_hw_vers_reserved:
            // we need to check the rev / part from strings
            _card_map_rev_and_part( p_part_number,
                                    p_revision,
                                    p_part,
                                    p_hw_rev );
            break;

        default:
            skiq_unknown("Invalid / unsupported hardware revision (%u) specified", hw_vers);
            break;
    }
}

skiq_hw_rev_t card_map_hw_rev( const char *p_revision )
{
    skiq_hw_rev_t hw_rev = hw_rev_invalid;
    char lowerRev = '\0';

    if( NULL == p_revision )
    {
        return hw_rev_invalid;
    }

    // figure out the revision...note: we don't really care about the minor number related
    // to the board rev for determining what version it is so we really only want to look at
    // the first character
    lowerRev = tolower(p_revision[0]);

    if(lowerRev == tolower(BOARD_REV_A[0]))
    {
        hw_rev = hw_rev_a;
    }
    else if(lowerRev == tolower(BOARD_REV_B[0]))
    {
        hw_rev = hw_rev_b;
    }
    else if(lowerRev == tolower(BOARD_REV_C[0]))
    {
        hw_rev = hw_rev_c;
    }
    else if(lowerRev == tolower(BOARD_REV_D[0]))
    {
        hw_rev = hw_rev_d;
    }
    else if(lowerRev == tolower(BOARD_REV_E[0]))
    {
        hw_rev = hw_rev_e;
    }
    else
    {
        skiq_unknown("Invalid / unsupported board revision detected: %s\n", p_revision);
        hw_rev = hw_rev_invalid;
    }

    return hw_rev;
}

skiq_part_t card_map_part( const char *p_part_number )
{
    skiq_part_t part = skiq_part_invalid;

    if( NULL == p_part_number )
    {
        return skiq_part_invalid;
    }

    if( (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_MPCIE_001)) ||
        (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_MPCIE_002)) )
    {
        part = skiq_mpcie;
    }
    else if( (_does_part_match(p_part_number, (const char *) PART_NUM_M2_001)) ||
             (_does_part_match(p_part_number, (const char *) PART_NUM_M2_002)) )
    {
        /**
            @TODO   these constants have "*" in them - will that cause any problems during the
                    compare?
        */
        part = skiq_m2;
    }
    else if( (_does_part_match(p_part_number, (const char *) PART_NUM_M2_001_1)) ||
             (_does_part_match(p_part_number, (const char *) PART_NUM_M2_001_2)) ||
             (_does_part_match(p_part_number, (const char *) PART_NUM_M2_001_3)) ||
             (_does_part_match(p_part_number, (const char *) PART_NUM_M2_002_1)) ||
             (_does_part_match(p_part_number, (const char *) PART_NUM_M2_002_2)) )
    {
        part = skiq_m2;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_X2) )
    {
        part = skiq_x2;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_Z2) )
    {
        part = skiq_z2;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_X4) )
    {
        part = skiq_x4;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_M2_2280) )
    {
        part = skiq_m2_2280;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_Z2P) )
    {
        part = skiq_z2p;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_Z3U) )
    {
        part = skiq_z3u;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_NV100) )
    {
        part = skiq_nv100;
    }
    else
    {
        skiq_unknown("Invalid / unsupported part number detected: %s\n", p_part_number);
        part = skiq_part_invalid;
    }

    return part;
}

skiq_product_t card_map_part_to_product( const char *p_part_number )
{
    skiq_product_t product = skiq_product_invalid;

    if( NULL == p_part_number )
    {
        return skiq_product_invalid;
    }

    if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_MPCIE_001) )
    {
        product = skiq_product_mpcie_001;
    }
    else if( _does_part_match(p_part_number, SKIQ_PART_NUM_STRING_MPCIE_002) )
    {
        product = skiq_product_mpcie_002;
    }
    else if( (_does_part_match(p_part_number, (const char *)PART_NUM_M2_001)) ||
             (_does_part_match(p_part_number, (const char *)PART_NUM_M2_001_1)) ||
             (_does_part_match(p_part_number, (const char *)PART_NUM_M2_001_2)) ||
             (_does_part_match(p_part_number, (const char *)PART_NUM_M2_001_3)) )
    {
        product = skiq_product_m2_001;
    }
    else if( (_does_part_match(p_part_number, (const char *)PART_NUM_M2_002)) ||
             (_does_part_match(p_part_number, (const char *)PART_NUM_M2_002_1)) ||
             (_does_part_match(p_part_number, (const char *)PART_NUM_M2_002_2)) )
    {
        product = skiq_product_m2_002;
    }
    else if( (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_X2)) ||
             (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_Z2)) ||
             (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_X4)) ||
             (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_M2_2280)) ||
             (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_Z2P)) ||
             (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_Z3U)) ||
             (_does_part_match(p_part_number, SKIQ_PART_NUM_STRING_NV100)) )
    {
        product = skiq_product_reserved;
    }

    return product;
}


