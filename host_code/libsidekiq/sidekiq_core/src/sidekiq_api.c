/*! \file sidekiq_api.c
 * \brief This file contains the implementation of libsidekiq.
 *
 * <pre>
 * Copyright 2013-2021 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 7/17/2013   MEZ   Created file
 *
 *</pre>*/

#include <pthread.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#include <dma_interface_api.h>

#include <ad9361_driver.h>
#include <sidekiq_api.h>
#include <sidekiq_api_factory.h>
#include <sidekiq_flash.h>
#include <sidekiq_hal.h>
#include <libsidekiq_debug.h>
#include <bit_ops.h>
#include <flash_defines.h>

#include "card_services.h"
#include "calibration.h"
#include "hardware.h"
#include "io_expander_cache.h"
#include "portable_endian.h"
#include "tune.h"
#include "sidekiq_xport.h"
#include "sidekiq_private.h"
#include "sidekiq_card_mgr.h"
#include "sidekiq_fpga_ctrl_decimator.h"
#include "sidekiq_rpc_client_api.h"
#include "hal_nv100.h"
#include "io_m2_2280.h"

#include "dropkiq_api.h"

#include <assert.h>

#ifndef __cplusplus
/* we need to make sure that the error always matches up correctly */
#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#if (GCC_VERSION >= 40600)
static_assert( SKIQ_MAX_NUM_CARDS==DKIQ_MAX_SUPPORTED_CARDS, "Dropkiq max supported cards mismatch");
static_assert( SKIQ_TX_ASYNC_SEND_QUEUE_FULL==DMA_INTERFACE_STATUS_SEND_QUEUE_FULL, "send queue full error code mismatch");
static_assert( SKIQ_TX_MAX_NUM_THREADS==DMA_MAX_NUM_SEND_THREADS, "max send threads mismatch");
static_assert( SKIQ_TX_MIN_NUM_THREADS==DMA_MIN_NUM_SEND_THREADS, "min send threads mismatch");
static_assert( SKIQ_MIN_RX_GAIN==AD9361_MIN_RX_GAIN, "minimum receive gain mismatch");
static_assert( SKIQ_MAX_RX_GAIN==AD9361_MAX_RX_GAIN, "maximum receive gain mismatch");
static_assert( (int32_t)(skiq_rx_status_success)==(int32_t)(DMA_INTERFACE_STATUS_SUCCESSFUL), "dma success mismatch");
static_assert( (int32_t)(skiq_rx_status_no_data)==(int32_t)(DMA_INTERFACE_STATUS_NO_DATA), "dma no data mismatch");
static_assert( (int32_t)(skiq_rx_status_error_generic)==(int32_t)(DMA_INTERFACE_ERR_RX_ERROR), "dma rx error mismatch");
static_assert( (int32_t)(skiq_rx_status_error_overrun)==(int32_t)(DMA_INTERFACE_ERR_DMA_OVERRUN_ERROR), "dma overrun mismatch");
static_assert( (int32_t)(skiq_rx_status_error_packet_malformed)==(int32_t)(DMA_INTERFACE_ERR_PACKET_MALFORMED_ERROR), "dma packet malformed mismatch");
static_assert( (int32_t)(skiq_tx_transfer_mode_sync)==(int32_t)(dma_interface_send_mode_sync), "dma interface sync mode mismatch" );
static_assert( (int32_t)(skiq_tx_transfer_mode_async)==(int32_t)(dma_interface_send_mode_async), "dma interface async mode mismatch" );
static_assert( SKIQ_MAX_NUM_TX_QUEUED_PACKETS==DMA_MAX_SEND_QUEUE_SIZE, "dma interface send queue mismatch" );
#endif
#endif

/***** DEFINES *****/

#define LOCK_REGS(card)                         CARD_LOCK(lock_regs,card)
#define UNLOCK_REGS(card)                       CARD_UNLOCK(lock_regs,card)
#define UNLOCK_REGS_AND_RETURN(card,val)        CARD_UNLOCK_AND_RETURN(lock_regs,card,val)

#define LOCK_RX(card)                           CARD_LOCK(lock_rx,card)
#define UNLOCK_RX(card)                         CARD_UNLOCK(lock_rx,card)
#define UNLOCK_RX_AND_RETURN(card,val)          CARD_UNLOCK_AND_RETURN(lock_rx,card,val)

#define LOCK_TX(card)                           CARD_LOCK(lock_tx,card)
#define UNLOCK_TX(card)                         CARD_UNLOCK(lock_tx,card)
#define UNLOCK_TX_AND_RETURN(card,val)          CARD_UNLOCK_AND_RETURN(lock_tx,card,val)

#define DEFAULT_TX_BLOCK_SIZE (1020) // need 4 words for the header

#define DEFAULT_TX_NUM_THREADS (4)   // # threads to use for Tx async by default

#define DEFAULT_TEMP_SENSOR_INDEX       0 /* Not all products have more than one temperature
                                           * sensors.  For now, this defines which temperature
                                           * sensor to offer through skiq_read_temp() API
                                           * function */

#define MAX_LOG_MSG_SIZE (500) // arbitrarily chosen constant...should be tons of room

// utility macro that converts to a string literal
#define SKIQ_GEN_STRING(STRING)         [STRING] = #STRING

#define MIN_FPGA_VERS_TIMESTAMP_BASE        FPGA_VERSION(3,15,1)
#define MIN_FPGA_VERS_TIMESTAMP_BASE_STR    FPGA_VERSION_STR(3,15,1)
#define FPGA_INACCESSIBLE_VERSION_NUM       FPGA_VERSION_STR(0,0,0)

/**
    @note   This should be a "static const char *" but it seems that the logging macros
            aren't happy with that... so we'll just use a macro.
*/
#define NOT_INITIALIZED_MESSAGE \
    "libsidekiq not yet initialized; please call skiq_init(), skiq_init_without_cards(), or" \
    " skiq_init_by_serial_str() first\n"

#define FPGA_NOT_ACCESSIBLE \
    "Unexpected FPGA version " FPGA_INACCESSIBLE_VERSION_NUM " has been returned." \
    " This may mean that libsidekiq could not communicate with the FPGA." \
    " Verify the expected bitstream is loaded onto the device and" \
    " that kernel modules are loaded on the host (if applicable).\n"

/***** STRUCTS / ENUMS *****/
/**
    @brief  State enumeration type to track the initialization state of a card.

    As a card is being initialized, a state tracking variable should be updated with an increasing
    initialization state (as initialization steps are completed). If the card then needs to be
    uninitialized somewhere during a complete initialization, the preceding state values can be used
    to only uninitialize what's needed. Therefore, the ordering of these states is important here
    and when uninitializing.
*/
enum CardInitState {
    NotInitialized = 0,
    Finished,
    CardLocked,
    CardInitialized,
    FwInitialized,
    FpgaInitialized,
    FlashInitialized,
    RfBasicInitialized,
    RfFullInitialized,
};

/***** LOCAL DATA *****/
skiq_sys skiq = SKIQ_INITIALIZER; /* cannot make this static here because sidekiq_api_factory.c needs it.  fortunately build.git hides it at a higher layer */

uint8_t _fpga_num_tx_chans[] = {0, 1, 2, 0}; // last entry is invalid so report 0 chans
uint8_t _fpga_num_rx_chans[] = {0, 1, 2, 0}; // last 2 entries really isn't valid for Sidekiq...

// mapping of syslog priorities to strings to print
#ifndef __MINGW32__
const char* PRIORITY_STR[]  =
{
    "EMERGENCY",
    "ALERT",
    "CRITICAL",
    "ERROR",
    "WARNING",
    "NOTICE",
    "INFO",
    "DEBUG"
};
#else
/* TODO hack - this should be defined with the logger, not in this file
 * and note that the order differs for the win64 build using __MINGW32__
 * Also, we've included enough entries to match the syslog values used
 * though the os_logger only has five levels */
const char* PRIORITY_STR[]  =
{
    "FINEST",
    "FINE",
    "INFO",
    "WARNING",
    "ERROR",
    "-5-",
    "-6-",
    "-7-"
};
#endif /* __MINGW32__ */

/* mutex for setting and using the skiq.log_msg_cb function pointer */
pthread_mutex_t log_callback_mutex = PTHREAD_MUTEX_INITIALIZER;
const char* SKIQ_LOGNAME = "SKIQ";

// array of filter strings (to be indexed by skiq_filt_t
const char* SKIQ_FILT_STRINGS[skiq_filt_max] =
{
    SKIQ_GEN_STRING(skiq_filt_0_to_3000_MHz),
    SKIQ_GEN_STRING(skiq_filt_3000_to_6000_MHz),
    SKIQ_GEN_STRING(skiq_filt_0_to_440MHz),
    SKIQ_GEN_STRING(skiq_filt_440_to_6000MHz),
    SKIQ_GEN_STRING(skiq_filt_440_to_580MHz),
    SKIQ_GEN_STRING(skiq_filt_580_to_810MHz),
    SKIQ_GEN_STRING(skiq_filt_810_to_1170MHz),
    SKIQ_GEN_STRING(skiq_filt_1170_to_1695MHz),
    SKIQ_GEN_STRING(skiq_filt_1695_to_2540MHz),
    SKIQ_GEN_STRING(skiq_filt_2540_to_3840MHz),
    SKIQ_GEN_STRING(skiq_filt_3840_to_6000MHz),
    SKIQ_GEN_STRING(skiq_filt_0_to_300MHz),
    SKIQ_GEN_STRING(skiq_filt_300_to_6000MHz),
    SKIQ_GEN_STRING(skiq_filt_50_to_435MHz),
    SKIQ_GEN_STRING(skiq_filt_435_to_910MHz),
    SKIQ_GEN_STRING(skiq_filt_910_to_1950MHz),
    SKIQ_GEN_STRING(skiq_filt_1950_to_6000MHz),
    SKIQ_GEN_STRING(skiq_filt_0_to_6000MHz),
    SKIQ_GEN_STRING(skiq_filt_390_to_620MHz),
    SKIQ_GEN_STRING(skiq_filt_540_to_850MHz),
    SKIQ_GEN_STRING(skiq_filt_770_to_1210MHz),
    SKIQ_GEN_STRING(skiq_filt_1130_to_1760MHz),
    SKIQ_GEN_STRING(skiq_filt_1680_to_2580MHz),
    SKIQ_GEN_STRING(skiq_filt_2500_to_3880MHz),
    SKIQ_GEN_STRING(skiq_filt_3800_to_6000MHz),

    SKIQ_GEN_STRING(skiq_filt_47_to_135MHz),
    SKIQ_GEN_STRING(skiq_filt_135_to_145MHz),
    SKIQ_GEN_STRING(skiq_filt_145_to_150MHz),
    SKIQ_GEN_STRING(skiq_filt_150_to_162MHz),
    SKIQ_GEN_STRING(skiq_filt_162_to_175MHz),
    SKIQ_GEN_STRING(skiq_filt_175_to_190MHz),
    SKIQ_GEN_STRING(skiq_filt_190_to_212MHz),
    SKIQ_GEN_STRING(skiq_filt_212_to_230MHz),
    SKIQ_GEN_STRING(skiq_filt_230_to_280MHz),
    SKIQ_GEN_STRING(skiq_filt_280_to_366MHz),
    SKIQ_GEN_STRING(skiq_filt_366_to_475MHz),
    SKIQ_GEN_STRING(skiq_filt_475_to_625MHz),
    SKIQ_GEN_STRING(skiq_filt_625_to_800MHz),
    SKIQ_GEN_STRING(skiq_filt_800_to_1175MHz),
    SKIQ_GEN_STRING(skiq_filt_1175_to_1500MHz),
    SKIQ_GEN_STRING(skiq_filt_1500_to_2100MHz),
    SKIQ_GEN_STRING(skiq_filt_2100_to_2775MHz),
    SKIQ_GEN_STRING(skiq_filt_2775_to_3360MHz),
    SKIQ_GEN_STRING(skiq_filt_3360_to_4600MHz),
    SKIQ_GEN_STRING(skiq_filt_4600_to_6000MHz),

    SKIQ_GEN_STRING(skiq_filt_30_to_450MHz),
    SKIQ_GEN_STRING(skiq_filt_450_to_600MHz),
    SKIQ_GEN_STRING(skiq_filt_600_to_800MHz),
    SKIQ_GEN_STRING(skiq_filt_800_to_1200MHz),
    SKIQ_GEN_STRING(skiq_filt_1200_to_1700MHz),
    SKIQ_GEN_STRING(skiq_filt_1700_to_2700MHz),
    SKIQ_GEN_STRING(skiq_filt_2700_to_3600MHz),
    SKIQ_GEN_STRING(skiq_filt_3600_to_6000MHz),
};

// array of RF port strings (to be indexed by skiq_rf_port_t
const char* SKIQ_RF_PORT_STRINGS[skiq_rf_port_max] =
{
    SKIQ_GEN_STRING(skiq_rf_port_J1),
    SKIQ_GEN_STRING(skiq_rf_port_J2),
    SKIQ_GEN_STRING(skiq_rf_port_J3),
    SKIQ_GEN_STRING(skiq_rf_port_J4),
    SKIQ_GEN_STRING(skiq_rf_port_J5),
    SKIQ_GEN_STRING(skiq_rf_port_J6),
    SKIQ_GEN_STRING(skiq_rf_port_J7),
    SKIQ_GEN_STRING(skiq_rf_port_J300),
    SKIQ_GEN_STRING(skiq_rf_port_Jxxx_RX1),
    SKIQ_GEN_STRING(skiq_rf_port_Jxxx_TX1RX2),
    SKIQ_GEN_STRING(skiq_rf_port_J8),
};

const char* RF_PORT_INVALID = "RF_PORT_INVALID";

const char* SKIQ_RX_STREAM_MODE_STRINGS[skiq_rx_stream_mode_end] =
{
    SKIQ_GEN_STRING(skiq_rx_stream_mode_high_tput),
    SKIQ_GEN_STRING(skiq_rx_stream_mode_low_latency),
    SKIQ_GEN_STRING(skiq_rx_stream_mode_balanced),
};

/* three mutexes are used to prevent multiple threads within
   a host app from accessing libsidekiq independently.  These mutexes must
   protect entire sequences of operations, since we want API calls to
   run atomically (as many of them perform multiple read/writes to FPGA
   registers, and we want to ensure a give API call completes in its
   entirety before another one begins) */
static pthread_mutex_t lock_regs[SKIQ_MAX_NUM_CARDS] = {
#ifdef __linux
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP,
#elif _WIN32
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER,
#else
#error target OS not supported cleanly!
#endif
};
static MUTEX_ARRAY(lock_rx, SKIQ_MAX_NUM_CARDS);
static MUTEX_ARRAY(lock_tx, SKIQ_MAX_NUM_CARDS);

/* mutex for setting and using the skiq.use_exit_handler flag */
pthread_mutex_t use_exit_handler_mutex = PTHREAD_MUTEX_INITIALIZER;

static bool exit_handler_initialized = false;

/***** LOCAL FUNCTIONS *****/
static void init_rx_interfaces(uint8_t card);
static void pause_all_rx_streams(uint8_t card);
static void resume_and_flush_all_rx_streams(uint8_t card);
static int32_t start_tx_streaming(uint8_t card, skiq_tx_hdl_t hdl,
                                  bool wait_for_pps, uint64_t sys_timestamp);
static int32_t _stop_rx_streaming_multi(uint8_t card, skiq_rx_hdl_t handles[], uint8_t nr_handles,
                                        skiq_trigger_src_t trigger, uint64_t sys_timestamp);

static int32_t get_fpga_num_chans( uint8_t card, uint8_t *p_num_rx_chans, uint8_t *p_num_tx_chans );

static int32_t tx_late_timestamps_supported( uint8_t card, skiq_tx_hdl_t hdl,
        bool *supported);
static int32_t configure_tx_late_timestamp_mode( uint8_t card,
        skiq_tx_hdl_t hdl);

static int32_t _skiq_probe( void );
static int32_t _skiq_init_card( uint8_t card,
                                skiq_xport_type_t type,
                                skiq_xport_init_level_t level );
static int32_t _skiq_init_fw(uint8_t card);
static int32_t _skiq_init_fpga(uint8_t card, skiq_xport_init_level_t level);
static int32_t _skiq_init_rf_basic(uint8_t card);
static int32_t _skiq_init_rf_full(uint8_t card);
static int32_t _skiq_init_rx_hdl(uint8_t card, skiq_rx_hdl_t hdl);
static int32_t _skiq_init_tx_hdl(uint8_t card, skiq_tx_hdl_t hdl);
static int32_t _skiq_exit_card(uint8_t card);
static int32_t _skiq_exit_fw(uint8_t card);
static int32_t _skiq_exit_fpga(uint8_t card);
static int32_t _skiq_exit_rf_basic(uint8_t card);
static int32_t _skiq_exit_rf_full(uint8_t card);

static int32_t _skiq_stop_streaming(uint8_t card);
static int32_t _skiq_disable_card( uint8_t card );

static int32_t _serial_num_to_serial_str(uint32_t num, char* str)
{
    int32_t status = 0;
    uint8_t n = 0;
    uint8_t s = 0;

    if( num == INVALID_SERIAL_NUM )
    {
        status = -EINVAL;
    }
    else if( num < SKIQ_FACT_SERIAL_NUM_LEGACY_MAX )
    {
        // Legacy serial numbers were stored as 16 bit integers.
        snprintf(str, SKIQ_SERIAL_NUM_STRLEN, "%d", num);
    }
    else
    {
        // Null terminate the 4 byte alphanumeric serial number.
        str[SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN] = '\0';
        for( n = 0;
             (n < SKIQ_FACT_SERIAL_NUM_ALPHANUMERIC_LEN) && (0 == status);
             n++ )
        {
            // Calculate shift value needed to pull out the next character.
            s = (3 - n) * 8;
            str[n] = (num >> s) & 0xFF;
            if( 0 == isalnum(str[n]) )
            {
                status = -EINVAL;
            }
        }
    }

    if( 0 != status )
    {
        // Set to defined invalid value.
        snprintf(str,
                 SKIQ_SERIAL_NUM_STRLEN,
                 "%s",
                 SKIQ_FACT_SERIAL_NUM_INVALID_STR);
    }

    return status;
}

/*****************************************************************************/
/**
    @brief  A function to be called on normal program termination.
*/
static void skiq_shutdown_handler(void)
{
    if (skiq.initialized)
    {
        skiq_exit();
    }
}


/**************************************************************************************************/
/**
   @brief Get the number of valid receive channels associated with the specified Sidekiq card
*/
uint8_t _skiq_get_nr_rx_hdl( uint8_t card )
{
    return skiq.card[card].rf_params.num_rx_channels;
}


/**************************************************************************************************/
/**
   @brief Get the 'idx-th' receive handle associated with the specified Sidekiq card
*/
skiq_rx_hdl_t _skiq_get_rx_hdl( uint8_t card,
                                uint8_t idx )
{
    skiq_rx_hdl_t hdl = skiq_rx_hdl_end;

    if ( idx < skiq_rx_hdl_end )
    {
        hdl = skiq.card[card].rf_params.rx_handles[idx];
    }

    return hdl;
}


/**************************************************************************************************/
/**
   @brief Get the number of valid transmit channels associated with the specified Sidekiq card
*/
uint8_t _skiq_get_nr_tx_hdl( uint8_t card )
{
    return skiq.card[card].rf_params.num_tx_channels;
}


/**************************************************************************************************/
/**
   @brief Get the 'idx-th' transmit handle associated with the specified Sidekiq card
*/
static skiq_tx_hdl_t _skiq_get_tx_hdl( uint8_t card,
                                       uint8_t idx )
{
    skiq_tx_hdl_t hdl = skiq_tx_hdl_end;

    if ( idx < skiq_tx_hdl_end )
    {
        hdl = skiq.card[card].rf_params.tx_handles[idx];
    }

    return hdl;
}

/*****************************************************************************/
/** The init_rx_interfaces function is responsible for performing all
    initialization of the Rx interfaces.  Sane defaults should be available
    for all Rx interface parameters by the end of this call.

    @param card card index of the Sidekiq of interest
    @return void
*/
static void init_rx_interfaces(uint8_t card)
{
    skiq_rx_hdl_t rx_hdl;
    skiq_tx_hdl_t tx_hdl;
    uint8_t num_rx_chans = _skiq_get_nr_rx_hdl(card);
    uint8_t num_tx_chans = _skiq_get_nr_tx_hdl(card);
    uint8_t hdl_idx = 0;

    TRACE();

    LOCK_REGS(card);

    /* default to single channel mode */
    skiq_write_chan_mode(card, skiq_chan_mode_single);

    /* configure all available RX handles */
    for (hdl_idx = 0; hdl_idx < num_rx_chans; hdl_idx++)
    {
        rx_hdl = _skiq_get_rx_hdl(card, hdl_idx);
        if ( rx_hdl < skiq_rx_hdl_end )
        {
            _skiq_init_rx_hdl(card, rx_hdl);
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, num_rx_chans, hdl_idx);
        }
    }

    for (hdl_idx = 0; hdl_idx < num_tx_chans; hdl_idx++)
    {
        /* default to not streaming and the transfer mode sync */
        tx_hdl = _skiq_get_tx_hdl(card, hdl_idx);
        if ( tx_hdl < skiq_tx_hdl_end )
        {
            skiq.card[card].tx_list[tx_hdl].streaming = false;
            skiq.card[card].tx_list[tx_hdl].tx_transfer_mode = skiq_tx_transfer_mode_sync;
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds transmit handle for card %" PRIu8
                       " (number of TX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, num_tx_chans, hdl_idx);
        }
    }
    UNLOCK_REGS(card);
}


/*****************************************************************************/
/** calculate an aggregate data rate based on the number of active RX handles
 * and packed mode
 */
static void
rx_configure( uint8_t card )
{
    uint32_t aggregate_data_rate = 0, data_rate;
    uint8_t rx_chan_idx;
    uint32_t sample_rate;
    double actual_rate;
    rfic_t rfic_instance;

    /* iterate over active channels and aggregate the sample rate */
    for ( rx_chan_idx = 0; rx_chan_idx < skiq_rx_hdl_end; rx_chan_idx++ )
    {
        if ( skiq.card[card].rx_list[rx_chan_idx].streaming )
        {
            rfic_instance = _skiq_get_rx_rfic_instance( card, rx_chan_idx );
            rfic_read_rx_sample_rate( &rfic_instance, &sample_rate, &actual_rate );

            /* data rate is sample rate x 4 and accounting for header overhead */
            data_rate = sample_rate * sizeof(uint32_t) * (SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS / (SKIQ_MAX_RX_BLOCK_SIZE_IN_WORDS - SKIQ_RX_HEADER_SIZE_IN_WORDS));
 
            /* adjust data rate based on packed mode (more samples per byte) */
            if ( skiq.card[card].iq_packed )
            {
                aggregate_data_rate += SKIQ_NUM_PACKED_SAMPLES_IN_BLOCK( data_rate );
            }
            else
            {
                aggregate_data_rate += data_rate ;
            }
        }
    }

    skiq_xport_rx_configure( card, aggregate_data_rate );
}

/*****************************************************************************/
/** Checks a specified Rx handle of a Sidekiq card to see if it is currently
    streaming.

    @param card     card ID of Sidekiq of interest
    @param hdl      receive handle
    @return bool    true if streaming, false otherwise
*/
static bool is_rx_hdl_streaming( uint8_t card,
                                 skiq_rx_hdl_t hdl )
{
    return skiq.card[card].rx_list[hdl].streaming;
}

/*****************************************************************************/
/** Checks each Rx interface of a Sidekiq card to see if it is currently
    streaming.

    @param card     card ID of Sidekiq of interest
    @return bool    true if streaming, false otherwise
*/
static bool is_rx_streaming( uint8_t card )
{
    uint8_t n = 0;
    uint8_t nr_hdls = _skiq_get_nr_rx_hdl( card );

    for ( n = 0; n < nr_hdls; n++ )
    {
        skiq_rx_hdl_t hdl = _skiq_get_rx_hdl( card, n );

        if ( hdl < skiq_rx_hdl_end )
        {
            if ( is_rx_hdl_streaming( card, hdl ) )
            {
                return (true);
            }
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, nr_hdls, n);
            return (false);
        }
    }

    return (false);
}

/*****************************************************************************/
/** Checks a specified Tx handle of a Sidekiq card to see if it is currently
    streaming.

    @param card     card ID of Sidekiq of interest
    @param hdl      receive handle
    @return bool    true if streaming, false otherwise
*/
static bool is_tx_hdl_streaming( uint8_t card,
                                 skiq_tx_hdl_t hdl )
{
    return skiq.card[card].tx_list[hdl].streaming;
}

/*****************************************************************************/
/** Checks each Tx interface of a Sidekiq card to see if it is currently
    streaming.

    @param card     card ID of Sidekiq of interest
    @return bool    true if streaming, false otherwise
*/
static bool is_tx_streaming( uint8_t card )
{
    uint8_t n = 0;
    uint8_t nr_hdls = _skiq_get_nr_tx_hdl( card );

    for ( n = 0; n < nr_hdls; n++ )
    {
        skiq_tx_hdl_t hdl = _skiq_get_tx_hdl( card, n );

        if ( hdl < skiq_tx_hdl_end )
        {
            if ( is_tx_hdl_streaming( card, hdl ) )
            {
                return (true);
            }
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds transmit handle for card %" PRIu8
                       " (number of TX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, nr_hdls, n);
            return false;
        }
    }

    return (false);
}

/*****************************************************************************/
/** Checks each Rx and Tx handle for a given card to see if either is currently
    streaming.

    @param card     card ID of Sidekiq of interest
    @return bool    true if streaming, false otherwise
*/
static bool is_streaming( uint8_t card )
{
    if( true == is_rx_streaming(card) )
    {
        return (true);
    }
    else if( true == is_tx_streaming(card) )
    {
        return (true);
    }

    return (false);
}


int32_t _skiq_validate_rx_hdl(uint8_t card, skiq_rx_hdl_t hdl)
{
    int32_t status=0;

    // Note: we're assuming that the card has already been validated prior to this call...

    if ( hdl >= skiq_rx_hdl_end )
    {
        status = -EINVAL;
    }
    else
    {
        uint8_t hdl_idx, found = UINT8_MAX;

        for (hdl_idx = 0; ( hdl_idx < _skiq_get_nr_rx_hdl( card ) ) && ( found == UINT8_MAX ); hdl_idx++ )
        {
            if ( hdl == _skiq_get_rx_hdl( card, hdl_idx ) )
            {
                found = hdl_idx;
            }
        }

        if ( found == UINT8_MAX )
        {
            status = -EDOM;
        }
    }

    return (status);
}


int32_t _skiq_validate_tx_hdl(uint8_t card, skiq_tx_hdl_t hdl)
{
    int32_t status=0;

    // Note: we're assuming that the card has already been validated prior to this call...

    if ( hdl >= skiq_tx_hdl_end )
    {
        status = -EINVAL;
    }
    else
    {
        uint8_t hdl_idx, found = UINT8_MAX;

        for (hdl_idx = 0; ( hdl_idx < _skiq_get_nr_tx_hdl( card ) ) && ( found == UINT8_MAX ); hdl_idx++ )
        {
            if ( hdl == _skiq_get_tx_hdl( card, hdl_idx ) )
            {
                found = hdl_idx;
            }
        }

        if ( found == UINT8_MAX )
        {
            status = -EDOM;
        }
    }

    return (status);
}


static int32_t probe_hw_info_warp_capabilities( skiq_part_t part_type,
                                                skiq_rf_param_t *p_rf_params )
{
    int32_t status = 0;

    /* LIB-146: Set the Sidekiq card's warp value capabilities */
    switch ( part_type )
    {
        case skiq_mpcie:
        case skiq_m2:
        case skiq_z2:
            /* Sidekiq mPCIe, M.2, and Z2 use AD9361/4's AUXDAC1 */
            p_rf_params->warp_value_min = 0;
            p_rf_params->warp_value_max = 1023;
            p_rf_params->warp_value_unit = 13.07;
            break;

        case skiq_x2:
            /* Sidekiq X2 uses an AD5660, but production data shows a consistent difference
             * in the ppb/value from X4 */
            p_rf_params->warp_value_min = 0;
            p_rf_params->warp_value_max = 65535;
            p_rf_params->warp_value_unit = 0.29;
            break;

        case skiq_x4:
            /* Sidekiq X4 uses an AD5660 */
            p_rf_params->warp_value_min = 0;
            p_rf_params->warp_value_max = 65535;
            p_rf_params->warp_value_unit = 0.27;
            break;

        case skiq_z2p:
            /* Sidekiq Z2+ uses an AD5693 */
            p_rf_params->warp_value_min = 0;
            p_rf_params->warp_value_max = 65535;
            p_rf_params->warp_value_unit = 0.27;
            break;

        case skiq_m2_2280:
        case skiq_z3u:
        case skiq_nv100:
            /* Sidekiq Stretch, Z3u, and NV100 use the SiT5356 */
            p_rf_params->warp_value_min = 0;
            p_rf_params->warp_value_max = 65535;
            p_rf_params->warp_value_unit = 0.1907;
            break;

        default:
            skiq_unknown("An unsupported Sidekiq part (%u) has been detected\n", part_type);
            status = -EINVAL;
            break;
    }

    return status;
}


static int32_t probe_hw_info( uint8_t *p_card_list,
                              uint8_t num_cards )
{
    skiq_card_t *p_card = NULL;
    int32_t status=0;
    uint8_t curr_card=0, sys_card_index = 0;
    uint16_t hardware=0;

    for( curr_card=0; (curr_card<num_cards) && (0==status); curr_card++ )
    {
        /* curr_card is an index into p_card_list, so the card index in p_sys
         * must be translated using p_card_list */
        sys_card_index = p_card_list[curr_card];
        p_card = &(skiq.card[sys_card_index]);

        // successfully opened the card, now determine the serial number / hw vers

        status = card_mgr_get_card_info( sys_card_index,
                                         &(p_card->serial_num),
                                         &(hardware),
                                         &(p_card->card_params.part_info) );
        if( 0 != status )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "unable to determine serial number/hardware version for "
                      "card %u\n",
                      sys_card_index);
        }

        if ( 0 == status )
        {
            status = card_mgr_is_card_present( sys_card_index, &(p_card->present) );
        }

        if( 0 == status )
        {
            // set the hardware / product version
            p_card->hardware_vers = card_map_hw_vers(hardware);
            p_card->product_vers = card_map_product_vers(hardware);

            if( (p_card->hardware_vers == skiq_hw_vers_invalid) ||
                (p_card->product_vers == skiq_product_invalid) )
            {
                /* The `hardware_vers` and `product_vers` are from EEPROM, so they can represent a
                 * future product that this libsidekiq version does not support */
                skiq_unknown("Unknown / unsupported product and hardware revision for card %u\n",
                             sys_card_index);
                hal_critical_exit(-EINVAL);
            }
        }

        if( 0 == status )
        {
            card_map_rev_and_part( p_card->hardware_vers,
                                   p_card->card_params.part_info.number_string,
                                   p_card->card_params.part_info.revision_string,
                                   &(p_card->card_params.part_type),
                                   &(p_card->hardware_rev) );
        }

        if( 0 == status )
        {
            /*
               Discover if the card is masquerading its revision (as is done with mPCIe rev E and
               m.2 rev D for backwards compatibility with older versions of libsidekiq) and
               correct the hardware identification if needed.

               This is being done in this function as the card manager needs to use the
               "masqueraded" version internally so users can switch between versions of libsidekiq
               and not worry about compatibility... if a user using, say, 4.15 enumerates the card
               and then runs 4.4 utilities, the 4.4 utilities will early exit as the older card
               manager doesn't recognize the newer revision number that already exists in the
               shared memory card information table. Granted, this isn't a likely scenario, but
               we would like to maintain compatibilty whenever possible...

               This is also being done after `card_map_rev_and_part()` so that function can use the
               original information to determine if the card is masquerading (and not the corrected
               information).
            */
            skiq_hw_vers_t actual_hw_vers = skiq_hw_vers_invalid;
            skiq_hw_rev_t actual_hw_rev = hw_rev_invalid;

            card_unmasquerade(sys_card_index, p_card->hardware_vers,
                (const char *) &(skiq.card[sys_card_index].card_params.part_info.revision_string[0]),
                &actual_hw_vers, &actual_hw_rev);
            if( (p_card->hardware_vers != actual_hw_vers) &&
                (actual_hw_vers != skiq_hw_vers_invalid) )
            {
                debug_print("--- %s():%d : it appears that card %" PRIu8 " is masquerading"
                    " (current hw vers %d); adjusting hardware to use version %d and"
                    " revision %d.\n", __FUNCTION__, __LINE__, sys_card_index,
                    p_card->hardware_vers, actual_hw_vers, actual_hw_rev);
                p_card->hardware_vers = actual_hw_vers;
                p_card->hardware_rev = actual_hw_rev;
            }
        }

        if ( 0 == status )
        {
            p_card->card_params.card = curr_card;

            // initialize the IDs
            p_card->rx_list[skiq_rx_hdl_A1].id.hdl = skiq_rx_hdl_A1;
            p_card->rx_list[skiq_rx_hdl_A2].id.hdl = skiq_rx_hdl_A2;
            p_card->rx_list[skiq_rx_hdl_B1].id.hdl = skiq_rx_hdl_B1;
            p_card->rx_list[skiq_rx_hdl_B2].id.hdl = skiq_rx_hdl_B2;
            p_card->rx_list[skiq_rx_hdl_C1].id.hdl = skiq_rx_hdl_C1;
            p_card->rx_list[skiq_rx_hdl_D1].id.hdl = skiq_rx_hdl_D1;

            p_card->rx_list[skiq_rx_hdl_A1].id.chip_id = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_A2].id.chip_id = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_B1].id.chip_id = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_B2].id.chip_id = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_C1].id.chip_id = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_D1].id.chip_id = sys_card_index;

            p_card->tx_list[skiq_tx_hdl_A1].id.hdl = skiq_tx_hdl_A1;
            p_card->tx_list[skiq_tx_hdl_A1].id.chip_id = sys_card_index;

            p_card->tx_list[skiq_tx_hdl_A2].id.hdl = skiq_tx_hdl_A2;
            p_card->tx_list[skiq_tx_hdl_A2].id.chip_id = sys_card_index;

            p_card->tx_list[skiq_tx_hdl_B1].id.hdl = skiq_tx_hdl_B1;
            p_card->tx_list[skiq_tx_hdl_B1].id.chip_id = sys_card_index;

            p_card->tx_list[skiq_tx_hdl_B2].id.hdl = skiq_tx_hdl_B2;
            p_card->tx_list[skiq_tx_hdl_B2].id.chip_id = sys_card_index;

            p_card->rx_list[skiq_rx_hdl_A1].rf_id.card = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_A2].rf_id.card = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_B1].rf_id.card = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_B2].rf_id.card = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_C1].rf_id.card = sys_card_index;
            p_card->rx_list[skiq_rx_hdl_D1].rf_id.card = sys_card_index;
            
            p_card->tx_list[skiq_tx_hdl_A1].rf_id.card = sys_card_index;
            p_card->tx_list[skiq_tx_hdl_A2].rf_id.card = sys_card_index;
            p_card->tx_list[skiq_tx_hdl_B1].rf_id.card = sys_card_index;
            p_card->tx_list[skiq_tx_hdl_B2].rf_id.card = sys_card_index;

            p_card->rx_list[skiq_rx_hdl_A1].rf_id.hdl = skiq_rx_hdl_A1;
            p_card->rx_list[skiq_rx_hdl_A2].rf_id.hdl = skiq_rx_hdl_A2;
            p_card->rx_list[skiq_rx_hdl_B1].rf_id.hdl = skiq_rx_hdl_B1;
            p_card->rx_list[skiq_rx_hdl_B2].rf_id.hdl = skiq_rx_hdl_B2;
            p_card->rx_list[skiq_rx_hdl_C1].rf_id.hdl = skiq_rx_hdl_C1;
            p_card->rx_list[skiq_rx_hdl_D1].rf_id.hdl = skiq_rx_hdl_D1;
            
            p_card->tx_list[skiq_tx_hdl_A1].rf_id.hdl = skiq_tx_hdl_A1;
            p_card->tx_list[skiq_tx_hdl_A2].rf_id.hdl = skiq_tx_hdl_A2;
            p_card->tx_list[skiq_tx_hdl_B1].rf_id.hdl = skiq_tx_hdl_B1;
            p_card->tx_list[skiq_tx_hdl_B2].rf_id.hdl = skiq_tx_hdl_B2;

            p_card->rx_list[skiq_rx_hdl_A1].rf_id.chip_id =
                p_card->rx_list[skiq_rx_hdl_A1].id.chip_id;
            p_card->rx_list[skiq_rx_hdl_A2].rf_id.chip_id =
                p_card->rx_list[skiq_rx_hdl_A2].id.chip_id;
            p_card->rx_list[skiq_rx_hdl_B1].rf_id.chip_id =
                p_card->rx_list[skiq_rx_hdl_B1].id.chip_id;
            p_card->rx_list[skiq_rx_hdl_B2].rf_id.chip_id =
                p_card->rx_list[skiq_rx_hdl_B2].id.chip_id;
            p_card->rx_list[skiq_rx_hdl_C1].rf_id.chip_id =
                p_card->rx_list[skiq_rx_hdl_C1].id.chip_id;
            p_card->rx_list[skiq_rx_hdl_D1].rf_id.chip_id =
                p_card->rx_list[skiq_rx_hdl_D1].id.chip_id;
            
            p_card->tx_list[skiq_tx_hdl_A1].rf_id.chip_id =
                p_card->tx_list[skiq_tx_hdl_A1].id.chip_id;
            p_card->tx_list[skiq_tx_hdl_A2].rf_id.chip_id =
                p_card->tx_list[skiq_tx_hdl_A2].id.chip_id;
            p_card->tx_list[skiq_tx_hdl_B1].rf_id.chip_id =
                p_card->tx_list[skiq_tx_hdl_B1].id.chip_id;
            p_card->tx_list[skiq_tx_hdl_B2].rf_id.chip_id =
                p_card->tx_list[skiq_tx_hdl_B2].id.chip_id;

            switch( p_card->card_params.part_type )
            {
                case skiq_mpcie:
                    p_card->card_params.is_accelerometer_present = true;
                    p_card->has_iq_packed_mode = true;
                    p_card->has_dc_offset = true;
                    break;

                case skiq_m2:
                    p_card->card_params.is_accelerometer_present = true;
                    p_card->has_iq_packed_mode = true;
                    p_card->has_dc_offset = true;
                    break;

                case skiq_x2:
                    p_card->card_params.is_accelerometer_present = false;
                    p_card->has_iq_packed_mode = false;
                    p_card->has_dc_offset = false;
                    break;

                case skiq_z2:
                    p_card->has_iq_packed_mode = false;
                    p_card->has_dc_offset = true;

                    /* revC of Sidekiq Z2 has an IMU */
                    if ( hw_rev_c == _skiq_get_hw_rev( p_card->card_params.card ) )
                    {
                        p_card->card_params.is_accelerometer_present = true;
                    }
                    else
                    {
                        p_card->card_params.is_accelerometer_present = false;
                    }
                    break;

                case skiq_x4:
                    p_card->card_params.is_accelerometer_present = false;
                    p_card->has_iq_packed_mode = false;
                    p_card->has_dc_offset = false;
                    break;

                case skiq_m2_2280:
                    p_card->card_params.is_accelerometer_present = true;
                    p_card->has_iq_packed_mode = true;
                    p_card->has_dc_offset = true;
                    break;

                case skiq_z2p:
                    p_card->has_iq_packed_mode = false;
                    p_card->card_params.is_accelerometer_present = true;
                    p_card->has_dc_offset = true;
                    break;

                case skiq_z3u:
                    p_card->has_iq_packed_mode = false;
                    p_card->card_params.is_accelerometer_present = true;
                    p_card->has_dc_offset = true;
                    break;

                case skiq_nv100:
                    p_card->tx_list[skiq_tx_hdl_A1].rf_id.port = 1;
                    p_card->tx_list[skiq_tx_hdl_A2].rf_id.port = 2;
                    p_card->card_params.is_accelerometer_present = true;
                    p_card->has_iq_packed_mode = false;
                    p_card->has_dc_offset = false;
                    break;

                default:
                    skiq_unknown("An unsupported Sidekiq part (%u) has been detected\n",
                                 p_card->card_params.part_type);
                    break;
            }

            if ( status == 0 )
            {
                status = probe_hw_info_warp_capabilities( p_card->card_params.part_type,
                                                          &(p_card->rf_params) );
            }
        }
    }

    return status;
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
skiq_xport_type_t _skiq_resolve_transport_type( uint8_t card )
{
    skiq_xport_type_t resolved_type = skiq_xport_type_unknown;

    switch (skiq.card[card].card_params.xport)
    {
        case skiq_xport_type_auto:
            if ( ( card_mgr_is_xport_avail( card, skiq_xport_type_pcie ) ) &&
                 ( card_mgr_is_xport_avail( card, skiq_xport_type_usb ) ) )
            {
                resolved_type = skiq_xport_type_usb;
            }
            else if ( card_mgr_is_xport_avail( card, skiq_xport_type_pcie ) )
            {
                resolved_type = skiq_xport_type_pcie;
            }
            else if ( card_mgr_is_xport_avail( card, skiq_xport_type_usb ) )
            {
                resolved_type = skiq_xport_type_usb;
            }
            else if ( card_mgr_is_xport_avail( card, skiq_xport_type_custom ) )
            {
                resolved_type = skiq_xport_type_custom;
            }
            break;

        case skiq_xport_type_pcie:
            if ( card_mgr_is_xport_avail( card, skiq_xport_type_usb ) )
            {
                resolved_type = skiq_xport_type_usb;
            }
            else
            {
                resolved_type = skiq_xport_type_pcie;
            }
            break;

        case skiq_xport_type_usb:
        case skiq_xport_type_custom:
            resolved_type = skiq.card[card].card_params.xport;
            break;

        default:
            resolved_type = skiq_xport_type_unknown;
            skiq_error("Failed to resolve transport type on card %" PRIu8 "\n", card);
            break;
    }

    return resolved_type;
}


void _skiq_vlog( int32_t priority,
                 const char *format,
                 va_list args )
{
    char str[MAX_LOG_MSG_SIZE];
    int num_bytes = 0;

    LOCK(log_callback_mutex);
    if( skiq.log_msg_cb != NULL )
    {
        num_bytes = snprintf( str, MAX_LOG_MSG_SIZE, "<%s> ", PRIORITY_STR[priority] );
        vsnprintf( &(str[num_bytes]), MAX_LOG_MSG_SIZE-num_bytes, format, args );

        // make sure string is NULL terminated
        str[MAX_LOG_MSG_SIZE-1] = '\0';
        // call the logging function
        skiq.log_msg_cb( priority, str );
    }
    UNLOCK(log_callback_mutex);
}


void _skiq_log( int32_t priority, const char *format, ... )
{
    va_list args;

    va_start( args, format );
    _skiq_vlog( priority, format, args );
    va_end( args );
}


void _skiq_log_str( int32_t priority, const char *str )
{
    LOCK(log_callback_mutex);
    if( skiq.log_msg_cb != NULL )
    {
        // call the registered logging function
        skiq.log_msg_cb( priority, str );
    }
    UNLOCK(log_callback_mutex);
}


void _skiq_syslog( int32_t priority, const char* message )
{
#ifndef __MINGW32__
    syslog( priority, "%s", message );
#else
    LOG_logMsg(GENERIC_MODULE_ID, priority, "%s", message);
#endif /* __MINGW32__ */
}

void _skiq_save_ref_clock( uint8_t card, skiq_ref_clock_select_t ref_clock )
{
    skiq.card[card].ref_clock = ref_clock;
}

int32_t _skiq_probe( void )
{
    int32_t status=0;
    uint8_t card_list[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = 0 };

#if (!defined __MINGW32__)
    LOCK(log_callback_mutex);
    if ( skiq.log_msg_cb == _skiq_syslog )
    {
        openlog( SKIQ_LOGNAME, (LOG_PID | LOG_NDELAY | LOG_CONS | LOG_PERROR), LOG_USER );
    }
    UNLOCK(log_callback_mutex);
#endif /* __MINGW32__ */

    if(!skiq.initialized)
    {
        status = card_mgr_init();
    }
    if ( status == 0 )
    {
        status = card_mgr_reprobe();
    }
    else
    {
        goto exit;
    }
    

    if ( status == 0 )
    {
        // get the list of cards
        card_mgr_get_cards( card_list, &(skiq.num_cards_avail) );
        probe_hw_info( card_list, skiq.num_cards_avail );
        /* Windows platform does not init and exit multiple times */
    }

#ifndef __MINGW32__
    if(!skiq.initialized)
    {
        card_mgr_exit();
    }
#endif

exit:
#if (!defined __MINGW32__)
    LOCK(log_callback_mutex);
    if ( skiq.log_msg_cb == _skiq_syslog )
    {
        // close syslog
        closelog();
    }
    UNLOCK(log_callback_mutex);
#endif /* __MINGW32__ */

    return (status);
}


/*************************************************************************************************/
/** Helper function that queries a cache of the FPGA semantic version and checks whether or not it
    meets a minimum required version.

    @param[in] card card index of the Sidekiq of interest
    @param[in] req_major minimum required major version
    @param[in] req_minor minimum required minor version
    @param[in] req_patch minimum required patch version

    @return bool
    @retval true FPGA bitstream version associated with card meets the minimum required version
    @retval false FPGA bitstream version FAILS to meet the minimum required version
 */
bool _skiq_meets_fpga_version( uint8_t card,
                               uint8_t req_major,
                               uint8_t req_minor,
                               uint8_t req_patch )
{
    uint8_t major, minor, patch;
    int32_t status = 0;

    status = skiq_read_fpga_semantic_version( card, &major, &minor, &patch );
    if ( status == 0 )
    {
        /* check FPGA version for feature availability */
        if ( FPGA_VERSION(major,minor,patch) < FPGA_VERSION(req_major,req_minor,req_patch) )
        {
            status = -ENOSYS;
        }
    }

    /* FPGA version is considered to be met when status is 0 */
    return ( status == 0 );
}


/*************************************************************************************************/
/** Helper function that queries the FPGA semantic version directly and checks whether or not it
    meets a minimum required version.

    @param[in] card card index of the Sidekiq of interest
    @param[in] req_major minimum required major version
    @param[in] req_minor minimum required minor version
    @param[in] req_patch minimum required patch version

    @return bool
    @retval true FPGA bitstream version associated with card meets the minimum required version
    @retval false FPGA bitstream version FAILS to meet the minimum required version
 */
bool _skiq_meets_fpga_version_nocache( uint8_t card,
                                       uint8_t req_major,
                                       uint8_t req_minor,
                                       uint8_t req_patch )
{
    skiq_fpga_tx_fifo_size_t tx_fifo_size;
    uint32_t git_hash, build_date, baseline_hash;
    uint8_t major, minor, patch;
    int32_t status = 0;

    status = fpga_ctrl_read_version( card, &git_hash, &build_date, &major, &minor, &patch,
                                     &baseline_hash, &tx_fifo_size );
    if ( status == 0 )
    {
        /* check FPGA version for feature availability */
        if ( FPGA_VERSION(major,minor,patch) < FPGA_VERSION(req_major,req_minor,req_patch) )
        {
            status = -ENOSYS;
        }
    }

    /* FPGA version is considered to be met when status is 0 */
    return ( status == 0 );
}


/*************************************************************************************************/
/** Helper function that returns whether or not a give Sidekiq card has a DC offset correlator in
    the FPGA
 */
bool _skiq_has_dc_offset_corr( uint8_t card )
{
    return skiq.card[card].has_dc_offset;
}


/*****************************************************************************/
/** The _skiq_get_part function obtains the part enumerated type for a given
    Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return skiq_part_t part enumerated type for the specified Sidekiq card
*/
skiq_part_t _skiq_get_part(uint8_t card)
{
    return skiq.card[card].card_params.part_type;
}


/*****************************************************************************/
/** The _skiq_get_hw_iface_vers function obtains the hardware interface version
    for a given Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return hw_iface_vers_t hardware interface version for the specified
    Sidekiq card
*/
hw_iface_vers_t _skiq_get_hw_iface_vers(uint8_t card)
{
    return skiq.card[card].hw_iface_vers;
}


/**************************************************************************************************/
/** Helper function to provide translation between @p board_id_ext to ::skiq_fmc_carrier_t type for
    use by _skiq_get_fmc_carrier() and _skiq_get_fmc_carrier_nocache().

    @param[in] board_id_ext Extended board ID

    @return ::skiq_fmc_carrier_t
 */
static skiq_fmc_carrier_t board_id_ext_to_fmc_carrier( uint8_t card,
                                                       uint8_t board_id_ext )
{
    skiq_fmc_carrier_t board = skiq_fmc_carrier_not_applicable;

    switch ( board_id_ext )
    {
        case BOARD_ID_EXTENDED_AMS_WB3XBM:
            board = skiq_fmc_carrier_ams_wb3xbm;
            break;

        case BOARD_ID_EXTENDED_HTG_K810_WITH_XCKU060:
        case BOARD_ID_EXTENDED_HTG_K810_WITH_XCKU115:
            board = skiq_fmc_carrier_htg_k810;
            break;

        case BOARD_ID_EXTENDED_NOT_APPLICABLE:
        default:
            /* The `board_id_ext` is set by the FPGA, so it can represent a future product that this
             * libsidekiq version does not support */
            board = skiq_fmc_carrier_unknown;
            skiq_unknown("Found unknown / unsupported FMC carrier (%" PRIu8 ":%" PRIu8 ") on card %"
                         PRIu8 "\n", BOARD_ID_EXTENDED, board_id_ext, card);
            break;
    }

    return board;
}


/**************************************************************************************************/
/** Helper function to provide translation between (part,board_id,board_id_ext) to
    ::skiq_fmc_carrier_t type for use by _skiq_get_fmc_carrier() and
    _skiq_get_fmc_carrier_nocache().

    @param[in] part [::skiq_part_t] part type of the associated Sidekiq
    @param[in] board_id Board ID of the associated Sidekiq
    @param[in] board_id_ext Board ID (extended) of the associated Sidekiq

    @return ::skiq_fmc_carrier_t
 */
static skiq_fmc_carrier_t board_id_to_fmc_carrier( uint8_t card,
                                                   uint8_t board_id,
                                                   uint8_t board_id_ext )
{
    skiq_fmc_carrier_t board = skiq_fmc_carrier_not_applicable;
    skiq_part_t part = _skiq_get_part( card );

    if ( ( part == skiq_x2 ) || ( part == skiq_x4 ) )
    {
        switch ( board_id )
        {
#if __DO_NOT_USE__
            /* This board ID is assigned, but not public */
            case BOARD_ID_IVEIA:
                board = skiq_fmc_carrier_iveia;
                break;
#endif
            case BOARD_ID_AMS_WB3XZD:
                board = skiq_fmc_carrier_ams_wb3xzd;
                break;

            case BOARD_ID_HTG_K800_WITH_XCKU060:
            case BOARD_ID_HTG_K800_WITH_XCKU115:
                board = skiq_fmc_carrier_htg_k800;
                break;

            case BOARD_ID_EXTENDED:
                board = board_id_ext_to_fmc_carrier( card, board_id_ext );
                break;

            default:
                /* The `board_id` is set by the FPGA, so it can represent a future product that this
                 * libsidekiq version does not support */
                board = skiq_fmc_carrier_unknown;
                skiq_unknown("Found unknown / unsupported FMC carrier (%u) on card %" PRIu8 "\n",
                             board_id, card);
                break;
        }
    }

    return board;
}


/*************************************************************************************************/
/** The _skiq_get_fmc_carrier function returns the board enumerated type for a given Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return skiq_fmc_carrier_t board enumerated type for the specified Sidekiq card
*/
skiq_fmc_carrier_t _skiq_get_fmc_carrier(uint8_t card)
{
    return skiq.card[card].card_params.part_fmc_carrier;
}


/**************************************************************************************************/
/** The _skiq_get_fmc_carrier_nocache function returns the board enumerated type read directly for a
    given Sidekiq card without relying on cached values.

    @warning The ::skiq_part_t may be cached, however all other information is retrieved from the
    hardware.

    @param[in] card card index of the Sidekiq of interest

    @return skiq_fmc_carrier_t board enumerated type for the specified Sidekiq card
*/
skiq_fmc_carrier_t _skiq_get_fmc_carrier_nocache(uint8_t card)
{
    skiq_fmc_carrier_t board = skiq_fmc_carrier_unknown;
    int32_t status;
    uint8_t board_id, board_id_ext;

    status = fpga_ctrl_read_board_id( card, &board_id );
    if ( status == 0 )
    {
        status = fpga_ctrl_read_board_id_ext( card, &board_id_ext );
    }

    if ( status == 0 )
    {
        board = board_id_to_fmc_carrier( card, board_id, board_id_ext );
    }

    return board;
}


/**************************************************************************************************/
/**
   Helper function to provide translation between (part,board_id_ext) to ::skiq_fpga_device_t type
   for use by _skiq_get_fpga_device() and _skiq_get_fpga_device_nocache().

   @param[in] part           [::skiq_part_t] part type of the associated Sidekiq
   @param[in] board_id_ext   Extended Board ID of the associated Sidekiq

    @return ::skiq_fpga_device_t
 */
static skiq_fpga_device_t board_id_ext_to_fpga_device( uint8_t card,
                                                       uint8_t board_id_ext )
{
    skiq_fpga_device_t fpga_device = skiq_fpga_device_unknown;

    switch ( board_id_ext )
    {
        case BOARD_ID_EXTENDED_HTG_K810_WITH_XCKU060:
            fpga_device = skiq_fpga_device_xcku060;
            break;

        case BOARD_ID_EXTENDED_AMS_WB3XBM:
        case BOARD_ID_EXTENDED_HTG_K810_WITH_XCKU115:
            fpga_device = skiq_fpga_device_xcku115;
            break;

        default:
            /* The `board_id` is set by the FPGA, so it can represent a future product that this
             * libsidekiq version does not support */
            fpga_device = skiq_fpga_device_unknown;
            skiq_unknown("Found unknown / unsupported FPGA device on card %" PRIu8 "\n", card);
            break;
    }

    return fpga_device;
}


/**************************************************************************************************/
/**
   Helper function to provide translation between (part,board_id,board_id_ext) to
   ::skiq_fpga_device_t type for use by _skiq_get_fpga_device() and _skiq_get_fpga_device_nocache().

   @param[in] part           [::skiq_part_t] part type of the associated Sidekiq
   @param[in] board_id       Board ID of the associated Sidekiq
   @param[in] board_id_ext   Extended Board ID of the associated Sidekiq

   @return ::skiq_fpga_device_t
 */
static skiq_fpga_device_t board_id_to_fpga_device( uint8_t card,
                                                   uint8_t board_id,
                                                   uint8_t board_id_ext )
{
    skiq_fpga_device_t fpga_device = skiq_fpga_device_unknown;
    skiq_part_t part = _skiq_get_part( card );

    if ( ( part == skiq_x2 ) || ( part == skiq_x4 ) )
    {
        switch ( board_id )
        {
#if __DO_NOT_USE__
            /* This board ID is assigned, but not public */
            case BOARD_ID_IVEIA:
                fpga_device = skiq_fpga_device_unknown;
                break;
#endif
            case BOARD_ID_AMS_WB3XZD:
            case BOARD_ID_HTG_K800_WITH_XCKU115:
                fpga_device = skiq_fpga_device_xcku115;
                break;

            case BOARD_ID_HTG_K800_WITH_XCKU060:
                fpga_device = skiq_fpga_device_xcku060;
                break;

            case BOARD_ID_EXTENDED:
                fpga_device = board_id_ext_to_fpga_device( card, board_id_ext );
                break;

            default:
                /* The `board_id` is set by the FPGA, so it can represent a future product that this
                 * libsidekiq version does not support */
                fpga_device = skiq_fpga_device_unknown;
                skiq_unknown("Found unknown / unsupported FPGA device on card %" PRIu8 "\n", card);
                break;
        }
    }

    return fpga_device;
}


/**************************************************************************************************/
/**
   The _get_fpga_device() function returns the FPGA device enumerated type for a given Sidekiq card,
   board_id, and board_id_ext.

   @param[in] card           card index of the Sidekiq of interest
   @param[in] board_id       BOARD ID of the Sidekiq of interest
   @param[in] board_id_ext   Extended BOARD ID of the Sidekiq of interest

    @return skiq_fpga_device_t FPGA device enumerated type for the specified Sidekiq card
*/
static skiq_fpga_device_t _get_fpga_device( uint8_t card,
                                            uint8_t board_id,
                                            uint8_t board_id_ext )
{
    skiq_fpga_device_t fpga_device = skiq_fpga_device_unknown;
    skiq_part_t part = _skiq_get_part( card );

    switch (part)
    {
        case skiq_mpcie:
            fpga_device = skiq_fpga_device_xc6slx45t;
            break;

        case skiq_m2:
        case skiq_m2_2280:
        case skiq_nv100:
            fpga_device = skiq_fpga_device_xc7a50t;
            break;

        case skiq_z2:
            fpga_device = skiq_fpga_device_xc7z010;
            break;

        case skiq_x2:
        case skiq_x4:
            fpga_device = board_id_to_fpga_device( card, board_id, board_id_ext );
            break;

        case skiq_z2p:
        case skiq_z3u:
            fpga_device = skiq_fpga_device_xczu3eg;
            break;

        default:
            /* The `skiq_part_t` is derived from values stored in EEPROM, so it can represent a
             * future product that this libsidekiq version does not support or know about */
            fpga_device = skiq_fpga_device_unknown;
            skiq_unknown("Found unknown / unsupported Sidekiq part (%u) on card %" PRIu8 "\n",
                         part, card);
            break;
    }

    return fpga_device;
}


/*************************************************************************************************/
/** The _skiq_get_fpga_device function returns the FPGA device enumerated type for a given Sidekiq
    card.

    @param card card index of the Sidekiq of interest

    @return skiq_fpga_device_t FPGA device enumerated type for the specified Sidekiq card
*/
skiq_fpga_device_t _skiq_get_fpga_device(uint8_t card)
{
    return skiq.card[card].fpga_params.fpga_device;
}


/*************************************************************************************************/
/** The _skiq_get_fpga_device_nocache function returns the FPGA device enumerated type read directly
    for a given Sidekiq card without relying on cached values.

    @warning The ::skiq_part_t may be cached, however all other information is retrieved from the
    hardware.

    @param card card index of the Sidekiq of interest

    @return skiq_fpga_device_t FPGA device enumerated type for the specified Sidekiq card
*/
skiq_fpga_device_t _skiq_get_fpga_device_nocache(uint8_t card)
{
    int32_t status;
    uint8_t board_id, board_id_ext;
    skiq_fpga_device_t fpga_device = skiq_fpga_device_unknown;

    status = fpga_ctrl_read_board_id( card, &board_id );
    if ( status == 0 )
    {
        status = fpga_ctrl_read_board_id_ext( card, &board_id_ext );
    }

    if ( status == 0 )
    {
        fpga_device = _get_fpga_device( card, board_id, board_id_ext );
    }

    return fpga_device;
}


/*****************************************************************************/
/** The _skiq_get_hw_vers function obtains the hardware vers enumerated type for a given
    Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return skiq_part_t part enumerated type for the specified Sidekiq card
*/
skiq_hw_vers_t _skiq_get_hw_vers( uint8_t card )
{
    return (skiq.card[card].hardware_vers);
}

/*****************************************************************************/
/** The _skiq_get_hw_rev function obtains the hardware rev enumerated type for a given
    Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return skiq_hw_rev_t hw_rev enumerated type for the specified Sidekiq card
*/
skiq_hw_rev_t _skiq_get_hw_rev( uint8_t card )
{
    return (skiq.card[card].hardware_rev);
}

/*************************************************************************************************/
/** The _skiq_get_hw_rev_minor function obtains the minor number of the hardware revision for a
    given Sidekiq card.

    @param[in] card card index of the Sidekiq of interest

    @return uint8_t revision minor number for the specified Sidekiq card
*/
uint8_t _skiq_get_hw_rev_minor( uint8_t card )
{
    /* revision_string takes the form [A-Z][0-9], so the minor number is only a single digit */
    uint8_t minor = skiq.card[card].card_params.part_info.revision_string[1];

    /* check that it is a digit, then convert to decimal equivalent */
    if ( isdigit(minor) != 0 )
    {
        minor = minor - '0';
    }
    else
    {
        minor = 0;
    }

    return minor;
}
/*****************************************************************************/
/** The _skiq_get_fpga_caps function obtains the FPGA's capabilities for a given
    Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return struct fpga_capabilities FPGA capabilities for the specified Sidekiq card
*/
struct fpga_capabilities _skiq_get_fpga_caps(uint8_t card)
{
    return skiq.card[card].fpga_priv.caps;
}

/*****************************************************************************/
/** The _skiq_get_fpga_board_id function obtains the FPGA's board ID for a given
    Sidekiq card.

    @param card card index of the Sidekiq of interest

    @return FPGA boardID for the specified Sidekiq card
*/
uint8_t _skiq_get_fpga_board_id(uint8_t card)
{
    return skiq.card[card].fpga_priv.board_id;
}

/**************************************************************************************************/
/**
   The _skiq_get_fpga_board_id_ext() function obtains the FPGA's extended board ID for a given
   Sidekiq card.
*/
uint8_t _skiq_get_fpga_board_id_ext(uint8_t card)
{
    return skiq.card[card].fpga_priv.board_id_ext;
}

/*****************************************************************************/
/** The _skiq_get_max_rfic_cs obtains the maximum valid chip select value for 
    the Sidekiq card specified.

    @param card card index of the Sidekiq of interest

    @return maximum value of RFIC chip select
*/
uint8_t _skiq_get_max_rfic_cs( uint8_t card )
{
    skiq_part_t part = _skiq_get_part( card );
    uint8_t max_rfic_cs=0;

    switch( part )
    {
        case skiq_mpcie:
            // intentional fallthru
        case skiq_m2:
            // intentional fallthru
        case skiq_z2:
            // intentional fallthru
        case skiq_z2p:
            // intentional fallthru            
        case skiq_z3u:
            // intentional fallthru
            max_rfic_cs=0;
            break;

        case skiq_x2:
            max_rfic_cs=1; // 0: 9371, 1: 9528
            break;

        case skiq_x4:
            max_rfic_cs=2; // 0: 9379 A, 1: 9528, 2: 9379 B
            break;

        default:
            skiq_error("Invalid part detected, defaulting to 0 for CS (card=%u)",
                       card);
            break;
    }

    return (max_rfic_cs);
}


/**************************************************************************************************/
/* This function performs the necessary steps to tell various subsystems that the FPGA's
 * configuration is going away.
 */
static int32_t
_prepare_for_fpga_reload( uint8_t card )
{
    int32_t status = 0;

    /* give the RF layer a chance to exit before pulling the FPGA out from underneath it */
    if ( skiq_xport_init_level_full == skiq_xport_card_get_level( card ) )
    {
        status = _skiq_exit_rf_full( card );
    }

    return status;

} /* _prepare_for_fpga_reload */


/**************************************************************************************************/
/* This function performs the necessary steps to tell various subsystems that the FPGA's
 * configuration is back and needs initialization.
 */
static int32_t
_perform_post_fpga_reload_actions( uint8_t card )
{
    int32_t status = 0;
    skiq_xport_init_level_t level = skiq_xport_init_level_unknown;

    level = skiq_xport_card_get_level( card );

    /* re-cache FPGA information */
    status = _skiq_init_fpga(card, level);

    if( (status == 0) && (skiq_xport_init_level_full == level) )
    {
        skiq_info("Re-initializing RF interface on card %" PRIu8 "\n", card);

        /* fully initialize the RFIC layer (includes configuring the reference clock source */
        status = _skiq_init_rf_full( card );
    }

    return status;

} /* _perform_post_fpga_reload_actions */


/*****************************************************************************/
/** The _skiq_write_rx_preselect_filters function is responsible for configuring
    available RX preselect filters of the card and handle specified.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the RX interface to configure
    @param num_filters number of filters in list
    @param p_filters pointer to the filter list

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_write_rx_preselect_filters( uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint8_t num_filters,
                                          const skiq_filt_t *p_filters )
{
    int32_t status=0;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS(card);

    rfe_instance = _skiq_get_rx_rfe_instance(card,hdl);

    rfe_write_filters( &rfe_instance,
                       num_filters,
                       p_filters );

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The _skiq_write_tx_preselect_filters function is responsible for configuring
    available TX preselect filters of the card and handle specified.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the TX interface to configure
    @param num_filters number of filters in list
    @param p_filters pointer to the filter list

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_write_tx_preselect_filters( uint8_t card,
                                          skiq_tx_hdl_t hdl,
                                          uint8_t num_filters,
                                          const skiq_filt_t *p_filters )
{
    int32_t status=0;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);

    rfe_instance = _skiq_get_tx_rfe_instance(card,hdl);

    rfe_write_filters( &rfe_instance,
                       num_filters,
                       p_filters );

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The _skiq_get_rx_rfe_instance obtains the RF front end instance for a
    given Rx handle.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the desired Rx interface

    @return rfe_t   the RF front end instance
*/
rfe_t _skiq_get_rx_rfe_instance( uint8_t card, skiq_rx_hdl_t hdl )
{
    rfe_t rfe;

    rfe.p_id = &(skiq.card[card].rx_list[hdl].rf_id);
    rfe.p_funcs = skiq.card[card].p_rfe_funcs;

    return rfe;
}

/*****************************************************************************/
/** The _skiq_get_rx_rfe_instance obtains the RF front end instance for a
    given Tx handle.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the desired Tx interface

    @return rfe_t   the RF front end instance
*/
rfe_t _skiq_get_tx_rfe_instance( uint8_t card, skiq_tx_hdl_t hdl )
{
    rfe_t rfe;

    rfe.p_id = &(skiq.card[card].tx_list[hdl].rf_id);
    rfe.p_funcs = skiq.card[card].p_rfe_funcs;

    return rfe;
}

/**************************************************************************************************/
/** The _skiq_get_generic_rfe_instance obtains the RFFE instance not related to a particular
    handle.
*/
rfe_t _skiq_get_generic_rfe_instance( uint8_t card )
{
    rfe_t rfe;

    rfe.p_id = &(skiq.card[card].rx_list[skiq_rx_hdl_A1].rf_id);
    rfe.p_funcs = skiq.card[card].p_rfe_funcs;

    return rfe;
}

/*****************************************************************************/
/** The _skiq_get_rx_rfic_instance obtains the RF IC instance for a given Rx
    handle.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the desired Rx interface

    @return rfic_t  the RF IC instance
*/
rfic_t _skiq_get_rx_rfic_instance( uint8_t card, skiq_rx_hdl_t hdl )
{
    rfic_t rfic;

    rfic.p_id = &(skiq.card[card].rx_list[hdl].rf_id);
    rfic.p_funcs = skiq.card[card].p_rfic_funcs;

    return rfic;
}

/*****************************************************************************/
/** The _skiq_get_tx_rfic_instance obtains the RF IC instance for a given Tx
    handle.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the desired Tx interface

    @return rfic_t  the RF IC instance
*/
rfic_t _skiq_get_tx_rfic_instance( uint8_t card, skiq_tx_hdl_t hdl )
{
    rfic_t rfic;

    rfic.p_id = &(skiq.card[card].tx_list[hdl].rf_id);
    rfic.p_funcs = skiq.card[card].p_rfic_funcs;

    return rfic;
}

/**************************************************************************************************/
/** The _skiq_get_generic_rfic_instance obtains the RF IC instance not related to a particular
    handle.

    @param card card index of the Sidekiq of interest

    @return rfic_t  the RF IC instance
*/
rfic_t _skiq_get_generic_rfic_instance( uint8_t card )
{
    rfic_t rfic;

    rfic.p_id = &(skiq.card[card].rx_list[skiq_rx_hdl_A1].rf_id);
    rfic.p_funcs = skiq.card[card].p_rfic_funcs;

    return rfic;
}

/*****************************************************************************/
/** The skiq_get_cards() function is responsible for generating a list
    of valid Sidekiq card indices for the transport specified.  Return of the
    card does not mean that it is available for use by the application.  To
    check card availability, refer to skiq_is_card_avail().

    @param xport_type transport type to detect card
    @param p_num_cards pointer to where to store the number of cards
    @param p_cards pointer to where to store the card indices of the
    Sidekiqs available.  There should be room to store at least
    SKIQ_MAX_NUM_CARDS at this location.

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_get_cards( skiq_xport_type_t type, uint8_t *p_num_cards, uint8_t *p_cards )
{
    int32_t status = 0;
    uint8_t i = 0;
    uint8_t card = 0;

    _skiq_probe();
    *p_num_cards = 0;

    for( i = 0; i < SKIQ_MAX_NUM_CARDS; i++ )
    {
        if( (skiq.card[i].serial_num != INVALID_SERIAL_NUM) &&
            (skiq.card[i].card_params.card != INVALID_CARD_INDEX) )
        {
            if( 0 == skiq_is_xport_avail(skiq.card[i].card_params.card, type) )
            {
                p_cards[card] = skiq.card[i].card_params.card;
                card++;
                *p_num_cards = (*(p_num_cards))+1;
            }
        }
    }
    return status;
}

/*****************************************************************************/
/** The skiq_get_card_from_serial_string() function is responsible for obtaining
    the Sidekiq card index for the specified serial number.

    @param serial_num serial number of Sidekiq card
    @param p_card pointer to where to store the corresponding card index
    of the specified Sidekiq

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_get_card_from_serial_string( char* p_serial, uint8_t *p_card )
{
    uint32_t serial_num = INVALID_SERIAL_NUM;
    int32_t status=-1;
    uint8_t i=0;

    _skiq_probe();

    i = strnlen(p_serial, SKIQ_SERIAL_NUM_STRLEN);
    if( 5 == i )
    {
        // Legacy serial number is between 30000 and UINT16_MAX, implying a
        // length of 5 characters.
        serial_num = strtol(p_serial, NULL, 10);
    }
    else if( 4 == i )
    {
        // New 4 byte alpha numeric serial number. We package this into a
        // a uint32_t for ease of use.
        serial_num =
            ((toupper(p_serial[0])) << 24) |
            ((toupper(p_serial[1])) << 16) |
            ((toupper(p_serial[2])) <<  8) |
            ((toupper(p_serial[3])) <<  0);
    }
    else
    {
        return -1;
    }

    for( i=0; i<SKIQ_MAX_NUM_CARDS; i++ )
    {
        if( skiq.card[i].serial_num == serial_num )
        {
            *p_card = i;
            status=0;
            break;
        }
    }

    return status;
}

/*****************************************************************************/
/** The skiq_read_serial_string() function is responsible for returning the serial
    number of the Sidekiq.

    Note: Memory used for holding the string representation of the serial
    number is managed internally by libsidekiq and does not need to be managed
    in any manner by the end user (i.e. no need to free memory).

    @param card card index of the Sidekiq of interest
    @param p_serial: a pointer to hold the serial number

    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_serial_string(uint8_t card, char** pp_serial_num)
{
    // This memory is used to hold the string representation of the serial
    // number. This memory location will be handed out to users, so it could in
    // effect be modified by the user. Since this isn't the serial number value
    // that is used internally (we use a uint32_t in the skiq struct for that),
    // there shouldn't be any threat of the user being able to do anything bad.
    static char pp_serial[SKIQ_MAX_NUM_CARDS][SKIQ_SERIAL_NUM_STRLEN];
    int32_t status = 0;

    CHECK_CARD_RANGE( card );

    if( NULL == pp_serial_num )
    {
        status = -EFAULT;
    }

    if( 0 == status )
    {
        status = _skiq_probe();
    }

    if( 0 == status )
    {
        status = _serial_num_to_serial_str(skiq.card[card].serial_num,
                                           pp_serial[card]);
    }

    if( 0 == status )
    {
        *pp_serial_num = pp_serial[card];
    }

    return status;
}

/*****************************************************************************/
/** The _skiq_init_xport() function is responsible for initializing the
    transport layer of a given card.

    @param card card index of the Sidekiq of interest
    @param type the transport type that to initialize
    @param level the transport functionality level required

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_init_xport( uint8_t card,
                          skiq_xport_type_t type,
                          skiq_xport_init_level_t level )
{
    int32_t status = 0;

    if( ((skiq_xport_type_pcie == type) ||
         (skiq_xport_type_auto == type)) &&
        (card_mgr_is_xport_avail(card, skiq_xport_type_pcie)) )
    {
        // We handle PCIE first because most Sidekiq cards will come up with
        // both PCIE and USB listed as supported xports (ex MPCIE and M2). In
        // those cases, we want to use PCIE for transport and USB for HAL.
        if( card_mgr_is_xport_avail(card, skiq_xport_type_usb) )
        {
            // Initialize USB xport for basic access so that hal/card services
            // are conducted using USB rather than PCIE.
            if ( status == 0 )
            {
                status = skiq_xport_card_init(skiq_xport_type_usb,
                                              skiq_xport_init_level_basic,
                                              card);
            }

            if ( status == 0 )
            {
                hal_select_functions_for_card(card, skiq_xport_type_usb);
                flash_select_functions_for_card(card, skiq_xport_type_usb);
            }
        }

        if ( 0 == status )
        {
            /* initialize the PCIe transport before flash_select_functions_for_card() because it
             * relies on there being an appropriate and registered transport */
            status = skiq_xport_card_init(skiq_xport_type_pcie, level, card);
            if( 0 == status )
            {
                skiq.card[card].card_params.xport = skiq_xport_type_pcie;
                skiq.card[card].card_params.init_level = level;
            }

            if ( ( 0 == status ) && !card_mgr_is_xport_avail(card, skiq_xport_type_usb) )
            {
                // USB is not present (hopefully this just means that the USB pins
                // aren't routed to the host or this is something like X2 that has
                // no USB). Use the FPGA registers instead.
                hal_select_functions_for_card(card, skiq_xport_type_pcie);
                flash_select_functions_for_card(card, skiq_xport_type_pcie);
            }
        }
    }
    else if( ((skiq_xport_type_usb == type) ||
              (skiq_xport_type_auto == type)) &&
             (card_mgr_is_xport_avail(card, skiq_xport_type_usb)) )
    {
        // At this point, the USB VID and PID are present. This however doesn't
        // gurantee that it can stream over USB. We'll count on the fact that
        // the RFIC will fail to initialize if a non-USB bitstream is loaded
        // to handle that error case.
        status = skiq_xport_card_init(skiq_xport_type_usb, level, card);
        if( 0 == status )
        {
            skiq.card[card].card_params.xport = skiq_xport_type_usb;
            skiq.card[card].card_params.init_level = level;
            hal_select_functions_for_card(card, skiq_xport_type_usb);
            flash_select_functions_for_card(card, skiq_xport_type_usb);
        }
    }
    else if( ((skiq_xport_type_custom == type) ||
              (skiq_xport_type_auto == type)) &&
             (card_mgr_is_xport_avail(card, skiq_xport_type_custom)) )
    {
        // Custom transport, cool...
        status = skiq_xport_card_init(skiq_xport_type_custom, level, card);
        if( 0 == status )
        {
            skiq.card[card].card_params.xport = skiq_xport_type_custom;
            skiq.card[card].card_params.init_level = level;

            hal_select_functions_for_card(card, skiq_xport_type_custom);
            flash_select_functions_for_card(card, skiq_xport_type_custom);
        }
    }
    else if( ((skiq_xport_type_net == type) ||
              (skiq_xport_type_auto == type)) &&
             (card_mgr_is_xport_avail(card, skiq_xport_type_net)) )
    {
        // Net transport
        status = skiq_xport_card_init(skiq_xport_type_net, level, card);
        if( 0 == status )
        {
            skiq.card[card].card_params.xport = skiq_xport_type_net;
            skiq.card[card].card_params.init_level = level;

            hal_select_functions_for_card(card, skiq_xport_type_net);
            flash_select_functions_for_card(card, skiq_xport_type_net);
        }
    }
    else
    {
        // Unknown transport, not cool...
        status = -EINVAL;
    }

    return status;
}

/*****************************************************************************/
/** @brief Initializes the card's transport and hal functions, placing them
    and their associated features into a default state. The members of the
    skiq_card_param_t struct should also be populated in this function.

    @param card card index of the Sidekiq of interest
    @param type the transport type that to initialize
    @param level the transport functionality level required

    @return int32_t: status where 0=success, anything else is an error
*/
int32_t _skiq_init_card( uint8_t card,
                         skiq_xport_type_t type,
                         skiq_xport_init_level_t level )
{
    skiq_card_param_t* p_param =  &(skiq.card[card].card_params);
    skiq_card_t *p_card = &(skiq.card[card]);
    char serial_string[SKIQ_SERIAL_NUM_STRLEN] = {0};
    uint32_t serial_num = 0;
    int32_t status = 0;

    status = _skiq_init_xport(card, type, level);

    // Validate that the serial number provided by card manager (shared memory)
    // maps to that found in EEPROM. It is possible to alter shared memory from
    // userspace, potentially causing licensing issues!
    if( 0 == status )
    {
        status = card_read_serial_num(card, &serial_num);
        if( 0 == status )
        {
            // we only want to check the serial number if it's valid.  In the
            // case of a fresh board, the serial # will be invalid and we
            // still need to allow initialization to continue so we can configure it
            if( serial_num != INVALID_SERIAL_NUM )
            {
                _serial_num_to_serial_str(serial_num, serial_string);
                status = strncmp(p_param->serial_string,
                                 serial_string,
                                 SKIQ_SERIAL_NUM_STRLEN);
                if( 0 != status )
                {
                    _skiq_log(SKIQ_LOG_ERROR,
                              "Serial number validation error (%s != %s)\n",
                              p_param->serial_string,
                              serial_string);
                    status = -EINVAL;
                }
            }
        }
    }

    // Update the HAL functions based on part (if needed)
    if( 0 == status )
    {
        hal_select_functions_for_card_by_part( card, p_param->part_type );
    }

    /* Initialize the I/O expander's I2C cache */
    if( 0 == status )
    {
        io_exp_initialize_cache(card);
    }

    if( 0 == status )
    {
        // map part to accelerometer support and I/Q packed mode support
        switch(p_param->part_type)
        {
            case skiq_mpcie:
            case skiq_m2:
            case skiq_m2_2280:
                p_param->is_accelerometer_present = true;
                p_card->has_iq_packed_mode = true;
                p_card->has_dc_offset = true;
                break;

            case skiq_z2p:
            case skiq_z3u:
                p_param->is_accelerometer_present = true;
                p_card->has_iq_packed_mode = false;
                p_card->has_dc_offset = true;
                break;

            case skiq_x2:
            case skiq_x4:
                p_param->is_accelerometer_present = false;
                p_card->has_iq_packed_mode = false;
                p_card->has_dc_offset = false;
                break;

            case skiq_nv100:
                p_param->is_accelerometer_present = true;
                p_card->has_iq_packed_mode = false;
                p_card->has_dc_offset = false;
                break;

            case skiq_z2:
                /* revC of Sidekiq Z2 has an IMU */
                if ( hw_rev_c == _skiq_get_hw_rev( card ) )
                {
                    p_param->is_accelerometer_present = true;
                }
                else
                {
                    p_param->is_accelerometer_present = false;
                }
                p_card->has_iq_packed_mode = false;
                p_card->has_dc_offset = true;
                break;

            default:
                status = -EINVAL;
                p_param->is_accelerometer_present = false;
                p_card->has_iq_packed_mode = false;
                p_card->has_dc_offset = false;
                break;
        }
    }

    if( 0 == status )
    {
        // transport and hal are active, set to true to enable API calls
        skiq.card[card].card_active = 1;
    }

    if( (0 == status) && (p_param->is_accelerometer_present) )
    {
        status = skiq_write_accel_state(card, 0);
    }

    return status;
}

/*****************************************************************************/
/** @brief  Uninitialize the card's transport and hal functions, placing them
            into a default state.

    @param[in]  card    card index of the Sidekiq of interest

    @related    _skiq_init_card()

    @return int32_t
    @retval 0 on success
*/
static int32_t _skiq_exit_card(uint8_t card)
{
    int32_t result = 0;

    result = skiq_xport_card_exit( skiq.card[card].card_params.card );
    if (0 != result)
    {
        skiq_warning("Warning: failed to uninitialize transport on card %"
            PRIu8 " (return code %" PRIi32 "); attempting to continue.\n",
            card, result);
    }

    skiq.card[card].card_active = 0;
    skiq.card[card].card_params.xport = skiq_xport_type_unknown;
    skiq.card[card].card_params.init_level = skiq_xport_init_level_unknown;

    hal_reset_functions( skiq.card[card].card_params.card );

    return result;
}

/*****************************************************************************/
/** @brief Initializes features related to the Sidekiq's firmware. The members
    of the skiq_fw_param_t struct should also be populated in this function.

    @param card card index of the Sidekiq of interest

    @return int32_t: status where 0=success, anything else is an error
*/
int32_t _skiq_init_fw(uint8_t card)
{
    skiq_card_param_t* p_card_params = &(skiq.card[card].card_params);
    skiq_fw_param_t* p_fw_params = &(skiq.card[card].fw_params);
    uint16_t delay_ms = 0;
    int32_t status = 0;

    if( (skiq_m2 != p_card_params->part_type) &&
        (skiq_mpcie != p_card_params->part_type) )
    {
        p_fw_params->is_present = false;
    }
    else
    {
        p_fw_params->is_present = true;

        // read the FW version
        status = hal_read_fw_ver(card,
                                 &(p_fw_params->version_major),
                                 &(p_fw_params->version_minor));
        if( 0 == status )
        {
            _skiq_log(SKIQ_LOG_INFO,
                      "Sidekiq card %u firmware v%u.%u\n",
                      card,
                      p_fw_params->version_major,
                      p_fw_params->version_minor);
        }

        if( 0 == status )
        {
            status = card_read_usb_enumeration_delay(card, &delay_ms);
        }

        if( 0 == status )
        {
            p_fw_params->enumeration_delay_ms = delay_ms;
        }
    }

    return status;
}

/*****************************************************************************/
/** @brief Uninitializes features related to the Sidekiq's firmware.

    @param[in]  card    card index of the Sidekiq of interest

    @related    _skiq_init_fw()

    @return int32_t
    @retval 0   on success
*/
static int32_t _skiq_exit_fw(uint8_t card)
{
    int32_t result = 0;

    skiq_fw_param_t* p_fw_params = &(skiq.card[card].fw_params);
    skiq_fw_param_t defaultFwParams = SKIQ_FW_PARAM_INITIALIZER;

    *p_fw_params = defaultFwParams;

    return result;
}

/**************************************************************************************************/
/** Helper function to check the state of the fpga version.

    TODO Return a non zero error if fpga_state is FPGA_STATE_VERSION_INACCESSIBLE.
    TODO If this returns a non zero error, clear the error in all basic initialization code paths.
    TODO Call this function before all fpga_params accessors.

    @param[in] card card index of the Sidekiq of interest

    @return int32_t
    @retval 0 Success
 */
static int32_t _check_fpga_state(uint8_t card)
{
    int32_t status = 0;

    if ( skiq.card[card].fpga_params.fpga_state == FPGA_STATE_VERSION_GOLDEN )
    {
        skiq_warning("Sidekiq card %u currently running golden FPGA version\n", card);
    }
    else if ( skiq.card[card].fpga_params.fpga_state == FPGA_STATE_VERSION_INACCESSIBLE )
    {
        skiq_warning("Sidekiq card %u is unable to access the FPGA\n", card);
    }

    return status;
}

/*****************************************************************************/
/** @brief Initializes features related to the Sidekiq's FPGA. The members of
    the skiq_fpga_param_t struct should also be populated in this function.

    @param[in] card card index of the Sidekiq of interest

    @return int32_t
    @retval 0 Success
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ERANGE The system timestamp frequency indicated by the FPGA is out of range
    @retval -ENOTSUP Sidekiq RFIC does not support querying system timestamp frequency
*/
int32_t _skiq_init_fpga(uint8_t card, 
                        skiq_xport_init_level_t level)
{
    skiq_fpga_param_t* p_param = &(skiq.card[card].fpga_params);
    int32_t status = 0;

    /* initialize FPGA feature configuration */
    status = fpga_ctrl_init_config( card );

    if ( status == 0 )
    {
    // read the FPGA version info
    status = fpga_ctrl_read_version(card,
                                    &(p_param->git_hash),
                                    &(p_param->build_date),
                                    &(p_param->version_major),
                                    &(p_param->version_minor),
                                    &(p_param->version_patch),
                                    &(p_param->baseline_hash),
                                    &(p_param->tx_fifo_size));
    }

    if ( status == 0 )
    {
        /*The FPGA version will return all zeroes if the register transport is absent. This occurs on
          mpcie or m2 devices when the wrong bitstream is loaded. */
        if ( FPGA_VERSION(p_param->version_major,p_param->version_minor,p_param->version_patch) ==
             FPGA_VERS_INVALID ) 
        {
            p_param->fpga_state = FPGA_STATE_VERSION_INACCESSIBLE;

            if ( level == skiq_xport_init_level_full ) 
            {
                skiq_error(FPGA_NOT_ACCESSIBLE);
                status = -ENOSYS;
            }
            else 
            {
                /*Allow basic init to proceed without error so an alternate bitstream can be loaded. */
                skiq_warning(FPGA_NOT_ACCESSIBLE);
            }
        }
        else if ( FPGA_VERSION(p_param->version_major,p_param->version_minor,p_param->version_patch) ==
                  FPGA_VERS_GOLDEN )
        {
            p_param->fpga_state = FPGA_STATE_VERSION_GOLDEN;
            skiq_warning("Sidekiq card %u currently running golden FPGA version\n", card);
        }
        else
        {
            p_param->fpga_state = FPGA_STATE_VERSION_VALID;

            if ( FPGA_VERSION(p_param->version_major,p_param->version_minor,p_param->version_patch) <
                  FPGA_VERSION(3,8,0) )
            {
                skiq_info("Sidekiq card %u FPGA v%u.%u, (date %x, FIFO size %s)\n",
                        card,
                        p_param->version_major,
                        p_param->version_minor,
                        p_param->build_date,
                        fpga_tx_fifo_size_cstr( p_param->tx_fifo_size ));
            }
            else
            {
                skiq_info("Sidekiq card %u FPGA v%u.%u.%u, (date %x, FIFO size %s)\n",
                        card,
                        p_param->version_major,
                        p_param->version_minor,
                        p_param->version_patch,
                        p_param->build_date,
                        fpga_tx_fifo_size_cstr( p_param->tx_fifo_size ));
            }
        }
    }

    if( 0 == status )
    {
        // if present, attempt to reset the PDK registers using the soft reset functionality;
        // only return a failure on a failure and not if the transport and/or FPGA don't support
        // it
        status = fpga_ctrl_issue_soft_reset(card);
        if ((-ENOSYS == status) || (-ENOTSUP == status))
        {
            status = 0;
        }
    }

    if( 0 == status )
    {
        // Cache the Board ID
        status = fpga_ctrl_read_board_id(card, &skiq.card[card].fpga_priv.board_id);
    }

    if( 0 == status )
    {
        // Cache the Extended Board ID
        status = fpga_ctrl_read_board_id_ext(card, &skiq.card[card].fpga_priv.board_id_ext);
    }

    if( 0 == status )
    {
        // Cache the FPGA device
        p_param->fpga_device = _get_fpga_device( card, _skiq_get_fpga_board_id(card),
                                                 _skiq_get_fpga_board_id_ext(card) );

        // Cache the FMC carrier here now that .board_id has been cached
        skiq.card[card].card_params.part_fmc_carrier = \
            board_id_to_fmc_carrier( card, _skiq_get_fpga_board_id(card),
                                     _skiq_get_fpga_board_id_ext(card) );
    }

    if( 0 == status )
    {
        // Cache the FPGA capabilities
        status = fpga_ctrl_read_capabilities(card, &skiq.card[card].fpga_priv.caps);
    }


    return status;
}

/*****************************************************************************/
/** @brief Uninitializes features related to the Sidekiq's FPGA.

    @param card card index of the Sidekiq of interest

    @related    _skiq_init_fpga()

    @return int32_t
    @retval 0 on success
*/
static int32_t _skiq_exit_fpga(uint8_t card)
{
    int32_t result = 0;

    result = fpga_ctrl_reset_config( card );
    skiq_fpga_param_t* p_param = &(skiq.card[card].fpga_params);
    skiq_fpga_priv_t *p_priv_param = &(skiq.card[card].fpga_priv);

    *p_param = (skiq_fpga_param_t) SKIQ_FPGA_PARAM_INITIALIZER;
    *p_priv_param = (skiq_fpga_priv_t) SKIQ_FPGA_PRIV_INITIALIZER;

    return result;
}

/*****************************************************************************/
/** @brief Initializes a given Rx handle for use with streaming. The members
    of skiq_rx_param_t should also be populated in this function.

    @param card card index of the Sidekiq of interest
    @param hdl the Rx port to initialize.

    @return int32_t: status where 0=success, anything else is an error
*/
int32_t _skiq_init_rx_hdl(uint8_t card,
                          skiq_rx_hdl_t hdl)
{
    skiq_rx_param_t* p_params;
    rfic_t rfic;
    rfe_t rfe;
    uint32_t addr = 0;
    int32_t status = 0;

    // iterate through each rx channel
    rfic = _skiq_get_rx_rfic_instance(card,hdl);
    rfe = _skiq_get_rx_rfe_instance(card,hdl);

    p_params = &(skiq.card[card].rx_list[hdl].params);
    p_params->handle = hdl;

    addr = FPGA_REG_DMA_CTRL_(hdl);

    /* disable all Rx interfaces by default */
    skiq.card[card].rx_list[hdl].streaming = false;

    /* default the "start acquisition" bit in each interface to off */
    sidekiq_fpga_reg_RMWV(card, 0, DMA_RX_FIFO_CTRL, addr);

    /* just set the tune mode to normal */
    skiq.card[card].rx_list[hdl].tune_mode = skiq_freq_tune_mode_standard;
    skiq.card[card].rx_list[hdl].num_hop_freqs = 0;

    if( 0 == _skiq_validate_rx_hdl(card, hdl) )
    {
        if( 0 == status )
        {
            status = rfe_read_rx_filter_capabilities(&rfe,
                                                     p_params->filters,
                                                     &(p_params->num_filters));
        }

        if( 0 == status )
        {
            status = rfe_read_rx_attenuation_range(&rfe,
                                                   &p_params->atten_quarter_db_max,
                                                   &p_params->atten_quarter_db_min);
        }

        if( 0 == status )
        {
            status = rfic_read_rx_gain_range(&rfic,
                                             &(p_params->gain_index_max),
                                             &(p_params->gain_index_min));
        }

        if( 0 == status )
        {
            status = rfic_read_max_rx_sample_rate(&rfic,
                                               &(p_params->sample_rate_max));
        }

        if( 0 == status )
        {
            status = rfic_read_min_rx_sample_rate(&rfic,
                                               &(p_params->sample_rate_min));
        }

        if( 0 == status )
        {
            status = rfic_read_adc_resolution(&rfic,
                                              &(p_params->iq_resolution));
        }

        if( 0 == status )
        {
            tune_read_rx_LO_max_freq(card, hdl, &(p_params->lo_freq_max));
            tune_read_rx_LO_min_freq(card, hdl, &(p_params->lo_freq_min));
        }

        if( 0 == status )
        {
            status=rfe_read_rx_rf_ports_avail( &rfe,
                                               &(p_params->num_fixed_rf_ports),
                                               &(p_params->fixed_rf_ports[0]),
                                               &(p_params->num_trx_rf_ports),
                                               &(p_params->trx_rf_ports[0]) );
        }
        if( 0 == status )
        {
            status=rfic_read_rx_cal_types_avail( &rfic,
                                                 &(p_params->cal_type_mask) );
        }

        /* default to IQ mode */
        if( 0 == status )
        {
            status = skiq_write_rx_data_src(card, hdl, skiq_data_src_iq);
        }

        /* default to manual mode */
        if( 0 == status )
        {
            status = skiq_write_rx_gain_mode(card, hdl, skiq_rx_gain_manual);
        }

        /* enable DC offset correction block by default */
        if( 0 == status )
        {
            if ( _skiq_has_dc_offset_corr( card ) )
            {
                status = skiq_write_rx_dc_offset_corr(card, hdl, true);
            }
        }
    }
    else
    {
        *p_params = (skiq_rx_param_t) SKIQ_RX_PARAM_INITIALIZER;
    }

    return status;
}

/*****************************************************************************/
/** @brief Initializes a given Tx handle for use with streaming. The members
    of skiq_tx_param_t should also be populated in this function.

    @param card card index of the Sidekiq of interest
    @param hdl the Rx port to initialize.

    @return int32_t: status where 0=success, anything else is an error
*/
int32_t _skiq_init_tx_hdl(uint8_t card,
                          skiq_tx_hdl_t hdl)
{
    skiq_tx_param_t* p_params;
    rfic_t rfic;
    rfe_t rfe;
    int32_t status = 0;

    rfic = _skiq_get_tx_rfic_instance(card,hdl);
    rfe = _skiq_get_tx_rfe_instance(card,hdl);

    p_params = &(skiq.card[card].tx_list[hdl].params);
    p_params->handle = hdl;

    skiq.card[card].tx_list[hdl].streaming = false;
    skiq.card[card].tx_list[hdl].tx_transfer_mode = skiq_tx_transfer_mode_sync;
    /* default the Tx flow mode to immediate */
    skiq.card[card].tx_list[hdl].flow_mode = skiq_tx_immediate_data_flow_mode;
    /* default the block size */
    skiq_write_tx_block_size( card, hdl, DEFAULT_TX_BLOCK_SIZE );

    /* just set the tune mode to normal */
    skiq.card[card].tx_list[hdl].tune_mode = skiq_freq_tune_mode_standard;
    skiq.card[card].tx_list[hdl].num_hop_freqs = 0;

    if( 0 == _skiq_validate_tx_hdl(card, hdl) )
    {
        if( 0 == status )
        {
            status = rfe_read_tx_filter_capabilities(&rfe,
                                                     p_params->filters,
                                                     &(p_params->num_filters));
        }

        if( 0 == status )
        {
            status = rfic_read_tx_attenuation_range(&rfic,
                                                    &p_params->atten_quarter_db_max,
                                                    &p_params->atten_quarter_db_min);
        }

        if( 0 == status )
        {
            status = rfic_read_max_tx_sample_rate(&rfic, &(p_params->sample_rate_max));
        }

        if( 0 == status )
        {
            status = rfic_read_min_tx_sample_rate(&rfic, &(p_params->sample_rate_min));
        }

        if( 0 == status )
        {
            status = rfic_read_dac_resolution(&rfic, &(p_params->iq_resolution));
        }

        if( 0 == status )
        {
            tune_read_tx_LO_max_freq(card, hdl, &(p_params->lo_freq_max));
            tune_read_tx_LO_min_freq(card, hdl, &(p_params->lo_freq_min));
        }

        if( 0 == status )
        {
            status=rfe_read_tx_rf_ports_avail( &rfe,
                                               &(p_params->num_fixed_rf_ports),
                                               &(p_params->fixed_rf_ports[0]),
                                               &(p_params->num_trx_rf_ports),
                                               &(p_params->trx_rf_ports[0]) );
        }

        if( 0 == status )
        {
            status = rfic_read_tx_attenuation( &(rfic),
                                               &(skiq.card[card].tx_list[hdl].attenuation));
        }
    }
    else
    {
        *p_params = (skiq_tx_param_t) SKIQ_TX_PARAM_INITIALIZER;
    }

    return status;
}

/*****************************************************************************/
/** @brief Performs the minimum level of initialization needed for the RF
    system in order for a Sidekiq to function at a basic level. The members
    of skiq_rf_param_t that do not require full initialization should also be
    populated in this function.

    @param card card index of the Sidekiq of interest

    @return int32_t: status where 0=success, anything else is an error
*/
int32_t _skiq_init_rf_basic(uint8_t card)
{
    skiq_rf_param_t* p_param = &(skiq.card[card].rf_params);
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( card );

    LOCK_REGS(card);

    // read the configured ref clock
    status = card_read_ref_clock(card,
                                 &(p_param->ref_clock_config));

    // make sure it's a valid clock and not mPCIe B before printing
    if (0 == status)
    {
        if ( (skiq.card[card].hardware_vers != skiq_hw_vers_mpcie_b) &&
             (part != skiq_z2p) )
        {
            if ( p_param->ref_clock_config != skiq_ref_clock_invalid )
            {
                if((p_param->ref_clock_config == skiq_ref_clock_external) ||
                    (p_param->ref_clock_config == skiq_ref_clock_carrier_edge))
                {
                    card_read_ext_ref_clock_freq(card, &(p_param->ref_clock_freq));
                    skiq_info("Sidekiq card %" PRIu8 " is configured for %s %d Hz reference "
                              "clock\n", card, ref_clock_select_cstr(p_param->ref_clock_config),
                              p_param->ref_clock_freq);
                }
                else if( p_param->ref_clock_config == skiq_ref_clock_host )
                {
                    if ( part == skiq_x4 )
                    {
                        p_param->ref_clock_freq = SKIQ_X4_REF_CLOCK_DEFAULT;
                    }
                    else
                    {
                        card_read_ext_ref_clock_freq(card, &(p_param->ref_clock_freq));
                    }
                    skiq_info("Sidekiq card %" PRIu8 " is configured for %s %d Hz reference "
                              "clock\n", card, ref_clock_select_cstr(p_param->ref_clock_config),
                              p_param->ref_clock_freq);
                }
                else
                {
                    switch( _skiq_get_part( card ) )
                    {
                        case skiq_mpcie:
                            p_param->ref_clock_freq = SKIQ_MPCIE_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_m2:
                            p_param->ref_clock_freq = SKIQ_M2_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_m2_2280:
                            p_param->ref_clock_freq = SKIQ_M2_2280_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_x2:
                            p_param->ref_clock_freq = SKIQ_X2_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_z2:
                            p_param->ref_clock_freq = SKIQ_Z2_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_x4:
                            p_param->ref_clock_freq = SKIQ_X4_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_z3u:
                            p_param->ref_clock_freq = SKIQ_Z3U_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_nv100:
                            p_param->ref_clock_freq = SKIQ_NV100_REF_CLOCK_DEFAULT;
                            break;
                        case skiq_part_invalid:
                        default:
                            /* The `skiq_part_t` is derived from values stored in EEPROM, so it can
                             * represent a future product that this libsidekiq version does not
                             * support or know about */
                            skiq_unknown("Sidekiq card %" PRIu8 " unknown/unsupported part!\n",
                                         card);
                            status = -ENXIO; /* No such device or address */
                            break;
                    }
                    if ( status == 0 )
                    {
                        skiq_info("Sidekiq card %" PRIu8 " is configured for an internal reference "
                                  "clock\n", card);
                    }
                }
            }
            else
            {
                skiq_warning("Reference clock configuration invalid for card %" PRIu8 "\n", card);
            }
        }
    }

    /* load any card-specific calibration values */
    if ( status == 0 )
    {
        status = card_calibration_load( card, part );

        if ( status == -ENOTSUP )
        {
            /* if card_calibration_load returns -ENOTSUP, the storage method is not available or not
             * supported, but it is not a reason to halt initialization */
            skiq_warning("Calibration storage device not available, skipping loading on card %u\n",
                         card);
            status = 0;
        }
    }

    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** @brief Uninitializes the minimum level of initialization needed for the RF
    system in order for a Sidekiq to function at a basic level.

    @param[in]  card    card index of the Sidekiq of interest

    @related    _skiq_init_rf_basic()

    @return int32_t
    @retval 0   on success
*/
static int32_t _skiq_exit_rf_basic(uint8_t card)
{
    int32_t result = 0;
    int32_t status = 0;

    skiq_rf_param_t* p_param = &(skiq.card[card].rf_params);

    p_param->ref_clock_config = skiq_ref_clock_invalid;
    p_param->ref_clock_freq = 0;

    LOCK_REGS(card);
    status = card_calibration_clear( card );
    if (0 != status)
    {
        skiq_warning( "Warning: calibration not loaded or failed to clear"
            " calibration resources on card %u (%" PRIi32 ")\n",
            card, status );
    }
    UNLOCK_REGS(card);

    return result;
}


/*****************************************************************************/
/** @brief Performs additional initialization that is not perfored in
    _skiq_init_rf_basic such that the Sidekiq can fully send and receive
    samples. The members of skiq_rf_param_t that require full initialization
    should also be populated in this function.

    Note: Parameters related to the Rx and Tx ports should be set in their
    respective helper functions.

    @param card card index of the Sidekiq of interest

    @return int32_t: status where 0=success, anything else is an error
*/
int32_t _skiq_init_rf_full(uint8_t card)
{
    skiq_rf_param_t* p_param = &(skiq.card[card].rf_params);
    skiq_tx_hdl_t tx_hdl = skiq_tx_hdl_end;
    skiq_rx_hdl_t rx_hdl = skiq_rx_hdl_end;
    skiq_part_t part;
    rfic_t rfic_instance;
    rfe_t rfe_instance;
    uint8_t fpga_maj = skiq.card[card].fpga_params.version_major;
    uint8_t fpga_min = skiq.card[card].fpga_params.version_minor;
    uint8_t fpga_patch = skiq.card[card].fpga_params.version_patch;
    uint16_t init_warp = 0;
    uint16_t read_warp = 0;
    uint8_t fpga_rx_chans = 0;
    uint8_t fpga_tx_chans = 0;
    skiq_rx_hdl_t rx_hdl_idx = skiq_rx_hdl_end;
    skiq_tx_hdl_t tx_hdl_idx = skiq_tx_hdl_end;
    int32_t status = 0;
    skiq_rf_port_config_t rf_port_config;

    if ( !_skiq_meets_FPGA_VERSION( card, FPGA_VERS_MINIMUM ) )
    {
        skiq_error("FPGA version v%" PRIu8 ".%" PRIu8 ".%" PRIu8 " does not meet minimum "
                   "requirement of %s on card %" PRIu8 "\n", fpga_maj, fpga_min, fpga_patch,
                   FPGA_VERS_MINIMUM_CSTR, card);

        return -ENOTSUP;
    }
    else if ( _skiq_meets_FPGA_VERSION( card, FPGA_VERS_MAXIMUM ) )
    {
        skiq_error("FPGA version v%" PRIu8 ".%" PRIu8 ".%" PRIu8 " exceeds maximum compatible "
                   "version (must be strictly less than %s) on card %" PRIu8 "\n", fpga_maj,
                   fpga_min, fpga_patch, FPGA_VERS_MAXIMUM_CSTR, card);

        return -ENOTSUP;
    }

    // read hardware interface version before setting up RFE
    skiq_card_t *p_card = &(skiq.card[card]);
    status = card_read_hw_iface_vers(card, &p_card->hw_iface_vers);
    if ( 0 != status )
    {
        skiq_unknown("Sidekiq card %u (%s rev %c) was unable to read hardware interface "
                        "version; using default version (v255.255) to select RF front-end\n", card,
                        part_cstr( p_card->card_params.part_type ),
                        hw_rev_cstr( p_card->hardware_rev ));
    }

    // setup the RFE
    p_card->p_rfe_funcs = rfe_init_functions(_skiq_get_part(card), _skiq_get_hw_rev(card),  _skiq_get_hw_iface_vers(card));
    if ( p_card->p_rfe_funcs == NULL )
    {
        skiq_unknown("Sidekiq card %u (%s rev %c) has no available RF front-end "
                        "functionality in this library version; a later version may be "
                        "necessary\n", card,
                        part_cstr( p_card->card_params.part_type ),
                        hw_rev_cstr( p_card->hardware_rev ));
        status = -ENOTSUP;
        return status;
    }

    if ( 0 == status )
    {
        // free any rfic functions that have been initialized
        rfic_free_functions( p_card->p_rfic_funcs );
        
        // setup the RFIC
        p_card->p_rfic_funcs =
            rfic_init_functions( p_card->card_params.part_type,
                                    p_card->hardware_rev );
        if ( p_card->p_rfic_funcs == NULL )
        {
            skiq_unknown("Sidekiq card %u (%s rev %c) has no available RFIC "
                            "functionality in this library version; a later version may be "
                            "necessary\n", card,
                            part_cstr( p_card->card_params.part_type ),
                            hw_rev_cstr( p_card->hardware_rev ));
            status = -ENOTSUP;
            return status;
        }
    }

    /* if we're using RPC, we have to disable the IO cache since it's updated both with register transactions
    on the client as well as through RPC calls, resulting in a potential mismatch.  A specific example where
    issues are encountered are when the TX frequency is updated via an RPC call, resulting in the server IOE 
    cache having the IOE configured a certain way based on the requested frequency.  However, at a later point
    in time, the TX PA is enabled (which results in a IOE ran on the client).  If the cache is used, the client
    cache does not have the frequency update and when updating the PA, will used the client cached version which
    is not matching the server cache. */
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        io_exp_enable_cache(card, false);
    }
    else
    {
        io_exp_enable_cache(card, true);
    }

    tune_select_functions_for_card(card);
    if( 0 != (status = tune_card_init(card)) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to initialize LO tune module (status=%d)\n", status);
        hal_critical_exit(status);
        return status;
    }

    rfic_display_version_info( skiq.card[card].p_rfic_funcs );

    // initialize the RFIC ports for all RX handles
    for( rx_hdl_idx=skiq_rx_hdl_A1; rx_hdl_idx<skiq_rx_hdl_end; rx_hdl_idx++ )
    {
        rfe_instance = _skiq_get_rx_rfe_instance(card, rx_hdl_idx);
        rfe_read_rfic_port_for_rx_hdl( &rfe_instance,
                                        &(rfe_instance.p_id->port) );
    }
    // initialize the RFIC ports for all TX handles
    for( tx_hdl_idx=skiq_tx_hdl_A1; tx_hdl_idx<skiq_tx_hdl_end; tx_hdl_idx++ )
    {
        rfe_instance = _skiq_get_tx_rfe_instance(card, tx_hdl_idx);
        rfe_read_rfic_port_for_tx_hdl( &rfe_instance,
                                        &(rfe_instance.p_id->port) );
    }

    // for init, we only need the card/chip...just use the ID of the first RX handle
    rfe_instance = _skiq_get_generic_rfe_instance(card);
    rfic_instance = _skiq_get_generic_rfic_instance(card);

    // update RPC functions for RFIC
    if( 0 != (status = rfic_add_rpc_functions(card, rfic_instance.p_funcs)) )
    {
        skiq_error("Unable to update RFIC RPC functions for card %u (status=%d)\n", card, status);
        return status;
    }

    rfe_init( &(rfe_instance) );
    status = rfic_reset_and_init( &(rfic_instance) );
    if (status != 0)
    {
        skiq_error("failed to reset and initialize RFIC on card %" PRIu8 " (status %" PRIi32 ")\n",
                    card, status);
        hal_critical_exit(status);
        return status;
    }
    rfe_post_init( &(rfe_instance) );

    // if we're working with m.2, then we can have var # rx/tx channels
    // Note: this probably looks goofy, but the TX chan configuration was provided by the
    // FPGA in a different release than the RX chan configuration.  Also, both TX and RX
    // channels supported are available in the same register and mapped the same way.
    // As a result, we query the number of channels regardless of FPGA version and then
    // fall back to the appropriate default if the FPGA version is prior to that which
    // supported configuration by the FPGA developer
    if( (skiq.card[card].product_vers == skiq_product_m2_001) ||
        (skiq.card[card].product_vers == skiq_product_m2_002) )
    {
        get_fpga_num_chans( skiq.card[card].card_params.card,
                            &fpga_rx_chans,
                            &fpga_tx_chans );

        if ( !_skiq_meets_FPGA_VERSION( card, FPGA_VERS_TX_CHANS_VALID ) )
        {
            // prior builds always had 2 TX channels
            fpga_tx_chans = 2;
        }

        if ( !_skiq_meets_FPGA_VERSION( card, FPGA_VERS_RX_CHANS_VALID ) )
        {
            // prior builds always had 2 RX channels
            fpga_rx_chans = 2;
        }
    }
    else
    {
        // mPCIe only ever had 1 / 2
        fpga_tx_chans = 1;
        fpga_rx_chans = 2;
    }

    /* Two local macros to make the next two switch statements look a little cleaner */
#define ADD_RX_HANDLE(_p_param, _hdl)     do { (_p_param)->rx_handles[(_p_param)->num_rx_channels++] = _hdl; } while (0)
#define ADD_TX_HANDLE(_p_param, _hdl)     do { (_p_param)->tx_handles[(_p_param)->num_tx_channels++] = _hdl; } while (0)

    /* Prior to populating rx_handles[] and tx_handles[], set all entries to something invalid
     * (skiq_xx_hdl_end) */
    for ( rx_hdl = skiq_rx_hdl_A1; rx_hdl < skiq_rx_hdl_end; rx_hdl++ )
    {
        p_param->rx_handles[rx_hdl] = skiq_rx_hdl_end;
    }
    for ( tx_hdl = skiq_tx_hdl_A1; tx_hdl < skiq_tx_hdl_end; tx_hdl++ )
    {
        p_param->tx_handles[tx_hdl] = skiq_tx_hdl_end;
    }

    // initialize num rx/tx channels based on hardware type
    part = _skiq_get_part( card );
    p_param->num_rx_channels = 0;
    p_param->num_tx_channels = 0;
    switch( skiq.card[card].product_vers )
    {
        case skiq_product_mpcie_001:
            ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);
            ADD_RX_HANDLE(p_param, skiq_rx_hdl_A2);

            ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);
            break;

        case skiq_product_mpcie_002:
            ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);

            ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);
            break;

        case skiq_product_m2_001:
            ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);
            ADD_RX_HANDLE(p_param, skiq_rx_hdl_A2);

            ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);
            ADD_TX_HANDLE(p_param, skiq_tx_hdl_A2);
            break;

        case skiq_product_m2_002:
            ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);

            ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);

            // set the RF port to invalid for the 2nd tx path
            skiq.card[card].tx_list[skiq_tx_hdl_A2].rf_id.port = INVALID_RF_PORT;
            break;

        // TODO: handle this differently
        case skiq_product_reserved:
            if ( part == skiq_x2 )
            {
                // # RX / TX channels currently fixed
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A2);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_B1);

                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);
                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A2);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else if( part == skiq_z2 )
            {
                // # RX / TX channels currently fixed
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);

                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else if( part == skiq_x4 )
            {
                // # RX / TX channels currently fixed
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A2);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_B1);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_B2);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_C1);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_D1);

                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);
                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A2);
                ADD_TX_HANDLE(p_param, skiq_tx_hdl_B1);
                ADD_TX_HANDLE(p_param, skiq_tx_hdl_B2);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else if ( part == skiq_m2_2280 )
            {
                // # RX / TX channels currently fixed
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);

                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else if( part == skiq_z2p )
            {
                // # RX / TX channels currently fixed
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);
                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else if( part == skiq_z3u )
            {
                // # RX / TX channels currently fixed
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A2);

                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else if ( part == skiq_nv100 )
            {
                /* # RX / TX channels currently fixed */
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_A1);

                /* Prohibit A2 on NV100 unless the bitstream supports it*/
                if (_skiq_meets_FPGA_VERSION( card, FPGA_VERS_A2_B1_META ) )
                {
                    ADD_RX_HANDLE(p_param, skiq_rx_hdl_A2);
                }
                ADD_RX_HANDLE(p_param, skiq_rx_hdl_B1);

                ADD_TX_HANDLE(p_param, skiq_tx_hdl_A1);
                ADD_TX_HANDLE(p_param, skiq_tx_hdl_B1);

                fpga_rx_chans = p_param->num_rx_channels;
                fpga_tx_chans = p_param->num_tx_channels;
            }
            else
            {
                /* The `product_vers` is stored in EEPROM, so it can represent a future product that
                 * this libsidekiq version does not support or know about */
                skiq_unknown("An unknown/unsupported product type (%u) was detected\n", part);
                status=-1;
            }
            break;

        default:
            /* The `product_vers` is stored in EEPROM, so it can represent a future product that
             * this libsidekiq version does not support or know about */
            skiq_unknown("An unknown/unsupported product version (%u) was detected\n",
                         skiq.card[card].product_vers);
            break;
    }

#undef ADD_RX_HANDLE
#undef ADD_TX_HANDLE

    skiq_info("Card %u: number of tx channels supported %u, number of rx channels supported %u\n",
              card,
              p_param->num_tx_channels,
              p_param->num_rx_channels);

    if( p_param->num_tx_channels > fpga_tx_chans )
    {
        skiq_info("Number of FPGA TX channels is %u but Sidekiq %s supports %u channels, "
                  "reducing available TX channels to match FPGA capabilities\n", fpga_tx_chans,
                  part_cstr( _skiq_get_part( card ) ), p_param->num_tx_channels);
        p_param->num_tx_channels = fpga_tx_chans;
    }

    if( p_param->num_rx_channels > fpga_rx_chans )
    {
        skiq_info("Number of FPGA RX channels is %u but Sidekiq %s supports %u channels, "
                  "reducing available RX channels to match FPGA capabilities\n", fpga_rx_chans,
                  part_cstr( _skiq_get_part( card ) ), p_param->num_rx_channels);
        p_param->num_rx_channels = fpga_rx_chans;
    }

    /* need to make sure the DMA engine was shutdown / disabled properly over all rx_handles[]  */
    int8_t hdl_idx;
    for ( hdl_idx = 0; hdl_idx < p_param->num_rx_channels; hdl_idx++ )
    {
        uint32_t addr;
        uint32_t data = 0;
        rx_hdl = p_param->rx_handles[hdl_idx];
        addr = FPGA_REG_DMA_CTRL_(rx_hdl);

        /* turn off the RX FIFO and TX FIFO associated with the handle */
        RBF_SET(data, 0, DMA_RX_FIFO_CTRL);
        RBF_SET(data, 1, DMA_TX_FIFO_CTRL);
        sidekiq_fpga_reg_write_and_verify( card, addr, data );
    }

    fpga_ctrl_finalize_stream_disable( card );

    /* initialize the ad9361 using the appropriate warp voltage setting */
    /* determine the default warp voltage (if any) */
    if( skiq_read_user_tcvcxo_warp_voltage(card, &init_warp) != 0 )
    {
        if( skiq_read_default_tcvcxo_warp_voltage(card, &init_warp) != 0 )
        {
            init_warp = 0;
            skiq_info("No stored default warp voltage is available for card %u, using the library "
                      "default\n", card);
        }
    }

    /* initialize the RF front end */
    init_rx_interfaces( card );
    // save off the warp voltage config if the read doesn't match the init value
    if( skiq_read_tcvcxo_warp_voltage( card, &read_warp ) == 0 )
    {
        if( (init_warp != read_warp) &&
            (init_warp != 0) )
        {
            skiq_write_tcvcxo_warp_voltage( card, init_warp );
        }
    }

    /* default the priority to minimum */
    skiq.card[card].thread_priority = sched_get_priority_min(SCHED_FIFO);

    for( hdl_idx = 0; hdl_idx < p_param->num_tx_channels; hdl_idx++ )
    {
        _skiq_init_tx_hdl(card, p_param->tx_handles[hdl_idx]);
    }

    /* default the # of threads */
    skiq_write_num_tx_threads( card, DEFAULT_TX_NUM_THREADS );

    /* disable the packed mode */
    skiq.card[card].iq_packed = false;

    /* cancel any pending PPS transactions */
    fpga_ctrl_cancel_1pps(card);

    // TODO: push this down to the RFIC layer in the reset_and_init
    if ( (_skiq_get_part(card) != skiq_x2) &&
         (_skiq_get_part(card) != skiq_x4) &&
         (_skiq_get_part(card) != skiq_nv100) )
    {
        if ( p_param->num_rx_channels > 0 )
        {
            /* get the first RX handle and initialize it */
            rx_hdl = p_param->rx_handles[0];

            /* our filter's have a unity gain, so we should set the FIR gain to 0 */
            skiq_write_rx_fir_gain( card, rx_hdl, skiq_rx_fir_gain_0);
            skiq_write_rx_sample_rate_and_bandwidth( card, rx_hdl, 1000000, 500000 );
        }

        if ( p_param->num_tx_channels > 0 )
        {
            /* get the first TX handle and initialize it */
            tx_hdl = p_param->tx_handles[0];

            /* init the FIR filter gain settings */
            skiq_write_tx_fir_gain(card, tx_hdl, skiq_tx_fir_gain_0);
            skiq_write_tx_sample_rate_and_bandwidth( card, tx_hdl, 1000000, 500000 );
        }
    }

    /* configure RX as having no data rate and set rx transfer timeout
     * to non-blocking */
    rx_configure( card );
    skiq_xport_rx_set_transfer_timeout( card, RX_TRANSFER_NO_WAIT );

    // initialize the RF port config based on what's available
    // only set to TRX if that's all that's available
    skiq_read_rf_port_config_avail( card,
                                    &(p_param->is_rf_port_fixed),
                                    &(p_param->is_rf_port_trx_supported) );
    skiq.card[card].rf_port_config = -1; // force to invalid so it's configured
    if( p_param->is_rf_port_trx_supported && !(p_param->is_rf_port_fixed) )
    {
        rf_port_config = skiq_rf_port_config_trx;
    }
    else
    {
        // default to fixed
        rf_port_config = skiq_rf_port_config_fixed;
    }

    // make sure to actually config the hardware to match (only matters for m2 C)
    skiq_write_rf_port_config( card, rf_port_config );

    // configure the timestamp base to the default setting
    skiq_write_tx_timestamp_base(card, skiq.card[card].timestamp_base);

    // Read FPGA system timestamp frequency at the end of full RF initialization
    status = fpga_ctrl_read_sys_timestamp_freq(card, &skiq.card[card].fpga_params.sys_timestamp_freq);

    return status;
}

/*****************************************************************************/
/**
    @brief  Stop RX & TX streaming on all handles

    @param[in]  card    card index of the Sidekiq of interest

    @note   This function assumes that the card's registers are already locked
            by the LOCK_REGS() macro.

    @return int32_t
    @retval 0       on success
    @retval -ERANGE if the card index isn't valid
    @retval -ENODEV if the card isn't active
    @retval -EDOM   if the TX handle isn't valid
*/
static int32_t _skiq_stop_streaming(uint8_t card)
{
    int32_t status = 0;
    int32_t result = 0;
    skiq_rx_hdl_t rx_hdl = skiq_rx_hdl_A1;
    ARRAY_WITH_DEFAULTS(skiq_rx_hdl_t, rx_handles, skiq_rx_hdl_end, skiq_rx_hdl_end);
    skiq_tx_hdl_t tx_hdl = skiq_tx_hdl_A1;
    uint8_t num_handles = 0, hdl_idx;

    /*
       Stop RX streaming on all handles; start by creating a list of all of the active handles
       and then attempt to shut them all down.
    */
    for (hdl_idx = 0; (hdl_idx < _skiq_get_nr_rx_hdl( card )) && ( status == 0 ); hdl_idx++)
    {
        rx_hdl = _skiq_get_rx_hdl( card, hdl_idx );

        if ( rx_hdl < skiq_rx_hdl_end )
        {
            if (is_rx_hdl_streaming(card, rx_hdl))
            {
                rx_handles[num_handles++] = rx_hdl;
            }
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, _skiq_get_nr_rx_hdl( card ), hdl_idx);
            status = -EPROTO;
        }
    }

    if ( ( status == 0 ) && ( 0 < num_handles ) )
    {
        /* Some handles were streaming; stop them. */
        result = _stop_rx_streaming_multi(card, &(rx_handles[0]), num_handles,
                    skiq_trigger_src_immediate, 0);
        if (0 != result)
        {
            skiq_warning("Failed to stop RX streaming on all handles for card %" PRIu8 " (result"
                " code %" PRIi32 "); continuing\n", card, result);
            status = result;
        }
    }

    /* Stop TX streaming on all active handles. */
    for (hdl_idx = 0; (hdl_idx < _skiq_get_nr_tx_hdl( card )) && ( status == 0 ); hdl_idx++)
    {
        tx_hdl = _skiq_get_tx_hdl( card, hdl_idx );

        if ( tx_hdl < skiq_tx_hdl_end )
        {
            if ( is_tx_hdl_streaming( card, tx_hdl ) )
            {
                /*
                  skiq_stop_tx_streaming() returns:
                  -ERANGE if the card index isn't valid
                  -ENODEV if the card isn't active
                  -EDOM   if the TX handle isn't valid
                */
                result = skiq_stop_tx_streaming(card, tx_hdl);
                if ((-ERANGE == result) || (-ENODEV == result))
                {
                    /* No need to worry about the card. */
                    break;
                }
                else if ((-EDOM != result) && (0 != result))
                {
                    /* The error was something other than an invalid TX handle or success. */

                    skiq_warning("Failed to stop TX streaming on handle %s for card %" PRIu8
                                 " (result code %" PRIi32 "); continuing\n", tx_hdl_cstr(tx_hdl),
                                 card, result);
                    status = result;
                }
            }
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds transmit handle for card %" PRIu8
                       " (number of TX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, _skiq_get_nr_tx_hdl( card ), hdl_idx);
            status = -EPROTO;
        }
    }

    return (status);
}

/*****************************************************************************/
/** @brief Performs additional uninitialization that is not performed in
    _skiq_exit_rf_basic.

    @param[in]  card    card index of the Sidekiq of interest

    @related    _skiq_init_rf_full()

    @return int32_t
    @retval 0   on success
*/
static int32_t _skiq_exit_rf_full(uint8_t card)
{
    int32_t result = 0;
    skiq_rf_param_t* p_param = &(skiq.card[card].rf_params);

    /** @todo   Does the RF port config need to be defaulted here? */

    LOCK_REGS(card);

    /* Stop streaming on all handles */
    result = _skiq_stop_streaming(card);
    if (0 != result)
    {
        skiq_warning("Failed to stop streaming on all handles on card %" PRIu8
            " (status code %" PRIi32 "); continuing\n", card, result);
    }

    tune_card_exit(skiq.card[card].card_params.card);

    rfic_t rfic_instance = _skiq_get_generic_rfic_instance( card );
    rfe_t rfe_instance = _skiq_get_generic_rfe_instance( card );
    rfe_pre_release( &(rfe_instance) );
    rfic_release( &(rfic_instance) );
    rfe_release( &(rfe_instance) );

    // update RPC functions for RFIC
    if( 0 != (result = rfic_remove_rpc_functions(card, rfic_instance.p_funcs)) )
    {
        skiq_error("Unable to remove RFIC RPC functions for card %u (status=%d)\n", card, result);
        UNLOCK_REGS(card);
        return result;
    }


    *p_param = (skiq_rf_param_t) SKIQ_RF_PARAM_INITIALIZER;

    UNLOCK_REGS(card);

    return result;
}

/*****************************************************************************/
/** The skiq_init() function is responsible for performing all
    initialization tasks for the sidekiq platform.

    @ingroup cardfunctions
    @see skiq_init_by_serial_str
    @see skiq_exit
    @see skiq_enable_cards
    @see skiq_enable_cards_by_serial_str
    @see skiq_disable_cards

    @param[in] type the transport type that is required:
                type_auto: automatically detect and use available transport.
                type_pcie: communicate with Sidekiq over PCIe.  If USB is available
                it will also be used for certain functionality.
                type_usb: communicate with Sidekiq entirely over USB.  A USB
                FPGA bitstream must be utilized if streaming.
                type_custom: communicate with Sidekiq using the registered
                             transport implementation provided by a call to
                             skiq_register_custom_transport().  If USB is available,
                             it will also be used for certain functionality.

    @param[in] level the transport functionality level of initialization that is required:
                level_basic: minimal initialization necessary to bring up the
                             requested transport interface for register
                             reads/writes, and initialize the mutexes that
                             serializes access to libsidekiq
                level_full:  Same as level_basic + perform the complete bring up
                             of all hardware (most applications concerned with
                             sending/receiving RF will use this)

    @attention  As of libsidekiq v4.8.0, the @a type parameter is ignored as the
                transport type is automatically set to ::skiq_xport_type_auto, which
                will select the correct transport for the specified card(s).

    @attention  ::skiq_init() and ::skiq_init_by_serial_str() should only be called when starting an
                application or after ::skiq_exit() has been called; these functions are not
                designed to be called multiple times to initialize individual cards.

    @param[in] p_card_nums pointer to the list of Sidekiq cards to be initialized
    @param[in] num_cards number of Sidekiq cards to initialize

    @return int32_t status where 0=success, anything else is an error
    @retval -EEXIST libsidekiq has already been initialized in this application without
                    skiq_exit() being called
    @retval -E2BIG  if the number of cards requested exceeds the maximum (::SKIQ_MAX_NUM_CARDS)
    @retval -EINVAL if one of the specified cards numbers exceeds the maximum of a valid
                    card with that number cannot be found
*/
EPIQ_API int32_t skiq_init( skiq_xport_type_t type,
			    skiq_xport_init_level_t level,
			    uint8_t *p_card_nums,
			    uint8_t num_cards )
{
    int32_t status = 0;
    uint8_t i = 0;

    if( skiq.initialized )
    {
        skiq_warning("libsidekiq is already initialized (skiq_init(), skiq_init_without_cards(), or"
            " skiq_init_by_serial_str() was already called from this application)\n");
        return -EEXIST;
    }

    /* Initialize some of the card params */
    for( i = 0; i < SKIQ_MAX_NUM_CARDS; i++ )
    {
        skiq.card[i].card_params.card = INVALID_CARD_INDEX;
    }

#if (!defined __MINGW32__)
    LOCK(log_callback_mutex);
    if( skiq.log_msg_cb == _skiq_syslog )
    {
        /* immediately open syslog */
        openlog( SKIQ_LOGNAME, (LOG_PID | LOG_NDELAY | LOG_CONS | LOG_PERROR), LOG_USER );
    }
    UNLOCK(log_callback_mutex);
#endif /* __MINGW32__ */

    /* print out the libsidekiq version info */
    _skiq_log(SKIQ_LOG_INFO,
              "libsidekiq v%u.%u.%u%s (g%s)\n",
              LIBSIDEKIQ_VERSION_MAJOR,
              LIBSIDEKIQ_VERSION_MINOR,
              LIBSIDEKIQ_VERSION_PATCH,
              LIBSIDEKIQ_VERSION_LABEL,
              _GIT_HASH);

    /* make sure that we're not requesting too many cards */
    if( num_cards > SKIQ_MAX_NUM_CARDS )
    {
        _skiq_log(SKIQ_LOG_ERROR,
                  "maximum number of sidekiq cards is %u\n",
                  SKIQ_MAX_NUM_CARDS );
        status = -E2BIG;
    }

    if( 0 == status )
    {
        status = _skiq_probe();
    }

    /* on Windows, it's sufficient to call card_mgr_init() once and that is done by _skiq_probe() */
#ifndef __MINGW32__
    if( 0 == status )
    {
        status = card_mgr_init();
    }
#endif

    /* print out the valid cards that we found */
    for( i = 0; (i < SKIQ_MAX_NUM_CARDS) && (0 == status); i++ )
    {
        if ( skiq.card[i].present && ( skiq.card[i].serial_num != INVALID_SERIAL_NUM ) )
        {
           _serial_num_to_serial_str(skiq.card[i].serial_num,
                                     skiq.card[i].card_params.serial_string);

            if ( (skiq.card[i].hardware_vers == skiq_hw_vers_reserved) ||
                 (skiq.card[i].product_vers == skiq_product_reserved) )
            {
                /* non-legacy hardware / products */
                skiq_info("Sidekiq card %u is serial number=%s, %s (rev %c) (part ES%s-%s-%s)\n",
                      skiq.card[i].card_params.card,
                      skiq.card[i].card_params.serial_string,
                      skiq_part_string(skiq.card[i].card_params.part_type),
                      hw_rev_cstr(skiq.card[i].hardware_rev),
                      skiq.card[i].card_params.part_info.number_string,
                      skiq.card[i].card_params.part_info.revision_string,
                      skiq.card[i].card_params.part_info.variant_string );
            }
            else
            {
                /* legacy hardware / products: mPCIe and M.2 */
                skiq_info("Sidekiq card %u is serial number=%s, hardware %s (rev %c), product %s "
                          "(%s) (part ES%s-%s-%s)\n",
                          skiq.card[i].card_params.card,
                          skiq.card[i].card_params.serial_string,
                          skiq_hardware_vers_string(skiq.card[i].hardware_vers),
                          hw_rev_cstr(skiq.card[i].hardware_rev),
                          skiq_product_vers_string(skiq.card[i].product_vers),
                          skiq_part_string(skiq.card[i].card_params.part_type),
                          skiq.card[i].card_params.part_info.number_string,
                          skiq.card[i].card_params.part_info.revision_string,
                          skiq.card[i].card_params.part_info.variant_string );
            }
        }
    }

    if( 0 == status )
    {
        skiq.initialized = true;

        /* check that requested cards were initialized */
        status = skiq_enable_cards(p_card_nums,
                                   num_cards,
                                   level);
    }

    if( status != 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR,
                  "unable to initialize requested card list, status %d\n",
                  status);
        (void) skiq_exit();
    }

    return status;
}

/*****************************************************************************/
/** The skiq_init_by_serial_str function is identical to skiq_init() except a
    list of serial numbers can be requested instead of card indices.

    @param[in] type the transport type that is required:
                type_pcie: communicate with Sidekiq over PCIe.  If USB is available
                it will also be used for certain functionality.
                type_usb: communicate with Sidekiq entirely over USB.  A USB
                FPGA bitstream must be utilized if streaming.
                type_custom: communicate with Sidekiq using the registered
                             transport implementation provided by a call to
                             skiq_register_custom_transport().  If USB is available,
                             it will also be used for certain functionality.

    @param[in] level the transport functionality level of initialization that is required:
                level_basic: minimal initialization necessary to bring up the
                             requested transport interface for register
                             reads/writes, and initialize the mutexes that
                             serializes access to libsidekiq
                level_full:  Same as level_basic + perform the complete bring up
                             of all hardware (most applications concerned with
                             sending/receiving RF will use this)

    @attention  As of libsidekiq v4.8.0, the @a type parameter is ignored as the
                transport type is automatically set to ::skiq_xport_type_auto, which
                will select the correct transport for the specified card(s).

    @attention  ::skiq_init() and ::skiq_init_by_serial_str() should only be called when starting an
                application or after ::skiq_exit() has been called; these functions are not
                designed to be called multiple times to initialize individual cards.

    @param pp_serial_nums pointer to the list of Sidekiq serial number strings to initialize
    @param num_cards number of Sidekiq cards to initialize
    @param[out] p_card_nums pointer to the list of Sidekiq card indices corresponding with
    serial strings provided; this list should be able to hold at least ::SKIQ_MAX_NUM_CARDS

    @return int32_t  status where 0=success, anything else is an error
    @retval -EEXIST libsidekiq has already been initialized in this application without
                    skiq_exit() being called
    @retval -E2BIG  if the number of cards requested exceeds the maximum (::SKIQ_MAX_NUM_CARDS)
    @retval -ENXIO  if one of the specified serial numbers cannot be found
*/
EPIQ_API int32_t skiq_init_by_serial_str( skiq_xport_type_t type,
					  skiq_xport_init_level_t level,
					  char **pp_serial_nums,
					  uint8_t num_cards,
					  uint8_t *p_card_nums )
{
    int32_t status=0;
    uint8_t cards[SKIQ_MAX_NUM_CARDS];
    uint8_t i=0;

    for( i=0; ((i<num_cards) && (status==0)); i++ )
    {
        status=skiq_get_card_from_serial_string( pp_serial_nums[i],
                                                 &cards[i] );
        if( status != 0 )
        {
            _skiq_log( SKIQ_LOG_ERROR, "Unable to obtain card for serial %s", pp_serial_nums[i] );
            status = -ENXIO;
        }
        else
        {
            p_card_nums[i] = cards[i];
        }
    }
    if( status==0 )
    {
        status = skiq_init( type, level, cards, num_cards );
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_init_without_cards() function initializes the library (like ::skiq_init()) without
    having to specify any cards; this is mainly used when claiming cards dynamically
    after startup using ::skiq_enable_cards().
*/
EPIQ_API int32_t skiq_init_without_cards(void)
{
    /*
        We're not actually initializing a card, but we need to pass something to
        ::skiq_init()... so we'll give it a single card with an invalid card index.
    */
    uint8_t singleCard[1] = { INVALID_CARD_INDEX };

    /** @note   Since v4.8.0 (see ::skiq_init() documentation) the transport is always auto */
    /**
        @note   (Currently) ::skiq_init() does nothing with the initialization level if no
                cards are specified, but we'll just use full in case this changes and somehow
                this value determines the max initialization level for future cards.
    */
    return skiq_init(skiq_xport_type_auto, skiq_xport_init_level_full, &(singleCard[0]), 0);
}

/*****************************************************************************/
/** The skiq_enable_cards() function is responsible for performing all initialization
    tasks for the specified Sidekiq cards.

    @attention  The Sidekiq library must have been previously initialized with skiq_init() or
                skiq_init_by_serial_str(). The transport type is automatically selected based
                on availability.

    @since Function added in API @b v4.8.0

    @ingroup cardfunctions
    @see skiq_init
    @see skiq_init_by_serial_str
    @see skiq_exit
    @see skiq_enable_cards_by_serial_str
    @see skiq_disable_cards

    @param[in] cards array of Sidekiq cards to be initialized
    @param[in] num_cards number of Sidekiq cards to initialize
    @param[in] level [::skiq_xport_init_level_t] the transport functionality
    level of initialization that is required:
      - ::skiq_xport_init_level_basic - minimal initialization necessary to bring
          up the requested transport interface for FPGA / RFIC register
          reads/writes, and initialize the mutexes that serializes access to
          libsidekiq
      - ::skiq_xport_init_level_full - Same as ::skiq_xport_init_level_basic and
          perform the complete bring up of all hardware (most applications
          concerned with sending/receiving RF will use this)

    @return int32_t  status where 0=success, anything else is an error
    @retval -EPERM  if libsidekiq has not been initialized yet (through skiq_init()
                    or skiq_init_by_serial_str())
    @retval -EINVAL if one of the specified card indices is out of range or refers to a
                    non-existent card
    @retval -E2BIG  if the number of cards specified exceeds the maximum (::SKIQ_MAX_NUM_CARDS)
*/
EPIQ_API int32_t skiq_enable_cards(const uint8_t cards[],
                                   uint8_t num_cards,
                                   skiq_xport_init_level_t level)
{
    int32_t status=0;
    uint8_t num_cards_locked = 0;
    uint8_t curr_card = 0;
    uint8_t i = 0;
    bool useExitHandler = false;
    pid_t card_owner = 0;
    ARRAY_WITH_DEFAULTS(enum CardInitState, cardInitState, SKIQ_MAX_NUM_CARDS, NotInitialized);
    skiq_xport_type_t xport_type = skiq_xport_type_auto;

    if (!skiq.initialized)
    {
        skiq_warning(NOT_INITIALIZED_MESSAGE);
        return -EPERM;
    }

    if (SKIQ_MAX_NUM_CARDS < num_cards)
    {
        skiq_warning("Number of cards to enable exceeds maximum (%" PRIu32 ")\n",
                SKIQ_MAX_NUM_CARDS);
        return -E2BIG;
    }

    /* verify requested card have valid serial numbers and are in range */
    for( i = 0; (i < num_cards) && (0 == status); i++ )
    {
        curr_card = cards[i];

        if( (curr_card >= SKIQ_MAX_NUM_CARDS) ||
            (skiq.card[curr_card].card_params.card == INVALID_CARD_INDEX) )
        {
            skiq_error("No valid Sidekiq detected at card index %" PRIu8 "\n", curr_card);
            status = -EINVAL;
        }
    }

    for( i = 0; (i < num_cards) && (0 == status); i++ )
    {
        curr_card = cards[i];

        /* make sure the card isn't in use and we can lock it */
        status = card_mgr_card_trylock(curr_card, &card_owner);
        if( 0 != status )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "Unable to obtain lock for card %u, already locked by "
                      "another process (PID=%u)\n",
                      curr_card,
                      (unsigned int)card_owner);

            /* Map error codes for consistency */
            if (EBUSY == status)
            {
                status = -EBUSY;
            }
        }
        else
        {
            cardInitState[curr_card] = CardLocked;

            num_cards_locked++;
            LOCK_REGS(curr_card);

            // Note: The initialization order is important!

            if( 0 == status )
            {
                status = _skiq_init_card(curr_card, xport_type, level);
            }

            if( 0 == status )
            {
                cardInitState[curr_card] = CardInitialized;
                status = _skiq_init_fw(curr_card);
            }

            if( 0 == status )
            {
                cardInitState[curr_card] = FwInitialized;
                status = _skiq_init_fpga(curr_card, level);
            }

            if ( 0 == status )
            {
                cardInitState[curr_card] = FpgaInitialized;
                status = flash_init_card(curr_card);
            }

            if( 0 == status )
            {
                cardInitState[curr_card] = FlashInitialized;
                status = _skiq_init_rf_basic(curr_card);
            }

            if( 0 == status)
            {
                /*
                    Now that basic initialization succeeded, let's be extra careful and
                    make sure our EEPROM write protection is enabled
                */
                card_eeprom_unlock_and_enable_wp(curr_card);
                card_fmc_eeprom_unlock_and_enable_wp(curr_card);

                cardInitState[curr_card] = RfBasicInitialized;
                if( level == skiq_xport_init_level_full )
                {
                    status = _skiq_init_rf_full(curr_card);
                    if( 0 == status )
                    {
                        cardInitState[curr_card] = RfFullInitialized;
                    }
                }
            }

            UNLOCK_REGS(curr_card);
        }
    }

    /* check that requested cards were initialized */
    if ( 0 != status)
    {
        skiq_info( "Unable to initialize %s; undoing partial initialization\n",
               (1 < num_cards) ? "all of the requested cards" : "the requested card" );

        /*
            Uninitialize only the number of cards that were locked - as this is the first
            initialization step, cards that were never locked can assume to be uninitialized.
        */
        for( i = 0; i < num_cards_locked; i++ )
        {
            curr_card = cards[i];

            /*
                Undo the card initialization, which involves uninitializing components in the
                reverse order in which they were initialized. This means that the order these steps
                are performed is important (e.g. the position of the case statements is meaningful)
                and all of the fall-throughs in the case statements are intentional.
            */
            switch( cardInitState[curr_card] )
            {
            case RfFullInitialized:
                (void) _skiq_exit_rf_full( curr_card );
            case RfBasicInitialized:
                (void) _skiq_exit_rf_basic( curr_card );
            case FlashInitialized:
                (void) flash_exit_card( curr_card );
            case FpgaInitialized:
                (void) _skiq_exit_fpga( curr_card );
            case FwInitialized:
                (void) _skiq_exit_fw( curr_card );
            case CardInitialized:
                (void) _skiq_exit_card( curr_card );
            case CardLocked:
                card_mgr_card_unlock( curr_card );
            case Finished:
                cardInitState[curr_card] = NotInitialized;
            case NotInitialized:
                debug_print("card %" PRIu8 ": finished uninitialization\n", curr_card);
                break;
            default:
                skiq_warning( "card %" PRIu8 ": unknown uninitialization state (%d); not"
                        " uninitializing card\n", curr_card, (int) (cardInitState[curr_card]));
                break;
            }
        }
    }
    else
    {
        LOCK(use_exit_handler_mutex);
        useExitHandler = skiq.use_exit_handler;
        UNLOCK(use_exit_handler_mutex);

        if ((useExitHandler) && (!exit_handler_initialized))
        {
            int tmpResult = 0;
            tmpResult = atexit(skiq_shutdown_handler);
            if (0 != tmpResult)
            {
                skiq_warning("Failed to install exit handler (%d); possible resource leak on"
                    " shutdown\n", tmpResult);
            }
            else
            {
                debug_print("libsidekiq exit handler installed!\n");
                exit_handler_initialized = true;
            }
        }
        else
        {
            debug_print("Skipping installing exit handler...\n");
        }
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_enable_cards_by_serial_str() function is responsible for performing all initialization
    tasks for the specified Sidekiq cards.

    @attention  The Sidekiq library must have been previously initialized with skiq_init() or
                skiq_init_by_serial_str(). The transport type is automatically selected based
                on availability.

    @since Function added in API @b v4.9.0

    @ingroup cardfunctions
    @see skiq_init
    @see skiq_init_by_serial_str
    @see skiq_exit
    @see skiq_enable_cards
    @see skiq_disable_cards

    @param[in]  pp_serial_nums  pointer to the list of Sidekiq serial number strings to initialize
    @param[in]  num_cards       number of Sidekiq cards to initialize
    @param[in]  level           the transport functionality level of initialization that is
                                required:
      - ::skiq_xport_init_level_basic - minimal initialization necessary to bring
          up the requested transport interface for FPGA / RFIC register
          reads/writes, and initialize the mutexes that serializes access to
          libsidekiq
      - ::skiq_xport_init_level_full - Same as ::skiq_xport_init_level_basic and
          perform the complete bring up of all hardware (most applications
          concerned with sending/receiving RF will use this)
    @param[out] p_card_nums     pointer to the list of Sidekiq card indices corresponding with
                                serial strings provided; this list should be able to hold at
                                least ::SKIQ_MAX_NUM_CARDS entries

    @return int32_t  status where 0=success, anything else is an error
    @retval -EPERM  if libsidekiq has not been initialized yet (through skiq_init()
                    or skiq_init_by_serial_str())
    @retval -E2BIG  if the number of cards specified exceeds the maximum (::SKIQ_MAX_NUM_CARDS)
    @retval -ENXIO  if one of the specified serial numbers cannot be obtained
*/
EPIQ_API int32_t skiq_enable_cards_by_serial_str(const char **pp_serial_nums,
                                                 uint8_t num_cards,
                                                 skiq_xport_init_level_t level,
                                                 uint8_t *p_card_nums)
{
    int32_t status = 0;
    ARRAY_WITH_DEFAULTS(uint8_t, cards, SKIQ_MAX_NUM_CARDS, INVALID_CARD_INDEX);
    uint8_t i = 0;

    if (!skiq.initialized)
    {
        skiq_warning(NOT_INITIALIZED_MESSAGE);
        return -EPERM;
    }

    if (SKIQ_MAX_NUM_CARDS < num_cards)
    {
        skiq_warning("Number of cards to enable exceeds maximum (%" PRIu32 ")\n",
                SKIQ_MAX_NUM_CARDS);
        return -E2BIG;
    }

    for (i = 0; ((i < num_cards) && (0 == status)); i++)
    {
        status = skiq_get_card_from_serial_string((char *) pp_serial_nums[i], &(cards[i]));
        if (0 != status)
        {
            skiq_error("Unable to obtain card index for serial %s", pp_serial_nums[i]);
            status = -ENXIO;
        }
        else
        {
            p_card_nums[i] = cards[i];
        }
    }

    if (0 == status)
    {
        status = skiq_enable_cards(cards, num_cards, level);
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_parameters() function is used for populating the skiq_param_t
    struct for a given card. This structure can be queried for various values
    relating to the card. For further information regarding that structure,
    reference the documentation provided in sidekiq_params.h.

    Note: The initialization level influences what can be populated in the
    structure. This is fully documented in skiq_params.h.

    @since Function added in API @b v4.3.0

    @param card card index of the Sidekiq of interest
    @param p_param pointer to structure to be populated.

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_parameters( uint8_t card, skiq_param_t* p_param )
{
    uint32_t n = 0;
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_param );

    // reset struct to default
    *p_param = (skiq_param_t) SKIQ_PARAM_INITIALIZER;

    p_param->card_param = skiq.card[card].card_params;
    p_param->fw_param = skiq.card[card].fw_params;
    p_param->fpga_param = skiq.card[card].fpga_params;
    p_param->rf_param = skiq.card[card].rf_params;

    for ( n = 0; ( n < p_param->rf_param.num_rx_channels ) && ( status == 0 ); n++ )
    {
        skiq_rx_hdl_t hdl = _skiq_get_rx_hdl( card, n );

        if ( hdl < skiq_rx_hdl_end )
        {
            /* Copying [hdl] -> [n] is intentional to keep some level of backward compatibility
             * prior to @b v4.17.0 since all Sidekiq product before Sidekiq NV100 had contiguous
             * receive handle lists.  Keeping this mapping provides _most_ of what users could want
             * with existing code.  It would only be missing what handle is associated with the
             * 'nth' entry in rx_param[] */
            p_param->rx_param[n] = skiq.card[card].rx_list[hdl].params;
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, p_param->rf_param.num_rx_channels, n);
            status = -EPROTO;
        }
    }

    for ( n = 0; ( n < p_param->rf_param.num_tx_channels ) && ( status == 0 ); n++ )
    {
        skiq_tx_hdl_t hdl = _skiq_get_tx_hdl( card, n );

        if ( hdl < skiq_tx_hdl_end )
        {
        /* Copying [hdl] -> [n] is intentional to keep some level of backward compatibility prior to
         * @b v4.17.0 since all Sidekiq product before Sidekiq NV100 had contiguous receive handle
         * lists.  Keeping this mapping provides _most_ of what users could want with existing code.
         * It would only be missing what handle is associated with the 'nth' entry in tx_param[] */
        p_param->tx_param[n] = skiq.card[card].tx_list[hdl].params;
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds transmit handle for card %" PRIu8
                       " (number of TX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, p_param->rf_param.num_tx_channels, n);
            status = -EPROTO;
        }
    }

    return status;
}

/*****************************************************************************/
/** The skiq_is_xport_avail() function is responsible for determining if the
    requested transport type is available for the card index specified.

    @param card card index of the Sidekiq of interest
    @param type transport type to check for card specified

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_is_xport_avail( uint8_t card, skiq_xport_type_t type )
{
    skiq_xport_type_t xport = skiq_xport_type_unknown;
    bool is_avail = false;
    
    CHECK_CARD_RANGE(card);

    if( false == skiq.initialized )
    {
        // TODO: clean this up when probe updated
        card_mgr_init();
    }

    if( skiq_xport_type_auto != type )
    {
        is_avail = card_mgr_is_xport_avail(card, type);
    }
    else
    {
        for( xport = skiq_xport_type_pcie; 
             (xport < skiq_xport_type_max) && (false == is_avail); 
             xport++ )
        {
            is_avail = card_mgr_is_xport_avail(card, xport);
        }
    }
    
    if( false == skiq.initialized )
    {
        // TODO: clean this up when probe updated
        card_mgr_exit();
    }

    return ((is_avail) ? 0 : -1);
}

/*****************************************************************************/
/** The skiq_is_card_avail() function is responsible for determining if the
    requested card is currently available and free for use.  If the card is
    already locked, the process ID of the current card owner is provided.

    @note This only reflects the instantaneous availability of the Sidekiq card
    and does not reserve any resources for future use.

    @note If a card is locked by another thread within the current process,
    the process ID (PID) returned in p_card_owner can be the PID of the current process.

    @since Function added in API @b v4.0.0

    @ingroup cardfunctions

    @param[in]  card            card index of the Sidekiq of interest
    @param[out] p_card_owner    is a pointer where the process ID of the current
    card owner is provided (only if the card is already locked). May be NULL
    if the caller does not require the information; if not NULL, this value is
    set if the function returns 0 or EBUSY.

    @return int32_t status code
    @retval -ERANGE if the specified card index exceeds the maximum (SKIQ_MAX_NUM_CARDS)
    @retval -ENODEV if a card was not detected at the specified card index
    @retval 0       if the card is available
    @retval EBUSY   if the specified card is not available (already in use)
    @retval other values on failure
*/

EPIQ_API int32_t skiq_is_card_avail( uint8_t card,
				     pid_t *p_card_owner )
{
    int32_t status = -1;
    pid_t p_temp_card_owner = 0;

    CHECK_CARD_RANGE(card);

    if( false == skiq.initialized )
    {
        // TODO: clean this up when probe updated
        card_mgr_init();
    }

    if ( ( status = card_mgr_card_trylock( card, &p_temp_card_owner ) ) == 0 )
    {
        uint32_t serial_num;
        uint16_t hardware_vers;
        skiq_part_info_t part_info;

        /* if the card exists, then card_mgr_get_card_info() will return 0, otherwise it returns a
         * -1.  The status from getting the card info is sufficient to tell if the card exists AND
         * is available */
        status = card_mgr_get_card_info( card, &serial_num, &hardware_vers, &part_info );
        if (-1 == status)
        {
            status = -ENODEV;
        }

        card_mgr_card_unlock( card );
    }
    if ( (status == 0) || (status == EBUSY) )
    {
        if (p_card_owner) {
            *p_card_owner = p_temp_card_owner;
        }
    }

    if( false == skiq.initialized )
    {
        // TODO: clean this up when probe updated
        card_mgr_exit();
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_exit() function is responsible for performing all shutdown tasks
    for libsidekiq.  It should be called once when the associated application
    is closing.

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_exit(void)
{
    int32_t status = 0;
    uint8_t i = 0;
    uint8_t card = 0;

    TRACE();

    if( skiq.initialized == false )
    {
        skiq_warning("Attempting to exit libsidekiq but it is not currently initialized");
        return 0;
    }

    /* Shutdown the cards that were initialized */
    for( i = 0; i < SKIQ_MAX_NUM_CARDS; i++ )
    {
        card = skiq.card[i].card_params.card;
        if( skiq.card[i].card_active == 1 )
        {
            status =_skiq_disable_card(card);
            if (0 != status)
            {
                skiq_warning("Failed to shut down card %" PRIu8 " (status code %" PRIi32 ");"
                    " continuing\n", card, status);
            }
        }
        rfic_free_functions( skiq.card[i].p_rfic_funcs );
        skiq.card[i].p_rfic_funcs = NULL;

    }

    /* Free the cache for the IO expander */
    (void)io_exp_free_cache_mem();
    
#ifndef __MINGW32__
    /* Close the syslog */
    closelog();
#endif /* __MINGW32__ */

    card_mgr_exit();

    skiq.num_cards_avail = 0;
    skiq.initialized = false;

    return( status );
}

/*****************************************************************************/
/** The skiq_disable_cards() function is responsible for performing all shutdown tasks
    for the specified Sidekiq card(s).  This does not perform the various shutdown
    tasks for all of libsidekiq, only for the card(s) specified.

    @since Function added in API @b v4.8.0

    @attention  The Sidekiq library must have been previously initialized with skiq_init()
                and the cards must have been initialized with either skiq_init(),
                skiq_init_by_serial_str(), skiq_enable_cards(), or
                skiq_enable_cards_by_serial_str(). If libsidekiq is no longer in use, skiq_exit()
                must be called to release all libsidekiq resources.

    @ingroup cardfunctions
    @see skiq_init
    @see skiq_init_by_serial_str
    @see skiq_enable_cards
    @see skiq_enable_cards_by_serial_str

    @param[in] cards array of Sidekiq cards to be disabled
    @param[in] num_cards number of Sidekiq cards to disabled

    @return int32_t status where 0=success, anything else is an error
    @retval -EPERM  if libsidekiq has not been initialized yet (through skiq_init()
                    or skiq_init_by_serial_str())
    @retval -E2BIG  if the number of cards requested exceeds the maximum (::SKIQ_MAX_NUM_CARDS)
    @retval -EINVAL if one of the specified card indices is out of range or refers to a
                    non-existent card.
*/
EPIQ_API int32_t skiq_disable_cards(const uint8_t cards[],
                                    uint8_t num_cards)
{
    int32_t status = 0;
    uint8_t i = 0;
    uint8_t curr_card = 0;

    if( skiq.initialized == false )
    {
        skiq_warning(NOT_INITIALIZED_MESSAGE);
        return -EPERM;
    }

    if (SKIQ_MAX_NUM_CARDS < num_cards)
    {
        skiq_warning("Number of cards to disable exceeds maximum (%" PRIu32 ")\n",
                SKIQ_MAX_NUM_CARDS);
        return -E2BIG;
    }

    for( i = 0; i < num_cards; i++ )
    {
        curr_card = cards[i];

        if( (curr_card >= SKIQ_MAX_NUM_CARDS) ||
            (skiq.card[curr_card].card_params.card == INVALID_CARD_INDEX) )
        {
            skiq_error("No valid Sidekiq detected at card index %u\n", curr_card);
            status = -EINVAL;
        }
    }

    for (i = 0; (i < num_cards) && (0 == status); i++ )
    {
        skiq_info("Disabling card %u", cards[i]);
        status = _skiq_disable_card(cards[i]);
        if (-ENODEV == status)
        {
            skiq_info("Card %" PRIu8 " is already disabled; continuing.\n", cards[i]);
            status = 0;
        }
        else if (0 != status)
        {
            skiq_error("Failed to disable card %" PRIu8 " (return code %" PRIi32 ")\n",
                    cards[i], status);
        }
    }

    return (status);
}

/**
    @brief  Helper function to disable a card

    @param[in]  card    card index of the Sidekiq of interest

    @return int32_t     status where 0=success, anything else is an error.
    @retval -ERANGE     if the card index is out of range
    @retval -ENODEV     if the specified card is not active
*/
static int32_t _skiq_disable_card( uint8_t card )
{
    int32_t status = 0, error_status = 0;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    uint32_t data=0;
    uint8_t j=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);

    /* free resources associated with calibration tables */
    status = card_calibration_clear( card );
    if ( status != 0 )
    {
        if ( error_status == 0 ) error_status = status;
        skiq_warning("Calibration not loaded or failed to clear calibration resources for "
                     "card %u with status %d during card disable; continuing.\n", card, status);
        status = 0;
    }

    /* reset stream_mode back to default */
    status = skiq_write_rx_stream_mode( card, skiq_rx_stream_mode_high_tput );
    if ( status != 0 )
    {
        skiq_warning("Failed to write RX stream mode to high throughput for card %u with "
                   "status %d during card disable; continuing.\n", card, status);
    }

    /* disable the flash interface for this card */
    status = flash_exit_card( card );
    if ( status != 0 )
    {
        skiq_warning("Failed to disable Flash interface for card %" PRIu8 " with status %"
            PRIi32 " during card disable; continuing.\n", card, status);
    }

    if( skiq_xport_init_level_full == skiq_xport_card_get_level(card) )
    {
        status = _skiq_stop_streaming(card);
        if (0 != status)
        {
            if ( error_status == 0 ) error_status = status;
            skiq_warning("Failed to stop streaming on all handles on card %" PRIu8
                " (status code %" PRIi32 "); continuing\n", card, status);
        }

        status = skiq_xport_rx_set_block_size( card, RX_BLOCK_SIZE_STREAM_HIGH_TPUT );
        if ( status != 0 )
        {
            if ( error_status == 0 ) error_status = status;
            skiq_warning("Failed to set transport RX block size for card %u with status "
                    "%d during card disable; continuing.\n", card, status);
        }

        /* make sure everything is in a clean state */
        /* let's clear the "wait for PPS bit and flush the Tx FIFO */
        sidekiq_fpga_reg_read(card, FPGA_REG_DMA_CTRL_RX1, &data);
        BF_SET(data,0,WAIT_FOR_TRIGGER_OFFSET,WAIT_FOR_TRIGGER_LEN);
        BF_SET(data,1,DMA_TX_FIFO_CTRL_OFFSET,DMA_TX_FIFO_CTRL_LEN);
        sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_DMA_CTRL_RX1, data);

        /* make sure the PAs are disabled for all the hdls */
        for ( j = 0; j < _skiq_get_nr_tx_hdl( card ); j++ )
        {
            skiq_tx_hdl_t tx_hdl = _skiq_get_tx_hdl( card, j );

            if ( tx_hdl < skiq_tx_hdl_end )
            {
                rfe_instance = _skiq_get_tx_rfe_instance(card, tx_hdl);
                rfe_update_tx_pa( &(rfe_instance), false );
            }
            else
            {
                skiq_error("Internal error (L%u); out-of-bounds transmit handle for card %" PRIu8
                           " (number of TX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                           card, _skiq_get_nr_tx_hdl( card ), j);
            }
        }

        /* make sure the LNAs are disabled for all the hdls */
        for ( j = 0; j < _skiq_get_nr_rx_hdl( card ); j++ )
        {
            skiq_rx_hdl_t rx_hdl = _skiq_get_rx_hdl( card, j );

            if ( rx_hdl < skiq_rx_hdl_end )
            {
                rfe_instance = _skiq_get_rx_rfe_instance(card, rx_hdl);
                rfe_update_rx_lna( &(rfe_instance), lna_disabled );
            }
            else
            {
                skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                           " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                           card, _skiq_get_nr_rx_hdl( card ), j);
            }
        }

        /* Release the RFIC instance */
        rfic_instance = _skiq_get_generic_rfic_instance(card);
        rfic_release( &(rfic_instance) );
    }

    status = fpga_ctrl_reset_config( card );
    if ( status != 0 )
    {
        if ( error_status == 0 ) error_status = status;
        skiq_warning("Failed to reset FPGA features for card %u with status %d during card "
                     "disable; continuing.\n", card, status);
    }

    /* unlock any mutex that may have been previously locked */
    skiq_info( "Unlocking card %u\n", card );
    card_mgr_card_unlock( card );

    tune_card_exit( skiq.card[card].card_params.card );

    /*
        Shutdown the card; ignore the return value for now as there's not much to be done on
        failure.
    */
    (void) _skiq_exit_card( card );

    UNLOCK_REGS(card);

    return (error_status);
}

/* applies the stored receive stream mode setting for the specified card

    @return int32_t
    @retval 0 successful setting of RX stream mode
    @retval -ENOTSUP specified RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL specified RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -ENOSYS Transport does not support FPGA register access
    @retval -1 other error occurred
*/
static int32_t apply_rx_stream_mode( uint8_t card )
{
    skiq_rx_stream_mode_t stream_mode = skiq_rx_stream_mode_high_tput;
    int32_t status = 0;

    /* In order to leave the FPGA version checking in one location, namely
     * skiq_write_rx_stream_mode(), first read the stored mode, then try to
     * write it again.  This way the FPGA version will be checked again.  If
     * writing the mode succeeds, set the transport's block size according to
     * receive stream mode selection.  If writing the mode fails, it's likely
     * because the FPGA does not support the low latency setting and it should
     * default to high throughput */
    skiq_read_rx_stream_mode( card, &stream_mode );
    if ( stream_mode == skiq_rx_stream_mode_low_latency )
    {
        /* check to see that the mode is still valid (given the FPGA bitstream
         * version) by writing the receive stream mode */
        if ( (status=skiq_write_rx_stream_mode( card, stream_mode )) != 0 )
        {
            /* revert to skiq_rx_stream_mode_high_tput if writing mode fails */
            stream_mode = skiq_rx_stream_mode_high_tput;
            status = skiq_write_rx_stream_mode( card, stream_mode );
        }
    }

    if( status == 0 )
    {
        /* perform configuration based on resulting stream_mode */
        if ( stream_mode == skiq_rx_stream_mode_high_tput )
        {
            status = skiq_xport_rx_set_block_size( card, RX_BLOCK_SIZE_STREAM_HIGH_TPUT );
            if ( status == 0 )
            {
                status = fpga_ctrl_disable_rx_packet_64_words( card );
                if( status == 0 )
                {
                    status = skiq_xport_rx_set_buffered( card, true );
                }
            }
        }
        else if( stream_mode == skiq_rx_stream_mode_balanced )
        {
            status = skiq_xport_rx_set_block_size( card, RX_BLOCK_SIZE_STREAM_BALANCED );
            if ( status == 0 )
            {
                status = fpga_ctrl_disable_rx_packet_64_words( card );
                if( status == 0 )
                {
                    status = skiq_xport_rx_set_buffered( card, false );
                }
            }
        }
        else if ( stream_mode == skiq_rx_stream_mode_low_latency )
        {
            status = skiq_xport_rx_set_block_size( card, RX_BLOCK_SIZE_STREAM_LOW_LATENCY );
            if ( status == 0 )
            {
                status = fpga_ctrl_enable_rx_packet_64_words( card );
            }
        }
    }

    return (status);

} /* apply_rx_stream_mode */


/* applies the stored receive iq order mode setting for the specified card

    @return int32_t
    @retval 0 successful setting of iq order mode
    @retval -ENOTSUP configuration of IQ ordering is not supported for the loaded FPGA bitstream
    @retval -EINVAL specified iq order mode is not a valid mode, see ::skiq_iq_order_t for valid modes
    @retval -ENOSYS Transport does not support FPGA register access
    @retval -1 other error occurred
*/
static int32_t apply_iq_order_mode( uint8_t card )
{
    skiq_iq_order_t iq_order = skiq_iq_order_qi;
    int32_t status = 0;

    /* In order to leave the FPGA version checking in one location, first read 
     * the stored mode, then try to write it again. This way the FPGA version 
     * will be checked again. If writing the mode fails, it's likely because the 
     * FPGA does not support the iq-order setting and it should default to 
     * qi_ordering_mode */

    iq_order = skiq.card[card].iq_order_mode;

    if ( status == 0  )
    {
        /* check to see that the mode is still valid (given the FPGA bitstream
         * version) by writing the iq order mode */
        if((status=skiq_write_iq_order_mode( card, iq_order )) != 0)
        {
        /* revert to skiq_iq_order_qi if writing mode fails */
            status=skiq_write_iq_order_mode( card, skiq_iq_order_qi);
        }
    }

    if( status == 0 )
    {
        /* perform configuration based on resulting iq_order_mode */
        LOCK_REGS(card);
        status = fpga_ctrl_write_iq_order_mode(card, iq_order);
        UNLOCK_REGS(card);
    }

    return (status);

} /* apply_iq_order_mode */


/*****************************************************************************/
/** The _start_rx_streaming_multi() helper function allows a user to start
    multiple receive streams after the specified trigger occurs.

    @warning If one of the receive handles is already streaming then this
    function returns an error.

    @attention If ::skiq_trigger_src_1pps is used as a trigger then this
    function will @b block until the 1PPS edge occurs.

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[], must be greater than 0
    @param[in] trigger [::skiq_trigger_src_t] type of trigger to use
    @param[in] sys_timestamp System Timestamp after the next positive trigger will begin the data flow

    @return int32_t
    @retval 0 successful start streaming for handle specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed (nr_handles < 1, etc)
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : Configured RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : Configured RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -EIO Failed to start streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval -EPROTO System Timestamp is not changing or moving backwards (when using ::skiq_trigger_src_1pps trigger)
    @retval non-zero : An unspecified error occurred
 */
static int32_t _start_rx_streaming_multi( uint8_t card,
                                          skiq_rx_hdl_t handles[],
                                          uint8_t nr_handles,
                                          skiq_trigger_src_t trigger,
                                          uint64_t sys_timestamp )
{
    int32_t status = 0;
    uint32_t data = 0;
    ARRAY_WITH_DEFAULTS(bool,handle_requested,skiq_rx_hdl_end,false);
    skiq_rx_hdl_t hdl_idx;
    uint8_t i;
    uint8_t hdl_conflict_idx;
    skiq_rx_hdl_t hdl_conflicts[skiq_rx_hdl_end];
    uint8_t num_conflicts=0;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    int32_t chan_enable_status=0;

    TRACE();

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE( card );

    if ( nr_handles < 1 )
    {
        return -EINVAL;
    }

    /* check that all requested handles are valid */
    for (i = 0; i < nr_handles; i++)
    {
        CHECK_VALID_RX_HDL( card, handles[i] );
        handle_requested[handles[i]] = true;
    }

    /* lock access to FPGA registers as well as receive data stream */
    LOCK_REGS(card);
    LOCK_RX(card);

    /* check if any handles are already streaming, if not, apply the receive stream mode */
    if ( !is_rx_streaming(card) )
    {
        status = apply_rx_stream_mode( card );
        if ( status != 0 )
        {
            skiq_error("Failed to apply RX stream mode for card %u when starting RX streaming with "
                       "status %d\n", card, status);
            UNLOCK_RX(card);
            UNLOCK_REGS(card);
            return (status);
        }
        status = apply_iq_order_mode( card );
        if ( status != 0 )
        {
            skiq_error("Failed to apply iq order mode for card %u when starting RX streaming with "
                       "status %d\n", card, status);
            UNLOCK_RX(card);
            UNLOCK_REGS(card);
            return (status);
        }
    }

    /* check to see if any of the requested handles are already streaming */
    for (hdl_idx = skiq_rx_hdl_A1; (hdl_idx < skiq_rx_hdl_end) && (status==0); hdl_idx++)
    {
        if ( handle_requested[hdl_idx] == true )
        {
            // check the conflict list
            status = skiq_read_rx_stream_handle_conflict(card, hdl_idx, hdl_conflicts,
                                                         &num_conflicts);
            for( hdl_conflict_idx=0; (hdl_conflict_idx<num_conflicts) && (status==0); hdl_conflict_idx++ )
            {
                // see if a conflicting handle is already streaming
                if( is_rx_hdl_streaming(card, hdl_conflicts[hdl_conflict_idx]) == true )
                {
                    skiq_error( "Handle %s conflicts with handle %s and cannot be streaming "
                                "simultaneously (card=%u)\n",
                                rx_hdl_cstr(hdl_conflicts[hdl_conflict_idx]), rx_hdl_cstr(hdl_idx),
                                card );
                    status = -EBUSY;
                }
                // loop through all requested handles to see if it generates a conflict
                for( i=0; i<skiq_rx_hdl_end; i++ )
                {
                    if( (hdl_conflicts[hdl_conflict_idx] == i) &&
                        (handle_requested[i] == true) )
                    {
                        skiq_error( "Handle %s conflicts with handle %s and cannot be streaming "
                                    "simultaneously (card=%u)\n", rx_hdl_cstr(hdl_idx),
                                    rx_hdl_cstr(hdl_conflicts[hdl_conflict_idx]),
                                    card );
                        status = -EBUSY;
                    }
                }
            }
        }
    }
    // make sure we haven't had any errors thus far
    if( status != 0 )
    {
        UNLOCK_RX(card);
        UNLOCK_REGS(card);
        return (status);
    }

    /* temporarily pause all the streaming data so we can drain the DMA buffers of all stale
       data before re-enabling it */
    pause_all_rx_streams(card);

    /* re-enable the streams */
    resume_and_flush_all_rx_streams(card);

    /* now enable transport streaming for all requested handles */
    for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
    {
        if ( handle_requested[hdl_idx] )
        {
            status = skiq_xport_rx_start_streaming( card, hdl_idx );
            if( status != 0 )
            {
                skiq_rx_hdl_t stop_hdl_idx;

                skiq_error("Transport layer failed to start streaming for card %u and handle %s with status %d\n",
                           card, rx_hdl_cstr(hdl_idx), status);

                /* disable streaming on those handles already started */
                for (stop_hdl_idx = skiq_rx_hdl_A1; stop_hdl_idx < hdl_idx; stop_hdl_idx++)
                {
                    if ( handle_requested[stop_hdl_idx] )
                    {
                        skiq_xport_rx_stop_streaming( card, stop_hdl_idx);
                    }
                }

                /* squash error from transport function into a generic I/O error */
                status = -EIO;
                UNLOCK_RX(card);
                UNLOCK_REGS(card);
                return (status);
            }
        }
    }

    // set the Rx mode...note that we're not going to check the port config here since
    // that's done for us in the underlying function
    skiq_write_rf_port_operation(card, false);

    /* set the channel mode based on our config before doing anything else */
    sidekiq_fpga_reg_read( card, FPGA_REG_TIMESTAMP_RST, &data );
    if( skiq.card[card].chan_mode == skiq_chan_mode_single )
    {
        BF_SET( data, 0, CHAN_MODE_OFFSET, CHAN_MODE_LEN );
    }
    else
    {
        BF_SET( data, 1, CHAN_MODE_OFFSET, CHAN_MODE_LEN );
    }
    sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_TIMESTAMP_RST, data );

    fpga_ctrl_enable_stream( card );

    // initialize the RX stream parameters and start all of the streams
    {
        fpga_ctrl_rx_params_t rx_params[skiq_rx_hdl_end];
        uint8_t nr_rx_params = 0;

        for (hdl_idx = skiq_rx_hdl_A1; (hdl_idx < skiq_rx_hdl_end) && (status==0); hdl_idx++)
        {
            if ( handle_requested[hdl_idx] )
            {
                rx_params[nr_rx_params].ctrl_reg = FPGA_REG_DMA_CTRL_(hdl_idx);
                rx_params[nr_rx_params].hdl = hdl_idx;
                rx_params[nr_rx_params].packed = skiq.card[card].iq_packed;
                rx_params[nr_rx_params].complex_mult = skiq.card[card].iq_complex_mult_enabled;
                rx_params[nr_rx_params].data_src = skiq.card[card].rx_list[hdl_idx].data_src;
                rx_params[nr_rx_params].dc_offset = skiq.card[card].rx_list[hdl_idx].dc_offset_ena;
                nr_rx_params++;
                // enable the channel at the RFIC and RFE layers
                rfe_instance = _skiq_get_rx_rfe_instance(card, hdl_idx);
                rfic_instance = _skiq_get_rx_rfic_instance(card, hdl_idx);
                chan_enable_status = rfe_enable_rx_chan( &rfe_instance, true );

                // not all RFEs support disabling channel so don't consider it an error
                if( ((chan_enable_status==0) || (chan_enable_status==-ENOTSUP)) )
                {
                    if( (rfic_is_chan_enable_xport_dependent( &rfic_instance ) == false) )
                    {
                        status = rfic_enable_rx_chan( &rfic_instance, true );
                        if (status == -EPERM)
                        {
                            //TODO: Update this error mesage
                            skiq_error("Failed to enable %s on card %" PRIu8 "." \
                                "Check that handle is configured prior to streaming\n", rx_hdl_cstr( hdl_idx ), card);
                        }
                    }
                }
                else
                {
                    status = chan_enable_status;
                }
            }
        }

        // see if we need to unwind the channel enable...if we're not going to be streaming
        if( status != 0 )
        {
            for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
            {
                if ( handle_requested[hdl_idx] )
                {
                    rfe_instance = _skiq_get_rx_rfe_instance(card, hdl_idx);
                    rfic_instance = _skiq_get_rx_rfic_instance(card, hdl_idx);
                    chan_enable_status = rfe_enable_rx_chan( &rfe_instance, false );
                    // not all RFE support disabling the channel, not an error
                    if( (chan_enable_status != 0) && (chan_enable_status != -ENOTSUP) )
                    {
                        skiq_warning("Unable to disable RFE for handle %s (card=%u)",
                                     rx_hdl_cstr(hdl_idx), card);
                    }
                    if( rfic_is_chan_enable_xport_dependent( &rfic_instance) == false )
                    {
                        if( rfic_enable_rx_chan( &rfic_instance, false ) != 0 )
                        {
                            skiq_warning("Unable to disable RFIC for handle %s (card=%u)",
                                         rx_hdl_cstr(hdl_idx), card);
                        }
                    }
                }
            }
        }

        if( status == 0 )
        {
            status = fpga_ctrl_start_rx_stream_multi( card,
                                                      rx_params, nr_rx_params,
                                                      trigger,
                                                      sys_timestamp );
            if ( status == 0 )
            {
                /* set the streaming flag */
                for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
                {
                    if ( handle_requested[hdl_idx] )
                    {
                        skiq.card[card].rx_list[hdl_idx].streaming = true;
                    }
                }

                /* inform transport layer about RX configuration change */
                rx_configure( card );
            }
        }
    }

    /* permit access to FPGA registers as well as receive data stream */
    UNLOCK_RX(card);
    UNLOCK_REGS(card);

    return( status );
}


/*****************************************************************************/
/** The skiq_start_rx_streaming_multi_immediate() function allows a user to
    start multiple receive streams immediately (not necessarily
    timestamp-synchronized depending on FPGA support and library support).

    @warning If one of the receive handles is already streaming then this
    function returns an error.

    @since Function added in @b v4.5.0

    @ingroup rxfunctions

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]

    @return int32_t
    @retval 0 successful start streaming for handle specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed (nr_handles < 1, etc)
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : Configured RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : Configured RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -EIO Failed to start streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval non-zero : An unspecified error occurred
 */
EPIQ_API int32_t skiq_start_rx_streaming_multi_immediate( uint8_t card,
                                                          skiq_rx_hdl_t handles[],
                                                          uint8_t nr_handles )
{
    return (_start_rx_streaming_multi(card, handles, nr_handles, skiq_trigger_src_immediate, 0));
}


/*************************************************************************************************/
/** The skiq_start_rx_streaming_multi_synced() function allows a user to start multiple receive
    streams immediately and with timestamp synchronization (not necessarily phase coherent however).

    @warning If one of the receive handles is already streaming then this function returns an error.

    @attention Not all Sidekiq products support the ::skiq_trigger_src_synced trigger source.

    @since Function added in @b v4.9.0, requires FPGA bitstream @b v3.11.0 or greater

    @ingroup rxfunctions

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]

    @return int32_t
    @retval 0 successful start streaming for handle specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed (nr_handles < 1, etc)
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : Configured RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : Configured RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -EIO Failed to start streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval -ENOTSUP the ::skiq_trigger_src_synced trigger source is not supported for the given Sidekiq product or FPGA bitstream
    @retval non-zero : An unspecified error occurred
 */
EPIQ_API int32_t skiq_start_rx_streaming_multi_synced( uint8_t card,
                                                       skiq_rx_hdl_t handles[],
                                                       uint8_t nr_handles )
{
    return (_start_rx_streaming_multi(card, handles, nr_handles, skiq_trigger_src_synced, 0));
}


/*****************************************************************************/
/** The skiq_start_rx_streaming_multi_on_trigger() function allows a user to
    start multiple receive streams after the specified trigger occurs.

    @warning If one of the receive handles is already streaming then this
    function returns an error.

    @attention If ::skiq_trigger_src_1pps is used as a trigger then this
    function will @b block until the 1PPS edge occurs.

    @since Function added in @b v4.5.0

    @ingroup rxfunctions

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]
    @param[in] trigger [::skiq_trigger_src_t] type of trigger to use
    @param[in] sys_timestamp System Timestamp after the next positive trigger will begin the data flow

    @return int32_t
    @retval 0 successful start streaming for handle specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed (nr_handles < 1, etc)
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : Configured RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : Configured RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -EIO Failed to start streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval -EPROTO System Timestamp is not changing or moving backwards (when using ::skiq_trigger_src_1pps trigger)
    @retval non-zero : An unspecified error occurred
 */
EPIQ_API int32_t skiq_start_rx_streaming_multi_on_trigger( uint8_t card,
                                                           skiq_rx_hdl_t handles[],
                                                           uint8_t nr_handles,
                                                           skiq_trigger_src_t trigger,
                                                           uint64_t sys_timestamp)
{
    return (_start_rx_streaming_multi(card, handles, nr_handles, trigger, sys_timestamp));
}

/*****************************************************************************/
/** The skiq_read_rx_streaming_handles() function is responsible for providing
    a list of RX handles currently streaming.

    @ingroup rxfunctions

    @since Function added in @b v4.9.0

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_hdls_streaming [::skiq_rx_hdl_t] array of handles currently streaming
    @param[out] p_num_hdls pointer of where to store number of handles in streaming list

    @return int32_t
    @retval 0 p_hdls_streaming populated with RX handles currently streaming
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval non-zero : other error occurred
*/
EPIQ_API int32_t skiq_read_rx_streaming_handles(uint8_t card,
                                                skiq_rx_hdl_t *p_hdls_streaming,
                                                uint8_t *p_num_hdls )
{
    int32_t status=0;
    uint32_t hdl=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_num_hdls = 0;

    // loop through all the handles to see which ones are currently streaming
    for( hdl=skiq_rx_hdl_A1; hdl<skiq_rx_hdl_end; hdl++ )
    {
        if( skiq.card[card].rx_list[hdl].streaming == true )
        {
            p_hdls_streaming[*p_num_hdls] = (skiq_rx_hdl_t)(hdl);
            *p_num_hdls = *p_num_hdls + 1;
        }
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_stream_handle_conflict() function is responsible for providing
    a list of RX handles that cannot be streaming simultaneous to the handle 
    specified.  If streaming is requested with a conflicting handle, the stream
    cannot be started.

    @ingroup rxfunctions

    @since Function added in @b v4.9.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t]  the handle of the requested rx interface
    @param[out] p_conflicting_hdls [::skiq_rx_hdl_t] array of handles that conflict
                Must be large enough to contain ::skiq_rx_hdl_end elements.
    @param[out] p_num_hdls pointer of where to store number of handles in conflict list

    @return int32_t
    @retval 0 p_hdls_streaming populated with RX handles currently streaming
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EINVAL Error occurred reading conflicting handles
    @retval non-zero : other error occurred
*/
EPIQ_API int32_t skiq_read_rx_stream_handle_conflict(uint8_t card,
                                                     skiq_rx_hdl_t hdl_to_stream,
                                                     skiq_rx_hdl_t *p_conflicting_hdls,
                                                     uint8_t *p_num_hdls)
{
    int32_t status=0;
    rfe_t rfe_instance;
    skiq_rx_hdl_t rfe_conflicts[skiq_rx_hdl_end];
    uint8_t rfe_num_conflicts=0;
    rfic_t rfic_instance;
    skiq_rx_hdl_t rfic_conflicts[skiq_rx_hdl_end];
    uint8_t rfic_num_conflicts=0;
    uint8_t i=0;
    uint8_t j=0;
    bool hdl_present=false;
    
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl_to_stream );

    *p_num_hdls = 0;

    rfe_instance = _skiq_get_rx_rfe_instance( card, hdl_to_stream );
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl_to_stream );

    if( (rfe_read_rx_stream_hdl_conflict( &rfe_instance,
                                          rfe_conflicts,
                                          &rfe_num_conflicts ) == 0) &&
        (rfic_read_rx_stream_hdl_conflict( &rfic_instance,
                                           rfic_conflicts,
                                           &rfic_num_conflicts ) == 0) )
    {
        // need to merge RFE and RFIC lists

        // add all of the RFE handles
        for( i=0; i<rfe_num_conflicts; i++ )
        {
            p_conflicting_hdls[*p_num_hdls] = rfe_conflicts[i];
            *p_num_hdls = *p_num_hdls + 1;
        }
        // add any RFIC handles that aren't already in the list
        for( i=0; i<rfic_num_conflicts; i++ )
        {
            hdl_present = false;
            for( j=0; (j<rfe_num_conflicts) && (hdl_present==false); j++ )
            {
                if( rfic_conflicts[i] == rfe_conflicts[j] )
                {
                    hdl_present = true;
                }
            }
            // didn't find the handle in the RFE list, add it
            if( hdl_present == false )
            {
                p_conflicting_hdls[*p_num_hdls] = rfic_conflicts[i];
                *p_num_hdls = *p_num_hdls + 1;
            }
        }
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_start_rx_streaming() function is responsible for starting the
    flow of data between the FPGA and the CPU.  This function triggers the
    FPGA to start receiving data and transferring it to the CPU.  A continuous
    flow of packets will be transferred from the FPGA to the CPU until the user
    app calls skiq_stop_rx_streaming().  These packets will be received by the
    user app by calling skiq_receive(), which returns one packet at a time.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl  the handle of the requested rx interface

    @return int32_t
    @retval 0 successful start streaming for handle specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed (nr_handles < 1, etc)
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : Configured RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : Configured RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -EIO Failed to start streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval non-zero : An unspecified error occurred
*/
EPIQ_API int32_t skiq_start_rx_streaming( uint8_t card,
                                          skiq_rx_hdl_t hdl )
{
    return (_start_rx_streaming_multi(card, &hdl, 1, skiq_trigger_src_immediate, 0));
}

/*****************************************************************************/
/** The skiq_start_rx_streaming_on_1pps() function is identical to the
    skiq_start_rx_streaming with exception of when the data stream starts to
    flow.  When calling this function, the data does not begin to flow until
    the rising 1PPS edge after the system timestamp specified has occurred.
    If a timestamp of 0 is provided, then the next 1PPS edge will begin the
    data flow. This function blocks until the data starts flowing.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl  the handle of the requested rx interface
    @param[in] sys_timestamp system timestamp after the next 1PPS will begin the data flow

    @return int32_t
    @retval 0 successful start streaming for handle specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed (nr_handles < 1, etc)
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : Configured RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : Configured RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -EIO Failed to start streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval -EPROTO System Timestamp is not changing or moving backwards
    @retval non-zero : An unspecified error occurred
*/
EPIQ_API int32_t skiq_start_rx_streaming_on_1pps( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  uint64_t sys_timestamp )
{
    return (_start_rx_streaming_multi(card, &hdl, 1, skiq_trigger_src_1pps, sys_timestamp));
}


/*****************************************************************************/
/** The start_tx_streaming function is common helper function that both
    skiq_start_tx_streaming and skiq_start_tx_streaming_on_1pps functions call.
    This function performs all of the actual work involved to get the stream
    running with the various parameters setup correctly.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param wait_for_pps  flag on whether the streaming should wait for 1PPS
    @param sys_timestamp system timestamp after which the 1PPS will start the
              stream if the wait_for_pps is set
    @return int32_t  status where 0=success, anything else is an error

    @retval -ENOTSUP    if ::skiq_tx_with_timestamps_allow_late_data_flow_mode
                        is selected and the currently loaded bitfile does not
                        support that feature.
*/
static int32_t start_tx_streaming(uint8_t card, skiq_tx_hdl_t hdl,
                                  bool wait_for_pps, uint64_t sys_timestamp)
{
    uint32_t data=0;
    int32_t status=0;
    uint16_t fpga_block_size;
    uint8_t mult_factor=1;
    fpga_ctrl_tx_params_t tx_params;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    bool update_enable=false;

    TRACE();

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfe_instance = _skiq_get_tx_rfe_instance(card,hdl);
    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
    
    LOCK_REGS(card);

    // determine if we need to update the TX enable states
    update_enable = !(rfic_is_chan_enable_xport_dependent( &rfic_instance ));

    /* check if any handles are already streaming, if not, apply the iq order mode */
    if ( !is_tx_streaming(card) )
    {
        status = apply_iq_order_mode( card );
        if ( status != 0 )
        {
            skiq_error("Failed to apply iq order mode for card %u when starting Tx streaming with "
                        "status %d\n", card, status);

            UNLOCK_REGS(card);
            return (status);
        }
    }
    
    if((skiq.card[card].timestamp_base == skiq_tx_system_timestamp) &&
       (skiq.card[card].tx_list[hdl].flow_mode != skiq_tx_with_timestamps_allow_late_data_flow_mode))
    {
        status = -EINVAL;
        skiq_error("A timestamp base of skiq_tx_system_timestamp on card %" PRIu8 " (hdl=%s) must set"
                    "the data flow mode to skiq_tx_with_timestamps_allow_late_data_flow_mode; status=%" PRIi32 "\n",
                    card, tx_hdl_cstr(hdl), status);
        UNLOCK_REGS(card);
        return (status);
    }
    
    status = configure_tx_late_timestamp_mode(card, hdl);
    if (0 != status)
    {
        UNLOCK_REGS(card);
        return (status);
    }

    if ( hdl == skiq_tx_hdl_A2 )
    {
        skiq_info("Selecting TxA2 as the second transmit channel on card %u\n", card);
        status = fpga_ctrl_select_TxA2_as_second_chan(card);
    }
    else if ( hdl == skiq_tx_hdl_B1 )
    {
        if ( _skiq_meets_FPGA_VERSION( card, FPGA_VERS_A2_MAP_TO_B1 ) )
        {
            skiq_info("Selecting TxB1 as the second transmit channel on card %u\n", card);
            status = fpga_ctrl_select_TxB1_as_second_chan(card);
        }
        else
        {
            uint8_t major = 0, minor = 0, patch = 0;

            status = skiq_read_fpga_semantic_version( card, &major, &minor, &patch );
            if ( status == 0 )
            {
                skiq_error("Loaded FPGA version v%u.%u.%u does not meet minimum requirement "
                           "(" FPGA_VERS_A2_MAP_TO_B1_CSTR ") to select TxB1 as the second "
                           "transmit channel on card %u\n", major, minor, patch, card);
            }
            else
            {
                skiq_error("Loaded FPGA version does not meet minimum requirement ("
                           FPGA_VERS_A2_MAP_TO_B1_CSTR ") to select TxB1 as the second transmit "
                           "channel on card %u\n", card);
            }

            status = -ENOTSUP;
            UNLOCK_REGS(card);
            return (status);
        }
    }

    if (0 != status)
    {
        skiq_error("Failed to select second transmit channel on card %u\n", card);
        UNLOCK_REGS(card);
        return (status);
    }

    // set the Tx mode...note that we're not going to check the port config here since
    // that's done for us in the underlying function
    skiq_write_rf_port_operation(card, true);

    /* set the channel mode based on our chan config.  the FPGA doesn't have a FIFO for this
       register, so we need to verify before we move on*/
    sidekiq_fpga_reg_read( card, FPGA_REG_TIMESTAMP_RST, &data );
    if( skiq.card[card].chan_mode == skiq_chan_mode_single )
    {
        BF_SET( data, 0, CHAN_MODE_OFFSET, CHAN_MODE_LEN );
        mult_factor=1;
    }
    else
    {
        BF_SET( data, 1, CHAN_MODE_OFFSET, CHAN_MODE_LEN );
        mult_factor=2;
    }
    sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_TIMESTAMP_RST, data );

    fpga_ctrl_enable_stream( card );

    fpga_block_size = skiq.card[card].tx_list[hdl].block_size - 1;

    /* calculate the number of bytes to send */
    skiq.card[card].tx_list[hdl].num_bytes_to_send =
        ((skiq.card[card].tx_list[hdl].block_size*mult_factor) + SKIQ_TX_HEADER_SIZE_IN_WORDS)*4;
    if( ((skiq.card[card].tx_list[hdl].num_bytes_to_send/4) % SKIQ_TX_PACKET_SIZE_INCREMENT_IN_WORDS) != 0 )
    {
        UNLOCK_REGS(card);
        skiq_error("Tx packet size must be a multiple of %u words.  Current packet size is "
                   "specified at %u words for card %u\n", SKIQ_TX_PACKET_SIZE_INCREMENT_IN_WORDS,
                   skiq.card[card].tx_list[hdl].num_bytes_to_send/4, card);
        return (-1);
    }

    // if we're running in packed mode, we need to make sure that we end on an even boundary */
    if( skiq.card[card].iq_packed == true )
    {
        if( ((skiq.card[card].tx_list[hdl].block_size * 4) % 3) != 0 )
        {
            UNLOCK_REGS(card);
            skiq_error("Whenever configured for packed mode, the Tx block size must result in an "
                       "even number of packed samples\n");
            return (-2);
        }
    }

    // cancel any pending PPS transactions 
    //  This may be redundant with fpga_ctrl_cancel_1pps also accessed via the call chain:
    //  fpga_ctrl_start_tx_streaming >> _wait_1pps_after_ts >> _wait_1pps >> fpga_ctrl_cancel_1pps
    fpga_ctrl_cancel_1pps(card);

    /* enable the transmit channel in the RFE */
    rfe_enable_tx_chan( &(rfe_instance), true );

    /* enable the output buffer in the FPGA */
    rfe_update_tx_pa( &(rfe_instance), true );

    /* enable the channel in RFIC */
    if( update_enable == true )
    {
        rfic_enable_tx_chan( &rfic_instance, true );
    }
    if( skiq.card[card].chan_mode == skiq_chan_mode_dual )
    {
        skiq_fpga_tx_fifo_size_t fifo_size = skiq_fpga_tx_fifo_size_unknown;
        uint32_t fifo_bytes;
        skiq_tx_hdl_t other_hdl;

        if ( _skiq_get_nr_tx_hdl( card ) < 2 )
        {
            UNLOCK_REGS(card);
            skiq_error("Dual channel transmit not supported on card %u, only one transmit handle "
                       "available\n", card);
            return (-1);
        }

        // we need to make sure the block size requested is within the FPGA FIFO size
        skiq_read_fpga_tx_fifo_size( card, &fifo_size );
        fifo_bytes = (pow(2, (fifo_size+1)))*1024*4;
        if( (skiq.card[card].tx_list[hdl].num_bytes_to_send) > (fifo_bytes-2048) )
        {
            UNLOCK_REGS(card);
            skiq_error("Whenever configured for dual channel mode, Tx packet size must be 2k words "
                       "less than or equal to the FPGA FIFO size.  Current FIFO size (in words) is "
                       "%u\n", fifo_bytes/4);
            return (-1);
        }

        /* get rfe and rfic instances for the "other" handle */
        other_hdl = rfic_tx_hdl_map_other(&rfic_instance);
        if ( hdl == skiq_tx_hdl_B1 )
        {
            /* special case, choose A1 as the "other" handle when TX streaming is requested for B1
             * and skiq_chan_mode_dual is configured */
            skiq_info("Choosing TxA1 as the other handle for dual channel transmit on card %u\n", card);
            other_hdl = skiq_tx_hdl_A1;
        }

        if( other_hdl != skiq_tx_hdl_end )
        {
            rfe_t rfe_other = _skiq_get_tx_rfe_instance(card, other_hdl);
            rfic_t rfic_other = _skiq_get_tx_rfic_instance(card, other_hdl);

            /* enable the output buffer of the 2nd channel */
            rfe_update_tx_pa( &(rfe_other), true );

            /* enable the "other" transmit channel in the RFE */
            rfe_enable_tx_chan( &(rfe_other), true );

            /* enable the channel in RFIC */
            if( update_enable == true )
            {
                rfic_enable_tx_chan( &rfic_other, true );
            }
        }
    }

    if( status == 0 )
    {
        /* initialize the send paramters */
        status = skiq_xport_tx_initialize( card,
                                           skiq.card[card].tx_list[hdl].tx_transfer_mode,
                                           skiq.card[card].tx_list[hdl].num_bytes_to_send,
                                           skiq.card[card].num_send_threads,
                                           skiq.card[card].thread_priority,
                                           skiq.card[card].tx_complete );
    }

    if( status == 0 )
    {
        // fill out TX stream params
        tx_params.fpga_block_size = fpga_block_size;
        tx_params.flow_mode = skiq.card[card].tx_list[hdl].flow_mode;
        tx_params.packed = skiq.card[card].iq_packed;
        tx_params.wait_for_pps = wait_for_pps;
        tx_params.pps_sys_timestamp = sys_timestamp;
        tx_params.tx_enabled_cb = skiq.card[card].tx_enabled_cb;

        status = fpga_ctrl_start_tx_streaming( card,
                                               &tx_params );
    }


    /* notify the xport that we're starting streaming */
    if( status == 0 )
    {
        status = skiq_xport_tx_start_streaming( card, hdl );
    }

    if( status == 0 )
    {
        /* set the streaming flag */
        skiq.card[card].tx_list[hdl].streaming = true;
    }

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_start_tx_streaming() function is responsible for preparing for the
    flow of data between the CPU and the FPGA.  Once started, the
    data flow can be stopped with a call to skiq_stop_tx_streaming().

    The total size of the transmit packet must be in an increment of
    SKIQ_TX_PACKET_SIZE_INCREMENT_IN_WORDS.  The packet size is calculated by:
    block_size + header_size.  If this condition is not met, an error will be
    returned and the transmit stream will not begin.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the tx interface to start streaming
    @return int32_t status where 0=success, anything else is an error

    @retval -ENOTSUP    if ::skiq_tx_with_timestamps_allow_late_data_flow_mode
                        TX data flow mode is selected and the currently loaded
                        bitfile on the selected card does not support that
                        feature.
*/
EPIQ_API int32_t skiq_start_tx_streaming(uint8_t card, skiq_tx_hdl_t hdl)
{
    return (start_tx_streaming(card, hdl, false, 0));
}

/*****************************************************************************/
/** The skiq_start_tx_streaming_on_1pps() function is identical to the
    skiq_start_tx_streaming with exception of when the data stream starts to
    flow.  When calling this function, the data does not begin to flow until
    the rising 1PPS edge after the system timestamp specified has occurred.
    If a timestamp of 0 is provided, then the next 1PPS edge will begin the
    data flow. This function blocks until the data starts flowing.

    The total size of the transmit packet must be in an increment of
    SKIQ_TX_PACKET_SIZE_INCREMENT_IN_WORDS.  The packet size is calculated by:
    block_size + header_size.  If this condition is not met, an error will be
    returned and the transmit stream will not begin.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param sys_timestamp system timestamp after the next 1PPS will begin the
             data flow
    @return int32_t  status where 0=success, anything else is an error

    @retval -ENOTSUP    if ::skiq_tx_with_timestamps_allow_late_data_flow_mode
                        TX data flow mode is selected and the currently loaded
                        bitfile on the selected card does not support that
                        feature.
*/
EPIQ_API int32_t skiq_start_tx_streaming_on_1pps(uint8_t card, skiq_tx_hdl_t hdl,
                                        uint64_t sys_timestamp)
{
    return (start_tx_streaming(card, hdl, true, sys_timestamp));
}


/*****************************************************************************/
/** The _stop_rx_streaming_multi() helper function allows a user to stop
    multiple receive streams after the specified trigger occurs.

    @warning If one of the receive handles is not streaming then this function
    returns an error.

    @attention If ::skiq_trigger_src_1pps is used as a trigger then this
    function will @b block until the 1PPS edge occurs.

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]
    @param[in] trigger [::skiq_trigger_src_t] type of trigger to use
    @param[in] sys_timestamp System Timestamp after the next positive trigger will stop the data flow

    @return int32_t
    @retval 0 Success
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL Invalid parameter passed (nr_handles < 1, etc)
    @retval -ENODEV One of the specified handles is not currently streaming
    @retval -EIO Failed to stop streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP the ::skiq_trigger_src_synced trigger source is not supported for the given Sidekiq product or FPGA bitstream
    @retval -EPROTO System Timestamp is not changing or moving backwards (when using ::skiq_trigger_src_1pps trigger)
 */
static int32_t _stop_rx_streaming_multi( uint8_t card,
                                         skiq_rx_hdl_t handles[],
                                         uint8_t nr_handles,
                                         skiq_trigger_src_t trigger,
                                         uint64_t sys_timestamp )
{
    int32_t status = 0;
    ARRAY_WITH_DEFAULTS(bool,handle_requested,skiq_rx_hdl_end,false);
    skiq_rx_hdl_t hdl_idx;
    uint8_t i;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    int32_t chan_enable_status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ( nr_handles < 1 )
    {
        return -EINVAL;
    }

    /* check that all requested handles are valid */
    for (i = 0; i < nr_handles; i++)
    {
        CHECK_VALID_RX_HDL( card, handles[i] );
        handle_requested[handles[i]] = true;
    }

    LOCK_REGS(card);
    LOCK_RX(card);

    /* check to see if any of the requested handles are not streaming */
    for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
    {
        if ( handle_requested[hdl_idx] && !is_rx_hdl_streaming( card, hdl_idx ) )
        {
            skiq_error("At least one of the specified handles (%s) is not streaming for card %u\n",
                       rx_hdl_cstr(hdl_idx), card);

            status = -ENODEV;
            UNLOCK_RX(card);
            UNLOCK_REGS(card);
            return (status);
        }
    }

    /* notify the xport layer to stop streaming for all requested handles */
    for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
    {
        if ( handle_requested[hdl_idx] )
        {
            status = skiq_xport_rx_stop_streaming( card, hdl_idx );
            if ( status != 0 )
            {
                skiq_error("Transport layer failed to stop streaming for card %u and handle %s with status %d\n",
                           card, rx_hdl_cstr(hdl_idx), status);

                /* squash error from transport function into a generic I/O error */
                status = -EIO;
                UNLOCK_RX(card);
                UNLOCK_REGS(card);
                return (status);
            }
        }
    }

    /* stop the streams for all of the requested handles */
    {
        fpga_ctrl_rx_params_t rx_params[skiq_rx_hdl_end];
        uint8_t nr_rx_params = 0;

        for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
        {
            if ( handle_requested[hdl_idx] )
            {
                rx_params[nr_rx_params].ctrl_reg = FPGA_REG_DMA_CTRL_(hdl_idx);
                rx_params[nr_rx_params].hdl = hdl_idx;
                nr_rx_params++;
            }
        }

        status = fpga_ctrl_stop_rx_stream_multi( card,
                                                 rx_params, nr_rx_params,
                                                 trigger,
                                                 sys_timestamp );

        if ( status == 0 )
        {
            /* set the streaming flag */
            for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
            {
                if ( handle_requested[hdl_idx] )
                {
                    skiq.card[card].rx_list[hdl_idx].streaming = false;
                }
            }
        }
    }

    /* inform transport layer about RX configuration change */
    rx_configure( card );

    /* if all streaming is done, unconfigure streaming mode in the FPGA */
    if ( false == is_streaming(card) )
    {
        status = fpga_ctrl_finalize_stream_disable(card);
        if ( status != 0 ) status = -EBADMSG;
    }

    /* make sure the RFE/RFIC channels are disabled */
    for (hdl_idx = skiq_rx_hdl_A1; hdl_idx < skiq_rx_hdl_end; hdl_idx++)
    {
        if ( handle_requested[hdl_idx] )
        {
            rfe_instance = _skiq_get_rx_rfe_instance(card, hdl_idx);
            rfic_instance = _skiq_get_rx_rfic_instance(card, hdl_idx);
            chan_enable_status = rfe_enable_rx_chan( &rfe_instance, false );
            // not all RFEs support disabling channel...NBD
            if( (chan_enable_status != 0) && (chan_enable_status != -ENOTSUP) )
            {
                skiq_warning("Unable to disable RFE for handle %s (card=%u)",
                             rx_hdl_cstr(hdl_idx), card);
            }
            if( rfic_is_chan_enable_xport_dependent( &rfic_instance) == false )
            {
                if( rfic_enable_rx_chan( &rfic_instance, false ) != 0 )
                {
                    skiq_warning("Unable to disable RFIC for handle %s (card=%u)",
                                 rx_hdl_cstr(hdl_idx), card);
                }
            }
        }
    }

    UNLOCK_RX(card);
    UNLOCK_REGS(card);

    return(status);
}

/*****************************************************************************/
/** The skiq_stop_rx_streaming_multi_immediate() function allows a user to stop
    multiple receive streams immediately (not necessarily timestamp-synchronized
    depending on FPGA support and library support).

    @warning If one of the receive handles is not streaming then this function
    returns an error.

    @since Function added in @b v4.5.0

    @ingroup rxfunctions

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]

    @return int32_t
    @retval 0 Success
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL Invalid parameter passed (nr_handles < 1, etc)
    @retval -ENODEV One of the specified handles is not currently streaming
    @retval -EIO Failed to stop streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
EPIQ_API int32_t skiq_stop_rx_streaming_multi_immediate( uint8_t card,
                                                         skiq_rx_hdl_t handles[],
                                                         uint8_t nr_handles )
{
    return (_stop_rx_streaming_multi(card, handles, nr_handles, skiq_trigger_src_immediate, 0));
}

/*************************************************************************************************/
/** The skiq_stop_rx_streaming_multi_synced() function allows a user to stop multiple receive
    streams immediately and with timestamp synchronization (not necessarily phase coherent however).

    @warning If one of the receive handles is not streaming then this function returns an error.

    @attention Not all Sidekiq products support the ::skiq_trigger_src_synced trigger source.

    @since Function added in @b v4.9.0, requires FPGA bitstream @b v3.11.0 or greater

    @ingroup rxfunctions

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]

    @return int32_t
    @retval 0 Success
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL Invalid parameter passed (nr_handles < 1, etc)
    @retval -ENODEV One of the specified handles is not currently streaming
    @retval -EIO Failed to stop streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP the ::skiq_trigger_src_synced trigger source is not supported for the given Sidekiq product or FPGA bitstream
 */
EPIQ_API int32_t skiq_stop_rx_streaming_multi_synced( uint8_t card,
                                                         skiq_rx_hdl_t handles[],
                                                         uint8_t nr_handles )
{
    return (_stop_rx_streaming_multi(card, handles, nr_handles, skiq_trigger_src_synced, 0));
}

/*****************************************************************************/
/** The skiq_stop_rx_streaming_multi_on_trigger() function allows a user to
    stop multiple receive streams after the specified trigger occurs.

    @warning If one of the receive handles is not streaming then this function
    returns an error.

    @attention If ::skiq_trigger_src_1pps is used as a trigger then this
    function will @b block until the 1PPS edge occurs.

    @since Function added in @b v4.5.0

    @ingroup rxfunctions

    @param[in] card  card index of the Sidekiq of interest
    @param[in] handles  [array of ::skiq_rx_hdl_t] the receive handles to start streaming
    @param[in] nr_handles  the number of entries in handles[]
    @param[in] trigger [::skiq_trigger_src_t] type of trigger to use
    @param[in] sys_timestamp System Timestamp after the next positive trigger will stop the data flow

    @retval 0 successful start streaming for handle(s) specified
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL : Invalid parameter passed
    @retval -ENODEV One of the specified handles is not currently streaming
    @retval -EBUSY : One of the specified handles is already streaming
    @retval -EBUSY : A conflicting handle is already streaming
    @retval -ENOTSUP : specified RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL : specified RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
    @retval -EPERM : I/Q packed mode is already enabled and conflicts with the requested RX stream mode
    @retval -ENOSYS : Transport does not support FPGA register access
    @retval -EIO Failed to stop streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOTSUP the ::skiq_trigger_src_synced trigger source is not supported for the given Sidekiq product or FPGA bitstream
    @retval -EPROTO System Timestamp is not changing or moving backwards (when using ::skiq_trigger_src_1pps trigger)
    @retval non-zero : An unspecified error occurred
 */
EPIQ_API int32_t skiq_stop_rx_streaming_multi_on_trigger( uint8_t card,
                                                          skiq_rx_hdl_t handles[],
                                                          uint8_t nr_handles,
                                                          skiq_trigger_src_t trigger,
                                                          uint64_t sys_timestamp )
{
    return (_stop_rx_streaming_multi(card, handles, nr_handles, trigger, sys_timestamp));
}

/*****************************************************************************/
/** The skiq_stop_rx_streaming() function is responsible for stopping the
    streaming of data between the FPGA and the CPU.  This function can only
    be called after an interface has previously started streaming.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl  the handle of the requested rx interface

    @return int32_t
    @retval 0 Success
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL Invalid parameter passed (nr_handles < 1, etc)
    @retval -ENODEV One of the specified handles is not currently streaming
    @retval -EIO Failed to stop streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
EPIQ_API int32_t skiq_stop_rx_streaming( uint8_t card,
                                         skiq_rx_hdl_t hdl )
{
    return (_stop_rx_streaming_multi( card, &hdl, 1, skiq_trigger_src_immediate, 0 ) );
}

/*****************************************************************************/
/** The skiq_stop_rx_streaming_on_1pps() function stops the data from flowing on
    the rising edge of the 1PPS after the timestamp specified.  If a timestamp
    of 0 is provided, then the next 1PPS edge will stop the data flow.  This
    function blocks until the data stream has been stopped.

    Note: this stops the data at the FPGA.  However, there will be data
    remaining in the internal FIFOs, so skiq_receive() should continue to be
    called until no data remains.  Once that is complete, the
    skiq_stop_rx_streaming() function should be called to finalize the
    disabling of the data flow.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl  the handle of the requested rx interface
    @param[in] sys_timestamp system timestamp after the next 1PPS will begin the data flow

    @return int32_t
    @retval 0 Success
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -EINVAL Invalid parameter passed (nr_handles < 1, etc)
    @retval -ENODEV One of the specified handles is not currently streaming
    @retval -EIO Failed to stop streaming for given transport
    @retval -EBADMSG Error occurred transacting with FPGA registers
*/
EPIQ_API int32_t skiq_stop_rx_streaming_on_1pps( uint8_t card,
                                                 skiq_rx_hdl_t hdl,
                                                 uint64_t sys_timestamp )
{
    return (_stop_rx_streaming_multi( card, &hdl, 1, skiq_trigger_src_1pps, sys_timestamp ) );
}

/*****************************************************************************/
/** The skiq_stop_tx_streaming() function is responsible for stopping the
    streaming of data between the CPU and the FPGA.  This function can only
    be called after an interface has previously started streaming.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_stop_tx_streaming( uint8_t card, skiq_tx_hdl_t hdl )
{
    int32_t status=0;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    bool update_enable = false;

    TRACE();

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    LOCK_TX(card);

    // determine if we need to update the TX enable states
    rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    rfe_instance = _skiq_get_tx_rfe_instance(card, hdl);
    update_enable = !(rfic_is_chan_enable_xport_dependent( &rfic_instance ));

    skiq_xport_tx_pre_stop_streaming( card, hdl );

    fpga_ctrl_stop_tx_stream(card);

    skiq_xport_tx_stop_streaming(card, hdl);

    /* clear the streaming flag */
    skiq.card[card].tx_list[hdl].streaming = false;
    /* if all streaming is done, unconfigure streaming mode in the FPGA */
    if( false == is_streaming(card) )
    {
        fpga_ctrl_finalize_stream_disable( card );
    }

    // disable the output buffer
    rfe_update_tx_pa( &(rfe_instance), false );

    /* disable the transmit channel in the RFE */
    rfe_enable_tx_chan( &(rfe_instance), false );

    if( update_enable == true )
    {
        rfic_enable_tx_chan( &rfic_instance, false );
    }
    // if dual chan, turn off the 2nd path
    if( skiq.card[card].chan_mode == skiq_chan_mode_dual )
    {
        skiq_tx_hdl_t other_hdl = rfic_tx_hdl_map_other(&rfic_instance);

        if ( hdl == skiq_tx_hdl_B1 )
        {
            /* special case, choose A1 as the "other" handle when TX streaming is requested for B1
             * and skiq_chan_mode_dual is configured */
            other_hdl = skiq_tx_hdl_A1;
        }

        if ( other_hdl != skiq_tx_hdl_end )
        {
            rfe_t rfe_other = _skiq_get_tx_rfe_instance(card, other_hdl);
            rfic_t rfic_other = _skiq_get_tx_rfic_instance(card, other_hdl);

            // disable the output buffer for the "other" handle
            rfe_update_tx_pa( &(rfe_other), false );

            /* disable the "other" transmit channel in the RFE */
            rfe_enable_tx_chan( &(rfe_other), false );

            if( update_enable == true )
            {
                rfic_enable_tx_chan( &rfic_other, false );
            }
        }
    }

    if ( hdl == skiq_tx_hdl_B1 )
    {
        /* don't bother to check FPGA version here since we're setting to the 'default' value and it
         * should *always* work */
        status = fpga_ctrl_select_TxA2_as_second_chan(card);
    }

    UNLOCK_TX(card);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_stop_tx_streaming_on_1pps() function is identical to the
    skiq_stop_tx_streaming function with the exception of when the data stops
    streaming.  When calling this function, the data stream is disabled on the
    rising 1PPS edge after the system timestamp specified has occurred.  If a
    timestamp of 0 is provided, then the next 1PPS edge will stop the data flow.
    This function blocks until the data flow is disabled.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_stop_tx_streaming_on_1pps(uint8_t card, skiq_tx_hdl_t hdl,
                                       uint64_t sys_timestamp)
{
    int32_t status=0;

    TRACE();

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);

    skiq_xport_tx_pre_stop_streaming( card, hdl );
    
    fpga_ctrl_stop_tx_stream_on_1pps(card, sys_timestamp);

    /* finalize the stop streaming */
    skiq_stop_tx_streaming(card, hdl);

    UNLOCK_REGS(card);

    return (status);
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_read_last_1pps_timestamp( uint8_t card,
                                                uint64_t* p_rf_timestamp,
                                                uint64_t* p_sys_timestamp )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        status = fpga_ctrl_read_last_1pps_timestamps( card, p_rf_timestamp, p_sys_timestamp );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_read_last_1pps_timestamp */


/*****************************************************************************/
/** The skiq_write_timestamp_reset_on_1pps() function is responsible for
    configuring the FPGA to reset all the timestamps at a well defined
    point in the future.  This point in the future is the occurrance of a
    1PPS AFTER the specified system timestamp.

    @param card card index of the Sidekiq of interest
    @param future_sys_timestamp the value of the system timestamp of a well
    defined point in the future, where the next 1PPS signal after this timestamp
    value will cause the timestamp to reset back to 0
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_timestamp_reset_on_1pps(uint8_t card, uint64_t future_sys_timestamp)
{
    int32_t status=0;

    status = skiq_write_timestamp_update_on_1pps(card, future_sys_timestamp, 0);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_timestamp_update_on_1pps() function is responsible for
    configuring the FPGA to set all timestamps to a specific value at a well
    defined point in the future.  This point in the future is the occurrance
    of a 1PPS AFTER the specified system timestamp.

    @param card card index of the Sidekiq of interest
    @param future_sys_timestamp the value of the system timestamp of a well
    defined point in the future, where the next 1PPS signal after this timestamp
    value will cause the timestamp to update to the value specified
    @param new_timestamp the value to set all timestamps to after the 1PPS
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_timestamp_update_on_1pps(uint8_t card,
                                            uint64_t future_sys_timestamp,
                                            uint64_t new_timestamp)
{
    int32_t status=0;
    uint32_t data=0;

    LOCK_REGS(card);

    // write the new timestamp
    sidekiq_fpga_reg_write_64(card,
                              FPGA_REG_SET_ALL_TIMESTAMP_HIGH,
                              FPGA_REG_SET_ALL_TIMESTAMP_LOW,
                              new_timestamp);

    // write the timestamp reset value
    sidekiq_fpga_reg_write_64(card,
                              FPGA_REG_1PPS_RESET_TIMESTAMP_HIGH,
                              FPGA_REG_1PPS_RESET_TIMESTAMP_LOW,
                              future_sys_timestamp);

    sidekiq_fpga_reg_read(card, FPGA_REG_TIMESTAMP_RST, &data);

    /* unfortunately, this register space doesn't have a FIFO associated with it
       and it crosses clock domains, so we need to verify that the write happened
       before we do an additional write */

    // set the reset on PPS bit
    BF_SET(data, 1, RESET_TS_ON_PPS_OFFSET, RESET_TS_ON_PPS_LEN);
    sidekiq_fpga_reg_write(card, FPGA_REG_TIMESTAMP_RST, data);
    sidekiq_fpga_reg_verify(card, FPGA_REG_TIMESTAMP_RST, data);
    // clear the reset on PPS bit
    BF_SET(data, 0, RESET_TS_ON_PPS_OFFSET, RESET_TS_ON_PPS_LEN);
    sidekiq_fpga_reg_write(card, FPGA_REG_TIMESTAMP_RST, data);
    sidekiq_fpga_reg_verify(card, FPGA_REG_TIMESTAMP_RST, data);

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** @brief this function is responsible for returning the current timestamp
    base for transmitting on timestamp.
*/
EPIQ_API int32_t skiq_read_tx_timestamp_base(uint8_t card, skiq_tx_timestamp_base_t* p_timestamp_base)
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_timestamp_base);

    LOCK_REGS(card);
    {
        *p_timestamp_base = skiq.card[card].timestamp_base;
    }
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** @brief this function is responsible for configuring the timestamp base for
    transmitting on timestamp.
*/
EPIQ_API int32_t skiq_write_tx_timestamp_base(uint8_t card, skiq_tx_timestamp_base_t timestamp_base)
{
    int32_t status = -ENOTSUP;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if((timestamp_base < skiq_tx_rf_timestamp) ||
       (timestamp_base > skiq_tx_system_timestamp))
    {
        status = -EINVAL;
        skiq_error("Invalid tx timestamp base selection made for configuration on card %" PRIu8 ""
        " with status=%" PRIi32 "\n", card, status);
        return status;
    }

    LOCK_REGS(card);
    {
        if(_skiq_get_part(card) != skiq_mpcie)
        {
            if(_skiq_meets_FPGA_VERSION(card, MIN_FPGA_VERS_TIMESTAMP_BASE))
            {
                if(timestamp_base == skiq_tx_system_timestamp)
                {
                    status = sidekiq_fpga_reg_RMWV(card, 1, FRC_SEL_FOR_TX, FPGA_REG_FPGA_CTRL);
                    if(status == 0)
                    {
                        debug_print("Configured timestamp base to use the system free running clock");
                    }
                }
                else if(timestamp_base == skiq_tx_rf_timestamp)
                {
                    status = sidekiq_fpga_reg_RMWV(card, 0, FRC_SEL_FOR_TX, FPGA_REG_FPGA_CTRL);
                    if(status == 0)
                    {
                        debug_print("Configured timestamp base to use the rf free running clock");
                    }
                }
                if(status == 0)
                {
                    skiq.card[card].timestamp_base = timestamp_base;
                }
                else
                {
                    skiq_error("Unable to write tx timestamp base on card %" PRIu8 " with status=%" PRIi32 "\n", card, status);
                }
            }
            else
            {
                if(timestamp_base != skiq_tx_rf_timestamp)
                {
                    status = -ENOSYS;
                }
                else
                {
                    status = 0;
                }
            }
        }
        else
        {
            if(timestamp_base == skiq_tx_rf_timestamp)
            {
                status = 0;
            }
        }
    }
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_tx_data_flow_mode() function is responsible for returning the
    current data flow mode for the Tx interface; this can be one of the
    following:

    - ::skiq_tx_immediate_data_flow_mode, where timestamps are ignored, and data
    is transmitted as soon as possible.
    - ::skiq_tx_with_timestamps_data_flow_mode, where the FPGA will ensure that
    the data is sent at the appropriate timestamp.
    - ::skiq_tx_with_timestamps_allow_late_data_flow_mode, where the FPGA will
    ensure that the data is sent at the appropriate timestamp, but will also
    send data with timestamps that have already passed.

    @note   With ::skiq_tx_with_timestamps_data_flow_mode, if data arrives
    when the FPGA's timestamp is greater than the data's associated timestamp,
    the data is considered late and not transmitted. This is not the case with
    ::skiq_tx_with_timestamps_allow_late_data_flow_mode, which will allow late
    data to be transmitted.

    @ingroup txfunctions

    @param[in]  card    card index of the Sidekiq of interest
    @param[in]  hdl     the handle of the Tx interface of interest
    @param[out] p_mode  a pointer to where the current data flow mode will be written

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_data_flow_mode(uint8_t card, skiq_tx_hdl_t hdl,    \
                    skiq_tx_flow_mode_t* p_mode)
{
    int32_t status=0;
    TRACE();

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    *p_mode = skiq.card[card].tx_list[hdl].flow_mode;

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_data_flow_mode() function is responsible for updating the
    current data flow mode for the interface; this can be one of the following:

    - ::skiq_tx_immediate_data_flow_mode, where timestamps are ignored, and data
    is transmitted as soon as possible.
    - ::skiq_tx_with_timestamps_data_flow_mode, where the FPGA will ensure that
    the data is sent at the appropriate timestamp.
    - ::skiq_tx_with_timestamps_allow_late_data_flow_mode, where the FPGA will
    ensure that the data is sent at the appropriate timestamp, but will also
    send data with timestamps that have already passed.

    @note   The data flow modes can be changed at any time, but updates are only
    honored whenever an interface is started through the
    skiq_start_tx_interface() call.

    @note   With ::skiq_tx_with_timestamps_data_flow_mode, if data arrives
    when the FPGA's timestamp is greater than the data's associated timestamp,
    the data is considered late and not transmitted. This is not the case with
    ::skiq_tx_with_timestamps_allow_late_data_flow_mode, which will allow late
    data to be transmitted.

    @attention  ::skiq_tx_with_timestamps_allow_late_data_flow_mode is only
    available on certain bitstreams; if this mode is set and the card's
    bitstream doesn't support it, -ENOTSUP is returned.

    @attention  The late timestamp counter is not updated when in
    ::skiq_tx_write_timestamps_allow_late_data_flow_mode, even if the data is
    transmitted later than its timestamp.

    @ingroup txfunctions

    @param[in]  card    card index of the Sidekiq of interest
    @param[in]  hdl     the handle of the requested Tx interface
    @param[in]  mode    the requested data flow mode

    @return int32_t  status where 0=success, anything else is an error

    @retval -ENOTSUP    if ::skiq_tx_with_timestamps_allow_late_data_flow_mode
                        TX data flow mode is selected and the currently loaded
                        bitfile on the selected card does not support that
                        feature.
    @retval -EINVAL     if ::skiq_tx_with_timestamps_allow_late_data_flow_mode
                        TX data flow mode is not selected and the current config
                        for the timestamp base is to use system timestamps
*/
EPIQ_API int32_t skiq_write_tx_data_flow_mode(uint8_t card, skiq_tx_hdl_t hdl, skiq_tx_flow_mode_t mode)
{
    int32_t status=0;
    TRACE();

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    // TODO: ensure flow mode is consistent between handles...

    LOCK_REGS(card);

    /*
        If late timestamp mode is selected, verify that the card supports it
        before setting it.
    */
    {
        bool late_supported = false;
        if (skiq_tx_with_timestamps_allow_late_data_flow_mode == mode)
        {
            status = tx_late_timestamps_supported(card, hdl, &late_supported);
            if ((0 == status) && (!late_supported))
            {
                _skiq_log(SKIQ_LOG_WARNING,
                    "[card %u]: FPGA bitstream does not support late timestamp"
                    " TX mode.\n", card);
                status = -ENOTSUP;
            }
        }
        else if(skiq.card[card].timestamp_base == skiq_tx_system_timestamp)
        {
            skiq_error("Data flow mode must allow late timestamps when transmit is configured to use"
            " system timestamps on card %" PRIu8 "\n", card);
            status = -EPERM;
        }
    }
    
    if (0 == status)
    {
        skiq.card[card].tx_list[hdl].flow_mode = mode;
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_transfer_mode() function is responsible for returning
    the current transfer mode for the Tx interface.  This can be either
    tx synchronous or asynchronous.  In synchronous mode, the skiq_transmit
    call blocks until the packet has been received by the FPGA.  In the
    aynchronous mode, the skiq_transmit will accept the packet immediately
    as long as there's room within the buffer to store the pointer.  In the
    async mode, a callback function can be registered to notify the application
    when the transfer to the FPGA has been completed.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the Tx interface of interest
    @param p_transfer_mode  a pointer to where the current transfer mode will be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_transfer_mode( uint8_t card,
                                    skiq_tx_hdl_t hdl,
                                    skiq_tx_transfer_mode_t *p_transfer_mode )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    *p_transfer_mode = skiq.card[card].tx_list[hdl].tx_transfer_mode;

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_transfer_mode() function is responsible for updating the
    current transfer mode for the tx interface.  Note that this can only be
    changed if the transmit interface is not currently streaming.  If it is
    attempted to change this mode while streaming, an error will be returned.
    In synchronous mode, the skiq_transmit call blocks until the packet has
    been received by the FPGA.  In the aynchronous mode, the skiq_transmit
    will accept the packet immediately as long as there's room within the
    buffer to store the pointer.  In the async mode, a callback function can
    be registered to notify the application when the transfer to the FPGA
    has been completed.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested Tx interface
    @param transfer_mode  the requested transfer flow mode
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_transfer_mode( uint8_t card,
                                     skiq_tx_hdl_t hdl,
                                     skiq_tx_transfer_mode_t transfer_mode )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    if( skiq.card[card].tx_list[hdl].streaming == true )
    {
        _skiq_log(SKIQ_LOG_ERROR, "unable to change transfer mode while streaming\n");
        return -4;
    }

    LOCK_REGS(card);
    skiq.card[card].tx_list[hdl].tx_transfer_mode = transfer_mode;
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_register_tx_complete_callback() function registers a callback
    function that should be called when the transfer of a packet at the
    address provided has been completed.  Once the callback function is called
    the memory location specified by p_data has completed processing.  Note:
    this callback function is used only when streaming in the async mode.

    @param card card index of the Sidekiq of interest
    @param tx_complete pointer to function to call when a packet has finished transfer
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_register_tx_complete_callback( uint8_t card,
                                            skiq_tx_callback_t tx_complete )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    skiq.card[card].tx_complete = tx_complete;

    return (status);
}

/*****************************************************************************/
/** The skiq_register_tx_enabled_callback() function registers a callback
    function that should be called when the transmit FIFO is enabled and
    available to queue packets. 

    @since Function added in since v4.3.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] tx_ena_cb pointer to function to call when FIFO is enabled
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_register_tx_enabled_callback( uint8_t card,
                                                   skiq_tx_ena_callback_t tx_ena_cb )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    skiq.card[card].tx_enabled_cb = tx_ena_cb;

    return (status);
}

/*****************************************************************************/
/** The skiq_read_chan_mode() function is responsible for returning the
    current Rx channel mode setting.

    @param card card index of the Sidekiq of interest
    @param p_mode pointer to where to store the Rx channel mode setting
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_chan_mode(uint8_t card, skiq_chan_mode_t *p_mode)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_mode = skiq.card[card].chan_mode;

    return status;
}

/*****************************************************************************/
/** The skiq_write_chan_mode() function is responsible for configuring the
    channel mode.  If only A1 is needed for receiving or if transmit is
    being used it is recommended to configure the mode to skiq_chan_mode_single.
    If A2 is being used as a receiver or if both A1 and A2 are being used as
    receivers, than the mode should be configured to skiq_rx_chan_mode_dual.

    @param card card index of the Sidekiq of interest
    @param p_mode pointer to where to store the Rx channel mode setting
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_chan_mode(uint8_t card, skiq_chan_mode_t mode)
{
    int32_t status=0;
    rfic_t rfic_rxA1, rfic_rxA2, rfic_txA1, rfic_txA2;
    uint32_t rx_sample_rate;
    double actual_rx_sample_rate;
    uint32_t tx_sample_rate;
    double actual_tx_sample_rate;
    uint64_t freq = 0;
    double actual_freq = 0.0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_rxA1 = _skiq_get_rx_rfic_instance(card, skiq_rx_hdl_A1);
    rfic_rxA2 = _skiq_get_rx_rfic_instance(card, skiq_rx_hdl_A2);
    rfic_txA1 = _skiq_get_tx_rfic_instance(card, skiq_tx_hdl_A1);
    rfic_txA2 = _skiq_get_tx_rfic_instance(card, skiq_tx_hdl_A2);

    if( (mode == skiq_chan_mode_dual) )
    {
        uint32_t max_dual_sample_rate=0;
        skiq_part_t part = _skiq_get_part(card);

        if ( _skiq_get_nr_rx_hdl( card ) < 2 )
        {
            skiq_error("Dual channel mode not supported (number of RX channels available %u)\n",
                       _skiq_get_nr_rx_hdl( card ));
            return -4;
        }
        // make sure we don't exceed our max sample rate for dual chan mPCIe / Z3u
        else if( (part == skiq_mpcie) || (part == skiq_z3u) )
        {
            if( part == skiq_mpcie )
            {
                max_dual_sample_rate = SKIQ_MAX_DUAL_CHAN_MPCIE_SAMPLE_RATE;
            }
            else if( part == skiq_z3u )
            {
                max_dual_sample_rate = SKIQ_MAX_DUAL_CHAN_Z3U_SAMPLE_RATE;
            }
            else
            {
                skiq_error("Unexpected part %s in special dual chan mode handling for card %u", part_cstr(part), card);
                return (-EPROTO);
            }

            rfic_read_rx_sample_rate( &rfic_rxA1, &rx_sample_rate, &actual_rx_sample_rate );
            rfic_read_tx_sample_rate( &rfic_txA1, &tx_sample_rate, &actual_tx_sample_rate );

            if( (actual_rx_sample_rate > max_dual_sample_rate ) ||
                (actual_tx_sample_rate > max_dual_sample_rate) )
            {
                skiq_error("sample rate configured not supported with dual channel mode on %s for card %u\n", part_cstr(part), card);
                return -5;
            }
        }
    }


    // TODO: make sure we're not streaming...can't change this on the fly
    LOCK_REGS(card);

    //////////////////////////////
    // For radios such as X2/X4, we are going to rely on the appropriate channel being enabled in
    // the RFIC when the streaming call is made.  However, for legacy radios (9361 based), we
    // have to handle the enable independent of the stream.  This is to help deal with the
    // case where you want A2 streaming for example...we need to enable A1 and A2 in the
    // radio in order to provide the correct number of channels to the FPGA.  However,
    // we want to be able to turn on just the A2 stream from the FPGA even if the RFIC is
    // streaming both channels.  For radios such as the X4, the JESD link always provides
    // all the channels we expect, regardless of what's enabled in the RFIC...if the channel
    // is not enabled, then the RFIC just provides 0s.
    if( rfic_is_chan_enable_xport_dependent( &rfic_rxA1 ) == true )
    {
        if( mode == skiq_chan_mode_single )
        {
            // Note: we're assuming single channel=RxA1/TxA1 only!
            if( (rfic_enable_rx_chan( &rfic_rxA1, true ) != 0) ||
                (rfic_enable_rx_chan( &rfic_rxA2, false ) != 0) ||
                (rfic_enable_tx_chan( &rfic_txA1, true ) != 0) )
            {
                skiq_error("Unable to update channel enables (card=%u)", card);
                status = -EIO;
            }

            // if we support multiple channels, then enable TX otherwise disable it
            if ( _skiq_get_nr_tx_hdl( card ) > 1 )
            {
                if( (rfic_enable_tx_chan( &rfic_txA2, false ) != 0) )
                {
                    skiq_error("Unable to update Tx channel enables (card=%u)", card);
                    status = -EIO;
                }
            }
        }
        else if( mode == skiq_chan_mode_dual )
        {
            // Note: we're assuming single channel=RxA1/TxA1 only!
            if( (rfic_enable_rx_chan( &rfic_rxA1, true ) != 0) ||
                (rfic_enable_rx_chan( &rfic_rxA2, true ) != 0) )
            {
                skiq_error("Unable to update Rx channel enables (card=%u)", card);
                status = -EIO;
            }
            
            // if we support multiple channels, than enable TX otherwise disable it
            if ( _skiq_get_nr_tx_hdl( card ) > 1 )
            {
                if( (rfic_enable_tx_chan( &rfic_txA1, true ) != 0) ||
                    (rfic_enable_tx_chan( &rfic_txA2, true ) != 0) )
                {
                    skiq_error("Unable to update Tx channel enables (card=%u)", card);
                    status = -EIO;
                }
            }
            else
            {
                if( rfic_enable_tx_chan( &rfic_txA1, false ) != 0 )
                {
                    skiq_error("Unable to enable Tx channel (card=%u)", card);
                    status = -EIO;
                }
            }
        }
        else
        {
            _skiq_log(SKIQ_LOG_ERROR, "unsupported channel mode (%u) specified (card=%u)\n",
                      mode, card);
            status=-EINVAL;
        }
    }
    //////////////////////////////

    if( status==0 )
    {
        skiq.card[card].chan_mode = mode;
        // Note: update RFE based on the current configuration
        /* DLYDENTODO:  Think about this.  When setting channel mode on nv100, are we aware the LNA
           is being disabled on A2.  Will this matter or will the setting be clobbered*/
        if( mode == skiq_chan_mode_single )
        {
            if ( _skiq_get_nr_rx_hdl( card ) > 1 )
            {
                // make sure the LNA for the 2nd channel is off
                rfe_t rfe_rxA2 = _skiq_get_rx_rfe_instance(card,skiq_rx_hdl_A2);
                rfe_update_rx_lna( &(rfe_rxA2), lna_disabled );
            }

            if ( _skiq_get_nr_tx_hdl( card ) > 1 )
            {
                // make sure the PA for the 2nd channel is off
                rfe_t rfe_txA2 = _skiq_get_tx_rfe_instance(card,skiq_tx_hdl_A2);
                rfe_update_tx_pa( &(rfe_txA2), false );
            }
        }
        else
        {
            // make sure the 2nd path is configured properly
            tune_read_rx_LO_freq(card, skiq_rx_hdl_A1, &freq, &actual_freq);
            if ( 0 != freq )
            {
                status = tune_write_rx_LO_freq(card, skiq_rx_hdl_A2, freq,
                                               skiq.card[card].rx_list[skiq_rx_hdl_A2].tune_mode, 0);
            }

            tune_read_tx_LO_freq(card, skiq_tx_hdl_A1, &freq, &actual_freq);
            if ( (0 != freq) && (_skiq_get_nr_tx_hdl( card ) > 1) )
            {
                status = tune_write_tx_LO_freq(card, skiq_tx_hdl_A2, freq,
                                               skiq.card[card].tx_list[skiq_tx_hdl_A2].tune_mode,0);
            }
        }
    }
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_write_rx_preselect_filter_path() function is responsible for
    selecting either a lowband or highband RF path for the specified Rx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param path  an enum indicating which path is being requested
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_preselect_filter_path(uint8_t card, skiq_rx_hdl_t hdl, skiq_filt_t path)
{
    int32_t status = 0;
    rfe_t rfe_instance;

    // make sure a valid handle and path was specified
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    if( (path >= skiq_filt_max) || (path <= skiq_filt_invalid) )
    {
        status = -1;
    }
    else
    {
        LOCK_REGS(card);
        rfe_instance = _skiq_get_rx_rfe_instance(card,hdl);

        status=rfe_write_rx_filter_path( &(rfe_instance),
                                         path );

        UNLOCK_REGS(card);
    }

    return status;
}

/*****************************************************************************/
/** The skiq_read_rx_preselect_filter_path() function is responsible for
    returning the currently selected RF path on the specified Rx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param p_path  a pointer to where the current value of the filter path
    should be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_preselect_filter_path(uint8_t card,
                       skiq_rx_hdl_t hdl,
                       skiq_filt_t* p_path)
{
    int32_t status = 0;
    rfe_t rfe_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    *p_path = skiq_filt_invalid;
    rfe_instance = _skiq_get_rx_rfe_instance(card,hdl);

    LOCK_REGS(card);
    status=rfe_read_rx_filter_path( &(rfe_instance),
                                    p_path );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_filter_path() function is responsible for selecting either
    a lowband or highband RF path for the specified Tx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param path  an enum indicating which path is being requested
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_filter_path(uint8_t card, skiq_tx_hdl_t hdl, skiq_filt_t path)
{
    int32_t status = 0;
    rfe_t rfe_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfe_instance = _skiq_get_tx_rfe_instance(card,hdl);

    LOCK_REGS(card);
    status=rfe_write_tx_filter_path( &(rfe_instance),
                                     path );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_filter_path() function is responsible for returning the
    currently selected RF path on the specified Tx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_path  a pointer to where the current value of the filter path
    should be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_filter_path(uint8_t card, skiq_tx_hdl_t hdl, skiq_filt_t* p_path)
{
    int32_t status = 0;
    rfe_t rfe_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    *p_path = skiq_filt_invalid;
    rfe_instance = _skiq_get_tx_rfe_instance(card,hdl);

    LOCK_REGS(card);
    *p_path = skiq_filt_invalid;
    status=rfe_read_tx_filter_path( &(rfe_instance),
                                    p_path );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_overload_state() function is responsible for reporting the
    overload state of the specified Rx interface.  An overload condition is
    detected when an RF input in excess of 0dBm is detected.  If an overload
    condition is detected, the state is 1, otherwise it is 0.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param p_overload a pointer to where to store the overload state.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_overload_state( uint8_t card, skiq_rx_hdl_t hdl, uint8_t *p_overload )
{
    int32_t status=0;
    uint32_t data=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status=sidekiq_fpga_reg_read(card, FPGA_REG_READ_RF_BAND_SEL, &data);
    if( hdl == skiq_rx_hdl_A1 )
    {
        *p_overload = BF_GET(data, RX_CHAN_1_OVERLOAD_OFFSET, RX_CHAN_1_OVERLOAD_LEN);
    }
    else if( hdl == skiq_rx_hdl_A2 )
    {
        *p_overload = BF_GET(data, RX_CHAN_2_OVERLOAD_OFFSET, RX_CHAN_2_OVERLOAD_LEN);
    }
    else
    {
        status=-1;
    }
    UNLOCK_REGS(card);

    return( status );
}

/*****************************************************************************/
/** The skiq_read_rx_LO_freq() function reads the current setting for the LO
    frequency of the requested rx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param p_freq  a pointer to the variable that should be updated with
    the programmed frequency (in Hertz)
    @param p_actual_freq  a pointer to the variable that should be updated with
    the actual tuned frequency (in Hertz)

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid RX handle specified
    @retval -ENODATA RX LO frequency has not yet been configured
*/
EPIQ_API int32_t skiq_read_rx_LO_freq(uint8_t card, skiq_rx_hdl_t hdl, uint64_t* p_freq, \
                 double* p_actual_freq )
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS(card);
    status = tune_read_rx_LO_freq(card, hdl, p_freq, p_actual_freq);
    UNLOCK_REGS(card);

    return( status );
}

/*****************************************************************************/
/** The skiq_write_rx_LO_freq() function writes the current setting for the LO
    frequency of the requested rx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param freq  the new value for the LO freq (in Hertz)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_LO_freq(uint8_t card, skiq_rx_hdl_t hdl, uint64_t freq)
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS(card);
    {
        bool pause_streams_during_retune = false;

        /* If the card is configured to deliver packed receive samples, the streams must be paused
         * during a frequency change or they run the risk of becoming misaligned (see LIB-35:
         * Samples unaligned after changing LO frequency in packed mode) */
        if ( ( skiq.card[card].rx_list[hdl].tune_mode == skiq_freq_tune_mode_standard ) &&
             ( skiq.card[card].iq_packed == true ) )
        {
            pause_streams_during_retune = true;
        }

        if ( pause_streams_during_retune )
        {
            LOCK_RX(card);
            pause_all_rx_streams(card);
        }

        status = tune_write_rx_LO_freq(card, hdl, freq, skiq.card[card].rx_list[hdl].tune_mode, 0);

        if ( pause_streams_during_retune )
        {
            resume_and_flush_all_rx_streams(card);
            UNLOCK_RX(card);
        }
    }
    UNLOCK_REGS(card);

    return(status);
}

/*****************************************************************************/
/** The skiq_read_rx_sample_rate() function reads the current setting for the
    rate of receive samples being transferred into the FPGA.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param p_rate  a pointer to the variable that should be updated with
    the current sample rate setting (in Hertz) currently set for the specified
    interface
    @param p_actual_rate  a pointer to the variable that should be updated with
    the actual rate of received samples being transferred into the FPGA
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_sample_rate(uint8_t card, skiq_rx_hdl_t hdl, uint32_t *p_rate,
                 double* p_actual_rate)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );
    rfic_read_rx_sample_rate( &rfic_instance, p_rate, p_actual_rate );

    return(status);
}

/*****************************************************************************/
/** The skiq_write_rx_sample_rate_and_bandwidth() function writes the current
    setting for the rate of receive samples being transferred into the FPGA.
    Additionally, the channel bandwidth is also configured.  Note: Rx/Tx
    sample rates are derived from the same clock so modifications to the Rx
    sample rate will also update the Tx sample rate to the same value.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param rate  the new value of the sample rate (in Hertz)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_sample_rate_and_bandwidth(uint8_t card,
                        skiq_rx_hdl_t hdl,
                        uint32_t rate,
                        uint32_t bandwidth)
{
    int32_t status=0;
    rfic_t rfic_instance;
    uint32_t curr_sample_rate;
    double actual_rate;
    uint32_t curr_bandwidth;
    uint32_t actual_bandwidth;
    skiq_part_t part;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    /* clamp the bandwidth to the rate at most */
    bandwidth = MIN( bandwidth, rate );
    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    part = _skiq_get_part(card);

    if( (skiq.card[card].chan_mode == skiq_chan_mode_dual) &&
        ( (part == skiq_mpcie) || (part == skiq_z3u) ) )
    {
        uint32_t max_dual_sample_rate=0;

        if( part == skiq_mpcie )
        {
            max_dual_sample_rate = SKIQ_MAX_DUAL_CHAN_MPCIE_SAMPLE_RATE;
        }
        else if( part == skiq_z3u )
        {
            max_dual_sample_rate = SKIQ_MAX_DUAL_CHAN_Z3U_SAMPLE_RATE;
        }
        else
        {
            skiq_error("Unexpected part %s in special dual chan mode handling", part_cstr(part));
            return (-EPROTO);
        }


        if( rate > max_dual_sample_rate )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "Dual channel sample rate for %s maximum is %u Hz\n",
                      part_cstr(part),
                      max_dual_sample_rate);
            return (-EINVAL);
        }
    }

    LOCK_REGS(card);
    LOCK_RX(card);
    // save off the config to restore if there's an error with the LO config
    rfic_read_rx_sample_rate( &rfic_instance, &curr_sample_rate, &actual_rate );
    rfic_read_rx_chan_bandwidth( &rfic_instance, &curr_bandwidth, &actual_bandwidth );
    pause_all_rx_streams(card);
    status = rfic_write_rx_sample_rate_and_bandwidth( &rfic_instance, rate, bandwidth );

    if( 0 == status )
    {
        // will return error if sample rate is unsupported/incompatible with the
        // current LO configuration
        status = tune_notify_rx_sample_rate_and_bandwidth(card,
                                                          hdl,
                                                          rate,
                                                          bandwidth);
        // restore the sample rate/bandwidth setting if the tune notify indicated a failure
        if( status != 0 )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "Invalid sample rate request with current LO frequency, "
                      "restoring previous configuration\n");
            if( rfic_write_rx_sample_rate_and_bandwidth( &rfic_instance,
                                                         curr_sample_rate,
                                                         curr_bandwidth ) != 0 )
            {
                _skiq_log(SKIQ_LOG_ERROR,
                          "Unable to restore sample rate and bandwidth\n");
            }
        }
    }
    else if (-EPERM == status)
    {
        skiq_error("Unable to write rx_sample_rate_and_bandwidth for %s on card %" PRIu8 "." \
            "LO source cannot be changed while streaming\n", rx_hdl_cstr( hdl ), card);
    }
    resume_and_flush_all_rx_streams(card);

    /* refresh cached system timestamp frequency value whenever RX sample rate changes */
    if ( 0 == status )
    {
        status = fpga_ctrl_read_sys_timestamp_freq(card, &skiq.card[card].fpga_params.sys_timestamp_freq);
    }

    UNLOCK_RX(card);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_sample_rate_and_bandwidth() function reads the current
    setting for the rate of receive samples being transferred into the FPGA as
    well as the configured channel bandwidth.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param p_rate  a pointer to the variable that should be updated with
    the current sample rate setting (in Hertz) currently set for the specified
    interface
    @param p_actual_rate  a pointer to the variable that should be updated with
    the actual rate of received samples being transferred into the FPGA
    @param p_bandwidth a pointer to the variable that is updated with the current
    channel bandwidth setting (in Hertz)
    @param p_actual_bandwidth a pointer to the variable that is updated with the
    actual channel bandwidth configured (in Hertz)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_sample_rate_and_bandwidth(uint8_t card,
                           skiq_rx_hdl_t hdl,
                           uint32_t *p_rate,
                           double *p_actual_rate,
                           uint32_t *p_bandwidth,
                           uint32_t *p_actual_bandwidth)
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    LOCK_REGS(card);
    if( (status=rfic_read_rx_sample_rate(&rfic_instance, p_rate, p_actual_rate)) == 0 )
    {
        status = rfic_read_rx_chan_bandwidth( &rfic_instance, p_bandwidth, p_actual_bandwidth );
    }
    UNLOCK_REGS(card);

    return( status );
}

/*****************************************************************************/
/** The skiq_write_rx_sample_rate_and_bandwidth_multi() function allows users
    to configure the sample rate and bandwith for multiple receive handles.
*/
EPIQ_API int32_t skiq_write_rx_sample_rate_and_bandwidth_multi(uint8_t card,
                                                               skiq_rx_hdl_t handles[],
                                                               uint8_t nr_handles,
                                                               uint32_t rate[],
                                                               uint32_t bandwidth[])
{
    int32_t status=0;
    rfic_t rfic_instance;
    uint32_t curr_sample_rate[skiq_rx_hdl_end] = {0};
    double actual_rate[skiq_rx_hdl_end] = {0.0};
    uint32_t curr_bandwidth[skiq_rx_hdl_end] = {0};
    uint32_t actual_bandwidth[skiq_rx_hdl_end] = {0};

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ((nr_handles > skiq_rx_hdl_end) || (nr_handles == 0))
    {
        skiq_error("Invalid number of handles (%d) specified for card %u\n", nr_handles, card);
        status = -EINVAL;
    }

    for(uint8_t i=0; (i<nr_handles) && (status == 0);i++)
    {
        CHECK_VALID_RX_HDL( card, handles[i] );

        /* clamp the bandwidth to the rate at most */
        bandwidth[i] = MIN( bandwidth[i], rate[i] );

        rfic_instance = _skiq_get_rx_rfic_instance( card, handles[i] );
        // save off the config to restore if there's an error with the LO config
        status = rfic_read_rx_sample_rate( &rfic_instance, &curr_sample_rate[handles[i]], &actual_rate[handles[i]] );
        if (status == 0)
        {
            status = rfic_read_rx_chan_bandwidth( &rfic_instance, &curr_bandwidth[handles[i]], &actual_bandwidth[handles[i]] );
        }
    }


    LOCK_REGS(card);
    LOCK_RX(card);

    if (status == 0)
    {
        pause_all_rx_streams(card);
        status = rfic_write_rx_sample_rate_and_bandwidth_multi( &rfic_instance, handles,
                                                                                nr_handles,
                                                                                rate,
                                                                                bandwidth);
    }

    for(uint8_t i=0; (i<nr_handles) && (status == 0);i++)
    {
        // will return error if sample rate is unsupported/incompatible with the
        // current LO configuration
        status = tune_notify_rx_sample_rate_and_bandwidth(card,
                                                        handles[i],
                                                        rate[i],
                                                        bandwidth[i]);
    }

    if (status == 0)
    {
        resume_and_flush_all_rx_streams(card);
        /* refresh cached system timestamp frequency value whenever RX sample rate changes */
        status = fpga_ctrl_read_sys_timestamp_freq(card, &skiq.card[card].fpga_params.sys_timestamp_freq);
    }

    // restore the sample rate/bandwidth setting if the tune notify indicated a failure
    if( status != 0 )
    {
        skiq_error("Failed to configure the rate/bandwidth specified for card %u. "
                   "Restoring previous configuration.\n", card);

        rfic_instance = _skiq_get_rx_rfic_instance( card, handles[0] );
        if( rfic_write_rx_sample_rate_and_bandwidth_multi( &rfic_instance,
                                                    &handles[0],
                                                    nr_handles,
                                                    &curr_sample_rate[0],
                                                    &curr_bandwidth[0] ) != 0 )
        {
            skiq_error("Unable to restore sample rate and bandwidth for card %u\n", card);
        }
    }

    UNLOCK_RX(card);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_sample_rate_and_bandwidth() function writes the current
    setting for the rate of transmit samples being transferred from the FPGA.
    Additionally, the channel bandwidth is also configured.  Note: Rx/Tx
    sample rates are derived from the same clock so modifications to the Tx
    sample rate will also update the Rx sample rate to the same value.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param rate  the new value of the sample rate (in Hertz)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_sample_rate_and_bandwidth(uint8_t card,
                                                         skiq_tx_hdl_t hdl,
                                                         uint32_t rate,
                                                         uint32_t bandwidth)
{
    int32_t status=0;
    rfic_t rfic_instance;
    uint32_t curr_sample_rate;
    double actual_rate;
    uint32_t curr_bandwidth;
    uint32_t actual_bandwidth;
    skiq_part_t part;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    part = _skiq_get_part(card);

    /* clamp the bandwidth to the rate at most */
    bandwidth = MIN( bandwidth, rate );
    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );
    if( (skiq.card[card].chan_mode == skiq_chan_mode_dual) &&
        ( (part == skiq_mpcie) || (part == skiq_z3u) ) )
    {
        uint32_t max_dual_sample_rate=0;

        if( part == skiq_mpcie )
        {
            max_dual_sample_rate = SKIQ_MAX_DUAL_CHAN_MPCIE_SAMPLE_RATE;
        }
        else if( part == skiq_z3u )
        {
            max_dual_sample_rate = SKIQ_MAX_DUAL_CHAN_Z3U_SAMPLE_RATE;
        }
        else
        {
            skiq_error("Unexpected part %s in special dual chan mode handling (card=%u)", part_cstr(part), card);
            return (-EPROTO);
        }


        if( rate > max_dual_sample_rate )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "Dual channel sample rate for %s maximum is %u\n",
                      part_cstr(part),
                      max_dual_sample_rate);
            return (-EINVAL);
        }
    }

    LOCK_REGS(card);
    LOCK_TX(card);
    
    LOCK_RX(card);
    // we need to pause and prep to flush the RX stream with the rate changing
    pause_all_rx_streams(card);

    // read back the current rate so we can revert in case of failure
    rfic_read_tx_sample_rate( &rfic_instance,
                              &curr_sample_rate,
                              &actual_rate );

    rfic_read_tx_chan_bandwidth( &rfic_instance,
                                 &curr_bandwidth,
                                 &actual_bandwidth );
    
    status = rfic_write_tx_sample_rate_and_bandwidth( &rfic_instance, 
                                                      rate,
                                                      bandwidth );
    
    if( 0 == status )
    {
        // will return error if sample rate is unsupported/incompatible with the
        // current LO configuration
        status = tune_notify_tx_sample_rate_and_bandwidth( card,
                                                           hdl,
                                                           rate,
                                                           bandwidth );
        if( status != 0 )
        {
            // restore the sample rate/bandwidth setting to previous
            _skiq_log(SKIQ_LOG_ERROR,
                      "Invalid sample rate request with current LO frequency, "
                      "restoring previous configuration\n");
            if( rfic_write_tx_sample_rate_and_bandwidth( &rfic_instance,
                                                         curr_sample_rate,
                                                         curr_bandwidth ) != 0 )
            {
                _skiq_log(SKIQ_LOG_ERROR,
                          "Unable to restore sample rate and bandwidth\n");
            }
        }
    }

    resume_and_flush_all_rx_streams(card);

    /* refresh cached system timestamp frequency value whenever TX sample rate changes */
    if ( 0 == status )
    {
        status = fpga_ctrl_read_sys_timestamp_freq(card, &skiq.card[card].fpga_params.sys_timestamp_freq);
    }

    UNLOCK_RX(card);

    UNLOCK_TX(card);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_tx_sample_rate_and_bandwidth() function reads the current
    setting for the rate of transmit samples being transferred from the FPGA as
    well as the configured channel bandwidth.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested rx interface
    @param p_rate  a pointer to the variable that should be updated with
    the current sample rate setting (in Hertz) currently set for the specified
    interface
    @param p_actual_rate  a pointer to the variable that should be updated with
    the actual rate of received samples being transferred into the FPGA
    @param p_bandwidth a pointer to the variable that is updated with the current
    channel bandwidth setting (in Hertz)
    @param p_actual_bandwidth a pointer to the variable that is updated with the
    actual channel bandwidth configured (in Hertz)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_sample_rate_and_bandwidth(uint8_t card,
                                               skiq_tx_hdl_t hdl,
                                               uint32_t *p_rate,
                                               double *p_actual_rate,
                                               uint32_t *p_bandwidth,
                                               uint32_t *p_actual_bandwidth)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    LOCK_REGS(card);
    if( (status=rfic_read_tx_sample_rate(&rfic_instance, p_rate, p_actual_rate)) == 0 )
    {
        status = rfic_read_tx_chan_bandwidth( &rfic_instance, p_bandwidth, p_actual_bandwidth );
    }
    UNLOCK_REGS(card);

    return (status);
}


/*****************************************************************************/
/** The skiq_set_rx_transfer_timeout() function is responsible for updating the
    current receive transfer timeout for the provided card.  The currently
    permitted range of timeout is RX_TRANSFER_WAIT_FOREVER, RX_TRANSFER_NO_WAIT,
    or a value between 20 and 1000000.

    @param[in] card card index of the Sidekiq of interest
    @param[in] timeout_us minimum timeout in microseconds for a blocking skiq_receive.
    can be RX_TRANSFER_WAIT_FOREVER, RX_TRANSFER_NO_WAIT, or 20-1000000.
    @return int32_t  status where 0=success, anything else is an error.
*/
EPIQ_API int32_t skiq_set_rx_transfer_timeout( const uint8_t card,
                              const int32_t timeout_us )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ( ( RX_TRANSFER_WAIT_FOREVER == timeout_us ) ||
         ( RX_TRANSFER_NO_WAIT == timeout_us ) ||
         ( ( 20 <= timeout_us ) && ( timeout_us <= 1000000 ) ) )
    {
        LOCK_REGS(card);

        /* intentionally do not lock RX here since we want to be able to update
         * timeout when another process is blocked in skiq_receive. */
        status = skiq_xport_rx_set_transfer_timeout( card, timeout_us );

        UNLOCK_REGS(card);
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "timeout_us value (%d) is invalid.\n", timeout_us);
        errno = EINVAL;
        return -1;
    }

    return status;
}


/*****************************************************************************/
/** The skiq_get_rx_transfer_timeout() function returns the currently configured
    receive transfer timeout.  If the return code indicates success, the
    p_timeout_us is guaranteed to be RX_TRANSFER_NO_WAIT,
    RX_TRANSFER_WAIT_FOREVER, or 20-1000000.

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_timeout_us reference to an int32_t to populate
    @return int32_t  status where 0=success, anything else is an error.
*/
EPIQ_API int32_t skiq_get_rx_transfer_timeout( const uint8_t card,
                              int32_t *p_timeout_us )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if( NULL == p_timeout_us )
    {
        _skiq_log(SKIQ_LOG_ERROR, "NULL pointer passed to %s\n", __FUNCTION__);
        return -EINVAL;
    }

    status = skiq_xport_rx_get_transfer_timeout( card, p_timeout_us );

    return status;
}

/*************************************************************************************************/
/** The skiq_receive() function is responsible for receiving a contiguous block of data from the
    FPGA.  The type of data being returned is specified in the metadata, but is typically
    timestamped I/Q samples.  One contiguous block of data will be returned each time this function
    is called.

    @warning The Rx interface from which the data was received is specified in the p_hdl parameter.
    This is needed because the underlying driver may have multiple Rx interfaces streaming
    simultaneously, and these data streams will be interleaved by the hardware.

    @attention The format of the data returned by the receive call is specified by the
    ::skiq_rx_block_t structure.

    @ingroup rxfunctions
    @see skiq_start_rx_streaming
    @see skiq_start_rx_streaming_on_1pps
    @see skiq_stop_rx_streaming
    @see skiq_stop_rx_streaming_on_1pps
    @see skiq_rx_block_t

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_hdl [::skiq_rx_hdl_t]  a pointer to the Rx handle that will be updated by
    libsidekiq to specify the handle associated with the received data
    @param[out] pp_block [::skiq_rx_block_t]  a reference to a receive block reference
    @param[out] p_data_len  a pointer to be filled in with the # of bytes
    are returned as part of the transfer
    @return ::skiq_rx_status_t  status of the receive call
*/
EPIQ_API skiq_rx_status_t skiq_receive( uint8_t card,
                                        skiq_rx_hdl_t* p_hdl,
                                        skiq_rx_block_t** pp_block,
                                        uint32_t* p_data_len )
{
    TRACE();

    skiq_rx_status_t status = 0;

    CHECK_CARD_ACTIVE(card);

    LOCK_RX(card);
    {
        if ( !is_rx_streaming( card ) )
        {
            status = skiq_rx_status_error_not_streaming;
        }
        else
        {
            status = (skiq_rx_status_t)(skiq_xport_rx_receive( card, (uint8_t**)pp_block,
                                                               p_data_len ));
        }

        /* When the transport layer indicates an overrun, it may choose to flush and/or reset its
         * handling of the sample blocks, but it does not have the ability to re-align sample blocks
         * with respect to one another if the user had requested a timestamp synchronized alignment.
         * When all of the Rx streams are paused, then resumed (and flushed), the re-alignment is
         * performed here at the core, regardless of what the transport layer did. */
        if ( status == skiq_rx_status_error_overrun )
        {
            pause_all_rx_streams(card);
            resume_and_flush_all_rx_streams(card);
        }
    }
    UNLOCK_RX(card);

    /* if there's valid data, we need to grab the handle from the meta */
    if( status == skiq_rx_status_success )
    {
#if BYTE_ORDER == BIG_ENDIAN
        /* adjust for endianness on BIG_ENDIAN platforms in the timestamps and metadata on a 64-bit
         * boundary */
        (*pp_block)->rf_timestamp =  le64toh((*pp_block)->rf_timestamp);
        (*pp_block)->sys_timestamp = le64toh((*pp_block)->sys_timestamp);
        (*pp_block)->__raw_meta =    le64toh((*pp_block)->__raw_meta);
#endif

        /* translate from a v0 packet to a v1 packet */
        if ( (*pp_block)->version == 0 )
        {
            union rx_packet_v0
            {
                volatile uint64_t __raw_meta;
                struct
                {
                    /* the structure is defined bitwise starting with the LSb */
                    volatile uint64_t hdl:2;
                    volatile uint64_t overload:1;
                    volatile uint64_t rfic_control:8;
                    volatile uint64_t system_meta:21;
                    volatile uint64_t user_meta:32;
                };
            } meta_v0;

            meta_v0.__raw_meta = (*pp_block)->__raw_meta;
            (*pp_block)->hdl = meta_v0.hdl;
            (*pp_block)->overload = meta_v0.overload;
            (*pp_block)->rfic_control = meta_v0.rfic_control;
            (*pp_block)->version = 1;
        }
        else if ( (*pp_block)->version != 1 )
        {
            /* @todo should received packet versions other than 0 and 1 emit error messages here? */
        }

        *p_hdl = (*pp_block)->hdl;
        if( skiq.card[card].iq_packed == true )
        {
            *p_data_len = (*p_data_len) - 4; // subtract off 1 word
        }
    }

    return( status );
}

/*****************************************************************************/
/** The skiq_transmit() function is responsible for writing a block of I/Q
    samples to transmit.  When running in synchronous mode, this function will
    block until the FPGA has queued the samples to send.  If running in
    asynchronous mode, the function will return immediately.  If the packet has
    successfully been buffered for transfer, a 0 will be returned.  If there is
    not enough room left in the buffer, @ref SKIQ_TX_ASYNC_SEND_QUEUE_FULL is
    returned.

    The first @ref SKIQ_TX_HEADER_SIZE_IN_WORDS contain metadata associated with
    transmit packet.  Included in the metadata is the desired timestamp to send
    the samples.  If running in @ref skiq_tx_immediate_data_flow_mode the
    timestamp is ignored and the data is sent immediately. Following the
    metadata is the block_size (in words) of sample data.  The number of words
    contained in p_samples should match the previously configured Tx block size
    plus the header size.

    The format of the data provided to the transmit call
    <pre>
                   -31-------------------------------------------------------0-
          word 0   |                                                          |
                   |                      META0 (misc)                        |
          word 1   |                                                          |
                   -31-------------------------------------------------------0-
          word 2   |                                                          |
                   |                    RF TIMESTAMP                          |
          word 3   |                                                          |
                   -31-------------------------------------------------------0-
                   |         12-bit I0_A1        |       12-bit Q0_A1         |
 n |-     word 4   | (sign extended to 16 bits   | (sign extended to 16 bits) |
 u |               ------------------------------------------------------------
 m |               |         12-bit I1_A1        |       12-bit Q1_A1         |
 _ |      word 5   | (sign extended to 16 bits   | (sign extended to 16 bits) |
 b |               ------------------------------------------------------------
 l |               |           ...               |          ...               |
 o |               ------------------------------------------------------------
 c |      word     |   12-bit Iblock_size_A1     |   12-bit Ablock_size_A1    |
 k |       3 +     | (sign extended to 16 bits   | (sign extended to 16 bits) |
 s |-  block_size  ------------------------------------------------------------

    </pre>

    @since Function signature modified v4.0.0 to take @ref skiq_tx_block_t
    instead of int32_t pointer for transmit data and a new void pointer argument
    for user data to be passed back into the callback function if the transmit
    transfer mode is @ref skiq_tx_transfer_mode_async.

    @note If the user does not need user data or the transmit transfer mode is
    @ref skiq_tx_transfer_mode_sync, pass @ref NULL as p_user.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl the handle of the desired interface
    @param[in] p_block a reference to a transmit block
    @param[in] p_user a pointer to user data that is passed back into the callback function if async
    @return int32_t status where 0=success, @ref SKIQ_TX_ASYNC_SEND_QUEUE_FULL
    indicates out of room to buffer if async, anything else is an error
*/
EPIQ_API int32_t skiq_transmit( uint8_t card,
                       skiq_tx_hdl_t hdl,
                       skiq_tx_block_t *p_block,
                       void *p_user )
{
    int32_t status=0;

    TRACE();

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_TX( card );
    status = skiq_xport_tx_transmit( card, hdl, (int32_t *)p_block, p_user );
    UNLOCK_TX( card );

    return( status );
}

/*****************************************************************************/
/** The skiq_read_rx_gain_index_range() function is responsible for obtaining
    the viable range of gain index values that can be used to call into the
    skiq_write_rx_gain() function. Note that the range provided is inclusive.
    
    @since Function added in API @b v4.2.0
    
    @param card card index of the Sidekiq of interest
    @param hdl the handle of the desired interface
    @param p_gain_index_min pointer to be updated with minimum index value
    @param p_gain_index_max pointer to be updated with maximum index value
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_gain_index_range(uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               uint8_t* p_gain_index_min,
                                               uint8_t* p_gain_index_max)
{
    int32_t status=0;
    rfic_t rfic_instance;
    
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    
    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_rx_gain_range(&(rfic_instance),
                                     p_gain_index_max,
                                     p_gain_index_min);
    UNLOCK_REGS(card);

    return (status);
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_write_rx_gain(uint8_t card,
                                    skiq_rx_hdl_t hdl,
                                    uint8_t gain_index)
{
    int32_t status=0;
    rfic_t rfic_instance;

    TRACE();

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    /* write down the Rx gain to the hardware */
    if ((status=rfic_write_rx_gain(&(rfic_instance), gain_index)) < 0)
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to write Rx gain with status %d\n",status);
    }
    else
    {
        skiq.card[card].rx_list[hdl].gain = gain_index;
    }
    UNLOCK_REGS(card);

    return (status);
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_read_rx_gain(uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint8_t* p_gain_index)
{
    int32_t status=0;
    rfic_t rfic_instance;

    TRACE();

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    /* write down the Rx gain to the hardware */
    if ((status=rfic_read_rx_gain(&(rfic_instance),p_gain_index)) < 0)
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to read Rx gain with status %d\n",status);
    }
    UNLOCK_REGS(card);

    return (status);
}


/*****************************************************************************/
/** The skiq_read_rx_gain_mode() function is responsible for reading the
    current gain mode being used by the Rx interface.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the requested Rx interface
    @param p_gain_mode a pointer to where the currently set Rx gain mode
    will be written.  Valid values are skiq_rx_gain_manual and
    skiq_rx_gain_auto.
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_gain_mode(uint8_t card, skiq_rx_hdl_t hdl, skiq_rx_gain_t* p_gain_mode)
{
    int32_t status = 0;
    rfic_t rfic_instance;

    TRACE();

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status=rfic_read_rx_gain_mode(&rfic_instance, p_gain_mode);
    UNLOCK_REGS(card);

    return(status);
}

/*****************************************************************************/
/** The skiq_write_rx_gain_mode() function is responsible for writing the
    current gain mode being used by the Rx interface.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the requested Rx interface
    @param gain_mode the requested Rx gain mode to be written.  Valid values
    are skiq_rx_manual_gain and skiq_rx_auto_gain.
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_gain_mode(uint8_t card, skiq_rx_hdl_t hdl, skiq_rx_gain_t gain_mode)
{
    int32_t status=0;
    rfic_t rfic_instance;

    TRACE();

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_write_rx_gain_mode( &(rfic_instance), gain_mode );
    if( status == 0 )
    {
        skiq.card[card].rx_list[hdl].gain_mode = gain_mode;
    }
    UNLOCK_REGS(card);

    return(status);
}

/*****************************************************************************/
/** The skiq_write_rx_attenuation_mode() function is responsible for writing the
    current attenuation mode being used by the Rx interface.

    @note This is only supported for Sidekiq X2.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the requested Rx interface
    @param mode the requested Rx attenuation mode to be written
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_attenuation_mode(uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               skiq_rx_attenuation_mode_t mode)
{
    int32_t status = 0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfe = _skiq_get_rx_rfe_instance(card, hdl);
    LOCK_REGS(card);
    status = rfe_write_rx_attenuation_mode(&rfe, mode);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_rx_attenuation_mode() function is responsible for reading the
    current attenuation mode being used by the Rx interface.

    @note This is only supported for Sidekiq X2.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the requested Rx interface
    @param p_mode pointer to be updated with the current Rx attenuation mode
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_attenuation_mode(uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               skiq_rx_attenuation_mode_t *p_mode)
{
    int32_t status = 0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfe = _skiq_get_rx_rfe_instance(card, hdl);    
    LOCK_REGS(card);
    status = rfe_read_rx_attenuation_mode(&rfe, p_mode);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_write_rx_attenuation() function is responsible for writing the
    Rx attenuation. Note that the Rx attenuation is applied to an external
    analog attunator before the Rx signal reaches the RFIC.

    @note This is only supported for Sidekiq X2.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the requested Rx interface
    @param atten the attenuation to be applied in dB
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_attenuation(uint8_t card,
                                           skiq_rx_hdl_t hdl,
                                           uint16_t attenuation)
{
    int32_t status = 0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfe = _skiq_get_rx_rfe_instance(card, hdl);
    LOCK_REGS(card);
    status = rfe_write_rx_attenuation(&rfe, attenuation);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_rx_attenuation() function is responsible for reading the
    current Rx attenuation. Note that the Rx attenuation is read from an
    external analog attunator before the Rx signal reaches the RFIC.

    @note This is only supported for Sidekiq X2.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the requested Rx interface
    @param p_atten pointer to be updated with the attenuation in dB
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_attenuation(uint8_t card,
                                          skiq_rx_hdl_t hdl,
                                          uint16_t *p_attenuation)
{
    int32_t status = 0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfe = _skiq_get_rx_rfe_instance(card, hdl);
    LOCK_REGS(card);
    status = rfe_read_rx_attenuation(&rfe, p_attenuation);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_tx_LO_freq() function reads the current setting for the LO
    frequency of the requested tx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_freq  a pointer to the variable that should be updated with
    the current frequency (in Hertz)
    @param p_actual_freq: a pointer to the variable that should be updated with
    the actual tuned frequency (in Hertz)

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM Invalid TX handle specified
    @retval -ENODATA TX LO frequency has not yet been configured
*/
EPIQ_API int32_t skiq_read_tx_LO_freq(uint8_t card,
                 skiq_tx_hdl_t hdl, uint64_t* p_freq,
                 double* p_tuned_freq )
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    status = tune_read_tx_LO_freq(card, hdl, p_freq, p_tuned_freq);
    UNLOCK_REGS(card);

    return(status);
}

/*****************************************************************************/
/** The skiq_write_tx_LO_freq() function writes the current setting for the LO
    frequency of the requested tx interface.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param freq  the new value for the LO freq (in Hertz)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_LO_freq(uint8_t card, skiq_tx_hdl_t hdl, uint64_t freq)
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    status = tune_write_tx_LO_freq(card, hdl, freq, skiq.card[card].tx_list[hdl].tune_mode, 0);
    UNLOCK_REGS(card);

    return(status);
}

/*****************************************************************************/
/** The skiq_enable_tx_tone() function configures the RF IC to send out a single
    cycle of a CW tone at the sample rate set for the interface.  Note: the RF
    IC is responsible generating the tone.  There is no reliance on the FPGA or
    software here.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_enable_tx_tone(uint8_t card, skiq_tx_hdl_t hdl)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);

    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    status = rfic_config_tx_test_tone( &rfic_instance,
                                       true );

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_disable_tx_tone() function disables the CW tone from being sent out
    when the transmitter is enabled.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_disable_tx_tone(uint8_t card, skiq_tx_hdl_t hdl)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    status = rfic_config_tx_test_tone( &rfic_instance,
                                       false );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_tone_freq() function returns the frequency of the TX test
    tone.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_freq pointer to where to store the frequency (in Hz) f the test tone
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_tone_freq(uint8_t card,
                                        skiq_tx_hdl_t hdl,
                                        uint64_t *p_freq)
{
    int32_t status=0;
    rfic_t rfic_instance;
    int32_t freq_offset=0;
    uint64_t freq = 0;
    double actual_freq = 0.0;
    
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    status = rfic_read_tx_test_tone_freq( &rfic_instance,
                                          &freq_offset );
    if( status == 0 )
    {
        status=tune_read_tx_LO_freq(card, hdl, &freq, &actual_freq);
        if( status == 0 )
        {
            *p_freq = ((uint64_t) actual_freq) + freq_offset;
        }
    }

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_tone_freq_offset() function returns the the TX test tone
    offset relative to the configured TX LO frequency.

    @since Function added in API @b v4.9.0

    @ingroup txfunctions
    @see skiq_enable_tx_tone
    @see skiq_disable_tx_tone
    @see skiq_read_tx_tone_freq
    @see skiq_write_tx_tone_freq_offset

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t]  the handle of the requested tx interface
    @param[out] p_freq_offset pointer to where to store the frequency (in Hz) offset

    @return int32_t  status where 0=success, anything else is an error

    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
*/
EPIQ_API int32_t skiq_read_tx_tone_freq_offset(uint8_t card,
                                               skiq_tx_hdl_t hdl,
                                               int32_t *p_freq_offset)
{
    int32_t status=0;
    rfic_t rfic_instance;
    
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    status = rfic_read_tx_test_tone_freq( &rfic_instance,
                                          p_freq_offset );

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_tone_freq_offset() function configures the frequency of 
    the TX test tone offset from the configured TX LO frequency.

    @since Function added in API @b v4.9.0
    @note This is not available for all products
    @note The frequency offset generally needs to fall within the +/- 0.5*sample_rate

    @ingroup txfunctions
    @see skiq_enable_tx_tone
    @see skiq_disable_tx_tone
    @see skiq_read_tx_tone_freq
    @see skiq_read_tx_tone_freq_offset

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t]  the handle of the requested tx interface
    @param[in] test_freq_offset test tone frequency (in Hz) offset

    @return int32_t  status where 0=success, anything else is an error

    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
*/
EPIQ_API int32_t skiq_write_tx_tone_freq_offset(uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                int32_t test_freq_offset)
{
    int32_t status=0;
    rfic_t rfic_instance;
    
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    status = rfic_write_tx_test_tone_freq( &rfic_instance,
                                           test_freq_offset );
    UNLOCK_REGS(card);

    return (status);
}


/*****************************************************************************/
/** The skiq_write_tx_attenuation() function configures the attenuation of the
    transmitter for the Tx handle specified.  The value of the attenuation is
    0.25 dB steps such that an attenuation value of 4 would equate to 1 dB of
    actual attenuation.  A value of 0 would provide result in 0 attenuation, or
    maximum transmit power.  Valid attenuation settings are
    0:SKIQ_MAX_TX_ATTENUATION.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param attenuation value of attenuation
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_attenuation(uint8_t card,
                                  skiq_tx_hdl_t hdl,
                                  uint16_t attenuation)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    /* grab the tx path based on the handle */
    LOCK_REGS(card);
    if( (status=rfic_write_tx_attenuation(&(rfic_instance), attenuation)) == 0 )
    {
        skiq.card[card].tx_list[hdl].attenuation = attenuation;
    }

    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_tx_attenuation() function reads the attenuation setting of the
    transmitter for the Tx handle specified.  The value of the attenuation is
    0.25 dB steps such that an attenuation value of 4 would equate to 1 dB of
    actual attenuation.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_attenuation pointer to where to store the attenuation read
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_attenuation(uint8_t card,
                                 skiq_tx_hdl_t hdl,
                                 uint16_t *p_attenuation)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    /* grab the tx path based on the handle */
    LOCK_REGS(card);
    *p_attenuation = skiq.card[card].tx_list[hdl].attenuation;
    UNLOCK_REGS(card);

    return status;
}


/*****************************************************************************/
/** The skiq_read_tx_sample_rate() function reads the current setting for the
    rate at which samples will be delivered from the FPGA to the RF front end
    for transmission.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_rate  a pointer to the variable that should be updated with
    the actualy sample rate (in Hertz) currently set for the D/A converter
    @param p_actual_rate  a pointer to the variable that should be updated with
    the actual sample rate (in Hertz) currently set
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_sample_rate(uint8_t card,
                 skiq_tx_hdl_t hdl, uint32_t *p_rate,
                 double* p_actual_rate)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );
    status = rfic_read_tx_sample_rate( &rfic_instance, p_rate, p_actual_rate );

    return(status);
}

/*****************************************************************************/
/** The skiq_read_tx_block_size() function reads the current setting for the
    block size of transmit packets.  Note: the block size is represented in
    words and does not include the header size, it accounts only for the number
    of samples.  The total Tx packet size includes both the header size and
    block size.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_block_size_in_words  a pointer to the variable that should be
    updated with current Tx block size
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_block_size(uint8_t card,
                                skiq_tx_hdl_t hdl,
                                uint16_t *p_block_size_in_words)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    *p_block_size_in_words = skiq.card[card].tx_list[hdl].block_size;

    return status;
}

/*****************************************************************************/
/** The skiq_write_tx_block_size() function configures the block size of
    transmit packets.  Note: the block size is represented in words and is the
    size (in words) of the IQ samples for each channel, not including the
    metadata.  When using packed mode, this is the number of words (not number
    of samples) in the payload, not including the metadata.  Also, while in
    packed mode, the value specified must result in an even number of samples
    included in a block.  For instance, a block size of 252 * 4/3 = 336 samples
    per block of data, which is a valid configuration.  A block size of 508 *
    4/3 - 677.3 samples per block would be invalid.  The validity of the
    configuration will not be confirmed until start streaming is called.


    This must be set prior to the Tx interface being started.  If set after
    the Tx interface has been started, the setting will be stored but won't
    be used until the interface is stopped and re-started.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param block_size_in_words number of words to configure the Tx block size
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_block_size(uint8_t card,
                                 skiq_tx_hdl_t hdl,
                                 uint16_t block_size_in_words)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );
    if( block_size_in_words > SKIQ_MAX_TX_PACKET_SIZE_IN_WORDS )
    {
        _skiq_log(SKIQ_LOG_ERROR, "block size is too large, max size is %u\n",
                SKIQ_MAX_TX_PACKET_SIZE_IN_WORDS);
        return (-5);
    }

    skiq.card[card].tx_list[hdl].block_size = block_size_in_words;

    return status;
}

/*****************************************************************************/
/** The skiq_read_tx_num_underruns() function reads the current number of Tx
    underruns observed by the FPGA.  This value is reset only when calling
    skiq_start_tx_streaming().  Note: this number is only valid if running with
    Tx data flow mode set to "immediate".

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested tx interface
    @param p_num_underrun  a pointer to the variable that is updated with
    the number of underruns observed since starting streaming
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_num_underruns(uint8_t card, skiq_tx_hdl_t hdl, uint32_t *p_num_underrun)
{
    int32_t status=0;
    skiq_tx_flow_mode_t flow_mode;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    flow_mode = skiq.card[card].tx_list[hdl].flow_mode;
    if( flow_mode != skiq_tx_immediate_data_flow_mode )
    {
        _skiq_log(SKIQ_LOG_ERROR, "invalid Tx mode\n");
        return -4;
    }

    LOCK_REGS(card);
    status=sidekiq_fpga_reg_read(card, FPGA_REG_TX_ERROR_COUNT, p_num_underrun);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_tx_num_late_timestamps() functions reads the current number of
    "late" Tx timestamps observed by the FPGA.  When the FPGA encounters a Tx
    timestamp that has occurred in the past, the FPGA Tx FIFO is flushed of all
    packets and a counter is incremented.  This function returns the count of
    how many times the FIFO was flushed due to a timestamp in the past.  The
    value is reset only after calling skiq_stop_tx_streaming().

    @warning    The late timestamp count value is only valid if running with Tx
    data flow mode set to ::skiq_tx_with_timestamps_data_flow_mode and not
    ::skiq_tx_immediate_data_flow_mode or
    ::skiq_tx_with_timestamps_allow_late_data_flow_mode.

    @attention  The late timestamp counter is not updated when in
    ::skiq_tx_write_timestamps_allow_late_data_flow_mode, even if the data is
    transmitted later than its timestamp.

    @param[in]  card        card index of the Sidekiq of interest
    @param[in]  hdl         the handle of the requested tx interface
    @param[out] p_num_late  a pointer to the variable that is updated with the
    number of times the FIFO is flushed due to a "late" timestamp
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_num_late_timestamps(uint8_t card, skiq_tx_hdl_t hdl,
                                         uint32_t *p_num_late)
{
    int32_t status=0;
    skiq_tx_flow_mode_t flow_mode;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    flow_mode = skiq.card[card].tx_list[hdl].flow_mode;
    if( (flow_mode != skiq_tx_with_timestamps_data_flow_mode) &&
        (flow_mode != skiq_tx_with_timestamps_allow_late_data_flow_mode) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "invalid Tx mode\n");
        return -4;
    }

    LOCK_REGS(card);
    status=sidekiq_fpga_reg_read(card, FPGA_REG_TX_ERROR_COUNT, p_num_late);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_temp() function is responsible for reading and providing
    the current temperature of the unit (in deg C).

    @param card card index of the Sidekiq of interest
    @param p_temp_in_deg_C-a pointer to where the current temp should
    be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_temp(uint8_t card, int8_t* p_temp_in_deg_C)
{
    int32_t status = -1;

    CHECK_CARD_RANGE( card );

    LOCK_REGS(card);
    status = card_read_temp( card, DEFAULT_TEMP_SENSOR_INDEX, p_temp_in_deg_C);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_is_accel_supported() function is responsible for determining if
    the accelerometer is supported on the hardware platform of the card specified.

    @param card card index of the Sidekiq of interest
    @param p_supported pointer to where to accelerometer support
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_is_accel_supported( uint8_t card, bool *p_supported )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );

    *p_supported = skiq.card[card].card_params.is_accelerometer_present;

    return (status);
}

/*************************************************************************************************/
/** The skiq_read_accel() function is responsible for reading and providing the accelerometer data.
    The data format is twos compliment and 16 bits.  If measurements are not available, -EAGAIN is
    returned and the accelerometer should be queried again for position.

    @since As of libsidekiq @b v4.7.2, for all supported products, this function will populate @a
    p_x_data, @a p_y_data, and @a p_z_data with measurements in units of thousandths of standard
    gravity (g<sub>0</sub>).

    @ingroup accelfunctions
    @see skiq_is_accel_supported
    @see skiq_read_accel_state
    @see skiq_read_accel_reg
    @see skiq_write_accel_state
    @see skiq_write_accel_reg

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_x_data a pointer to where the X-axis accelerometer measurement is written
    @param[out] p_y_data a pointer to where the Y-axis accelerometer measurement is written
    @param[out] p_z_data a pointer to where the Z-axis accelerometer measurement is written

    @return int32_t  status where 0=success, anything else is an error

    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -EAGAIN accelerometer measurement is not available
    @retval -EIO error communicating with the accelerometer
*/
EPIQ_API int32_t skiq_read_accel( uint8_t card,
                                  int16_t *p_x_data,
                                  int16_t *p_y_data,
                                  int16_t *p_z_data )
{
    int32_t status = -ENOTSUP;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    if ( skiq.card[card].accel_enabled )
    {
        status = card_read_accel( card, p_x_data, p_y_data, p_z_data );
    }
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_write_accel_state() function is responsible for enabling or
    disabling the accelerometer to take measurements.

    @param card card index of the Sidekiq of interest
    @param enabled-accelerometer state (1=enabled, 0=disabled)

    @return int32_t  status where 0=success, anything else is an error
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -EIO error communicating with the accelerometer
*/
EPIQ_API int32_t skiq_write_accel_state(uint8_t card, uint8_t enabled)
{
    int32_t status = -ENOTSUP;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ( skiq.card[card].card_params.is_accelerometer_present )
    {
        LOCK_REGS(card);
        status = card_write_accel_state(card, enabled);
        if ( 0 == status )
        {
            skiq.card[card].accel_enabled = (enabled == 1);
        }
        UNLOCK_REGS(card);
    }

    return status;
}

/*****************************************************************************/
/** The skiq_write_accel_reg() function provides generic write access to the
    onboard accelerometer, if supported.

    @attention Sidekiq products have different accelerometer ICs that each have a specific register
    map.  Please refer to the Hardware User Guide associated with the Sidekiq product to determine
    the part number of the accelerometer before reading / writing registers.

    @param card card index of the Sidekiq of interest
    @param reg regiser address to access
    @param p_data pointer to buffer of data to write
    @param len number of bytes to write

    @return int32_t  status where 0=success, anything else is an error
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -EIO error communicating with the accelerometer
*/
EPIQ_API int32_t skiq_write_accel_reg(uint8_t card,
                                      uint8_t reg,
                                      uint8_t* p_data,
                                      uint32_t len)
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status = card_write_accel_reg(card, reg, p_data, len);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_accel_reg() function provides generic read access to the
    onboard accelerometer, if supported.

    @attention Sidekiq products have different accelerometer ICs that each have a specific register
    map.  Please refer to the Hardware User Guide associated with the Sidekiq product to determine
    the part number of the accelerometer before reading / writing registers.

    @param card card index of the Sidekiq of interest
    @param reg regiser address to access
    @param p_data pointer to buffer to read data into
    @param len number of bytes to read

    @return int32_t  status where 0=success, anything else is an error
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -EIO error communicating with the accelerometer
*/
EPIQ_API int32_t skiq_read_accel_reg(uint8_t card,
                                     uint8_t reg,
                                     uint8_t* p_data,
                                     uint32_t len)
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status = card_read_accel_reg(card, reg, p_data, len);
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_accel_state() function is responsible for reading the current
    state of the accelerometer.

    @param card card index of the Sidekiq of interest
    @param p_enabled-pointer to where to store the accelerometer state
                    (1=enabled, 0=disabled)

    @return int32_t  status where 0=success, anything else is an error
    @retval -ERANGE specified card index is out of range
    @retval -ENODEV specified card has not been initialized
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
*/
EPIQ_API int32_t skiq_read_accel_state(uint8_t card, uint8_t *p_enabled)
{
    int32_t status = -ENOTSUP;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if( skiq.card[card].card_params.is_accelerometer_present )
    {
        *p_enabled = skiq.card[card].accel_enabled ? 1 : 0;
        status = 0;
    }

    return status;
}


/*****************************************************************************/
/** The skiq_write_tcvcxo_warp() function is responsible for setting a new
    warp value for the ref clock oscillator.  A 10-bit DAC is controlled
    by this function, where the DAC can generate voltage between 0.75 and 2.25V.
    Valid warp voltages for the ref clock oscillator are from 0.75 - 2.25V
    (which corresponds to DAC values between 0-1023).

    @param card card index of the Sidekiq of interest
    @param warp_voltage-a 16-bit value corresponding to the desired DAC
    voltage to be applied.  Valid values are between 0 and 1023.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tcvcxo_warp_voltage(uint8_t card, uint16_t warp_voltage)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance(card);

    LOCK_REGS(card);
    status=rfic_write_warp_voltage( &rfic_instance, warp_voltage );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_txcvxo_warp() function is responsible for returning the
    current value of the warp voltage.

    @param card card index of the Sidekiq of interest
    @param p_warp_voltage-a pointer to where the currently set warp voltage
    will be written.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tcvcxo_warp_voltage(uint8_t card, uint16_t* p_warp_voltage)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance(card);

    LOCK_REGS(card);
    status = rfic_read_warp_voltage( &rfic_instance, p_warp_voltage );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_default_txcvxo_warp_voltage() function is responsible for
    returning the default value of the warp voltage.  This default value is
    determined during factory calibration and is accessible to read only.
    If no factory calibrated value is available, an error is returned.  If no
    user value is defined, then this value is automatically loaded during
    skiq_init.

    @param card card index of the Sidekiq of interest
    @param p_warp_voltage-a pointer to where the currently set warp voltage
    will be written.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_default_tcvcxo_warp_voltage( uint8_t card,
                                               uint16_t *p_warp_voltage )
{
    int32_t status=0;

    LOCK_REGS(card);
    status = card_read_warp_voltage_calibration( card,
                                                 DEFAULT_WARP_VOLTAGE_OFFSET,
                                                 p_warp_voltage );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_user_txcvxo_warp_voltage() function is responsible for returning
    the user defined warp voltage value.  This value can be specified by the
    user and is automatically loaded during initialization.  This value takes
    presedence over the default value loaded by the factory.

    @param card card index of the Sidekiq of interest
    @param p_warp_voltage-a pointer to where the currently set warp voltage
    will be written.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_user_tcvcxo_warp_voltage( uint8_t card,
                                            uint16_t *p_warp_voltage )
{
    int32_t status=0;

    LOCK_REGS(card);
    status = card_read_warp_voltage_calibration( card,
                                                 DEFAULT_USER_WARP_VOLTAGE_OFFSET,
                                                 p_warp_voltage );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_user_txcvxo_warp_voltage() function is responsible for
    configuring the user defined warp voltage value.  This value can be
    specified by the user and is automatically loaded during initialization.
    This value takes presedence over the default value loaded by the factory.

    @param card card index of the Sidekiq of interest
    @param p_warp_voltage-a pointer to where the currently set warp voltage
    will be written.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_user_tcvcxo_warp_voltage( uint8_t card,
                                             uint16_t warp_voltage )
{
    int32_t status=0;

    LOCK_REGS(card);
    status = card_write_warp_voltage_calibration( card,
                                                  DEFAULT_USER_WARP_VOLTAGE_OFFSET,
                                                  warp_voltage );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_iq_pack_mode() function is responsible for setting
    whether or not the IQ samples being received/transmitted and to/from the FPGA
    to/from the CPU should be packed/compressed before being sent.  This allows
    four 12-bit complex I/Q samples to be transferred in three 32-bit words,
    increasing the efficiency of the channel.  An interface defaults to
    using un-packed mode if the skiq_write_iq_pack_mode() is not called.

    Note that this can be changed at any time, but updates are only honored
    whenever streaming is started.

    If the pack "mode" is set to false, the behavior is to have the I/Q
    sent up as twos-complement, sign-extended, little-endian, unpacked in the
    following format:

    <pre>
           -31-------------------------------------------------------0-
           |         12-bit I0           |       12-bit Q0            |
    word 0 | (sign extended to 16 bits   | (sign extended to 16 bits) |
           ------------------------------------------------------------
           |         12-bit I1           |       12-bit Q1            |
    word 1 | (sign extended to 16 bits   | (sign extended to 16 bits) |
           ------------------------------------------------------------
           |         12-bit I2           |       12-bit Q2            |
    word 2 |  (sign extended to 16 bits  | (sign extended to 16 bits) |
           ------------------------------------------------------------
           |           ...               |          ...               |
           ------------------------------------------------------------

   </pre>
    When the mode is set to true, then the 12-bit samples are packed in to
    make optimal use of the available bits, and packed as follows:
   <pre>
           -31-------------------------------------------------------0-
    word 0 |I0b11|...|I0b0|Q0b11|.................|Q0b0|I1b11|...|I1b4|
           ------------------------------------------------------------
    word 1 |I1b3|...|I1b0|Q1b11|...|Q1b0|I2b11|...|I2b0|Q2b11|...|Q2b8|
           -31-------------------------------------------------------0-
    word 2 |Q2b7|...|Q2b0|I3b11|.................|I3b0|Q1311|....|Q3b4|
           ------------------------------------------------------------
           |           ...               |          ...               |
           ------------------------------------------------------------
    </pre>
    (with the above sequence repeated every three words)

    Of course, once the packed I/Q samples are received up in the CPU,
    there are extra cycles needed to de-compress/un-pack them, which will
    likely impact actually processing throughput.  But for cases where an
    app simply needs to transfer a large block of contiguous I/Q samples
    up to the CPU for non-real time post processing, this will increase
    the bandwidth without sacrificing dynamic range.

    @warning I/Q pack mode conflicts with ::skiq_rx_stream_mode_low_latency.  As
    such, caller may not configure a card to use both packed I/Q mode and RX low
    latency mode at the same time.  This function will return an error (@c
    -EPERM) if mode=true and ::skiq_rx_stream_mode_low_latency is currently set.

    @param card card index of the Sidekiq of interest
    @param mode  false=use normal (non-packed) I/Q mode (default)
                 true=use packed I/Q mode
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_iq_pack_mode(uint8_t card, bool mode)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ( mode && ( skiq.card[card].rx_stream_mode == skiq_rx_stream_mode_low_latency ) )
    {
        status = -EPERM;
    }
    else if ( mode && !skiq.card[card].has_iq_packed_mode )
    {
        /* packed mode is not supported */
        status = -ENOTSUP;
    }
    else
    {
        skiq.card[card].iq_packed = mode;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_iq_pack_mode() function is responsible for
    retrieving the currently set value for the pack mode setting for
    the interface

    @param card card index of the Sidekiq of interest
    @param p_mode  the currently set value of the pack mode setting
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_iq_pack_mode(uint8_t card, bool* p_mode)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_mode = skiq.card[card].iq_packed;

    return (status);
}

/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/

EPIQ_API int32_t skiq_write_iq_order_mode(uint8_t card,
                                         skiq_iq_order_t mode)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    switch (mode)
    {
        case skiq_iq_order_qi:
            skiq.card[card].iq_order_mode = skiq_iq_order_qi; 
            break;
        case skiq_iq_order_iq:
            if ( _skiq_meets_fpga_version( card, 3, 12, 0 ) )
            {
                skiq.card[card].iq_order_mode = skiq_iq_order_iq; 
            }
            else
            {
                skiq.card[card].iq_order_mode = skiq_iq_order_qi; 
                status = -ENOTSUP;
            }
            break;
        default:
            status = -EINVAL;
            break;
    }

    return (status);
}                                         
/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/

EPIQ_API int32_t skiq_read_iq_order_mode(uint8_t card,
                                        skiq_iq_order_t* p_mode)
{
    int32_t status=0;
    
    CHECK_NULL_PARAM( p_mode );

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    
    LOCK_REGS(card);
    status = fpga_ctrl_read_iq_order_mode(card,p_mode);
    UNLOCK_REGS(card);

    return (status);

}                                        


/*****************************************************************************/
/** The skiq_write_rx_data_src() function is responsible for setting the
    data souce for the Rx interface.  This is typically complex I/Q samples,
    but can also be set to use an incrementing counter for various test
    purposes.  This must be set prior to the Rx interface being started.
    If set after the Rx interface has been started, the setting will be
    stored but won't be used until the interface is stopped and re-started.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested Rx interface
    @param src  the source of the data (either skiq_data_src_iq or
                skiq_data_src_counter)
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_data_src(uint8_t card, skiq_rx_hdl_t hdl, skiq_data_src_t src)
{
    int32_t status=0;

    if (src > skiq_data_src_counter)
    {
        _skiq_log(SKIQ_LOG_ERROR, "invalid Rx data source %d\n",src);
        return(-1);
    }

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    skiq.card[card].rx_list[hdl].data_src = src;

    return(status);
}

/*****************************************************************************/
/** The skiq_read_rx_data_src() function is responsible for retrieving the
    currently set data source value.

    @param card card index of the Sidekiq of interest
    @param hdl  the handle of the requested Rx interface
    @param p_src  the currently set value of the pack mode setting
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_data_src(uint8_t card, skiq_rx_hdl_t hdl, skiq_data_src_t* p_src)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    *p_src = skiq.card[card].rx_list[hdl].data_src;

    return(status);
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_write_rx_stream_mode( uint8_t card,
                                            skiq_rx_stream_mode_t stream_mode )
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    switch (stream_mode)
    {
        case skiq_rx_stream_mode_high_tput:
        case skiq_rx_stream_mode_balanced:
            /* always allow a high throughput and balanced receive stream mode setting, it's
             * available in all FPGA bitstreams */
            break;
        case skiq_rx_stream_mode_low_latency:
            /* low latency mode was introduced in FPGA v3.9.0 and is available
             * for all parts */
            if ( _skiq_meets_fpga_version( card, 3, 9, 0 ) )
            {
                if ( skiq.card[card].iq_packed )
                {
                    status = -EPERM;
                }
            }
            else
            {
                status = -ENOTSUP;
            }
            break;
        default:
            status = -EINVAL;
            break;
    }

    /* if the status is 0, all conditions are met to update the stream mode.
     * The actual configuration is applied when the receive handles start
     * streaming. */
    if ( status == 0 )
    {
        skiq.card[card].rx_stream_mode = stream_mode;
    }

    return (status);
}


/*****************************************************************************/
/** The skiq_read_rx_stream_mode() function is responsible for retrieving the
    currently stored receive stream mode (::skiq_rx_stream_mode_t).

    @attention The receive stream mode is only applied when receive streaming is
    started and thus may not reflect the current stream state.

    @since Function added in @b v4.6.0, requires FPGA @b v3.9.0 or later

    @ingroup rxfunctions
    @see skiq_write_rx_stream_mode
    @see skiq_rx_stream_mode_t

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_stream_mode [::skiq_rx_stream_mode_t]  the current value of the receive stream mode

    @return int32_t
    @retval 0 successful query of RX stream mode
    @retval -1 specified card index is out of range or has not been initialized
*/
EPIQ_API int32_t skiq_read_rx_stream_mode( uint8_t card,
                                           skiq_rx_stream_mode_t* p_stream_mode )
{
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_stream_mode = skiq.card[card].rx_stream_mode;

    return (0);
}


/**************************************************************************************************/
/** The skiq_read_curr_rx_timestamp() function is responsible for retrieving a current sample of the
    Rx timestamp counter (i.e., free-running counter) of the specified interface handle.  This
    timestamp is maintained by the FPGA and is shared across each RFIC regardless of the Rx or Tx
    interface.

    @note by the time the timestamp has been returned back to software, it will already be in the
    past, but this is still useful to determine if a specific timestamp has occurred already or not.

    @attention See @ref AD9361TimestampSlip for details on how calling this function can affect the
    RF timestamp metadata associated with received I/Q blocks.
*/
EPIQ_API int32_t skiq_read_curr_rx_timestamp(uint8_t card, skiq_rx_hdl_t hdl, uint64_t* p_timestamp)
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    CHECK_NULL_PARAM( p_timestamp );

    LOCK_REGS(card);
    {
        uint32_t addr_hi, addr_lo;

        switch (hdl)
        {
            case skiq_rx_hdl_A1:
            case skiq_rx_hdl_A2:
                addr_hi = FPGA_REG_TIMESTAMP_A_HIGH;
                addr_lo = FPGA_REG_TIMESTAMP_A_LOW;
                break;

            case skiq_rx_hdl_B1:
            case skiq_rx_hdl_B2:
                addr_hi = FPGA_REG_TIMESTAMP_B_HIGH;
                addr_lo = FPGA_REG_TIMESTAMP_B_LOW;
                break;

            case skiq_rx_hdl_C1:
                addr_hi = FPGA_REG_TIMESTAMP_C_HIGH;
                addr_lo = FPGA_REG_TIMESTAMP_C_LOW;
                break;

            case skiq_rx_hdl_D1:
                addr_hi = FPGA_REG_TIMESTAMP_D_HIGH;
                addr_lo = FPGA_REG_TIMESTAMP_D_LOW;
                break;

            default:
                status = -EDOM;
                break;
        }

        if ( status == 0 )
        {
            status = sidekiq_fpga_reg_read_64( card, addr_hi, addr_lo, p_timestamp );
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_curr_tx_timestamp() function is responsible for retrieving the
    currently set value for the timestamp (i.e., free-running counter) of the
    specified interface handle.  This timestamp is maintained by the FPGA and
    is shared across each RF IC regardless of the Rx or Tx interface.

    @note By the time the timestamp has been returned back to
    software, it will already be in the past, but this is still useful to
    determine if a specific timestamp has occurred already or not.
*/
EPIQ_API int32_t skiq_read_curr_tx_timestamp(uint8_t card, skiq_tx_hdl_t hdl, uint64_t* p_timestamp)
{
    int32_t status=-1;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    /* make sure the handle is valid */
    LOCK_REGS(card);
    status=sidekiq_fpga_reg_read_64(card,
                                    FPGA_REG_TIMESTAMP_A_HIGH,
                                    FPGA_REG_TIMESTAMP_A_LOW,
                                    p_timestamp);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_curr_sys_timestamp() function is responsible for retrieving the
    currently set value for the system timestamp.  The system timestamp increments
    at the SKIQ_SYS_TIMESTAMP_FREQ rate.  This timestamp is maintained by the
    FPGA and increments independent of the sample rate.

    Note: by the time the timestamp has been returned back to
    software, it will already be in the past, but this is still useful to
    determine if a specific timestamp has occurred already or not.

    @param p_timestamp  a pointer to where the 64-bit timestamp value
    should be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_curr_sys_timestamp(uint8_t card, uint64_t* p_timestamp)
{
    int32_t status=-1;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status=sidekiq_fpga_reg_read_64(card,
                                    FPGA_REG_SYS_TIMESTAMP_HIGH,
                                    FPGA_REG_SYS_TIMESTAMP_LOW,
                                    p_timestamp);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_reset_timestamp() function is responsible for resetting the
    timestamps (Rx/Tx and system) back to 0.

    @param card card index of the Sidekiq of interest
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_reset_timestamps(uint8_t card)
{
    int32_t status=-1;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = skiq_update_timestamps( card, 0 );

    return (status);
}

/*****************************************************************************/
/** The skiq_update_timestamps() function is responsible for updating the
    both the RF and system timestamps to the value specified.

    @param card card index of the Sidekiq of interest
    @param new_timestamp value to set both the RF and system timestamps to
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_update_timestamps(uint8_t card, uint64_t new_timestamp)
{
    int32_t status=0;
    uint32_t data=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    /* make sure to set the timestamp value to 0 */
    sidekiq_fpga_reg_write_64(card,
                              FPGA_REG_SET_ALL_TIMESTAMP_HIGH,
                              FPGA_REG_SET_ALL_TIMESTAMP_LOW,
                              new_timestamp);
    status=sidekiq_fpga_reg_read(card, FPGA_REG_TIMESTAMP_RST, &data);
    /* set the reset bit and write */
    /* unfortunately, this register space doesn't have a FIFO associated with it
       and it crosses clock domains, so we need to verify that the write happened
       before we do an additional write */
    BF_SET(data, 1, TIMESTAMP_RST_OFFSET, TIMESTAMP_RST_LEN);
    sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_TIMESTAMP_RST, data);
    /* clear the reset bit and write */
    BF_SET(data, 0, TIMESTAMP_RST_OFFSET, TIMESTAMP_RST_LEN);
    sidekiq_fpga_reg_write_and_verify(card, FPGA_REG_TIMESTAMP_RST, data);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_libsidekiq_version() function is responsible for returning the
    major/minor revision numbers for the version of libsidekiq used by
    the application.

    @param p_major  a pointer to where the major rev # should be written
    @param p_minor  a pointer to where the minor rev # should be written
    @param p_patch  a pointer to where the patch rev # should be written
    @param p_label  a pointer which will be set to point to a NULL-terminated string,
                    which is possibly the empty string ""
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_libsidekiq_version(uint8_t* p_major,
                                     uint8_t* p_minor,
                                     uint8_t* p_patch,
                                     const char **p_label)
{
    *p_major = LIBSIDEKIQ_VERSION_MAJOR;
    *p_minor = LIBSIDEKIQ_VERSION_MINOR;
    *p_patch = LIBSIDEKIQ_VERSION_PATCH;
    *p_label = LIBSIDEKIQ_VERSION_LABEL;

    return 0;
}

/*****************************************************************************/
/** The skiq_read_fpga_version() function is responsible for returning the
    major/minor revision numbers for the currently loaded FPGA bitstream.

    @deprecated Use skiq_read_fpga_semantic_version() and
    skiq_read_fpga_tx_fifo_size() instead of skiq_read_fpga_version()

    @param card card index of the Sidekiq of interest
    @param p_git_hash  a pointer to where the 32-bit git hash will be written
    @param p_build_date  a pointer to where the 32-bit build date will be
    written
    @param p_major a pointer to where the major rev # should be written
    @param p_minor a pointer to where the minor rev # should be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_fpga_version(uint8_t card,
                               uint32_t* p_git_hash,
                               uint32_t* p_build_date,
                               uint8_t *p_major,
                               uint8_t *p_minor,
                               skiq_fpga_tx_fifo_size_t *p_tx_fifo_size )
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = _check_fpga_state(card);

    if ( status == 0 )
    {
        *p_git_hash = skiq.card[card].fpga_params.git_hash;
        *p_build_date = skiq.card[card].fpga_params.build_date;
        *p_major = skiq.card[card].fpga_params.version_major;
        *p_minor = skiq.card[card].fpga_params.version_minor;
        *p_tx_fifo_size = skiq.card[card].fpga_params.tx_fifo_size;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_fpga_base_githash() function is responsible for returning the
    baseline githash for the delivered FPGA bitstream. This githash will remain
    unchanged after the product is delivered and will be a baseline for any
    FPGA changes that are made so that the end user will be able to revert to
    a known good FPGA bitstream if need be.

    @param card card index of the Sidekiq of interest
    @param p_git_hash  a pointer to where the 32-bit git hash will be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_fpga_base_githash(uint8_t card,
                               uint32_t* p_git_hash)
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_git_hash );

    status = _check_fpga_state(card);

    if ( status == 0 )
    {
        *p_git_hash = skiq.card[card].fpga_params.baseline_hash;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_fpga_semantic_version() function is responsible for returning the
    major/minor/patch revision numbers for the currently loaded FPGA bitstream.

    @since Function added in API @b 4.3.0

    @param card card index of the Sidekiq of interest
    @param p_major a pointer to where the major rev # should be returned
    @param p_minor a pointer to where the minor rev # should be returned
    @param p_patch a pointer to where the patch rev # should be returned
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_fpga_semantic_version( uint8_t card,
                                                  uint8_t *p_major,
                                                  uint8_t *p_minor,
                                                  uint8_t *p_patch )
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = _check_fpga_state(card);

    if ( status == 0 )
    {
        *p_major = skiq.card[card].fpga_params.version_major;
        *p_minor = skiq.card[card].fpga_params.version_minor;
        *p_patch = skiq.card[card].fpga_params.version_patch;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_fpga_tx_fifo_size() function is responsible for returning the
    Transmit FIFO size (enum @a skiq_fpga_tx_fifo_size_t representing the number
    of samples) for the currently loaded FPGA bitstream.

    @since Function added in API @b 4.3.0

    @param card card index of the Sidekiq of interest
    @param p_tx_fifo_size reference to where the TX FIFO size enum should be returned
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_fpga_tx_fifo_size( uint8_t card,
                                              skiq_fpga_tx_fifo_size_t *p_tx_fifo_size )
{
    int32_t status=0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = _check_fpga_state(card);

    if ( status == 0 )
    {
        *p_tx_fifo_size = skiq.card[card].fpga_params.tx_fifo_size;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_fw_version() function is responsible for returning the
    major/minor revision numbers for the microcontroller firmware
    within the Sidekiq unit.  Note: this is currently only supported if the
    USB interface has been initialized.

    @param card card index of the Sidekiq of interest
    @param p_major  a pointer to where the major rev # should be written
    @param p_minor  a pointer to where the minor rev # should be written
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_fw_version(uint8_t card, uint8_t* p_major, uint8_t* p_minor)
{
    int32_t result = 0;

    CHECK_CARD_ACTIVE( card );

    if( skiq.card[card].fw_params.is_present )
    {
        *p_major = skiq.card[card].fw_params.version_major;
        *p_minor = skiq.card[card].fw_params.version_minor;
    }
    else
    {
        result = -ENOTSUP;
        _skiq_log(SKIQ_LOG_INFO, "no firmware for current hardware\n");
    }

    return result;
}

/*****************************************************************************/
/** The skiq_read_hw_version() function is responsible for returning the
    hardware version number of the Sidekiq board.

    @param card card index of the Sidekiq of interest
    @param p_hw_version: a pointer to hold the hardware version
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_hw_version(uint8_t card, skiq_hw_vers_t* p_hw_version)
{
    int32_t result = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_hw_version = skiq.card[card].hardware_vers;

    return result;
}

/*****************************************************************************/
/** The skiq_read_product_version() function is responsible for returning the
    product version of the Sidekiq board.

    @param card card index of the Sidekiq of interest
    @param p_product: a pointer to hold the product version
    @return int32_t: status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_product_version(uint8_t card, skiq_product_t *p_product)
{
    int32_t result = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_product = skiq.card[card].product_vers;

    return result;
}

/*****************************************************************************/
/** The skiq_write_user_fpga_reg() function is used to update the 32-bit value
    of the requested user-definable FPGA register.  This function is useful
    when adding custom logic to the FPGA, which is controlled by software
    through this interface.

    @param card card index of the Sidekiq of interest
    @param addr the register address to access in the FPGA's memory map
    @param data the 32-bit value to be written to the requested FPGA reg
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_user_fpga_reg(uint8_t card, uint32_t addr, uint32_t data)
{
    int32_t status=0;

    if( ((addr < SKIQ_START_USER_FPGA_REG_ADDR) ||
         (addr > SKIQ_END_USER_FPGA_REG_ADDR)) &&
        ((addr != FPGA_REG_GPIO_TRISTATE) &&
         (addr != FPGA_REG_GPIO_WRITE)) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "write of requested FPGA reg at address 0x%08x is out of the user-definable range\n",addr);
        return (-1);
    }
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    if ( ( status = sidekiq_fpga_reg_write( card, addr, data ) ) < 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to write FPGA register at address 0x%08x...status is %d\n",addr,status);
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_user_fpga_reg() function is responsible for reading out the
    current value in a user-definable FPGA register.

    @param card card index of the Sidekiq of interest
    @param addr the register address to access in the FPGA's memory map
    @param p_data a pointer to a uint32_t to be updated with the current value
    of the requested FPGA reg
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_user_fpga_reg(uint8_t card, uint32_t addr, uint32_t* p_data)
{
    int32_t status=0;

    if( ((addr < SKIQ_START_USER_FPGA_REG_ADDR) ||
         (addr > SKIQ_END_USER_FPGA_REG_ADDR)) &&
        ((addr != FPGA_REG_GPIO_TRISTATE) &&
         (addr != FPGA_REG_GPIO_READ)))
    {
        _skiq_log(SKIQ_LOG_ERROR,"read requested of FPGA reg at address 0x%08x is out of the user-definable range\n",addr);
        return (-1);
    }
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    if ( ( status = sidekiq_fpga_reg_read(card, addr, p_data) ) < 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR,"failed to read FPGA register at address 0x%08x...status is %d\n",addr,status);
    }
    UNLOCK_REGS(card);

    return (status);
}

/*************************************************************************************************/
/** The skiq_write_and_verify_user_fpga_reg() function is used to update the 32-bit value of the
    requested user-definable FPGA register.  After the register has been written, this function
    verifies that reading the register returns the value previously written.  This is useful to
    ensure that an FPGA register contains the expected value.  This verification should be done in
    cases when performing a read immediately following the write since it is possible that the reads
    and writes could occur out-of-order, depending on the transport.  Additionally, this is useful
    to verify in the cases where the register clock is running at a slower rate, such as the sample
    rate clock.

    @ingroup fpgafunctions
    @see skiq_read_user_fpga_reg
    @see skiq_write_user_fpga_reg

    @since Function added in API @b v4.9.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] addr the register address to access in the FPGA's memory map
    @param[in] data the 32-bit value to be written to the requested FPGA reg

    @retval 0 successful write and verification of user FPGA register
    @retval -EINVAL specified card index is out of range
    @retval -EFAULT addr is outside of valid FPGA user address range
    @retval -ENODEV specified card index has not been initialized
    @retval -EIO data readback does not match what was written
*/
EPIQ_API int32_t skiq_write_and_verify_user_fpga_reg(uint8_t card,
                                                     uint32_t addr,
                                                     uint32_t data)
{
    int32_t status=0;

    if( ((addr < SKIQ_START_USER_FPGA_REG_ADDR) ||
         (addr > SKIQ_END_USER_FPGA_REG_ADDR)) &&
        ((addr != FPGA_REG_GPIO_TRISTATE) &&
         (addr != FPGA_REG_GPIO_WRITE)))
    {
        skiq_error("requested of FPGA reg at address 0x%08x is out of the user-definable range\n",addr);
        return (-EFAULT);
    }
    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status = sidekiq_fpga_reg_write_and_verify(card, addr, data);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_prog_rfic_from_file() function is responsible for pushing
    down a configuration file to the rfic to reconfigure it.  This allows
    libsidekiq-based apps to reconfigure the RF IC from a config file at
    run-time if needed.

    @param fp pointer to the already opened file to load to the RF IC
    @param card card index of the Sidekiq of interest
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_prog_rfic_from_file(FILE* fp, uint8_t card)
{
    int32_t status=0;
    rfic_t rfic_instance;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    
    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        skiq_error("Reprogramming RFIC from file using network interface not currently supported (card=%u)",
                   card);
        return (-ENODEV);
    }

    LOCK_REGS(card);

    // init all of the RFIC
    rfic_instance = _skiq_get_generic_rfic_instance(card);
    status = rfic_init_from_file( &rfic_instance, fp );

    /* make sure the channel mode is valid */
    if( status == 0 )
    {
        status = skiq_write_chan_mode(card, skiq.card[card].chan_mode);
    }

    /* refresh cached system timestamp frequency value whenever a custom RFIC profile is loaded */
    if ( 0 == status )
    {
        status = fpga_ctrl_read_sys_timestamp_freq(card, &skiq.card[card].fpga_params.sys_timestamp_freq);
    }

    UNLOCK_REGS(card);

    return (status);
}


/**************************************************************************************************/
/**
   @brief The skiq_prog_fpga_from_file() function is responsible for programming the FPGA with an
   already opened bitstream file. This allows libsidekiq-based apps to reprogram the FPGA at
   run-time if needed.
*/
EPIQ_API int32_t skiq_prog_fpga_from_file(uint8_t card, FILE* fp)
{
    int32_t status = 0;
    skiq_part_t part;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( fp );

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        skiq_error("Reprogramming FPGA from file using network interface not currently supported (card=%u)",
                   card);
        return (-ENODEV);
    }

    part = _skiq_get_part(card);

    /* programming the FPGA from file is outright forbidden with custom
     * transports, there's simply no OOB support */
    if ( (skiq_xport_type_custom == skiq_xport_card_get_type( card )) &&
           ( part != skiq_z2 ) &&
           ( part != skiq_m2_2280 ) &&
           ( part != skiq_z2p ) &&
           ( part != skiq_z3u ) &&
           ( part != skiq_nv100 ) )
    {
        skiq_warning("Programming FPGA from a file is not supported for cards with a custom "
                     "transport (card %" PRIu8 ")!", card);
        return -ENOTSUP;
    }
    else
    {
        /* These card types might have OOB FPGA programming, so let them pass down to a lower
         * layer */
        switch ( part )
        {
            case skiq_mpcie:
            case skiq_m2:
            case skiq_z2:
            case skiq_m2_2280:
            case skiq_z2p:
            case skiq_z3u:
            case skiq_nv100:
                /* allowed part types for on-the-fly FPGA reconfiguration */
                break;

            default:
                skiq_warning("Programming FPGA from a file is not supported for this card type"
                             " (card %" PRIu8 ")!", card);
                return -ENOTSUP;
        }
    }

    LOCK_REGS(card);

    status = _prepare_for_fpga_reload( card );

    if ( status == 0 )
    {
        card_mgr_fpga_programming_lock();
        {
            status = hal_prog_fpga_from_file(card, fp);
            if ( status != 0 )
            {
                /* a lot of different (some undocumented) error codes can be returned from
                 * hal_prog_fpga_from_file(), so squash them to -EIO to let the user that something
                 * happened with this function.  The extraneous skiq_log() messages should be enough
                 * to distinguish what really happened */
                status = -EIO;
            }
        }
        card_mgr_fpga_programming_unlock();
    }

    if( status == 0 )
    {
        status = _perform_post_fpga_reload_actions( card );
    }

    UNLOCK_REGS(card);

    return status;
}


/**************************************************************************************************/
/**
   @brief The skiq_prog_fpga_from_flash() function is responsible for programming the FPGA from the
   image previously stored in flash. This allows libsidekiq-based apps to reprogram the FPGA at
   run-time if needed.
*/
EPIQ_API int32_t skiq_prog_fpga_from_flash(uint8_t card)
{
    return skiq_prog_fpga_from_flash_slot( card,
                                           FLASH_CONFIG_DEFAULT_SLOT );

} /* skiq_prog_fpga_from_flash */


/**************************************************************************************************/
/**
   @brief The skiq_save_fpga_config_to_flash() function stores a FPGA bitstream into flash memory,
   allowing it to be automatically loaded on power cycle or calling skiq_prog_fpga_from_flash().
*/
EPIQ_API int32_t skiq_save_fpga_config_to_flash(uint8_t card, FILE* p_file)
{
    return skiq_save_fpga_config_to_flash_slot( card,
                                                FLASH_CONFIG_DEFAULT_SLOT,
                                                p_file,
                                                FLASH_CONFIG_DEFAULT_METADATA );
} /* skiq_save_fpga_config_to_flash */


/**************************************************************************************************/
/**
   @brief The skiq_verify_fpga_config_from_flash() function verifies the contents of flash memory
   against a given file. This can be used to validate that a given FPGA bitstream is accurately
   stored within flash memory.
*/
EPIQ_API int32_t skiq_verify_fpga_config_from_flash(uint8_t card, FILE* p_file)
{
    uint32_t address = 0;
    int32_t status = -1;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_file );

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        skiq_error("Verifying FPGA flash configuration using network interface not currently supported (card=%u)",
                   card);
        return (-ENODEV);
    }

    LOCK_REGS(card);

    status = flash_get_user_fpga_addr( card, &address );
    if (0 != status)
    {
        skiq_error("Failed to get user FPGA image address for card %" PRIu8 " (status = %" PRIi32
                   ")\n", card, status);
    }

    if ( status == 0 )
    {
        status = flash_verify_file(card, address, p_file);
    }

    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_part_string() function returns a string representation of the
    passed in part value.

    @since Function added in API @b v4.3.0

    @param part Sidekiq part value
    @return const char* string representing the Sidekiq part
*/
EPIQ_API const char* skiq_part_string( skiq_part_t part )
{
    return part_cstr( part );
}

/*****************************************************************************/
/** The skiq_hardware_vers_string() function returns a string representation
    of the passed in hardware version.

    @param hardware_vers hardware version value
    @return const char* string representing the hardware version
*/
EPIQ_API const char* skiq_hardware_vers_string( skiq_hw_vers_t hardware_vers )
{
    return hw_vers_cstr( hardware_vers );
}

/*****************************************************************************/
/** The skiq_product_vers_string() function returns a string representation
    of the passed in product version.

    @param product_vers product version value
    @return const char* string representing the product version
*/
EPIQ_API const char* skiq_product_vers_string( skiq_product_t product_vers )
{
    return product_vers_cstr( product_vers );
}

/*****************************************************************************/
/** The skiq_rf_port_string() function returns a string representation
    of the passed in @ref skiq_rf_port_t.

    @since Function added in API @b v4.5.0

    @param[in] rf_port RF port value
    @return const char* string representing the RF port
*/
EPIQ_API const char* skiq_rf_port_string( skiq_rf_port_t rf_port )
{
    if( rf_port < skiq_rf_port_J1 ||
        rf_port >= skiq_rf_port_max )
    {
        return RF_PORT_INVALID;
    }
    else
    {
        return (SKIQ_RF_PORT_STRINGS[rf_port]);
    }
}

/*****************************************************************************/
/** The skiq_part_info() function returns strings representating the various
    components of a part.

    @param card card index of Sidekiq of interest
    @param p_part_number pointer to where to store the part number (ex: "020201")
       Must be able to contain SKIQ_PART_NUM_STRLEN # of bytes.
    @param p_revision pointer to where to store the revision. (ex: "B0")
       Must be able to contain SKIQ_REVISION_STRLEN # of bytes.
    @param p_variant pointer to where to store the variant. (ex: "01")
       Must be able to contain SKIQ_VARIANT_STRLEN # of bytes.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_part_info( uint8_t card,
                                   char *p_part_number,
                                   char *p_revision,
                                   char *p_variant )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    _skiq_probe();

    strncpy( p_part_number,
             skiq.card[card].card_params.part_info.number_string,
             SKIQ_PART_NUM_STRLEN );
    strncpy( p_revision,
             skiq.card[card].card_params.part_info.revision_string,
             SKIQ_REVISION_STRLEN );
    strncpy( p_variant,
             skiq.card[card].card_params.part_info.variant_string,
             SKIQ_VARIANT_STRLEN );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_max_sample_rate() function returns the maximum sample rate
    possible for the Sidekiq card requested based on the current channel mode.

    @deprecated This function has been deprecated and may not return the
    correct maximum sample rate for all handles, this has been replaced
    with @ref skiq_read_parameters.
*/
EPIQ_API int32_t skiq_read_max_sample_rate( uint8_t card, uint32_t *p_max_sample_rate )
{
    int32_t status=0;
    int32_t rx_status=0;
    int32_t tx_status=0;
    rfic_t rfic_instance;
    uint32_t max_rx_sample_rate = 0;
    uint32_t max_tx_sample_rate = 0;
    skiq_part_t part;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance(card);

    if( (skiq.card[card].chan_mode == skiq_chan_mode_dual) &&
        ( (part == skiq_mpcie) || (part == skiq_z3u) ) )
    {
        if( part == skiq_mpcie )
        {
            *p_max_sample_rate = SKIQ_MAX_DUAL_CHAN_MPCIE_SAMPLE_RATE;
        }
        else if( part == skiq_z3u )
        {
            *p_max_sample_rate = SKIQ_MAX_DUAL_CHAN_Z3U_SAMPLE_RATE;    
        }
        else
        {
            skiq_error("Unexpected part %s in special dual chan mode handling (card=%u)", part_cstr(part), card);
            return (-EPROTO);
        }

    }
    else
    {
        rx_status = rfic_read_max_rx_sample_rate( &rfic_instance, &max_rx_sample_rate );
        if(rx_status == 0 || rx_status == -ENOTSUP)
        {
            tx_status = rfic_read_max_tx_sample_rate( &rfic_instance, &max_tx_sample_rate );
            if(rx_status == 0 && tx_status == 0)
            {
                *p_max_sample_rate = MAX(max_rx_sample_rate, max_tx_sample_rate);
            }
            else if(rx_status == -ENOTSUP && tx_status == 0)
            {
                *p_max_sample_rate = max_tx_sample_rate;
            }
            else if(rx_status == 0 && tx_status == -ENOTSUP)
            {
                *p_max_sample_rate = max_rx_sample_rate;

            }
            else
            {
                status = tx_status;
            }
        }
        else
        {
            status = rx_status;
        }
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_min_sample_rate() function returns the minimum sample rate
    possible for the Sidekiq card requested based on the product.

    @deprecated This function has been deprecated and may not return the
    correct minimum sample rate for all handles, this has been replaced
    with @ref skiq_read_parameters.
*/
EPIQ_API int32_t skiq_read_min_sample_rate( uint8_t card, uint32_t *p_min_sample_rate )
{
    int32_t status=0;
    int32_t rx_status=0;
    int32_t tx_status=0;
    rfic_t rfic_instance;
    uint32_t min_rx_sample_rate = 0;
    uint32_t min_tx_sample_rate = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance(card);

    rx_status = rfic_read_min_rx_sample_rate( &rfic_instance, &min_rx_sample_rate );
    if(rx_status == 0 || rx_status == -ENOTSUP)
    {
        tx_status = rfic_read_min_tx_sample_rate( &rfic_instance, &min_tx_sample_rate );
        if(rx_status == 0 && tx_status == 0)
        {
            *p_min_sample_rate = MIN(min_rx_sample_rate, min_tx_sample_rate);
        }
        else if(rx_status == -ENOTSUP && tx_status == 0)
        {
            *p_min_sample_rate = min_tx_sample_rate;
        }
        else if(rx_status == 0 && tx_status == -ENOTSUP)
        {
            *p_min_sample_rate = min_rx_sample_rate;

        }
        else
        {
            status = tx_status;
        }
    }
    else
    {
        status = rx_status;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rx_dc_offset_corr() function is used to configure the DC
    offset correction block in the FPGA.  This is a simple 1-pole filter with
    a knee very close to DC.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the Rx interface to acc
    @param enable true to enable the DC offset correction block
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_dc_offset_corr( uint8_t card, skiq_rx_hdl_t hdl, bool enable )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if ( !_skiq_has_dc_offset_corr( card ) )
    {
        return -ENOTSUP;
    }

    LOCK_REGS(card);
    {
        status = sidekiq_fpga_reg_RMWV(card, enable ? 1 : 0, DC_OFFSET_CORR_ENA,
                                       FPGA_REG_DMA_CTRL_(hdl));

        if ( status == 0 )
        {
            skiq.card[card].rx_list[hdl].dc_offset_ena = enable;
        }
    }
    UNLOCK_REGS(card);

    return status;
}

/*****************************************************************************/
/** The skiq_read_rx_dc_offset_corr() function is responsible for returning
    whether the FPGA-based DC offset correction block is enabled.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the Rx interface to access
    @param p_enable pointer to where to store the enable state, true
    indicates that DC offset correction block is enabled
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_dc_offset_corr( uint8_t card, skiq_rx_hdl_t hdl, bool *p_enable )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    *p_enable = skiq.card[card].rx_list[hdl].dc_offset_ena;

    return status;
}

/*****************************************************************************/
/** The skiq_read_rfic_reg() function reads the value of the RF IC register
    specified.

    @param card card index of the Sidekiq of interest
    @param addr RF IC register address to read
    @param p_data pointer to where to store the value read
    @return const char* string representing the product version
*/
EPIQ_API int32_t skiq_read_rfic_reg(uint8_t card, uint16_t addr, uint8_t *p_data)
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status = hal_rfic_read_reg(card, 0, addr, p_data);
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rfic_tx_fir_config() function provides access to the current
    number of Tx FIR taps as well as the Tx FIR interpolation.
*/
EPIQ_API int32_t skiq_read_rfic_tx_fir_config( uint8_t card,
                                               uint8_t *p_num_taps,
                                               uint8_t *p_fir_interpolation )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_read_tx_fir_config( &rfic_instance, p_num_taps, p_fir_interpolation );
        if ( status == -ENOTSUP )
        {
            skiq_error("Reading RF IC FIR config not currently supported!\n");
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rfic_tx_fir_coeffs() function provides access to the current
    Tx FIR coefficients programmed.  To determine the number of taps and
    the interpolation factor of the FIR, use skiq_read_rfic_tx_fir_config().
*/
EPIQ_API int32_t skiq_read_rfic_tx_fir_coeffs( uint8_t card,
                                               int16_t *p_coeffs )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_read_tx_fir_coeffs( &rfic_instance, p_coeffs );
        if ( status == -ENOTSUP )
        {
            skiq_error("Reading RF IC FIR coeffs not currently supported!\n");
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rfic_tx_fir_coeffs() function allows the coefficients of the
    Tx FIR to be written.  The number of taps and interpolation factor are
    determined by the sample rate and can be queried using
    skiq_read_rfic_tx_fir_config().
*/
EPIQ_API int32_t skiq_write_rfic_tx_fir_coeffs( uint8_t card,
                                                int16_t *p_coeffs )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_write_tx_fir_coeffs( &rfic_instance, p_coeffs );
        if ( status == -ENOTSUP )
        {
            skiq_error("Writing RF IC FIR coeffs not currently supported!\n");
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rfic_rx_fir_config() function provides access to the current
    number of Rx FIR taps as well as the Rx FIR decimation.
*/
EPIQ_API int32_t skiq_read_rfic_rx_fir_config( uint8_t card,
                                               uint8_t *p_num_taps,
                                               uint8_t *p_fir_decimation )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_read_rx_fir_config( &rfic_instance, p_num_taps, p_fir_decimation );
        if ( status == -ENOTSUP )
        {
            skiq_error("Reading RF IC FIR config not currently supported!\n");
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rfic_rx_fir_coeffs() function provides access to the current
    Rx FIR coefficients programmed.  To determine the number of taps and
    the decimation factor of the Rx FIR, use skiq_read_rfic_rx_fir_config().
*/
EPIQ_API int32_t skiq_read_rfic_rx_fir_coeffs(uint8_t card, int16_t *p_coeffs)
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_read_rx_fir_coeffs( &rfic_instance, p_coeffs );
        if ( status == -ENOTSUP )
        {
            skiq_error("Reading RF IC FIR coeffs not currently supported!\n");
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rfic_rx_fir_coeffs() function allows the coefficients of the
    Rx FIR to be written.  The number of taps and interpolation factor are
    determined by the sample rate and can be queried using
    skiq_read_rfic_rx_fir_config().
*/
EPIQ_API int32_t skiq_write_rfic_rx_fir_coeffs(uint8_t card, int16_t *p_coeffs)
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_generic_rfic_instance(card);
        status = rfic_write_rx_fir_coeffs( &rfic_instance, p_coeffs );
        if ( status == -ENOTSUP )
        {
            skiq_error("Writing RF IC FIR coeffs not currently supported!\n");
        }
    }
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rfic_reg() function writes the data specified to the RF IC
    register provided.

    @note writing directly to RF IC registers is not recommended.  Modifying
    register settings may result in incorrect or unexpected behavior.

    @param card card index of the Sidekiq of interest
    @param addr RF IC register address to write to
    @param data value to actually write to the register
    @return const char* string representing the product version
*/
EPIQ_API int32_t skiq_write_rfic_reg(uint8_t card, uint16_t addr, uint8_t data)
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS( card );
    status = hal_rfic_write_reg(card, 0, addr, data);
    UNLOCK_REGS( card );

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rx_fir_gain() function is responsible for configuring the
    gain of the Rx FIR filter.  The Rx FIR filter is used in configuring the
    Rx channel bandwidth.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the Rx interface to access
    @param gain gain of the filter
    @return int32_t status of the operation (0=success, anything else is
    an error code)
*/
EPIQ_API int32_t skiq_write_rx_fir_gain( uint8_t card,
                                         skiq_rx_hdl_t hdl,
                                         skiq_rx_fir_gain_t gain )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS( card );
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
        status = rfic_write_rx_fir_gain( &rfic_instance, gain );
        if ( status == 0 )
        {
            skiq.card[card].rx_fir_gain = gain;
        }
        else if ( status == -ENOTSUP )
        {
            skiq_error("Writing FIR gain not currently supported!\n");
        }
    }
    UNLOCK_REGS( card );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_fir_gain() function is responsible for reading the
    gain of the Rx FIR filter.  The Rx FIR filter is used in configuring the
    Rx channel bandwidth.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the Rx interface to access
    @param p_gain pointer to where to store the gain setting
    @return int32_t status of the operation (0=success, anything else is
    an error code)
*/
EPIQ_API int32_t skiq_read_rx_fir_gain( uint8_t card,
                                        skiq_rx_hdl_t hdl,
                                        skiq_rx_fir_gain_t *p_gain )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS( card );
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
        status = rfic_read_rx_fir_gain( &rfic_instance, p_gain );
        if ( status == 0 )
        {
            skiq.card[card].rx_fir_gain = *p_gain;
        }
        else if ( status == -ENOTSUP )
        {
            skiq_error("Reading FIR gain not currently supported!\n");
        }
    }
    UNLOCK_REGS( card );

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_fir_gain() function is responsible for configuring the
    gain of the Tx FIR filter.  The Tx FIR filter is used in configuring the
    Tx channel bandwidth.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the Tx interface to access
    @param gain gain of the filter
    @return int32_t status of the operation (0=success, anything else is
    an error code)
*/
EPIQ_API int32_t skiq_write_tx_fir_gain( uint8_t card,
                                         skiq_tx_hdl_t hdl,
                                         skiq_tx_fir_gain_t gain )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS( card );
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
        status = rfic_write_tx_fir_gain( &rfic_instance, gain );
        if ( status == -ENOTSUP )
        {
            skiq_error("Writing FIR gain not currently supported!\n");
        }
    }
    UNLOCK_REGS( card );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_fir_gain() function is responsible for reading the
    gain of the Tx FIR filter.  The Tx FIR filter is used in configuring the
    Tx channel bandwidth.

    @param card card index of the Sidekiq of interest
    @param hdl the handle of the Rx interface to access
    @param p_gain pointer to where to store the gain setting
    @return int32_t status of the operation (0=success, anything else is
    an error code)
*/
EPIQ_API int32_t skiq_read_tx_fir_gain( uint8_t card,
                                        skiq_tx_hdl_t hdl,
                                        skiq_tx_fir_gain_t *p_gain )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS( card );
    {
        rfic_t rfic_instance;

        rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
        status = rfic_read_tx_fir_gain( &rfic_instance, p_gain );
        if ( status == -ENOTSUP )
        {
            skiq_error("Reading FIR gain not currently supported!\n");
        }
    }
    UNLOCK_REGS( card );

    return (status);
}

/*****************************************************************************/
/** The pause_all_rx_streams function is responsible for performing
    stopping any Rx handle that is streaming temporarily.  The streaming flag is not
    updated as it is expected that the streaming will be resumed using the
    resume_all_rx_streams function, which checks that flag.

    Note: it is assumed that both the register and Rx mutexes are locked prior
    to calling this function.

    @param card Sidekiq card of interest
    @return void
*/
static void pause_all_rx_streams(uint8_t card)
{
    uint32_t hdl;
    bool at_least_one_streaming = false;

    /* "pause" streaming at the transport layer IFF at least one handle is marked as streaming */
    for (hdl = skiq_rx_hdl_A1; ( hdl < skiq_rx_hdl_end ) && !at_least_one_streaming ; hdl++)
    {
        if ( skiq.card[card].rx_list[hdl].streaming )
        {
            at_least_one_streaming = true;
        }
    }

    if ( at_least_one_streaming )
    {
        skiq_xport_rx_pause_streaming( card );
    }

    for (hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++)
    {
        if( skiq.card[card].rx_list[hdl].streaming == true )
        {
            uint32_t addr, data = 0;

            addr = FPGA_REG_DMA_CTRL_(hdl);
            sidekiq_fpga_reg_read(card, addr, &data);

            /* disable the FIFO control but allow timestamps to continue */
            RBF_SET(data, 0, DMA_RX_FIFO_CTRL);
            sidekiq_fpga_reg_write_and_verify(card, addr, data);
        }
    }
}

/*****************************************************************************/
/** The resume_all_rx_streams function is responsible for performing resuming
    the streaming of any Rx handle that was previously streaming.  The streaming
    flag is checked to determine if the handle was previously streaming. Prior
    to resuming streaming, all of the DMA in the DMA buffers are flushed.
    Note: it is assumed that both the register and Rx mutexes are locked prior
    to calling this function.

    @param card Sidekiq card of interest
    @return void
*/
static void resume_and_flush_all_rx_streams(uint8_t card)
{
    uint32_t hdl;
    int32_t rx_transfer_timeout = RX_TRANSFER_NO_WAIT;
    fpga_ctrl_rx_params_t rx_params[skiq_rx_hdl_end];
    uint8_t nr_rx_params = 0;

    /* save current rx transfer timeout and set to non-blocking so everything
     * can be drained */
    skiq_xport_rx_get_transfer_timeout( card, &rx_transfer_timeout );
    skiq_xport_rx_set_transfer_timeout( card, RX_TRANSFER_NO_WAIT );

    /* clear out old data and resume streaming; this function call order is
     * important as revering this can corrupt data for some systems */
    skiq_xport_rx_flush( card );

    /* "resume" streaming at the transport layer IFF at least one handle is marked as streaming */
    {
        bool at_least_one_streaming = false;

        for (hdl = skiq_rx_hdl_A1; (hdl < skiq_rx_hdl_end) && !at_least_one_streaming; hdl++)
        {
            if( skiq.card[card].rx_list[hdl].streaming == true )
            {
                at_least_one_streaming = true;
            }
        }

        if ( at_least_one_streaming )
        {
            skiq_xport_rx_resume_streaming( card );
        }
    }

    /* construct an array of ::fpga_ctrl_rx_params_t entries to pass to
     * fpga_ctrl_resume_rx_stream_multi() */
    for (hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++)
    {
        if ( skiq.card[card].rx_list[hdl].streaming )
        {
            rx_params[nr_rx_params].ctrl_reg = FPGA_REG_DMA_CTRL_(hdl);
            rx_params[nr_rx_params].hdl = hdl;
            rx_params[nr_rx_params].data_src = skiq.card[card].rx_list[hdl].data_src;
            nr_rx_params++;
        }
    }
    fpga_ctrl_resume_rx_stream_multi( card, rx_params, nr_rx_params );

    /* revert the rx transfer timeout */
    skiq_xport_rx_set_transfer_timeout( card, rx_transfer_timeout );
}

/*****************************************************************************/
/** The skiq_read_ref_clock_select() function is responsible for reading the
    reference clock configuration.

    @attention this is not supported on rev B mPCIe

    @ingroup cardfunctions
    @see skiq_read_ext_ref_clock_freq

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_ref_clk [::skiq_ref_clock_select_t] pointer to where to store the reference clock setting
    @return int32_t status of the operation (0=success, anything else is
    an error code)
*/
EPIQ_API int32_t skiq_read_ref_clock_select( uint8_t card,
                                    skiq_ref_clock_select_t *p_ref_clk )
{
    int32_t status=-ENOTSUP;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if( (skiq.card[card].hardware_vers == skiq_hw_vers_mpcie_c) ||
        (skiq.card[card].hardware_vers == skiq_hw_vers_mpcie_d) ||
        (skiq.card[card].hardware_vers == skiq_hw_vers_mpcie_e) ||
        (skiq.card[card].card_params.part_type == skiq_m2) ||
        (skiq.card[card].card_params.part_type == skiq_m2_2280) ||
        (skiq.card[card].card_params.part_type == skiq_x2) ||
        (skiq.card[card].card_params.part_type == skiq_z2) ||
        (skiq.card[card].card_params.part_type == skiq_x4) ||
        (skiq.card[card].card_params.part_type == skiq_z2p) ||
        (skiq.card[card].card_params.part_type == skiq_z3u) ||
        (skiq.card[card].card_params.part_type == skiq_nv100) )
    {
        LOCK_REGS(card);
        if( skiq.card[card].ref_clock == skiq_ref_clock_invalid )
        {
            status = card_read_ref_clock(card, p_ref_clk);
            if( status == 0 )
            {
                skiq.card[card].ref_clock = *p_ref_clk;
            }
        }
        else
        {
            *p_ref_clk = skiq.card[card].ref_clock;
            status = 0;
        }
        UNLOCK_REGS(card);
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_ext_ref_clock_freq() function is responsible for reading the
    external reference clock's configured frequency. Note that the default
    value is 40Mhz if unconfigured.

    @param card card index of the Sidekiq of interest
    @param p_freq pointer to where to store the external clock's frequency
    @return int32_t status of the operation (0=success, anything else is
    an error code)
*/
EPIQ_API int32_t skiq_read_ext_ref_clock_freq( uint8_t card, uint32_t *p_freq )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = card_read_ext_ref_clock_freq(card, p_freq);

    return status;
}

/*****************************************************************************/
/** The skiq_read_num_tx_threads() function is responsible for returning the
    number of threads used to transfer data when operating in asynchronous mode.

    @param card card index of the Sidekiq of interest
    @param p_num_threads pointer to where to store the number of threads
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_num_tx_threads( uint8_t card,
                                  uint8_t *p_num_threads )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_num_threads = skiq.card[card].num_send_threads;

    return (status);
}

/*****************************************************************************/
/** The skiq_write_num_tx_threads() function is responsible for updating the
    number of threads used to transfer data when operating in asynchronous mode.
    This must be set prior to the Tx interface being started.  If set after the
    Tx interface has been started, the setting will be stored but won't be used
    until the interface is stopped and re-started.

    @param card card index of the Sidekiq of interest
    @param num_threads number of threads to use when running in Tx asynchronous mode
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_num_tx_threads( uint8_t card,
                                   uint8_t num_threads )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    if( num_threads > SKIQ_TX_MAX_NUM_THREADS || num_threads < SKIQ_TX_MIN_NUM_THREADS )
    {
        _skiq_log(SKIQ_LOG_ERROR, "# of tx threads must be between %u-%u, requested %u\n",
                SKIQ_TX_MIN_NUM_THREADS, SKIQ_TX_MAX_NUM_THREADS, num_threads);
        status=-1;
    }
    else
    {
        skiq.card[card].num_send_threads = num_threads;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_thread_priority() function is responsible for returning the
    priority of the threads when operating in asynchronous mode.

    @param card card index of the Sidekiq of interest
    @param p_priority pointer to where to store the priority of the TX threads
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_thread_priority( uint8_t card,
                                      int32_t *p_priority )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_priority = skiq.card[card].thread_priority;

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_thread_priority() function is responsible for updating the
    priority of the threads used to transfer data when operating in asynchronous mode.
    This must be set prior to the Tx interface being started.  If set after the
    Tx interface has been started, the setting will be stored but won't be used
    until the interface is stopped and re-started.

    @param card card index of the Sidekiq of interest
    @param priority TX thread priority
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_thread_priority( uint8_t card,
                                       int32_t priority )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    // the priority required should probably be validated at the xport level, for now just save it
    // it gets checked/validated in the init call
    skiq.card[card].thread_priority = priority;

    return (status);
}


/*****************************************************************************/
/** The skiq_read_num_rx_chans() function is responsible for returning the number
    of Rx channels supported for the Sidekiq card of interest.  The handle for
    the first Rx interface is skiq_rx_hdl_A1 and increments from there.

    @param card card index of the Sidekiq of interest
    @param p_num_rx_chans pointer to the number of Rx channels
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_num_rx_chans( uint8_t card, uint8_t *p_num_rx_chans )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_num_rx_chans );

    *p_num_rx_chans = _skiq_get_nr_rx_hdl( card );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_num_tx_chans() function is responsible for returning the number
    of Rx channels supported for the Sidekiq card of interest.The handle for
    the first Tx interface is skiq_tx_hdl_A1 and increments from there.

    @param card card index of the Sidekiq of interest
    @param p_num_tx_chans pointer to the number of Tx channels
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_num_tx_chans( uint8_t card, uint8_t *p_num_tx_chans )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_num_tx_chans );

    *p_num_tx_chans = _skiq_get_nr_tx_hdl( card );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_iq_resolution() function is responsible for returning the
    resolution (in bits) per RX (ADC) IQ sample.

    @param card card index of the Sidekiq of interest
    @param p_adc_res pointer to where to store the ADC resolution
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_iq_resolution( uint8_t card, uint8_t *p_adc_res )
{
    rfic_t rfic_instance;
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance(card);

    rfic_read_adc_resolution( &rfic_instance, p_adc_res );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_iq_resolution() function is responsible for returning the
	resolution (in bits) per TX (DAC) IQ sample.

    @param card card index of the Sidekiq of interest
    @param p_dac_res pointer to the number of DAC bits
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_iq_resolution( uint8_t card, uint8_t *p_dac_res )
{
    rfic_t rfic_instance;
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance(card);

    rfic_read_dac_resolution( &rfic_instance, p_dac_res );

    return (status);
}

/*****************************************************************************/
/** The skiq_register_critical_error_callback() function allows a custom handler
    to be registered in the case of critical errors.  If a critical error occurs
    and a callback function is registered, then the critical_handler will be
    called.  If no handler is registered, then exit() is called.  Continued use
    of libsidekiq after a critical error has occurred will result in undefined
    behavior.

    @param critical_handler function pointer to handler to call in the case of
    a critical error.  If no handler is registered, exit() will be called.
    @param p_user_data a pointer to user data to be provided as an argument to
    the critiral_handler function when called. This can safely be set to NULL
    if unneeded. However, this will cause the argument of the critical handler
    to also be set to NULL.
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API void skiq_register_critical_error_callback( void (*critical_handler)(int32_t status, void* p_user_data),
                                            void* p_user_data )
{
    hal_register_critical_error_cb( critical_handler, p_user_data );
}

/*****************************************************************************/
/** The skiq_register_logging allows a custom logging handler to be registered.
    The priority (as defined by the SKIQ_LOG_* definitions) and the logging 
    message are provided to the function.  If no callback is registered, the 
    logging messages are displayed in the console as well as syslog.  If it is 
    desired to completely disable any output of the library NULL can be 
    registered for the logging function, in which case no logging will occur.

    @param log_msg function pointer to handler to call when logging a message
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API void skiq_register_logging( void (*log_msg)(int32_t priority,
                                            const char *message) )
{
    LOCK(log_callback_mutex);
    skiq.log_msg_cb = log_msg;
    UNLOCK(log_callback_mutex);
}


/*****************************************************************************/
/** The skiq_read_golden_fpga_present_in_flash() function is responsible for
    determining if there is a valid golden image stored in flash.  The
    p_present is set based on whether a golden FPGA image is detected.

    @param card card index of the Sidekiq of interest
    @param p_present pointer to where to store whether the golden image is present
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_golden_fpga_present_in_flash( uint8_t card, uint8_t *p_present )
{
    int32_t status=-1;
    bool present = false;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = flash_verify_golden_present(card, &present);
    // verbose mapping of bool to uint8, just to be safe
    *p_present = (true == present) ? 1 : 0;

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rfic_control_output_rx_gain_config() function provides the
    mode and enable settings to configure the control output to present the
    gain of the handle specified.

    @ingroup rficfunctions
    @see skiq_write_rfic_control_output_config
    @see skiq_read_rfic_control_output_config
    @see skiq_rx_block_t::rfic_control

    @since Function added in @b v4.8.0, requires FPGA @b v3.11.0 or later for Sidekiq X2 and X4

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] RX handle of the gain setting to present in control output
    @param[out] p_mode pointer to where to store the control output mode setting
    @param[out] p_ena pointer to where to store the control output enable setting
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rfic_control_output_rx_gain_config( uint8_t card,
                                                               skiq_rx_hdl_t hdl,
                                                               uint8_t *p_mode,
                                                               uint8_t *p_ena )
{
    int32_t status=0;
    rfic_t rfic_instance;
    
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );
    status = rfic_read_control_output_rx_gain_config( &rfic_instance,
                                                      p_mode,
                                                      p_ena );
    
    return (status);
}

/*****************************************************************************/
/** The skiq_write_rfic_control_output_config() function allows the control
    output configuration of the RF IC to be configured. The control output
    readings are included within each receive packet's metadata. 

    For details on the fields available for the control output, refer to the 
    "Monitor Output" section of the appropriate reference manual.

    For Sidekiq mPCIe / m.2 / Z2, refer to p.73 of the <a
    href="http://www.analog.com/media/en/technical-documentation/user-guides/AD9361_Reference_Manual_UG-570.pdf">AD9361
    Reference Manual UG-570</a>: 

    - For Sidekiq X2, refer to Table 142 on p.192 of the AD9371 User Guide (UG-992):

    - For Sidekiq X4, refer to Table 130 on p.214 of the ADRV9008-1/ADRV9008-2/ADRV9009 Hardware Reference Manual UG-1295:

    @param[in] card card index of the Sidekiq of interest
    @param[in] mode control output mode
    @param[in] ena control output enable
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rfic_control_output_config( uint8_t card, uint8_t mode, uint8_t ena )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance( card );
    LOCK_REGS(card);
    status = rfic_write_control_output_config( &rfic_instance,
                                               mode,
                                               ena );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rfic_control_output_config() function allows the control
    output configuration of the RF IC to be read. 

    For details on the fields available for the control output, refer to the 
    "Monitor Output" section of the appropriate reference manual.

    For Sidekiq mPCIe / m.2 / Z2, refer to p.73 of the <a
    href="http://www.analog.com/media/en/technical-documentation/user-guides/AD9361_Reference_Manual_UG-570.pdf">AD9361
    Reference Manual UG-570</a>:

    - For Sidekiq X2, refer to Table 142 on p.192 of the AD9371 User Guide (UG-992):

    - For Sidekiq X4, refer to Table 130 on p.214 of the ADRV9008-1/ADRV9008-2/ADRV9009 Hardware Reference Manual UG-1295:

    @param card card index of the Sidekiq of interest
    @param p_mode pointer to where to store the control output mode setting
    @param p_ena pointer to where to store the control output enable setting
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rfic_control_output_config( uint8_t card, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_generic_rfic_instance( card );
    LOCK_REGS(card);
    status = rfic_read_control_output_config( &rfic_instance,
                                              p_mode,
                                              p_ena );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_enable_rfic_control_output_rx_gain() function applies the RF IC
    mode and enable settings to configure the control output to represent the
    gain of the handle specified.  This is equivalent to calling 
    skiq_read_rfic_control_output_rx_gain_config() followed by 
    skiq_write_rfic_control_output_config() with the appropriate mode and enable
    settings for the RX handle.

    @ingroup rficfunctions
    @see skiq_write_rfic_control_output_config
    @see skiq_read_rfic_control_output_config
    @see skiq_rx_block_t::rfic_control

    @since Function added in @b v4.8.0, requires FPGA @b v3.11.0 or later for Sidekiq X2 and X

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] RX handle of the gain setting to present in control output
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_enable_rfic_control_output_rx_gain( uint8_t card,
                                                          skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    uint8_t mode=0;
    uint8_t ena;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if( (status=skiq_read_rfic_control_output_rx_gain_config( card,
                                                             hdl,
                                                             &mode,
                                                              &ena )) == 0 )
    {
        status = skiq_write_rfic_control_output_config( card, mode, ena );
    }

    return (status);
}


/*****************************************************************************/
/** The skiq_read_rx_LO_freq_range() function allows an application to obtain
    the maximum and minimum frequencies that a Sidekiq can tune to receive at.

    @param card card index of the Sidekiq of interest
    @param p_max pointer to update with maximum LO frequency
    @param p_min pointer to update with minimum LO frequency
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_LO_freq_range( uint8_t card, uint64_t* p_max, uint64_t* p_min )
{
    int32_t status;

    /* CHECK_CARD_RANGE performed by these two API calls */

    status = skiq_read_max_rx_LO_freq( card, p_max );
    if ( 0 == status ) status = skiq_read_min_rx_LO_freq( card, p_min );

    return status;
}

/*****************************************************************************/
/** The skiq_read_max_rx_LO_freq() function allows an application to obtain the
    maximum frequency that a Sidekiq can tune to receive at.

    @param card card index of the Sidekiq of interest
    @param p_max pointer to update with maximum LO frequency
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_max_rx_LO_freq( uint8_t card, uint64_t* p_max )
{
    CHECK_CARD_RANGE( card );

    // TODO: need to expose per handle tuning range
    tune_read_rx_LO_max_freq(card, skiq_rx_hdl_A1, p_max);

    return 0;
}

/*****************************************************************************/
/** The skiq_read_min_rx_LO_freq() function allows an application to obtain
    minimum frequency that a Sidekiq can tune to receive at.

    @param card card index of the Sidekiq of interest
    @param p_min pointer to update with minimum LO frequency
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_min_rx_LO_freq( uint8_t card, uint64_t* p_min )
{
    CHECK_CARD_RANGE( card );

    // TODO: need to expose per handle tuning range
    tune_read_rx_LO_min_freq(card, skiq_rx_hdl_A1, p_min);

    return 0;
}

/*****************************************************************************/
/** The skiq_read_tx_LO_freq_range() function allows an application to obtain
    the maximum and minimum frequencies that a Sidekiq can tune to transmit at.

    @param card card index of the Sidekiq of interest
    @param p_max pointer to update with maximum LO frequency
    @param p_min pointer to update with minimum LO frequency
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_LO_freq_range( uint8_t card, uint64_t* p_max, uint64_t* p_min )
{
    int32_t status;

    /* CHECK_CARD_RANGE performed by these two API calls */

    // TODO: need to expose per handle tuning range
    status = skiq_read_max_tx_LO_freq( card, p_max );
    if ( 0 == status ) status = skiq_read_min_tx_LO_freq( card, p_min );

    return status;
}

/*****************************************************************************/
/** The skiq_read_max_tx_LO_freq() function allows an application to obtain the
    maximum frequency that a Sidekiq can tune to transmit at.

    @param card card index of the Sidekiq of interest
    @param p_max pointer to update with maximum LO frequency
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_max_tx_LO_freq( uint8_t card, uint64_t* p_max )
{
    CHECK_CARD_RANGE( card );

    // TODO: need to expose per handle tuning range
    tune_read_tx_LO_max_freq(card, skiq_tx_hdl_A1, p_max);

    return 0;
}

/*****************************************************************************/
/** The skiq_read_min_tx_LO_freq() function allows an application to obtain
    minimum frequency that a Sidekiq can tune to transmit at.

    @param card card index of the Sidekiq of interest
    @param p_min pointer to update with minimum LO frequency
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_min_tx_LO_freq( uint8_t card, uint64_t* p_min )
{
    CHECK_CARD_RANGE( card );

    // TODO: need to expose per handle tuning range
    tune_read_tx_LO_min_freq(card, skiq_tx_hdl_A1, p_min);

    return 0;
}

/*****************************************************************************/
/** The skiq_read_rf_port_config_avail() function determines the RF port
    configuration options supported by the specified Sidekiq.  The RF port
    configuration controls the Rx/Tx capabilities for a given RF port.

    @param card card index of the Sidekiq of interest
    @param p_fixed pointer indicating if fixed RF port config available
    @param p_trx pointer indicating if TRX RF port config avail
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rf_port_config_avail( uint8_t card,
                                                 bool *p_fixed,
                                                 bool *p_trx )
{
    int32_t status=0;
    rfe_t rfe_instance;
    
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_fixed );
    CHECK_NULL_PARAM( p_trx );

    // RX or TX doesn't matter....just grab the RX one
    rfe_instance = _skiq_get_generic_rfe_instance(card);
    status = rfe_read_rf_port_config_avail( &rfe_instance, p_fixed, p_trx );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rf_port_config() function reads the current RF port
    configuration for the specified Sidekiq.

    @param card card index of the Sidekiq of interest
    @param p_config pointer to the current antenna configuration
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rf_port_config( uint8_t card,
                                  skiq_rf_port_config_t *p_config )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    *p_config = skiq.card[card].rf_port_config;

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rf_port_config() function allows the RF port configuration of
    the Sidekiq card specified to be configured.  Note that only particular
    hardware variants support certain RF port configurations.  To determine the
    available RF port configuration options, use
    skiq_read_rf_port_config_avail().

    @param card card index of the Sidekiq of interest
    @param config RF port configuration to apply
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rf_port_config( uint8_t card,
                                   skiq_rf_port_config_t config )
{
    int32_t status=-ENOTSUP;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if( skiq.card[card].rf_port_config == config )
    {
        // configuration already matches, just return a success
        status = 0;
    }
    else
    {
        // RX or TX doesn't matter....just grab the RX one
        rfe_instance = _skiq_get_generic_rfe_instance(card);
        LOCK_REGS(card);
        if( (status=rfe_write_rf_port_config( &rfe_instance, config)) == 0 )
        {
            // save our config it we were able to set it successfully
            skiq.card[card].rf_port_config = config;

            // update RFIC port mapping change (if needed)
            status = _skiq_update_rfic_port_mapping( card );
        }
        UNLOCK_REGS(card);
   }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rf_port_operation() function reads the operation mode of the
    RF port(s).  If the transmit flag is set, then the port(s) are configured to
    transmit, otherwise it is configured for receive.

    @note this configuration is available only with the @ref
    skiq_rf_port_config_trx

    @param card card index of the Sidekiq of interest
    @param p_transmit pointer to flag indicating whether to transmit or receive
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rf_port_operation( uint8_t card, bool *p_transmit )
{
    int32_t status=-ENOTSUP;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    // make sure we're in TDD/TRX config first
    if( (skiq.card[card].rf_port_config == skiq_rf_port_config_tdd) ||
        (skiq.card[card].rf_port_config == skiq_rf_port_config_trx) )
    {
        // RX or TX doesn't matter....just grab the RX one
        rfe_instance = _skiq_get_generic_rfe_instance(card);

        LOCK_REGS(card);
        status = rfe_read_rf_port_operation( &rfe_instance, p_transmit );
        UNLOCK_REGS(card);
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_write_rf_port_operation() function sets the operation mode of the
    RF port(s) to either transmit or receive.  If the transmit flag is set, then
    the port(s) are configured to transmit, otherwise it is configured for
    receive.

    @note: this configuration is available only with the @ref
    skiq_rf_port_config_trx

    @param card card index of the Sidekiq of interest
    @param transmit flag indicating whether to transmit or receive
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rf_port_operation( uint8_t card, bool transmit )
{
    int32_t status=-1;
    rfe_t rfe_instance = _skiq_get_rx_rfe_instance(card, skiq_rx_hdl_A1);
    rfic_t rfic_txA1 = _skiq_get_tx_rfic_instance(card,skiq_tx_hdl_A1);
    rfic_t rfic_txA2 = _skiq_get_tx_rfic_instance(card,skiq_tx_hdl_A2);
    uint16_t atten_txA1 = skiq.card[card].tx_list[skiq_tx_hdl_A1].attenuation;
    uint16_t atten_txA2 = skiq.card[card].tx_list[skiq_tx_hdl_A2].attenuation;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    // make sure we're in TDD/TRX config first
    if( (skiq.card[card].rf_port_config == skiq_rf_port_config_tdd) ||
        (skiq.card[card].rf_port_config == skiq_rf_port_config_trx) )
    {
        LOCK_REGS(card);

        // update the attenuation settings appropriately
        if( transmit == true )
        {
            // set the attenuation to the cached value
            status=rfic_write_tx_attenuation( &(rfic_txA1), atten_txA1 );
            if ( ( _skiq_get_nr_tx_hdl( card ) > 1 ) && (status==0) )
            {
                status=rfic_write_tx_attenuation( &(rfic_txA2), atten_txA2 );
            }
        }
        else
        {
            // set the attenuation to max
            status=rfic_write_tx_attenuation( &(rfic_txA1), SKIQ_MAX_TX_ATTENUATION);
            if ( ( _skiq_get_nr_tx_hdl( card ) > 1 ) && (status==0) )
            {
                status=rfic_write_tx_attenuation( &(rfic_txA2), SKIQ_MAX_TX_ATTENUATION );
            }
        }          
        if( status == 0 )
        {
            status = rfe_write_rf_port_operation( &rfe_instance, transmit );
        }

        UNLOCK_REGS(card);
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_rf_ports_avail_for_hdl() function reads a list of RF ports
    supported for the specified RX handle.

    @since Function added in API @b v4.5.0

    @note The fixed port list is only available for use when the RF port 
    configuration is set to @ref skiq_rf_port_config_fixed.  The TRx port list is only 
    available for use when the RF port configuration is set to @ref skiq_rf_port_config_trx.

    @note p_num_fixed_rf_port_list and p_trx_rf_port_list must contain at least
    skiq_rf_port_max number of elements.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl RX handle of interest
    @param[out] p_num_fixed_rf_ports pointer to the number of fixed RF ports available
    @param[out] p_fixed_rf_port_list [::skiq_rf_port_t] pointer list of fixed RF ports
    @param[out] p_num_trx_rf_ports pointer to the number of TRX RF ports available
    @param[out] p_fixed_rf_port_list [::skiq_rf_port_t] pointer list of TRX RF ports
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_rf_ports_avail_for_hdl( uint8_t card,
                                                      skiq_rx_hdl_t hdl,
                                                      uint8_t *p_num_fixed_rf_ports,
                                                      skiq_rf_port_t *p_fixed_rf_port_list,
                                                      uint8_t *p_num_trx_rf_ports,
                                                      skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfe = _skiq_get_rx_rfe_instance(card, hdl);    
    status = rfe_read_rx_rf_ports_avail( &rfe,
                                         p_num_fixed_rf_ports,
                                         p_fixed_rf_port_list,
                                         p_num_trx_rf_ports,
                                         p_trx_rf_port_list );
    
    return (status);
}


/*****************************************************************************/
/** The skiq_read_rx_rf_port_for_hdl() function reads the current RF port 
    configured for the RX handle specified.

    @since Function added in API @b v4.5.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl RX handle of interest
    @param[out] p_rf_port [::skiq_rf_port_t] pointer to the current RF port
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_rf_port_for_hdl( uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfe = _skiq_get_rx_rfe_instance(card, hdl);    
    status = rfe_read_rx_rf_port( &rfe, p_rf_port );
    
    return (status);
}

/*****************************************************************************/
/** The skiq_write_rx_rf_port_for_hdl() function configures the RF port for use 
    with the RX handle.

    @since Function added in API @b v4.5.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl RX handle of interest
    @param[out] rf_port [::skiq_rf_port_t] RF port to use for hdl
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_rx_rf_port_for_hdl( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                skiq_rf_port_t rf_port )
{
    int32_t status=0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );


    rfe = _skiq_get_rx_rfe_instance(card, hdl);    
    status = rfe_write_rx_rf_port( &rfe, rf_port );

    LOCK_REGS(card);
    // update RFIC port mapping change (if needed)
    if( status == 0 )
    {
        status = _skiq_update_rfic_port_mapping( card );
    }
    UNLOCK_REGS(card);
    
    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_rf_ports_avail_for_hdl() function reads a list of RF ports
    supported for the specified TX handle.

    @since Function added in API @b v4.5.0

    @note The fixed port list is only available for use when the RF port 
    configuration is set to @ref skiq_rf_port_config_fixed.  The TRx port list is only 
    available for use when the RF port configuration is set to @ref skiq_rf_port_config_trx.

    @note p_num_fixed_rf_port_list and p_trx_rf_port_list must contain at least
    skiq_rf_port_max number of elements.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl TX handle of interest
    @param[out] p_num_rf_port pointer to the number of ports available
    @param[out] p_fixed_rf_port_list [::skiq_rf_port_t] pointer list of fixed RF ports
    @param[out] p_num_trx_rf_ports pointer to the number of TRX RF ports available
    @param[out] p_fixed_rf_port_list [::skiq_rf_port_t] pointer list of TRX RF ports
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_rf_ports_avail_for_hdl( uint8_t card,
                                                      skiq_tx_hdl_t hdl,
                                                      uint8_t *p_fixed_num_rf_ports,
                                                      skiq_rf_port_t *p_fixed_rf_port_list,
                                                      uint8_t *p_num_trx_rf_ports,
                                                      skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfe = _skiq_get_tx_rfe_instance(card, hdl);    
    status = rfe_read_tx_rf_ports_avail( &rfe,
                                         p_fixed_num_rf_ports,
                                         p_fixed_rf_port_list,
                                         p_num_trx_rf_ports,
                                         p_trx_rf_port_list );
    
    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_rf_port_for_hdl() function reads the current RF port 
    configured for the TX handle specified.

    @since Function added in API @b v4.5.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl TX handle of interest
    @param[out] p_rf_port [::skiq_rf_port_t] pointer to the current RF port
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_rf_port_for_hdl( uint8_t card,
                                               skiq_tx_hdl_t hdl,
                                               skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfe = _skiq_get_tx_rfe_instance(card, hdl);    
    status = rfe_read_tx_rf_port( &rfe, p_rf_port );
    
    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_rf_port_for_hdl() function configures the RF port for use 
    with the TX handle.

    @since Function added in API @b v4.5.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl TX handle of interest
    @param[out] rf_port [::skiq_rf_port_t] RF port to use for hdl
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_rf_port_for_hdl( uint8_t card,
                                                skiq_tx_hdl_t hdl,
                                                skiq_rf_port_t rf_port )
{
    int32_t status=0;
    rfe_t rfe;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    LOCK_REGS(card);
    
    rfe = _skiq_get_tx_rfe_instance(card, hdl);    
    status = rfe_write_tx_rf_port( &rfe, rf_port );
    
    // update RFIC port mapping change (if needed)
    if( status == 0 )
    {
        status = _skiq_update_rfic_port_mapping( card );
    }

    UNLOCK_REGS(card);
    
    return (status);
}


int32_t get_fpga_num_chans( uint8_t card, uint8_t *p_num_rx_chans, uint8_t *p_num_tx_chans )
{
    int32_t status=0;
    uint32_t val=0;
    uint32_t tmp=0;

    CHECK_CARD_RANGE( card );

    if( (status=sidekiq_fpga_reg_read( card, FPGA_REG_VERSION, &val )) == 0 )
    {
        tmp = BF_GET(val, NUM_TX_CHANNELS_OFFSET, NUM_TX_CHANNELS_LEN);
        *p_num_tx_chans = _fpga_num_tx_chans[tmp];
        tmp = BF_GET(val, NUM_RX_CHANNELS_OFFSET, NUM_RX_CHANNELS_LEN);
        *p_num_rx_chans = _fpga_num_rx_chans[tmp];
    }
    return (status);
}

/**
    @brief  Check if the card and handle in question support TX late timestamps.

    @param[in]  card        The card to use.
    @param[in]  hdl         The handle to use.
    @param[out] supported   If not NULL, set to true if the card supports
                            transmitting late timestamps else false.

    @note   It's assumed that LOCK_REG was called for the card before this
            function.

    @return 0 on success, else an error value.
*/
static int32_t
tx_late_timestamps_supported( uint8_t card, skiq_tx_hdl_t hdl, bool *supported )
{
    int32_t status = 0;
    uint32_t addr = FPGA_REG_CAPABILITIES;
    uint32_t data = 0;
    uint8_t late_supported = 0;

    /* Query if the card can support late timestamps. */
    status = sidekiq_fpga_reg_read(card, addr, &data);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR,
            "[card %u]: failed to get late timestamp TX supported register"
            " (status %" PRIi32 ")\n", card, status);
        return status;
    }

    late_supported = BF_GET(data, TX_TS_LATE_SUPPORT_OFFSET,
                        TX_TS_LATE_SUPPORT_LEN);

    if (NULL != supported)
    {
        *supported = (1 == late_supported);
    }

    return (status);
}

/**
    @brief  Configure the FPGA's TX late timestamp settings based upon the
            card's current TX data flow mode.

    @param[in]  card        The card to use.
    @param[in]  hdl         The handle to use.

    This function mainly deals with setting or clearing the flags for the
    late TX timestamp feature based upon capabilities and current settings.

    @note   It's assumed that LOCK_REG was called for the card before this
            function.

    @attention  If the card and handle were set to
            ::skiq_tx_with_timestamps_allow_late_data_flow_mode and the card
            doesn't support it, this function will reset the mode back to
            ::skiq_tx_with_timestamps_data_flow_mode.

    @return 0 on success, else an error code.
    @retval -EINVAL if ::skiq_tx_with_timestamps_allow_data_flow_mode on the
                    specified card was selected but the FPGA bitstream doesn't
                    support it.
*/
int32_t configure_tx_late_timestamp_mode( uint8_t card, skiq_tx_hdl_t hdl )
{
    skiq_tx_flow_mode_t flow_mode = skiq.card[card].tx_list[hdl].flow_mode;
    int32_t status = 0;
    uint32_t addr = FPGA_REG_TIMESTAMP_RST;
    uint32_t data = 0;
    bool late_supported = 0;
    uint8_t late_enabled = 0;

    /*
        Query if the card can support late timestamps; this is a verification
        of the check made in skiq_write_tx_mode.
    */
    status = tx_late_timestamps_supported(card, hdl, &late_supported);
    if( 0 != status )
    {
        return (status);
    }
    if (!late_supported)
    {
        /*
            This card doesn't support late transmit; reset the flow_mode to
            skiq_tx_with_timestamps_data_flow_mode if the late mode is
            selected.
        */
        if( skiq_tx_with_timestamps_allow_late_data_flow_mode ==
            skiq.card[card].tx_list[hdl].flow_mode )
        {
            _skiq_log(SKIQ_LOG_WARNING,
                "[card %u]: FPGA bitstream does not support late timestamp TX"
                " mode; reverting to regular timestamp mode.\n", card);
            skiq.card[card].tx_list[hdl].flow_mode = \
                skiq_tx_with_timestamps_data_flow_mode;
            status = -ENOTSUP;
        }
        else
        {
            /* This function didn't technically fail, so return success. */
            status = 0;
        }

        return (status);
    }

    /* Set the late timestamp register based on the current mode. */
    status = sidekiq_fpga_reg_read(card, addr, &data);
    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR,
            "[card %u]: failed to get late timestamp TX enabled register"
            " (status %" PRIi32 ")\n", card, status);
        return (status);
    }

    if( skiq_tx_with_timestamps_allow_late_data_flow_mode == flow_mode )
    {
        late_enabled = 1;
    }
    else
    {
        late_enabled = 0;
    }

    BF_SET(data, late_enabled, TX_TRANSMIT_LATE_OFFSET,
            TX_TRANSMIT_LATE_LEN);

    status = sidekiq_fpga_reg_write(card, addr, data);
    if( 0 == status )
    {
        status = sidekiq_fpga_reg_verify(card, addr, data);
    }

    if( 0 != status )
    {
        _skiq_log(SKIQ_LOG_ERROR, "[card %u]: failed to change TX timestamp"
            " register (status %" PRIi32 ")\n", card, status);
    }

    return (status);
}


/*****************************************************************************/
/** The _read_rx_cal_offset_by_LO_freq_and_gain_index() function is a helper
 * function that calls into card services to provide a receive calibration
 * offset given a card index, receive handle, LO frequency and gain index.  This
 * function translates the configured RX FIR gain from an enum to a value in dB
 * to pass into the card services call.
 *
 *
 * @param[in] card card index of the Sidekiq of interest
 * @param[in] hdl receive handle of interest
 * @param[in] lo_freq LO frequency in Hertz
 * @param[in] gain_index gain index as set in the RFIC
 * @param[out] p_cal_off_dB reference to container for calibration offset in dB
 *
 * @return int32_t status where 0=success, anything else is an error
 */
static int32_t _read_rx_cal_offset_by_LO_freq_and_gain_index( uint8_t card,
                                                              skiq_rx_hdl_t hdl,
                                                              uint64_t lo_freq,
                                                              uint8_t gain_index,
                                                              double *p_cal_off_dB )
{
    int32_t status = 0;
    double rxfir_gain_dB;
    skiq_rf_port_t rx_port;

    /* assumption: 'card' and 'hdl' are checked by calling function */

    /* restrict lo_freq to be within bounds for interpolation's sake */
    lo_freq = MAX(lo_freq, skiq.card[card].rx_list[hdl].params.lo_freq_min);
    lo_freq = MIN(lo_freq, skiq.card[card].rx_list[hdl].params.lo_freq_max);

    /* restrict gain_index to be within bounds for interpolation's sake */
    gain_index = MAX(gain_index, skiq.card[card].rx_list[hdl].params.gain_index_min);
    gain_index = MIN(gain_index, skiq.card[card].rx_list[hdl].params.gain_index_max);

    if ( skiq.card[card].rx_fir_gain == skiq_rx_fir_gain_neg_12 )
        rxfir_gain_dB = -12.0;
    else if ( skiq.card[card].rx_fir_gain == skiq_rx_fir_gain_neg_6 )
        rxfir_gain_dB = -6.0;
    else if ( skiq.card[card].rx_fir_gain == skiq_rx_fir_gain_6 )
        rxfir_gain_dB = +6.0;
    else
        rxfir_gain_dB = 0.0;

    status = skiq_read_rx_rf_port_for_hdl( card, hdl, &rx_port );

    if ( status == 0 )
    {
        status = card_read_cal_offset_in_dB( card, hdl, rx_port, lo_freq,
                                             gain_index, rxfir_gain_dB, p_cal_off_dB );
    }

    return (status);

} /* _read_rx_cal_offset_by_LO_freq_and_gain_index */


/*****************************************************************************/
/** The skiq_read_rx_cal_offset() function provides a receive calibration offset
 * based on the current settings of the receive handle.  This function may not
 * be used if the gain mode for the handle is set to \ref skiq_rx_gain_auto and
 * will return an error.
 *
 * @since Function added in API @b v3.7.0
 *
 * @param card card index of the Sidekiq of interest
 * @param hdl receive handle of interest
 * @param p_cal_off_dB reference to container for calibration offset in dB
 *
 * @return int32_t status where 0=success, anything else is an error
 */
EPIQ_API int32_t skiq_read_rx_cal_offset( uint8_t card,
                                 skiq_rx_hdl_t hdl,
                                 double *p_cal_off_dB )
{
    skiq_rx_iface *p_rx;
    uint64_t freq = 0;
    double actual = 0.0;
    uint8_t gain_index;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    /* after validation, assign the pointer */
    p_rx = &(skiq.card[card].rx_list[hdl]);

    /* check rx gain mode.  if in auto, complain and suggest
     * skiq_read_rx_cal_offset_by_gain_index is more appropriate, passing in the
     * gain from the metadata */
    if ( p_rx->gain_mode == skiq_rx_gain_auto )
    {
        _skiq_log( SKIQ_LOG_ERROR, "Cannot determine RX gain index when gain mode is 'skiq_rx_gain_auto',"
                   " instead use skiq_read_rx_cal_offset_by_gain_index and metadata\n" );
        return -ENOTSUP;
    }

    /* grab cached values */
    tune_read_rx_LO_freq(card, hdl, &freq, &actual);
    gain_index = (uint8_t)(p_rx->gain);

    return _read_rx_cal_offset_by_LO_freq_and_gain_index( card,
                                                          hdl,
                                                          (uint64_t) actual,
                                                          gain_index,
                                                          p_cal_off_dB );
} /* skiq_read_rx_cal_offset */


/*****************************************************************************/
/** The skiq_read_rx_cal_offset_by_LO_freq() function provides a receive
 * calibration offset given an LO frequency and based on the present gain index
 * of the receive handle.  This function may not be used if the gain mode for
 * the handle is set to \ref skiq_rx_gain_auto and will return an error.
 *
 * @since Function added in API @b v3.7.0
 *
 * @param[in] card card index of the Sidekiq of interest
 * @param[in] hdl receive handle of interest
 * @param[in] lo_freq LO frequency in Hertz
 * @param[out] p_cal_off_dB reference to container for calibration offset in dB
 *
 * @return int32_t status where 0=success, anything else is an error
 */
EPIQ_API int32_t skiq_read_rx_cal_offset_by_LO_freq( uint8_t card,
                                            skiq_rx_hdl_t hdl,
                                            uint64_t lo_freq,
                                            double *p_cal_off_dB )
{
    skiq_rx_iface *p_rx;
    uint8_t gain_index;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    /* after validation, assign the pointer */
    p_rx = &(skiq.card[card].rx_list[hdl]);

    /* check rx gain mode.  if in auto, complain and suggest
     * skiq_read_rx_cal_offset_by_LO_freq_and_gain_index is more appropriate, passing in the
     * gain from the metadata */
    if ( p_rx->gain_mode == skiq_rx_gain_auto )
    {
        _skiq_log( SKIQ_LOG_ERROR, "Cannot determine RX gain index when gain mode is 'skiq_rx_gain_auto',"
                   " instead use skiq_read_rx_cal_offset_by_LO_freq_and_gain_index and metadata\n" );
        return -ENOTSUP;
    }

    /* grab cached values */
    gain_index = (uint8_t)(p_rx->gain);

    return _read_rx_cal_offset_by_LO_freq_and_gain_index( card, hdl, lo_freq, gain_index, p_cal_off_dB );

} /* skiq_read_rx_cal_offset_by_LO_freq */


/*****************************************************************************/
/** The skiq_read_rx_cal_offset_by_gain_index() function provides a receive
 * calibration offset given a receive gain index and based on the present LO
 * frequency of the receive handle.  This function is useful when the gain mode
 * for the handle is set to \ref skiq_rx_gain_auto and the caller feeds in the
 * gain index from the receive packet's metadata.
 *
 * @since Function added in API @b v3.7.0
 *
 * @param[in] card card index of the Sidekiq of interest
 * @param[in] hdl receive handle of interest
 * @param[in] gain_index gain index as set in the RFIC
 * @param[out] p_cal_off_dB reference to container for calibration offset in dB
 *
 * @return int32_t status where 0=success, anything else is an error
 */
EPIQ_API int32_t skiq_read_rx_cal_offset_by_gain_index( uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               uint8_t gain_index,
                                               double *p_cal_off_dB )
{
    uint64_t freq = 0;
    double actual = 0.0;
    
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    /* grab cached values */
    tune_read_rx_LO_freq(card, hdl, &freq, &actual);
    
    return _read_rx_cal_offset_by_LO_freq_and_gain_index( card,
                                                          hdl,
                                                          (uint64_t) actual,
                                                          gain_index,
                                                          p_cal_off_dB );

} /* skiq_read_rx_cal_offset_by_gain_index */


/*****************************************************************************/
/** The skiq_read_rx_cal_offset_by_LO_freq_and_gain_index() function provides a
 * receive calibration offset given an LO frequency and receive gain index and
 * based on the present RX FIR filter gain of the receive handle.  This function
 * is useful when the gain mode for the handle is set to \ref skiq_rx_gain_auto
 * and the caller feeds in the gain index from the receive packet's metadata and
 * when the radio is not presently tuned to the frequency of interest.
 *
 * @since Function added in API @b v3.7.0
 *
 * @param[in] card card index of the Sidekiq of interest
 * @param[in] hdl receive handle of interest
 * @param[in] lo_freq LO frequency in Hertz
 * @param[in] gain_index gain index as set in the RFIC
 * @param[out] p_cal_off_dB reference to container for calibration offset in dB
 *
 * @return int32_t status where 0=success, anything else is an error
 */
EPIQ_API int32_t skiq_read_rx_cal_offset_by_LO_freq_and_gain_index( uint8_t card,
                                                           skiq_rx_hdl_t hdl,
                                                           uint64_t lo_freq,
                                                           uint8_t gain_index,
                                                           double *p_cal_off_dB )
{
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    return _read_rx_cal_offset_by_LO_freq_and_gain_index( card, hdl, lo_freq, gain_index, p_cal_off_dB );

} /* skiq_read_rx_cal_offset_by_LO_freq_and_gain_index */


/*****************************************************************************/
/** The skiq_read_rx_cal_data_present() function provides an indication for
 * whether or not receive calibration data is present for a specified card and
 * handle and currently configured RF port.  If the receive calibration data is
 * not present, the default calibration (if supported / available) is used in
 * skiq_read_rx_cal_offset(), skiq_read_rx_cal_offset_by_LO_freq(),
 * skiq_read_rx_cal_offset_by_gain_index(), and
 * skiq_read_rx_cal_offset_by_LO_freq_and_gain_index().
 *
 * @note This function provides indication of calibration data presence based on
 * the @b currently configured RF port.
 *
 * @since Function added in API @b v4.3.0
 *
 * @param[in] card card index of the Sidekiq of interest
 * @param[in] hdl receive handle of interest
 * @param[out] p_present reference to a boolean value indicating data presence
 *
 * @return int32_t status where 0=success, anything else is an error
 */
EPIQ_API int32_t skiq_read_rx_cal_data_present( uint8_t card,
                                                skiq_rx_hdl_t hdl,
                                                bool *p_present )
{
    int32_t status;
    skiq_rf_port_t rx_port;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS(card);
    /* read the current RF port and use it to look up calibration offset */
    status = skiq_read_rx_rf_port_for_hdl( card, hdl, &rx_port );
    if ( status == 0 )
    {
        *p_present = card_is_rx_cal_data_present( card, hdl, rx_port );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_read_rx_cal_data_present */


/*****************************************************************************/
/** The skiq_read_rx_cal_data_present_for_port() function provides an indication
 * for whether or not receive calibration data is present for a specified card,
 * handle, and RF port.  If the receive calibration data is not present, the
 * default calibration (if supported / available) is used in
 * skiq_read_rx_cal_offset(), skiq_read_rx_cal_offset_by_LO_freq(),
 * skiq_read_rx_cal_offset_by_gain_index(), and
 * skiq_read_rx_cal_offset_by_LO_freq_and_gain_index().
 *
 * @since Function added in API @b v4.3.0
 *
 * @param[in] card card index of the Sidekiq of interest
 * @param[in] hdl receive handle of interest
 * @param[out] p_present reference to a boolean value indicating data presence
 *
 * @return int32_t status where 0=success, anything else is an error
 */
EPIQ_API int32_t skiq_read_rx_cal_data_present_for_port( uint8_t card,
                                                         skiq_rx_hdl_t hdl,
                                                         skiq_rf_port_t port,
                                                         bool *p_present )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    /* check that `port` is in range */
    if ( ( port < skiq_rf_port_J1 ) || ( port >= skiq_rf_port_max ) )
    {
        status = -EINVAL;
    }

    LOCK_REGS(card);
    /* use the specified RF port and use it to look up calibration offset */
    if ( status == 0 )
    {
        *p_present = card_is_rx_cal_data_present( card, hdl, port );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_read_rx_cal_data_present */


/*****************************************************************************/
/** The skiq_read_last_tx_timestamp() function queries the FPGA to determine
    what transmit timestamp it last encountered.  The last transmit timestamp is
    only representative if the transmit flow mode is @ref
    skiq_tx_with_timestamps_data_flow_mode.  The last transmit timestamp has two
    interpretations.  Firstly, if the current RF timestamp is greater than the
    timestamp returned by this function, then the FPGA has already transmitted
    the block.  Secondly, if the current RF timestamp is less than the timestamp
    returned by this function, then the FPGA is holding the transmit block and
    waiting until the RF timestamp matches the block's transmit timestamp.

    @since Function added in API @b v3.7.0, requires FPGA @b v3.5 or later

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl transmit handle of interest
    @param[out] p_last_timestamp pointer to 64-bit timestamp value, will be zero if not transmitting
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_last_tx_timestamp( uint8_t card,
                                     skiq_tx_hdl_t hdl,
                                     uint64_t *p_last_timestamp )
{
    int32_t status = -1;
    skiq_tx_flow_mode_t flow_mode;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    flow_mode = skiq.card[card].tx_list[hdl].flow_mode;
    if( (flow_mode != skiq_tx_with_timestamps_data_flow_mode) &&
        (flow_mode != skiq_tx_with_timestamps_allow_late_data_flow_mode) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "invalid Tx mode for last timestamp query\n");
        return -4;
    }

    LOCK_REGS( card );
    status = sidekiq_fpga_reg_read_64(card,
                                      FPGA_REG_LAST_TX_TIMESTAMP_HIGH,
                                      FPGA_REG_LAST_TX_TIMESTAMP_LOW,
                                      p_last_timestamp);
    UNLOCK_REGS( card );

    return (status);

} /* skiq_read_last_tx_timestamp */

/*****************************************************************************/
/** The skiq_read_rx_filters_avail() function allows an application to obtain
    the preselect filters available for the specified card and handle.  Note:
    by default, when the LO frequency of the handle is adjusted, the filter
    encompassing the configured LO frequency is automatically configured.
    There will never be more than skiq_filt_invalid filters returned and
    p_filters should be sized such that it can hold that many filter values.

    @param card card index of the Sidekiq of interest
    @param hdl RX handle of the filter availability in question
    @param p_filters pointer to list of filters available
    @param p_num_filters pointer to where to store the number of filters
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_rx_filters_avail( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters )
{
    int32_t status = -1;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    *p_num_filters = 0;

    rfe_instance = _skiq_get_rx_rfe_instance(card,hdl);

    status = rfe_read_rx_filter_capabilities( &rfe_instance,
                                              p_filters,
                                              p_num_filters );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_filters_avail() function allows an application to obtain
    the preselect filters available for the specified card and handle.  Note:
    by default, when the LO frequency of the handle is adjusted, the filter
    encompassing the configured LO frequency is automatically configured.
    There will never be more than skiq_filt_invalid filters returned and
    p_filters should be sized such that it can hold that many filter values.

    @param card card index of the Sidekiq of interest
    @param hdl TX handle of the filter availability in question
    @param p_filters pointer to list of filters available
    @param p_num_filters pointer to where to store the number of filters
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_filters_avail( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters)
{
    int32_t status = -1;
    rfe_t rfe_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    *p_num_filters = 0;

    rfe_instance = _skiq_get_tx_rfe_instance(card,hdl);

    status = rfe_read_tx_filter_capabilities( &rfe_instance,
                                              p_filters,
                                              p_num_filters );

    return (status);
}

/*****************************************************************************/
/** The skiq_read_filter_range() function provides a mechanism to determine
    the frequency range covered by the specified filter.

    @param filter filter of interest
    @param p_start_freq pointer to where to store the start frequency covered
                        by the filter
    @param p_end_freq pointer to where to store the end frequency covered by
                      the filter
    @return int32_t status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_filter_range( skiq_filt_t filter,
                                         uint64_t *p_start_freq,
                                         uint64_t *p_end_freq )
{
    int32_t status=-1;

    return (status);
}

/*****************************************************************************/
/** The skiq_read_usb_enumeration_delay() function reads the number of
    milliseconds that the Sidekiq should delay USB enumeration, if supported.

    @note This function will return an error if called on a unit that does
    not have an FX2 placed on it.

    @param card card index of the Sidekiq of interest
    @param p_delay_ms pointer to take total enumeration delay in milliseconds
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_usb_enumeration_delay( uint8_t card,
						  uint16_t *p_delay_ms )
{
    int32_t status = -1;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = card_read_usb_enumeration_delay(card, p_delay_ms);

    return (status);
}


/*****************************************************************************/
/** The skiq_read_sys_timestamp_freq() function reads the system timestamp
    frequency (in Hz).  This API should be used in place of
    SKIQ_SYS_TIMESTAMP_FREQ.

    @param card card index of the Sidekiq of interest
    @param p_sys_timestamp_freq pointer to where to store the system timestamp frequency
    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_sys_timestamp_freq( uint8_t card, uint64_t *p_sys_timestamp_freq )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_sys_timestamp_freq );

    status = _check_fpga_state(card);

    if ( status == 0 )
    {
        *p_sys_timestamp_freq = skiq.card[card].fpga_params.sys_timestamp_freq;
    }

    return (status);
}

/*****************************************************************************/
/** The _skiq_read_rx_filter_overflow obtains filter overflow condition for
    a given Rx handle.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the desired Rx interface
    @param p_rx_fir pointer to flag indicating if the RX FIR filter overflowed
    
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_read_rx_filter_overflow( uint8_t card,
				       skiq_rx_hdl_t hdl,
				       bool *p_rx_fir )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_rx_filter_overflow( &rfic_instance, p_rx_fir );
    UNLOCK_REGS(card);
    
    return (status);
}

/*****************************************************************************/
/** The _skiq_read_tx_filter_overflow obtains filter overflow condition for
    a given Tx handle.

    @param card card index of the Sidekiq of interest
    @param hdl handle of the desired Tx interface
    @param p_int3 pointer to flag indicating if the TX INT3 filter overflowed
    @param p_hb33 pointer to flag indicating if the TX HB3 filter overflowed
    @param p_qec pointer to flag indicating if the TX QEC filter overflowed
    @param p_hb1 pointer to flag indicating if the TX HB1 filter overflowed
    @param p_tx_fir pointer to flag indicating if the TX FIR filter overflowed
    
    @return int32_t  status where 0=success, anything else is an error
*/
int32_t _skiq_read_tx_filter_overflow( uint8_t card,
				       skiq_tx_hdl_t hdl,
				       bool *p_int3,
				       bool *p_hb3,
				       bool *p_hb2,
				       bool *p_qec,
				       bool *p_hb1,
				       bool *p_tx_fir )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_tx_filter_overflow( &rfic_instance, p_int3, p_hb3,
                                           p_hb2, p_qec, p_hb1, p_tx_fir );

    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_read_rx_block_size returns the expected RX block size for a
    specified ::skiq_rx_stream_mode_t.

    @since Function added in API @b v4.6.0

    @ingroup rxfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] stream_mode [::skiq_rx_stream_mode_t] RX stream mode associated with RX block size

    @return int32_t
    @retval >0 expected block size (in bytes) for the specified RX stream mode
    @retval -1 specified card index is out of range or has not been initialized
    @retval -ENOTSUP specified RX stream mode is not supported for the loaded FPGA bitstream
    @retval -EINVAL specified RX stream mode is not a valid mode, see ::skiq_rx_stream_mode_t for valid modes
*/
EPIQ_API int32_t skiq_read_rx_block_size( uint8_t card,
                                          skiq_rx_stream_mode_t stream_mode )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ( stream_mode == skiq_rx_stream_mode_high_tput )
    {
        if ( skiq.card[card].iq_packed )
        {
            status = RX_BLOCK_SIZE_STREAM_HIGH_TPUT_PACKED;
        }
        else
        {
            status = RX_BLOCK_SIZE_STREAM_HIGH_TPUT;
        }
    }
    else if( stream_mode == skiq_rx_stream_mode_balanced )
    {
        if ( skiq.card[card].iq_packed )
        {
            status = RX_BLOCK_SIZE_STREAM_BALANCED_PACKED;
        }
        else
        {
            status = RX_BLOCK_SIZE_STREAM_BALANCED;
        }
    }
    else if ( stream_mode == skiq_rx_stream_mode_low_latency )
    {
        skiq_part_t part = _skiq_get_part( card );

        /* low latency mode was introduced in FPGA v3.9.0 and is available
         * for all parts */
        if ( ( part == skiq_z2 && _skiq_meets_fpga_version( card, 3, 9, 1 ) ) ||
             ( part != skiq_z2 && _skiq_meets_fpga_version( card, 3, 9, 0 ) ) )
        {
            status = RX_BLOCK_SIZE_STREAM_LOW_LATENCY;
        }
        else
        {
            status = -ENOTSUP;
        }
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_tx_quadcal_mode() function reads the TX quadrature calibration
    algorithm mode.

    @since Function added in API @b v4.6.0

    @ingroup txfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] transmit handle of interest
    @param[out] p_mode [::skiq_tx_quadcal_mode_t] the currently set value of the 
    TX quadrature calibration mode setting

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_read_tx_quadcal_mode( uint8_t card,
                                            skiq_tx_hdl_t hdl,
                                            skiq_tx_quadcal_mode_t *p_mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_tx_quadcal_mode( &rfic_instance, p_mode );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_write_tx_quadcal_mode() function writes the TX quadrature calibration
    algorithm mode.  If automatic mode is configured, writing the TX LO frequency 
    may result in the TX quadrature calibration algorithm to be run, resulting in 
    the transmission of calibration waveforms which can take a significant amount 
    of time to complete.  If manual mode is configured, it is the user's 
    responsibility to determine when to run the TX quadrature 
    calibration algorithm via ::skiq_run_tx_quadcal().

    @since Function added in API @b v4.6.0

    @ingroup txfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] transmit handle of interest
    @param[in] mode [::skiq_tx_quadcal_mode_t] TX quadrature calibration mode 
    mode to configure

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_write_tx_quadcal_mode( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             skiq_tx_quadcal_mode_t mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_write_tx_quadcal_mode( &rfic_instance, mode );
    UNLOCK_REGS(card);

    return (status);
}

/*****************************************************************************/
/** The skiq_run_tx_quadcal() performs the TX quadrature calibration 
    algorithm based on the current RF IC settings.  

    @note that this may take some time to complete.  Additionally, running of the 
    TX quadrature algorithm results in transmissions of calibration waveforms, 
    resulting in the appearance of erroneous transmissions in the spectrum during 
    execution of the algorithm.  

    @note streaming RX or TX while running the TX quadrature algorithm will result 
    in a momentary gap in received and/or transmitted samples.  It is recommended 
    that this is ran after the desired Tx LO frequency has been configured.

    @attention In the case of Sidekiq X2, calibration is performed on all TX handles, 
    regardless of the handle specified.  

    @since Function added in API @b v4.6.0

    @ingroup txfunctions

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] transmit handle of interest

    @return int32_t  status where 0=success, anything else is an error
*/
EPIQ_API int32_t skiq_run_tx_quadcal( uint8_t card, skiq_tx_hdl_t hdl )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

    // we need to make sure we stop streaming before running quadcal as it will
    // potentially result in us changing sample rates and transmitting / receiving garbage
    LOCK_REGS(card);
    LOCK_TX(card);
    LOCK_RX(card);

    pause_all_rx_streams(card);
    status = rfic_run_tx_quadcal( &rfic_instance );
    resume_and_flush_all_rx_streams(card);

    UNLOCK_RX(card);
    UNLOCK_TX(card);
    UNLOCK_REGS(card);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_read_rx_cal_mode( uint8_t card,
                                        skiq_rx_hdl_t hdl,
                                        skiq_rx_cal_mode_t *p_mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    CHECK_NULL_PARAM( p_mode );
    
    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_rx_cal_mode( &rfic_instance, p_mode );
    UNLOCK_REGS(card);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_write_rx_cal_mode( uint8_t card,
                                         skiq_rx_hdl_t hdl,
                                         skiq_rx_cal_mode_t mode )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_write_rx_cal_mode( &rfic_instance, mode );
    UNLOCK_REGS(card);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_run_rx_cal( uint8_t card, skiq_rx_hdl_t hdl )
{
    int32_t status=0;
    rfic_t rfic_instance;
    rfe_t rfe_instance;
    skiq_rx_hdl_t rx_hdl;
    uint8_t j;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
    rfe_instance = _skiq_get_rx_rfe_instance(card,hdl);

    // we need to make sure we stop streaming before running calibration as it will
    // potentially result in us changing sample rates and transmitting / receiving garbage
    LOCK_REGS(card);
    LOCK_TX(card);
    LOCK_RX(card);

    pause_all_rx_streams(card);

    /* make sure the LNAs are disabled for all the hdls */
    for ( j = 0; j < _skiq_get_nr_rx_hdl( card ); j++)
    {
        rx_hdl = _skiq_get_rx_hdl( card, j );
        if ( rx_hdl < skiq_rx_hdl_end )
        {
            rfe_instance = _skiq_get_rx_rfe_instance(card, rx_hdl);
            rfe_update_rx_lna( &(rfe_instance), lna_disabled );
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, _skiq_get_nr_rx_hdl( card ), j);
        }
    }

    status = rfic_run_rx_cal( &rfic_instance );

    for ( j = 0; j < _skiq_get_nr_rx_hdl( card ); j++)
    {
        rx_hdl = _skiq_get_rx_hdl( card, j );
        if ( rx_hdl < skiq_rx_hdl_end )
        {
            if (is_rx_hdl_streaming(card, rx_hdl))
            {
                rfe_instance = _skiq_get_rx_rfe_instance(card, rx_hdl);
                rfe_update_rx_lna( &(rfe_instance), lna_enabled );
            }
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, _skiq_get_nr_rx_hdl( card ), j);
        }
    }
    resume_and_flush_all_rx_streams(card);

    UNLOCK_RX(card);
    UNLOCK_TX(card);
    UNLOCK_REGS(card);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_read_rx_cal_type_mask( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint32_t *p_cal_mask )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    CHECK_NULL_PARAM( p_cal_mask );
    
    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_rx_cal_mask( &rfic_instance, p_cal_mask );
    UNLOCK_REGS(card);

    return (status);    
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_write_rx_cal_type_mask( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              uint32_t cal_mask )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_write_rx_cal_mask( &rfic_instance, cal_mask );
    UNLOCK_REGS(card);

    return (status);
}

/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_read_rx_cal_types_avail( uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               uint32_t *p_cal_mask )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    CHECK_NULL_PARAM( p_cal_mask );
    
    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

    LOCK_REGS(card);
    status = rfic_read_rx_cal_types_avail( &rfic_instance, p_cal_mask );
    UNLOCK_REGS(card);

    return (status);
}

/*************************************************************************************************/
/** The skiq_read_1pps_source() function reads the currently configured source of the 1PPS signal.

    @since Function added in API @b v4.7.0

    @ingroup ppsfunctions
    @see skiq_write_1pps_source

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_pps_source [::skiq_1pps_source_t] pointer to 1pps source

    @note p_pps_source updated only upon success

    @return int32_t
    @retval 0 Success
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ESRCH Internal error, Sidekiq part misidentified or invalid
*/
EPIQ_API int32_t skiq_read_1pps_source( uint8_t card,
                                        skiq_1pps_source_t *p_pps_source )
{
    int32_t status = 0;
    struct fpga_capabilities fpga_caps;
    uint32_t val=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    switch ( _skiq_get_part( card ) )
    {
        case skiq_mpcie:
        case skiq_m2:
            /* m.2 and mPCIe is always external (not configurable) */
            *p_pps_source = skiq_1pps_source_external;
            break;

        case skiq_x2:
        case skiq_x4:
        case skiq_m2_2280:
        case skiq_z3u:
        case skiq_nv100:
            /* Sidekiq part can possibly be configured as external or host (depending on FPGA
             * capabilities) */
            fpga_caps = _skiq_get_fpga_caps(card);
            if ( fpga_caps.gps_module_support != 0 )
            {
                status = sidekiq_fpga_reg_read( card, FPGA_REG_GPSDO_CONFIG2, &val );
                if(status == 0)
                {
                    if ( RBF_GET( val, GPSDO_RESET_OVERRIDE ) > 0 )
                    {
                        // The PPS_SEL is determined by the GPSDO_CONFIG2 register
                        if ( RBF_GET( val, PPS_SEL_GPS ) > 0 )
                        {
                            *p_pps_source = skiq_1pps_source_host;
                        }
                        else
                        {
                            *p_pps_source = skiq_1pps_source_external;
                        }
                    }
                    else
                    {
                        // The PPS_SEL is determined by the FPGA_CTRL register
                        status = sidekiq_fpga_reg_read( card, FPGA_REG_FPGA_CTRL, &val );
                        if ( status == 0 )
                        {
                            if ( RBF_GET( val, PPS_SEL ) > 0 )
                            {
                                *p_pps_source = skiq_1pps_source_host;
                            }
                            else
                            {
                                *p_pps_source = skiq_1pps_source_external;
                            }
                        }
                    }
                }

                if(status != 0)
                {
                    /* Error occurred transacting with FPGA registers */
                    status = -EBADMSG;
                }
            }
            else
            {
                *p_pps_source = skiq_1pps_source_external;
            }
            break;

        case skiq_z2:
            /* Z2 does not currently support 1PPS functionality */
            *p_pps_source = skiq_1pps_source_unavailable;
            break;

        case skiq_z2p:
            /* Z2+ does not currently support 1PPS functionality */
            *p_pps_source = skiq_1pps_source_unavailable;
            break;

        default:
            /* The `skiq_part_t` is derived from values stored in EEPROM, so it can represent a
             * future product that this libsidekiq version does not support or know about */
            skiq_unknown("Unsupported part %u for card %u\n", _skiq_get_part( card ), card);
            status = -ESRCH;
            break;
    }

    return (status);
}


/*************************************************************************************************/
/** The skiq_write_1pps_source() function configures the source of the 1PPS signal.

    @note Refer to the hardware user's manual for physical location of signal

    @warning Not all sources are available with all Sidekiq products

    @attention Supported sources may depend on FPGA bitstream

    @since Function added in API @b v4.7.0

    @ingroup ppsfunctions
    @see skiq_read_1pps_source

    @param[in] card card index of the Sidekiq of interest
    @param[in] pps_source [::skiq_1pps_source_t] source of 1PPS signal

    @return int32_t
    @retval 0 Success
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EBADMSG Error occurred transacting with FPGA registers
    @retval -ENOSYS FPGA bitstream does not support specified 1PPS source
    @retval -ENOTSUP Sidekiq product does not specified 1PPS source
    @retval -EINVAL Invalid 1PPS source specified
*/
EPIQ_API int32_t skiq_write_1pps_source( uint8_t card,
                                         skiq_1pps_source_t pps_source )
{
    int32_t status = 0;
    skiq_part_t part;
    bool perform_register_update = false;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    if ( ( pps_source != skiq_1pps_source_external ) &&
         ( pps_source != skiq_1pps_source_host ) )
    {
        skiq_error("Invalid 1PPS source (%d) specified for card %u\n", pps_source, card);
        status = -EINVAL;
    }

    /*
      Check Sidekiq part's 1PPS source capabilities, possibly depending on FPGA capabilities

                            external     host
      Sidekiq mPCIe / M.2     Yes         No
      Sidekiq X2              Yes        Maybe
      Sidekiq X4              Yes        Maybe
      Sidekiq M.2-2280        Yes         Yes
      Sidekiq Z2              No          No
      Sidekiq Z2p             No          No
      Sidekiq Z3u             Yes         Yes
      For those parts with 'Maybe' support for host, additionally check the FPGA bitstream
      capabilities.
     */
    if ( status == 0 )
    {
        part = _skiq_get_part( card );
        switch ( part )
        {
            case skiq_mpcie:
            case skiq_m2:
                if( pps_source == skiq_1pps_source_host )
                {
                    status = -ENOTSUP;
                }
                break;

            case skiq_x2:
            case skiq_x4:
            {
                struct fpga_capabilities fpga_caps;

                fpga_caps = _skiq_get_fpga_caps(card);
                if ( ( fpga_caps.gps_module_support == 0 ) &&
                     ( pps_source == skiq_1pps_source_host ) )
                {
                    skiq_error("Configured FPGA bitstream does not support specified 1PPS source "
                               "'%s' for card %u\n", pps_source_cstr( pps_source ), card);
                    status = -ENOSYS;
                }
                else if ( fpga_caps.gps_module_support != 0 )
                {
                    perform_register_update = true;
                }
                break;
            }

            case skiq_m2_2280:
            case skiq_z3u:
            case skiq_nv100:
                perform_register_update = true;
                break;

            case skiq_z2:
            case skiq_z2p:
                status = -ENOTSUP;
                break;

            default:
                status = -EINVAL;
                break;
        }
    }
    /* apply the 1PPS source setting if all of the above checks passed and 'perform_register_update'
     * indicates the FPGA_CTRL register needs an update.
     * The PPS_SEL signal is in the FPGA CTRL register which gets reset FPGA register soft-reset on initialization & exit.
     * This is a problem when we want to maintain the GPSDO lock.
     * To solve this, we use a PPS_SEL override introduced in FPGA V3.16.3. 
     * The override is a MUX that selects which PPS_SEL signal is used.  
     * This MUX control selects which PPS_SEL is is used: GPSDO_CONFIG2 or FPGA CTRL.
     * Out of hard reset, the FPGA sets the PPS_SEL in GPSDO_CONFIG2 and FPGA CTRL
     * to the appropriate state depending on the SDR model.
     * GPSDO_RESET_OVERRIDE: controls the MUX.
     * When the MUX selects GPSDO_CONFIG2, PPS_SEL_GPS controls the PPS source (1 = GPS)
     */
    if ( ( status == 0 ) && perform_register_update )
    {
        bool     update_register = false;
        uint32_t register_value  = 0;
        switch( pps_source )
        {
            case skiq_1pps_source_host:         /**< 1PPS source from host connector */
                register_value  = 1;
                update_register = true;  
                break;

            case skiq_1pps_source_external:    /**< 1PPS source from external connector */
                register_value  = 0;
                update_register = true;  
                break;

            case skiq_1pps_source_unavailable:
                break;
        }

        if( update_register )
        {
            status = sidekiq_fpga_reg_RMWV(card, register_value, PPS_SEL, FPGA_REG_FPGA_CTRL);

            if( status == 0 )
            {
                status = gpsdo_fpga_update_reset_override( card );
            }
            if( status != 0 )
            {
                /* Error occurred transacting with FPGA registers */
                status = -EBADMSG;
            }
        }
    }
    else if ( status == -ENOTSUP )
    {
        skiq_error("Unsupported 1PPS source '%s' for Sidekiq %s at card %u\n",
                   pps_source_cstr( pps_source ), part_cstr( part ), card);
    }

    return (status);
}

/*************************************************************************************************/
/** The skiq_read_iq_complex_multiplier() function provides the complex multiplication factor that
    is currently in use for the supplied receive handle.

    @attention I/Q phase and amplitude multiplication factors are only supported on a subset of
    Sidekiq products and only if the FPGA is @b v3.10.0 or later

    @since Function added in API @b v4.7.0, requires FPGA @b v3.10.0 or later

    @ingroup calfunctions
    @see skiq_read_iq_cal_complex_multiplier
    @see skiq_read_iq_cal_complex_multiplier_by_LO_freq
    @see skiq_write_iq_complex_multiplier_absolute
    @see skiq_write_iq_complex_multiplier_user
    @see skiq_read_iq_complex_cal_data_present

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_factor reference to the complex multiplication factor

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -ENOSYS  Sidekiq platform is not running an FPGA that meets the minimum interface version requirements
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
    @retval -EINVAL  An invalid / unsupported receive handle was specified

 */
EPIQ_API int32_t skiq_read_iq_complex_multiplier( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  float complex *p_factor )
{
    int32_t status=0;

    CHECK_NULL_PARAM( p_factor );
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if ( ( _skiq_get_part( card ) == skiq_x2 ) || ( _skiq_get_part( card ) == skiq_x4 ) )
    {
        LOCK_REGS(card);
        {
            status = fpga_ctrl_read_iq_complex_multiplier( card, hdl, p_factor );
        }
        UNLOCK_REGS(card);
    }
    else
    {
        status = -ENOTSUP;
    }

    return (status);

} /* skiq_read_iq_complex_multiplier */


/*************************************************************************************************/
/** The skiq_write_iq_complex_multiplier_absolute() function overwrites the complex multiplication
    factor that is currently in use for the supplied receive handle.

    @attention I/Q phase and amplitude multiplication factors are only supported on a subset of
    Sidekiq products and only if the FPGA is @b v3.10.0 or later

    @since Function added in API @b v4.7.0, requires FPGA @b v3.10.0 or later

    @ingroup calfunctions
    @see skiq_read_iq_cal_complex_multiplier
    @see skiq_read_iq_cal_complex_multiplier_by_LO_freq
    @see skiq_read_iq_complex_multiplier
    @see skiq_write_iq_complex_multiplier_user
    @see skiq_read_iq_complex_cal_data_present

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] factor complex multiplication factor to overwrite factory calibrated settings

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -ENOSYS  Sidekiq platform is not running an FPGA that meets the minimum interface version requirements
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform

 */
EPIQ_API int32_t skiq_write_iq_complex_multiplier_absolute( uint8_t card,
                                                            skiq_rx_hdl_t hdl,
                                                            float complex factor )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if ( ( _skiq_get_part( card ) == skiq_x2 ) || ( _skiq_get_part( card ) == skiq_x4 ) )
    {
        LOCK_REGS(card);
        {
            status = fpga_ctrl_write_iq_complex_multiplier( card, hdl, factor );
        }
        UNLOCK_REGS(card);
    }
    else
    {
        status = -ENOTSUP;
    }

    return (status);

} /* skiq_write_iq_complex_multiplier */


/*************************************************************************************************/
/** The skiq_write_iq_complex_multiplier_user() function further applies an I/Q phase and amplitude
    correction to the factory specified calibration factors.  This function may be useful to users
    that have a two or four antenna configuration that they wish to "zero" out by applying an
    additional correction factor.

    @attention I/Q phase and amplitude multiplication factors are only supported on a subset of
    Sidekiq products and only if the FPGA is @b v3.10.0 or later

    <pre>
    i'[n] + j*q'[n] = (i[n] + j*q[n])*(re_cal + j*im_cal)*(re_user + j*im_user)
    </pre>

    @since Function added in API @b v4.7.0, requires FPGA @b v3.10.0 or later

    @ingroup calfunctions
    @see skiq_read_iq_cal_complex_multiplier
    @see skiq_read_iq_cal_complex_multiplier_by_LO_freq
    @see skiq_read_iq_complex_multiplier
    @see skiq_write_iq_complex_multiplier_absolute
    @see skiq_read_iq_complex_cal_data_present

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] factor complex multiplication factor to apply in addition to factory calibrated settings

    @return int32_t status where 0=success, anything else is an error
    @retval 0 Success
    @retval -ENOTSUP Card index references a Sidekiq platform that does not currently support this functionality
    @retval -ENOSYS  Sidekiq platform is not running an FPGA that meets the minimum interface version requirements
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform

 */
EPIQ_API int32_t skiq_write_iq_complex_multiplier_user( uint8_t card,
                                                        skiq_rx_hdl_t hdl,
                                                        float complex factor )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if ( ( _skiq_get_part( card ) == skiq_x2 ) || ( _skiq_get_part( card ) == skiq_x4 ) )
    {
        LOCK_REGS(card);
        {
            float complex cal_factor;

            status = skiq_read_iq_cal_complex_multiplier( card, hdl, &cal_factor );
            if ( status == 0 )
            {
                status = skiq_write_iq_complex_multiplier_absolute( card, hdl, cal_factor * factor );
            }
        }
        UNLOCK_REGS(card);
    }
    else
    {
        status = -ENOTSUP;
    }

    return (status);

} /* skiq_write_iq_complex_multiplier_user */


/*************************************************************************************************/
/** The skiq_read_iq_cal_complex_multiplier() function provides the complex multiplication factor
    based on the current settings of the receive handle as determined by factory settings.

    @warning The factors returned by this function may not represent the current factors in use
    whenever they are overwritten by skiq_write_iq_complex_multiplier_absolute() or
    skiq_write_iq_complex_multiplier_user().  Use the skiq_read_iq_complex_multiplier() instead
    to query the current factors.

    @attention IQ phase and amplitude calibration may be present but it is only active if the FPGA
    is @b v3.10.0 or later.

    @since Function added in API @b v4.7.0

    @ingroup calfunctions
    @see skiq_read_iq_cal_complex_multiplier_by_LO_freq
    @see skiq_read_iq_complex_multiplier
    @see skiq_write_iq_complex_multiplier_absolute
    @see skiq_write_iq_complex_multiplier_user
    @see skiq_read_iq_complex_cal_data_present

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_factor reference to the complex multiplication factor

    @return int32_t status where 0=success, anything else is an error
    @retval 0        Success
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform

 */
EPIQ_API int32_t skiq_read_iq_cal_complex_multiplier( uint8_t card,
                                                      skiq_rx_hdl_t hdl,
                                                      float complex *p_factor )
{
    int32_t status = 0;

    CHECK_NULL_PARAM( p_factor );
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS(card);
    {
        uint64_t lo_freq;
        double actual_lo_freq;

        tune_read_rx_LO_freq( card, hdl, &lo_freq, &actual_lo_freq );
        status = card_read_iq_cal_complex_multiplier( card, hdl, lo_freq, OPER_TEMPER_PLACEHOLDER,
                                                      p_factor );
    }
    UNLOCK_REGS(card);

    return (status);

} /* skiq_read_iq_cal_complex_multiplier */


/*************************************************************************************************/
/** The skiq_read_iq_cal_complex_multiplier_by_LO_freq() function provides the complex
    multiplication factor at given a receive LO frequency for the receive handle as determined by
    factory settings.

    @warning The factor returned by this function may not represent the current factor in use.  They
    may have been overwritten by skiq_write_iq_complex_multiplier_absolute() or
    skiq_write_iq_complex_multiplier_user().  Use the skiq_read_iq_complex_multiplier() instead to
    query the factor that is currently in use.

    @attention IQ phase and amplitude calibration data may be present but it is only active if the
    FPGA is @b v3.10.0 or later.

    @since Function added in API @b v4.7.0

    @ingroup calfunctions
    @see skiq_read_iq_cal_complex_multiplier
    @see skiq_read_iq_complex_multiplier
    @see skiq_write_iq_complex_multiplier_absolute
    @see skiq_write_iq_complex_multiplier_user
    @see skiq_read_iq_complex_cal_data_present

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] lo_freq receive LO frequency of interest
    @param[out] p_factor reference to the complex multiplication factor

    @return int32_t status where 0=success, anything else is an error
    @retval 0        Success
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform

 */
EPIQ_API int32_t skiq_read_iq_cal_complex_multiplier_by_LO_freq( uint8_t card,
                                                                 skiq_rx_hdl_t hdl,
                                                                 uint64_t lo_freq,
                                                                 float complex *p_factor )
{
    int32_t status = 0;

    CHECK_NULL_PARAM( p_factor );
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    LOCK_REGS(card);
    {
        status = card_read_iq_cal_complex_multiplier( card, hdl, lo_freq, OPER_TEMPER_PLACEHOLDER,
                                                      p_factor );
    }
    UNLOCK_REGS(card);

    return (status);

} /* skiq_read_iq_cal_complex_multiplier_by_LO_freq */


/*************************************************************************************************/
/** The skiq_read_iq_complex_cal_data_present() function provides an indication for whether or not
    I/Q phase and amplitude calibration data is present for a specified card and handle.

    @warning If the calibration data is not present, there is no default calibration.  As such,
    there will be no IQ phase and amplitude correction.

    @attention I/Q phase and amplitude multiplication factors are only supported on a subset of
    Sidekiq products and only if the FPGA is @b v3.10.0 or later

    @since Function added in API @b v4.7.0

    @ingroup calfunctions
    @see skiq_read_iq_cal_complex_multiplier
    @see skiq_read_iq_cal_complex_multiplier_by_LO_freq
    @see skiq_read_iq_complex_multiplier
    @see skiq_write_iq_complex_multiplier_absolute
    @see skiq_write_iq_complex_multiplier_user

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_present reference to a boolean value indicating data presence

    @return int32_t status where 0=success, anything else is an error
    @retval 0        Success
    @retval -ERANGE  Requested card index is out of range
    @retval -ENODEV  Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform

 */
EPIQ_API int32_t skiq_read_iq_complex_cal_data_present( uint8_t card,
                                                        skiq_rx_hdl_t hdl,
                                                        bool *p_present )
{
    int32_t status = 0;

    CHECK_NULL_PARAM( p_present );
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    *p_present = card_is_iq_complex_cal_data_present( card, hdl );

    return status;

} /* skiq_read_iq_complex_cal_data_present */

/*****************************************************************************/
/** The skiq_read_calibration_date() function reads details on when calibration
    was last performed.  Additionally, a recommended date to perform the next
    calibration is provided.

    @since Function added in API @b v4.7.0

    @param[in] card card index of the Sidekiq of interest
    @param[out] p_last_cal_year pointer to where to store the year when calibration
                was last performed.  
    @param[out] p_last_cal_week pointer to where to store the week number 
                when the calibration was last performed.  The week number with
                the calibration year provides a full represenation of when the calibration
                was performed.
    @param[out] p_cal_interval pointer to where to store the interval (in years) of how
                often calibration should be performed.  The year of the last calibration 
                (adjusted by this interval) along with the week of the last calibration 
                provides a recommendation for when the next calibration should be performed.

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -ENOENT Calibration date information cannot be located
*/
EPIQ_API int32_t skiq_read_calibration_date( uint8_t card,
                                             uint16_t *p_last_cal_year,
                                             uint8_t *p_last_cal_week,
                                             uint8_t *p_cal_interval )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    status = card_read_cal_date( card,
                                 p_last_cal_year,
                                 p_last_cal_week,
                                 p_cal_interval );

    return (status);
}


/*****************************************************************************/
/** The skiq_write_rx_freq_tune_mode() function configures the frequency tune 
    mode for the handle specified.

    @since Function added in API @b v4.10.0

    @note For Sidekiq X4, this configures the tune mode for both receive and transmit
    of the RFIC specified by the RX handle (ex. RX A1/A2/C1 configures RFIC A)

    @note For Sidekiq X2, skiq_freq_tune_mode_hop_on_timestamp is not supported.  
    Additionally, skiq_rx_hdl_B1 is not supported.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] mode [::skiq_tune_mode_t] tune mode

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -ENOTSUP Mode is not supported by hardware
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_write_rx_freq_tune_mode( uint8_t card,
                                               skiq_rx_hdl_t hdl,
                                               skiq_freq_tune_mode_t mode )
{
    int32_t status=0;
    rfic_t rfic_instance;
    skiq_freq_tune_mode_t cache_mode;
    skiq_rx_hdl_t other_hdl = skiq_rx_hdl_end;
    uint32_t rfic_hop_gpio = 0;
    bool on_timestamp = false;
    uint8_t chip_index=0;
        
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    cache_mode = skiq.card[card].rx_list[hdl].tune_mode;
    rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
    other_hdl = rfic_rx_hdl_map_other(&rfic_instance);
    
    LOCK_REGS(card);
    status = rfic_write_rx_freq_tune_mode( &rfic_instance, mode );
    if( status == 0 )
    {
        if( mode == skiq_freq_tune_mode_standard )
        {
            // reset the hop list back to 0
            skiq.card[card].rx_list[hdl].num_hop_freqs = 0;
        }
        else
        {
            // we need to update the FPGA hop on timestamp config
            rfic_hop_gpio = rfic_read_hop_on_ts_gpio( &rfic_instance, &chip_index );
            if( mode == skiq_freq_tune_mode_hop_on_timestamp )
            {
                on_timestamp = true;
            }
            status = fpga_ctrl_config_hop_on_timestamp( card,
                                                        on_timestamp,
                                                        rfic_hop_gpio,
                                                        chip_index );
        }
    }

    if( status != 0 )
    {
        // restore the previous config
        rfic_write_rx_freq_tune_mode( &rfic_instance, cache_mode );
    }
    else
    {
        // save the updated mode for this hdl and other
        if ( other_hdl != skiq_rx_hdl_end )
        {
            skiq.card[card].rx_list[other_hdl].tune_mode = mode;
        }
        skiq.card[card].rx_list[hdl].tune_mode = mode;
    }
    
    UNLOCK_REGS(card);

    return (status);
}


/*****************************************************************************/
/** The skiq_read_rx_freq_tune_mode() function reads the configured frequency 
    tune mode for the handle specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_mode [::skiq_tune_mode_t] pointer to tune mode

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_read_rx_freq_tune_mode( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );
    
    *p_mode = skiq.card[card].rx_list[hdl].tune_mode;

    return (status);
}


/*****************************************************************************/
/** The skiq_write_tx_freq_tune_mode() function configures the frequency tune 
    mode for the handle specified.

    @since Function added in API @b v4.10.0

    @note For Sidekiq X4, this configures the tune mode for both receive and transmit
    of the RFIC specified by the TX handle (ex. TX A1/A2 configures RFIC A)

    @note For Sidekiq X2, skiq_freq_tune_mode_hop_on_timestamp is not supported.  

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] transmit handle of interest
    @param[in] mode [::skiq_tune_mode_t] tune mode

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -ENOTSUP Mode is not supported by hardware
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_write_tx_freq_tune_mode( uint8_t card,
                                               skiq_tx_hdl_t hdl,
                                               skiq_freq_tune_mode_t mode )
{
    int32_t status=0;
    rfic_t rfic_instance;
    skiq_freq_tune_mode_t cache_mode;
    skiq_tx_hdl_t other_hdl = skiq_tx_hdl_end;
    uint32_t rfic_hop_gpio = 0;
    bool on_timestamp = false;
    uint8_t chip_index=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );
    
    cache_mode = skiq.card[card].tx_list[hdl].tune_mode;
    rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
    other_hdl = rfic_tx_hdl_map_other(&rfic_instance);
    
    LOCK_REGS(card);
    status = rfic_write_tx_freq_tune_mode( &rfic_instance, mode );
    if( status == 0 )
    {
        if( mode == skiq_freq_tune_mode_standard )
        {
            // reset the hop list back to 0
            skiq.card[card].tx_list[hdl].num_hop_freqs = 0;
        }
        else
        {
            // we need to update the FPGA hop on timestamp config
            rfic_hop_gpio = rfic_read_hop_on_ts_gpio( &rfic_instance, &chip_index );
            if( mode == skiq_freq_tune_mode_hop_on_timestamp )
            {
                on_timestamp = true;
            }
            status = fpga_ctrl_config_hop_on_timestamp( card,
                                                        on_timestamp,
                                                        rfic_hop_gpio,
                                                        chip_index);
        }
    }

    if( status != 0 )
    {
        // restore the previous config
        rfic_write_tx_freq_tune_mode( &rfic_instance, cache_mode );
    }
    else
    {
        // save the updated mode for this hdl and other
        if ( other_hdl != skiq_tx_hdl_end )
        {
            skiq.card[card].tx_list[other_hdl].tune_mode = mode;
        }
        skiq.card[card].tx_list[hdl].tune_mode = mode;
    }
    
    UNLOCK_REGS(card);

    return (status);
}


/*****************************************************************************/
/** The skiq_read_tx_freq_tune_mode() function reads the configured frequency 
    tune mode for the handle specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] receive handle of interest
    @param[out] p_mode [::skiq_tune_mode_t] pointer to tune mode

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform    
*/
EPIQ_API int32_t skiq_read_tx_freq_tune_mode( uint8_t card,
                                              skiq_tx_hdl_t hdl,
                                              skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );
    
    *p_mode = skiq.card[card].tx_list[hdl].tune_mode;

    return (status);
}



/*****************************************************************************/
/** The skiq_write_rx_freq_hop_list() function configures the frequency hop 
    list to the values specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] num_freq number of frequencies included in freq_list;
               this value cannot exceed ::SKIQ_MAX_NUM_FREQ_HOPS
    @param[in] freq_list list of frequencies supported in hopping list
    @param[in] initial_index initial index of frequency for first hop

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range or # freqs out of range or initial index out of range 
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM   Requested handle is not available or out of range for the Sidekiq platform
    @retval -EINVAL freq_list contains invalid frequency
*/
EPIQ_API int32_t skiq_write_rx_freq_hop_list( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              uint16_t num_freq,
                                              uint64_t freq_list[],
                                              uint16_t initial_index )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    rfe_t rfe_instance;
    uint64_t lo_freq_min;
    uint64_t lo_freq_max;
    uint16_t i=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    // validate freq range
    lo_freq_min = skiq.card[card].rx_list[hdl].params.lo_freq_min;
    lo_freq_max = skiq.card[card].rx_list[hdl].params.lo_freq_max;
    for( i=0; i<num_freq; i++ )
    {
        if( (freq_list[i] < lo_freq_min) ||
            (freq_list[i] > lo_freq_max) )
        {
            skiq_info("Requested frequency %" PRIu64 " is not within %" PRIu64 "-%" PRIu64 " Hz",
                      freq_list[i], lo_freq_min, lo_freq_max);
            status = -EINVAL;
        }
    }

    if( (skiq.card[card].rx_list[hdl].tune_mode != skiq_freq_tune_mode_standard) && (status != -EINVAL) )
    {
        if( (num_freq <= SKIQ_MAX_NUM_FREQ_HOPS) && (num_freq > 0) && (initial_index < num_freq) )
        {
            rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
            rfe_instance = _skiq_get_rx_rfe_instance(card,hdl);
            LOCK_REGS(card);
            status = rfic_config_rx_hop_list( &rfic_instance, num_freq, freq_list, initial_index );
            if( (status == 0) &&
                (skiq.card[card].rx_list[hdl].tune_mode == skiq_freq_tune_mode_hop_on_timestamp) )
            {
                // set up the filters
                status = rfe_write_rx_freq_hop_list( &rfe_instance, num_freq, freq_list );
            }
            UNLOCK_REGS(card);
            if( status == 0 )
            {
                skiq.card[card].rx_list[hdl].num_hop_freqs = num_freq;
            }
        }
        else
        {
            status = -ERANGE;
        }
    }
    else
    {
        if( status != -EINVAL )
        {
            skiq_warning("Tune mode must be configured first (hdl=%s, card=%u)",
                         rx_hdl_cstr(hdl), card);
        }
    }

    return (status);
}


/*****************************************************************************/
/** The skiq_read_rx_freq_hop_list() function populates the frequency hop list 
    with the frequency values previously specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_num_freq pointer to number of frequencies included in list
    @param[out] freq_list hopping list currently configured; this list should 
                be able to hold at least ::SKIQ_MAX_NUM_FREQ_HOPS

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_read_rx_freq_hop_list( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint16_t *p_num_freq,
                                             uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if( skiq.card[card].rx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
    {
        rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);

        LOCK_REGS(card);
        status = rfic_read_rx_hop_list( &rfic_instance, p_num_freq, freq_list );
        UNLOCK_REGS(card);
    }    

    return (status);
}


/*****************************************************************************/
/** The skiq_write_tx_freq_hop_list() function configures the frequency hop list 
    to the values specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] receive handle of interest
    @param[in] num_freq number of frequencies included in freq_list
               this value cannot exceed ::SKIQ_MAX_NUM_FREQ_HOPS
    @param[in] freq_list list of frequencies supported in hopping list
    @param[in] initial_index initial index of frequency for first hop

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range or # freqs out of range or initial index out of range 
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
    @retval -EINVAL freq_list contains invalid frequency
*/
EPIQ_API int32_t skiq_write_tx_freq_hop_list( uint8_t card,
                                              skiq_tx_hdl_t hdl,
                                              uint16_t num_freq,
                                              uint64_t freq_list[],
                                              uint16_t initial_index )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    rfe_t rfe_instance;
    uint64_t lo_freq_min;
    uint64_t lo_freq_max;
    uint16_t i=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    // validate freq range
    lo_freq_min = skiq.card[card].tx_list[hdl].params.lo_freq_min;
    lo_freq_max = skiq.card[card].tx_list[hdl].params.lo_freq_max;
    for( i=0; i<num_freq; i++ )
    {
        if( (freq_list[i] < lo_freq_min) ||
            (freq_list[i] > lo_freq_max) )
        {
            skiq_info("Requested frequency %" PRIu64 " is not within %" PRIu64 "-%" PRIu64 " Hz",
                      freq_list[i], lo_freq_min, lo_freq_max);
            status = -EINVAL;
        }
    }


    if( (skiq.card[card].tx_list[hdl].tune_mode != skiq_freq_tune_mode_standard) && (status != -EINVAL) )
    {
        if( (num_freq <= SKIQ_MAX_NUM_FREQ_HOPS) && (num_freq > 0) && (initial_index < num_freq) )
        {
            rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
            rfe_instance = _skiq_get_tx_rfe_instance(card,hdl);
            LOCK_REGS(card);
            status = rfic_config_tx_hop_list( &rfic_instance, num_freq, freq_list, initial_index );
            if( (status == 0) &&
                (skiq.card[card].tx_list[hdl].tune_mode == skiq_freq_tune_mode_hop_on_timestamp) )
            {
                // set up the filters
                status = rfe_write_tx_freq_hop_list( &rfe_instance, num_freq, freq_list );
            }
            UNLOCK_REGS(card);
            if( status == 0 )
            {
                skiq.card[card].tx_list[hdl].num_hop_freqs = num_freq;
            }
        }
        else
        {
            status = -ERANGE;
        }
    }
    else
    {
        if( status != -EINVAL )
        {
            skiq_warning("Tune mode must be configured first (hdl=%s, card=%u)",
                         tx_hdl_cstr(hdl), card);
        }
    }

    return (status);
}


/*****************************************************************************/
/** The skiq_read_tx_freq_hop_list() function populates the frequency hop list 
    with the values previously specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] receive handle of interest
    @param[out] p_num_freq pointer to number of frequencies included in list
    @param[out] freq_list hopping list currently configured; this list should 
                be able to hold at least ::SKIQ_MAX_NUM_FREQ_HOPS

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_read_tx_freq_hop_list( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint16_t *p_num_freq,
                                             uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    if( skiq.card[card].tx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
    {
        rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);

        LOCK_REGS(card);
        status = rfic_read_tx_hop_list( &rfic_instance, p_num_freq, freq_list );
        UNLOCK_REGS(card);
    }    

    return (status);
}


/*****************************************************************************/
/** The skiq_write_next_rx_freq_hop() function performs the various 
    configuration required to support the next frequency hop but does not 
    execute the hop until skiq_perform_rx_freq_hop() is called.

    @since Function added in API @b v4.10.0

    @note For Sidekiq X4, this updates both the RX and TX LO frequency.
    @note For any radio based on the AD9361 RF IC (mPCIe, m.2, Z2), when operating
    in the ::skiq_freq_tune_mode_hop_on_timestamp, this updates both the RX and TX LO frequency
    based on the index specified.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] freq_index index into hopping list of frequency to configure

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range or freq index out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_write_next_rx_freq_hop( uint8_t card,
                                              skiq_rx_hdl_t hdl,
                                              uint16_t freq_index )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if( skiq.card[card].rx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
    {
        if( freq_index < skiq.card[card].rx_list[hdl].num_hop_freqs )
        {
            rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
    
            LOCK_REGS(card);
            status = rfic_write_next_rx_hop( &rfic_instance, freq_index );
            UNLOCK_REGS(card);
        }
        else
        {
            status = -ERANGE;
        }
    }
    else
    {
        skiq_warning("Tune mode must be configured first (hdl==%s, card=%u)",
                     rx_hdl_cstr(hdl), card);
    }
    
    return (status);
}


/*****************************************************************************/
/** The skiq_write_next_tx_freq_hop() function performs the various 
    configuration required to support the next frequency hop but does not 
    execute the hop until skiq_perform_tx_freq_hop() is called.

    @since Function added in API @b v4.10.0

    @note For Sidekiq X4, this updates both the RX and TX LO frequency.
    @note For any radio based on the AD9361 RF IC (mPCIe, m.2, Z2), when operating
    in the ::skiq_freq_tune_mode_hop_on_timestamp, this updates both the RX and TX LO frequency
    based on the index specified.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] transmit handle of interest
    @param[in] freq_index index into hopping list of frequency to configure

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range or freq index out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_write_next_tx_freq_hop( uint8_t card,
                                              skiq_tx_hdl_t hdl,
                                              uint16_t freq_index )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    if( skiq.card[card].tx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
    {
        if( freq_index < skiq.card[card].tx_list[hdl].num_hop_freqs )
        {
            rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
    
            LOCK_REGS(card);
            status = rfic_write_next_tx_hop( &rfic_instance, freq_index );
            UNLOCK_REGS(card);
        }
        else
        {
            status = -ERANGE;
        }
    }
    else
    {
        skiq_warning("Tune mode must be configured first (hdl=%s, card=%u)",
                     tx_hdl_cstr(hdl), card);
    }
    
    return (status);
}


/*****************************************************************************/
/** The skiq_perform_rx_freq_hop() function performs the frequency hop for the 
    handle specified.

    @since Function added in API @b v4.10.0

    @note For Sidekiq X4, this updates both the RX and TX LO frequency.
    @note For any radio based on the AD9361 RF IC (mPCIe, m.2, Z2), when operating
    in the ::skiq_freq_tune_mode_hop_on_timestamp, this updates both the RX and TX LO frequency
    based on the index specified.
    @note if operating in ::skiq_freq_tune_mode_hop_on_timestamp and a rf_timestamp that has
    already passed is specified, the frequency hop will be executed immediately.  If
    running in ::skiq_freq_tune_mode_hop_immediate, the timestamp parameter is ignored.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[in] rf_timestamp timestamp to execute the hop (only for ::skiq_freq_tune_mode_hop_on_timestamp)

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init() or error applying hopping in to RF IC
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_perform_rx_freq_hop( uint8_t card,
                                           skiq_rx_hdl_t hdl,
                                           uint64_t rf_timestamp )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    freq_hop_t hop;
    
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if( skiq.card[card].rx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
    {
        rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
        if( (status=rfic_read_next_rx_hop( &rfic_instance, &hop )) == 0 )
        {
            LOCK_REGS(card);
            status = tune_write_rx_LO_freq( card, hdl, hop.freq,
                                            skiq.card[card].rx_list[hdl].tune_mode, rf_timestamp );
            UNLOCK_REGS(card);
        }
    }
    else
    {
        skiq_warning("Tune mode must be configured first (hdl=%s, card=%u)",
                     tx_hdl_cstr(hdl), card);
    }
    
    return (status);
}

/*****************************************************************************/
/** The skiq_perform_tx_freq_hop() function performs the frequency hop for the 
    handle specified.

    @since Function added in API @b v4.10.0

    @note For Sidekiq X4, this updates both the RX and TX LO frequency.
    @note For any radio based on the AD9361 RF IC (mPCIe, m.2, Z2), when operating
    in the ::skiq_freq_tune_mode_hop_on_timestamp, this updates both the RX and TX LO frequency
    based on the index specified.
    @note if operating in ::skiq_freq_tune_mode_hop_on_timestamp and a rf_timestamp that has
    already passed is specified, the frequency hop will be executed immediately.  If
    running in ::skiq_freq_tune_mode_hop_immediate, the timestamp parameter is ignored.

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] receive handle of interest
    @param[in] rf_timestamp timestamp to execute the hop (only for ::skiq_freq_tune_mode_hop_on_timestamp)

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_perform_tx_freq_hop( uint8_t card,
                                           skiq_tx_hdl_t hdl,
                                           uint64_t rf_timestamp )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    freq_hop_t hop;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    if( skiq.card[card].tx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
    {
        rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
        if( (status=rfic_read_next_tx_hop( &rfic_instance, &hop )) == 0 )
        {
            LOCK_REGS(card);
            status = tune_write_tx_LO_freq( card, hdl, hop.freq,
                                            skiq.card[card].tx_list[hdl].tune_mode, rf_timestamp );
            UNLOCK_REGS(card);
        }
    }
    else
    {
        skiq_warning("Tune mode must be configured first (hdl=%s, card=%u)",
                     tx_hdl_cstr(hdl), card);
    }

    return (status);
}


/***********************************************************/
/* function documentation is in the associated header file */
/***********************************************************/
EPIQ_API int32_t skiq_read_curr_rx_freq_hop( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint16_t *p_hop_index,
                                             uint64_t *p_curr_freq )
{
    int32_t status = -EPROTO;
    rfic_t rfic_instance;
    freq_hop_t hop;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if( p_hop_index == NULL || p_curr_freq == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        if( skiq.card[card].rx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
        {
            rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
            status = rfic_read_curr_rx_hop( &rfic_instance, &hop );
        }

        if( status == 0 )
        {
            *p_hop_index = hop.index;
            *p_curr_freq = hop.freq;
        }
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_next_rx_freq_hop() function reads the next frequency hopping 
    configuration for the handle specified.  This is the configuration that 
    will be applied the next "perform hop" function is called.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_rx_hdl_t] receive handle of interest
    @param[out] p_hop_index pointer to the current hopping index
    @param[out] p_curr_freq pointer to the current frequency

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EINVAL Invalid pointers provided
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_read_next_rx_freq_hop( uint8_t card,
                                             skiq_rx_hdl_t hdl,
                                             uint16_t *p_hop_index,
                                             uint64_t *p_curr_freq )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    freq_hop_t hop;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    if( p_hop_index == NULL || p_curr_freq == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        if( skiq.card[card].rx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
        {
            rfic_instance = _skiq_get_rx_rfic_instance(card,hdl);
            status = rfic_read_next_rx_hop( &rfic_instance, &hop );
        }
        if( status == 0 )
        {
            *p_hop_index = hop.index;
            *p_curr_freq = hop.freq;
        }
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_curr_tx_freq_hop() function reads the current frequency 
    hopping configuration for the handle specified.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] receive handle of interest
    @param[out] p_hop_index pointer to the current hopping index
    @param[out] p_curr_freq pointer to the current frequency

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EINVAL Invalid pointers provided
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_read_curr_tx_freq_hop( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint16_t *p_hop_index,
                                             uint64_t *p_curr_freq )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    freq_hop_t hop;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    if( p_hop_index == NULL || p_curr_freq == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        if( skiq.card[card].tx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
        {
            rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
            status = rfic_read_curr_tx_hop( &rfic_instance, &hop );
        }
        if( status == 0 )
        {
            *p_hop_index = hop.index;
            *p_curr_freq = hop.freq;
        }
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_read_next_tx_freq_hop() function reads the next frequency hopping 
    configuration for the handle specified.  This is the configuration that 
    will be applied the next "perform hop" function is called.

    @since Function added in API @b v4.10.0

    @param[in] card card index of the Sidekiq of interest
    @param[in] hdl [::skiq_tx_hdl_t] receive handle of interest
    @param[out] p_hop_index pointer to the current hopping index
    @param[out] p_curr_freq pointer to the current frequency

    @return int32_t
    @retval 0 successful
    @retval -ERANGE Requested card index is out of range
    @retval -ENODEV Requested card index is not initialized using skiq_init()
    @retval -EINVAL Invalid pointers provided
    @retval -EPROTO Tune mode is not hopping
    @retval -EDOM    Requested handle is not available or out of range for the Sidekiq platform
*/
EPIQ_API int32_t skiq_read_next_tx_freq_hop( uint8_t card,
                                             skiq_tx_hdl_t hdl,
                                             uint16_t *p_hop_index,
                                             uint64_t *p_curr_freq )
{
    int32_t status=-EPROTO;
    rfic_t rfic_instance;
    freq_hop_t hop;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    if( p_hop_index == NULL || p_curr_freq == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        if( skiq.card[card].tx_list[hdl].tune_mode != skiq_freq_tune_mode_standard )
        {
            rfic_instance = _skiq_get_tx_rfic_instance(card,hdl);
            status = rfic_read_next_tx_hop( &rfic_instance, &hop );
        }
        if( status == 0 )
        {
            *p_hop_index = hop.index;
            *p_curr_freq = hop.freq;
        }
    }

    return (status);
}


/**************************************************************************************************/
/**
   @brief This function is responsible for programming the FPGA from an image stored in flash at the
   specified slot.
*/
EPIQ_API int32_t skiq_prog_fpga_from_flash_slot( uint8_t card,
                                                 uint8_t slot )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);

    status = _prepare_for_fpga_reload( card );

    if ( status == 0 )
    {
        card_mgr_fpga_programming_lock();
        {
            status = flash_load_fpga_by_slot(card, slot);
            if ( status != 0 )
            {
                /* a lot of different (some undocumented) error codes can be returned from
                 * flash_load_fpga_default(), so squash them to -EIO to let the user that something
                 * happened with this function.  The extraneous skiq_log() messages should be enough
                 * to distinguish what really happened */
                status = -EIO;
            }
        }
        card_mgr_fpga_programming_unlock();
    }

    if ( status == 0 )
    {
        status = _perform_post_fpga_reload_actions( card );
    }

    UNLOCK_REGS(card);

    return (status);

} /* skiq_prog_fpga_from_flash_slot */


/**************************************************************************************************/
/**
   @brief This function stores a FPGA bitstream into flash memory at the specified slot.  If the
   slot is @c 0, it is automatically loaded on power cycle or calling @c
   skiq_prog_fpga_from_flash(card).  If the slot is greater than @c 0 (and the card has more than
   one slot available), the FPGA configuration can be loaded by calling @c
   skiq_prog_fpga_from_flash_slot(card, slot) with the same specified @p slot value.
*/
EPIQ_API int32_t skiq_save_fpga_config_to_flash_slot( uint8_t card,
                                                      uint8_t slot,
                                                      FILE *p_file,
                                                      uint64_t metadata )
{
    int32_t status = 0;

    /* make sure the requested card is active */
    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_file );

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        skiq_error("Saving FPGA to flash using network interface not currently supported (card=%u)",
                   card);
        return (-ENODEV);
    }

    LOCK_REGS(card);
    {
        status = flash_save_fpga_config_to_flash_slot( card, slot, p_file, metadata );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_save_fpga_config_to_flash_slot */


/**************************************************************************************************/
/**
   @brief This function verifies the contents of flash memory at a specified against the provided
   FILE reference @p p_file and @p metadata. This can be used to validate that a given FPGA
   bitstream and its metadata are accurately stored within flash memory.
*/
EPIQ_API int32_t skiq_verify_fpga_config_in_flash_slot( uint8_t card,
                                                        uint8_t slot,
                                                        FILE *p_file,
                                                        uint64_t metadata )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_file );

    if( sidekiq_rpc_is_client_avail(card) == true )
    {
        skiq_error("Saving FPGA to flash using network interface not currently supported (card=%u)",
                   card);
        return (-ENODEV);
    }

    LOCK_REGS(card);
    {
        status = flash_verify_fpga_config_in_flash_slot( card, slot, p_file, metadata );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_verify_fpga_config_in_flash_slot */


/**************************************************************************************************/
/**
   @brief This function reads the stored metadata associated with the specified slot value.
*/
EPIQ_API int32_t skiq_read_fpga_config_flash_slot_metadata( uint8_t card,
                                                            uint8_t slot,
                                                            uint64_t *p_metadata )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_metadata );

    LOCK_REGS(card);
    {
        status = flash_read_fpga_config_flash_slot_metadata( card, slot, p_metadata );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_read_fpga_config_flash_slot_metadata */


/**************************************************************************************************/
/**
   @brief This function uses calls to skiq_read_fpga_config_flash_slots_avail() and
   skiq_read_fpga_config_flash_slot_metadata() to provide the caller with the lowest slot index
   whose metadata matches the specified @p metadata.
*/
EPIQ_API int32_t skiq_find_fpga_config_flash_slot_metadata( uint8_t card,
                                                            uint64_t metadata,
                                                            uint8_t *p_slot )
{
    int32_t status = 0;
    uint8_t slot_idx, nr_slots = 0;
    bool found_metadata = false;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_slot );

    status = skiq_read_fpga_config_flash_slots_avail( card, &nr_slots );
    for (slot_idx = 0; ( slot_idx < nr_slots ) && ( status == 0 ) && !found_metadata; slot_idx++)
    {
        uint64_t slot_metadata = 0;

        LOCK_REGS(card);
        {
            status = flash_read_fpga_config_flash_slot_metadata( card, slot_idx, &slot_metadata );
        }
        UNLOCK_REGS(card);
        if ( ( status == 0 ) && ( slot_metadata == metadata ) )
        {
            found_metadata = true;
            *p_slot = slot_idx;
        }
    }

    if ( !found_metadata )
    {
        status = -ESRCH;
    }

    return status;

} /* skiq_find_fpga_config_flash_slot_metadata */


/**************************************************************************************************/
/**
   @brief This function provides the number of FPGA configuration slots available for a specified
   Sidekiq card.
*/
EPIQ_API int32_t skiq_read_fpga_config_flash_slots_avail( uint8_t card,
                                                          uint8_t *p_nr_slots )
{
    int32_t status = 0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_nr_slots );

    LOCK_REGS(card);
    {
        status = flash_read_fpga_config_flash_slots_avail( card, p_nr_slots );
    }
    UNLOCK_REGS(card);

    return status;

} /* skiq_read_fpga_config_flash_slots_avail */


/**************************************************************************************************/
/**
    @brief  Set the state of the exit handler.
*/
EPIQ_API int32_t skiq_set_exit_handler_state( bool enabled )
{
    LOCK(use_exit_handler_mutex);
    skiq.use_exit_handler = enabled;
    UNLOCK(use_exit_handler_mutex);

    skiq_debug("The libsidekiq exit handler is now %s\n", (enabled) ? "ENABLED" : "DISABLED");

    return 0;
}

/**************************************************************************************************/
/**
   @brief This function sets the method (GPIO or SW) which is used to enable Rx channels
*/
EPIQ_API int32_t skiq_write_rx_rfic_pin_ctrl_mode( uint8_t card,
                                                   skiq_rx_hdl_t hdl,
                                                   skiq_rfic_pin_mode_t mode )
{
    int32_t status = 0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    
    skiq_part_t part = _skiq_get_part( card );
    
    rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    status = rfic_write_rx_enable_mode( &rfic_instance, mode);

    if ( status == -ENOTSUP )
    {
        skiq_error("%s() is unsupported for Sidekiq %s at card %u\n",
                   __FUNCTION__, part_cstr( part ), card);
    }

    return status;

} /* skiq_write_rx_rfic_pin_ctrl_mode */


/**************************************************************************************************/
/**
   @brief This function allows the user to switch between different reference clock sources
*/
EPIQ_API int32_t skiq_write_ref_clock_select(uint8_t card, skiq_ref_clock_select_t ref_clock_source)
{
    int32_t status = 0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if( ref_clock_source >= skiq_ref_clock_invalid )
    {
        skiq_error("Invalid reference source selected\n");
        return -EINVAL;
    }

    skiq_part_t part = _skiq_get_part(card);

    // M.2 and mPCIe cards do not have hardware that supports this functionality
    if( (part == skiq_m2) ||
        (part == skiq_mpcie) )
    {
        status = -ENOTSUP;
        skiq_error("Writing the ref_clock source not supported for Sidekiq %s"
                  "on card %" PRIu8 "\n", part_cstr(part), card);
        goto skiq_write_ref_clock_select_exit;
    }

    // Switching to the same reference clock source as the current reference clock source does not require any action
    skiq_ref_clock_select_t current_ref_clock_source;
    status = skiq_read_ref_clock_select(card, &current_ref_clock_source);
    if( status != 0 )
    {
        skiq_warning("Unable to read current reference clock source on card %" PRIu8 " with status %" PRIi32 "\n", card, status);
        goto skiq_write_ref_clock_select_exit;
    }

    if( ref_clock_source == current_ref_clock_source )
    {
        skiq_info("No action required to switch reference clock source");
        goto skiq_write_ref_clock_select_exit;
    }

    LOCK_REGS(card);
    {
        // save the init level of the card for re-init after switching the ref clock
        skiq_xport_init_level_t init_level = skiq_xport_card_get_level(card);
        if( init_level == skiq_xport_init_level_full )
        {
            status = _skiq_exit_rf_full(card);
            if( status != 0 )
            {
                skiq_error("Unable to disable card %" PRIu8 " to prep for switching reference clock with status %" PRIi32 "\n", card, status);
                UNLOCK_REGS(card);
                goto skiq_write_ref_clock_select_exit;
            }
        }

        _skiq_save_ref_clock(card, ref_clock_source);

        if( init_level == skiq_xport_init_level_full )
        {
            status = _skiq_init_rf_full(card);
            if( status != 0 )
            {
                // if full initialization fails reset the ref clock back to its initial value to preserve card functionality
                _skiq_save_ref_clock(card, current_ref_clock_source);
                skiq_error("Unable to enable card %" PRIu8 " after switching reference clock with status %" PRIi32 ", "
                            "resetting reference clock back to its original value\n", card, status);
            }
        }
    }
    UNLOCK_REGS(card);

skiq_write_ref_clock_select_exit:
    return status;
} /* skiq_write_ref_clock_select */

/**************************************************************************************************/
/**
   @brief This function allows the user to switch the external clock frequency
*/
EPIQ_API int32_t skiq_write_ext_ref_clock_freq(uint8_t card, uint32_t ext_freq)
{
    int32_t status = -ENOTSUP;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    skiq_part_t part = _skiq_get_part(card);

    /* This is only available to Stretch and NV100, do initial checking to make sure that the
        card is supported and external frequency provided is supported by the card */

    if((part != skiq_m2_2280) &&
        (part != skiq_nv100))
    {
        skiq_error("Writing the ref_clock frequency not supported for Sidekiq %s"
                  "on card %" PRIu8 "\n", part_cstr(part), card);
        return status;
    }

    // Both Stretch and NV100 support 10MHz and 40MHz frequencies for external reference clocks
    if( (ext_freq != SIGNAL_10MHZ) &&
        (ext_freq != SIGNAL_40MHZ) )
    {
        // Stretch supports a 30.72MHz external reference clock frequency as well
        if((part == skiq_m2_2280) &&
            (ext_freq != SIGNAL_30_72MHZ))
        {
            status = -EINVAL;
            skiq_error("Invalid external frequency selected for Sidekiq %s on card %" PRIu8 "\n",
                        part_cstr(part), card);
            return status;
        }
    }

    LOCK_REGS(card);
    {
        // save the init level of the card for re-init after switching the ref clock
        skiq_xport_init_level_t init_level = skiq_xport_card_get_level(card);
        if(init_level == skiq_xport_init_level_full)
        {
            status = _skiq_exit_rf_full(card);
            if(status != 0)
            {
                skiq_error("Unable to disable card %" PRIu8 " to prep for switching ref clock frequency with status %" PRIi32 "\n", card, status);
                UNLOCK_REGS(card);
                return status;
            }
        }

        if(part == skiq_m2_2280)
        {
            status = m2_2280_io_set_ext_ref_clock_source(card, ext_freq);
        }
        else if(part == skiq_nv100)
        {
            status = nv100_select_external_ref_clock(card, ext_freq);
        }
        if(status != 0)
        {
            skiq_error("Unable to select an external clock source and set the frequency for Sidekiq %s"
                        "on card %" PRIu8 "with status %" PRIi32 "\n", part_cstr(part), card, status);
            UNLOCK_REGS(card);
            return status;
        }

        if(init_level == skiq_xport_init_level_full)
        {
            status = _skiq_init_rf_full(card);
            if( status != 0 )
            {
                skiq_error("Unable to enable card %" PRIu8 " after switching ref clock frequency with status %" PRIi32 "\n", card, status);
            }
        }
    }
    UNLOCK_REGS(card);

    return status;
} /* skiq_write_ext_ref_clock_freq */


/**************************************************************************************************/
/**
   @brief This function sets the method (GPIO or SW) which is used to enable Tx channels
*/
EPIQ_API int32_t skiq_write_tx_rfic_pin_ctrl_mode( uint8_t card,
                                                   skiq_tx_hdl_t hdl,
                                                   skiq_rfic_pin_mode_t mode )
{
    int32_t status = 0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    skiq_part_t part = _skiq_get_part( card );

    rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    status = rfic_write_tx_enable_mode( &rfic_instance, mode);

    if ( status == -ENOTSUP )
    {
        skiq_error("%s() is unsupported for Sidekiq %s at card %u\n",
                   __FUNCTION__, part_cstr( part ), card);
    }

    return status;

} /* skiq_write_tx_rfic_pin_ctrl_mode */




/**************************************************************************************************/
/**
   @brief This function reads the method (GPIO or SW) which is used to enable Rx channels
*/
EPIQ_API int32_t skiq_read_rx_rfic_pin_ctrl_mode( uint8_t card,
                                                  skiq_rx_hdl_t hdl,
                                                  skiq_rfic_pin_mode_t *p_mode )
{
    int32_t status = 0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_mode );

    skiq_part_t part = _skiq_get_part( card );

    rfic_instance = _skiq_get_rx_rfic_instance(card, hdl);
    status = rfic_read_rx_enable_mode( &rfic_instance, p_mode);

    if ( status == -ENOTSUP )
    {
        skiq_error("%s() is unsupported for Sidekiq %s at card %u\n",
                   __FUNCTION__, part_cstr( part ), card);
    }

    return status;

} /* skiq_read_rx_rfic_pin_ctrl_mode */

/**************************************************************************************************/
/**
   @brief This function reads the method (GPIO or SW) which is used to enable Tx channels
*/
EPIQ_API int32_t skiq_read_tx_rfic_pin_ctrl_mode( uint8_t card,
                                                  skiq_tx_hdl_t hdl,
                                                  skiq_rfic_pin_mode_t *p_mode )
{
    int32_t status = 0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_mode );

    skiq_part_t part = _skiq_get_part( card );

    rfic_instance = _skiq_get_tx_rfic_instance(card, hdl);
    status = rfic_read_tx_enable_mode( &rfic_instance, p_mode);

    if ( status == -ENOTSUP )
    {
        skiq_error("%s() is unsupported for Sidekiq %s at card %u\n",
                   __FUNCTION__, part_cstr( part ), card);
    }

    return status;

} /* skiq_read_tx_rfic_pin_ctrl_mode */

/**************************************************************************************************/
/**
   @brief This function determines if an RFIC / RFE configurations need to be updated
*/
int32_t _skiq_update_rfic_port_mapping( uint8_t card )
{
    int32_t status=0;
    uint8_t num_chans;
    uint8_t hdl_idx = 0;
    uint8_t curr_rfic_port=0;
    rfe_t rfe_instance;
    rfic_t rfic_instance;
    uint8_t i=0;
    uint8_t a_port=0;
    uint8_t b_port=0;
    skiq_rx_hdl_t rx_hdl;
    skiq_tx_hdl_t tx_hdl;

    num_chans = _skiq_get_nr_rx_hdl( card );
    for ( hdl_idx = 0; ( hdl_idx < num_chans ) && ( status == 0 ); hdl_idx++ )
    {
        rx_hdl = _skiq_get_rx_hdl( card, hdl_idx );
        if ( rx_hdl < skiq_rx_hdl_end )
        {
            rfe_instance = _skiq_get_rx_rfe_instance( card, rx_hdl );
            status = rfe_read_rfic_port_for_rx_hdl( &rfe_instance, &curr_rfic_port );
        }
        else
        {
            skiq_error("Internal error (L%u); out-of-bounds receive handle for card %" PRIu8
                       " (number of RX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                       card, num_chans, hdl_idx);
            status = -EPROTO;
        }

        if( status == 0 )
        {
            if( curr_rfic_port != rfe_instance.p_id->port )
            {
                pause_all_rx_streams( card );

                a_port = rfe_instance.p_id->port;
                b_port = curr_rfic_port;

                // update RFE configuration
                status = rfe_swap_rx_port_config( &rfe_instance, a_port, b_port );
                if( status == 0 )
                {
                    rfic_instance = _skiq_get_rx_rfic_instance( card, rx_hdl );
                    status = rfic_swap_rx_port_config( &rfic_instance, a_port, b_port );

                    // loop through all the RX handles and update ports
                    for ( i = 0; (i < num_chans) && ( status == 0); i++ )
                    {
                        skiq_rx_hdl_t rx_hdl_i = _skiq_get_rx_hdl( card, i );

                        if ( rx_hdl_i < skiq_rx_hdl_end )
                        {
                            if( skiq.card[card].rx_list[rx_hdl_i].rf_id.port == a_port )
                            {
                                skiq.card[card].rx_list[rx_hdl_i].rf_id.port = b_port;
                            }
                            else if( skiq.card[card].rx_list[rx_hdl_i].rf_id.port == b_port )
                            {
                                skiq.card[card].rx_list[rx_hdl_i].rf_id.port = a_port;
                            }
                        }
                        else
                        {
                            skiq_error("Internal error (L%u); out-of-bounds receive handle for "
                                       "card %" PRIu8 " (number of RX handles is %" PRIu8
                                       ") at index %" PRIu8 "\n", __LINE__, card, num_chans, i);
                            status = -EPROTO;
                        }
                    }
                }
            }
        }
    }

    if ( status == 0 )
    {
        resume_and_flush_all_rx_streams( card );
    }

    // make sure everything's been successful before checking the TX side
    if( status == 0 )
    {
        num_chans = _skiq_get_nr_tx_hdl( card );
        for ( hdl_idx = 0; (hdl_idx < num_chans) && (status == 0); hdl_idx++ )
        {
            tx_hdl = _skiq_get_tx_hdl( card, hdl_idx );
            if ( tx_hdl < skiq_tx_hdl_end )
            {
                rfe_instance = _skiq_get_tx_rfe_instance( card, tx_hdl );
                status = rfe_read_rfic_port_for_tx_hdl( &rfe_instance, &curr_rfic_port );
            }
            else
            {
                skiq_error("Internal error (L%u); out-of-bounds transmit handle for card %" PRIu8
                           " (number of TX handles is %" PRIu8 ") at index %" PRIu8 "\n", __LINE__,
                           card, num_chans, hdl_idx);
                status = -EPROTO;
            }

            if( status == 0 )
            {
                if( curr_rfic_port != rfe_instance.p_id->port )
                {
                    a_port = rfe_instance.p_id->port;
                    b_port = curr_rfic_port;

                    skiq_debug("TX RFIC port needs to be updated for %s (from=%u, to=%u)\n",
                               tx_hdl_cstr(tx_hdl), a_port, b_port);

                    // update RFIC configuration
                    rfic_instance = _skiq_get_tx_rfic_instance( card, tx_hdl );
                    status = rfic_swap_tx_port_config( &rfic_instance, a_port, b_port );

                    // loop through all the TX handles and update ports
                    for( i = 0; (i < num_chans) && ( status == 0 ); i++ )
                    {
                        skiq_tx_hdl_t tx_hdl_i = _skiq_get_tx_hdl( card, i );

                        if ( tx_hdl_i < skiq_tx_hdl_end )
                        {
                            if( skiq.card[card].tx_list[tx_hdl_i].rf_id.port == a_port )
                            {
                                skiq.card[card].tx_list[tx_hdl_i].rf_id.port = b_port;
                            }
                            else if( skiq.card[card].tx_list[tx_hdl_i].rf_id.port == b_port )
                            {
                                skiq.card[card].tx_list[tx_hdl_i].rf_id.port = a_port;
                            }
                        }
                        else
                        {
                            skiq_error("Internal error (L%u); out-of-bounds transmit handle for "
                                       "card %" PRIu8 " (number of TX handles is %" PRIu8
                                       ") at index %" PRIu8 "\n", __LINE__, card, num_chans, i);
                            status = -EPROTO;
                        }
                    } // end looping through TX handles
                } // end swap needed
            } // RFIC port read ok
        } // end looping through channels
    } // end checking TX config

    return (status);
} /* _skiq_update_rfic_port_mapping */


/**************************************************************************************************/
/**
   @brief Indicates whether the GPSDO is available for product and FPGA bitstream
*/
EPIQ_API int32_t skiq_is_gpsdo_supported( uint8_t card, skiq_gpsdo_support_t *p_supported )
{
    int32_t status=0;
    int32_t detailed_status=0;
    bool is_avail=false;
    skiq_gpsdo_support_t supported = skiq_gpsdo_support_unknown;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_supported );

    /*
        This code assumes that `gpsdo_fpga_has_module()` returns 0 (function success) and correctly
        updates `is_avail` when the card / FPGA does not have the GPSDO module. Thus, any failures
        returned by the function are of the more critical kind (e.g. failed to talk to the FPGA).
    */
    LOCK_REGS(card);
    status = gpsdo_fpga_has_module( card, &is_avail, &detailed_status );
    UNLOCK_REGS(card);

    switch (detailed_status)
    {
    case 0:
        supported = (is_avail) ? skiq_gpsdo_support_is_supported : \
                        skiq_gpsdo_support_not_supported;
        break;
    case -ENOTSUP:
        supported = skiq_gpsdo_support_card_not_supported;
        break;
    case -ENOSYS:
        supported = skiq_gpsdo_support_fpga_not_supported;
        break;
    default:
        supported = skiq_gpsdo_support_unknown;
        break;
    }

    if( status == 0 )
    {
        *p_supported = supported;
    }

    return (status);
} /* skiq_is_gpsdo_supported */

/**************************************************************************************************/
/**
   @brief Enable the GPSDO control algorithm on the specified card
*/
EPIQ_API int32_t skiq_gpsdo_enable( uint8_t card )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status = gpsdo_fpga_configure_and_enable( card );
    UNLOCK_REGS(card);

    return (status);
} /* skiq_gpsdo_enabled */

/**************************************************************************************************/
/**
   @brief  Disable the GPSDO control algorithm on the specified card
*/
EPIQ_API int32_t skiq_gpsdo_disable( uint8_t card )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );

    LOCK_REGS(card);
    status = gpsdo_fpga_disable( card );
    UNLOCK_REGS(card);

    return (status);
} /* skiq_gpsdo_disable */

/**************************************************************************************************/
/**
   @brief Check the enable status of the GPSDO control algorithm on the specified card
*/
EPIQ_API int32_t skiq_gpsdo_is_enabled( uint8_t card, bool *p_is_enabled )
{
    int32_t status=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_is_enabled );

    LOCK_REGS(card);
    status = gpsdo_fpga_is_enabled( card, p_is_enabled );
    UNLOCK_REGS(card);

    return (status);
} /* skiq_gpsdo_is_enabled */

/**************************************************************************************************/
/**
   @brief Calculate the frequency accuracy of the FPGA's GPSDO oscillator frequency (in ppm)
*/
EPIQ_API int32_t skiq_gpsdo_read_freq_accuracy( uint8_t card, double *p_ppm )
{
    int32_t status=0;
    int32_t freq_counter=0;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_ppm );

    LOCK_REGS(card);
    status = gpsdo_fpga_get_freq_accuracy( card, p_ppm, &freq_counter );
    UNLOCK_REGS(card);

    return (status);
} /* skiq_gpsdo_read_freq_accuracy */

/**************************************************************************************************/
/**
    @brief Check the lock status of the GPSDO control algorithm on the specified card
*/

EPIQ_API int32_t skiq_gpsdo_is_locked( uint8_t card, bool *p_is_locked )
{
    int32_t status=0;
    gpsdo_fpga_status_registers_t gpsdoStatus = 
                        GPSDO_FPGA_STATUS_REGISTERS_T_INITIALIZER;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_is_locked );

    LOCK_REGS(card);
    status = gpsdo_fpga_read_status(card, &gpsdoStatus);
    UNLOCK_REGS(card);

    if( status == 0 )
    {
        *p_is_locked=gpsdoStatus.gpsdo_locked_state;
    }

    return (status);
} /* skiq_gpsdo_is_locked */


EPIQ_API int32_t skiq_read_rx_analog_filter_bandwidth( uint8_t card, 
                                                       skiq_rx_hdl_t hdl,
                                                       uint32_t *p_bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_bandwidth );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    LOCK_REGS( card );
    status = rfic_read_rx_analog_filter_bandwidth( &rfic_instance, p_bandwidth );
    UNLOCK_REGS( card );

    return (status);
}

EPIQ_API int32_t skiq_read_tx_analog_filter_bandwidth( uint8_t card, 
                                                       skiq_tx_hdl_t hdl,
                                                       uint32_t *p_bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_NULL_PARAM( p_bandwidth );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    LOCK_REGS( card );
    status = rfic_read_tx_analog_filter_bandwidth( &rfic_instance, p_bandwidth );
    UNLOCK_REGS( card );


    return (status);
}

EPIQ_API int32_t skiq_write_rx_analog_filter_bandwidth( uint8_t card,
                                                        skiq_rx_hdl_t hdl, 
                                                        uint32_t bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_RX_HDL( card, hdl );

    rfic_instance = _skiq_get_rx_rfic_instance( card, hdl );

    LOCK_REGS( card );
    status = rfic_write_rx_analog_filter_bandwidth( &rfic_instance, bandwidth );
    UNLOCK_REGS( card );

    return (status);
}

EPIQ_API int32_t skiq_write_tx_analog_filter_bandwidth( uint8_t card,
                                                        skiq_tx_hdl_t hdl, 
                                                        uint32_t bandwidth )
{
    int32_t status=0;
    rfic_t rfic_instance;

    CHECK_CARD_RANGE( card );
    CHECK_CARD_ACTIVE( card );
    CHECK_VALID_TX_HDL( card, hdl );

    rfic_instance = _skiq_get_tx_rfic_instance( card, hdl );

    LOCK_REGS( card );
    status = rfic_write_tx_analog_filter_bandwidth( &rfic_instance, bandwidth );
    UNLOCK_REGS( card );

    return (status);
}
