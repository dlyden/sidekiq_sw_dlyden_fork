/*****************************************************************************/
/** @file rfe_mpcie_b.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <stdlib.h>
#include <string.h>

#include "rfe_mpcie_b.h"

#include "bit_ops.h"

#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "ad9361_driver.h"
#include "sidekiq_private.h"

#define HIGH_BAND_THRESHOLD_FREQ (3000000000llu)
#define BAND_2_RX_ONLY_MASK (0x3)  // note: we only care about the lower 2 bits for the RX path


static const skiq_filt_t _filter_list[] =
{
    skiq_filt_0_to_3000_MHz,
    skiq_filt_3000_to_6000_MHz
};
#define _NUM_FILTERS (sizeof(_filter_list)/sizeof(skiq_filt_t))

#define MPCIE_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define MPCIE_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define MPCIE_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define MPCIE_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

static int32_t _update_tx_pa( rf_id_t *p_id,
                              bool enable );
static int32_t _update_rx_lna( rf_id_t *p_id,
                               rfe_lna_state_t new_state );
static int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                          uint64_t rx_lo_freq );
static int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                         uint64_t rx_lo_freq );
static int32_t _update_tx_lo( rf_id_t *p_id,
                              uint64_t tx_lo_freq );
static int32_t _write_rx_filter_path( rf_id_t *p_id,
                                       skiq_filt_t path );
static int32_t _write_tx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t path );
static int32_t _read_rx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t *p_path );
static int32_t _read_tx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t *p_path );
static int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters );
static int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                              skiq_filt_t *p_filters,
                                              uint8_t *p_num_filters );

static int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );

static int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );

static int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );

static int32_t _read_tx_rf_port( rf_id_t *p_id,
                                 skiq_rf_port_t *p_rf_port );

static int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );
static int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );

static int32_t _read_rfic_port_for_tx_hdl( rf_id_t *p_id, uint8_t *p_port );

static int32_t _calc_filt_for_hopping( uint16_t num_freq, uint64_t freq_list[], skiq_filt_t *p_filt );
static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

rfe_functions_t rfe_mpcie_b_funcs =
{
    .init                           = NULL,
    .release                        = NULL,
    .update_tx_pa                   = _update_tx_pa,
    .override_tx_pa                 = NULL,    
    .update_rx_lna                  = _update_rx_lna,
    .update_rx_lna_by_idx           = NULL,
    .update_before_rx_lo_tune       = _update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = _update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = _update_tx_lo,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = _write_rx_filter_path,
    .write_tx_filter_path           = _write_tx_filter_path,
    .read_rx_filter_path            = _read_rx_filter_path,
    .read_tx_filter_path            = _read_tx_filter_path,
    .read_rx_filter_capabilities    = _read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = _read_tx_filter_capabilities,
    .write_filters                  = NULL,
    .write_rx_attenuation           = NULL,
    .read_rx_attenuation            = NULL,
    .read_rx_attenuation_range      = NULL,
    .write_rx_attenuation_mode      = NULL,
    .read_rx_attenuation_mode       = NULL,
    .read_rx_rf_ports_avail         = _read_rx_rf_ports_avail, 
    .read_rx_rf_port                = _read_rx_rf_port, 
    .write_rx_rf_port               = NULL, 
    .read_tx_rf_ports_avail         = _read_tx_rf_ports_avail, 
    .read_tx_rf_port                = _read_tx_rf_port, 
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = NULL,
    .write_rf_port_config           = NULL,
    .read_rf_port_operation         = NULL,
    .write_rf_port_operation        = NULL,
    .read_bias_index                = NULL,
    .write_bias_index               = NULL,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = NULL,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = _write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = _write_tx_freq_hop_list,
    .config_custom_rx_filter        = NULL,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = _read_rfic_port_for_tx_hdl,
    .swap_rx_port_config            = NULL,
    .read_rf_capabilities           = _read_rf_capabilities,
};

int32_t _update_tx_pa( rf_id_t *p_id,
                       bool enable )
{
    int32_t status=-1;
    uint32_t data=0;
    
    sidekiq_fpga_reg_read(p_id->card, FPGA_REG_RF_BAND_SEL, &data);
    if( enable == true )
    {
        BF_SET(data, 1, TX_ENA_OFFSET,TX_ENA_LEN);
        BF_SET(data, 1, BAND_2_TX_CONFIG_OFFSET, BAND_2_TX_CONFIG_LEN);
        // we also need to clear out the RX Band 2 settings
        BF_SET(data, 0, BAND_2_CONFIG_OFFSET, BAND_2_CONFIG_LEN);
    }
    else
    {
        BF_SET(data, 0, TX_ENA_OFFSET,TX_ENA_LEN);
        BF_SET(data, 0, BAND_2_TX_CONFIG_OFFSET, BAND_2_TX_CONFIG_LEN);
    }
    sidekiq_fpga_reg_write(p_id->card, FPGA_REG_RF_BAND_SEL, data);
    status = sidekiq_fpga_reg_verify(p_id->card, FPGA_REG_RF_BAND_SEL, data);

    return (status);
}

int32_t _update_rx_lna( rf_id_t *p_id,
                        rfe_lna_state_t new_state )
{
    int32_t status;

    switch (new_state)
    {
        case lna_enabled:
            status = sidekiq_fpga_reg_RMWV( p_id->card, 1, LNA_ENA, FPGA_REG_RF_BAND_SEL );
            break;

        case lna_disabled:
            status = sidekiq_fpga_reg_RMWV( p_id->card, 0, LNA_ENA, FPGA_REG_RF_BAND_SEL );
            break;

        case lna_bypassed:
            status = -ENOTSUP;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return (status);
}

int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                   uint64_t rx_lo_freq )
{
    return _update_rx_lna( p_id, lna_disabled );
}

int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                  uint64_t rx_lo_freq )
{
    int32_t status=-1;
    skiq_filt_t filt;

    // update the preselect filter and LNA selection

    // figure out if we're low or high band
    if( rx_lo_freq > HIGH_BAND_THRESHOLD_FREQ )
    {
        filt = skiq_filt_3000_to_6000_MHz;
        // turn on the LNA
        status = _update_rx_lna( p_id, lna_enabled );
    }
    else
    {
        filt = skiq_filt_0_to_3000_MHz;
        // turn off the LNA
        status = _update_rx_lna( p_id, lna_disabled );
    }

    if ( status == 0 )
    {
        status = _write_rx_filter_path( p_id, filt );
    }

    return (status);
}

int32_t _update_tx_lo( rf_id_t *p_id,
                       uint64_t tx_lo_freq )
{
    int32_t status=-1;
    skiq_filt_t filt;

    // update the preselect filter path
    if( tx_lo_freq > HIGH_BAND_THRESHOLD_FREQ )
    {
        filt = skiq_filt_3000_to_6000_MHz;
    }
    else
    {
        filt = skiq_filt_0_to_3000_MHz;
    }
    status=_write_tx_filter_path( p_id, filt );

    return (status);
}

int32_t _write_rx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status=-1;
    uint32_t val = 0;

    sidekiq_fpga_reg_read( p_id->card, FPGA_REG_RF_BAND_SEL, &val );
    switch( path )
    {
        case skiq_filt_0_to_3000_MHz:
            status=ad9361_write_rx_input_port(p_id->chip_id,
                                              ad9361_rx_port_b);

            if( p_id->hdl == skiq_rx_hdl_A1 )
            {
                RBF_SET(val, 2, BAND_1_CONFIG);
            }
            else if( p_id->hdl == skiq_rx_hdl_A2 )
            {
                RBF_SET(val, 1, BAND_2_CONFIG);
            }
            break;

        case skiq_filt_3000_to_6000_MHz:
            status=ad9361_write_rx_input_port( p_id->chip_id,
                                               ad9361_rx_port_a );
            if( p_id->hdl == skiq_rx_hdl_A1 )
            {
                RBF_SET(val, 1, BAND_1_CONFIG);
            }
            else if( p_id->hdl == skiq_rx_hdl_A2 )
            {
                RBF_SET(val, 2, BAND_2_CONFIG);
            }
            break;

        default:
            status=-1;
            break;
    }
    if( status==0 )
    {
        sidekiq_fpga_reg_write( p_id->card,
                                FPGA_REG_RF_BAND_SEL, val );
        status = sidekiq_fpga_reg_verify( p_id->card,
                                          FPGA_REG_RF_BAND_SEL,
                                          val);
    }
    
    return (status);
}

int32_t _write_tx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status=-1;
    uint32_t val=0;

    sidekiq_fpga_reg_read( p_id->card, FPGA_REG_RF_BAND_SEL, &val );
    switch( path )
    {
        case skiq_filt_0_to_3000_MHz:
            status=ad9361_write_tx_output_port(p_id->chip_id,
                                        ad9361_tx_port_b);
            RBF_SET(val, 2, TX_SELECT);
            break;

        case skiq_filt_3000_to_6000_MHz:
            status=ad9361_write_tx_output_port(p_id->chip_id,
                                        ad9361_tx_port_a);
            RBF_SET(val, 1, TX_SELECT);
            break;

        default:
            break;
    }
    if( status == 0 )
    {
        status = sidekiq_fpga_reg_write( p_id->card, FPGA_REG_RF_BAND_SEL, val );
    }
    
    return (status);
}

int32_t _read_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    int32_t status=-1;
    uint32_t val=0;
    uint32_t band_sel=0;

    status = sidekiq_fpga_reg_read( p_id->card, FPGA_REG_READ_RF_BAND_SEL, &val );
    if( status==0 )
    {
        status=-2;
        switch( p_id->hdl )
        {
            case skiq_rx_hdl_A1:
                band_sel = RBF_GET(val, BAND_1_VALUE);
                if( band_sel == 1 )
                {
                    *p_path = skiq_filt_3000_to_6000_MHz;
                    status = 0;
                }
                else if( band_sel == 2 )
                {
                    *p_path = skiq_filt_0_to_3000_MHz;
                    status = 0;
                }
                break;

            case skiq_rx_hdl_A2:
                band_sel = RBF_GET(val, BAND_2_VALUE);
                band_sel = band_sel & BAND_2_RX_ONLY_MASK;
                if( band_sel == 1 )
                {
                    *p_path = skiq_filt_0_to_3000_MHz;
                    status = 0;
                }
                else if( band_sel == 2 )
                {
                    *p_path = skiq_filt_3000_to_6000_MHz;
                    status = 0;
                }

            default:
                break;
        }
    }

    return (status);
}

int32_t _read_tx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    int32_t status=-1;
    uint32_t val = 0;
    uint32_t band_sel;

    status = sidekiq_fpga_reg_read( p_id->card, FPGA_REG_RF_BAND_SEL, &val );
    band_sel = RBF_GET(val, TX_SELECT);
    if( status==0 )
    {
        if( band_sel == 1 )
        {
            *p_path = skiq_filt_3000_to_6000_MHz;
        }
        else if( band_sel == 2 )
        {
            *p_path = skiq_filt_0_to_3000_MHz;
        }
        else
        {
            *p_path = skiq_filt_invalid;
            status = -2;
        }
    }

    return (status);
}

int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status=0;

    *p_num_filters = _NUM_FILTERS;
    memcpy( p_filters, _filter_list, *p_num_filters*sizeof(skiq_filt_t) );

    return (status);
}

int32_t _read_tx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    int32_t status=0;

    *p_num_filters = _NUM_FILTERS;
    memcpy( p_filters, _filter_list, *p_num_filters*sizeof(skiq_filt_t) );

    return (status);
}

int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;

    *p_num_fixed_rf_ports=0;
    *p_num_trx_rf_ports=0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_num_fixed_rf_ports = 1;
        p_fixed_rf_port_list[0] = skiq_rf_port_Jxxx_RX1;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_num_fixed_rf_ports = 1;
        p_fixed_rf_port_list[0] = skiq_rf_port_Jxxx_TX1RX2;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}


int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_Jxxx_RX1;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_rf_port = skiq_rf_port_Jxxx_TX1RX2;
    }
    else
    {
        status = -ENODEV;
    }    

    return (status);
}

int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;

    *p_num_fixed_rf_ports=0;
    *p_num_trx_rf_ports=0;

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_num_fixed_rf_ports = 1;
        p_fixed_rf_port_list[0] = skiq_rf_port_Jxxx_TX1RX2;
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t _read_tx_rf_port( rf_id_t *p_id,
                          skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_Jxxx_TX1RX2;
    }
    else
    {
        status = -ENODEV;
    }    

    return (status);
}

int32_t _calc_filt_for_hopping( uint16_t num_freq, uint64_t freq_list[], skiq_filt_t *p_filt )
{
    int32_t status=0;
    uint16_t i=0;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    *p_filt = skiq_filt_invalid;

    for( i=0; (i<num_freq) && (status==0); i++ )
    {
        // Low band
        if( freq_list[i] < HIGH_BAND_THRESHOLD_FREQ )
        {
            if( *p_filt == skiq_filt_3000_to_6000_MHz )
            {
                skiq_error("Hopping range must all <3GHz or all >=3GHz for Sidekiq mPCIe");
                status = -EINVAL;
            }
            else
            {
                *p_filt = skiq_filt_0_to_3000_MHz;
            }
        }
        // High band
        else
        {
            if( *p_filt == skiq_filt_0_to_3000_MHz )
            {
                skiq_error("Hopping range must all <3GHz or all >=3GHz for Sidekiq mPCIe");
                status = -EINVAL;
            }
            else
            {
                *p_filt = skiq_filt_3000_to_6000_MHz;
            }
        }
    }

    return (status);
}

int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    status = _calc_filt_for_hopping( num_freq, freq_list, &skiq_filt );

    // if the frequencies are within a band, then actually apply it.  Since we know
    // all of the frequencies use the same filter setting, we can just set RFE for the
    // first frequency in the hop list
    if( status == 0 )
    {
        status = _update_before_rx_lo_tune( p_id, freq_list[0] );
    }
    if( status == 0 )
    {
        status = _update_after_rx_lo_tune( p_id, freq_list[0] );
    }

    return (status);
}

int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    status = _calc_filt_for_hopping( num_freq, freq_list, &skiq_filt );

    // if the frequencies are within a band, then actually apply it.  Since we know
    // all of the frequencies use the same filter setting, we can just set RFE for the
    // first frequency in the hop list
    if( status == 0 )
    {
        status = _update_tx_lo( p_id, freq_list[0] );
    }

    return (status);
}

int32_t _read_rfic_port_for_tx_hdl( rf_id_t *p_id, uint8_t *p_port )
{
    int32_t status=0;

    switch( p_id->hdl )
    {
        case skiq_tx_hdl_A1:
            *p_port = 2;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = MPCIE_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = MPCIE_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = MPCIE_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = MPCIE_MAX_RX_FREQUENCY_IN_HZ;

    return (status);
}
