/**
 * @file   rfic_common.c
 * @date   Fri May 10 11:00:06 EDT 2019
 * 
 * @brief  A collection of common functions for use across RFIC implementations.
 * 
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>

#include "rfic_common.h"
#include "sidekiq_fpga_ctrl.h"  /* for decimator_* functions */
#include "sidekiq_private.h"    /* for MIN() */

/* enable debug_print and debug_print_plain when DEBUG_RFIC_COMMON is defined */
#if (defined DEBUG_RFIC_COMMON)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/



/***** TYPE DEFINITIONS *****/



/***** STRUCTS *****/



/***** LOCAL DATA *****/

/* This stores the requested sample rate / bandwidth configuration */
static rate_config_t _rx_rate_config[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end];
static rate_config_t _tx_rate_config[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end];


/***** LOCAL FUNCTIONS *****/

static void
clear_rx_rate_config( uint8_t card )
{
    skiq_rx_hdl_t hdl;

    for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
    {
        _rx_rate_config[card][hdl].sample_rate = 0;
        _rx_rate_config[card][hdl].bandwidth = 0;
    }
}


static void
clear_tx_rate_config( uint8_t card )
{
    skiq_tx_hdl_t hdl;

    for ( hdl = skiq_tx_hdl_A1; hdl < skiq_tx_hdl_end; hdl++ )
    {
        _tx_rate_config[card][hdl].sample_rate = 0;
        _tx_rate_config[card][hdl].bandwidth = 0;
    }
}


/***** GLOBAL FUNCTIONS *****/


void
rfic_consider_profile( uint8_t card,
                       skiq_rx_hdl_t hdl,
                       uint32_t request_rate,
                       uint32_t request_bw,
                       uint32_t profile_rate,
                       uint32_t profile_bw,
                       uint8_t nr_stages_available,
                       uint32_t half_max_sample_clk_hz,
                       struct result *p_result )
{
    uint8_t nr_stages = 0;

    /* clamp number of decimator stages in the case where the profile_rate is greater than
     * half of the maximum sample rate and the decimator cannot support it */
    if ( ( profile_rate > half_max_sample_clk_hz ) &&
         decimator_only_supports_half_max_rate( card, hdl ) )
    {
        nr_stages = 0;
        debug_print("No decimation support for profile, rate is above half of maximum "
                    "sample clock\n");
    }
    else
    {
        nr_stages = nr_stages_available;
    }

    while ( ( nr_stages > 0 ) && ( p_result->dec_rate == 0 ) )
    {
        uint32_t decimated_rate = profile_rate >> nr_stages;
        uint32_t decimated_bw = MIN( profile_bw, decimated_rate );

        if ( decimated_rate >= request_rate )
        {
            if ( decimated_bw >= request_bw )
            {
                debug_print("Decimated pair (%u,%u) satisfies (%u,%u)\n",
                            decimated_rate, decimated_bw, request_rate, request_bw);
            }
            else
            {
                debug_print("Decimated pair (%u,%u) satisfies sample rate %u, but not "
                            "bandwidth %u\n", decimated_rate, decimated_bw, request_rate,
                            request_bw);
            }

            p_result->dec_rate = nr_stages;
            p_result->delta_rate = decimated_rate - request_rate;
            p_result->delta_bw = decimated_bw - request_bw;
        }

        nr_stages--;
    }

    /* no decimated rate / bw satisfied request, check profile rate / bw */
    if ( p_result->dec_rate == 0 )
    {
        if ( profile_rate >= request_rate )
        {
            if ( profile_bw >= request_bw )
            {
                debug_print("Profile pair (%u,%u) satisfies (%u,%u)\n", profile_rate, profile_bw,
                            request_rate, request_bw);
            }
            else
            {
                debug_print("Profile pair (%u,%u) satisfies sample rate %u, but not bandwidth %u\n",
                            profile_rate, profile_bw, request_rate, request_bw);
            }

            p_result->dec_rate = 0; /* use without decimation */
            p_result->delta_rate = profile_rate - request_rate;
            p_result->delta_bw = profile_bw - request_bw;
        }
    }
}


int32_t
rfic_find_best_result( struct result results[],
                       uint8_t nr_results,
                       uint8_t *p_best_index )
{
    int32_t status = 0;
    struct result minimum = {
        .dec_rate = UINT8_MAX,
        .delta_rate = INT32_MAX,
        .delta_bw = INT32_MAX,
    };
    bool is_minimum_valid = false;
    int16_t i, minimum_index = -1;

    for ( i = 0; i < nr_results; i++ )
    {
        if ( results[i].delta_rate == INT32_MAX )
        {
            debug_print("Skipping profile #%u, cannot meet requested rate and/or bandwidth or "
                        "is not valid for card\n", i);
        }
        else
        {
            /* for this pass, rates must match and the delta_bw must be >= 0 (i.e. completely satisfies the request) */
            if ( ( results[i].delta_rate == 0 ) && ( results[i].delta_bw >= 0 ) )
            {
                /*
                  There are two cases where the `minimum` is updated:

                  1. This profile's delta_bw is smaller than the current minimum's
                  2. This profile's delta_bw is equal to the current minimum's, but the dec_rate is smaller
                */
                if ( ( results[i].delta_bw < minimum.delta_bw ) ||

                     ( ( results[i].delta_bw == minimum.delta_bw ) &&
                       ( results[i].dec_rate < minimum.dec_rate ) ) )
                {
                    if ( is_minimum_valid )
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) is better then #%u "
                                    "(dec=%u,drate=%d,dbw=%d)\n", i, results[i].dec_rate,
                                    results[i].delta_rate, results[i].delta_bw, minimum_index,
                                    minimum.dec_rate, minimum.delta_rate, minimum.delta_bw);
                    }
                    else
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) chosen as default\n",
                                    i, results[i].dec_rate, results[i].delta_rate,
                                    results[i].delta_bw);
                    }

                    minimum.dec_rate = results[i].dec_rate;
                    minimum.delta_rate = results[i].delta_rate;
                    minimum.delta_bw = results[i].delta_bw;
                    minimum_index = i;
                    is_minimum_valid = true;
                }
                else
                {
                    if (is_minimum_valid)
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) is worse than #%u "
                                    "(dec=%u,drate=%d,dbw=%d)\n", i, results[i].dec_rate,
                                    results[i].delta_rate, results[i].delta_bw, minimum_index,
                                    minimum.dec_rate, minimum.delta_rate, minimum.delta_bw);
                    }
                }
            }
        }
    }

    if ( !is_minimum_valid )
    {
        debug_print("No profile completely satisfies requested rate / bandwidth, checking for "
                    "ones that come close\n");

        /* make another pass and allow those results that match rate, but don't satisfy the requested bandwidth */
        minimum.delta_bw = INT32_MIN;
        for ( i = 0; i < nr_results; i++ )
        {
            if ( ( results[i].delta_rate == 0 ) && ( results[i].delta_bw < 0 ) )
            {
                if ( ( results[i].delta_bw > minimum.delta_bw ) ||

                     ( ( results[i].delta_bw == minimum.delta_bw ) &&
                       ( results[i].dec_rate < minimum.dec_rate ) ) )
                {
                    if ( is_minimum_valid )
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) is better then #%u "
                                    "(dec=%u,drate=%d,dbw=%d)\n", i, results[i].dec_rate,
                                    results[i].delta_rate, results[i].delta_bw, minimum_index,
                                    minimum.dec_rate, minimum.delta_rate, minimum.delta_bw);
                    }
                    else
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) chosen as default\n",
                                    i, results[i].dec_rate, results[i].delta_rate,
                                    results[i].delta_bw);
                    }

                    minimum.dec_rate = results[i].dec_rate;
                    minimum.delta_rate = results[i].delta_rate;
                    minimum.delta_bw = results[i].delta_bw;
                    minimum_index = i;
                    is_minimum_valid = true;
                }
            }
        }
    }

    if ( !is_minimum_valid )
    {
        debug_print("No profile completely satisfies requested rate / bandwidth, checking for "
                    "ones that come close\n");

        /* make another pass and allow those results that don't satisfy the requested rate and/or bandwidth */
        minimum.delta_bw = INT32_MIN;
        for ( i = 0; i < nr_results; i++ )
        {
            if ( ( results[i].delta_rate != 0 ) && ( results[i].delta_bw >= 0 ) )
            {
                /*
                  1. This profile's delta_rate is strictly less than the current minimum's
                  2. This profile's delta_rate is equal to the current minimum's, but the delta_bw is smaller
                  3. This profile's delta_rate and delta_bw are both equal to the current minimum's, but the dec_rate is smaller
                */
                if ( ( results[i].delta_rate < minimum.delta_rate ) ||

                     ( ( results[i].delta_rate == minimum.delta_rate ) &&
                       ( results[i].delta_bw < minimum.delta_bw ) ) ||

                     ( ( results[i].delta_rate == minimum.delta_rate ) &&
                       ( results[i].delta_bw == minimum.delta_bw ) &&
                       ( results[i].dec_rate < minimum.dec_rate ) ) )
                {
                    if ( is_minimum_valid )
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) is better then #%u "
                                    "(dec=%u,drate=%d,dbw=%d)\n", i, results[i].dec_rate,
                                    results[i].delta_rate, results[i].delta_bw, minimum_index,
                                    minimum.dec_rate, minimum.delta_rate, minimum.delta_bw);
                    }
                    else
                    {
                        debug_print("Profile #%u (dec=%u,drate=%d,dbw=%d) chosen as default\n",
                                    i, results[i].dec_rate, results[i].delta_rate,
                                    results[i].delta_bw);
                    }

                    minimum.dec_rate = results[i].dec_rate;
                    minimum.delta_rate = results[i].delta_rate;
                    minimum.delta_bw = results[i].delta_bw;
                    minimum_index = i;
                    is_minimum_valid = true;
                }
            }
        }

        if ( is_minimum_valid )
        {
            skiq_warning("!!! RFIC Profile DOES NOT MEET requested sample rate !!!\n");
        }
    }

    if ( !is_minimum_valid )
    {
        skiq_error("!!! No RFIC Profile meets the requested sample rate and/or bandwidth !!!\n");
        status = -ERANGE;
    }

    if ( status == 0 )
    {
        debug_print("Profile #%u best meets requested rate / bandwidth\n", minimum_index);
        *p_best_index = minimum_index;
    }

    return status;
}


int32_t
rfic_store_rx_rate_config( rf_id_t *p_id,
                           uint32_t sample_rate,
                           uint32_t bandwidth )
{
    int32_t status = 0;

    if ( p_id->hdl < skiq_rx_hdl_end )
    {
        debug_print("Storing RX sample_rate=%u and bandwidth=%u for card %u, hdl %s\n", sample_rate,
                    bandwidth, p_id->card, rx_hdl_cstr( p_id->hdl ) );
        _rx_rate_config[p_id->card][p_id->hdl].sample_rate = sample_rate;
        _rx_rate_config[p_id->card][p_id->hdl].bandwidth = bandwidth;
    }
    else
    {
        skiq_error("Invalid receive handle (%u) provided to store RX rate configuration for "
                   "card %u\n", p_id->hdl, p_id->card );
        status = -EINVAL;
    }

    return status;
}


int32_t
rfic_store_tx_rate_config( rf_id_t *p_id,
                           uint32_t sample_rate,
                           uint32_t bandwidth )
{
    int32_t status = 0;

    if ( p_id->hdl < skiq_tx_hdl_end )
    {
        debug_print("Storing TX sample_rate=%u and bandwidth=%u for card %u, hdl %s\n", sample_rate,
                    bandwidth, p_id->card, tx_hdl_cstr( p_id->hdl ) );
        _tx_rate_config[p_id->card][p_id->hdl].sample_rate = sample_rate;
        _tx_rate_config[p_id->card][p_id->hdl].bandwidth = bandwidth;
    }
    else
    {
        skiq_error("Invalid transmit handle (%u) provided to store TX rate configuration for "
                   "card %u\n", p_id->hdl, p_id->card );
        status = -EINVAL;
    }

    return status;
}


void rfic_clear_rate_config( rf_id_t *p_id )
{
    clear_rx_rate_config( p_id->card );
    clear_tx_rate_config( p_id->card );
}


bool
rfic_hdl_has_rx_rate_config( rf_id_t *p_id )
{
    bool has_rx_rate_config = false;

    if ( p_id->hdl < skiq_rx_hdl_end )
    {
        has_rx_rate_config = (_rx_rate_config[p_id->card][p_id->hdl].sample_rate > 0);
    }

    return has_rx_rate_config;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t
rfic_get_rx_rate_config( rf_id_t *p_id,
                         uint32_t *p_sample_rate,
                         uint32_t *p_bandwidth )
{
    int32_t status = 0;

    if ( p_id->hdl >= skiq_rx_hdl_end )
    {
        skiq_error("Invalid receive handle (%u) provided to retrieve RX rate configuration for "
                   "card %u\n", p_id->hdl, p_id->card );
        status = -EINVAL;
    }

    if ( ( status == 0 ) && ( p_sample_rate != NULL ) )
    {
        *p_sample_rate = _rx_rate_config[p_id->card][p_id->hdl].sample_rate;
    }

    if ( ( status == 0 ) && ( p_bandwidth != NULL ) )
    {
        *p_bandwidth = _rx_rate_config[p_id->card][p_id->hdl].bandwidth;
    }

    return status;
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t
rfic_get_tx_rate_config( rf_id_t *p_id,
                         uint32_t *p_sample_rate,
                         uint32_t *p_bandwidth )
{
    int32_t status = 0;

    if ( p_id->hdl >= skiq_tx_hdl_end )
    {
        skiq_error("Invalid transmit handle (%u) provided to retrieve TX rate configuration for "
                   "card %u\n", p_id->hdl, p_id->card );
        status = -EINVAL;
    }

    if ( ( status == 0 ) && ( p_sample_rate != NULL ) )
    {
        *p_sample_rate = _tx_rate_config[p_id->card][p_id->hdl].sample_rate;
    }

    if ( ( status == 0 ) && ( p_bandwidth != NULL ) )
    {
        *p_bandwidth = _tx_rate_config[p_id->card][p_id->hdl].bandwidth;
    }

    return status;
}
