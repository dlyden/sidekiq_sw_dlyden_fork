/*****************************************************************************/
/** @file rfe_x4_b.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <assert.h>

#include "rfe_x4_b.h"
#include "rfic_x4_private.h"
#include "sidekiq_rfic.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "bit_ops.h"
#include "card_services.h"

#if (defined DEBUG_RFE_X4)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

#define NUM_RX_HDLS (6)

// Filters / LNAs outlined in http://confluence/display/EN/Sidekiq+X4-revA+HW+Bring+Up
// also on p. 7 of the schematic
#define RX_LNA_OFFSET (3)
#define RX_LNA_LEN    (1)

#define RX1_ORX_FILT_OFFSET             (0)
#define RX2_FILT_OFFSET                 (4)
#define RX_FILT_LEN                     (3) // both Rx1/2 filters have same control

#define RX_SELECT_OFFSET   (8) // ORX: 1, RX1: 0
#define RX_SELECT_LEN      (1)

#define FILT_SEL_0_TO_6000MHz     (0x0)
#define FILT_SEL_390_TO_620MHz    (0x4)
#define FILT_SEL_540_TO_850MHz    (0x7)
#define FILT_SEL_770_TO_1210MHz   (0x3)
#define FILT_SEL_1130_TO_1760MHz  (0x2)
#define FILT_SEL_1680_TO_2580MHz  (0x5)
#define FILT_SEL_2500_TO_3880MHz  (0x1)
#define FILT_SEL_3800_TO_6000MHz  (0x6)

#define RXA1_FILTER_SEL_OFFSET (0)
#define RXA2_FILTER_SEL_OFFSET (3)
#define RXB1_FILTER_SEL_OFFSET (0)

#define EEPROM_FILTER_CONFIG_OFFSET (100)
#define EEPROM_FILTER_INDEX_OFFSET  (EEPROM_FILTER_CONFIG_OFFSET+1)
#define EEPROM_FILTER_CONFIG_ELEMENT_SIZE (sizeof(uint8_t))

/*  Max number of filter elements + 1 */
/* the +1 is due to the operator choice on line 634
   num_filters >= EEPROM_FILTER_CONFIG_NUM_ELEMENTS
   Keeping this implementation similar to rfe_x2_a.c
*/
#define EEPROM_FILTER_CONFIG_NUM_ELEMENTS (9)
#define EEPROM_FILTER_CONFIG_NUM_ELEMENTS_UNDEFINED (0xFF)

#define RX_PARAM_INIT                                                   \
{                                                                           \
    .filters = {NULL, 0, 0},                                                \
    .filter_config = skiq_filt_invalid                                      \
}

#define RFE_PARAM_INIT                                                      \
{                                                                           \
    .rx = {                                                                 \
        [0 ... (NUM_RX_HDLS-1)] = RX_PARAM_INIT,                            \
    },                                                                      \
    .rx_filter_indexes = {                                                  \
        [0 ... (EEPROM_FILTER_CONFIG_NUM_ELEMENTS-1)] = 0,                  \
    },                                                                      \
}


typedef struct
{
    freq_range_t freqRange;
    skiq_filt_t skiq_filt;
    uint32_t filt_sel;
} filtConfig_t;

typedef struct
{
    uint8_t *p_filter_indexes;
    uint8_t num_filters;
    uint8_t select_offset;
} _filter_path_t;

typedef struct
{
    _filter_path_t filters;
    skiq_filt_t filter_config;
} _rx_param_t;

typedef struct
{
    _rx_param_t rx[NUM_RX_HDLS];
    uint8_t rx_filter_indexes[EEPROM_FILTER_CONFIG_NUM_ELEMENTS];
} _rfe_param_t;

const skiq_filt_t _x4_rev_c_rx_filters_with_preselect[] =
{
    skiq_filt_390_to_620MHz,
    skiq_filt_540_to_850MHz,
    skiq_filt_770_to_1210MHz,
    skiq_filt_1130_to_1760MHz,
    skiq_filt_1680_to_2580MHz,
    skiq_filt_2500_to_3880MHz,
    skiq_filt_3800_to_6000MHz,
    skiq_filt_0_to_6000MHz
};

const uint8_t _x4_rev_c_num_rx_filters_with_preselect =
    sizeof(_x4_rev_c_rx_filters_with_preselect)/sizeof(skiq_filt_t);

const skiq_filt_t _x4_rev_c_rx_filters_no_preselect[] =
{
    skiq_filt_0_to_6000MHz
};
const uint8_t _x4_rev_c_num_rx_filters_no_preselect =
    sizeof(_x4_rev_c_rx_filters_no_preselect)/sizeof(skiq_filt_t);

/***
 * From Bobby's email...updated freq band
def get_filter_band(freq_MHz):
  63     """
  64     return appropriate filter band for given frequency in MHz
  65     :Args:
  66         :freq_MHz (int or float): RX LO frequency in MHz
  67     :Returns:
  68         filter index as int
  69     """
  70     if freq_MHz <= 450:
  71         return 0
  72     elif (freq_MHz > 450) and (freq_MHz <= 600):
  73         return 1
  74     elif (freq_MHz > 600) and (freq_MHz <= 800):
  75         return 2
  76     elif (freq_MHz > 800) and (freq_MHz <= 1200):
  77         return 3
  78     elif (freq_MHz > 1200) and (freq_MHz <= 1700):
  79         return 4
  80     elif (freq_MHz > 1700) and (freq_MHz <= 2700):
  81         return 5
  82     elif (freq_MHz > 2700) and (freq_MHz <= 3600):
  83         return 6
  84     elif (freq_MHz > 3600) and (freq_MHz <= 6000):
  85         return 7
  86     else:
  87         return 0
*/

static const filtConfig_t _rx_filters[] =
{
    { {450000000, 600000000},   skiq_filt_390_to_620MHz, FILT_SEL_390_TO_620MHz },
    { {600000000, 800000000},   skiq_filt_540_to_850MHz, FILT_SEL_540_TO_850MHz },
    { {800000000, 1200000000},  skiq_filt_770_to_1210MHz, FILT_SEL_770_TO_1210MHz },
    { {1200000000, 1700000000}, skiq_filt_1130_to_1760MHz, FILT_SEL_1130_TO_1760MHz },
    { {1700000000, 2700000000}, skiq_filt_1680_to_2580MHz, FILT_SEL_1680_TO_2580MHz },
    { {2700000000, 3600000000}, skiq_filt_2500_to_3880MHz, FILT_SEL_2500_TO_3880MHz },
    { {3600000000, 6000000000}, skiq_filt_3800_to_6000MHz, FILT_SEL_3800_TO_6000MHz },
    { {0, 6000000000}, skiq_filt_0_to_6000MHz, FILT_SEL_0_TO_6000MHz } // this should be the catchall / bypass state /the only one for the no filters!
};

#define NUM_RX_FILTERS (sizeof(_rx_filters)/sizeof(filtConfig_t))

#define X4_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define X4_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define X4_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define X4_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)


static _rfe_param_t _rfe[SKIQ_MAX_NUM_CARDS] =
{
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = RFE_PARAM_INIT,
};

static uint32_t _rfe_gpio_config_a[SKIQ_MAX_NUM_CARDS] = { [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = 0, };
static uint32_t _rfe_gpio_config_b[SKIQ_MAX_NUM_CARDS] = { [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = 0, };

static uint32_t _find_rx_filt_sel_by_freq( uint64_t lo_freq );
static bool _find_rx_filt_sel_by_skiq_filt( skiq_filt_t skiq_filt, uint32_t *p_filt_sel );
static bool _find_skiq_filt_by_rx_filt_sel( uint32_t filt_sel, skiq_filt_t *p_skiq_filt );

static void _find_gpio( rf_id_t *p_id, uint32_t **pp_gpio );

static int32_t _write_rx_filt( rf_id_t *p_id, uint32_t filt_sel );

static int32_t _init( rf_id_t *p_id );

static int32_t _update_tx_pa( rf_id_t *p_id,
                              bool enable );
static int32_t _update_rx_lna( rf_id_t *p_id,
                               rfe_lna_state_t new_state );
static int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                          uint64_t rx_lo_freq );
static int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                         uint64_t rx_lo_freq );
static int32_t _update_tx_lo( rf_id_t *p_id,
                              uint64_t tx_lo_freq );
static int32_t _write_rx_filter_path( rf_id_t *p_id,
                                       skiq_filt_t path );
static int32_t _read_rx_filter_path( rf_id_t *p_id,
                                      skiq_filt_t *p_path );
static int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                             skiq_filt_t *p_filters,
                                             uint8_t *p_num_filters );
static int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list );
static int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
static int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                        uint8_t *p_num_fixed_rf_ports,
                                        skiq_rf_port_t *p_fixed_rf_port_list,
                                        uint8_t *p_num_trx_rf_ports,
                                        skiq_rf_port_t *p_trx_rf_port_list);
static int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port );
static int32_t _read_rx_stream_hdl_conflict( rf_id_t *p_id,
                                             skiq_rx_hdl_t *p_conflicting_hdls,
                                             uint8_t *p_num_hdls );

static int32_t _enable_rx_chan( rf_id_t *p_id, bool enable );

static int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );
static int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] );

static int32_t _write_filters( rf_id_t *p_id,
                               uint8_t num_filters,
                               const skiq_filt_t *p_filters );

static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

/***** Global Variables *****/

rfe_functions_t rfe_x4_b_funcs =
{
    .init                           = _init,
    .release                        = NULL,
    .update_tx_pa                   = _update_tx_pa,
    .override_tx_pa                 = NULL,    
    .update_rx_lna                  = _update_rx_lna,
    .update_rx_lna_by_idx           = NULL,
    .update_before_rx_lo_tune       = _update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = _update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = _update_tx_lo,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = _write_rx_filter_path,
    .write_tx_filter_path           = NULL,
    .read_rx_filter_path            = _read_rx_filter_path,
    .read_tx_filter_path            = NULL,
    .read_rx_filter_capabilities    = _read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = NULL, /* No TX filters */
    .write_filters                  = _write_filters,
    .write_rx_attenuation           = NULL,
    .read_rx_attenuation            = NULL,
    .read_rx_attenuation_range      = NULL,
    .write_rx_attenuation_mode      = NULL,
    .read_rx_attenuation_mode       = NULL,
    .read_rx_rf_ports_avail         = _read_rx_rf_ports_avail,
    .read_rx_rf_port                = _read_rx_rf_port,
    .write_rx_rf_port               = NULL,
    .read_tx_rf_ports_avail         = _read_tx_rf_ports_avail,
    .read_tx_rf_port                = _read_tx_rf_port,
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = NULL,
    .write_rf_port_config           = NULL,
    .read_rf_port_operation         = NULL,
    .write_rf_port_operation        = NULL,
    .read_rx_stream_hdl_conflict    = _read_rx_stream_hdl_conflict,
    .enable_rx_chan                 = _enable_rx_chan,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = _write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = _write_tx_freq_hop_list,
    .config_custom_rx_filter        = NULL,
    .read_rfic_port_for_rx_hdl      = NULL,
    .read_rfic_port_for_tx_hdl      = NULL,
    .swap_rx_port_config            = NULL,
    .read_rf_capabilities           = _read_rf_capabilities,
};

uint32_t _find_rx_filt_sel_by_freq( uint64_t lo_freq )
{
    uint8_t i=0;
    uint32_t filt_sel=FILT_SEL_0_TO_6000MHz;
    bool bFound = false;

    // TODO: we should account for the sample rate config to select the "ideal" filter since
    // there is some filt overlap
    for( i=0; (i<NUM_RX_FILTERS) && (bFound==false); i++ )
    {
        if( (lo_freq > _rx_filters[i].freqRange.startFreq) &&
            (lo_freq <= _rx_filters[i].freqRange.stopFreq) )
        {
            filt_sel = _rx_filters[i].filt_sel;
            bFound = true;
        }
    }

    return (filt_sel);
}

bool _find_rx_filt_sel_by_skiq_filt( skiq_filt_t skiq_filt, uint32_t *p_filt_sel )
{
    uint8_t i=0;
    bool bFound = false;

    // TODO: we should account for the sample rate config to select the "ideal" filter since
    // there is some filt overlap
    for( i=0; (i<NUM_RX_FILTERS) && (bFound==false); i++ )
    {
        if( (skiq_filt == _rx_filters[i].skiq_filt) )
        {
            *p_filt_sel = _rx_filters[i].filt_sel;
            bFound = true;
        }
    }

    return (bFound);
}

bool _find_skiq_filt_by_rx_filt_sel( uint32_t filt_sel, skiq_filt_t *p_skiq_filt )
{
    uint8_t i=0;
    bool bFound = false;

    for( i=0; (i<NUM_RX_FILTERS) && (bFound==false); i++ )
    {
        if( (filt_sel == _rx_filters[i].filt_sel) )
        {
            *p_skiq_filt = _rx_filters[i].skiq_filt;
            bFound = true;
        }
    }

    return (bFound);
}

void _find_gpio( rf_id_t *p_id, uint32_t **pp_gpio )
{
    if( p_id->hdl == skiq_rx_hdl_A1 ||
        p_id->hdl == skiq_rx_hdl_A2 ||
        p_id->hdl == skiq_rx_hdl_C1 )
    {
        *pp_gpio = &_rfe_gpio_config_a[p_id->card];
    }
    else
    {
        *pp_gpio = &_rfe_gpio_config_b[p_id->card];
    }
}

int32_t _write_rx_filt( rf_id_t *p_id, uint32_t filt_sel )
{
    int32_t status=0;
    uint32_t offset=0;
    uint32_t *p_gpio = NULL;
    uint32_t curr_gpio=0;

    _find_gpio( p_id, &p_gpio );
    curr_gpio = *p_gpio;
    
    // determine the GPIO offset based on the handle
    if( (p_id->hdl == skiq_rx_hdl_A2) ||
        (p_id->hdl == skiq_rx_hdl_B2) ||
        (p_id->hdl == skiq_rx_hdl_C1) ||
        (p_id->hdl == skiq_rx_hdl_D1) )
    {
        offset = RX1_ORX_FILT_OFFSET;
    }
    else if( (p_id->hdl == skiq_rx_hdl_A1) ||
             (p_id->hdl == skiq_rx_hdl_B1) )
    {
        offset = RX2_FILT_OFFSET;
    }
    // TODO: other channels
    else
    {
        status = -ENODEV;
    }
    
    if( status == 0 )
    {
        BF_SET( curr_gpio, filt_sel, offset, RX_FILT_LEN); 
        // config the gpio
        if( (status=_x4_write_3v3_gpio( p_id, curr_gpio )) == 0 )
        {
            // cache the updated value
            *p_gpio = curr_gpio;
        }
    }
    return (status);
}

int32_t _init( rf_id_t *p_id )
{
    int32_t status,i=0;
    uint8_t card = p_id->card;
    uint8_t num_filters=0;

    /////////////////////////////////
    // read the filter config from EEPROM
    status = hal_read_eeprom( card,
                    EEPROM_FILTER_CONFIG_OFFSET,
                    &(num_filters),
                    1 );
    if (status == 0)
    {
        if (num_filters == EEPROM_FILTER_CONFIG_NUM_ELEMENTS_UNDEFINED)
        {
            /* For the initial shipment of X4 Rev C and X4 Rev B, there was no filter configuration
                stored in EEPROM.  In this case, the num filters will read 0xFF, and we
                are to assume the unit has filters */
            for(i=0; i<NUM_RX_HDLS; i++ )
            {
                _rfe[card].rx[i].filters.num_filters = num_filters;
            }
        }
        else if( num_filters >= EEPROM_FILTER_CONFIG_NUM_ELEMENTS )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                    "Invalid # (%u) of filter elements configured, defaulting to none\n",
                    num_filters);

            for( i=0; i<NUM_RX_HDLS; i++ )
            {
                _rfe[card].rx[i].filters.select_offset = 0;
                _rfe[card].rx[i].filters.num_filters = 0;
                _rfe[card].rx[i].filters.p_filter_indexes = NULL;
            }
        }
        else
        {
            status = hal_read_eeprom( card,
                            EEPROM_FILTER_INDEX_OFFSET,
                            &(_rfe[card].rx_filter_indexes[0]),   //read the filter indices
                            num_filters );
            // note: we only have a single filter config for all RX handles...
            if (status == 0)
            {
                for(i=0; i<NUM_RX_HDLS; i++ )
                {
                    //store the indices, num_filters, and the select offset
                    _rfe[card].rx[i].filters.p_filter_indexes = _rfe[card].rx_filter_indexes;
                    _rfe[card].rx[i].filters.num_filters = num_filters;
                    _rfe[card].rx[i].filters.select_offset =
                        (i == skiq_rx_hdl_A1) ? RXA1_FILTER_SEL_OFFSET :
                        (i == skiq_rx_hdl_A2) ? RXA2_FILTER_SEL_OFFSET :
                        (i == skiq_rx_hdl_B1) ? RXB1_FILTER_SEL_OFFSET :
                        0;
                }
            }
        }
    }
    else
    {
        skiq_error("Unable to read filter config from EEPROM on card %" PRIu8 " (status %" PRIi32 ")\n",card, status);

    }
    /////////////////////////////////

    return (status);
}

int32_t _update_tx_pa( rf_id_t *p_id,
                       bool enable )
{
    int32_t status=0;

    // No PA, just return success

    return (status);
}

int32_t _update_rx_lna( rf_id_t *p_id,
                        rfe_lna_state_t new_state )
{
    int32_t status=0;
    uint32_t *p_gpio;
    uint32_t curr_gpio=0;
    uint32_t lna_state=0;
    uint32_t offset=0;

    _find_gpio( p_id, &p_gpio );
    curr_gpio = *p_gpio;

    if( new_state == lna_disabled )
    {
        lna_state = 0;
    }
    else if( new_state == lna_enabled )
    {
        lna_state = 1;
    }
    // no bypass path
    else
    {
        status = -EINVAL;
    }

    if( status == 0 )
    {
        if( (p_id->hdl == skiq_rx_hdl_A2) ||
            (p_id->hdl == skiq_rx_hdl_B2) ||
            (p_id->hdl == skiq_rx_hdl_C1) ||
            (p_id->hdl == skiq_rx_hdl_D1) )
        {
            offset = RX1_ORX_FILT_OFFSET;
        }
        else if( (p_id->hdl == skiq_rx_hdl_A1) ||
                 (p_id->hdl == skiq_rx_hdl_B1) )
        {
            offset = RX2_FILT_OFFSET;
        }
        else
        {
            status = -ENODEV;
        }

        if( status == 0 )
        {
            BF_SET( curr_gpio, lna_state, offset+RX_LNA_OFFSET, RX_LNA_LEN);

            // config the gpio
            if( (status=_x4_write_3v3_gpio( p_id, curr_gpio )) == 0 )
            {
                // cache the updated value
                *p_gpio = curr_gpio;
            }
        }
    }

    if (status == 0)
    {
        debug_print("Configured LNA to %s (hdl=%s)\n" , 
                   ((lna_state == 0) ? "Disabled" : "Enabled"), 
                   rx_hdl_cstr(p_id->hdl));
    }

    return (status);
}

int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                   uint64_t rx_lo_freq )
{
    return _update_rx_lna( p_id, lna_disabled );
}

int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                  uint64_t rx_lo_freq )
{
    int32_t status=0;
    uint32_t filt_sel = 0;

    // determine the filt sel based on the freq
    filt_sel = _find_rx_filt_sel_by_freq( rx_lo_freq );
    
    debug_print("Using RX filter %d for (hdl=%s)\n",
               filt_sel, rx_hdl_cstr(p_id->hdl) );

    status = _write_rx_filt( p_id, filt_sel );
    if( status == 0 )
    {
        // TODO: should we do this every time we need to update the freq?
        // now make sure the LNA is actually on
        status = _update_rx_lna( p_id, lna_enabled );
    }
    return (status);
}

int32_t _update_tx_lo( rf_id_t *p_id,
                       uint64_t tx_lo_freq )
{
    int32_t status=0;

    // No TX filters, just return success

    return (status);
}

int32_t _write_rx_filter_path( rf_id_t *p_id,
                               skiq_filt_t path )
{
    int32_t status=0;
    uint32_t filt_sel = RX1_ORX_FILT_OFFSET;

    if( _find_rx_filt_sel_by_skiq_filt( path, &filt_sel ) == true )
    {
        status = _write_rx_filt( p_id, filt_sel );
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "Filter %u is invalid (card=%u)\n", path, p_id->card);
        status = -EINVAL;
    }

    return (status);
}

int32_t _read_rx_filter_path( rf_id_t *p_id,
                              skiq_filt_t *p_path )
{
    int32_t status=0;
    uint32_t *p_gpio = NULL;
    uint32_t filt_sel=0;
    uint32_t offset=0;

    _find_gpio( p_id, &p_gpio );

    // determine the GPIO offset based on the handle
    if( (p_id->hdl == skiq_rx_hdl_A2) ||
        (p_id->hdl == skiq_rx_hdl_B2) ||
        (p_id->hdl == skiq_rx_hdl_C1) ||
        (p_id->hdl == skiq_rx_hdl_D1) )
    {
        offset = RX1_ORX_FILT_OFFSET;
    }
    else if( (p_id->hdl == skiq_rx_hdl_A1) ||
             (p_id->hdl == skiq_rx_hdl_B1) )
    {
        offset = RX2_FILT_OFFSET;
    }
    // TODO: other channels
    else
    {
        status = -ENODEV;
    }

    if( status == 0 )
    {
        filt_sel = BF_GET( *p_gpio, offset, RX_FILT_LEN );
        if( _find_skiq_filt_by_rx_filt_sel( filt_sel, p_path ) != true )
        {
            status = -EINVAL;
        }
    }

    return (status);
}

int32_t _read_rx_filter_capabilities( rf_id_t *p_id,
                                      skiq_filt_t *p_filters,
                                      uint8_t *p_num_filters )
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    uint8_t card = p_id->card;
    int32_t status=0;
    uint8_t i=0;
    uint8_t numFilters = _rfe[card].rx[hdl].filters.num_filters;

    if (numFilters == EEPROM_FILTER_CONFIG_NUM_ELEMENTS_UNDEFINED)
    {
        numFilters = NUM_RX_FILTERS;
        for( i=0; i<NUM_RX_FILTERS; i++ )
        {
            p_filters[i] = _rx_filters[i].skiq_filt;
        }
    }
    else
    {
        uint8_t *p_indexes = _rfe[card].rx[hdl].filters.p_filter_indexes;
        for( i=0; i<numFilters; i++ )
        {
            p_filters[i] = _rx_filters[p_indexes[i]].skiq_filt;
        }
    }

    *p_num_filters = numFilters;

    return (status);
}

int32_t _read_rx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status=0;

    *p_num_fixed_rf_ports = 1;
    *p_num_trx_rf_ports = 0;

    switch( p_id->hdl )
    {
        case skiq_rx_hdl_A1:
            p_fixed_rf_port_list[0] = skiq_rf_port_J2;
            break;
        case skiq_rx_hdl_A2:
            p_fixed_rf_port_list[0] = skiq_rf_port_J3;
            break;
        case skiq_rx_hdl_B1:
            p_fixed_rf_port_list[0] = skiq_rf_port_J6;
            break;
        case skiq_rx_hdl_B2:
            p_fixed_rf_port_list[0] = skiq_rf_port_J7;
            break;
        case skiq_rx_hdl_C1:
            p_fixed_rf_port_list[0] = skiq_rf_port_J3;
            break;
        case skiq_rx_hdl_D1:
            p_fixed_rf_port_list[0] = skiq_rf_port_J7;
            break;
        default:
            status = -EINVAL;
            break;
    }

    return (status);
}

int32_t _read_rx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    *p_rf_port = skiq_rf_port_unknown;

    switch( p_id->hdl )
    {
        case skiq_rx_hdl_A1:
            *p_rf_port = skiq_rf_port_J2;
            break;
        case skiq_rx_hdl_A2:
            *p_rf_port = skiq_rf_port_J3;
            break;
        case skiq_rx_hdl_B1:
            *p_rf_port = skiq_rf_port_J6;
            break;
        case skiq_rx_hdl_B2:
            *p_rf_port = skiq_rf_port_J7;
            break;
        case skiq_rx_hdl_C1:
            *p_rf_port = skiq_rf_port_J3;
            break;
        case skiq_rx_hdl_D1:
            *p_rf_port = skiq_rf_port_J7;
            break;
        default:
            status = -EINVAL;
            break;
    }


    return (status);
}

int32_t _read_tx_rf_ports_avail( rf_id_t *p_id,
                                 uint8_t *p_num_fixed_rf_ports,
                                 skiq_rf_port_t *p_fixed_rf_port_list,
                                 uint8_t *p_num_trx_rf_ports,
                                 skiq_rf_port_t *p_trx_rf_port_list)
{
    int32_t status=0;

    *p_num_fixed_rf_ports = 1;
    *p_num_trx_rf_ports = 0;

    switch( p_id->hdl )
    {
        case skiq_tx_hdl_A1:
            p_fixed_rf_port_list[0] = skiq_rf_port_J4;
            break;
        case skiq_tx_hdl_A2:
            p_fixed_rf_port_list[0] = skiq_rf_port_J1;
            break;
        case skiq_tx_hdl_B1:
            p_fixed_rf_port_list[0] = skiq_rf_port_J8;
            break;
        case skiq_tx_hdl_B2:
            p_fixed_rf_port_list[0] = skiq_rf_port_J5;
            break;
        default:
            status = -EINVAL;
            break;
    }

    return (status);
}

int32_t _read_tx_rf_port( rf_id_t *p_id, skiq_rf_port_t *p_rf_port )
{
    int32_t status=0;

    *p_rf_port = skiq_rf_port_unknown;

    switch( p_id->hdl )
    {
        case skiq_tx_hdl_A1:
            *p_rf_port = skiq_rf_port_J4;
            break;
        case skiq_tx_hdl_A2:
            *p_rf_port = skiq_rf_port_J1;
            break;
        case skiq_tx_hdl_B1:
            *p_rf_port = skiq_rf_port_J8;
            break;
        case skiq_tx_hdl_B2:
            *p_rf_port = skiq_rf_port_J5;
            break;
        default:
            status = -EINVAL;
            break;
    }


    return (status);
}

int32_t _read_rx_stream_hdl_conflict( rf_id_t *p_id,
                                      skiq_rx_hdl_t *p_conflicting_hdls,
                                      uint8_t *p_num_hdls )
{
    int32_t status=0;

    // default to no conflicts
    *p_num_hdls = 0;

    // A2 can't share with C1
    if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_C1;
        *p_num_hdls = 1;
    }
    // C1 can't share with A2
    else if( p_id->hdl == skiq_rx_hdl_C1 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_A2;
        *p_num_hdls = 1;
    }
    // B2 can't share with D1
    else if( p_id->hdl == skiq_rx_hdl_B2 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_D1;
        *p_num_hdls = 1;
    }
    // D1 can't share with B2
    else if( p_id->hdl == skiq_rx_hdl_D1 )
    {
        p_conflicting_hdls[*p_num_hdls] = skiq_rx_hdl_B2;
        *p_num_hdls = 1;
    }


    return (status);
}

int32_t _enable_rx_chan( rf_id_t *p_id, bool enable )
{
    int32_t status=0;
    uint32_t curr_gpio = 0;
    uint32_t *p_gpio = NULL;
    bool update_gpio=false;
    uint32_t orx_active_bit = 0;
    
    _find_gpio( p_id, &p_gpio );
    curr_gpio = *p_gpio;

    // only handle the "enable" case since this switch isn't really an enable, but rather a select
    if( enable == true )
    {
        // we only need to do something if we need to manage the switch between Rx1 (A2/B2) and OR1 (C1/D1)
        if( (p_id->hdl == skiq_rx_hdl_A2) ||
            (p_id->hdl == skiq_rx_hdl_B2) )
        {
            orx_active_bit = 0;
            update_gpio = true;
        }
        else if( (p_id->hdl == skiq_rx_hdl_C1) ||
                 (p_id->hdl == skiq_rx_hdl_D1) )
        {
            orx_active_bit = 1;
            update_gpio = true;
        }

        // see if we actually need to update the GPIO state
        if( update_gpio == true )
        {
            RBF_SET( curr_gpio, orx_active_bit, RX_SELECT ); 
            // config the gpio
            if( (status=_x4_write_3v3_gpio( p_id, curr_gpio )) == 0 )
            {
            // cache the updated values for GPIO and active
                *p_gpio = curr_gpio;
            }
        }
    }
    
    return (status);
}

int32_t _write_rx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;
    uint16_t i=0;
    uint32_t filt_sel=0;
    uint32_t first_filt_sel=0;
    skiq_filt_t skiq_filt=skiq_filt_0_to_6000MHz;

    // Note that we're guaranteed at the upper layer that num_freq falls within valid bounds

    // determine if all the hop freqs reside in the same filter range...if not, use the bypass
    first_filt_sel = _find_rx_filt_sel_by_freq( freq_list[0] );
    filt_sel = first_filt_sel;
    for( i=1; ((i<num_freq) && (filt_sel != FILT_SEL_0_TO_6000MHz)); i++ )
    {
        filt_sel = _find_rx_filt_sel_by_freq( freq_list[i] );
        if( filt_sel != first_filt_sel )
        {
            filt_sel = FILT_SEL_0_TO_6000MHz;
        }
    }
    if( filt_sel == FILT_SEL_0_TO_6000MHz )
    {
        skiq_info("RX frequency hop list not within single filter band, uses bypass filter (card=%u, hdl=%s)",
                  p_id->card, rx_hdl_cstr(p_id->hdl));
    }
    
    if( _find_skiq_filt_by_rx_filt_sel( filt_sel, &skiq_filt ) == true )
    {
        debug_print("Using RX filter %s (hdl=%s)\n",
                   SKIQ_FILT_STRINGS[skiq_filt], rx_hdl_cstr(p_id->hdl) );
        status = _write_rx_filt( p_id, filt_sel );
        if( status == 0 )
        {
            // make sure the LNA is on
            status = _update_rx_lna( p_id, lna_enabled );
        }
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

int32_t _write_tx_freq_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[] )
{
    int32_t status=0;

    // no filtering so nothing to do here

    return (status);
}

int32_t _write_filters( rf_id_t *p_id,
                        uint8_t num_filters,
                        const skiq_filt_t *p_filters )
{
    int32_t status=-1;
    uint8_t i=0;
    uint8_t j=0;
    bool bFound = false;
    uint8_t filter_indexes[EEPROM_FILTER_CONFIG_NUM_ELEMENTS];
    uint8_t filters_found=0;

    // Note: for this hardware version, we only support a global RX configuration

    if( num_filters > EEPROM_FILTER_CONFIG_NUM_ELEMENTS )
    {
        _skiq_log(SKIQ_LOG_ERROR, "too many filter elements specified to configure\n");
        status = -EINVAL;
    }
    else
    {
        // we need to loop through all of the filters specified and find the matching index
        for( i=0; i<num_filters; i++ )
        {
            bFound = false;
            for( j=0; (j<EEPROM_FILTER_CONFIG_NUM_ELEMENTS) && (bFound==false); j++ )
            {
                // found our filter index
                if( _rx_filters[j].skiq_filt == p_filters[i] )
                {
                    bFound = true;
                    filter_indexes[i] = j;
                    filters_found++;
                    _skiq_log(SKIQ_LOG_DEBUG, "found filter enum %u at index %u\n",
                              p_filters[i], j);
                }
            }
            if( bFound == false )
            {
                _skiq_log(SKIQ_LOG_ERROR, "unable to find index for filter %u\n",
                          p_filters[i]);
                status = -EINVAL;
                break;
            }
        }
        if( filters_found == num_filters )
        {
            status = 0;

            _skiq_log(SKIQ_LOG_DEBUG, "about to write %u filters\n", num_filters);

            // write the number of filters first
            status = card_write_eeprom( p_id->card,
                                        EEPROM_FILTER_CONFIG_OFFSET,
                                        &num_filters,
                                        sizeof(num_filters) );
            if( status == 0 )
            {
                // write the active filter indexes
                status = card_write_eeprom( p_id->card,
                                            (EEPROM_FILTER_CONFIG_OFFSET+1),
                                            filter_indexes,
                                            num_filters );
            }
        }
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = X4_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = X4_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = X4_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = X4_MAX_RX_FREQUENCY_IN_HZ;


    return (status);
}
