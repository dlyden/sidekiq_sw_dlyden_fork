/**
 * @file   card_services.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Jun  8 15:31:49 2016
 *
 * @brief
 *
 *
 * <pre>
 * Copyright 2016-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <inttypes.h>

/* @todo this needs to change! */
#include <sidekiq_usb_cmds.h>

#include "card_services.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_fpga.h"
#include "ad9361_driver.h"
#include "calibration.h"
#include "hardware.h"
// TODO: is there a cleaner way?
#include "rfe_z2_b.h"
#include "io_m2_2280.h"
#include "hal_nv100.h"

#include "libsidekiq_debug.h"

/***** DEFINES *****/

/* https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html */
#define xstr(s)                         str(s)
#define str(s)                          #s

#define SKIQ_EEPROM_ADDR                (0x51)

#define EXT_REF_CLOCK_FREQ_INVALID (UINT32_MAX)

#define CAL_YEAR_START (2000)
#define CAL_YEAR_OFFSET (0)
#define CAL_YEAR_LEN (6)
#define CAL_WEEK_NUM_OFFSET (6)
#define CAL_WEEK_NUM_LEN (6)
#define MIN_WEEK_NUM (1)
#define MAX_WEEK_NUM (53) // https://en.wikipedia.org/wiki/ISO_week_date
#define CAL_INTERVAL_OFFSET (12)
#define CAL_INTERVAL_LEN (4)
#define CAL_DATE_UNCONFIGURED (UINT16_MAX)

// IO expander that controls the write protect pins
#define SKIQ_X4_IO_EXP_ADDR       (0x24)

#define SKIQ_X4_EEPROM_WP_OFFSET  (0)
#define SKIQ_X4_FMC_WP_OFFSET     (1)
#define SKIQ_X4_WP_LEN            (1)

// refer to X4 schematic...WP is active high
#define SKIQ_X4_WP_ENABLE         (1)
#define SKIQ_X4_WP_DISABLE        (0)

// DISABLE_EEPROM_ACCESS
#define MANUAL_WARP_VOLTAGE         UNCONFIGURED_WARP_VOLTAGE
#define MANUAL_REF_CLOCK            skiq_ref_clock_internal
#define MANUAL_CAL_YEAR             (22)
#define MANUAL_CAL_WEEK             (5)
#define MANUAL_CAL_INTERVAL         (52)

/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/
#ifdef __linux
static pthread_mutex_t _eeprom_mutex[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP };
static pthread_mutex_t _fmc_eeprom_mutex[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP};
static pthread_mutex_t _x4_gpo_exp_mutex[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP};
#elif __WIN32
static pthread_mutex_t _eeprom_mutex[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER};
static pthread_mutex_t _fmc_eeprom_mutex[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER};
static pthread_mutex_t _x4_gpo_exp_mutex[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = PTHREAD_RECURSIVE_MUTEX_INITIALIZER};
#else
#error target OS not supported cleanly!
#endif


/***** LOCAL FUNCTIONS *****/
static int32_t card_eeprom_wp_update_if_avail( uint8_t card, bool write_protect );

static int32_t card_fmc_eeprom_wp_update_if_avail( uint8_t card, bool write_protect );
static bool card_fmc_eeprom_wp_avail( skiq_part_t part );
static int32_t card_set_fmc_eeprom_wp( uint8_t card, bool write_protect );

static int32_t _write_x4_gpo_exp( uint8_t card, uint8_t offset, uint8_t len, uint8_t val ); 

/*****************************************************************************/



/***** GLOBAL FUNCTIONS *****/

int32_t card_read_serial_num(uint8_t card, uint32_t *p_serial)
{
     int32_t status=0;
     uint8_t buf[4];

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING FAKE SERIAL NUMBER !!!!!\n");
    buf[0] = 'E';
    buf[1] = 'K';
    buf[2] = 'A';
    buf[3] = 'F';

    *p_serial =
        (buf[0] <<  0) |
        (buf[1] <<  8) |
        (buf[2] << 16) |
        (buf[3] << 24);
#else
     // Bottom two bytes of 4 byte serial number is stored at VERSION_INFO_ADDR.
     // For older Sidekiq's the legacy serial number is a 16 bit integer an is
     // wholly contained within these two bytes in EEPROM.
     status = hal_read_eeprom( card, VERSION_INFO_LEGACY_ADDR, buf, 2 );
     if( 0 != status )
     {
         skiq_error("Failed to read EEPROM, code %d (card=%u)\n", status, card);
         return status;
     }

     *p_serial = buf[0];
     *p_serial |= buf[1] << 8;

     if( INVALID_SERIAL_NUM == *p_serial )
     {
         // Check if an alphanumeric serial number is set...
         status = hal_read_eeprom( card, VERSION_INFO_ALPHANUMERIC_ADDR, buf, 4 );

         if( (0 == status) &&
             (isalnum( buf[0]) != 0 ) &&
             (isalnum( buf[1]) != 0 ) &&
             (isalnum( buf[2]) != 0 ) &&
             (isalnum( buf[3]) != 0 ) )
         {
             // Serial number is stored little endian.
             *p_serial =
                 (buf[0] <<  0) |
                 (buf[1] <<  8) |
                 (buf[2] << 16) |
                 (buf[3] << 24);
         }
     }
#endif

     return status;
}

/**
    @brief  Determine if a legacy mPCIe / m.2 card is masquerading its revision
*/
void card_unmasquerade(uint8_t card, skiq_hw_vers_t hw_vers, const char *p_revision_string,
    skiq_hw_vers_t *p_actual_hw_vers, skiq_hw_rev_t *p_actual_hw_rev)
{
    skiq_hw_rev_t hw_rev = hw_rev_invalid;
    skiq_hw_vers_t actual_hw_vers = hw_vers;

    /*
        Legacy mPCIe and m.2 cards should not have any information in the "extended
        product information" section of the EEPROM; if the first letter of the revision
        string isn't "blank" (0xFF generally means "blank" when reading from the EEPROM)
        and is an ASCII letter, then assume this section is populated and that the
        card is masquerading.

        We check this first before attempting to map the revision as (of this comment)
        `card_map_hw_rev()` prints an error message to the screen if the revision can't
        be mapped and we don't want to scare the end users with warning messages.
    */
    if( (0xFF != (unsigned char) p_revision_string[0]) && (isalpha(p_revision_string[0])) )
    {
        hw_rev = card_map_hw_rev((char *) &(p_revision_string[0]));
    }

    /*
        Check to see if the mPCIe / m.2 part could be masquerading as an older version for backwards
        compatibility reasons
    */
    if( hw_vers == skiq_hw_vers_mpcie_masquerade )
    {
        if (hw_rev == hw_rev_d)
        {
            actual_hw_vers = skiq_hw_vers_mpcie_d;
        }
        else if (hw_rev == hw_rev_e)
        {
            actual_hw_vers = skiq_hw_vers_mpcie_e;
        }
        else
        {
            /* This isn't a recognized version, so mark it as such */
            actual_hw_vers = skiq_hw_vers_invalid;
        }
    }
    else if( hw_vers == skiq_hw_vers_m2_masquerade )
    {
        if (hw_rev == hw_rev_d)
        {
            actual_hw_vers = skiq_hw_vers_m2_d;
        }
    }

    if( p_actual_hw_vers != NULL )
    {
        *p_actual_hw_vers = actual_hw_vers;
    }
    if( p_actual_hw_rev != NULL )
    {
        *p_actual_hw_rev = hw_rev;
    }
}

int32_t card_read_hw_version(uint8_t card, uint16_t *p_hw_version)
{
    int32_t status=0;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING EXTENDED HARDWARE VERSION !!!!!\n");
    // when disabling access to eeprom we want the device to have an "extended" hardware version
    *p_hw_version = (skiq_eeprom_hw_vers_ext << 0) | (skiq_eeprom_product_ext << 12);
#else
    uint16_t addr = PCB_INFO_ADDR;
    uint8_t buf[2] = { 0, 0 };

    status = hal_read_eeprom( card, addr, buf, 2 );

    if (0 == status)
    {
        *p_hw_version = buf[0];
        *p_hw_version |= buf[1] << 8;
    }

    debug_print("Read HW version 0x%0" PRIx16 " (0x%0" PRIx8 " 0x%0" PRIx8
        ") on card %" PRIu8 " (status = %" PRIi32 ")\n", *p_hw_version,
       buf[0], buf[1], card, status);
#endif

    return status;
}

int32_t card_read_hw_iface_vers(uint8_t card, hw_iface_vers_t *p_hw_iface_vers)
{
    int32_t status=0;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING EXTENDED HARDWARE VERSION !!!!!\n");
    // when disabling access to eeprom we want the device to have an "extended" hardware version
    *p_hw_iface_vers = HW_IFACE_VERSION(0, 0);
#else
    uint16_t addr = HW_IFACE_VERS_EEPROM_ADDR;
    uint16_t len = HW_IFACE_VERS_EEPROM_LEN;
    uint8_t buf[HW_IFACE_VERS_EEPROM_LEN] = { 0, 0 };

    status = hal_read_eeprom( card, addr, buf, len );

    if (0 == status)
    {
        p_hw_iface_vers->version = (buf[1] << 8) | buf[0];
    }

    debug_print("Read HW interface version 0x%0" PRIx16 " (0x%0" PRIx8 " 0x%0" PRIx8
        ") on card %" PRIu8 " (status = %" PRIi32 ")\n", p_hw_iface_vers->version,
       buf[0], buf[1], card, status);
#endif

    return status;
}

int32_t card_read_part_info(uint8_t card,
                            uint16_t hw_version,
                            char *p_part_number,
                            char *p_revision,
                            char *p_variant)
{
    int32_t status=0;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING PART NUMBER !!!!!\n");
    memcpy(p_part_number, xstr(OVERRIDE_PART_NUMBER), SKIQ_PART_NUM_STRLEN);
    p_part_number[SKIQ_PART_NUM_STRLEN-1] = '\0';

    memcpy(p_revision, "C0", SKIQ_REVISION_STRLEN);
    p_revision[SKIQ_REVISION_STRLEN-1] = '\0';

    memcpy(p_variant, "00", SKIQ_VARIANT_STRLEN);
    p_variant[SKIQ_VARIANT_STRLEN-1] = '\0';
#else
    skiq_product_t product;
    skiq_hw_vers_t hw_vers;
    uint16_t base_address = EEPROM_EXT_PRODUCT_INFO_BASE_ADDR;

    /** @TODO   verify that the EEPROM read addresses don't wrap */

    hw_vers = card_map_hw_vers(hw_version);
    product = card_map_product_vers(hw_version);

    /*
        Special case handle: some cards (such as the mPCIe rev E) masquerade their revision to
        provide backwards compatibility with older version of libsidekiq and they store their
        product info in the "extended hardware information" section of EEPROM (unlke the previous
        revisions). If the card is masquerading, select the correct EEPROM address and force the
        use of the extended hardware information
    */
    if( ( hw_vers == skiq_hw_vers_mpcie_masquerade ) ||
        ( hw_vers == skiq_hw_vers_m2_masquerade) )
    {
        int32_t tmp_status = 0;
        skiq_hw_vers_t actual_hw_vers = skiq_hw_vers_invalid;
        skiq_hw_rev_t actual_hw_rev = hw_rev_invalid;
        ARRAY_WITH_DEFAULTS(char, tmp_revision, SKIQ_REVISION_STRLEN, '\0');

        /*
            Attempt to read the revision number from the "extended hardware information"
            EEPROM section; this section shouldn't exist on the non-masqueraded part
        */
        tmp_status = hal_read_eeprom(card,
                        EEPROM_EXT_PRODUCT_INFO_MPCIE_M2_BASE_ADDR + EEPROM_REVISION_OFFSET,
                        (uint8_t *) &(tmp_revision[0]), SKIQ_REVISION_STRLEN);

        if( tmp_status == 0 )
        {
            card_unmasquerade(card, hw_vers, (const char *) &(tmp_revision[0]), &actual_hw_vers,
                &actual_hw_rev);
            if( (hw_vers != actual_hw_vers) && (actual_hw_vers != skiq_hw_vers_invalid) )
            {
                base_address = EEPROM_EXT_PRODUCT_INFO_MPCIE_M2_BASE_ADDR;
                product = skiq_product_reserved;
                hw_vers = skiq_hw_vers_reserved;
            }
        }
    }

    if( (product == skiq_product_reserved) && (hw_vers == skiq_hw_vers_reserved) )
    {
        status = hal_read_eeprom( card,
                                  base_address + EEPROM_PART_NUM_OFFSET,
                                  (uint8_t*)(p_part_number),
                                  SKIQ_PART_NUM_STRLEN );

        if ( status == 0 )
        {
            status = hal_read_eeprom( card,
                                      base_address + EEPROM_REVISION_OFFSET,
                                      (uint8_t*)(p_revision),
                                      SKIQ_REVISION_STRLEN );
        }

        if ( status == 0 )
        {
            status = hal_read_eeprom( card,
                                      base_address + EEPROM_VARIANT_OFFSET,
                                      (uint8_t*)(p_variant),
                                      SKIQ_VARIANT_STRLEN );
        }
    }
    else
    {
        status = card_map_legacy_part_info( hw_vers, product,
                                            p_part_number,
                                            p_revision,
                                            p_variant );
    }
#endif

    return (status);
}

int32_t card_read_warp_voltage_calibration( uint8_t card,
                                            uint8_t index,
                                            uint16_t *p_warp_voltage )
{
    int32_t status = 0;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING MANUAL WARP VOLTAGE !!!!!\n");
    *p_warp_voltage = MANUAL_WARP_VOLTAGE;
#else
    uint16_t addr = WARP_VOLTAGE_DATA_0_ADDR;
    uint8_t buf[2] = { 0, 0 };

    // invalid range
    if( WARP_VOLTAGE_DATA_COUNT <= index )
    {
        skiq_error("requested warp voltage calibration data does not exist (card=%u)\n", card);
        errno = EINVAL;
        status = -(errno);
    }
    // make sure the calibration date offset isn't requested
    else if( index == CAL_DATE_WARP_VOLTAGE_OFFSET )
    {
        skiq_error("Invalid warp voltage index requested (%u) (card=%u)\n", index, card);
        status = -EINVAL;
    }
    else
    {
        *p_warp_voltage = UNCONFIGURED_WARP_VOLTAGE;

        // increment address by two bytes for every entry
        addr += index * 2;

        status = hal_read_eeprom( card, addr, buf, 2 );
        if ( 0 == status )
        {
            *p_warp_voltage = buf[0];
            *p_warp_voltage |= buf[1] << 8;

            if( (*(p_warp_voltage) == UNCONFIGURED_WARP_VOLTAGE) )
            {
                if ( ( index == DEFAULT_USER_WARP_VOLTAGE_OFFSET ) ||
                     ( index == DEFAULT_WARP_VOLTAGE_OFFSET ) )
                {
                    skiq_info("Default warp voltage not currently stored (%s) (card=%u)\n",
                              ( index == DEFAULT_USER_WARP_VOLTAGE_OFFSET ) ? "user" : "factory",
                              card);
                }
                else
                {
                    skiq_info("Default warp voltage not currently stored (%u) (card=%u)\n",
                              index, card);
                }
                status = -1;
            }
        }
    }
#endif

    return status;
}

int32_t card_write_warp_voltage_calibration( uint8_t card,
                                             uint8_t index,
                                             uint16_t warp_voltage )
{
    int32_t status = 0;
    uint16_t addr = WARP_VOLTAGE_DATA_0_ADDR;

    if( WARP_VOLTAGE_DATA_COUNT <= index )
    {
        skiq_error("requested warp voltage calibration data does not exist (card=%u)\n", card);
        errno = EINVAL;
        status = -(errno);
    }
    else if( index == CAL_DATE_WARP_VOLTAGE_OFFSET )
    {
        skiq_error("Invalid warp voltage offset requested (%u) (card=%u)\n", index, card);
        status = -EINVAL;
    }
    else
    {
        // increment address by two bytes for every entry
        addr += index * 2;
        status = card_write_eeprom( card, addr, (uint8_t*)(&warp_voltage), 2 );
    }

    return status;
}


/*****************************************************************************/
/** The card_read_ref_clock function is responsible for reading the reference
    clock configuration from EEPROM.

    @param card card number of the Sidekiq of interest
    @param p_ref_clock pointer to where to store the read reference clock value

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_read_ref_clock( uint8_t card, skiq_ref_clock_select_t *p_ref_clock )
{
    int32_t status=-1;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING MANUAL REF CLOCK CFG !!!!!\n");
    *p_ref_clock = MANUAL_REF_CLOCK;
    status = 0;
#else
    uint8_t ref_clock[REFERENCE_CLOCK_INFO_LENGTH];

    *p_ref_clock = skiq_ref_clock_invalid;

    status = hal_read_eeprom(card,
                             REFERENCE_CLOCK_INFO_ADDR,
                             ref_clock,
                             REFERENCE_CLOCK_INFO_LENGTH);

    if( status == 0 )
    {
        switch( ref_clock[0] )
        {
            case REFERENCE_CLOCK_INTERNAL:
                *p_ref_clock = skiq_ref_clock_internal;
                status=0;
                break;

            case REFERENCE_CLOCK_EXTERNAL:
                *p_ref_clock = skiq_ref_clock_external;
                status=0;
                break;

            case REFERENCE_CLOCK_CARRIER_EDGE:
                *p_ref_clock = skiq_ref_clock_carrier_edge;
                status=0;
                break;

            case REFERENCE_CLOCK_HOST:
                *p_ref_clock = skiq_ref_clock_host;
                status=0;
                break;

            default:
                *p_ref_clock = skiq_ref_clock_invalid;
                status = 0;
                break;
        }
    }
#endif

    return (status);
}

/*****************************************************************************/
/** The card_write_ext_ref_clock_freq writes the external reference clock's
    frequency to EEPROM.

    @param card card number of the Sidekiq of interest
    @param freq the reference clock's frequency

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_write_ext_ref_clock_freq( uint8_t card, uint32_t freq )
{
    uint8_t buf[REFERENCE_CLOCK_EXTERNAL_FREQ_LEN];
    int32_t status = 0;

    // little endian
    buf[0] = (uint8_t) (freq >> 0);
    buf[1] = (uint8_t) (freq >> 8);
    buf[2] = (uint8_t) (freq >> 16);
    buf[3] = (uint8_t) (freq >> 24);

    status = card_write_eeprom(card,
                               REFERENCE_CLOCK_EXTERNAL_FREQ_ADDR,
                               buf,
                               REFERENCE_CLOCK_EXTERNAL_FREQ_LEN);
    
    return status;
}

/*****************************************************************************/
/** The card_read_ext_ref_clock_freq reads the external reference clock
    frequency programmed in EEPROM.

    @param card card number of the Sidekiq of interest
    @param p_freq pointer to be updated with the reference clock's frequency

    @return int32_t  status where 0=success, anything else is an error
*/
int32_t card_read_ext_ref_clock_freq( uint8_t card, uint32_t *p_freq )
{
    uint8_t buf[REFERENCE_CLOCK_EXTERNAL_FREQ_LEN];
    int32_t status = 0;

    status = hal_read_eeprom(card,
                             REFERENCE_CLOCK_EXTERNAL_FREQ_ADDR,
                             buf,
                             REFERENCE_CLOCK_EXTERNAL_FREQ_LEN);
    if( 0 == status )
    {
        // little endian
        *p_freq = (((uint32_t) buf[0]) << 0)  |
                  (((uint32_t) buf[1]) << 8)  |
                  (((uint32_t) buf[2]) << 16) |
                  (((uint32_t) buf[3]) << 24);

        if( EXT_REF_CLOCK_FREQ_INVALID == *p_freq )
        {
            switch( _skiq_get_part(card) )
            {
            case( skiq_mpcie ):
                *p_freq = SKIQ_MPCIE_REF_CLOCK_DEFAULT;
                break;

            case( skiq_m2 ):
                *p_freq = SKIQ_M2_REF_CLOCK_DEFAULT;
                break;

            case( skiq_m2_2280 ):
            case( skiq_nv100 ):
                *p_freq = SKIQ_M2_2280_REF_CLOCK_DEFAULT;
                break;

            case( skiq_z2 ):
                *p_freq = SKIQ_Z2_REF_CLOCK_DEFAULT;
                break;

            case( skiq_x2 ):
                *p_freq = SKIQ_X2_REF_CLOCK_DEFAULT;
                break;

            case( skiq_x4 ):
                *p_freq = SKIQ_X4_REF_CLOCK_DEFAULT;
                break;

            case( skiq_z2p ):
                // no external ref supported for Z2p
                status = -ENOTSUP;
                break;

            case( skiq_z3u ):
                *p_freq = SKIQ_Z3U_REF_CLOCK_DEFAULT;
                break;
                
            default:
                status = -EINVAL;
                break;
            }
        }
    }

    return status;
}

int32_t card_write_usb_enumeration_delay(uint8_t card,
                                         uint16_t delay_ms)
{
    skiq_part_t part = skiq_part_invalid;
    uint8_t buf[USB_ENUMERATION_DELAY_LEN] = {0};
    int32_t status = -ENOTSUP;

    part = _skiq_get_part( card );
    // TX port has weird mapping for mPCIe
    if( (skiq_mpcie == part) || (skiq_m2 == part) )
    {
        // little endian
        buf[0] = delay_ms & 0xFF;
        buf[1] = (delay_ms >> 8) & 0xFF;

        status = card_write_eeprom(card,
                                   USB_ENUMERATION_DELAY_ADDR,
                                   buf,
                                   USB_ENUMERATION_DELAY_LEN);
    }

    return (status);
}

int32_t card_read_usb_enumeration_delay(uint8_t card,
                                        uint16_t* p_delay_ms)
{
    skiq_part_t part = skiq_part_invalid;
    uint8_t buf[USB_ENUMERATION_DELAY_LEN] = {0};
    uint8_t maj = 0, min = 0;
    uint16_t delay_ms = 0;
    int32_t status = -ENOTSUP;

    part = _skiq_get_part(card);
    if( (skiq_mpcie == part) || (skiq_m2 == part) )
    {
        status = skiq_read_fw_version(card, &maj, &min);
        if( 0 != status )
        {
            return status;
        }

        status = hal_read_eeprom(card,
                                 USB_ENUMERATION_DELAY_ADDR,
                                 buf,
                                 USB_ENUMERATION_DELAY_LEN);
        if( 0 != status )
        {
            return status;
        }

        // stored little endian
        delay_ms = buf[0] | (buf[1] << 8);
        if (UINT16_MAX == delay_ms)
        {
            // unconfigured EEPROM is treated as zero delay in firmware
            delay_ms = 0;
        }

        *p_delay_ms = delay_ms;

        if( FW_VERSION(FX2_MAJ_ENUM_DELAY_REQ, FX2_MIN_ENUM_DELAY_REQ) >
            FW_VERSION(maj, min) )
        {
            if( 0 != delay_ms )
            {
                // older firmware won't delay, display warning
                skiq_info("firmware v%d.%d is required for enumeration delay\n",
                          FX2_MAJ_ENUM_DELAY_REQ,
                          FX2_MIN_ENUM_DELAY_REQ);
            }
        }

    }

    return status;
}


int32_t
card_update_iq_complex_multiplier( uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint64_t lo_freq )
{
    float complex factor;
    int32_t status = 0;

    if ( ( _skiq_get_part( card ) == skiq_x2 ) ||
         ( _skiq_get_part( card ) == skiq_x4 ) )
    {
        if( hdl != skiq_rx_hdl_C1 && hdl != skiq_rx_hdl_D1 )
        {
            status = card_read_iq_cal_complex_multiplier( card, hdl, lo_freq, OPER_TEMPER_PLACEHOLDER,
                                                          &factor );
            if ( 0 == status )
            {
                debug_print("Updating multiplier on card %u and hdl %u to %f %c i*%f (mag %f) when retuning to %" PRIu64 " Hz",
                            card, hdl, crealf(factor),
                            ( fabsf(cimagf(factor)) == cimagf(factor) ) ? '+' : '-',
                            fabsf(cimagf(factor)), cabsf(factor), lo_freq);
                status = fpga_ctrl_write_iq_complex_multiplier( card, hdl, factor );
            }
        }
        else
        {
            // I/Q complex multiple not supported for C1/D1
            skiq_debug("I/Q multiplier not supported for handle %u (card=%u)\n", hdl, card);
            status = 0;
        }
    }
    else
    {
        /* not having I/Q complex calibration supported is not a crime */
        status = 0;
    }

    return status;
}

int32_t card_read_bias_index_calibration( uint8_t card,
                                          uint8_t *p_bias_index )
{
    int32_t status = 0;

    *p_bias_index = EEPROM_Z2_BIAS_CURRENT_UNCONFIGURED;

    if( _skiq_get_part(card) == skiq_z2 )
    {
        status = hal_read_eeprom( card,
                                  EEPROM_Z2_BIAS_CURRENT_ADDR,
                                  p_bias_index,
                                  EEPROM_Z2_BIAS_CURRENT_LENGTH );
        if ( 0 == status )
        {
            if( (*(p_bias_index) == EEPROM_Z2_BIAS_CURRENT_UNCONFIGURED) )
            {
                status = -ENOENT;
            }
        }
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}

int32_t card_write_bias_index_calibration( uint8_t card,
                                           uint8_t bias_index )
{
    int32_t status = 0;

    if( _skiq_get_part(card) == skiq_z2 )
    {
        if( bias_index < Z2_BIAS_INDEX_MIN ||
            bias_index > Z2_BIAS_INDEX_MAX )
        {
            _skiq_log(SKIQ_LOG_ERROR,
                      "Invalid bias current specified for card %u (must be %u-%u)\n",
                      card, Z2_BIAS_INDEX_MIN, Z2_BIAS_INDEX_MAX);
            status = -EINVAL;
        }
        else
        {
            status = card_write_eeprom( card,
                                        EEPROM_Z2_BIAS_CURRENT_ADDR,
                                        &bias_index,
                                        EEPROM_Z2_BIAS_CURRENT_LENGTH );
        }
    }
    else
    {
        status = -ENOTSUP;
    }

    return status;
}

int32_t card_read_cal_date( uint8_t card,
                            uint16_t *p_cal_year,
                            uint8_t *p_cal_week,
                            uint8_t *p_cal_interval )
{
    int32_t status=0;

#if (defined DISABLE_EEPROM_ACCESS)
    skiq_warning("!!!!! ACCESS TO EEPROM IS DISABLED, PROVIDING MANUAL CAL DATE !!!!!\n");
    *p_cal_year = MANUAL_CAL_YEAR;
    *p_cal_week = MANUAL_CAL_WEEK;
    *p_cal_interval = MANUAL_CAL_INTERVAL;
#else
    uint16_t addr = WARP_VOLTAGE_DATA_0_ADDR; // we're re-purposing warp voltage address for cal date
    uint8_t buf[2] = { 0, 0 };

    // increment address by two bytes for every entry
    addr += (CAL_DATE_WARP_VOLTAGE_OFFSET) * 2;

    status = hal_read_eeprom( card, addr, buf, 2 );
    if ( 0 == status )
    {
        uint16_t raw_cal_date=0;

        raw_cal_date = buf[0];
        raw_cal_date |= buf[1] << 8;

        if( (raw_cal_date == CAL_DATE_UNCONFIGURED) )
        {
            status = -ENOENT;
        }
        else
        {
            if( p_cal_year != NULL )
            {
                *p_cal_year = RBF_GET(raw_cal_date, CAL_YEAR);
                *p_cal_year += CAL_YEAR_START;
            }
            if( p_cal_week != NULL )
            {
                *p_cal_week = RBF_GET(raw_cal_date, CAL_WEEK_NUM);
                if( (*p_cal_week > MAX_WEEK_NUM) ||
                    (*p_cal_week < MIN_WEEK_NUM) )
                {
                    _skiq_log(SKIQ_LOG_ERROR, "Calibration week must be in range of %u-%u, read %u\n",
                              MIN_WEEK_NUM,
                              MAX_WEEK_NUM,
                              *p_cal_week);
                    status = -ERANGE;
                }
            }
            if( p_cal_interval != NULL )
            {
                *p_cal_interval = RBF_GET(raw_cal_date, CAL_INTERVAL);
            }
        }
    }
#endif

    return (status);
}

int32_t card_write_cal_date( uint8_t card,
                             uint16_t cal_year,
                             uint8_t cal_week,
                             uint8_t cal_interval )
{
    int32_t status=0;
    uint16_t addr = WARP_VOLTAGE_DATA_0_ADDR; // we're re-purposing warp voltage address for cal date
    uint16_t raw_cal_date;

    // make sure everything is within bounds
    if( (cal_year < CAL_YEAR_START) ||
        (cal_year > ((1<<CAL_YEAR_LEN)-1+CAL_YEAR_START)) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Calibration year must be in range of %u-%u\n",
                  CAL_YEAR_START, (1<<CAL_YEAR_LEN)-1+CAL_YEAR_START);
        status = -ERANGE;
    }
    else if( (cal_week > MAX_WEEK_NUM) ||
             (cal_week < MIN_WEEK_NUM) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Calibration week must be in range of %u-%u\n",
                  MIN_WEEK_NUM,
                  MAX_WEEK_NUM);
        status = -ERANGE;
    }
    else if( cal_interval > ((1<<CAL_INTERVAL_LEN)-1) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Calibration interval must be less than %u\n",
                  (1<<CAL_INTERVAL_LEN));
        status = -ERANGE;
    }
    else
    {
        raw_cal_date = cal_year-CAL_YEAR_START;
        RBF_SET(raw_cal_date, cal_week, CAL_WEEK_NUM);
        RBF_SET(raw_cal_date, cal_interval, CAL_INTERVAL);

        // increment address by two bytes for every entry
        addr += (CAL_DATE_WARP_VOLTAGE_OFFSET) * 2;

        status = card_write_eeprom( card, addr, (uint8_t*)(&raw_cal_date), 2 );
    }

    return (status);
}

int32_t card_reset_cal_date( uint8_t card )
{
    int32_t status=0;
    uint16_t addr = WARP_VOLTAGE_DATA_0_ADDR; // we're re-purposing warp voltage address for cal date
    uint16_t raw_cal_date = CAL_DATE_UNCONFIGURED;

    // increment address by two bytes for every entry
    addr += (CAL_DATE_WARP_VOLTAGE_OFFSET) * 2;
    status = card_write_eeprom( card, addr, (uint8_t*)(&raw_cal_date), 2 );

    return (status);
}

// locks mutex, disables write protect
int32_t card_eeprom_lock_and_disable_wp( uint8_t card )
{
    int32_t status=0;
    
    pthread_mutex_lock( &(_eeprom_mutex[card]) );
    status=card_eeprom_wp_update_if_avail( card, false );

    return (status);
}

// enables write protect, releases mutex
int32_t card_eeprom_unlock_and_enable_wp( uint8_t card )
{
    int32_t status=0;
    
    status=card_eeprom_wp_update_if_avail( card, true );
    pthread_mutex_unlock( &(_eeprom_mutex[card]) );

    return (status);
}

int32_t card_write_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status=0;

    if( (status=card_eeprom_lock_and_disable_wp(card)) == 0 )
    {
        status=hal_write_eeprom( card, addr, p_data, num_bytes );
    }
    if( card_eeprom_unlock_and_enable_wp(card) != 0 )
    {
        skiq_warning("Unable to successfully unlock EEPROM (card=%u)", card);
    }

    return (status);
}

int32_t card_eeprom_wp_update_if_avail( uint8_t card, bool write_protect )
{
    int32_t status = 0;
    
    if( card_eeprom_wp_avail(_skiq_get_part(card)) == true )
    {
        status = card_set_eeprom_wp( card, write_protect );
    }

    return (status);
}

bool card_eeprom_wp_avail( skiq_part_t part )
{
    bool wp_avail = false;

    switch( part )
    {
        case skiq_x4:
        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
        case skiq_nv100:
            wp_avail = true;
            break;

        default:
            wp_avail = false;
            break;
    }

    return (wp_avail);
}

int32_t card_set_eeprom_wp( uint8_t card, bool write_protect )
{
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( card );

    switch (part)
    {
        case skiq_x4:
            status = _write_x4_gpo_exp( card,
                                        SKIQ_X4_EEPROM_WP_OFFSET,
                                        SKIQ_X4_WP_LEN,
                                        write_protect ? SKIQ_X4_WP_ENABLE : SKIQ_X4_WP_DISABLE );
            break;

        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
            if ( write_protect )
            {
                status = io_enable_eeprom_wp( card );
            }
            else
            {
                status = io_disable_eeprom_wp( card );
            }
            break;

        case skiq_nv100:
            status = nv100_set_eeprom_wp( card, write_protect );
            break;

        default:
            status = -ENOTSUP;
            break;
    }

    return (status);
}

// locks mutex, disables write protect
int32_t card_fmc_eeprom_lock_and_disable_wp( uint8_t card )
{
    int32_t status=0;
    
    pthread_mutex_lock( &(_fmc_eeprom_mutex[card]) );
    status=card_fmc_eeprom_wp_update_if_avail( card, false );

    return (status);
}

// enables write protect, releases mutex
int32_t card_fmc_eeprom_unlock_and_enable_wp( uint8_t card )
{
    int32_t status=0;
    
    status=card_fmc_eeprom_wp_update_if_avail( card, true );
    pthread_mutex_unlock( &(_fmc_eeprom_mutex[card]) );

    return (status);
}

int32_t card_write_fmc_eeprom( uint8_t card, uint16_t addr, uint8_t *p_data, uint16_t num_bytes )
{
    int32_t status=0;

    if( (status=card_fmc_eeprom_lock_and_disable_wp(card)) == 0 )
    {
        status=hal_write_fmc_eeprom( card, addr, p_data, num_bytes );
    }
    if( card_fmc_eeprom_unlock_and_enable_wp(card) != 0 )
    {
        skiq_warning("Unable to successfully unlock EEPROM (card=%u)", card);
    }


    return (status);
}

int32_t card_fmc_eeprom_wp_update_if_avail( uint8_t card, bool write_protect )
{
    int32_t status = 0;
    
    if( card_fmc_eeprom_wp_avail(_skiq_get_part(card)) == true )
    {
        status = card_set_fmc_eeprom_wp( card, write_protect );
    }

    return (status);
}

bool card_fmc_eeprom_wp_avail( skiq_part_t part )
{
    bool wp_avail = false;

    switch( part )
    {
        case skiq_x4:
            wp_avail = true;
            break;
            
        default:
            wp_avail = false;
            break;
    }

    return (wp_avail);
}

int32_t card_set_fmc_eeprom_wp( uint8_t card, bool write_protect )
{
    int32_t status=0;
    uint8_t wp_pin = SKIQ_X4_WP_DISABLE;

    if( write_protect == true )
    {
        wp_pin = SKIQ_X4_WP_ENABLE;
    }
    status = _write_x4_gpo_exp( card,
                                SKIQ_X4_FMC_WP_OFFSET,
                                SKIQ_X4_WP_LEN,
                                wp_pin );
    
    return (status);
}

int32_t _write_x4_gpo_exp( uint8_t card, uint8_t offset, uint8_t len, uint8_t val )
{
    int32_t status=0;
    uint8_t read_val=0;

    pthread_mutex_lock( &_x4_gpo_exp_mutex[card] );
    // read the current state of the pins
    status = hal_read_i2c( card, SKIQ_X4_IO_EXP_ADDR, &read_val, 1 );
    if( status == 0 )
    {
        BF_SET( read_val, val, offset, len );
        status = hal_write_i2c( card, SKIQ_X4_IO_EXP_ADDR, &read_val, 1 );
    }
    pthread_mutex_unlock( &_x4_gpo_exp_mutex[card] );
    
    return (status);
}


/**************************************************************************************************/
int32_t card_fmc_eeprom_size( uint8_t card )
{
    int32_t size = 0;

    /*
      X2 rev A -- no libsidekiq support
      X2 rev B3 and earlier -- DBA FMC EEPROM (24AA128T-I/MNY 128kbit (16KB) )
      X2 rev B4 and later -- SBA FMC EEPROM (24AA024T-I/MNY 2kbit)
      X2 rev C -- SBA FMC EEPROM (24AA024T-I/MNY 2kbit)
      X4 rev A -- DBA FMC EEPROM (AT24C256C-MAHL-T 256kbit (32KB) )
      X4 rev B -- SBA FMC EEPROM (24AA024HT-I 2kbit)
     */

    switch ( _skiq_get_part( card ) )
    {
        case skiq_x2:
            if ( _skiq_get_hw_rev( card ) == hw_rev_b )
            {
                if ( _skiq_get_hw_rev_minor( card ) > 3 )
                {
                    size = 256;
                }
                else
                {
                    size = 16 * 1024;
                }
            }
            else
            {
                size = 256;
            }
            break;

        case skiq_x4:
            size = ( _skiq_get_hw_rev( card ) == hw_rev_a ) ? (32 * 1024) : 256;
            break;

        default:
            size = 0;
            break;
    }

    return size;
}


/**************************************************************************************************/
bool card_fmc_eeprom_has_dba( uint8_t card )
{
    bool has_dba = false;

    /*
      X2 rev A -- no libsidekiq support
      X2 rev B3 and earlier -- DBA FMC EEPROM (24AA128T-I/MNY 128kbit (16KB) )
      X2 rev B4 and later -- SBA FMC EEPROM (24AA024T-I/MNY 2kbit)
      X2 rev C -- SBA FMC EEPROM (24AA024T-I/MNY 2kbit)
      X4 rev A -- DBA FMC EEPROM (AT24C256C-MAHL-T 256kbit (32KB) )
      X4 rev B -- SBA FMC EEPROM (24AA024HT-I 2kbit)
     */

    switch ( _skiq_get_part( card ) )
    {
        case skiq_x2:
            if ( _skiq_get_hw_rev( card ) == hw_rev_b )
            {
                if ( _skiq_get_hw_rev_minor( card ) > 3 )
                {
                    has_dba = false;
                }
                else
                {
                    has_dba = true;
                }
            }
            else
            {
                has_dba = false;
            }
            break;

        case skiq_x4:
            has_dba = ( _skiq_get_hw_rev( card ) == hw_rev_a );
            break;

        default:
            has_dba = false;
            break;
    }
    debug_print("Has two byte addressing on Sidekiq %s (card %u)? --> %s\n",
                part_cstr( _skiq_get_part( card ) ), card, bool_cstr( has_dba ));

    return has_dba;
}
