/**
 * @file   card_services_temperature.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Wed Jun  8 15:31:49 2016
 *
 * @brief  Provides temperature sensor services
 *
 *
 * <pre>
 * Copyright 2016-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <inttypes.h>
#include <math.h>

#include "card_services.h"
#include "card_services_private.h"

#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_fpga.h"

#include "ad9361_driver.h"
#include "hal_nv100.h"

#include "gpsdo_fpga.h"

/* enable debug_print and debug_print_plain when DEBUG_TEMP_SENSOR is defined */
#if (defined DEBUG_TEMP_SENSOR)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** MACROS *****/


/***** DEFINES *****/

#define FPGA_VERS_XADC_TEMP_SUPPORT     FPGA_VERSION(3,4,0)


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


static bool
use_register_sensor( uint8_t card )
{
    bool bstatus = false;
    int32_t status = 0;
    bool has_gpsdo = false;
    int32_t gpsdo_status = 0;
    
    status = gpsdo_fpga_has_module( card, &has_gpsdo, &gpsdo_status );
    if( status == 0 )
    {
        /* not a problem to not have GPSDO */
        if( has_gpsdo == true )
        {
            /* if the GPSDO is enabled and supported, use the register sensor(s) */
            status = gpsdo_fpga_is_enabled( card, &bstatus );
            if ( status != 0 )
            {
                /* in the case of error reading GPSDO state, fallback to not using register sensor(s) */
                bstatus = false;
            }
        }
    }    

    return bstatus;
}


/***** GLOBAL FUNCTIONS *****/


/*************************************************************************************************/
/**
     @retval 0 Success
     @retval -EINVAL Sensor index is out of range
     @retval -EIO I/O communication error occurred during measurement
     @retval -ENODEV device is not available for a temperature measurement
     @retval -EAGAIN Sensor measurement is temporarily unavailable, try again later
     @retval -ENOTSUP no sensors for associated Sidekiq product
 */
int32_t card_read_temp( uint8_t card,
                        uint8_t sensor,
                        int8_t *p_temp_in_deg_C )
{
    int32_t status = 0;

    /* Sidekiq NV100 is the oddball, it has no traditional temperature sensors (I2C or FPGA
     * registers). */
    if ( _skiq_get_part( card ) == skiq_nv100 )
    {
        debug_print("Using the product specific sensor interface for temperature on card %" PRIu8
                    " and sensor %" PRIu8 "\n", card, sensor);
        status = nv100_read_temp( card, sensor, p_temp_in_deg_C );
    }
    else if ( use_register_sensor( card ) )
    {
        debug_print("Using the register sensors interface for temperature on card %" PRIu8
                    " and sensor %" PRIu8 "\n", card, sensor);
        status = card_read_temp_reg( card, sensor, p_temp_in_deg_C );
    }
    else
    {
        debug_print("Using the I2C sensors interface for temperature on card %" PRIu8
                    " and sensor %" PRIu8 "\n", card, sensor);
        status = card_read_temp_i2c( card, sensor, p_temp_in_deg_C );
    }

    return status;
}


/*************************************************************************************************/
int32_t card_read_rfic_temp(uint8_t card, int8_t* p_temp_in_deg_C)
{
    int32_t status = 0;
    rfic_t rfic;

    rfic = _skiq_get_generic_rfic_instance(card);

    status = rfic_read_temp( &rfic, p_temp_in_deg_C );

    return(status);
}


/*************************************************************************************************/
int32_t card_read_fpga_temp(uint8_t card, int8_t* p_temp_in_deg_C)
{
    // The XADC interface of the FPGA has registers that can be read to
    // obtain specific values. Register address 0x00 gets us the internal
    // temperature of the FPGA.
    const uint32_t xadc_temp_register = 0;
    const int32_t register_poll_max = 1000;
    float temperature = 0.0;
    int32_t status = -ENOTSUP;
    uint32_t data = 0;
    int32_t i = 0;
    skiq_part_t part;

    /* NOTE: This function is only supported on certain FPGAs and with a minimum required FPGA
     * bitstream version (::FPGA_VERS_XADC_TEMP_SUPPORT) */
    part = _skiq_get_part( card );
    if ( ( part == skiq_m2 ) ||
         ( part == skiq_x2 ) ||
         ( part == skiq_x4 ) ||
         ( part == skiq_m2_2280 ) ||
         ( part == skiq_z2p ) ||
         ( part == skiq_z3u ) ||
         ( part == skiq_nv100 ) )
    {
        if ( _skiq_meets_FPGA_VERSION(card, FPGA_VERS_XADC_TEMP_SUPPORT) )
        {
            status = 0;
        }
        else
        {
            status = -ENOSYS;
        }
    }

    if ( status == 0 )
    {
        // 1. Write address [6:0] + start bit [7]
        data = (0x01 << XADC_START_STROBE_OFFSET) | xadc_temp_register;
        status = sidekiq_fpga_reg_write(card, FPGA_REG_XADC_START, data);
    }

    // 2. Read until data_valid (bit 16) is high, and grab data when high.
    for( i = 0; ( i < register_poll_max ) && ( status == 0 ); i++ )
    {
        status = sidekiq_fpga_reg_read(card, FPGA_REG_XADC_READ_BACK, &data);
        if ( ( 0 == status ) && ( RBF_GET(data, XADC_READ_BACK_DONE) > 0 ) )
        {
            break;
        }

        hal_nanosleep(1000);
    }

    /* if the loop is complete, but the XADC read-back hasn't indicated completion, mark it
     * ETIMEDOUT */
    if ( ( 0 == status ) && ( RBF_GET(data, XADC_READ_BACK_DONE) == 0 ) )
    {
        status = -ETIMEDOUT;
    }

    // 3. Write to 0 (really only bit 7 needs to be zero, but why not all).
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_XADC_START, 0);
    }

    if ( status == 0 )
    {
        // Data is stored in bits 15:0.
        data &= 0x0000FFFF;
        data >>= 4; // 12 bit result left justified
        // Equation taken from UG480 7 Series XADC user guide, page 33.
        temperature = ((data * 503.975) / 4096.0) - 273.15;
        if( ((float) INT8_MAX) < temperature )
        {
            temperature = (float) INT8_MAX;
        }
        else if( ((float) INT8_MIN) > temperature )
        {
            temperature = (float) INT8_MIN;
        }
        *p_temp_in_deg_C = (int8_t) temperature;
    }

    return status;
}
