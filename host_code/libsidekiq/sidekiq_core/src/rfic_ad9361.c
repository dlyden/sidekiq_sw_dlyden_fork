/*****************************************************************************/
/** @file rfic_ad9361.c
 
 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include "rfic_ad9361.h"
#include "ad9361_driver.h"
#include "sidekiq_api.h"
#include "sidekiq_hal.h" // 9361 GPIO mapping
#include "sidekiq_private.h"
#include "sidekiq_types_private.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_reg_defs.h"
#include "bit_ops.h"
#include "help_prog_ad9361_from_file.h"
#include "sidekiq_fpga_ctrl.h"

#include "card_services.h"
#include "io_m2_2280.h"
#include "dctcxo_sit5356.h"
#include "gpsdo_fpga.h"

#if (defined DEBUG_RFIC_AD9361)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>

#define AD9361_ADC_RES (12)
#define AD9361_DAC_RES (12)

/* Register address to control the rx_delay setting in the ad9361 */
#define AD9361_RX_DELAY_ADDR (6)

/* Delay setting necessary for Sidekiq */
#define AD9361_MPCIE_RX_DELAY_VAL  (3)
#define AD9361_M2_RX_DELAY_VAL  (4) // TODO: we probably need to fine tune this for m2
#define AD9361_Z2_RX_DELAY_VAL (10) // prbs sweep passed from 7-15...so we'll pick the middle ground
#define AD9361_M2_2280_RX_DELAY_VAL     8 /* PRBS sweep passed from 5 to 11, so pick the middle */
#define AD9361_Z2P_RX_DELAY_VAL (10) // selected to match Z2...needs to be optimized
#define AD9361_Z3U_RX_DELAY_VAL (10) // selected to match Z2p...needs to be optimized

/* Register address to control the tx_delay setting in the ad9361 */
#define AD9361_TX_DELAY_ADDR (7)
/* Delay setting necessary for Sidekiq */
#define AD9361_MPCIE_TX_DELAY_VAL  (3) 
#define AD9361_M2_TX_DELAY_VAL (4) // TODO: this has not been analyzed for m2 and loopback should be used to dial this in
#define AD9361_Z2_TX_DELAY_VAL (10) // TODO: completely arbitrary
#define AD9361_M2_2280_TX_DELAY_VAL  4 /* @todo this has not been analyzed for M.2-2280 and loopback should be used to dial this in */
#define AD9361_Z2P_TX_DELAY_VAL (10) // TODO: just match Z2 for now
#define AD9361_Z3U_TX_DELAY_VAL (10) // TODO: just match Z2p for now

/* AD9361 Frequency Range - RX:70 to 6000 MHz, TX:46.875 to 6000 MHz */
#define AD9361_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(70ULL*RATE_MHZ_TO_HZ)
#define AD9361_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define AD9361_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(46.875L*RATE_MHZ_TO_HZ)
#define AD9361_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

// TX test tone offset...see reg 0x3F4
#define TX_TEST_TONE_FREQ_OFFSET (32)

/** @brief Max quarter dB attentuation for Tx. */
#define MAX_TX_QUARTER_DB_ATTEN (359)
/** @brief Min quarter dB attentuation for Tx. */
#define MIN_TX_QUARTER_DB_ATTEN (0)

// the default warp voltage value if one isn't stored in EEPROM
#define DEFAULT_WARP_VOLTAGE_AUX_DAC    511
#define DEFAULT_WARP_VOLTAGE_M2_2280    (1 << 15) /* default warp voltage on M.2 2280 / Z2p / Z3u is mid-rail of a 16-bit value */

#define AD9361_CONTROL_OUTPUT_MODE_GAIN_CONTROL_RXA1 (0x16)
#define AD9361_CONTROL_OUTPUT_MODE_GAIN_CONTROL_RXA2 (0x17)
#define AD9361_CONTROL_OUTPUT_MODE_GAIN_BITS (0x7F)

#define SLOT_0 (0)
#define Z2P_DAC_I2C_ADDR (0x4E)

typedef struct
{
    bool chan1_enable;
    bool chan2_enable;
} chan_enable_t;

typedef struct
{
    uint32_t sample_rate;
    uint32_t bandwidth;
    double bw_percent;
} sample_rate_t;

typedef struct
{
    skiq_freq_tune_mode_t tune_mode;
    uint8_t curr_ad9361_index; // current hop frequency
    uint8_t next_ad9361_index; // next hop freq to use
    uint8_t next_ad9361_slot;  // next slot available
    // refer to http://confluence/display/EN/Sidekiq+X4+-+Fast+Frequency+Hopping
    freq_hop_t mailbox; // this is the only thing users can write to
    freq_hop_t curr_hop; // this is populated from mailbox when writing next hop
    freq_hop_t next_hop; // this is populated only after a hop was performed from next hop
    uint16_t num_profiles;
    ad9361_fastlock_profile_t *p_profiles;
} fastlock_config_t;

#define FASTLOCK_CONFIG_INITIALIZER                            \
    {                                                          \
        .tune_mode = skiq_freq_tune_mode_standard,             \
        .curr_ad9361_index = 0,                                \
        .next_ad9361_index = 0,                                \
        .next_ad9361_slot = 0,                                 \
        .curr_hop =                                            \
            {.index=SKIQ_MAX_NUM_FREQ_HOPS, .freq=0},          \
        .next_hop =                                            \
            {.index=SKIQ_MAX_NUM_FREQ_HOPS, .freq=0},          \
        .mailbox =                                             \
            {.index=SKIQ_MAX_NUM_FREQ_HOPS, .freq=0},          \
        .num_profiles = 0,                                     \
        .p_profiles = NULL,                                    \
    }


static chan_enable_t _rx_enable[SKIQ_MAX_NUM_CARDS];
static chan_enable_t _tx_enable[SKIQ_MAX_NUM_CARDS];

static skiq_tx_quadcal_mode_t _tx_quadcal[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end];

static sample_rate_t _rx_sample_rate[SKIQ_MAX_NUM_CARDS][skiq_rx_hdl_end];
static sample_rate_t _tx_sample_rate[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end];

static fastlock_config_t _rx_fastlock_config[SKIQ_MAX_NUM_CARDS] =
{ [0 ... (SKIQ_MAX_NUM_CARDS-1)] = FASTLOCK_CONFIG_INITIALIZER };

static fastlock_config_t _tx_fastlock_config[SKIQ_MAX_NUM_CARDS] =
{ [0 ... (SKIQ_MAX_NUM_CARDS-1)] = FASTLOCK_CONFIG_INITIALIZER };

static uint16_t _warp_voltage[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = 0 };

static skiq_rx_cal_mode_t _rx_cal_mode[SKIQ_MAX_NUM_CARDS] =
{ [0 ... (SKIQ_MAX_NUM_CARDS-1)] = skiq_rx_cal_mode_manual };

static uint32_t _rx_cal_mask[SKIQ_MAX_NUM_CARDS] =
{ [0 ... (SKIQ_MAX_NUM_CARDS-1)] = skiq_rx_cal_type_none };

static ARRAY_WITH_DEFAULTS(bool, rfic_active, SKIQ_MAX_NUM_CARDS, false);

// TODO: there's probably a cleaner way to do this
// need to initialize the read/write reg functions so the ad9361_driver knows how to do reg transactions
int32_t hal_ad9361_write_reg(uint32_t chip_id, uint16_t addr, uint8_t data)
{
    // Note: chip_id = card...always use CS=0
    return (hal_rfic_write_reg(chip_id, 0, addr, data));
}
int32_t hal_ad9361_read_reg(uint32_t chip_id, uint16_t addr, uint8_t *p_data)
{
    // Note: chip_id = card...always use CS=0
    return (hal_rfic_read_reg(chip_id, 0, addr, p_data));
}

static void _get_rfic_config_by_part( uint8_t card,
                                      bool *p_configure_gpio,
                                      uint8_t *p_gpio_pos,
                                      bool *p_gpio_state,
                                      uint8_t *p_num_gpio,
                                      uint8_t *p_rx_delay,
                                      uint8_t *p_tx_delay );

static int32_t _reset_and_init( rf_id_t *p_id );
static void _display_version_info( void );
static int32_t _release( rf_id_t *p_id );

static int32_t _write_rx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq );
static int32_t _write_tx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq );

static int32_t _write_rx_spectrum_invert( rf_id_t *p_id, bool invert );
static int32_t _write_tx_spectrum_invert( rf_id_t *p_id, bool invert );

static int32_t _write_tx_attenuation( rf_id_t *p_id, uint16_t atten );
static int32_t _read_tx_attenuation( rf_id_t *p_id, uint16_t *p_atten );
static int32_t _read_tx_attenuation_range( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min );

static int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode );
static int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode );

static int32_t _read_rx_gain_range( rf_id_t *p_id, uint8_t *p_gain_max, uint8_t *p_gain_min );
static int32_t _write_rx_gain( rf_id_t *p_id, uint8_t gain );
static int32_t _read_rx_gain( rf_id_t *p_id, uint8_t *p_gain );

static int32_t _read_adc_resolution( uint8_t *p_adc_res );
static int32_t _read_dac_resolution( uint8_t *p_dac_res );

static int32_t _read_warp_voltage( rf_id_t *p_id, uint16_t *p_warp_voltage );
static int32_t _write_warp_voltage( rf_id_t *p_id, uint16_t warp_voltage );
static int32_t _read_warp_voltage_extended_range( rf_id_t *p_id, uint32_t *p_warp_voltage );
static int32_t _write_warp_voltage_extended_range( rf_id_t *p_id, uint32_t warp_voltage );

static int32_t _enable_rx_chan( rf_id_t *p_id, bool enable );
static int32_t _enable_rx_chan_all( rf_id_t *p_id, bool enable );
static int32_t _enable_tx_chan( rf_id_t *p_id, bool enable );
static int32_t _enable_tx_chan_all( rf_id_t *p_id, bool enable );

static int32_t _config_tx_test_tone( rf_id_t *p_id, bool enable );
static int32_t _read_tx_test_tone_freq( rf_id_t *p_id,
                                        int32_t *p_test_freq_offset );

static void _read_min_sample_rate( rf_id_t *p_id, uint32_t *p_min_sample_rate );
static void _read_max_sample_rate( rf_id_t *p_id, uint32_t *p_max_sample_rate );

static int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );
static int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );

static int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );
static int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );

static int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
static int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );

static int32_t _write_sample_rate( rf_id_t *p_id,
                                   uint32_t rate,
                                   double tx_percent,
                                   double rx_percent );

static int32_t _read_rx_filter_overflow( rf_id_t *p_id, bool *p_rx_fir );
static int32_t _read_tx_filter_overflow( rf_id_t *p_id, bool *p_int3, bool *p_hb3,
                                         bool *p_hb2, bool *p_qec, bool *p_hb1, bool *p_tx_fir );

static int32_t _read_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode );
static int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode );
static int32_t _run_tx_quadcal( rf_id_t *p_id );

static bool _is_chan_enable_xport_dependent( rf_id_t *p_id );

static int32_t _read_control_output_rx_gain_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );
static int32_t _write_control_output_config( rf_id_t *p_id, uint8_t mode, uint8_t ena );
static int32_t _read_control_output_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );

static int32_t _init_from_file( rf_id_t *p_id, FILE* p_file );

static int32_t _power_down_tx( rf_id_t *p_id );

static int32_t _enable_pin_gain_ctrl( rf_id_t *p_id );

static uint32_t _read_hop_on_ts_gpio( rf_id_t *p_id, uint8_t *p_chip_index );

static int32_t _write_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
static int32_t _read_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

static int32_t _write_freq_tune_mode( uint8_t id,
                                      skiq_freq_tune_mode_t mode,
                                      fastlock_config_t *p_fastlock_config,
                                      int32_t (*p_disable_fastlock)( uint32_t id ) );

static int32_t _write_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
static int32_t _read_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

static int32_t _read_rx_hop_list( rf_id_t *p_id,
                                  uint16_t *p_num_freq,
                                  uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );

static int32_t _read_tx_hop_list( rf_id_t *p_id,
                                  uint16_t *p_num_freq,
                                  uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );

static int32_t _config_rx_hop_list( rf_id_t *p_id,
                                    uint16_t num_freq,
                                    uint64_t freq_list[],
                                    uint16_t initial_index );

static int32_t _config_tx_hop_list( rf_id_t *p_id,
                                    uint16_t num_freq,
                                    uint64_t freq_list[],
                                    uint16_t initial_index );

static int32_t _config_hop_list( uint8_t card,
                                 fastlock_config_t *p_fastlock_config,
                                 uint16_t num_freq,
                                 uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS],
                                 uint16_t initial_index,
                                 int32_t (*p_enable_fastlock)( uint32_t id ),
                                 int32_t (*p_disable_fastlock)( uint32_t id ),
                                 int32_t (*p_write_fastlock_ctrl)( uint32_t id, ad9361_fastlock_ctrl_t ctrl ),
                                 int32_t (*p_gen_fastlock_regs)( uint32_t id, uint64_t lo, uint8_t regs[AD9361_NUM_FASTLOCK_REGS] ),
                                 int32_t (*p_store_fastlock_regs)( uint32_t id, uint8_t fastlock_index, uint8_t regs[AD9361_NUM_FASTLOCK_REGS] ) );


static int32_t _write_next_rx_hop( rf_id_t *p_id, uint16_t freq_index );

static int32_t _write_next_tx_hop( rf_id_t *p_id, uint16_t freq_index );

static int32_t _write_next_hop( uint8_t card,
                                uint16_t freq_index,
                                fastlock_config_t *p_fastlock_config,
                                int32_t (*p_store_fastlock_regs)( uint32_t id, uint8_t fastlock_index, uint8_t regs[] ) );


static int32_t _rx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );

static int32_t _tx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );

static int32_t _hop( uint8_t card,
                     uint64_t rf_timestamp,
                     double *p_act_freq,
                     fastlock_config_t *p_fastlock_config,
                     int32_t (*p_config_fastlock_profile)( uint32_t id, uint8_t fastlock_index ) );

static int32_t _read_curr_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop );
static int32_t _read_next_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop );

static int32_t _read_curr_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop );
static int32_t _read_next_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop );

static inline int32_t _read_temp( rf_id_t *p_id, int8_t *p_temp_in_deg_C )
{
    return ad9361_read_temp( p_id->card, p_temp_in_deg_C );
}

static int32_t _write_ad5693_dac( uint8_t card, uint16_t val );
static int32_t _configure_ref_clock( rf_id_t *p_id );
static int32_t _read_warp_voltage_aux_dac( rf_id_t *p_id,
                                           uint16_t *p_warp_voltage );
static int32_t _read_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode );
static int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode );

static int32_t _run_rx_cal( rf_id_t *p_id );

static int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask );
static int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask );

static int32_t _read_rx_cal_types_avail( rf_id_t *p_id, uint32_t *p_cal_mask );

static int32_t _swap_rx_port_config( rf_id_t *p_id,
                                     uint8_t a_port,
                                     uint8_t b_port );

static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

static skiq_rx_hdl_t _rx_hdl_map_other(rf_id_t *p_id);
static skiq_tx_hdl_t _tx_hdl_map_other(rf_id_t *p_id);

static int32_t _read_rx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth );
static int32_t _read_tx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth );

static int32_t _write_rx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t bandwidth );
static int32_t _write_tx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t bandwidth );

static int32_t _read_rx_fir_config( rf_id_t *p_id, uint8_t *p_num_taps, uint8_t *p_fir_decimation );
static int32_t _read_rx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs );
static int32_t _write_rx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs );
static int32_t _read_rx_fir_gain( rf_id_t *p_id, skiq_rx_fir_gain_t *p_gain );
static int32_t _write_rx_fir_gain( rf_id_t *p_id, skiq_rx_fir_gain_t gain );

static int32_t _read_tx_fir_config( rf_id_t *p_id, uint8_t *p_num_taps, uint8_t *p_fir_interpolation );
static int32_t _read_tx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs );
static int32_t _write_tx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs );
static int32_t _read_tx_fir_gain( rf_id_t *p_id, skiq_tx_fir_gain_t *p_gain );
static int32_t _write_tx_fir_gain( rf_id_t *p_id, skiq_tx_fir_gain_t gain );


rfic_functions_t rfic_ad9361_funcs =
{
    .reset_and_init           = _reset_and_init,
    .display_version_info     = _display_version_info,
    .release                  = _release,
    .write_rx_freq            = _write_rx_freq,
    .write_tx_freq            = _write_tx_freq,
    .write_rx_spectrum_invert = _write_rx_spectrum_invert,
    .write_tx_spectrum_invert = _write_tx_spectrum_invert,
    .write_tx_attenuation     = _write_tx_attenuation,
    .read_tx_attenuation      = _read_tx_attenuation,
    .read_tx_attenuation_range = _read_tx_attenuation_range,
    .write_rx_gain_mode       = _write_rx_gain_mode,
    .read_rx_gain_mode        = _read_rx_gain_mode,
    .read_rx_gain_range       = _read_rx_gain_range,
    .write_rx_gain            = _write_rx_gain,
    .read_rx_gain             = _read_rx_gain,
    .read_adc_resolution      = _read_adc_resolution,
    .read_dac_resolution      = _read_dac_resolution,
    .read_warp_voltage        = _read_warp_voltage,
    .write_warp_voltage       = _write_warp_voltage,
    .read_warp_voltage_extended_range = _read_warp_voltage_extended_range,
    .write_warp_voltage_extended_range = _write_warp_voltage_extended_range,
    .enable_rx_chan           = _enable_rx_chan,
    .enable_tx_chan           = _enable_tx_chan,
    .config_tx_test_tone      = _config_tx_test_tone,
    .read_tx_test_tone_freq   = _read_tx_test_tone_freq,
    .write_tx_test_tone_freq  = NULL,
    .read_min_rx_sample_rate     = _read_min_sample_rate,
    .read_max_rx_sample_rate     = _read_max_sample_rate,
    .read_min_tx_sample_rate     = _read_min_sample_rate,
    .read_max_tx_sample_rate     = _read_max_sample_rate,
    .write_rx_sample_rate_and_bandwidth = _write_rx_sample_rate_and_bandwidth,
    .write_rx_sample_rate_and_bandwidth_multi = NULL,
    .write_tx_sample_rate_and_bandwidth = _write_tx_sample_rate_and_bandwidth,
    .read_rx_sample_rate      = _read_rx_sample_rate,
    .read_tx_sample_rate      = _read_tx_sample_rate,
    .read_rx_chan_bandwidth   = _read_rx_chan_bandwidth,
    .read_tx_chan_bandwidth   = _read_tx_chan_bandwidth,
    .init_gpio                = NULL,   // TODO
    .write_gpio               = NULL,   // TODO
    .read_gpio                = NULL,   // TODO
    .read_rx_filter_overflow  = _read_rx_filter_overflow,
    .read_tx_filter_overflow  = _read_tx_filter_overflow,
    .read_fpga_rf_clock       = NULL, // not sure this concept applies to 9361-based FPGAs
    .read_tx_quadcal_mode     = _read_tx_quadcal_mode,
    .write_tx_quadcal_mode    = _write_tx_quadcal_mode,
    .run_tx_quadcal           = _run_tx_quadcal,
    .read_rx_stream_hdl_conflict = NULL,
    .is_chan_enable_xport_dependent = _is_chan_enable_xport_dependent,
    .read_control_output_rx_gain_config = _read_control_output_rx_gain_config,
    .write_control_output_config = _write_control_output_config,
    .read_control_output_config = _read_control_output_config,
    .init_from_file           = _init_from_file,
    .power_down_tx            = _power_down_tx,
    .enable_pin_gain_ctrl     = _enable_pin_gain_ctrl,
    .read_hop_on_ts_gpio      = _read_hop_on_ts_gpio,
    .write_rx_freq_tune_mode  = _write_rx_freq_tune_mode,
    .read_rx_freq_tune_mode   = _read_rx_freq_tune_mode,
    .write_tx_freq_tune_mode  = _write_tx_freq_tune_mode,
    .read_rx_freq_tune_mode   = _read_rx_freq_tune_mode,
    .config_rx_hop_list       = _config_rx_hop_list,
    .read_rx_hop_list         = _read_rx_hop_list,
    .read_tx_freq_tune_mode   = _read_tx_freq_tune_mode,
    .config_tx_hop_list       = _config_tx_hop_list,
    .read_tx_hop_list         = _read_tx_hop_list,
    .write_next_rx_hop        = _write_next_rx_hop,
    .write_next_tx_hop        = _write_next_tx_hop,
    .rx_hop                   = _rx_hop,
    .tx_hop                   = _tx_hop,
    .read_curr_rx_hop         = _read_curr_rx_hop,
    .read_next_rx_hop         = _read_next_rx_hop,
    .read_curr_tx_hop         = _read_curr_tx_hop,
    .read_next_tx_hop         = _read_next_tx_hop,
    .read_rx_cal_mode         = _read_rx_cal_mode,
    .write_rx_cal_mode        = _write_rx_cal_mode,
    .run_rx_cal               = _run_rx_cal,
    .read_rx_cal_mask         = _read_rx_cal_mask,
    .write_rx_cal_mask        = _write_rx_cal_mask,
    .read_rx_cal_types_avail  = _read_rx_cal_types_avail,
    .read_rx_enable_mode      = NULL,
    .read_tx_enable_mode      = NULL,
    .write_rx_enable_mode     = NULL,
    .write_tx_enable_mode     = NULL,
    .read_temp                = _read_temp,
    .read_auxadc              = NULL,
    .swap_rx_port_config      = _swap_rx_port_config,
    .swap_tx_port_config      = NULL,
    .read_rf_capabilities     = _read_rf_capabilities,
    .write_ext_clock_select   = NULL,
    .rx_hdl_map_other      = _rx_hdl_map_other,
    .tx_hdl_map_other      = _tx_hdl_map_other,
    .read_rx_analog_filter_bandwidth = _read_rx_analog_filter_bandwidth,
    .read_tx_analog_filter_bandwidth = _read_tx_analog_filter_bandwidth,
    .write_rx_analog_filter_bandwidth = _write_rx_analog_filter_bandwidth,
    .write_tx_analog_filter_bandwidth = _write_tx_analog_filter_bandwidth,

    /****** RFIC Rx FIR function pointers ******/
    .read_rx_fir_config        = _read_rx_fir_config,
    .read_rx_fir_coeffs        = _read_rx_fir_coeffs,
    .write_rx_fir_coeffs       = _write_rx_fir_coeffs,
    .read_rx_fir_gain          = _read_rx_fir_gain,
    .write_rx_fir_gain         = _write_rx_fir_gain,

    /****** RFIC Tx FIR function pointers ******/
    .read_tx_fir_config        = _read_tx_fir_config,
    .read_tx_fir_coeffs        = _read_tx_fir_coeffs,
    .write_tx_fir_coeffs       = _write_tx_fir_coeffs,
    .read_tx_fir_gain          = _read_tx_fir_gain,
    .write_tx_fir_gain         = _write_tx_fir_gain,
};


static int32_t ad9361_rx_fir_gain_to_skiq( ad9361_rx_fir_gain_t ad9361_gain,
                                           skiq_rx_fir_gain_t *p_skiq_gain )
{
    int32_t status = 0;

    /* Provide a translation between enums */
    switch ( ad9361_gain )
    {
        case ad9361_rx_fir_gain_neg_12:
            *p_skiq_gain = skiq_rx_fir_gain_neg_12;
            break;

        case ad9361_rx_fir_gain_neg_6:
            *p_skiq_gain = skiq_rx_fir_gain_neg_6;
            break;

        case ad9361_rx_fir_gain_0:
            *p_skiq_gain = skiq_rx_fir_gain_0;
            break;

        case ad9361_rx_fir_gain_6:
            *p_skiq_gain = skiq_rx_fir_gain_6;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}


static int32_t skiq_rx_fir_gain_to_ad9361( skiq_rx_fir_gain_t skiq_gain,
                                           ad9361_rx_fir_gain_t *p_ad9361_gain )
{
    int32_t status = 0;

    /* Provide a translation between enums */
    switch ( skiq_gain )
    {
        case skiq_rx_fir_gain_neg_12:
            *p_ad9361_gain = ad9361_rx_fir_gain_neg_12;
            break;

        case skiq_rx_fir_gain_neg_6:
            *p_ad9361_gain = ad9361_rx_fir_gain_neg_6;
            break;

        case skiq_rx_fir_gain_0:
            *p_ad9361_gain = ad9361_rx_fir_gain_0;
            break;

        case skiq_rx_fir_gain_6:
            *p_ad9361_gain = ad9361_rx_fir_gain_6;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}


static int32_t ad9361_tx_fir_gain_to_skiq( ad9361_tx_fir_gain_t ad9361_gain,
                                           skiq_tx_fir_gain_t *p_skiq_gain )
{
    int32_t status = 0;

    /* Provide a translation between enums */
    switch ( ad9361_gain )
    {
        case ad9361_tx_fir_gain_neg_6:
            *p_skiq_gain = skiq_tx_fir_gain_neg_6;
            break;

        case ad9361_tx_fir_gain_0:
            *p_skiq_gain = skiq_tx_fir_gain_0;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}


static int32_t skiq_tx_fir_gain_to_ad9361( skiq_tx_fir_gain_t skiq_gain,
                                           ad9361_tx_fir_gain_t *p_ad9361_gain )
{
    int32_t status = 0;

    /* Provide a translation between enums */
    switch ( skiq_gain )
    {
        case skiq_tx_fir_gain_neg_6:
            *p_ad9361_gain = ad9361_tx_fir_gain_neg_6;
            break;

        case skiq_tx_fir_gain_0:
            *p_ad9361_gain = ad9361_tx_fir_gain_0;
            break;

        default:
            status = -EINVAL;
            break;
    }

    return status;
}


// NOTE: this code is from dropkiq dkiq_hal_write_dac...which uses the same chip
int32_t _write_ad5693_dac( uint8_t card, uint16_t val )
{
    uint8_t p_buf[3] = {0, 0, 0};
    int32_t status = 0;

    // See page 21 of AD5693RACPZ datasheet.
    p_buf[0] = 0x30; // DAC_CMD_WRITE_INPUT_AND_DAC_REG
    p_buf[1] = (val >> 8) & 0xFF;
    p_buf[2] = val & 0xFF;

    status = hal_write_i2c(card, Z2P_DAC_I2C_ADDR, p_buf, 3);

    // update the cached value since the parts write only
    if( status == 0 )
    {
        _warp_voltage[card] = val;
    }
    
    return (status);
}

int32_t _configure_ref_clock( rf_id_t *p_id )
{
    int32_t status;
    skiq_part_t part = _skiq_get_part( p_id->card );
    skiq_ref_clock_select_t ref_clock;

    /* found out what ref clock source is selected */
    status = skiq_read_ref_clock_select( p_id->card, &ref_clock );
    if ( status == -ENOTSUP )
    {
        /* Not supporting reading the reference clock selection is not an error, it just means that
         * the setting is not well-known because the platform does not offer software configuration
         * to switch between reference clocks.  Presently it is only mPCIe rev B that is affected as
         * all other platforms (so far) support selecting a reference clock via software */
        status = 0;
    }

    /* If the reference clock is external, consider the stored reference clock frequency */
    if ( status == 0 )
    {
        if ( ref_clock == skiq_ref_clock_external )
        {
            uint32_t ref_clk_freq = 0;

            status = card_read_ext_ref_clock_freq(p_id->card, &ref_clk_freq);
            if ( status == 0 )
            {
                /* If the part is an M.2-2280/Z3u, there's an extra step to reference clock selection
                 * that is handled by an I/O expander and not the AD9361 GPIO like the other
                 * Sidekiq radios */
                if ( (part == skiq_m2_2280) || (part == skiq_z3u) )
                {
                    /* Note: while this may look strange, the control for the reference clock 
                       is actually the same for m2_2280 and Z3u, so this function works the same */
                    /* Set an external reference clock source and reference frequency */
                    status = m2_2280_io_set_ext_ref_clock_source( p_id->card, ref_clk_freq );

                    /* If the reference clock frequency is 10MHz, the on-board circuitry will use a
                     * PLL to get it to 40MHz (SKIQ_M2_2280_REF_CLOCK_DEFAULT).  The above call to
                     * m2_2280_io_set_ext_ref_clock_source() sets up the PLL accordingly, but the
                     * AD9361 will see 40MHz, so munge ref_clk_freq before the call to
                     * ad9361_config_ext_ref_clock_freq() */
                    if ( ref_clk_freq == (uint32_t)10e6 )
                    {
                        if( part == skiq_m2_2280 )
                        {
                            ref_clk_freq = (uint32_t)SKIQ_M2_2280_REF_CLOCK_DEFAULT;
                        }
                        else if( part == skiq_z3u )
                        {
                            ref_clk_freq = (uint32_t)SKIQ_Z3U_REF_CLOCK_DEFAULT;
                        }
                    }
                }
            }

            if ( status == 0 )
            {
                status = ad9361_config_ext_ref_clock_freq(p_id->chip_id, ref_clk_freq);
            }
        }
        else if ( ( ref_clock == skiq_ref_clock_internal ) && ( (part == skiq_m2_2280) || (part == skiq_z3u) ) )
        {
            /* Set an internal reference clock source for M.2-2280/Z3u */
            status = m2_2280_io_set_int_ref_clock_source( p_id->card );
        }
        else if ( ( ref_clock == skiq_ref_clock_internal ) && ( part == skiq_z2p ) )
        {
            /* if the part is Z2p, we need to setup the FPGAs GPIOs for the reference clock */
            uint32_t ddr_val=0;
            uint32_t val=0;

            // TODO: do we actually need to do this at all??
            // https://confluence.epiq.rocks/display/EN/Sidekiq+Z2P+RF+FE+control+information
            // REF_INT_MEMS = REF_OSC_PWR_EN (active low)
            // REF_EXT_40M = WFL_OSC_EN (active high)
            sidekiq_fpga_reg_read( p_id->card, FPGA_REG_GPIO_TRISTATE, &ddr_val );
            sidekiq_fpga_reg_read( p_id->card, FPGA_REG_GPIO_WRITE, &val );
            RBF_SET( ddr_val, 1, REF_INT_MEMS );
            RBF_SET( ddr_val, 1, REF_EXT_40M );
            //  internal always
            // REF_OSC_PWR_EN on, WFL_OSC_ENA off
            RBF_SET( val, 0, REF_INT_MEMS );
            RBF_SET( val, 0, REF_EXT_40M );
            status = sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_GPIO_TRISTATE, ddr_val );
            if( status == 0 )
            {
                sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_GPIO_WRITE, val );
            }
        }
    }

    return status;
}


int32_t _reset_and_init( rf_id_t *p_id )
{
    int32_t status = 0;
    bool configure_gpio = false;
    uint8_t num_gpio = 0;
    bool gpio_state[AD9361_MAX_NUM_GPIO_INIT];
    uint8_t gpio_pos[AD9361_MAX_NUM_GPIO_INIT];
    uint8_t rx_delay = 0;
    uint8_t tx_delay = 0;
    uint8_t val=0;
    skiq_part_t part;
    bool use_cmos = false;
    uint8_t i=0;
    uint32_t data = 0;

    // register logging...we're assuming that the same log levels are mapped but that is a safe
    // assumption in this case (there may be a better way to support this in the future)
    ad9361_register_logging( p_id->chip_id, SKIQ_LOG_INFO, _skiq_log_str );
    
    // initialize the channel variables to disabled, this should get configured in an upper layer
    _rx_enable[p_id->card].chan1_enable = false;
    _rx_enable[p_id->card].chan2_enable = false;
    _tx_enable[p_id->card].chan1_enable = false;
    _tx_enable[p_id->card].chan2_enable = false;

    for( i=0; i<skiq_tx_hdl_end; i++ )
    {
        _tx_quadcal[p_id->card][i] = skiq_tx_quadcal_mode_auto;
    }

    // toggle the RFIC reset line
    status = sidekiq_fpga_reg_read( p_id->card, FPGA_REG_TIMESTAMP_RST, &data );
    if( status == 0 )
    {
        RBF_SET( data, 1, RFIC_RST );
        if( sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_TIMESTAMP_RST, data ) != 0 )
        {
            _skiq_log( SKIQ_LOG_ERROR, "Unable to reset RF IC\n" );
            return (-EFAULT);
        }
        hal_nanosleep( 1*MILLISEC );
        RBF_SET( data, 0, RFIC_RST );
        if( sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_TIMESTAMP_RST, data ) != 0 )
        {
            _skiq_log( SKIQ_LOG_ERROR, "Unable to reset RF IC\n" );
            hal_critical_exit( -EFAULT );
        }
    }
    else
    {
        _skiq_log( SKIQ_LOG_ERROR, "Unable to read FPGA register\n");
        return (-EFAULT);
    }

    // TODO: do this differently...
    part = _skiq_get_part( p_id->card );
    if( (part == skiq_z2) || (part == skiq_z2p) || (part == skiq_z3u) )
    {
        use_cmos = true;
    }

    _get_rfic_config_by_part( p_id->card,
                              &configure_gpio,
                              gpio_pos,
                              gpio_state,
                              &num_gpio,
                              &rx_delay,
                              &tx_delay );

    if ( status == 0 )
    {
        status = _configure_ref_clock( p_id );
    }

    if ( status == 0 )
    {
        status = ad9361_full_init( p_id->chip_id,
                                   use_cmos,
                                   configure_gpio,
                                   gpio_pos, 
                                   gpio_state,
                                   num_gpio);
    }
    if ( status == 0 )
    {
        /* power down the TX Synth and LO.  User may call
         * skiq_write_tx_LO_freq() to bring them back up. */
        status = ad9361_power_down_tx( p_id->chip_id );
    }

    if( status == 0 )
    {
        /* make sure the ad9361 clock out is enabled for the xtaln */
        status=ad9361_config_clkout(p_id->chip_id, ad9361_clkout_enabled, 0);
    }
    
    if( status == 0 )
    {
        /* explicitly default to normal/non-inverted spectrum */
        status = ad9361_write_rx_spectrum_invert(p_id->chip_id, false);
    }
    
    if( status == 0 )
    {
        /* explicitly default to normal/non-inverted spectrum */
        status = ad9361_write_tx_spectrum_invert(p_id->chip_id, false);
    }
    if( status == 0 )
    {
        /* explicitly disable auto cal */
        status = ad9361_write_rx_cal_mode( p_id->chip_id, false );
    }
    
    // set the delay registers
    hal_rfic_write_reg(p_id->chip_id, 0, AD9361_RX_DELAY_ADDR, rx_delay);
    hal_rfic_write_reg(p_id->chip_id, 0, AD9361_TX_DELAY_ADDR, tx_delay);

    // setup the DAC to use AUX DAC 1 with manual control, step=1
    hal_ad9361_read_reg(p_id->chip_id, 0x026, &val);
    val = val | 0x80;
    hal_ad9361_write_reg(p_id->chip_id, 0x026, val);
    hal_ad9361_write_reg(p_id->chip_id, 0x023, 0xBF);
    hal_ad9361_write_reg(p_id->chip_id, 0x01a, 0x04);

    // configure the default warp
    if ( (part == skiq_m2_2280) || (part == skiq_z2p) || (part == skiq_z3u) )
    {
        _write_warp_voltage( p_id, DEFAULT_WARP_VOLTAGE_M2_2280 );
    }
    else
    {
        _write_warp_voltage( p_id, DEFAULT_WARP_VOLTAGE_AUX_DAC );
    }

    // for Z3u, we need the channel mapping swapped
    if( part == skiq_z3u )
    {
        status = ad9361_write_rx_swap_channel( p_id->card, true );
    }

    if( status == 0 )
    {
        rfic_active[p_id->card] = true;
    }

    return (status);
}

void _display_version_info( void )
{
    _skiq_log(SKIQ_LOG_INFO,
              "RF IC version %u.%u.%u\n",
              AD9361_VERSION_MAJ,
              AD9361_VERSION_MIN,
              AD9361_VERSION_PATCH);
}

int32_t _release( rf_id_t *p_id )
{
    int32_t status=0;

    /* Disable RX and TX channels prior to entering sleep mode */
    status = _enable_rx_chan_all(p_id, false);

    if ( status == 0 )
    {
        status = _enable_tx_chan_all(p_id, false);
    }

    if ( status == 0)
    {
        /* Disable RX calibration mode upon RFIC release since it will be entering sleep mode shortly */
        status = ad9361_write_rx_cal_mode( p_id->chip_id, false );
    }

    if ( status == 0 )
    {
        status = ad9361_enter_sleep_mode( p_id->chip_id );
    }

    rfic_active[p_id->card] = false;

    return (status);
}

int32_t _write_rx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq )
{
    int32_t status=0;

    if( (status=ad9361_write_rx_synth_freq(p_id->chip_id,
                                           freq)) == 0 )
    {
        *p_act_freq = (double)(freq);
    }

    return (status);
}

int32_t _write_tx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq )
{
    int32_t status=0;
    // enable auto tx quadcal by default
    bool tx1_quadcal = true;
    bool tx2_quadcal = true;
    skiq_tx_hdl_t other_hdl = skiq_tx_hdl_end;
    rfic_t other_rfic_instance;

    if( (status=ad9361_power_up_tx( p_id->chip_id, freq )) != 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR, "failed to power up Tx Synth and LO\n");
    }
    else
    {
        // we need to get the "other" hdl / port combo and set the TX quadcal flag appropriately
        other_hdl = _tx_hdl_map_other( p_id );
        if ( other_hdl != skiq_tx_hdl_end )
        {
            other_rfic_instance = _skiq_get_tx_rfic_instance( p_id->card, other_hdl );

            // Note that this code is a bit weird becuase port != hdl in every case...we
            // configure the TX quadcal setting based on handle but the 9361 code is
            // expecting to be configured by port, so we need to make sure that is is
            // mapped properly here.  We're relying on defaulting to auto, so
            // we only need to check for the manual case
            if( _tx_quadcal[p_id->card][p_id->hdl] == skiq_tx_quadcal_mode_manual )
            {
                if( p_id->port == 1  )
                {
                    tx1_quadcal = false;
                }
                else if( p_id->port == 2 )
                {
                    tx2_quadcal = false;
                }
                else
                {
                    skiq_error("Invalid port (%u) specified when trying to configure TX LO on "
                               "card %u\n", p_id->port, p_id->card);
                    status = -ENODEV;
                }
            }

            if( _tx_quadcal[p_id->card][other_hdl] == skiq_tx_quadcal_mode_manual )
            {
                if( other_rfic_instance.p_id->port == 1  )
                {
                    tx1_quadcal = false;
                }
                else if( other_rfic_instance.p_id->port == 2 )
                {
                    tx2_quadcal = false;
                }
            }

            if( status == 0 )
            {
                if( (status=ad9361_write_tx_synth_freq(p_id->chip_id,
                                                       freq, tx1_quadcal, tx2_quadcal)) == 0 )
                {
                    *p_act_freq = (double)(freq);
                }
            }
        }
    }

    return (status);
}

int32_t _write_rx_spectrum_invert( rf_id_t *p_id, bool invert )
{
    int32_t status=0;

    status = ad9361_write_rx_spectrum_invert( p_id->chip_id, invert );

    return (status);
}

int32_t _write_tx_spectrum_invert( rf_id_t *p_id, bool invert )
{
    int32_t status=0;

    status = ad9361_write_tx_spectrum_invert( p_id->chip_id, invert );

    return (status);
}


int32_t _write_tx_attenuation( rf_id_t *p_id, uint16_t atten )
{
    int32_t status=0;

    status = ad9361_write_tx_attenuation( p_id->chip_id,
                                          p_id->port,
                                          atten );
        
    return (status);
}

int32_t _read_tx_attenuation( rf_id_t *p_id, uint16_t *p_atten )
{
    int32_t status=0;

    status = ad9361_read_tx_attenuation( p_id->chip_id,
                                         p_id->port,
                                         p_atten );
        
    return (status);
}

int32_t _read_tx_attenuation_range( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min )
{
    int32_t status = 0;

    // TODO: handle verification?
    *p_max = MAX_TX_QUARTER_DB_ATTEN;
    *p_min = MIN_TX_QUARTER_DB_ATTEN;

    return status;
}

int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode )
{
    int32_t status=0;
    ad9361_rx_gain_mode_t rfic_gain_mode;

    switch( gain_mode )
    {
        case skiq_rx_gain_manual:
            rfic_gain_mode = ad9361_gain_mode_mgc_full_gain_table;
            break;

        case skiq_rx_gain_auto:
            rfic_gain_mode = ad9361_gain_mode_fast_agc;
            break;

        default:
            status=-1;
            _skiq_log(SKIQ_LOG_ERROR, "Invalid gain mode specified\n");
            break;
    }
    if( status==0 )
    {
        status = ad9361_write_rx_gain_mode( p_id->chip_id,
                                            p_id->port,
                                            rfic_gain_mode );
    }

    return (status);
}

int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode )
{
    int32_t status=0;
    ad9361_rx_gain_mode_t gain_mode;

    if( (status=ad9361_read_rx_gain_mode(p_id->chip_id,
                                         p_id->port,
                                         &(gain_mode))) == 0 )
    {
        switch( gain_mode )
        {
            case ad9361_gain_mode_mgc_full_gain_table:
                *p_gain_mode = skiq_rx_gain_manual;
                break;
                
            case ad9361_gain_mode_fast_agc:
                *p_gain_mode = skiq_rx_gain_auto;
                break;

            default:
                _skiq_log(SKIQ_LOG_ERROR, "invalid Rx gain mode %d returned\n", gain_mode);
                status=-1;
        }
    }

    return (status);
}

int32_t _read_rx_gain_range( rf_id_t *p_id,
                             uint8_t *p_gain_max,
                             uint8_t *p_gain_min )
{
    int32_t status = 0;
    
    if( (p_id->hdl == skiq_rx_hdl_A1) || (p_id->hdl == skiq_rx_hdl_A2) )
    {
        *p_gain_max = AD9361_MAX_RX_GAIN;
        *p_gain_min = AD9361_MIN_RX_GAIN;
    }
    else
    {
        status = -EINVAL;
    }
    
    return (status);
}

int32_t _write_rx_gain( rf_id_t *p_id, uint8_t gain )
{
    int32_t status=0;

    status = ad9361_write_rx_gain( p_id->chip_id,
                                   p_id->port,
                                   gain );

    return (status);
}

int32_t _read_rx_gain( rf_id_t *p_id, uint8_t *p_gain )
{
    int32_t status=0;

    status = ad9361_read_rx_gain( p_id->chip_id,
                                  p_id->port,
                                  p_gain );

    return (status);
}

int32_t _read_adc_resolution( uint8_t *p_adc_res )
{
    int32_t status=0;

    *p_adc_res = AD9361_ADC_RES;

    return (status);
}

int32_t _read_dac_resolution( uint8_t *p_dac_res )
{
    int32_t status=0;

    *p_dac_res = AD9361_DAC_RES;

    return (status);
}


int32_t _read_warp_voltage_aux_dac( rf_id_t *p_id,
                                    uint16_t *p_warp_voltage )
{
    int32_t status;
    uint16_t warp_voltage;

    status = ad9361_read_aux_dac1(p_id->chip_id, &warp_voltage);
    if ( status == 0 )
    {
        *p_warp_voltage = warp_voltage;
    }

    return status;
}


int32_t _read_warp_voltage( rf_id_t *p_id, uint16_t *p_warp_voltage )
{
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( p_id->card );

    if ( !rfic_active[p_id->card] )
    {
        status = -ENODEV;
    }

    if ( status == 0 )
    {
        if ( (part == skiq_m2_2280) || (part == skiq_z3u) )
        {
            skiq_ref_clock_select_t ref_clock;

            /* only write the warp voltage if the reference clock is set as internal because on the
            * M.2-2280/Z3u, the DAC is powered off when not using the internal reference */
            status = skiq_read_ref_clock_select( p_id->card, &ref_clock );
            if ( ( status == 0 ) && ( ref_clock == skiq_ref_clock_internal ) )
            {
                uint32_t warp_voltage_extended;

                /* @attention The user and factory warp voltage values that are stored in EEPROM
                * represent the upper 16 bits of the 26-bit resolution of the SiT5356. If the
                * caller wishes to have access to the full 26-bit resolution, use
                * _read_warp_voltage_extended_range() instead. */
                status = _read_warp_voltage_extended_range( p_id, &warp_voltage_extended );
                if ( status == 0 )
                {
                    warp_voltage_extended = (warp_voltage_extended >> 10) & 0xFFFF;
                    *p_warp_voltage = (uint16_t)warp_voltage_extended;
                }
            }
            else
            {
                status = -ENODEV;
            }
        }
        else if( part == skiq_z2p )
        {
            *p_warp_voltage = _warp_voltage[p_id->card];
        }
        else
        {
            status = _read_warp_voltage_aux_dac( p_id, p_warp_voltage );
        }
    }

    return (status);
}

int32_t _write_warp_voltage( rf_id_t *p_id, uint16_t warp_voltage )
{
    int32_t status = 0;
    skiq_part_t part = _skiq_get_part( p_id->card );

    if (  part == skiq_m2_2280 || part == skiq_z3u )
    {
        skiq_ref_clock_select_t ref_clock;

        /* only write the warp voltage if the reference clock is set as internal because on the
         * M.2-2280, the DAC is powered off when not using the internal reference */
        status = skiq_read_ref_clock_select( p_id->card, &ref_clock );
        if ( ( status == 0 ) && ( ref_clock == skiq_ref_clock_internal ) )
        {
            uint32_t warp_voltage_extended;

            /* @attention The user and factory warp voltage values that are stored in EEPROM
             * represent the upper 16 bits of the 26-bit resolution of the SiT5356. If the caller
             * wishes to have access to the full 26-bit resolution, use
             * _write_warp_voltage_extended_range() instead. */
            warp_voltage_extended = (uint32_t)warp_voltage << 10;

            status = _write_warp_voltage_extended_range( p_id, warp_voltage_extended );
        }
        else
        {
            status = -ENODEV;
        }
    }
    else if( part == skiq_z2p )
    {
        status = _write_ad5693_dac( p_id->card, warp_voltage );
    }
    else
    {
        // see reg interface and dac_calc.py
        // step=2, vref=1.5
        // 0.97 * vref + (0.000738+(9e-6)*(vref*1.6-2))*dac*step-0.3752*step+0.05
        status = ad9361_write_aux_dac1(p_id->chip_id, warp_voltage);
    }

    return (status);
}


int32_t _read_warp_voltage_extended_range( rf_id_t *p_id,
                                           uint32_t *p_warp_voltage )
{
    int32_t status = 0;
    bool has_gpsdo = false, is_gpsdo_running = false;
    int32_t gpsdo_status=0;
    
    /*
      Check to see if the card has a GPSDO module in the FPGA.  If it does, check to see if the
      algorithm is running.
     */
    status = gpsdo_fpga_has_module( p_id->card, &has_gpsdo, &gpsdo_status );
    if ( status == 0 )
    {
        /* entry assumption: has_gpsdo is valid */

        if ( has_gpsdo )
        {
            status = gpsdo_fpga_is_enabled( p_id->card, &is_gpsdo_running );
        }
    }

    /*
      Here's where the "read extended warp voltage" functionality is dispatched to either the GPSDO
      interface, the SiT3536, or indicated as not implemented (-ENOSYS).
     */
    if ( status == 0 )
    {
        /* entry assumption: is_gpsdo_running is valid because:
             1. status is 0 and has_gpsdo is true
             2. is_gpsdo_running retained its default value (false)
        */

        skiq_part_t part = _skiq_get_part( p_id->card );

        if ( is_gpsdo_running )
        {
            status = gpsdo_fpga_read_warp_voltage( p_id->card, p_warp_voltage );
        }
        else if ( ( part == skiq_m2_2280 ) || ( part == skiq_z3u ) )
        {
            status = sit5356_dctcxo_read_warp_voltage( p_id->card, p_warp_voltage );
        }
        else
        {
            /* no function implemented for reading extended warp voltage */
            status = -ENOSYS;
        }
    }

    return status;
}


int32_t _write_warp_voltage_extended_range( rf_id_t *p_id,
                                            uint32_t warp_voltage )
{
    int32_t status = -ENOSYS;
    bool has_gpsdo = false, is_gpsdo_running = false;
    int32_t gpsdo_status=0;
    
    /*
      Check to see if the card has a GPSDO module in the FPGA.  If it does, check to see if the
      algorithm is running.
     */
    status = gpsdo_fpga_has_module( p_id->card, &has_gpsdo, &gpsdo_status );
    if ( status == 0 )
    {
        /* entry assumption: has_gpsdo is valid */

        if ( has_gpsdo )
        {
            status = gpsdo_fpga_is_enabled( p_id->card, &is_gpsdo_running );
        }
    }

    /*
      Here's where the "write extended warp voltage" functionality is dispatched to either indicate
      resource is busy (-EBUSY), the SiT3536, or indicate as not implemented (-ENOSYS).
     */
    if ( status == 0 )
    {
        skiq_part_t part = _skiq_get_part( p_id->card );

        /* entry assumption: is_gpsdo_running is valid because:
             1. status is 0 and has_gpsdo is true
             2. is_gpsdo_running retained its default value (false)
        */

        if ( is_gpsdo_running )
        {
            status = -EBUSY;
        }
        else if ( ( part == skiq_m2_2280 ) || ( part == skiq_z3u ) )
        {
            status = sit5356_dctcxo_write_warp_voltage( p_id->card, warp_voltage );
        }
        else
        {
            /* no function implemented for writing extended warp voltage */
            status = -ENOSYS;
        }
    }

    return status;
}


int32_t _enable_rx_port( uint8_t card,
                         uint8_t chip_id,
                         uint8_t port,
                         bool enable )
{
    int32_t status = 0;
    bool rx1_enable = _rx_enable[card].chan1_enable;
    bool rx2_enable = _rx_enable[card].chan2_enable;

    if ( port == 1 )
    {
        rx1_enable = enable;
    }
    else if ( port == 2 )
    {
        rx2_enable = enable;
    }
    else
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        status = ad9361_write_rx_enable_state( chip_id, rx1_enable, rx2_enable );
    }

    // update our local enable variables
    if ( status == 0 )
    {
        _rx_enable[card].chan1_enable = rx1_enable;
        _rx_enable[card].chan2_enable = rx2_enable;
    }

    return status;
}


int32_t _enable_rx_chan( rf_id_t *p_id, bool enable )
{
    return _enable_rx_port( p_id->card, p_id->chip_id, p_id->port, enable );
}

int32_t _enable_rx_chan_all( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;
    uint8_t port;

    port = 1;
    status = _enable_rx_port( p_id->card, p_id->chip_id, port, enable);
    if (status != 0)
    {
        skiq_error("Unable to update RX channel (port %" PRIu8 ") on Sidekiq, failed to update "
                   "handle on card %" PRIu8 " with status %" PRIi32 "\n", port, p_id->card, status);
    }

    if ( status == 0 )
    {
        port = 2;
        status = _enable_rx_port( p_id->card, p_id->chip_id, port, enable);
        if (status != 0)
        {
            skiq_error("Unable to update RX channel (port %" PRIu8 ") on Sidekiq, failed to update "
                       "handle on card %" PRIu8 " with status %" PRIi32 "\n", port, p_id->card,
                       status);
        }
    }

    return status;
}


int32_t _enable_tx_port( uint8_t card,
                         uint8_t chip_id,
                         uint8_t port,
                         bool enable )
{
    int32_t status = 0;
    bool tx1_enable = _tx_enable[card].chan1_enable;
    bool tx2_enable = _tx_enable[card].chan2_enable;

    if ( port == 1 )
    {
        tx1_enable = enable;
    }
    else if ( port == 2 )
    {
        tx2_enable = enable;
    }
    else
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        status = ad9361_write_tx_enable_state( chip_id, tx1_enable, tx2_enable );
    }

    // update our local enable variables
    if ( status == 0 )
    {
        _tx_enable[card].chan1_enable = tx1_enable;
        _tx_enable[card].chan2_enable = tx2_enable;
    }

    return status;
}

int32_t _enable_tx_chan( rf_id_t *p_id, bool enable )
{
    return _enable_tx_port( p_id->card, p_id->chip_id, p_id->port, enable );
}

int32_t _enable_tx_chan_all( rf_id_t *p_id, bool enable )
{
    int32_t status = 0;
    uint8_t port;

    port = 1;
    status = _enable_tx_port( p_id->card, p_id->chip_id, port, enable);
    if (status != 0)
    {
        skiq_error("Unable to update TX channel (port %" PRIu8 ") on Sidekiq, failed to update "
                   "handle on card %" PRIu8 " with status %" PRIi32 "\n", port, p_id->card, status);
    }

    if ( status == 0 )
    {
        port = 2;
        status = _enable_tx_port( p_id->card, p_id->chip_id, port, enable);
        if (status != 0)
        {
            skiq_error("Unable to update TX channel (port %" PRIu8 ") on Sidekiq, failed to update "
                       "handle on card %" PRIu8 " with status %" PRIi32 "\n", port, p_id->card,
                       status);
        }
    }

    return status;
}

int32_t _config_tx_test_tone( rf_id_t *p_id, bool enable )
{
    int32_t status=0;

    if( enable == true )
    {
        status = ad9361_enable_tx_tone( p_id->card );
    }
    else
    {
        status = ad9361_disable_tx_tone( p_id->card );
    }

    return (status);
}

int32_t _read_tx_test_tone_freq( rf_id_t *p_id,
                                 int32_t *p_test_freq_offset )
{
    int32_t status = 0;
    bool invert = false;

    /* The BIST TX test tone is not affected by the spectral inversion bit in the AD9361, so it will
     * appear at a negative offset whenever mixing is performed (with Dropkiq for example).  Check
     * for mixing by checking the TX spectral inversion setting. */
    status = ad9361_read_tx_spectrum_invert( p_id->chip_id, &invert );
    if ( status == 0 )
    {
        *p_test_freq_offset = (_tx_sample_rate[p_id->card][skiq_tx_hdl_A1].sample_rate) / TX_TEST_TONE_FREQ_OFFSET;
        if ( invert )
        {
            *p_test_freq_offset = -(*p_test_freq_offset);
        }
    }

    return (status);
}

void _read_min_sample_rate( rf_id_t *p_id,
                            uint32_t *p_min_sample_rate )
{
    *p_min_sample_rate = AD9361_MIN_RX_SAMPLE_RATE;
}

void _read_max_sample_rate( rf_id_t *p_id,
                            uint32_t *p_max_sample_rate )
{
    if ( ( _skiq_get_part( p_id->card ) == skiq_mpcie ) &&
         ( skiq_rx_hdl_A2 == p_id->hdl ) )
    {
        *p_max_sample_rate = SKIQ_MAX_DUAL_CHAN_MPCIE_SAMPLE_RATE;
    }
    else
    {
        *p_max_sample_rate = AD9361_MAX_RX_SAMPLE_RATE;
    }
}

int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status=0;
    double rx_percent = 0;
    double tx_percent = 0;

    if( (rate<AD9361_MIN_RX_SAMPLE_RATE) || (rate>AD9361_MAX_RX_SAMPLE_RATE) )
    {
        status = -EINVAL;
    }
    else
    {
        rx_percent = (((double)(bandwidth) / (double)(rate))*100);
        tx_percent = _tx_sample_rate[p_id->card][skiq_tx_hdl_A1].bw_percent;

        status = _write_sample_rate( p_id, rate, tx_percent, rx_percent );
        if( status == 0 )
        {
            _rx_sample_rate[p_id->card][skiq_rx_hdl_A1].bandwidth = bandwidth;
            _rx_sample_rate[p_id->card][skiq_rx_hdl_A2].bandwidth = bandwidth;
        }
    }

    return (status);
}

int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status=0;
    double rx_percent = 0;
    double tx_percent = 0;

    if( (rate<AD9361_MIN_RX_SAMPLE_RATE) || (rate>AD9361_MAX_RX_SAMPLE_RATE) )
    {
        status = -EINVAL;
    }
    else
    {
        tx_percent = (((double)(bandwidth) / (double)(rate))*100);
        rx_percent = _rx_sample_rate[p_id->card][skiq_rx_hdl_A1].bw_percent;
        
        status = _write_sample_rate( p_id, rate, tx_percent, rx_percent );
        if( status == 0 )
        {
            _tx_sample_rate[p_id->card][skiq_tx_hdl_A1].bandwidth = bandwidth;
            _tx_sample_rate[p_id->card][skiq_tx_hdl_A2].bandwidth = bandwidth;
        }
    }

    return (status);
}

int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;

    *p_rate = _rx_sample_rate[p_id->card][p_id->hdl].sample_rate;
    *p_actual_rate = (double)(_rx_sample_rate[p_id->card][p_id->hdl].sample_rate);

    return (status);    
}

int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;

    *p_rate = _tx_sample_rate[p_id->card][p_id->hdl].sample_rate;
    *p_actual_rate = (double)(_tx_sample_rate[p_id->card][p_id->hdl].sample_rate);

    return (status);
}

int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;

    *p_bandwidth = _rx_sample_rate[p_id->card][p_id->hdl].bandwidth;
    *p_actual_bandwidth =
        (uint32_t)((_rx_sample_rate[p_id->card][p_id->hdl].bw_percent / 100.0) *
                   (double)((_rx_sample_rate[p_id->card][p_id->hdl].sample_rate)));

    return (status);
}

int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;

    *p_bandwidth = _tx_sample_rate[p_id->card][p_id->hdl].bandwidth;
    *p_actual_bandwidth =
        (uint32_t)((_tx_sample_rate[p_id->card][p_id->hdl].bw_percent / 100.0) *
                   (double)((_tx_sample_rate[p_id->card][p_id->hdl].sample_rate)));

    return (status);
}

int32_t _write_sample_rate( rf_id_t *p_id,
                            uint32_t rate,
                            double tx_percent,
                            double rx_percent)
{
    int32_t status=0;
    double actual_rx_percent = 0;
    double actual_tx_percent = 0;
    
    status = ad9361_write_sample_rate(p_id->chip_id, rate,
                                      rx_percent, tx_percent,
                                      &actual_rx_percent, &actual_tx_percent);

    // Let's assume that we only have A1/A2 for both RX and TX...so update it all
    if( status == 0 )
    {
        // RX sample rate
        _rx_sample_rate[p_id->card][skiq_rx_hdl_A1].sample_rate = rate;
        _rx_sample_rate[p_id->card][skiq_rx_hdl_A2].sample_rate = rate;
        // RX bw percent
        _rx_sample_rate[p_id->card][skiq_rx_hdl_A1].bw_percent = actual_rx_percent;
        _rx_sample_rate[p_id->card][skiq_rx_hdl_A2].bw_percent = actual_rx_percent;
        // TX sample rate
        _tx_sample_rate[p_id->card][skiq_tx_hdl_A1].sample_rate = rate;
        _tx_sample_rate[p_id->card][skiq_tx_hdl_A2].sample_rate = rate;
        // TX bw percent
        _tx_sample_rate[p_id->card][skiq_tx_hdl_A1].bw_percent = actual_tx_percent;
        _tx_sample_rate[p_id->card][skiq_tx_hdl_A2].bw_percent = actual_tx_percent;
    }

    return (status);
}

int32_t _read_rx_filter_overflow( rf_id_t *p_id, bool *p_rx_fir )
{
    int32_t status=0;
    bool int3=false;
    bool hb3=false;
    bool hb2=false;
    bool qec=false;
    bool hb1=false;
    bool tx_fir=false;

    status = ad9361_read_filter_overflow( p_id->card, p_id->port,
                                          &int3, &hb3, &hb2, &qec,
                                          &hb1, &tx_fir, p_rx_fir );

    return (status);
}

int32_t _read_tx_filter_overflow( rf_id_t *p_id, bool *p_int3, bool *p_hb3,
                                  bool *p_hb2, bool *p_qec, bool *p_hb1, bool *p_tx_fir )
{
    int32_t status=0;
    bool rx_fir=false;

    status = ad9361_read_filter_overflow( p_id->card, p_id->port,
                                          p_int3, p_hb3, p_hb2, p_qec,
                                          p_hb1, p_tx_fir, &rx_fir );

    return (status);
}

int32_t _read_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _tx_quadcal[p_id->card][p_id->hdl];

    return (status);
}

int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode )
{
    int32_t status=0;

    _tx_quadcal[p_id->card][p_id->hdl] = mode;

    return (status);
}

int32_t _run_tx_quadcal( rf_id_t *p_id )
{
    int32_t status=0;

    status = ad9361_run_tx_quad_cal( p_id->card, p_id->port );

    return (status);
}

bool _is_chan_enable_xport_dependent( rf_id_t *p_id )
{
    return (true);
}

int32_t _read_control_output_rx_gain_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;

    *p_ena = AD9361_CONTROL_OUTPUT_MODE_GAIN_BITS;
    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_mode = AD9361_CONTROL_OUTPUT_MODE_GAIN_CONTROL_RXA1;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_mode = AD9361_CONTROL_OUTPUT_MODE_GAIN_CONTROL_RXA2;
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

int32_t _write_control_output_config( rf_id_t *p_id, uint8_t mode, uint8_t ena )
{
    int32_t status=0;

    status=ad9361_write_control_output_mode(p_id->chip_id, mode);
    if( status==0 )
    {
        status=ad9361_write_control_output_ena(p_id->chip_id, ena);
    }

    return (status);
}

int32_t _read_control_output_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;

    status=ad9361_read_control_output_mode(p_id->chip_id, p_mode);
    if( status == 0 )
    {
        status=ad9361_read_control_output_ena(p_id->chip_id, p_ena);
    }

    return (status);
}

int32_t _init_from_file( rf_id_t *p_id, FILE* p_file )
{
    int32_t status=0;
    uint8_t rx_delay = 0;
    uint8_t tx_delay = 0;
    bool configure_gpio = false;
    uint8_t num_gpio = 0;
    bool gpio_state[AD9361_MAX_NUM_GPIO_INIT];
    uint8_t gpio_pos[AD9361_MAX_NUM_GPIO_INIT];

    _get_rfic_config_by_part( p_id->card,
                              &configure_gpio,
                              gpio_pos,
                              gpio_state,
                              &num_gpio,
                              &rx_delay,
                              &tx_delay );

    status = process_ad9361_file(p_file, p_id->card, configure_gpio, gpio_pos, gpio_state, num_gpio);
    if ( status == 0 )
    {
        /* make sure the ad9361 clock out is enabled for the xtaln */
        status = ad9361_config_clkout(p_id->chip_id, ad9361_clkout_enabled, 0);
    }

    // set the delay registers
    hal_rfic_write_reg(p_id->chip_id, 0, AD9361_RX_DELAY_ADDR, rx_delay);
    hal_rfic_write_reg(p_id->chip_id, 0, AD9361_TX_DELAY_ADDR, tx_delay);

    return (status);
}

void _get_rfic_config_by_part( uint8_t card,
                               bool *p_configure_gpio,
                               uint8_t *p_gpio_pos,
                               bool *p_gpio_state,
                               uint8_t *p_num_gpio,
                               uint8_t *p_rx_delay,
                               uint8_t *p_tx_delay )
{
    skiq_ref_clock_select_t ref_clock;
    bool gpio_osc_config=false;
    bool gpio_ref_clock_host=false;
    bool gpio_ref_clock_ext=false;
    skiq_part_t part;

    // TODO: do this differently...
    skiq_read_ref_clock_select( card, &ref_clock );
    part = _skiq_get_part( card );
    
    *p_num_gpio = 0;
    *p_configure_gpio = false;

    if( part == skiq_m2 )
    {
        *p_configure_gpio = true;
        p_gpio_state[*p_num_gpio] = true;
        gpio_osc_config = true;

        p_gpio_pos[*p_num_gpio] = AD9361_GPIO_RF_PWR_ENA_POS;
        *p_num_gpio = *p_num_gpio+1;

        *p_rx_delay = AD9361_M2_RX_DELAY_VAL;
        *p_tx_delay = AD9361_M2_TX_DELAY_VAL;
    }
    else if( part == skiq_z2 )
    {
        *p_configure_gpio = true;
        gpio_osc_config = true;
        gpio_ref_clock_host = true;
        gpio_ref_clock_ext = true;
        
        *p_rx_delay = AD9361_Z2_RX_DELAY_VAL;
        *p_tx_delay = AD9361_Z2_TX_DELAY_VAL;
    }
    else if( part == skiq_mpcie )
    {
        *p_rx_delay = AD9361_MPCIE_RX_DELAY_VAL;
        *p_tx_delay = AD9361_MPCIE_TX_DELAY_VAL;
    }
    else if ( part == skiq_m2_2280 )
    {
        *p_rx_delay = AD9361_M2_2280_RX_DELAY_VAL;
        *p_tx_delay = AD9361_M2_2280_TX_DELAY_VAL;
    }
    else if( part == skiq_z2p )
    {
        *p_rx_delay = AD9361_Z2P_RX_DELAY_VAL;
        *p_tx_delay = AD9361_Z2P_TX_DELAY_VAL;
    }
    else if( part == skiq_z3u )
    {
        *p_rx_delay = AD9361_Z3U_RX_DELAY_VAL;
        *p_tx_delay = AD9361_Z3U_TX_DELAY_VAL;
    }

    if( gpio_osc_config == true )
    {
        p_gpio_pos[*p_num_gpio] = AD9361_GPIO_OSC_PWR_ENA_POS;
        /* enable_osc_pwr if ref clock is external */
        if ( ref_clock == skiq_ref_clock_internal )
        {
            // make sure the osc is on
            p_gpio_state[*p_num_gpio] = true;
        }
        else
        {
            // make sure the osc is off
            p_gpio_state[*p_num_gpio] = false;

        }
        *p_num_gpio = *p_num_gpio+1;
    }

    if( gpio_ref_clock_host == true )
    {
        p_gpio_pos[*p_num_gpio] = AD9361_GPIO_REF_CLOCK_HOST;
        if( ref_clock == skiq_ref_clock_host )
        {
            p_gpio_state[*p_num_gpio] = true;
        }
        else
        {
            p_gpio_state[*p_num_gpio] = false;
        }
        *p_num_gpio = *p_num_gpio+1;
    }

    if( gpio_ref_clock_ext == true )
    {
        p_gpio_pos[*p_num_gpio] = AD9361_GPIO_REF_CLOCK_EXT;
        if( ref_clock == skiq_ref_clock_external )
        {
            p_gpio_state[*p_num_gpio] = true;
        }
        else
        {
            p_gpio_state[*p_num_gpio] = false;
        }
        *p_num_gpio = *p_num_gpio+1;
    }

}

int32_t _power_down_tx( rf_id_t *p_id )
{
    int32_t status=0;

    /* power down the TX Synth and LO.  User may call
     * skiq_write_tx_LO_freq() to bring them back up. */
    status = ad9361_power_down_tx( p_id->chip_id );

    return (status);
}

uint32_t _read_hop_on_ts_gpio( rf_id_t *p_id, uint8_t *p_chip_index )
{
    uint32_t gpio=0;

    // we have 4 RFIC CTRL IN bits, located at RFIC_CTRL_IN
    BF_SET( gpio, 0xF, RFIC_CTRL_IN_OFFSET, RFIC_CTRL_IN_LEN );
    *p_chip_index = 0;

    return (gpio);
}

int32_t _write_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode )
{
    int32_t status=0;

    status = _write_freq_tune_mode( p_id->card,
                                    mode,
                                    &(_rx_fastlock_config[p_id->card]),
                                    ad9361_disable_rx_fastlock );

    return (status);
}

int32_t _write_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode )
{
    int32_t status=0;

    status = _write_freq_tune_mode( p_id->card,
                                    mode,
                                    &(_tx_fastlock_config[p_id->card]),
                                    ad9361_disable_tx_fastlock );
    return (status);
}

int32_t _write_freq_tune_mode( uint8_t id,
                               skiq_freq_tune_mode_t mode,
                               fastlock_config_t *p_fastlock_config,
                               int32_t (*p_disable_fastlock)( uint32_t id ) )
{
    int32_t status=0;

    if( mode == skiq_freq_tune_mode_standard )
    {
        // if the profiles aren't empty, be sure to free them
        FREE_IF_NOT_NULL( p_fastlock_config->p_profiles );
        p_fastlock_config->num_profiles = 0;

        status = p_disable_fastlock( id );
    }

    if( status == 0 )
    {
        p_fastlock_config->tune_mode = mode;
    }    

    return (status);
}

int32_t _read_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _rx_fastlock_config[p_id->card].tune_mode;

    return (status);
}

int32_t _read_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _tx_fastlock_config[p_id->card].tune_mode;

    return (status);
}

#define display_freq_hop(s,fh)                                  \
    do {                                                        \
        debug_print("%s.index: %u\n", s, fh.index);             \
        debug_print("%s.freq:  %" PRIu64 "\n", s, fh.freq);     \
    } while (0)

#define display_fastlock_config( fc )                                  \
    do {                                                               \
        debug_print("curr_ad9361_index: %u\n", fc->curr_ad9361_index); \
        debug_print("next_ad9361_index: %u\n", fc->next_ad9361_index); \
        debug_print("next_ad9361_slot:  %u\n", fc->next_ad9361_slot);  \
        display_freq_hop("mailbox", fc->mailbox);                     \
        display_freq_hop("curr_hop", fc->curr_hop);                    \
        display_freq_hop("next_hop", fc->next_hop);                    \
        debug_print("num_profiles:      %u\n", fc->num_profiles);      \
        debug_print("p_profiles:        %p\n", fc->p_profiles);        \
    } while (0)

static void display_fastlock_regs( ad9361_fastlock_profile_t *p_profile )
{
    uint8_t j;
    for (j = 0; j < AD9361_NUM_FASTLOCK_REGS; j++)
    {
        debug_print("regs[%u] = %02X\n", j, p_profile->regs[j]);
    }
}


int32_t _config_rx_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index )
{
    int32_t status=0;

    status = _config_hop_list( p_id->card,
                               &(_rx_fastlock_config[p_id->card]),
                               num_freq,
                               freq_list,
                               initial_index,
                               ad9361_enable_rx_fastlock,
                               ad9361_disable_rx_fastlock,
                               ad9361_write_rx_fastlock_ctrl,
                               ad9361_gen_rx_fastlock_regs,
                               ad9361_store_rx_fastlock_regs );

    return (status);
}

int32_t _config_tx_hop_list( rf_id_t *p_id, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index )
{
    int32_t status=0;

    // force TX on for now
    ad9361_power_up_tx( p_id->chip_id, freq_list[0] );

    status = _config_hop_list( p_id->card,
                               &(_tx_fastlock_config[p_id->card]),
                               num_freq,
                               freq_list,
                               initial_index,
                               ad9361_enable_tx_fastlock,
                               ad9361_disable_tx_fastlock,
                               ad9361_write_tx_fastlock_ctrl,
                               ad9361_gen_tx_fastlock_regs,
                               ad9361_store_tx_fastlock_regs );

    
    return (status);
}

int32_t _config_hop_list( uint8_t card,
                          fastlock_config_t *p_fastlock_config,
                          uint16_t num_freq,
                          uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS],
                          uint16_t initial_index,
                          int32_t (*p_enable_fastlock)( uint32_t id ),
                          int32_t (*p_disable_fastlock)( uint32_t id ),
                          int32_t (*p_write_fastlock_ctrl)( uint32_t id, ad9361_fastlock_ctrl_t ctrl ),
                          int32_t (*p_gen_fastlock_regs)( uint32_t id, uint64_t lo, uint8_t regs[AD9361_NUM_FASTLOCK_REGS] ),
                          int32_t (*p_store_fastlock_regs)( uint32_t id, uint8_t fastlock_index, uint8_t regs[AD9361_NUM_FASTLOCK_REGS] ) )
{
    int32_t status=0;
    ad9361_fastlock_profile_t *p_profile;
    uint16_t i=0;
    uint8_t slot_to_write = 1;  //Slot 0 is reserved for initial_index, so write the subsequent hops starting at 1.

    // note: this needs to be disabled in order for us to generate the register set properly
    status = p_disable_fastlock( card );
    if (status != 0)
    {
        skiq_error("Unable to disable fastlock before generating the register set with status %d (card=%u)", status, card);
    }
    else
    {
        // Note: validation of num freqs (>0, <MAX_NUM_FREQ) is done at a higher layer

        FREE_IF_NOT_NULL( p_fastlock_config->p_profiles );
        p_fastlock_config->p_profiles = malloc( num_freq * sizeof(ad9361_fastlock_profile_t) );
        if( p_fastlock_config->p_profiles == NULL )
        {
            skiq_error("Unable to allocate memory to store hopping profiles with status %d (card=%u)", status, card);
            status = -ENOMEM;
        }
        else
        {
            /* store the hop list entry at initial_index in AD9361's index 0 */
            debug_print("Storing frequency %" PRIu64 " to AD9361 index 0 (card=%u)\n",
                        freq_list[initial_index], card);

            p_profile = &(p_fastlock_config->p_profiles[initial_index]);
            status = p_gen_fastlock_regs( card, freq_list[initial_index], p_profile->regs );
            if(status == 0)
            {
                p_profile->freq = freq_list[initial_index];
                display_fastlock_regs(p_profile);
                status = p_store_fastlock_regs( card, SLOT_0, p_profile->regs );
                p_profile->ad9361_index = SLOT_0;
            }
            else
            {
                skiq_error("Unable to generate fastlock registers with status %d (card=%u)", status, card);
            }

            /* store AD9361_MAX_FASTLOCK_INDEX-1 more into the RFIC */
            for( i=0; ((i<num_freq) && (status==0)); i++ )
            {
                p_profile = &(p_fastlock_config->p_profiles[i]);
                status = p_gen_fastlock_regs( card, freq_list[i], p_profile->regs );
                if (status == 0)
                {
                    p_profile->freq = freq_list[i];
                    if ( ( i != initial_index ) && ( slot_to_write < AD9361_MAX_FASTLOCK_INDEX ) )
                    {
                        debug_print("Storing frequency %" PRIu64 " to AD9361 index %u (card=%u)\n",
                                    freq_list[i], slot_to_write, card);
                        display_fastlock_regs(p_profile);
                        status = p_store_fastlock_regs( card, slot_to_write, p_profile->regs );
                        if (status !=0)
                        {
                            skiq_error("Unable to store fastlock registers with status %d (card=%u)", status, card);
                        }
                        p_profile->ad9361_index = slot_to_write;
                        slot_to_write++;
                    }
                    else if ( i != initial_index )
                    {
                        /* Set the index value of the profile to indicate it's not
                           currently in an AD9361 slot */
                        p_profile->ad9361_index = AD9361_MAX_FASTLOCK_INDEX;
                    }
                }
                else
                {
                    skiq_error("Unable to generate fastlock registers with status %d (card=%u)", status, card);
                }
            }
        }
    }

    if( status == 0 )
    {
        p_fastlock_config->num_profiles = num_freq;
        status = p_enable_fastlock( card );

        if (status == 0)
        {
            // if immediate, use SPI to set the profile
            if( p_fastlock_config->tune_mode == skiq_freq_tune_mode_hop_immediate )
            {
                status = p_write_fastlock_ctrl( card, ad9361_fastlock_ctrl_spi );
            }
            else
            {
                // NOTE: CTRL0-2 are used to select both RX and TX profiles
                status = p_write_fastlock_ctrl( card, ad9361_fastlock_ctrl_pin );
            }

            if (status !=0)
            {
                skiq_error("Unable to write fastlock registers with status %d (card=%u)", status, card);

            }
            else
            {
                p_fastlock_config->curr_ad9361_index = 0;
                
                // update our hop config
                p_fastlock_config->next_hop.index = initial_index;
                p_fastlock_config->next_hop.freq =
                    p_fastlock_config->p_profiles[initial_index].freq;

                //set current / mailbox to none
                p_fastlock_config->curr_hop.index = SKIQ_MAX_NUM_FREQ_HOPS;
                p_fastlock_config->curr_hop.freq = 0;
                p_fastlock_config->mailbox.index = SKIQ_MAX_NUM_FREQ_HOPS;
                p_fastlock_config->mailbox.freq = 0;

                /* Set the next slot to some index that won't be used next */
                p_fastlock_config->next_ad9361_slot = 7;

                display_fastlock_config( p_fastlock_config );
            }
        }
        else
        {
            skiq_error("Unable to enable fastlock with status %d (card=%u)", status, card);
        }
    }
    else
    {
        p_fastlock_config->num_profiles = 0;
        FREE_IF_NOT_NULL( p_fastlock_config->p_profiles );
    }

    return (status);
}

int32_t _read_rx_hop_list( rf_id_t *p_id, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=0;
    uint16_t i=0;

    *p_num_freq = _rx_fastlock_config[p_id->card].num_profiles; 
    for( i=0; i<(*p_num_freq); i++ )
    {
        freq_list[i] = _rx_fastlock_config[p_id->card].p_profiles[i].freq;
    }
    
    return (status);
}

int32_t _read_tx_hop_list( rf_id_t *p_id, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=0;
    uint16_t i=0;

    *p_num_freq = _tx_fastlock_config[p_id->card].num_profiles; 
    for( i=0; i<(*p_num_freq); i++ )
    {
        freq_list[i] = _tx_fastlock_config[p_id->card].p_profiles[i].freq;
    }
    
    return (status);
}

int32_t _write_next_rx_hop( rf_id_t *p_id, uint16_t freq_index )
{
    int32_t status=0;

    status = _write_next_hop( p_id->card,
                              freq_index,
                              &(_rx_fastlock_config[p_id->card]),
                              ad9361_store_rx_fastlock_regs );
    
    return (status);
}

int32_t _write_next_tx_hop( rf_id_t *p_id, uint16_t freq_index )
{
    int32_t status=0;

    status = _write_next_hop( p_id->card,
                              freq_index,
                              &(_tx_fastlock_config[p_id->card]),
                              ad9361_store_tx_fastlock_regs );
    
    return (status);
}

int32_t _locate_slot_and_mark_for_replacement( uint8_t card, fastlock_config_t *p_fastlock_config)

{
    int32_t status=0;
    bool found = false;
    uint16_t i=0;

    for( i=0; ((i<p_fastlock_config->num_profiles) && (!found) && (status == 0) ); i++ )
    {
        if( p_fastlock_config->p_profiles[i].ad9361_index ==
            p_fastlock_config->next_ad9361_slot )
        {
            /* Check to ensure we're not evicting the soon to be current hop */
            if (p_fastlock_config->p_profiles[i].ad9361_index ==
                p_fastlock_config->p_profiles[p_fastlock_config->next_hop.index].ad9361_index)
            {
                /* If so, pick the next slot, wrapping around */
                p_fastlock_config->next_ad9361_slot++;
                if( p_fastlock_config->next_ad9361_slot >= AD9361_MAX_FASTLOCK_INDEX )
                {
                    p_fastlock_config->next_ad9361_slot = 0;
                }
                found = true;  /*The profile of interest was located, but shouldn't be replaced. (Not fatal)*/
                status = -EXDEV;
                continue;
            }
            debug_print("Overwriting index AD9361 %u, hop index %u (card=%u)\n",
                        p_fastlock_config->next_ad9361_slot, i, card);
            p_fastlock_config->p_profiles[i].ad9361_index = AD9361_MAX_FASTLOCK_INDEX;
            found = true;
        }
    }
    if (!found)
    {
        status = -ENXIO;
    }

    return status;
}


int32_t _write_next_hop( uint8_t card,
                         uint16_t freq_index,
                         fastlock_config_t *p_fastlock_config,
                         int32_t (*p_store_fastlock_regs)( uint32_t id, uint8_t fastlock_index, uint8_t regs[] ) )
{
    int32_t status=0;

    // if this profile isn't stored in the chip, then we need to store it first
    if( p_fastlock_config->p_profiles[freq_index].ad9361_index == AD9361_MAX_FASTLOCK_INDEX )
    {
        // Loop through the profiles and determine the one currently occupying next_ad9361_slot.
        status = _locate_slot_and_mark_for_replacement(card, p_fastlock_config);
        if (status == -EXDEV) 
        {
            /* A return code of EXDEV indicates the next_ad9361_slot cannot be used due to a pending hop.*/            
            debug_print("Can't overwrite next_ad9361_slot since it will become the next hop, incremented next_ad9361_slot to %d (card=%u)\n",
                 p_fastlock_config->next_ad9361_slot, card);
            status = _locate_slot_and_mark_for_replacement(card, p_fastlock_config);
        }

        if( status == 0 )
        {
            p_fastlock_config->p_profiles[freq_index].ad9361_index =
                p_fastlock_config->next_ad9361_slot;

            // increment the next avail index
            p_fastlock_config->next_ad9361_slot++;
            if( p_fastlock_config->next_ad9361_slot >= AD9361_MAX_FASTLOCK_INDEX )
            {
                p_fastlock_config->next_ad9361_slot = 0;
            }

            // program the register set down
            debug_print("Saving freq_index %u to AD9361 index %u (card=%u)\n",
                        freq_index, p_fastlock_config->p_profiles[freq_index].ad9361_index, card );
            status = p_store_fastlock_regs( card,
                                   p_fastlock_config->p_profiles[freq_index].ad9361_index,
                                   p_fastlock_config->p_profiles[freq_index].regs );
        }
    }

    if( status == 0 )
    {
        p_fastlock_config->next_ad9361_index =
            p_fastlock_config->p_profiles[freq_index].ad9361_index;

        // update our hop config
        p_fastlock_config->mailbox.index = freq_index;
        p_fastlock_config->mailbox.freq =
            p_fastlock_config->p_profiles[freq_index].freq;

        display_fastlock_config( p_fastlock_config );
    }

    return (status);
}


int32_t _rx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status=0;

    status = _hop( p_id->card,
                   rf_timestamp,
                   p_act_freq,
                   &_rx_fastlock_config[p_id->card],
                   ad9361_apply_rx_fastlock_profile );
    
    return (status);
}

int32_t _tx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status=0;

    status = _hop( p_id->card,
                   rf_timestamp,
                   p_act_freq,
                   &_tx_fastlock_config[p_id->card],
                   ad9361_apply_tx_fastlock_profile );
    
    return (status);
}

int32_t _hop( uint8_t card,
              uint64_t rf_timestamp,
              double *p_act_freq,
              fastlock_config_t *p_fastlock_config,
              int32_t (*p_config_fastlock_profile)( uint32_t id, uint8_t fastlock_index ) )
{
    int32_t status=0;
    uint8_t ctrl_in;

    // now actually enable the profile
    status = p_config_fastlock_profile( card,
                                        p_fastlock_config->curr_ad9361_index );

    if( status == 0 )
    {
        if( (p_fastlock_config->tune_mode == skiq_freq_tune_mode_hop_on_timestamp) )
        {
            ctrl_in = p_fastlock_config->curr_ad9361_index << 1;
            status = fpga_ctrl_hop( card,
                                    0,
                                    ctrl_in,
                                    p_fastlock_config->next_hop.index, /* freq index */
                                    rf_timestamp );
        }

        if( status == 0 )
        {
            *p_act_freq = (double)(p_fastlock_config->next_hop.freq);
            
            // now move next to current
            p_fastlock_config->curr_ad9361_index = p_fastlock_config->next_ad9361_index;
            // update our current hop config:

            //next moves to current
            memcpy( &(p_fastlock_config->curr_hop),
                    &(p_fastlock_config->next_hop),
                    sizeof(freq_hop_t) );

            //mailbox moves to next
            memcpy( &(p_fastlock_config->next_hop),
                    &(p_fastlock_config->mailbox),
                    sizeof(freq_hop_t) );

            // reset the mailbox
            p_fastlock_config->mailbox.index = SKIQ_MAX_NUM_FREQ_HOPS;
            p_fastlock_config->mailbox.freq = 0;
            display_fastlock_config( p_fastlock_config );
        }
    }
    
    return (status);
}

int32_t _read_curr_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=-ENODEV;

    if( _rx_fastlock_config[p_id->card].curr_hop.index != SKIQ_MAX_NUM_FREQ_HOPS )
    {
        status = 0;
        memcpy( p_hop,
                &(_rx_fastlock_config[p_id->card].curr_hop),
                sizeof(freq_hop_t) );
    }
    
    return (status);
}

int32_t _read_next_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=-ENODEV;

    if( _rx_fastlock_config[p_id->card].next_hop.index != SKIQ_MAX_NUM_FREQ_HOPS )
    {
        status = 0;
        memcpy( p_hop,
                &(_rx_fastlock_config[p_id->card].next_hop),
                sizeof(freq_hop_t) );
    }
    
    return (status);
}

int32_t _read_curr_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=-ENODEV;

    if( _tx_fastlock_config[p_id->card].curr_hop.index != SKIQ_MAX_NUM_FREQ_HOPS )
    {
        status = 0;
        memcpy( p_hop,
                &(_tx_fastlock_config[p_id->card].curr_hop),
                sizeof(freq_hop_t) );
    }
    
    return (status);    
}

int32_t _read_next_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status = -ENODEV;
    
    if( _tx_fastlock_config[p_id->card].next_hop.index != SKIQ_MAX_NUM_FREQ_HOPS )
    {
        status = 0;
        memcpy( p_hop,
                &(_tx_fastlock_config[p_id->card].next_hop),
                sizeof(freq_hop_t) );
    }

    return (status);
}

int32_t _enable_pin_gain_ctrl( rf_id_t *p_id )
{
    int32_t status=0;
    ad9361_rx_gain_mode_t rfic_gain_mode = 
                        ad9361_gain_mode_mgc_full_gain_table_pin_ctrl;

    status = ad9361_write_rx_gain_mode( p_id->chip_id,
                                            p_id->port,
                                            rfic_gain_mode );

    return (status);
}

int32_t _read_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _rx_cal_mode[p_id->card];

    return (status);
}

int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode )
{
    int32_t status=0;
    bool automatic=false;

    if( mode == skiq_rx_cal_mode_auto )
    {
        automatic = true;
        // only update the 9361 if DC offset cal is enabled
        if( (_rx_cal_mask[p_id->card] & skiq_rx_cal_type_dc_offset) != 0 )
        {
            status = ad9361_write_rx_cal_mode( p_id->chip_id, automatic );
        }
    }
    else
    {
        status = ad9361_write_rx_cal_mode( p_id->chip_id, automatic );
    }

    // store the mode configured
    if( status == 0 )
    {
        if( automatic == true )
        {
            _rx_cal_mode[p_id->card] = skiq_rx_cal_mode_auto;
        }
        else
        {
            _rx_cal_mode[p_id->card] = skiq_rx_cal_mode_manual;
        }
    }

    return (status);
}

int32_t _run_rx_cal( rf_id_t *p_id )
{
    int32_t status=0;

    // nothing to run if cal is set to none
    if( _rx_cal_mask[p_id->card] != skiq_rx_cal_type_none )
    {
        status = ad9361_run_rf_dc_offset_cal( p_id->chip_id );
    }

    return (status);
}


int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    *p_cal_mask = _rx_cal_mask[p_id->card];

    return (status);
}

int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask )
{
    int32_t status=0;
    bool automatic_cal = true;

    // only DC offset or none is supported
    if( cal_mask == skiq_rx_cal_type_none )
    {
        automatic_cal = false;
    }
    else
    {
        if( (cal_mask & ~skiq_rx_cal_type_dc_offset) != 0 )
        {
            status = -EINVAL;
        }
        else
        {
            automatic_cal = true;
        }
    }
    
    if( status == 0 )
    {
        if( _rx_cal_mode[p_id->card] == skiq_rx_cal_mode_auto )
        {
            status = ad9361_write_rx_cal_mode( p_id->chip_id, automatic_cal );
        }
    }

    if( status == 0 )
    {
        _rx_cal_mask[p_id->card] = cal_mask;
    }

    return (status);
}

int32_t _read_rx_cal_types_avail( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    *p_cal_mask = skiq_rx_cal_type_dc_offset | skiq_rx_cal_type_none;

    return (status);
}

int32_t _swap_rx_port_config( rf_id_t *p_id,
                              uint8_t a_port,
                              uint8_t b_port )
{
    int32_t status=0;
    bool rx1_enable = false;
    bool rx2_enable = false;
    rf_id_t a_port_id;
    rf_id_t b_port_id;
    skiq_rx_gain_t curr_a_gain_mode = skiq_rx_gain_manual;
    skiq_rx_gain_t curr_b_gain_mode = skiq_rx_gain_manual;
    uint8_t curr_a_gain = 0;
    uint8_t curr_b_gain = 0;

    // swap our enable state...if 1 was enabled, then 2 should be enabled now
    rx1_enable =  _rx_enable[p_id->card].chan2_enable; // swap 1 with 2
    rx2_enable = _rx_enable[p_id->card].chan1_enable; // swap 2 with 1    

    // configure the RF id's so we can save off the gain/ gain mode / enable
    a_port_id.card = p_id->card;
    b_port_id.card = p_id->card;
    a_port_id.chip_id = p_id->chip_id;
    b_port_id.chip_id = p_id->chip_id;
    // for the 9361, we don't care about handle...it's all about port
    a_port_id.port = a_port;
    b_port_id.port = b_port;

    if( _read_rx_gain_mode( &a_port_id, &curr_a_gain_mode ) != 0 )
    {
        skiq_error("Unable to read gain mode for A in swap port config\n");
    }
    if( _read_rx_gain( &a_port_id, &curr_a_gain ) != 0 )
    {
        skiq_error("Unable to read gain for A in swap port config\n");
    }
    if( _read_rx_gain_mode( &b_port_id, &curr_b_gain_mode ) != 0 )
    {
        skiq_error("Unable to read gain mode for B in swap port config\n");
    }
    if( _read_rx_gain( &b_port_id, &curr_b_gain ) != 0 )
    {
        skiq_error("Unable to read gain fro B in swap port config\n");
    }

    // now A's values go to B, B goes to A

    // channel enables
    status = ad9361_write_rx_enable_state( p_id->chip_id, rx1_enable, rx2_enable );
    if( status == 0 )
    {
        // channel enable was stored to the RFIC, now update our cached state to reflect the change
        _rx_enable[p_id->card].chan1_enable = rx1_enable;
        _rx_enable[p_id->card].chan2_enable = rx2_enable;
    }

    // gain mode
    if( _write_rx_gain_mode( &a_port_id, curr_b_gain_mode ) != 0 )
    {
        skiq_error("Unable to update gain mode for A in swap port config");
    }
    if( _write_rx_gain_mode( &b_port_id, curr_a_gain_mode ) != 0 )
    {
        skiq_error("Unable to update gain mode for B in swap port config");
    }
    // gain
    if( _write_rx_gain( &a_port_id, curr_b_gain ) != 0 )
    {
        skiq_error("Unable to update gain for A in swap port config");
    }
    if( _write_rx_gain( &b_port_id, curr_a_gain ) != 0 )
    {
        skiq_error("Unable to update gain for B in swap port config");
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void)p_id;

    p_rf_capabilities->minTxFreqHz = AD9361_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = AD9361_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = AD9361_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = AD9361_MAX_RX_FREQUENCY_IN_HZ;

    return (status);
}

static skiq_rx_hdl_t _rx_hdl_map_other(rf_id_t *p_id)
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    return (                                            
            ((hdl) == skiq_rx_hdl_A1) ? skiq_rx_hdl_A2 :
            ((hdl) == skiq_rx_hdl_A2) ? skiq_rx_hdl_A1 :
            skiq_rx_hdl_end);
}

static skiq_tx_hdl_t _tx_hdl_map_other(rf_id_t *p_id)
{
    skiq_tx_hdl_t hdl = p_id->hdl;

    return (((hdl) == skiq_tx_hdl_A1) ? skiq_tx_hdl_A2 :
            ((hdl) == skiq_tx_hdl_A2) ? skiq_tx_hdl_A1 :
            skiq_tx_hdl_end);
}

static int32_t _read_rx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth )
{
    int32_t status=0;
    uint32_t act_lpf_freq=0;

    status = ad9361_read_rx_bb_lpf_freq( p_id->card, &act_lpf_freq );
    if( status == 0 )
    {
        // LPF freq is half ot the bandwidth
        *p_bandwidth = act_lpf_freq*2;
    }

    return (status);
}

static int32_t _read_tx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth )
{
    int32_t status=0;
    uint32_t act_lpf_freq=0;

    status = ad9361_read_tx_bb_lpf_freq( p_id->card, &act_lpf_freq );
    if( status == 0 )
    {
        // LPF freq is half ot the bandwidth
        *p_bandwidth = act_lpf_freq*2;
    }

    return (status);
}

static int32_t _write_rx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t bandwidth )
{
    int32_t status=0;

    // LPF freq is half ot the bandwidth
    status = ad9361_write_rx_bb_lpf_freq( p_id->card, bandwidth/2 );

    return (status);
}

static int32_t _write_tx_analog_filter_bandwidth( rf_id_t *p_id, uint32_t bandwidth )
{
    int32_t status=0;

    // LPF freq is half ot the bandwidth
    status = ad9361_write_tx_bb_lpf_freq( p_id->card, bandwidth/2 );

    return (status);
}


/**************************************************************************************************/
static int32_t _read_rx_fir_config( rf_id_t *p_id, uint8_t *p_num_taps, uint8_t *p_fir_decimation )
{
    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_rx_fir_config() always returns 0.  If that
       changes, there may need to be a translation from what it returns to the "negative errno"
       range of values that libsidekiq prefers
    */
    return ad9361_read_rx_fir_config( p_id->chip_id, p_num_taps, p_fir_decimation );
}


/**************************************************************************************************/
static int32_t _read_rx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs )
{
    int32_t status = 0;
    uint8_t num_taps = 0;
    uint8_t decimate = 0;

    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_rx_fir_config() always returns 0.  If that
       changes, there may need to be a translation from what it returns to the "negative errno"
       range of values that libsidekiq prefers
    */
    status = ad9361_read_rx_fir_config( p_id->chip_id, &num_taps, &decimate );
    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_read_rx_fir_coeffs() always returns 0.  If
           that changes, there may need to be a translation from what it returns to the "negative
           errno" range of values that libsidekiq prefers
        */
        status = ad9361_read_rx_fir_coeffs( p_id->chip_id, num_taps, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _write_rx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs )
{
    int32_t status = 0;
    uint8_t num_taps = 0;
    uint8_t decimate = 0;

    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_rx_fir_config() always returns 0. If that
       changes, there may need to be a translation from what it returns to the "negative errno"
       range of values that libsidekiq prefers
    */
    status = ad9361_read_rx_fir_config( p_id->chip_id, &num_taps, &decimate );
    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_write_rx_fir_coeffs() can return -1 if
           num_taps is invalid or 0 if successful.  If that changes, there may need to be a
           translation from what it returns to the "negative errno" range of values that libsidekiq
           prefers
        */
        status = ad9361_write_rx_fir_coeffs( p_id->chip_id, num_taps, p_coeffs );
        if ( status == -1 )
        {
            /* number of taps provided to ad9361_write_rx_fir_coeffs was invalid */
            status = -EINVAL;
        }
    }

    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_save_custom_rx_fir_coeffs() always returns
           0. If that changes, there may need to be a translation from what it returns to the
           "negative errno" range of values that libsidekiq prefers
        */
        status = ad9361_save_custom_rx_fir_coeffs( p_id->chip_id, num_taps, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _read_rx_fir_gain( rf_id_t *p_id, skiq_rx_fir_gain_t *p_gain )
{
    int32_t status = 0;
    ad9361_rx_fir_gain_t ad9361_rx_fir_gain;

    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_rx_fir_gain() returns 0 or negative errno
    */
    status = ad9361_read_rx_fir_gain( p_id->chip_id, &ad9361_rx_fir_gain );
    if ( status == 0 )
    {
        status = ad9361_rx_fir_gain_to_skiq( ad9361_rx_fir_gain, p_gain );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _write_rx_fir_gain( rf_id_t *p_id, skiq_rx_fir_gain_t gain )
{
    int32_t status = 0;
    ad9361_rx_fir_gain_t ad9361_rx_fir_gain;

    status = skiq_rx_fir_gain_to_ad9361( gain, &ad9361_rx_fir_gain );
    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_write_rx_fir_gain() returns 0 or negative
           errno
        */
        status = ad9361_write_rx_fir_gain( p_id->chip_id, ad9361_rx_fir_gain );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _read_tx_fir_config( rf_id_t *p_id,
                                    uint8_t *p_num_taps,
                                    uint8_t *p_fir_interpolation )
{
    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_tx_fir_config() always returns 0.  If that
       changes, there may need to be a translation from what it returns to the "negative errno"
       range of values that libsidekiq prefers
    */
    return ad9361_read_tx_fir_config( p_id->chip_id, p_num_taps, p_fir_interpolation );
}


/**************************************************************************************************/
static int32_t _read_tx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs )
{
    int32_t status = 0;
    uint8_t num_taps = 0;
    uint8_t interp = 0;

    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_tx_fir_config() always returns 0.  If that
       changes, there may need to be a translation from what it returns to the "negative errno"
       range of values that libsidekiq prefers
    */
    status = ad9361_read_tx_fir_config( p_id->chip_id, &num_taps, &interp );
    if ( status == 0 )
    {
        status = ad9361_read_tx_fir_coeffs( p_id->chip_id, num_taps, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _write_tx_fir_coeffs( rf_id_t *p_id, int16_t *p_coeffs )
{
    int32_t status = 0;
    uint8_t num_taps = 0;
    uint8_t interp = 0;

    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_tx_fir_config() always returns 0.  If that
       changes, there may need to be a translation from what it returns to the "negative errno"
       range of values that libsidekiq prefers
    */
    status = ad9361_read_tx_fir_config( p_id->chip_id, &num_taps, &interp );
    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_write_tx_fir_coeffs() always returns 0.  If
           that changes, there may need to be a translation from what it returns to the "negative
           errno" range of values that libsidekiq prefers
        */
        status = ad9361_write_tx_fir_coeffs( p_id->chip_id, num_taps, p_coeffs );
    }

    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_save_custom_tx_fir_coeffs() always returns
           0.  If that changes, there may need to be a translation from what it returns to the
           "negative errno" range of values that libsidekiq prefers
        */
        status = ad9361_save_custom_tx_fir_coeffs( p_id->chip_id, num_taps, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _read_tx_fir_gain( rf_id_t *p_id, skiq_tx_fir_gain_t *p_gain )
{
    int32_t status = 0;
    ad9361_tx_fir_gain_t ad9361_tx_fir_gain;

    /**
       @note In v5.5.1 of the AD9361 driver, ad9361_read_tx_fir_gain() returns 0 or negative errno
    */
    status = ad9361_read_tx_fir_gain( p_id->chip_id, &ad9361_tx_fir_gain );
    if ( status == 0 )
    {
        status = ad9361_tx_fir_gain_to_skiq( ad9361_tx_fir_gain, p_gain );
    }

    return status;
}


/**************************************************************************************************/
static int32_t _write_tx_fir_gain( rf_id_t *p_id, skiq_tx_fir_gain_t gain )
{
    int32_t status = 0;
    ad9361_tx_fir_gain_t ad9361_tx_fir_gain;

    status = skiq_tx_fir_gain_to_ad9361( gain, &ad9361_tx_fir_gain );
    if ( status == 0 )
    {
        /**
           @note In v5.5.1 of the AD9361 driver, ad9361_write_tx_fir_gain() returns 0 or negative
           errno
        */
        status = ad9361_write_tx_fir_gain( p_id->chip_id, ad9361_tx_fir_gain );
    }

    return status;
}
