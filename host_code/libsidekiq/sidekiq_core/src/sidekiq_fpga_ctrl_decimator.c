/**
 * @file   sidekiq_fpga_ctrl_decimator.c
 * @date   Tue Feb 12 15:34:41 2019
 * 
 * @brief  Provides access to the decimator instances in the FPGA
 * 
 * <pre>
 * Copyright 2019 Epiq Solutions, All Rights Reserved
 * </pre>
 * 
 */

/***** INCLUDES *****/

#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <time.h>

#include "sidekiq_fpga_ctrl_decimator.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_fpga.h"
#include "bit_ops.h"

#include "sidekiq_private.h"

/* enable debug_print and debug_print_plain when DEBUG_DECIMATOR is defined */
#if (defined DEBUG_DECIMATOR)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

#define NR_MAX_TAPS                     (1 << DECIMATOR_EVEN_TAPS_HALF_LENGTH_LEN)

#define DEC_REGS(_rx)                                                  \
    {                                                                  \
        .prog_coeff_register = FPGA_REG_DECIMATOR_PROG_COEFFS_ ## _rx, \
        .ctrl_register = FPGA_REG_DECIMATOR_CTRL_ ## _rx,              \
        .build_info_register = FPGA_REG_DECIMATOR_BUILD_INFO_ ## _rx,  \
    }

/***** TYPE DEFINITIONS *****/

enum dec_filt_type
{
    dec_filt_type_sinc,
    dec_filt_type_impulse,
};


/***** STRUCTS *****/

struct decimator
{
    uint32_t prog_coeff_register;
    uint32_t ctrl_register;
    uint32_t build_info_register;
};


/***** LOCAL DATA *****/

static struct decimator decimators[skiq_rx_hdl_end] =
{
    [skiq_rx_hdl_A1] = DEC_REGS(RX1),
    [skiq_rx_hdl_A2] = DEC_REGS(RX2),
    [skiq_rx_hdl_B1] = DEC_REGS(RX3),
    [skiq_rx_hdl_B2] = DEC_REGS(RX4),
    [skiq_rx_hdl_C1] = DEC_REGS(RX5),
    [skiq_rx_hdl_D1] = DEC_REGS(RX6),
};


/***** LOCAL FUNCTIONS *****/

static int32_t
reset_decimator( uint8_t card,
                 struct decimator *dec )
{
    int32_t status;

    status = sidekiq_fpga_reg_RMWV( card, 1, DECIMATOR_RESET, dec->ctrl_register );
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV( card, 0, DECIMATOR_RESET, dec->ctrl_register );
    }

    return status;
}


static int32_t
write_coefficient( uint8_t card,
                   struct decimator *dec,
                   int16_t coeff )
{
    int32_t status;

    status = sidekiq_fpga_reg_RMWV( card, coeff, DECIMATOR_COEFF, dec->prog_coeff_register );
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV( card, 1, DECIMATOR_WRITE, dec->prog_coeff_register );
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_RMWV( card, 0, DECIMATOR_WRITE, dec->prog_coeff_register );
    }

    return status;
}


/**************************************************************************************************/
/* controlling_decimator() returns a reference to 'struct decimator' that controls the decimator
   associated with the specified handle.  This could be:

   1. NULL (no decimator present)
   2. decimators[hdl] if it is independently controlled or the dominant handle
   3. decimators["other" hdl] if controlled via the other handle's decimator

   Independently controlled means that the DECIMATOR_A1_CONTROLS_A2 bit in DECIMATOR_BUILD_INFO
   field is cleared.  Please note that DECIMATOR_A1_CONTROLS_A2 is poorly named and it can also mean
   that B1 controls B2.

   Dominant handle means that the DECIMATOR_A1_CONTROLS_A2 bit in DECIMATOR_BUILD_INFO field is set
   and the handle is either A1 or B1.

   The FPGA can be configured a few different way and this function is meant to resolve each case as
   follows:
   - No decimators
     - Return NULL for either A1 or A2
   - A1 has a decimator
     - Return A1's instance for A1
     - Return NULL for A2
   - A2 has a decimator
     - Return NULL for A1
     - Return A2's instance for A2
   - A1 and A2 have decimators and are independently controlled
     - Return A1's instance for A1
     - Return A2's instance for A2
   - A1 and A2 have decimators and are dependently controlled (by A1's instance)
     - Return A1's instance for A1
     - Return A1's instance for A2  <-- this is the oddball case, A2's decimator is controlled by A1's instance

 */
static struct decimator *
controlling_decimator( uint8_t card,
                       skiq_rx_hdl_t hdl )
{
    int32_t status;
    struct decimator *dec = &(decimators[hdl]);
    uint32_t value = 0;

    status = sidekiq_fpga_reg_read( card, dec->build_info_register, &value );
    if ( status == 0 )
    {
        if ( RBF_GET( value, DECIMATOR_STAGES ) == 0 )
        {
            /* if the number of stages is zero, there's still a chance there's a decimator on the
             * corresponding "other" handle that controls this handle.  Otherwise leave the
             * decimator reference associated with this handle as the return value */
            if ( hdl == skiq_rx_hdl_A2 )
            {
                dec = controlling_decimator( card, skiq_rx_hdl_A1 );
            }
            else if ( hdl == skiq_rx_hdl_B2 )
            {
                dec = controlling_decimator( card, skiq_rx_hdl_B1 );
            }
            else
            {
                /* If the handle is A1, B1, C1, D1 and the number of stages is zero, that's it,
                 * there's no decimator for the specified handle */
                dec = NULL;
            }

            /* verify that the decimator on the corresponding "other" handle has the A1_CONTROLS_A2
             * bit set.  If it does, then it's a valid controlling decimator instance, otherwise set
             * dec to NULL */
            if ( dec != NULL )
            {
                status = sidekiq_fpga_reg_read( card, dec->build_info_register, &value );
                if ( RBF_GET( value, DECIMATOR_A1_CONTROLS_A2 ) == 0 )
                {
                    dec = NULL;
                }
            }
        }
    }
    else
    {
        dec = NULL;
    }

    return dec;
}


/**************************************************************************************************/
/** The program_decimator_taps() function operates on the decimator associated with the specified
    Sidekiq card and receive handle.  If the decimator is present, it will first reset the
    decimator, then write the coefficients of a Sinc decimation filter (type == DEC_FILT_TYPE_SINC)
    or an Impulse decimation filter (type == DEC_FILT_TYPE_IMPULSE) to every stage based on the
    number of taps.

    @note This algorithm was gleefully ported from sidekiqx2_decimator.git:sw/user_app.c

    @param[in] card Sidekiq card number of interest
    @param[in] hdl Sidekiq receive handle of interest
    @param[in] type Decimation filter type enumeration

    @return int32_t
    @retval 0 success
    @retval -EINVAL Invalid decimation filter type specified (only DEC_FILT_TYPE_SINC and DEC_FILT_TYPE_IMPULSE are valid)
    @retval -ENOSYS No decimator available for specified card and receive handle
    @retval -EBADMSG Error occurred transacting with FPGA registers
 */
static int32_t
program_decimator_taps( uint8_t card,
                        skiq_rx_hdl_t hdl,
                        enum dec_filt_type type )
{
    int32_t status = 0;
    struct decimator *dec = &(decimators[hdl]);
    uint32_t num_stages, even_taps_half_length;

    if ( ( dec_filt_type_sinc == type ) || ( dec_filt_type_impulse == type ) )
    {
        if ( dec_filt_type_sinc == type )
        {
            debug_print("Sinc decimation filter selected\n");
        }
        else if ( dec_filt_type_impulse == type )
        {
            debug_print("Impulse decimation filter selected\n");
        }
    }
    else
    {
        status = -EINVAL;
    }

    /* read the build information: number of stages and number of taps */
    if ( status == 0 )
    {
        uint32_t value = 0;

        status = sidekiq_fpga_reg_read( card, dec->build_info_register, &value );
        if ( status == 0 )
        {
            debug_print("BUILD INFO (0x%04x): 0x%08x\n", dec->build_info_register, value);

            /* read the number of decimator stages that are available in the FPGA */
            num_stages = RBF_GET( value, DECIMATOR_STAGES );
            if ( num_stages == 0 )
            {
                /* if the number of stages is zero, there's no decimator to use */
                status = -ENOSYS;
            }
        }

        if ( status == 0 )
        {
            /* reset the decimator */
            status = reset_decimator( card, dec );
        }

        if ( status == 0 )
        {
            /* read the per stage number of taps parameter:
               the value is actually half the number of taps on the right side of the filter
               impulse response */
            even_taps_half_length = RBF_GET( value, DECIMATOR_EVEN_TAPS_HALF_LENGTH );
            debug_print("  Decimator has %u stages\n", num_stages);
            debug_print("  Each decimator stage has %u taps\n", 4 * (even_taps_half_length - 1) + 3);
        }
    }

    /* calculate tap values, then program the taps for each stage */
    if ( status == 0 )
    {
        float taps[NR_MAX_TAPS];
        float norm_fact, sum_sq = 1.;
        int16_t dtaps[NR_MAX_TAPS];
        uint16_t i, s;

        /* generate and store a sinc impulse response when type == dec_filt_type_sinc and when
         * type == dec_filt_type_impulse we generate and store an impulse */
        taps[0] = 1.;

        for ( i = 1; i <= even_taps_half_length; i++ )
        {
            if ( dec_filt_type_sinc == type )
            {
                float x = 0.5 * (float)( 2 * i - 1 );
                taps[i] = sin(M_PI*x) / (M_PI*x);
            }
            else if ( dec_filt_type_impulse == type )
            {
                taps[i] = 0.;
            }

            /* compute the sum-of-squares to normalize the taps, the factor of 2 is needed because
             * we're only summing the taps on the right side of the impulse response */
            sum_sq += 2. * taps[i] * taps[i];
            debug_print("taps[%u] = %f, sum_sq = %f\n", i, taps[i], sum_sq);
        }

        /* store the normalization factor for a power-preserving filter when
         * type == dec_filt_type_sinc */
        if ( dec_filt_type_sinc == type )
        {
            norm_fact = sqrt( 2. * sum_sq );
        }
        else if ( dec_filt_type_impulse == type )
        {
            norm_fact = powf(2.,15.) / ( powf(2.,15.) - 1. );
        }
        debug_print("norm_fact = %f\n", norm_fact);

        for ( i = 0; i <= even_taps_half_length; i++ )
        {
            /* compute the coeff to <16,15> quantization */
            dtaps[i] = (int16_t)roundf( taps[i] / norm_fact * (float)(1<<15) );
            debug_print("dtaps[%u] = %d\n", i, dtaps[i]);
        }

        debug_print("  Programming decimator taps...\n");
        for ( s = 1; (s <= num_stages) && (status == 0); s++ )
        {
            status = sidekiq_fpga_reg_RMWV( card, s, DECIMATOR_STAGE, dec->prog_coeff_register );
            for ( i = 0; (i <= even_taps_half_length) && (status == 0); i++ )
            {
                status = write_coefficient( card, dec, dtaps[i] );
            }
        }
        if ( status == 0 )
        {
            debug_print("  Done programming decimator taps\n");
        }
        else
        {
            debug_print("  ERROR programming decimator taps! (status = %d)\n", status);
        }
    }

    /* Finally, set the rate to 1:1 and disable zero-padding */
    if ( status == 0 )
    {
        debug_print("Setting decimator rate to 1:1 on card %u and handle %s\n", card,
                    rx_hdl_cstr( hdl ) );
        status = sidekiq_fpga_reg_RMWV(card, 0, DECIMATOR_RATE, dec->ctrl_register );
    }

    if ( status == 0 )
    {
        debug_print("Disabling zero-padded output on decimator for card %u and handle %s\n", card,
                    rx_hdl_cstr( hdl ) );
        status = sidekiq_fpga_reg_RMWV(card, 0, DECIMATOR_ZERO_PADDED_OUTPUT, dec->ctrl_register );
    }

    /* squash return code to -EBADMSG for any error encountered reading / writing FPGA registers not
     * associated with absent support */
    if ( ( status != 0 ) && ( status != -ENOSYS ) )
    {
        status = -EBADMSG;
    }

    return status;
}


/**************************************************************************************************/
/* GLOBAL FUNCTIONS */
/**************************************************************************************************/


/**************************************************************************************************/
/* function documentation in header file */
int32_t
decimator_init_config( uint8_t card )
{
    int32_t status = 0;
    skiq_rx_hdl_t hdl;

    for ( hdl = skiq_rx_hdl_A1; ( hdl < skiq_rx_hdl_end ) && ( status == 0 ); hdl++ )
    {
        status = program_decimator_taps( card, hdl, dec_filt_type_sinc );
        if ( status == -ENOSYS )
        {
            uint32_t nr_stages = decimator_nr_stages( card, hdl );

            if ( nr_stages > 0 )
            {
                skiq_info("Decimator on card %u and handle %s has %u stages, but control is "
                          "shared\n", card, rx_hdl_cstr( hdl ), decimator_nr_stages( card, hdl ) );
            }
            else
            {
                debug_print("No decimator available on card %u and handle %s\n", card,
                            rx_hdl_cstr( hdl ) );
            }

            /* during initialization, ultimately ignore -ENOSYS since decimator is not required */
            status = 0;
        }
        else
        {
            skiq_info("Decimator on card %u and handle %s has %u stages\n", card,
                      rx_hdl_cstr( hdl ), decimator_nr_stages( card, hdl ) );
        }
    }

    /* squash return code to -EBADMSG for any error encountered reading / writing FPGA registers */
    if ( status != 0 )
    {
        status = -EBADMSG;
    }

    return status;
}


/**************************************************************************************************/
/* function documentation in header file */
int32_t
decimator_reset_config( uint8_t card )
{
    int32_t status = 0;
    skiq_rx_hdl_t hdl;

    for ( hdl = skiq_rx_hdl_A1; ( hdl < skiq_rx_hdl_end ) && ( status == 0 ); hdl++ )
    {
        struct decimator *dec = &(decimators[hdl]);
        uint32_t value = 0;

        status = sidekiq_fpga_reg_read( card, dec->build_info_register, &value );
        if ( status == 0 )
        {
            debug_print("BUILD INFO (0x%04x): 0x%08x\n", dec->build_info_register, value);

            /* read the number of decimator stages that are available in the FPGA */
            if ( RBF_GET( value, DECIMATOR_STAGES ) > 0 )
            {
                /* reset the decimator */
                status = reset_decimator( card, dec );

                if ( status == 0 )
                {
                    debug_print("Disabling decimator on card %u and handle %s\n", card,
                                rx_hdl_cstr( hdl ) );
                    status = sidekiq_fpga_reg_RMWV(card, 0, DECIMATOR_ENABLE, dec->ctrl_register );
                }

                if ( status == 0 )
                {
                    debug_print("Setting decimator rate to 1:1 on card %u and handle %s\n", card,
                                rx_hdl_cstr( hdl ) );
                    status = sidekiq_fpga_reg_RMWV(card, 0, DECIMATOR_RATE, dec->ctrl_register );
                }

                if ( status == 0 )
                {
                    debug_print("Disabling zero-padded output on decimator for card %u and handle "
                                "%s\n", card, rx_hdl_cstr( hdl ) );
                    status = sidekiq_fpga_reg_RMWV(card, 0, DECIMATOR_ZERO_PADDED_OUTPUT,
                                                   dec->ctrl_register );
                }
            }
            else
            {
                /* if the number of stages is zero, there's no decimator to use.  This can be safely
                 * ignored when "resetting" card configuration since presence of a decimator is not
                 * required for this function. */
            }
        }
    }

    /* squash return code to -EBADMSG for any error encountered reading / writing FPGA registers */
    if ( status != 0 )
    {
        status = -EBADMSG;
    }

    return status;
}


/**************************************************************************************************/
/* function documentation in header file */
uint32_t
decimator_nr_stages( uint8_t card,
                     skiq_rx_hdl_t hdl )
{
    int32_t status = 0;
    uint32_t nr_stages = 0;
    struct decimator *dec = controlling_decimator( card, hdl );

    if ( dec != NULL )
    {
        /* report the number of decimator stages available in the referenced decimator's build info
         * register */
        status = sidekiq_fpga_reg_read( card, dec->build_info_register, &nr_stages );
        if ( status == 0 )
        {
            nr_stages = RBF_GET( nr_stages, DECIMATOR_STAGES );
        }
    }

    return nr_stages;
}


/**************************************************************************************************/
/* function documentation in header file */
int32_t
decimator_enable_if_present( uint8_t card,
                             skiq_rx_hdl_t hdl )
{
    int32_t status = 0;

    if ( decimator_nr_stages( card, hdl ) > 0 )
    {
        struct decimator *dec = controlling_decimator( card, hdl );

        if ( dec == NULL )
        {
            status = -EPROTO;
        }
        else
        {
            uint32_t value = 0;

            status = sidekiq_fpga_reg_read( card, dec->ctrl_register, &value );
            if ( ( status == 0 ) && ( RBF_GET( value, DECIMATOR_RATE ) > 0 ) )
            {
                debug_print("Enabling decimator on card %u and handle %s\n", card,
                            rx_hdl_cstr( hdl ) );
                status = sidekiq_fpga_reg_RMWV( card, 1, DECIMATOR_ENABLE, dec->ctrl_register );

                /* squash return code to -EBADMSG for any error encountered reading / writing FPGA
                 * registers */
                if ( status != 0 )
                {
                    status = -EBADMSG;
                }
            }
            else if ( status == 0 )
            {
                debug_print("Not enabling decimator on card %u and handle %s because configured "
                            "rate is 0\n", card, rx_hdl_cstr( hdl ) );
            }
            else
            {
                status = -EBADMSG;
            }
        }
    }

    return status;
}


/**************************************************************************************************/
/* function documentation in header file */
int32_t
decimator_disable_if_present( uint8_t card,
                              skiq_rx_hdl_t hdl )
{
    int32_t status = 0;

    if ( decimator_nr_stages( card, hdl ) > 0 )
    {
        struct decimator *dec = controlling_decimator( card, hdl );

        if ( dec == NULL )
        {
            status = -EPROTO;
        }
        else
        {
            debug_print("Disabling decimator on card %u and handle %s\n", card,
                        rx_hdl_cstr( hdl ) );
            status = sidekiq_fpga_reg_RMWV( card, 0, DECIMATOR_ENABLE, dec->ctrl_register );

            /* squash return code to -EBADMSG for any error encountered reading / writing FPGA
             * registers */
            if ( status != 0 )
            {
                status = -EBADMSG;
            }
        }
    }

    return status;
}


/**************************************************************************************************/
/* function documentation in header file */
int32_t
decimator_set_rate( uint8_t card,
                    skiq_rx_hdl_t hdl,
                    uint8_t rate )
{
    uint32_t nr_stages = decimator_nr_stages( card, hdl );
    int32_t status = 0;

    if ( rate > nr_stages )
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        struct decimator *dec = controlling_decimator( card, hdl );
        if ( dec == NULL )
        {
            if ( rate > 0 )
            {
                skiq_error("Trying to set a decimation rate of %u without a decimator block "
                           "present on card %u and handle %s\n", rate, card, rx_hdl_cstr( hdl ) );
            }
            else
            {
                /* silent ignore setting the rate to 0 */
            }
        }
        else
        {
            debug_print("Setting decimator rate to %u -> (1/%u) on card %u and handle %s\n", rate,
                        1 << rate, card, rx_hdl_cstr( hdl ));
            status = sidekiq_fpga_reg_RMWV( card, rate, DECIMATOR_RATE, dec->ctrl_register );
            if ( ( status == 0 ) && ( rate == 0 ) )
            {
                /* automatically disable the decimator if the decimation rate is set to 0 */
                status = sidekiq_fpga_reg_RMWV( card, 0, DECIMATOR_ENABLE, dec->ctrl_register );
            }
        }
    }

    /* squash return code to -EBADMSG for any error encountered reading / writing FPGA registers not
     * associated with invalid arguments */
    if ( ( status != 0 ) && ( status != -EINVAL ) )
    {
        status = -EBADMSG;
    }

    return status;
}


/**************************************************************************************************/
/* function documentation in header file */
int32_t
decimator_get_rate( uint8_t card,
                    skiq_rx_hdl_t hdl,
                    uint8_t *p_rate )
{
    struct decimator *dec = controlling_decimator( card, hdl );
    int32_t status = 0;
    uint32_t value = 0;

    if ( dec == NULL )
    {
        *p_rate = 0;
    }
    else
    {
        status = sidekiq_fpga_reg_read( card, dec->ctrl_register, &value );
        if ( status == 0 )
        {
            *p_rate = RBF_GET( value, DECIMATOR_RATE );
        }
        else
        {
            status = -EBADMSG;
        }
    }

    return status;
}


/**************************************************************************************************/
/* function documentation in header file */
bool
decimator_only_supports_half_max_rate( uint8_t card,
                                       skiq_rx_hdl_t hdl )
{
    struct decimator *dec = &(decimators[hdl]);
    int32_t status;
    uint32_t value = 0;

    /* default to only supporting half max rate in the case the FPGA register transaction fails */
    bool only_supports_half_max_rate = true;

    status = sidekiq_fpga_reg_read( card, dec->build_info_register, &value );
    if ( status == 0 )
    {
        only_supports_half_max_rate = ( RBF_GET( value, DECIMATOR_HALF_MAX_RATE ) > 0 );
    }

    return only_supports_half_max_rate;
}
