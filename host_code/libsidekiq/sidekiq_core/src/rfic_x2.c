/*****************************************************************************/
/** @file rfic_x2.c

 <pre>
 Copyright 2020 Epiq Solutions, All Rights Reserved
 </pre>
*/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <inttypes.h>
#include <stdlib.h>

#include "rfic_x2.h"
#include "rfic_x2_private.h"
#include "rfic_common.h"
#include "sidekiq_hal.h"
#include "sidekiq_fpga.h"
#include "sidekiq_fpga_ctrl.h"
#include "fpga_jesd_ctrl.h"
#include "calc_ad9528.h"
#include "sidekiq_rffc5071a_private.h"
#include "rffc5071a_api.h"

#include "t_ad9528.h"
#include "ad9528.h"
#include "t_mykonos.h"
#include "mykonos.h"
#include "mykonos_gpio.h"
#include "mykonos_firmware.h"
#include "bit_ops.h"
#include "ad9371_profiles.h"
#include "ad9371_parse_profile.h"

/* enable debug_print and debug_print_plain when DEBUG_RFIC_X2 is defined */
#if (defined DEBUG_RFIC_X2)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/**************************************************************************/
#define AD9371_MAX_COEFFS (100) // filter coefficients will never exceed 100 (from ADI)

#define AD9371_ADC_RES (16)
#define AD9371_DAC_RES (14)

// refer to Table 151/152 of UG-992 (p.197)
#define AD9371_MONITOR_INDEX_AGC_MON_9   (0x09)
#define AD9371_MONITOR_INDEX_AGC_MON_10  (0x0A)

#define RFIC_SPI_CS_AD9371 (0)
#define RFIC_SPI_CS_AD9528 (1)

#define AD9371_SCRATCH_REG (0x022) // SLEW_CTL_0
#define AD9528_SCRATCH_REG (0x07)

#define MCS_STATUS_OK (0xB) // refer to MYKONOS_enableMultichipSync()
#define NUM_SYSREF_PULSES (4)

#define RX_SYNC_STATUS (0x3E) // refer to MYKONOS_readRxFramerStatus()
#define RX_SYNC_STATUS_MASK (0xBF) // we don't really care about R/W ptr change (bit 6)
#define TX_SYNC_STATUS (0x28) // refer to MYKONOS_readDeframerStatus()
#define TX_SYNC_STATUS_MASK (0x28) // we don't care about anything outside of 0x28

#define DEFAULT_JESD_TX_DIFFCTRL (1)
#define DEFAULT_JESD_TX_PRECURSOR (10)

// refer to MYKONOS_checkPllLockStatus()
#define RX_PLL_LOCK    (1<<2)
#define TX_PLL_LOCK    (1<<3)

// Note: if the default config has TX enabled, we need to always include TX_BB_FILTER otherwise it'll fail
// For details on the calibration flags, refer to USG-992 (p. 79)
#define MISC_CAL_STATE (TX_BB_FILTER | ADC_TUNER | TIA_3DB_CORNER | LOOPBACK_RX_LO_DELAY | \
                        RX_GAIN_DELAY | FLASH_CAL | PATH_DELAY | TX_ATTENUATION_DELAY)
#define INIT_TX_CAL_STATE (TX_LO_LEAKAGE_INTERNAL | TX_QEC_INIT)

#define SKIQ_RX_CAL_TYPES \
    (skiq_rx_cal_type_dc_offset | skiq_rx_cal_type_quadrature)

#define TRACKING_RX_CAL_STATE (TRACK_RX1_QEC | TRACK_RX2_QEC | TRACK_ORX1_QEC)
#define TRACKING_TX1_CAL_STATE (TRACK_TX1_LOL | TRACK_TX1_QEC)
#define TRACKING_TX2_CAL_STATE (TRACK_TX2_LOL | TRACK_TX2_QEC)

// Note: we're always just going to generate a test tone at a fixed offset of 500kHz
#define TX_TEST_TONE_FREQ_OFFSET_KHZ (500) 

/** @brief Max quarter dB attentuation for Tx.
    \note (41950 mdB / 1000) * 4 = 167.8 quarter dB = 167 (uint16_t) */
#define MAX_TX_QUARTER_DB_ATTEN (167)
/** @brief Min quarter dB attentuation for Tx. */
#define MIN_TX_QUARTER_DB_ATTEN (0)

#define MAX_FREQ_TABLE_ENTRIES          ARRAY_SIZE(_freqTable)

#define MAX_FREQ_CAL_OFFSET (100000000)

/* AD9371 Frequency Range RX: RX: 300 to 6000 MHz, TX: 300 to 6000 Mhz*/
#define AD9371_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(300ULL*RATE_MHZ_TO_HZ)
#define AD9371_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define AD9371_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(300ULL*RATE_MHZ_TO_HZ)
#define AD9371_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

/* warp voltage defines...Preferred operating range is 0.5V to 2.5V, 
   but permit the full range of the DAC to be configured by the user.
   According to Jeff P "I would suggest making the limits 0-65535 in order 
   to support a wider range of oscillators.  Normally it won't be programmed 
   outside of the 0.4-2.4V range, but no harm will come to the part in the 
   0-2.5V range.  (Certain specs might not apply at the extremes.)" 
*/
#define MIN_WARP_VOLTAGE (0)
#define DEFAULT_WARP_VOLTAGE (32768)
#define MAX_WARP_VOLTAGE (65535)

// FPGA version requirement
#define FPGA_VERS_X2_REV_C              FPGA_VERSION(3,10,0)
#define FPGA_VERS_X2_REV_C_CSTR         FPGA_VERSION_STR(3,10,0)


/********************* half of maximum sample clock (not rate!) *********************/
/* In order to meet timing, the FPGA cannot make the decimator available at the full maximum sample
 * clock and will indicate so with the DECIMATOR_HALF_MAX_RATE bit in each DECIMATOR_BUILD_INFO
 * register.  So if there's a future FPGA design that works at the full sample clock, the code is
 * designed to handle both cases. */
#define X2_HALF_MAX_SAMPLE_CLK_HZ                 76800000


/********************* ORX to dual lane *********************/
#define ORX_HDL                                   (skiq_rx_hdl_B1)
#define ORX_DUAL_LANE_RATE_THRESHOLD_KHZ          153600
#define ORX_SINGLE_LANE_SERIALIZER_SETTING        (1 << 2) /* Lane 2 */
#define ORX_DUAL_LANE_SERIALIZER_SETTING          ((1 << 2) | (1 << 3)) /* Lanes 2 & 3 */

#define LANE_SEL_0_DESERIALIZER_LANES (0x3) // lanes 0 and 1 enabled
#define LANE_SEL_1_DESERIALIZER_LANES (0xC) // lanes 2 and 3 enabled

#define MAX_NUM_JESD_RETRIES (10)

typedef struct
{
    uint8_t freqTableIndex; // index into freqTable of current freq
    uint64_t lastCalFreq;   // last freq when cal was performed
} freqCalState_t;


struct found_profile_t
{
    uint8_t dec_rate;
    ad9371_profile_t *p_profile;
};

typedef struct
{
    skiq_freq_tune_mode_t tune_mode;
    uint16_t num_freqs;
    uint64_t hop_list[SKIQ_MAX_NUM_FREQ_HOPS];
    uint16_t mailbox; // this is the only thing users can write to
    uint16_t next_hop; // this is populated when writing next hop
    uint16_t curr_hop; // this is populated only after a hop was performed from next hop
} x2_freq_hop_t;

#define FREQ_HOP_INITIALIZER                           \
    {                                                  \
        .tune_mode = skiq_freq_tune_mode_standard,     \
        .num_freqs = 0,                                \
        .next_hop = 0,                                 \
        .curr_hop = 0,                                 \
        .mailbox  = 0,                                 \
    }
        
mykonosDevice_t *_p_user_mykDevice[SKIQ_MAX_NUM_CARDS] = { [0 ... (SKIQ_MAX_NUM_CARDS-1)] = NULL };

ARRAY_WITH_DEFAULTS(static x2_freq_hop_t, _rx_freq_hop, SKIQ_MAX_NUM_CARDS, FREQ_HOP_INITIALIZER);
ARRAY_WITH_DEFAULTS(static x2_freq_hop_t, _tx_freq_hop, SKIQ_MAX_NUM_CARDS, FREQ_HOP_INITIALIZER);

// Table 95 from UG-992 (p. 117)...any time we cross the boundary, we need to re-run cal
static const freq_range_t _freqTable[] =
{
    { 400000000, 750000000 },    // divide by 16 (400-750M)
    { 750000000, 1500000000 },   // divide by 8 (750-1500M)
    { 1500000000, 3000000000 },  // divide by 4 (1500-3000M)
    { 3000000000, 6000000000 },  // divide by 2 (3000-6000M)
};

static ad9528Device_t _clockDevice[SKIQ_MAX_NUM_CARDS];
static mykonosDevice_t _mykDevice[SKIQ_MAX_NUM_CARDS];

// it seems that this is only for Rx1/2 and the config is shared
static freqCalState_t _rx_cal_state[SKIQ_MAX_NUM_CARDS];
// it seems that this is only for Tx1/2 and the config is shared
static freqCalState_t _tx_cal_state[SKIQ_MAX_NUM_CARDS];

static bool _tx_test_tone[SKIQ_MAX_NUM_CARDS];

static uint32_t _dev_clock[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = 0,
};
static uint64_t _fpga_gbt_clock[SKIQ_MAX_NUM_CARDS] =
{
    [ 0 ... (SKIQ_MAX_NUM_CARDS-1) ] = 0,
};

static const ad9371_profile_t *_p_min_rx_profile = NULL, *_p_min_orx_profile = NULL, *_p_min_tx_profile = NULL;
static const ad9371_profile_t *_p_max_rx_profile = NULL, *_p_max_orx_profile = NULL, *_p_max_tx_profile = NULL;
static const ad9371_profile_t *_p_max_dual_lane_orx_profile = NULL;
static uint32_t _max_rx_profile_index = 0, _max_orx_profile_index = 0, _max_tx_profile_index = 0;
static uint32_t _max_dual_lane_orx_profile_index = 0;

static uint16_t _warp_voltage[SKIQ_MAX_NUM_CARDS] =
{ [0 ... (SKIQ_MAX_NUM_CARDS-1)] = DEFAULT_WARP_VOLTAGE };

static skiq_tx_quadcal_mode_t _tx_quadcal[SKIQ_MAX_NUM_CARDS][skiq_tx_hdl_end];

// RX cal mode (only supported for Rx1/2 and it's shared)
ARRAY_WITH_DEFAULTS(static skiq_rx_cal_mode_t, _rx_cal_mode, SKIQ_MAX_NUM_CARDS, skiq_rx_cal_mode_manual);
// RX cal type (only supported for Rx1/2 and it's shared)
ARRAY_WITH_DEFAULTS(static uint32_t, _rx_cal_mask, SKIQ_MAX_NUM_CARDS, SKIQ_RX_CAL_TYPES);
                                                 

static const ad9528SpiSettings_t _x2_clockSpiSettings =
{
    2, //chip select Index
    0, //Write bit polarity
    1, //16bit instruction word
    1, //MSB first
    0, //Clock phase
    0, //Clock polarity
    0, //uint8_t enSpiStreaming;
    1, //uint8_t autoIncAddrUp;
    1  //uint8_t fourWireMode;
};

static const ad9528sysrefSettings_t _x2_clockSysrefSettings =
{
    0,                                          // sysrefRequestMethod, SPI = 0, PIN = 1
    2,                                          // sysrefSource,        EXTERNAL = 0, EXT_RESAMPLED = 1, INTERNAL = 2
    0,                                          // sysrefPinEdgeMode,   LEVEL_ACTIVE_HIGH = 0, LEVEL_ACTIVE_LOW = 1, RISING_EDGE = 2, FALLING_EDGE = 3
    0,                                          // sysrefPinBufferMode, 
    0,                                          // sysrefPatternMode,   NSHOT = 0, CONTINUOUS = 1, PRBS = 2, STOP = 3
    0,                                          // sysrefNshotMode,     ONE_PULSE = 0, TWO_PULSES = 2, FOUR_PULSES = 3, SIX_PULSES = 4, EIGHT_PULSES = 5
    512                                         // sysrefDivide,        
};


// X2 rev B
static const ad9528outputSettings_t _x2_clockOutputSettings =
{
    0x015C,                                      // outPowerDown,        bit per output, if 1 power down that output, 14 outputs in total
    {0,0,0,0,0,0,0,0,0,2,0,0,0,2},               // outSource[14],       CHANNEL_DIV, PLL1_OUTPUT, SYSREF, INV_PLL1_OUTPUT  : order is 0....14
    {0,2,0,0,0,2,0,0,0,2,0,2,0,0},               // outBufferCtrl[14],   LVDS, LVDS_BOOST, HSTL (see https://confluence.epiq.rocks/display/EN/TX+Anomaly+Investigation#TXAnomalyInvestigation-X2FPGAIOPadTerminationvs.9528andRFIC)
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0},               // outAnalogDelay[14]   5 bits
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0},               // outDigitalDelay[14]; 6 bits, 1/2 clock resolution @ channel div input frequency
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // outChannelDiv[14];   8 bit channel divider, default to 1
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // output clock frequency per clock output (note: this is informational only)
    0,           // VCXO to distribution
};

static const clk_sel_t _x2_clk_sel =
{
    .fpga_clk_sel_mask = AD9528_GBT_CLK_OUT(7),
    .debug_clk_sel = 10,
    .rffc1_clk_sel = 5,
    .rffc2_clk_sel = 1,
};

static ARRAY_WITH_DEFAULTS(bool, rfic_active, SKIQ_MAX_NUM_CARDS, false);

extern int32_t switch_clock_osc( uint8_t card,
                                 uint32_t osc_freq );

static int32_t _apply_profile_change( rf_id_t *p_id,
                                      const ad9371_profile_t *p_profile );

static int32_t _reset_ad9371( rf_id_t *p_id );
static int32_t _reset_ad9528( rf_id_t *p_id );

// Note: this can return an invalid index (MAX_FREQ_TABLE_ENTRIES) if the frequency specified is out of bounds 
static uint8_t _find_freq_table_index( uint64_t lo_freq );

static int32_t _tx_test_tone_actual_freq( uint32_t tx_sample_rate_kHz, int32_t test_tone_kHz );

static int32_t _convert_skiq_rx_cal_to_mykonos_cal( skiq_rx_hdl_t hdl,
                                                    uint32_t skiq_cal_mask,
                                                    uint32_t *p_mykonos_cal_mask );

/* CURRENTLY UNUSED */
#if 0
static int32_t init_prbs( uint8_t card, uint8_t preEmphasis, uint8_t serialAmplitude, uint8_t prbs_type );
#endif

static int32_t _init_mykonos_arm(mykonosDevice_t *p_mykonos);
static int32_t _perform_mcs( ad9528Device_t *p_ad9528, mykonosDevice_t *p_ad9371 );
static int32_t _init_rf_pll( mykonosDevice_t *p_mykonos);
static int32_t _init_cal( mykonosDevice_t *p_mykonos, uint32_t cal_mask );
static int32_t _init_framer_deframer( uint8_t card,
                                      ad9528Device_t *p_ad9528,
                                      mykonosDevice_t *p_mykonos,
                                      jesd_sync_result_t *p_sync_result );

static int32_t _mykonos_profile_alloc( mykonosDevice_t *p_mykonos );
static void _mykonos_profile_free( mykonosDevice_t *p_mykonos );

static int32_t _reset_and_init( rf_id_t *p_id );
static void _display_version_info( void );
static int32_t _release( rf_id_t *p_id );

static void _init_min_max_sample_rate( uint8_t card );

static int32_t _write_rx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq );
static int32_t _write_tx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq );

static int32_t _write_rx_spectrum_invert( rf_id_t *p_id, bool invert );
static int32_t _write_tx_spectrum_invert( rf_id_t *p_id, bool invert );

static int32_t _write_tx_attenuation( rf_id_t *p_id, uint16_t atten );
static int32_t _read_tx_attenuation( rf_id_t *p_id, uint16_t *p_atten );
static int32_t _read_tx_attenuation_range( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min );

static int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode );
static int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode );
static int32_t _read_rx_gain_range( rf_id_t *p_id, uint8_t *p_gain_max, uint8_t *p_gain_min );
static int32_t _write_rx_gain( rf_id_t *p_id, uint8_t gain );
static int32_t _read_rx_gain( rf_id_t *p_id, uint8_t *p_gain );

static int32_t _read_adc_resolution( uint8_t *p_adc_res );
static int32_t _read_dac_resolution( uint8_t *p_dac_res );

static int32_t _read_warp_voltage( rf_id_t *p_id, uint16_t *p_warp_voltage );
static int32_t _write_warp_voltage( rf_id_t *p_id, uint16_t warp_voltage );

static int32_t _enable_rx_chan( rf_id_t *p_id, bool enable );
static int32_t _enable_tx_chan( rf_id_t *p_id, bool enable );

static int32_t _config_tx_test_tone( rf_id_t *p_id, bool enable );
static int32_t _read_tx_test_tone_freq( rf_id_t *p_id,
                                        int32_t *p_test_freq_offset );

static void _read_min_rx_sample_rate( rf_id_t *p_id, uint32_t *p_min_rx_sample_rate );
static void _read_max_rx_sample_rate( rf_id_t *p_id, uint32_t *p_max_rx_sample_rate );
static void _read_min_tx_sample_rate( rf_id_t *p_id, uint32_t *p_min_tx_sample_rate );
static void _read_max_tx_sample_rate( rf_id_t *p_id, uint32_t *p_max_tx_sample_rate );

static int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );
static int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth );

static int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );
static int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate );

static int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );
static int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth );

static int32_t _init_gpio( rf_id_t *p_id, uint32_t output_enable );
static int32_t _write_gpio( rf_id_t *p_id, uint32_t value );
static int32_t _read_gpio( rf_id_t *p_id, uint32_t *p_value );

static int32_t _read_fpga_rf_clock( rf_id_t *p_id, uint64_t *p_freq );

static int32_t _read_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode );
static int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode );
static int32_t _run_tx_quadcal( rf_id_t *p_id );

static int32_t _read_control_output_rx_gain_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );
static int32_t _write_control_output_config( rf_id_t *p_id, uint8_t mode, uint8_t ena );
static int32_t _read_control_output_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena );

static int32_t _init_from_file( rf_id_t *p_id, FILE* p_file );

static int32_t _write_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
static int32_t _read_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

static int32_t _write_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode );
static int32_t _read_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode );

static int32_t _read_rx_hop_list( rf_id_t *p_id,
                                  uint16_t *p_num_freq,
                                  uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );
static int32_t _read_tx_hop_list( rf_id_t *p_id,
                                  uint16_t *p_num_freq,
                                  uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] );

static int32_t _config_rx_hop_list( rf_id_t *p_id,
                                    uint16_t num_freq,
                                    uint64_t freq_list[],
                                    uint16_t initial_index );
static int32_t _config_tx_hop_list( rf_id_t *p_id,
                                    uint16_t num_freq,
                                    uint64_t freq_list[],
                                    uint16_t initial_index );

static int32_t _write_next_rx_hop( rf_id_t *p_id, uint16_t freq_index );
static int32_t _write_next_tx_hop( rf_id_t *p_id, uint16_t freq_index );

static int32_t _read_curr_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop );
static int32_t _read_next_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop );

static int32_t _read_curr_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop );
static int32_t _read_next_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop );

static int32_t _rx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );
static int32_t _tx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq );

static int32_t _read_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode );
static int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode );

static int32_t _run_rx_cal( rf_id_t *p_id );

static int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask );
static int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask );

static int32_t _read_rx_cal_types_avail( rf_id_t *p_id, uint32_t *p_cal_mask );

static int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities);

static skiq_rx_hdl_t _rx_hdl_map_other(rf_id_t *p_id);
static skiq_tx_hdl_t _tx_hdl_map_other(rf_id_t *p_id);

rfic_functions_t rfic_x2_funcs =
{
    .reset_and_init           = _reset_and_init,
    .display_version_info     = _display_version_info,
    .release                  = _release,
    .write_rx_freq            = _write_rx_freq,
    .write_tx_freq            = _write_tx_freq,
    .write_rx_spectrum_invert = _write_rx_spectrum_invert,
    .write_tx_spectrum_invert = _write_tx_spectrum_invert,
    .write_tx_attenuation     = _write_tx_attenuation,
    .read_tx_attenuation      = _read_tx_attenuation,
    .read_tx_attenuation_range = _read_tx_attenuation_range,
    .write_rx_gain_mode       = _write_rx_gain_mode,
    .read_rx_gain_mode        = _read_rx_gain_mode,
    .read_rx_gain_range       = _read_rx_gain_range,
    .write_rx_gain            = _write_rx_gain,
    .read_rx_gain             = _read_rx_gain,
    .read_adc_resolution      = _read_adc_resolution,
    .read_dac_resolution      = _read_dac_resolution,
    .read_warp_voltage        = _read_warp_voltage,
    .write_warp_voltage       = _write_warp_voltage,
    .enable_rx_chan           = _enable_rx_chan,
    .enable_tx_chan           = _enable_tx_chan,
    .config_tx_test_tone      = _config_tx_test_tone,
    .read_tx_test_tone_freq   = _read_tx_test_tone_freq,
    .write_tx_test_tone_freq  = NULL, // NOTE: this could be supported if requested in the future
    .read_min_rx_sample_rate     = _read_min_rx_sample_rate,
    .read_max_rx_sample_rate     = _read_max_rx_sample_rate,
    .read_min_tx_sample_rate     = _read_min_tx_sample_rate,
    .read_max_tx_sample_rate     = _read_max_tx_sample_rate,
    .write_rx_sample_rate_and_bandwidth = _write_rx_sample_rate_and_bandwidth,
    .write_rx_sample_rate_and_bandwidth_multi = NULL,
    .write_tx_sample_rate_and_bandwidth = _write_tx_sample_rate_and_bandwidth,
    .read_rx_sample_rate      = _read_rx_sample_rate,
    .read_tx_sample_rate      = _read_tx_sample_rate,
    .read_rx_chan_bandwidth   = _read_rx_chan_bandwidth,
    .read_tx_chan_bandwidth   = _read_tx_chan_bandwidth,
    .init_gpio                = _init_gpio,
    .write_gpio               = _write_gpio,
    .read_gpio                = _read_gpio,
    .read_rx_filter_overflow  = NULL,
    .read_tx_filter_overflow  = NULL,
    .read_fpga_rf_clock       = _read_fpga_rf_clock,
    .read_tx_quadcal_mode     = _read_tx_quadcal_mode,
    .write_tx_quadcal_mode    = _write_tx_quadcal_mode,
    .run_tx_quadcal           = _run_tx_quadcal,
    .read_rx_stream_hdl_conflict = NULL,
    .is_chan_enable_xport_dependent = NULL,
    .read_control_output_rx_gain_config = _read_control_output_rx_gain_config,
    .write_control_output_config = _write_control_output_config,
    .read_control_output_config = _read_control_output_config,
    .init_from_file           = _init_from_file,
    .power_down_tx            = NULL,
    .read_warp_voltage_extended_range = NULL,
    .write_warp_voltage_extended_range = NULL,
    .enable_pin_gain_ctrl     = NULL,
    .read_hop_on_ts_gpio      = NULL,
    .write_rx_freq_tune_mode  = _write_rx_freq_tune_mode,
    .read_rx_freq_tune_mode   = _read_rx_freq_tune_mode,
    .write_tx_freq_tune_mode  = _write_tx_freq_tune_mode,
    .read_rx_freq_tune_mode   = _read_rx_freq_tune_mode,
    .config_rx_hop_list       = _config_rx_hop_list,
    .read_rx_hop_list         = _read_rx_hop_list,
    .read_tx_freq_tune_mode   = _read_tx_freq_tune_mode,
    .config_tx_hop_list       = _config_tx_hop_list,
    .read_tx_hop_list         = _read_tx_hop_list,
    .write_next_rx_hop        = _write_next_rx_hop,
    .write_next_tx_hop        = _write_next_tx_hop,
    .rx_hop                   = _rx_hop,
    .tx_hop                   = _tx_hop,
    .read_curr_rx_hop         = _read_curr_rx_hop,
    .read_next_rx_hop         = _read_next_rx_hop,
    .read_curr_tx_hop         = _read_curr_tx_hop,
    .read_next_tx_hop         = _read_next_tx_hop,
    .read_rx_cal_mode         = _read_rx_cal_mode,
    .write_rx_cal_mode        = _write_rx_cal_mode,
    .run_rx_cal               = _run_rx_cal,
    .read_rx_cal_mask         = _read_rx_cal_mask,
    .write_rx_cal_mask        = _write_rx_cal_mask,
    .read_rx_cal_types_avail  = _read_rx_cal_types_avail,
    .read_rx_enable_mode      = NULL,
    .read_tx_enable_mode      = NULL,
    .write_rx_enable_mode     = NULL,
    .write_tx_enable_mode     = NULL,
    .read_temp                = NULL,
    .read_auxadc              = NULL,
    .swap_rx_port_config      = NULL,
    .swap_tx_port_config      = NULL,
    .read_rf_capabilities     = _read_rf_capabilities,
    .write_ext_clock_select   = NULL,
    .rx_hdl_map_other         = _rx_hdl_map_other,
    .tx_hdl_map_other         = _tx_hdl_map_other,
    .read_rx_analog_filter_bandwidth    = NULL,
    .read_tx_analog_filter_bandwidth    = NULL,
    .write_rx_analog_filter_bandwidth   = NULL,
    .write_tx_analog_filter_bandwidth   = NULL,    

    /****** RFIC Rx FIR function pointers ******/
    .read_rx_fir_config       = NULL,
    .read_rx_fir_coeffs       = NULL,
    .write_rx_fir_coeffs      = NULL,
    .read_rx_fir_gain         = NULL,
    .write_rx_fir_gain        = NULL,

    /****** RFIC Tx FIR function pointers ******/
    .read_tx_fir_config       = NULL,
    .read_tx_fir_coeffs       = NULL,
    .write_tx_fir_coeffs      = NULL,
    .read_tx_fir_gain         = NULL,
    .write_tx_fir_gain        = NULL,
};


/**************************************************************************************************/
/* RX_IQRATE simplifies queries to p_profile->p_rxProfile->iqRate_kHz
 */
static inline uint32_t
RX_IQRATE( const ad9371_profile_t *p_profile )
{ return p_profile->p_rxProfile->iqRate_kHz; }

static inline uint32_t
RX_IQRATE_HZ( const ad9371_profile_t *p_profile )
{ return RX_IQRATE(p_profile) * RATE_KHZ_TO_HZ; }


/**************************************************************************************************/
/* RX_BW_HZ simplifies queries to p_profile->p_rxProfile->rfBandwidth_Hz
 */
static inline uint32_t
RX_BW_HZ( const ad9371_profile_t *p_profile )
{ return p_profile->p_rxProfile->rfBandwidth_Hz; }


/**************************************************************************************************/
/* ORX_IQRATE simplifies queries to p_profile->p_orxSettings->orxProfile->iqRate_kHz
 */
static inline uint32_t
ORX_IQRATE( const ad9371_profile_t *p_profile )
{ return p_profile->p_orxSettings->orxProfile->iqRate_kHz; }

static inline uint32_t
ORX_IQRATE_HZ( const ad9371_profile_t *p_profile )
{ return ORX_IQRATE(p_profile) * RATE_KHZ_TO_HZ; }


/**************************************************************************************************/
/* ORX_BW_HZ simplifies queries to p_profile->p_orxSettings->orxProfile->rfBandwidth_Hz
 */
static inline uint32_t
ORX_BW_HZ( const ad9371_profile_t *p_profile )
{ return p_profile->p_orxSettings->orxProfile->rfBandwidth_Hz; }


static inline uint32_t
IQRATE_HZ( rf_id_t *p_id, const ad9371_profile_t *p_profile )
{ return (p_id->hdl == ORX_HDL) ? ORX_IQRATE_HZ(p_profile) : RX_IQRATE_HZ(p_profile); }

static inline uint32_t
BW_HZ( rf_id_t *p_id, const ad9371_profile_t *p_profile )
{ return (p_id->hdl == ORX_HDL) ? ORX_BW_HZ(p_profile) : RX_BW_HZ(p_profile); }


/**************************************************************************************************/
/* TX_IQRATE simplifies queries to p_profile->p_txProfile->iqRate_kHz
 */
static inline uint32_t
TX_IQRATE( const ad9371_profile_t *p_profile )
{ return p_profile->p_txProfile->iqRate_kHz; }

static inline uint32_t
TX_IQRATE_HZ( const ad9371_profile_t *p_profile )
{ return TX_IQRATE(p_profile) * RATE_KHZ_TO_HZ; }


/**************************************************************************************************/
/* TX_BW_HZ simplifies queries to p_profile->p_txProfile->rfBandwidth_Hz
 */
static inline uint32_t
TX_BW_HZ( const ad9371_profile_t *p_profile )
{ return p_profile->p_txProfile->rfBandwidth_Hz; }

int32_t _mykonos_profile_alloc( mykonosDevice_t *p_mykonos )
{
    int32_t status = -ENOMEM;

    // RX Profile
    if( (p_mykonos->rx = malloc(sizeof(mykonosRxSettings_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->rx->rxProfile = malloc(sizeof(mykonosRxProfile_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->rx->rxProfile->rxFir = malloc(sizeof(mykonosFir_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->rx->rxProfile->rxFir->coefs = malloc(sizeof(int16_t)*AD9371_MAX_COEFFS)) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->rx->rxProfile->customAdcProfile = malloc(sizeof(uint16_t)*AD9371_MAX_COEFFS)) == NULL )
    {
        goto profile_alloc_complete;
    }

    // TX Profile
    if( (p_mykonos->tx = malloc(sizeof(mykonosTxSettings_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->tx->txProfile = malloc(sizeof(mykonosTxProfile_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->tx->txProfile->txFir = malloc(sizeof(mykonosFir_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->tx->txProfile->txFir->coefs = malloc(sizeof(int16_t)*AD9371_MAX_COEFFS)) == NULL )
    {
        goto profile_alloc_complete;
    }

    // ORx Profile
    if( (p_mykonos->obsRx = malloc(sizeof(mykonosObsRxSettings_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->obsRx->orxProfile = malloc(sizeof(mykonosRxProfile_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->obsRx->orxProfile->rxFir = malloc(sizeof(mykonosFir_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->obsRx->orxProfile->rxFir->coefs = malloc(sizeof(int16_t)*AD9371_MAX_COEFFS)) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->obsRx->orxProfile->customAdcProfile = malloc(sizeof(uint16_t)*AD9371_MAX_COEFFS)) == NULL )
    {
        goto profile_alloc_complete;
    }
    if( (p_mykonos->obsRx->customLoopbackAdcProfile = malloc(sizeof(uint16_t)*AD9371_MAX_COEFFS)) == NULL )
    {
        goto profile_alloc_complete;
    }

    // Dig Clocks
    if( (p_mykonos->clocks = malloc(sizeof(mykonosDigClocks_t))) == NULL )
    {
        goto profile_alloc_complete;
    }
    else
    {
        status = 0;
    }

profile_alloc_complete:
    if( status != 0 )
    {
        _mykonos_profile_free( p_mykonos );
    }

    return (status);
}

void _mykonos_profile_free( mykonosDevice_t *p_mykonos )
{
    // free up our previously allocated memory
    if( p_mykonos != NULL )
    {
        FREE_IF_NOT_NULL( p_mykonos->rx->rxProfile->customAdcProfile );
        FREE_IF_NOT_NULL( p_mykonos->rx->rxProfile->rxFir->coefs );
        FREE_IF_NOT_NULL( p_mykonos->rx->rxProfile->rxFir );
        FREE_IF_NOT_NULL( p_mykonos->rx->rxProfile );
        FREE_IF_NOT_NULL( p_mykonos->rx );

        FREE_IF_NOT_NULL( p_mykonos->tx->txProfile->txFir->coefs );
        FREE_IF_NOT_NULL( p_mykonos->tx->txProfile->txFir );
        FREE_IF_NOT_NULL( p_mykonos->tx->txProfile );
        FREE_IF_NOT_NULL( p_mykonos->tx );

        FREE_IF_NOT_NULL( p_mykonos->obsRx->customLoopbackAdcProfile );
        FREE_IF_NOT_NULL( p_mykonos->obsRx->orxProfile->customAdcProfile );
        FREE_IF_NOT_NULL( p_mykonos->obsRx->orxProfile->rxFir->coefs );
        FREE_IF_NOT_NULL( p_mykonos->obsRx->orxProfile->rxFir );
        FREE_IF_NOT_NULL( p_mykonos->obsRx->orxProfile );
        FREE_IF_NOT_NULL( p_mykonos->obsRx );

        FREE_IF_NOT_NULL( p_mykonos->clocks );
    }
}


int32_t _init_mykonos_arm(mykonosDevice_t *p_mykonos)
{
    int32_t status=0;
    uint8_t major,minor,rc=0;
    mykonosBuild_t buildType;

    if( MYKONOS_initArm(p_mykonos) != 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Failed to initialize RFIC micro\n");
        status = -1;
    }
    _skiq_log(SKIQ_LOG_INFO, "Loading RFIC micro\n");
    if( MYKONOS_loadArmFromBinary(p_mykonos,
                                  Mykonos_M3_bin,
                                  Mykonos_M3_bin_len) != 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Failed to load RFIC micro\n");
        status = -2;
    }

    if( status==0 )
    {
        _skiq_log(SKIQ_LOG_INFO, "Loaded RFIC micro successfully, verifying version\n");
        if( MYKONOS_getArmVersion(p_mykonos, &major, &minor, &rc, &buildType) == 0 )
        {
            _skiq_log(SKIQ_LOG_INFO, "RFIC micro firmware %u.%u.%u loaded successfully!\n",
                      major, minor, rc);
        }
    }

    return (status);
}

int32_t _perform_mcs( ad9528Device_t *p_ad9528, mykonosDevice_t *p_ad9371 )
{
    int32_t status=0;
    uint8_t mcsStatus=0;
    uint8_t i=0;

    if( MYKONOS_enableMultichipSync(p_ad9371, 1, &mcsStatus) == 0 )
    {
        // pulse sysref 4xs
        for( i=0; i<NUM_SYSREF_PULSES; i++ )
        {
            AD9528_requestSysref( p_ad9528, 1 );
        }
        // now perform MCS again...make sure the status is ok
        if( MYKONOS_enableMultichipSync(p_ad9371, 0, &mcsStatus) == 0 )
        {
            _skiq_log(SKIQ_LOG_INFO, "MCS procedure complete, status 0x%x\n", mcsStatus);
            if( mcsStatus != MCS_STATUS_OK )
            {
                status = -3;
                _skiq_log(SKIQ_LOG_ERROR, "MCS failed\n");
            }
        }
        else
        {
            status=-2;
        }
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "Initial MCS failed!\n");
        status=-1;
    }

    return (status);
}

int32_t _init_rf_pll( mykonosDevice_t *p_mykonos)
{
    int32_t status=0;
    
    // set the RX, TX, and Sniffer PLL to default values
    if( ((MYKONOS_setRfPllFrequency(p_mykonos, RX_PLL, p_mykonos->rx->rxPllLoFrequency_Hz)) != 0) ||
        ((MYKONOS_setRfPllFrequency(p_mykonos, TX_PLL, p_mykonos->tx->txPllLoFrequency_Hz)) != 0) ||
        ((MYKONOS_setRfPllFrequency(p_mykonos, SNIFFER_PLL, p_mykonos->obsRx->snifferPllLoFrequency_Hz)) != 0) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to configure PLL to default value\n");
        status=-1;
        hal_critical_exit(status);
    }
    
    _skiq_log(SKIQ_LOG_INFO, "Default RF PLL configuration complete!\n");

    return (status);
}

int32_t _init_cal( mykonosDevice_t *p_mykonos, uint32_t cal_mask )
{
    int32_t status=0;
    uint8_t errorFlag=0;
    uint8_t errorCode=0;
    
    _skiq_log(SKIQ_LOG_INFO, "Performing RFIC calibration 0x%" PRIx32 "\n", cal_mask);
    if( (MYKONOS_runInitCals(p_mykonos, cal_mask) != 0) )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Failed to run initial calibration\n");
        status=-ENODEV;
        hal_critical_exit(status);
    }
    if( (status=MYKONOS_waitInitCals(p_mykonos, 100000, &errorFlag, &errorCode)) != 0 )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Initial calibration did not complete successfully (flag=0x%x, code=0x%x) (retVal=%d)\n",
                  errorFlag, errorCode, status);
        status=-ETIMEDOUT;
        hal_critical_exit(status);
    }

    _skiq_log(SKIQ_LOG_INFO, "Initial RFIC calibration completed successfully!\n");

    return (status);
}

int32_t reset_jesd_x2( uint8_t card,
                       uint8_t rx_preEmphasis,
                       uint8_t rx_serialAmplitude,
                       uint8_t tx_diffctrl,
                       uint8_t tx_precursor,
                       jesd_sync_result_t *p_sync_result )
{
    int32_t status=-1;

    ad9528Device_t *p_ad9528 = &(_clockDevice[card]);
    mykonosDevice_t *p_mykonos =&( _mykDevice[card]);

    // TODO: if we change the clock, we also need to reset the PHY (fpga_jesd_ctrl_phy_reset)

    p_mykonos->rx->framer->preEmphasis = rx_preEmphasis;
    p_mykonos->rx->framer->serializerAmplitude = rx_serialAmplitude;

    _skiq_log(SKIQ_LOG_DEBUG, "JESD RX pre-emphasis is %u, amplitude %u\n",
              p_mykonos->rx->framer->preEmphasis,
              p_mykonos->rx->framer->serializerAmplitude);

    _skiq_log(SKIQ_LOG_DEBUG, "JESD TX diffctrl is %u, precursor %u\n",
              tx_diffctrl, tx_precursor);

    if( MYKONOS_radioOff( p_mykonos ) == 0 )
    {
        // disable the sysref to the framers / deframer
        if( (MYKONOS_enableSysrefToRxFramer( p_mykonos, 0 ) == 0) &&
            (MYKONOS_enableSysrefToObsRxFramer( p_mykonos, 0 ) == 0) &&
            (MYKONOS_enableSysrefToDeframer( p_mykonos, 0 ) == 0) )
        {

            fpga_jesd_ctrl_core_reset( card );
            if( MYKONOS_setupSerializers( p_mykonos ) == 0 )
            {
                _skiq_log(SKIQ_LOG_DEBUG, "Setup serializer successfully\n");
            }
            else
            {
                _skiq_log(SKIQ_LOG_ERROR, "Failed to setup serializer\n");
            }

            if( MYKONOS_setupDeserializers( p_mykonos ) == 0 )
            {
                _skiq_log(SKIQ_LOG_DEBUG, "Setup deserializer successfully\n");
            }
            else
            {
                _skiq_log(SKIQ_LOG_ERROR, "Failed to setup deserializer\n");
            }

            if( fpga_jesd_ctrl_tx_config(card, tx_diffctrl, tx_precursor) == 0 )
            {
                _skiq_log(SKIQ_LOG_DEBUG, "Setup TX JESD parameters successfully\n");
            }
            else
            {
                _skiq_log(SKIQ_LOG_ERROR, "Unable to configure TX JESD parameters\n");
            }

            status = _init_framer_deframer( card,
                                            p_ad9528,
                                            p_mykonos,
                                            p_sync_result );
        }
    }

    return (status);
}

int32_t reset_jesd_x2_defaults( uint8_t card,
                                jesd_sync_result_t *p_sync_result )
{
    return reset_jesd_x2( card,
                          DEFAULT_JESD_RX_PREEMPHASIS,
                          DEFAULT_JESD_RX_SERIALIZER_AMPLITUDE,
                          DEFAULT_JESD_TX_DIFFCTRL,
                          DEFAULT_JESD_TX_PRECURSOR,
                          p_sync_result );
}


/* CURRENTLY UNUSED */
#if 0
// Note: this function may not be working (or FPGA PRBS isn't working)
int32_t init_prbs( uint8_t card, uint8_t preEmphasis, uint8_t serialAmplitude, uint8_t prbs_type )
{
    mykonosPrbsOrder_t prbs = (mykonosPrbsOrder_t)(prbs_type);
    uint8_t i=0;

    ad9528Device_t *p_ad9528 = &(_clockDevice[card]);
    mykonosDevice_t *p_mykonos =&( _mykDevice[card]);

    p_mykonos->rx->framer->preEmphasis = preEmphasis;
    p_mykonos->rx->framer->serializerAmplitude = serialAmplitude;

    _skiq_log(SKIQ_LOG_DEBUG, "JESD RX pre-emphasis is %u, amplitude %u\n",
              p_mykonos->rx->framer->preEmphasis,
              p_mykonos->rx->framer->serializerAmplitude);

    MYKONOS_radioOff( p_mykonos );

    // disable the sysref to the framers / deframer
    MYKONOS_enableSysrefToRxFramer( p_mykonos, 0 );
    MYKONOS_enableSysrefToObsRxFramer( p_mykonos, 0 );
    MYKONOS_enableSysrefToDeframer( p_mykonos, 0 );

    fpga_jesd_ctrl_core_reset( card );
    if( MYKONOS_setupSerializers( p_mykonos ) == 0 )
    {
        _skiq_log(SKIQ_LOG_DEBUG, "setup serializer successfully\n");
    }
    else
    {
        _skiq_log(SKIQ_LOG_DEBUG, "failed to setup serializer\n");
    }

    // set the sysref to the framers / deframer
    MYKONOS_enableSysrefToRxFramer( p_mykonos, 1 );
    MYKONOS_enableSysrefToObsRxFramer( p_mykonos, 1 );
    MYKONOS_enableSysrefToDeframer( p_mykonos, 1 ); // enable this for TX

    // pulse sysref 4xs
    for( i=0; i<NUM_SYSREF_PULSES; i++ )
    {
        AD9528_requestSysref( p_ad9528, 1 );
    }

    if( (MYKONOS_enableRxFramerPrbs(p_mykonos, prbs, 1) == 0) &&
        (MYKONOS_enableRxFramerPrbs(p_mykonos, prbs, 1) == 0) )
    {
        _skiq_log(SKIQ_LOG_DEBUG, "Successfully enabled PRBS\n");
    }
    else
    {
        _skiq_log(SKIQ_LOG_DEBUG, "failed to enable PRBS\n");
    }

    return (0);
}
#endif

int32_t _init_framer_deframer( uint8_t card,
                               ad9528Device_t *p_ad9528,
                               mykonosDevice_t *p_mykonos,
                               jesd_sync_result_t *p_sync_result )
{
    int32_t status=-1;
    uint8_t i=0;
    uint8_t rx_sync;
    uint8_t obs_sync;
    uint8_t tx_sync;
    bool sync_ok=false;
    uint16_t mismatch=0;
    uint32_t jesd_status=0;
    uint32_t jesd_cnt=0;
    uint8_t num_retries=0;
    jesd_unsync_t unsync_a;
    jesd_unsync_t unsync_b;
    uint32_t unsync;
    
    for( num_retries=0; (num_retries<MAX_NUM_JESD_RETRIES) && (sync_ok==false); num_retries++ )
    {
        // if this is the 2nd time through the loop, make sure to reset the FPGA
        if( num_retries > 0 )
        {
            fpga_jesd_ctrl_core_reset( card );
        }       

        if( MYKONOS_resetDeframer( p_mykonos ) == 0 )
        {
            // set the sysref to the framers / deframer
            if( (MYKONOS_enableSysrefToRxFramer( p_mykonos, 1 ) == 0) &&
                (MYKONOS_enableSysrefToObsRxFramer( p_mykonos, 1 ) == 0) &&
                (MYKONOS_enableSysrefToDeframer( p_mykonos, 1 ) == 0) )
            {            
                // pulse sysref 4xs
                for( i=0; i<NUM_SYSREF_PULSES; i++ )
                {
                    AD9528_requestSysref( p_ad9528, 1 );
                }
                
#if (defined _IS_VPX1)
                fpga_jesd_ctrl_phy_reset( p_mykonos->spiSettings->id );
                fpga_jesd_ctrl_core_reset( p_mykonos->spiSettings->id );
#endif

                if( (status=MYKONOS_radioOn(p_mykonos)) == 0 )
                {
                    _skiq_log(SKIQ_LOG_INFO, "Radio on successfully!\n");
                    if( (MYKONOS_readRxFramerStatus( p_mykonos, &rx_sync ) == 0) &&
                        (MYKONOS_readOrxFramerStatus( p_mykonos, &obs_sync ) == 0) &&
                        (MYKONOS_readDeframerStatus( p_mykonos, &tx_sync ) == 0) )
                    {
                        _skiq_log(SKIQ_LOG_DEBUG, "rx framer 0x%x obs framer 0x%x tx deframer 0x%x\n",
                                  rx_sync, obs_sync, tx_sync );
                        MYKONOS_jesd204bIlasCheck( p_mykonos, &mismatch );
                        _skiq_log(SKIQ_LOG_DEBUG, "ILAS check 0x%x\n", mismatch);
                        
                        if( ((rx_sync & RX_SYNC_STATUS_MASK) == RX_SYNC_STATUS) &&
                            ((obs_sync & RX_SYNC_STATUS_MASK) == RX_SYNC_STATUS) &&
                            ((tx_sync & TX_SYNC_STATUS_MASK) == TX_SYNC_STATUS) )
                        {
                            if( fpga_jesd_sync_status(p_mykonos->spiSettings->id,
                                                      true /* TX */,
                                                      false /* RFIC B */) == 0 )
                            {
                                skiq_debug("JESD sync successful (retries=%u) (card=%u)\n",
                                   num_retries, card);

                                // wait 1 ms and make sure the JESD errors
                                hal_nanosleep(1*MILLISEC);
                                fpga_jesd_read_unsync_counts(card,
                                                             &unsync_a,
                                                             &unsync_b,
                                                             &unsync);
                                // TX must be <2, everything else should be 0
                                if( (unsync_a.rx_unsync == 0) &&
                                    (unsync_a.orx_unsync == 0) &&
                                    (unsync_a.tx_unsync < 2) )
                                {
                                    sync_ok = true;
                                    if( p_sync_result != NULL )
                                    {
                                        // populate our JESD result structure with more info
                                        
                                        // FPGA registers
                                        p_sync_result->jesd_status_imm = jesd_status;
                                        p_sync_result->jesd_unsync_imm = jesd_cnt;
                                        
                                        // A framer/deframer
                                        p_sync_result->framer_a_rx = rx_sync;
                                        p_sync_result->framer_a_orx = obs_sync;
                                        p_sync_result->deframer_a = tx_sync;
                                        
                                        p_sync_result->num_retries = num_retries;
                                    }
                                }
                                else
                                {
                                    if( num_retries < MAX_NUM_JESD_RETRIES )
                                    {
                                        skiq_debug("Detected errors in JESD link, retrying (retries=%u) (card=%u)\n",
                                                   num_retries, card);
                                        continue;
                                    }
                                    else
                                    {
                                        hal_critical_exit(-1);
                                        status = -1;
                                    }
                                } // end unsync count check
                            } 
                            else
                            {
                                _skiq_log(SKIQ_LOG_DEBUG, "FPGA indicates JESD sync status invalid\n");
                                continue;
                            } // end FPGA jesd sync status
                        }
                        else
                        {
                            // read FPGA registers for some additional details
                            sidekiq_fpga_reg_read(card, FPGA_REG_JESD_STATUS, &jesd_status);
                            sidekiq_fpga_reg_read(card, FPGA_REG_JESD_UNSYNC_COUNTERS, &jesd_cnt);
                            _skiq_log(SKIQ_LOG_ERROR, "(de)framer sync failed, FPGA unsync=0x%x, status=0x%x\n", 
                                      jesd_cnt, jesd_status );
                            if( num_retries < MAX_NUM_JESD_RETRIES )
                            {
                                skiq_debug("Retrying JESD sync (retries=%u) (card=%u)", num_retries, card);
                                continue;
                            }
                            else
                            {
                                hal_critical_exit(-1);
                            }
                        } // end checking sync status
                    } // end reading framer status
                }
                else
                {
                    _skiq_log(SKIQ_LOG_ERROR, "Failed to turn on the radio\n");
                } // end turning on radio
            } // end enable sysref to framer
        } // end reset deframer
    } // end retry loop
    if( sync_ok == true )
    {
        status = 0;
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to obtain sync with radio\n");
        status = -1;
    }

    return (status);
}

void _init_min_max_sample_rate( uint8_t card )
{
    uint8_t i=0;
    uint64_t fpga_min_freq=0;
    uint64_t fpga_max_freq=0;
    uint32_t actual_sample_rate=0;
    ad9528pll1Settings_t pll1;
    ad9528pll2Settings_t pll2;
    ad9528outputSettings_t output;
    ad9528sysrefSettings_t sysref;
    clk_div_t fpga_div;
    uint32_t dev_clock;
    clk_div_t dev_clock_div;
    uint32_t rffc_freq;
    uint64_t fpga_gbt_clock;

    fpga_jesd_qpll_freq_range( card, &fpga_min_freq, &fpga_max_freq );

    if ( _p_min_rx_profile == NULL && _p_min_orx_profile == NULL && _p_min_tx_profile == NULL &&
         _p_max_rx_profile == NULL && _p_max_orx_profile == NULL && _p_max_tx_profile == NULL &&
         _p_max_dual_lane_orx_profile == NULL )
    {
        // let's find the best profile match
        for ( i = 0; i < AD9371_NR_PROFILES; i++ )
        {
            debug_print("Profile #%u:\n", i);
            debug_print("  RX1/RX2: sample rate %u ksps, RF bandwidth %u Hz\n",
                        RX_IQRATE(&ad9371_profiles[i]), RX_BW_HZ(&ad9371_profiles[i]));
            debug_print("      ORX: sample rate %u ksps, RF bandwidth %u Hz\n",
                        ORX_IQRATE(&ad9371_profiles[i]), ORX_BW_HZ(&ad9371_profiles[i]));

            // see what the clock settings are
            if( calc_ad9528_for_sample_rate( RX_IQRATE(&ad9371_profiles[i])*RATE_KHZ_TO_HZ,
                                             &actual_sample_rate,
                                             &pll1,
                                             &pll2,
                                             &output,
                                             &sysref,
                                             &fpga_div,
                                             &dev_clock,
                                             &dev_clock_div,
                                             &rffc_freq,
                                             fpga_min_freq,
                                             fpga_max_freq,
                                             &fpga_gbt_clock,
                                             _x2_clk_sel,
                                             skiq_x2) == 0 )
            {
                /**********************************************************************************/
                /* Find the minimum RX1/RX2 profile */
                if( _p_min_rx_profile == NULL )
                {
                    _p_min_rx_profile = &ad9371_profiles[i];
                }
                else if ( RX_IQRATE(&ad9371_profiles[i]) < RX_IQRATE(_p_min_rx_profile) )
                {
                    _p_min_rx_profile = &ad9371_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if ( ( RX_IQRATE(&ad9371_profiles[i]) == RX_IQRATE(_p_min_rx_profile) ) &&
                          ( RX_BW_HZ(&ad9371_profiles[i]) < RX_BW_HZ(_p_min_rx_profile) ) )
                {
                    _p_min_rx_profile = &ad9371_profiles[i];
                }

                /**********************************************************************************/
                /* Find the maximum RX1/RX2 profile */
                if( _p_max_rx_profile == NULL )
                {
                    _p_max_rx_profile = &ad9371_profiles[i];
                    _max_rx_profile_index = i;
                }
                else if( RX_IQRATE(&ad9371_profiles[i]) > RX_IQRATE(_p_max_rx_profile) )
                {
                    _p_max_rx_profile = &ad9371_profiles[i];
                    _max_rx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( RX_IQRATE(&ad9371_profiles[i]) == RX_IQRATE(_p_max_rx_profile) ) &&
                         ( RX_BW_HZ(&ad9371_profiles[i]) > RX_BW_HZ(_p_max_rx_profile) ) )
                {
                    _p_max_rx_profile = &ad9371_profiles[i];
                    _max_rx_profile_index = i;
                }

                /**********************************************************************************/
                /* Find the minimum TX1/TX2 profile */
                if( _p_min_tx_profile == NULL )
                {
                    _p_min_tx_profile = &ad9371_profiles[i];
                }
                else if ( TX_IQRATE(&ad9371_profiles[i]) < TX_IQRATE(_p_min_tx_profile) )
                {
                    _p_min_tx_profile = &ad9371_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if ( ( TX_IQRATE(&ad9371_profiles[i]) == TX_IQRATE(_p_min_tx_profile) ) &&
                          ( TX_BW_HZ(&ad9371_profiles[i]) < TX_BW_HZ(_p_min_tx_profile) ) )
                {
                    _p_min_tx_profile = &ad9371_profiles[i];
                }

                /**********************************************************************************/
                /* Find the maximum TX1/TX2 profile */
                if( _p_max_tx_profile == NULL )
                {
                    _p_max_tx_profile = &ad9371_profiles[i];
                    _max_tx_profile_index = i;
                }
                else if( TX_IQRATE(&ad9371_profiles[i]) > TX_IQRATE(_p_max_tx_profile) )
                {
                    _p_max_tx_profile = &ad9371_profiles[i];
                    _max_tx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( TX_IQRATE(&ad9371_profiles[i]) == TX_IQRATE(_p_max_tx_profile) ) &&
                         ( TX_BW_HZ(&ad9371_profiles[i]) > TX_BW_HZ(_p_max_tx_profile) ) )
                {
                    _p_max_tx_profile = &ad9371_profiles[i];
                    _max_tx_profile_index = i;
                }

                /**********************************************************************************/
                /* Find the minimum ORX profile */
                if( _p_min_orx_profile == NULL )
                {
                    _p_min_orx_profile = &ad9371_profiles[i];
                }
                else if ( ORX_IQRATE(&ad9371_profiles[i]) < ORX_IQRATE(_p_min_orx_profile) )
                {
                    _p_min_orx_profile = &ad9371_profiles[i];
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if ( ( ORX_IQRATE(&ad9371_profiles[i]) == ORX_IQRATE(_p_min_orx_profile) ) &&
                          ( ORX_BW_HZ(&ad9371_profiles[i]) < ORX_BW_HZ(_p_min_orx_profile) ) )
                {
                    _p_min_orx_profile = &ad9371_profiles[i];
                }

                /**********************************************************************************/
                /* Find the maximum ORX profile */
                if( _p_max_orx_profile == NULL )
                {
                    _p_max_orx_profile = &ad9371_profiles[i];
                    _max_orx_profile_index = i;
                }
                else if ( ( ORX_IQRATE(&ad9371_profiles[i]) <= ORX_DUAL_LANE_RATE_THRESHOLD_KHZ ) &&
                          ( ORX_IQRATE(&ad9371_profiles[i]) > ORX_IQRATE(_p_max_orx_profile) ) )
                {
                    _p_max_orx_profile = &ad9371_profiles[i];
                    _max_orx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( ORX_IQRATE(&ad9371_profiles[i]) <= ORX_DUAL_LANE_RATE_THRESHOLD_KHZ ) &&
                         ( ORX_IQRATE(&ad9371_profiles[i]) == ORX_IQRATE(_p_max_orx_profile) ) &&
                         ( ORX_BW_HZ(&ad9371_profiles[i]) > ORX_BW_HZ(_p_max_orx_profile) ) )
                {
                    _p_max_orx_profile = &ad9371_profiles[i];
                    _max_orx_profile_index = i;
                }

                /**********************************************************************************/
                /* Find the maximum dual-lane ORX profile */
                if( _p_max_dual_lane_orx_profile == NULL )
                {
                    _p_max_dual_lane_orx_profile = &ad9371_profiles[i];
                    _max_dual_lane_orx_profile_index = i;
                }
                else if ( ORX_IQRATE(&ad9371_profiles[i]) > ORX_IQRATE(_p_max_dual_lane_orx_profile) )
                {
                    _p_max_dual_lane_orx_profile = &ad9371_profiles[i];
                    _max_dual_lane_orx_profile_index = i;
                }
                /* when sample rate matches, consider RF bandwidth as a tie breaker */
                else if( ( ORX_IQRATE(&ad9371_profiles[i]) == ORX_IQRATE(_p_max_dual_lane_orx_profile) ) &&
                         ( ORX_BW_HZ(&ad9371_profiles[i]) > ORX_BW_HZ(_p_max_dual_lane_orx_profile) ) )
                {
                    _p_max_dual_lane_orx_profile = &ad9371_profiles[i];
                    _max_dual_lane_orx_profile_index = i;
                }
            }
        }
        debug_print("Profile #%" PRIu32 " is maximum RX profile\n", _max_rx_profile_index);
        debug_print("Profile #%" PRIu32 " is maximum TX profile\n", _max_tx_profile_index);
        debug_print("Profile #%" PRIu32 " is maximum ORX profile\n", _max_orx_profile_index);
        debug_print("Profile #%" PRIu32 " is maximum dual-lane ORX profile\n", _max_dual_lane_orx_profile_index);
    }
}

int32_t _reset_ad9371( rf_id_t *p_id )
{
    int32_t status=0;
    uint32_t val=0;
    uint8_t scratch_val=0x5A;
    uint8_t verify_val=0;
    
    _skiq_log(SKIQ_LOG_DEBUG, "Resetting AD9371\n");
    
    // assert reset
    sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
    BF_SET(val, 0, RESET_N_OFFSET, RESET_N_LEN );
    sidekiq_fpga_reg_write( p_id->card, FPGA_REG_FMC_CTRL, val );
    sidekiq_fpga_reg_verify( p_id->card, FPGA_REG_FMC_CTRL, val );

    // deassert reset 
    BF_SET(val, 1, RESET_N_OFFSET, RESET_N_LEN );
    sidekiq_fpga_reg_write( p_id->card, FPGA_REG_FMC_CTRL, val );
    sidekiq_fpga_reg_verify( p_id->card, FPGA_REG_FMC_CTRL, val );

    // configure the SPI interface
    MYKONOS_setSpiSettings( &_mykDevice[p_id->card] );

    // write/read to scratch register and verify
    hal_rfic_write_reg( p_id->card, RFIC_SPI_CS_AD9371, AD9371_SCRATCH_REG, scratch_val );
    hal_rfic_read_reg( p_id->card, RFIC_SPI_CS_AD9371, AD9371_SCRATCH_REG, &verify_val );
    if( verify_val != scratch_val )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Reset AD9371 interface failed, read 0x%x, expected 0x%x\n",
                  verify_val, scratch_val);
        status = -ENODEV;
    }

    return (status);
}

uint8_t _find_freq_table_index( uint64_t lo_freq )
{
    uint8_t i = 0, idx = MAX_FREQ_TABLE_ENTRIES;
    bool bFound = false;
    
    for( i=0; (i<MAX_FREQ_TABLE_ENTRIES) && (bFound==false); i++ )
    {
        if( (lo_freq >= _freqTable[i].startFreq) &&
            (lo_freq <= _freqTable[i].stopFreq) )
        {
            bFound = true;
            idx = i;
        }
    }

    if( bFound == false )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Unable to locate frequency table offset for %" PRIu64 "\n", lo_freq);
        idx = MAX_FREQ_TABLE_ENTRIES;
    }
    
    return idx;
}


int32_t _reset_ad9528( rf_id_t *p_id )
{
    int32_t status=0;
    uint32_t val=0;
    uint8_t scratch_val=0x5A;
    uint8_t verify_val=0;

    _skiq_log(SKIQ_LOG_DEBUG, "Resetting AD9528\n");
    
    // assert reset
    sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
    BF_SET(val, 0, AD9528_SPI_RESET_N_OFFSET, AD9528_SPI_RESET_N_LEN );
    sidekiq_fpga_reg_write( p_id->card, FPGA_REG_FMC_CTRL, val );
    sidekiq_fpga_reg_verify( p_id->card, FPGA_REG_FMC_CTRL, val );

    // deassert reset 
    BF_SET(val, 1, AD9528_SPI_RESET_N_OFFSET, AD9528_SPI_RESET_N_LEN );
    sidekiq_fpga_reg_write( p_id->card, FPGA_REG_FMC_CTRL, val );
    sidekiq_fpga_reg_verify( p_id->card, FPGA_REG_FMC_CTRL, val );

    // configure the SPI interface
    AD9528_setSpiSettings( &_clockDevice[p_id->card] );

    // write/read to scratch register and verify
    hal_rfic_write_reg( p_id->card, RFIC_SPI_CS_AD9528, AD9528_SCRATCH_REG, scratch_val );
    hal_rfic_read_reg( p_id->card, RFIC_SPI_CS_AD9528, AD9528_SCRATCH_REG, &verify_val );
    if( verify_val != scratch_val )
    {
        _skiq_log(SKIQ_LOG_ERROR, "Reset AD9528 interface failed, read 0x%x, expected 0x%x\n",
                  verify_val, scratch_val);
        status = -ENODEV;
    }

    return (status);
}

int32_t _reset_and_init( rf_id_t *p_id )
{
    int32_t status = 0;
    skiq_ref_clock_select_t ref_clock;
    uint32_t val=0;
    uint8_t i=0;
    skiq_hw_rev_t hw_rev;

    // make sure to reset our dev clock variable
    _dev_clock[p_id->card] = 0;

    // init the hw_rev
    hw_rev = _skiq_get_hw_rev( p_id->card );

    // v3.10.0 of the FPGA makes sure that the 10M enable line is always tied off to
    // the correct value, so we don't need to worry about controlling that line if
    // v3.10.0 is loaded.  Additionally, for rev C, software control of the 10M line
    // must be completely removed and we must only use v3.10.0 FPGA, so if we detect
    // a rev C board with an older FPGA version, don't let init proceed any further
    if ( !(_skiq_meets_FPGA_VERSION(p_id->card, FPGA_VERS_X2_REV_C)) )
    {
        // for X2 rev C, we must use >=3.10.0 FPGA
        if( hw_rev == hw_rev_b )
        {
            // The very first thing we need to do is make sure that the 10M is enabled always
            sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
            RBF_SET( val, 1, TEN_MEG_EN );
            sidekiq_fpga_reg_write_and_verify( p_id->card, FPGA_REG_FMC_CTRL, val );
        }
        else
        {
            skiq_error("FPGA does not meet minimum version requirement (%s) for hardware revision "
                       "C and later (card=%" PRIu8 ")\n", FPGA_VERS_X2_REV_C_CSTR, p_id->card);
            status = -ENODEV;
            return (status);
        }
    }

    // init min/max sample rate
    _init_min_max_sample_rate(p_id->card);

    /* zero out the rate_config arrays for the specified RF ID */
    rfic_clear_rate_config( p_id );

    /////////////////////
    // alloc mem and copy default params
    _clockDevice[p_id->card].spiSettings =
        (ad9528SpiSettings_t*)(malloc(sizeof(ad9528SpiSettings_t)));
    memcpy( (void*)(_clockDevice[p_id->card].spiSettings),
            (void*)(&_x2_clockSpiSettings),
            sizeof(ad9528SpiSettings_t) );

    _clockDevice[p_id->card].pll1Settings =
        (ad9528pll1Settings_t*)(malloc(sizeof(ad9528pll1Settings_t)));

    _clockDevice[p_id->card].pll2Settings =
        (ad9528pll2Settings_t*)(malloc(sizeof(ad9528pll2Settings_t)));

    _clockDevice[p_id->card].outputSettings =
        (ad9528outputSettings_t*)(malloc(sizeof(ad9528outputSettings_t)));
    memcpy( (void*)(_clockDevice[p_id->card].outputSettings),
            (void*)(&_x2_clockOutputSettings),
            sizeof(ad9528outputSettings_t) );

    _clockDevice[p_id->card].sysrefSettings =
        (ad9528sysrefSettings_t*)(malloc(sizeof(ad9528sysrefSettings_t)));
    memcpy( (void*)(_clockDevice[p_id->card].sysrefSettings),
            (void*)(&_x2_clockSysrefSettings),
            sizeof(ad9528sysrefSettings_t) );

    _mykDevice[p_id->card].spiSettings =
        (mykonosSpiSettings_t*)(malloc(sizeof(mykonosSpiSettings_t)));
    memcpy( (void*)(_mykDevice[p_id->card].spiSettings),
            (void*)(ad9371_default_profile.spiSettings),
            sizeof(mykonosSpiSettings_t) );

    _mykDevice[p_id->card].auxIo =
        (mykonosAuxIo_t*)(malloc(sizeof(mykonosAuxIo_t)));
    memcpy( (void*)(_mykDevice[p_id->card].auxIo),
            (void*)(ad9371_default_profile.auxIo),
            sizeof(mykonosAuxIo_t) );

    _mykDevice[p_id->card].clocks =
        (mykonosDigClocks_t*)(malloc(sizeof(mykonosDigClocks_t)));
    memcpy( (void*)(_mykDevice[p_id->card].clocks),
            (void*)(ad9371_default_profile.clocks),
            sizeof(mykonosDigClocks_t) );

    if( (status=_mykonos_profile_alloc( &_mykDevice[p_id->card] )) != 0 )
    {
        skiq_error("Unable to allocate memory for RFIC configuration on card %u\n", p_id->card);
        return (status);
    }
    memcpy( (void*)(_mykDevice[p_id->card].rx),
            (void*)(ad9371_default_profile.rx),
            sizeof(mykonosRxSettings_t) );
    memcpy( (void*)(_mykDevice[p_id->card].tx),
            (void*)(ad9371_default_profile.tx),
            sizeof(mykonosTxSettings_t) );
    memcpy( (void*)(_mykDevice[p_id->card].obsRx),
            (void*)(ad9371_default_profile.obsRx),
            sizeof(mykonosObsRxSettings_t) );
    //////////////

    //////////////
    // default TX to disabled, manual quad cal, and test tone disabled
    _config_tx_test_tone(p_id, false);
    for( i=0; i<skiq_tx_hdl_end; i++ )
    {
        _tx_quadcal[p_id->card][i] = skiq_tx_quadcal_mode_manual;
    }
    //////////////

    ad9528_init_struct( _clockDevice[p_id->card].pll1Settings,
                        _clockDevice[p_id->card].pll2Settings );

    // init func ptrs, other misc params
    _clockDevice[p_id->card].spiSettings->chipSelectIndex = RFIC_SPI_CS_AD9528;
    _clockDevice[p_id->card].spiSettings->id = p_id->card;
    _clockDevice[p_id->card].id = p_id->card;
    _clockDevice[p_id->card].CMB_hardReset = NULL; // we don't need a reset here, we're already toggling reset line ourselves
    _clockDevice[p_id->card].CMB_regRead = NULL;
    _clockDevice[p_id->card].CMB_regWrite = NULL;
    _clockDevice[p_id->card].spiSettings->reg_read = hal_rfic_read_reg;
    _clockDevice[p_id->card].spiSettings->reg_write = hal_rfic_write_reg;

    // setup the clock based on internal or external reference
    skiq_read_ref_clock_select( p_id->card, &ref_clock );
    if( (ref_clock == skiq_ref_clock_external) ||
        (ref_clock == skiq_ref_clock_host) )
    {
        // now set the ref clock to A on the 9528 (turn off B)
        _clockDevice[p_id->card].pll1Settings->refA_bufferCtrl = DISABLED;
        _clockDevice[p_id->card].pll1Settings->refB_bufferCtrl = DIFFERENTIAL;
        _clockDevice[p_id->card].ref_sel_mode = ref_sel_mode_refB;
        if( ref_clock == skiq_ref_clock_external )
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Configuring reference clock for external\n");
            _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = 10000000;
            _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = 10000000;
        }
        else
        {
            _skiq_log(SKIQ_LOG_DEBUG, "Configuring reference clock for host\n");
            _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = 40000000;
            _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = 40000000;
        }
        _skiq_log(SKIQ_LOG_DEBUG, "Configuring ref frequency to %" PRIu32 "\n",
                  _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz);
    }
    else
    {
        if( ref_clock != skiq_ref_clock_internal )
        {
            _skiq_log(SKIQ_LOG_ERROR, "invalid reference clock detected, assuming internal\n");
        }
        _skiq_log(SKIQ_LOG_DEBUG, "Configuring reference clock for internal\n");
        // now set the ref clock to A on the 9528 (turn off B)
        _clockDevice[p_id->card].pll1Settings->refA_bufferCtrl = DIFFERENTIAL;
        _clockDevice[p_id->card].pll1Settings->refB_bufferCtrl = DISABLED;
        _clockDevice[p_id->card].ref_sel_mode = ref_sel_mode_refA;
        // rev B used 10M
        if( hw_rev == hw_rev_b )
        {
            _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = 10000000;
            _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = 10000000;
        }
        else
        {
            _clockDevice[p_id->card].pll1Settings->refA_Frequency_Hz = 40000000;
            _clockDevice[p_id->card].pll1Settings->refB_Frequency_Hz = 40000000;
        }
    }

    _mykDevice[p_id->card].spiSettings->chipSelectIndex = RFIC_SPI_CS_AD9371;
    _mykDevice[p_id->card].spiSettings->id = p_id->card;
    _mykDevice[p_id->card].CMB_hardReset = NULL; // we don't need a reset here, we're already toggling reset line ourselves
    _mykDevice[p_id->card].CMB_regRead = NULL;
    _mykDevice[p_id->card].CMB_regWrite = NULL;
    _mykDevice[p_id->card].spiSettings->reg_read = hal_rfic_read_reg;
    _mykDevice[p_id->card].spiSettings->reg_write = hal_rfic_write_reg;
    _mykDevice[p_id->card].profilesValid = 0; // used as output of init...default to 0

    _skiq_log(SKIQ_LOG_DEBUG, "JESD RX pre-emphasis is %u, amplitude %u\n",
           _mykDevice[p_id->card].rx->framer->preEmphasis,
           _mykDevice[p_id->card].rx->framer->serializerAmplitude);

    _skiq_log(SKIQ_LOG_DEBUG, "JESD TX lane select 0x%x, cross bar 0x%x\n",
              _mykDevice[p_id->card].tx->deframer->deserializerLanesEnabled,
              _mykDevice[p_id->card].tx->deframer->deserializerLaneCrossbar);

    // configure default warp for X2 rev C
    if( hw_rev == hw_rev_c )
    {
        status = _write_warp_voltage( p_id, DEFAULT_WARP_VOLTAGE );
    }

    if( status == 0 )
    {
        if( (status=_reset_ad9528( p_id )) == 0 &&
            (status=_reset_ad9371( p_id )) == 0 )
        {
            // apply the default config to use max sample rate
            status = _apply_profile_change( p_id, _p_max_rx_profile );
        }
    }

    if( status == 0 )
    {
        rfic_active[p_id->card] = true;
    }

    return (status);
}

void _display_version_info( void )
{
    uint32_t silicon, major, minor, build = 0;

    if( MYKONOS_getApiVersion(&(_mykDevice[0]), &silicon, &major, &minor, &build ) == 0 )
    {
        _skiq_log(SKIQ_LOG_INFO,
                  "RF IC version %u.%u.%u\n",
                  major, minor, build);
    }
}

int32_t _release( rf_id_t *p_id )
{
    int32_t status=0;

    /* we want to reset the divider back to 1 in case you're using an older version of
       of libsidekiq which doesn't set this at all...so just reset to the default */
    fpga_jesd_phy_modify_tx_rate( p_id->card, skiq_tx_hdl_A1, 1, 1 /* nr_lanes */ );
    fpga_jesd_phy_modify_tx_rate( p_id->card, skiq_tx_hdl_A2, 1, 1 /* nr_lanes */ );

    /* zero out the rate_config arrays for the specified card index */
    rfic_clear_rate_config( p_id );

    _mykonos_profile_free( _p_user_mykDevice[p_id->card] );
    FREE_IF_NOT_NULL( _p_user_mykDevice[p_id->card] );

    rfic_active[p_id->card] = false;

    return (status);
}

int32_t _write_rx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq )
{
    int32_t status=-1;
    uint8_t pll_lock=0;
    mykonosDevice_t *p_mykonos =&( _mykDevice[p_id->card]);
    uint8_t freqIndex=0;
    bool runRxCal=false;
    uint32_t cal_mask=0;

    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) ||
        (p_id->hdl == ORX_HDL) )
    {
        status=-2;
        
        // turn off the radio
        if( MYKONOS_radioOff(&(_mykDevice[p_id->card])) == 0 )
        {
            status=-3;
            // update the RF PLL
            if( p_id->hdl == skiq_rx_hdl_A1 ||
                p_id->hdl == skiq_rx_hdl_A2 )
            {
                status=MYKONOS_setRfPllFrequency(&(_mykDevice[p_id->card]), RX_PLL, freq);
                // if we're running with auto cal, we need to check if it's time to re-cal
                if( _rx_cal_mode[p_id->card] == skiq_rx_cal_mode_auto )
                {
                    // check if we're in a diff freq table or if the freq >100M change
                    freqIndex = _find_freq_table_index( freq );
                    if( (freqIndex != _rx_cal_state[p_id->card].freqTableIndex) ||
                        ((ABS_DIFF(freq, _rx_cal_state[p_id->card].lastCalFreq)) >= MAX_FREQ_CAL_OFFSET) )
                    {
                        runRxCal = true;
                        status=_convert_skiq_rx_cal_to_mykonos_cal( p_id->hdl,
                                                                    _rx_cal_mask[p_id->hdl],
                                                                    &cal_mask );
                    }
                }
            }
            else
            {
                status=MYKONOS_setRfPllFrequency(&(_mykDevice[p_id->card]), SNIFFER_PLL, freq);
            }
            if( status==0 )
            {
                status=-4;
                // check for PLL lock: bit 1 is RX PLL lock
                if( MYKONOS_checkPllsLockStatus(&(_mykDevice[p_id->card]), &pll_lock) == 0 &&
                    ((pll_lock & RX_PLL_LOCK) == RX_PLL_LOCK) )
                {
                    if( runRxCal == true )
                    {
                        _skiq_log(SKIQ_LOG_DEBUG, "Running RX cal\n");
                        if( (status=_init_cal( p_mykonos, cal_mask )) == 0 )
                        {
                            // save the cal state
                            _rx_cal_state[p_id->card].freqTableIndex = freqIndex;
                            _rx_cal_state[p_id->card].lastCalFreq = freq;
                        }
                    }

                    // turn the radio back on
                    if( MYKONOS_radioOn(&(_mykDevice[p_id->card])) == 0 )
                    {
                        status=0;
                        // save the new freq config
                        if( p_id->hdl == ORX_HDL )
                        {
                            // we need to make sure OBS RX path is enabled
                            _skiq_log( SKIQ_LOG_DEBUG, "Enabling OBS Rx\n");
                            if( MYKONOS_setObsRxPathSource( p_mykonos, OBS_RX1_SNIFFERLO ) != 0 )
                            {
                                _skiq_log( SKIQ_LOG_ERROR, "Unable to configure OBS RX path\n" );
                            }
                            p_mykonos->obsRx->snifferPllLoFrequency_Hz = freq;
                            p_mykonos->obsRx->defaultObsRxChannel = OBS_RX1_SNIFFERLO;
                        }
                        else
                        {
                            p_mykonos->rx->rxPllLoFrequency_Hz = freq;
                        }
                        // for now, just returned the cached freq
                        *p_act_freq = (double)(freq);
                    }
                }
            }
        }
    }

    return (status);
}

int32_t _write_tx_freq( rf_id_t *p_id, uint64_t freq, double *p_act_freq )
{
    int32_t status=-1;
    uint8_t pll_lock=0;
    mykonosDevice_t *p_mykonos =&( _mykDevice[p_id->card]);
    uint8_t freqIndex=0;
    mykonosObsRxChannels_t obs_chan;

    if( (p_id->hdl == skiq_tx_hdl_A1) ||
        (p_id->hdl == skiq_tx_hdl_A2) )
    {
        status=-2;

        // change obs to internal for cal...cache previous setting to restore at the end
        if( MYKONOS_getObsRxPathSource(&(_mykDevice[p_id->card]), &obs_chan) != 0 )
        {
            skiq_warning("Unable to restore current channel setting prior to configuration (card=%u)",
                         p_id->card);
            obs_chan = OBS_RX1_SNIFFERLO;
        }
        if( MYKONOS_setObsRxPathSource(&(_mykDevice[p_id->card]), OBS_INTERNALCALS) != 0 )
        {
            skiq_warning("Unable to set RF path prior to calibration (card=%u)\n",
                         p_id->card);
        }        
        
        // turn off the radio
        if( MYKONOS_radioOff(&(_mykDevice[p_id->card])) == 0 )
        {
            status=-3;
            // update the RF PLL
            status=MYKONOS_setRfPllFrequency(&(_mykDevice[p_id->card]), TX_PLL, freq);
            if( status==0 )
            {
                status=-4;
                // check for PLL lock: bit 2 is TX PLL lock
                if( MYKONOS_checkPllsLockStatus(&(_mykDevice[p_id->card]), &pll_lock) == 0 &&
                    ((pll_lock & TX_PLL_LOCK) == TX_PLL_LOCK) )
                {
                    // if we're running in auto tx quad cal, determine if we need to re-run cal
                    if( _tx_quadcal[p_id->card][p_id->hdl] == skiq_tx_quadcal_mode_auto )
                    {
                        // check if we're in a diff freq table or if the freq >100M change
                        freqIndex = _find_freq_table_index( _mykDevice[p_id->card].tx->txPllLoFrequency_Hz );
                        if( (freqIndex != _tx_cal_state[p_id->card].freqTableIndex) ||
                            ((ABS_DIFF(_mykDevice[p_id->card].tx->txPllLoFrequency_Hz,
                                       _tx_cal_state[p_id->card].lastCalFreq)) >= MAX_FREQ_CAL_OFFSET) )
                        {
                            _skiq_log(SKIQ_LOG_DEBUG, "Running TX quadcal\n");
                            if( (status=_init_cal( p_mykonos, INIT_TX_CAL_STATE )) == 0 )
                            {
                                // save the cal state
                                _tx_cal_state[p_id->card].freqTableIndex = freqIndex;
                                _tx_cal_state[p_id->card].lastCalFreq = _mykDevice[p_id->card].tx->txPllLoFrequency_Hz;
                            }
                        }
                    }
                    // make sure to update the TX test tone freq
                    status=_config_tx_test_tone( p_id, _tx_test_tone[p_id->card] );
                    
                    // turn the radio back on
                    if( MYKONOS_radioOn(&(_mykDevice[p_id->card])) == 0 )
                    {
                        status=0;
                        p_mykonos->tx->txPllLoFrequency_Hz = freq;
                        // for now, just returned the configured frequency
                        *p_act_freq = (double)(freq);
                    }

                    if( MYKONOS_setObsRxPathSource(&(_mykDevice[p_id->card]), obs_chan) != 0 )
                    {
                        skiq_warning("Unable to restore RF path after calibration (card=%u)\n",
                                     p_id->card);
                    }
                }
            }
        }
    }

    return (status);
}

int32_t _write_rx_spectrum_invert( rf_id_t *p_id, bool invert )
{
    int32_t status=-1;

    // no way to invert it...return success if not inverted
    if( invert == false )
    {
        status = 0;
    }

    return (status);
}
int32_t _write_tx_spectrum_invert( rf_id_t *p_id, bool invert )
{
    int32_t status=-1;

    // no way to invert it...return success if not inverted
    if( invert == false )
    {
        status = 0;
    }

    return (status);
}


int32_t _write_tx_attenuation( rf_id_t *p_id, uint16_t atten )
{
    int32_t status=0;

    // Convert quarter dB to milli dB.
    atten = (((float) atten) / 4.0) * 1000;

    if( (p_id->hdl == skiq_tx_hdl_A1) )
    {
        if( MYKONOS_setTx1Attenuation( &(_mykDevice[p_id->card]), atten ) != 0 )
        {
            status=-2;
        }
        else
        {
            _mykDevice[p_id->card].tx->tx1Atten_mdB = atten;
        }
    }
    else if( p_id->hdl == skiq_tx_hdl_A2 )
    {
        if( MYKONOS_setTx2Attenuation( &(_mykDevice[p_id->card]), atten ) != 0 )
        {
            status=-2;
        }
        else
        {
            _mykDevice[p_id->card].tx->tx2Atten_mdB = atten;
        }
    }
    else
    {
        status = -1;
    }

    return (status);
}

int32_t _read_tx_attenuation( rf_id_t *p_id, uint16_t *p_atten )
{
    int32_t status=0;
    uint16_t atten=0;

    if( (p_id->hdl == skiq_tx_hdl_A1) )
    {
        if( MYKONOS_getTx1Attenuation( &(_mykDevice[p_id->card]), &atten ) != 0 )
        {
            status=-2;
        }
    }
    else if( p_id->hdl == skiq_tx_hdl_A2 )
    {
        if( MYKONOS_getTx2Attenuation( &(_mykDevice[p_id->card]), &atten ) != 0 )
        {
            status=-2;
        }
    }
    else
    {
        status = -1;
    }

    if ( status == 0 )
    {
        // Convert milli dB to quarter dB.
        *p_atten = (((float) atten) / 1000.0) * 4.0;

        /* Conditionally modify *p_atten to be no lower than minimum or higher than maximum */
        *p_atten = MIN(*p_atten, MAX_TX_QUARTER_DB_ATTEN);
        *p_atten = MAX(*p_atten, MIN_TX_QUARTER_DB_ATTEN);
    }

    return (status);
}

int32_t _read_tx_attenuation_range( rf_id_t *p_id, uint16_t *p_max, uint16_t *p_min )
{
    int32_t status = 0;

    if( (p_id->hdl == skiq_tx_hdl_A1) || (p_id->hdl == skiq_tx_hdl_A2) )
    {
        *p_max = MAX_TX_QUARTER_DB_ATTEN;
        *p_min = MIN_TX_QUARTER_DB_ATTEN;
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}

int32_t _write_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t gain_mode )
{
    int32_t status=0;
    mykonosGainMode_t mykMode=AGC;

    if( gain_mode == skiq_rx_gain_manual )
    {
        mykMode = MGC;
    }
    if( p_id->hdl == skiq_rx_hdl_A1 ||
        p_id->hdl == skiq_rx_hdl_A2 )
    {
        status = MYKONOS_setRxGainControlMode( &(_mykDevice[p_id->card]), mykMode );
    }
    else
    {
        status = MYKONOS_setObsRxGainControlMode( &(_mykDevice[p_id->card]), mykMode );
    }

    return (status);
}

int32_t _read_rx_gain_mode( rf_id_t *p_id, skiq_rx_gain_t *p_gain_mode )
{
    int32_t status=-1;
    mykonosGainMode_t gainMode;

    if( (p_id->hdl == skiq_rx_hdl_A1) || (p_id->hdl == skiq_rx_hdl_A2) )
    {
        status=0;
        // note: the gain control mode struct is supposed to be up-to-date
        gainMode = _mykDevice[p_id->card].rx->rxGainCtrl->gainMode;
    }
    else if( (p_id->hdl == ORX_HDL) )
    {
        status=0;
        // note: the gain control mode struct is supposed to be up-to-date
        gainMode = _mykDevice[p_id->card].obsRx->orxGainCtrl->gainMode;
    }

    if( status==0 )
    {
        if( gainMode == MGC )
        {
            *p_gain_mode = skiq_rx_gain_manual;
        }
        else
        {
            *p_gain_mode = skiq_rx_gain_auto;
        }
    }

    return (status);
}

int32_t _read_rx_gain_range( rf_id_t *p_id,
                             uint8_t *p_gain_max,
                             uint8_t *p_gain_min )
{
    mykonosDevice_t *p_mykonos =&( _mykDevice[p_id->card]);
    int32_t status=0;
    
    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_gain_max = p_mykonos->rx->rxGainCtrl->rx1MaxGainIndex;
        *p_gain_min = p_mykonos->rx->rxGainCtrl->rx1MinGainIndex;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_gain_max = p_mykonos->rx->rxGainCtrl->rx2MaxGainIndex;
        *p_gain_min = p_mykonos->rx->rxGainCtrl->rx2MinGainIndex;
    }
    else if( p_id->hdl == ORX_HDL )
    {
        *p_gain_max = p_mykonos->obsRx->orxGainCtrl->maxGainIndex;
        *p_gain_min = p_mykonos->obsRx->orxGainCtrl->minGainIndex;
    }
    else
    {
        status=-EINVAL;
    }
    
    return (status);
}

int32_t _write_rx_gain( rf_id_t *p_id, uint8_t gain )
{
    int32_t status=0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        status = MYKONOS_setRx1ManualGain( &(_mykDevice[p_id->card]), gain );
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        status = MYKONOS_setRx2ManualGain( &(_mykDevice[p_id->card]), gain );
    }
    else if( p_id->hdl == ORX_HDL )
    {
        status = MYKONOS_setObsRxManualGain( &(_mykDevice[p_id->card]), OBS_RX1_SNIFFERLO, gain );
    }
    else
    {
        status=-1;
    }


    return (status);
}

int32_t _read_rx_gain( rf_id_t *p_id, uint8_t *p_gain )
{
    int32_t status=0;

    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        status = MYKONOS_getRx1Gain( &(_mykDevice[p_id->card]), p_gain );
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        status = MYKONOS_getRx2Gain( &(_mykDevice[p_id->card]), p_gain );
    }
    else if( p_id->hdl == ORX_HDL )
    {
        status = MYKONOS_getObsRxGain( &(_mykDevice[p_id->card]), p_gain );
    }
    else
    {
        status=-1;
    }

    return (status);
}

int32_t _read_adc_resolution( uint8_t *p_adc_res )
{
    int32_t status=0;

    *p_adc_res = AD9371_ADC_RES;

    return (status);
}

int32_t _read_dac_resolution( uint8_t *p_dac_res )
{
    int32_t status=0;

    *p_dac_res = AD9371_DAC_RES;

    return (status);
}

int32_t _read_warp_voltage( rf_id_t *p_id, uint16_t *p_warp_voltage )
{
    int32_t status=0;

    if ( !rfic_active[p_id->card] )
    {
        status = -ENODEV;
    }

    if ( status == 0 )
    {
        if( _skiq_get_hw_rev( p_id->card ) == hw_rev_b )
        {
            status = -ENOTSUP;
        }
        else
        {
            // the DAC doesn't support reading, so we'll just return the cached value
            *p_warp_voltage = _warp_voltage[p_id->card];
        }
    }
    
    return (status);
}

int32_t _write_warp_voltage( rf_id_t *p_id, uint16_t warp_voltage )
{
    int32_t status=0;

    if( _skiq_get_hw_rev( p_id->card ) == hw_rev_b )
    {
        status = -ENOTSUP;
    }
    else
    {
        if( (warp_voltage >= MIN_WARP_VOLTAGE) &&
            (warp_voltage <= MAX_WARP_VOLTAGE) )
        {
            status = hal_spi_dac_write( p_id->card, warp_voltage );
            // cache the value
            if( status == 0 )
            {
                _warp_voltage[p_id->card] = warp_voltage;
            }
        }
        else
        {
            _skiq_log( SKIQ_LOG_ERROR, "Warp voltage must be in the range of %u-%u\n",
                       MIN_WARP_VOLTAGE, MAX_WARP_VOLTAGE );
            status = -EINVAL;
        }
    }

    return (status);
}

int32_t _enable_rx_chan( rf_id_t *p_id, bool enable )
{
    // nothing to do, just return success
    return (0);
}

int32_t _enable_tx_chan( rf_id_t *p_id, bool enable )
{
    // nothing to do, just return success
    return (0);
}

int32_t _tx_test_tone_actual_freq( uint32_t tx_sample_rate_kHz, int32_t test_tone_kHz )
{
    int32_t actual_freq_kHz=0;
    int16_t nco_tune_word = 0;
    
    // NOTE: ADI does not provide us with the ability to read the "actual" TX frequency,
    // so we need to perform the inverse of the calculation that they perform to enable the
    // test tone.  This code is the inverse of MYKONOS_enableTxNco() in mykonos.c.  If
    // we receive a new release from ADI for the AD9371, we need to be sure to ensure that
    // this code is updated if necessary
    nco_tune_word = (int16_t)(((int64_t)(test_tone_kHz) << 16) / tx_sample_rate_kHz  * -1);
    actual_freq_kHz = (-1 * (int64_t)nco_tune_word * tx_sample_rate_kHz) >> 16;
    
    return (actual_freq_kHz);
}

int32_t _config_tx_test_tone( rf_id_t *p_id, bool enable )
{
    int32_t status=0;

    if( MYKONOS_enableTxNco( &(_mykDevice[p_id->card]),
                             enable,
                             TX_TEST_TONE_FREQ_OFFSET_KHZ,
                             TX_TEST_TONE_FREQ_OFFSET_KHZ ) != 0 )
    {
        status = -1;
    }
    else
    {
        _tx_test_tone[p_id->card] = enable;
    }

    return (status);
}

int32_t _read_tx_test_tone_freq( rf_id_t *p_id,
                                 int32_t *p_test_freq_offset )
{
    int32_t status=0;

    *p_test_freq_offset = (_tx_test_tone_actual_freq( _mykDevice[p_id->card].tx->txProfile->iqRate_kHz,
                                                      TX_TEST_TONE_FREQ_OFFSET_KHZ ))*RATE_KHZ_TO_HZ;

    return (status);
}


/**************************************************************************************************/
void _read_min_rx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_min_rx_sample_rate )
{
    uint32_t nr_stages = decimator_nr_stages( p_id->card, p_id->hdl );

    if ( p_id->hdl == ORX_HDL )
    {
        *p_min_rx_sample_rate = ORX_IQRATE(_p_min_orx_profile) >> nr_stages;
    }
    else
    {
        *p_min_rx_sample_rate = RX_IQRATE(_p_min_rx_profile) >> nr_stages;
    }
    *p_min_rx_sample_rate *= RATE_KHZ_TO_HZ;
}


/**************************************************************************************************/
void _read_max_rx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_max_rx_sample_rate )
{
    if ( p_id->hdl == ORX_HDL )
    {
        if ( _skiq_meets_fpga_version( p_id->card, 3, 11, 0 ) )
        {
            *p_max_rx_sample_rate = ORX_IQRATE(_p_max_dual_lane_orx_profile);
        }
        else
        {
            *p_max_rx_sample_rate = ORX_IQRATE(_p_max_orx_profile);
        }
    }
    else
    {
        *p_max_rx_sample_rate = RX_IQRATE(_p_max_rx_profile);
    }
    *p_max_rx_sample_rate *= RATE_KHZ_TO_HZ;
}

void _read_min_tx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_min_tx_sample_rate )
{
    *p_min_tx_sample_rate = TX_IQRATE(_p_min_tx_profile) * RATE_KHZ_TO_HZ;
}


/**************************************************************************************************/
void _read_max_tx_sample_rate( rf_id_t *p_id,
                            uint32_t *p_max_tx_sample_rate )
{
    *p_max_tx_sample_rate = TX_IQRATE(_p_max_tx_profile) * RATE_KHZ_TO_HZ;
}


/**************************************************************************************************/
/** The is_profile_valid_for_card() function determines if the specified @a p_profile is valid for
    the referenced @a rf_id_t and the requested sample rate.

    @param[in] p_id Pointer to the RF identification
    @param[in] p_profile Pointer to an AD9371 profile to consider
    @param[in] request_rate Requested sample rate in samples per second

    @return bool
    @retval true Profile is valid for given card configuration
    @retval false Profile is NOT valid for given card configuration
 */
static bool
is_profile_valid_for_card( rf_id_t *p_id,
                           ad9371_profile_t *p_profile,
                           uint32_t request_rate )
{
    int32_t status;
    bool is_valid = false;
    uint32_t actual_sample_rate;
    ad9528pll1Settings_t pll1;
    ad9528pll2Settings_t pll2;
    ad9528outputSettings_t output;
    ad9528sysrefSettings_t sysref;
    clk_div_t fpga_div;
    uint32_t dev_clk_freq;
    clk_div_t dev_clk_div;
    uint32_t rffc_freq;
    uint64_t fpga_min_freq;
    uint64_t fpga_max_freq;
    uint64_t fpga_gbt_clock;

    fpga_jesd_qpll_freq_range( p_id->card, &fpga_min_freq, &fpga_max_freq );

    status = calc_ad9528_for_sample_rate( RX_IQRATE_HZ( p_profile ),
                                          &actual_sample_rate,
                                          &pll1,
                                          &pll2,
                                          &output,
                                          &sysref,
                                          &fpga_div,
                                          &dev_clk_freq,
                                          &dev_clk_div,
                                          &rffc_freq,
                                          fpga_min_freq,
                                          fpga_max_freq,
                                          &fpga_gbt_clock,
                                          _x2_clk_sel,
                                          skiq_x2);

    is_valid = (status == 0);
    if ( is_valid )
    {
        /* FPGA bitstreams prior to 3.11.0 cannot handle ORX profiles above 153.6Msps */
        if ( !_skiq_meets_fpga_version( p_id->card, 3, 11, 0 ) &&
             ( ORX_IQRATE(p_profile) > ORX_DUAL_LANE_RATE_THRESHOLD_KHZ ) &&
             ( p_id->hdl == ORX_HDL ) &&
             ( request_rate > (ORX_DUAL_LANE_RATE_THRESHOLD_KHZ * RATE_KHZ_TO_HZ) ) )
        {
            uint8_t tx_lane_sel;

            /* right now, we can't support the max rate for ORx with the TX lanes mapped to 1 since
             * ORx uses PHY2 and we need PHY2 when using lane_sel = 1 */
            fpga_jesd_read_tx_lane_sel( p_id->card, &tx_lane_sel );
            if ( tx_lane_sel != 0 )
            {
                skiq_warning("FPGA version must be at least v3.11.0 to support sample rates above"
                             " %.1f Msps, skipping profile\n",
                             (float)ORX_DUAL_LANE_RATE_THRESHOLD_KHZ * RATE_HZ_TO_KHZ);
            }

            is_valid = false;
        }
    }

    return is_valid;
}


/**************************************************************************************************/
/** The find_profile_with_decimation() function considers all of the available profiles to determine
    which one, considering available decimator stages, best satisfies the requested sample rate
    (first priority) and bandwidth (second priority).

    @param[in] p_id Pointer to the RF identification
    @param[in] request_rate Requested sample rate in samples per second
    @param[in] request_bw Requested RF bandwidth in Hertz
    @param[in] nr_stages_available Number of decimator stages available for p_id->hdl, may be 0
    @param[in] look_for_tx_profiles flag to determine which rate/bw of the profile to consider
    @param[out] p_found_profile Details on the profile that meets requested rate / bw

    @return int32_t
    @retval 0 Success
    @retval -EINVAL p_found_profile pointer is NULL
    @retval -ERANGE No profile meets both requested rate and bandwidth
 */
static int32_t
find_profile_with_decimation( rf_id_t *p_id,
                              uint32_t request_rate,
                              uint32_t request_bw,
                              uint8_t nr_stages_available,
                              bool look_for_tx_profiles,
                              struct found_profile_t *p_found_profile )
{
    int32_t status = 0;
    ad9371_profile_t *p_profile = NULL;
    uint8_t i;
    RESULTS_ARRAY(results, AD9371_NR_PROFILES);

    if ( p_found_profile == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Looking for RX handle %s profile to meet rate=%u, bw=%u for card %u\n",
                    rx_hdl_cstr(p_id->hdl), request_rate, request_bw, p_id->card);
    }

    /* first pass: calculate all of the differences between the profile rates / bandwidths and the
     * requested rate / bandwidth while considering the available decimation stages */
    for (i = 0; ( i < AD9371_NR_PROFILES ) && ( status == 0 ); i++)
    {
        debug_print("Considering profile #%u\n", i);
        p_profile = (ad9371_profile_t *) &(ad9371_profiles[i]);

        /* make sure it's a valid profile before performing additional checking */
        if ( is_profile_valid_for_card( p_id, p_profile, request_rate ) )
        {
            /* find the maximum decimation rate (even if that decimation rate is 0) that provides a
             * sample rate and bandwidth that satisfies the request */
            uint32_t profile_rate, profile_bw;

            if ( look_for_tx_profiles )
            {
                profile_rate = TX_IQRATE_HZ( p_profile );
                profile_bw = TX_BW_HZ( p_profile );
            }
            else
            {
                profile_rate = IQRATE_HZ( p_id, p_profile );
                profile_bw = BW_HZ( p_id, p_profile );
            }

            debug_print("Considering profile #%u -- %u,%u\n", i, profile_rate, profile_bw);
            rfic_consider_profile( p_id->card, p_id->hdl,
                                   request_rate, request_bw,
                                   profile_rate, profile_bw,
                                   nr_stages_available,
                                   X2_HALF_MAX_SAMPLE_CLK_HZ,
                                   &(results[i]) );
            debug_print("------- Best decimation choice for #%u -- decimation rate %u, delta_rate: "
                        "%d, delta_bw: %d\n", i, results[i].dec_rate, results[i].delta_rate,
                        results[i].delta_bw );
        }
    }

    /* second pass: minimize delta_rate (first) and delta_bw (second) */
    if ( status == 0 )
    {
        uint8_t best_index;

        status = rfic_find_best_result( results, AD9371_NR_PROFILES, &best_index );
        if ( status == 0 )
        {
            p_found_profile->dec_rate = results[best_index].dec_rate;
            p_found_profile->p_profile = (ad9371_profile_t *) &(ad9371_profiles[best_index]);
        }
    }

    return status;
}


static int32_t
find_rx_profile( rf_id_t *p_id,
                 uint32_t request_rate,
                 uint32_t request_bw,
                 struct found_profile_t *p_found_profile )
{
    uint8_t nr_stages = decimator_nr_stages( p_id->card, p_id->hdl );
    skiq_rx_hdl_t hdl;
    rf_id_t id = *p_id;

    /* restrict nr_stages to the minimum across all "configured" handles according to stored
     * sample_rate and bandwidth in rfic_common */
    for ( hdl = skiq_rx_hdl_A1; hdl < skiq_rx_hdl_end; hdl++ )
    {
        id.hdl = hdl;
        if ( rfic_hdl_has_rx_rate_config( &id ) )
        {
            nr_stages = MIN(nr_stages, decimator_nr_stages( id.card, id.hdl ));
        }
    }

    return find_profile_with_decimation( p_id, request_rate, request_bw,
                                         nr_stages,
                                         false, /* look for RX profiles */
                                         p_found_profile );
}


static int32_t
find_tx_profile( rf_id_t *p_id,
                 uint32_t request_rate,
                 uint32_t request_bw,
                 struct found_profile_t *p_found_profile )
{
    return find_profile_with_decimation( p_id, request_rate, request_bw,
                                         0,     /* no decimation */
                                         true,  /* look for TX profiles */
                                         p_found_profile );
}


// actually load the new profile by reconfiguring the 9528/9371
int32_t _apply_profile_change( rf_id_t *p_id,
                               const ad9371_profile_t *p_profile )
{
    int32_t status = -EINVAL;
    uint32_t actual_sample_rate=0;
    clk_div_t fpga_div;
    clk_div_t dev_clk_div;
    uint32_t dev_clk_freq;
    uint32_t rffc_freq;
    ad9528Device_t *p_ad9528 = &(_clockDevice[p_id->card]);
    mykonosDevice_t *p_mykonos =&(_mykDevice[p_id->card]);
    uint32_t val=0;
    uint64_t fpga_min_freq;
    uint64_t fpga_max_freq;
    uint64_t fpga_gbt_clock;
    uint32_t trackingCal = TRACKING_RX_CAL_STATE; // turn on all RX tracking
    uint32_t initCal=0;
    uint32_t rxCal=0;
    uint8_t tx_lane_sel=0;
    uint8_t ena=0;
    uint8_t mode=0;

    if ( p_profile == NULL )
    {
        /* if the passed p_profile reference is NULL, return an error immediately */
        return -EINVAL;
    }

    // configure the TX lane select
    fpga_jesd_read_tx_lane_sel( p_id->card, &tx_lane_sel );
    switch( tx_lane_sel )
    {
        case 0:
            _mykDevice[p_id->card].tx->deframer->deserializerLanesEnabled = LANE_SEL_0_DESERIALIZER_LANES;
            break;
        case 1:
            _mykDevice[p_id->card].tx->deframer->deserializerLanesEnabled = LANE_SEL_1_DESERIALIZER_LANES;
            break;
        default:
            _mykDevice[p_id->card].tx->deframer->deserializerLanesEnabled = LANE_SEL_0_DESERIALIZER_LANES;
            break;
    }


    // 2 lanes for Rx1/2 shared
    fpga_jesd_config_rx_lanes( p_id->card, skiq_rx_hdl_A1, 2, true, false );

    // turn on TX if auto enabled
    if( _tx_quadcal[p_id->card][skiq_tx_hdl_A1] == skiq_tx_quadcal_mode_auto )
    {
        trackingCal |= TRACKING_TX1_CAL_STATE;
    }
    if( _tx_quadcal[p_id->card][skiq_tx_hdl_A2] == skiq_tx_quadcal_mode_auto )
    {
        trackingCal |= TRACKING_TX2_CAL_STATE;
    }

    fpga_jesd_qpll_freq_range( p_id->card, &fpga_min_freq, &fpga_max_freq );

    // update the profile pointers to the new ones
    if( p_profile != NULL )
    {
        p_mykonos->rx->rxProfile = (mykonosRxProfile_t*)(p_profile->p_rxProfile);
        p_mykonos->obsRx->orxProfile = p_profile->p_orxSettings->orxProfile;
        p_mykonos->obsRx->customLoopbackAdcProfile = p_profile->p_orxSettings->customLoopbackAdcProfile;
        p_mykonos->tx->txProfile = (mykonosTxProfile_t*)(p_profile->p_txProfile);
        p_mykonos->clocks = (mykonosDigClocks_t*)(p_profile->p_clocks);
    }

    if ( p_mykonos->obsRx->orxProfile->iqRate_kHz > ORX_DUAL_LANE_RATE_THRESHOLD_KHZ )
    {
        debug_print("setting serializerLanesEnabled for ORX to dual lane\n");
        p_mykonos->obsRx->framer->serializerLanesEnabled = ORX_DUAL_LANE_SERIALIZER_SETTING;
    }
    else
    {
        debug_print("setting serializerLanesEnabled for ORX to single lane\n");
        p_mykonos->obsRx->framer->serializerLanesEnabled = ORX_SINGLE_LANE_SERIALIZER_SETTING;
    }

    // TODO: this is horrible!  We're performing a full reset and init procedure
    // just to change the sample rate...there has to be a better way...

    ///////////////////////////////////////////////
    // Configure 9528
    // figure out our clock config
    calc_ad9528_for_sample_rate( RX_IQRATE(p_profile) * RATE_KHZ_TO_HZ,
                                 &actual_sample_rate,
                                 p_ad9528->pll1Settings,
                                 p_ad9528->pll2Settings,
                                 p_ad9528->outputSettings,
                                 p_ad9528->sysrefSettings,
                                 &fpga_div,
                                 &dev_clk_freq,
                                 &dev_clk_div,
                                 &rffc_freq,
                                 fpga_min_freq,
                                 fpga_max_freq,
                                 &fpga_gbt_clock,
                                 _x2_clk_sel,
                                 skiq_x2);

    // don't reset/configure 9528 unless dev clock is different
    if( _dev_clock[p_id->card] != dev_clk_freq )
    {
        // hold the 9528 in reset before switching the oscillator
        sidekiq_fpga_reg_read( p_id->card, FPGA_REG_FMC_CTRL, &val );
        BF_SET( val, 0, AD9528_SPI_RESET_N_OFFSET, AD9528_SPI_RESET_N_LEN ); // 9528 reset        
        sidekiq_fpga_reg_write( p_id->card, FPGA_REG_FMC_CTRL, val );
        sidekiq_fpga_reg_verify( p_id->card, FPGA_REG_FMC_CTRL, val );
        
        // select the appropriate oscialltor
        if( p_ad9528->pll1Settings->vcxo_Frequency_Hz == 153600000 )
        {
            BF_SET( val, 1, VCXO_153M6_EN_OFFSET, VCXO_153M6_EN_LEN );
            BF_SET( val, 0, VCXO_100M_EN_OFFSET, VCXO_100M_EN_LEN );
        }
        else if( p_ad9528->pll1Settings->vcxo_Frequency_Hz == 100000000 )
        {
            BF_SET( val, 0, VCXO_153M6_EN_OFFSET, VCXO_153M6_EN_LEN );
            BF_SET( val, 1, VCXO_100M_EN_OFFSET, VCXO_100M_EN_LEN );
        }
        else
        {
            status = -EINVAL;
            _skiq_log(SKIQ_LOG_ERROR, "Invalid VCXO frequency calculated");
        }
        sidekiq_fpga_reg_write( p_id->card, FPGA_REG_FMC_CTRL, val );
        sidekiq_fpga_reg_verify( p_id->card, FPGA_REG_FMC_CTRL, val );

        // reset the 9528 now that we have the right oscillator selected
        _reset_ad9528( p_id );
        
        if( AD9528_initialize( p_ad9528 ) == 0 )
        {
            _skiq_log(SKIQ_LOG_INFO, "AD9528 initialization success!\n");
        }
        else
        {
            _skiq_log(SKIQ_LOG_ERROR, "AD9528 initialization failed\n");
            hal_critical_exit(-1);
            return (-1);
        }
        
        // update the RFFC freq
        uint64_t rffc_id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID( p_id->card, skiq_rx_hdl_A1 );
        rffc5071a_set_ref_clock_value( rffc_id, (uint64_t)(rffc_freq) );
        rffc_id = X2_MAP_CARD_AND_RX_HDL_TO_PLL_ID( p_id->card, ORX_HDL );
        rffc5071a_set_ref_clock_value( rffc_id, (uint64_t)(rffc_freq) );
        
        // now save the dev_clk settings
        _dev_clock[p_id->card] = dev_clk_freq;
        _fpga_gbt_clock[p_id->card] = fpga_gbt_clock;
    }

    // read the control output config to restore after reset
    if( _read_control_output_config( p_id, &mode, &ena ) != 0 )
    {
        skiq_error("Unable to read control output configuration (card=%u", p_id->card);
    }
    
    // reset the 9371
    _reset_ad9371( p_id );
    
    // set hdl 0/1 to the same config
    fpga_jesd_write_phy_divider( p_id->card, 0, fpga_div );
    fpga_jesd_write_phy_divider( p_id->card, 1, fpga_div );
    if ( ( RX_IQRATE(p_profile) != ORX_IQRATE(p_profile) ) &&
         ( ORX_IQRATE(p_profile) <= ORX_DUAL_LANE_RATE_THRESHOLD_KHZ ) )
    {
        fpga_jesd_write_phy_divider( p_id->card, 2, fpga_div/2 );
    }
    else
    {
        fpga_jesd_write_phy_divider( p_id->card, 2, fpga_div );
    }
    
    // update TX
    if ( TX_IQRATE(p_profile) != RX_IQRATE(p_profile) )
    {
        fpga_jesd_phy_modify_tx_rate( p_id->card, skiq_tx_hdl_A1, fpga_div/2, 1 /* nr_lanes */ );
        fpga_jesd_phy_modify_tx_rate( p_id->card, skiq_tx_hdl_A2, fpga_div/2, 1 /* nr_lanes */ );
    }
    else
    {
        fpga_jesd_phy_modify_tx_rate( p_id->card, skiq_tx_hdl_A1, fpga_div, 1 /* nr_lanes */ );
        fpga_jesd_phy_modify_tx_rate( p_id->card, skiq_tx_hdl_A2, fpga_div, 1 /* nr_lanes */ );
    }

    /* special case for ORX: if the ORX I/Q rate needs dual lanes, then enable the ORX as a single
     * channel on dual lane configuration for JESD */
    if ( ORX_IQRATE(p_profile) > ORX_DUAL_LANE_RATE_THRESHOLD_KHZ )
    {
        fpga_jesd_config_rx_lanes( p_id->card, ORX_HDL, 2, false, false );
    }
    else
    {
        fpga_jesd_config_rx_lanes( p_id->card, ORX_HDL, 1, false, false );
    }

    ///////////////////////////////////////////////
    
    fpga_jesd_ctrl_phy_reset( p_id->card );
    
    ///////////////////////////////////////////////
    // Configure 9371
    if( (status=MYKONOS_initialize(p_mykonos)) == 0 )
    {
        _skiq_log(SKIQ_LOG_INFO, "Mykonos init completed successfully!\n");
        // disable the SYSREF to the framer prior to MCS (which toggles SYSREF)
        MYKONOS_enableSysrefToRxFramer( p_mykonos, 0 );
        MYKONOS_enableSysrefToObsRxFramer( p_mykonos, 0 );
        MYKONOS_enableSysrefToDeframer( p_mykonos, 0 );
        
        status = _perform_mcs( p_ad9528, p_mykonos );
        if( status == 0 )
        {
            status = _init_mykonos_arm( p_mykonos );
            if( status==0 )
            {
                status=_init_rf_pll( p_mykonos );
                if( status == 0 )
                {
                    if( (_tx_quadcal[p_id->card][skiq_tx_hdl_A1] == skiq_tx_quadcal_mode_auto) ||
                        (_tx_quadcal[p_id->card][skiq_tx_hdl_A2] == skiq_tx_quadcal_mode_auto) )
                    {
                        initCal |= INIT_TX_CAL_STATE;
                        // save the TX cal state
                        _tx_cal_state[p_id->card].freqTableIndex =
                            _find_freq_table_index( p_mykonos->tx->txPllLoFrequency_Hz );
                        _tx_cal_state[p_id->card].lastCalFreq = p_mykonos->tx->txPllLoFrequency_Hz;

                    }
                    else
                    {
                        // reset the TX cal state
                        _tx_cal_state[p_id->card].freqTableIndex = MAX_FREQ_TABLE_ENTRIES;
                        _tx_cal_state[p_id->card].lastCalFreq = 0;
                    }

                    // we need to always run the RX cal after switching profiles
                    if( (status=_convert_skiq_rx_cal_to_mykonos_cal( skiq_rx_hdl_A1,
                                                                     SKIQ_RX_CAL_TYPES,
                                                                     &rxCal )) == 0 )
                    {
                        // save the RX cal state
                        _rx_cal_state[p_id->card].freqTableIndex =
                            _find_freq_table_index( p_mykonos->rx->rxPllLoFrequency_Hz );
                        _rx_cal_state[p_id->card].lastCalFreq = p_mykonos->rx->rxPllLoFrequency_Hz;
                        
                        initCal |= MISC_CAL_STATE | rxCal;
                        
                        status = _init_cal( p_mykonos, initCal );
                        if( status == 0 )
                        {
                            if( (status=MYKONOS_enableTrackingCals(p_mykonos, trackingCal)) == 0 )
                            {
                                _skiq_log( SKIQ_LOG_DEBUG, "Enabled tracking calibration to 0x%x\n", trackingCal );
                                fpga_jesd_ctrl_tx_config(p_id->card,
                                                         DEFAULT_JESD_TX_DIFFCTRL,
                                                         DEFAULT_JESD_TX_PRECURSOR);
                                fpga_jesd_ctrl_core_reset( p_id->card );
                                status = _init_framer_deframer( p_id->card,
                                                                p_ad9528,
                                                                p_mykonos,
                                                                NULL );
                                if( status == 0 )
                                    
                                {
                                    _config_tx_test_tone( p_id, _tx_test_tone[p_id->card]);
                                    status=_write_control_output_config( p_id, mode, ena );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        _skiq_log(SKIQ_LOG_ERROR, "Mykonos unable to initialize (%d)!\n", status);
    }
    return (status);
}

int32_t _write_rx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status = -EINVAL;
    struct found_profile_t fp;
    ad9371_profile_t *p_profile;

    status = find_rx_profile( p_id, rate, bandwidth, &fp );
    if ( status == 0 )
    {
        p_profile = fp.p_profile;
        skiq_debug("Found RX profile: A1/A2 sample rate=%u bw=%u\n", RX_IQRATE_HZ(p_profile), RX_BW_HZ(p_profile));
        skiq_debug("Found RX profile: B1 sample rate=%u bw=%u\n", ORX_IQRATE_HZ(p_profile), ORX_BW_HZ(p_profile));

        // only apply the change if it's different
        if( (p_profile->p_rxProfile != _mykDevice[p_id->card].rx->rxProfile) ||
            (p_profile->p_orxSettings->orxProfile != _mykDevice[p_id->card].obsRx->orxProfile) ||
            (p_profile->p_txProfile != _mykDevice[p_id->card].tx->txProfile) )
        {
            status = _apply_profile_change( p_id, p_profile );
        }
    }

    if ( status == 0 )
    {
        status = rfic_store_rx_rate_config( p_id, rate, bandwidth );
    }

    /* apply the decimation rate across "configured" handles regardless of a change of profile */
    if ( status == 0 )
    {
        rf_id_t id = *p_id;
        skiq_rx_hdl_t hdl;

        for ( hdl = skiq_rx_hdl_A1; ( hdl < skiq_rx_hdl_end ) && ( status == 0 ); hdl++ )
        {
            id.hdl = hdl;
            if ( rfic_hdl_has_rx_rate_config( &id ) )
            {
                status = decimator_set_rate( p_id->card, hdl, fp.dec_rate );
            }
        }
    }

    return (status);
}

int32_t _write_tx_sample_rate_and_bandwidth( rf_id_t *p_id, uint32_t rate, uint32_t bandwidth )
{
    int32_t status=-EINVAL;
    struct found_profile_t fp;
    ad9371_profile_t *p_profile = NULL;

    status = find_tx_profile( p_id, rate, bandwidth, &fp );
    if ( status == 0 )
    {
        p_profile = fp.p_profile;
        skiq_debug("Found profile: TX sample rate=%u bw=%u\n", TX_IQRATE(p_profile), TX_BW_HZ(p_profile));

        // only apply the change if it's different
        if( (p_profile->p_rxProfile != _mykDevice[p_id->card].rx->rxProfile) ||
            (p_profile->p_orxSettings->orxProfile != _mykDevice[p_id->card].obsRx->orxProfile) ||
            (p_profile->p_txProfile != _mykDevice[p_id->card].tx->txProfile) )
        {
            status=_apply_profile_change( p_id, p_profile );
        }
    }
    if( status == 0 )
    {
        status = rfic_store_tx_rate_config( p_id, rate, bandwidth );
    }

    return (status);
}


static double _read_profile_rx_sample_rate( rf_id_t *p_id )
{
    double actual_rate;

    if ( p_id->hdl == ORX_HDL )
    {
        actual_rate = _mykDevice[p_id->card].obsRx->orxProfile->iqRate_kHz;
    }
    else
    {
        actual_rate = _mykDevice[p_id->card].rx->rxProfile->iqRate_kHz;
    }

    return actual_rate * RATE_KHZ_TO_HZ;
}


int32_t _read_rx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;
    uint8_t nr_stages_in_use;

    status = rfic_get_rx_rate_config( p_id, p_rate, NULL /* don't need bandwidth */);
    if ( status == 0 )
    {
        *p_actual_rate = _read_profile_rx_sample_rate( p_id );
    }

    if ( status == 0 )
    {
        status = decimator_get_rate( p_id->card, p_id->hdl, &nr_stages_in_use );
    }

    if ( status == 0 )
    {
        *p_actual_rate /= (double)(1 << nr_stages_in_use);
    }

    return (status);
}

int32_t _read_tx_sample_rate( rf_id_t *p_id, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status=0;

    status = rfic_get_tx_rate_config( p_id, p_rate, NULL /* don't need bandwidth */);
    if ( status == 0 )
    {
        *p_actual_rate = _mykDevice[p_id->card].tx->txProfile->iqRate_kHz*RATE_KHZ_TO_HZ;
    }

    return (status);
}

int32_t _read_rx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;
    uint8_t nr_stages_in_use;

    status = rfic_get_rx_rate_config( p_id, NULL /* don't need sample_rate */, p_bandwidth);
    if ( status == 0 )
    {
        if ( p_id->hdl == ORX_HDL )
        {
            *p_actual_bandwidth = _mykDevice[p_id->card].obsRx->orxProfile->rfBandwidth_Hz;
        }
        else
        {
            *p_actual_bandwidth = _mykDevice[p_id->card].rx->rxProfile->rfBandwidth_Hz;
        }
    }

    if ( status == 0 )
    {
        status = decimator_get_rate( p_id->card, p_id->hdl, &nr_stages_in_use );
    }

    if ( ( status == 0 ) && ( nr_stages_in_use > 0 ) )
    {
        double actual_rate;

        actual_rate = _read_profile_rx_sample_rate( p_id );
        *p_actual_bandwidth = MIN( *p_actual_bandwidth,
                                   actual_rate / (double)(1 << nr_stages_in_use) );
    }

    return (status);
}

int32_t _read_tx_chan_bandwidth( rf_id_t *p_id, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status=0;

    status = rfic_get_tx_rate_config( p_id, NULL /* don't need sample_rate */, p_bandwidth);
    if ( status == 0 )
    {
        *p_actual_bandwidth = _mykDevice[p_id->card].tx->txProfile->rfBandwidth_Hz;
    }

    return (status);
}

int32_t _init_gpio( rf_id_t *p_id, uint32_t output_enable )
{
    mykonosDevice_t *p_mykonos = &(_mykDevice[p_id->card]);
    mykonosGpioErr_t err = MYKONOS_ERR_GPIO_OK;

    // TODO: Is there a better way to do this?
    if( NULL == p_mykonos->auxIo )
    {
        return -1;
    }

    // GPIO can be configured in RADIO_ON and RADIO_OFF states unless ARM GPIO
    // mode is being configured.
    p_mykonos->auxIo->gpio->gpioOe = output_enable & RFIC_X2_GPIO_MASK;
    p_mykonos->auxIo->gpio->gpioSrcCtrl3_0 = GPIO_BITBANG_MODE;
    p_mykonos->auxIo->gpio->gpioSrcCtrl7_4 = GPIO_BITBANG_MODE;
    p_mykonos->auxIo->gpio->gpioSrcCtrl11_8 = GPIO_BITBANG_MODE;
    p_mykonos->auxIo->gpio->gpioSrcCtrl15_12 = GPIO_BITBANG_MODE;
    p_mykonos->auxIo->gpio->gpioSrcCtrl18_16 = GPIO_BITBANG_MODE;

    err = MYKONOS_setupGpio(p_mykonos);
    if( MYKONOS_ERR_GPIO_OK != err )
    {
        // Should we do something with the error codes?
        return -1;
    }
    
    return 0;
}

int32_t _write_gpio( rf_id_t *p_id, uint32_t value )
{
    mykonosDevice_t *p_mykonos = &(_mykDevice[p_id->card]);
    mykonosGpioErr_t err = MYKONOS_ERR_GPIO_OK;
    
    // TODO: Is there a better way to do this?
    if( NULL == p_mykonos->auxIo )
    {
        return -1;
    }
    
    err = MYKONOS_setGpioPinLevel(p_mykonos, value & RFIC_X2_GPIO_MASK);
    if( MYKONOS_ERR_GPIO_OK != err )
    {
        // Should we do something with the error codes?
        return -1;
    }
    
    return 0;
}

int32_t _read_gpio( rf_id_t *p_id, uint32_t *p_value )
{
    mykonosDevice_t *p_mykonos = &(_mykDevice[p_id->card]);
    mykonosGpioErr_t err = MYKONOS_ERR_GPIO_OK;
    
    // TODO: Is there a better way to do this?
    if( NULL == p_mykonos->auxIo )
    {
        return -1;
    }
    
    err = MYKONOS_getGpioPinLevel(p_mykonos, p_value);
    if( MYKONOS_ERR_GPIO_OK != err )
    {
        // Should we do something with the error codes?
        return -1;
    }
    
    return 0;
}

int32_t _read_fpga_rf_clock( rf_id_t *p_id, uint64_t *p_freq )
{
    *p_freq = _fpga_gbt_clock[p_id->card]; 
    
    return 0;
}

int32_t _read_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _tx_quadcal[p_id->card][p_id->hdl];

    return (status);
}

int32_t _write_tx_quadcal_mode( rf_id_t *p_id, skiq_tx_quadcal_mode_t mode )
{
    int32_t status=0;

    _tx_quadcal[p_id->card][p_id->hdl] = mode;

    return (status);
}

int32_t _run_tx_quadcal( rf_id_t *p_id )
{
    mykonosDevice_t *p_mykonos = &(_mykDevice[p_id->card]);
    int32_t status=0;
    mykonosObsRxChannels_t obs_chan;

    _skiq_log( SKIQ_LOG_INFO, "Running TX quadcal\n");

    // change obs to internal for cal...cache previous setting to restore at the end
    if( MYKONOS_getObsRxPathSource(&(_mykDevice[p_id->card]), &obs_chan) != 0 )
    {
        skiq_warning("Unable to restore current channel setting prior to configuration (card=%u)",
                     p_id->card);
        obs_chan = OBS_RX1_SNIFFERLO;
    }
    if( MYKONOS_setObsRxPathSource(&(_mykDevice[p_id->card]), OBS_INTERNALCALS) != 0 )
    {
        skiq_warning("Unable to set RF path prior to calibration (card=%u)\n",
                     p_id->card);
    }        
    
    // turn the radio off
    MYKONOS_radioOff( p_mykonos );

    status = _init_cal( p_mykonos, INIT_TX_CAL_STATE );

    // save the TX cal state
    if( status == 0 )
    {
        _tx_cal_state[p_id->card].freqTableIndex =
            _find_freq_table_index( p_mykonos->tx->txPllLoFrequency_Hz );
        _tx_cal_state[p_id->card].lastCalFreq = p_mykonos->tx->txPllLoFrequency_Hz;
    }

    // turn the radio on
    MYKONOS_radioOn( p_mykonos );

    // restore the previously cached state
    if( MYKONOS_setObsRxPathSource(&(_mykDevice[p_id->card]), obs_chan) != 0 )
    {
        skiq_warning("Unable to restore RF path after calibration (card=%u)\n", p_id->card);
    }

    return (status);
}

int32_t _read_control_output_rx_gain_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;

    *p_ena = 0xFF; // all bits on
    if( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_mode = AD9371_MONITOR_INDEX_AGC_MON_9;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_mode = AD9371_MONITOR_INDEX_AGC_MON_10;
    }
    else if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        skiq_warning("Gain Index monitoring not available for handle %s (card=%u)",
                     rx_hdl_cstr(p_id->hdl), p_id->card);
        status = -ENOTSUP;
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

int32_t _write_control_output_config( rf_id_t *p_id, uint8_t mode, uint8_t ena )
{
    int32_t status=0;
    uint32_t src_ctrl =
        ((GPIO_MONITOR_MODE) |
         (GPIO_MONITOR_MODE << 4));
    uint32_t read_src_ctrl=0;

    status = MYKONOS_setGpioMonitorOut( &(_mykDevice[p_id->card]), mode, ena );
    if( status == 0 )
    {
        // turn on outputs based on enable mask
        status = MYKONOS_setGpioOe( &(_mykDevice[p_id->card]), ena, ena );
        if( status == 0 )
        {
            // read the current setting
            MYKONOS_getGpioSourceCtrl( &(_mykDevice[p_id->card]), &read_src_ctrl );
            src_ctrl = read_src_ctrl | src_ctrl;
            // setup GPIO 0-7 to monitor mode
            status = MYKONOS_setGpioSourceCtrl( &(_mykDevice[p_id->card]), src_ctrl );
        }
    }                                       
    
    return (status);
}

int32_t _read_control_output_config( rf_id_t *p_id, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status=0;

    status = MYKONOS_getGpioMonitorOut( &(_mykDevice[p_id->card]), p_mode, p_ena );

    return (status);
}


int32_t _init_from_file( rf_id_t *p_id, FILE* p_file )
{
    int32_t status=0;
    ad9371_profile_t tmp_profile;

    if( _p_user_mykDevice[p_id->card] == NULL )
    {
        skiq_debug("Allocating memory for user profile (card=%u)", p_id->card);
        _p_user_mykDevice[p_id->card] = (mykonosDevice_t*)(malloc(sizeof(mykonosDevice_t)));
        status=_mykonos_profile_alloc( _p_user_mykDevice[p_id->card] );
    }

    if( status == 0 )
    {
        skiq_info("Applying user provided profile for card %u\n", p_id->card);
        status = ad9371_parse_profile_from_file( _p_user_mykDevice[p_id->card], p_file );
        if( status == 0 )
        {
            tmp_profile.p_rxProfile = _p_user_mykDevice[p_id->card]->rx->rxProfile;
            tmp_profile.p_orxSettings = _p_user_mykDevice[p_id->card]->obsRx;
            tmp_profile.p_txProfile = _p_user_mykDevice[p_id->card]->tx->txProfile;
            tmp_profile.p_clocks = _p_user_mykDevice[p_id->card]->clocks;
            status = _apply_profile_change( p_id, &tmp_profile );
        }
        else
        {
            skiq_error("Parsing profile failed for card %u\n", p_id->card);
            _mykonos_profile_free( _p_user_mykDevice[p_id->card] );
            FREE_IF_NOT_NULL( _p_user_mykDevice[p_id->card] );
        }
    }
    else
    {
        skiq_error("Unable to allocate memory for user profile (card=%u)", p_id->card);
        FREE_IF_NOT_NULL( _p_user_mykDevice[p_id->card] );
    }

    return (status);
}

int32_t _write_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode )
{
    int32_t status=0;

    if( (p_id->hdl == skiq_rx_hdl_B1) )
    {
        status = -EINVAL;
        skiq_error("Specified frequency tune mode %u is not supported for handle %s (card=%u)",
                   mode, rx_hdl_cstr(p_id->hdl), p_id->card);
    }

    if( status == 0 )
    {
        if( mode == skiq_freq_tune_mode_standard )
        {
            // reset num freqs
            _rx_freq_hop[p_id->card].num_freqs = 0;
            _rx_freq_hop[p_id->card].tune_mode = mode;
        }
        else if( mode == skiq_freq_tune_mode_hop_immediate )
        {
            _rx_freq_hop[p_id->card].tune_mode = mode;
        }
        else
        {
            status = -EINVAL;
            skiq_error("Specified frequency tune mode %u is not supported (card=%u)",
                       mode, p_id->card);
        }
    }

    return (status);
}

int32_t _read_rx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;

    if( (p_id->hdl == skiq_rx_hdl_B1) )
    {
        // only standard supported for this handle
        *p_mode = skiq_freq_tune_mode_standard;
    }
    else
    {        
        *p_mode = _rx_freq_hop[p_id->card].tune_mode;
    }

    return (status);
}

int32_t _write_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t mode )
{
    int32_t status=0;

    if( mode == skiq_freq_tune_mode_standard )
    {
        // reset num freqs
        _tx_freq_hop[p_id->card].num_freqs = 0;
        _tx_freq_hop[p_id->card].tune_mode = mode;
    }
    else if( mode == skiq_freq_tune_mode_hop_immediate )
    {
        _tx_freq_hop[p_id->card].tune_mode = mode;
    }
    else
    {
        status = -EINVAL;
        skiq_error("Specified frequency tune mode %u is not supported (card=%u)",
                   mode, p_id->card);
    }

    return (status);
}

int32_t _read_tx_freq_tune_mode( rf_id_t *p_id, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status=0;

    *p_mode = _tx_freq_hop[p_id->card].tune_mode;

    return (status);
}


int32_t _read_rx_hop_list( rf_id_t *p_id,
                           uint16_t *p_num_freq,
                           uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=0;
    uint16_t i=0;

    if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        status = -EINVAL;
    }
    else
    {
        *p_num_freq = _rx_freq_hop[p_id->card].num_freqs;
        for( i=0; i<(*p_num_freq); i++ )
        {
            freq_list[i] = _rx_freq_hop[p_id->card].hop_list[i];
        }
    }

    return (status);
}

int32_t _read_tx_hop_list( rf_id_t *p_id,
                           uint16_t *p_num_freq,
                           uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status=0;
    uint16_t i=0;

    *p_num_freq = _tx_freq_hop[p_id->card].num_freqs;
    for( i=0; i<(*p_num_freq); i++ )
    {
        freq_list[i] = _tx_freq_hop[p_id->card].hop_list[i];
    }

    return (status);
}

int32_t _config_rx_hop_list( rf_id_t *p_id,
                             uint16_t num_freq,
                             uint64_t freq_list[],
                             uint16_t initial_index )
{
    int32_t status=0;
    uint16_t i=0;

    if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        status = -EINVAL;
    }
    else
    {
        _rx_freq_hop[p_id->card].num_freqs = num_freq;
        _rx_freq_hop[p_id->card].next_hop = initial_index;

        for( i=0; i<num_freq; i++ )
        {
            _rx_freq_hop[p_id->card].hop_list[i] = freq_list[i];
        }
    }

    return (status);
}

int32_t _config_tx_hop_list( rf_id_t *p_id,
                             uint16_t num_freq,
                             uint64_t freq_list[],
                             uint16_t initial_index )
{
    int32_t status=0;
    uint16_t i=0;

    for( i=0; i<num_freq; i++ )
    {
        _tx_freq_hop[p_id->card].hop_list[i] = freq_list[i];
    }
    _tx_freq_hop[p_id->card].num_freqs = num_freq;
    _tx_freq_hop[p_id->card].next_hop = initial_index;

    return (status);
}

int32_t _write_next_rx_hop( rf_id_t *p_id, uint16_t freq_index )
{
    int32_t status=0;

    if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        status = -EINVAL;
    }
    else
    {
        _rx_freq_hop[p_id->card].mailbox = freq_index;
    }

    return (status);
}

int32_t _write_next_tx_hop( rf_id_t *p_id, uint16_t freq_index )
{
    int32_t status=0;
    
    _tx_freq_hop[p_id->card].mailbox = freq_index;

    return (status);
}

int32_t _read_curr_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=0;
    
    if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        status = -EINVAL;
    }
    else
    {
        p_hop->index = _rx_freq_hop[p_id->card].curr_hop;
        p_hop->freq = _rx_freq_hop[p_id->card].hop_list[p_hop->index];
    }

    return (status);
}

int32_t _read_next_rx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=0;
    
    if( p_id->hdl == skiq_rx_hdl_B1 )
    {
        status = -EINVAL;
    }
    else
    {
        p_hop->index = _rx_freq_hop[p_id->card].next_hop;
        p_hop->freq = _rx_freq_hop[p_id->card].hop_list[p_hop->index];
    }

    return (status);
}

int32_t _read_curr_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=0;
    
    p_hop->index = _tx_freq_hop[p_id->card].curr_hop;
    p_hop->freq = _tx_freq_hop[p_id->card].hop_list[p_hop->index];

    return (status);
}

int32_t _read_next_tx_hop( rf_id_t *p_id, freq_hop_t *p_hop )
{
    int32_t status=0;
    
    p_hop->index = _tx_freq_hop[p_id->card].next_hop;
    p_hop->freq = _tx_freq_hop[p_id->card].hop_list[p_hop->index];

    return (status);
}

int32_t _rx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status=0;
    (void) rf_timestamp;
    x2_freq_hop_t *p_hop_config = &(_rx_freq_hop[p_id->card]);
    
    status = _write_rx_freq( p_id,
                             p_hop_config->hop_list[p_hop_config->next_hop],
                             p_act_freq );
    if( status == 0 )
    {
        // update current, reset next
        p_hop_config->curr_hop = p_hop_config->next_hop;

        //mailbox moves to next
        p_hop_config->next_hop = p_hop_config->mailbox;

        //reset the mailbox
        p_hop_config->mailbox = 0;
    }

    return (status);
}

int32_t _tx_hop( rf_id_t *p_id, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status=0;
    (void) rf_timestamp;
    x2_freq_hop_t *p_hop_config = &(_tx_freq_hop[p_id->card]);

    status = _write_tx_freq( p_id,
                             p_hop_config->hop_list[p_hop_config->next_hop],
                             p_act_freq );
    if( status == 0 )
    {
        // update current, reset next
        p_hop_config->curr_hop = p_hop_config->next_hop;

        //mailbox moves to next
        p_hop_config->next_hop = p_hop_config->mailbox;

        //reset the mailbox
        p_hop_config->mailbox = 0;
    }

    return (status);
}

int32_t _read_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t *p_mode )
{
    int32_t status=0;

    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) )
    {
        *p_mode = _rx_cal_mode[p_id->card];
    }
    else
    {
        *p_mode = skiq_rx_cal_mode_auto;
    }

    return (status);
}

int32_t _write_rx_cal_mode( rf_id_t *p_id, skiq_rx_cal_mode_t mode )
{
    int32_t status=0;

    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) )
    {
        _rx_cal_mode[p_id->card] = mode;
    }
    else if( mode != skiq_rx_cal_mode_auto )
    {
        status = -ENOTSUP;
    }

    return (status);
}

int32_t _run_rx_cal( rf_id_t *p_id )
{
    int32_t status=0;
    mykonosDevice_t *p_mykonos = &(_mykDevice[p_id->card]);
    uint32_t cal_mask=0;

    if( (status=_convert_skiq_rx_cal_to_mykonos_cal( p_id->hdl,
                                                     _rx_cal_mask[p_id->card],
                                                     &cal_mask )) == 0 )
    {
        // turn the radio off
        status = MYKONOS_radioOff( p_mykonos );
        if( status == 0 )
        {
            status = _init_cal( p_mykonos, cal_mask );
        
            // save the RX cal state
            if( status == 0 )
            {
                _rx_cal_state[p_id->card].freqTableIndex =
                    _find_freq_table_index( p_mykonos->rx->rxPllLoFrequency_Hz );
                _rx_cal_state[p_id->card].lastCalFreq = p_mykonos->rx->rxPllLoFrequency_Hz;
            }
            // turn the radio on
            if( MYKONOS_radioOn( p_mykonos ) != 0 )
            {
                skiq_error("Unable to turn radio on (card=%u)", p_id->card);
            }
        }
    }

    return (status);
}

int32_t _read_rx_cal_mask( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) )
    {
        *p_cal_mask = _rx_cal_mask[p_id->card];
    }
    else
    {
        *p_cal_mask = skiq_rx_cal_type_none;
    }

    return (status);
}

int32_t _write_rx_cal_mask( rf_id_t *p_id, uint32_t cal_mask )
{
    int32_t status=0;

    // validate the calibration mask
    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) )
    {
        if( (cal_mask & ~SKIQ_RX_CAL_TYPES) != 0 )
        {
            status = -EINVAL;
            skiq_error("Invalid calibration requested, only the following 0x%x is supported for card=%u / hdl=%s",
                       SKIQ_RX_CAL_TYPES, p_id->card, rx_hdl_cstr(p_id->hdl));
        }
        else
        {
            _rx_cal_mask[p_id->card] = cal_mask;
        }
    }
    else if( cal_mask != skiq_rx_cal_type_none )
    {
        status = -ENOTSUP;
    }

    return (status);
}

int32_t _read_rx_cal_types_avail( rf_id_t *p_id, uint32_t *p_cal_mask )
{
    int32_t status=0;

    // only supported for A1/2
    if( (p_id->hdl == skiq_rx_hdl_A1) ||
        (p_id->hdl == skiq_rx_hdl_A2) )
    {
        *p_cal_mask = SKIQ_RX_CAL_TYPES;
    }
    else
    {
        *p_cal_mask = skiq_rx_cal_type_none;
    }

    return (status);
}

int32_t _convert_skiq_rx_cal_to_mykonos_cal( skiq_rx_hdl_t hdl,
                                             uint32_t skiq_rx_cal_mask,
                                             uint32_t *p_mykonos_cal_mask )
{
    int32_t status=0;
    int32_t mykonos_cal_mask = 0;

    switch( hdl )
    {
        case skiq_rx_hdl_A1:
        case skiq_rx_hdl_A2:
            mykonos_cal_mask = RX_LO_DELAY; // TODO: what is this?
            if( (skiq_rx_cal_mask & skiq_rx_cal_type_dc_offset) != 0 )
            {
                mykonos_cal_mask |= DC_OFFSET;
            }
            if( (skiq_rx_cal_mask & skiq_rx_cal_type_quadrature) != 0 )
            {
                mykonos_cal_mask |= RX_QEC_INIT;
            }
            break;

        default:
            status = -ENODEV;
            break;
    }

    if( status == 0 )
    {
        *p_mykonos_cal_mask = mykonos_cal_mask;
    }

    return (status);
}

int32_t _read_rf_capabilities( rf_id_t *p_id, skiq_rf_capabilities_t *p_rf_capabilities)
{
     int32_t status=0;

     (void)p_id;

     p_rf_capabilities->minTxFreqHz = AD9371_MIN_TX_FREQUENCY_IN_HZ;
     p_rf_capabilities->maxTxFreqHz = AD9371_MAX_TX_FREQUENCY_IN_HZ;
     p_rf_capabilities->minRxFreqHz = AD9371_MIN_RX_FREQUENCY_IN_HZ;
     p_rf_capabilities->maxRxFreqHz = AD9371_MAX_RX_FREQUENCY_IN_HZ;


     return (status);
}

static skiq_rx_hdl_t _rx_hdl_map_other(rf_id_t *p_id)
{
    skiq_rx_hdl_t hdl = p_id->hdl;
    return (                                            
            ((hdl) == skiq_rx_hdl_A1) ? skiq_rx_hdl_A2 :
            ((hdl) == skiq_rx_hdl_A2) ? skiq_rx_hdl_A1 :
            ((hdl) == skiq_rx_hdl_B1) ? skiq_rx_hdl_end :
            skiq_rx_hdl_end);
}

static skiq_tx_hdl_t _tx_hdl_map_other(rf_id_t *p_id)
{
    skiq_tx_hdl_t hdl = p_id->hdl;

    return (((hdl) == skiq_tx_hdl_A1) ? skiq_tx_hdl_A2 :
            ((hdl) == skiq_tx_hdl_A2) ? skiq_tx_hdl_A1 :
            skiq_tx_hdl_end);
}

