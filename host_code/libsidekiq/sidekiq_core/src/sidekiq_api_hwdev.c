/**
 * @file   sidekiq_api_hwdev.c
 * @author  <meaghan@epiq-solutions.com>
 *
 * @brief
 *
 * <pre>
 * Copyright 2016-2019 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */
#include <stdbool.h>
#include <sidekiq_fpga.h>
#include <sidekiq_private.h>
#include <sidekiq_hal.h>
#include <sidekiq_api_hwdev.h>
#include <io_expander_cache.h>

extern skiq_sys skiq; // this is needed for the CHECK_CARD_* macros


/*****************************************************************************/
/** The skiq_hwdev_write_fpga_reg function is responsible for writing data to
    the FPGA register specified by address. 

    @param[in] card card number of the Sidekiq of interest
    @param[in] address FPGA register address to write to
    @param[in] data value to be written to the address

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_write_fpga_reg(uint8_t card,
                                         uint32_t address,
                                         uint32_t data )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    
    status = sidekiq_fpga_reg_write( card, address, data );
    
    return (status);
}

/*****************************************************************************/
/** The skiq_hwdev_read_fpga_reg function is responsible for reading the 
    FPGA register specified by the address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] address FPGA register address to read from
    @param[out] p_data pointer to where to store the value read

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_read_fpga_reg( uint8_t card, uint32_t address, uint32_t *p_data )
{
    int32_t status=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    
    status = sidekiq_fpga_reg_read( card, address, p_data );
    
    return (status);
}

/*****************************************************************************/
/** The skiq_hwdev_write_rfic_reg function is responsible for writing data to
    the RFIC register specified by address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] cs chip select of the RFIC (0 for cards with single RFIC)
    @param[in] address RFIC register address to write to
    @param[in] data value to be written to the address

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_write_rfic_reg( uint8_t card, uint8_t cs, uint16_t addr, uint8_t data)
{
    int32_t status=0;
    uint8_t max_rfic_cs=0;
    
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if( cs > (max_rfic_cs=_skiq_get_max_rfic_cs(card)) )
    {
        status = -EFAULT;
        skiq_error("Invalid CS of %u specified, max is %u (card=%u)",
                   cs, max_rfic_cs, card);
    }
    else
    {
        status = hal_rfic_write_reg( card, cs, addr, data );
    }

    return (status);
}

/*****************************************************************************/
/** The skiq_hwdev_read_rfic_reg function is responsible for reading the 
    RFIC register specified by the address.

    @param[in] card card number of the Sidekiq of interest
    @param[in] cs chip select of the RFIC (0 for cards with single RFIC)
    @param[in] address RFIC register address to read from
    @param[out] p_data pointer to where to store the value read

    @return int32_t: status where 0=success, anything else is an error
*/
extern int32_t skiq_hwdev_read_rfic_reg( uint8_t card, uint8_t cs, uint16_t addr, uint8_t* p_data)
{
    int32_t status=0;
    uint8_t max_rfic_cs=0;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    if( cs > (max_rfic_cs=_skiq_get_max_rfic_cs(card)) )
    {
        status = -EFAULT;
        skiq_error("Invalid CS of %u specified, max is %u (card=%u)",
                   cs, max_rfic_cs, card);
    }
    else
    {
        status = hal_rfic_read_reg( card, cs, addr, p_data );
    }

    return (status);
}

extern int32_t skiq_hwdev_io_exp_enable_cache_validation(uint8_t card, bool enable)
{
    int32_t status = 0;
    io_exp_enable_cache_validation(card, enable);
    return status;
}



/** @brief  Provide indication whether or not the GPSDO is present and active */
int32_t
skiq_hwdev_gpsdo_is_enabled(uint8_t card, bool *p_is_enabled)
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_is_enabled);

    return gpsdo_fpga_is_enabled(card, p_is_enabled);
}

/**
    @brief  Enable the GPSDO control algorithm on the specified card
*/
int32_t
skiq_hwdev_gpsdo_enable(uint8_t card)
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    return gpsdo_fpga_enable(card);
}

/**
    @brief  Disable the GPSDO control algorithm on the specified card
*/
int32_t
skiq_hwdev_gpsdo_disable(uint8_t card)
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    return gpsdo_fpga_disable(card);
}

/**
    @brief  Get the GPSDO frequency accuracy information from the specified card
*/
int32_t
skiq_hwdev_gpsdo_get_freq_accuracy(uint8_t card, double *p_ppm, int32_t *p_freq_counter)
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_ppm);
    CHECK_NULL_PARAM(p_freq_counter);

    return gpsdo_fpga_get_freq_accuracy(card, p_ppm, p_freq_counter);
}

/**
    @brief  Read the current warp voltage used by the GPSDO
*/
int32_t
skiq_hwdev_gpsdo_read_warp_voltage(uint8_t card, uint32_t *p_warp_voltage)
{
    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_warp_voltage);

    return gpsdo_fpga_read_warp_voltage(card, p_warp_voltage);
}

/**
    @brief  Read the GPSDO algorithm status registers
*/
int32_t
skiq_hwdev_gpsdo_read_status(uint8_t card, skiq_hwdev_gpsdo_status_t *p_status)
{
    int32_t status = 0;
    gpsdo_fpga_status_registers_t gpsdo_status = GPSDO_FPGA_STATUS_REGISTERS_T_INITIALIZER;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);
    CHECK_NULL_PARAM(p_status);

    status = gpsdo_fpga_read_status(card, &gpsdo_status);
    if (0 == status)
    {
        p_status->raw_value = gpsdo_status.raw_value;
        p_status->gpsdo_locked_state = gpsdo_status.gpsdo_locked_state;
        p_status->gps_has_fix = gpsdo_status.gps_has_fix;
        p_status->pll_locked_state = gpsdo_status.pll_locked_state;
        p_status->counter_unlocked = gpsdo_status.counter_unlocked;
        p_status->counter_locked = gpsdo_status.counter_locked;
        p_status->ki = gpsdo_status.ki;
        p_status->stage_timer = gpsdo_status.stage_timer;
    }

    return status;
}

/**
    @brief  Set the GPSDO algorithm control parameters
*/
int32_t skiq_hwdev_gpsdo_set_control_parameters(uint8_t card, uint8_t ki_start,
    uint8_t ki_end, uint8_t thresh_locked, uint8_t thresh_unlocked, uint8_t count_locked,
    uint8_t count_unlocked, uint8_t time_in_stage, int32_t warp_set_value)
{
    gpsdo_fpga_control_parameters_t params = GPSDO_FPGA_CONTROL_PARAMETERS_T_INITIALIZER;

    CHECK_CARD_RANGE(card);
    CHECK_CARD_ACTIVE(card);

    params.ki_start = ki_start;
    params.ki_end = ki_end;
    params.thresh_locked = thresh_locked;
    params.thresh_unlocked = thresh_unlocked;
    params.count_locked = count_locked;
    params.count_unlocked = count_unlocked;
    params.time_in_stage = time_in_stage;
    params.warp_set_value = warp_set_value;

    return gpsdo_fpga_set_control_parameters(card, &params);
}
