/*****************************************************************************/
/** @file sidekiq_rfic.c

    These functions perform a mapping of RFIC related functionality to an underlying implementation
 
 <pre>
 Copyright 2022 Epiq Solutions, All Rights Reserved
 </pre>
*/

#include <stdlib.h>
#include <errno.h>

#include "sidekiq_private.h"

#include "sidekiq_rfic.h"
#include "sidekiq_private.h"
#include "rfic_ad9361.h"
#include "rfic_adrv9002.h"
#include "rfic_rpc.h"
#include "rfic_x2.h"
#include "rfic_x4.h"

#include "sidekiq_rpc_client_api.h"

/***** DEFINES *****/
#define CHECK_RFIC_NULL_PTR(_p_rfic)               \
    do {                                  \
        if ( (_p_rfic) == NULL )          \
        { return -ENODEV; }               \
        if ( (_p_rfic)->p_funcs == NULL ) \
        { return -ENODEV; }               \
    } while (0)

rfic_functions_t* rfic_init_functions( skiq_part_t part,
                                       skiq_hw_rev_t hw_rev )
{
    rfic_functions_t *p_tmp_rfic_functions = NULL;
    rfic_functions_t *p_rfic_functions = NULL;

    switch (part)
    {
        /* mPCIe, M.2, Z2, M.2-2280, Z2p, and Z3u all use the 9361 RFIC implementation */
        case skiq_mpcie:
        case skiq_m2:
        case skiq_z2:
        case skiq_m2_2280:
        case skiq_z2p:
        case skiq_z3u:
            p_tmp_rfic_functions = &(rfic_ad9361_funcs);
            break;

        case skiq_x2:
#if (defined HAS_X2_SUPPORT)
            p_tmp_rfic_functions = &(rfic_x2_funcs);
#else
            skiq_warning("Support for X2 hardware not available in this library configuration\n");
#endif  /* HAS_X2_SUPPORT */
            break;

        case skiq_x4:
#if (defined HAS_X4_SUPPORT)
            p_tmp_rfic_functions = &(rfic_x4_funcs);
#else
            skiq_warning("Support for X4 hardware not available in this library configuration\n");
#endif  /* HAS_X4_SUPPORT */
            break;

        case skiq_nv100:
#if (defined HAS_NV100_SUPPORT)
            p_tmp_rfic_functions = &(rfic_adrv9002_funcs);
#else
            skiq_warning("Support for NV100 hardware not available in this library "
                         "configuration\n");
#endif  /* HAS_NV100_SUPPORT */
            break;

        default:
            skiq_error("Unhandled Sidekiq part %s (%u) while selecting RFIC interface functions\n",
                       part_cstr(part), part);
            break;
    }

    if( p_tmp_rfic_functions != NULL )
    {
        p_rfic_functions = malloc( sizeof(rfic_functions_t) );

        if( p_rfic_functions != NULL )
        {
            memcpy( p_rfic_functions,
                    p_tmp_rfic_functions,
                    sizeof( rfic_functions_t ) );
        }
        else
        {
            skiq_error("Unable to allocate memory for RFIC function pointers");
        }
    }

    return p_rfic_functions;
}

void rfic_free_functions( rfic_functions_t *p_funcs )
{
    FREE_IF_NOT_NULL( p_funcs );
    p_funcs = NULL;
}

int32_t rfic_reset_and_init( rfic_t *p_rfic )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->reset_and_init != NULL )
    {
        status = p_rfic->p_funcs->reset_and_init( p_rfic->p_id );
    }

    return (status);
}

void rfic_display_version_info( rfic_functions_t *p_rfic_funcs )
{
    if( p_rfic_funcs->display_version_info != NULL )
    {
        p_rfic_funcs->display_version_info();
    }
    else
    {
        _skiq_log( SKIQ_LOG_INFO, "Invalid RFIC version detected");
    }
}

int32_t rfic_release( rfic_t *p_rfic )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->release != NULL )
    {
        status = p_rfic->p_funcs->release( p_rfic->p_id );
    }

    return (status);
}

int32_t rfic_write_rx_freq( rfic_t *p_rfic, uint64_t freq, double *p_act_freq )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_freq != NULL )
    {
        status = p_rfic->p_funcs->write_rx_freq( p_rfic->p_id, freq, p_act_freq );
    }

    return (status);
}

int32_t rfic_write_tx_freq( rfic_t *p_rfic, uint64_t freq, double *p_act_freq )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_freq != NULL )
    {
        status = p_rfic->p_funcs->write_tx_freq( p_rfic->p_id, freq, p_act_freq );
    }

    return (status);
}

int32_t rfic_write_rx_spectrum_invert( rfic_t *p_rfic, bool spectrum_invert )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_spectrum_invert != NULL )
    {
        status = p_rfic->p_funcs->write_rx_spectrum_invert( p_rfic->p_id, spectrum_invert );
    }

    return (status);
}

int32_t rfic_write_tx_spectrum_invert( rfic_t *p_rfic, bool spectrum_invert )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_spectrum_invert != NULL )
    {
        status = p_rfic->p_funcs->write_tx_spectrum_invert( p_rfic->p_id, spectrum_invert );
    }

    return (status);
}

int32_t rfic_write_tx_attenuation( rfic_t *p_rfic, uint16_t atten )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_attenuation != NULL )
    {
        status = p_rfic->p_funcs->
            write_tx_attenuation( p_rfic->p_id, atten );
    }

    return (status);
}

int32_t rfic_read_tx_attenuation( rfic_t *p_rfic, uint16_t *p_atten )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_attenuation != NULL )
    {
        status = p_rfic->p_funcs->
            read_tx_attenuation( p_rfic->p_id, p_atten );
    }

    return (status);
}

int32_t rfic_read_tx_attenuation_range( rfic_t *p_rfic, uint16_t *p_max, uint16_t *p_min )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_attenuation != NULL )
    {
        status = p_rfic->p_funcs->
            read_tx_attenuation_range( p_rfic->p_id, p_max, p_min );
    }

    return (status);
}

int32_t rfic_write_rx_gain_mode( rfic_t *p_rfic, skiq_rx_gain_t gain_mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_gain_mode != NULL )
    {
        status = p_rfic->p_funcs->
            write_rx_gain_mode( p_rfic->p_id, gain_mode );
    }

    return (status);
}

int32_t rfic_read_rx_gain_mode( rfic_t *p_rfic, skiq_rx_gain_t *p_gain_mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_gain_mode != NULL )
    {
        status = p_rfic->p_funcs->
            read_rx_gain_mode( p_rfic->p_id, p_gain_mode );
    }

    return (status);
}

int32_t rfic_read_rx_gain_range( rfic_t *p_rfic,
                                 uint8_t *p_gain_max,
                                 uint8_t *p_gain_min )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_gain_range != NULL )
    {
        status = p_rfic->p_funcs->
            read_rx_gain_range( p_rfic->p_id, p_gain_max, p_gain_min );
    }

    return (status);
}

int32_t rfic_write_rx_gain( rfic_t *p_rfic, uint8_t gain )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_gain != NULL )
    {
        status = p_rfic->p_funcs->write_rx_gain( p_rfic->p_id, gain );
    }

    return (status);
}

int32_t rfic_read_rx_gain( rfic_t *p_rfic, uint8_t *p_gain )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_gain != NULL )
    {
        status = p_rfic->p_funcs->read_rx_gain( p_rfic->p_id, p_gain );
    }

    return (status);
}

int32_t rfic_read_adc_resolution( rfic_t *p_rfic, uint8_t *p_adc_res )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_adc_resolution != NULL )
    {
        status = p_rfic->p_funcs->read_adc_resolution( p_adc_res );
    }

    return (status);
}

int32_t rfic_read_dac_resolution( rfic_t *p_rfic, uint8_t *p_dac_res )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_dac_resolution != NULL )
    {
        status = p_rfic->p_funcs->read_dac_resolution( p_dac_res );
    }

    return (status);
}

int32_t rfic_read_warp_voltage( rfic_t *p_rfic, uint16_t *p_warp_voltage )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_warp_voltage != NULL )
    {
        status = p_rfic->p_funcs->read_warp_voltage( p_rfic->p_id, p_warp_voltage );
    }

    return (status);
}

int32_t rfic_write_warp_voltage( rfic_t *p_rfic, uint16_t warp_voltage )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_warp_voltage != NULL )
    {
        status = p_rfic->p_funcs->write_warp_voltage( p_rfic->p_id, warp_voltage );
    }

    return (status);
}

int32_t rfic_read_warp_voltage_extended_range( rfic_t *p_rfic,
                                               uint32_t *p_warp_voltage )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_warp_voltage_extended_range != NULL )
    {
        status = p_rfic->p_funcs->read_warp_voltage_extended_range( p_rfic->p_id,
                                                                    p_warp_voltage );
    }

    return (status);
}

int32_t rfic_write_warp_voltage_extended_range( rfic_t *p_rfic,
                                                uint32_t warp_voltage )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->write_warp_voltage_extended_range != NULL )
    {
        status = p_rfic->p_funcs->write_warp_voltage_extended_range( p_rfic->p_id,
                                                                    warp_voltage );
    }

    return (status);
}

int32_t rfic_enable_rx_chan( rfic_t *p_rfic, bool enable )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->enable_rx_chan != NULL )
    {
        status = p_rfic->p_funcs->enable_rx_chan( p_rfic->p_id, enable );
    }

    return (status);
}

int32_t rfic_enable_tx_chan( rfic_t *p_rfic, bool enable )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->enable_tx_chan != NULL )
    {
        status = p_rfic->p_funcs->enable_tx_chan( p_rfic->p_id, enable );
    }

    return (status);
}

int32_t rfic_config_tx_test_tone( rfic_t *p_rfic, bool enable )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->config_tx_test_tone != NULL )
    {
        status = p_rfic->p_funcs->config_tx_test_tone( p_rfic->p_id, enable );
    }

    return (status);
}

int32_t rfic_read_tx_test_tone_freq( rfic_t *p_rfic,
                                     int32_t *p_test_freq_offset )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_test_tone_freq != NULL )
    {
        status = p_rfic->p_funcs->read_tx_test_tone_freq( p_rfic->p_id,
                                                        p_test_freq_offset );
    }

    return (status);
}

int32_t rfic_write_tx_test_tone_freq( rfic_t *p_rfic,
                                      int32_t test_freq_offset )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_test_tone_freq != NULL )
    {
        status = p_rfic->p_funcs->write_tx_test_tone_freq( p_rfic->p_id,
                                                        test_freq_offset );
    }

    return (status);
}

int32_t rfic_read_min_rx_sample_rate( rfic_t *p_rfic, uint32_t *p_min_rx_sample_rate )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_min_rx_sample_rate != NULL )
    {
        status=0;
        p_rfic->p_funcs->read_min_rx_sample_rate( p_rfic->p_id,
                                            p_min_rx_sample_rate );
    }

    return (status);
}

int32_t rfic_read_max_rx_sample_rate( rfic_t *p_rfic, uint32_t *p_max_rx_sample_rate )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_max_rx_sample_rate != NULL )
    {
        status=0;
        p_rfic->p_funcs->read_max_rx_sample_rate( p_rfic->p_id,
                                            p_max_rx_sample_rate );
    }

    return (status);
}

int32_t rfic_read_min_tx_sample_rate( rfic_t *p_rfic, uint32_t *p_min_tx_sample_rate )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_min_tx_sample_rate != NULL )
    {
        status=0;
        p_rfic->p_funcs->read_min_tx_sample_rate( p_rfic->p_id,
                                               p_min_tx_sample_rate );
    }

    return (status);
}

int32_t rfic_read_max_tx_sample_rate( rfic_t *p_rfic, uint32_t *p_max_tx_sample_rate )
{
    int32_t status=-ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_max_tx_sample_rate != NULL )
    {
        status=0;
        p_rfic->p_funcs->read_max_tx_sample_rate( p_rfic->p_id,
                                               p_max_tx_sample_rate );
    }

    return (status);
}

int32_t rfic_write_rx_sample_rate_and_bandwidth( rfic_t *p_rfic, uint32_t rate, uint32_t bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_sample_rate_and_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->write_rx_sample_rate_and_bandwidth( p_rfic->p_id,
                                                                    rate,
                                                                    bandwidth );
    }

    return (status);
}

int32_t rfic_write_rx_sample_rate_and_bandwidth_multi( rfic_t *p_rfic, skiq_rx_hdl_t handles[],
                                                       uint8_t nr_handles,
                                                       uint32_t rate[],
                                                       uint32_t bandwidth[])
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_sample_rate_and_bandwidth_multi != NULL )
    {
        status = p_rfic->p_funcs->write_rx_sample_rate_and_bandwidth_multi( p_rfic->p_id,
                                                                    &handles[0],
                                                                    nr_handles,
                                                                    &rate[0],
                                                                    &bandwidth[0]);
    }
    else if( p_rfic->p_funcs->write_rx_sample_rate_and_bandwidth != NULL )
    {
        uint8_t hdl_idx;

        status = 0;
        for ( hdl_idx = 0; ( hdl_idx < nr_handles ) && ( status == 0 ); hdl_idx++ )
        {
            rf_id_t id_for_hdl = *(p_rfic->p_id);

            id_for_hdl.hdl = handles[hdl_idx];
            status = p_rfic->p_funcs->write_rx_sample_rate_and_bandwidth( &id_for_hdl,
                                                                        rate[hdl_idx],
                                                                        bandwidth[hdl_idx] );
        }
    }

    return (status);

}

int32_t rfic_write_tx_sample_rate_and_bandwidth( rfic_t *p_rfic, uint32_t rate, uint32_t bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_sample_rate_and_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->write_tx_sample_rate_and_bandwidth( p_rfic->p_id,
                                                                    rate,
                                                                    bandwidth );
    }

    return (status);
}

int32_t rfic_read_rx_sample_rate( rfic_t *p_rfic, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_sample_rate != NULL )
    {
        status = p_rfic->p_funcs->read_rx_sample_rate( p_rfic->p_id,
                                                    p_rate,
                                                    p_actual_rate );
    }

    return (status);
}

int32_t rfic_read_tx_sample_rate( rfic_t *p_rfic, uint32_t *p_rate, double *p_actual_rate )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_sample_rate != NULL )
    {
        status = p_rfic->p_funcs->read_tx_sample_rate( p_rfic->p_id,
                                                    p_rate,
                                                    p_actual_rate );
    }

    return (status);
}

int32_t rfic_read_rx_chan_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_chan_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->read_rx_chan_bandwidth( p_rfic->p_id,
                                                        p_bandwidth,
                                                        p_actual_bandwidth );
    }

    return (status);
}

int32_t rfic_read_tx_chan_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth, uint32_t *p_actual_bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_chan_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->read_tx_chan_bandwidth( p_rfic->p_id,
                                                        p_bandwidth,
                                                        p_actual_bandwidth );
    }

    return (status);
}

int32_t rfic_init_gpio( rfic_t *p_rfic, uint32_t output_enable )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->init_gpio != NULL )
    {
        status = p_rfic->p_funcs->init_gpio( p_rfic->p_id, output_enable );
    }
    
    return (status);
}

int32_t rfic_write_gpio( rfic_t *p_rfic, uint32_t value )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->write_gpio != NULL )
    {
        status = p_rfic->p_funcs->write_gpio( p_rfic->p_id, value );
    }
    
    return (status);
}

int32_t rfic_read_gpio( rfic_t *p_rfic, uint32_t *p_value )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->read_gpio != NULL )
    {
        status = p_rfic->p_funcs->read_gpio( p_rfic->p_id, p_value );
    }

    return (status);
}

int32_t rfic_read_rx_filter_overflow( rfic_t *p_rfic, bool *p_rx_fir )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->read_rx_filter_overflow != NULL )
    {
        status = p_rfic->p_funcs->read_rx_filter_overflow( p_rfic->p_id, p_rx_fir );
    }
    
    return (status);
}

int32_t rfic_read_tx_filter_overflow( rfic_t *p_rfic, bool *p_int3, bool *p_hb3,
                                      bool *p_hb2, bool *p_qec, bool *p_hb1, bool *p_tx_fir )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->read_tx_filter_overflow != NULL )
    {
        status = p_rfic->p_funcs->read_tx_filter_overflow( p_rfic->p_id, p_int3, p_hb3,
                                                        p_hb2, p_qec, p_hb1, p_tx_fir);
    }
    
    return (status);
}

int32_t rfic_read_fpga_rf_clock( rfic_t *p_rfic, uint64_t *p_freq )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->read_fpga_rf_clock != NULL )
    {
        status = p_rfic->p_funcs->read_fpga_rf_clock( p_rfic->p_id, p_freq );
    }
    
    return (status);
}

int32_t rfic_read_tx_quadcal_mode( rfic_t *p_rfic, skiq_tx_quadcal_mode_t *p_mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->read_tx_quadcal_mode != NULL )
    {
        status = p_rfic->p_funcs->read_tx_quadcal_mode( p_rfic->p_id, p_mode );
    }

    return (status);
}

int32_t rfic_write_tx_quadcal_mode( rfic_t *p_rfic, skiq_tx_quadcal_mode_t mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->write_tx_quadcal_mode != NULL )
    {
        status = p_rfic->p_funcs->write_tx_quadcal_mode( p_rfic->p_id, mode );
    }

    return (status);
}

int32_t rfic_run_tx_quadcal( rfic_t *p_rfic )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    if( p_rfic->p_funcs->run_tx_quadcal != NULL )
    {
        status = p_rfic->p_funcs->run_tx_quadcal( p_rfic->p_id );
    }

    return (status);
}

int32_t rfic_read_rx_stream_hdl_conflict( rfic_t *p_rfic,
                                          skiq_rx_hdl_t *p_conflicting_hdls,
                                          uint8_t *p_num_hdls )
{
    int32_t status = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);

    // if there's no specific implementation, assume no conflicts
    *p_num_hdls = 0;
    if( p_rfic->p_funcs->read_rx_stream_hdl_conflict != NULL )
    {
        status = p_rfic->p_funcs->read_rx_stream_hdl_conflict( p_rfic->p_id,
                                                            p_conflicting_hdls,
                                                            p_num_hdls );
    }

    return (status);
}

bool rfic_is_chan_enable_xport_dependent( rfic_t *p_rfic )
{
    bool chan_dependent = false;
    CHECK_RFIC_NULL_PTR(p_rfic);

    // if no specific implementation, than assume not dependent
    if( p_rfic->p_funcs->is_chan_enable_xport_dependent != NULL )
    {
        chan_dependent = p_rfic->p_funcs->is_chan_enable_xport_dependent( p_rfic->p_id );
    }

    return (chan_dependent);
}

int32_t rfic_read_control_output_rx_gain_config( rfic_t *p_rfic, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_control_output_rx_gain_config != NULL )
    {
        status = p_rfic->p_funcs->read_control_output_rx_gain_config( p_rfic->p_id,
                                                                    p_mode,
                                                                    p_ena );
    }

    return (status);
}

int32_t rfic_write_control_output_config( rfic_t *p_rfic, uint8_t mode, uint8_t ena )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_control_output_config != NULL )
    {
        status = p_rfic->p_funcs->write_control_output_config( p_rfic->p_id,
                                                            mode,
                                                            ena );
    }

    return (status);
}

int32_t rfic_read_control_output_config( rfic_t *p_rfic, uint8_t *p_mode, uint8_t *p_ena )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_control_output_config != NULL )
    {
        status = p_rfic->p_funcs->read_control_output_config( p_rfic->p_id,
                                                            p_mode,
                                                            p_ena );
    }

    return (status);
}

int32_t rfic_init_from_file( rfic_t *p_rfic, FILE* p_file )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->init_from_file != NULL )
    {
        status = p_rfic->p_funcs->init_from_file( p_rfic->p_id,
                                                p_file );
    }

    return (status);
}

int32_t rfic_power_down_tx( rfic_t *p_rfic )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->power_down_tx != NULL )
    {
        status = p_rfic->p_funcs->power_down_tx( p_rfic->p_id );
    }

    return (status);
}

int32_t rfic_enable_pin_gain_ctrl( rfic_t *p_rfic )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->enable_pin_gain_ctrl != NULL )
    {
        status = p_rfic->p_funcs->enable_pin_gain_ctrl( p_rfic->p_id );
    }

    return (status);
}

uint32_t rfic_read_hop_on_ts_gpio( rfic_t *p_rfic, uint8_t *p_chip_index )
{
    uint32_t gpio = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);

    *p_chip_index = 0;

    if( p_rfic->p_funcs->read_hop_on_ts_gpio != NULL )
    {
        gpio = p_rfic->p_funcs->read_hop_on_ts_gpio( p_rfic->p_id, p_chip_index );
    }

    return (gpio);
}

int32_t rfic_write_rx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_freq_tune_mode != NULL )
    {
        status = p_rfic->p_funcs->write_rx_freq_tune_mode( p_rfic->p_id, mode );
    }
    // if there isn't a specific handler, than we only allow standard
    else if( mode == skiq_freq_tune_mode_standard ) 
    {
        status = 0;
    }

    return (status);
}

int32_t rfic_write_tx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_freq_tune_mode != NULL )
    {
        status = p_rfic->p_funcs->write_tx_freq_tune_mode( p_rfic->p_id, mode );
    }
    // if there isn't a specific handler, than we only allow standard
    else if( mode == skiq_freq_tune_mode_standard ) 
    {
        status = 0;
    }

    return (status);
}

int32_t rfic_read_rx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_freq_tune_mode != NULL )
    {
        status = p_rfic->p_funcs->read_rx_freq_tune_mode( p_rfic->p_id, p_mode );
    }
    else
    {
        // if we don't have a handler, assume standard
        *p_mode = skiq_freq_tune_mode_standard;
    }

    return (status);
}

int32_t rfic_read_tx_freq_tune_mode( rfic_t *p_rfic, skiq_freq_tune_mode_t *p_mode )
{
    int32_t status = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_freq_tune_mode != NULL )
    {
        status = p_rfic->p_funcs->read_tx_freq_tune_mode( p_rfic->p_id, p_mode );
    }
    else
    {
        // if we don't have a handler, assume standard
        *p_mode = skiq_freq_tune_mode_standard;
    }

    return (status);
}

int32_t rfic_config_rx_hop_list( rfic_t *p_rfic, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->config_rx_hop_list != NULL )
    {
        status = p_rfic->p_funcs->config_rx_hop_list( p_rfic->p_id, num_freq, freq_list, initial_index );
    }

    return (status);
}

int32_t rfic_config_tx_hop_list( rfic_t *p_rfic, uint16_t num_freq, uint64_t freq_list[], uint16_t initial_index )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->config_tx_hop_list != NULL )
    {
        status = p_rfic->p_funcs->config_tx_hop_list( p_rfic->p_id, num_freq, freq_list, initial_index );
    }

    return (status);
}

int32_t rfic_read_rx_hop_list( rfic_t *p_rfic, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_hop_list != NULL )
    {
        status = p_rfic->p_funcs->read_rx_hop_list( p_rfic->p_id, p_num_freq, freq_list );
    }

    return (status);
}

int32_t rfic_read_tx_hop_list( rfic_t *p_rfic, uint16_t *p_num_freq, uint64_t freq_list[SKIQ_MAX_NUM_FREQ_HOPS] )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_hop_list != NULL )
    {
        status = p_rfic->p_funcs->read_tx_hop_list( p_rfic->p_id, p_num_freq, freq_list );
    }

    return (status);
}

int32_t rfic_write_next_rx_hop( rfic_t *p_rfic, uint16_t freq_index )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_next_rx_hop != NULL )
    {
        status = p_rfic->p_funcs->write_next_rx_hop( p_rfic->p_id, freq_index );
    }

    return (status);
}

int32_t rfic_write_next_tx_hop( rfic_t *p_rfic, uint16_t freq_index )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_next_tx_hop != NULL )
    {
        status = p_rfic->p_funcs->write_next_tx_hop( p_rfic->p_id, freq_index );
    }

    return (status);
}

int32_t rfic_rx_hop( rfic_t *p_rfic, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->rx_hop != NULL )
    {
        status = p_rfic->p_funcs->rx_hop( p_rfic->p_id, rf_timestamp, p_act_freq );
    }

    return (status);
}

int32_t rfic_tx_hop( rfic_t *p_rfic, uint64_t rf_timestamp, double *p_act_freq )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->tx_hop != NULL )
    {
        status = p_rfic->p_funcs->tx_hop( p_rfic->p_id, rf_timestamp, p_act_freq );
    }

    return (status);
}

int32_t rfic_read_curr_rx_hop( rfic_t *p_rfic, freq_hop_t *p_hop )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_curr_rx_hop != NULL )
    {
        status = p_rfic->p_funcs->read_curr_rx_hop( p_rfic->p_id, p_hop );
    }

    return (status);
}

int32_t rfic_read_next_rx_hop( rfic_t *p_rfic, freq_hop_t *p_hop )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_next_rx_hop != NULL )
    {
        status = p_rfic->p_funcs->read_next_rx_hop( p_rfic->p_id, p_hop );
    }

    return (status);
}

int32_t rfic_read_curr_tx_hop( rfic_t *p_rfic, freq_hop_t *p_hop )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_curr_tx_hop != NULL )
    {
        status = p_rfic->p_funcs->read_curr_tx_hop( p_rfic->p_id, p_hop );
    }

    return (status);
}

int32_t rfic_read_next_tx_hop( rfic_t *p_rfic, freq_hop_t *p_hop )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_next_tx_hop != NULL )
    {
        status = p_rfic->p_funcs->read_next_tx_hop( p_rfic->p_id, p_hop );
    }

    return (status);
}

int32_t rfic_read_rx_cal_mode( rfic_t *p_rfic, skiq_rx_cal_mode_t *p_mode )
{
    int32_t status = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    // if no read mode supported, than it should always be auto
    if( p_rfic->p_funcs->read_rx_cal_mode != NULL )
    {
        status = p_rfic->p_funcs->read_rx_cal_mode( p_rfic->p_id, p_mode );
    }
    else
    {
        *p_mode = skiq_rx_cal_mode_auto;
    }

    return (status);
}

int32_t rfic_write_rx_cal_mode( rfic_t *p_rfic, skiq_rx_cal_mode_t mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    skiq_rx_cal_mode_t curr_mode;

    if( p_rfic->p_funcs->write_rx_cal_mode != NULL )
    {
        status = p_rfic->p_funcs->write_rx_cal_mode( p_rfic->p_id, mode );
    }
    else
    {
        if( rfic_read_rx_cal_mode( p_rfic, &curr_mode ) == 0 )
        {
            // if no write is defined but we're setting to what it's already set to, call it a success
            if( curr_mode == mode )
            {
                status = 0;
            }
        }
    }

    return (status);
}

int32_t rfic_run_rx_cal( rfic_t *p_rfic )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->run_rx_cal != NULL )
    {
        status = p_rfic->p_funcs->run_rx_cal( p_rfic->p_id );
    }

    return (status);
}

int32_t rfic_read_rx_cal_mask( rfic_t *p_rfic, uint32_t *p_cal_mask )
{
    int32_t status = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_cal_mask != NULL )
    {
        status = p_rfic->p_funcs->read_rx_cal_mask( p_rfic->p_id, p_cal_mask );
    }
    else
    {
        // just set the cal_mask to none
        *p_cal_mask = skiq_rx_cal_type_none;
    }

    return (status);
}

int32_t rfic_write_rx_cal_mask( rfic_t *p_rfic, uint32_t cal_mask )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_cal_mask != NULL )
    {
        status = p_rfic->p_funcs->write_rx_cal_mask( p_rfic->p_id, cal_mask );
    }
    else if( cal_mask == skiq_rx_cal_type_none )
    {
        status = 0;
    }

    return (status);
}

int32_t rfic_read_rx_cal_types_avail( rfic_t *p_rfic, uint32_t *p_cal_mask )
{
    int32_t status = 0;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_cal_types_avail != NULL )
    {
        status = p_rfic->p_funcs->read_rx_cal_types_avail( p_rfic->p_id, p_cal_mask );
    }
    else
    {
        // if no read defined, just report back none
        *p_cal_mask = skiq_rx_cal_type_none;
    }

    return (status);
}

int32_t rfic_read_rx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t *p_mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_enable_mode != NULL )
    {
        status = p_rfic->p_funcs->read_rx_enable_mode( p_rfic->p_id, p_mode );
    }

    return (status);
}

int32_t rfic_read_tx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t *p_mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_enable_mode != NULL )
    {
        status = p_rfic->p_funcs->read_tx_enable_mode( p_rfic->p_id, p_mode );
    }

    return (status);
}

int32_t rfic_write_rx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_enable_mode != NULL )
    {
        status = p_rfic->p_funcs->write_rx_enable_mode( p_rfic->p_id, mode );
    }

    return (status);
}

int32_t rfic_write_tx_enable_mode( rfic_t *p_rfic, skiq_rfic_pin_mode_t mode )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_enable_mode != NULL )
    {
        status = p_rfic->p_funcs->write_tx_enable_mode( p_rfic->p_id, mode );
    }

    return (status);
}

int32_t rfic_read_temp( rfic_t *p_rfic,
                        int8_t *p_temp_in_deg_C )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_temp != NULL )
    {
        status = p_rfic->p_funcs->read_temp( p_rfic->p_id, p_temp_in_deg_C );
    }

    return status;
}

int32_t rfic_read_auxadc( rfic_t *p_rfic,
                          uint8_t channel,
                          uint16_t *p_millivolts )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_auxadc != NULL )
    {
        status = p_rfic->p_funcs->read_auxadc( p_rfic->p_id, channel, p_millivolts );
    }

    return status;
}

int32_t rfic_swap_rx_port_config( rfic_t *p_rfic, uint8_t a_port, uint8_t b_port )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->swap_rx_port_config != NULL )
    {
        status = p_rfic->p_funcs->swap_rx_port_config( p_rfic->p_id,
                                                    a_port,
                                                    b_port );
    }

    return (status);
}

int32_t rfic_swap_tx_port_config( rfic_t *p_rfic, uint8_t a_port, uint8_t b_port )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);
    
    skiq_error("Swapping TX port not currently supported\n");

    return (status);
}

int32_t rfic_read_rf_capabilities( rfic_t *p_rfic, skiq_rf_capabilities_t *p_rf_capabilities )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rf_capabilities != NULL )
    {
        status = p_rfic->p_funcs->read_rf_capabilities( p_rfic->p_id, p_rf_capabilities );
    }

    return (status);
}

skiq_rx_hdl_t rfic_rx_hdl_map_other(rfic_t *p_rfic)
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if(p_rfic->p_funcs->rx_hdl_map_other != NULL)
    {
        status = p_rfic->p_funcs->rx_hdl_map_other(p_rfic->p_id);
    }

    return status;
}

skiq_tx_hdl_t rfic_tx_hdl_map_other(rfic_t *p_rfic)
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if(p_rfic->p_funcs->tx_hdl_map_other != NULL)
    {
        status = p_rfic->p_funcs->tx_hdl_map_other(p_rfic->p_id);
    }

    return status;
}

int32_t rfic_read_rx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_rx_analog_filter_bandwidth != NULL )
    { 
        status = p_rfic->p_funcs->read_rx_analog_filter_bandwidth( p_rfic->p_id, 
                                                                p_bandwidth );
    }

    return (status);
}

int32_t rfic_read_tx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t *p_bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->read_tx_analog_filter_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->read_tx_analog_filter_bandwidth( p_rfic->p_id, 
                                                                p_bandwidth );
    }

    return (status);
}

int32_t rfic_write_rx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_rx_analog_filter_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->write_rx_analog_filter_bandwidth( p_rfic->p_id,
                                                                    bandwidth );
    }

    return (status);
}

int32_t rfic_write_tx_analog_filter_bandwidth( rfic_t *p_rfic, uint32_t bandwidth )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if( p_rfic->p_funcs->write_tx_analog_filter_bandwidth != NULL )
    {
        status = p_rfic->p_funcs->write_tx_analog_filter_bandwidth( p_rfic->p_id,
                                                                    bandwidth );
    }

    return (status);
}

int32_t rfic_gpio_test_read( rfic_t *p_rfic, uint32_t *p_exp_value )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    uint32_t act_value = 0;

    status = rfic_read_gpio(p_rfic, &act_value);
    if(status == 0)
    {
        if(act_value != *p_exp_value)
        {
            status = -EIO;
            skiq_error("Actual value in GPIO %d does not match expected value %d\n", act_value, *p_exp_value);
        }
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_read_tx_fir_config( rfic_t *p_rfic,
                                 uint8_t *p_num_taps,
                                 uint8_t *p_fir_interpolation )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_tx_fir_config != NULL )
    {
        status = p_rfic->p_funcs->read_tx_fir_config( p_rfic->p_id, p_num_taps, p_fir_interpolation );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_read_tx_fir_coeffs( rfic_t *p_rfic,
                                 int16_t *p_coeffs )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_tx_fir_coeffs != NULL )
    {
        status = p_rfic->p_funcs->read_tx_fir_coeffs( p_rfic->p_id, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_write_tx_fir_coeffs( rfic_t *p_rfic,
                                  int16_t *p_coeffs )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->write_tx_fir_coeffs != NULL )
    {
        status = p_rfic->p_funcs->write_tx_fir_coeffs( p_rfic->p_id, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_read_rx_fir_config( rfic_t *p_rfic,
                                 uint8_t *p_num_taps,
                                 uint8_t *p_fir_decimation )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_rx_fir_config != NULL )
    {
        status = p_rfic->p_funcs->read_rx_fir_config( p_rfic->p_id, p_num_taps, p_fir_decimation );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_read_rx_fir_coeffs( rfic_t *p_rfic,
                                 int16_t *p_coeffs )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_rx_fir_coeffs != NULL )
    {
        status = p_rfic->p_funcs->read_rx_fir_coeffs( p_rfic->p_id, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_write_rx_fir_coeffs( rfic_t *p_rfic,
                                  int16_t *p_coeffs )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->write_rx_fir_coeffs != NULL )
    {
        status = p_rfic->p_funcs->write_rx_fir_coeffs( p_rfic->p_id, p_coeffs );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_write_rx_fir_gain( rfic_t *p_rfic,
                                skiq_rx_fir_gain_t gain )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->write_rx_fir_gain != NULL )
    {
        status = p_rfic->p_funcs->write_rx_fir_gain( p_rfic->p_id, gain );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_read_rx_fir_gain( rfic_t *p_rfic,
                               skiq_rx_fir_gain_t *p_gain )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_rx_fir_gain != NULL )
    {
        status = p_rfic->p_funcs->read_rx_fir_gain( p_rfic->p_id, p_gain );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_write_tx_fir_gain( rfic_t *p_rfic,
                                skiq_tx_fir_gain_t gain )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->write_tx_fir_gain != NULL )
    {
        status = p_rfic->p_funcs->write_tx_fir_gain( p_rfic->p_id, gain );
    }

    return status;
}


/**************************************************************************************************/
int32_t rfic_read_tx_fir_gain( rfic_t *p_rfic,
                               skiq_tx_fir_gain_t *p_gain )
{
    int32_t status = -ENOTSUP;
    CHECK_RFIC_NULL_PTR(p_rfic);

    if ( p_rfic->p_funcs->read_tx_fir_gain != NULL )
    {
        status = p_rfic->p_funcs->read_tx_fir_gain( p_rfic->p_id, p_gain );
    }

    return status;
}
