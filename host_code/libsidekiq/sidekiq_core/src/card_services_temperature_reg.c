/**
 * @file   card_services_temperature_reg.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Thu Feb 27 10:39:30 CST 2020
 *
 * @brief  Provides temperature sensor access over FPGA registers
 *
 *
 * <pre>
 * Copyright 2016-2020 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <inttypes.h>

#include "card_services.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"
#include "sidekiq_fpga.h"
#include "ad9361_driver.h"

/* enable debug_print and debug_print_plain when DEBUG_TEMP_SENSOR_REG is defined */
#if (defined DEBUG_TEMP_SENSOR_REG)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** MACROS *****/

/*
  A register based temperature sensor has three components: the FPGA register address, the
  measurement offset (in bits), and the validity offset (in bits).  This implementation assumes a
  constant field width for measurement and validity fields (SKIQ_REG_SENSOR_LEN and
  SKIQ_REG_SENSOR_VALID_LEN).
 */
#define REG_SENSOR(_addr,_offset,_valid)        \
    {                                           \
        .addr = (_addr),                        \
        .offset = (_offset),                    \
        .valid = (_valid),                      \
    }

/* the default register based temperature sensors are TEMP1, TEMP2, and TEMP3 and their naming
 * convention is a nice pattern that this macro can help reduce typing */
#define DFLT_REG_SENSOR(_temp)     REG_SENSOR(FPGA_REG_TEMP_READINGS, \
                                              _temp ## _READING_OFFSET, \
                                              _temp ## _READING_VALID_OFFSET)

#define REG_SENSOR_LIST(_part,_reg_list)        \
    {                                           \
        .part = (_part),                        \
        .nr_sensors = ARRAY_SIZE(_reg_list),    \
        .reg_list = (_reg_list),                \
    }


/***** DEFINES *****/

/* for register based temperature sensors, the measurement field is 8 bits wide */
#define SKIQ_REG_SENSOR_LEN             TEMP1_READING_LEN

/* for register based temperature sensors, the validity field is 1 bit wide */
#define SKIQ_REG_SENSOR_VALID_LEN       TEMP1_READING_VALID_LEN


/***** TYPEDEFS *****/


/***** STRUCTS *****/

struct register_sensor
{
    uint32_t addr;
    uint32_t offset;
    uint32_t valid;
};

struct register_sensor_list
{
    skiq_part_t part;
    uint8_t nr_sensors;
    const struct register_sensor *reg_list;
};


/***** LOCAL VARIABLES *****/


static const struct register_sensor m2_2280_reg_sensors[] = {
    DFLT_REG_SENSOR(TEMP1),
    DFLT_REG_SENSOR(TEMP2),
    DFLT_REG_SENSOR(TEMP3),
};

static const struct register_sensor z3u_reg_sensors[] = {
    DFLT_REG_SENSOR(TEMP1),
    DFLT_REG_SENSOR(TEMP2),
    DFLT_REG_SENSOR(TEMP3),
};

static const struct register_sensor_list register_sensor_lists[] = {
    REG_SENSOR_LIST(skiq_m2_2280, m2_2280_reg_sensors),
    REG_SENSOR_LIST(skiq_z3u, z3u_reg_sensors),
};
static const uint8_t nr_register_sensor_lists = ARRAY_SIZE(register_sensor_lists);


/***** LOCAL FUNCTIONS *****/


/*************************************************************************************************/
static int32_t
get_reg_sensor( uint8_t card,
                uint8_t sensor,
                struct register_sensor *p_reg_sensor )
{
    skiq_part_t part = _skiq_get_part( card );
    int32_t status = -ENOTSUP;
    uint8_t i;

    for ( i = 0; i < nr_register_sensor_lists; i++ )
    {
        if ( part == register_sensor_lists[i].part )
        {
            if ( sensor < register_sensor_lists[i].nr_sensors )
            {
                status = 0;
                *p_reg_sensor = register_sensor_lists[i].reg_list[sensor];
                debug_print("Found sensor at index %" PRIu8 " (addr = 0x%04" PRIx16
                            ", offset = 0x%02" PRIx8 ") for %s on card %" PRIu8 "\n", i,
                            p_reg_sensor->addr, p_reg_sensor->offset, part_cstr( part ), card);
            }
            else
            {
                status = -EINVAL;
            }
            break;
        }
    }

    if ( status == -ENOTSUP )
    {
        debug_print("No sensors found for part %s on card %" PRIu8 "\n", part_cstr( part ), card);
    }

    return status;
}
/*************************************************************************************************/


/***** GLOBAL FUNCTIONS *****/


/*************************************************************************************************/
/**
     @retval 0 Success
     @retval -EINVAL Sensor index is out of range
     @retval -EIO I/O communication error occurred during measurement
     @retval -EAGAIN Sensor measurement is temporarily unavailable, try again later
 */
int32_t
card_read_temp_reg( uint8_t card,
                    uint8_t sensor,
                    int8_t *p_temp_in_deg_C )
{
    int32_t status;
    struct register_sensor reg_sensor;

    status = get_reg_sensor( card, sensor, &reg_sensor );
    if ( status == 0 )
    {
        uint32_t value = 0;

        status = sidekiq_fpga_reg_read( card, reg_sensor.addr, &value );
        if ( status == 0 )
        {
            if ( BF_GET(value, reg_sensor.valid, SKIQ_REG_SENSOR_VALID_LEN) > 0 )
            {
                uint32_t tmp = 0;

                tmp = BF_GET(value, reg_sensor.offset, SKIQ_REG_SENSOR_LEN);
                *p_temp_in_deg_C = (int8_t)(tmp);
            }
            else
            {
                /* There is a period of time after the GPSDO has been enabled where the register
                 * based temperatures (on the same I2C bus as the DCTCXO) are not populated in the
                 * register and thus not valid.  Returning -EAGAIN should indicate to the user that
                 * they should try again later. */
                status = -EAGAIN;
            }
        }
        else
        {
            /* squash non-zero return code from sidekiq_fpga_reg_read to indicate generic I/O
             * communication error */
            status = -EIO;
        }
    }
    else
    {
        debug_print("Card %u (%s) and sensor index %u are an invalid combination\n", card,
                    part_cstr( _skiq_get_part( card ) ), sensor);
    }

    return (status);
}
