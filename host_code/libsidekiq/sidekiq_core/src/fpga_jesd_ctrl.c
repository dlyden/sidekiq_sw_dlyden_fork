/**
 * @file   fpga_jesd_ctrl.c
 * @author  <info@epiq-solutions.com>
 * @date   Wed Dec 20 16:39:24 2017
 *
 * @brief
 *
 * <pre>
 * Copyright 2017-2018 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */

/***** INCLUDES *****/

#include <errno.h>

#include "fpga_jesd_ctrl.h"
#include "sidekiq_fpga_reg_defs.h"
#include "sidekiq_fpga.h"

#include "bit_ops.h"
#include "sidekiq_hal.h"        /* for hal_nanosleep and MICROSEC */
#include "sidekiq_private.h"

#if (defined DEBUG_FPGA_JESD_CTRL)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

/***** DEFINES / MACROS *****/

#define DATA_STROBE                     (1 << 30)
#define DRP_WRITE_ENABLE                (1 << 31)

#define DEFAULT_JESD_SYNC_COUNT         (0x3FF)

#define JESD_PHY_RESET_DONE             ((1<<RX_RESET_DONE_RX_OFFSET) | (1<<RX_RESET_DONE_OBSRX_OFFSET) | (1<<TX_RESET_DONE_OFFSET))
#define JESD_PHY_RESET_POLL             (100*MICROSEC)
#define JESD_PHY_RESET_MAX_TRIES        (50)

#define DRP_GTHE4_CHANNEL_TXOUT_DIV     (0x7C)
#define TXRATE_NO_DIV                   (0xE0)
#define X2_ORX_DUAL_LANE_TX_PHY         (2)

#define JESD_SYNCED_RX_A                ( BIT(RX_SYNC_OFFSET)    | BIT(RX_TVALID_OFFSET) | \
                                          BIT(OBSRX_SYNC_OFFSET) | BIT(OBSRX_TVALID_OFFSET) )
#define JESD_SYNCED_TX_A                ( BIT(TX_SYNC_OFFSET)    | BIT(TX_TREADY_OFFSET) )

#define JESD_SYNCED_RX_B                ( BIT(RX_SYNC_B_OFFSET)    | BIT(RX_TVALID_B_OFFSET) | \
                                          BIT(OBSRX_SYNC_B_OFFSET) | BIT(OBSRX_TVALID_B_OFFSET) )
#define JESD_SYNCED_TX_B                ( BIT(TX_SYNC_B_OFFSET)    | BIT(TX_TREADY_B_OFFSET) )

#define JESD_DRP_ADDR_CTRL_PHY_SEL_OFFSET (16)
#define JESD_DRP_ADDR_CTRL_PHY_SEL_LEN    (4)

// Rates to divide the dev clock by in configuring the FPGA JESD link
#define RX_RATE_1 (0x1)
#define RX_RATE_2 (0x2)
#define RX_RATE_4 (0x3)
#define RX_RATE_8 (0x4)
#define RX_RATE_16 (0x5)

// QUAD PLL is described on p.48 of UltraScale Architecture GTH Transceivers UG576
// There are different freq ranges supported for QPLL0/1
// X2 FPGA builds have M/N hardcoded (M=1, N=80) for QPLL1...so our dev clock in gets limited by PLL clock / 80??

// FPGA using QPLL0
#define QPLL0_FREQ_MIN (122500000) // 9.8G / 80
// Note: Xilinx documentation indicates that 16.375 G is supported by QPLL0, however
// we see that is not the case.  As a result, we're forcing the max such that it does
// not match the datasheet and are planning on following up with Xilinx.  Also,
// Rob tried regenerating the QPLL0 core with the freq range set to 16G and the PLL
// then seemed to behave properly.  However, we started having intermittent TVALID
// issues.  It seems like the JESD interface always momentairly would sync but then
// TVALID would go low.  We'll wait to investigate more if this becomes necessary.
// In the mean time, for the 9 profiles that we support, we need to make sure
// we are unable to find a valid dev clock so we're arbirarily limiting this freq
// range even though it doesn't match the datasheet for now.
#define QPLL0_FREQ_MAX (198000000)//(204687500) // 16.375G / 80
// FPGA >=v3.8 uses QPLL1
#define FPGA_VERS_QPLL1                 FPGA_VERSION(3,8,0)

#define QPLL1_FREQ_MIN (100000000) // 8G / 80
#define QPLL1_FREQ_MAX (162500000) // 13G / 80

// If the TX lane select is set to 1, we need to offset our phy access by 2
// ex. what was PHY0 would be PHY2 when lane select = 1
#define TX_PHY_OFFSET_LANE_SEL_1 (2)

#define FPGA_VERS_SYSREF_CONT_MAJ (3)
#define FPGA_VERS_SYSREF_CONT_MIN (15)
#define FPGA_VERS_SYSREF_CONT_PATCH (0)

/***** TYPEDEFS *****/

/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/

static ARRAY_WITH_DEFAULTS(uint8_t, _tx_lane_sel, SKIQ_MAX_NUM_CARDS, 0);
static ARRAY_WITH_DEFAULTS(uint8_t, _orx_lane_sel, SKIQ_MAX_NUM_CARDS, 0);


/***** LOCAL FUNCTIONS *****/
// The FPGA has specific mapping of JESD PHY to RF interface of the RFIC...these
// are helper functions to handle the mapping
int32_t _fpga_jesd_rx_hdl_to_phy( skiq_part_t part, skiq_rx_hdl_t hdl, uint8_t *p_phy );


/** @brief This function takes the @a hdl and based on @a nr_lanes and @a tx_lane_sel, it produces a
    @a p_phy_mask that represents which JESD PHYs are associated with the configuration.

    For example, on the X4 and TX handle A2, a dual lane and lane_sel=0 means that PHY 2 and 3
    should be configured.
 */
static int32_t
_fpga_jesd_tx_hdl_to_phy_mask( skiq_part_t part,
                               skiq_tx_hdl_t hdl,
                               uint8_t nr_lanes,
                               uint8_t tx_lane_sel,
                               uint8_t *p_phy_mask )
{
    int32_t status = 0;
    uint8_t phy_mask = 0;
    uint8_t offset = (tx_lane_sel == 1) ? 2 : 0;
    uint8_t single_lane = 0, dual_lane = 0;

    if ( part == skiq_x2 )
    {
        /** @note X2 does not presently support dual lane transmit, so it ignores @a nr_lanes */

        /* For TX, PHY0 can be considered the "master" of all of the other PHYs...
           PHY0 must always match the alternate PHYs, so we'll just always set it here... */
        phy_mask = BIT(0);
        switch( hdl )
        {
            case skiq_tx_hdl_A1:
                single_lane = BIT(0);
                break;

            case skiq_tx_hdl_A2:
                single_lane = BIT(1);
                break;

            default:
                status = -EFAULT;
                break;
        }

        if ( status == 0 )
        {
            /* the `offset` adjusts the mask according to the @a tx_lane_sel */
            phy_mask |= (single_lane << offset);
        }
    }
    else if ( part == skiq_x4 )
    {
        /* For TX, PHY0 can be considered the "master" of all of the other PHYs...
           PHY0 must always match the alternate PHYs, so we'll just always set it here...
           Note that if we ever have A and B run clocks run independent, we'll likely need to 
           update this again */
        phy_mask = BIT(0);
        switch( hdl )
        {
            case skiq_tx_hdl_A1:
                single_lane = BIT(0);
                dual_lane =   BIT(0) | BIT(1);
                break;

            case skiq_tx_hdl_A2:
                single_lane = BIT(1);
                dual_lane =   BIT(2) | BIT(3);
                break;

            case skiq_tx_hdl_B1:
                single_lane = BIT(4);
                dual_lane =   BIT(4) | BIT(5);
                break;

            case skiq_tx_hdl_B2:
                single_lane = BIT(5);
                dual_lane =   BIT(6) | BIT(7);
                break;

            default:
                status = -EFAULT;
                break;
        }

        if ( status == 0 )
        {
            /* the `offset` adjusts the single and dual lane masks according to the @a
             * tx_lane_sel */
            single_lane <<= offset;
            dual_lane <<= offset;

            /* amend the `phy_mask` according to number of lanes requested */
            phy_mask |= ( nr_lanes == 2 ) ? dual_lane : single_lane;
        }
    }
    else
    {
        status = -ENODEV;
    }

    if ( status == 0 )
    {
        *p_phy_mask = phy_mask;
    }

    return (status);
}


/**************************************************************************************************/
/** _fpga_jesd_phy_addr_ctrl() configures the address control value based on the phy specified.

    @param[in] phy     JESD PHY index
    @param[in] address address of DRP register

    @return uint32_t  value of addr_ctrl based on phy index
 */
static uint32_t
_fpga_jesd_phy_addr_ctrl( uint8_t phy,
                          uint32_t address )
{
    uint32_t addr_ctrl = 0;
    uint32_t phy_offset = JESD_DRP_ADDR_CTRL_PHY_SEL_OFFSET;
    // PHYs 0-3, we add 1, 4-7 we need to add 2 for JESD PHY in FPGA...this mapping is based on the
    // FPGA implementation and should never change unless instructed by FPGA developer
    uint32_t phy_increment = 1;

    addr_ctrl = address & 0xFFFF;
    // PHYs 4-7 require additional increment of the phy address
    if( phy >= 4 )
    {
        phy_increment = 2;
    }
    BF_SET( addr_ctrl, (phy+phy_increment), phy_offset, JESD_DRP_ADDR_CTRL_PHY_SEL_LEN );

    return (addr_ctrl);
}


/**************************************************************************************************/
/** _fpga_jesd_phy_write() writes data to the specified JESD PHY DRP register address on the
    specified card.

    @param[in] card    Sidekiq card index of interest
    @param[in] phy     JESD PHY index
    @param[in] address address of DRP register
    @param[in] data    data to write to specified address

    @return int32_t
    @retval 0 Successful
    @retval -ENOSYS No transport support for FPGA register transactions
    @retval non-zero Transport's FPGA register transaction failed
 */
static int32_t
_fpga_jesd_phy_write( uint8_t card,
                     uint8_t phy,
                     uint32_t address,
                     uint32_t data )
{
    int32_t status=0;
    uint32_t addr_ctrl = 0;

    addr_ctrl = _fpga_jesd_phy_addr_ctrl( phy, address );
    addr_ctrl |= DRP_WRITE_ENABLE;
    
    if( status == 0 )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_WDATA, data);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_ADDR_CTRL, addr_ctrl);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_ADDR_CTRL, addr_ctrl | DATA_STROBE);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_ADDR_CTRL, addr_ctrl);
    }

    return status;
}


/**************************************************************************************************/
/** fpga_jesd_phy_read() reads from the specified JESD PHY DRP register address on the specified
    card and updates the provided data value that p_data references.

    @param[in] card    Sidekiq card index of interest
    @param[in] phy     JESD PHY index
    @param[in] address address of DRP register
    @param[out] p_data reference to data to populate with value from specified address

    @return int32_t
    @retval 0 Successful
    @retval -ENOSYS No transport support for FPGA register transactions
    @retval non-zero Transport's FPGA register transaction failed
 */
static int32_t
fpga_jesd_phy_read( uint8_t card,
                    uint8_t phy,
                    uint32_t address,
                    uint32_t *p_data )
{
    int32_t status=0;
    uint32_t addr_ctrl = 0;

    addr_ctrl = _fpga_jesd_phy_addr_ctrl( phy, address );

    status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_ADDR_CTRL, addr_ctrl);
    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_ADDR_CTRL, addr_ctrl | DATA_STROBE);
    }

    if ( status == 0 )
    {
        status = sidekiq_fpga_reg_read( card, FPGA_REG_JESD_DRP_RDATA, p_data);
    }

    return status;
}


/***** GLOBAL FUNCTIONS *****/


int32_t fpga_jesd_ctrl_core_reset(uint8_t card)
{
    int32_t status=0;
    uint32_t val=0;
    uint8_t i=0;

    // soft reset the FPGA
    sidekiq_fpga_reg_read( card, FPGA_REG_FPGA_CTRL, &val );
    BF_SET( val, 0, FPGA_SOFT_RESET_OFFSET, FPGA_SOFT_RESET_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_FPGA_CTRL, val );
    BF_SET( val, 1, FPGA_SOFT_RESET_OFFSET, FPGA_SOFT_RESET_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_FPGA_CTRL, val );
    BF_SET( val, 0, FPGA_SOFT_RESET_OFFSET, FPGA_SOFT_RESET_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_FPGA_CTRL, val );

    // check some reset done signal and update status appropriately
    sidekiq_fpga_reg_read( card, FPGA_REG_JESD_STATUS, &val );
    for( i=0; ((i<JESD_PHY_RESET_MAX_TRIES) &&
               ((val & JESD_PHY_RESET_DONE) != JESD_PHY_RESET_DONE));
         i++ )
    {
        hal_nanosleep( JESD_PHY_RESET_POLL );
        sidekiq_fpga_reg_read( card, FPGA_REG_JESD_STATUS, &val );
    }
    if( i == JESD_PHY_RESET_MAX_TRIES )
    {
        status=-1;
        skiq_error("JESD core never completed reset on card %u\n", card);
    }

    return (status);
}

int32_t fpga_jesd_ctrl_phy_reset(uint8_t card)
{
    int32_t status=0;
    uint32_t val=0;
    uint8_t i=0;

    sidekiq_fpga_reg_read( card, FPGA_REG_JESD_PRBS_CTRL, &val );
    BF_SET( val, 0, PHY_RESET_OFFSET, PHY_RESET_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_JESD_PRBS_CTRL, val );
    BF_SET( val, 1, PHY_RESET_OFFSET, PHY_RESET_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_JESD_PRBS_CTRL, val );
    BF_SET( val, 0, PHY_RESET_OFFSET, PHY_RESET_LEN );
    BF_SET( val, DEFAULT_JESD_SYNC_COUNT, SYNC_COUNT_OFFSET, SYNC_COUNT_LEN );
    sidekiq_fpga_reg_write( card, FPGA_REG_JESD_PRBS_CTRL, val );

    // check some reset done signal and update status appropriately
    sidekiq_fpga_reg_read( card, FPGA_REG_JESD_STATUS, &val );
    for( i=0; ((i<JESD_PHY_RESET_MAX_TRIES) &&
               ((val & JESD_PHY_RESET_DONE) != JESD_PHY_RESET_DONE));
         i++ )
    {
        hal_nanosleep( JESD_PHY_RESET_POLL );
        sidekiq_fpga_reg_read( card, FPGA_REG_JESD_STATUS, &val );
    }
    if( i == JESD_PHY_RESET_MAX_TRIES )
    {
        status=-1;
        skiq_error("JESD PHY never completed reset on card %u\n", card);
    }

    return (status);
}

int32_t fpga_jesd_ctrl_tx_config(uint8_t card, uint8_t diffctrl, uint8_t precursor)
{
    uint32_t val=0;
    int32_t status=0;

    sidekiq_fpga_reg_read( card, FPGA_REG_FPGA_CTRL, &val );

    BF_SET( val, diffctrl, GT_TXDIFFCTRL_OFFSET, GT_TXDIFFCTRL_LEN );
    BF_SET( val, precursor, GT_TXPRECURSOR_OFFSET, GT_TXPRECURSOR_LEN );

    status = sidekiq_fpga_reg_write( card, FPGA_REG_FPGA_CTRL, val );

    return (status);
}



int32_t fpga_jesd_read_tx_lane_sel( uint8_t card, uint8_t *p_lane_sel )
{
    int32_t status=-ENOTSUP;
    skiq_part_t part = _skiq_get_part(card);

    // must be either X2 or X4
    if( (part == skiq_x2) ||
        (part == skiq_x4) )
    {
        *p_lane_sel = _tx_lane_sel[card];
        status = 0;
    }

    return (status);
}


int32_t fpga_jesd_write_tx_lane_sel( uint8_t card, uint8_t lane_sel )
{
    int32_t status=-ENOTSUP;
    skiq_part_t part = _skiq_get_part(card);

    // must be either X2 or X4
    if( (part == skiq_x2) ||
        (part == skiq_x4) )
    {
        _tx_lane_sel[card] = lane_sel;
        status = 0;
    }

    return (status);
}


int32_t fpga_jesd_phy_modify_tx_rate( uint8_t card,
                                      skiq_tx_hdl_t tx_hdl,
                                      uint8_t divider,
                                      uint8_t nr_lanes )
{
    uint32_t data = 0;
    uint32_t reg=0;
    int32_t status = 0;
    uint8_t tx_lane_sel = 0;
    uint8_t phy_mask = 0, phy_index = 0;

    // Ok, the "divider" is a bit mis-named...we're not really dividing my 0 in this case,
    // we're just not applying any divide setting, so this is just a pass-thru
    if( divider == 0 )
    {
        reg = 0x0E0;
    }
    // actually dividing by 2
    else if( divider == 1 )
    {
        reg = 0x1E0;
    }
    // actually dividing by 4
    else if( divider == 2 )
    {
        reg = 0x2E0;
    }
    // actually dividing by 8
    else if( divider == 4 )
    {
        reg = 0x3E0;
    }
    // actually dividing by 16
    else if( divider == 8 )
    {
        reg = 0x4E0;
    }
    else
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        status = fpga_jesd_read_tx_lane_sel( card, &tx_lane_sel );
    }

    if ( status == 0 )
    {
        status = _fpga_jesd_tx_hdl_to_phy_mask( _skiq_get_part(card), tx_hdl, nr_lanes, tx_lane_sel,
                                                &phy_mask );
    }

    /* Write 'reg' value to each PHY whose bit is set in phy_mask.  The loop tracks the PHY with
     * phy_index and also shifts phy_mask to the right every iteration, eventually making phy_mask
     * 0, indicating all PHYs have been considered */
    phy_index = 0;
    while ( ( phy_mask > 0 ) && ( status == 0 ) )
    {
        if ( ( phy_mask & 0x1 ) != 0 )
        {
            status = _fpga_jesd_phy_write( card, phy_index, DRP_GTHE4_CHANNEL_TXOUT_DIV, reg );
            if ( status == 0 )
            {
                status = fpga_jesd_phy_read(  card, phy_index, DRP_GTHE4_CHANNEL_TXOUT_DIV, &data );
            }

            if ( ( status == 0 ) && ( data != reg ) )
            {
                skiq_error("Writing to JESD DRP TXOUT_DIV failed for PHY %u on card %u\n",
                           phy_index, card);
                status = -EIO;
            }
        }

        phy_mask >>= 1;
        phy_index++;
    }

    /* On Sidekiq X2's FPGA design, transmit PHY 2's clock is used to drive the Observation Port
     * (ORX) at a faster sample rate. */
    if ( ( status == 0 ) && ( _skiq_get_part( card ) == skiq_x2 ) )
    {
        /* PHY2 is also used for X2 when the lane_sel!=0 for TX, so when the lane_sel is 
           !=0, we cannot support updating the PHY2 for the observation RX rate.  Therefore
           we must enforce that we are only modifying the PHY2 config when lane_sel=0 */
        if( _tx_lane_sel[card] == 0 )
        {
            /* Modify PHY2 DRP settings for JESD OBSRx at 245Msps */
            status = _fpga_jesd_phy_write(card, X2_ORX_DUAL_LANE_TX_PHY, DRP_GTHE4_CHANNEL_TXOUT_DIV, TXRATE_NO_DIV );
            if ( status == 0 )
            {
                status = fpga_jesd_phy_read( card, X2_ORX_DUAL_LANE_TX_PHY, DRP_GTHE4_CHANNEL_TXOUT_DIV, &data );
            }

            if ( ( status == 0 ) && ( data != TXRATE_NO_DIV ) )
            {
                skiq_error("Writing to JESD DRP TXOUT_DIV failed for PHY %u on card %u\n",
                           X2_ORX_DUAL_LANE_TX_PHY, card);
                status = -EIO;
            }
        }
        else
        {
            skiq_info("Cannot update PHY2 when lane select is != 0 (card=%u)", card);
        }
    }

    return status;
}

int32_t fpga_jesd_write_phy_divider( uint8_t card,
                                     skiq_rx_hdl_t hdl,
                                     uint8_t divider )
{
    int32_t status=0;
    uint32_t val=0;
    uint32_t offset=0;
    uint32_t len=0;
    uint32_t reg_div=0;
    uint8_t phy=0;
    
    status = _fpga_jesd_rx_hdl_to_phy( _skiq_get_part(card), hdl, &phy );
    if( status == 0 )
    {
        // determine the correct PHY bits to set based on the phy specified
        switch( phy )
        {
            case 0:
                offset = GT_RXRATE_PHY0_1_OFFSET;
                len = GT_RXRATE_PHY0_1_LEN;
                break;
            case 1:
                offset = GT_RXRATE_PHY1_UNUSED_OFFSET;
                len = GT_RXRATE_PHY1_UNUSED_LEN;
                break;
            case 2:
                offset = GT_RXRATE_PHY2_3_OFFSET;
                len = GT_RXRATE_PHY2_3_LEN;
                break;
            case 3:
                offset = GT_RXRATE_PHY3_UNUSED_OFFSET;
                len = GT_RXRATE_PHY3_UNUSED_LEN;
                break;
            case 4:
            case 5:
                offset = GT_RXRATE_PHY4_5_OFFSET;
                len = GT_RXRATE_PHY4_5_LEN;
                break;
            case 6:
            case 7:
                offset = GT_RXRATE_PHY6_7_OFFSET;
                len = GT_RXRATE_PHY6_7_LEN;
                break;
            default:
                skiq_error("Invalid handle specified\n");
                status = -EINVAL;
                break;
        }
    }
    
    if( status == 0 )
    {
        // now check the divider
        if( divider == 0 )
        {
            reg_div = RX_RATE_1;
        }
        else if( divider == 1 )
        {
            reg_div = RX_RATE_2;
        }
        else if( divider == 2 ) 
        {
            reg_div = RX_RATE_4;
        }
        else if( divider == 4 ) 
        {
            reg_div = RX_RATE_8;
        }
        else if( divider == 8 )
        {
            reg_div = RX_RATE_16;
        }
        else
        {
            skiq_error("Invalid RX divider (%u) specified for card %u on handle %s\n", divider,
                       card, rx_hdl_cstr(hdl));
            status = -EINVAL;
        }
        if( status == 0 )
        {            
            sidekiq_fpga_reg_read( card, FPGA_REG_JESD_PHY_RX_RATE, &val );
            BF_SET( val, reg_div, offset, len );
            // we always run with phy 2 and 3 at the same rate...so be sure to also up
            // phy3 when phy 2 is specified
            // TODO: phy2/3 should probably be shared in the FPGA
            if( (phy == 2) || (phy == 3) )
            {
                BF_SET(val, reg_div, GT_RXRATE_PHY3_UNUSED_OFFSET, GT_RXRATE_PHY3_UNUSED_LEN);
            }
            sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_JESD_PHY_RX_RATE, val );
        }
    }
    
    return (status);
}


/*****************************************************************************/
/* function documentation is in the associated header file */
/*****************************************************************************/
int32_t fpga_jesd_sync_status( uint8_t card, bool check_tx, bool check_rfic_b )
{
    int32_t status=-1;
    uint32_t val=0;
    uint32_t synced_mask = JESD_SYNCED_RX_A;

    if ( check_tx )
    {
        synced_mask |= JESD_SYNCED_TX_A;
    }

    if ( check_rfic_b )
    {
        synced_mask |= JESD_SYNCED_RX_B;

        if ( check_tx )
        {
            synced_mask |= JESD_SYNCED_TX_B;
        }
    }

    status = sidekiq_fpga_reg_read(card, FPGA_REG_JESD_STATUS, &val);
    if ( status == 0 )
    {
        // make sure the JESD status bits are set properly
        if( (val & synced_mask) == synced_mask )
        {
            status = 0;
        }
        else
        {
            skiq_debug("FPGA JESD sync failed, JESD status register (0x%04X) is 0x%08X for "
                       "card %u\n", FPGA_REG_JESD_STATUS, val, card);
            status = -EIO;
        }
    }

    return (status);
}


int32_t fpga_jesd_qpll_freq_range( uint8_t card, uint64_t *p_min_freq, uint64_t *p_max_freq )
{
    int32_t status=0;

    // we need to use the right range based on version
    if ( _skiq_meets_FPGA_VERSION(card, FPGA_VERS_QPLL1) )
    {
        *p_min_freq = QPLL1_FREQ_MIN;
        *p_max_freq = QPLL1_FREQ_MAX;
    }
    else
    {
        *p_min_freq = QPLL0_FREQ_MIN;
        *p_max_freq = QPLL0_FREQ_MAX;
    }

    return (status);
}

int32_t _fpga_jesd_rx_hdl_to_phy( skiq_part_t part, skiq_rx_hdl_t hdl, uint8_t *p_phy )
{
    int32_t status=0;
    
    // unfortunately, we need to know what the part is to map the phy appropriately
    if( part == skiq_x2 )
    {
        switch( hdl )
        {
            case skiq_rx_hdl_A1:
                *p_phy = 0;
                break;
            case skiq_rx_hdl_A2:
                *p_phy = 1;
                break;
            case skiq_rx_hdl_B1:
                *p_phy = 2;
                break;
            default:
                status = -EFAULT;
                break;
        }
    }
    else if( part == skiq_x4 )
    {
        switch( hdl )
        {
            case skiq_rx_hdl_A1:
                *p_phy = 0;
                break;
            case skiq_rx_hdl_A2:
                *p_phy = 1;
                break;
            case skiq_rx_hdl_B1:
                *p_phy = 4;
                break;
            case skiq_rx_hdl_B2:
                *p_phy = 5;
                break;
            case skiq_rx_hdl_C1:
                *p_phy = 2;
                break;
            case skiq_rx_hdl_D1:
                *p_phy = 6;
                break;
            default:
                status = -EFAULT;
                break;
        }
    }
    else
    {
        status = -ENODEV;
    }

    return (status);
}

int32_t fpga_jesd_config_tx_lanes( uint8_t card,
                                   skiq_tx_hdl_t tx_hdl,
                                   uint8_t nr_lanes )
{
    int32_t status = 0;

    if ( tx_hdl == skiq_tx_hdl_A1 )
    {
        status = sidekiq_fpga_reg_RMWV( card, ( nr_lanes == 2) ? 1 : 0,
                                        PHY0_PHY3_TX_SINGLE_CHANNEL_ON_DUAL_LANE,
                                        FPGA_REG_JESD_PHY_RX_RATE );
    }
    else if ( tx_hdl == skiq_tx_hdl_B1 )
    {
        status = sidekiq_fpga_reg_RMWV( card, ( nr_lanes == 2) ? 1 : 0,
                                        PHY4_PHY7_TX_SINGLE_CHANNEL_ON_DUAL_LANE,
                                        FPGA_REG_JESD_PHY_RX_RATE );
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}

int32_t fpga_jesd_config_rx_lanes( uint8_t card,
                                   skiq_rx_hdl_t hdl,
                                   uint8_t num_lanes,
                                   bool dual_chan,
                                   bool swap_chans )
{
    int32_t status=0;
    uint8_t phy=0;
    uint32_t val=0;
    uint32_t dual_chan_bit = 0;
    uint32_t single_on_dual_bit = 0;
    uint32_t swap_chan_bit = 0;

    if( num_lanes != 1 && num_lanes != 2 )
    {
        skiq_error("Only single or dual lanes are supported (card=%u\n)", card);
        status = -EDOM;
    }
    else
    {
        if( num_lanes == 2 )
        {
            dual_chan_bit = 1;
        }
    }

    if( status == 0 )
    {
        status = _fpga_jesd_rx_hdl_to_phy( _skiq_get_part(card), hdl, &phy );
    }

    if( status == 0 )
    {
        // if we're using 2 JESD lanes for a single channel
        if( (dual_chan == false) && (num_lanes == 2)  )
        {
            single_on_dual_bit = 1;
        }

        // we can switch data coming from lane 0 to be chan 1
        if( swap_chans == true )
        {
            swap_chan_bit = 1;
        }
        
        sidekiq_fpga_reg_read( card, FPGA_REG_JESD_PHY_RX_RATE, &val );
        // set the number of lanes
        switch( phy )
        {
            case 0:
                // intentional fall-thru
            case 1:
                RBF_SET(val, dual_chan_bit, PHY0_PHY1_RX_DUAL_LANE);
                RBF_SET(val, swap_chan_bit, PHY0_PHY1_RX_LANE_MAP);
                RBF_SET(val, single_on_dual_bit, PHY0_PHY1_RX_SINGLE_CHANNEL_ON_DUAL_LANE);
                break;
                
            case 2:
                // intentional fall-thru
            case 3:
                RBF_SET(val, dual_chan_bit, PHY2_PHY3_RX_DUAL_LANE);
                RBF_SET(val, swap_chan_bit, PHY2_PHY3_RX_LANE_MAP);
                RBF_SET(val, single_on_dual_bit, PHY2_PHY3_RX_SINGLE_CHANNEL_ON_DUAL_LANE);
                break;
                
            case 4:
                // intentional fall-thru
            case 5:
                RBF_SET(val, dual_chan_bit, PHY4_PHY5_RX_DUAL_LANE);
                RBF_SET(val, swap_chan_bit, PHY4_PHY5_RX_LANE_MAP);
                RBF_SET(val, single_on_dual_bit, PHY4_PHY5_RX_SINGLE_CHANNEL_ON_DUAL_LANE);
                break;
                
            case 6:
                // intentional fall-thru
            case 7:
                RBF_SET(val, dual_chan_bit, PHY6_PHY7_RX_DUAL_LANE);
                RBF_SET(val, swap_chan_bit, PHY6_PHY7_RX_LANE_MAP);
                RBF_SET(val, single_on_dual_bit, PHY6_PHY7_RX_SINGLE_CHANNEL_ON_DUAL_LANE);
                break;

            default:
                status = -ENODEV;
                skiq_error("Invalid phy %u specified for handle %u (card=%u)", phy, hdl, card);
                break;
        }
        if( status == 0 )
        {
            debug_print("FPGA_REG_JESD_PHY_RX_RATE: 0x%X\n", val );
            status = sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_JESD_PHY_RX_RATE, val );
        }
    }
    return (status);
}

int32_t fpga_jesd_read_orx_lane_sel( uint8_t card, uint8_t *p_lane_sel )
{
    int32_t status = -ENOTSUP;
    skiq_part_t part = _skiq_get_part(card);

    /* only supported on X4 since we don't yet have a profile that uses two lanes.  Sidekiq X2 is
     * covered by running skiq_rx_hdl_B1 at 245.76Msps */
    if ( part == skiq_x4 )
    {
        *p_lane_sel = _orx_lane_sel[card];
        status = 0;
    }

    return (status);
}

int32_t fpga_jesd_write_orx_lane_sel( uint8_t card, uint8_t lane_sel )
{
    int32_t status = -ENOTSUP;
    skiq_part_t part = _skiq_get_part(card);

    /* only supported on X4 since we don't yet have a profile that uses two lanes.  Sidekiq X2 is
     * covered by running skiq_rx_hdl_B1 at 245.76Msps */
    if ( part == skiq_x4 )
    {
        _orx_lane_sel[card] = lane_sel;
        status = 0;
    }

    return (status);
}


/* @warning The caller is responsible for resyncing the JESD link after this function is called and
 * if the setting is changed */
int32_t fpga_jesd_write_rx_phase_coherent( uint8_t card, bool enable )
{
    int32_t status=0;

    status = sidekiq_fpga_reg_RMWV( card, enable ? 1 : 0, RX_4_CHANNEL_PHASE_COHERENT,
                                    FPGA_REG_JESD_PRBS_CTRL );

    return (status);
}


/* @warning The caller is responsible for resyncing the JESD link after this function is called and
 * if the setting is changed */
int32_t fpga_jesd_write_tx_phase_coherent( uint8_t card, bool enable )
{
    int32_t status=0;

    status = sidekiq_fpga_reg_RMWV( card, enable ? 1 : 0, TX_4_CHANNEL_PHASE_COHERENT,
                                    FPGA_REG_JESD_PRBS_CTRL );

    return (status);
}

int32_t fpga_jesd_read_unsync_counts( uint8_t card,
                                      jesd_unsync_t *p_unsync_a,
                                      jesd_unsync_t *p_unsync_b,
                                      uint32_t *p_unsync_val)
{
    int32_t status=0;
    uint32_t val=0;
    
    status = sidekiq_fpga_reg_read(card, FPGA_REG_JESD_UNSYNC_COUNTERS, &val);
    if( status == 0 )
    {
        *p_unsync_val = val;
        p_unsync_a->rx_unsync = RBF_GET(val, RX_UNSYNC);
        p_unsync_a->orx_unsync = RBF_GET(val, OBSRX_UNSYNC);
        p_unsync_a->tx_unsync = RBF_GET(val, TX_UNSYNC);
        p_unsync_b->rx_unsync = RBF_GET(val, RX_UNSYNC_B);
        p_unsync_b->orx_unsync = RBF_GET(val, OBSRX_UNSYNC_B);
        p_unsync_b->tx_unsync = RBF_GET(val, TX_UNSYNC_B);
    }
    return (status);
}


/**************************************************************************************************/
/* documentation co-located with the function prototype */
/**************************************************************************************************/
int32_t fpga_jesd_calc_lane_div( uint8_t card,
                                 uint32_t sample_rate_khz,
                                 uint32_t devclk_khz,
                                 uint8_t fpga_div,
                                 bool allow_dual_lane,
                                 uint8_t *p_lane_div,
                                 uint8_t *p_nr_lanes )
{
    int32_t status = 0;
    uint8_t lane_div, nr_lanes;

    lane_div = fpga_div;
    nr_lanes = 1;

    if ( (2 * sample_rate_khz) == devclk_khz )
    {
        lane_div <<= 1;
    }
    else if ( sample_rate_khz == devclk_khz )
    {
        /* lane rate is equal to dev_clk, no adjustments necessary */
    }
    else if ( sample_rate_khz == (2 * devclk_khz) )
    {
        lane_div >>= 1;
    }
    else if ( sample_rate_khz == (4 * devclk_khz) )
    {
        if ( lane_div > 1 )
        {
            lane_div >>= 2;
        }
        else if ( allow_dual_lane )
        {
            lane_div >>= 1;
            nr_lanes = 2;
        }
        else
        {
            skiq_error( "Cannot achieve sample rate (%u kHz) with dev clock (%u kHz) and fpga_div "
                        "(%u) (card=%u)\n", sample_rate_khz,  devclk_khz, fpga_div, card );
            status = -ERANGE;
        }
    }
    else
    {
        skiq_error( "Unsupported sample rate (%u kHz) and dev clock (%u kHz) combination (card=%u)\n",
                    sample_rate_khz,  devclk_khz, card );
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        if ( p_lane_div != NULL ) *p_lane_div = lane_div;
        if ( p_nr_lanes != NULL ) *p_nr_lanes = nr_lanes;
    }

    return status;
}

/**************************************************************************************************/
/* documentation co-located with the function prototype */
/**************************************************************************************************/

int32_t fpga_jesd_config_sysref_continuous( uint8_t card,
                                            bool enable )
{
    int32_t status=-ENOTSUP;
    uint32_t val=0;
    uint32_t data=0;

    if( skiq_x4 == _skiq_get_part(card) )
    {
        if( _skiq_meets_FPGA_VERSION(card, FPGA_VERSION(FPGA_VERS_SYSREF_CONT_MAJ,
                                                        FPGA_VERS_SYSREF_CONT_MIN,
                                                        FPGA_VERS_SYSREF_CONT_PATCH) ) )
        {

            /******************************
             * Yes this is ugly...sorry...
             * We're repurposing bit 31 of the FPGA_REG_JESD_DRP_WDATA to indicate
             * that we're running SW that supports control of the SYSREF of the FPGA
             * this is needed so that the FPGA knows if it should allow control of the
             * SYSREF signal.  This is really only needed in the case where when
             * we may be running an FPGA that supports SYSREF control with software that
             * does not.  If the FPGA is expecting SYSREF control but the software doesn't
             * provide it, then we'll never be able to sync or we may see issues with phase
             * coherency.  We want to make sure that bit is only set in the case where
             * we're running updated software, so we set it here.  However, we need to
             * use a register that will always get set to 0 in older versions of software.
             * So, here we are, setting the bit in FPGA_REG_JESD_DRP_WDATA to indicate that
             * we'll control sysref.  In older versions of software, this register always has
             * bit 31 cleared.  Also, we need to be sure that the bit is set before any JESD
             * sync procedure is ran.  Since this function is called to setup all the clocks
             * before JESD sync, we should be covered.
             *****************************/
            status = sidekiq_fpga_reg_read(card, FPGA_REG_JESD_DRP_WDATA, &data);
            if( status == 0 )
            {
                RBF_SET( data, 1, JESD_ALLOW_LMFC_RESET );
                status = sidekiq_fpga_reg_write(card, FPGA_REG_JESD_DRP_WDATA, data);
            }

            if( status == 0 )
            {
                status=sidekiq_fpga_reg_read( card, FPGA_REG_JESD_CTRL, &val );
                if( status == 0 )
                {
                    if( enable == false )
                    {
                        // disable SYSREF
                        RBF_SET( val, 1, SYSREF_DISABLE );
                        // disable continuous
                        RBF_SET( val, 0, SYSREF_ONESHOT );
                    }
                    else
                    {
                        // enable SYSREF
                        RBF_SET( val, 0, SYSREF_DISABLE );
                        // enable continuous
                        RBF_SET( val, 1, SYSREF_ONESHOT );
                    }
                    status=sidekiq_fpga_reg_write_and_verify( card, FPGA_REG_JESD_CTRL, val );
                }
            }
        }
        else
        {
            skiq_warning("FPGA version without critical issue fix detected, upgrade to FPGA v%u.%u.%u highly recommended\n",
                         FPGA_VERS_SYSREF_CONT_MAJ, FPGA_VERS_SYSREF_CONT_MIN, FPGA_VERS_SYSREF_CONT_PATCH);
        }
    }
    
    return (status);
}
