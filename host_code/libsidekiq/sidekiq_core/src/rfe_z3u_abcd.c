/**
 * @file   rfe_z3u_a.c
 * @date   Thurs Jun 18 09:15:24 2020
 *
 * @brief  Implements the RF Front End for Sidekiq Z3u rev A (nearly identical to 2280)
 *
 * <pre>
 * Copyright 2020-2021 Epiq Solutions, All Rights Reserved
 * </pre>
 *
 */


/***** INCLUDES *****/

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "sidekiq_types.h"
#include "rfe_z3u_abcd.h"
#include "sidekiq_fpga.h"
#include "io_expander_pcal6524.h"

#include "sidekiq_hal.h"
#include "ad9361_driver.h"
#include "card_services.h"

/* enable TRACE_* macros when DEBUG_RFE_Z3U_TRACE is defined */
#if (defined DEBUG_RFE_Z3U_TRACE)
#define DEBUG_PRINT_TRACE_ENABLED
#endif

#if (defined DEBUG_RFE_Z3U_LNA)
#define DEBUG_PRINT_ENABLED
#endif

#if (defined DEBUG_RFE_Z3U_IOE)
#define DEBUG_PRINT_ENABLED
#endif

#include "libsidekiq_debug.h"

/***** DEFINES *****/

#define Z3U_NR_LNA                      2
#define LNA1                            0 /* used to index in current_lna_state[card] */
#define LNA2                            1 /* used to index in current_lna_state[card] */

#define RX1_PORT                        (1)
#define RX2_PORT                        (2)

#define Z3U_IOE_I2C_BUS                 1
#define x20_ADDR                        0x20 /* I2C address of U2008 (PCAL6524) */
#define x20_PORT2_MASK                  0xFF
#define x21_ADDR                        0x21 /* I2C address of U2011 (PCAL6524) */
#define x21_PORT0_MASK                  0xFF
#define x21_PORT1_MASK                  0xFF
#define x22_ADDR                        0x22 /* I2C address of U2001 (PCAL6524) */
#define x22_PORT0_MASK                  0xFF
#define x22_PORT1_MASK                  0xFF

/* p. 20 of schematic */
#define ANT_IO_EXP_ADDR                 x20_ADDR
#define TX_FLT1_SEL                     PORT(0,0)     /* P00 */
#define TX_FLT2_SEL                     PORT(0,1)     /* P01 */
#define TX_AMP_MANUAL_BIAS_ON_N         PORT(0,2)     /* P02 */
#define TX1A_SEL                        PORT(0,3)     /* P03 */
#define TX1B_SEL                        PORT(0,4)     /* P04 */
#define RXTX_ANT1_TOG_EN                PORT(0,5)     /* P05 */
#define RXTX_ANT1_TOG_EN_N              PORT(0,6)     /* P06 */
#define RXTX_AMPS_TOG_EN_N              PORT(0,7)     /* P07 */

/*********************************************
  RX*_LNA1_BYPASS_TOG - controls LNA1 state

      HIGH= LNA1 selected, bypassed disabled.
      LOW = LNA1 is bypassed
      HiZ = not supported (no bypass mode)

  Note: the logic is opposite LNA2.
*/
#define RX1_LNA1_BYPASS_TOG                 PORT(1,1)     /* P11 */
    
#define RX2_LNA1_BYPASS_TOG                 PORT(2,1)     /* x22 - P21 */
/*********************************************/

/*********************************************    
  RX*_LNA2_BYPASS_TOG - controls LNA2 state

      HIGH= LNA2 is bypassed
      LOW = LNA2 selected, bypassed disabled.
      HiZ = not supported (no bypass mode)

  Note: the logic is opposite LNA1.
*/
#define RX1_LNA2_BYPASS_TOG                 PORT(1,2)     /* P12 */
    
#define RX2_LNA2_BYPASS_TOG                 PORT(2,2)     /* x22 - P22 */
/*********************************************/

/*********************************************
  RX*_LNA*_MANUAL_BIAS_EN_N - how LNA is switched in and controlled

      HIGH = LNA1 forced OFF (Bypass still needs to be selected if desired)
      LOW = LNA1  forced ON  (LNA still needs to be switched in to be used)
      HIZ = TDD Mode: Setting it to HiZ will put the LNA1 bias under the control of PL_CTRL_TDD_TXRX_N
*/
#define RX1_LNA1_MANUAL_BIAS_EN_N       PORT(1,3)     /* P13 */
#define RX1_LNA2_MANUAL_BIAS_EN_N       PORT(1,4)     /* P14 */

#define RX2_LNA1_MANUAL_BIAS_EN_N       PORT(2,3)     /* x22 - P23 */
#define RX2_LNA2_MANUAL_BIAS_EN_N       PORT(2,4)     /* x22 - P24 */
/*********************************************/

/*********************************************
  RX*_SEL - selects which RFIC port is used (A, B, or C)
*/
#define RX1A_SEL                        PORT(1,5)     /* P15 */
#define RX1B_SEL                        PORT(1,6)     /* P16 */
#define RX1C_SEL                        PORT(1,7)     /* P17 */
    
#define RX2A_SEL                        PORT(2,5)     /* x22 - P25 */
#define RX2B_SEL                        PORT(2,6)     /* x22 - P26 */
#define RX2C_SEL                        PORT(2,7)     /* x22 - P27 */
/*********************************************/

#define RX1_LNA_IO_EXP_ADDR              x20_ADDR
#define RX1_SEL_IO_EXP_ADDR              x20_ADDR
#define RX1_SEL_IO_EXP_MASK              (uint32_t)(RX1A_SEL | RX1B_SEL | RX1C_SEL)

#define RX2_LNA_IO_EXP_ADDR              x22_ADDR
#define RX2_SEL_IO_EXP_ADDR              x22_ADDR
#define RX2_SEL_IO_EXP_MASK              (uint32_t)(RX2A_SEL | RX2B_SEL | RX2C_SEL)

#define Z3U_MIN_TX_FREQUENCY_IN_HZ (uint64_t)(0ULL*RATE_MHZ_TO_HZ)
#define Z3U_MAX_TX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)
#define Z3U_MIN_RX_FREQUENCY_IN_HZ (uint64_t)(47ULL*RATE_MHZ_TO_HZ)
#define Z3U_MAX_RX_FREQUENCY_IN_HZ (uint64_t)(6000ULL*RATE_MHZ_TO_HZ)

////////////////////////////
// J1 is RX only (Rx2)
// J2 is TRX (Rx1)
#define RF_PORT_PIN_MASK                (RXTX_ANT1_TOG_EN |    \
                                         RXTX_ANT1_TOG_EN_N |  \
                                         RXTX_AMPS_TOG_EN_N)
// Fixed vs TRx is essentially inverted
#define RF_PORT_FIXED_VALUE             (RXTX_ANT1_TOG_EN_N | RXTX_AMPS_TOG_EN_N) 
#define RF_PORT_FIXED_RX1_VALUE         (RXTX_ANT1_TOG_EN)
#define RF_PORT_TRX_VALUE               (RXTX_ANT1_TOG_EN) 
#define RF_PORT_NONE_VALUE              (RXTX_ANT1_TOG_EN_N | RXTX_AMPS_TOG_EN_N)
////////////////////////////

/*

  TX filter path  (stand-by, <3GHz, >=3GHz)
  - TX_FLT1_SEL (<3GHz)
  - TX_FLT2_SEL (>=3GHz)
  - TX1A_SEL    (>=3GHz)
  - TX1B_SEL    (<3GHz)

  RX filter path  (stand-by, 17 bands)
  - RX1/2A_SEL
  - RX1/2B_SEL
  - RX1/2C_SEL
  - I/O expanders at 0x20 (P2), 0x21 (P0 and P1), and 0x22 (P0 and P1)

  RX port selection  (none, fixed (J1/Rx2), TRX (J2/Rx1) )
  - RXTX_ANT1_TOG_EN    - when high, antenna control under PL_CTRL_TDD_TXRX_N (TRX mode)
  - RXTX_ANT1_TOG_EN_N  - when high, antenna TX only (fixed mode)
  - RXTX_AMPS_TOG_EN_N  - when high, amps under I2C control (fixed mode)

  LNA1 control  (enable, disable)
  - RX1/2_LNA1_MANUAL_BIAS_EN_N
  - RX1/2_LNA1_BYPASS_TOG

  LNA2 control  (enable, disable)
  - RX1/2_LNA2_MANUAL_BIAS_EN_N
  - RX1/2_LNA2_BYPASS_TOG

  PA control  (enable, disable, T/R controlled)
  - TX_AMP_MANUAL_BIAS_ON_N
  - PL_CTRL_TDD_TXRX_N

 */


/***** ENUMS *****/

typedef enum
{
    z3u_pa_disabled,       /* TX_AMP_MANUAL_BIAS_ON_N high */
    z3u_pa_enabled_fixed,  /* TX_AMP_MANUAL_BIAS_ON_N low */
    z3u_pa_enabled_trx,    /* TX_AMP_MANUAL_BIAS_ON_N is HiZ - PA under PL control */

} z3u_rfe_pa_state_t;


/***** TYPE DEFINITIONS *****/
typedef struct
{
    skiq_filt_t filter;
    uint8_t ad9361_port;
    uint16_t low_freq_mhz;
    uint16_t high_freq_mhz;
    uint8_t x20_port0_valu, x20_port0_mask;
    uint8_t x20_port0_hiz;      /* used in skiq_rf_port_config_trx mode only */
    uint8_t x20_port1_valu, x20_port1_mask;
    uint8_t x20_port2_valu;
    uint8_t x21_port0_valu, x21_port1_valu;
    uint8_t x22_port0_valu, x22_port1_valu;
    uint8_t x22_port2_valu, x22_port2_mask;
} filter_selection_t;

typedef struct
{
    uint8_t id;
    uint8_t i2c_addr;
    uint32_t pin_mask;
    uint32_t pin_disable;
    uint32_t pin_enable;
    uint32_t hiz_enable;
} lna_setting_t;

typedef struct
{
    bool valid;
    skiq_rf_port_config_t config;
    bool operation;
} z3u_rf_port_state_t;

typedef struct
{
    bool valid;
    rfe_lna_state_t state;
} z3u_lna_state_t;

typedef struct
{
    bool valid;
    z3u_rfe_pa_state_t state;
} z3u_pa_state_t;

/***** MACROS ******/

#define PORT_to_PIN(_port)                                              \
    (((_port) & (0xFF << 16)) == (_port)) ? (((_port) >> 16) & 0xFF) :  \
    (((_port) & (0xFF <<  8)) == (_port)) ? (((_port) >>  8) & 0xFF) :  \
    (((_port) & (0xFF <<  0)) == (_port)) ? (((_port) >>  0) & 0xFF) : 0

#define RX1_PORT_to_PIN(_rfic_port)                                      \
    ((_rfic_port) == ad9361_rx_port_a) ? PORT_to_PIN(RX1A_SEL) :        \
    ((_rfic_port) == ad9361_rx_port_b) ? PORT_to_PIN(RX1B_SEL) :        \
    ((_rfic_port) == ad9361_rx_port_c) ? PORT_to_PIN(RX1C_SEL) : 0

#define RX2_PORT_to_PIN(_rfic_port)                                      \
    ((_rfic_port) == ad9361_rx_port_a) ? PORT_to_PIN(RX2A_SEL) :        \
    ((_rfic_port) == ad9361_rx_port_b) ? PORT_to_PIN(RX2B_SEL) :        \
    ((_rfic_port) == ad9361_rx_port_c) ? PORT_to_PIN(RX2C_SEL) : 0

#define RX1_SEL_PIN_MASK          (uint8_t)PORT_to_PIN(RX1_SEL_IO_EXP_MASK)
#define RX2_SEL_PIN_MASK          (uint8_t)PORT_to_PIN(RX2_SEL_IO_EXP_MASK)
#define RX_FILT_SEL_(_range,_lo,_hi,_port,_b,_c,_d,_e,_f)               \
    RX_FILT_SEL__(skiq_filt_##_range,_lo,_hi,ad9361_rx_##_port,_b,_c,_d,_e,_f)
#define RX_FILT_SEL__(_filt,_lo,_hi,_rfic_port,_b,_c,_d,_e,_f)          \
    { .filter = _filt,                                                  \
            .ad9361_port = _rfic_port,                                  \
            .low_freq_mhz = (_lo),                                      \
            .high_freq_mhz = (_hi),                                     \
            .x20_port0_valu =    0, .x20_port1_mask = 0x00,             \
            .x20_port1_valu = RX1_PORT_to_PIN(_rfic_port),              \
            .x20_port1_mask = RX1_SEL_PIN_MASK,                         \
            .x20_port2_valu = (_b),                                     \
            .x21_port0_valu = (_c),                                     \
            .x21_port1_valu = (_d),                                     \
            .x22_port0_valu = (_e),                                     \
            .x22_port1_valu = (_f),                                     \
            .x22_port2_valu = RX2_PORT_to_PIN(_rfic_port),              \
            .x22_port2_mask = RX2_SEL_PIN_MASK,                         \
            }

#define _OFF                             0xF0
#define RX_FILT0_(_filt,_lo,_hi)         RX_FILT_SEL_(_filt,_lo,_hi,port_b,0xA2,_OFF,_OFF,_OFF,_OFF)
#define RX_FILT1_(_filt,_lo,_hi,_sel)    RX_FILT_SEL_(_filt,_lo,_hi,port_b,0x62,_sel,_OFF,_OFF,_OFF)
#define RX_FILT2A_(_filt,_lo,_hi,_sel)   RX_FILT_SEL_(_filt,_lo,_hi,port_b,0x25,_OFF,_sel,_OFF,_OFF)
#define RX_FILT2B_(_filt,_lo,_hi,_sel)   RX_FILT_SEL_(_filt,_lo,_hi,port_c,0x25,_OFF,_sel,_OFF,_OFF)
#define RX_FILT3_(_filt,_lo,_hi,_sel)    RX_FILT_SEL_(_filt,_lo,_hi,port_c,0x29,_OFF,_OFF,_sel,_OFF)
#define RX_FILT4_(_filt,_lo,_hi,_sel)    RX_FILT_SEL_(_filt,_lo,_hi,port_a,0x30,_OFF,_OFF,_OFF,_sel)

#define TX_FILT_(_filt,_lo,_hi,_port,_sel,_hiz)                         \
    { .filter = skiq_filt_##_filt,                                      \
            .ad9361_port = ad9361_tx_##_port,                           \
            .low_freq_mhz = (_lo),                                      \
            .high_freq_mhz = (_hi),                                     \
            .x20_port0_valu = (_sel),                                   \
            .x20_port0_mask = 0x1B,             /* bits 0,1,3,4 */      \
            .x20_port0_hiz  = (_hiz),                                   \
            .x20_port1_valu = 0, .x20_port1_mask = 0,                   \
            .x20_port2_valu = 0,                                        \
            .x21_port0_valu = 0,                                        \
            .x21_port1_valu = 0,                                        \
            .x22_port0_valu = 0,                                        \
            .x22_port1_valu = 0,                                        \
            }

#define RF_PORT_STATE_INITIALIZER \
    { .valid = false, .config = skiq_rf_port_config_fixed, .operation = false }

#define LNA_STATE_INITIALIZER \
    { .valid = false, .state = lna_disabled }

#define PA_STATE_INITIALIZER \
    { .valid = false, .state = z3u_pa_disabled }

#define LOCK_RFE(_card)                 CARD_LOCK(rfe_mutex,_card)
#define UNLOCK_RFE(_card)               CARD_UNLOCK(rfe_mutex,_card)

/***** LOCAL DATA *****/
static ARRAY_WITH_DEFAULTS(filter_selection_t *, current_rx_filter, SKIQ_MAX_NUM_CARDS, NULL);
static ARRAY_WITH_DEFAULTS(filter_selection_t *, current_tx_filter, SKIQ_MAX_NUM_CARDS, NULL);

/**
   @brief Mutex for protecting all things RFE
 */
static ARRAY_WITH_DEFAULTS(pthread_mutex_t,
                           rfe_mutex,
                           SKIQ_MAX_NUM_CARDS,
                           PTHREAD_MUTEX_INITIALIZER);

/**
 * @note The only times when RF port configuration is invalid is before rfe_init() and after
 * rfe_release()
 */
static ARRAY_WITH_DEFAULTS(z3u_rf_port_state_t,
                           current_rf_port_state,
                           SKIQ_MAX_NUM_CARDS,
                           RF_PORT_STATE_INITIALIZER);

/**
   @brief track LNA1 and LNA2 states
*/
static z3u_lna_state_t current_lna_state_rx1[SKIQ_MAX_NUM_CARDS][Z3U_NR_LNA] =
{
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [LNA1] = LNA_STATE_INITIALIZER,
        [LNA2] = LNA_STATE_INITIALIZER,
    },
};
static z3u_lna_state_t current_lna_state_rx2[SKIQ_MAX_NUM_CARDS][Z3U_NR_LNA] =
{
    [0 ... (SKIQ_MAX_NUM_CARDS-1)] = {
        [LNA1] = LNA_STATE_INITIALIZER,
        [LNA2] = LNA_STATE_INITIALIZER,
    },
};

/**
   @brief track PA states
 */
static ARRAY_WITH_DEFAULTS(z3u_pa_state_t,
                           current_pa_state,
                           SKIQ_MAX_NUM_CARDS,
                           PA_STATE_INITIALIZER);

/**************************************************************************************************/
static skiq_filt_t available_rx_filters[] =
{
    skiq_filt_47_to_135MHz,
    skiq_filt_135_to_145MHz,
    skiq_filt_145_to_150MHz,
    skiq_filt_150_to_162MHz,
    skiq_filt_162_to_175MHz,
    skiq_filt_175_to_190MHz,
    skiq_filt_190_to_212MHz,
    skiq_filt_212_to_230MHz,
    skiq_filt_230_to_280MHz,
    skiq_filt_280_to_366MHz,
    skiq_filt_366_to_475MHz,
    skiq_filt_475_to_625MHz,
    skiq_filt_625_to_800MHz,
    skiq_filt_800_to_1175MHz,
    skiq_filt_1175_to_1500MHz,
    skiq_filt_1500_to_2100MHz,
    skiq_filt_2100_to_2775MHz,
    skiq_filt_2775_to_3360MHz,
    skiq_filt_3360_to_4600MHz,
    skiq_filt_4600_to_6000MHz,
};

static filter_selection_t rx_filter_standby =
{
    .filter = 0, .low_freq_mhz = 0, .high_freq_mhz = 0,
    .ad9361_port = ad9361_rx_port_a,
    .x20_port0_valu = 0,
    .x20_port0_mask = 0,
    .x20_port1_valu = 0x18, /* RX1_LNA1_MANUAL_BIAS_EN_N | RX1_LNA2_MANUAL_BIAS_EN_N */
    .x20_port1_mask = 0xE0, /* RX1*_SEL */
    .x20_port2_valu = 0,
    .x21_port0_valu = 0,
    .x21_port1_valu = 0,
    .x22_port0_valu = 0,
    .x22_port1_valu = 0,
    .x22_port2_valu = 0x18, /* RX2_LNA1_MANUAL_BIAS_EN_N | RX2_LNA2_MANUAL_BIAS_EN_N */
    .x22_port2_mask = 0xE0, /* RX2*_SEL */
};

static filter_selection_t rx_filter_selections[] =
{
    RX_FILT0_(47_to_135MHz,      47, 135),
    RX_FILT1_(135_to_145MHz,    135, 145, 0x22),
    RX_FILT1_(145_to_150MHz,    145, 150, 0x53),
    RX_FILT1_(150_to_162MHz,    150, 162, 0x77),
    RX_FILT1_(162_to_175MHz,    162, 175, 0x98),
    RX_FILT1_(175_to_190MHz,    175, 190, 0xB9),
    RX_FILT1_(190_to_212MHz,    190, 212, 0xCB),
    RX_FILT1_(212_to_230MHz,    212, 230, 0xDC),
    RX_FILT1_(230_to_280MHz,    230, 280, 0xED),
    RX_FILT1_(280_to_366MHz,    280, 366, 0xFF),
    RX_FILT2A_(366_to_475MHz,   366, 475, 0x04),
    RX_FILT2A_(475_to_625MHz,   475, 625, 0x89),
    RX_FILT2B_(625_to_800MHz,   625, 800, 0xDC),
    RX_FILT2B_(800_to_1175MHz,  800, 1175, 0xFF),
    RX_FILT3_(1175_to_1500MHz, 1175, 1500, 0x26),
    RX_FILT3_(1500_to_2100MHz, 1500, 2100, 0xCB),
    RX_FILT3_(2100_to_2775MHz, 2100, 2775, 0xFE),
    RX_FILT4_(2775_to_3360MHz, 2775, 3360, 0x52),
    RX_FILT4_(3360_to_4600MHz, 3360, 4600, 0xCB),
    RX_FILT4_(4600_to_6000MHz, 4600, 6001, 0xFF),
};

/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */
#if (defined ATE_SUPPORT)
#define FACT_RX_FILT_SEL_(_rfic_port,_fl_sel,_fl1,_fl2,_fl3,_fl4) \
    RX_FILT_SEL__(skiq_filt_max,0,6001,_rfic_port,_fl_sel,_fl1,_fl2,_fl3,_fl4)

/*                                      (FLTR_EN | FLTR23_SEL | FLTRn_SEL | FLTR1_SW) */
#define FLTR1_SEL(_sw)                  (0x20 | 0x00 | 0x02 | (((_sw) & 0x03) << 6))
#define FLTR2_SEL                       (0x20 | 0x01 | 0x04)
#define FLTR3_SEL                       (0x20 | 0x01 | 0x08)
#define FLTR4_SEL                       (0x20 | 0x00 | 0x10)

static filter_selection_t rx_filter_factory = \
    FACT_RX_FILT_SEL_(ad9361_rx_port_a,0,_OFF,_OFF,_OFF,_OFF);

#endif  /* ATE_SUPPORT */
/* =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= SUPPORT =-=-=-= */

static skiq_filt_t available_tx_filters[] =
{
    skiq_filt_0_to_3000_MHz,
    skiq_filt_3000_to_6000_MHz,
};

static filter_selection_t tx_filter_standby =
{
    .filter = 0, .low_freq_mhz = 0, .high_freq_mhz = 0,
    .ad9361_port = ad9361_tx_port_a,
    .x20_port0_valu = 0x00, .x20_port0_mask = 0x1B,
    .x20_port0_hiz  = 0,
    .x20_port1_valu = 0, .x20_port1_mask = 0,
    .x20_port2_valu = 0,
    .x21_port0_valu = 0,
    .x21_port1_valu = 0,
    .x22_port0_valu = 0,
    .x22_port1_valu = 0,
    .x22_port2_valu = 0,
    .x22_port2_mask = 0,
};

static filter_selection_t tx_filter_selections[] =
{
    /**
       @attention, TX_FLT1_SEL, TX_FLT2_SEL, TX1A_SEL, and TX1B_SEL are PORTS, but also happen to be
       in the lowest bank, so their definitions are safe to use here

       @note the filter selections are either asserted (in fixed) or Hi-Z (in trx), so when in
       fixed RF port configuration:
       -  x20_port0_valu & x20_port0_mask => high outputs
       - ~x20_port0_valu & x20_port0_mask => low outputs
       and when in trx RF port configuration:
       -  (x20_port0_valu ^ x20_port0_hiz) & x20_port0_mask => high outputs
       -                   (x20_port0_hiz) & x20_port0_mask => Hi-Z

       Example:
       - x20_port0_valu = 0x11
       - x20_port0_mask = 0x1B
       - x20_port0_hiz  = 0x01
       ====== fixed ======
       - high outputs =>  0x11 & 0x1B => 0x11
       -  low outputs => ~0x11 & 0x1B => 0xEE & 0x1B => 0x0A
       ======= trx =======
       - high outputs =>  (0x11 ^ 0x01) & 0x1B => 0x10 & 0x1B => 0x10
       -   Hi-Z state =>         (0x01) & 0x1B => 0x01
       -  low outputs =>  everything not high / Hi-Z => 0x0A

    */
    TX_FILT_(0_to_3000_MHz,       0, 3000, port_b, TX_FLT1_SEL | TX1B_SEL, TX_FLT1_SEL ),
    TX_FILT_(3000_to_6000_MHz, 3000, 6001, port_a, TX_FLT2_SEL | TX1A_SEL, TX_FLT2_SEL ),
};


/* all necessary information needed to disable / enable the LNA of interest, \
   indexed by lna_idx */
static const lna_setting_t lna_settings_rx1[] =
{
    [0] = {
        .id = 1,
        .i2c_addr = RX1_LNA_IO_EXP_ADDR,
        .pin_mask = RX1_LNA1_MANUAL_BIAS_EN_N | RX1_LNA1_BYPASS_TOG,
        .pin_disable = RX1_LNA1_MANUAL_BIAS_EN_N,
        .pin_enable = RX1_LNA1_BYPASS_TOG,
        .hiz_enable = RX1_LNA1_MANUAL_BIAS_EN_N,
    },
    [1] = {
        .id = 2,
        .i2c_addr = RX1_LNA_IO_EXP_ADDR,
        .pin_mask = RX1_LNA2_MANUAL_BIAS_EN_N | RX1_LNA2_BYPASS_TOG,
        .pin_disable = RX1_LNA2_MANUAL_BIAS_EN_N | RX1_LNA2_BYPASS_TOG,
        .pin_enable = 0,
        .hiz_enable = RX1_LNA2_MANUAL_BIAS_EN_N,
    },
};

static const lna_setting_t lna_settings_rx2[] =
{
    [0] = {
        .id = 1,
        .i2c_addr = RX2_LNA_IO_EXP_ADDR,
        .pin_mask = RX2_LNA1_MANUAL_BIAS_EN_N | RX2_LNA1_BYPASS_TOG,
        .pin_disable = RX2_LNA1_MANUAL_BIAS_EN_N,
        .pin_enable = RX2_LNA1_BYPASS_TOG,
        .hiz_enable = 0,
    },
    [1] = {
        .id = 2,
        .i2c_addr = RX2_LNA_IO_EXP_ADDR,
        .pin_mask = RX2_LNA2_MANUAL_BIAS_EN_N | RX2_LNA2_BYPASS_TOG,
        .pin_disable = RX2_LNA2_MANUAL_BIAS_EN_N | RX2_LNA2_BYPASS_TOG,
        .pin_enable = 0,
        .hiz_enable = 0,
    },
};


/***** DEBUG FUNCTIONS *****/
#if (defined DEBUG_PRINT_ENABLED)
static const char *
z3u_pa_state_cstr( z3u_rfe_pa_state_t state )
{
    const char * p_pa_state_str =
        (state == z3u_pa_disabled) ? "disabled" :
        (state == z3u_pa_enabled_fixed) ? "enabled (fixed)" :
        (state == z3u_pa_enabled_trx) ? "enabled (trx)" :
        "unknown";

    return p_pa_state_str;
}
#endif  /* DEBUG_PRINT_ENABLED */

/***** LOCAL FUNCTIONS *****/

// validates and returns the current port config
static skiq_rf_port_config_t current_rf_port_config( uint8_t card );

// points to current LNA state based on LNA index
static z3u_lna_state_t* current_lna_state( uint8_t card,
                                               uint8_t port,
                                               uint8_t lna_idx );

// returns the current LNA settings
static rfe_lna_state_t z3u_rfe_lna_state( uint8_t card,
                                          uint8_t port,
                                          uint8_t lna_index );

// returns Z3u PA state for card
static z3u_rfe_pa_state_t z3u_rfe_pa_state( uint8_t card );

// translates from RFE PA state to Z3u-specific RFE state
static z3u_rfe_pa_state_t rfe_pa_state_to_z3u( uint8_t card, bool enable );

// helper debug function to print IO expander state
static void _display_ioe( uint8_t card, const char* p_string );

// configures IOE based on RX/TX filter selection
static int32_t _select_rx_filter( uint8_t card, uint8_t chip_id, filter_selection_t *rxfs );
static int32_t _select_tx_filter( uint8_t card, uint8_t chip_id, filter_selection_t *txfs );
//////////////////
// filter/freq lookup functions
static filter_selection_t* _find_filter_by_enum( filter_selection_t *fs_table,
                                                 uint8_t nr_entries,
                                                 skiq_filt_t filter );
static filter_selection_t* _find_rx_filter_by_enum( skiq_filt_t filter );
static filter_selection_t* _find_tx_filter_by_enum( skiq_filt_t filter );
static filter_selection_t* _find_filter_by_freq( filter_selection_t *fs_table,
                                                 uint8_t nr_entries,
                                                 uint64_t freq );
static filter_selection_t* _find_rx_filter_by_freq( uint64_t freq );
static filter_selection_t* _find_tx_filter_by_freq( uint64_t freq );
//////////////////

// returns IOE value based on LNA pin configurations and current state
static uint32_t _lna_cfg_output_values( const lna_setting_t *lna,
                                        rfe_lna_state_t state );

// LNA enable/disable helper functions
static int32_t _disable_both_lna( uint8_t card, uint8_t port );
static int32_t _enable_both_lna( uint8_t card, uint8_t port );

// connects up the RX control signals to the IOE
static int32_t _connect_rx_lineup( uint8_t card, uint8_t port );
// isolates the RX circuitry (used while tuning)
static int32_t _isolate_rx_lineup( uint8_t card,
                                   uint8_t port );
static int32_t _switch_rf_mode( uint8_t card,
                                uint8_t chip_id,
                                skiq_rf_port_config_t mode );

//////////////////////
// Sidekiq RFE functions
static int32_t init( rf_id_t *p_id );
static int32_t release( rf_id_t *p_id );
static int32_t update_tx_pa( rf_id_t *p_id, bool enable );
static int32_t update_rx_lna( rf_id_t *p_id, rfe_lna_state_t new_state );
static int32_t update_rx_lna_by_idx( rf_id_t *p_id,
                                     uint8_t idx,
                                     rfe_lna_state_t new_state );
static int32_t update_before_rx_lo_tune( rf_id_t *p_id,
                                         uint64_t rx_lo_freq );
static int32_t update_after_rx_lo_tune( rf_id_t *p_id,
                                        uint64_t rx_lo_freq );
static int32_t update_before_tx_lo_tune( rf_id_t *p_id,
                                         uint64_t tx_lo_freq );
static int32_t write_rx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t path );
static int32_t write_tx_filter_path( rf_id_t *p_id,
                                     skiq_filt_t path );
static int32_t read_rx_filter_path( rf_id_t *p_id,
                                    skiq_filt_t *p_path );
static int32_t read_tx_filter_path( rf_id_t *p_id,
                                    skiq_filt_t *p_path );
static int32_t read_rx_filter_capabilities( rf_id_t *p_id,
                                            skiq_filt_t *p_filters,
                                            uint8_t *p_num_filters );
static int32_t read_tx_filter_capabilities( rf_id_t *p_id,
                                            skiq_filt_t *p_filters,
                                            uint8_t *p_num_filters );
static int32_t read_rx_rf_ports_avail( rf_id_t *p_id,
                                       uint8_t *p_num_fixed_rf_ports,
                                       skiq_rf_port_t *p_fixed_rf_port_list,
                                       uint8_t *p_num_trx_rf_ports,
                                       skiq_rf_port_t *p_trx_rf_port_list );
static int32_t read_rx_rf_port( rf_id_t *p_id,
                                skiq_rf_port_t *p_rf_port );
static int32_t read_tx_rf_ports_avail( rf_id_t *p_id,
                                       uint8_t *p_num_fixed_rf_ports,
                                       skiq_rf_port_t *p_fixed_rf_port_list,
                                       uint8_t *p_num_trx_rf_ports,
                                       skiq_rf_port_t *p_trx_rf_port_list );
static int32_t read_tx_rf_port( rf_id_t *p_id,
                                skiq_rf_port_t *p_rf_port );
static int32_t rf_port_config_avail( rf_id_t *p_id,
                                     bool *p_fixed,
                                     bool *p_trx );
static int32_t write_rf_port_config( rf_id_t *p_id,
                                     skiq_rf_port_config_t config );
static int32_t read_rf_port_operation( rf_id_t *p_id,
                                       bool *p_transmit );
static int32_t write_rf_port_operation( rf_id_t *p_id,
                                        bool transmit );
static int32_t enable_rx_chan( rf_id_t *p_id,
                               bool enable );
static int32_t write_rx_freq_hop_list( rf_id_t *p_id,
                                       uint16_t nr_freq,
                                       uint64_t freq_list[] );
static int32_t write_tx_freq_hop_list( rf_id_t *p_id,
                                       uint16_t nr_freq,
                                       uint64_t freq_list[] );
static int32_t config_custom_rx_filter( rf_id_t *p_id,
                                        uint8_t filt_index,
                                        uint8_t hpf_lpf_setting,
                                        uint8_t rx_fltr1_sw,
                                        uint8_t rfic_port );

static int32_t read_rf_capabilities( rf_id_t *p_id, 
                                     skiq_rf_capabilities_t *p_rf_capabilities);

//////////////////////

//////////////////////
// Z3u RFE realizations
static int32_t _update_tx_pa( uint8_t card,
                              z3u_rfe_pa_state_t new_state );
static int32_t _update_rx_lna( uint8_t card,
                               uint8_t port,
                               uint8_t lna_index,
                               rfe_lna_state_t new_state );
static int32_t _write_rf_port_operation( uint8_t card,
                                         bool transmit );
static int32_t _write_rf_port_config( rf_id_t *p_id,
                                      skiq_rf_port_config_t config,
                                      bool update_rfe_states );
static int32_t _update_before_rx_lo_tune( rf_id_t *p_id,
                                         uint64_t rx_lo_freq );
static int32_t _update_after_rx_lo_tune( rf_id_t *p_id,
                                         uint64_t rx_lo_freq );
static int32_t _update_before_tx_lo_tune( rf_id_t *p_id,
                                          uint64_t tx_lo_freq );

//////////////////////

static skiq_rf_port_config_t
current_rf_port_config( uint8_t card )
{
    skiq_rf_port_config_t config = skiq_rf_port_config_invalid;

    if ( current_rf_port_state[card].valid )
    {
        config = current_rf_port_state[card].config;
    }

    return config;
}

static z3u_lna_state_t*
current_lna_state( uint8_t card,
                   uint8_t port,
                   uint8_t lna_idx )
{
    z3u_lna_state_t *p_curr_lna_state=NULL;

    if( lna_idx < Z3U_NR_LNA )
    {
        if( port == RX1_PORT )
        {
            p_curr_lna_state = &(current_lna_state_rx1[card][lna_idx]);
        }
        else if( port == RX2_PORT )
        {
            p_curr_lna_state = &(current_lna_state_rx2[card][lna_idx]);
        }
        else
        {
            skiq_error("Invalid port specified (%u) (card=%u)", port, card);
        }
    }
    else
    {
        skiq_error("Invalid LNA index specified (%u) (card=%u)", lna_idx, card);
    }

#ifdef DEBUG_RFE_Z3U_LNA
    if( p_curr_lna_state != NULL )
    {
        debug_print("RFE_Z3U_LNA: current config port=%u, lna_idx=%u, lna_state=%s\n",
                    port, lna_idx, rfe_lna_state_cstr(p_curr_lna_state->state));
    }
#endif

    return (p_curr_lna_state);
}

/**
   @brief Provides current `rfe_lna_state_t` of the specified card and LNA index
 */
static rfe_lna_state_t
z3u_rfe_lna_state( uint8_t card,
                   uint8_t port,
                   uint8_t lna_index )
{
    rfe_lna_state_t z3u_state = lna_disabled;
    z3u_lna_state_t *p_lna_state = NULL;

    if( (p_lna_state=current_lna_state(card, port, lna_index)) != NULL )
    {
        if ( p_lna_state->valid )
        {
            z3u_state = p_lna_state->state;
        }
        else
        {
            skiq_error("Internal Error: current LNA state (%u/%u) not valid, but LNA state "
                       "requested for card %u (port=%u)\n",
                       p_lna_state->valid, p_lna_state->state, card, port);
        }
    }

    return z3u_state;
}

/**
   @brief Provides current `z3u_rfe_pa_state_t` from a specified card's PA state
 */
static z3u_rfe_pa_state_t
z3u_rfe_pa_state( uint8_t card )
{
    z3u_rfe_pa_state_t z3u_state = z3u_pa_disabled;

    if ( current_pa_state[card].valid )
    {
        z3u_state = current_pa_state[card].state;
    }
    else
    {
        skiq_error("Internal Error: PA state not valid, but PA state requested for "
                   "card %u\n", card);
    }

    return z3u_state;
}


/**
   @brief Translates from `bool` to `z3u_rfe_pa_state_t` taking a card's RF port configuration
   into account.
 */
static z3u_rfe_pa_state_t
rfe_pa_state_to_z3u( uint8_t card,
                     bool enable )
{
    z3u_rfe_pa_state_t state=z3u_pa_disabled;

    if ( current_rf_port_state[card].valid )
    {
        if ( enable )
        {
            if ( current_rf_port_state[card].config == skiq_rf_port_config_fixed )
            {
                state = z3u_pa_enabled_fixed;
            }
            else
            {
                state = z3u_pa_enabled_trx;
            }
        }
        else
        {
            state = z3u_pa_disabled;
        }
    }
    else
    {
        skiq_error("Internal Error: RF port configuration not valid, but PA state "
                   "translation requested for card %u\n", card);
        state = z3u_pa_disabled;
    }

    return state;
}


static int32_t
_select_rx_filter( uint8_t card,
                   uint8_t chip_id,
                   filter_selection_t *rxfs )
{
    int32_t status = 0;

    // filter selects
    if ( status == 0 )
    {
        uint32_t x20_mask, x20_values;

        x20_mask =   (rxfs->x20_port0_mask << 0) |
                     (rxfs->x20_port1_mask << 8) |
                     (x20_PORT2_MASK << 16);
        x20_values = (rxfs->x20_port0_valu << 0) |
                     (rxfs->x20_port1_valu << 8) |
                     (rxfs->x20_port2_valu << 16);

        status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x20_ADDR, x20_mask, x20_values );
    }

    // filter band control for Rx2 
    if ( status == 0 )
    {
        uint32_t x22_mask, x22_values;

        x22_mask =   (x22_PORT0_MASK << 0) |
                     (x22_PORT1_MASK << 8) |
                     (rxfs->x22_port2_mask << 16);
        x22_values = (rxfs->x22_port0_valu << 0) |
                     (rxfs->x22_port1_valu << 8) |
                     (rxfs->x22_port2_valu << 16);

        status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x22_ADDR, x22_mask, x22_values );
    }

    // filter band control for Rx1
    if ( status == 0 )
    {
        uint32_t x21_mask, x21_values;

        x21_mask =   (x21_PORT0_MASK << 0) | (x21_PORT1_MASK << 8);
        x21_values = (rxfs->x21_port0_valu << 0) | (rxfs->x21_port1_valu << 8);
        status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x21_ADDR, x21_mask, x21_values );
    }

    _display_ioe( card, __func__ );

    // update the RFIC port selection (updates Rx1 and Rx2)
    if ( status == 0 )
    {
        status = ad9361_write_rx_input_port( chip_id, rxfs->ad9361_port );
    }

    if ( status == 0 )
    {
        current_rx_filter[card] = rxfs;
    }

    return status;
}


static int32_t
_select_tx_filter( uint8_t card,
                   uint8_t chip_id,
                   filter_selection_t *txfs )
{
    int32_t status = 0;

    /* The method used to select the transmit filter depends on the RF port configuration.
     *
     * If it is fixed (FDD), then 1 enables the filter path and 0 disables the filter path.
     *
     * If it is TRX (TDD), the Hi-Z enables the filter path (when the RF port operation is TX), and
     * 0 disables the filter path.
     */
    if ( current_rf_port_state[card].valid )
    {
        if ( current_rf_port_state[card].config == skiq_rf_port_config_fixed )
        {
            status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x20_ADDR,
                                                    txfs->x20_port0_mask, txfs->x20_port0_valu );
        }
        else
        {
            /* pins in x20_port0_mask should either be Hi-Z or low output */
            status = pcal6524_io_exp_set_as_hiz( card, Z3U_IOE_I2C_BUS, x20_ADDR,
                                                 txfs->x20_port0_mask,
                                                 txfs->x20_port0_hiz,
                                                 txfs->x20_port0_hiz ^ txfs->x20_port0_valu /* high_mask */ );
        }
    }
    else
    {
        skiq_error("Internal error, TX filter selection without RF port configured on card %u\n", card);
        status = -EPROTO;
    }

    _display_ioe( card, __func__ );

    if ( status == 0 )
    {
        status = ad9361_write_tx_output_port( chip_id, txfs->ad9361_port );
    }

    if ( status == 0 )
    {
        current_tx_filter[card] = txfs;
    }

    return status;
}


static filter_selection_t *
_find_filter_by_enum( filter_selection_t *fs_table,
                      uint8_t nr_entries,
                      skiq_filt_t filter )
{
    uint8_t i;
    filter_selection_t *fs_entry = NULL;

    for (i = 0; ( i < nr_entries ) && ( fs_entry == NULL ); i++)
    {
        if ( fs_table[i].filter == filter )
        {
            fs_entry = &(fs_table[i]);
        }
    }

    return fs_entry;
}


static filter_selection_t *
_find_rx_filter_by_enum( skiq_filt_t filter )
{
    return _find_filter_by_enum( rx_filter_selections, ARRAY_SIZE(rx_filter_selections), filter );
}


static filter_selection_t *
_find_tx_filter_by_enum( skiq_filt_t filter )
{
    return _find_filter_by_enum( tx_filter_selections, ARRAY_SIZE(tx_filter_selections), filter );
}


static filter_selection_t *
_find_filter_by_freq( filter_selection_t *fs_table,
                      uint8_t nr_entries,
                      uint64_t freq )
{
    uint8_t i;
    filter_selection_t *fs_entry = NULL;

    TRACE_ENTER;
    TRACE_ARGS("fs_table: %p, nr_entries: %u, freq: %" PRIu64 "\n", fs_table, nr_entries, freq);

    for (i = 0; ( i < nr_entries ) && ( fs_entry == NULL ); i++)
    {
        if ( ( (RATE_MHZ_TO_HZ*(uint64_t)(fs_table[i].low_freq_mhz)) <= freq ) &&
             ( freq < (RATE_MHZ_TO_HZ*(uint64_t)(fs_table[i].high_freq_mhz)) ) )
        {
            fs_entry = &(fs_table[i]);
        }
    }

    return fs_entry;
}

static filter_selection_t *
_find_rx_filter_by_freq( uint64_t freq )
{
    return _find_filter_by_freq( rx_filter_selections, ARRAY_SIZE(rx_filter_selections), freq );
}

static filter_selection_t *
_find_tx_filter_by_freq( uint64_t freq )
{
    return _find_filter_by_freq( tx_filter_selections, ARRAY_SIZE(tx_filter_selections), freq );
}


static uint32_t
_lna_cfg_output_values( const lna_setting_t *lna,
                        rfe_lna_state_t state )
{
    return (state == lna_disabled) ? lna->pin_disable :
           (state == lna_enabled) ? lna->pin_enable :
           lna->pin_disable;
}


/**
///////////////////////////
    LNA1
      BT = BypassToggle D1 => x20 P1 Rx1 / x22 P2 Rx2
      MB = ManualBias   D3 => x20 P1 Rx1 / x22 P2 Rx2

                Fixed                    TRX
    Disabled    BT=1, MB=1               BT=1, MB=1
    Enabled     BT=1, MB=0               BT=1, MB=Z

//////////////////////////
    LNA2
      BT = BypassToggle D2 => x20 P1 Rx1 / x22 P2 Rx2
      MB = ManualBias   D4 => x20 P1 Rx1 / x22 P2 Rx2
                Fixed                    TRX
    Disabled    BT=0/Z, MB=1             BT=0/Z, MB=1
    Enabled     BT=0/Z, MB=0             BT=0/Z, MB=Z
 */
static int32_t
_update_rx_lna( uint8_t card,
                uint8_t port,
                uint8_t lna_index,
                rfe_lna_state_t new_state )
{
    int32_t status = 0;
    z3u_lna_state_t *p_lna_state; 
    const lna_setting_t *lna_rx;
    bool b_use_hiz=false;

    debug_print("RFE_Z3U_LNA: LNA requested new state=%s  / port=%u / index=%u\n",
                rfe_lna_state_cstr(new_state), port, lna_index);

    if( (p_lna_state=current_lna_state(card, port, lna_index)) != NULL )
    {
        if ( !p_lna_state->valid ||
             ( new_state != p_lna_state->state ) )
        {
            /* RX1 requires special attention since the RF port config (fixed or trx) matters here in 
               how we control the LNA state (we can use MB directly or put it in hi-z and update with PL pins) */
            if( port == RX1_PORT )
            {
                lna_rx = &(lna_settings_rx1[lna_index]);
                // if we're in TRx mode, we need to use hi-z, the RF port should already be configured properly
                if( (current_rf_port_state[card].config == skiq_rf_port_config_trx) )
                {
                    b_use_hiz = true;
                    status = pcal6524_io_exp_set_as_hiz( card,
                                                         Z3U_IOE_I2C_BUS,
                                                         lna_rx->i2c_addr,
                                                         lna_rx->pin_mask,
                                                         lna_rx->hiz_enable,
                                                         ((_lna_cfg_output_values(lna_rx, new_state)) & (~lna_rx->hiz_enable)) );
                    if( status == 0 )
                    {
                        switch (new_state)
                        {
                            case lna_bypassed:
                                skiq_error("LNA bypass mode not currently supported (card=%u)", card);
                                status = -ENOTSUP;
                                break;
  
                            case lna_enabled:
                                /* configure to "receive" if not transmitting */
                                if( current_rf_port_state[card].operation == false )
                                {
                                    status = sidekiq_fpga_reg_RMWV( card, 0, PL_CTRL_TDD_TXRX_N, FPGA_REG_GPIO_WRITE );
                                }
                                break;

                            case lna_disabled:
                                /* gotta switch off the LNAs by setting this high (to transmit mode) */
                                status = sidekiq_fpga_reg_RMWV( card, 1, PL_CTRL_TDD_TXRX_N, FPGA_REG_GPIO_WRITE );
                                break;

                            default:
                                skiq_error("Unhandled LNA state (%u) requested on card %u\n", new_state, card);
                                status = -EINVAL;
                                break;
                        }
                    }
                }
                else
                {
                    // if we're in fixed mode, we need to configure the RF port for fixed + RX1 (control ANT1 signals)
                    status = pcal6524_io_exp_set_as_output( card, 
                                                            Z3U_IOE_I2C_BUS, 
                                                            x20_ADDR, 
                                                            RF_PORT_PIN_MASK, 
                                                            RF_PORT_FIXED_RX1_VALUE );
                }
            }
            else if( port == RX2_PORT )
            {
                lna_rx = &(lna_settings_rx2[lna_index]);
            }
            else
            {
                // not a valid port, nothing to do
                status = -EINVAL;
            }

            if( (status == 0) && (b_use_hiz == false) )
            {
                switch (new_state)
                {
                    case lna_bypassed:
                        skiq_error("LNA bypass mode not currently supported (card=%u)", card);
                        status = -ENOTSUP;
                        break;
  
                    case lna_enabled:
                    /* intentional fall-thru */
                    case lna_disabled:
                        status = pcal6524_io_exp_set_as_output( card,
                                                                Z3U_IOE_I2C_BUS,
                                                                lna_rx->i2c_addr,
                                                                lna_rx->pin_mask,
                                                                _lna_cfg_output_values( lna_rx, new_state ) );
                        break;

                    default:
                        skiq_error("Unhandled LNA state (%u) requested on card %u\n", new_state, card);
                        status = -EINVAL;
                        break;
                }
            }

            if ( status == 0 )
            {
                p_lna_state->state = new_state;
                p_lna_state->valid = true;

#ifdef DEBUG_RFE_Z3U_LNA
                debug_print("RFE_Z3U_LNA: LNA%u updated successfully to %s for port %u\n",
                            lna_rx->id, rfe_lna_state_cstr(new_state), port);
#endif
                _display_ioe( card, __func__ );
            }
        }        
    }
    else
    {
        // no such LNA
        status = -ENODEV;
    }

    return status;
}


static int32_t
_disable_both_lna( uint8_t card, uint8_t port )
{
    int32_t status = 0;

    status = _update_rx_lna( card, port, LNA1, lna_disabled );
    if ( status == 0 )
    {
        status = _update_rx_lna( card, port, LNA2, lna_disabled );
    }

    return status;
}

static int32_t
_enable_both_lna( uint8_t card, uint8_t port )
{
    int32_t status = 0;

    status = _update_rx_lna( card, port, LNA1, lna_enabled );
    if ( status == 0 )
    {
        status = _update_rx_lna( card, port, LNA2, lna_enabled );
    }

    return status;
}

/**
    PA
                Fixed      TRX
    Disabled    MB=1       MB=1
    Enabled     MB=0       MB=Z
 */
static int32_t
_update_tx_pa( uint8_t card,
              z3u_rfe_pa_state_t new_state )
{
    int32_t status = 0;

    if ( !current_pa_state[card].valid || ( new_state != current_pa_state[card].state ) )
    {
        debug_print("Updating PA to %s on card %u\n", z3u_pa_state_cstr( new_state ), card );
        switch (new_state)
        {
            case z3u_pa_disabled:
                status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x20_ADDR, TX_AMP_MANUAL_BIAS_ON_N,
                                                        TX_AMP_MANUAL_BIAS_ON_N );
                break;

            case z3u_pa_enabled_fixed:
                status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x20_ADDR, TX_AMP_MANUAL_BIAS_ON_N, 0 );
                break;

            case z3u_pa_enabled_trx:
                status = pcal6524_io_exp_set_as_hiz( card, Z3U_IOE_I2C_BUS, x20_ADDR, TX_AMP_MANUAL_BIAS_ON_N,
                                                     TX_AMP_MANUAL_BIAS_ON_N, 0 );
                break;

            default:
                skiq_error("Internal Error: Unhandled PA state (%u) requested on card %u\n",
                           new_state, card);
                status = -EINVAL;
                break;
        }

        if ( status == 0 )
        {
            current_pa_state[card].state = new_state;
            current_pa_state[card].valid = true;
        }
    }

    _display_ioe( card, __func__ );

    return status;
}

static int32_t
_connect_rx_lineup( uint8_t card, uint8_t port )
{
    int32_t status = 0;

    if ( status == 0 )
    {
        uint32_t rx1_sel = current_rx_filter[card]->x20_port1_valu << 8;
        uint32_t rx2_sel = current_rx_filter[card]->x22_port2_valu << 16;
        /*
          Set I2C_CTRL_RX1/2A_SEL or I2C_CTRL_RX1/2B_SEL or I2C_CTRL_RX1/2C_SEL = HIGH as appropriate

          Use the 'current RX filter' pointer to determine which RX port should be selected.
         */
        status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, RX1_SEL_IO_EXP_ADDR,
                                                RX1_SEL_IO_EXP_MASK, rx1_sel );
        if( status == 0 )
        {
            status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, RX2_SEL_IO_EXP_ADDR,
                                                    RX2_SEL_IO_EXP_MASK, rx2_sel );
        }
    }

    if ( status == 0 )
    {
        status = _enable_both_lna( card, port );
    }

    _display_ioe( card, __func__ );

    return status;
}


static int32_t
_isolate_rx_lineup( uint8_t card,
                    uint8_t port )
{
    int32_t status=0;
    
    if ( status == 0 )
    {
        /* disable LNAs if they are enabled, leave them alone if they are bypassed */
        status = _disable_both_lna( card, port );
    }

    _display_ioe( card, __func__ );

    return status;
}


/*************************************************************************************************/
static int32_t
_write_rf_port_operation( uint8_t card,
                          bool transmit )
{
    int32_t status = 0;

    /**
       Set GPO PL_CTRL_TDD_TXRX_N to 0 (@a transmit is false) or 1 (@a transmit is true)

       @note PL_CTRL_TDD_TXRX_N in FPGA design is output-only
    */
    status = sidekiq_fpga_reg_RMWV( card, (transmit) ? 1 : 0, PL_CTRL_TDD_TXRX_N,
                                    FPGA_REG_GPIO_WRITE );
    if ( status == 0 )
    {
        current_rf_port_state[card].operation = transmit;
    }

    _display_ioe( card, __func__ );

    return status;
}


/*************************************************************************************************/
static int32_t
_switch_rf_mode( uint8_t card,
                 uint8_t chip_id,
                 skiq_rf_port_config_t mode )
{
    int32_t status = 0;

    /* Configure RFFE for Receive on 'to_port' */
    if ( status == 0 )
    {
        uint32_t pin_mask;
        uint32_t output_mask;

        switch (mode)
        {
            case skiq_rf_port_config_fixed:
                pin_mask = RF_PORT_PIN_MASK;
                output_mask = RF_PORT_FIXED_VALUE;
                break;

            case skiq_rf_port_config_trx:
                // intentional fall-thru
            case skiq_rf_port_config_tdd:
                pin_mask = RF_PORT_PIN_MASK;
                output_mask = RF_PORT_TRX_VALUE;
                break;

            default:
                pin_mask = RF_PORT_PIN_MASK;
                output_mask = RF_PORT_NONE_VALUE;
                break;
        }

        if ( status == 0 )
        {
            status = pcal6524_io_exp_set_as_output( card, Z3U_IOE_I2C_BUS, x20_ADDR, pin_mask, output_mask );
        }
    }

    _display_ioe( card, __func__ );

    return status;
}


/*************************************************************************************************/
static int32_t
_write_rf_port_config( rf_id_t *p_id,
                       skiq_rf_port_config_t config,
                       bool update_rfe_states )
{
    int32_t status = 0;

    switch (config)
    {
        case skiq_rf_port_config_fixed:
            status = _switch_rf_mode( p_id->card, p_id->chip_id, skiq_rf_port_config_fixed );
            break;

        case skiq_rf_port_config_trx:
            status = _switch_rf_mode( p_id->card, p_id->chip_id, skiq_rf_port_config_trx );
            break;

        default:
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        /* store RF port configuration and mark it valid for the time being */
        current_rf_port_state[p_id->card].config = config;
        current_rf_port_state[p_id->card].valid = true;

        /* Default RF port operation to 'receive', even in fixed mode, the FPGA (PL) output needs to
         * be in a well-defined state */
        status = _write_rf_port_operation( p_id->card, false /* receive */ );
    }

    /* Update Tx PA state to its current state, possibly taking a RF port configuration into
     * consideration */
    if ( update_rfe_states && ( status == 0 ) )
    {
        status = _update_tx_pa( p_id->card, z3u_rfe_pa_state( p_id->card ) );
    }

    /* Update RX LNA1 state to its current state, possibly taking an RF port configuration into
     * consideration */
    if ( update_rfe_states && ( status == 0 ) )
    {
        status = _update_rx_lna( p_id->card,
                                 p_id->port,
                                 LNA1,
                                 z3u_rfe_lna_state( p_id->card, p_id->port, LNA1 ) );
    }

    /* Update RX LNA2 state to its current state, possibly taking an RF port configuration into
     * consideration */
    if ( update_rfe_states && ( status == 0 ) )
    {
        status = _update_rx_lna( p_id->card,
                                 p_id->port,
                                 LNA2,
                                 z3u_rfe_lna_state( p_id->card, p_id->port, LNA2 ) );
    }

    /* if it turns out that writing the various elements of the RF port configuration did not
     * succeed, mark the RF port state as not valid */
    if ( status != 0 )
    {
        current_rf_port_state[p_id->card].valid = false;
    }

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
_update_before_rx_lo_tune( rf_id_t *p_id,
                           uint64_t rx_lo_freq )
{
    int32_t status = 0;

    if ( status == 0 )
    {
        status = _isolate_rx_lineup( p_id->card,
                                     p_id->port );
    }

    if ( status == 0 )
    {
        filter_selection_t *rxfs;

        rxfs = _find_rx_filter_by_freq( rx_lo_freq );
        if ( rxfs == NULL )
        {
            status = -EINVAL;
        }
        else
        {
            debug_print("Selecting RX filter %s on card %u for LO frequency %" PRIu64 "\n",
                        SKIQ_FILT_STRINGS[ rxfs->filter ], p_id->card, rx_lo_freq);
            status = _select_rx_filter( p_id->card, p_id->chip_id, rxfs );
        }
    }

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency AFTER the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
_update_after_rx_lo_tune( rf_id_t *p_id,
                          uint64_t rx_lo_freq )
{
    int32_t status = 0;

    status = _connect_rx_lineup( p_id->card, p_id->port );

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Tx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param tx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
_update_before_tx_lo_tune( rf_id_t *p_id,
                          uint64_t tx_lo_freq )
{
    int32_t status = 0;
    filter_selection_t *txfs;

    txfs = _find_tx_filter_by_freq( tx_lo_freq );
    if ( txfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting TX filter %s on card %u for LO frequency %" PRIu64 "\n",
                    SKIQ_FILT_STRINGS[ txfs->filter ], p_id->card, tx_lo_freq);
        status = _select_tx_filter( p_id->card, p_id->chip_id, txfs );
    }

    return status;
}


/*************************************************************************************************/
/***** RFE FUNCTIONS *****/
/*************************************************************************************************/

/*************************************************************************************************/
/** @brief Enables or disables the Tx power amplifier.

    @param p_id Pointer to RF identifier struct.
    @param new_state New state for the PA

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_tx_pa( rf_id_t *p_id,
              bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    status = _update_tx_pa( p_id->card, rfe_pa_state_to_z3u( p_id->card, enable ) );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Enables / disables / bypasses all Rx low noise amplifiers in the RFE.

    @param p_id Pointer to RF identifier struct.
    @param new_state [::rfe_lna_state_t] New state for all LNAs

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_rx_lna( rf_id_t *p_id,
               rfe_lna_state_t new_state )
{
    int32_t status = 0;
    uint8_t card = p_id->card;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("new_state: %s\n", rfe_lna_state_cstr( new_state ));

    LOCK_RFE(card);

    status = _update_rx_lna( card, p_id->port, LNA1, new_state );
    if ( status == 0 )
    {
        status = _update_rx_lna( card, p_id->port, LNA2, new_state );
    }

    UNLOCK_RFE(card);

    TRACE_EXIT_STATUS(status);

    return status;
}

static int32_t
update_rx_lna_by_idx( rf_id_t *p_id,
                      uint8_t idx,
                      rfe_lna_state_t new_state )
{
    int32_t status = 0;
    uint8_t card = p_id->card;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("new_state: %s\n", rfe_lna_state_cstr( new_state ));

    LOCK_RFE(card);

    if ( idx == 0 )
    {
        status = _update_rx_lna( card,
                                 p_id->port,
                                 LNA1,
                                 new_state );
    }
    else if ( idx == 1 )
    {
        status = _update_rx_lna( card,
                                 p_id->port,
                                 LNA2,
                                 new_state );
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_before_rx_lo_tune( rf_id_t *p_id,
                          uint64_t rx_lo_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rx_lo_freq: %" PRIu64 "\n", rx_lo_freq);

    LOCK_RFE(p_id->card);

    status = _update_before_rx_lo_tune( p_id, rx_lo_freq );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Rx LO frequency AFTER the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param rx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_after_rx_lo_tune( rf_id_t *p_id,
                         uint64_t rx_lo_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("rx_lo_freq: %" PRIu64 "\n", rx_lo_freq);

    LOCK_RFE(p_id->card);

    status = _update_after_rx_lo_tune( p_id, rx_lo_freq );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Updates the radio front end according to a given Tx LO frequency BEFORE the RFIC tune
    operation has occurred.

    @param p_id Pointer to RF identifier struct.
    @param tx_lo_freq The LO frequency to configure for.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
update_before_tx_lo_tune( rf_id_t *p_id,
                          uint64_t tx_lo_freq )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("tx_lo_freq: %" PRIu64 "\n", tx_lo_freq);

    LOCK_RFE(p_id->card);

    status = _update_before_tx_lo_tune( p_id, tx_lo_freq );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Configures the Rx filter configuration.

    @param p_id Pointer to RF identifier struct.
    @param path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
write_rx_filter_path( rf_id_t *p_id,
                      skiq_filt_t path )
{
    int32_t status = 0;
    filter_selection_t *rxfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    rxfs = _find_rx_filter_by_enum( path );
    if ( rxfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting RX filter %s on card %u as requested\n",
                    SKIQ_FILT_STRINGS[ rxfs->filter ], p_id->card);
        status = _select_rx_filter( p_id->card, p_id->chip_id, rxfs );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Configures the Tx filter configuration for this RFE

    @param[in] p_id Pointer to RF identifier struct.
    @param[in] path Filter path to select.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
write_tx_filter_path( rf_id_t *p_id,
                      skiq_filt_t path )
{
    int32_t status = 0;
    filter_selection_t *txfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    txfs = _find_tx_filter_by_enum( path );
    if ( txfs == NULL )
    {
        status = -EINVAL;
    }
    else
    {
        debug_print("Selecting TX filter %s on card %u as requested\n",
                    SKIQ_FILT_STRINGS[ txfs->filter ], p_id->card);
        status = _select_tx_filter( p_id->card, p_id->chip_id, txfs );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the current Rx filter configuration.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
read_rx_filter_path( rf_id_t *p_id,
                     skiq_filt_t *p_path )
{
    int32_t status = 0;
    filter_selection_t *rxfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    rxfs = current_rx_filter[p_id->card];
    if ( rxfs == NULL )
    {
        status = -ERANGE;
    }
    else
    {
        *p_path = rxfs->filter;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the current Tx filter configuration.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_path Pointer to be updated with configured filter.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
read_tx_filter_path( rf_id_t *p_id,
                     skiq_filt_t *p_path )
{
    int32_t status = 0;
    filter_selection_t *txfs;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    txfs = current_tx_filter[p_id->card];
    if ( txfs == NULL )
    {
        status = -ERANGE;
    }
    else
    {
        *p_path = txfs->filter;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the available Rx filters.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_filters Pointer to be updated with available filters.
    @param[out] p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
read_rx_filter_capabilities( rf_id_t *p_id,
                             skiq_filt_t *p_filters,
                             uint8_t *p_num_filters )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    *p_num_filters = ARRAY_SIZE(available_rx_filters);
    memcpy( p_filters, available_rx_filters, sizeof( available_rx_filters ) );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Obtains the available Tx filters.

    @param[in] p_id Pointer to RF identifier struct.
    @param[out] p_filters Pointer to be updated with available filters.
    @param[out] p_num_filters Number of filters available.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
read_tx_filter_capabilities( rf_id_t *p_id,
                             skiq_filt_t *p_filters,
                             uint8_t *p_num_filters )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    *p_num_filters = ARRAY_SIZE(available_tx_filters);
    memcpy( p_filters, available_tx_filters, sizeof( available_tx_filters ) );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
read_rx_rf_ports_avail( rf_id_t *p_id,
                        uint8_t *p_num_fixed_rf_ports,
                        skiq_rf_port_t *p_fixed_rf_port_list,
                        uint8_t *p_num_trx_rf_ports,
                        skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_num_fixed_rf_ports = 0;
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J1;

        *p_num_trx_rf_ports = 0;
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J2;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_num_fixed_rf_ports = 0;
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J2;
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
read_rx_rf_port( rf_id_t *p_id,
                         skiq_rf_port_t *p_rf_port )
{
    int32_t status = 0;
    skiq_rf_port_config_t config = current_rf_port_config( p_id->card );

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_rx_hdl_A1 )
    {
        if( config == skiq_rf_port_config_fixed )
        {
            *p_rf_port = skiq_rf_port_J1;
        }
        else
        {
            *p_rf_port = skiq_rf_port_J2;
        }
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        if( config == skiq_rf_port_config_fixed )
        {
            *p_rf_port = skiq_rf_port_J2;
        }
        else
        {
            status = -EPROTO;
        }
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

/*************************************************************************************************/
static int32_t
read_tx_rf_ports_avail( rf_id_t *p_id,
                        uint8_t *p_num_fixed_rf_ports,
                        skiq_rf_port_t *p_fixed_rf_port_list,
                        uint8_t *p_num_trx_rf_ports,
                        skiq_rf_port_t *p_trx_rf_port_list )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_num_fixed_rf_ports = 0;
        p_fixed_rf_port_list[(*p_num_fixed_rf_ports)++] = skiq_rf_port_J2;

        *p_num_trx_rf_ports = 0;
        p_trx_rf_port_list[(*p_num_trx_rf_ports)++] = skiq_rf_port_J2;
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
read_tx_rf_port( rf_id_t *p_id,
                 skiq_rf_port_t *p_rf_port )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_rf_port = skiq_rf_port_J2;
    }
    else
    {
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
rf_port_config_avail( rf_id_t *p_id,
                      bool *p_fixed,
                      bool *p_trx )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if ( p_id->hdl == skiq_rx_hdl_A1 )
    {
        *p_fixed = true;
        *p_trx = true;
    }
    else if( p_id->hdl == skiq_rx_hdl_A2 )
    {
        *p_fixed = true;
        *p_trx = false;
    }
    else
    {
        // invalid
        status = -EINVAL;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
read_rf_port_operation( rf_id_t *p_id,
                        bool *p_transmit )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    *p_transmit = current_rf_port_state[p_id->card].operation;

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
write_rf_port_operation( rf_id_t *p_id,
                         bool transmit )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    status = _write_rf_port_operation( p_id->card, transmit );

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
write_rf_port_config( rf_id_t *p_id,
                      skiq_rf_port_config_t config )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("config: %s\n", rf_port_config_cstr(config));

    LOCK_RFE(p_id->card);

    switch (config)
    {
        case skiq_rf_port_config_fixed:
        case skiq_rf_port_config_trx:
            debug_print("Updating RF port config to %s on card %u\n", rf_port_config_cstr( config ),
                        p_id->card );
            status = _write_rf_port_config( p_id, config, true /* update_rfe_states */ );
            break;

        default:
            skiq_error("Unhandled RF port configuration (%u) requested on card %u\n", config,
                       p_id->card);
            status = -EINVAL;
            break;
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
enable_rx_chan( rf_id_t *p_id,
                bool enable )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    if ( enable )
    {
        status = _enable_both_lna( p_id->card, p_id->port );
    }
    else
    {
        status = _disable_both_lna( p_id->card, p_id->port );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
write_rx_freq_hop_list( rf_id_t *p_id,
                        uint16_t nr_freq,
                        uint64_t freq_list[] )
{
    int32_t status = 0;
    uint16_t i = 0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;
    uint64_t freq = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("nr_freq: %u\n", nr_freq);

    if ( nr_freq == 0 )
    {
        return -EINVAL;
    }

    LOCK_RFE(p_id->card);

    for ( i = 0; ( i < nr_freq ) && ( status == 0 ); i++ )
    {
        filter_selection_t *pfs;

        pfs = _find_rx_filter_by_freq( freq_list[i] );
        if ( pfs == NULL )
        {
            status = -EINVAL;
        }
        else if ( pfs->filter == skiq_filt_invalid )
        {
            status = -EPROTO;
        }
        else if ( ( skiq_filt != skiq_filt_invalid ) &&
                  ( skiq_filt != pfs->filter ) )
        {
            status = -EINVAL;
            skiq_error("All frequencies in the Rx hop list must be within a single frequency "
                       "filter band for Sidekiq %s on card %u\n",
                       part_cstr( _skiq_get_part( p_id->card ) ), p_id->card);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq,
                       SKIQ_FILT_STRINGS[skiq_filt]);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq_list[i],
                       SKIQ_FILT_STRINGS[pfs->filter]);
        }
        else
        {
            skiq_filt = pfs->filter;
            freq = freq_list[i];
        }
    }

    /* if the frequencies are within a band, then actually apply it.  Since we know all of the
     * frequencies use the same filter setting, we can just set RFE for the first frequency in the
     * hop list */
    if ( status == 0 )
    {
        status = _update_before_rx_lo_tune( p_id, freq_list[0] );
    }

    if ( status == 0 )
    {
        status = _update_after_rx_lo_tune( p_id, freq_list[0] );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
static int32_t
write_tx_freq_hop_list( rf_id_t *p_id,
                        uint16_t nr_freq,
                        uint64_t freq_list[] )
{
    int32_t status = 0;
    uint16_t i = 0;
    skiq_filt_t skiq_filt = skiq_filt_invalid;
    uint64_t freq = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);
    TRACE_ARGS("nr_freq: %u\n", nr_freq);

    if ( nr_freq == 0 )
    {
        return -EINVAL;
    }

    LOCK_RFE(p_id->card);

    for ( i = 0; ( i < nr_freq ) && ( status == 0 ); i++ )
    {
        filter_selection_t *pfs;

        pfs = _find_tx_filter_by_freq( freq_list[i] );
        if ( pfs == NULL )
        {
            status = -EINVAL;
        }
        else if ( pfs->filter == skiq_filt_invalid )
        {
            status = -EPROTO;
        }
        else if ( ( skiq_filt != skiq_filt_invalid ) &&
                  ( skiq_filt != pfs->filter ) )
        {
            status = -EINVAL;
            skiq_error("All frequencies in the Tx hop list must be within a single frequency "
                       "filter band for Sidekiq %s on card %u\n",
                       part_cstr( _skiq_get_part( p_id->card ) ), p_id->card);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq,
                       SKIQ_FILT_STRINGS[skiq_filt]);
            skiq_error(" - Frequency %10" PRIu64 " Hz is in filter %s\n", freq_list[i],
                       SKIQ_FILT_STRINGS[pfs->filter]);
        }
        else
        {
            skiq_filt = pfs->filter;
            freq = freq_list[i];
        }
    }

    /* if the frequencies are within a band, then actually apply it.  Since we know all of the
     * frequencies use the same filter setting, we can just set RFE for the first frequency in the
     * hop list */
    if ( status == 0 )
    {
        status = _update_before_tx_lo_tune( p_id, freq_list[0] );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Initializes the RFE for the M.2 2280 card.

    @param p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
init( rf_id_t *p_id )
{
    int32_t status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 0. Unconditionally unset both LNA valid flags, Tx PA state valid flag and RF port
     * configuration valid flag, nothing can be trusted during initialization */
    current_lna_state_rx1[p_id->card][LNA1].valid = false;
    current_lna_state_rx1[p_id->card][LNA2].valid = false;
    current_lna_state_rx2[p_id->card][LNA1].valid = false;
    current_lna_state_rx2[p_id->card][LNA2].valid = false;
    current_pa_state[p_id->card].valid = false;
    current_rf_port_state[p_id->card].valid = false;

    /* 1. Set default RF port configuration for fixed */
    if ( status == 0 )
    {
        status = _write_rf_port_config( p_id, skiq_rf_port_config_fixed,
                                        false /* update_rfe_states */ );
    }

    /* 1a. Set receive filter to standby */
    if ( status == 0 )
    {
        debug_print("Selecting RX filter 'standby' on card %u\n", p_id->card);
        status = _select_rx_filter( p_id->card, p_id->chip_id, &rx_filter_standby );
    }

    /* 1b. Set transmit filter to standby */
    if ( status == 0 )
    {
        debug_print("Selecting TX filter 'standby' on card %u\n", p_id->card);
        status = _select_tx_filter( p_id->card, p_id->chip_id, &tx_filter_standby );
    }

    /* 2. Disable both LNAs */
    if ( status == 0 )
    {
        status = _disable_both_lna( p_id->card, RX1_PORT );
    }
    if( status == 0 )
    {
        status = _disable_both_lna( p_id->card, RX2_PORT );
    }

    /* 3. Disable Tx PA */
    if ( status == 0 )
    {
        status = _update_tx_pa( p_id->card, z3u_pa_disabled );
    }

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}


/*************************************************************************************************/
/** @brief Shuts down the RFE for the M.2 2280 card.

    @param p_id Pointer to RF identifier struct.

    @return int32_t status where 0=success, anything else is an error
*/
static int32_t
release( rf_id_t *p_id )
{
    int32_t status = 0, op_status = 0;

    TRACE_ENTER;
    TRACE_ARGS("p_id: %p, p_id->card: %u, p_id->hdl: %u\n", p_id, p_id->card, p_id->hdl);

    LOCK_RFE(p_id->card);

    /* 1. Set RF port configuration to 'fixed' */
    op_status = _write_rf_port_config( p_id, skiq_rf_port_config_fixed,
                                       false /* update_rfe_states */ );
    status = NONZERO(op_status,status);

    /* 2. Disable Tx PA */
    op_status = _update_tx_pa( p_id->card, z3u_pa_disabled );
    status = NONZERO(op_status,status);

    /* 3. Disable both LNAs */
    op_status = _disable_both_lna( p_id->card, RX1_PORT );
    status = NONZERO(op_status,status);
    op_status = _disable_both_lna( p_id->card, RX2_PORT );
    status = NONZERO(op_status,status);

    /* 4a. Set receive filter to standby */
    debug_print("Selecting RX filter 'standby' on card %u\n", p_id->card);
    op_status = _select_rx_filter( p_id->card, p_id->chip_id, &rx_filter_standby );
    status = NONZERO(op_status,status);

    /* 4b. Set transmit filter to standby */
    debug_print("Selecting TX filter 'standby' on card %u\n", p_id->card);
    op_status = _select_tx_filter( p_id->card, p_id->chip_id, &tx_filter_standby );
    status = NONZERO(op_status,status);

    /* 5. Unconditionally unset both LNA state valid flags, Tx PA state valid flag and RF port
     * configuration valid flag */
    current_lna_state_rx1[p_id->card][LNA1].valid = false;
    current_lna_state_rx1[p_id->card][LNA2].valid = false;
    current_lna_state_rx2[p_id->card][LNA1].valid = false;
    current_lna_state_rx2[p_id->card][LNA2].valid = false;
    current_pa_state[p_id->card].valid = false;
    current_rf_port_state[p_id->card].valid = false;

    UNLOCK_RFE(p_id->card);

    TRACE_EXIT_STATUS(status);

    return status;
}

static int32_t
config_custom_rx_filter( rf_id_t *p_id,
                         uint8_t filt_index,
                         uint8_t hpf_lpf_setting,
                         uint8_t rx_fltr1_sw,
                         uint8_t rfic_port )
#if (defined ATE_SUPPORT)
{
    int32_t status = 0;

    if ( rfic_port > ad9361_rx_port_c )
    {
        status = -EINVAL;
    }

    if ( rx_fltr1_sw > 2 )
    {
        status = -EINVAL;
    }

    if ( status == 0 )
    {
        switch (filt_index)
        {
            case 1:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR1_SEL(rx_fltr1_sw),
                                      hpf_lpf_setting,_OFF,_OFF,_OFF);
                break;

            case 2:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR2_SEL,
                                      _OFF,hpf_lpf_setting,_OFF,_OFF);
                break;

            case 3:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR3_SEL,
                                      _OFF,_OFF,hpf_lpf_setting,_OFF);
                break;

            case 4:
                rx_filter_factory = (filter_selection_t)
                    FACT_RX_FILT_SEL_(rfic_port,FLTR4_SEL,
                                      _OFF,_OFF,_OFF,hpf_lpf_setting);
                break;

            default:
                status = -EINVAL;
                break;
        }
    }

    if ( status == 0 )
    {
        debug_print("Selecting factory filter on card %u\n", p_id->card);
        status = _select_rx_filter( p_id->card, p_id->chip_id, &rx_filter_factory );
    }

    return status;
}
#else
{ return -ENOTSUP; }
#endif  /* ATE_SUPPORT */

static int32_t
read_rfic_port_for_rx_hdl( rf_id_t *p_id, uint8_t *p_port )
{
    int32_t status=0;

    // mapping depends on mode: http://confluence/display/EN/Z3u+RF+Use+Cases
    switch( current_rf_port_config(p_id->card) )
    {
        case skiq_rf_port_config_fixed:
            // note: we're treating invalid the same as fixed for now
        case skiq_rf_port_config_invalid:
            if( p_id->hdl == skiq_rx_hdl_A1 )
            {
                *p_port = 2;
            }
            else if( p_id->hdl == skiq_rx_hdl_A2 )
            {
                *p_port = 1;
            }
            else
            {
                status = -EINVAL;
            }
            break;

        case skiq_rf_port_config_tdd:
            // intentional fall thru
        case skiq_rf_port_config_trx:
            if( p_id->hdl == skiq_rx_hdl_A1 )
            {
                *p_port = 1;
            }
            else if( p_id->hdl == skiq_rx_hdl_A2 )
            {
                // not really valid but don't want the swapping API to bomb out
                *p_port = 2;
            }
            else
            {
                status = -EINVAL;
            }
            break;

        default:
            status = -ENODEV;
            break;
    }

    return (status);
}

static int32_t
read_rfic_port_for_tx_hdl( rf_id_t *p_id, uint8_t *p_port )
{
    int32_t status=0;

    // we only ever support a single TX and it always maps to the same RFIC port

    if( p_id->hdl == skiq_tx_hdl_A1 )
    {
        *p_port = 1;
    }
    else
    {
        status = -EINVAL;
    }

    return (status);
}

static int32_t
swap_rx_port_config( rf_id_t *p_id, uint8_t a_port, uint8_t b_port )
{
    int32_t status=0;
    rfe_lna_state_t a_lna[Z3U_NR_LNA];
    rfe_lna_state_t b_lna[Z3U_NR_LNA];
    uint8_t i=0;
    
    // all we need to do is swap the LNA states
    for( i=0; i<Z3U_NR_LNA; i++ )
    {
        if( a_port == 1 )
        {
            a_lna[i] = current_lna_state_rx1[p_id->card][i].state;
        }
        else
        {
            a_lna[i] = current_lna_state_rx2[p_id->card][i].state;
        }
        if( b_port == 1 )
        {
            b_lna[i] = current_lna_state_rx1[p_id->card][i].state;
        }
        else
        {
            b_lna[i] = current_lna_state_rx2[p_id->card][i].state;
        }
    }

    for( i=0; i<Z3U_NR_LNA; i++ )
    {
        _update_rx_lna( p_id->card, a_port, i, b_lna[i] );
        _update_rx_lna( p_id->card, b_port, i, a_lna[i] );
    }

    return (status);
}

int32_t read_rf_capabilities( rf_id_t *p_id, 
                              skiq_rf_capabilities_t *p_rf_capabilities)
{
    int32_t status=0;

    (void) p_id;

    p_rf_capabilities->minTxFreqHz = Z3U_MIN_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxTxFreqHz = Z3U_MAX_TX_FREQUENCY_IN_HZ;
    p_rf_capabilities->minRxFreqHz = Z3U_MIN_RX_FREQUENCY_IN_HZ;
    p_rf_capabilities->maxRxFreqHz = Z3U_MAX_RX_FREQUENCY_IN_HZ;

    return (status);
}


/***** GLOBAL DATA *****/

rfe_functions_t rfe_z3u_abcd_funcs =
{
    .init                           = init,
    .release                        = release,
    .update_tx_pa                   = update_tx_pa,
    .override_tx_pa                 = rfe_z3u_override_tx_pa,
    .update_rx_lna                  = update_rx_lna,
    .update_rx_lna_by_idx           = update_rx_lna_by_idx,
    .update_before_rx_lo_tune       = update_before_rx_lo_tune,
    .update_after_rx_lo_tune        = update_after_rx_lo_tune,
    .update_before_tx_lo_tune       = update_before_tx_lo_tune,
    .update_after_tx_lo_tune        = NULL,
    .write_rx_filter_path           = write_rx_filter_path,
    .write_tx_filter_path           = write_tx_filter_path,
    .read_rx_filter_path            = read_rx_filter_path,
    .read_tx_filter_path            = read_tx_filter_path,
    .read_rx_filter_capabilities    = read_rx_filter_capabilities,
    .read_tx_filter_capabilities    = read_tx_filter_capabilities,
    .write_filters                  = NULL,
    .write_rx_attenuation           = NULL,
    .read_rx_rf_ports_avail         = read_rx_rf_ports_avail,
    .read_rx_rf_port                = read_rx_rf_port,
    .write_rx_rf_port               = NULL,
    .read_tx_rf_ports_avail         = read_tx_rf_ports_avail,
    .read_tx_rf_port                = read_tx_rf_port,
    .write_tx_rf_port               = NULL,
    .rf_port_config_avail           = rf_port_config_avail,
    .write_rf_port_config           = write_rf_port_config,
    .read_rf_port_operation         = read_rf_port_operation,
    .write_rf_port_operation        = write_rf_port_operation,
    .read_bias_index                = NULL,
    .write_bias_index               = NULL,
    .read_rx_stream_hdl_conflict    = NULL,
    .enable_rx_chan                 = enable_rx_chan,
    .enable_tx_chan                 = NULL,
    .write_rx_freq_hop_list         = write_rx_freq_hop_list,
    .write_tx_freq_hop_list         = write_tx_freq_hop_list,
    .config_custom_rx_filter        = config_custom_rx_filter,
    .read_rfic_port_for_rx_hdl      = read_rfic_port_for_rx_hdl,
    .read_rfic_port_for_tx_hdl      = read_rfic_port_for_tx_hdl,
    .swap_rx_port_config            = swap_rx_port_config,
    .read_rf_capabilities           = read_rf_capabilities,
};


/***** GLOBAL FUNCTIONS *****/

int32_t
rfe_z3u_override_tx_pa( rf_id_t *p_id,
                        skiq_pin_value_t pin_value )
#if (defined ATE_SUPPORT)
{
    int32_t status = 0;
    uint32_t pin_mask = TX_AMP_MANUAL_BIAS_ON_N;
    uint32_t hiz_mask = 0, high_mask = 0;

    switch (pin_value)
    {
        case skiq_pin_value_hiz:
            hiz_mask = pin_mask;
            break;

        case skiq_pin_value_low:
            high_mask = 0;
            break;

        case skiq_pin_value_high:
            high_mask = pin_mask;
            break;

        default:
            skiq_error("Unknown / unsupported pin value %u for card %u\n", pin_value, p_id->card);
            status = -EINVAL;
            break;
    }

    if ( status == 0 )
    {
        status = pcal6524_io_exp_set_as_hiz( p_id->card, Z3U_IOE_I2C_BUS, x20_ADDR, pin_mask, hiz_mask, high_mask );
    }

    return status;
}
#else
{ return -ENOTSUP; }
#endif  /* ATE_SUPPORT */

void _display_ioe( uint8_t card, const char* p_string )
{
#ifdef DEBUG_RFE_Z3U_IOE
    uint32_t dir=0;
    uint32_t val=0;
        
    pcal6524_io_exp_read_output( card, Z3U_IOE_I2C_BUS, 0x20, &val );
    pcal6524_io_exp_read_direction( card, Z3U_IOE_I2C_BUS, 0x20, &dir );
    debug_print("[%s]: IO Exp 0x20: dir=0x%x, val=0x%x\n", p_string, dir, val);
    
    pcal6524_io_exp_read_output( card, Z3U_IOE_I2C_BUS, 0x21, &val );
    pcal6524_io_exp_read_direction( card, Z3U_IOE_I2C_BUS, 0x21, &dir );
    debug_print("[%s]: IO Exp 0x21: dir=0x%x, val=0x%x\n", p_string, dir, val);
    
    pcal6524_io_exp_read_output( card, Z3U_IOE_I2C_BUS, 0x22, &val );
    pcal6524_io_exp_read_direction( card, Z3U_IOE_I2C_BUS, 0x22, &dir );

    debug_print("[%s]: IO Exp 0x22: dir=0x%x, val=0x%x\n", p_string, dir, val);
    debug_print("\n");

#endif
}

