/*! \file calc_ad9528.c
 * \brief This file contains the implementation to calculate
 * the AD9528 settings based on the requested sample rate
 *
 * <pre>
 * Copyright 2017 Epiq Solutions, All Rights Reserved
 *
 * Revision History
 * Date       | Who | Description
 * 12/26/2017   MEZ   Created file
 *
 *</pre>*/
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <limits.h>

#include <sidekiq_private.h>

#include "calc_ad9528.h"

/* enable debug_print and debug_print_plain when DEBUG_AD9528_CONFIG is defined */
#if (defined DEBUG_AD9528_CONFIG)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"

// Oscillator selectable by FPGA...it can either be 153.6 or 100M
#define OSC_153P6_MHZ (153600000)
#define OSC_100_MHZ (100000000)

#define DEV_CLK_MIN (50000000)   // 50 MHz
#define DEV_CLK_MAX (153600000)  // 153.6 MHz
#define DEV_CLK_MAX_X4 (307200000) // X4 supports a higher JESD lane rate
#define AD9371_PFD_MAX (80000000) // 80 MHz...empirically determined by 9371 Filter Wizard

#define FVCO_MIN (3450000000) // 3450 MHz (min for PLL2 VCO)
#define FVCO_MAX (4025000000) // 4025 MHz (max for PLL2 VCO)

#define OUT_DIV_MIN (1)      // lowest output divider for 9528
#define OUT_DIV_MAX (256)    // highest output divider for 9528

// PLL2 - N2 divider - 8 bits
#define N2_MIN (1)
#define N2_MAX (256)

// VCO divider for PLL2 - p. 57 of 9528 manual
#define MIN_M_DIV (3)
#define MAX_M_DIV (5)

#define OUT_FREQ_MIN (2700000) // 2.7MHz (3450/M=5/OUT_DIV_MAX)
#define OUT_FREQ_MAX (1000000000) // highest allowed for 9528 clock distro

#define MAX_MULT_AVAIL (3) // 1,2,4

#define FPFD_MAX (275000000) // Hz

#define NTOTAL_MIN (20)
#define NTOTAL_MAX (200)

#define DIST_FREQ_MAX (1250000000)

#define INVALID_FDELTA (-1)

#define DEFAULT_VCXO_FREQ (153600000)

#define RFFC_REF_MIN (10000000)  // 10M
#define RFFC_REF_MAX (100000000) // 100M

#define DIV_40MHZ (4)
#define DIV_100MHZ (10)

#define JESD_LMFC_VAL (32) // k: frames per multiframe...always fixed for us

// inclusive range check
#define CHECK_IN_RANGE_INCL(val, min, max) ( (val>=min) && (val<=max) )
// exclusive range check
#define CHECK_IN_RANGE_EXCL(val, min, max) ( (val>min) && (val<max) )

typedef struct
{
    uint32_t vcxo_freq;
    uint32_t dev_clk;
    uint32_t out_div;
    uint8_t fpga_out_div_mult;
    float fdelta;
    float signed_fdelta;
    uint32_t rffc_ref_freq;
    uint32_t rffc_div;
    bool en_vcxo_to_distro;
    bool pll2_power_down;
    // PLL2 only
    float fpfd;
    uint32_t fvco;
    uint32_t m1;
    uint32_t n2;
    uint32_t n_total;
    float r2;
    bool r2_doubler;
} _clock_config_t;

// greatest commond denominator
static uint32_t _gcd( uint32_t a, uint32_t b );
// calculate the clock configuration
static int32_t _calc_clock_config( uint32_t dev_clock_freq,
                                   uint64_t fpga_min_dev_clock,
                                   uint64_t fpga_max_dev_clock,
                                   _clock_config_t *p_clock,
                                   skiq_part_t part );

// returns 0 if it's able to find a valid config
static int32_t _find_best_loop_config( uint32_t osc_freq,
                                       uint32_t dev_clock_freq,
                                       uint8_t out_div_mult,
                                       _clock_config_t *p_clock );
// returns 0 if valid config found
static int32_t _find_highest_fpfd_n2( uint32_t freq_after_div,
                                      uint32_t ref_freq,
                                      _clock_config_t *p_clock );

// prints out the clock_config data structure
static void _print_clock( _clock_config_t *p_clock );

// populate the parameters from clock_config to ad9528 structures
static int32_t _update_ad9528_struct( _clock_config_t *p_clock,
                                      ad9528pll1Settings_t *p_pll1,
                                      ad9528pll2Settings_t *p_pll2,
                                      ad9528outputSettings_t *p_output,
                                      ad9528sysrefSettings_t *p_sysref,
                                      const clk_sel_t clk_sel,
                                      uint32_t actual_sample_rate );
// update PLL1 for VCXO setting
static int32_t _update_pll1_for_vcxo( uint32_t vcxo_freq,
                                      ad9528pll1Settings_t *p_pll1 );
// uses dev_clock and out_div from p_clock
static void _calc_rffc_clk( _clock_config_t *p_clock );


/*****************************************************************************/
/** calc_ad9528_for_sample_rate() determines the required clocking
 *  configuration to acheive the  desired sample rate.  This includes the
 *  oscillator selection, ad9528, and FPGA QPLL.
 *
 * @param[in]  req_sample_rate         Requested sample rate
 * @param[out] p_actual_sample_rate    Actual or achievable rate
 * @param[out] p_pll1                  Computed settings for PLL1
 * @param[out] p_pll2                  Computed settings for PLL2
 * @param[out] p_output                Pointer to a structure containing AD9528
 *                                        clock output settings
 * @param[out] p_sysref                Pointer to a structure containing AD9528
 *                                        SYSREF output settings
 * @param[out] p_fpga_div              Pointer to the FPGA divider/multipler
 *                                        required to acheive the requested rate
 * @param[out] p_dev_clk               Pointer to the dev clock
 * @param[out] p_dev_clk_div           Pointer to the dev clock divider
 * @param[out] p_rffc_freq             Pointer to RFFC frequency, only relevant for X2
 * @param[in] fpga_min_dev_clk         min supported dev clock (as reported by
 *                                      fpga_jesd_qpll_freq_range)
 * @param[in] fpga_max_dev_clk         max supported dev clock (as reported bv
 *                                      fpga_jesd_qpll_freq_range)
 * @param[out] p_fpga_gbt_clock        computed FPGA GBT clock (dev_clock * fpga mult)
 * @param[out] clk_sel                 clock selections: GBT, debug, and RFFC
 * @param[in]  part                    Sidekiq part of interest
 *
 * @return int32_t
 * @retval 0 Success
 * @retval -EINVAL
 */

int32_t calc_ad9528_for_sample_rate( uint32_t req_sample_rate,
                                     uint32_t *p_actual_sample_rate,
                                     ad9528pll1Settings_t *p_pll1,
                                     ad9528pll2Settings_t *p_pll2,
                                     ad9528outputSettings_t *p_output,
                                     ad9528sysrefSettings_t *p_sysref,
                                     clk_div_t *p_fpga_div,
                                     uint32_t *p_dev_clk,
                                     clk_div_t *p_dev_clk_div,
                                     uint32_t *p_rffc_freq,
                                     uint64_t fpga_min_dev_clk,
                                     uint64_t fpga_max_dev_clk,
                                     uint64_t *p_fpga_gbt_clock,
                                     const clk_sel_t clk_sel,
                                     skiq_part_t part )
{
    int32_t status=0;

    _clock_config_t tmp_clock;
    _clock_config_t final_clock;
    bool exact_match = false; // fdelta=0

    uint8_t num_mult_avail=0;
    clk_div_t mult_avail[MAX_MULT_AVAIL];

    int8_t i=0;

    if( part == skiq_x2)
    {
        // sample rate must be within dev_clock_min/4 and dev_clk_max
        if( CHECK_IN_RANGE_INCL(req_sample_rate, (DEV_CLK_MIN/4), DEV_CLK_MAX) != true )
        {
            skiq_error("Supported sample rate is %u-%u\n", DEV_CLK_MIN/4,
                      DEV_CLK_MAX);
            return (-EINVAL);
        }
    }
    else if( part == skiq_x4)
    {
        if( CHECK_IN_RANGE_INCL(req_sample_rate, (DEV_CLK_MIN/4), DEV_CLK_MAX_X4) != true )
        {
            skiq_error("Supported sample rate is %u-%u\n",
                      DEV_CLK_MIN/4, DEV_CLK_MAX_X4);
            return (-EINVAL);
        }
    }
    else
    {
        return(-EINVAL);
    }

    // default everything to 0
    memset(&final_clock, 0, sizeof(_clock_config_t));
    memset(&tmp_clock, 0, sizeof(_clock_config_t));
    // set fdelta to invalid
    final_clock.fdelta = INVALID_FDELTA;

    uint32_t dev_clock_before_fpga_mult = req_sample_rate;

    if (req_sample_rate > DEV_CLK_MAX)
    {
        dev_clock_before_fpga_mult >>= 1;
    }
    // determine which multipliers are available based on min/max clocks
    if( dev_clock_before_fpga_mult >= DEV_CLK_MIN  )
    {
        mult_avail[num_mult_avail++] = CLK_DIV_1;
    }
    if( CHECK_IN_RANGE_INCL((dev_clock_before_fpga_mult*CLK_DIV_2), DEV_CLK_MIN,
                            DEV_CLK_MAX) == true )
    {
        mult_avail[num_mult_avail++] = CLK_DIV_2;
    }
    if( CHECK_IN_RANGE_INCL((dev_clock_before_fpga_mult*CLK_DIV_4), DEV_CLK_MIN,
                            DEV_CLK_MAX) == true )
    {
        mult_avail[num_mult_avail++] = CLK_DIV_4;
    }

    // we want to start with the highest divider to maximize dev_clk
    for( i=(num_mult_avail-1); (i>=0) && (exact_match==false); i-- )
    {
        // 0 indicates we got a valid config
        if( _calc_clock_config( mult_avail[i]*dev_clock_before_fpga_mult,
                                fpga_min_dev_clk,
                                fpga_max_dev_clk,
                                &tmp_clock,
                                part ) == 0 )
        {
            status=0;
            // save this as our final if fdelta is lower or if we haven't saved a config
            if( (tmp_clock.fdelta < final_clock.fdelta) || (final_clock.fdelta==-1) )
            {
                // if fdelta is 0, we actually got an exact config...break out of the
                // loop now since we have an exact config with the highest multiple
                if( tmp_clock.fdelta == 0 )
                {
                    exact_match = true;
                }
                // copy our new config over
                memcpy( &final_clock, &tmp_clock, sizeof(_clock_config_t) );
                // be sure to save our FPGA divider and dev clock
                *p_fpga_div = mult_avail[i]*final_clock.fpga_out_div_mult;

                *p_dev_clk = final_clock.dev_clk;
                *p_fpga_gbt_clock = (uint64_t)final_clock.dev_clk *
                                    (uint64_t)final_clock.fpga_out_div_mult;

                debug_print("_calc_clock_config: i:%d  *p_fpga_div: %d\n", i, *p_fpga_div);
                debug_print("fpga_div: %" PRIu32 ", dev_clk: %" PRIu32 ", "
                            "fpga_gbt_clock: %" PRIu64 "\n", *p_fpga_div, *p_dev_clk,
                            *p_fpga_gbt_clock );
            }
        }
    }


    // calculate actual sample rate
    *p_actual_sample_rate = req_sample_rate + final_clock.signed_fdelta;
    // determine the dev clock divider now...we want to minimize this to maximize
    // the clock going in to the RFIC
    if( final_clock.dev_clk <= AD9371_PFD_MAX )
    {
        *p_dev_clk_div = CLK_DIV_1;
    }
    else if( final_clock.dev_clk <= (AD9371_PFD_MAX*2) )
    {
        *p_dev_clk_div = CLK_DIV_2;
    }
    else if( final_clock.dev_clk <= (AD9371_PFD_MAX*4) )
    {
        *p_dev_clk_div = CLK_DIV_4;
    }
    else
    {
        skiq_error("Unable to find valid dev clock divider");
        status = -EINVAL;
    }

    if( part == skiq_x4)
    {
        *p_dev_clk_div = *p_fpga_div;
    }

    if( status == 0 )
    {
        // populate the values from _clock_config_t to ad9528 data structures
        status=_update_ad9528_struct( &final_clock,
                                      p_pll1, p_pll2, p_output, p_sysref, clk_sel,
                                      *p_actual_sample_rate );
        *p_rffc_freq = final_clock.rffc_ref_freq;
        _print_clock( &final_clock );
    }

    return (status);
}

void ad9528_init_struct( ad9528pll1Settings_t *p_pll1,
                         ad9528pll2Settings_t *p_pll2 )
{
    // Default settings

    // PLL1
    // Note: frequency is set based on ref clock selection and overwritten
    p_pll1->refA_Frequency_Hz = 10000000; // always 10MHz (this is our ref clock)
    p_pll1->refB_Frequency_Hz = 10000000; // always 10MHz
    p_pll1->refA_bufferCtrl = DISABLED; // set by ref clock
    p_pll1->refB_bufferCtrl = DISABLED; // set by ref clock
    p_pll1->vcxoBufferCtrl = SINGLE_ENDED;
    p_pll1->bypassPll1 = 0;
    _update_pll1_for_vcxo( DEFAULT_VCXO_FREQ, p_pll1 );
    p_pll1->vcxo_Frequency_Hz = DEFAULT_VCXO_FREQ;

    // power down PLL2
    p_pll2->power_down = 1;

}

int32_t _update_ad9528_struct( _clock_config_t *p_clock,
                               ad9528pll1Settings_t *p_pll1,
                               ad9528pll2Settings_t *p_pll2,
                               ad9528outputSettings_t *p_output,
                               ad9528sysrefSettings_t *p_sysref,
                               const clk_sel_t clk_sel,
                               uint32_t actual_sample_rate )
{
    int32_t status=0;
    uint8_t i=0;
    uint64_t distro_freq=0;
    float tmp_cp=0;
    uint32_t sysref_gcd=1;
    uint32_t sysref_src_freq=0;
    uint32_t tmp_sysref_div=0;

    ////////////////////////////////////////
    // Determine SYSREF divider
    // Refer to https://confluence.epiq.rocks/display/EN/TX+Anomaly+Investigation#TXAnomalyInvestigation-CalculatingSYSREFDivider for details
    
    // determine source freq based on PLL1 vs PLL2
    if( p_clock->pll2_power_down == false )
    {
        sysref_src_freq = p_clock->fvco / (p_clock->n_total*2);
    }
    else
    {
        sysref_src_freq = p_clock->vcxo_freq;
    }
    
    sysref_gcd = _gcd( actual_sample_rate/JESD_LMFC_VAL, sysref_src_freq );
    tmp_sysref_div = sysref_src_freq / sysref_gcd;
    if( tmp_sysref_div > UINT16_MAX )
    {
        p_sysref->sysrefDivide = UINT16_MAX;
    }
    else
    {
        p_sysref->sysrefDivide = tmp_sysref_div;
    }
    ////////////////////////////////////////

    ////////////////////////////////////////
    // PLL1
    // constant
    // our ref clock can come from internal 10M osc (refA) or external 10M (refB)
    // either way, we want to configure refA and refB the same since we always use the same freq
    if( (status=_update_pll1_for_vcxo( p_clock->vcxo_freq, p_pll1 )) != 0 )
    {
        return (status);
    }
    p_pll1->vcxo_Frequency_Hz = p_clock->vcxo_freq;

    ////////////////////////////////////////
    // PLL2
    if( p_clock->pll2_power_down == false )
    {
        p_pll2->power_down = 0;

        p_pll2->rfDivider = p_clock->m1;
        p_pll2->n2Divider = p_clock->n2;
        p_pll2->totalNdiv = p_clock->n_total;
        if( p_clock->r2_doubler == true )
        {
            p_pll2->doubler = 1;
            p_pll2->r_div = (uint8_t)(p_clock->r2*2);
        }
        else
        {
            p_pll2->doubler = 0;
            p_pll2->r_div = (uint8_t)(p_clock->r2);
        }
        tmp_cp = ((float)(p_clock->n_total) / 20.0) * 0.00009;
        p_pll2->charge_pump_curr = (uint8_t)(tmp_cp / 0.0000035);
        // if PLL2 is enabled, SYSREF should come from PLL2
        p_sysref->sysrefClock = SYSREF_PLL2_CLK;
    }
    else
    {
        // PLL2 not in use, don't change any setting, just power it down
        p_pll2->power_down = 1;
        // PLL2 not enabled, use SYSREF from PLL2
        p_sysref->sysrefClock = SYSREF_PLL1_CLK;
    }

    ////////////////////////////////////////
    // Clock Output
    // note: out_div is used for output dividers of clock output except for the RFFC clocks
    distro_freq = (uint64_t)p_clock->dev_clk * (uint64_t)p_clock->out_div;
    for( i=0; i<AD9528_MAX_CLOCK_OUTS; i++ )
    {
        if( (i != clk_sel.rffc1_clk_sel) &&
            (i != clk_sel.rffc2_clk_sel) )
        {
            if ( AD9528_GBT_CLK_ISSET(clk_sel.fpga_clk_sel_mask, i) ||
                 (i == clk_sel.debug_clk_sel) )
            {
                if (p_clock->fpga_out_div_mult > 0)
                {
                    p_output->outChannelDiv[i] = p_clock->out_div / p_clock->fpga_out_div_mult;
                }
                else
                {
                    skiq_error("Invalid value for fpga_out_div_mult, cannot = 0");
                }
            }
            else
            {
                p_output->outChannelDiv[i] = p_clock->out_div;
            }
        }
        else
        {
            p_output->outChannelDiv[i] = p_clock->rffc_div;
        }

        // output the frequency (although it's not actually used in the 9528 driver)
        p_output->outFrequency_Hz[i] = distro_freq / p_output->outChannelDiv[i];
    }

    // we need to make sure the output source for the SYSREF is correct
    for( i=0; i<AD9528_MAX_CLOCK_OUTS; i++ )
    {
        // PLL2 is off, need to make sure to use PLL1 for SYSREF
        if( p_pll2->power_down == 1 )
        {
            if( p_output->outSource[i] == SYSREF_PLL2 )
            {
                p_output->outSource[i] = SYSREF_PLL1;
            }
        }
        // PLL2 is on, need to make sure SYSREF comes from it
        else
        {
            if( p_output->outSource[i] == SYSREF_PLL1 )
            {
                p_output->outSource[i] = SYSREF_PLL2;
            }
            else if( p_output->outSource[i] == PLL1_OUTPUT )
            {
                p_output->outSource[i] = CHANNEL_DIV;
            }
        }
    }
    p_output->vcxo_to_dist = p_clock->en_vcxo_to_distro;

    return (status);
}

int32_t _update_pll1_for_vcxo( uint32_t vcxo_freq,
                               ad9528pll1Settings_t *p_pll1 )
{
    int32_t status=0;
    uint32_t mult=1;

    // divider settings are for 10M...if we're running with 40M, we need to multiply
    // divider by a factor of 4
    if( (p_pll1->refA_Frequency_Hz == SIGNAL_40MHZ) &&
        (p_pll1->refB_Frequency_Hz == SIGNAL_40MHZ) )
    {
        mult = DIV_40MHZ;
    }
    else if( (p_pll1->refA_Frequency_Hz == SIGNAL_100MHZ) &&
             (p_pll1->refB_Frequency_Hz == SIGNAL_100MHZ) )
    {
        mult = DIV_100MHZ;
    }

    // defined in http://confluence/display/PROD/ECO-ES020-201-B-3

    // set the dividers based on the VCXO select (100M or 153.6M)
    if( vcxo_freq == 100000000 )
    {
        p_pll1->refA_Divider = 5*mult;
        p_pll1->refB_Divider = 5*mult;
        p_pll1->nDivider = 50; // N1
        p_pll1->charge_pump_curr = 20; // 10uA
    }
    else if( vcxo_freq == 153600000 )
    {
        p_pll1->refA_Divider = 25*mult;
        p_pll1->refB_Divider = 25*mult;
        p_pll1->nDivider = 384; // N1
        p_pll1->charge_pump_curr = 100; // 50uA
    }
    else
    {
        skiq_error("Invalid VCXO frequency");
        status = -EINVAL;
    }

    return (status);
}

// returns 0 if exact match
int32_t _calc_clock_config( uint32_t dev_clock_freq,
                            uint64_t fpga_min_dev_clock,
                            uint64_t fpga_max_dev_clock,
                            _clock_config_t *p_clock,
                            skiq_part_t part )
{
    int32_t status = -EINVAL;
    _clock_config_t clock_100M;   // uses 100M osc
    _clock_config_t clock_153P6M; // uses 153.6 osc
    _clock_config_t *p_clock_ref; // pointer to the best
    bool valid_100M = false;
    bool valid_153P6M = false;
    uint8_t out_div_mult=OUT_DIV_MIN;

    // reset our config
    memset( &clock_100M, 0, sizeof(_clock_config_t) );
    memset( &clock_153P6M, 0, sizeof(_clock_config_t) );

    if( CHECK_IN_RANGE_INCL(dev_clock_freq, fpga_min_dev_clock, fpga_max_dev_clock) == true )
    {
        out_div_mult = 1;
    }
    else if( CHECK_IN_RANGE_INCL((dev_clock_freq*2), fpga_min_dev_clock, fpga_max_dev_clock) == true )
    {
        out_div_mult = 2;
    }
    else if( CHECK_IN_RANGE_INCL((dev_clock_freq*4), fpga_min_dev_clock, fpga_max_dev_clock) == true )
    {
        out_div_mult = 4;
    }
    else
    {
        skiq_error("Unable to achieve dev clock within range\n");
        return -EINVAL;
    }

    // find the best configs for both 100M and 153.6, then pick the best
    if( _find_best_loop_config( 100000000, dev_clock_freq, out_div_mult, &clock_100M ) == 0 )
    {
        valid_100M = true;
        status = 0;
        clock_100M.fpga_out_div_mult = out_div_mult;
        p_clock_ref = &clock_100M;
    }
    if( _find_best_loop_config( 153600000, dev_clock_freq, out_div_mult, &clock_153P6M ) == 0 )
    {
        valid_153P6M = true;
        status = 0;
        clock_153P6M.fpga_out_div_mult = out_div_mult;
        p_clock_ref = &clock_153P6M;
    }

    if( (valid_100M==true) && (valid_153P6M==true) )
    {
        // if fdeltas are equal, minimize r2, otherwise minimze fdelta
        if( (clock_100M.fdelta == clock_153P6M.fdelta) &&
            (clock_100M.r2 < clock_153P6M.r2) )
        {
            p_clock_ref = &clock_100M;
        }
        else if( clock_100M.fdelta < clock_153P6M.fdelta )
        {
            p_clock_ref = &clock_100M;
        }
        else
        {
            p_clock_ref = &clock_153P6M;
        }
    }

    if(status == 0 )
    {
        // save off the config
        memcpy(p_clock, p_clock_ref, sizeof(_clock_config_t));

        /* RFFC is only available on x2 */
        if (part == skiq_x2)
        {
            _calc_rffc_clk( p_clock );
        }
    }

    return (status);
}

void _calc_rffc_clk( _clock_config_t *p_clock )
{
    uint16_t i=0;
    uint64_t distro_freq = (uint64_t)p_clock->dev_clk * (uint64_t)p_clock->out_div;

    bool found_rffc_config=false;
    for( i=OUT_DIV_MIN; (i<=OUT_DIV_MAX) && (found_rffc_config==false); i++ )
    {
        if( (distro_freq/i) <= RFFC_REF_MAX &&
            (distro_freq/i) >= RFFC_REF_MIN )
        {
            found_rffc_config=true;
            p_clock->rffc_div = i;
            p_clock->rffc_ref_freq = distro_freq / i;
        }
    }
}

int32_t _find_best_loop_config( uint32_t osc_freq,
                                uint32_t dev_clock_freq,
                                uint8_t out_div_mult,
                                _clock_config_t *p_clock )
{
    int32_t status=-1;
    uint32_t i,j=0;
    _clock_config_t tmp_config;
    bool update_config=false;
    uint32_t n_tmp;

    // make sure the optimal_config parameters are all reset
    memcpy(&tmp_config, p_clock, sizeof(_clock_config_t));

    // first see if VCXO is an even multiple of dev clock
    if( (osc_freq % (dev_clock_freq*out_div_mult)) == 0 )
    {
        p_clock->vcxo_freq = osc_freq;
        p_clock->dev_clk = dev_clock_freq;
        p_clock->out_div = osc_freq / (dev_clock_freq*out_div_mult);
        p_clock->fdelta = 0; // not freq delta since we have a perfect match
        p_clock->signed_fdelta = 0;
        // note: PLL2 is not needed in this case
        p_clock->pll2_power_down = true;
        p_clock->en_vcxo_to_distro = true;

        status=0;
    }
    else
    {
        memset(&tmp_config, 0, sizeof(_clock_config_t));
        p_clock->fdelta = -1;
        // if we fall to here, we're using PLL2...make sure it's on
        p_clock->pll2_power_down = false;
        p_clock->en_vcxo_to_distro = false;
        // we want to keep the config with the highest fpfd
        // find_all_loops_configs()
        // find_fvco
        // loop through output dividers and M
        for( i=out_div_mult; i<=OUT_DIV_MAX; i+=out_div_mult )
        {
            for( j=MIN_M_DIV; j<=MAX_M_DIV; j++ )
            {
                for( n_tmp=NTOTAL_MIN; n_tmp<NTOTAL_MAX; n_tmp++ )
                {
                    // make sure we don't exceed our clock bounds
                    if( CHECK_IN_RANGE_INCL((uint64_t)((dev_clock_freq*j)), OUT_FREQ_MIN, OUT_FREQ_MAX) == true )
                    {
                        update_config=false;
                        // see if fvco and n_total would be in range
                        tmp_config.fvco = dev_clock_freq*i*j;
                        tmp_config.m1 = j;
                        tmp_config.out_div = i;
                        tmp_config.n2 = n_tmp/tmp_config.m1;
                        tmp_config.n_total = tmp_config.m1 * tmp_config.n2;
                        tmp_config.dev_clk = dev_clock_freq;
                        tmp_config.vcxo_freq = osc_freq;

                        if( (CHECK_IN_RANGE_INCL(tmp_config.fvco, FVCO_MIN, FVCO_MAX)==true) &&
                            ((tmp_config.fvco/tmp_config.m1) <= DIST_FREQ_MAX) &&
                            (tmp_config.n2 <= N2_MAX) &&
                            (CHECK_IN_RANGE_INCL(tmp_config.n_total, NTOTAL_MIN, NTOTAL_MAX)==true) )
                        {
                            if( _find_highest_fpfd_n2( (tmp_config.fvco/tmp_config.m1),
                                                       osc_freq, &tmp_config) == 0 )
                            {
                                tmp_config.fdelta = 0;
                                tmp_config.signed_fdelta = 0;
                                // optimal config has max fpfd
                                // if FPFD is the same, we want to minimize the out_div
                                if( p_clock->fdelta == 0 )
                                {

                                    if( (tmp_config.fpfd > p_clock->fpfd) ||
                                        ((tmp_config.fpfd == p_clock->fpfd) &&
                                         (tmp_config.out_div < p_clock->out_div)) )
                                    {
                                        update_config = true;
                                    }
                                }
                                else
                                {
                                    // always update the config if we never had something pass
                                    update_config = true;
                                }
                            } // found a valid config
                            else
                            {
                                // reset n2 and n_total (highest_fpfd changes these values)
                                tmp_config.n2 = n_tmp/tmp_config.m1;
                                tmp_config.n_total = tmp_config.m1 * tmp_config.n2;

                                // r2 needs to be rounded to nearest 0.5
                                tmp_config.r2 = floor((float)(osc_freq/((float)(tmp_config.fvco/tmp_config.m1/tmp_config.n2)))*2+0.5)/2;
                                tmp_config.fpfd = (float)(osc_freq/tmp_config.r2);
                                tmp_config.fvco = tmp_config.fpfd*(tmp_config.m1*tmp_config.n2);
                                tmp_config.signed_fdelta = (float)((tmp_config.fvco/tmp_config.m1/tmp_config.out_div))-dev_clock_freq;
                                tmp_config.fdelta = abs(tmp_config.signed_fdelta);
                                tmp_config.vcxo_freq = osc_freq;
                                tmp_config.dev_clk = tmp_config.signed_fdelta + dev_clock_freq;
                                // figure out when to do this vs find_highest
                                // we want to minimize fdelta?
                                if( (tmp_config.fdelta < p_clock->fdelta) ||
                                    (p_clock->fdelta == -1) )
                                {
                                    update_config = true;
                                }
                            }
                        }
                        if( update_config == true )
                        {
                            if( ((int)(tmp_config.r2*2) % 2) != 0 )
                            {
                                tmp_config.r2_doubler = true;
                            }
                            else
                            {
                                tmp_config.r2_doubler = false;
                            }
                            memcpy( p_clock, &tmp_config, sizeof(_clock_config_t));
                            status=0;
                        }
                    } // end dev clk bound
                } // end FVCO bound
            } // end M loop
        } // end out div loop
    } // end uneven bound

    return (status);
}

// freq_after_div=freturn, ref_freq=fref
int32_t _find_highest_fpfd_n2( uint32_t freq_after_div, uint32_t ref_freq, _clock_config_t *p_clock )
{
    int32_t status=0;

    p_clock->fpfd = _gcd(freq_after_div, ref_freq);
    uint32_t fpfd_doubler = _gcd(freq_after_div, ref_freq*2); // option to use doubler

    if( p_clock->fpfd > FPFD_MAX  )
    {
        // reset the fpfd
        p_clock->fpfd = 0;
        status = -1;
    }
    else
    {
        // we want to maximum fpfd, so use the doubler if it's in bounds and greater than single
        if( (fpfd_doubler > p_clock->fpfd) &&
            (fpfd_doubler <= FPFD_MAX) )
        {
            p_clock->fpfd = fpfd_doubler;
        }
        // populate n2/r2 if n2 is within bounds
        p_clock->n2 = freq_after_div/(int)(p_clock->fpfd);
        p_clock->r2 = ref_freq/p_clock->fpfd;
        p_clock->n_total = p_clock->m1 * p_clock->n2;
        if( p_clock->n2 > N2_MAX )
        {
            status=-2;
        }
        if( (p_clock->n_total < NTOTAL_MIN) ||
            (p_clock->n_total > NTOTAL_MAX) )
        {
            status = -3;
        }
    }
    return (status);
}

// https://stackoverflow.com/questions/19738919/gcd-function-for-c
uint32_t _gcd( uint32_t a, uint32_t b )
{
    uint32_t tmp;
    while( b != 0 )
    {
        tmp = a % b;
        a = b;
        b = tmp;
    }
    return (a);
}

void _print_clock( _clock_config_t *p_clock )
{
    debug_print("clock_config: \tvcxo=%u\n", p_clock->vcxo_freq);
    debug_print("clock_config: \tdev_clk=%u\n", p_clock->dev_clk);
    debug_print("clock_config: \tout_div=%u\n", p_clock->out_div);
    debug_print("clock_config: \tfpga_out_div_mult=%u\n", p_clock->fpga_out_div_mult);
    debug_print("clock_config: \tfdelta=%f\n", p_clock->fdelta);
    debug_print("clock_config: \trffc_ref_freq=%u\n", p_clock->rffc_ref_freq);
    debug_print("clock_config: \trffc_div=%u\n", p_clock->rffc_div);
    if( p_clock->pll2_power_down == false )
    {
        debug_print("clock_config: \tPLL2 is enabled\n");
        // PLL2 only
        debug_print("clock_config: \t\tfpfd=%f\n", p_clock->fpfd);
        debug_print("clock_config: \t\tfvco=%u\n", p_clock->fvco);
        debug_print("clock_config: \t\tm1=%u\n", p_clock->m1);
        debug_print("clock_config: \t\tn2=%u\n", p_clock->n2);
        debug_print("clock_config: \t\tn_total=%u\n", p_clock->n_total);
        debug_print("clock_config: \t\tr2=%f\n", p_clock->r2);
        if( p_clock->r2_doubler == true )
        {
            debug_print("clock_config: \t\tR2 doubler enabled\n");
        }
        else
        {
            debug_print("clock_config: \t\tR2 doubler disabled\n");
        }
    }
    else
    {
        debug_print("clock_config: \tPLL2 is disabled\n");
    }

    if( p_clock->en_vcxo_to_distro == true )
    {
        debug_print("clock_config: \tVCXO to distro is enabled\n");
    }
    else
    {
        debug_print("clock_config: \tVCXO to distro is disabled\n");
    }
}
