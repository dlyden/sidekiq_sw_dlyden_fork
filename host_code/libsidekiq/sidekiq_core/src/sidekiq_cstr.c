/**
 * @file   sidekiq_cstr.c
 * @author <info@epiq-solutions.com>
 * @date   Fri Sep 14 14:35:50 2018
 *
 * @brief
 *
 * <pre>
 * Copyright 2018-2021 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include "sidekiq_cstr.h"


/***** DEFINES *****/


/***** TYPEDEFS *****/


/***** STRUCTS *****/


/***** LOCAL VARIABLES *****/


/***** LOCAL FUNCTIONS *****/


/***** GLOBAL FUNCTIONS *****/


/**************************************************************************************************/
const char *
bool_cstr( bool flag )
{
    const char* p_bool_str = (flag) ? "true" : "false";

    return p_bool_str;
}


/**************************************************************************************************/
const char *
part_cstr( skiq_part_t part )
{
    const char* p_part_str =
        (part == skiq_mpcie) ? "PCIe" :
        (part == skiq_m2) ? "M.2" :
        (part == skiq_x2) ? "X2" :
        (part == skiq_z2) ? "Z2" :
        (part == skiq_x4) ? "X4" :
        (part == skiq_m2_2280) ? "M.2-2280" :
        (part == skiq_z2p) ? "Z2P" :
        (part == skiq_z3u) ? "Z3U" :
        (part == skiq_nv100) ? "NV100" :
        "unknown";

    return p_part_str;
}


/**************************************************************************************************/
const char *
rx_hdl_cstr( skiq_rx_hdl_t hdl )
{
    const char* p_hdl_str =
        (hdl == skiq_rx_hdl_A1) ? "A1" :
        (hdl == skiq_rx_hdl_A2) ? "A2" :
        (hdl == skiq_rx_hdl_B1) ? "B1" :
        (hdl == skiq_rx_hdl_B2) ? "B2" :
        (hdl == skiq_rx_hdl_C1) ? "C1" :
        (hdl == skiq_rx_hdl_D1) ? "D1" :
        "unknown";

    return p_hdl_str;
}


/**************************************************************************************************/
const char *
tx_hdl_cstr( skiq_tx_hdl_t hdl )
{
    const char* p_hdl_str =
        (hdl == skiq_tx_hdl_A1) ? "A1" :
        (hdl == skiq_tx_hdl_A2) ? "A2" :
        (hdl == skiq_tx_hdl_B1) ? "B1" :
        (hdl == skiq_tx_hdl_B2) ? "B2" :
        "unknown";

    return p_hdl_str;
}


/**************************************************************************************************/
const char *
filt_cstr( skiq_filt_t filt )
{
    const char* p_filt_str =
        (filt == skiq_filt_invalid) ? "invalid" :
        (filt == skiq_filt_0_to_3000_MHz) ? "0-3GHz" :
        (filt == skiq_filt_3000_to_6000_MHz) ? "3GHz-6GHz" :

        (filt == skiq_filt_0_to_440MHz) ? "0-440MHz" :
        (filt == skiq_filt_440_to_6000MHz) ? "440MHz-6GHz" :

        (filt == skiq_filt_440_to_580MHz) ? "440MHz-580MHz" :
        (filt == skiq_filt_580_to_810MHz) ? "580MHz-810MHz" :
        (filt == skiq_filt_810_to_1170MHz) ? "810MHz-1.17GHz" :
        (filt == skiq_filt_1170_to_1695MHz) ? "1.17GHz-1.695GHz" :
        (filt == skiq_filt_1695_to_2540MHz) ? "1.695GHz-2.54GHz" :
        (filt == skiq_filt_2540_to_3840MHz) ? "2.54GHz-3.84GHz" :
        (filt == skiq_filt_3840_to_6000MHz) ? "3.84GHz-6GHz" :

        (filt == skiq_filt_0_to_300MHz) ? "0-300MHz" :
        (filt == skiq_filt_300_to_6000MHz) ? "300MHz-6GHz" :

        (filt == skiq_filt_50_to_435MHz) ? "50MHz-435MHz" :
        (filt == skiq_filt_435_to_910MHz) ? "435MHz-910MHz" :
        (filt == skiq_filt_910_to_1950MHz) ? "910MHz-1.95GHz" :
        (filt == skiq_filt_1950_to_6000MHz) ? "1.95GHz-6GHz" :

        (filt == skiq_filt_47_to_135MHz) ? "47MHz-135MHz" :
        (filt == skiq_filt_135_to_145MHz) ? "135MHz-145MHz" :
        (filt == skiq_filt_145_to_150MHz) ? "145MHz-150MHz" :
        (filt == skiq_filt_150_to_162MHz) ? "150MHz-162MHz" :
        (filt == skiq_filt_162_to_175MHz) ? "162MHz-175MHz" :
        (filt == skiq_filt_175_to_190MHz) ? "175MHz-190MHz" :
        (filt == skiq_filt_190_to_212MHz) ? "190MHz-212MHz" :
        (filt == skiq_filt_212_to_230MHz) ? "212MHz-230MHz" :
        (filt == skiq_filt_230_to_280MHz) ? "230MHz-280MHz" :
        (filt == skiq_filt_280_to_366MHz) ? "280MHz-366MHz" :
        (filt == skiq_filt_366_to_475MHz) ? "366MHz-475MHz" :
        (filt == skiq_filt_475_to_625MHz) ? "475MHz-625MHz" :
        (filt == skiq_filt_625_to_800MHz) ? "625MHz-800MHz" :
        (filt == skiq_filt_800_to_1175MHz) ? "800MHz-1175MHz" :
        (filt == skiq_filt_1175_to_1500MHz) ? "1175MHz-1.5GHz" :
        (filt == skiq_filt_1500_to_2100MHz) ? "1.5GHz-2.1GHz" :
        (filt == skiq_filt_2100_to_2775MHz) ? "2.1GHz-2775MHz" :
        (filt == skiq_filt_2775_to_3360MHz) ? "2775MHz-3.36GHz" :
        (filt == skiq_filt_3360_to_4600MHz) ? "3.36GHz-4.6GHz" :
        (filt == skiq_filt_4600_to_6000MHz) ? "4.6GHz-6GHz" :

        "unknown";

    return p_filt_str;
}


/**************************************************************************************************/
const char *
rf_port_cstr( skiq_rf_port_t rf_port )
{
    const char* p_rf_port_str =
        (rf_port == skiq_rf_port_unknown) ? "unknown" :
        (rf_port == skiq_rf_port_J1) ? "J1" :
        (rf_port == skiq_rf_port_J2) ? "J2" :
        (rf_port == skiq_rf_port_J3) ? "J3" :
        (rf_port == skiq_rf_port_J4) ? "J4" :
        (rf_port == skiq_rf_port_J5) ? "J5" :
        (rf_port == skiq_rf_port_J6) ? "J6" :
        (rf_port == skiq_rf_port_J7) ? "J7" :
        (rf_port == skiq_rf_port_J300) ? "J300" :
        (rf_port == skiq_rf_port_Jxxx_RX1) ? "Rx1" :
        (rf_port == skiq_rf_port_Jxxx_TX1RX2) ? "Tx1/Rx2" :
        (rf_port == skiq_rf_port_J8) ? "J8" :
        "out-of-range";

    return p_rf_port_str;
}


/**************************************************************************************************/
const char *
rf_port_config_cstr( skiq_rf_port_config_t config )
{
    const char* p_config_str =
        (config == skiq_rf_port_config_fixed) ? "fixed" :
        (config == skiq_rf_port_config_tdd) ? "TDD" :
        (config == skiq_rf_port_config_trx) ? "T/R" :
        "out-of-range";

    return p_config_str;
}


/**************************************************************************************************/
const char *
pps_source_cstr( skiq_1pps_source_t source )
{
    const char* p_source_str =
        (source == skiq_1pps_source_unavailable) ? "unavailable" :
        (source == skiq_1pps_source_external) ? "external" :
        (source == skiq_1pps_source_host) ? "host" :
        "unknown";

    return p_source_str;
}


/**************************************************************************************************/
const char *
fpga_tx_fifo_size_cstr( skiq_fpga_tx_fifo_size_t fifo_size )
{
    const char* p_fifo_size_str =
        (fifo_size == skiq_fpga_tx_fifo_size_4k) ? "4k" :
        (fifo_size == skiq_fpga_tx_fifo_size_8k) ? "8k" :
        (fifo_size == skiq_fpga_tx_fifo_size_16k) ? "16k" :
        (fifo_size == skiq_fpga_tx_fifo_size_32k) ? "32k" :
        (fifo_size == skiq_fpga_tx_fifo_size_64k) ? "64k" :
        (fifo_size == skiq_fpga_tx_fifo_size_unknown) ? "unknown" :
        "invalid";

    return p_fifo_size_str;
}


/**************************************************************************************************/
char
hw_rev_cstr( skiq_hw_rev_t rev )
{
    return (rev < hw_rev_invalid) ? ('A' + rev) : '?';
}


/**************************************************************************************************/
const char*
hw_vers_cstr( skiq_hw_vers_t vers )
{
    const char* p_hw_version =
        (vers == skiq_hw_vers_mpcie_a) ? "MPCIE A" :
        (vers == skiq_hw_vers_mpcie_b) ? "MPCIE B" :
        (vers == skiq_hw_vers_mpcie_c) ? "MPCIE C" :
        (vers == skiq_hw_vers_mpcie_d) ? "MPCIE D" :
        (vers == skiq_hw_vers_mpcie_e) ? "MPCIE E" :
        (vers == skiq_hw_vers_m2_b) ? "M2 B" :
        (vers == skiq_hw_vers_m2_c) ? "M2 C" :
        (vers == skiq_hw_vers_m2_d) ? "M2 D" :
        (vers == skiq_hw_vers_reserved) ? "reserved" :
        "invalid";

    return p_hw_version;
};


/**************************************************************************************************/
const char*
product_vers_cstr( skiq_product_t product_vers )
{
    const char* p_product_version =
        (product_vers == skiq_product_mpcie_001) ? "SKIQ-MPCIE-001" :
        (product_vers == skiq_product_mpcie_002) ? "SKIQ-MPCIE-002" :
        (product_vers == skiq_product_m2_001) ? "SKIQ-M2-001" :
        (product_vers == skiq_product_m2_002) ? "SKIQ-M2-002" :
        (product_vers == skiq_product_reserved) ? "reserved" :
        "invalid";

    return p_product_version;
};


/**************************************************************************************************/
const char *
rfe_lna_state_cstr( rfe_lna_state_t state )
{
    const char * p_lna_state_str =
        (state == lna_bypassed) ? "bypassed" :
        (state == lna_disabled) ? "disabled" :
        (state == lna_enabled) ? "enabled" :
        "unknown";

    return p_lna_state_str;
}


/**************************************************************************************************/
const char *
fpga_device_cstr( skiq_fpga_device_t fpga_device )
{
    const char* p_fpga_device_str =
        (fpga_device == skiq_fpga_device_xc6slx45t) ? "XC6SLX54T" :
        (fpga_device == skiq_fpga_device_xc7a50t) ? "XC7A50T" :
        (fpga_device == skiq_fpga_device_xc7z010) ? "XC7Z010" :
        (fpga_device == skiq_fpga_device_xcku060) ? "XCKU060" :
        (fpga_device == skiq_fpga_device_xcku115) ? "XCKU115" :
        (fpga_device == skiq_fpga_device_xczu3eg) ? "XCZU3EG" :
        "unknown";

    return p_fpga_device_str;
}


/**************************************************************************************************/
const char *
fmc_carrier_cstr( skiq_fmc_carrier_t fmc_carrier )
{
    const char* p_fmc_carrier_str =
        (fmc_carrier == skiq_fmc_carrier_not_applicable) ? "N/A" :
        (fmc_carrier == skiq_fmc_carrier_ams_wb3xzd) ? "AMS WILDSTAR WB3XZD" :
        (fmc_carrier == skiq_fmc_carrier_htg_k800) ? "HiTech Global K800" :
        (fmc_carrier == skiq_fmc_carrier_ams_wb3xbm) ? "AMS WILDSTAR WB3XBM" :
        (fmc_carrier == skiq_fmc_carrier_htg_k810) ? "HiTech Global K810" :
        "unknown";

    return p_fmc_carrier_str;
}


/**************************************************************************************************/
const char *
xport_type_cstr( skiq_xport_type_t type )
{
    const char* p_xport_type_str =
        (type == skiq_xport_type_pcie) ? "PCIe" :
        (type == skiq_xport_type_usb) ? "USB" :
        (type == skiq_xport_type_custom) ? "custom" :
        (type == skiq_xport_type_net) ? "network" :
        "unknown";

    return p_xport_type_str;
}


/**************************************************************************************************/
const char *
xport_level_cstr( skiq_xport_init_level_t level )
{
    const char* p_xport_level_str =
        (level == skiq_xport_init_level_basic) ? "basic" :
        (level == skiq_xport_init_level_full) ? "full" :
        "unknown";

    return p_xport_level_str;
}


/**************************************************************************************************/
const char *
ref_clock_select_cstr( skiq_ref_clock_select_t ref_clock )
{
    const char * p_ref_clock_str = 
        (ref_clock == skiq_ref_clock_internal) ? "internal" :
        (ref_clock == skiq_ref_clock_external) ? "external" :
        (ref_clock == skiq_ref_clock_carrier_edge) ? "carrier edge" :
        (ref_clock == skiq_ref_clock_host) ? "host" :
        (ref_clock == skiq_ref_clock_invalid) ? "invalid" :
        "unknown";

    return p_ref_clock_str;
}
