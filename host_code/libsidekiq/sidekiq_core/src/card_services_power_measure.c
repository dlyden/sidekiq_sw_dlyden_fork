/**
 * @file   card_services_power_measure.c
 * @author  <jeremy@epiq-solutions.com>
 * @date   Mon Nov  5 11:52:49 CST 2018
 *
 * @brief
 *
 *
 * <pre>
 * Copyright 2018 Epiq Solutions, All Rights Reserved
 *
 * </pre>
 */

/***** INCLUDES *****/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "card_services.h"
#include "sidekiq_private.h"
#include "sidekiq_hal.h"

/* enable debug_print and debug_print_plain when DEBUG_POWER_MEAS is defined */
#if (defined DEBUG_POWER_MEAS)
#define DEBUG_PRINT_ENABLED
#endif
#include "libsidekiq_debug.h"


/***** DEFINES *****/

/* power monitor registers: https://confluence.epiq.rocks/pages/viewpage.action?pageId=14188833 */
#define INA219_REG_CONFIG               0x00
#define INA219_REG_SHUNT_VOLTAGE        0x01
#define INA219_REG_BUS_VOLTAGE          0x02
#define INA219_SHUNT_V_TO_MA            2      /* convert shunt voltage to mA current */
#define INA219_BUS_V_SCALE              0.0005 /* bus voltage to actual voltage */

#define SENSOR_LIST(_part,_list)                \
    {                                           \
        .part = (_part),                        \
        .nr_sensors = ARRAY_SIZE(_list),        \
        .list = (_list),                        \
    }

/***** TYPEDEFS *****/



/***** STRUCTS *****/

struct sensor_list
{
    skiq_part_t part;
    uint8_t nr_sensors;
    const uint8_t *list;
};


/***** LOCAL VARIABLES *****/

static const uint8_t x2_sensors[] = { 0x40 };
static const uint8_t x4_sensors[] = { 0x40, 0x41 };

static const struct sensor_list sensor_lists[] = {
    SENSOR_LIST(skiq_x2, x2_sensors),
    SENSOR_LIST(skiq_x4, x4_sensors),
};
static const uint8_t nr_sensor_lists = ARRAY_SIZE(sensor_lists);

/* boolean array to keep track of which power measurement ICs have been configured by card index */
static ARRAY_WITH_DEFAULTS(bool, configured, SKIQ_MAX_NUM_CARDS, false);


/***** LOCAL FUNCTIONS *****/


/*************************************************************************************************/
/** Returns the I2C address (or error) associated with the (card,sensor) tuple.
 *
 * @retval >=0 sensor's I2C address
 * @retval -EINVAL sensor index out of range
 * @retval -ENOTSUP no sensors for associated Sidekiq product
 */
static int32_t
pwr_mon_addr( uint8_t card,
              uint8_t sensor )
{
    skiq_part_t part = _skiq_get_part( card );
    int32_t status = -ENOTSUP;
    uint8_t i;

    for ( i = 0; i < nr_sensor_lists; i++ )
    {
        if ( part == sensor_lists[i].part )
        {
            if ( sensor < sensor_lists[i].nr_sensors )
            {
                status = sensor_lists[i].list[sensor];
            }
            else
            {
                status = -EINVAL;
            }
            break;
        }
    }

    return status;
}


/*************************************************************************************************/
static int32_t
ina219_configure( uint8_t card,
                  uint8_t i2c_addr )
{
    /* INA219 configuration detailed on this page --
       https://confluence.epiq.rocks/pages/viewpage.action?pageId=14188833 */

    /* BRNG    0                 (16V FSR) */
    /* PG      |0 0              (+- 40mV) */
    /* BADC    || |101 1         (8x averaging, 4.26mS conversion) */
    /* SADC    || |||| |101 1    (8x averaging, 4.26mS conversion) */
    /* MODE    || |||| |||| |111 (Shunt and bus, continuous) */
    /*       0000 0101 1101 1111 = 0 5 D F = 0x05DF */
    uint8_t data[] = { INA219_REG_CONFIG, 0x05, 0xDF };

    return hal_write_i2c( card, i2c_addr, data, ARRAY_SIZE(data) );
}


/*************************************************************************************************/
static int32_t
cond_ina219_configure( uint8_t card )
{
    int32_t status = 0;

    if ( !configured[card] )
    {
        uint8_t sensor = 0, i2c_addr;

        debug_print("Power measurement sensors for card %u are not configured\n", card);
        for ( sensor = 0; ( sensor < UINT8_MAX ) && ( status == 0 ); sensor++)
        {
            status = pwr_mon_addr( card, sensor );
            if ( status >= 0 )
            {
                i2c_addr = (uint8_t)status;
                debug_print("Configuring sensor index %u (addr 0x%02X) for card %u\n", sensor,
                            i2c_addr, card);
                status = ina219_configure( card, i2c_addr );
            }
            else if ( status == -EINVAL )
            {
                /* an -EINVAL from pwr_mon_addr() means the sensor index was out-of-range (end of
                 * list), so all sensors must be been configured. */
                configured[card] = true;
                debug_print("Completed configuring all sensor(s) for card %u\n", card);
                status = 0;
                break;
            }
        }
    }

    return status;
}


/*************************************************************************************************/
static int32_t
ina219_read_current_voltage( uint8_t card,
                             uint8_t i2c_addr,
                             float *p_current,
                             float *p_voltage )
{
    int32_t status = 0;
    uint16_t current_u16, voltage_u16;

    if ( status == 0 )
    {
        status = hal_read_beu16_i2c_reg( card, i2c_addr, INA219_REG_SHUNT_VOLTAGE, &current_u16 );
    }

    if ( status == 0 )
    {
        status = hal_read_beu16_i2c_reg( card, i2c_addr, INA219_REG_BUS_VOLTAGE, &voltage_u16 );
    }

    if ( status == 0 )
    {
        debug_print("Raw: shunt voltage %u, bus voltage %u\n", current_u16, voltage_u16);
        *p_current = (float)current_u16 * INA219_SHUNT_V_TO_MA;
        *p_voltage = (float)voltage_u16 * INA219_BUS_V_SCALE;
    }

    return (status);
}


/*************************************************************************************************/


/***** GLOBAL FUNCTIONS *****/


/*************************************************************************************************/
/**
     @retval 0 Success
     @retval -ENOTSUP Sidekiq product does not have any power measurement sensors
     @retval -EINVAL Sensor index is out of range
     @retval -EIO I/O communication error occurred during measurement
 */
int32_t card_read_current_voltage( uint8_t card,
                                   uint8_t sensor,
                                   float *p_current,
                                   float *p_voltage )
{
    int32_t status = -ENOTSUP;

    status = cond_ina219_configure( card );

    if ( status == 0 )
    {
        status = pwr_mon_addr( card, sensor );
        if ( status >= 0 )
        {
            skiq_part_t part = _skiq_get_part( card );
            uint8_t i2c_addr = (uint8_t)status;
            switch( part )
            {
                case skiq_x2:
                case skiq_x4:
                    status = ina219_read_current_voltage( card, i2c_addr, p_current, p_voltage );
                    if ( status != 0 )
                    {
                        /* squash non-zero return code from ina219_read_current_voltage to indicate
                         * generic I/O communication error */
                        status = -EIO;
                    }
                    break;

                default:
                    debug_print("%s part (card %u) does not have a power sensor, even though"
                                " pwr_mon_addr returned an address\n", part_cstr( part ), card );
                    status = -ENOTSUP;
                    break;
            }
        }
        else
        {
            debug_print("Card %u (%s) and sensor index %u are an invalid combination\n", card,
                        part_cstr( _skiq_get_part( card ) ), sensor);
        }
    }

    return (status);
}
