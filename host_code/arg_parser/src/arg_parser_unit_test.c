/* <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>*/

/***** Includes *****/
#include <stdio.h>
#include <inttypes.h>

#include "arg_parser.h"


/*****************************************************************************/
/** This application allows us to perform a unit test of the argument parser,
    ensuring that all command line arguments are handled appropriately.
    
    This isn't an exhaustive list, but it should help us to vet out most of the
    potential bugs.
    
    Test Cases:
        1) PASS
            --u8=1 --u16=2 --u32=3 --u64=4 \
            --s8=-1 --s16=-2 --s32=-3 --s64=-4 \
            --float=1.0 --double=2.0 --str=foobar \
            --bool
        
        2) FAIL
            --u8=-1

        3) FAIL
            --u16=-1

        4) FAIL
            --u32=-1
        
        5) FAIL
            --u64=-1

        6) FAIL
            --s8=18446744073709551615

        7) FAIL
            --s16=18446744073709551615

        8) FAIL
            --s32=18446744073709551615

        9) FAIL
            --s64=18446744073709551615

        10) FAIL
            --u8=foobar
*/

/***** Local Variables *****/
// Help text displayed when "-h" or "--help" is specified via command line.
static const char* p_help_short = "- test arg_parser.c";
static const char* p_help_long = "\
Ensure that arg_parser.c isn't bork.";


/***** Global Function Definitions *****/
int main(int argc, char* argv[])
{
    int32_t status = 0;
    // Variables supported by "arg_parser.h", need to test all of them.
    uint8_t u8_var = 0;
    uint16_t u16_var = 0;
    uint32_t u32_var = 0;
    uint64_t u64_var = 0;
    int8_t s8_var = 0;
    int16_t s16_var = 0;
    int32_t s32_var = 0;
    int64_t s64_var = 0;
    float f_var = 0.0;
    double d_var = 0.0;
    char* p_test = NULL;
    char* p_str = NULL;
    bool bool_var = false;

    // Struct from "arg_parser.h" used for adding command line arguments.
    struct application_argument p_args[] =
    {
        APP_ARG_REQ("test", 't', "required", "VAL", &p_test, STRING_VAR_TYPE),
        APP_ARG_OPT("u8", 0, "uint8_t", "VAL", &u8_var, UINT8_VAR_TYPE),
        APP_ARG_OPT("u16", 0, "uint16_t", "VAL", &u16_var, UINT16_VAR_TYPE),
        APP_ARG_OPT("u32", 0, "uint32_t", "VAL", &u32_var, UINT32_VAR_TYPE),
        APP_ARG_OPT("u64", 0, "uint64_t", "VAL", &u64_var, UINT64_VAR_TYPE),
        APP_ARG_OPT("s8", 0, "int8_t", "VAL", &s8_var, INT8_VAR_TYPE),
        APP_ARG_OPT("s16", 0, "int16_t", "VAL", &s16_var, INT16_VAR_TYPE),
        APP_ARG_OPT("s32", 0, "int32_t", "VAL", &s32_var, INT32_VAR_TYPE),
        APP_ARG_OPT("s64", 0, "int64_t", "VAL", &s64_var, INT64_VAR_TYPE),
        APP_ARG_OPT("float", 0, "float", "VAL", &f_var, FLOAT_VAR_TYPE),
        APP_ARG_OPT("double", 0, "double", "VAL", &d_var, DOUBLE_VAR_TYPE),
        APP_ARG_OPT("str", 0, "string", "TEXT", &p_str, STRING_VAR_TYPE),
        APP_ARG_OPT("bool", 0, "bool", NULL, &bool_var, BOOL_VAR_TYPE),
        APP_ARG_TERMINATOR,
    };

    // Function from "arg_parser.h" used for parsing arguments.
    status = arg_parser(argc, argv, p_help_short, p_help_long, p_args);
    if( 0 != status )
    {
        perror("Command Line");
        arg_parser_print_help(argv[0], p_help_short, p_help_long, p_args);
        return (-1);
    }

    // Quick debug print.
    printf("test   =    %s\n"
           "u8     =    %" PRIu8 "\n"
           "u16    =    %" PRIu16" \n"
           "u32    =    %" PRIu32 "\n"
           "u64    =    %" PRIu64 "\n"
           "s8     =    %" PRId8 "\n"
           "s16    =    %" PRId16 "\n"
           "s32    =    %" PRId32 "\n"
           "s64    =    %" PRId64 "\n"
           "float  =    %e\n"
           "double =    %e\n"
           "str    =    %s\n"
           "bool   =    %s\n",
           (p_test == NULL) ? "(NULL)" : p_test,
           u8_var,
           u16_var,
           u32_var,
           u64_var,
           s8_var,
           s16_var,
           s32_var,
           s64_var,
           f_var,
           d_var,
           p_str,
           (bool_var == true ? "true" : "false"));

    return 0;
}
