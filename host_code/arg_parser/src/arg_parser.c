/* <pre>
 * Copyright 2016,2018 Epiq Solutions, All Rights Reserved
 * </pre>
 */

/***** Includes *****/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include "glib.h"
#include "arg_parser.h"


/***** Local Function Prototypes *****/
static int32_t _arg_parser(
    int argc,
    char** argv,
    const char* p_help_short,
    const char* p_help_long,
    struct application_argument* p_app_arg,
    bool print_help_and_exit);

static bool _is_terminator(
    struct application_argument* p_param);

static int32_t _convert(
    void* p_src, 
    void* p_dst, 
    enum variable_type dst_type);

static void _print_invalid_argument(
    GOptionEntry* p_arg_entry,
    enum variable_type type);

static void _print_required_argument(
    GOptionEntry* p_arg_entry);

static void _print_unknown_argument(
    char* p_unknown_arg);


/***** Global Function Defines *****/

/*****************************************************************************/
/** The arg_parser function simply calls _arg_parser with
    print_help_and_exit=false to perform a full argument parsing.
*/
int32_t arg_parser(
    int argc,
    char** argv,
    const char* p_help_short,
    const char* p_help_long,
    struct application_argument* p_app_arg)
{
    return _arg_parser(argc, argv, p_help_short, p_help_long, p_app_arg, false);
}


/*****************************************************************************/
/** The arg_parser_print_help function can be called at any point where the
    application needs to print the help text and exit. This can be useful in
    cases where a variable check on a command line argument outside of
    arg_parser() failed and displaying the help text would clarify the user's
    mistake. This function will call exit() with a non-zero value upon
    completion.

    @param p_app_name String representation of the application's name. This
           argument should be populated with something akin to argv[0].
    @param p_help_short String used for short description of application when
           "-h" or "--help" is specified on the command line.
    @param p_help_long String used for long description of application when
           "-h" or "--help" is specified on the command line.
    @param p_app_arg Array of command line arguments for the application. Note
           that these should be created using either the APP_ARG_OPT() or 
           APP_ARG_REQ() macro with the final element being the 
           APP_ARG_TERMINATOR macro. 

    @return void
*/
void arg_parser_print_help(
    const char* p_app_name,
    const char* p_help_short,
    const char* p_help_long,
    struct application_argument* p_param)
{
    char* help_argv[] = { (char*) p_app_name, NULL };
    int help_argc = 1;
    int32_t r = 0;

    r = _arg_parser(help_argc, help_argv, p_help_short, p_help_long, p_param, true);
    if( 0 != r )
    {
        // We should NEVER get here, but it doesn't hurt to check.
        perror("Print Help");
    }

    // Call to _arg_parser shouldn't return. Force the application to end if for
    // some reason it does.
    exit(-1);
}

/***** Local Function Defines *****/

/*****************************************************************************/
/** The _arg_parser function is used to abstract some of the code set up
    required to parse command line arguments.

    @param argc Number of arguments received from "main" function.
    @param argv Pointer to arguments received from "main" function.
    @param p_help_short String used for short description of application when
           "-h" or "--help" is specified on the command line.
    @param p_help_long String used for long description of application when
           "-h" or "--help" is specified on the command line.
    @param p_app_arg Array of command line arguments for the application. Note
           that these should be created using the APP_ARG() macro with the 
           final element being the APP_ARG_TERMINATOR macro.
    @param print_help_and_exit Boolean indicating whether or not to print help
           and exit with an error

    @return int Zero on success, negative value on error. Errno set to one of 
            the following.
                - ENOMEM: Memory allocation error or corruption.
                - ELIBBAD: Bad Glib instance.
                - ERANGE: Variable value over/underflow.
                - EINVAL: Invalid variable type.
*/
static int32_t _arg_parser(
    int argc,
    char** argv,
    const char* p_help_short,
    const char* p_help_long,
    struct application_argument* p_app_arg,
    bool print_help_and_exit)
{
    GOptionEntry* p_arg_entry = NULL;
    GOptionContext* p_context = NULL;
    GError* p_error = NULL;
    gboolean parse_success = FALSE;
    gboolean* p_bool_var = NULL;
    gchar** pp_str_var = NULL;
    void* p_tmp = NULL;
    uint32_t n_bool = 0;
    uint32_t n_str = 0;
    uint32_t n_gopt = 1;    // NOTE: need extra entry for terminating array
    uint32_t n = 0;
    int32_t r = -1;
    
    if( NULL == p_app_arg )
    {
        errno = EINVAL;
        return -1;
    }
    else if( true == _is_terminator(&p_app_arg[0]) )
    {
        // Corner case where p_app_arg consists only of APP_ARG_TERMINATOR.
        return 0;
    }

    // For the following, I'd recommend reading up on realloc(), as it has 
    // some intereseting yet well defined behavior at some of the corner cases.
    
    // Build up the GOptionEntry array and its required Glib variable. For our
    // argument parser, we are going to restrict ourselves to gbooleans for
    // simple enable/disable style arguments and treat everything else, whether
    // it be a number or text, as a gchar* string. 
    while( false == _is_terminator(&p_app_arg[n]) )
    {
        // Add a new GOptionEntry to be used by the command line parser.
        p_tmp = NULL;
        p_tmp = realloc(p_arg_entry, (sizeof(GOptionEntry) * (n_gopt + 1)));
        if( NULL == p_tmp )
        {
            // realloc() failed!
            errno = ENOMEM;
            goto arg_glib_parse_exit;
        }
        p_arg_entry = (GOptionEntry*) p_tmp;
        n_gopt++;

        if( BOOL_VAR_TYPE == p_app_arg[n].type )
        {
            // boolean command line argument
            p_tmp = NULL;
            p_tmp = realloc(p_bool_var, (sizeof(gboolean) * (n_bool + 1)));
            if( NULL == p_tmp )
            {
                // realloc() failed!
                errno = ENOMEM;
                goto arg_glib_parse_exit;
            }
            p_bool_var = (gboolean*) p_tmp;
            p_bool_var[n_bool++] = FALSE; 
        }
        else
        {
            // string command line argument
            p_tmp = NULL;
            p_tmp = realloc(pp_str_var, (sizeof(gchar*) * (n_str + 1)));
            if( NULL == p_tmp )
            {
                // realloc() failed!
                errno = ENOMEM;
                goto arg_glib_parse_exit;
            }
            pp_str_var = (gchar**) p_tmp;
            pp_str_var[n_str++] = NULL;
        }
        // Increment our index variable for the next array entry.
        n++;
    }

    n_bool = 0;
    n_str = 0;

    // Now that all memory has been assigned and will no longer change its
    // location, perform the actual initialization assignment of the 
    // GOptionEntry array.
    for( n = 0; n < (n_gopt - 1); n++ )
    { 
        // Initialize using values passed in by application_argument struct.
        p_arg_entry[n].flags = 0;
        p_arg_entry[n].long_name = p_app_arg[n].p_long_flag;
        p_arg_entry[n].short_name = p_app_arg[n].short_flag;
        p_arg_entry[n].description = p_app_arg[n].p_info;
        p_arg_entry[n].arg_description = p_app_arg[n].p_label;
        // Remember, everything is either a gboolean or a gchar*.
        p_arg_entry[n].arg = 
            (p_app_arg[n].type == BOOL_VAR_TYPE) ?
            G_OPTION_ARG_NONE :
            G_OPTION_ARG_STRING;
        // Assign pointer to variable that will take the command line value.
        p_arg_entry[n].arg_data =
            (p_app_arg[n].type == BOOL_VAR_TYPE) ?
            (gpointer) &p_bool_var[n_bool++] : 
            (gpointer) &pp_str_var[n_str++];
    }
    // Null identifier required to terminate GOptionEntry array. If this isn't
    // set, you'll get a friendly segmentation fault from inside of Glib.
    p_arg_entry[n].long_name = NULL;

    // Glib Option required function calls used for initialization, parsing,
    // and when finished, freeing all Glib Option related resources.
    p_context = g_option_context_new(p_help_short);
    g_option_context_set_ignore_unknown_options(p_context, TRUE);
    g_option_context_set_summary(p_context, p_help_long);
    g_option_context_add_main_entries(p_context, p_arg_entry, NULL);

    // now that p_app_arg has been parsed and a context has been established,
    // conditionally display the help message and exit with a non-zero value
    if ( print_help_and_exit )
    {
        gchar *help;

        help = g_option_context_get_help (p_context, TRUE, NULL);
        g_print ("%s", help);
        g_free (help);

        g_option_context_free(p_context);
        if( NULL != p_bool_var )
        {
            free(p_bool_var);
        }
        if( NULL != pp_str_var )
        {
            free(pp_str_var);
        }
        if( NULL != p_arg_entry )
        {
            free(p_arg_entry);
        }

        exit (-1);
    }

    parse_success = g_option_context_parse(p_context, &argc, &argv, &p_error);
    // Check to see if the Glib option parser failed in some manner.
    if( TRUE != parse_success )
    {
        switch( p_error->code )
        {
        case( G_OPTION_ERROR_UNKNOWN_OPTION ):
        case( G_OPTION_ERROR_BAD_VALUE ):
            // These are the only two errors that will be returned that 
            // indicate that Glib isn't at fault and that the user provided a
            // bad command line value.
            errno = EINVAL;
            break;
            
        default:
            // Glib has gone and done a goof.
#ifdef __MINGW32__
            /* return as if the library is not found */
            errno = ENOENT;
#else
            errno = ELIBBAD;
#endif /* __MINGW32__ */
        }
        r = -1;
        goto arg_glib_parse_exit;
    }
    
    for( n = 1; n < argc; n++ )
    {
        // During execution of g_option_context_parse, the function will set 
        // argv values to NULL if they are used by a command line argument. 
        // Anything left over is invalid in our case.
        if( NULL != argv[n] )
        {
            _print_unknown_argument(argv[n]);
            errno = EINVAL;
            r = -1;
            goto arg_glib_parse_exit;
        }
    }
    
    // Now we convert from Glib specific variable types to those that we really
    // want for our application.
    for( n = 0; n < (n_gopt - 1); n++ )
    {
        bool is_set = false;    /* for a given argument, default to not set */

        r = _convert(
                p_arg_entry[n].arg_data,
                p_app_arg[n].p_var, 
                p_app_arg[n].type);
        // Manually handle error cases that involve user input problems.
        if( (EINVAL == r) || (ERANGE == r) ) 
        {
            // User provided an invalid value for the argument.
            _print_invalid_argument(&p_arg_entry[n], p_app_arg[n].type);
        }
        else if( ENODATA == r )
        {
            // No data provided, this may or may not be an issue.
            if( true == p_app_arg[n].required )
            {
                _print_required_argument(&p_arg_entry[n]);
            }
            else
            {
                // Argument wasn't required, don't treat this as an error.
                r = 0;
            }
        }
        else
        {
            /* set the `is_set` flag to true when the conversion goes okay, meaning data is
             * present */
            is_set = true;
        }

        /* store the `is_set` flag if the user provided a non-NULL reference */
        if ( p_app_arg[n].p_is_set != NULL )
        {
            *p_app_arg[n].p_is_set = is_set;
        }

        // If our return code is still set to nonzero, we need to exit.
        if( 0 != r )
        {
            errno = r;
            r = -1;
            goto arg_glib_parse_exit;
        }
    }
    
arg_glib_parse_exit:
    // Free all allocated memory.
    if (NULL != p_context)
    {
        g_option_context_free(p_context);
    }
    if( NULL != p_bool_var )
    {
        free(p_bool_var);
    }
    if( NULL != pp_str_var )
    {
        free(pp_str_var);
    }
    if( NULL != p_arg_entry )
    {
        free(p_arg_entry);
    }

    return r;
}

/*****************************************************************************/
/** The _convert function is used to assist in converting a Glib gboolean or 
    gchar* into either a standard integer type featured within stdint.h, such 
    as a uint8_t or int32_t, a stdbool.h style boolean, or a string. 
    
    If p_src is a pointer to a gchar* which points to NULL, the function will
    return a zero without doing anything as this indicates that no command line
    argument was specified on the command line.
    
    If the destination is an integer, the resulting  number is checked to 
    ensure it fits within the bounds of the type it is to be converted to, 
    returning an error if an overflow/underflow occurs.

    @param p_src Pointer to Glib source variable.
    @param p_dst Pointer to standard C destination variable.
    @param dst_type Enumerated value used to declare the destination type.

    @return int Zero on success, one of the following ERRNO values on error.
               - ENOMEM: bad pointers
               - ERANGE: value exceeds range of destination variable
               - EINVAL: value is not valid in regards to detination variable
               - ENODATA: no value to convert
*/
int32_t _convert(
    void* p_src, 
    void* p_dst, 
    enum variable_type dst_type)
{
    double exp_tmp;
    int64_t tmp;
    char* p_str = NULL;
    char* p_err = NULL;
    
    // Welcome to pointer hell...
    //
    //      Lasciate ogne speranza, voi ch'intrate
    //        Abandon all hope, ye who enter here
    
    // First, check obvious error cases...
    if( (NULL == p_src) || (NULL == p_dst) )
    {
        // We should always have some sort of source variable to convert. If 
        // either of these are NULL, it implies that our memory is corrupted
        // in an irrecoverable manner.
        return ENOMEM;
    }

    // The variable p_src is either a pointer to a gboolean or a gchar*.
    if( BOOL_VAR_TYPE == dst_type )
    {
        if( (TRUE != *(gboolean*) p_src) && (FALSE != *(gboolean*) p_src) )
        {
            return ERANGE;
        }
        *(bool*) p_dst = (TRUE == *(gboolean*) p_src) ? true : false;
        return 0;
    }
    else if( STRING_VAR_TYPE == dst_type ) 
    {
        if( NULL == *(char**) p_src )
        {
            // If set to NULL, no argument was passed in at the command line.
            // Note, this catches something like "-s ".
            return ENODATA;
        }
        else if( '\0' == **(char**) p_src ) 
        {
            // If this is set to the terminating character, the user entered an
            // empty string. This could have been done by something like 
            // "--string=". Treat this as being invalid.
            return EINVAL;
        }
        *(char**) p_dst = *(char**) p_src;
        return 0;
    }

    // Beyond here, we are dealing with converting a gchar* to a specific
    // numeric type. This should be easy, right?
    p_str = *(char**) p_src;
    if( NULL == p_str )
    {
        // If set to NULL, no argument was passed in at the command line.
        return ENODATA;
    }
    else if( '\0' == **(char**) p_src )
    {
        // If this is set to NULL, the user forgot to supply a value. This
        // could have been done by doing something like "--number=". Treat this
        // as being invalid.
        return EINVAL;
    }
    
    if( FLOAT_VAR_TYPE == dst_type )
    {
        // Special case for floating point variables.
        // NOTE: Documentation on strtof advises to set errno to zero before
        // conversion. We'll do as they say I guess.....
        errno = 0;
        *(float*)p_dst = strtof(p_str, &p_err);
        // Functions in the strto* family will set errno if there is an 
        // over/underflow converting from a string to the desired numeric type.
        if( (ERANGE == errno) || (EINVAL == errno) )
        {
            return errno;
        }
        // If the entire string wasn't consumed, p_err will point to something
        // other than the terminating character. If any invalid chracters were 
        // present in the string, we're going to treat it as an error and halt 
        // the conversion.
        else if( '\0' != *p_err )
        {
            return EINVAL;
        }
        return 0;
    }
    else if( DOUBLE_VAR_TYPE == dst_type )
    {
        // Special case for double floating point variables.
        // NOTE: Documentation on strtod advises to set errno to zero before
        // conversion. We'll do as they say I guess.....
        errno = 0;
        *(double*)p_dst = strtod(p_str, &p_err);
        if( (ERANGE == errno) || (EINVAL == errno) )
        {
            return errno;
        }
        else if( '\0' != *p_err )
        {
            return EINVAL;
        }
        return 0;
    }
    else if( UINT64_VAR_TYPE == dst_type )
    {
        // Special case UINT64, the largest integer supported by stdint.h.
        *(uint64_t*)p_dst = strtoull(p_str, &p_err, 0);
        if( (ERANGE == errno) || (EINVAL == errno) )
        {
            return errno;
        }
        else if( ('e' == *p_err) || ('E' == *p_err) || ('.' == *p_err) )
        {
            // we have a number in scientific notation, attempt conversion
            exp_tmp = strtod(p_str, &p_err);
            *(uint64_t*)p_dst = (uint64_t) exp_tmp;
            if( (ERANGE == errno) || (EINVAL == errno) )
            {
                return errno;
            }
            else if( 0.0 > exp_tmp )
            {
                // negative double, bad for unsigned
                return EINVAL;
            }
        }
        // catches exponent reattempt as well
        if( '\0' != *p_err )
        {
            return EINVAL;
        }
        return 0;
    }
    
    // The remaining cases can be parsed using just strtoll.
    tmp = strtoll(p_str, &p_err, 0);
    if( (ERANGE == errno) || (EINVAL == errno) )
    {
        return errno;
    }
    else if( ('e' == *p_err) || ('E' == *p_err) || ('.' == *p_err) )
    {
        // we have a number in scientific notation, attempt conversion
        exp_tmp = strtod(p_str, &p_err);
        tmp = (int64_t) exp_tmp;
        if( (ERANGE == errno) || (EINVAL == errno) )
        {
            return errno;
        }
    }
    // catches exponent reattempt as well
    if( '\0' != *p_err )
    {
        return EINVAL;
    }
    
    // Check over/underflow conditions before assigning to stdint.h variable.
    switch( dst_type )
    {
    case( INT8_VAR_TYPE ):
        if( (INT8_MIN > tmp) || (INT8_MAX < tmp) ) return ERANGE;
        *(int8_t*)p_dst = (int8_t) tmp;
        break;

    case( UINT8_VAR_TYPE ):
        if( (0 > tmp) || (UINT8_MAX < tmp) ) return ERANGE;
        *(uint8_t*)p_dst = (uint8_t) tmp;
        break;
    
    case( INT16_VAR_TYPE ):
        if( (INT16_MIN > tmp) || (INT16_MAX < tmp) ) return ERANGE;
        *(int16_t*)p_dst = (int16_t) tmp;
        break;

    case( UINT16_VAR_TYPE ):
        if( (0 > tmp) || (UINT16_MAX < tmp) ) return ERANGE;
        *(uint16_t*)p_dst = (uint16_t) tmp;
        break;
    
    case( INT32_VAR_TYPE ):
        if( (INT32_MIN > tmp) || (INT32_MAX < tmp) ) return ERANGE;
        *(int32_t*)p_dst = (int32_t) tmp;
        break;

    case( UINT32_VAR_TYPE ):
        if( (0 > tmp) || (UINT32_MAX < tmp) ) return ERANGE;
        *(uint32_t*)p_dst = (uint32_t) tmp;
        break;

    case( INT64_VAR_TYPE ):
        *(int64_t*)p_dst = (int64_t) tmp;
        break;

    default:
        // Making up your own integer types is definitely not encouraged.
        return EINVAL;
    }

    return 0;
}

/*****************************************************************************/
/** Utility function used for determining if the application argument struct is
    set to the APP_ARG_TERMINATOR macro value.

    @param p_app_arg Pointer to application argument struct.

    @return bool Returns "true" if application argument is APP_ARG_TERMINATOR.
*/
bool _is_terminator(struct application_argument* p_app_arg)
{
    const struct application_argument term = APP_ARG_TERMINATOR;
    
    if( NULL == p_app_arg )
    {
        return true;
    }
    
    return ( (p_app_arg->p_long_flag != term.p_long_flag) ? false
           : (p_app_arg->short_flag  != term.short_flag)  ? false
           : (p_app_arg->p_info      != term.p_info)      ? false
           : (p_app_arg->p_var       != term.p_var)       ? false
           : (p_app_arg->type        != term.type)        ? false
           : true );
}

/*****************************************************************************/
/** Utility function used for printing an error message for an invalid 
    argument value.

    @param p_app_arg Pointer to application argument that was invalid.
    @param type The expected variable type for this argument.

    @return void
*/
void _print_invalid_argument(
    GOptionEntry* p_arg_entry,
    enum variable_type type)
{
    char* p_type = NULL;
    char* p_bad_data = "NULL";

    switch( type )
    {
    case( BOOL_VAR_TYPE ):
        p_type = "boolean";
        break;

    case( STRING_VAR_TYPE ):
        p_type = "string";
        break;
    
    case( INT8_VAR_TYPE ):
        p_type = "int8";
        break;
    
    case( UINT8_VAR_TYPE ):
        p_type = "uint8";
        break;
    
    case( INT16_VAR_TYPE ):
        p_type = "int16";
        break;
    
    case( UINT16_VAR_TYPE ):
        p_type = "uint16";
        break;
    
    case( INT32_VAR_TYPE ):
        p_type = "int32";
        break;
    
    case( UINT32_VAR_TYPE ):
        p_type = "uint32";
        break;
    
    case( INT64_VAR_TYPE ):
        p_type = "int64";
        break;
    
    case( UINT64_VAR_TYPE ):
        p_type = "uint64";
        break;
    
    case( FLOAT_VAR_TYPE ):
        p_type = "float";
        break;
    
    case( DOUBLE_VAR_TYPE ):
        p_type = "double";
        break;

    default:
        // If we see this, there's a major problem. Someone tried to 
        // parse a value that is not supported.
        p_type = "undefined";
    }

    if( '\0' != **(char**) p_arg_entry->arg_data )
    {
        p_bad_data = *(char**) p_arg_entry->arg_data;
    }
    
    if( (0 != p_arg_entry->short_name) && (NULL != p_arg_entry->long_name) )
    {
        printf(
            "Command Line: Invalid -%c/--%s argument %s, expected %s value\n",
            p_arg_entry->short_name,
            p_arg_entry->long_name,
            p_bad_data,
            p_type);
    }
    else if( 0 != p_arg_entry->short_name )
    {
        printf(
            "Command Line: Invalid -%c argument %s, expected %s value\n",
            p_arg_entry->short_name,
            p_bad_data,
            p_type);
    }
    else if( NULL != p_arg_entry->long_name )
    {
        printf(
            "Command Line: Invalid --%s argument %s, expected %s value\n",
            p_arg_entry->long_name,
            p_bad_data,
            p_type);
    }
}

/*****************************************************************************/
/** Utility function used for printing an error message for a missing required
    argument value.

    @param p_app_arg Pointer to application argument that was invalid.

    @return void
*/
void _print_required_argument(
    GOptionEntry* p_arg_entry)
{
    if( (0 != p_arg_entry->short_name) && (NULL != p_arg_entry->long_name) )
    {
        printf(
            "Command Line: Missing -%c/--%s argument\n", 
            p_arg_entry->short_name, 
            p_arg_entry->long_name);
    }
    else if( 0 != p_arg_entry->short_name )
    {
        printf(
            "Command Line: Missing -%c argument\n", 
            p_arg_entry->short_name);
    }
    else if( NULL != p_arg_entry->long_name )
    {
        printf(
            "Command Line: Missing --%s argument\n", 
            p_arg_entry->long_name);
    }
}

/*****************************************************************************/
/** Utility function used for printing an error message for an argument 
    provided on the command line that does not belong to any of the listed
    command line arguments

    @param p_unknown_arg Pointer to the unknown command line argument.

    @return void
*/
void _print_unknown_argument(
    char* p_unknown_arg)
{
    if( NULL != p_unknown_arg )
    {
        printf("Command Line: Unknown option \'%s\'\n", p_unknown_arg);
    }
}
