#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <stdbool.h>

#include <string.h>

#include "sidekiq_usb_cmds.h"

#define BUF_SIZE                        (10000)

struct libusb_context *pContext = NULL;
struct libusb_device_handle *pHandle = NULL;

bool initialize_usb(void)
{
    if( libusb_init(&pContext) != 0 )
    {
        return false;
    }
    libusb_set_debug( pContext, 3 ); // maximum debug
    return true;
}

void shutdown_usb(void)
{
    if( pContext != NULL )
    {
        libusb_exit(pContext);
        pContext = NULL;
    }
}

bool reserve_usb_dev(void)
{
    ssize_t num_devices = 0;
    bool bDevFound = false;

    struct libusb_device **ppDevList;
    struct libusb_device *pDevice;
    struct libusb_device_descriptor desc;

    int i=0;

    num_devices = libusb_get_device_list( pContext, &ppDevList );
    printf("Found %zd USB devices\n", num_devices);
    for( i=0; i<num_devices; i++ )
    {
        pDevice = ppDevList[i];
        // get the descriptor
        if( libusb_get_device_descriptor( pDevice, &desc ) == 0 )
        {
            if( desc.idVendor == VENDOR_ID && desc.idProduct == PRODUCT_ID )
            {
                printf("Found Sidekiq!!\n");
                // open a handle
                if( libusb_open( pDevice, &pHandle ) == 0 )
                {
                    bDevFound = true;
                    printf("Handle opened successfully!\n");
                }
                break;
            }
        }
        else
        {
            printf("Failed to get device descriptor\n");
        }
    
    }
    // free dev list and unreference all the devices
    libusb_free_device_list( ppDevList, 1 );
    
    return bDevFound;
}

void release_usb_dev(void)
{
    if( pHandle != NULL )
    {
        libusb_close( pHandle );
        pHandle = NULL;
    }
}

int write_usb_cmd( SIDEKIQ_USB_CMDS cmd, 
           uint8_t *pAddr,
           uint16_t len,
           uint8_t *pData )
{
    int num_bytes = 0;
    
    num_bytes = 
    libusb_control_transfer( pHandle,               // USB device handle
                 WRITE_REQUEST_TYPE,                // requestType SETUPDAT[0]
                 cmd,                               // request=SETUPDAT[1]
                 *((uint16_t*)(pAddr)),             // wValue=SETUPDAT[2:3]
                 0,                                 // wIndex=SETUPDAT[4:5]
                 pData,                             // data
                 len,                               // wLength=SETUPDAT[6:7]
                 0 );//20000 );                     // timeout of 100 ms

    printf("Desired bytes sent %u, actual bytes sent %d\n",
       len, num_bytes);
    return num_bytes;
}


int main(int argc, char *argv[])
{
    uint8_t addr[2];
    uint8_t buf[BUF_SIZE];

    if( initialize_usb() != true )
    {
        printf("Error: unable to initialize USB\n");
        return (-1);
    }

    if( reserve_usb_dev() != true)
    {
        printf("Error: unable to find/reserve USB device 0x%.4x:0x%.4x\n",
               VENDOR_ID, PRODUCT_ID);
        return (-1);
    }

    printf("Erasing flash (this may take >30 seconds)\n");
    addr[0] = 0;
    addr[1] = 0;
    write_usb_cmd( CMD_FLASH_BULK_ERASE,
                   addr,
                   1,
                   buf );
    printf("Flash erase complete!\n");

    return (0);
}
