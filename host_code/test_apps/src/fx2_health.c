#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <stdbool.h>

#include <string.h>

#include "sidekiq_usb_cmds.h"

struct libusb_context *pContext = NULL;
struct libusb_device_handle *pHandle = NULL;

bool initialize_usb(void)
{
    const struct libusb_version *lusb_ver = libusb_get_version();
    printf("libusb version %d.%d.%d\n", lusb_ver->major, lusb_ver->minor, lusb_ver->micro);
    if( libusb_init(&pContext) != 0 )
    {
        return false;
    }
    libusb_set_debug( pContext, 3 ); // maximum debug
    return true;
}

void shutdown_usb(void)
{
    if( pHandle != NULL )
    {
        libusb_close( pHandle );
        pHandle = NULL;
    }
    if( pContext != NULL )
    {
        libusb_exit(pContext);
        pContext = NULL;
    }
}

bool reserve_usb_dev(void)
{
    ssize_t num_devices = 0;
    bool bDevFound = false;

    struct libusb_device **ppDevList;
    struct libusb_device *pDevice;
    struct libusb_device_descriptor desc;

    int i=0;

    num_devices = libusb_get_device_list( pContext, &ppDevList );
    printf("Found %zd USB devices\n", num_devices);
    for( i=0; i<num_devices; i++ )
    {
        pDevice = ppDevList[i];
        // get the descriptor
        if( libusb_get_device_descriptor( pDevice, &desc ) == 0 )
        {
            if( desc.idVendor == VENDOR_ID && desc.idProduct == PRODUCT_ID )
            {
                printf("Got it!\n");
                // open a handle
                if( libusb_open( pDevice, &pHandle ) == 0 )
                {
                    bDevFound = true;
                    printf("Handle opened successfully!\n");
                }
                break;
            }
        }
        else
        {
            printf("Failed to get device descriptor\n");
        }
    
    }
    // free dev list and unreference all the devices
    libusb_free_device_list( ppDevList, 1 );
    
    return bDevFound;
}

void print_health(uint8_t* p_report)
{
    uint32_t tmp = 0x00;
    
    //EP0BUF[0] = IOA;
    printf("IOA: %02x\n", *p_report++);
    //EP0BUF[1] = IOB;
    printf("IOB: %02x\n", *p_report++);
    //EP0BUF[2] = IOC;
    printf("IOC: %02x\n", *p_report++);
    //EP0BUF[3] = IOD;
    printf("IOD: %02x\n", *p_report++);
    //EP0BUF[4] = IOE;
    printf("IOE: %02x\n", *p_report++);
    //EP0BUF[5] = OEA;
    printf("OEA: %02x\n", *p_report++);
    //EP0BUF[6] =OEB;
    printf("OEB: %02x\n", *p_report++);
    //EP0BUF[7] = OEC;
    printf("OEC: %02x\n", *p_report++);
    //EP0BUF[8] = OED;
    printf("OED: %02x\n", *p_report++);
    //EP0BUF[9] = OEE;
    printf("OEE: %02x\n", *p_report++);
    //EP0BUF[10] = EP2468STAT;
    printf("EP2468STAT: %02x\n", *p_report);
    printf("    EP6 Full: %s\n", (*p_report & 0x20) ? "true" : "false");
    printf("    EP6 Empty: %s\n", (*p_report & 0x10) ? "true" : "false");
    printf("    EP2 Full: %s\n", (*p_report & 0x02) ? "true" : "false");
    printf("    EP2 Empty: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[11] = EP24FIFOFLGS;
    printf("EP24FIFOFLGS: %02x\n", *p_report);
    printf("    EP2 PF: %s\n", (*p_report & 0x04) ? "true" : "false");
    printf("    EP2 EF: %s\n", (*p_report & 0x02) ? "true" : "false");
    printf("    EP2 FF: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[12] = EP68FIFOFLGS;
    printf("EP68FIFOFLGS: %02x\n", *p_report);
    printf("    EP6 PF: %s\n", (*p_report & 0x04) ? "true" : "false");
    printf("    EP6 EF: %s\n", (*p_report & 0x02) ? "true" : "false");
    printf("    EP6 FF: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[13] = EP01STAT;
    printf("EP01STAT: %02x\n", *p_report);
    printf("    EP0 Busy: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[14] = GPIFTRIG;
    printf("GPIFTRIG: %02x\n", *p_report++);
    //EP0BUF[15] = GPIFSGLDATLX;
    printf("GPIFSGLDATLX: %02x\n", *p_report++);
    //EP0BUF[16] = CPUCS;
    printf("CPUCS: %02x\n", *p_report++);
    //EP0BUF[17] = IFCONFIG;
    printf("IFCONFIG: %02x\n", *p_report);
    printf("    GPIF FIFO: %s\n", (*p_report++ & 0x03) ? "true" : "false");
    //EP0BUF[18] = PINFLAGSAB;
    printf("PINFLAGSAB: %02x\n", *p_report++);
    //EP0BUF[19] = PINFLAGSCD;
    printf("PINFLAGSCD: %02x\n", *p_report++);
    //EP0BUF[20] = FIFORESET;
    printf("FIFORESET: %02x\n", *p_report++);
    //EP0BUF[21] = FIFOPINPOLAR;
    printf("FIFOPINPOLAR: %02x\n", *p_report++);
    //EP0BUF[22] = REVCTL;
    printf("REVCTL: %02x\n", *p_report++);
    //EP0BUF[23] = EP1OUTCFG;
    printf("EP1OUTCFG: %02x\n", *p_report++);
    //EP0BUF[24] = EP1INCFG;
    printf("EP1INCFG: %02x\n", *p_report++);
    //EP0BUF[25] = EP2CFG;
    printf("EP2CFG: %02x\n", *p_report++);
    //EP0BUF[26] = EP6CFG;
    printf("EP6CFG: %02x\n", *p_report++);
    //EP0BUF[27] = EP2FIFOCFG;
    printf("EP2FIFOCFG: %02x\n", *p_report++);
    //EP0BUF[28] = EP6FIFOCFG;
    printf("EP6FIFOCFG: %02x\n", *p_report++);
    //EP0BUF[29] = NAKIRQ;
    printf("NAKIRQ: %02x\n", *p_report++);
    //EP0BUF[30] = USBIRQ;
    printf("USBIRQ: %02x\n", *p_report++);
    //EP0BUF[31] = EPIRQ;
    printf("USBIRQ: %02x\n", *p_report++);
    //EP0BUF[32] = USBCS;
    printf("USBCS: %02x\n", *p_report++);
    //EP0BUF[33] = EPIRQ;
    printf("EPIRQ: %02x\n", *p_report++);
    //EP0BUF[34] = EP2BCH;
    //EP0BUF[35] = EP2BCL;
    tmp = *p_report << 8;
    tmp |= *(p_report+1);
    printf("EP2BCH: %02x\n", *p_report++);
    printf("EP2BCL: %02x\n", *p_report++);
    printf("    EP2 Byte Count: %d\n", tmp);
    //EP0BUF[36] = EP6BCH;
    //EP0BUF[37] = EP6BCL;
    tmp = *p_report << 8;
    tmp |= *(p_report+1);
    printf("EP6BCH: %02x\n", *p_report++);
    printf("EP6BCL: %02x\n", *p_report++);
    printf("    EP6 Byte Count: %d\n", tmp);
    //EP0BUF[38] = EP0CS;
    printf("EP0CS: %02x\n", *p_report);
    printf("    EP0 Busy: %s\n", (*p_report & 0x02) ? "true" : "false");
    printf("    EP0 Stall: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[39] = EP2CS;
    printf("EP2CS: %02x\n", *p_report);
    printf("    EP2 Packet Count: %d\n", (*p_report & 0x70) >> 4);
    printf("    EP2 Full: %s\n", (*p_report & 0x08) ? "true" : "false");
    printf("    EP2 Empty: %s\n", (*p_report & 0x04) ? "true" : "false");
    printf("    EP2 Stall: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[40] = EP6CS;
    printf("EP6CS: %02x\n", *p_report);
    printf("    EP6 Packet Count: %d\n", (*p_report & 0x70) >> 4);
    printf("    EP6 Full: %s\n", (*p_report & 0x08) ? "true" : "false");
    printf("    EP6 Empty: %s\n", (*p_report & 0x04) ? "true" : "false");
    printf("    EP6 Stall: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[41] = EP2FIFOFLGS;
    printf("EP2FIFOFLGS: %02x\n", *p_report);
    printf("    EP2 FIFO PF: %s\n", (*p_report & 0x04) ? "true" : "false");
    printf("    EP2 FIFO EF: %s\n", (*p_report & 0x02) ? "true" : "false");
    printf("    EP2 FIFO FF: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[42] = EP6FIFOFLGS;
    printf("EP6FIFOFLGS: %02x\n", *p_report);
    printf("    EP6 FIFO PF: %s\n", (*p_report & 0x04) ? "true" : "false");
    printf("    EP6 FIFO EF: %s\n", (*p_report & 0x02) ? "true" : "false");
    printf("    EP6 FIFO FF: %s\n", (*p_report++ & 0x01) ? "true" : "false");
    //EP0BUF[43] = EP2FIFOBCH;
    //EP0BUF[44] = EP2FIFOBCL;
    tmp = *p_report << 8;
    tmp |= *(p_report+1);
    printf("EP2FIFOBCH: %02x\n", *p_report++);
    printf("EP2FIFOBCL: %02x\n", *p_report++);
    printf("    EP2 FIFO Byte Count: %d\n", tmp);
    //EP0BUF[45] = EP6FIFOBCH;
    //EP0BUF[46] = EP6FIFOBCL;
    tmp = *p_report << 8;
    tmp |= *(p_report+1);
    printf("EP6FIFOBCH: %02x\n", *p_report++);
    printf("EP6FIFOBCL: %02x\n", *p_report++);
    printf("    EP6 FIFO Byte Count: %d\n", tmp);
    //EP0BUF[47] = EP2AUTOINLENH;
    //EP0BUF[48] = EP2AUTOINLENL;
    tmp = *p_report << 8;
    tmp |= *(p_report+1);
    printf("EP2AUTOINLENH: %02x\n", *p_report++);
    printf("EP2AUTOINLENL: %02x\n", *p_report++);
    printf("    EP2 Auto In Length: %d\n", tmp);
    printf("EP2 ISR Rolling Counter: %d\n", *p_report++);
}

int main(int argc, char *argv[])
{
    uint8_t buf[FX2_HEALTH_REPORT_LEN];
    int num_bytes = 0;
    
    if( initialize_usb() != true )
    {
        printf("Error: unable to initialize USB\n");
        return -1;
    }

    if( reserve_usb_dev() != true)
    {
        printf("Error: unable to find/reserve USB device 0x%.4x:0x%.4x\n",
               VENDOR_ID, PRODUCT_ID);
        goto usb_shutdown;
    }

    num_bytes = libusb_control_transfer(pHandle,               
                                        READ_REQUEST_TYPE,     
                                        CMD_READ_FX2_HEALTH,                               
                                        0,
                                        0,
                                        buf,
                                        FX2_HEALTH_REPORT_LEN,               
                                        5000 );
    if (FX2_HEALTH_REPORT_LEN != num_bytes) {
        printf("Error: health report failed\n");
        printf("Error: received %d of %d bytes\n", 
               num_bytes, FX2_HEALTH_REPORT_LEN);
        goto usb_shutdown;
    }

    print_health(buf);

usb_shutdown:
    shutdown_usb();

    return 0;
}
