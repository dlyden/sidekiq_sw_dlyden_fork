#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <stdbool.h>
#include <errno.h>

#include <string.h>

#include "sidekiq_usb_cmds.h"
#include "flash_defines.h"

#define MIN(a,b)                        (((a) < (b)) ? (a) : (b))
#define BUF_SIZE                        (10000)
#define MAX_COMMAND_SIZE                (16)

#define CHECK_N(_n,_e)                  \
    do {                                \
        if ( (_n) != (_e) )             \
        {                               \
            printf("not enough entries (expected %d, got %d)\n", (_e), (_n)); \
            continue;                   \
        }                               \
    } while (0);

struct libusb_context *pContext = NULL;
struct libusb_device_handle *pHandle = NULL;

static int
read_long_int( long int *value )
{
    int local_errno = 0;
    int local_value = 0;
    char command[MAX_COMMAND_SIZE] = { 0x00 };

    // This is in a loop as sometimes scanf() will leave an additional
    // newline in the buffer; this should flush the newling and wait for
    // the next command.
    do
    {
        errno = 0;
        if (NULL == fgets(&(command[0]), MAX_COMMAND_SIZE, stdin))
        {
            local_errno = errno;
            return local_errno;
        }
    } while ((1 == strlen(command)) &&
             ((0x0A == command[0]) || (0x0D == command[0])));

    errno = 0;
    local_value = strtol(&(command[0]), NULL, 10);
    if (0 != errno)
    {
        local_errno = errno;
    }
    else if ((LONG_MIN == local_value) || (LONG_MAX == local_value))
    {
        local_errno = EINVAL;
    }

    if ((0 == local_errno) && (NULL != value))
    {
        *value = local_value;
    }

    return (local_errno);
}


bool initialize_usb(void)
{
    if( libusb_init(&pContext) != 0 )
    {
        return false;
    }
    libusb_set_debug( pContext, 3 ); // maximum debug
    return true;
}

void shutdown_usb(void)
{
    if( pContext != NULL )
    {
        libusb_exit(pContext);
        pContext = NULL;
    }
}

bool reserve_usb_dev(void)
{
    ssize_t num_devices = 0;
    bool bDevFound = false;

    struct libusb_device **ppDevList;
    struct libusb_device *pDevice;
    struct libusb_device_descriptor desc;

    int i=0;

    num_devices = libusb_get_device_list( pContext, &ppDevList );
    printf("Found %zd USB devices\n", num_devices);
    for( i=0; i<num_devices; i++ )
    {
        pDevice = ppDevList[i];
        // get the descriptor
        if( libusb_get_device_descriptor( pDevice, &desc ) == 0 )
        {
            if( desc.idVendor == VENDOR_ID && desc.idProduct == PRODUCT_ID )
            {
                printf("Got it!\n");
                // open a handle
                if( libusb_open( pDevice, &pHandle ) == 0 )
                {
                    bDevFound = true;
                    printf("Handle opened successfully!\n");
                }
                break;
            }
        }
        else
        {
            printf("Failed to get device descriptor\n");
        }
    }
    // free dev list and unreference all the devices
    libusb_free_device_list( ppDevList, 1 );

    return bDevFound;
}

void release_usb_dev(void)
{
    if( pHandle != NULL )
    {
    libusb_close( pHandle );
    pHandle = NULL;
    }
}

int write_usb_cmd( SIDEKIQ_USB_CMDS cmd,
           uint8_t *pAddr,
           uint16_t len,
           uint8_t *pData )
{
    int num_bytes = 0;

    num_bytes =
    libusb_control_transfer( pHandle,               // USB device handle
                 WRITE_REQUEST_TYPE,                // requestType SETUPDAT[0]
                 cmd,                               // request=SETUPDAT[1]
                 *((uint16_t*)(pAddr)),             // wValue=SETUPDAT[2:3]
                 0,                                 // wIndex=SETUPDAT[4:5]
                 pData,                             // data
                 len,                               // wLength=SETUPDAT[6:7]
                 0 );//20000 );                     // timeout of 100 ms

    printf("Desired bytes sent %u, actual bytes sent %d\n",
       len, num_bytes);
    return num_bytes;
}

int read_usb_cmd( SIDEKIQ_USB_CMDS cmd,
          uint8_t *pAddr,
          uint16_t len,
          uint8_t *pData )
{
    int num_bytes = 0;
    int i=0;

    printf("Setupdat 0x%x, pAddr %u %u\n", *((uint16_t*)(pAddr)), pAddr[0], pAddr[1]);

    num_bytes =
    libusb_control_transfer( pHandle,               // USB device handle
                 READ_REQUEST_TYPE,                 // requestType SETUPDAT[0]
                 cmd,                               // request=SETUPDAT[1]
                 *((uint16_t*)(pAddr)),             // wValue=SETUPDAT[2:3]
                 0,                                 // wIndex=SETUPDAT[4:5]
                 pData,                 // data
                 len,                   // wLength=SETUPDAT[6:7]
                 5000 );                 // timeout of 100 ms

    printf("Desired bytes read %u, actual bytes read %d\n",
       len, num_bytes);

    int count = 0;
    printf("\t%04X\t", 0x0000);
    for( i=0; i<num_bytes; i++ )
    {
        printf("%.2X ", pData[i]);
        count++;
        if (16 == count)
        {
            printf("\n\t%04X\t", (i+1));
            count = 0;
        }
    }
    printf("\n");
    return num_bytes;
}

int main(int argc, char *argv[])
{
    int res = 0;
    long int choice = 0;
    int n = 0;

    if( initialize_usb() != true )
    {
        printf("Error: unable to initialize USB\n");
        return -1;
    }

    if( reserve_usb_dev() != true)
    {
        printf("Error: unable to find/reserve USB device 0x%.4x:0x%.4x\n",
               VENDOR_ID, PRODUCT_ID);
        goto usb_shutdown;
    }

    while( choice != -1 )
    {
        printf("Commands supported\n");
        printf("\t1) CMD_READ_LAST_TRANS_STATUS\n");
        printf("\t2) CMD_READ_LED_STATE\n");
        printf("\t3) CMD_WRITE_LED_STATE\n");
        printf("\t4) CMD_READ_VERSION\n");
        printf("\t5) CMD_WRITE_VERSION\n");
        printf("\t6) CMD_READ_I2C\n");
        printf("\t7) CMD_WRITE_I2C\n");
        printf("\t8) CMD_READ_FLASH\n");
        printf("\t9) CMD_WRITE_FLASH\n");
        printf("\t10) CMD_INIT_FPGA_PROGRAM\n");
        printf("\t11) CMD_PROGRAM_FPGA\n");
        printf("\t12) CMD_FINISH_FPGA_PROGRAM\n");
        printf("\t13) CMD_READ_EEPROM\n");
        printf("\t14) CMD_WRITE_EEPROM\n");
        printf("\t15) CMD_READ_FLASH_ID\n");
        printf("\t16) CMD_FLASH_SECTOR_ERASE\n");
        printf("\t17) CMD_FLASH_BULK_ERASE\n");
        printf("\t18) CMD_READ_FPGA_STATE\n");
        printf("\t19) CMD_INIT_FPGA_FROM_FLASH\n");
        printf("\t20) CMD_READ_FLASH_STATUS\n");
        printf("\t21) CMD_FLASH_RESET\n");
        printf("\t22) CMD_FLASH_RESCUE\n");
        printf("\t23) CMD_READ_PROG_B\n");
        printf("\t24) CMD_WRITE_PROG_B\n");
        printf("\t25) CMD_READ_FPGA_REG\n");
        printf("\t26) CMD_WRITE_FPGA_REG\n");
        printf("\t27) CMD_RESET_FPGA\n");
        printf("\t28) CMD_READ_GPIF_INIT\n");
        printf("\t29) CMD_WRITE_GPIF_INIT\n");
        printf("\t30) CMD_READ_GPIF_TEST\n");
        printf("\t31) CMD_WRITE_GPIF_TEST\n");
        printf("\t32) CMD_GPIF_END_TEST\n");
        printf("\t33) CMD_READ_GPIF_STATE\n");
        printf("\t34) CMD_GPIF_FIFO_STATE\n");
        printf("\t35) CMD_FIFO_ENDPOINT_RESET\n");
        printf("\t36) CMD_READ_IO_EXPANDER\n");
        printf("\t37) CMD_WRITE_IO_EXPANDER\n");
        printf("\t38) CMD_RF_STATE\n");
        printf("\t39) CMD_FPGA_STATE\n");
        printf("\t40) CMD_FPGA_PIN_TOGGLE\n");
        printf("\t41) CMD_READ_CLOCK_REFERENCE\n");
        printf("\t42) CMD_WRITE_CLOCK_REFERENCE\n");
        printf("\t43) CMD_FLASH_READ_MANUFACTURER\n");
        printf("\t44) CMD_FLASH_QUERY_GOLDEN_SECTORS_LOCK\n");
        printf("\t45) CMD_FLASH_LOCK_GOLDEN_SECTORS\n");
        printf("\t46) CMD_FLASH_UNLOCK_GOLDEN_SECTORS\n");
        printf("\t47) CMD_READ_DEBUG_VARS\n");
        printf("Enter command to test (or -1 to quit): ");

        res = read_long_int( &choice );
        if (0 != res)
        {
            printf("Invalid selection (errno %d).\n\n", res);
            choice = 0;
            continue;
        }

        uint8_t addr[2];
        uint8_t buf[BUF_SIZE];

        uint16_t tmp1,tmp2;
        uint32_t tmp32;

        memset( buf, 0, 100 );
        switch( choice )
        {
            case 1:
                // read trans status
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_LAST_TRANS_STATUS,
                              addr,
                              1, // 1 byte to read
                              buf );
                printf("Last transaction status %d\n", (int8_t)(buf[0]));
                break;
            case 2:
                // read LED state
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_WRITE_LED_STATE,
                              addr,
                              1,
                              buf );
                printf("LED state is %u\n", buf[0]);
                break;
            case 3:
                // write LED state
                addr[0] = 0;
                addr[1] = 0;
                printf("LED state to write? (0/1) ");

                res = read_long_int( &choice );
                if (0 != res)
                {
                    printf("Invalid choice.\n");
                    choice = 0;
                    continue;
                }
                if( choice != 0 && choice != 1 )
                {
                    printf("Invalid LED state entered...\n");
                }
                else
                {
                    buf[0] = (uint8_t)(choice);
                    write_usb_cmd( CMD_READ_WRITE_LED_STATE,
                                   addr,
                                   1,
                                   buf );
                }
                break;
            case 4:
                // read version
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_WRITE_VERSION,
                              addr,
                              6,
                              buf );
                tmp1 = buf[0] | (buf[1] << 8);
                tmp2 = buf[2] | (buf[3] << 8);
                buf[3] >>= 4;
                tmp2 &= 0x0FFF;
                printf("Serial number %u, product %u, pcbRev %u, firmware vers %u.%u\n",
                       tmp1, buf[3], tmp2, buf[4], buf[5]);
                break;
            case 5:
                // write version
                addr[0] = 0;
                addr[1] = 0;
                printf("Enter serial number followed by PCB rev ");
                n = scanf("%hu %hu", &tmp1, &tmp2);
                CHECK_N(n,2);

                buf[0] = (uint8_t)(tmp1);
                buf[1] = (uint8_t)(tmp1 >> 8);
                buf[2] = (uint8_t)(tmp2);
                buf[3] = (uint8_t)(tmp2 >> 8);

                printf("buf %u %u %u %u\n", buf[0], buf[1], buf[2], buf[3]);

                write_usb_cmd( CMD_READ_WRITE_VERSION,
                               addr,
                               4,
                               buf );

                break;
            case 6:
                // read I2C
                printf("Enter slave address and register (in hex): ");
                n = scanf("%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("Enter num bytes to receive: ");
                n = scanf("%hu", &tmp1);
                CHECK_N(n,1);
                read_usb_cmd( CMD_READ_WRITE_I2C,
                              addr,
                              MIN(tmp1,BUF_SIZE),
                              buf );
                break;
            case 7:
                // write I2C
                printf("Enter slave address and register (in hex): ");
                n = scanf("%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                for( tmp1=0; tmp1<10; tmp1++ )
                {
                    buf[tmp1] = tmp1;
                }
                write_usb_cmd( CMD_READ_WRITE_I2C,
                               addr,
                               10,
                               buf );
                break;
            case 8:
                // read flash
                printf("Enter address MSB first (in hex): ");
                n = scanf( "%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("Enter num bytes to receive: ");
                n = scanf("%hu", &tmp1);
                CHECK_N(n,1);
                printf("Trying to read %u bytes from addr 0x%x %x\n",
                       tmp1, addr[0], addr[1]);
                read_usb_cmd( CMD_READ_WRITE_FLASH,
                              addr,
                              MIN(tmp1,BUF_SIZE),
                              buf );
                break;
            case 9:
                printf("Enter address MSB first (in hex): ");
                n = scanf("%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("enter num bytes to write: ");
                n = scanf("%hu", &tmp1);
                CHECK_N(n,1);
                printf("enter pattern type (0 or 1): ");
                n = scanf("%hu", &tmp2);
                CHECK_N(n,1);
                printf("Trying to write %d bytes...\n", tmp1);

                for( choice=0; choice<256; choice++ )
                {
                    if( tmp2 == 0 )
                    {
                        //buf[choice] = choice;
                        buf[choice] = 0x11;
                    }
                    else
                    {
                        buf[choice] = 255-choice;
                    }
                    if( buf[choice] == 0 )
                    {
                        // something weird with all 0s
                        buf[choice] = 0x77;
                    }
                    if( choice<4 )
                    {
                        printf("%d ", buf[choice]);
                    }
                }
                printf("\n");
                write_usb_cmd( CMD_READ_WRITE_FLASH,
                               addr,
                               MIN(tmp1,BUF_SIZE),
                               buf );

                // write flash
                break;
            case 10:
                // init FPGA program
                break;
            case 11:
                // program FPGA
                break;
            case 12:
                // finish FPGA
                break;
            case 13:
                // read EEPROM
                printf("Enter address LSB first(in hex): ");
                n = scanf("%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("Enter num bytes to receive: ");
                n = scanf("%hu", &tmp1);
                CHECK_N(n,1);
                read_usb_cmd( CMD_READ_WRITE_EEPROM,
                              addr,
                              MIN(tmp1,BUF_SIZE),
                              buf );

                break;
            case 14:
                // write EEPROM
                printf("Enter address LSB first(in hex): ");
                n = scanf("%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("Enter data to write (in hex): ");
                n = scanf("%hhx", &buf[0]);
                CHECK_N(n,1);
                write_usb_cmd( CMD_READ_WRITE_EEPROM,
                               addr,
                               1,
                               buf );

                break;

            case 15:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_FLASH_ID,
                              addr,
                              10,
                              buf );
                break;

            case 16:
                printf("Enter address MSB first (in hex): ");
                n = scanf( "%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                write_usb_cmd( CMD_FLASH_SECTOR_ERASE,
                               addr,
                               1,
                               buf );
                break;

            case 17:
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_FLASH_BULK_ERASE,
                               addr,
                               1,
                               buf );
                break;

            case 18:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_FPGA_STATE,
                              addr,
                              1,
                              buf );
                break;

            case 19:
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_INIT_FPGA_FROM_FLASH,
                               addr,
                               1,
                               buf );
                break;

            case 20:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_FLASH_STATUS,
                              addr,
                              3,
                              buf );
                break;

            case 21:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_FLASH_RESET,
                              addr,
                              1,
                              buf );
                break;

            case 22:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_FLASH_RESCUE,
                              addr,
                              1,
                              buf );
                break;

            case 23:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_WRITE_FPGA_PROG_B,
                              addr,
                              1,
                              buf );
                break;

            case 24:
                addr[0] = 0;
                addr[1] = 0;
                printf("Enter prog_b state: ");
                n = scanf("%hu", &tmp1);
                CHECK_N(n,1);
                buf[0] = tmp1;
                write_usb_cmd( CMD_READ_WRITE_FPGA_PROG_B,
                               addr,
                               1,
                               buf );
                break;

            case 25:
                // read register
                printf("Enter address MSB first (in hex): ");
                n = scanf( "%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("Trying to read register 0x%x%x\n",
                       addr[0], addr[1]);
                read_usb_cmd( CMD_READ_WRITE_FPGA_REG,
                              addr,
                              4, // always 4 bytes to read...
                              buf );
                break;

            case 26:
                // write register
                printf("Enter address MSB first (in hex): ");
                n = scanf( "%hhx %hhx", &addr[0], &addr[1]);
                CHECK_N(n,2);
                printf("Trying to write register 0x%x%x\n",
                       addr[0], addr[1]);
                printf("Enter register value to write (in hex): ");
                n = scanf("%x", &tmp32);
                CHECK_N(n,1);
                write_usb_cmd( CMD_READ_WRITE_FPGA_REG,
                               addr,
                               4,
                               (uint8_t*)(&tmp32) );
                break;

            case 27:
                // reset fpga
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_RESET_FPGA,
                               addr,
                               1, // 1 byte to read
                               buf );
                break;

            case 28:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_GPIF_INIT_TEST_READ_WRITE,
                              addr,
                              1,
                              buf );
                break;

            case 29:
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_GPIF_INIT_TEST_READ_WRITE,
                               addr,
                               1,
                               buf );
                break;

            case 30:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_GPIF_TEST_READ_WRITE,
                              addr,
                              2,
                              buf );
                break;

            case 31:
                buf[0] = 0x55;
                buf[1] = 0xAA;
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_GPIF_TEST_READ_WRITE,
                               addr,
                               2,
                               buf );
                break;

            case 32:
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_GPIF_END_TEST,
                               addr,
                               1,
                               buf );
                break;

            case 33:
                // read GPIF state
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_GPIF_FIFO_READ_WRITE,
                              addr,
                              1,
                              buf );
                printf( "GPIF state is %u\n", buf[0] );
                break;

            case 34:
                // enable/disable gpif slave fifo
                addr[0] = 0;
                addr[1] = 0;
                printf( "enable slave FIFO interface? (0/1) " );

                res = read_long_int( &choice );
                if (0 != res)
                {
                    printf("Invalid choice.\n");
                    choice = 0;
                    continue;
                }
                if( choice != 0 && choice != 1 )
                {
                    printf( "Invalid choice entered...\n" );
                }
                else
                {
                    buf[0] = (uint8_t)(choice);
                    write_usb_cmd( CMD_GPIF_FIFO_READ_WRITE,
                                   addr,
                                   1,
                                   buf );
                }
                break;
            case 35:
                // reset fifo
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_BULK_IN_RESET,
                               addr,
                               1,
                               buf );
                addr[0] = 0;
                addr[1] = 0;
                write_usb_cmd( CMD_BULK_OUT_RESET,
                               addr,
                               1,
                               buf );
                break;

            case 36:
                // read register
                addr[0] = 0;
                printf("Enter register (in hex): ");
                n = scanf( "%hhx", &addr[1]);
                CHECK_N(n,1);
                printf("Trying to read register 0x%x\n", addr[1]);
                read_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                              addr,
                              1,
                              buf );
                break;

            case 37:
                // write register
                addr[0] = 0;
                printf("Enter register (in hex): ");
                n = scanf( "%hhx", &addr[1]);
                CHECK_N(n,1);
                printf("Trying to write register 0x%x\n", addr[1]);
                printf("Enter register value to write (in hex): ");
                n = scanf("%hhx", &buf[0]);
                CHECK_N(n,1);
                write_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                               addr,
                               1,
                               &buf[0] );
                break;

            case 38:
                // rf dis/en
                addr[0] = 0;
                addr[1] = 0x23;
                printf("Trying to read register 0x%x\n", addr[1]);
                read_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                              addr,
                              1,
                              buf );
                printf("Disable or enable? (0/1): ");
                n = scanf( "%hhx", &buf[1]);
                CHECK_N(n,1);
                if( 1 == buf[1] )
                {
                    buf[0] |= 0x01;
                }
                else
                {
                    buf[0] &= ~(0x01);
                }
                printf("Writing 0x%x to 0x%x\n", buf[0], addr[1]);
                write_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                               addr,
                               1,
                               &buf[0] );
                break;

            case 39:
                // fpga state
                addr[0] = 0;
                addr[1] = 0x23;
                printf("Trying to read register 0x%x\n", addr[1]);
                read_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                              addr,
                              1, 
                              buf );
                printf("Suspend or wake? (0/1): ");
                n = scanf( "%hhx", &buf[1]);
                CHECK_N(n,1);
                if( 1 == buf[1] )
                {
                    buf[0] |= 0x02;
                }
                else
                {
                    buf[0] &= ~(0x02);
                }
                printf("Writing 0x%x to 0x%x\n", buf[0], addr[1]);
                write_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                               addr,
                               1,
                               &buf[0] );
                break;

            case 40:
                // fpga pin toggle
                addr[0] = 0;
                addr[1] = 0x24;
                printf("Trying to read register 0x%x\n", addr[1]);
                read_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                              addr,
                              1,
                              buf );
                printf("Pin state? (0/1): ");
                n = scanf( "%hhx", &buf[1]);
                CHECK_N(n,1);
                if( 1 == buf[1] )
                {
                    buf[0] |= 0x01;
                }
                else
                {
                    buf[0] &= ~(0x01);
                }
                printf("Writing 0x%x to 0x%x\n", buf[0], addr[1]);
                write_usb_cmd( CMD_READ_WRITE_IO_EXPANDER_REG,
                               addr,
                               1,
                               &buf[0] );
                break;

            case 41:
                addr[0] = 0;
                addr[1] = 0;
                read_usb_cmd( CMD_READ_WRITE_REFERENCE,
                              addr,
                              1,
                              buf );
                break;

            case 42:
                addr[0] = 0;
                addr[1] = 0;
                printf("Internal or external? (0/1): ");
                n = scanf( "%hhx", &buf[1]);
                CHECK_N(n,1);
                if( 1 == buf[1] )
                {
                    buf[0] = REFERENCE_CLOCK_EXTERNAL;
                }
                else
                {
                    buf[0] = REFERENCE_CLOCK_INTERNAL;
                }

                write_usb_cmd( CMD_READ_WRITE_REFERENCE,
                               addr,
                               1,
                               buf );
                break;

            case 43:
                addr[0] = 0;
                addr[1] = 0;
                n = read_usb_cmd( CMD_FLASH_READ_MANUFACTURER, addr, 4,
                                  buf );
                if (4 == n)
                {
                    printf("\n\tDetected FLASH chip: %s\n",
                        ((int) FLASH_CHIP_MICRON_N25 == buf[0]) ? "Micron N25" :
                         ((int) FLASH_CHIP_MICRON_MT25Q == buf[0]) ? "Micron MT25Q" :
                          ((int) FLASH_CHIP_WINBOND_W25 == buf[0]) ? "Winbond W25" :
                           ((int) FLASH_CHIP_ISSI_IS25 == buf[0]) ? "ISSI IS25" :
                            ((int) FLASH_CHIP_MACRONIX_MX25 == buf[0]) ? "Macronix MX25" :
                             ((int) FLASH_CHIP_SPANSION_S25 == buf[0]) ? "Spansion S25" :
                             "Unknown");
                    printf("\tManufacturer       : %02X\n", buf[1]);
                    printf("\tDevice ID          : %02X\n", buf[2]);
                    printf("\tDevice Capacity    : %02X\n", buf[3]);
                }
                else
                {
                    printf("\n\tDid not receive expected response (n = %d)\n", n);
                }
                break;

            case 44:
                addr[0] = 0;
                addr[1] = 0;
                n = read_usb_cmd( CMD_FLASH_QUERY_GOLDEN_SECTORS_LOCK, addr, 1,
                              buf );

                if (1 == n)
                {
                    printf("\n\tGolden Sectors are %s (%02X)\n",
                        (0 == buf[0]) ? "unlocked" :
                         (1 == buf[0]) ? "locked" :
                          "indeterminate due to error", buf[0]);
                }
                else
                {
                    printf("\n\tDid not receive expected response (n = %d)\n", n);
                }
                break;

            case 45:
                addr[0] = 0;
                addr[1] = 0;
                n = read_usb_cmd( CMD_FLASH_LOCK_GOLDEN_SECTORS, addr, 1, buf);

                if (1 == n)
                {
                    switch(buf[0])
                    {
                    case 0:
                        printf("\n\tGolden Sectors were locked\n");
                        break;
                    case ENODEV:
                        printf("\n\tThe Flash chip can't be identified\n");
                        break;
                    case ENOSYS:
                        printf("\n\tThe firmware isn't a factory build and doesn't support"
                            " locking / unlocking the Golden Sectors\n");
                        break;
                    case ENOTSUP:
                        printf("\n\tThe platform doesn't support locking / unlocking the"
                            " Golden Sectors\n");
                        break;
                    default:
                        printf("\n\tFailed to lock Golden Sectors (code = %d)\n", buf[0]);
                        break;
                    }
                }
                else
                {
                    printf("\n\tDid not receive expected response (n = %d)\n", n);
                }
                break;

            case 46:
                addr[0] = 0;
                addr[1] = 0;
                n = read_usb_cmd( CMD_FLASH_UNLOCK_GOLDEN_SECTORS, addr, 1,
                              buf );

                if (1 == n)
                {
                    switch(buf[0])
                    {
                    case 0:
                        printf("\n\tGolden Sectors were unlocked\n");
                        break;
                    case ENODEV:
                        printf("\n\tThe Flash chip can't be identified\n");
                        break;
                    case ENOSYS:
                        printf("\n\tThe firmware isn't a factory build and doesn't support"
                            " locking / unlocking the Golden Sectors\n");
                        break;
                    case ENOTSUP:
                        printf("\n\tThe platform doesn't support locking / unlocking the"
                            " Golden Sectors\n");
                        break;
                    default:
                        printf("\n\tFailed to unlock Golden Sectors (code = %d)\n", buf[0]);
                        break;
                    }
                }
                else
                {
                    printf("\n\tDid not receive expected response (n = %d)\n", n);
                }
                break;

            case 47:
                addr[0] = 0;
                addr[1] = 0;
                n = read_usb_cmd( CMD_READ_DEBUG_VARS, addr,
                        USB_CMD_READ_DEBUG_VARS_RESPONSE_LEN, buf);

                if ((1 == n) ||
                    ((USB_CMD_READ_DEBUG_VARS_RESPONSE_LEN == n) && (0xFF == buf[0])))
                {
                    printf("\n\tDebug variables not supported in this firmware\n");
                }
                else if (USB_CMD_READ_DEBUG_VARS_RESPONSE_LEN != n)
                {
                    printf("\n\tFailed to get debug variables (n = %d)\n", n);
                }
                break;

            case -1:
                printf("Exiting...\n");
                break;

            default:
                printf("Invalid choice entered\n");
                break;
        }

        if (-1 != choice)
        {
            choice = 0;
        }
    }

    release_usb_dev();

usb_shutdown:
    shutdown_usb();

    return 0;
}
