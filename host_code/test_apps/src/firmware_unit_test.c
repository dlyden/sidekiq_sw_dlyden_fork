/* <pre>
 * Copyright 2016 Epiq Solutions, All Rights Reserved
 * </pre>*/

/***** Includes *****/
#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include "sidekiq_usb_cmds.h"
#include "flash_defines.h"


/***** Defines *****/
#define VERBOSE(fmt, ...) \
      do { if (_is_verbose) fprintf(stderr, fmt, ##__VA_ARGS__); } while (0)

#define ACCEL_POWER_MODE_REG (0x2D)
#define ACCEL_X_COORD_REG    (0x32)
#define ACCEL_Y_COORD_REG    (0x34)
#define ACCEL_Z_COORD_REG    (0x36)
#define ACCEL_ENABLE_MEASURE (0x08)


/***** Local Variables *****/
// Variables required for libusb to work with the FX2
static struct libusb_context* _p_usb_ctx = NULL;
static struct libusb_device_handle* _p_dev_hdl = NULL;
// Set to true to enable USB debugging info.
static bool _is_verbose = false;


/***** Structs *****/
/*****************************************************************************/
/** Struct used to help abstract some of the boiler plate code.
 
    @param p_name Name of the test to perform.
    @param fn Pointer to function that contains the test to be performed.
    @param status For future use.
    @param do_test For uture use.
*/
struct firmware_test_case {
    char* p_name;
    int32_t (*fn)(void);
    int32_t status;
    bool do_test;
};


/***** Local Function Prototypes *****/
static int32_t _test_led(void);

static int32_t _test_accel(void);

static int32_t _test_eeprom(void);

static int32_t _test_temp(void);

static int32_t _test_flash(void);

static int32_t _init_usb(void);

static void _close_usb(void);

static int32_t _write_usb_cmd( 
    SIDEKIQ_USB_CMDS cmd, 
    uint8_t *p_addr,
    uint8_t *p_data,
    uint16_t len);

static int32_t _read_usb_cmd(
    SIDEKIQ_USB_CMDS cmd,
    uint8_t *p_addr,
    uint8_t *p_data,
    uint16_t len);


/***** Global Function Definitions *****/
/*****************************************************************************/
/** Test the firmware to ensure that it's basic features work as intended.
 
    @param argc Number of command line arguments.
    @param argv Pointer to command line arguments.

    @return int32_t status where 0=success, anything else is an error
*/
int main(
    int argc, 
    char* argv[])
{
    struct firmware_test_case p_test_case[] = {
        {"read flash ID", _test_flash, 0, true},
        {"read temperature", _test_temp, 0, true},
        {"read/write eeprom", _test_eeprom, 0, true},
        {"read/write accelerometer", _test_accel, 0, true},
        {"read/write led", _test_led, 0, true},
    };
    uint32_t test_cases = sizeof(p_test_case) / sizeof(p_test_case[0]);
    uint32_t n = 0;

    if (0 != _init_usb()) {
        printf("Error: failed to connect to FX2\n");
        return -1;
    }
    printf("Info: connected to FX2 device\n");

    // start test case loop - - - - - - - - - - - - - - - - - - - - - - - - - -
    for (n = 0; n < test_cases; n++) {
        if (false == p_test_case[n].do_test) {
            continue;
        }
        
        if (0 != p_test_case[n].fn()) {
            printf("Error: %s failed\n", p_test_case[n].p_name);
            goto main_exit;
        }
        printf("Info: %s succeeded\n\n", p_test_case[n].p_name);
    }
    // end test case loop - - - - - - - - - - - - - - - - - - - - - - - - - - -

main_exit:
    _close_usb();

    return 0;
}


/***** Local Function Definitions *****/
// Test Case Functions ########################################################
/*****************************************************************************/
/** Test the on board LED by toggling it multiple times while checking that
    the reported state of it changes.
    
    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _test_led(void)
{
    uint8_t p_addr[2] = {0, 0};
    uint8_t r_led = 0;
    uint8_t w_led = 0;
    uint16_t len = 1;
    uint32_t n = 0;
    int32_t r = 0;
    
    for (n = 0; n < 100; n++) {
        r = _write_usb_cmd(CMD_READ_WRITE_LED_STATE, p_addr, &w_led, len);
        if (len != r) {
            printf("Error: failed to write LED\n");
            return -1;
        }

        r = _read_usb_cmd(CMD_READ_WRITE_LED_STATE, p_addr, &r_led, len);
        if (len != r) {
            printf("Error: failed to read LED\n");
            return -1;
        }
        
        if (w_led != r_led) {
            printf("Error: was not able to toggle LED\n");
            return -1;
        }
        
        w_led = r_led == 1 ? 0 : 1;
        usleep(50000);
    }
    
    return 0;
}

/*****************************************************************************/
/** Test the on board accelerometer by setting it to perform measurements 
    and then reading from it.
    
    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _test_accel(void)
{
    uint8_t p_addr[2] = {ACCEL_I2C_ADDRESS, 0};
    int16_t p_accel_data[3];
    uint8_t* p_tmp = NULL;
    uint8_t data = 0;
    uint16_t len = 1;
    uint32_t n = 0;
    int32_t r = 0;
    
    p_addr[1] = ACCEL_POWER_MODE_REG;
    data = ACCEL_ENABLE_MEASURE;
    len = 1;
    r = _write_usb_cmd(CMD_READ_WRITE_I2C, p_addr, &data, len);
    if (len != r) {
        printf("Error: failed to write accelerometer\n");
        return -1;
    }

    for (n = 0; n < 3; n++) {
        p_addr[1] = ACCEL_X_COORD_REG + (2*n);
        p_tmp = (uint8_t*) p_accel_data + n;
        len = 2;
        r = _read_usb_cmd(CMD_READ_WRITE_I2C, p_addr, p_tmp, 2);
        if (len != r) {
            printf("Error: failed to read accelerometer\n");
            return -1;
        }
    }
    
    printf(
        "Info: accelerometer x=%d, y=%d, z=%d\n",
        p_accel_data[0],
        p_accel_data[1],
        p_accel_data[2]);

    return 0;
}

/*****************************************************************************/
/** Test the on board EEPROM by reading data, writing it back modified, and
    then reading it back to ensure the value changed.
    
    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _test_eeprom(void)
{
    // TODO: EEPROM only appears to work with single byte writes, multiple 
    // bytes fail to take anything beyond the first byte of data.
    #define EEPROM_TEST_LENGTH 2
    uint16_t eeprom_address = 0x3FE4;
    // LSB first
    uint8_t p_addr[2];
    uint8_t p_w_buf[EEPROM_TEST_LENGTH];
    uint8_t p_r_buf[EEPROM_TEST_LENGTH];
    uint16_t len = EEPROM_TEST_LENGTH;
    int32_t r = 0;
    uint32_t n = 0;

    p_addr[0] = eeprom_address & 0xFF;
    p_addr[1] = (eeprom_address >> 8) & 0xFF;

    r = _read_usb_cmd(CMD_READ_WRITE_EEPROM, p_addr, p_r_buf, len);
    if (len != r) {
        printf("Error: failed to read EEPROM\n");
        return -1;
    }

    for (n = 0; n < len; n++) {
        p_w_buf[n] = p_r_buf[n] != 0xAA ? 0xAA : 0x55;
    }
    
    r = _write_usb_cmd(CMD_READ_WRITE_EEPROM, p_addr, p_w_buf, len);
    if (len != r) {
        printf("Error: failed to write EEPROM\n");
        return -1;
    }
    
    r = _read_usb_cmd(CMD_READ_WRITE_EEPROM, p_addr, p_r_buf, len);
    if (len != r) {
        printf("Error: failed to read EEPROM\n");
        return -1;
    }

    for (n = 0; n < len; n++) {
        if (p_w_buf[n] != p_r_buf[n]) {
            printf(
                "Error: EEPROM address 0x%x, expected 0x%x, read 0x%x\n",
                eeprom_address + n,
                p_w_buf[n],
                p_r_buf[n]);
            return -1;
        }
    }

    return 0;
}

/*****************************************************************************/
/** Test the on board temperature sensor by attempting to read from it.
    
    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _test_temp(void)
{
    uint8_t p_addr[2] = {TEMP_I2C_ADDRESS, 0};
    int8_t data;
    uint16_t len = 1;
    int32_t r = 0;
    
    r = _read_usb_cmd(CMD_READ_WRITE_I2C, p_addr, (uint8_t*) &data, len);
    if (len != r) {
        printf("Error: failed to read temperature sensor\n");
        return -1;
    }

    printf("Info: temperature sensor reading %d degrees Celsius\n", data);

    return 0;
}

/*****************************************************************************/
/** Test the on board flash by erasing a sector, writing a page, reading it 
    back, and check that it matches the write data.
    
    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _test_flash(void)
{
#if 0
    static uint8_t p_w_buf[FLASH_PAGE_SIZE];
    uint32_t n = 0;
#endif

    static uint8_t p_r_buf[FLASH_PAGE_SIZE];
    uint8_t p_addr[2] = {0x00, 0x00};
    uint16_t len = 0;
    int32_t r = 0;

    len = 10;
    r = _read_usb_cmd(CMD_READ_FLASH_ID, p_addr, p_r_buf,  len);
    if (len != r) {
        printf("Error: failed to read flash ID\n");
        return -1;
    }
#if 0
    len = 1;
    r = _write_usb_cmd(CMD_FLASH_SECTOR_ERASE, p_addr, p_w_buf, len);
    if (len != r) {
        printf("Error: failed to erase flash sector\n");
        return -1;
    }
    
    for (n = 0; n < FLASH_PAGE_SIZE; n++) {
        p_w_buf[n] = n & 0xFF;
        p_r_buf[n] = 0;
    }
    
    len = FLASH_PAGE_SIZE;
    r = _write_usb_cmd(CMD_READ_WRITE_FLASH, p_addr, p_w_buf, len);
    if (len != r) {
        printf("Error: failed to write flash\n");
        return -1;
    }

    len = FLASH_PAGE_SIZE;
    r = _read_usb_cmd(CMD_READ_WRITE_FLASH, p_addr, p_r_buf, len);
    if (len != r) {
        printf("Error: failed to read flash\n");
        return -1;
    }
    
    for (n = 0; n < FLASH_PAGE_SIZE; n++) {
        if (p_w_buf[n] != p_r_buf[n]) {
            printf(
                "Error: flash byte %d, expected %u, read %u\n",
                n,
                p_w_buf[n],
                p_r_buf[n]);
            return -1;
        }
    }
#endif
    return 0;
}

// Utility Functions ##########################################################
/*****************************************************************************/
/** Initializes libusb and connects to the first FX2 it finds.

    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _init_usb(void)
{
    struct libusb_device** pp_dev_list;
    struct libusb_device* p_dev;
    struct libusb_device_descriptor desc;
    ssize_t num_devices = 0;
    bool is_found = false;
    uint32_t i = 0;

    if (libusb_init(&_p_usb_ctx) != 0) {
        return -1;
    }
    
    libusb_set_debug(_p_usb_ctx, 3); // maximum debug

    num_devices = libusb_get_device_list(_p_usb_ctx, &pp_dev_list);
    VERBOSE("[USB] %zd devices detected\n", num_devices);
    for (i=0; i < num_devices; i++) {
        p_dev = pp_dev_list[i];
        // get the descriptor
        if (0 != libusb_get_device_descriptor(p_dev, &desc)) {
            continue;
        }
        
        if ((desc.idVendor != VENDOR_ID) || (desc.idProduct != PRODUCT_ID)) {
            continue;
        }
        
        VERBOSE("[USB] found Sidekiq FX2\n");

        if (0 != libusb_open(p_dev, &_p_dev_hdl)) {
            VERBOSE("[USB] failed to obtain handle\n");
        }

        is_found = true;
        VERBOSE("[USB] handle opened successfully\n");
        break;
    }

    // free dev list and unreference all the devices
    libusb_free_device_list(pp_dev_list, 1);

    if (false == is_found) {
        _close_usb();
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/** Closes the device handle to the FX2 and libusb if opened.

    @param none 

    @return int32_t status where 0=success, anything else is an error
*/
void _close_usb(void)
{
    if (_p_dev_hdl != NULL) {
        libusb_close(_p_dev_hdl);
        _p_dev_hdl = NULL;
    }

    if (_p_usb_ctx != NULL) {
        libusb_exit(_p_usb_ctx);
        _p_usb_ctx = NULL;
    }
}

/*****************************************************************************/
/** Performs a usb control write command to the FX2.

    @param cmd Sidekiq FX2 vendor command. 
    @param p_addr Command address bytes. Must be an array of 2 bytes. 
    @param p_data Command data to be written. 
    @param len Byte length of p_data. 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _write_usb_cmd( 
    SIDEKIQ_USB_CMDS cmd, 
    uint8_t *p_addr,
    uint8_t *p_data,
    uint16_t len)
{
    int32_t m = 0;
    int32_t n = 0;

    VERBOSE(
        "[USB] write cmd=0x%x, p_addr[0]=0x%x, p_addr[1]=0x%u, len=%d\n", 
        (unsigned int) cmd, 
        p_addr[0], 
        p_addr[1],
        len);

    n = libusb_control_transfer( 
            _p_dev_hdl,
            WRITE_REQUEST_TYPE,
            cmd,
            *((uint16_t*)(p_addr)),
            0,
            p_data,
            len,
            5000);

    VERBOSE("[USB] wrote %d of %u bytes\n\t", n, len);
    for (m = 0; m < n; m++) {
        VERBOSE("0x%.2x ", p_data[m]);
        if (0 == ((m + 1) % 16)) {
            VERBOSE("\n\t");
        }
    }
    VERBOSE("\n");
    
    return n;
}

/*****************************************************************************/
/** Performs a usb control read command to the FX2.

    @param cmd Sidekiq FX2 vendor command. 
    @param p_addr Command address bytes. Must be an array of 2 bytes. 
    @param p_data Pointer to where read data should be stored. 
    @param len Bytes to read. 

    @return int32_t status where 0=success, anything else is an error
*/
int32_t _read_usb_cmd(
    SIDEKIQ_USB_CMDS cmd,
    uint8_t *p_addr,
    uint8_t *p_data,
    uint16_t len)
{
    int32_t m = 0;
    int32_t n = 0;

    VERBOSE(
        "[USB] read cmd=0x%x, p_addr[0]=0x%x, p_addr[1]=0x%u, len=%d\n", 
        (unsigned int) cmd, 
        p_addr[0], 
        p_addr[1],
        len);

    n = libusb_control_transfer( 
            _p_dev_hdl,
            READ_REQUEST_TYPE,
            cmd,
            *((uint16_t*)(p_addr)),
            0,
            p_data,
            len,
            5000);


    VERBOSE("[USB] read %d of %u bytes\n\t", n, len);
    
    for (m = 0; m < n; m++) {
        VERBOSE("0x%.2x ", p_data[m]);
        if (0 == ((m + 1) % 16)) {
            VERBOSE("\n\t");
        }
    }
    VERBOSE("\n");
    
    return n;
}
