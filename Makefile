# include tools and build rules
BUILD_ROOT ?= $(CURDIR)/build

include $(BUILD_ROOT)/top.mk

# define PROJECT_ROOT for build output directory
export PROJECT_ROOT:= $(CURDIR)
export SIDEKIQ_ROOT:= $(PROJECT_ROOT)

# map aliases (y,yes,1) to 'enabled' for ATE_SUPPORT
ifneq ($(filter $(call lc,$(ATE_SUPPORT)),y yes 1),)
override ATE_SUPPORT:=enabled
endif

# map aliases (y,yes,1) to 'enabled' for HWDEV_SUPPORT
ifneq ($(filter $(call lc,$(HWDEV_SUPPORT)),y yes 1),)
override HWDEV_SUPPORT:=enabled
endif

# export CFLAGS_GLOBAL
CFLAGS_GLOBAL= -std=gnu11 -D_GIT_HASH=\"$(shell git rev-parse --short HEAD)\"
ifeq ($(HWDEV_SUPPORT),enabled)
CFLAGS_GLOBAL+= -DHWDEV_SUPPORT=1
export ATE_SUPPORT=enabled
endif
ifeq ($(ATE_SUPPORT),enabled)
CFLAGS_GLOBAL+= -DATE_SUPPORT=1
endif
CFLAGS_GLOBAL+= -pipe
export CFLAGS_GLOBAL:= $(CFLAGS_GLOBAL) -Werror=implicit -Werror=format -Werror=shadow
export CFLAGS_GLOBAL:= $(CFLAGS_GLOBAL) -fno-asynchronous-unwind-tables -fno-exceptions
ifneq ($(DEBUG),enabled)
export CFLAGS_GLOBAL:= $(CFLAGS_GLOBAL) -fno-unwind-tables
endif

ifeq ($(ENABLE_ELAPSED),enabled)
CFLAGS_GLOBAL+= -DENABLE_ELAPSED=1
endif

# primary build product for this repo
LIB=.libsidekiq__$(SUFFIX).a
LIB_NO_FACT=libsidekiq__$(SUFFIX).a

# child libraries that roll up into the primary build product LIB
LIB_CHILD+= host_code/libsidekiq/libsidekiq__$(SUFFIX).a

# what to export
PUBLIC_LIBS= $(LIB_NO_FACT)
PUBLIC_HEADERS= \
    host_code/libsidekiq/sidekiq_core/inc/sidekiq_api.h \
    host_code/libsidekiq/sidekiq_core/inc/sidekiq_types.h \
    host_code/libsidekiq/sidekiq_core/inc/sidekiq_params.h \
    host_code/libsidekiq/transport/inc/sidekiq_xport_api.h \
    host_code/libsidekiq/transport/inc/sidekiq_xport_types.h

# API symbols
ifeq ($(HWDEV_SUPPORT),enabled)
INTERFACE_FILE = API_INTERFACE_HWDEV
else ifeq ($(ATE_SUPPORT),enabled)
INTERFACE_FILE= API_INTERFACE_ATE
else
INTERFACE_FILE= API_INTERFACE
endif

# required include
include $(BUILD_ROOT)/bottom.mk


####################################################################################################
# remove all factory functions from the library
####################################################################################################
REMOVE_FACT:=no

#
# objcopy does not work in the use case of STRIP_ENABLED=disabled and REMOVE_FACT=yes
#
# if all of (ATE_SUPPORT, DEBUG, HWDEV_SUPPORT) are disabled AND if STRIP_ENABLED is enabled, then
# remove factory functions.  otherwise the user wants the factory functions or does not mind if they
# are retained
#
ifeq ($(filter enabled,$(ATE_SUPPORT) $(DEBUG) $(HWDEV_SUPPORT)),)
ifneq ($(filter enabled,$(STRIP_ENABLED)),)
REMOVE_FACT:=yes
endif
endif

ifeq ($(REMOVE_FACT),yes)
$(LIB_NO_FACT): $(LIB)
	$(OBJCOPY) -w -R '.text.*.skiq_fact_*' -R '.text.skiq_fact_*' \
		 -R '.text.*.dkiq_fact_*' -R '.text.dkiq_fact_*' \
		 -R '.text.*.skiq_hwdev_*' -R '.text.skiq_hwdev_*' \
		 -R '.text.*._skiq_fact_*' -R '.text._skiq_fact_*' \
		 $(LIB) $(LIB_NO_FACT)
else
$(LIB_NO_FACT): $(LIB)
	cp -fv $(LIB) $(LIB_NO_FACT)
endif

.PHONY: print_debug build_test test clean_test test_clean

build_test: $(PUBLIC_LIBS) $(PUBLIC_SUPPORT)
	cd gtest && \
	cmake -DSUFFIX:STRING=$(SUFFIX) CMakeLists.txt && \
	make

test: build_test
	cd gtest && \
	./testAll__${SUFFIX}

clean_test:
	cd gtest && \
	rm -f testAll__* && \
	rm -f Makefile && \
	rm -Rf CMakeFiles && \
	rm -f cmake_install.cmake && \
	rm -f CMakeCache.txt

test_clean:
	make clean_test
	make test