def BUILD_CONFIG="x86_64.gcc"
def REMOTE="sidekiq@192.168.7.89"
def RESOURCE="honeybee"
def GIT_REVISION="*/sidekiq_develop"

def fetch_fpga_bitstream(zip_jenkins_path, zip_name) {
    def URL_BASE="https://jenkins-lts.epiq.rocks/job/"

    BITSTREAM_URL = URL_BASE+"$zip_jenkins_path"
    sh "echo curl -O $BITSTREAM_URL"
    sh "curl -O $BITSTREAM_URL"
    sh "unzip -d $FPGA_DIR -o $zip_name"
}

def pull_fpga_bitstreams() {
    UPSTREAM_PROJECT = UPSTREAM_PROJECT.split('/').join('/job/')
    ARTIFACT_PATH = "$UPSTREAM_PROJECT/$FPGA_BUILD_NUMBER/artifact"
    if(UPSTREAM_PROJECT == "Sidekiq_FPGAs/job/sidekiq-all-fpga-build") {
        // pull sidekiq_mpcie_pdk
        fetch_fpga_bitstream("$ARTIFACT_PATH/.output/build_sidekiq_mpcie_pdk/*zip*/build_sidekiq_mpcie_pdk.zip", "build_sidekiq_mpcie_pdk.zip")
    } else {
        fetch_fpga_bitstream("$ARTIFACT_PATH/*zip*/archive.zip", "archive.zip")
    }
}

def grab_bitstream(patt, ofn) {
    sh "find $FPGA_DIR -name $patt | head -1 | xargs -I BS ln -snf ../BS $FPGA_DIR/$ofn"
}

pipeline {
    agent { label 'sidekiq-testing'}
    stages {
        stage('Checkout SCM') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: GIT_REVISION ]],
                        extensions: [
                            [
                                $class: 'SubmoduleOption',
                                disableSubmodules: false,
                                recursiveSubmodules: true
                            ],
                            [
                                $class: 'CleanBeforeCheckout'    
                            ]
                        ],
                        userRemoteConfigs: [[url: 'git@bitbucket.org:epiq_solutions/sidekiq_sw.git']]
                    ]
                )
                script {
                    GIT_HASH = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                    currentBuild.displayName = "#${currentBuild.number}: ${GIT_BRANCH}[${GIT_HASH}] : ${UPSTREAM_PROJECT}"
                }
            }
        }
        stage('Pull FPGA Bitstreams') {
            steps {
                script {
                    pull_fpga_bitstreams()
                    
                    sh "mkdir -p $FPGA_DIR"
                    grab_bitstream('sidekiq_mpcie_pdk_????????_????????_v*.bit', 'sidekiq_image_mpcie_xport_pcie_rc.bit')
                    grab_bitstream('sidekiq_mpcie_usb_pdk_????????_????????_v*.bit', 'sidekiq_image_mpcie_xport_usb_rc.bit')
                    sh "ls -l $FPGA_DIR"
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    sh "make BUILD_CONFIG=$BUILD_CONFIG V=yes -j8 --output-sync=recurse"
                }
            }
        }
        stage('Test') {
            steps {
                lock(resource: null, label: RESOURCE) {
                    sh "./scripts/skiq_regression_remote.sh $REMOTE /tmp/jenkins --allow-usb-transport --no-load-driver --run-extended $COMMAND_LINE"
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts 'results/'
            script {
                def mailRecipients = "sidekiq-core@epiq-solutions.com,robert@epiqsolutions.com"
                def result = currentBuild.result.toLowerCase().capitalize()
                if (result == "Success") {
                    result = "Successful"
                }
                emailext from: "epiqbuildbot",
                    body: "$currentBuild.fullDisplayName - $result:<br><br>Check console output at $env.BUILD_URL to view the results.",
                    mimeType: 'text/html',
                    subject: "[Jenkins] $currentBuild.fullDisplayName - $result!",
                    to: "$mailRecipients",
                    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            }
            deleteDir()
        }
    }
}