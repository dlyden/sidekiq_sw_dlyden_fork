def BUILD_CONFIG="x86_64.gcc"
def REMOTE="sidekiq@192.168.7.90"
def RESOURCE="giraffe"
def GIT_REVISION="*/sidekiq_develop"

def fetch_fpga_bitstream(zip_jenkins_path, zip_name) {
    def URL_BASE="https://jenkins-lts.epiq.rocks/job/"

    BITSTREAM_URL = URL_BASE+"$zip_jenkins_path"
    sh "echo curl -O $BITSTREAM_URL"
    sh "curl -O $BITSTREAM_URL"
    sh "unzip -d $FPGA_DIR -o $zip_name"
}

def pull_fpga_bitstreams() {
    UPSTREAM_PROJECT = UPSTREAM_PROJECT.split('/').join('/job/')
    ARTIFACT_PATH = "$UPSTREAM_PROJECT/$FPGA_BUILD_NUMBER/artifact"
    if(UPSTREAM_PROJECT == "Sidekiq_FPGAs/job/sidekiq-all-fpga-build") {
        if(HAS_RCS_BITSTREAMS == "false") {
            // pull sidekiq_m2s_pr_pdk
            fetch_fpga_bitstream("$ARTIFACT_PATH/.output/build_sidekiq_m2s_pr_pdk/*zip*/build_sidekiq_m2s_pr_pdk.zip", "build_sidekiq_m2s_pr_pdk.zip")
        } else {
            // pull sidekiq_rcs_m2s_lte
            fetch_fpga_bitstream("$ARTIFACT_PATH/.output/build_sidekiq_rcs_m2s_lte/*zip*/build_sidekiq_rcs_m2s_lte.zip", "build_sidekiq_rcs_m2s_lte.zip")
            
            // pull sidekiq_rcs_m2s_umts
            fetch_fpga_bitstream("$ARTIFACT_PATH/.output/build_sidekiq_rcs_m2s_umts/*zip*/build_sidekiq_rcs_m2s_umts.zip", "build_sidekiq_rcs_m2s_umts.zip")
        }
    } else {
        fetch_fpga_bitstream("$ARTIFACT_PATH/*zip*/archive.zip", "archive.zip")
    }
}

def grab_bitstream(patt, ofn) {
    sh "find $FPGA_DIR -name $patt | head -1 | xargs -I BS ln -snf ../BS $FPGA_DIR/$ofn"
}

def link_bitstream(bitstream, link) {
    sh "cd $FPGA_DIR; ln -snf $bitstream $link; ls -l"
}

pipeline {
    agent { label 'sidekiq-testing'}
    stages {
        stage('Checkout SCM') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: GIT_REVISION ]],
                        extensions: [
                            [
                                $class: 'SubmoduleOption',
                                disableSubmodules: false,
                                recursiveSubmodules: true
                            ],
                            [
                                $class: 'CleanBeforeCheckout'    
                            ]
                        ],
                        userRemoteConfigs: [[url: 'git@bitbucket.org:epiq_solutions/sidekiq_sw.git']]
                    ]
                )
                script {
                    GIT_HASH = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                    currentBuild.displayName = "#${currentBuild.number}: ${GIT_BRANCH}[${GIT_HASH}] : ${UPSTREAM_PROJECT}"
                }
            }
        }
        stage('Pull FPGA Bitstreams') {
            steps {
                script {
                    pull_fpga_bitstreams()
                    
                    sh "mkdir -p $FPGA_DIR"
                    grab_bitstream('sidekiq_m2s_pr_pdk_????????_????????_v*.bin', 'sidekiq_image_m2_2280_xport_pcie_rc.bin')
                    grab_bitstream('sidekiq_rcs_m2s_lte_????????_????????_v*.bin', 'sidekiq_top_m2_2280_LTE.bin')
                    grab_bitstream('sidekiq_rcs_m2s_umts_????????_????????_v*.bin', 'sidekiq_top_m2_2280_UMTS.bin')
                    sh "ls -l $FPGA_DIR"
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    sh "make BUILD_CONFIG=$BUILD_CONFIG V=yes -j8 --output-sync=recurse"
                }
            }
        }
        stage('Test') {
            steps {
                lock(resource: null, label: RESOURCE) {
                    script {
                        if(HAS_RCS_BITSTREAMS == "false") {
                            sh "./scripts/skiq_regression_remote.sh $REMOTE /tmp/jenkins --build-driver-remotely --run-extended $COMMAND_LINE"
                        } else {
                            
                            link_bitstream("sidekiq_top_m2_2280_LTE.bin", "sidekiq_image_m2_2280_xport_pcie_rc.bin")
                            sh "./scripts/skiq_regression_remote.sh --rcs $REMOTE /tmp/jenkins --rcs --build-driver-remotely --run-extended $COMMAND_LINE"
                            
                            link_bitstream("sidekiq_top_m2_2280_UMTS.bin", "sidekiq_image_m2_2280_xport_pcie_rc.bin")
                            sh "./scripts/skiq_regression_remote.sh --rcs $REMOTE /tmp/jenkins --rcs --build-driver-remotely --run-extended $COMMAND_LINE"
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts 'results/'
            script {
                def mailRecipients = "sidekiq-core@epiq-solutions.com,robert@epiqsolutions.com"
                def result = currentBuild.result.toLowerCase().capitalize()
                if (result == "Success") {
                    result = "Successful"
                }
                emailext from: "epiqbuildbot",
                    body: "$currentBuild.fullDisplayName - $result:<br><br>Check console output at $env.BUILD_URL to view the results.",
                    mimeType: 'text/html',
                    subject: "[Jenkins] $currentBuild.fullDisplayName - $result!",
                    to: "$mailRecipients",
                    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            }
            deleteDir()
        }
    }
}