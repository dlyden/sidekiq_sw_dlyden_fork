def BUILD_CONFIG="x86_64.gcc"
def PLATFORM="skiq-pc-linux64"
def REMOTE="sidekiq@192.168.7.46"
def SIGGEN_REMOTE="192.168.7.8:5025"
def RF_SWITCH_HOST="192.168.6.210"
def PORT="A1"
def RESOURCE="billygoat"
def RF_SWITCH="RF-SWITCH-01-A"

pipeline {
    agent { label 'server06' }
    stages {
        stage('Checkout SCM') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: GIT_REVISION ]],
                        extensions: [
                            [
                                $class: 'SubmoduleOption',
                                disableSubmodules: false,
                                recursiveSubmodules: true
                            ],
                            [
                                $class: 'CleanBeforeCheckout'    
                            ]
                        ],
                        userRemoteConfigs: [[url: 'git@bitbucket.org:epiq_solutions/sidekiq_test_hop.git']]
                    ]
                )
                script {
                    GIT_HASH = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                    currentBuild.displayName = "#${currentBuild.number}: ${GIT_REVISION}[${GIT_HASH}] (libsidekiq ${LIBSIDEKIQ_GIT_REVISION})"
                }
            }
        }
        stage('Test') {
            steps {
                lock(resource: null, label: RESOURCE, extra: [[resource: RF_SWITCH]]) {
                    sh "bash rf_telnet_config.sh $RF_SWITCH_HOST $PORT 1 | telnet || echo 'telnet session closed'"
                    sh "bash run_frequency_hopping.sh $REMOTE $SIGGEN_REMOTE $PLATFORM $BUILD_CONFIG $LIBSIDEKIQ_GIT_REVISION $WORKSPACE"
                    sh "bash rf_telnet_config.sh $RF_SWITCH_HOST $PORT 0 | telnet || echo 'telnet session closed'"
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts 'rx-fh-tester/summary.json, *.epiq-data'
            script {
                def mailRecipients = "sidekiq-core-sw@epiq-solutions.com"
                def result = currentBuild.result.toLowerCase().capitalize()
                if (result == "Success") {
                    result = "Successful"
                }
                emailext from: "epiqbuildbot",
                    body: "$currentBuild.fullDisplayName - $result:<br><br>Check console output at $env.BUILD_URL to view the results.",
                    mimeType: 'text/html',
                    subject: "[Jenkins] $currentBuild.fullDisplayName - $result!",
                    to: "$mailRecipients",
                    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            }
            deleteDir()
        }
    }
}