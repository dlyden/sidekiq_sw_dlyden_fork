def BUILD_CONFIG="arm_cortex-a9.gcc5.2_glibc_openwrt"
def REMOTE="root@192.168.7.55"
def RESOURCE="cow"

pipeline {
    agent { label 'sidekiq-testing' }
    stages {
        stage('Checkout SCM') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: GIT_REVISION ]],
                        extensions: [
                            [
                                $class: 'SubmoduleOption',
                                disableSubmodules: false,
                                recursiveSubmodules: true
                            ],
                            [
                                $class: 'CleanBeforeCheckout'    
                            ]
                        ],
                        userRemoteConfigs: [[url: 'git@bitbucket.org:epiq_solutions/sidekiq_sw.git']]
                    ]
                )
                script {
                    GIT_HASH = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                    currentBuild.displayName = "#${currentBuild.number}: ${GIT_REVISION}[${GIT_HASH}]"
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    sh "make BUILD_CONFIG=$BUILD_CONFIG V=yes -j8 --output-sync=recurse HAS_X2_SUPPORT=no HAS_X4_SUPPORT=no HAS_NV100_SUPPORT=no"
                }
            }
        }
        stage('Test') {
            steps {
                lock(resource: null, label: RESOURCE) {
                    sh "./scripts/skiq_regression_remote.sh $REMOTE /tmp/jenkins --pps --run-extended $COMMAND_LINE"
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts 'results/'
            script {
                def mailRecipients = "sidekiq-core-sw@epiq-solutions.com"
                def result = currentBuild.result.toLowerCase().capitalize()
                if (result == "Success") {
                    result = "Successful"
                }
                emailext from: "epiqbuildbot",
                    body: "$currentBuild.fullDisplayName - $result:<br><br>Check console output at $env.BUILD_URL to view the results.",
                    mimeType: 'text/html',
                    subject: "[Jenkins] $currentBuild.fullDisplayName - $result!",
                    to: "$mailRecipients",
                    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            }
            deleteDir()
        }
    }
}