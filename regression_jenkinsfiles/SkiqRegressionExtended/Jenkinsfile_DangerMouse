def BUILD_CONFIG="arm_cortex-a9.gcc7.2.1_gnueabihf"
def REMOTE_IP="192.168.7.45"
def REMOTE="root@$REMOTE_IP"
def RESOURCE="dangermouse"

pipeline {
    agent { label 'sidekiq-testing' }
    stages {
        stage('Checkout SCM') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: GIT_REVISION ]],
                        extensions: [
                            [
                                $class: 'SubmoduleOption',
                                disableSubmodules: false,
                                recursiveSubmodules: true
                            ],
                            [
                                $class: 'CleanBeforeCheckout'    
                            ]
                        ],
                        userRemoteConfigs: [[url: 'git@bitbucket.org:epiq_solutions/sidekiq_sw.git']]
                    ]
                )
                script {
                    GIT_HASH = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
                    currentBuild.displayName = "#${currentBuild.number}: ${GIT_REVISION}[${GIT_HASH}]"
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    sh "make BUILD_CONFIG=$BUILD_CONFIG V=yes -j8 --output-sync=recurse HAS_X2_SUPPORT=no HAS_X4_SUPPORT=no HAS_NV100_SUPPORT=no"
                }
            }
        }
        stage('Test') {
            steps {
                lock(resource: null, label: RESOURCE) {
                    // REBOOT before testing to get the BSP's built-in FPGA
                    sh "echo '===== REBOOTING TARGET =====' >&2"
                    sh "ssh $REMOTE '/sbin/reboot; exit'"
                    sleep 10
                    script {
                        for(int i=0; i<20; i++) {
                            def status = sh(script: "nc -z -v -w3 $REMOTE_IP 22", returnStatus: true)
                            if(status == 0) {
                                break
                            }
                            if(i >= 19) {
                                sh "echo 'ERROR: Failed to reconnect to device after reboot'"
                                currentBuild.result = "FAILURE"
                                sh "exit 1"
                            }
                        }
                    }
                    
                    // Test with --no-load-fpga
                    sh "./scripts/skiq_regression_remote.sh $REMOTE /tmp/jenkins --no-load-driver --no-load-fpga --run-extended $COMMAND_LINE"
                    sh "mv -v results results-no-load-fpga"
                    
                    // Test without --no-load-fpga
                    sh "./scripts/skiq_regression_remote.sh $REMOTE /tmp/jenkins --no-load-driver --run-extended $COMMAND_LINE"
                    sh "mv -v results results-load-fpga"
                    
                    // Accumulate results
                    sh "mkdir -p results"
                    sh "mv -v results-* results/"
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts 'results/'
            script {
                def mailRecipients = "sidekiq-core-sw@epiq-solutions.com"
                def result = currentBuild.result.toLowerCase().capitalize()
                if (result == "Success") {
                    result = "Successful"
                }
                emailext from: "epiqbuildbot",
                    body: "$currentBuild.fullDisplayName - $result:<br><br>Check console output at $env.BUILD_URL to view the results.",
                    mimeType: 'text/html',
                    subject: "[Jenkins] $currentBuild.fullDisplayName - $result!",
                    to: "$mailRecipients",
                    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            }
            deleteDir()
        }
    }
}