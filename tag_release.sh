#! /bin/bash

if [ $# -ne 1 ]; then
    echo "Error: no version passed in (of the form Major.Minor.Patch, such as 0.12.0)"
    exit 1
fi

read -r -d '' REPOS <<EOF
libraries/ad9361_driver
libraries/ad9371_driver
libraries/ad9379_driver
libraries/ad9528_driver
libraries/adrv9002_driver
libraries/dropkiq
libraries/libiio
libraries/librffc5071a
libraries/libusb
libraries/nw_logic_dma
libraries/pci_manager
libraries/sidekiq_fw
sidekiq_fpga_bitstreams
EOF

TAG=libsidekiq_v${1}

while true; do
    read -n1 -p "Do you wish to tag the submodules with '$TAG' (y/N)? " yn
    if [ -z "$yn" ]; then
        yn="n"
    else
        echo
    fi
    case "$yn" in
        y|Y)
            break
            ;;
        *)
            exit
            ;;
    esac
done

###################################################
# tag-repo <DIR> <VERSION>
#
function tag-repo()
{
    local DIR TAG

    DIR=$1
    TAG=$2

    echo "===== Tagging '$(basename "$DIR")' with '$TAG'"

    (cd "$DIR";
     git tag "$TAG" &&
         git push origin "$TAG")
}

for repo in $REPOS; do
    tag-repo "$repo" "$TAG"
done
